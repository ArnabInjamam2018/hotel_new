<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    function __construct() {
        /* Session Checking Start */

        parent::__construct();
	   
        $this->arraay = $this->session->userdata('per');
        $this->load->model('account_model');
        $this->load->model('setting_model');
        $this->load->model('dashboard_model');
        $this->load->model('unit_class_model');
        $this->load->model('bookings_model');
        $this->load->library('upload');

        $this->load->model('superadminmodel/super_model');
        /* Session Checking End */

        // loading library.


        /* URL Value Encryption Start */
        function base64url_encode($data) {
            return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
        }

        /* URL Value Encryption End */

        /* URL Value Decryption Start */

        function base64url_decode($data) {
            return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
        }

        /* URL Value Decryption End */
    }

    //add_master_account

    function index() {
        $this->load->view('newbackend/dashboard1/index1');
    }

    function user_login() {


        $data['heading'] = '';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = '';
        $this->form_validation->set_rules('user_name', 'Username', 'trim|required|min_length[5]|max_length[50]|xss_clean');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required|min_length[5]|max_length[50]|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $data['msg'] = validation_errors();
            $this->load->view('newbackend/dashboard1/index1', $data);
        } else {

            $user_name = $this->input->post('user_name');
            $user_password = sha1(md5($this->input->post('user_password')));


            //echo "hello";
            $login_data = $this->super_model->login($user_name, $user_password);
			
            if (isset($login_data) && $login_data != '') {

                $newdata = array(
                    'username' => $login_data->user_name,
                    'email' => $login_data->user_email,
                    'status' => $login_data->user_status,
                    'user_id' => $login_data->id,
                    'per' => 'admin',
                    'logged_in' => TRUE
                );

                $this->session->set_userdata($newdata);

                function getBrowser() {
                    $u_agent = $_SERVER['HTTP_USER_AGENT'];
                    $bname = 'Unknown';
                    $platform = 'Unknown';
                    $version = "";

                    //First get the platform?
                    if (preg_match('/linux/i', $u_agent)) {
                        $platform = 'Linux';
                    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
                        $platform = 'Mac';
                    } elseif (preg_match('/windows|win32/i', $u_agent)) {
                        $platform = 'Windows';
                    }

                    // Next get the name of the useragent yes seperately and for good reason
                    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
                        $bname = 'Internet Explorer';
                        $ub = "MSIE";
                    } elseif (preg_match('/Firefox/i', $u_agent)) {
                        $bname = 'Mozilla Firefox';
                        $ub = "Firefox";
                    } elseif (preg_match('/Chrome/i', $u_agent)) {
                        $bname = 'Google Chrome';
                        $ub = "Chrome";
                    } elseif (preg_match('/Safari/i', $u_agent)) {
                        $bname = 'Apple Safari';
                        $ub = "Safari";
                    } elseif (preg_match('/Opera/i', $u_agent)) {
                        $bname = 'Opera';
                        $ub = "Opera";
                    } elseif (preg_match('/Netscape/i', $u_agent)) {
                        $bname = 'Netscape';
                        $ub = "Netscape";
                    }

                    // finally get the correct version number
                    $known = array('Version', $ub, 'other');
                    $pattern = '#(?<browser>' . join('|', $known) .
                            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
                    if (!preg_match_all($pattern, $u_agent, $matches)) {
                        // we have no matching number just continue
                    }

                    // see how many we have
                    $i = count($matches['browser']);
                    if ($i != 1) {
                        //we will have two since we are not using 'other' argument yet
                        //see if version is before or after the name
                        if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                            $version = $matches['version'][0];
                        } else {
                            $version = $matches['version'][1];
                        }
                    } else {
                        $version = $matches['version'][0];
                    }

                    // check if we have a number
                    if ($version == null || $version == "") {
                        $version = "?";
                    }

                    return array(
                        'userAgent' => $u_agent,
                        'name' => $bname,
                        'version' => $version,
                        'platform' => $platform,
                        'pattern' => $pattern
                    );
                }

// End function getBrowser
                $ua = getBrowser();
                $yourbrowser = $ua['name'] . " " . $ua['version'] . " on " . $ua['platform'];
                $getloc = json_decode(file_get_contents('http://ipinfo.io/'));

                $coordinates = explode(",", $getloc->loc); // -> '32,-72' becomes'32','-72'
                $latitude = $coordinates[0]; // latitude
                $longitude = $coordinates[1];

                function getAddress($latitude, $longitude) {
                    if (!empty($latitude) && !empty($longitude)) {
                        //Send request and receive json data by address
                        $geocodeFromLatLong = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($latitude) . ',' . trim($longitude) . '&sensor=false');
                        $output = json_decode($geocodeFromLatLong);
                        $status = $output->status;
                        //Get address from json data
                        $address = ($status == "OK") ? $output->results[1]->formatted_address : '';
                        //Return address of the given latitude and longitude
                        if (!empty($address)) {
                            return $address;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }

                $address = getAddress($latitude, $longitude);
                $system_info = array(
                    'user_name' => $login_data->user_name,
                    'user_id' => $login_data->id,
                    'ip_address' => $this->input->ip_address(),
                    'system_info' => $getloc->city,
                    //'system_info' => $getloc->city.''.$address,
                    'browser' => $yourbrowser
                );
                $user_name = $login_data->user_name;
                $login_data = $this->super_model->login_details($system_info, $user_name);


                $this->session->set_userdata($newdata);
				
                //echo '<pre>';
                //print_r($data);exit;
                $_SESSION['user'] = 'super_super_user';
                $data['page'] = 'newbackend/dashboard1/home';
                $this->load->view('newbackend/common/index', $data);
            } else {

                $data['msg'] = "Wrong userId and Password";
                $this->load->view('newbackend/dashboard1/index1', $data);
            }
            //print_r($login_data);
        }
    }
	
	//DATABSE CONNECTION DYNAMIC DATABASE
function db_connection($database,$user,$password){
	$db['superadmin'] = array(
    'dsn'   => '',
    'hostname' => 'localhost',
    'username' => $user,
	'password' => $password,
	'database' => $database,
    //'username' => $this->session->userdata('username'),
   // 'password' => $this->session->userdata('password'),
    //'database' => $this->session->userdata('database'),
   
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => TRUE,
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE 
);
        //echo "<pre>";
	//	echo $this->db1->database;
       //    print_r($db1['default']);exit;
            $CI = &get_instance();
        $this->db1 = $CI->load->database($db['superadmin'],true);
}
// END DYNAMIC DATABASE

    function add_hotel_m(){
        $found = 1;
        if ($found) {
            $data['heading'] = 'Child Account';
            $data['sub_heading'] = 'Hotel Management';
            $data['description'] = 'Add Hotel Here';
            $data['active'] = 'misc_settings';
            $data['hotel_name'] = $this->dashboard_model->get_hotel_s($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                ///print_r($this->input->post());exit;
				
				//databse instance
				$chainID=$this->input->post('hotel_chain_id');
				$unique_database = $this->dashboard_model->get_chain_database($chainID);	
				$user = $unique_database->database_user;
				$password = $unique_database->database_pwd;
				$database = $unique_database->database_name;
				$this->db_connection($database,$user,$password);
				

                $hotel_type = $this->input->post('hotel_type');
                $hotel_type_string = "";
                $hotel_type_string = $hotel_type_string . implode(',', $hotel_type);

                $guest_option = $this->input->post('guest');
                $guest_option_string = "";
                $guest_option_string = $guest_option_string . implode(',', $guest_option);

                $broker_option = $this->input->post('broker');
                $broker_option_string = "";
                $broker_option_string = $broker_option_string . implode(',', $broker_option);
                /* Start Logo Upload */

                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('image_photo')) {
                    //echo "uploaded"; exit();
                    $hotel_logo = $this->upload->data('file_name');
                    $filename = $hotel_logo;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/hotel/';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 100,
                        'height' => 100
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        //redirect('dashboard/add_hotel_m');
                        redirect('newbackend/dashboard1/add_hotel_m');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $logo_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                } else {
                    $hotel_logo_text = $this->input->post('images_text');
                }
                /* End Logo Upload */
                //exit();
                date_default_timezone_set("Asia/Kolkata");
                $hotel = array(
                    'hotel_name' => $this->input->post('hotel_name'),
                  //  'hotel_gpname' => $this->input->post('hotel_gpname'),
                    'hotel_year_established' => $this->input->post('hotel_year_established'),
                    'hotel_ownership_type' => $this->input->post('hotel_ownership_type'),
                    'hotel_type' => $hotel_type_string,
                    'hotel_rooms' => $this->input->post('hotel_rooms'),
                    'hotel_floor' => $this->input->post('hotel_floor'),
                    'hotel_logo_images' => $hotel_logo,
                    'hotel_logo_images_thumb' => $logo_thumb,
                    'hotel_logo_text' => $this->input->post('images_text'),
                    'hotel_status' => 'A',
                        //'hotel_id'=>$this->session->userdata('user_hotel'),
                        //'user_id'=>$this->session->userdata('user_id')
                        //'hotel_id'=>$this->session->userdata('user_hotel'),
                        //'user_id'=>12
                );

                $hotel_contact_details = array(
                    'hotel_street1' => $this->input->post('hotel_street1'),
                    'hotel_street2' => $this->input->post('hotel_street2'),
                    'hotel_landmark' => $this->input->post('hotel_landmark'),
                    'hotel_district' => $this->input->post('hotel_district'),
                    'hotel_pincode' => $this->input->post('hotel_pincode'),
                    'hotel_state' => $this->input->post('hotel_state'),
                    'hotel_country' => $this->input->post('hotel_country'),
                    'hotel_branch_street1' => $this->input->post('hotel_branch_street1'),
                    'hotel_branch_street2' => $this->input->post('hotel_branch_street2'),
                    'hotel_branch_landmark' => $this->input->post('hotel_branch_landmark'),
                    'hotel_branch_district' => $this->input->post('hotel_branch_district'),
                    'hotel_branch_pincode' => $this->input->post('hotel_branch_pincode'),
                    'hotel_branch_state' => $this->input->post('hotel_branch_state'),
                    'hotel_branch_country' => $this->input->post('hotel_branch_country'),
                    'hotel_booking_street1' => $this->input->post('hotel_booking_street1'),
                    'hotel_booking_street2' => $this->input->post('hotel_booking_street2'),
                    'hotel_booking_landmark' => $this->input->post('hotel_booking_landmark'),
                    'hotel_booking_district' => $this->input->post('hotel_booking_district'),
                    'hotel_booking_pincode' => $this->input->post('hotel_booking_pincode'),
                    'hotel_booking_state' => $this->input->post('hotel_booking_state'),
                    'hotel_booking_country' => $this->input->post('hotel_booking_country'),
                    'hotel_frontdesk_name' => $this->input->post('hotel_frontdesk_name'),
                    'hotel_frontdesk_mobile' => $this->input->post('hotel_frontdesk_mobile'),
                    'hotel_frontdesk_mobile_alternative' => $this->input->post('hotel_frontdesk_mobile_alternative'),
                    'hotel_frontdesk_email' => $this->input->post('hotel_frontdesk_email'),
                    'hotel_owner_name' => $this->input->post('hotel_owner_name'),
                    'hotel_owner_mobile' => $this->input->post('hotel_owner_mobile'),
                    'hotel_owner_mobile_alternative' => $this->input->post('hotel_owner_mobile_alternative'),
                    'hotel_owner_email' => $this->input->post('hotel_owner_email'),
                    'hotel_hr_name' => $this->input->post('hotel_hr_name'),
                    'hotel_hr_mobile' => $this->input->post('hotel_hr_mobile'),
                    'hotel_hr_mobile_alternative' => $this->input->post('hotel_hr_mobile_alternative'),
                    'hotel_hr_email' => $this->input->post('hotel_hr_email'),
                    'hotel_accounts_name' => $this->input->post('hotel_accounts_name'),
                    'hotel_accounts_mobile' => $this->input->post('hotel_accounts_mobile'),
                    'hotel_accounts_mobile_alternative' => $this->input->post('hotel_accounts_mobile_alternative'),
                    'hotel_accounts_email' => $this->input->post('hotel_accounts_email'),
                    'hotel_near_police_name' => $this->input->post('hotel_near_police_name'),
                    'hotel_near_police_mobile' => $this->input->post('hotel_near_police_mobile'),
                    'hotel_near_police_mobile_alternative' => $this->input->post('hotel_near_police_mobile_alternative'),
                    'hotel_near_police_email' => $this->input->post('hotel_near_police_email'),
                    'hotel_near_medical_name' => $this->input->post('hotel_near_medical_name'),
                    'hotel_near_medical_mobile' => $this->input->post('hotel_near_medical_mobile'),
                    'hotel_near_medical_mobile_alternative' => $this->input->post('hotel_near_medical_mobile_alternative'),
                    'hotel_near_medical_email' => $this->input->post('hotel_near_medical_email'),
                    'hotel_fax' => $this->input->post('hotel_fax'),
                    'hotel_website' => $this->input->post('hotel_website'),
                    'hotel_working_from' => '',
                    'hotel_working_upto' => '',
                    //	'hotel_id'=>$this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                        //'hotel_id'=>11,
                        //'user_id'=>12
                );

               

                $admin_details = array(
                    //  'admin_hotel' => $this->input->post('hotel_id'),
                    'admin_first_name' => $this->input->post('fName'),
                    'admin_last_name' => $this->input->post('lName'),
                    'admin_email' => $this->input->post('email'),
                    'admin_phone1' => $this->input->post('contact'),
                    'admin_phone2' => $this->input->post('alt_contact'),
                    'admin_user_type' => $this->input->post('admin_type'),
                    'admin_status' => 'I',
                    'dept' => $this->input->post('dept'),
                    'dept_role' => $this->input->post('dept_role'),
                    'profit_center' => implode(",", $this->input->post('profit_center')),
                    'class' => $this->input->post('class'),
                    'employee_id' => $this->input->post('employee_id'),
                    'about_me' => $this->input->post('about_me'),
					'chain_id' =>$this->input->post('hotel_chain_id'),
                );
                $login = array(
                    //  'hotel_id' => implode(",", $this->input->post('hotel_id')),
                    'chain_id' =>$this->input->post('hotel_chain_id'),
                    'user_name' => $this->input->post('uName'),
                    'user_password' => sha1(md5($this->input->post('password'))),
                    'user_email' => $this->input->post('email'),
                    'user_type_id' => $this->input->post('admin_type'),
                    'admin_role_id' => $this->input->post('admin_role_id'),
                    'user_status' => 'I'
                );








                //........end ....
                $hotel_tax_details = array(
                    'hotel_tax_applied' => $this->input->post('hotel_tax_applied'),
                    'hotel_service_tax' => $this->input->post('hotel_service_tax'),
                    'hotel_luxury_tax' => $this->input->post('hotel_luxury_tax'),
                    'hotel_service_charge' => $this->input->post('hotel_service_charge'),
                    'hotel_stander_tac' => $this->input->post('hotel_stander_tac'),
                    'hotel_vat_tax' => $this->input->post('hotel_vat_tax'),
                    'hotel_tax_food_vat' => $this->input->post('hotel_tax_food_vat'),
                    'hotel_tax_liquor_vat' => $this->input->post('hotel_tax_liquor_vat'),
                    'luxury_tax_slab1_range_to' => $this->input->post('luxury_tax_slab1_range_to'),
                    'luxury_tax_slab1_range_from' => $this->input->post('luxury_tax_slab1_range_from'),
                    'luxury_tax_slab1' => $this->input->post('luxury_tax_slab1'),
                    'luxury_tax_slab2_range_to' => $this->input->post('luxury_tax_slab2_range_to'),
                    'luxury_tax_slab2_range_from' => $this->input->post('luxury_tax_slab2_range_from'),
                    'luxury_tax_slab2' => $this->input->post('luxury_tax_slab2'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                        //'hotel_id'=>11,
                        //'user_id'=>12
                );

                $hotel_billing_settings = array(
                    'billing_name' => $this->input->post('billing_name'),
                    'billing_street1' => $this->input->post('billing_street1'),
                    'billing_street2' => $this->input->post('billing_street2'),
                    'billing_landmark' => $this->input->post('billing_landmark'),
                    'billing_district' => $this->input->post('billing_district'),
                    'billing_pincode' => $this->input->post('billing_pincode'),
                    'billing_state' => $this->input->post('billing_state'),
                    'billing_country' => $this->input->post('billing_country'),
                    'billing_email' => $this->input->post('billing_email'),
                    'billing_phone' => $this->input->post('billing_phone'),
                    'billing_fax' => $this->input->post('billing_fax'),
                    'billing_vat' => $this->input->post('billing_vat'),
                    'billing_bank_name' => $this->input->post('billing_bank_name'),
                    'billing_account_no' => $this->input->post('billing_account_no'),
                    'billing_ifsc_code' => $this->input->post('billing_ifsc_code'),
                    'luxury_tax_reg_no' => $this->input->post('luxury_tax_reg_no'),
                    'service_tax_no' => $this->input->post('service_tax_no'),
                    'vat_reg_no' => $this->input->post('vat_reg_no'),
                    'cin_no' => $this->input->post('cin_no'),
                    'tin_no' => $this->input->post('tin_no'),
                    //'hotel_id'=>$this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                        //'hotel_id'=>11,
                        //	'user_id'=>12
                );

                $hotel_general_preference = array(
                    'hotel_check_in_time_hr' => $this->input->post('hotel_check_in_time_hr'),
                    'hotel_check_in_time_mm' => $this->input->post('hotel_check_in_time_mm'),
                    'hotel_check_in_time_fr' => $this->input->post('hotel_check_in_time_fr'),
                    'hotel_check_out_time_hr' => $this->input->post('hotel_check_out_time_hr'),
                    'hotel_check_out_time_mm' => $this->input->post('hotel_check_out_time_mm'),
                    'hotel_check_out_time_fr' => $this->input->post('hotel_check_out_time_fr'),
                    'hotel_guest' => $guest_option_string,
                    'hotel_broker' => $broker_option_string,
                    //'hotel_buffer_hour' => $this->input->post('buffer_hour'),
                    'hotel_broker_commission' => 0,
                    'hotel_date_format' => $this->input->post('hotel_date_format'),
                    'hotel_time_format' => $this->input->post('hotel_time_format'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                        //'hotel_id'=>11,
                        //'user_id'=>12
                );

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
                $child_Account = array(
                    'child_id' => 1,
                    'hotel_name' => $this->input->post('hotel_name'),
                  //  'hotel_gpname' => $this->input->post('hotel_gpname'),
                    'image' => $logo_thumb
                        /* 'no_of_room' => $this->input->post('hotel_rooms'),
                          'plan' => $this->input->post('plan_id'),
                          'billing_cycle' => $this->input->post('bill_cycle'),
                          'billing_cycle_details' => $this->input->post('bill_cycle_details'),
                          'renewalcharge' => $this->input->post('renewal_change'),
                          'total_mount_paid' => $this->input->post('total_amount_paid'),
                          'contact_no' => $this->input->post('contact_no'),
                          'address' => $this->input->post('address'),
                          'city' => $this->input->post('city'),
                          'pincode' => $this->input->post('pincode'),
                          'state' => $this->input->post('state'),
                          'axis_room_config_details' => $this->input->post('axis_room_config_details'),
                          'special_notes' => $this->input->post('special_notes'),
                          'wheather_API_details' => $this->input->post('weather_API_Details'),
                          'country' => $this->input->post('country'),
                          'contact_person' => $this->input->post('contact_person'),
                          'email' => $this->input->post('email'),
                          'password' => $this->input->post('password'),
                          'secondary_email' => $this->input->post('hotel_name'),
                          'pending_amount' => $this->input->post('hotel_name'),
                          'db_config_details' => $this->input->post('hotel_name'),
                          'image' => $this->input->post('hotel_name'),
                          'date_added' => $this->input->post('hotel_name'),
                          'master_account_id' => $this->input->post('hotel_name'),
                          'status' => $this->input->post('hotel_name'),
                          'pos_config_details' => $this->input->post('hotel_name'),
                          'HRM_config_details' => $this->input->post('hotel_name'),
                          'account_config_details' => $this->input->post('hotel_name'),
                          'user_id' => $this->input->post('hotel_name'),
                          'hotel_id' => $this->input->post('hotel_name') */
                );

                /* 		
                  secondary_email, pending_amount, db_config_details,  image, date_added,  master_account_id,  status, pos_config_details, HRM_config_details, account_config_details, user_id, hotel_id
                  echo "<pre>";
                  print_r($hotel);
                  print_r($hotel_contact_details);
                  print_r($admin_details);
                  print_r($login);
                  print_r($hotel_general_preference);
                  exit;
                 */
                $query = $this->super_model->add_hotel($hotel, $hotel_contact_details, $hotel_tax_details, $hotel_billing_settings, $hotel_general_preference, $admin_details, $login, $child_Account);


                //	$user_result = $this->setting_model->add_admin_user($admin_details,$login);
                if ($query) {
                    $data['page'] = 'newbackend/dashboard1/add_hotel_m';
                    $this->load->view('newbackend/common/index', $data);
                } else {
                    echo "php error";
                }
            } else {
               // $data['country'] = $this->dashboard_model->get_country();
                //$data['star'] = $this->dashboard_model->get_star_rating();
                $data['page'] = 'newbackend/dashboard1/add_hotel_m';
                $this->load->view('newbackend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }
	
	 function add_hotel_m1(){
        $found = 1;
        if ($found) {
            $data['heading'] = 'Child Account';
            $data['sub_heading'] = 'Hotel Management';
            $data['description'] = 'Add Hotel Here';
            $data['active'] = 'misc_settings';
            $data['hotel_name'] = $this->dashboard_model->get_hotel_s($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                ///print_r($this->input->post());exit;
				
				//databse instance
				$chainID=$this->input->post('hotel_chain_id');
				$unique_database = $this->dashboard_model->get_chain_database($chainID);	
				$user = $unique_database->database_user;
				$password = $unique_database->database_pwd;
				$database = $unique_database->database_name;
				$this->db_connection($database,$user,$password);
				

                $hotel_type = $this->input->post('hotel_type');
                $hotel_type_string = "";
                $hotel_type_string = $hotel_type_string . implode(',', $hotel_type);

                $guest_option = $this->input->post('guest');
                $guest_option_string = "";
                $guest_option_string = $guest_option_string . implode(',', $guest_option);

                $broker_option = $this->input->post('broker');
                $broker_option_string = "";
                $broker_option_string = $broker_option_string . implode(',', $broker_option);
                /* Start Logo Upload */

                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('image_photo')) {
                    //echo "uploaded"; exit();
                    $hotel_logo = $this->upload->data('file_name');
                    $filename = $hotel_logo;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/hotel/';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 100,
                        'height' => 100
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        //redirect('dashboard/add_hotel_m');
                        redirect('newbackend/dashboard1/add_hotel_m');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $logo_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                } else {
                    $hotel_logo_text = $this->input->post('images_text');
                }
                /* End Logo Upload */
                //exit();
                date_default_timezone_set("Asia/Kolkata");
                $hotel = array(
                    'hotel_name' => $this->input->post('hotel_name'),
                    'hotel_year_established' => $this->input->post('hotel_year_established'),
                    'hotel_ownership_type' => $this->input->post('hotel_ownership_type'),
                    'hotel_type' => $hotel_type_string,
                    'hotel_rooms' => $this->input->post('hotel_rooms'),
                    'hotel_floor' => $this->input->post('hotel_floor'),
                    'hotel_logo_images' => $hotel_logo,
                    'hotel_logo_images_thumb' => $logo_thumb,
                    'hotel_logo_text' => $this->input->post('images_text'),
                    'hotel_status' => 'A',
                        //'hotel_id'=>$this->session->userdata('user_hotel'),
                        //'user_id'=>$this->session->userdata('user_id')
                        //'hotel_id'=>$this->session->userdata('user_hotel'),
                        //'user_id'=>12
                );

                $hotel_contact_details = array(
                    'hotel_street1' => $this->input->post('hotel_street1'),
                    'hotel_street2' => $this->input->post('hotel_street2'),
                    'hotel_landmark' => $this->input->post('hotel_landmark'),
                    'hotel_district' => $this->input->post('hotel_district'),
                    'hotel_pincode' => $this->input->post('hotel_pincode'),
                    'hotel_state' => $this->input->post('hotel_state'),
                    'hotel_country' => $this->input->post('hotel_country'),
                    'hotel_branch_street1' => $this->input->post('hotel_branch_street1'),
                    'hotel_branch_street2' => $this->input->post('hotel_branch_street2'),
                    'hotel_branch_landmark' => $this->input->post('hotel_branch_landmark'),
                    'hotel_branch_district' => $this->input->post('hotel_branch_district'),
                    'hotel_branch_pincode' => $this->input->post('hotel_branch_pincode'),
                    'hotel_branch_state' => $this->input->post('hotel_branch_state'),
                    'hotel_branch_country' => $this->input->post('hotel_branch_country'),
                    'hotel_booking_street1' => $this->input->post('hotel_booking_street1'),
                    'hotel_booking_street2' => $this->input->post('hotel_booking_street2'),
                    'hotel_booking_landmark' => $this->input->post('hotel_booking_landmark'),
                    'hotel_booking_district' => $this->input->post('hotel_booking_district'),
                    'hotel_booking_pincode' => $this->input->post('hotel_booking_pincode'),
                    'hotel_booking_state' => $this->input->post('hotel_booking_state'),
                    'hotel_booking_country' => $this->input->post('hotel_booking_country'),
                    'hotel_frontdesk_name' => $this->input->post('hotel_frontdesk_name'),
                    'hotel_frontdesk_mobile' => $this->input->post('hotel_frontdesk_mobile'),
                    'hotel_frontdesk_mobile_alternative' => $this->input->post('hotel_frontdesk_mobile_alternative'),
                    'hotel_frontdesk_email' => $this->input->post('hotel_frontdesk_email'),
                    'hotel_owner_name' => $this->input->post('hotel_owner_name'),
                    'hotel_owner_mobile' => $this->input->post('hotel_owner_mobile'),
                    'hotel_owner_mobile_alternative' => $this->input->post('hotel_owner_mobile_alternative'),
                    'hotel_owner_email' => $this->input->post('hotel_owner_email'),
                    'hotel_hr_name' => $this->input->post('hotel_hr_name'),
                    'hotel_hr_mobile' => $this->input->post('hotel_hr_mobile'),
                    'hotel_hr_mobile_alternative' => $this->input->post('hotel_hr_mobile_alternative'),
                    'hotel_hr_email' => $this->input->post('hotel_hr_email'),
                    'hotel_accounts_name' => $this->input->post('hotel_accounts_name'),
                    'hotel_accounts_mobile' => $this->input->post('hotel_accounts_mobile'),
                    'hotel_accounts_mobile_alternative' => $this->input->post('hotel_accounts_mobile_alternative'),
                    'hotel_accounts_email' => $this->input->post('hotel_accounts_email'),
                    'hotel_near_police_name' => $this->input->post('hotel_near_police_name'),
                    'hotel_near_police_mobile' => $this->input->post('hotel_near_police_mobile'),
                    'hotel_near_police_mobile_alternative' => $this->input->post('hotel_near_police_mobile_alternative'),
                    'hotel_near_police_email' => $this->input->post('hotel_near_police_email'),
                    'hotel_near_medical_name' => $this->input->post('hotel_near_medical_name'),
                    'hotel_near_medical_mobile' => $this->input->post('hotel_near_medical_mobile'),
                    'hotel_near_medical_mobile_alternative' => $this->input->post('hotel_near_medical_mobile_alternative'),
                    'hotel_near_medical_email' => $this->input->post('hotel_near_medical_email'),
                    'hotel_fax' => $this->input->post('hotel_fax'),
                    'hotel_website' => $this->input->post('hotel_website'),
                    'hotel_working_from' => '',
                    'hotel_working_upto' => '',
                    //	'hotel_id'=>$this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                        //'hotel_id'=>11,
                        //'user_id'=>12
                );

               

                $admin_details = array(
                    //  'admin_hotel' => $this->input->post('hotel_id'),
                    'admin_first_name' => $this->input->post('fName'),
                    'admin_last_name' => $this->input->post('lName'),
                    'admin_email' => $this->input->post('email'),
                    'admin_phone1' => $this->input->post('contact'),
                    'admin_phone2' => $this->input->post('alt_contact'),
                    'admin_user_type' => $this->input->post('admin_type'),
                    'admin_status' => 'I',
                    'dept' => $this->input->post('dept'),
                    'dept_role' => $this->input->post('dept_role'),
                    'profit_center' => implode(",", $this->input->post('profit_center')),
                    'class' => $this->input->post('class'),
                    'employee_id' => $this->input->post('employee_id'),
                    'about_me' => $this->input->post('about_me'),
					'chain_id' =>$this->input->post('hotel_chain_id'),
                );
                $login = array(
                    //  'hotel_id' => implode(",", $this->input->post('hotel_id')),
                    'chain_id' =>$this->input->post('hotel_chain_id'),
                    'user_name' => $this->input->post('uName'),
                    'user_password' => sha1(md5($this->input->post('password'))),
                    'user_email' => $this->input->post('email'),
                    'user_type_id' => $this->input->post('admin_type'),
                    'admin_role_id' => $this->input->post('admin_role_id'),
                    'user_status' => 'I'
                );








                //........end ....
                $hotel_tax_details = array(
                    'hotel_tax_applied' => $this->input->post('hotel_tax_applied'),
                    'hotel_service_tax' => $this->input->post('hotel_service_tax'),
                    'hotel_luxury_tax' => $this->input->post('hotel_luxury_tax'),
                    'hotel_service_charge' => $this->input->post('hotel_service_charge'),
                    'hotel_stander_tac' => $this->input->post('hotel_stander_tac'),
                    'hotel_vat_tax' => $this->input->post('hotel_vat_tax'),
                    'hotel_tax_food_vat' => $this->input->post('hotel_tax_food_vat'),
                    'hotel_tax_liquor_vat' => $this->input->post('hotel_tax_liquor_vat'),
                    'luxury_tax_slab1_range_to' => $this->input->post('luxury_tax_slab1_range_to'),
                    'luxury_tax_slab1_range_from' => $this->input->post('luxury_tax_slab1_range_from'),
                    'luxury_tax_slab1' => $this->input->post('luxury_tax_slab1'),
                    'luxury_tax_slab2_range_to' => $this->input->post('luxury_tax_slab2_range_to'),
                    'luxury_tax_slab2_range_from' => $this->input->post('luxury_tax_slab2_range_from'),
                    'luxury_tax_slab2' => $this->input->post('luxury_tax_slab2'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                        //'hotel_id'=>11,
                        //'user_id'=>12
                );

                $hotel_billing_settings = array(
                    'billing_name' => $this->input->post('billing_name'),
                    'billing_street1' => $this->input->post('billing_street1'),
                    'billing_street2' => $this->input->post('billing_street2'),
                    'billing_landmark' => $this->input->post('billing_landmark'),
                    'billing_district' => $this->input->post('billing_district'),
                    'billing_pincode' => $this->input->post('billing_pincode'),
                    'billing_state' => $this->input->post('billing_state'),
                    'billing_country' => $this->input->post('billing_country'),
                    'billing_email' => $this->input->post('billing_email'),
                    'billing_phone' => $this->input->post('billing_phone'),
                    'billing_fax' => $this->input->post('billing_fax'),
                    'billing_vat' => $this->input->post('billing_vat'),
                    'billing_bank_name' => $this->input->post('billing_bank_name'),
                    'billing_account_no' => $this->input->post('billing_account_no'),
                    'billing_ifsc_code' => $this->input->post('billing_ifsc_code'),
                    'luxury_tax_reg_no' => $this->input->post('luxury_tax_reg_no'),
                    'service_tax_no' => $this->input->post('service_tax_no'),
                    'vat_reg_no' => $this->input->post('vat_reg_no'),
                    'cin_no' => $this->input->post('cin_no'),
                    'tin_no' => $this->input->post('tin_no'),
                    //'hotel_id'=>$this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                        //'hotel_id'=>11,
                        //	'user_id'=>12
                );

                $hotel_general_preference = array(
                    'hotel_check_in_time_hr' => $this->input->post('hotel_check_in_time_hr'),
                    'hotel_check_in_time_mm' => $this->input->post('hotel_check_in_time_mm'),
                    'hotel_check_in_time_fr' => $this->input->post('hotel_check_in_time_fr'),
                    'hotel_check_out_time_hr' => $this->input->post('hotel_check_out_time_hr'),
                    'hotel_check_out_time_mm' => $this->input->post('hotel_check_out_time_mm'),
                    'hotel_check_out_time_fr' => $this->input->post('hotel_check_out_time_fr'),
                    'hotel_guest' => $guest_option_string,
                    'hotel_broker' => $broker_option_string,
                    //'hotel_buffer_hour' => $this->input->post('buffer_hour'),
                    'hotel_broker_commission' => 0,
                    'hotel_date_format' => $this->input->post('hotel_date_format'),
                    'hotel_time_format' => $this->input->post('hotel_time_format'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                        //'hotel_id'=>11,
                        //'user_id'=>12
                );

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
                $child_Account = array(
                    'child_id' => 1,
                    'hotel_name' => $this->input->post('hotel_name'),
                    'image' => $logo_thumb
                        /* 'no_of_room' => $this->input->post('hotel_rooms'),
                          'plan' => $this->input->post('plan_id'),
                          'billing_cycle' => $this->input->post('bill_cycle'),
                          'billing_cycle_details' => $this->input->post('bill_cycle_details'),
                          'renewalcharge' => $this->input->post('renewal_change'),
                          'total_mount_paid' => $this->input->post('total_amount_paid'),
                          'contact_no' => $this->input->post('contact_no'),
                          'address' => $this->input->post('address'),
                          'city' => $this->input->post('city'),
                          'pincode' => $this->input->post('pincode'),
                          'state' => $this->input->post('state'),
                          'axis_room_config_details' => $this->input->post('axis_room_config_details'),
                          'special_notes' => $this->input->post('special_notes'),
                          'wheather_API_details' => $this->input->post('weather_API_Details'),
                          'country' => $this->input->post('country'),
                          'contact_person' => $this->input->post('contact_person'),
                          'email' => $this->input->post('email'),
                          'password' => $this->input->post('password'),
                          'secondary_email' => $this->input->post('hotel_name'),
                          'pending_amount' => $this->input->post('hotel_name'),
                          'db_config_details' => $this->input->post('hotel_name'),
                          'image' => $this->input->post('hotel_name'),
                          'date_added' => $this->input->post('hotel_name'),
                          'master_account_id' => $this->input->post('hotel_name'),
                          'status' => $this->input->post('hotel_name'),
                          'pos_config_details' => $this->input->post('hotel_name'),
                          'HRM_config_details' => $this->input->post('hotel_name'),
                          'account_config_details' => $this->input->post('hotel_name'),
                          'user_id' => $this->input->post('hotel_name'),
                          'hotel_id' => $this->input->post('hotel_name') */
                );

                /* 		
                  secondary_email, pending_amount, db_config_details,  image, date_added,  master_account_id,  status, pos_config_details, HRM_config_details, account_config_details, user_id, hotel_id
                  echo "<pre>";
                  print_r($hotel);
                  print_r($hotel_contact_details);
                  print_r($admin_details);
                  print_r($login);
                  print_r($hotel_general_preference);
                  exit;
                 */
                $query = $this->super_model->add_hotel($hotel, $hotel_contact_details, $hotel_tax_details, $hotel_billing_settings, $hotel_general_preference, $admin_details, $login, $child_Account);


                //	$user_result = $this->setting_model->add_admin_user($admin_details,$login);
                if ($query) {
                    $data['page'] = 'newbackend/dashboard1/add_hotel_m';
                    $this->load->view('newbackend/common/index', $data);
                } else {
                    echo "php error";
                }
            } else {
               // $data['country'] = $this->dashboard_model->get_country();
                //$data['star'] = $this->dashboard_model->get_star_rating();
                $data['page'] = 'newbackend/dashboard1/add_hotel_m1';
                $this->load->view('newbackend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function add_admin_user() {
        $data['heading'] = 'Admin';
        $data['sub_heading'] = 'Add Admin User';
        $data['description'] = 'Add Admin User Here';
        $data['active'] = 'add_admin_user';
        $data['hotel'] = $this->dashboard_model->all_hotel_list();
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        if ($this->input->post()) {
            //echo "<pre>";
            //print_r($this->input->post());
            //exit;
            $admin_details = array(
                'admin_hotel' => implode(",", $this->input->post('hotel_id')),
                'admin_first_name' => $this->input->post('fName'),
                'admin_last_name' => $this->input->post('lName'),
                'admin_email' => $this->input->post('email'),
                'admin_phone1' => $this->input->post('contact'),
                'admin_phone2' => $this->input->post('alt_contact'),
                'admin_user_type' => $this->input->post('admin_type'),
                'admin_status' => 'I',
                'dept' => $this->input->post('dept'),
                'dept_role' => $this->input->post('dept_role'),
                'profit_center' => implode(",", $this->input->post('profit_center')),
                'class' => $this->input->post('class'),
                'employee_id' => $this->input->post('employee_id'),
                'about_me' => $this->input->post('about_me'),
            );
            $login = array(
                'hotel_id' => implode(",", $this->input->post('hotel_id')),
                'user_name' => $this->input->post('uName'),
                'user_password' => sha1(md5($this->input->post('password'))),
                'user_email' => $this->input->post('email'),
                'user_type_id' => $this->input->post('admin_type'),
                'admin_role_id' => $this->input->post('admin_role_id'),
                'user_status' => 'I'
            );
            $contact = $this->input->post('contact');
            $text = " New admin created" . " " . " User Name: " . $this->input->post('uName') . " Password:" . $this->input->post('password');
            $chk = $this->setting_model->add_admin_user($admin_details, $login);
            if (isset($chk) && $chk) {
                $this->setting_model->send_sms($text, $contact);
                $this->session->set_flashdata('succ_msg', "Admin User Added Successfully!");
                redirect('setting/all_admin_user');
            } else {
                $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                redirect('setting/all_admin_user');
            }
        } else {
            $data['page'] = 'newbackend/dashboard1/add_admin_user';
            $this->load->view('backend/common/index', $data);
        }
    }

    function add_master_account() {
        $found = 1;
        if ($found) {
            //$this->load->model('account_model');
            $data['heading'] = 'Chain Master';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['active'] = 'add_admin';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
            $data['page'] = 'backend/dashboard/account/add_master_account';
            $this->load->view('newbackend/common/index', $data);
        }
    }

    /* Upload Start */

    function set_upload_options() {
        $config = array(
            'upload_path' => './upload/',
            'allowed_types' => 'gif|jpg|png',
            'overwrite' => FALSE
        );
        return $config;
    }

    /* Upload End */

    function add_chain_master() {
        $data['heading'] = '';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = '';
        $data['hotel_name'] = $this->dashboard_model->get_hotel_s($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin_s($this->session->userdata('user_id'));
        $data['hotel_id'] = $this->dashboard_model->get_admin_s($this->session->userdata('hotel_id'));
        $found = 1;
        if ($found) {
            if ($this->input->post()) {
                //$g_type=$this->input->post('g_type');
                $this->load->model('account_model');
                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('image_photo')) {
                    /*  Image Upload 22.09.2016 */
                    $room_image = $this->upload->data('file_name');
                    $filename = $room_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/account/';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect(base_url() . 'account/view_chain_master');
                    }
                    // clear //
                    $file_image = explode(".", $filename);
                    $file_thumb = $file_image[0] . $config_manip['thumb_marker'] . "." . $file_image[1];
                    $this->image_lib->clear();
                } else {
                    $file_image = "no_images";
                    $filename = "no_images";
                    //$file_thumb ="";
                }

                if ($this->input->post('bill_cycle')) {
                    $bill = date("Y-m-d", strtotime($this->input->post('bill_cycle')));
                } else {
                    $bill = "0000-00-00";
                }
                $guest = array(
                   // 'master_id' => $this->input->post('master_id'),
                    'database_name' => $this->input->post('database_name'),
                    'database_pwd' => $this->input->post('password'),
                    'database_user' => 'root',
					
                    'hotel_group_name' => $this->input->post('hotel_group_name'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state'),
                    'address' => $this->input->post('address'),
                    'country' => $this->input->post('country'),
                    'contact_no' => $this->input->post('contact_no'),
                    'email' => $this->input->post('email_id'),
                    'password' => $this->input->post('password'),
                    'contact_prerson_name' => $this->input->post('contact_person'),
                    'no_of_accounts' => $this->input->post('account_no'),
                    'plan_id' => $this->input->post('plan_id'),
                    'amount_due' => $this->input->post('amount_due'),
                    'image' => $file_thumb,
                    'bill_cycle' => $bill,
                    'bill_cycle_details' => $this->input->post('bill_cycle_details'),
                    'comments' => $this->input->post('comment'),
                    'email_config_details' => $this->input->post('email_details'),
                    'sms_config_details' => $this->input->post('sms_details'),
                    'Mise_API_config_details' => $this->input->post('api_details'),
					
                        //'hotel_id'=>$this->session->userdata('user_hotel'),
                        //'user_id'=>$this->session->userdata('user_id')
                );
				//echo "<pre>";
                //print_r($guest);
                //exit();

                $query = $this->account_model->add_chain_master($guest);

                if ($query) {
                    $this->session->set_flashdata('succ_msg', "Guest Added Successfully!");
                    //redirect('dashboard/account/add_master_account');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    //redirect('dashboard/account/add_master_account');
                }
                redirect(base_url() . 'account/view_chain_master');
            } else {
                //$data['page'] = 'backend/dashboard/account/add_chain_master';
                $data['page'] = 'backend/dashboard/account/add_master_account';
                $this->load->view('newbackend/common/index', $data);
            }
        } else {
            redirect('account');
        }
    }

    function view_chain_master() {
        $this->load->model('account_model');
        $data['heading'] = 'Chain Master';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'chain_master';
        $data['hotel_name'] = $this->dashboard_model->get_hotel_s($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin_s($this->session->userdata('user_id'));
        $data['chain_master'] = $this->account_model->all_chain_master();
        //echo "<pre>";
		//print_r($data['chain_master']);exit;	
        $data['page'] = 'newbackend/dashboard1/account/view_chain_master';
        $this->load->view('newbackend/common/index', $data);
    }

    function view_child_account() {
        $this->load->model('account_model');
        $data['heading'] = 'Child Account';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'child';
        $data['hotel_name'] = $this->dashboard_model->get_hotel_s($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin_s($this->session->userdata('user_id'));
        //$data['child_account']=$this->account_model->all_child_account1(); 
        $data['child_account'] = $this->account_model->all_child_account();
        //	echo "<pre>";
//print_r($data['child_account']);exit;		
        $data['page'] = 'newbackend/dashboard1/account/view_child_account';
        $this->load->view('newbackend/common/index', $data);
    }

    function delete_chain_master() {
        $id = $this->input->post('id');
        $this->account_model->delete_chain_master($id);
        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function delete_child_account() {
        $id = $this->input->post('id');
        $this->account_model->delete_child_account($id);
        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function edit_chain_master() {
        $this->load->model('account_model');
        $data['heading'] = 'Account';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = '';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));

//upload file/image
        if ($this->input->post()) {

            $room_image = "";
            $room_thumb = "";
            $this->upload->initialize($this->set_upload_options());
            if ($this->upload->do_upload('image_photo')) {

                $room_image = $this->upload->data('file_name');

                $filename = $room_image;
                $source_path = './upload/' . $filename;
                $target_path = './upload/account/';
                $config_manip = array(
                    'image_library' => 'gd2',
                    'source_image' => $source_path,
                    'new_image' => $target_path,
                    'maintain_ratio' => TRUE,
                    'create_thumb' => TRUE,
                    'thumb_marker' => '_thumb',
                    'width' => 250,
                    'height' => 250
                );
                $this->load->library('image_lib', $config_manip);
                if (!$this->image_lib->resize()) {
                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                    redirect('dashboard/add_room');
                }
                // clear //
                $thumb_name = explode(".", $filename);
                $room_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                $this->image_lib->clear();
            }





            if ($this->input->post('bill_cycle')) {
                $bill = date("Y-m-d", strtotime($this->input->post('bill_cycle')));
            } else {
                $bill = "0000-00-00";
            }
            $guest = array(
                'id' => $this->input->post('hid'),
                'master_id' => $this->input->post('master_id'),
                'hotel_group_name' => $this->input->post('hotel_group_name'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'address' => $this->input->post('address'),
                'country' => $this->input->post('country'),
                'contact_no' => $this->input->post('contact_no'),
                'email' => $this->input->post('email_id'),
                'password' => $this->input->post('password'),
                'contact_prerson_name' => $this->input->post('contact_person'),
                'no_of_accounts' => $this->input->post('account_no'),
                'plan_id' => $this->input->post('plan_id'),
                'amount_due' => $this->input->post('amount_due'),
                'image' => $room_thumb,
                'bill_cycle' => $bill,
                'bill_cycle_details' => $this->input->post('bill_cycle_details'),
                'comments' => $this->input->post('comment'),
                'email_config_details' => $this->input->post('email_details'),
                'sms_config_details' => $this->input->post('sms_details'),
                'Mise_API_config_details' => $this->input->post('api_details'),
                'hotel_id' => $this->session->userdata('user_hotel'),
                'user_id' => $this->session->userdata('user_id')
            );


            //echo $thumb_name;	print_r($guest);exit;

            $query = $this->account_model->update_master_account($guest);
            if ($query) {
                $msg = "inserted";
                $this->session->set_flashdata('succ_msg', 'Data Updated Succefully');
            } else {

                $msg = " not inserted";
                $this->session->set_flashdata('succ_msg', 'Data Updated Not Succefully');
            }
            redirect(base_url() . 'account/view_chain_master');
        } else {
            $id = $_GET['s_id'];
            $data['master'] = $this->account_model->get_chain_master1($id);

            $data['page'] = 'newbackend/dashboard1/account/edit_master_account';
            $this->load->view('newbackend/common/index', $data);
            //print_r($data['master']);exit;
        }
    }

    function edit_child_account() {
        $this->load->model('account_model');
        $data['heading'] = 'Account';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = '';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));

//upload file/image
        if ($this->input->post()) {

            $this->upload->initialize($this->set_upload_options());
            if ($this->upload->do_upload('image_photo')) {

                $image = $this->upload->data('file_name');

                $filename = $image;
                $source_path = './upload/' . $filename;
                $target_path = './upload/account/';
                $config_manip = array(
                    'image_library' => 'gd2',
                    'source_image' => $source_path,
                    'new_image' => $target_path,
                    'maintain_ratio' => TRUE,
                    'create_thumb' => TRUE,
                    'thumb_marker' => '_thumb',
                    'width' => 250,
                    'height' => 250
                );
                $this->load->library('image_lib', $config_manip);

                if (!$this->image_lib->resize()) {
                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                    redirect('dashboard/add_asset');
                }
                $thumb_name = explode(".", $filename);
                $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                $this->image_lib->clear();
            } else {
                $target_path = './upload/account/';
                $filename = 'no_images.png';
            }





            if ($this->input->post('bill_cycle')) {
                $bill = date("Y-m-d", strtotime($this->input->post('bill_cycle')));
            } else {
                $bill = "0000-00-00";
            }
            $guest = array(
                'id' => $this->input->post('id'),
                'child_id' => $this->input->post('child_id'),
                'hotel_name' => $this->input->post('hotel_name'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'address' => $this->input->post('address'),
                'country' => $this->input->post('country'),
                'contact_no' => $this->input->post('contact_no'),
                'email' => $this->input->post('email_id'),
                'password' => $this->input->post('password'),
                'contact_person' => $this->input->post('contact_person'),
                'plan' => $this->input->post('plan'),
                'image' => $filename,
                'billing_cycle' => $bill,
                'status' => $this->input->post('status'),
                'billing_cycle_details' => $this->input->post('bill_cycle_details'),
                'axis_room_config_details' => $this->input->post('axis_room_config_details'),
                'account_config_details' => $this->input->post('account_config_details'),
                'HRM_config_details' => $this->input->post('hrm_config_details'),
                'pos_config_details' => $this->input->post('pos_config_details'),
                'hotel_id' => $this->session->userdata('user_hotel'),
                'user_id' => $this->session->userdata('user_id')
            );


            //print_r($session);exit;

            $query = $this->account_model->update_child_account($guest);
            if ($query) {
                $msg = "inserted";
                $this->session->set_flashdata('succ_msg', 'Data updated Succefully');
            } else {

                $msg = " not inserted";
                $this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
            }
            redirect(base_url() . 'account/view_child_account');
        } else {
            $id = $_GET['s_id'];
            $data['child_account'] = $this->account_model->get_child($id);

            $data['page'] = 'newbackend/dashboard1/account/edit_child_account';
            $this->load->view('newbackend/common/index', $data);
            //print_r($data['master']);exit;
        }
    }

    function add_transaction_account() {
        $data['heading'] = '';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'all_unit_class';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['page'] = 'newbackend/dashboard1/account/add_transaction_account';
        $this->load->view('newbackend/common/index', $data);

        $found = 1;
        if ($found) {
            if ($this->input->post()) {
                //$g_type=$this->input->post('g_type');
                $this->load->model('account_model');
                //tax,discount,amount_pending  , check_no,bank_name,exp_date,cvv_no,card_no,payment,time_startup,amount_id,c_id,t_id
                $data = array(
                    //'master_id' => $this->input->post('tax'),    	
                    //'hotel_group_name' => $this->input->post('discount'),    					
                    //'city' => $this->input->post('amount_pending'),
                    'check_no' => $this->input->post('check_no'),
                    'bank_name' => $this->input->post('bank_name'),
                    //'country' => $this->input->post('exp_date'),
                    //'contact_no' => $this->input->post('cvv_no'),
                    //'email' => $this->input->post('card_no'),
                    //'password' => $this->input->post('payment'),
                    //'contact_prerson_name' => $this->input->post('time_startup'),
                    'exp_date' => $this->input->post('exp_date'),
                    'cvv_no' => $this->input->post('cvv_no'),
                    'card_no' => $this->input->post('card_no'),
                    'mode_of_payment' => $this->input->post('payment'),
                    'amount_paid' => $this->input->post('amount'),
                    'child_account' => $this->input->post('c_id'),
                    'bill_id' => $this->input->post('bill_id'),
                        //'amount_due' => $this->input->post('t_id'),                        
                        //'hotel_id'=>$this->session->userdata('user_hotel'),
                        //'user_id'=>$this->session->userdata('user_id')
                );

                //print_r($guest);
                //// exit();

                $query = $this->account_model->add_transaction_account($data);

                if ($query) {
                    $this->session->set_flashdata('succ_msg', "Guest Added Successfully!");
                    redirect('account/add_transaction_account');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('account/add_transaction_account');
                }
                //redirect(base_url().'account/add_transaction_account');
            } else {
                //$data['page'] = 'backend/dashboard/account/add_chain_master';
                $data['page'] = 'newbackend/dashboard1/account/add_transaction_account';
                $this->load->view('newbackend/common/index', $data);
            }
        } else {
            redirect('account');
        }
    }

    function view_transaction_account() {
        $this->load->model('account_model');
        $data['heading'] = 'Account';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = '';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['transaction_account'] = $this->account_model->all_transaction_account();
        $data['page'] = 'newbackend/dashboard1/account/view_transaction_account';
        $this->load->view('newbackend/common/index', $data);
    }

    /* function add_bill_account(){
      $found = 1;
      if($found){
      //$this->load->model('account_model');
      $data['heading'] = '';
      $data['sub_heading'] = '';
      $data['description'] = '';
      $data['active'] = 'all_unit_class';
      $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
      $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
      $data['page'] = 'backend/dashboard/account/add_bill_account';
      $this->load->view('backend/common/index', $data);
      }
      } */

    function add_bill_account() {
        //$this->load->model('account_model');
        $data['heading'] = 'Bill Account';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'bill_account';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['page'] = 'newbackend/dashboard1/account/add_bill_account';
        $this->load->view('newbackend/common/index', $data);

        $found = 1;
        if ($found) {
            if ($this->input->post()) {

                $data = array(
                    'billing_cycle' => $this->input->post('bill_cycle'),
                    'billing_cycle_details' => $this->input->post('bill_cycle_details'),
                    'payable_amount' => $this->input->post('payable_amount'),
                    'previous_pending_amount' => $this->input->post('prev_payable_amount'),
                    'service_tax_amount' => $this->input->post('service_tax'),
                    'other_tax_1_amount' => $this->input->post('service_tax'),
                    'other_tax_2_amount' => $this->input->post('other_tax_1'),
                    'mise_charges' => $this->input->post('other_tax_2'),
                    'mise_charges_details' => $this->input->post('mise_charge'),
                    //'time_stamp' => $this->input->post('time'),
                    'status' => $this->input->post('status'),
                    'child_account_id' => $this->input->post('child_acc_id'),
                    'bill_generated_date' => $this->input->post('bill_generate_date'),
                    'minimum_amount_due' => $this->input->post('amount_due'),
                );



                $query = $this->account_model->add_bill_account($data);

                if ($query) {
                    $this->session->set_flashdata('succ_msg', "account Added Successfully!");
                    redirect('account/add_bill_account');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('account/add_bill_account');
                }
                //redirect(base_url().'account/add_transaction_account');
            } else {
                //$data['page'] = 'backend/dashboard/account/add_chain_master';
                $data['page'] = 'newbackend/dashboard1/account/add_bill_account';
                $this->load->view('newbackend/common/index', $data);
            }
        } else {
            redirect('account');
        }
    }

    function add_child_account() {
        $found = 1;

        //$this->load->model('account_model');


        if ($this->input->post()) {
            //$g_type=$this->input->post('g_type');
            $this->load->model('account_model');
            $this->upload->initialize($this->set_upload_options());
            if ($this->upload->do_upload('image_photo')) {
                /*  Image Upload 22.09.2016 */
                $room_image = $this->upload->data('file_name');
                $filename = $room_image;
                $source_path = './upload/' . $filename;
                $target_path = './upload/account/';

                $config_manip = array(
                    'image_library' => 'gd2',
                    'source_image' => $source_path,
                    'new_image' => $target_path,
                    'maintain_ratio' => TRUE,
                    'create_thumb' => TRUE,
                    'thumb_marker' => '_thumb',
                    'width' => 250,
                    'height' => 250
                );
                $this->load->library('image_lib', $config_manip);

                if (!$this->image_lib->resize()) {
                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                    redirect(base_url() . 'account/add_chain_master');
                }
                // clear //
                $file_image = explode(".", $filename);
                $file_thumb = $file_image[0] . $config_manip['thumb_marker'] . "." . $file_image[1];
                $this->image_lib->clear();
            } else {
                $file_image = "no_images.png";
                $filename = "no_images.png";
                //$file_thumb ="";
            }

            if ($this->input->post('bill_cycle')) {
                $bill = date("Y-m-d", strtotime($this->input->post('bill_cycle')));
            } else {
                $bill = "0000-00-00";
            }
            $guest = array(
                //'child_id' => $this->input->post('child_id'),
                'hotel_name' => $this->input->post('hotel_name'),
                'no_of_room' => $this->input->post('no_room'),
                'plan' => $this->input->post('plan'),
                //'billing_cycle' => $this->input->post('bill_cycle'),
                'billing_cycle_details' => $this->input->post('bill_cycle_details'),
                'renewalcharge' => $this->input->post('renewal_change'),
                'total_mount_paid' => $this->input->post('total_amount_paid'),
                'contact_no' => $this->input->post('contact_no'),
                'address' => $this->input->post('address'),
                'pincode' => $this->input->post('pincode'),
                //'plan' => $this->input->post('plan_id'),						
                'city' => $this->input->post('city'),
                'image' => $filename,
                'billing_cycle' => $bill,
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'contact_person' => $this->input->post('contact_person'),
                'email' => $this->input->post('email_id'),
                'password' => $this->input->post('password'),
                'secondary_email' => $this->input->post('secondary_email'),
                'pending_amount' => $this->input->post('pending_amount'),
                'date_added' => $this->input->post('date_added'),
                'master_account_id' => $this->input->post('master_account_id'),
                'status' => $this->input->post('status'),
                'db_config_details' => $this->input->post('db_config_details'),
                'pos_config_details' => $this->input->post('pos_config_details'),
                'HRM_config_details' => $this->input->post('hrm_config_details'),
                'account_config_details' => $this->input->post('account_config_details'),
                'axis_room_config_details' => $this->input->post('axis_room_config_details'),
                'wheather_API_details' => $this->input->post('weather_API_Details'),
                'special_notes' => $this->input->post('special_notes'),
                'hotel_id' => $this->session->userdata('user_hotel'),
                'user_id' => $this->session->userdata('user_id')
            );

			/*echo "<pre>";
            print_r($guest);
             exit();*/

            $query = $this->account_model->add_child_master($guest);

            if ($query) {
                $this->session->set_flashdata('succ_msg', "Guest Added Successfully!");
                //redirect('dashboard/account/add_master_account');
            } else {
                $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                //redirect('dashboard/account/add_master_account');
            }
            redirect(base_url() . 'account/view_child_account');
        } else {
            $data['heading'] = '';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['active'] = 'all_unit_class';
            $data['hotel_name'] = $this->dashboard_model->get_hotel_s($this->session->userdata('user_hotel'));
            $data['admin_name'] = $this->dashboard_model->get_admin_s($this->session->userdata('user_id'));
            $data['page'] = 'newbackend/dashboard1/account/add_child_account';
            $this->load->view('newbackend/common/index', $data);
        }
    }

    function get_transaction() {
        $id = $this->input->post('id');
        $query = $this->account_model->get_transaction($id);
        $data = array(
            'child_account' => $query->child_account,
            'amount_paid' => $query->amount_paid,
            'timestamp' => $query->timestamp,
            'mode_of_payment' => $query->mode_of_payment,
            'card_no' => $query->card_no,
            'cvv_no' => $query->cvv_no,
            'exp_date' => $query->exp_date,
            'bank_name' => $query->bank_name,
            'check_no' => $query->check_no,
            'bill_id' => $query->bill_id,
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function update_transaction() {
        $data = array(
            //'id'=> $this->input->post('hid'),   
            'child_account' => $this->input->post('child_account'),
            'amount_paid' => $this->input->post('amount_paid'),
            'mode_of_payment' => $this->input->post('mode_of_payment'),
            'card_no' => $this->input->post('card_no'),
            'cvv_no' => $this->input->post('cvv_no'),
            'exp_date' => $this->input->post('exp_date'),
            'bank_name' => $this->input->post('bank_name'),
            'check_no' => $this->input->post('check_no'),
            'bill_id' => $this->input->post('bill_id'),
        );

        $id = $this->input->post('hid');
        //echo $thumb_name;	print_r($guest);exit;

        $query = $this->account_model->update_transaction($data, $id);
        if ($query) {
            $msg = "updated";
            $this->session->set_flashdata('succ_msg', 'Data Updated Succefully');
        } else {

            $msg = " not updated";
            $this->session->set_flashdata('succ_msg', 'Data Updated Not Succefully');
            redirect(base_url() . 'account/view_chain_master');
        }
        //redirect(base_url().'account/view_chain_master');
    }

    function view_bill_account() {
        $this->load->model('account_model');
        $data['heading'] = 'Bill Account';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'bill_account';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['bill_account'] = $this->account_model->all_bill_account();
        $data['page'] = 'newbackend/dashboard1/account/view_bill_account';
        $this->load->view('newbackend/common/index', $data);
    }

    function logout() {

        /* echo "eeeee";
          echo $this->db->database;
          exit; */
        //UPDATE LOGIN_SESSION TABLE						
        $session_data = array(
            'logout_datetime' => date('Y-m-d H:i:s')
        );
        $user_id = $this->session->userdata('user_id');
        $this->super_model->logout_details($user_id, $session_data);


        $this->session->sess_destroy();
        redirect('account/index');
    }

    function checkUserName() {
        $userName = $this->input->post('userName');

        $query = $this->account_model->checkUserName($userName);
        if ($query>0) {
            $data = array(
               'status' => "sucess",
               'message' => $userName,
                'data'=>1,
            );
        }else{
              $data = array(
                'status' => "fail",
                'message' => $userName,
                'data'=>0,
            );
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

function loginSessionUpdate() {
        
		date_default_timezone_set("Asia/Kolkata");
		$data=array(
		'lastUse'=>date('Y-m-d H:i:s'),

			);
			
		//$id=$this->session->userdata('mach_id');
		//echo "<pre>";
		//print_r($data);
		//print_r($this->session->all_userdata());
		//echo $id;exit;
       // $query = $this->account_model->loginSessionUpdate($data,$id);
	   $query =1;
        if ($query) {
            $data = array(
                'status' => "sucess",
				'msg'=>$id
              
            );
        }else{
		$data = array(
                'status' => "fail",
                'msg'=>$id
            );
		}
		
		//$data=$this->session->userdata('login_session_id');
        header('Content-Type: application/json');
        echo json_encode($data);
    }

}

?>