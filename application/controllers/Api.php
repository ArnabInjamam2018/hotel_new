<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('file', 'url');
	}

	public function db5($database_chain_id)
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		$unique_database = $this->dashboard_model->get_chain_database($database_chain_id);
		$user11 = $unique_database->database_user;
		$pass11 = $unique_database->database_pwd;
		$database = $unique_database->database_name;
		//echo "$user11";echo "</br>";
		//	echo "$pass11";echo "</br>";
		//	echo "$database";echo "</br>";exit;
		$db['superadmin'] = array(
			'dsn'   => '',
			'hostname' => 'localhost',
			'username' => $user11,
			'password' => $pass11,
			'database' => $database,
			'dbdriver' => 'mysqli',
			'dbprefix' => '',
			'pconnect' => FALSE,
			'db_debug' => TRUE,
			'cache_on' => FALSE,
			'cachedir' => '',
			'char_set' => 'utf8',
			'dbcollat' => 'utf8_general_ci',
			'swap_pre' => '',
			'encrypt' => FALSE,
			'compress' => FALSE,
			'stricton' => FALSE,
			'failover' => array(),
			'save_queries' => TRUE
		);
		//echo "<pre>";
		//	echo $this->db1->database;
		//    print_r($db1['default']);exit;
		$CI = &get_instance();
		$this->db1 = $CI->load->database($db['superadmin'], true);
	}

	function getAvailableRoom1()
	{
		$hid = $_POST['hid'];
		$cid = $_POST['cid'];
		$this->db5($cid);
		date_default_timezone_set('Asia/Kolkata');
		//checkin time logic
		$this->load->model('dashboard_model');
		$date = date("Y-m-d");
		$array = $this->dashboard_model->get_booking_details_by_date($date, $hid);
		$i = 0;
		$rooms = array();
		foreach ($array as $key) {
			$room_number = $this->dashboard_model->get_room_number_exact($key->room_id);
			$rooms[$i] = $room_number->room_no;
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($rooms);
	}


	public function getAvailableRoom()
	{

		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		$hid = $_POST['hid'];
		$cid = $_POST['cid'];
		//print($cid);exit;
		$this->db5($cid);
		echo $this->db->database;
		//echo '<br>'.$this->db1->database;exit;
		date_default_timezone_set('Asia/Kolkata');
		//checkin time logic		  
		$this->load->model('dashboard_model');
		$this->load->model('bookings_model');
		$date = date("Y-m-d");
		$dateG = date("m/d/Y");
		$array = $this->dashboard_model->get_booking_details_by_date($date, $hid);
		$arrayG = $this->bookings_model->get_group_booking_details_by_date($dateG, $hid);
		//echo "<pre>";
		//print_r($arrayG);
		$i = 0;

		if (!empty($array)) {
			foreach ($array as $key) {
				$room_number = $this->dashboard_model->get_room_number_exact($key->room_id);
				if ($key->group_id != 0) {
					echo "<option value=\"" . $room_number->room_no . "_" . $key->booking_id . "_" . $key->hotel_id . "_" . $key->g_name . " \">" . $room_number->room_no . "&nbsp;->&nbsp;(&nbsp;" . $key->booking_id . " - G )&nbsp;-&nbsp;" . $key->g_name . "&nbsp;</option>";
				} else {
					echo "<option value=\"" . $room_number->room_no . "_" . $key->booking_id . "_" . $key->hotel_id . "_" . $key->g_name . " \">" . $room_number->room_no . "&nbsp;->&nbsp;(&nbsp;" . $key->booking_id . " )&nbsp;-&nbsp;" . $key->g_name . "&nbsp;</option>";
				}
			}
		}
	}

	function getAvailableRoomGP()
	{

		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		$hid = $_POST['hid'];
		$cid = $_POST['cid'];
		$this->db5($cid);
		date_default_timezone_set('Asia/Kolkata');
		//checkin time logic
		$this->load->model('dashboard_model');
		$this->load->model('bookings_model');
		$date = date("Y-m-d");
		//$dateG = date("Y-m-d");
		$dateG = date("m/d/Y");
		//$date="2017-06-29";
		//$dateG = "06/29/2017";
		$array = $this->dashboard_model->get_booking_details_by_date($date, $hid);
		$arrayG = $this->bookings_model->get_group_booking_details_by_date($dateG, $hid);
		//echo "<pre>";
		//print_r($arrayG);
		$i = 0;
		if (!empty($arrayG)) {
			foreach ($arrayG as $keyG) {
				$gbStat = $this->bookings_model->get_group_booking_status($keyG->id);
				//print_r($arr);
				//echo implode(',',$arr)	;		   
				if ($gbStat->booking_status_id == '5') {
					$gName = $this->dashboard_model->get_guest_details($keyG->guestID);
					$gbRoom = $this->bookings_model->get_group_booking_room($keyG->id);
					$arr = array();
					foreach ($gbRoom as $keyRoom) {
						array_push($arr, $keyRoom->roomNO);
					}
					echo "<option value=\" " . implode(',', $arr) . "_" . $keyG->id . "_" . $gbStat->hotel_id . "_" . $gName[0]->g_name . " \">" . $keyG->id . " -> ( " . implode(',', $arr) . " ) - " . $gName[0]->g_name . "</option>";
				}
			}
		}
	}

	// API to get the pos data to hotelObjects, function called from yummPos pos controller => function view($sale_id = NULL, $modal = NULL)... >_<
	function pos_fetch_data()
	{

		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		$hid = $_POST['hid'];
		$cid = $_POST['cid'];
		$this->db5($cid);
		// $val= $this->input->post("val");
		$val = $_POST["val1"];
		$val1 = $_POST["val2"];
		$invoice = json_decode($val);
		$item = json_decode($val1);
		//Invoice Data Generator
		$date = $invoice->date;
		$pos_sale_id = $invoice->id;
		$invoice_id = $invoice->reference_no;
		$room_no = $invoice->room_no;
		$room_gp = $invoice->room_gp;
		if ($room_gp == 'sb') {
			$booking_id = $invoice->hotel_booking_id;
		} else {
			$booking_id = $invoice->hotel_booking_id;;
			//$booking_id = $room_no;
		}
		$grand_total = $invoice->grand_total;
		$total_discount = $invoice->total_discount;
		$total_tax = ceil($invoice->lvat + $invoice->fvat + $invoice->sc + $invoice->st);
		$amount = $grand_total + $total_discount - $total_tax;
		$paid = $invoice->paid;
		$due = $grand_total - $paid;

		$invoice_array = array(
			'booking_id' => $booking_id,
			'booking_type' => $room_gp,
			'invoice_number' => $invoice_id,
			'bill_amount' => $amount,
			'tax' => $total_tax,
			'discount' => $total_discount,
			'total_amount' => $grand_total,
			'total_paid' => $paid,
			'total_paid_pos' => $paid,
			'total_due' => $due,
			'date' => date("Y-m-d H:i:s", strtotime($date)),
			'lvat' => $invoice->lvat,
			'fvat' => $invoice->fvat,
			'sc' => $invoice->sc,
			'st' => $invoice->st,
			'product_tax' => $invoice->product_tax,
			'pos_sale_id' => $pos_sale_id,
			'food_cgst' => $invoice->food_cgst,
			'food_sgst' => $invoice->food_sgst,
			'food_igst' => $invoice->food_igst,
			'liquor_cgst' => $invoice->liquor_cgst,
			'liquor_sgst' => $invoice->liquor_sgst,
			'liquor_igst' => $invoice->liquor_igst,
			'hotel_id' => $hid
		);

		$query = $this->dashboard_model->insert_pos($invoice_array);
		//insert items
		for ($i = 0; $i < count($item); $i++) {
			$item_array = array(
				'item_name' => $item[$i]->product_name,
				'item_quantity' => $item[$i]->quantity,
				'unit_price' =>  $item[$i]->unit_price,
				'pos_id' => $query,
				'hotel_id' => $hid
			);
			$query2 = $this->dashboard_model->insert_pos_item($item_array);
		}
		if ($query && isset($query) && $query2) {
			// echo "1";
		} else {
			echo "0";
		}
	}

	function add_purchase_item()
	{
		$hid = $_POST['hid'];
		$cid = $_POST['cid'];
		$this->db5($cid);
		$purchase = $_POST["val2"];
		$purchase_data = json_decode($purchase, true);
		//print_r($purchase_data);
		for ($i = 0; $i < count($purchase_data); $i++) {
			$p_price = ($purchase_data[$i]['net_unit_cost']) * ($purchase_data[$i]['quantity']);
			$tax = $purchase_data[$i]['item_tax'];
			$tax_percentage = ($tax / $p_price) * 100;
			$array_data = array(
				'p_id' => $purchase_data[$i]['product_id'],
				'hotel_id' => 6,
				'p_date' => $purchase_data[$i]['date'],
				'p_id' => $purchase_data[$i]['product_id'],
				'p_i_name' => $purchase_data[$i]['product_name'],
				'p_i_description' => "",
				'p_i_price' => $purchase_data[$i]['net_unit_cost'],
				'p_i_quantity' => $purchase_data[$i]['quantity'],
				'p_unit' => "pcs",
				'p_price' => $p_price,
				'p_tax' => $tax_percentage,
				'p_discount' => 0,
				'p_total_price' => $p_price + $tax,

			);

			echo $query = $this->dashboard_model->add_purchase_pos($array_data);
		}
	}


	function pos_fetch_data1()
	{
		$hid = $_POST['hid'];
		$cid = $_POST['cid'];
		$this->db5($cid);
		$val1 = $_POST['val1'];
		$val2 = $_POST['val2'];
		$sql = "INSERT INTO `test`(`extra1`, `extra2`) VALUES ('$val1','$val2')";
		$this->db1->query($sql);
	}

	function delete_pos_sale_data()
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		$hid = $_POST['hid'];
		$cid = $_POST['cid'];
		$this->db5($cid);
		$val = $_POST['sid'];
		$pos = $this->dashboard_model->getPosID($val, $hid);
		$posID = $pos->pos_id;
		//exit;
		$this->dashboard_model->deletePosSaleItem($posID, $hid);
		$this->dashboard_model->deletePosSale($posID, $hid);
	}

	function updatePOS()
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		$hid = $_REQUEST['hid'];
		$cid = $_REQUEST['cid'];
		$this->db5($cid);
		$val1 = $_REQUEST['reference_no'];
		$val2 = $_REQUEST['amount_paid'];
		$this->dashboard_model->updatePosPaidAmount($val1, $val2, $hid);
	}

	function getGuest()
	{
		$hid = $_POST['hid'];
		$cid = $_POST['cid'];
		$this->db5($cid);
		$guest = $this->dashboard_model->getGuest();
		$arr = array();
		foreach ($guest as $guest) {
			$val = $guest->g_id . "-" . $guest->g_name . "-HOTEL";
			$arr1 = array('id' => $val, 'text' => $guest->g_name);
			$arr[] = $arr1;
		}
		//print_r($arr);
		echo json_encode($arr);
	}

	/*function test(){
		$val1 = "hotel_aminity";
		$val2 = "4";
	   //$this->dashboard_model->test($val1,$val2);  
	   $query="select * from hotel_aminity";
	   $result=$this->db->query($query);
	while ($line = mysql_fetch_array($result)) {
		$i=0;
		foreach ($line as $col_value) {
			$field=mysql_field_name($result,$i);
			$array[$field] = $col_value;
			$i++;
		}
	} 
	print_r($array);  
	}*/

	function getAvailableRoom_openPos()
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		$hid = $_POST['hid'];
		$cid = $_POST['cid'];
		$this->db5($cid);
		date_default_timezone_set('Asia/Kolkata');
		$room_no = $_REQUEST['room_no'];
		$hotel_booking_id = $_REQUEST['hotel_booking_id'];
		$hotel_id = $_REQUEST['hotel_id'];
		$opt = $room_no . "-" . $hotel_booking_id . "-" . $hotel_id;
		//exit;
		//checkin time logic
		$this->load->model('dashboard_model');
		$this->load->model('bookings_model');
		$date = date("Y-m-d");
		$dateG = date("m/d/Y");
		$array = $this->dashboard_model->get_booking_details_by_date($date, $hid);
		$arrayG = $this->bookings_model->get_group_booking_details_by_date($dateG, $hid);
		//echo "<pre>";
		//print_r($arrayG);
		$i = 0;
		//$rooms=array();
		if (!empty($array)) {
			foreach ($array as $key) {
				$room_number = $this->dashboard_model->get_room_number_exact($key->room_id);
				//echo "W-".$room_number->room_no;
				//echo $key->booking_id;
				echo $room_no . "-" . $hotel_booking_id . "==" . $room_number->room_no . "-" . $key->booking_id;
				if ($key->group_id != 0) { ?>
					<option value="<?php echo $room_number->room_no . "_" . $key->booking_id . "_" . $key->hotel_id . "_" . $key->g_name; ?>" <?php if ($room_no . "" . $hotel_booking_id == $room_number->room_no . "" . $key->booking_id) {
																																					echo 'selected="selected"';
																																				} ?>>
						<?php echo $room_number->room_no . "&nbsp;->&nbsp;(&nbsp;" . $key->booking_id . "  - G )&nbsp;-&nbsp;" . $key->g_name; ?></option>
				<?php } else { ?>
					<option value="<?php echo $room_number->room_no . "_" . $key->booking_id . "_" . $key->hotel_id . "_" . $key->g_name; ?>" <?php if ($room_no . "-" . $hotel_booking_id == $room_number->room_no . "-" . $key->booking_id) {
																																					echo 'selected="selected"';
																																				} ?>>
						<?php echo $room_number->room_no . "&nbsp;->&nbsp;(&nbsp;" . $key->booking_id . " )&nbsp;-&nbsp;" . $key->g_name; ?></option>
				<?php
				}
			}
			exit;
		}
	}

	function getAvailableRoomGP_openPos()
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		$hid = $_POST['hid'];
		$cid = $_POST['cid'];
		$this->db5($cid);
		date_default_timezone_set('Asia/Kolkata');
		$room_no = $_REQUEST['room_no'];
		$hotel_booking_id = $_REQUEST['hotel_booking_id'];
		$hotel_id = $_REQUEST['hotel_id'];
		//checkin time logic
		$this->load->model('dashboard_model');
		$this->load->model('bookings_model');
		$date = date("Y-m-d");
		$dateG = date("m/d/Y");
		$array = $this->dashboard_model->get_booking_details_by_date($date, $hid);
		$arrayG = $this->bookings_model->get_group_booking_details_by_date($dateG, $hid);
		//echo "<pre>";
		//print_r($arrayG);
		$i = 0;
		if (!empty($arrayG)) {
			foreach ($arrayG as $keyG) {
				$gbStat = $this->bookings_model->get_group_booking_status($keyG->id, $hid);
				//print_r($arr);
				//echo implode(',',$arr)	;		   
				if ($gbStat->booking_status_id == '5') {
					$gName = $this->dashboard_model->get_guest_details($keyG->guestID);
					$gbRoom = $this->bookings_model->get_group_booking_room($keyG->id);
					$arr = array();
					foreach ($gbRoom as $keyRoom) {
						array_push($arr, $keyRoom->roomNO);
					} ?>
					<option value="<?php echo $keyG->id . "_" . $keyG->id . "_" . $gbStat->hotel_id . "_" . $gName[0]->g_name; ?>" <?php if ($room_no . "" . $hotel_booking_id == $keyG->id . "" . $keyG->id) {
																																		echo 'selected="selected"';
																																	} ?>><?php echo $keyG->id . " -> ( " . implode(',', $arr) . " ) - " . $gName[0]->g_name; ?>
					</option>
<?php
				}
			}
		}
	}

	public function frontendhotelbooking()
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		$data = json_decode(file_get_contents('php://input'), true);

		$this->load->model('dashboard_model');
		$this->load->model('bookings_model');


		$date1 = date("Y-m-d", strtotime($data['std']));
		$date2 = date("Y-m-d", strtotime($data['end']));
		$rmid =  $data['rmid'];
		$hotel_id =  $data['hotel_id'];
		$cid =  $data['cid'];
		$adult =  $data['adult'];
		$child =  $data['child'];
		$state_id = $data['state_id'];
		$total = $adult + $child;

		$this->db5($cid);


		$query = $this->db1->where('room_id', $rmid);
		$query = $this->db1->get('hotel_room');
		$room_array = $query->result_array();

		if ($room_array[0]['max_occupancy'] < $total) {
			echo json_encode(array("status" => 400, "msg" => "Bed limit exceeds"));
			die;
		}

		$sql = "select room_id,booking_id from hotel_bookings where ((date(cust_from_date) between '" . $date1 . "' and '" . $date2 . "' ) or  (date(cust_end_date) between '" . $date1 . "' and '" . $date2 . "' ) or (date(cust_from_date) <'" . $date1 . "' and date(cust_end_date) >'" . $date2 . "') or (date(cust_from_date) >'" . $date1 . "' and date(cust_end_date) <'" . $date2 . "') or  (date(cust_from_date) = '" . $date2 . "' and date(cust_end_date) >'" . $date2 . "')) and booking_status_id !='7' and booking_status_id !='6' and  date(cust_end_date) != '" . $date1 . "' and  date(cust_from_date) != '" . $date2 . "' and room_id = '" . $rmid . "' and hotel_id = " . $hotel_id . "";
		$query = $this->db1->query($sql);



		if ($query->num_rows() > 0) {
			echo json_encode(array("status" => 400, "msg" => "Room not avaiable"));
			die;
		}


		$sql2 = "select * from unitavaillog where ((date(checkin_date) between '" . $date1 . "' and '" . $date2 . "' ) or  (date(checkout_date) between '" . $date1 . "' and '" . $date2 . "' ) or (date(checkin_date) <'" . $date1 . "' and date(checkout_date) >'" . $date2 . "') or (date(checkin_date) >'" . $date1 . "' and date(checkout_date) <'" . $date2 . "') or date(checkin_date) = '" . $date1 . "' or (date(checkin_date) = '" . $date2 . "' and date(checkout_date) >'" . $date2 . "')) and  date(checkout_date) != '" . $date1 . "' and  date(checkin_date) != '" . $date2 . "' and unit_id = '" . $rmid . "' and hotel_id = " . $hotel_id . "";

		$query = $this->db1->query($sql2);

		if ($query->num_rows() > 0) {
			echo json_encode(array("status" => 400, "msg" => "Room not avaiable"));
			die;
		}


		$date1 = date_create($date1);
		$date2 = date_create($date2);
		$date_diff = date_diff($date1, $date2);
		$number_of_days = $date_diff->days;

		$total_with_out_tax = $number_of_days * $room_array[0]['room_rent'];

		if ($state_id == 29) {
			$same_state = 1;
		} else {
			$same_state = 0;
		}


		$sql = "SELECT * from taxplan_view
				WHERE `hotel_id` =" . $hotel_id . " AND
				`tc_type` = 'Booking' AND `tax_mode` = 'p' AND
				'$total_with_out_tax' BETWEEN `r_start_price` AND `r_end_price` AND `same_state`=" . $same_state . "";
		$result = $this->db1->query($sql);
		$tax_data = $result->result_array();


		$cgst_percentage = $tax_data[0]['tax_value'];
		$sgst_percentage = $tax_data[1]['tax_value'];
		$total_tax_percentage = $cgst_percentage + $sgst_percentage;
		$cgst_amount = (($total_with_out_tax * $cgst_percentage) / 100);
		$sgst_amount = (($total_with_out_tax * $sgst_percentage) / 100);
		$total_tax_amount = $cgst_amount + $sgst_amount;

		$all_total = $total_with_out_tax + (($total_with_out_tax * $total_tax_percentage) / 100);

		echo json_encode(array(
			"status" => 200,
			"msg" => "Success",
			"adult"=>$adult,
			"child"=>$child,
			"total_person"=>($adult+$child),
			"check_in"=>$date1,
			"check_out"=>$date2,
			"total_with_out_tax" => $total_with_out_tax,
			"all_total" => $all_total,
			"cgst_amount" => $cgst_amount,
			"sgst_amount" => $sgst_amount,
			"total_tax_amount" => $total_tax_amount,
			"total_tax_percentage" => $total_tax_percentage,
			"cgst_percentage" => $cgst_percentage,
			"sgst_percentage" => $sgst_percentage,
			"number_of_days" => $number_of_days,
			"room_array" => $room_array,
			"tax_data" => $tax_data
		));
		die;
	}


	public function hotelList()
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		$data = json_decode(file_get_contents('php://input'), true);

		$date1 = date("Y-m-d", strtotime($data['start_date']));
		$date2 = date("Y-m-d", strtotime($data['end_date']));

		$hotel_id =  $data['hotel_id'];
		$cid =  $data['cid'];
		$this->db5($cid);
		if (isset($data['start_date']) && isset($data['end_date'])) {
			$sql = "select room_id from hotel_bookings where ((date(cust_from_date) between '" . $date1 . "' and '" . $date2 . "' ) or  (date(cust_end_date) between '" . $date1 . "' and '" . $date2 . "' ) or (date(cust_from_date) <'" . $date1 . "' and date(cust_end_date) >'" . $date2 . "') or (date(cust_from_date) >'" . $date1 . "' and date(cust_end_date) <'" . $date2 . "') or  (date(cust_from_date) = '" . $date2 . "' and date(cust_end_date) >'" . $date2 . "')) and booking_status_id !='7' and booking_status_id !='6' and  date(cust_end_date) != '" . $date1 . "' and  date(cust_from_date) != '" . $date2 . "' and hotel_id = " . $hotel_id . "";
			$query = $this->db1->query($sql);
			$hotel_bookings = $query->result_array();

			$ignore = [];
			for ($i = 0; $i < count($hotel_bookings); $i++) {
				$ignore[] = $hotel_bookings[$i]['room_id'];
			}
			//print_r($ignore);die;
			$query = $this->db1->where_not_in('room_id', $ignore);
		}

		$query = $this->db1->where('room_status!=', 'D');
		$query = $this->db1->where('hotel_id', $hotel_id);

		$room_id =  $data['room_id'];
		if (isset($data['room_id'])) {
			$query = $this->db1->where('room_id', $room_id);
		}
		$query = $this->db1->get('hotel_room');
		//echo $this->db1->last_query();die;
		$result = $query->result_array();

		$this->db1->select('room_feature_id,room_feature_name,room_feature_slug');
		$query = $this->db1->get(TABLE_PRE . 'room_features');
		$room_fetature_data = $query->result_array();

		$sql = "SELECT `state_id`,`country_id`,`Name`,`Type` FROM `area_state`";
		$qry = $this->db->query($sql);
		$state_data = $qry->result_array();



		if (count($result) > 0) {
			echo json_encode(array("status" => 200, "count" => count($result), "msg" => "Success",  "data" => $result, "room_fetature_data" => $room_fetature_data, "state_data" => $state_data));
			die;
		} else {
			echo json_encode(array("status" => 500, "count" => count($result), "msg" => "No Data Found", "data" => $result, "room_fetature_data" => $room_fetature_data, "state_data" => $state_data));
			die;
		}
	}
}


?>