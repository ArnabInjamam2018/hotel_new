
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Api_v2 extends CI_Controller
{

	public function __construct()
    {
        parent::__construct();

      
    }

   


 function getAvailableRoom1(){




        date_default_timezone_set('Asia/Kolkata');

         //checkin time logic
        $this->load->model('dashboard_model');

       
       

      
        
        $date=date("Y-m-d");
        

       $array= $this->dashboard_model->get_booking_details_by_date($date);

       $i=0;
       	$rooms=array();

       foreach ($array as $key ) {

       	$room_number=$this->dashboard_model->get_room_number_exact($key->room_id);

       	$rooms[$i] =$room_number->room_no;
       	$i++;

       }





          /*  $data = array(
                'test' => $array,
                
                );*/

        header('Content-Type: application/json');
        echo json_encode($rooms);






    }


     function getAvailableRoom(){
        date_default_timezone_set('Asia/Kolkata');
         //checkin time logic
        $this->load->model('dashboard_model');
		$this->load->model('bookings_model');
        $date=date("Y-m-d");
		$dateG = date("m/d/Y");
        $array= $this->dashboard_model->get_booking_details_by_date($date);
		$arrayG= $this->bookings_model->get_group_booking_details_by_date($dateG);
		//echo "<pre>";
		//print_r($arrayG);
       $i=0;
	   
        //$rooms=array();
    if(!empty($array)){
		foreach ($array as $key ) {
			$room_number=$this->dashboard_model->get_room_number_exact($key->room_id);
			if($key->group_id != 0){
				echo "<option value=\" ".$room_number->room_no."-".$key->booking_id."-".$key->hotel_id." \">".$room_number->room_no."&nbsp;->&nbsp;(&nbsp;".$key->booking_id."  - G )&nbsp;-&nbsp;".$key->g_name."&nbsp;</option>";
			} else {
				echo "<option value=\" ".$room_number->room_no."-".$key->booking_id."-".$key->hotel_id." \">".$room_number->room_no."&nbsp;->&nbsp;(&nbsp;".$key->booking_id." )&nbsp;-&nbsp;".$key->g_name."&nbsp;</option>";				
			}

		}
	}
     //   header('Content-Type: application/json');
       // echo json_encode($rooms);

    }

     function getAvailableRoomGP(){
        date_default_timezone_set('Asia/Kolkata');
         //checkin time logic
        $this->load->model('dashboard_model');
		$this->load->model('bookings_model');
        $date=date("Y-m-d");
		$dateG = date("m/d/Y");
        $array= $this->dashboard_model->get_booking_details_by_date($date);
		$arrayG= $this->bookings_model->get_group_booking_details_by_date($dateG);
		//echo "<pre>";
		//print_r($arrayG);
       $i=0;
	   
	 if(!empty($arrayG)){
		foreach ($arrayG as $keyG ) {
			$gbStat=$this->bookings_model->get_group_booking_status($keyG->id);
			   //print_r($arr);
             //echo implode(',',$arr)	;		   
			if($gbStat->booking_status_id == '5'){
			   $gName = $this->dashboard_model->get_guest_details($keyG->guestID);	
			   $gbRoom=$this->bookings_model->get_group_booking_room($keyG->id);
			   $arr = array();
			   foreach ($gbRoom as $keyRoom ) {
				   array_push($arr,$keyRoom->roomNO);
			   }
			   echo "<option value=\" ".$keyG->id."-".$keyG->id."-".$gbStat->hotel_id." \">".$keyG->id." -> ( ".implode(',',$arr)." ) - ".$gName[0]->g_name."</option>";
			}
		}
	}


    }



	

    function pos_fetch_data(){

        // $val= $this->input->post("val");
      
         $val=$_POST["val1"];
         $val1=$_POST["val2"];
         //$val='{"id":"84","date":"2016-05-09 15:31:25","reference_no":"SALE/POS/2016/05/0133","customer_id":"1","customer":"Walk-in Customer","biller_id":"3","biller":"Billu ka Dhaba","warehouse_id":"2","note":"","staff_note":"","total":"109.0900","product_discount":"0.0000","order_discount_id":null,"total_discount":"0.0000","order_discount":"0.0000","product_tax":"10.9100","order_tax_id":"1","order_tax":"0.0000","total_tax":"10.9100","shipping":"0.0000","grand_total":"120.0000","sale_status":"completed","payment_status":"due","payment_term":"0","due_date":null,"created_by":"1","updated_by":null,"updated_at":null,"total_items":"1","pos":"1","paid":"0.0000","return_id":null,"surcharge":"0.0000","attachment":null,"order_type":"Room Service","lvat":"0.00","fvat":"0.00","sc":"0.00","st":"0.00","room_no":"567","is_send":"0","guest_type":"Hotel Guest"}';


          //$val1='[{"id":"142","sale_id":"84","product_id":"12","product_code":"chpto","product_name":"Chilly Potato","product_type":"standard","option_id":"0","net_unit_price":"109.0900","unit_price":"120.0000","quantity":"1.0000","warehouse_id":"2","item_tax":"10.9100","tax_rate_id":"2","tax":"10.0000%","discount":"0","item_discount":"0.0000","subtotal":"120.0000","serial_no":"","real_unit_price":"120.0000","tax_code":"VAT10","tax_name":"VAT @10%","tax_rate":"10.0000","variant":null}]';
   
          $invoice=json_decode($val);
          $item=json_decode($val1);

          //Invoice Data Generator

          $date=$invoice->date;
          $invoice_id=$invoice->reference_no;
          $room_no=$invoice->room_no;
          $room_gp=$invoice->room_gp;
		  if($room_gp == 'sb'){
			  $booking_id = $invoice->hotel_booking_id;
		  } else {
			  $booking_id = $room_no;
		  }
          $grand_total=$invoice->grand_total;
          $total_discount=$invoice->total_discount;
          $total_tax=ceil($invoice->lvat+$invoice->fvat+$invoice->sc+$invoice->st);
          $amount=$grand_total+$total_discount-$total_tax;
          $paid=$invoice->paid;
          $due=$grand_total-$paid;


          $invoice_array=array('booking_id' => $booking_id ,
			'booking_type' => $room_gp,	
            'invoice_number' => $invoice_id,
            'bill_amount' =>$amount,
            'tax' =>$total_tax,
            'discount' =>$total_discount,
            'total_amount' =>$grand_total,
            'total_paid' => $paid,
            'total_paid_pos' => $paid,
            'total_due' =>$due ,
            'date' => date("Y-m-d H:i:s",strtotime($date)),
            'lvat' => $invoice->lvat,
            'fvat' => $invoice->fvat,
            'sc' => $invoice->sc,
            'st' => $invoice->st,
            'product_tax' => $invoice->product_tax,



            );

           $query=$this->dashboard_model->insert_pos($invoice_array);

            //insert items

         for($i=0;$i<count($item);$i++){

            $item_array=array(

                 'item_name' => $item[$i]->product_name,
                  'item_quantity' => $item[$i]->quantity,
                  'unit_price' =>  $item[$i]->unit_price,
                  'pos_id' => $query,


              );

            $query2=$this->dashboard_model->insert_pos_item($item_array);
         }

            


         if($query && isset($query) && $query2){
         // echo "1";
         }else{
          echo "0";
         }



    }

function add_purchase_item(){
	//	$purchase='{"2":{"product_id":"9","product_code":"CR","product_name":"Chicken Rice","option_id":null,"net_unit_cost":125,"unit_cost":"150.00","quantity":"1","quantity_balance":"1","warehouse_id":"1","item_tax":"25.00","tax_rate_id":"4","tax":"20.0000%","discount":"0","item_discount":"0.00","subtotal":"150.00","expiry":null,"real_unit_cost":"150.00","date":"2016-05-04","status":"received"},"1":{"product_id":"21","product_code":"CTK","product_name":"Chicken Tikka kabab","option_id":null,"net_unit_cost":"199.00","unit_cost":"199.00","quantity":"1","quantity_balance":"1","warehouse_id":"1","item_tax":"0.00","tax_rate_id":"1","tax":"","discount":"0","item_discount":"0.00","subtotal":"199.00","expiry":null,"real_unit_cost":"199.00","date":"2016-05-04","status":"received"},"0":{"product_id":"24","product_code":"SW","product_name":"signature","option_id":"5","net_unit_cost":"599.00","unit_cost":"599.00","quantity":"100","quantity_balance":"100","warehouse_id":"1","item_tax":"0.00","tax_rate_id":"1","tax":"","discount":"0","item_discount":"0.00","subtotal":"59900.00","expiry":null,"real_unit_cost":"599.00","date":"2016-05-04","status":"received"}}';

   $purchase=$_POST["val2"];
		
		$purchase_data=json_decode($purchase,true);
   
    
    //print_r($purchase_data);

    for($i=0; $i<count($purchase_data); $i++){

      $p_price=($purchase_data[$i]['net_unit_cost'])*($purchase_data[$i]['quantity']);
      $tax =$purchase_data[$i]['item_tax'];
      $tax_percentage=($tax/$p_price)*100;

      $array_data=array(

        'p_id' =>$purchase_data[$i]['product_id'],
         'hotel_id' =>6,
          'p_date' =>$purchase_data[$i]['date'],
           'p_id' =>$purchase_data[$i]['product_id'],
           'p_i_name' => $purchase_data[$i]['product_name'],
            'p_i_description' =>"",
             'p_i_price' =>$purchase_data[$i]['net_unit_cost'],
              'p_i_quantity' =>$purchase_data[$i]['quantity'],
               'p_unit' =>"pcs",
                'p_price' =>$p_price,
                'p_tax' =>$tax_percentage,
                'p_discount' => 0,
                'p_total_price' => $p_price+$tax,

             


        );

      echo $query=$this->dashboard_model->add_purchase_pos($array_data);


     
    }

	
}




function pos_fetch_data1(){
		$val1=$_POST['val1'];
		$val2=$_POST['val2'];
		
		$sql= "INSERT INTO `test`(`extra1`, `extra2`) VALUES ('$val1','$val2')";
		$this->db->query($sql);
}

function delete_pos_sale_data(){
    $val=$_POST['val'];
	$pos = $this->dashboard_model->getPosID($val);  		
    $posID = $pos->pos_id;
	$this->dashboard_model->deletePosSaleItem($posID);
    $this->dashboard_model->deletePosSale($val);
}

function updatePOS(){
    $val1=$_POST['val1'];
	$val2=$_POST['val2'];
	$this->dashboard_model->updatePosPaidAmount($val1,$val2);  		
}

    function getGuest(){
		$guest=$this->dashboard_model->getGuest();
		$arr = array();
		foreach($guest as $guest){ 
			$val =$guest->g_id."-".$guest->g_name."-HOTEL";
			$arr1 = array('id'=>$val,'text'=>$guest->g_name);
			$arr[] = $arr1;
		 }
		 //print_r($arr);
        echo json_encode($arr);
	}

/*function test(){
	$val1 = "hotel_aminity";
	$val2 = "4";
   //$this->dashboard_model->test($val1,$val2);  
   $query="select * from hotel_aminity";
   $result=$this->db->query($query);
while ($line = mysql_fetch_array($result)) {
    $i=0;
    foreach ($line as $col_value) {
        $field=mysql_field_name($result,$i);
        $array[$field] = $col_value;
        $i++;
    }
} 

print_r($array);  
}*/




}


?>