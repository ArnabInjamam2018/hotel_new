<?php

class Room
{
}
class Result
{
}
class Event
{
}
class Bookings  extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('login');
        }
        // $this->load->database();
        // DYNAMIC BATABASE 
        $db['superadmin'] = array(
            'dsn'   => '',
            'hostname' => 'localhost',
            'username' => $this->session->userdata('username'),
            'password' => $this->session->userdata('password'),
            'database' => $this->session->userdata('database'),
            'dbdriver' => 'mysqli',
            'dbprefix' => '',
            'pconnect' => FALSE,
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array(),
            'save_queries' => TRUE
        );

        //echo "<pre>";
        //	echo $this->db1->database;
        //    print_r($db1['default']);exit;

        $CI = &get_instance();
        $this->db1 = $CI->load->database($db['superadmin'], true);
        $this->arraay = $this->session->userdata('per');
        //$login_id=$this->session->userdata('admin_id');
        //if(!$login_id>0){redirect(base_url('admin/login')); }
        $this->load->model('mod_common');
        $this->load->model('bookings_model');
        $this->load->model('dashboard_model');
        //$this->load->model('reports_model');
        $this->load->model('setting_model');

        $this->load->library('grocery_CRUD');
        $this->load->library('table');
    }
    public function customer()
    {
        $data = array();
        $data['page_title'] = 'Adminpanel : Content customer management';
        $data['success_message'] = $this->session->flashdata('success_message');

        $crud = new grocery_CRUD();

        $crud->set_table('customer_details')
            ->set_subject('customer')
            ->columns('customer_id', 'name', 'identification', 'zender', 'city', 'state', 'country_id')
            ->required_fields('name')
            ->display_as('name', 'Name')
            ->display_as('country_id', 'Country')
            ->display_as('is_deleated', 'Deleated')
            ->display_as('customer_id', 'ID')
            ->display_as('date_added', 'Created on')
            ->set_rules('email_id', 'Email', 'valid_email|required')
            ->set_rules('postal_code', 'Postal Code', 'integer|max_length[6]')
            ->add_fields('name', 'identification', 'zender', 'contact_number', 'email_id', 'address', 'city', 'state', 'country_id', 'postal_code', 'date_added')
            ->edit_fields('name', 'identification', 'zender', 'contact_number', 'email_id', 'address', 'city', 'state', 'country_id', 'postal_code')
            ->field_type('is_deleated', 'dropdown', array('1' => 'yes', '0' => 'No', '' => 'No'), 0)
            ->field_type('date_added', 'hidden', date("Y-m-d H:i:s"))
            ->set_relation('country_id', 'hotel_countries', 'country_name')
            ->unset_delete()
            ->add_action('remove', base_url('resources/images/erase.png'), 'admin/index/delete_customer')

            ->where('is_deleated', 0);

        $output = $crud->render();
        foreach ($output as $key => $val) {
            $data[$key] = $val;
        }
        $this->mod_common->display_admin('dashbord', $data);
    }
    public function hotel_room()
    {
        $data = array();
        $data['page_title'] = 'Adminpanel : Room management';
        $data['success_message'] = $this->session->flashdata('success_message');

        $crud = new grocery_CRUD();

        $crud->set_table('hotel_room')
            ->set_subject('Room')
            ->columns('room_id', 'hotel_id', 'room_bed', 'room_image_thumb', 'room_rent', 'hotel_city', 'hotel_country', 'room_availability')
            ->required_fields('hotel_id', 'room_bed', 'room_availability', 'room_rent')
            ->set_field_upload('room_image_thumb', 'assets/uploads/room_thumb')
            ->set_field_upload('room_image', 'assets/uploads/room')
            ->display_as('hotel_id', 'Hotel')

            ->display_as('room_modification_date', 'Modified on')
            ->display_as('is_deleated', 'Deleated')
            ->display_as('room_id', 'ID')
            ->display_as('room_add_date', 'Created on')
            ->set_rules('room_rent', 'Email', 'decimal|required')
            ->add_fields('hotel_id', 'room_bed', 'room_image_thumb', 'room_image', 'ac_availability', 'room_rent', 'room_availability', 'room_add_date')
            ->edit_fields('hotel_id', 'room_bed', 'room_image_thumb', 'room_image', 'ac_availability', 'room_rent', 'room_availability')
            ->field_type('room_add_date', 'hidden', date("Y-m-d H:i:s"))
            ->set_relation('hotel_id', 'hotel_hotel_details', 'hotel_name');

        $output = $crud->render();
        foreach ($output as $key => $val) {
            $data[$key] = $val;
        }

        $this->mod_common->display_admin('dashbord', $data);
    }
    public function events()
    {
        $data = array();
        $data['page_title'] = 'Adminpanel : Event management';
        $data['success_message'] = $this->session->flashdata('success_message');

        $crud = new grocery_CRUD();

        $crud->set_table('event_list')
            ->set_subject('Event')
            ->columns('event_id', 'name', 'date', 'to_date', 'city', 'country_id')
            ->required_fields('name', 'date', 'city', 'country_id')
            ->set_field_upload('room_image_thumb', 'assets/uploads/room_thumb')
            ->set_field_upload('room_image', 'assets/uploads/room')
            ->display_as('hotel_id', 'Hotel')

            ->display_as('date', 'From Date')
            ->display_as('to_date', 'To Date')

            ->display_as('room_modification_date', 'Modified on')
            ->display_as('country_id', 'Country')
            ->display_as('event_id', 'ID')
            ->display_as('date_added', 'Created on')
            ->set_rules('room_rent', 'Email', 'decimal|required')
            ->set_rules('to_date', 'To Date', 'callback_range_validation')
            ->add_fields('name', 'date', 'to_date', 'city', 'country_id', 'description', 'date_added')
            ->edit_fields('name', 'date', 'to_date', 'city', 'country_id', 'description')
            ->field_type('date_added', 'hidden', date("Y-m-d H:i:s"))
            ->set_relation('country_id', 'hotel_countries', 'country_name');

        $output = $crud->render();
        foreach ($output as $key => $val) {
            $data[$key] = $val;
        }

        $this->mod_common->display_admin('dashbord', $data);
    }
    function range_validation()
    {
        $date1 = date_create($this->input->post('date'));
        $date2 = date_create($this->input->post('to_date'));
        $diff = date_diff($date1, $date2);
        if ($diff > 0) {
            return true;
        }
        $this->form_validation->set_message("range_validation", 'To date should be grater than from date');
        return false;
    }
    /*  start on 05/11/15   */
    function hotel_backend_rooms()
    {
        //$id = $this->session->userdata('user_id');
        //$data = $this->dashboard_model->admin_status($id);
        //if(isset($data) && $data){
        $hotel_id = $this->session->userdata('user_hotel');
        /*}else{
                $hotel_id = '';
                }*/
        $rooms = $this->bookings_model->all_rooms_hotelwise($hotel_id);
        $units = $this->bookings_model->all_units();
        $result = array();

        foreach ($rooms as $room) {

            $unit_id = $room->unit_id;
            $unit = $this->bookings_model->get_unit_name($unit_id);
            $unit_name = $unit->unit_name;
            $unit_id = $unit->id;


            $r = new Room();
            $r->id = $room->room_id;
            $s = "";
            $style = "<div style=" . $s . "></div>";
            $r->name = $room->room_no . "<br/>" . $room->room_bed . " Bed" . $style;
            $r->capacity = $room->room_bed;
            $r->status = $room->room_status;
            $r->unit_name = $unit_name;
            $r->unit_id = $unit_id;
            $result[] = $r;

            $unit_id_prev = $unit_id;
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }


    // Day Pilot Tree View Loading
    function hotel_backend_rooms_tree()
    {

        $hotel_id = $this->session->userdata('user_hotel');
        $rooms = $this->bookings_model->all_rooms_hotelwiseSql($hotel_id);
        $units = $this->bookings_model->all_units();
        $result = array();
        $i = 0;
        $j = 0;
        $status = '';
        if (isset($units) && $units) {
            foreach ($units as $unit) {
                $a = array();
                $x = new Room();
                $x->id = "unit";
                $x->name = "<div style=\"color: white; font-weight: 800; \">" . $unit->unit_name . "</div>";
                $x->unit_id = $unit->id;
                $x->expanded = true;
                $x->eventHeight = 20;
                $x->backColor = "rgba(0, 150, 136, 0.64)";
                //   echo "<pre>";
                //	print_r($rooms); exit;
                foreach ($rooms as $room) {

                    if ($room->unit_id == $unit->id) {

                        $unit_id = $room->unit_id;
                        $r = new Room();
                        $r->id = $room->room_id;
                        $r->eventHeight = 50;
                        $color = $room->color_primary;

                        date_default_timezone_set('Asia/Kolkata');
                        $d = date('Y-m-d');
                        $flg = 0;
                        $stat = '';
                        $ot = '';
                        $av = '';

                        if ($flg) {
                            $stat = 'Ota';
                            $ot = " active";
                        } else {
                            $stat = 'Avail';
                        }
                        if ($room->clean_id == 1) {
                            $av = " active";
                            $status = 'AV';
                        } elseif ($room->clean_id == 2 || $room->clean_id == 3) {
                            $av = " active";
                            $status = 'UM';
                        } elseif ($room->clean_id == 4) {
                            $av = " active";
                            $status = 'UR';
                        } else {
                            $av = "active";
                            $status = 'UM';
                        }

                        $s = " border-right: 7px solid #f9ba25;";
                        $style = "<div style=" . $s . "></div>";
                        $r->capacity = $room->room_bed;
                        $r->status = $room->room_status;

                        $ac = (explode(',', $room->room_features));
                        if ($ac[0] == '1') {
                            $AC = " active";
                        } else {
                            $AC = "";
                        }

                        $r->html = "<div style=\"font-size:14px; border-right: 7px solid " . $color . " ; padding:6px; cursor:pointer; margin-left: 40px;\"><div class='ot-av'><span class='ota'>Ota</span><span><small class='ac" . $AC . "'>AC</small><small class='ava" . $av . "'>" . $status . "</small></div><div style='color:" . $color . "' class='room-noo'>" . $room->room_no . "<span>" . $room->unit_type . "</span></div><div class='bed'><span>" . $room->default_occupancy . " </span><span>(" . $room->max_occupancy . ")</span></div></div>";

                        $r->css = "resource_action_menu";
                        $r->bubbleHtml = "Test";
                        $r->borderColor = "green";

                        $a[] = $r;

                        $unit_id_prev = $unit_id;

                        $j++;
                    } // End if
                } // End Foreach Room

                $i++;
                $x->children = $a;
                $result[] = $x;
            } // End Foreach
        } // End isset
        //	echo "<pre>";
        //	print_r($result);
        header('Content-Type: application/json');
        echo json_encode($result);
    } // End Function hotel_backend_rooms_tree


    function hotel_backend_rooms_tree_2()
    {
        $hotel_id = $this->session->userdata('user_hotel');
        $rooms = $this->bookings_model->all_rooms_hotelwiseSql($hotel_id);
        $units = $this->bookings_model->all_units();
        $result = array();
        $i = 0;
        $j = 0;

        if (isset($units)) {
            foreach ($units as $unit) {
                $a = array();
                $x = new Room();
                $x->id = "unit";
                $x->name = "<div style=\"color: white; font-weight: 800; \">" . $unit->unit_name . "</div>";
                $x->unit_id = $unit->id;
                $x->expanded = true;
                $x->eventHeight = 20;
                $x->backColor = "#02CEFF";

                foreach ($rooms as $room) {

                    if ($room->unit_id == $unit->id) {

                        $unit_id = $room->unit_id;
                        $r = new Room();
                        $r->id = $room->room_id;
                        $r->eventHeight = 50;
                        $color = $room->color_primary;

                        date_default_timezone_set('Asia/Kolkata');
                        $d = date('Y-m-d');
                        $flg = 0;
                        $stat = '';
                        $ot = '';
                        $av = '';

                        if ($flg) {
                            $stat = 'Ota';
                            $ot = " active";
                        } else {
                            $stat = 'Avail';
                            $av = " active";
                        }

                        $s = " border-right: 7px solid #f9ba25;";
                        $style = "<div style=" . $s . "></div>";
                        $r->capacity = $room->room_bed;
                        $r->status = $room->room_status;

                        $ac = (explode(',', $room->room_features));
                        if ($ac[0] == '1') {
                            $AC = " active";
                        } else {
                            $AC = "";
                        }

                        $r->html = "<div style=\"font-size:14px; border-right: 7px solid " . $color . " ; padding:6px; cursor:pointer; margin-left: 40px;\"><div class='ot-av'><span class='ota'>Ota</span><span><small class='ac" . $AC . "'>AC</small><small class='ava" . $av . "'>AV</small><span></div><div style='color:" . $color . "' class='room-noo'>" . $room->room_no . "<span>" . $room->unit_type . "</span></div><div class='bed'><span>" . $room->default_occupancy . " </span><span>(" . $room->max_occupancy . ")</span></div></div>";

                        $r->css = "resource_action_menu";
                        $r->bubbleHtml = "Test";
                        $r->borderColor = "green";

                        $a[] = $r;

                        $unit_id_prev = $unit_id;

                        $j++;
                    } // End if
                } // End Foreach Room

                $i++;
                $x->children = $a;
                $result[] = $x;
            } // End Foreach
        } // End isset

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    function hotel_backend_rooms_snp()
    {

        $hotel_id = $this->session->userdata('user_hotel');

        $rooms = $this->bookings_model->all_rooms_hotelwise($hotel_id);
        $units = $this->bookings_model->all_units();
        $result = array();

        foreach ($rooms as $room) {

            $r = new Room();
            $r->id = $room->room_id;
            $s = " border-right: 7px solid #f9ba25;";
            $style = "<div style=" . $s . "></div>";
            $r->name = $room->room_no . "" . $style;
            $r->eventHeight = 50;
            $r->eventWidth = 20;
            //$r->capacity = $room->room_bed;
            //$r->status = $room->room_status;
            $result[] = $r;
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }


    function hotel_backend_onchange_room()
    {

        $hotel_id = $this->session->userdata('user_hotel');
        $capacity = $this->input->post('capacity');
        if ($capacity == 1 || $capacity == 2) {
            $rooms = $this->bookings_model->all_rooms_onchange_hotelwise($hotel_id, $capacity);
        } else {
            $rooms = $this->bookings_model->all_rooms_hotelwise($hotel_id);
        }
        $result = array();
        foreach ($rooms as $room) {
            $r = new Room();
            $r->id = $room->room_id;
            $r->name = $room->room_no;
            $r->capacity = $room->room_bed;
            $r->status = $room->room_status;
            $result[] = $r;
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    //unit name filter
    function hotel_backend_onchange_unit_tree()
    {

        $hotel_id = $this->session->userdata('user_hotel');
        $unit_selector = $this->input->post('unit_id');

        $hotel_id = $this->session->userdata('user_hotel');

        $rooms = $this->bookings_model->all_rooms_hotelwise($hotel_id);
        if ($unit_selector == 'all') {
            $units = $this->bookings_model->all_units();
        } else {
            $units = $this->bookings_model->all_units_limit($unit_selector);
        }
        $result = array();
        $i = 0;
        $j = 0;
        foreach ($units as $unit) {
            $a = array();
            $x = new Room();
            $x->id = "unit";
            $x->name = "<div style=\"color: white; font-weight: 800; \">" . $unit->unit_name . 'ug' . "</div>";
            $x->unit_id = $unit->id;
            $x->expanded = true;
            $x->eventHeight = 20;
            $x->backColor = "#A6A6A6";

            foreach ($rooms as $room) {

                if ($room->unit_id == $unit->id) {

                    $unit_id = $room->unit_id;
                    $unit = $this->bookings_model->get_unit_name($unit_id);
                    $unit_name = $unit->unit_name;
                    $unit_id = $unit->id;


                    $r = new Room();
                    $r->id = $room->room_id;
                    $r->eventHeight = 50;
                    $clean = $room->clean_id;
                    if ($clean == 1) {
                        $color = "#1BA39C";
                    } else if ($clean == 2) {
                        $color = "#F7CA18";
                    } else if ($clean == 3) {
                        $color = "#5E738B";
                    } else if ($clean == 4) {
                        $color = "#555555";
                    } else {
                        $color = "red";
                    }
                    $bgColor = $this->dashboard_model->get_color_by_houskeeping_status_id($clean);
                    if (isset($bgColor) && $bgColor) {
                        $color = $bgColor->color_primary;
                    } else {
                        $color = "#666";
                    }

                    date_default_timezone_set('Asia/Kolkata');
                    $d = date('Y-m-d');
                    $flg = $this->bookings_model->OTA_Avail($room->room_id, $d); //flg Active = 1 inactive = 0
                    $stat = '';
                    $ot = '';
                    $av = '';
                    if ($flg) {
                        $stat = 'Ota';
                        $ot = " active";
                    } else {
                        $stat = 'Avail';
                        $av = " active";
                    }
                    $s = " border-right: 7px solid #f9ba25;";
                    $style = "<div style=" . $s . "></div>";
                    //$r->name = $room->room_no . "<br/>" . $room->room_bed . " Bed" . $style;
                    $r->capacity = $room->room_bed;
                    $r->status = $room->room_status;
                    //$room->unit_id
                    $roomName = $this->unit_class_model->unit_type_fetch_id1($room->unit_id);
                    $unit_type_name = $roomName->name;
                    //$ac=$room->room_features;
                    $ac = (explode(',', $room->room_features));
                    if ($ac[0] == '1') {
                        $AC = " active";
                    } else {
                        $AC = "";
                    }
                    $r->html = "<div style=\"font-size:14px; border-right: 7px solid " . $color . " ; padding:6px; cursor:pointer; margin-left: 40px;\"><div class='ot-av'><span class='ota'>Ota</span><span><small class='ac" . $AC . "'>AC</small><small class='ava" . $av . "'>AV</small><span></div><div style='color:" . $color . "' class='room-noo'>" . $room->room_no . "<span>" . $unit_type_name . "</span></div><div class='bed'><span>" . $room->room_bed . " </span><span>(" . $room->max_occupancy . ")</span></div></div>";
                    $r->css = "resource_action_menu";
                    $r->borderColor = "green";

                    $a[] = $r;

                    $unit_id_prev = $unit_id;

                    $j++;
                }
            }
            $i++;
            $x->children = $a;
            $result[] = $x;
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }


    //unit type filter

    function hotel_backend_onchange_unittype_tree()
    {

        $hotel_id = $this->session->userdata('user_hotel');
        $unit_selector = $this->input->post('unit_type');
        //$id = $this->session->userdata('user_id');
        //$data = $this->dashboard_model->admin_status($id);
        //if(isset($data) && $data){
        $hotel_id = $this->session->userdata('user_hotel');
        /*}else{
        $hotel_id = '';
        }*/

        $rooms = $this->bookings_model->all_rooms_hotelwise($hotel_id);
        if ($unit_selector == 'all') {
            $units = $this->bookings_model->all_units();
        } else {
            $units = $this->bookings_model->all_unitstype_limit($unit_selector);
        }
        $result = array();
        $i = 0;
        $j = 0;
        foreach ($units as $unit) {
            $a = array();
            $x = new Room();
            $x->id = "unit";
            $x->name = "<div style=\"color: white; font-weight: 800; \">" . $unit->unit_name . "</div>";
            $x->unit_id = $unit->id;
            $x->expanded = true;
            $x->eventHeight = 20;
            $x->backColor = "#A6A6A6";


            foreach ($rooms as $room) {

                if ($room->unit_id == $unit->id) {

                    $unit_id = $room->unit_id;
                    $unit = $this->bookings_model->get_unit_name($unit_id);
                    $unit_name = $unit->unit_name;
                    $unit_id = $unit->id;


                    $r = new Room();
                    $r->id = $room->room_id;
                    $r->eventHeight = 50;
                    $clean = $room->clean_id;
                    if ($clean == 1) {
                        $color = "#1BA39C";
                    } else if ($clean == 2) {
                        $color = "#F7CA18";
                    } else if ($clean == 3) {
                        $color = "#5E738B";
                    } else if ($clean == 4) {
                        $color = "#555555";
                    } else {
                        $color = "red";
                    }

                    $bgColor = $this->dashboard_model->get_color_by_houskeeping_status_id($clean);
                    if (isset($bgColor) && $bgColor) {
                        $color = $bgColor->color_primary;
                    } else {
                        $color = "#666";
                    }
                    $s = " border-right: 7px solid #f9ba25;";
                    $style = "<div style=" . $s . "></div>";
                    //$r->name = $room->room_no . "<br/>" . $room->room_bed . " Bed" . $style;
                    $r->capacity = $room->room_bed;
                    $r->status = $room->room_status;
                    $r->html = "<div style=\"font-size:14px; border-right: 7px solid " . $color . " ; padding:6px; cursor:pointer; margin-left: 40px;\">" . $room->room_no . "<span style='display:block; font-size:13px;'>" . $room->room_bed . " Bed </span>   </div>";


                    $r->css = "resource_action_menu";
                    $r->borderColor = "green";
                    // $r->unit_name = $unit_name;
                    //$r->unit_id = $unit_id;



                    $a[] = $r;

                    $unit_id_prev = $unit_id;

                    $j++;
                }
            }
            $i++;
            $x->children = $a;
            $result[] = $x;
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }


    //unit class filter

    function hotel_backend_onchange_unitclass_tree()
    {

        $hotel_id = $this->session->userdata('user_hotel');
        $unit_selector = $this->input->post('unit_class');
        //$id = $this->session->userdata('user_id');
        //$data = $this->dashboard_model->admin_status($id);
        //if(isset($data) && $data){
        $hotel_id = $this->session->userdata('user_hotel');
        /*}else{
        $hotel_id = '';
        }*/

        $rooms = $this->bookings_model->all_rooms_hotelwise($hotel_id);
        if ($unit_selector == 'all') {
            $units = $this->bookings_model->all_units();
        } else {
            $units = $this->bookings_model->all_unitsclass_limit($unit_selector);
        }
        $result = array();
        $i = 0;
        $j = 0;
        foreach ($units as $unit) {
            $a = array();
            $x = new Room();
            $x->id = "unit";
            $x->name = "<div style=\"color: white; font-weight: 800; \">" . $unit->unit_name . "</div>";
            $x->unit_id = $unit->id;
            $x->expanded = true;
            $x->eventHeight = 20;
            $x->backColor = "#A6A6A6";

            foreach ($rooms as $room) {

                if ($room->unit_id == $unit->id) {

                    $unit_id = $room->unit_id;
                    $unit = $this->bookings_model->get_unit_name($unit_id);
                    $unit_name = $unit->unit_name;
                    $unit_id = $unit->id;


                    $r = new Room();
                    $r->id = $room->room_id;
                    $r->eventHeight = 50;
                    $clean = $room->clean_id;
                    if ($clean == 1) {
                        $color = "#1BA39C";
                    } else if ($clean == 2) {
                        $color = "#F7CA18";
                    } else if ($clean == 3) {
                        $color = "#5E738B";
                    } else if ($clean == 4) {
                        $color = "#555555";
                    } else {
                        $color = "red";
                    }

                    $bgColor = $this->dashboard_model->get_color_by_houskeeping_status_id($clean);
                    if (isset($bgColor) && $bgColor) {
                        $color = $bgColor->color_primary;
                    } else {
                        $color = "#666";
                    }
                    date_default_timezone_set('Asia/Kolkata');
                    $d = date('Y-m-d');
                    $flg = $this->bookings_model->OTA_Avail($room->room_id, $d); //flg Active = 1 inactive = 0
                    $stat = '';
                    $ot = '';
                    $av = '';
                    if ($flg) {
                        $stat = 'Ota';
                        $ot = " active";
                    } else {
                        $stat = 'Avail';
                        $av = " active";
                    }
                    $s = " border-right: 7px solid #f9ba25;";
                    $style = "<div style=" . $s . "></div>";
                    //$r->name = $room->room_no . "<br/>" . $room->room_bed . " Bed" . $style;
                    $r->capacity = $room->room_bed;
                    $r->status = $room->room_status;
                    //$room->unit_id
                    $roomName = $this->unit_class_model->unit_type_fetch_id1($room->unit_id);
                    $unit_type_name = $roomName->name;
                    //$ac=$room->room_features;
                    $ac = (explode(',', $room->room_features));
                    if ($ac[0] == '1') {
                        $AC = " active";
                    } else {
                        $AC = "";
                    }
                    $r->html = "<div style=\"font-size:14px; border-right: 7px solid " . $color . " ; padding:6px; cursor:pointer; margin-left: 40px;\"><div class='ot-av'><span class='ota'>Ota</span><span><small class='ac" . $AC . "'>AC</small><small class='ava" . $av . "'>AV</small><span></div><div style='color:" . $color . "' class='room-noo'>" . $room->room_no . "<span>" . $unit_type_name . "</span></div><div class='bed'><span>" . $room->room_bed . " </span><span>(" . $room->max_occupancy . ")</span></div></div>";
                    $r->css = "resource_action_menu";
                    $r->borderColor = "green";
                    // $r->unit_name = $unit_name;
                    //$r->unit_id = $unit_id;



                    $a[] = $r;

                    $unit_id_prev = $unit_id;

                    $j++;
                }
            }
            $i++;
            $x->children = $a;
            $result[] = $x;
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }


    //bed filter

    function hotel_backend_onchange_roombed_tree()
    {

        $hotel_id = $this->session->userdata('user_hotel');
        $no_bed = $this->input->post('no_bed');
        //$id = $this->session->userdata('user_id');
        //$data = $this->dashboard_model->admin_status($id);
        //if(isset($data) && $data){
        $hotel_id = $this->session->userdata('user_hotel');
        /*}else{
        $hotel_id = '';
        }*/
        if ($no_bed == 'all') {
            $rooms = $this->bookings_model->all_rooms_hotelwise($hotel_id);
        } else {
            $rooms = $this->bookings_model->all_rooms_hotelwise_limit($hotel_id, $no_bed);
        }

        $units = $this->bookings_model->all_units();


        $result = array();
        $i = 0;
        $j = 0;
        foreach ($units as $unit) {
            $a = array();
            $x = new Room();
            $x->id = "unit";
            $x->name = "<div style=\"color: white; font-weight: 800; \">" . $unit->unit_name . "</div>";
            $x->unit_id = $unit->id;
            $x->expanded = true;
            $x->eventHeight = 20;
            $x->backColor = "#A6A6A6";


            foreach ($rooms as $room) {

                if ($room->unit_id == $unit->id) {

                    $unit_id = $room->unit_id;
                    $unit = $this->bookings_model->get_unit_name($unit_id);
                    $unit_name = $unit->unit_name;
                    $unit_id = $unit->id;


                    $r = new Room();
                    $r->id = $room->room_id;
                    $r->eventHeight = 50;
                    $clean = $room->clean_id;
                    if ($clean == 1) {
                        $color = "#1BA39C";
                    } else if ($clean == 2) {
                        $color = "#F7CA18";
                    } else if ($clean == 3) {
                        $color = "#5E738B";
                    } else if ($clean == 4) {
                        $color = "#555555";
                    } else {
                        $color = "red";
                    }
                    $bgColor = $this->dashboard_model->get_color_by_houskeeping_status_id($clean);
                    if (isset($bgColor) && $bgColor) {
                        $color = $bgColor->color_primary;
                    } else {
                        $color = "#666";
                    }

                    date_default_timezone_set('Asia/Kolkata');
                    $d = date('Y-m-d');
                    $flg = $this->bookings_model->OTA_Avail($room->room_id, $d); //flg Active = 1 inactive = 0
                    $stat = '';
                    $ot = '';
                    $av = '';
                    if ($flg) {
                        $stat = 'Ota';
                        $ot = " active";
                    } else {
                        $stat = 'Avail';
                        $av = " active";
                    }
                    $s = " border-right: 7px solid #f9ba25;";
                    $style = "<div style=" . $s . "></div>";
                    //$r->name = $room->room_no . "<br/>" . $room->room_bed . " Bed" . $style;
                    $r->capacity = $room->room_bed;
                    $r->status = $room->room_status;
                    //$room->unit_id
                    $roomName = $this->unit_class_model->unit_type_fetch_id1($room->unit_id);
                    $unit_type_name = $roomName->name;
                    //$ac=$room->room_features;
                    $ac = (explode(',', $room->room_features));
                    if ($ac[0] == '1') {
                        $AC = " active";
                    } else {
                        $AC = "";
                    }
                    $r->html = "<div style=\"font-size:14px; border-right: 7px solid " . $color . " ; padding:6px; cursor:pointer; margin-left: 40px;\"><div class='ot-av'><span class='ota'>Ota</span><span><small class='ac" . $AC . "'>AC</small><small class='ava" . $av . "'>AV</small><span></div><div style='color:" . $color . "' class='room-noo'>" . $room->room_no . "<span>" . $unit_type_name . "</span></div><div class='bed'><span>" . $room->room_bed . " </span><span>(" . $room->max_occupancy . ")</span></div></div>";


                    $r->css = "resource_action_menu";
                    $r->borderColor = "green";

                    $a[] = $r;

                    $unit_id_prev = $unit_id;

                    $j++;
                }
            }
            $i++;
            $x->children = $a;
            $result[] = $x;
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    //room no filter

    function hotel_backend_onchange_roomno_tree()
    {

        $hotel_id = $this->session->userdata('user_hotel');
        //$room_no = '20';
        $room_no = $this->input->post('room_no');
        $hotel_id = $this->session->userdata('user_hotel');
        $rooms = $this->bookings_model->all_rooms_hotelwise($hotel_id);

        $units = $this->bookings_model->all_units();

        $result = array();
        $i = 0;
        $j = 0;
        foreach ($units as $unit) {
            $a = array();
            $x = new Room();
            $x->id = "unit";
            $x->name = "<div style=\"color: white; font-weight: 800; \">" . $unit->unit_name . "</div>";
            $x->unit_id = $unit->id;
            $x->expanded = true;
            $x->eventHeight = 20;
            $x->backColor = "#A6A6A6";
            // $x->html= "<div style=\"color: orange; font-weight: 800; \">".$unit->unit_name."</div>";


            //$result[][]=$x;


            foreach ($rooms as $room) {
                $lenRC = strlen($room_no);

                //echo substr($room->room_no, 0, $lenRC).' == '.$room_no.' '.$lenRC.'</br>'; //_sb

                if ($room->unit_id == $unit->id && strtoupper(substr($room->room_no, 0, $lenRC)) == strtoupper($room_no)) {


                    $unit_id = $room->unit_id;
                    $unit = $this->bookings_model->get_unit_name($unit_id);
                    $unit_name = $unit->unit_name;
                    $unit_id = $unit->id;


                    $r = new Room();
                    $r->id = $room->room_id;
                    $r->eventHeight = 50;
                    $clean = $room->clean_id;
                    if ($clean == 1) {
                        $color = "#1BA39C";
                    } else if ($clean == 2) {
                        $color = "#F7CA18";
                    } else if ($clean == 3) {
                        $color = "#5E738B";
                    } else if ($clean == 4) {
                        $color = "#555555";
                    } else {
                        $color = "red";
                    }

                    $bgColor = $this->dashboard_model->get_color_by_houskeeping_status_id($clean);
                    if (isset($bgColor) && $bgColor) {
                        $color = $bgColor->color_primary;
                    } else {
                        $color = "#666";
                    }

                    $d = date('Y-m-d');
                    $flg = 0; //$this->bookings_model->OTA_Avail($room->room_id,$d);//flg Active = 1 inactive = 0
                    $stat = '';
                    $ot = '';
                    $av = '';
                    if ($flg) {
                        $stat = 'Ota';
                        $ot = " active";
                    } else {
                        $stat = 'Avail';
                        $av = " active";
                    }

                    $s = " border-right: 7px solid #f9ba25;";
                    $style = "<div style=" . $s . "></div>";
                    //$r->name = $room->room_no . "<br/>" . $room->room_bed . " Bed" . $style;
                    $r->capacity = $room->room_bed;
                    $r->status = $room->room_status;
                    //$room->unit_id
                    $roomName = $this->unit_class_model->unit_type_fetch_id1($room->unit_id);
                    $unit_type_name = $roomName->name;
                    //$ac=$room->room_features;
                    $ac = (explode(',', $room->room_features));
                    if ($ac[0] == '1') {
                        $AC = " active";
                    } else {
                        $AC = "";
                    }
                    $unit_info = $this->unit_class_model->edit_unit_type($unit->id);
                    $r->html = "<div style=\"font-size:14px; border-right: 7px solid " . $color . " ; padding:6px; cursor:pointer; margin-left: 40px;\"><div class='ot-av'><span class='ota'>Ota</span><span><small class='ac" . $AC . "'>AC</small><small class='ava" . $av . "'>AV</small><span></div><div style='color:" . $color . "' class='room-noo'>" . $room->room_no . "<span>" . $unit_type_name . "</span></div><div class='bed'><span>" . $unit_info->default_occupancy . " </span><span>(" . $unit_info->max_occupancy . ")</span></div></div>";



                    $a[] = $r;

                    $unit_id_prev = $unit_id;

                    $j++;
                }
            } // End Inner Foreach
            $i++;
            $x->children = $a;
            $result[] = $x;
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }



    //price filter



    function hotel_backend_onchange_roomprice_tree()
    {
        /*$id = $this->session->userdata('user_id');
        $data = $this->dashboard_model->admin_status($id);
            if(isset($data) && $data){
                $hotel_id = $this->session->userdata('user_hotel');
                }else{
                $hotel_id = '';
                }*/
        $hotel_id = $this->session->userdata('user_hotel');
        $range = $this->input->post('range');
        $range_array = explode(";", $range);

        $hotel_id = $this->session->userdata('user_hotel');

        // if($no_bed=='all') {
        $rooms = $this->bookings_model->all_rooms_hotelwise_price($hotel_id, $range_array[0], $range_array[1]);
        /* }else {
            $rooms = $this->bookings_model->all_rooms_hotelwise_limit($hotel_id,$no_bed);
        }*/

        $units = $this->bookings_model->all_units();


        $result = array();
        $i = 0;
        $j = 0;
        foreach ($units as $unit) {
            $a = array();
            $x = new Room();
            $x->id = "unit";
            $x->name = "<div style=\"color: white; font-weight: 800; \">" . $unit->unit_name . "</div>";
            $x->unit_id = $unit->id;
            $x->expanded = true;
            $x->eventHeight = 20;
            $x->backColor = "#A6A6A6";
            // $x->html= "<div style=\"color: orange; font-weight: 800; \">".$unit->unit_name."</div>";

            //$result[][]=$x;


            foreach ($rooms as $room) {

                if ($room->unit_id == $unit->id) {

                    $unit_id = $room->unit_id;
                    $unit = $this->bookings_model->get_unit_name($unit_id);
                    $unit_name = $unit->unit_name;
                    $unit_id = $unit->id;


                    $r = new Room();
                    $r->id = $room->room_id;
                    $r->eventHeight = 50;
                    $clean = $room->clean_id;
                    if ($clean == 1) {
                        $color = "#1BA39C";
                    } else if ($clean == 2) {
                        $color = "#F7CA18";
                    } else if ($clean == 3) {
                        $color = "#5E738B";
                    } else if ($clean == 4) {
                        $color = "#555555";
                    } else {
                        $color = "red";
                    }

                    $bgColor = $this->dashboard_model->get_color_by_houskeeping_status_id($clean);
                    if (isset($bgColor) && $bgColor) {
                        $color = $bgColor->color_primary;
                    } else {
                        $color = "#666";
                    }

                    date_default_timezone_set('Asia/Kolkata');
                    $d = date('Y-m-d');
                    $flg = $this->bookings_model->OTA_Avail($room->room_id, $d); //flg Active = 1 inactive = 0
                    $stat = '';
                    $ot = '';
                    $av = '';
                    if ($flg) {
                        $stat = 'Ota';
                        $ot = " active";
                    } else {
                        $stat = 'Avail';
                        $av = " active";
                    }
                    $s = " border-right: 7px solid #f9ba25;";
                    $style = "<div style=" . $s . "></div>";
                    //$r->name = $room->room_no . "<br/>" . $room->room_bed . " Bed" . $style;
                    $r->capacity = $room->room_bed;
                    $r->status = $room->room_status;
                    //$room->unit_id
                    $roomName = $this->unit_class_model->unit_type_fetch_id1($room->unit_id);
                    $unit_type_name = $roomName->name;
                    //$ac=$room->room_features;
                    $ac = (explode(',', $room->room_features));
                    if ($ac[0] == '1') {
                        $AC = " active";
                    } else {
                        $AC = "";
                    }
                    $r->html = "<div style=\"font-size:14px; border-right: 7px solid " . $color . " ; padding:6px; cursor:pointer; margin-left: 40px;\"><div class='ot-av'><span class='ota'>Ota</span><span><small class='ac" . $AC . "'>AC</small><small class='ava" . $av . "'>AV</small><span></div><div style='color:" . $color . "' class='room-noo'>" . $room->room_no . "<span>" . $unit_type_name . "</span></div><div class='bed'><span>" . $room->room_bed . " </span><span>(" . $room->max_occupancy . ")</span></div></div>";
                    $r->css = "resource_action_menu";
                    $r->borderColor = "green";
                    // $r->unit_name = $unit_name;
                    //$r->unit_id = $unit_id;
                    $a[] = $r;
                    $unit_id_prev = $unit_id;
                    $j++;
                }
            }
            $i++;
            $x->children = $a;
            $result[] = $x;
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }









    function hotel_new_booking()
    {
        $found = in_array("25", $this->arraay);
        if ($found) {
            $hotel_id = $this->session->userdata('user_hotel');
            $data['resource'] = $_GET['resource'];
            $data['rooms'] = $this->bookings_model->room_hotelwise($hotel_id, $_GET['resource']);
            $data['events'] = $this->dashboard_model->all_events_limit();
            $data['taxes'] = $this->bookings_model->room_tax($hotel_id);
            $data['times'] = $this->dashboard_model->checkin_time();
            $data['tax'] = $this->bookings_model->service_tax($hotel_id);
            $data['start'] = $_GET['start'];
            $data['end'] = $_GET['end'];

            if ($data['rooms']) {
                $this->load->view("backend/dashboard/pop_new_booking", $data);
            }
        } else {
            $this->load->view("backend/dashboard/pop_new_booking_per");
        }
    }


    function hotel_new_booking_ver2()
    {
        $found = in_array("25", $this->arraay);
        if ($found) {


            $hotel_id = $this->session->userdata('user_hotel');
            $data['resource'] = $_GET['resource'];
            $data['rooms'] = $this->bookings_model->room_hotelwise($hotel_id, $_GET['resource']);
            $data['events'] = $this->dashboard_model->all_events_limit();
            $data['taxes'] = $this->bookings_model->room_tax($hotel_id);
            $data['times'] = $this->dashboard_model->checkin_time();
            $data['tax'] = $this->bookings_model->service_tax($hotel_id);
            $data['start'] = $_GET['start'];
            $data['end'] = $_GET['end'];

            if ($data['rooms']) {
                $this->load->view("backend/dashboard/pop_new_booking_ver2", $data);
            }
        } else {
            $this->load->view("backend/dashboard/pop_new_booking_per");
        }
    }


    //new booking ver 2

    //end new booking ver 2
    function clean_check()
    {
        $resource = $_GET["resource"];
        $clean = $this->bookings_model->get_clean_status($resource);
        $clean_id = $clean->clean_id;
        if ($clean_id != 1) {
            $data = array(
                'say' => 'no',
            );
        } else {
            $data = array(
                'say' => 'yes',
            );
        }

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function getBkDet()
    {

        $hotel_id = $this->session->userdata('user_hotel');
        $booking_id = $_GET['id'];
        $bkCnt = 0;

        $bkdata = $this->dashboard_model->getBookingById($booking_id);

        if (isset($bkdata->room_id)) {
            $bkCntM = $this->dashboard_model->getAllBookingCntByDate($bkdata->room_id);
            $bkCnt = $bkCntM->cnt;
            //echo '<bkCnt> '.$bkCnt.' ';
        }

        /*echo '</br> '.' '. $booking_id.'</br>';
		echo 'isArr '.(is_object($bkdata) ? 'Array' : 'not an Array');
		echo '<pre> ';
		echo '<bkCnt> '.$bkCnt.' ';
		print_r($bkdata);*/
        //exit;.

        if (isset($bkdata) && is_object($bkdata) && $bkCnt == 0) {
            $data = array(
                'canTake' => 'yes',
                'roomab' => $bkdata->room_id,
                'startab' => $bkdata->cust_from_date_actual,
                'endab' => $bkdata->cust_end_date_actual,
            );
        } else {
            $data = array(
                'canTake' => 'no',
            );
        }

        /*print_r($data);*/

        header('Content-Type: application/json');
        echo json_encode($data);
    } // End function getBkDet

    function hotel_new_booking2()
    {


        $hotel_id = $this->session->userdata('user_hotel');


        $this->load->view("backend/dashboard/pop_new_booking");
    }

    function return_broker()
    {


        $hotel_id = $this->session->userdata('user_hotel');
        $broker = $this->bookings_model->broker_hotelwise($hotel_id);
        header('Content-Type: application/json');
        echo json_encode($broker);
    }
    function return_channel()
    {


        $hotel_id = $this->session->userdata('user_hotel');
        $broker = $this->bookings_model->channel_hotelwise($hotel_id);
        header('Content-Type: application/json');
        echo json_encode($broker);
    }

    function  hotel_backend_create_basic()
    {

        $times = $this->dashboard_model->checkin_time();
        foreach ($times as $time) {
            if ($time->hotel_check_in_time_fr == 'PM' && $time->hotel_check_in_time_hr != "12") {
                $modifier = ($time->hotel_check_in_time_hr + 12) + $time->hotel_check_in_time_mm / 60;
            } else {
                $modifier = ($time->hotel_check_in_time_hr) + $time->hotel_check_in_time_mm / 60;
            }
        }

        $from_date = date('Y-m-d', strtotime($this->input->post('start_dt')));
        $end_date = date('Y-m-d', strtotime($this->input->post('end_dt')));

        $hotel_id = $this->session->userdata('user_hotel');
        $id = $this->session->userdata('user_id');
        $from_date_final = date('Y-m-d H:i:s', strtotime($from_date));
        $end_date_final = date('Y-m-d H:i:s', strtotime($end_date) + 60 * 60 * 24);

        $new_book = array(
            'hotel_id' => $hotel_id,
            'user_id' => $id,
            'room_id' => $this->input->post('room_id'),
            'cust_name' => $this->input->post('cust_name'),
            'cust_from_date' => $from_date_final, //$cust_from_date,
            'cust_end_date' => $end_date_final, //$cust_end_date,
            'booking_status_id' => 8,
        );

        $bookings_id = $this->bookings_model->add_new_room($new_book);
        $data = array(
            'bookings_id' => $bookings_id
        );
    } // end function hotel_backend_create_basic

    function hotel_backend_create()
    {

        //checkin time logic
        date_default_timezone_set("Asia/Kolkata");

        $times = $this->dashboard_model->checkin_time();

        foreach ($times as $time) {
            if ($time->hotel_check_in_time_fr == 'PM' && $time->hotel_check_in_time_hr != "12") {
                $modifier = ($time->hotel_check_in_time_hr + 12) + $time->hotel_check_in_time_mm / 60;
            } else {
                $modifier = ($time->hotel_check_in_time_hr) + $time->hotel_check_in_time_mm / 60;
            }
        }

        $times2 = $this->dashboard_model->checkout_time();

        foreach ($times2 as $time2) {
            if ($time2->hotel_check_out_time_fr == 'PM' && $time2->hotel_check_out_time_hr != "12") {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12) + $time->hotel_check_out_time_mm / 60;
            } else {
                $modifier2 = ($time2->hotel_check_out_time_hr) + $time->hotel_check_out_time_mm / 60;
            }
        }

        $id2 = $this->input->post('id_guest');
        $id = $this->session->userdata('user_id');
        $hotel_id = $this->session->userdata('user_hotel');
        $start_time = $this->input->post('start_time'); //'14:15:19';
        $end_time = $this->input->post('end_time');
        $start_hour = round($start_time);
        $end_hour = round($end_time);
        $final_start_hour = $start_hour - $modifier;
        $final_end_hour = $end_hour - $modifier;
        $final_start_time = $final_start_hour . ":00:00";
        $final_end_time = $final_end_hour . ":00:00";
        $checkin_time = date("H:i:s", strtotime($start_time));
        $checkout_time = date("H:i:s", strtotime($end_time));

        $start_dt1 = $this->input->post('start_dt1'); //'2017-05-26';
        if ($start_dt1 == 'no')
            $start_dt = $this->input->post('start_dt'); //'2017-05-26';
        else
            $start_dt = $this->input->post('start_dt1'); //'2017-05-26';

        $end_dt = $this->input->post('end_dt'); //'2017-05-27';
        $cust_from_date = date('Y-m-d', strtotime($start_dt)) . "T" . date("H:i:s", strtotime($final_start_time));
        $cust_end_date = date('Y-m-d', strtotime($end_dt)) . "T" . date("H:i:s", strtotime($final_end_time));
        $from_date = date('Y-m-d', strtotime($start_dt)) . "T" . date("H:i:s", strtotime($start_time));
        $from_date_final = date('Y-m-d H:i:s', strtotime($from_date) - 60 * 60 * $modifier);
        $end_date = date('Y-m-d', strtotime($end_dt)) . "T" . date("H:i:s", strtotime($end_time));
        $end_date_final = date('Y-m-d H:i:s', strtotime($end_date) - 60 * 60 * $modifier);
        $booking_status_id = 8;

        if ($this->input->post('cust_full_address') != "") {
            $address_array = explode(",", $this->input->post('cust_full_address'));
            $cust_full_address = $this->input->post('cust_full_address');
            $g_city = $address_array[0];
            $g_state = $address_array[1];
            $g_country = $address_array[2];
        } else {
            $cust_full_address = " ";
            $g_city = " ";
            $g_state = " ";
            $g_country = " ";
        }
        if ($this->input->post('g_state') != '') {

            $state = strtoupper($this->input->post('g_state'));
        } else {

            $state = strtoupper($this->input->post('g_state1'));
        }



        $guest = array(
            'hotel_id' => $hotel_id,
            'g_name' => $this->input->post('cust_name'),
            'g_pincode' => $this->input->post('cust_address'),
            'g_address' => $cust_full_address,
            'g_city' => $g_city,
            'g_state' => $g_state,
            'g_country' => $g_country,
            'g_contact_no' => $this->input->post('cust_contact_no'),
            'g_email' => $this->input->post('cust_mail'),
            'g_country' => $this->input->post('g_country'),
            'g_state' => $state,

        );

        if ($id2 == "") {
            $guest_id = $this->bookings_model->add_guest($guest);
        } else {
            $guest_id = $id2;
        }
        $maxID = "select max(booking_id) as MID from hotel_bookings";
        $query = $this->db1->query($maxID);
        $a = $query->row();
        $b = $a->MID;
        $b = $b + 1;
        // ...

        $new_book = array(
            'booking_id' => $b,
            'hotel_id' => $hotel_id,
            'user_id' => $id,
            'room_id' => $this->input->post('room_id'),
            'guest_id' => $guest_id,
            'cust_name' => $this->input->post('cust_name'),
            'cust_address' => $this->input->post('cust_address'),
            'cust_contact_no' => $this->input->post('cust_contact_no'),
            'cust_from_date' => $from_date_final,
            'cust_end_date' => $end_date_final,
            'cust_from_date_actual' => date("Y-m-d", strtotime($this->input->post('start_dt'))),
            'cust_end_date_actual' =>  date("Y-m-d", strtotime($this->input->post('end_dt'))),
            'checkin_time' => $checkin_time,
            'confirmed_checkin_time' => $checkin_time,
            'checkout_time' => $checkout_time,
            'confirmed_checkout_time' => $checkout_time,
            'nature_visit' => $this->input->post('nature_visit'),
            'next_destination' => $this->input->post('next_destination'),
            'cust_coming_from' => $this->input->post('coming_from'),
            'booking_status_id' => $booking_status_id,
            'p_center' => $this->input->post('p_center'),
            'marketing_personnel' => $this->input->post('marketing_personnel'),
        );

        $checkin_date = date("Y:m:d");
        $checkin_time = date("H:i:s");

        $new_book1 = array(
            'booking_id' => $b,
            'hotel_id' => $hotel_id,
            'user_id' => $id,
            'cust_from_date' => $from_date_final, //$cust_from_date,
            'checkin_time' => $checkin_time,
            'checkin_date' => $checkin_date,
            'cust_end_date' => $end_date_final,
            'cust_from_date_actual' => date("Y-m-d", strtotime($this->input->post('start_dt'))),
            'cust_end_date_actual' => date("Y-m-d", strtotime($this->input->post('end_dt'))),
            'checkout_time' => $checkout_time,
            'booking_status_id' => $booking_status_id,
        );
        echo json_encode($new_book);
        echo json_encode($new_book1);
        die;
        $bookings_id = $this->bookings_model->add_new_room($new_book, $new_book1);

        // creating array for hotel_stay_guest table..

        $doc = $this->dashboard_model->guest_verified($guest_id);

        $stay_guest = array(
            'booking_id' => $bookings_id,
            'group_id' => 0,
            'g_id' => $guest_id,
            'addEvent' => 'pending',
            'docVerif' => $doc,
            'charge_applicable' => '0',
            'chargeAmt' => 0.00,
            'check_in_date' => date("Y-m-d", strtotime($this->input->post('start_dt'))),
            'check_out_date' => date("Y-m-d", strtotime($this->input->post('end_dt'))),
            'user_id' => $this->session->userdata("user_id"),
            'hotel_id' => $hotel_id
        );

        // end of array creation..

        $this->dashboard_model->into_stay_guest($stay_guest, 'sb');


        $data = array(
            'bookings_id' => $bookings_id,
            'guest_name' =>  $this->input->post('cust_name'),
            'guest_id' => $guest_id,
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    } // End function hotel_backend_create


    function hotel_backend_create2()
    {

        date_default_timezone_set('Asia/Kolkata');
        $perc = $this->input->post('perc1');
        $mod = $this->input->post('modf_rr');

        $perc2 = $this->input->post('m_perc1');
        $m_mod = $this->input->post('modf_mp');


        $bookings_id = $this->input->post('booking_1st');
        $hotel_id = $this->session->userdata('user_hotel');
        $no_of_guest = $this->input->post('adult') + $this->input->post('child');

        $chk_last_bk = $this->dashboard_model->chk_last_bk();
        $chk_last_bk = $chk_last_bk + 1;
        $source = $this->input->post('booking_source');
        $source_info = $this->bookings_model->get_source_name($source);


        $new_book = array(
            'booking_id' => $bookings_id,
            'booking_id_actual' => "BK0" . $this->session->userdata('user_hotel') . "-" . $this->session->userdata('user_id') . "/" . date("d") . date("m") . date("y") . "/" . $chk_last_bk,
            'no_of_guest' => $no_of_guest,
            'no_of_adult' => $this->input->post('adult'),
            'no_of_child' => $this->input->post('child'),
            'booking_source' => $source,
            'booking_source_name' => $source_info->booking_source_name,
            'base_room_rent' => $this->input->post('base_room_rent'),
            'unit_room_rent' => $this->input->post('unit_room_rent'),
            'rent_mode_type' => $this->input->post('rent_mode_type'),
            'room_rent_total_amount' => $this->input->post('base_room_rent') * $this->input->post('stay_span'),
            'stay_days' => $this->input->post('stay_span'),
            'mod_room_rent' => $mod,
            'comment' => $this->input->post('comment'),
            'booking_taking_time' => date("H:i"),
            'booking_taking_date' => date("Y-m-d"),
            'food_plans' => $this->input->post('plan_id'),
            'food_plan_price' => $this->input->post('plan_tprice'),
            'food_plan_mod_type' => $this->input->post('m_rent_mode_type'),
            'food_plan_mod_rent' => $m_mod,
            'food_plan_uprice' => $this->input->post('plan_price1'),
            'ex_per_uprice' => $this->input->post('ex_u_price'),
            'ex_per_u_mprice' => $this->input->post('ex_u_mprice'),
            'charge_name' => $this->input->post('booking_rent'),

        );


        $query = $this->bookings_model->add_new_room2($new_book);

        $source = $this->bookings_model->get_source_type_name($this->input->post('booking_source'));
        $data = array(
            'bookings_id' => $bookings_id,
            'booking_source' => $this->input->post('booking_source'),
            'booking_source_name' => $source->bst_name,
        );



        header('Content-Type: application/json');
        echo json_encode($data);
    }


    function hotel_backend_create4()
    {
        date_default_timezone_set('Asia/Kolkata');
        // $tax_flag = $this->input->post('tax_track');
        $bookings_id = $this->input->post('booking_3rd');
        $rmNo = $this->input->post('booking_rm_no');
        $hotel_id = $this->session->userdata('user_hotel');
        $user_id = $this->session->userdata('user_id');
        $datetime = date("Y/m/d") . " " . date("H:i:s");
        $select_bookings = $this->bookings_model->get_booking_details($bookings_id);
        foreach ($select_bookings as $row) {
            $booking_status_id_pre = $row->booking_status_id;
            $cust_name = $row->cust_name;
            $cust_contact_no = $row->cust_contact_no;
            $cust_destination = $row->next_destination;
            $cust_from = $row->cust_from_date;
            $cust_end = $row->cust_end_date;
            $cust_total = $row->room_rent_sum_total;
        }

        if ($this->input->post('t_amount') != '') {
            if ($booking_status_id_pre == 2 || $booking_status_id_pre == 8) {
                $booking_status_id = 4;
            } else {
                $booking_status_id = 5;
            }
        } else {
            $booking_status_id = $booking_status_id_pre;
        }


        $new_book1 = array(
            'booking_id' => $bookings_id,
            'booking_status_id' => $booking_status_id,
            //'room_rent_tax_details'=>$tflg
        );
        $query = $this->bookings_model->add_new_room2($new_book1);
        $chk_lasltransaction = $this->dashboard_model->chk_lasltransaction();
        $chk_lasltransaction = $chk_lasltransaction + 1;
        $transaction_id = 'TA0' . $this->session->userdata('user_hotel') . "-" . $this->session->userdata('user_id') . "/" . date("d") . date("m") . date("y") . "/" . $chk_lasltransaction;
        $new_book = array(
            'actual_id' => $transaction_id,
            't_booking_id' => $bookings_id,
            't_date' => $datetime,
            't_payment_mode' => $this->input->post('t_payment_mode'),
            'transaction_type' => 'Booking Payment',
            'p_center' => $this->input->post('p_center'),
            't_bank_name' => $this->input->post('t_bank_name'),
            't_amount' => $this->input->post('t_amount'),
            'transaction_from_id' => 2,
            'transaction_to_id' => 4,
            'transaction_releted_t_id' => 1,
            'user_id' => $user_id,
            'transactions_detail_notes' => "( Booking Id: HM0" . $hotel_id . "00" . $bookings_id . " Room No : " . $rmNo . " " . $name . " From - " . $cust_from . " To - " . $cust_end . " )"

        );
        $query = $this->bookings_model->add_new_room4($new_book);

        //update cashdrawer 
        $amount = $this->input->post('t_amount');
        $cash = $this->unit_class_model->get_closing_stock($this->session->userdata('cd_key'));
        $c = $cash->clossing_cash; //get clossing cash
        $cash_collection = $cash->cash_collection; //get collect cash
        $mode = $this->input->post('t_payment_mode');
        if ($mode == 'cash' || $mode == 'Cash') {
            $query1 = $this->unit_class_model->update_cash_collection($amount, $c, $cash_collection);
        } else {
            $cash = $this->unit_class_model->get_mode_cash($mode);
            $d = $cash->$mode; //get mode cash	
            //$c+=$amount;
            $d += $amount;
            $query1 = $this->unit_class_model->update_cash_drawer_mode($d, $mode);
        }





        $new_book2 = array(
            'booking_id' => $bookings_id,
            //'room_rent_tax_details' => $this->input->post('booking_tax_types'),
            'room_rent_sum_total' => $this->input->post('total'),
            'room_rent_tax_amount' => $this->input->post('tax'),
            'service_tax' => $this->input->post('servicet'),
            'service_charge' => $this->input->post('servicec'),
            'luxury_tax' => $this->input->post('luxuryt'),
            // 'room_rent_tax_details'=>$tflg
        );
        $query = $this->bookings_model->add_new_room5($new_book2);

        $data = array(
            'bookings_id' => $bookings_id,
        );





        header('Content-Type: application/json');
        echo json_encode($data);
    }

    //For tax Calculation
    function hotel_backend_create5()
    {

        $bookings_id = $this->input->post('bookings_id');
        $tax_flag = $this->input->post('tax_track');
        $set_agent_ota = $this->input->post('set_agent_ota');
        $broker_id = $this->input->post('broker_id');
        $broker_commission = $this->input->post('broker_commission');
        $tcomV = $this->input->post('tcomV');
        $applyedAmount = $this->input->post('applyedAmount');
        $commission_applicable = $this->input->post('commission_applicable');
        $status = $this->input->post('status');

        $checkin_date = '';
        $checkin_time = '';
        if ($status == 'current') {
            $booking_status_id = 5;
            $checkin_date = date("Y:m:d");
            $checkin_time = date("H:i:s");
            $new_book = array(
                'booking_id' => $bookings_id,
                'checkin_date' => $checkin_date,
                'checkin_time' => $checkin_time
            );
            $query = $this->bookings_model->add_new_room2($new_book);
        } else if ($status == 'temporaly') {
            $booking_status_id = 1;
        } else {
            $booking_status_id = 2; //Advance Booking
        }

        $update_bk = array(
            'booking_id' => $bookings_id,
            'booking_status_id' => $booking_status_id,
        );
        $this->bookings_model->add_roomupdate($update_bk);

        if ($tax_flag == '1') {
            $tflg = 'Tax';
        } else {
            $tflg = '';
        }

        $new_book2 = array(
            'room_rent_sum_total' => $this->input->post('total'),
            'ex_ad_no' => $this->input->post('extra_ad_no'),
            'ex_ch_no' => $this->input->post('extra_ch_no'),
            'room_rent_tax_details' => $tflg,
            'booking_status_id' => $booking_status_id,
        );

        $query = $this->bookings_model->add_new_room5($new_book2, $bookings_id);
        $select_bookings2 = $this->dashboard_model->get_booking_details3($bookings_id);
        $statID = 0;
        $statID = $select_bookings2->booking_status_id;
        $booking_status_id_pre = $select_bookings2->booking_status_id;
        $cust_name = $select_bookings2->cust_name;
        $cust_contact_no = $select_bookings2->cust_contact_no;
        $cust_destination = $select_bookings2->next_destination;
        $cust_from = $select_bookings2->cust_from_date;
        $cust_end = $select_bookings2->cust_end_date;
        $cust_total = $select_bookings2->rm_total + $select_bookings2->rr_tot_tax + $select_bookings2->mp_tot + $select_bookings2->mp_tax;

        $this->load->model('dashboard_model');
        $hotel_name = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $hot_name = $hotel_name->hotel_name;
        $text = $cust_name . " from " . $cust_destination . " booked " . $hot_name . " from:" . $cust_from . " to: " . $cust_end . " , booking amount:" . $cust_total;
        $text_guest = "Booking in " . $hot_name . " successfull!! Name: " . $cust_name . " From: " . $cust_destination . " Booked from : " . $cust_from . " to: " . $cust_end . " ,Booking Amount: " . $cust_total;
        $this->load->model('bookings_model');
        $this->bookings_model->send_sms($text);
        $this->bookings_model->send_sms_guest($cust_contact_no, $text_guest);

        // populate table hoj_travelagentcommission
        if ($set_agent_ota == '5' || $set_agent_ota == '4') {
            if ($set_agent_ota == '5') {
                $typ = 'agent';
            } else if ($set_agent_ota == '4') {
                $typ = 'ota';
            }

            if ($commission_applicable == '1') {
                $capp = 'rr';
            } else if ($commission_applicable == '2') {
                $capp = 'rrf';
            } else if ($commission_applicable == '3') {
                $capp = 'tb';
            }

            $arr11 = array(
                'taType' => $typ,
                'taID' => $broker_id,
                'masterID' => 'N/A',
                'bookingID' => $bookings_id,
                'bkStatus' => $statID,
                'amount' => $tcomV,
                'tax' => 0,
                'applyedAmount' => $applyedAmount,
                'commAppOn' => $capp,
                'isPaid' => 'N/A',
                'note' => 'N/A',
                'paymentID' => 'N/A',
                'addedDate' => date('Y-m-d H:i:s'),
                'hotel_id' => $this->session->userdata('user_hotel'),
                'user_id' => $this->session->userdata('user_id')
            );

            $query11 = $this->db1->insert('hoj_travelagentcommission', $arr11);
        }

        $data = array(
            'bookings_id' => $bookings_id
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    } //End function hotel_backend_create5

    function hotel_backend_create_broker()
    {
        $bookings_id = $_GET['booking_id_broker'];
        $b_detail = $this->dashboard_model->particular_booking($bookings_id);
        $broker_det = $this->bookings_model->get_broker_details($this->input->post('broker_id'));
        if (isset($b_detail) && $b_detail) {
            foreach ($b_detail as $b) {
                $s_name = $b->booking_source_name;
            }
        }
        if (isset($broker_det) && $broker_det) {
            foreach ($broker_det as $d) {
                $b_name = $d->b_name;
            }
        }




        $new_book3 = array(
            'booking_id' => $bookings_id,
            'broker_id' => $this->input->post('broker_id'),
            'broker_commission' => $this->input->post('broker_commission'),
            'booking_source_name' => $s_name . '-' . $b_name,


        );




        $query = $this->bookings_model->add_new_room_broker($new_book3);


        $new_book8 = array(

            'broker_id' => $this->input->post('broker_id'),
            'broker_commission' => $this->input->post('broker_commission'),


        );

        $query = $this->dashboard_model->update_broker_commission($new_book8['broker_id'], $new_book8['broker_commission']);

        $data = array(
            'bookings_id' => $bookings_id
        );



        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function hotel_backend_create_channel()
    {
        $bookings_id = $_GET['booking_id_broker'];

        $b_detail = $this->dashboard_model->particular_booking($bookings_id);
        $broker_det = $this->bookings_model->get_channel_details($this->input->post('broker_id'));
        if (isset($b_detail) && $b_detail) {
            foreach ($b_detail as $b) {
                $s_name = $b->booking_source_name;
            }
        }
        if (isset($broker_det) && $broker_det) {
            foreach ($broker_det as $d) {
                $b_name = $d->channel_name;
            }
        }

        $new_book3 = array(
            'booking_id' => $bookings_id,
            'channel_id' => $this->input->post('broker_id'),
            'channel_commission' => $this->input->post('broker_commission'),
            'booking_source_name' => $s_name . '-' . $b_name,


        );
        $query = $this->bookings_model->add_new_room_channel($new_book3);

        $new_book8 = array(

            'channel_id' => $this->input->post('broker_id'),
            'channel_commission' => $this->input->post('broker_commission'),


        );

        $query = $this->dashboard_model->update_channel_commission2($new_book8['channel_id'], $new_book8['channel_commission']);

        $data = array(
            'bookings_id' => $bookings_id
        );



        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function hotel_backend_events2()
    {

        $hotel_id = $this->session->userdata('user_hotel');
        $start = '2017-05-06T00:00:00';
        $end = '2017-06-06T00:00:00';
        echo 'Start<br/>';
        $result = $this->bookings_model->all_rooms_bookings_event($hotel_id, $start, $end);
        echo json_encode($result);
        //print_r($result);
        echo 'Done';
    } // _sb


    // Used to load Day Pilot
    function hotel_backend_events()
    {

        $times = $this->dashboard_model->checkin_time();
        foreach ($times as $time) {
            if ($time->hotel_check_in_time_fr == 'PM' && $time->hotel_check_in_time_hr != "12") {
                $modifier = ($time->hotel_check_in_time_hr + 12);
            } else {
                $modifier = ($time->hotel_check_in_time_hr);
            }
        }

        $hotel_id = $this->session->userdata('user_hotel');
        $start = $_GET['st'];
        $end = $_GET['ed'];
        $result = $this->bookings_model->all_rooms_bookings_event($hotel_id, $start, $end);
        //echo "<pre>";
        //print_r($result);
        //exit;
        $events = array();
        date_default_timezone_set("UTC");
        $now = new DateTime("now");
        $today = $now->setTime(0, 0, 0);

        date_default_timezone_set('Asia/Kolkata');
        foreach ($result as $row) {
            //change all temp hold to pending after 2 hr.	
            if ($row->booking_status_id == 1) {
                //date_default_timezone_set('Asia/Kolkata');
                //$span=date('h:i:s')
                $time_checkin = $row->booking_taking_time;
                $time = date("H:i", strtotime($time_checkin) + 60 * 60 * 2);
                //$time=date('h:i:s', (strtotime($rs->booking_taking_time)+(100)));
                $current_time = date("H:i");
                if ($time < $current_time) {
                    $data5 = array(
                        'booking_status_id_secondary' => 3,
                    );
                    $query = $this->bookings_model->change_book_id2($row->booking_id, $data5);
                }
            }
            //change all advance to pending after checkin time passes.
            if ($row->booking_status_id == 2) {
                //date_default_timezone_set('Asia/Kolkata');
                if (date("Y-m-d H:i:s", strtotime($row->cust_from_date) + 60 * 60 * $modifier + 60 * 60 * 2) < date("Y-m-d H:i:s")) {
                    $data6 = array(
                        'booking_status_id_secondary' => 3,
                    );
                    $query = $this->bookings_model->change_book_id2($row->booking_id, $data6);
                }
            }
            //change all confirmed to pending after checkout time passes.
            if ($row->booking_status_id == 4) {
                //date_default_timezone_set('Asia/Kolkata');
                if (date("Y-m-d H:i:s", strtotime($row->cust_from_date) + 60 * 60 * $modifier + 60 * 60 * 2) < date("Y-m-d H:i:s")) {
                    $data8 = array(
                        'booking_status_id_secondary' => 3,
                    );
                    $query = $this->bookings_model->change_book_id2($row->booking_id, $data8);
                }
            }
            //change all checkin to due after checkout date passes.
            if ($row->booking_status_id == 5) {
                //date_default_timezone_set('Asia/Kolkata');
                if (date("Y-m-d H:i:s", strtotime($row->cust_end_date) + 60 * 60 * $modifier + 60 * 60 * 2) < date("Y-m-d H:i:s")) {
                    $data7 = array(
                        'booking_status_id_secondary' => 9,
                    );
                    $query = $this->bookings_model->change_book_id2($row->booking_id, $data7);
                }
            }

            $e = new Event();
            $e->id = $row->booking_id;
            $tax_flag = "";
            if ($row->group_id != 0) {
                $GPcost = $this->dashboard_model->GPcost($row->group_id);
                $SBcost = $this->dashboard_model->SBcost($row->booking_id);
                if (($GPcost->charges_cost > 0) && ($SBcost->booking_extra_charge_amount > 0)) //if Charge is in Both Grp & Single
                {
                    $charge_flag = '<i class="fa fa-plus" style="float:right; color:#9C1276;"></i>';
                } else if ($SBcost->booking_extra_charge_amount > 0) //if Charge is in Single under Grp
                {
                    $charge_flag = '<i class="fa fa-plus" style="float:right; color:#FF7A24;"></i>';
                } else if ($GPcost->charges_cost > 0) //if Charge is in grp
                {
                    $charge_flag = '<i class="fa fa-plus" style="float:right; color:#E54683;"></i>';
                } else {
                    $charge_flag = '';
                }

                $group_identifier = '<i class="fa fa-group" aria-hidden="true" style="color:#138e9d;"></i>';
                $showID = "HM0" . $hotel_id . "0" . $row->booking_id . " (HM0" . $hotel_id . "GB0" . $row->group_id . ")";
                $showIDs = "0" . $hotel_id . "0" . $row->booking_id . " (0" . $hotel_id . "GB0" . $row->group_id . ")";
                $g_t_glag = $this->dashboard_model->get_grp_details_tax($row->group_id);
                $g_details = $this->dashboard_model->particular_booking($row->booking_id);
                if ($g_details) {
                    foreach ($g_details as $g_detail) {
                        if ($g_detail->room_rent_tax_details == '') {
                            $tax_flag = '<i class="fa fa-minus-circle" style="float:right; color:#E27979;"></i>';
                        } else {
                            $tax_flag = '';
                        }
                    }
                }

                $gb = 'gb';
                $sb = 'sb';
                $pos = $this->bookings_model->pos_check($row->group_id, $gb);
                $pos_s = $this->bookings_model->pos_check($row->booking_id, $sb);
                if (($pos_s == 1) && ($pos == 1)) //if POS is in Both Grp & Single _sb
                {
                    $pos_flag = '<i class="fa fa-cutlery" style="float:right; color:#9C1276;"></i>';
                } else if ($pos_s == 1) //if POS is in Single under Grp
                {
                    $pos_flag = '<i class="fa fa-cutlery" style="float:right; color:#FF7A24;"></i>';
                } else if ($pos == 1) //if POS is in grp
                {
                    $pos_flag = '<i class="fa fa-cutlery" style="float:right; color:#E54683;"></i>';
                } else {
                    $pos_flag = '';
                }
            } else {
                $SBcost = $this->dashboard_model->SBcost($row->booking_id);
                if ($SBcost->booking_extra_charge_amount > 0 || $SBcost->service_price > 0) {
                    $charge_flag = '<i class="fa fa-plus" style="float:right; color:#0000B3;"></i>';
                } else {
                    $charge_flag = '';
                }

                $gb = 'sb';
                $pos = $this->bookings_model->pos_check($row->booking_id, $gb);

                if ($pos == 1) {
                    $pos_flag = '<i class="fa fa-cutlery" style="float:right; color:#0000B3;"></i>';
                } else {
                    $pos_flag = '';
                }
                $group_identifier = "";
                $showID = "HM0" . $hotel_id . "0" . $row->booking_id;
                $showIDs = "0" . $hotel_id . "0" . $row->booking_id;

                if ($row->room_rent_tax_details == 'Tax') {
                    $tax_flag = '';
                } else {

                    $tax_flag = '<i class="fa fa-minus-circle" style="float:right; color:#E27979;"></i>';
                }
            }

            $cust_name = $row->cust_name;
            $guests = $this->dashboard_model->get_guest_details($row->guest_id);
            if ($guests) {
                foreach ($guests as $key) {
                    $cust_name = $key->g_name;
                }
            }
            $e->cust_name = $group_identifier . " " . $cust_name;
            $e->text = "<strong>" . $cust_name . "</strong> (" . $row->cust_contact_no . ") ";
            $e->amt_till = $row->cust_payment_initial;
            $e->start = $row->cust_from_date;
            $e->source = $row->booking_source;
            $e->end = $row->cust_end_date;
            $e->resource = $row->room_id;
            $e->status = $row->booking_status_id;
            $e->status_secondary = $row->booking_status_id_secondary;
            //date("d-m-Y",strtotime($comp->c_renewal))

            if ($e->status == 5) {
                $time = "<strong>Checkin Time :</strong>" . date("d-m-Y", strtotime($row->cust_from_date)) . " " . $row->checkin_time;
            } else if ($e->status == 6) {
                $time = "<strong>Checkout Time :</strong>" . date("d-m-Y", strtotime($row->cust_end_date)) . " " . $row->checkout_time;
            } else {
                $time = "<strong class='nah'>Not arrived</strong> ";
            }

            if (isset($row->comment) && $row->comment != "") {
                $cmt = $row->comment;
            } else {
                $cmt = 'N/A';
            }

            if (isset($row->preference) && $row->preference != "") {
                $pmt = $row->preference;
            } else {
                $pmt = 'N/A';
            }

            $e->bubbleHtml = "<strong>Booking ID :</strong> " . $showID . "" . $tax_flag . " " . $pos_flag . " " . $charge_flag . "<br/> <strong>" . $e->text . "</strong><br/>  " . $time . "<br/> <strong>Source :</strong> " . $row->booking_source_name . "<br/> <strong>Note :</strong> " . $cmt . "<br/> <strong>Preference :</strong> " . $pmt;
            //$e->bubbleHtml = "Reservation details: <br/>".$e->text."<br/> Source:";
            // additional properties

            $e->booking_status = $row->booking_status . " " . $tax_flag . " " . $pos_flag . " " . $charge_flag . " </br>" . $showIDs;
            if ($row->booking_status_id_secondary != 0) {
                $codes = $this->bookings_model->fetch_color($row->booking_status_id_secondary);
                foreach ($codes as $color) {
                    $e->booking_status_secondary = $color->booking_status_slug;
                }
            } else {
                $e->booking_status_secondary = '';
            }
            if ($row->booking_status_id_secondary != 0) {
                $codes = $this->bookings_model->fetch_color($row->booking_status_id_secondary);
                foreach ($codes as $color) {
                    $e->bar_color_code = $color->bar_color_code;
                }
            } else {
                $e->bar_color_code = $row->bar_color_code;
            }

            $e->body_color_code = $row->body_color_code;
            $events[] = $e;
        } // End Foreach

        header('Content-Type: application/json');
        echo json_encode($events);
    } // End hotel_backend_events


    function hotel_edit_booking()
    {
        $booking_id = $_GET['id'];
        $data['rooms'] = $this->bookings_model->room_edit_hotelwise($booking_id);
        $data['transaction'] = $this->bookings_model->get_total_payment($booking_id);
        $data['view_transaction'] = $this->bookings_model->get_transaction_details($booking_id);
        $data['bookingLineItem'] = $this->dashboard_model->get_bookingLineItem($booking_id);
        $data['Tsettings'] = $this->dashboard_model->get_Tsettings($this->session->userdata('user_hotel'));
        //print_r($data);
        //exit();

        $this->load->view("backend/dashboard/pop_edit_booking", $data);
    }


    function hotel_backend_edit()
    {
        //date_default_timezone_set("Asia/Kolkata");

        $edit_book = array(
            'booking_id' => $this->input->post('booking_id'),

            'cust_name' => $this->input->post('cust_name'),
            'cust_address' => $this->input->post('cust_address'),
            'cust_contact_no' => $this->input->post('cust_contact_no'),
            'cust_from_date' => $this->input->post('cust_from_date'),
            'cust_end_date' => $this->input->post('cust_end_date'),
            'booking_status_id' => $this->input->post('cust_booking_status'),
            'cust_payment_initial' => $this->input->post('cust_payment_initial')
        );

        $query = $this->bookings_model->add_edit_room($edit_book);


        $response = new Result();
        $response->result = 'OK';



        header('Content-Type: application/json');
        echo json_encode($response);
    }


    function booking_delete()
    {
        $booking_id = $this->input->post('id');
        $query = $this->bookings_model->delete_booking_room($booking_id);
        $response = new Result();
        $response->result = 'OK';
        $response->message = 'Delete successful';

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function hotel_booking_resize()
    {

        $hotel_id = $this->session->userdata('user_hotel');


        $start = $_GET['newStart'];
        $end = $_GET['newEnd'];

        //echo "<br>";
        $datetime1 = date("Y-m-d", strtotime($start));
        $datetime2 = date("Y-m-d", strtotime($end));

        $diff = abs(strtotime($datetime2) - strtotime($datetime1));

        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days_updated = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));


        //echo "<br>";
        //exit;
        $booking_id = $_GET['id'];

        $group = $this->dashboard_model->get_group_id($booking_id);
        //print_r($group);
        $group_id = $group->group_id;
        // exit();

        $days_prev_obj = $this->bookings_model->get_stay_days($booking_id);
        if (isset($days_prev_obj)) {
            foreach ($days_prev_obj as $d) {

                $days_prev = $d->stay_days;
            }
        } else {
            $days_prev = 3;
        }
        //echo $days_prev ;
        //exit;

        $days_ratio = $days_updated / $days_prev;

        //new logic
        //get the exact checkin and checkout date

        $details = $this->bookings_model->get_booking_details_row($booking_id);
        $backresize = 0;
        $cust = date("Y-m-d", strtotime($details->cust_from_date));
        if ($cust > $datetime1) {

            $backresize = 1;
        }


        $times = $this->dashboard_model->checkin_time();
        foreach ($times as $time) {
            if ($time->hotel_check_in_time_fr == 'PM' && $time->hotel_check_in_time_hr != "12") {
                $modifier = ($time->hotel_check_in_time_hr + 12) + $time->hotel_check_in_time_mm / 60;
            } else {
                $modifier = ($time->hotel_check_in_time_hr) + $time->hotel_check_in_time_mm / 60;
            }
        }

        $times2 = $this->dashboard_model->checkout_time();

        foreach ($times2 as $time2) {
            if ($time2->hotel_check_out_time_fr == 'PM' && $time2->hotel_check_out_time_hr != "12") {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12) + $time->hotel_check_out_time_mm / 60;
            } else {
                $modifier2 = ($time2->hotel_check_out_time_hr) + $time->hotel_check_out_time_mm / 60;
            }
        }
        $checkin_date = date("Y-m-d H:i:s", strtotime($details->cust_from_date) + 60 * 60 * $modifier);
        $checkout_date = date("Y-m-d H:i:s", strtotime($details->cust_end_date) + 60 * 60 * $modifier2);
        //ending


        //checking start
        date_default_timezone_set('Asia/Kolkata');
        $current_date = date("Y-m-d H:i:s");
        if ($details->booking_status_id == "5") {
            $start = $details->cust_from_date;
        } else if ($details->booking_status_id == "6") {
            $data['checkout'] = 1;
        }

        //checkin end

        //new logic end

        $data['ratio'] = $days_ratio;
        $data['room_rent_sum_total'] = $this->bookings_model->get_total_amount($booking_id);
        $data['line_item_details_all'] = $this->bookings_model->hotelBookingsDetails_resize($hotel_id, $booking_id);
        $data['line_item_details'] = $this->bookings_model->hotelBookingsDetails($hotel_id, $booking_id);
        $data['id'] = $_GET['id'];
        $data['start'] = $start;
        $data['end'] = $end;
        $data['days_updated'] = $days_updated;
        $data['days_prev'] = $days_prev;
        $data['group_id'] = $group_id;
        $data['details'] = $details;
        $data['events'] = $this->dashboard_model->all_events_limit();

        /*echo "<pre>";
		print_r($data);
		echo "</pre>";
		exit;*/
        if ($backresize == '1') {
            $dataerror['backresize'] = 1;
            $this->load->view("backend/dashboard/hotel_move_error_msg", $dataerror);
        } else {
            $this->load->view("backend/dashboard/hotel_booking_resize", $data);
        }
    }

    function booking_backend_resize()
    {
        $start = $this->input->post('newStart');
        $end = $this->input->post('newEnd');

        $meal_plan_chrg = $this->input->post('meal_plan_chrg');
        $room_rent = $this->input->post('room_rent');
        $exrr_chrg = $this->input->post('exrr_chrg');
        $exmp_chrg = $this->input->post('exmp_chrg');

        $rr_tax_response = $this->input->post('rr_tax_response');
        $mp_tax_response = $this->input->post('mp_tax_response');
        $rr_total_tax = $this->input->post('rr_total_tax');
        $mp_total_tax = $this->input->post('mp_total_tax');



        $start_ac = date("Y-m-d", strtotime($start));
        $end_ac = date("Y-m-d", strtotime($end));
        $ammount = $this->input->post('final_sum_amount');

        $datetime1 = date("d-m-Y", strtotime($start));
        $datetime2 = date("d-m-y", strtotime($end));

        $days_updated = $datetime2 - $datetime1;
        //echo "days difference=".$days_updated;
        $diff = abs(strtotime($end_ac) - strtotime($start_ac));
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days_updated = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));


        $booking_id = $this->input->post('id');
        $lineitemdetails = array(

            'hotel_id' => $this->session->userdata("user_hotel"),
            'booking_id' => $booking_id,
            'meal_plan_chrg' => $meal_plan_chrg,
            'room_rent' =>         $room_rent,
            'exrr_chrg' => $exrr_chrg,
            'exmp_chrg' => $exmp_chrg,
            'rr_tax_response' => $rr_tax_response,
            'mp_tax_response' => $mp_tax_response,
            'rr_total_tax' => $rr_total_tax,
            'mp_total_tax' => $mp_total_tax,
            //'date' => $meal_plan_chrg,
            'added_on' => date('Y-m-d H:i:s')
        );


        $days_prev_obj = $this->bookings_model->get_stay_days($booking_id);
        if (isset($days_prev_obj)) {
            foreach ($days_prev_obj as $d) {
                $current_booking_end_date = $d->cust_end_date_actual;
                $days_prev = $d->stay_days;
            }
        } else {
            $days_prev = 3;
        }
        //echo "days previously".$days_prev;
        $ins = $days_updated - $days_prev;
        //echo "new insert".$ins;
        //echo "#####";
        if ($days_updated < $days_prev) {


            //echo "new insert===".$ins;
            for ($j = 0; $j < abs($ins); $j++) {

                if ($j == 0) {
                    //$lineitemdetails['date'] = $current_booking_end_date;
                    //	echo "I------------->>>>>>>>".$j;
                    $tempdate = $current_booking_end_date;
                    $date = date_create($tempdate);
                    //echo "oo".$date;
                    date_sub($date, date_interval_create_from_date_string("1 days"));
                    $tempdate = date_format($date, "Y-m-d");
                    //$tempdate = $lineitemdetails['date'];
                } else {
                    //echo "I------------->>>>>>>>".$j;
                    //$current_booking_end_date = date("d-m-Y",strtotime($current_booking_end_date));
                    $date = date_create($tempdate);
                    //echo "oo".$date;
                    date_sub($date, date_interval_create_from_date_string("1 days"));
                    $tempdate = date_format($date, "Y-m-d");
                }
                //	echo "I------------->>>>>>>>".$j;
                //	echo "Temp date" .$tempdate;

                $newresult = $this->bookings_model->delete_line_item($booking_id, $tempdate);
            }
            //	exit;
        } else {
            for ($i = 0; $i < $ins; $i++) {

                if ($i == 0) {
                    $lineitemdetails['date'] = $current_booking_end_date;
                    $tempdate = $current_booking_end_date;
                } else {
                    //$current_booking_end_date = date("d-m-Y",strtotime($current_booking_end_date));
                    $date = date_create($tempdate);
                    //echo "oo".$date;
                    date_add($date, date_interval_create_from_date_string("1 days"));
                    $lineitemdetails['date'] = date_format($date, "Y-m-d");
                    $tempdate = $lineitemdetails['date'];
                }
                /*	echo "I------------->>>>>>>>".$i;
					echo "<pre>";
		print_r($lineitemdetails);
		echo "</pre>";
		*/
                $newresult = $this->bookings_model->insert_line_item($lineitemdetails);
            }
        }
        //exit;
        $days_ratio = $days_updated / $days_prev;

        $result = $this->bookings_model->update_rooms_bookings_event($booking_id, $start, $end, $start_ac, $end_ac, $days_ratio, $days_updated, $ammount);




        $response = new Result();
        $response->result = 'OK';
        $response->message = $result;

        //$this->load->view("backend/dashboard/index",$data);
        //header('Content-Type: application/json');
        $data['message'] = $response->message;
        $this->load->view("backend/dashboard/hotel_booking_resize", $data);
        //echo $response->message;
    }

    /* function booking_backend_move()
    { 
        $start = $this->input->post('newStart');
        $end = $this->input->post('newEnd');
        $start_ac=date("Y-m-d",strtotime($start));
        $end_ac=date("Y-m-d",strtotime($end));
        $booking_id = $this->input->post('id');
		$stay_days = $this->input->post('stay_days');
        $room_id = $this->input->post('newResource');
        $sd = $this->input->post('sd');
		$amt = $this->input->post('amt');
		$tm = $this->input->post('final_sum_amount');
		$tax = $this->input->post('tax');
		$tax_response = $this->input->post('rr_tax_responsetax');
		$mealtax = $this->input->post('mealtax');
		$tax_response_mp = $this->input->post('mp_tax_responsetax');
		$extra_person_room_rent = $this->input->post('extra_person_room_rent');
		$extra_person_rate_price = $this->input->post('extra_person_rate_price');
		
		$meal_price = $this->input->post('meal_price');
		$applicableTax = ($tm * $tax)/100;
        $overlaps = $this->bookings_model->count_rooms_bookings_event($booking_id,$room_id,$start,$end) > 0;

            if ($overlaps) 
            {
                $response = new Result();
                $response->result = 'Error';
                $response->message = 'This reservation overlaps with an existing reservation.';

                header('Content-Type: application/json');
                echo json_encode($response);
                exit;
            }

            //get the exact checkin and checkout date

            $details=$this->bookings_model->get_booking_details_row($booking_id);
            $times=$this->dashboard_model->checkin_time();
        foreach($times as $time) {
            if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
                $modifier = ($time->hotel_check_in_time_hr + 12) + $time->hotel_check_in_time_mm/60;
            }
            else{
                $modifier = ($time->hotel_check_in_time_hr) + $time->hotel_check_in_time_mm/60;
            }
        }

        $times2=$this->dashboard_model->checkout_time();
		
        foreach($times2 as $time2) {
            if($time2->hotel_check_out_time_fr=='PM' && $time2->hotel_check_out_time_hr !="12") {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12) + $time->hotel_check_out_time_mm/60;
            }
            else{
                $modifier2 = ($time2->hotel_check_out_time_hr) + $time->hotel_check_out_time_mm/60;
            }
        }
            $checkin_date=date("Y-m-d H:i:s",strtotime($details->cust_from_date)+60*60*$modifier);
            $checkout_date=date("Y-m-d H:i:s",strtotime($details->cust_end_date)+60*60*$modifier2);
            //ending


            //checking start
            date_default_timezone_set('Asia/Kolkata');
            $current_date=date("Y-m-d H:i:s");
			
           if($details->booking_status_id=="5"){
               $start=$details->cust_from_date;
               //exit();
           }
		   
		   if($details->booking_status_id=="6"){
              $end=$details->cust_end_date;
              exit();
           }

            //checkin end


        
        $result = $this->bookings_model->update_rooms_bookings_move($booking_id,$room_id,$start,$end,$start_ac,$end_ac,$tm,$amt,$applicableTax);
		
		$lineitem_details = $this->bookings_model->get_booking_line_charge_item_details($booking_id);
		
				
		
		$lineitemarray = array(
		 
		 'room_rent' => $amt,
		 'rr_tax_response' => $tax_response,
		 'rr_total_tax' => $tax,
		 'date' => $start_ac,
		 'meal_plan_chrg' => $meal_price,
		 'mp_tax_response' => $tax_response_mp,
		 'mp_total_tax' => $mealtax,
		 'exrr_chrg' => $extra_person_room_rent,
		 'exmp_chrg' => $extra_person_rate_price,
		 'exrr_total_tax' => $this->input->post('room_charge_tax_exp'),
		 'exmp_total_tax' => $this->input->post('meal_charge_tax_exp')
		 );
      //   echo "stay_days".$stay_days;
		 for($u=0;$u< $stay_days;$u++){
			 
			 
			if($u==0){
				
				  $this->bookings_model->new_update_line_item($booking_id,$lineitemarray,$lineitem_details[$u]['bcli_id']);
					
			}	
					if($u!=0){
						$date=date_create($lineitemarray['date']);
						date_add($date,date_interval_create_from_date_string("1 days"));
						$lineitemarray['date'] = date_format($date,"Y-m-d");
						  $this->bookings_model->new_update_line_item($booking_id,$lineitemarray,$lineitem_details[$u]['bcli_id']);
				
					}
					
				
				
			} 
		
        $response = new Result();
        $response->result = 'OK';
        $response->message = 'Update successful';

       // header('Content-Type: application/json');
        //echo json_encode($response);
        $data['message']=$response->message;
        $this->load->view("backend/dashboard/hotel_booking_move",$data);
    }
    */


    function booking_backend_move()
    {
        $start = $this->input->post('newStart');
        $end = $this->input->post('newEnd');
        $start_ac = date("Y-m-d", strtotime($start));

        //   $end_ac=date("Y-m-d",strtotime($end));

        $booking_id = $this->input->post('id');
        $stay_days = $this->input->post('stay_days');
        $room_id = $this->input->post('newResource');
        $sd = $this->input->post('sd');
        $amt = $this->input->post('amt');
        $tm = $this->input->post('final_sum_amount');
        $tax = $this->input->post('tax');


        $stay_days_final = $stay_days . " " . "days";


        $end_ac = date_create($start_ac);
        date_add($end_ac, date_interval_create_from_date_string($stay_days_final));
        $end_ac = date_format($end_ac, "Y-m-d");



        $room_price_modifier = $this->input->post('room_price_modifier');
        $meal_price_modifier = $this->input->post('meal_price_modifier');
        $food_plan_mod_type = $this->input->post('food_plan_mod_type');
        $rent_mode_type = $this->input->post('rent_mode_type');

        $tax_response = $this->input->post('rr_tax_responsetax');
        $mealtax = $this->input->post('mealtax');
        $tax_response_mp = $this->input->post('mp_tax_responsetax');
        $extra_person_room_rent = $this->input->post('extra_person_room_rent');
        $extra_person_rate_price = $this->input->post('extra_person_rate_price');

        $meal_price = $this->input->post('meal_price');
        $applicableTax = ($tm * $tax) / 100;

        if (isset($meal_price_modifier) && $meal_price_modifier) {

            if (isset($food_plan_mod_type) && $food_plan_mod_type == 'add') {

                $meal_plan_chrg_line = $meal_price + $meal_price_modifier;
            } else {

                $meal_plan_chrg_line = $meal_price - $meal_price_modifier;
            }
        }

        /*echo "room_price_modifier<br>".$room_price_modifier."meal_price_modifier<br>".$meal_price_modifier."food_plan_mod_type<br>".$food_plan_mod_type."rent_mode_type<br>".$rent_mode_type."amt<br>".$amt."meal_price<br>".$meal_price;
		exit;
		*/


        $overlaps = $this->bookings_model->count_rooms_bookings_event($booking_id, $room_id, $start, $end) > 0;

        if ($overlaps) {
            $response = new Result();
            $response->result = 'Error';
            $response->message = 'This reservation overlaps with an existing reservation.';

            header('Content-Type: application/json');
            echo json_encode($response);
            exit;
        }

        //get the exact checkin and checkout date

        $details = $this->bookings_model->get_booking_details_row($booking_id);
        $times = $this->dashboard_model->checkin_time();
        foreach ($times as $time) {
            if ($time->hotel_check_in_time_fr == 'PM' && $time->hotel_check_in_time_hr != "12") {
                $modifier = ($time->hotel_check_in_time_hr + 12) + $time->hotel_check_in_time_mm / 60;
            } else {
                $modifier = ($time->hotel_check_in_time_hr) + $time->hotel_check_in_time_mm / 60;
            }
        }

        $times2 = $this->dashboard_model->checkout_time();

        foreach ($times2 as $time2) {
            if ($time2->hotel_check_out_time_fr == 'PM' && $time2->hotel_check_out_time_hr != "12") {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12) + $time->hotel_check_out_time_mm / 60;
            } else {
                $modifier2 = ($time2->hotel_check_out_time_hr) + $time->hotel_check_out_time_mm / 60;
            }
        }
        $checkin_date = date("Y-m-d H:i:s", strtotime($details->cust_from_date) + 60 * 60 * $modifier);
        $checkout_date = date("Y-m-d H:i:s", strtotime($details->cust_end_date) + 60 * 60 * $modifier2);
        //ending


        //checking start
        date_default_timezone_set('Asia/Kolkata');
        $current_date = date("Y-m-d H:i:s");

        if ($details->booking_status_id == "5") {
            $start = $details->cust_from_date;
            //exit();
        }

        if ($details->booking_status_id == "6") {
            $end = $details->cust_end_date;
            exit();
        }

        //checkin end



        $result = $this->bookings_model->update_rooms_bookings_move($booking_id, $room_id, $start, $end, $start_ac, $end_ac, $tm, $amt, $applicableTax, $meal_price, $meal_price_modifier);

        $lineitem_details = $this->bookings_model->get_booking_line_charge_item_details($booking_id);



        $lineitemarray = array(

            'room_rent' => $amt,
            'rr_tax_response' => $tax_response,
            'rr_total_tax' => $tax,
            'date' => $start_ac,
            'meal_plan_chrg' => $meal_plan_chrg_line,
            'mp_tax_response' => $tax_response_mp,
            'mp_total_tax' => $mealtax,
            'exrr_chrg' => $extra_person_room_rent,
            'exmp_chrg' => $extra_person_rate_price,
            //	 'exrr_total_tax' => $this->input->post('room_charge_tax_exp'),
            //	 'exmp_total_tax' => $this->input->post('meal_charge_tax_exp')
        );
        //   echo "stay_days".$stay_days;
        for ($u = 0; $u < $stay_days; $u++) {


            if ($u == 0) {

                $this->bookings_model->new_update_line_item($booking_id, $lineitemarray, $lineitem_details[$u]['bcli_id']);
            }
            if ($u != 0) {
                $date = date_create($lineitemarray['date']);
                date_add($date, date_interval_create_from_date_string("1 days"));
                $lineitemarray['date'] = date_format($date, "Y-m-d");
                $this->bookings_model->new_update_line_item($booking_id, $lineitemarray, $lineitem_details[$u]['bcli_id']);
            }
        }

        $response = new Result();
        $response->result = 'OK';
        $response->message = 'Update successful';

        // header('Content-Type: application/json');
        //echo json_encode($response);
        $data['message'] = $response->message;
        $this->load->view("backend/dashboard/hotel_booking_move", $data);
    }




    function pop_new_room()
    {
        $data = array(
            'room_id' => $_GET['id'],
        );

        if ($_GET['id'] == 'unit') {
            $this->load->view("backend/dashboard/popRoom");
        } else {
            $this->load->view("backend/dashboard/pop_new_room", $data);
        }
    }
    function get_guest()
    {
        $keyword = $this->input->post('keyword');
        $data = $this->bookings_model->get_guest($keyword);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function invoice_generate()
    {


        $id = $this->session->userdata('user_id');
        /*$data = $this->dashboard_model->admin_status($id);
            if(isset($data) && $data){
                $hotel_id = $data->admin_hotel;
                }else{
                $hotel_id = '';
                }*/
        $booking_id = $_GET['booking_id'];

        $data['booking_details']  = $this->bookings_model->get_booking_details($booking_id);
        $data['hotel_name'] = $this->dashboard_model->get_hotel($data['booking_details'][0]->hotel_id);
        $data['hotel_contact']  = $this->bookings_model->get_hotel_contact_details($data['booking_details'][0]->hotel_id);
        //echo "<pre>";
        //print_r($data['booking_details'][0]->hotel_id);
        $data['transaction_details']  = $this->bookings_model->get_transaction_details($booking_id);
        $data['payment_details']  = $this->bookings_model->get_payment_details($booking_id);
        $data['total_payment']  = $this->bookings_model->get_total_payment($booking_id);
        $this->load->view("backend/dashboard/invoice_generate", $data);
        //$this->load->view("backend/dashboard/pop_edit_booking",$data);
    }


    function pdf_generate()
    {
        $booking_id = $_GET['booking_id'];
        $type = "sb";
        $hotel_id = $this->session->userdata('user_hotel');
        $data['booking_details']  = $this->bookings_model->get_booking_details($booking_id);
        foreach ($data['booking_details'] as $key) {
            $data['group_id'] = $key->group_id;
            $bk_date = $key->booking_taking_date;
        }

        $data['hotel_name'] = $this->dashboard_model->get_hotel($data['booking_details'][0]->hotel_id);
        $data['hotel_contact']  = $this->bookings_model->get_hotel_contact_details($data['booking_details'][0]->hotel_id);
        //echo "<pre>";
        //print_r($data['booking_details'][0]->hotel_id);
        //$hotel_id =$this->session->userdata('user_hotel');
        //$hotel_id=$this->bookings_model->get_hotel_contact_details($data['booking_details'][0]->hotel_id);
        //  line_charge_item_tax($type,$booking_id);

        $inv_name = "BK0" . $hotel_id . "_" . $bk_date . "_" . $booking_id;
        $data['tax'] = $this->bookings_model->get_tax_details($hotel_id);
        $data['adjustment'] = $this->dashboard_model->get_adjuset_amt($booking_id);
        $data['all_adjustment'] = $this->dashboard_model->all_adjst($booking_id);
        $data['chagr_tax'] = $this->bookings_model->line_charge_item_tax($type, $booking_id);
        $data['transaction_details']  = $this->bookings_model->get_transaction_details($booking_id);
        $data['payment_details']  = $this->bookings_model->get_payment_details($booking_id);
        $data['total_payment']  = $this->bookings_model->get_total_payment($booking_id);
        $data['pos']  = $this->dashboard_model->all_pos_booking($booking_id);
        $data['printer']  = $this->dashboard_model->get_printer($hotel_id);
        $data['bid']  = $booking_id;
        $this->load->view("backend/dashboard/invoice_pdf", $data);
        //COMMENT THE BELOW 5 LINES TO STOP THE PDF GENERATION
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("invoice_$inv_name.pdf");
        //END COMMENT  
    }

    function pdf_single_view()
    {
        $booking_id = $_GET['booking_id'];
        $type = "sb";
        $hotel_id = $this->session->userdata('user_hotel');
        $data['booking_details']  = $this->bookings_model->get_booking_details($booking_id);
        foreach ($data['booking_details'] as $key) {
            $data['group_id'] = $key->group_id;
            $bk_date = $key->booking_taking_date;
        }

        $data['hotel_name'] = $this->dashboard_model->get_hotel($data['booking_details'][0]->hotel_id);
        $data['hotel_contact']  = $this->bookings_model->get_hotel_contact_details($data['booking_details'][0]->hotel_id);
        //echo "<pre>";
        //print_r($data['booking_details'][0]->hotel_id);
        //$hotel_id =$this->session->userdata('user_hotel');
        //$hotel_id=$this->bookings_model->get_hotel_contact_details($data['booking_details'][0]->hotel_id);
        //  line_charge_item_tax($type,$booking_id);

        $inv_name = "BK0" . $hotel_id . "_" . $bk_date . "_" . $booking_id;
        $data['tax'] = $this->bookings_model->get_tax_details($hotel_id);
        $data['adjustment'] = $this->dashboard_model->get_adjuset_amt($booking_id);
        $data['all_adjustment'] = $this->dashboard_model->all_adjst($booking_id);
        $data['chagr_tax'] = $this->bookings_model->line_charge_item_tax($type, $booking_id);
        $data['transaction_details']  = $this->bookings_model->get_transaction_details($booking_id);
        $data['payment_details']  = $this->bookings_model->get_payment_details($booking_id);
        $data['total_payment']  = $this->bookings_model->get_total_payment($booking_id);
        $data['pos']  = $this->dashboard_model->all_pos_booking($booking_id);
        $data['printer']  = $this->dashboard_model->get_printer($hotel_id);
        $data['bid']  = $booking_id;
        $this->load->view("backend/dashboard/invoice_single_view", $data);
        //COMMENT THE BELOW 5 LINES TO STOP THE PDF GENERATION
        $html = $this->output->get_output();
        echo $html;
        exit;
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("invoice_$inv_name.pdf");
        //END COMMENT  
    }

    function pdf_generate_view()
    {
        $booking_id = $_GET['booking_id'];
        $data['booking_details'] = $this->bookings_model->get_booking_details($booking_id);
        foreach ($data['booking_details'] as $key) {
            $data['group_id'] = $key->group_id;
        }

        $data['hotel_name'] = $this->dashboard_model->get_hotel($data['booking_details'][0]->hotel_id);
        $data['hotel_contact']  = $this->bookings_model->get_hotel_contact_details($data['booking_details'][0]->hotel_id);
        //echo "<pre>";
        //print_r($data['booking_details'][0]->hotel_id);
        $data['transaction_details']  = $this->bookings_model->get_transaction_details($booking_id);
        $data['payment_details']  = $this->bookings_model->get_payment_details($booking_id);
        $data['total_payment']  = $this->bookings_model->get_total_payment($booking_id);
        $data['pos']  = $this->dashboard_model->all_pos_booking($booking_id);
        $data['page'] = 'backend/dashboard/invoice_pdf_view';
        $this->load->view("backend/common/index", $data);
    }

    function send_mail()
    {
        $booking_id = $this->input->post('booking_id');
        $data['booking_details']  = $this->bookings_model->get_booking_details($booking_id);
        $data['hotel_name'] = $this->dashboard_model->get_hotel($data['booking_details'][0]->hotel_id);
        $data['hotel_contact']  = $this->bookings_model->get_hotel_contact_details($data['booking_details'][0]->hotel_id);
        //echo "<pre>";
        //print_r($data['booking_details'][0]->hotel_id);
        $data['transaction_details']  = $this->bookings_model->get_transaction_details($booking_id);
        $data['payment_details']  = $this->bookings_model->get_payment_details($booking_id);
        $data['total_payment']  = $this->bookings_model->get_total_payment($booking_id);
        $format =  $this->load->view("backend/dashboard/invoice_pdf", $data, TRUE);
        $html = $this->output->get_output();

        $booking_id = $this->input->post('booking_id');
        $booking_details = $this->dashboard_model->get_booking_details($booking_id);
        $guest_id = $booking_details->guest_id;

        $guest_details = $this->dashboard_model->get_guest_details($guest_id);

        foreach ($guest_details as $guest) {
            if ($guest->g_email != "") {
                $email = $guest->g_email;
                $name = $guest->g_name;
            } else {
                $email = "samrat360@gmail.com";
                $name = "No Email Address";
            }
        }

        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'samrat360@gmail.com'; //please change this & give an option in setting to update these for the hotels _sb
        $config['smtp_pass']    = 'samrat##';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not

        $this->email->initialize($config);

        $this->email->from('samrat360@gmail.com', 'Admin HotelObjects');
        $this->email->to($email);

        $this->email->subject('Thanks a lot' . $name . ' for your Stay at our hotel. please do come back.');
        //$body = $this->load->view('',$data,TRUE);
        // $format = $this->load->view("backend/dashboard/invoice_pdf",$data,TRUE);
        $this->email->message($format);

        $this->email->send();

        //echo $this->email->print_debugger();

        header('Content-Type: application/json');
        echo json_encode('Email Sent Sucessfully');
    }



    function popup_close()
    {
        $response = new Result();
        $response->result = 'OK';



        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function fetch_price()
    {
        $plan_id = $this->input->post('price_id');
        //$plan_id = $_GET['price_id'];

        $food_plan = $this->bookings_model->fetch_food_plan($plan_id);

        $price_adult = $food_plan->fp_price_adult;
        $price_child = $food_plan->fp_price_children;

        $data = array(
            'adult' => $price_adult,
            'child' => $price_child
        );

        echo json_encode($data);
    }

    function fetch_address()
    {
        $pincode = $this->input->post('pincode');
        //echo $pincode;
        //exit();
        $address = $this->dashboard_model->fetch_all_address_static($pincode);
        //print_r($address);
        foreach ($address as $row) {
            $country = $row->area_0;
            $state = $row->area_1;
            $city = $row->area_4;
        }
        $data = array(
            'country' => $country,
            'state'   => $state,
            'city'    => $city
        );
        echo json_encode($data);
        //exit();
    }

    function fetch_address2()
    {
        $pincode = $this->input->post('pincode');
        //  echo $pincode;

        $address = $this->dashboard_model->fetch_all_address_static2($pincode);

        if (isset($address) && $address != '') {

            $data = array(
                'country' => $address->area_0,
                'state'   => $address->area_1

            );
        } else {

            $data = array(
                'country' => 'INDIA',
                'state'   => 'WEST BENGAL'

            );
        }
        echo json_encode($data);
    }



    function add_booking_transaction()
    {
        date_default_timezone_set('Asia/Kolkata');
        $datetime = date("Y/m/d") . " " . date("H:i:s");
        $bookings_id = $this->input->post('t_booking_id');
        $hotel_id = $this->session->userdata('user_hotel');
        $guest_name = $this->bookings_model->get_guest_id($bookings_id);
        if ($guest_name != "") {
            $name = $guest_name->cust_name;
        }
        $unit_id = $this->bookings_model->get_unitId($bookings_id);
        if ($unit_id != '') {
            $rm_No = $this->bookings_model->get_rmNo($unit_id->room_id);
        } else {
            $rm_No = "";
        }
        $chk_lasltransaction = $this->dashboard_model->chk_lasltransaction();
        $chk_lasltransaction = $chk_lasltransaction + 1;
        $transaction_id = 'TA0' . $this->session->userdata('user_hotel') . "-" . $this->session->userdata('user_id') . "/" . date("d") . date("m") . date("y") . "/" . $chk_lasltransaction;
        $t_transaction = array(
            'actual_id' => $transaction_id,
            't_booking_id' => $this->input->post('t_booking_id'),
            'details_id' => $this->input->post('t_booking_id'),
            'details_type' => 'single booking',
            't_amount' => $this->input->post('t_amount'),
            't_date' => $datetime,
            'p_center' => $this->input->post('p_center'),
            't_payment_mode' => $this->input->post('t_payment_mode'),
            't_bank_name' => $this->input->post('t_bank_name'),
            't_status' => $this->input->post('pay_status'),
            'transaction_from_id' => 2,
            'transaction_to_id' => 4,
            'user_id' => $this->session->userdata('user_id'),
            'transaction_type' => 'Booking Payment',
            'transaction_type_id' => 1,
            'transaction_releted_t_id' => 1,
            'hotel_id' => $this->session->userdata('user_hotel'),
            't_cashDrawer_id' => $this->session->userdata('cd_key'),
            'transactions_detail_notes' => "( Booking Id: HM0" . $hotel_id . "00" . $this->input->post('t_booking_id') . " Room No : " . $rm_No->room_no . " " . $name . " 
			From - " . $this->input->post('start_date') . " To - " . $this->input->post('end_date') . " )"
        );

        $query = $this->dashboard_model->add_booking_transaction($t_transaction);
        $amount = $this->input->post('t_amount');

        $cash = $this->unit_class_model->get_closing_stock($this->session->userdata('cd_key'));
        $c = $cash->closing_cash; //get clossing cash
        $cash_collection = $cash->cash_collection; //get collect cash
        $mode = $this->input->post('t_payment_mode');
        $st = $this->input->post('pay_status');
        if ($st != 'Cancel') {
            if ($mode == 'cash' || $mode == 'Cash') {
                $query1 = $this->unit_class_model->update_cash_collection($amount, $c, $cash_collection);
            } else {
                $cash = $this->unit_class_model->get_mode_cash($mode, $this->session->userdata('cd_key'));
                foreach ($cash as $key => $value) {
                    $d = $value;
                }; //get mode cash	
                //$c+=$amount;
                $d += $amount;
                $query1 = $this->unit_class_model->update_cash_drawer_mode($d, $mode);
            }
        }
        $booking_status = $this->input->post('t_booking_status_id');
        if ($booking_status == '2') {
            $book = array(
                'booking_status_id' => 4,

            );

            $query = $this->bookings_model->change_book_id($this->input->post('t_booking_id'), $book);
        }

        $data = array(
            'a' => 1,

        );
        //print_r($data);
        echo json_encode($data);
    }


    function add_booking_transaction1()
    {

        //echo "hello"; exit;
        date_default_timezone_set('Asia/Kolkata');
        $datetime = date("Y/m/d") . " " . date("H:i:s");
        $bookings_id = $this->input->post('t_booking_id');
        $hotel_id = $this->session->userdata('user_hotel');
        $guest_name = $this->bookings_model->get_guest_id($bookings_id);
        if ($guest_name != "") {
            $name = $guest_name->cust_name;
        }
        $unit_id = $this->bookings_model->get_unitId($bookings_id);
        if ($unit_id != '') {
            $rm_No = $this->bookings_model->get_rmNo($unit_id->room_id);
        } else {
            $rm_No = "";
        }
        $chk_lasltransaction = $this->dashboard_model->chk_lasltransaction();
        $chk_lasltransaction = $chk_lasltransaction + 1;
        $transaction_id = 'TA0' . $this->session->userdata('user_hotel') . "-" . $this->session->userdata('user_id') . "/" . date("d") . date("m") . date("y") . "/" . $chk_lasltransaction;
        $t_transaction = array(
            'actual_id' => $transaction_id,
            't_booking_id' => $this->input->post('t_booking_id'),
            // 't_booking_status' => $this->input->post('t_booking_status_id'),
            'details_id' => $this->input->post('t_booking_id'),
            'details_type' => 'single booking',
            't_amount' => $this->input->post('t_amount'),
            't_date' => $datetime,
            'p_center' => $this->input->post('p_center'),
            't_payment_mode' => $this->input->post('t_payment_mode'),
            't_bank_name' => $this->input->post('t_bank_name'),
            't_status' => $this->input->post('t_status'),
            'transaction_from_id' => 2,
            'transaction_to_id' => 4,
            't_cashDrawer_id' => $this->session->userdata('cd_key'),
            'hotel_id' => $this->session->userdata('user_hotel'),
            'user_id' => $this->session->userdata('user_id'),
            'transaction_type' => 'Booking Payment',
            'transaction_type_id' => 1,
            'transaction_releted_t_id' => 1,
            'transactions_detail_notes' => "( Booking Id: HM0" . $hotel_id . "00" . $this->input->post('t_booking_id') . " Room No : " . $rm_No->room_no . " " . $name . " from - " . $this->input->post('start_date') . " to - " . $this->input->post('end_date') . " )",

            'ft_bank_name' => $this->input->post('fund_bank_name'),
            'ft_account_no' => $this->input->post('fund_ac_no'),
            'ft_ifsc_code' => $this->input->post('fund_ifsc'),

            't_card_type' => $this->input->post('card_type'),
            't_card_no' => $this->input->post('card_card_no'),
            't_card_name' => $this->input->post('card_name'),
            't_card_exp_m' => $this->input->post('card_expm'),
            't_card_exp_y' => $this->input->post('card_expy'),
            't_card_cvv' => $this->input->post('card_cvv'),

            'checkno' => $this->input->post('check_chq_no'),
            't_drw_name' => $this->input->post('drw_name_c'),

            'draft_no' => $this->input->post('draft_drf_no'),
            't_drw_name' => $this->input->post('draft_drawer_name'),

            't_w_name' => $this->input->post('ewallet_name'),
            't_tran_id' => $this->input->post('ewallet_tran_id'),
            't_recv_acc' => $this->input->post('ewallet_recv_acc'),

        );

        $query = $this->dashboard_model->add_booking_transaction($t_transaction);
        $amount = $this->input->post('t_amount');

        $cash = $this->unit_class_model->get_closing_stock($this->session->userdata('cd_key'));
        $c = $cash->closing_cash; //get clossing cash
        $cash_collection = $cash->cash_collection; //get collect cash
        $mode = $this->input->post('t_payment_mode');
        if ($mode == 'cash' || $mode == 'Cash') {
            $query1 = $this->unit_class_model->update_cash_collection($amount, $c, $cash_collection);
        } else {
            $cash = $this->unit_class_model->get_mode_cash($mode, $this->session->userdata('cd_key'));
            //get mode cash	
            foreach ($cash as $key => $value) {
                $d = $value;
            }
            $c += $amount;
            $d += $amount;
            $query1 = $this->unit_class_model->update_cash_drawer_mode($d, $mode);
        }

        $booking_status = $this->input->post('t_booking_status_id');
        if ($booking_status == '2') {
            $book = array(
                'booking_status_id' => 4,

            );

            $query = $this->bookings_model->change_book_id($this->input->post('t_booking_id'), $book);
        }
        $data = 1;
        //print_r($t_transaction);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function add_booking_transaction_grp()
    {
        $str = "";
        date_default_timezone_set('Asia/Kolkata');
        $datetime = date("Y/m/d") . " " . date("H:i:s");
        $hotel_id = $this->session->userdata('user_hotel');
        $type = $this->input->post('type');

        if ($type == 'sb') {
            $bookings_id = $this->input->post('t_booking_id');
            $guest_name = $this->bookings_model->get_guest_id($bookings_id);
            if ($guest_name != "") {
                $name = $guest_name->cust_name;
            }
            $unit_id = $this->bookings_model->get_unitId($bookings_id);
            if ($unit_id != '') {
                $rm_No = $this->bookings_model->get_rmNo($unit_id->room_id);
            } else {
                $rm_No = "";
            }
            $chk_lasltransaction = $this->dashboard_model->chk_lasltransaction();
            $chk_lasltransaction = $chk_lasltransaction + 1;
            $transaction_id = 'TA0' . $this->session->userdata('user_hotel') . "-" . $this->session->userdata('user_id') . "/" . date("d") . date("m") . date("y") . "/" . $chk_lasltransaction;
            $t_transaction = array(
                'actual_id' => $transaction_id,
                't_booking_id' => $this->input->post('t_booking_id'),
                // 't_booking_status' => $this->input->post('t_booking_status_id'),
                'details_id' => $this->input->post('t_booking_id'),
                'details_type' => 'single booking',
                't_amount' => $this->input->post('t_amount'),
                't_date' => $datetime,
                'p_center' => $this->input->post('p_center'),
                't_payment_mode' => $this->input->post('t_payment_mode'),
                't_bank_name' => $this->input->post('t_bank_name'),
                't_status' => $this->input->post('pay_status'),
                'transaction_from_id' => 2,
                'transaction_to_id' => 4,
                'user_id' => $this->session->userdata('user_id'),
                'transaction_type' => 'Booking Payment',
                'transaction_type_id' => 1,
                'transaction_releted_t_id' => 1,
                'hotel_id' => $this->session->userdata('user_hotel'),
                't_cashDrawer_id' => $this->session->userdata('cd_key'),
                'transactions_detail_notes' => "( Booking Id: HM0" . $hotel_id . "00" . $this->input->post('t_booking_id') . " Room No : " . $rm_No->room_no . " " . $name . " 
			From - " . $this->input->post('start_date') . " To - " . $this->input->post('end_date') . " )"
            );
            if ($this->session->userdata('user_hotel') != '') {
                $query = $this->dashboard_model->add_booking_transaction($t_transaction);

                $amount = $this->input->post('t_amount');

                $cash = $this->unit_class_model->get_closing_stock($this->session->userdata('cd_key'));
                $c = $cash->closing_cash; //get clossing cash
                $cash_collection = $cash->cash_collection; //get collect cash
                $mode = $this->input->post('t_payment_mode');
                $st = $this->input->post('pay_status');
                if ($st != 'Cancel') {
                    if ($mode == 'cash' || $mode == 'Cash') {
                        $query1 = $this->unit_class_model->update_cash_collection($amount, $c, $cash_collection);
                    } else {
                        $cash = $this->unit_class_model->get_mode_cash($mode, $this->session->userdata('cd_key'));
                        foreach ($cash as $key => $value) {
                            $d = $value;
                        }; //get mode cash	
                        //$c+=$amount;
                        $d += $amount;
                        $query1 = $this->unit_class_model->update_cash_drawer_mode($d, $mode);
                    }
                }
                $booking_status = $this->input->post('t_booking_status_id');
                if ($booking_status == '2') {
                    $book = array(
                        'booking_status_id' => 4,

                    );

                    $query = $this->bookings_model->change_book_id($this->input->post('t_booking_id'), $book);
                }

                $data = array(
                    'a' => 1,

                );

                echo json_encode($data);
            }
        } else if ($type == 'gb') {


            $bookings_id = $this->input->post('t_group_id');
            $guest_name = $this->bookings_model->get_guest_gp($bookings_id);
            $name = $guest_name->name;
            $unit_id = $this->bookings_model->get_unitId1($bookings_id);

            foreach ($unit_id as $u_id) {
                $rm_No[] = $this->bookings_model->get_rmNo1($u_id->room_id);
            }
            $rm_count = count($rm_No);

            foreach ($rm_No as $value) {
                $str .= " " . $value->room_no;
            }
            $chk_lasltransaction = $this->dashboard_model->chk_lasltransaction();
            $chk_lasltransaction = $chk_lasltransaction + 1;
            $transaction_id = 'TA0' . $this->session->userdata('user_hotel') . "-" . $this->session->userdata('user_id') . "/" . date("d") . date("m") . date("y") . "/" . $chk_lasltransaction;
            $t_transaction = array(
                'actual_id' => $transaction_id,
                't_group_id' => $this->input->post('t_group_id'),
                // 't_booking_status' => $this->input->post('t_booking_status_id'),
                'details_id' => $this->input->post('t_group_id'),
                'details_type' => 'grp booking',
                't_amount' => $this->input->post('t_amount'),
                't_date' => $datetime,
                'p_center' => $this->input->post('p_center'),
                't_payment_mode' => $this->input->post('t_payment_mode'),
                't_card_type' => $this->input->post('t_card_type'),
                't_card_no' => $this->input->post('t_card_no'),
                't_card_name' => $this->input->post('t_card_name'),
                't_card_exp_m' => $this->input->post('t_card_exp_m'),
                't_card_exp_y' => $this->input->post('t_card_exp_y'),
                't_card_cvv' => $this->input->post('t_card_cvv'),
                'ft_account_no' => $this->input->post('ft_account_no'),
                'ft_ifsc_code' => $this->input->post('ft_ifsc_code'),
                'checkno' => $this->input->post('checkno'),
                't_drw_name' => $this->input->post('t_drw_name'),
                'draft_no' => $this->input->post('draft_no'),
                't_w_name' => $this->input->post('t_w_name'),
                't_tran_id' => $this->input->post('t_tran_id'),
                't_recv_acc' => $this->input->post('t_recv_acc'),
                't_bank_name' => $this->input->post('t_bank_name'),
                't_status' => $this->input->post('t_status'),
                't_cashDrawer_id' => $this->session->userdata('cd_key'),
                'transaction_from_id' => 2,
                'transaction_to_id' => 4,
                'user_id' => $this->session->userdata('user_id'),
                'hotel_id' => $this->session->userdata('user_hotel'),
                'transaction_type' => 'Booking Payment',
                'transaction_type_id' => 1,
                'transaction_releted_t_id' => 1,
                'transactions_detail_notes' => "( Booking Id: HM0" . $hotel_id . "GRP00" . $this->input->post('t_group_id') . " Room No: " . $str . " " . $name . " from - " . $this->input->post('start_date') . " to - " . $this->input->post('end_date') . " )"
            );

            //print_r($t_transaction);
            $groupID = $this->input->post('t_group_id');
            $paidAMT = $this->input->post('t_amount');
            $dueAMT = $this->input->post('due_amount');
            if ($paidAMT >= $dueAMT) {
                $this->bookings_model->updatePos($groupID);
            }
            //exit();
            if ($this->session->userdata('user_hotel') != '') {
                $query = $this->dashboard_model->add_booking_transaction($t_transaction);

                //update cash 
                $amount = $this->input->post('t_amount');
                $cash = $this->unit_class_model->get_closing_stock($this->session->userdata('cd_key'));
                $c = $cash->closing_cash; //get closeing cash
                $cash_collection = $cash->cash_collection; //get collect cash
                $mode = $this->input->post('t_payment_mode');
                if ($mode == 'cash' || $mode == 'Cash') {
                    $query1 = $this->unit_class_model->update_cash_collection($amount, $c, $cash_collection);
                } else {
                    $cash = $this->unit_class_model->get_mode_cash($mode, $this->session->userdata('cd_key'));
                    foreach ($cash as $key => $value) {
                        $d = $value;
                    }; //get mode cash	
                    //$c+=$amount;
                    $d += $amount;
                    $query1 = $this->unit_class_model->update_cash_drawer_mode($d, $mode);
                }

                $booking_status = $this->input->post('t_booking_status_id');
                if ($booking_status == '2') {
                    $booking_status_id = 4;
                } else {
                    $booking_status_id = $booking_status;
                }
                $query = $this->bookings_model->upGP($this->input->post('t_group_id'), $booking_status_id);

                $dataG = '1';
                header('Content-Type: application/json');
                echo json_encode($dataG);
            }
        }
    }




    function return_guest_search()
    {
        $guest_name = $this->input->post('guest');
        $guest = $this->bookings_model->get_guest($guest_name);

        $i = 0;
        foreach ($guest as $key) {
            # code...

            $visits = $this->bookings_model->no_visit($key['g_id']);
            $spent = $this->bookings_model->spent($key['g_id']);
            $amount_spent = $spent['room_rent_sum_total'];
            $guest[$i]['visits'] = $visits;
            $guest[$i]['amount_spent'] = $amount_spent;
            $i++;
        }

        //print_r($guest);     


        header('Content-Type: application/json');
        echo json_encode($guest);
    }
    function return_guest_get()
    {
        $guest_id = $this->input->post('guest_id');
        $guest = $this->bookings_model->get_guest_details($guest_id);
        //$booking= $this->unit_class_model->get_guest_by_booking_id($guest_id);
        // $a=$booking->booking_id;
        foreach ($guest as $row) {
            $g_id = $row->g_id;
            $g_name = $row->g_name;
            $g_address = /*$row->g_address." , ".$row->g_city." , ".*/ $row->g_pincode;
            $g_contact_no = $row->g_contact_no;
            $g_pincode = $row->g_pincode;
            $g_email = $row->g_email;
            $g_type = $row->g_type;
        }

        $data = array(
            'g_id' => $g_id,
            'g_name' => $g_name,
            'g_address'   => $g_address,
            'g_contact_no'    => $g_contact_no,
            'g_pincode'    => $g_pincode,
            'g_email' => $g_email,
            'g_type' => $g_type,
            //'booking_id' =>$a,
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function get_commision()
    {
        $b_id = $this->input->post('b_id');
        $broker = $this->bookings_model->get_broker_details($b_id);

        foreach ($broker as $row) {
            $b_name = $row->b_name;
            $b_id = $row->b_id;
            $broker_commission = $row->broker_commission;
        }

        $bookings_id = $this->input->post('booking_id');
        $booking = $this->bookings_model->get_booking_details($bookings_id);

        foreach ($booking as $row) {
            $room_rent_total_amount = $row->room_rent_total_amount;
        }



        $data = array(
            'b_name' => $b_name,
            'broker_commission' => round($broker_commission),
            'base_room_rent' => $room_rent_total_amount,
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }
    function get_commision_grp()
    {
        $b_id = $this->input->post('b_id');
        $broker = $this->bookings_model->get_broker_details($b_id);

        foreach ($broker as $row) {
            $b_name = $row->b_name;
            $b_id = $row->b_id;
            $broker_commission = $row->broker_commission;
        }

        //$bookings_id = $this->input->post('booking_id');
        // $booking = $this->bookings_model->get_booking_details($bookings_id);

        /* foreach($booking as $row)
        {
            $room_rent_total_amount = $row->room_rent_total_amount;
        }*/



        $data = array(
            'b_name' => $b_name,
            'broker_commission' => round($broker_commission),
            //'base_room_rent' => $room_rent_total_amount,
        );
        //print_r($data);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function get_commision2_grp()
    {
        $b_id = $this->input->post('b_id');
        $broker = $this->bookings_model->get_channel_details($b_id);

        foreach ($broker as $row) {
            $b_name = $row->channel_name;
            $b_id = $row->channel_id;
            $broker_commission = $row->channel_commission;
        }

        /*  $bookings_id = $this->input->post('booking_id');
          $booking = $this->bookings_model->get_booking_details($bookings_id);

        foreach($booking as $row)
        {
            $room_rent_total_amount = $row->room_rent_total_amount;
        }*/



        $data = array(
            'b_name' => $b_name,
            'broker_commission' => round($broker_commission),
            //'base_room_rent' => $room_rent_total_amount,
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function get_commision2()
    {
        $b_id = $this->input->post('b_id');
        $broker = $this->bookings_model->get_channel_details($b_id);

        foreach ($broker as $row) {
            $b_name = $row->channel_name;
            $b_id = $row->channel_id;
            $broker_commission = $row->channel_commission;
        }

        $bookings_id = $this->input->post('booking_id');
        $booking = $this->bookings_model->get_booking_details($bookings_id);

        foreach ($booking as $row) {
            $room_rent_total_amount = $row->room_rent_total_amount;
        }



        $data = array(
            'b_name' => $b_name,
            'broker_commission' => round($broker_commission),
            'base_room_rent' => $room_rent_total_amount,
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }


    function master_id_validate()
    {

        $times = $this->dashboard_model->checkin_time();

        $booking_id = $this->input->post('booking_id');
        //$booking_id=$_GET['b'];
        $data = $this->bookings_model->get_booking_details($booking_id);
        $valid = "<b style=\"color:red\" >Not Vaid</b>";
        $token_id = 0;
        $cust_end_date = "";
        $checkout_time = "";
        $g_name = "";
        $g_address = "";
        $g_number = "";

        if ($data && isset($data)) {
            foreach ($data as $token) {

                if ($token->booking_status_id != 1 && $token->booking_status_id != 6 && $token->booking_status_id_secondary != 3 && $token->booking_status_id_secondary != 9) {
                    $valid = "<b style=\"color:green\" >Vaid</b>";
                    $token_id = 1;



                    //modifier start

                    foreach ($times as $time) {
                        if ($time->hotel_check_out_time_fr == 'PM') {
                            $checkout_time = ($time->hotel_check_out_time_hr + 12) . ":" . $time->hotel_check_out_time_mm;
                        } else {
                            $checkout_time = ($time->hotel_check_out_time_hr) . ":" . $time->hotel_check_out_time_mm;
                        }
                    }

                    //modifier end

                    $cust_end_date = date("d-m-Y", strtotime($token->cust_end_date) + $checkout_time * 60 * 60);


                    $checkout_time = date("H:i", strtotime($token->cust_end_date) + $checkout_time * 60 * 60);

                    $g_details = $this->bookings_model->get_guest_details($token->guest_id);
                    foreach ($g_details as $guest) {
                        $g_name = $guest->g_name;
                        $g_address = $guest->g_address;
                        $g_number = $guest->g_contact_no;
                    }
                } else {
                    $valid = "<b style=\"color:red\" >Not Vaid</b>";
                    $token_id = 0;
                    $cust_end_date = "";
                    $checkout_time = "";
                    $g_name = "";
                    $g_address = "";
                    $g_number = "";
                }
            }
        }
        $data = array(
            'token' => $token_id,
            'valid' => $valid,
            'end_dt' => $cust_end_date,
            'end_time' => $checkout_time,
            'g_name' => $g_name,
            'g_address' => $g_address,
            'g_number' => $g_number,
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }


    function add_split_booking()
    {

        $from_date = $this->input->post("from_date");
        $end_date = $this->input->post("end_date");
        $booking_id = $this->input->post("booking_id");
        $rent = $this->input->post("rent");

        $room_id = $this->input->post("room_id");



        header('Content-Type: application/json');
        echo json_encode($this->input->post());
    }


    function folio_generate_old()
    {
        $booking_id = $this->input->post('booking_id');
        // $booking_id=$_GET["id"];

        //delete previous data

        $this->bookings_model->delete_invoice_previous($booking_id);


        $data['booking_details']  = $this->bookings_model->get_booking_details($booking_id);

        //if booking exists
        if ($data['booking_details']) {




            $data['payment_details']  = $this->bookings_model->get_payment_details($booking_id);
            $data['total_payment']  = $this->bookings_model->get_total_payment($booking_id);

            //percentage payment calculation
            $per_paid1 = 0;
            foreach ($data['total_payment'] as $kep) {
                $total_payment1 = $kep->t_amount;
            }
            foreach ($data['booking_details'] as $ket) {
                if ($ket->room_rent_sum_total == 0) {
                    $total = $ket->room_rent_total_amount;
                } else {

                    $total = $ket->room_rent_sum_total;
                }

                $per_paid1 = ($total_payment1 / $total) * 100;
                $per_paid1 = round($per_paid1, 2);
            }
            //percentage payment calculation end


            foreach ($data['booking_details'] as $key) {
                $data['group_id'] = $key->group_id;
            }


            foreach ($data['payment_details'] as $key) {

                $cust_name = $key->cust_name;


                $units = $this->dashboard_model->get_unit_exact($key->unit_id);

                foreach ($units as $keym) {
                    # code...
                    $unit_name = $units->unit_name;
                }





                $array1 = array(

                    'invoice_no' => "HMINVC" . $this->session->userdata('user_hotel') . "BOOK",

                    'raised_by_hotel' => "yes",
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'invoice_type' => "booking",
                    'cust_name' => $key->cust_name,
                    'date_generated' => date("Y-m-d"),
                    'is_cancelled' => "no",
                    'related_id' => $booking_id,


                );
                $master_id = $this->bookings_model->insert_invoice_master($array1, $booking_id);

                if ($key->room_rent_sum_total == "0") {
                    $booktot = $key->room_rent_total_amount;
                } else {
                    $booktot = $key->room_rent_sum_total;
                }

                $array2 = array(

                    'item_name' => "Room No: " . $key->room_no,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'item_related_id' => $key->room_id,
                    'item_description' => $unit_name . "(" . $key->room_no . ") from" . $key->cust_from_date_actual . "to" . $key->cust_end_date_actual,
                    'item_type' => "room",
                    'qty' => '1',
                    'cust_name' => $key->cust_name,
                    'percentage_paying' => "$per_paid1",
                    'total' => $key->room_rent_total_amount,
                    'lt' => $key->luxury_tax,
                    'st' => $key->service_tax,
                    'sc' => $key->service_charge,
                    'tax' => $key->room_rent_tax_amount,
                    'grand_total' => $booktot,
                    'master_id' => $master_id,

                );

                $line_id = $this->bookings_model->insert_invoice_line($array2, $booking_id);


                $service_string = $key->service_id;
                $service_total_price = $key->service_price;
                $charge_string = $key->booking_extra_charge_id;
                $charge_total_price = $key->booking_extra_charge_amount;
            }
        } else {
            $service_string = "";
        }

        //end of booking invoice

        $parent_invoice_id = $master_id;

        //service start
        if ($service_string != "") {

            $service_array = explode(",", $service_string);

            $array1 = array(

                'invoice_no' => "HMINVC" . $this->session->userdata('user_hotel') . "SERVC",
                'parent_invoice_id' => $master_id,

                'raised_by_hotel' => "yes",
                'hotel_id' => $this->session->userdata('user_hotel'),
                'invoice_type' => "service",
                'cust_name' => $cust_name,
                'date_generated' => date("Y-m-d"),
                'is_cancelled' => "no",
                'related_id' => $booking_id,


            );
            $master_id_service = $this->bookings_model->insert_invoice_master($array1, $booking_id);




            for ($i = 1; $i < sizeof($service_array); $i++) {

                $services = $this->dashboard_model->get_service_details($service_array[$i]);

                //insertion start


                $array2 = array(

                    'item_name' => $services->s_name,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'item_related_id' => $services->s_id,
                    'item_description' => $services->s_description,
                    'item_type' => "service",
                    'qty' => '1',
                    'cust_name' => $cust_name,
                    // 'percentage_paying' => "$per_paid1",
                    'total' => $services->s_price,
                    //  'lt' => $key->luxury_tax,
                    // 'st' => $key->service_tax,
                    // 'sc' => $key->service_charge,
                    'tax' => $services->s_tax,
                    'grand_total' => ($services->s_price + $services->s_tax),
                    'master_id' => $master_id_service

                );

                $line_id_service = $this->bookings_model->insert_invoice_line($array2, $booking_id);


                //inserrtion end
            }
        }
        //service end


        //charge start
        if ($charge_string != "") {
            $charge_array = explode(",", $charge_string);

            $array1 = array(

                'invoice_no' => "HMINVC" . $this->session->userdata('user_hotel') . "CHRG",
                'parent_invoice_id' => $parent_invoice_id,

                'raised_by_hotel' => "yes",
                'hotel_id' => $this->session->userdata('user_hotel'),
                'invoice_type' => "charge",
                'cust_name' => $cust_name,
                'date_generated' => date("Y-m-d"),
                'is_cancelled' => "no",
                'related_id' => $booking_id,


            );
            $master_id_charge = $this->bookings_model->insert_invoice_master($array1, $booking_id);


            for ($i = 1; $i < sizeof($charge_array); $i++) {

                $charges = $this->dashboard_model->get_charge_details($charge_array[$i]);

                $array2 = array(

                    'item_name' => $charges->crg_description,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'item_related_id' => $charges->crg_id,
                    'item_description' => $charges->crg_description,
                    'item_type' => "charge",
                    'qty' => $charges->crg_quantity,
                    'cust_name' => $cust_name,
                    // 'percentage_paying' => "$per_paid1",
                    'total' => $charges->crg_unit_price * $charges->crg_quantity,
                    //  'lt' => $key->luxury_tax,
                    // 'st' => $key->service_tax,
                    // 'sc' => $key->service_charge,
                    'tax' => $charges->crg_tax,
                    'grand_total' => $charges->crg_total,
                    'master_id' => $master_id_charge

                );

                $line_id_charge = $this->bookings_model->insert_invoice_line($array2, $booking_id);
            }
        }
        //charge_end

        $pos  = $this->dashboard_model->all_pos_booking($booking_id);

        //pos  start

        if ($pos && isset($pos)) {


            $array1 = array(

                'invoice_no' => "HMINVC" . $this->session->userdata('user_hotel') . "POS",
                'parent_invoice_id' => $parent_invoice_id,

                'raised_by_hotel' => "yes",
                'hotel_id' => $this->session->userdata('user_hotel'),
                'invoice_type' => "pos",
                'cust_name' => $cust_name,
                'date_generated' => date("Y-m-d"),
                'is_cancelled' => "no",
                'related_id' => $booking_id,


            );
            $master_id_pos = $this->bookings_model->insert_invoice_master($array1, $booking_id);

            foreach ($pos as $key) {

                $description = "";

                $items = $this->dashboard_model->all_pos_items($key->pos_id);

                if ($items) {
                    foreach ($items as $item) {


                        $description = $description . " " . $item->item_name . "(" . $item->item_quantity . ") ";
                    }
                }

                $array2 = array(

                    'item_name' => $key->invoice_number,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'item_related_id' => $key->pos_id,
                    'item_description' => $description,
                    'item_type' => "pos",
                    'qty' => 1,
                    'cust_name' => $cust_name,
                    // 'percentage_paying' => "$per_paid1",
                    'total' => $key->bill_amount,
                    // 'lt' => $key->luxury_tax,
                    'st' => $key->st,
                    'sc' => $key->sc,
                    'fvat' => $key->fvat,
                    'lvat' => $key->lvat,
                    'tax' => $key->tax,
                    'grand_total' => $key->total_due,
                    'master_id' => $master_id_pos

                );

                $line_id_pos = $this->bookings_model->insert_invoice_line($array2, $booking_id);
            }
        }

        //pos end

        header('Content-Type: application/json');
        echo json_encode("success");
    }

    // _sb dt27_10_16 
    function folio_generate()
    {
        date_default_timezone_set('Asia/Kolkata');
        $booking_id = $this->input->post('booking_id');
        //$booking_id=1094;

        $data = $this->bookings_model->get_booking_details_row($booking_id);
        //print_r($data);exit;
        //if booking exists
        if ($data) {

            //foreach ($data as $key) {

            $array2 = array(
                'invoice_no' => "BKINV0" . $data->hotel_id . '/' . date("d") . date("m") . date("y") . '/',
                'raised_by_hotel' => "yes",
                'hotel_id' => $data->hotel_id,
                'user_id' => $this->session->userdata('user_id'),
                'invoice_type' => "booking",
                'cust_name' => $data->guest_id,
                'date_generated' => date("Y-m-d h:i:s"),
                'is_cancelled' => "no",
                'related_id' => $booking_id,
            );

            //}
            //print_r($array2);exit;

        } else // IF Booking Does not exist
        {
        }

        //end of booking invoice



        $invoice_id = $this->bookings_model->insert_invoice_master($array2, $booking_id);
        $arr = array('invoice_id' => $invoice_id);

        header('Content-Type: application/json');
        echo json_encode($arr);
    }   // End of Group Invoice Generate


    // _sb dt27_10_16 
    function folio_generate_grp()
    {
        date_default_timezone_set('Asia/Kolkata');
        $booking_id = $this->input->post('booking_id');
        //$booking_id=86;
        //delete previous data
        //$this->bookings_model->delete_invoice_previous($booking_id);                
        $data = $this->bookings_model->getDetails($booking_id);
        //print_r($data);exit;
        //if booking exists
        if ($data) {

            // foreach ($data as $key) {

            $array2 = array(
                'invoice_no' => "BKINV0" . $data->hotel_id . '/' . date("d") . date("m") . date("y") . '/',
                'raised_by_hotel' => "yes",
                'hotel_id' => $data->hotel_id,
                'invoice_type' => "grp_booking",
                'cust_name' => $data->name,
                'date_generated' => date("Y-m-d h:i:s"),
                'is_cancelled' => "no",
                'related_id' => $booking_id,
            );

            //}
            //print_r($array2);exit;

        } else // IF Booking Does not exist
        {
        }

        //end of booking invoice

        $invoice_id = $this->bookings_model->insert_invoice_master_grp($array2, $booking_id);
        $arr = array('invoice_id' => $invoice_id);
        header('Content-Type: application/json');
        echo json_encode($arr);
    }   // End of Invoice Generate



    /*	
    function hotel_booking_move(){   
        $hotel_id = $this->session->userdata('user_hotel');
        $booking_id = $_GET['id'];
        $newStart = $_GET['newStart'];
		$start = $_GET['newStart'];
        $newEnd = $_GET['newEnd'];
		$newResource = $_GET['newResource'];
		
		$grp_move = 0;
				$occupancyfail = 0;
		$newdate = date('Y-m-d',strtotime($start));
		
		
		$rooms= $this->bookings_model->hotelWiseRoomDetails($hotel_id,$newResource);
		
		$group=$this->dashboard_model->get_group_id($booking_id);
        //print_r($group);
        $group_id=$group->group_id;
        
		
       // $details=$this->bookings_model->get_booking_details_row($booking_id);  this is previously written by someone....
		$details = $this->bookings_model->get_booking_details_row($booking_id);
	
		$no_of_guest = 		$details->no_of_guest;
		$booking_source = 	$details->booking_source;
		$cust_from_date_actual = $details->cust_from_date_actual;
		$food_plans =		$details->food_plans;
		$room_id = 			$details->room_id;
		$extra_adult =      $details->ex_ad_no;
		$extra_child =      $details->ex_ch_no;
		$sameroom= 0;
		//echo "NS".$newStart."cst".$cust_from_date_actual."new".$newdate;
		//exit;
		if($newdate == $cust_from_date_actual && $room_id == $newResource){
			
		$sameroom='1';
		$occu =1;
		}
		
		else if($group_id > 0){
			
			$grp_move = '1';	
		
		}
		// start of new else ..
		else{
			
		$currentbookedRoom =  $this->bookings_model->hotelBookingsDetails($hotel_id,$booking_id);
		 if($details->charge_name =='b_room_charge')
				  {
					$room_rent_type ='Base';
				  }
				  elseif ($f->charge_name == 's_room_charge')
				  {
					  $room_rent_type = "Seasonal";
				  }
				  elseif ($bookingDetails->charge_name == 'w_room_charge')
				  {
					  $room_rent_type = "Weekend";
				  }
				  else
				  {
					  $room_rent_type = "Weekend";
				  }
		
		$booking_status_id = $details->booking_status_id;
		
		$unitidNB = $this->rate_plan_model->get_unit_type_id($room_id);
		if(isset($unitidNB) && $unitidNB){
			$unitID =  $unitidNB->unit_id;
		}
		
		$occupancy_details = $this->dashboard_model->getUnit_details($unitID);
		
		$defaultoccu = $occupancy_details->default_occupancy;
		$maxoccu =  $occupancy_details->max_occupancy;
		$occu = '';
		
		if($no_of_guest > 0){
		if( $maxoccu < $no_of_guest){
			$occupancyfail = '1';
		}
		else if($defaultoccu < $no_of_guest){
			$occu = $defaultoccu;
		}	
		else{
			
			if($defaultoccu >= $no_of_guest && $no_of_guest == 1) {
				$occu = 1;
			}
			else if($defaultoccu >= $no_of_guest && $no_of_guest == 2) {
				$occu = 2;
			}
			else if($defaultoccu >= $no_of_guest && $no_of_guest == 3) {
				$occu = 3;
			}
			else if($defaultoccu >= $no_of_guest && $no_of_guest > 3) {
					$occu = 4;
			}
			else if($defaultoccu < $no_of_guest && $no_of_guest <= 3){
				$occu = $defaultoccu;
			}
			else {				
				$occu = 4;
			}
		}
		
		
		}
		
		
	
		$season_name=$this->rate_plan_model->get_season($cust_from_date_actual);
		$season_id=$this->rate_plan_model->get_season_id($season_name->season_name);
		
		
		$rate_plan_id = $season_id->rate_plan_type_id;
	
		$query=$this->rate_plan_model->get_rmCh($food_plans,$booking_source,$rate_plan_id,$unitID,$occu);
		$query1=$this->rate_plan_model->get_exACh($food_plans,$booking_source,$rate_plan_id,$unitID);
	
		$query2=$this->rate_plan_model->get_exCCh($food_plans,$booking_source,$rate_plan_id,$unitID);
		
	
		
		if($room_rent_type == 'Weekend'){
		
	$data=array(
	//Weekend
		'room_charge'=>$query->w_room_charge,
		'room_charge_tax'=>$query->w_room_charge_tax,
		'meal_charge'=>$query->w_meal_charge,
		'meal_charge_tax'=>$query->w_meal_charge_tax,
		'room_charge_ea'=>$query1->w_room_charge,
		'room_charge_tax_ea'=>$query1->w_room_charge_tax,
		'meal_charge_ea'=>$query1->w_meal_charge,
		'meal_charge_tax_ea'=>$query1->w_meal_charge_tax,
		'room_charge_ec'=>$query2->w_room_charge,
		'room_charge_tax_ec'=>$query2->w_room_charge_tax,
		'meal_charge_ec'=>$query2->w_meal_charge,
		'meal_charge_tax_ec'=>$query2->w_meal_charge_tax
		);
	}
	else if($room_rent_type == 'Seasonal'){
	$data=array(
	//Seasonal
		'room_charge'=>$query->s_room_charge,
		'room_charge_tax'=>$query->s_room_charge_tax,
		'meal_charge'=>$query->s_meal_charge,
		'meal_charge_tax'=>$query->s_meal_charge_tax,
		'room_charge_ea'=>$query1->s_room_charge,
		'room_charge_tax_ea'=>$query1->s_room_charge_tax,
		'meal_charge_ea'=>$query1->s_meal_charge,
		'meal_charge_tax_ea'=>$query1->s_meal_charge_tax,
		'room_charge_ec'=>$query2->s_room_charge,
		'room_charge_tax_ec'=>$query2->s_room_charge_tax,
		'meal_charge_ec'=>$query2->s_meal_charge,
		'meal_charge_tax_ec'=>$query2->s_meal_charge_tax
		);
	}
	else{
		
	$data=array(	
	//Base
		//'room_charge'=>"hello",	
		'room_charge'=>$query->b_room_charge,	
		'room_charge_tax'=>$query->b_room_charge_tax,
		'meal_charge'=>$query->b_meal_charge,
		'meal_charge_tax'=>$query->b_meal_charge_tax,
		'room_charge_ea'=>$query1->b_room_charge,
		'room_charge_tax_ea'=>$query1->b_room_charge_tax,
		'meal_charge_ea'=>$query1->b_meal_charge,
		'meal_charge_tax_ea'=>$query1->b_meal_charge_tax,
		'room_charge_ec'=>$query2->b_room_charge,
		'room_charge_tax_ec'=>$query2->b_room_charge_tax,
		'meal_charge_ec'=>$query2->b_meal_charge,
		'meal_charge_tax_ec'=>$query2->b_meal_charge_tax
		);
	}
		
		
		if($extra_adult > 0 && $occu < $no_of_guest){
			
			$queryextra=$this->rate_plan_model->get_rmCh($food_plans,$booking_source,$rate_plan_id,$unitID,5);
			if($room_rent_type == 'Weekend'){
			
			$data['extra_person_room_rent'] = $data['room_charge_ea'];
			$data['extra_person_rate_price'] = $data['meal_charge_ea'];
			$data['room_charge_tax_exp'] = $data['room_charge_tax_ea'];
			$data['meal_charge_tax_exp'] = $data['meal_charge_tax_ea'];
		}
		else if($room_rent_type == 'Seasonal'){
			
			$data['extra_person_room_rent'] = $data['room_charge_ea'];
			$data['extra_person_rate_price'] = $data['meal_charge_ea'];
			$data['room_charge_tax_exp'] = $data['room_charge_tax_ea'];
			$data['meal_charge_tax_exp'] = $data['meal_charge_tax_ea'];
		}
		else{
			
			$data['extra_person_room_rent'] = $data['room_charge_ea'];
			$data['extra_person_rate_price'] = $data['meal_charge_ea'];
			$data['room_charge_tax_exp'] = $data['room_charge_tax_ea'];
			$data['meal_charge_tax_exp'] = $data['meal_charge_tax_ea'];
		}
		}
		else if($extra_child > 0 && $defaultoccu < $no_of_guest){
			
			$queryextra=$this->rate_plan_model->get_rmCh($food_plans,$booking_source,$rate_plan_id,$unitID,6);
			if($room_rent_type == 'Weekend'){
			
			$data['extra_person_room_rent'] = $data['room_charge_ec'];
			$data['extra_person_rate_price'] = $data['meal_charge_ec'];
			$data['room_charge_tax_exp'] = $data['room_charge_tax_ec'];
			$data['meal_charge_tax_exp'] = $data['meal_charge_tax_ec'];
			
		}
		else if($room_rent_type == 'Seasonal'){
			
			$data['extra_person_room_rent'] = $data['room_charge_ec'];
			$data['extra_person_rate_price'] = $data['meal_charge_ec'];
			$data['room_charge_tax_exp'] = $data['room_charge_tax_ec'];
			$data['meal_charge_tax_exp'] = $data['meal_charge_tax_ec'];
		}
		else{
			
			$data['extra_person_room_rent'] = $data['room_charge_ec'];
			$data['extra_person_rate_price'] = $data['meal_charge_ec'];
			$data['room_charge_tax_exp'] = $data['room_charge_tax_ec'];
			$data['meal_charge_tax_exp'] = $data['meal_charge_tax_ec'];
		}
		}
		else{
			
			$data['extra_person_room_rent'] = 0;
			$data['extra_person_rate_price'] = 0;
			$data['room_charge_tax_exp'] =  0;
			$data['meal_charge_tax_exp'] = 0;
		}
		
		$price = $data['room_charge'] + $data['extra_person_room_rent'];
		$price1 = $data['meal_charge'] + $data['extra_person_rate_price'];
		
		$args = 'a';
		
		$lineitemdetails = $this->bookings_model->booking_line_item_details($booking_id);
		$tax = array();
		$tax1 = array();
		if($lineitemdetails->rr_total_tax > 0){
			
			$times = $this->dashboard_model->getTaxElements($price,$cust_from_date_actual,'Booking',$args);
		
				  if($times!=0){
					$i = 0;
					$tot = 0;
					$tax['r_id'] = $times[0]->r_id;
					$tax['planName'] = $times[0]->r_name; 		// Assumed, plan name will be same for all returned rows
					if($args == 'a'){
						
						
						foreach($times as $res){
							if($times[$i]->calculated_on == 'n_r'){
								$taxAmt = ($times[$i]->tax_mode == 'p' ? ($times[$i]->tax_value * $price / 100) : ($times[$i]->tax_value));
								$tax[$times[$i]->tax_name] = $taxAmt;			
							}
							else if($times[$i]->calculated_on == 'n_p' && ($i > 0)){
								$taxAmt = ($times[$i]->tax_mode == 'p' ? ($times[$i]->tax_value * ($price + $tot) / 100) : ($times[$i]->tax_value));
								$data[$times[$i]->tax_name] = $taxAmt;
							}
							
							$tot = $tot + $taxAmt;	
							$i++;
						} // End foreach
					}
						else
							$tax = $times;
					 }
					 else{
						$tot = 0.00;
					 }
					 
		$data['total']=$tot;
		$tax['total']=$tot; 		//add total tax in array
		
		}
		else{
			
			$data['total']=0.00;
			
		}
		

	$data['rr_tax_response'] = $tax;
	
	if($lineitemdetails->mp_total_tax > 0){
			
			//echo "price1".$price1;exit;
			$times1 = $this->dashboard_model->getTaxElements($price1,$cust_from_date_actual,'Meal Plan',$args);
			
				
				  if($times1!=0){
					$i1 = 0;
					$tot1 = 0;
					$tax1['r_id'] = $times1[0]->r_id;
					$tax1['planName'] = $times1[0]->r_name; 		// Assumed, plan name will be same for all returned rows
					if($args == 'a'){
						
						
						foreach($times1 as $res){
							if($times1[$i1]->calculated_on == 'n_r'){
								$taxAmt1 = ($times1[$i1]->tax_mode == 'p' ? ($times1[$i1]->tax_value * $price1 / 100) : ($times1[$i1]->tax_value));
								$tax1[$times1[$i1]->tax_name] = $taxAmt1;			
							}
							else if($times1[$i1]->calculated_on == 'n_p' && ($i1 > 0)){
								$taxAmt1 = ($times1[$i1]->tax_mode == 'p' ? ($times1[$i1]->tax_value * ($price1 + $tot1) / 100) : ($times1[$i1]->tax_value));
								$data[$times1[$i1]->tax_name] = $taxAmt1;
								$tax1[$times1[$i1]->tax_name] = $taxAmt1;
							}
							
							
							$tot1 = $tot1 + $taxAmt1;	
							$i1++;
						} // End foreach
					}
						else
							$tax1 = $times1;
					 }
					 else{
						$tot1 = 0.00;
					 }
					 
		$data['total']=$tot1;
		$tax1['total']=$tot1; 		//add total tax in array
		
		//header('Content-Type: application/json');
       // $data['rr_tax_response'] = json_encode($tax);
		
		}
		else{
			
			$data['total']=0.00;
			
		}
	
	   $data['mp_tax_response'] = $tax1;
	   
	  /* 	echo "<pre>";
		print_r($data);
		echo "</pre>";
		exit;
		//checking start
		date_default_timezone_set('Asia/Kolkata');
		$current_date=date("Y-m-d H:i:s");
	    if($details->booking_status_id=="5"){
			$start=$details->cust_from_date;
	    }else if($details->booking_status_id=="6"){
		    $checkout=1;
	    }
		
		$data['newStart']=$newStart;
		$data['newEnd']=$newEnd;
		$data['newResource']=$newResource;		
        $data['id']=$booking_id;
        $data['group_id'] =$group_id;
		$data['rooms'] = $rooms;
        $data['details'] =$details;
		$data['events'] = $this->dashboard_model->all_events_limit();
		$data['currentbookedRoom']  = $currentbookedRoom;
		
	}// end of new else..
		
		if($details->booking_status_id=="5" && strtotime($newStart)!= strtotime($start)){   // This is previously written 
		
		    $dataerror['checkout']=1;
	
			$this->load->view("backend/dashboard/hotel_move_error_msg",$dataerror);
		}
		else if($details->booking_status_id=="6"){
			
			  $dataerror['checkout']=1;
	
			$this->load->view("backend/dashboard/hotel_move_error_msg",$dataerror);
			
		}
		else if($occupancyfail == '1'){
			
		$dataerror['occupancyfailed']=1;
	//	$this->hotel_move_error_message($dataerror);
	$this->load->view("backend/dashboard/hotel_move_error_msg",$dataerror);
		}
		else if($sameroom == '1'){
			
		$dataerror['sameroom']=1;
		$this->load->view("backend/dashboard/hotel_move_error_msg",$dataerror);
		}
		
		else if($grp_move == '1'){
			
			  $dataerror['grp_move']=1;
	
			$this->load->view("backend/dashboard/hotel_move_error_msg",$dataerror);
			
		}
		else{	
			
		/* echo "<pre>";
			print_r($data);
			exit;
		
             $this->load->view("backend/dashboard/hotel_booking_move",$data);
		}
		
		
    }
    */

    function hotel_booking_move()
    {
        $hotel_id = $this->session->userdata('user_hotel');
        $booking_id = $_GET['id'];
        $newStart = $_GET['newStart'];
        $start = $_GET['newStart'];
        //  $newEnd = $_GET['newEnd'];
        $newResource = $_GET['newResource'];

        $grp_move = 0;
        $occupancyfail = 0;
        $newdate = date('Y-m-d', strtotime($start));


        $rooms = $this->bookings_model->hotelWiseRoomDetails($hotel_id, $newResource);

        $group = $this->dashboard_model->get_group_id($booking_id);
        //print_r($group);
        $group_id = $group->group_id;


        // $details=$this->bookings_model->get_booking_details_row($booking_id);  this is previously written by someone....

        $details = $this->bookings_model->get_booking_details_row($booking_id);

        $no_of_guest =         $details->no_of_guest;
        $booking_source =     $details->booking_source;
        $cust_from_date_actual = $details->cust_from_date_actual;
        $food_plans =        $details->food_plans;
        $room_id =             $details->room_id;
        $extra_adult =      $details->ex_ad_no;
        $extra_child =      $details->ex_ch_no;
        $room_price_modifier =    $details->mod_room_rent;
        $meal_price_modifier =    $details->food_plan_mod_rent;
        $room_plan_mod_type =    $details->rent_mode_type;
        $food_plan_mod_type =    $details->food_plan_mod_type;
        $stay_days =  $details->stay_days;

        $stay_days_final = $stay_days . " " . "days";

        //	echo $stay_days_final;
        //exit;
        $newEnd = date_create($newStart);
        date_add($newEnd, date_interval_create_from_date_string($stay_days_final));
        $newEnd = date_format($newEnd, "Y-m-d");

        $g_state_details = $this->bookings_model->get_guest_state_details($details->guest_id);
        if (isset($g_state_details) && $g_state_details != '') {

            $g_state = $g_state_details->g_state;
        }

        /*	if(isset($meal_price_modifier) && $meal_price_modifier){
			
			if(isset($food_plan_mod_type) && $food_plan_mod_type=='add'){
				
				$meal_plan_chrg_line = $meal_price + $meal_price_modifier;
			}
			else{
				
				$meal_plan_chrg_line = $meal_price - $meal_price_modifier;
			}
			
		}
		if(isset($room_price_modifier) && $room_price_modifier){
			
			if(isset($room_plan_mod_type) && $room_plan_mod_type=='add'){
				
				$rent_plan_chrg_line = $meal_price + $meal_price_modifier;
			}
			else{
				
				$meal_plan_chrg_line = $meal_price - $meal_price_modifier;
			}
			
		}
		
		*/


        $sameroom = 0;
        //echo "NS".$newStart."cst".$cust_from_date_actual."new".$newdate;
        //exit;
        if ($newdate == $cust_from_date_actual && $room_id == $newResource) {

            $sameroom = '1';
            $occu = 1;
        } else if ($group_id > 0) {

            $grp_move = '1';
        }
        // start of new else ..
        else {

            $currentbookedRoom =  $this->bookings_model->hotelBookingsDetails($hotel_id, $booking_id);
            if ($details->charge_name == 'b_room_charge') {
                $room_rent_type = 'Base';
            } elseif ($f->charge_name == 's_room_charge') {
                $room_rent_type = "Seasonal";
            } elseif ($bookingDetails->charge_name == 'w_room_charge') {
                $room_rent_type = "Weekend";
            } else {
                $room_rent_type = "Weekend";
            }

            $booking_status_id = $details->booking_status_id;

            $unitidNB = $this->rate_plan_model->get_unit_type_id($room_id);
            if (isset($unitidNB) && $unitidNB) {
                $unitID =  $unitidNB->unit_id;
            }

            $occupancy_details = $this->dashboard_model->getUnit_details($unitID);

            $defaultoccu = $occupancy_details->default_occupancy;
            $maxoccu =  $occupancy_details->max_occupancy;
            $occu = '';

            if ($no_of_guest > 0) {
                if ($maxoccu < $no_of_guest) {
                    $occupancyfail = '1';
                } else if ($defaultoccu < $no_of_guest) {
                    $occu = $defaultoccu;
                } else {

                    if ($defaultoccu >= $no_of_guest && $no_of_guest == 1) {
                        $occu = 1;
                    } else if ($defaultoccu >= $no_of_guest && $no_of_guest == 2) {
                        $occu = 2;
                    } else if ($defaultoccu >= $no_of_guest && $no_of_guest == 3) {
                        $occu = 3;
                    } else if ($defaultoccu >= $no_of_guest && $no_of_guest > 3) {
                        $occu = 4;
                    } else if ($defaultoccu < $no_of_guest && $no_of_guest <= 3) {
                        $occu = $defaultoccu;
                    } else {
                        $occu = 4;
                    }
                }
            }



            $season_name = $this->rate_plan_model->get_season($cust_from_date_actual);
            $season_id = $this->rate_plan_model->get_season_id($season_name->season_name);


            $rate_plan_id = $season_id->rate_plan_type_id;

            $query = $this->rate_plan_model->get_rmCh($food_plans, $booking_source, $rate_plan_id, $unitID, $occu);
            $query1 = $this->rate_plan_model->get_exACh($food_plans, $booking_source, $rate_plan_id, $unitID);

            $query2 = $this->rate_plan_model->get_exCCh($food_plans, $booking_source, $rate_plan_id, $unitID);



            if ($room_rent_type == 'Weekend') {

                $data = array(
                    //Weekend
                    'room_charge' => $query->w_room_charge,
                    'room_charge_tax' => $query->w_room_charge_tax,
                    'meal_charge' => $query->w_meal_charge,
                    'meal_charge_tax' => $query->w_meal_charge_tax,
                    'room_charge_ea' => $query1->w_room_charge,
                    'room_charge_tax_ea' => $query1->w_room_charge_tax,
                    'meal_charge_ea' => $query1->w_meal_charge,
                    'meal_charge_tax_ea' => $query1->w_meal_charge_tax,
                    'room_charge_ec' => $query2->w_room_charge,
                    'room_charge_tax_ec' => $query2->w_room_charge_tax,
                    'meal_charge_ec' => $query2->w_meal_charge,
                    'meal_charge_tax_ec' => $query2->w_meal_charge_tax
                );
            } else if ($room_rent_type == 'Seasonal') {
                $data = array(
                    //Seasonal
                    'room_charge' => $query->s_room_charge,
                    'room_charge_tax' => $query->s_room_charge_tax,
                    'meal_charge' => $query->s_meal_charge,
                    'meal_charge_tax' => $query->s_meal_charge_tax,
                    'room_charge_ea' => $query1->s_room_charge,
                    'room_charge_tax_ea' => $query1->s_room_charge_tax,
                    'meal_charge_ea' => $query1->s_meal_charge,
                    'meal_charge_tax_ea' => $query1->s_meal_charge_tax,
                    'room_charge_ec' => $query2->s_room_charge,
                    'room_charge_tax_ec' => $query2->s_room_charge_tax,
                    'meal_charge_ec' => $query2->s_meal_charge,
                    'meal_charge_tax_ec' => $query2->s_meal_charge_tax
                );
            } else {

                $data = array(
                    //Base
                    //'room_charge'=>"hello",	
                    'room_charge' => $query->b_room_charge,
                    'room_charge_tax' => $query->b_room_charge_tax,
                    'meal_charge' => $query->b_meal_charge,
                    'meal_charge_tax' => $query->b_meal_charge_tax,
                    'room_charge_ea' => $query1->b_room_charge,
                    'room_charge_tax_ea' => $query1->b_room_charge_tax,
                    'meal_charge_ea' => $query1->b_meal_charge,
                    'meal_charge_tax_ea' => $query1->b_meal_charge_tax,
                    'room_charge_ec' => $query2->b_room_charge,
                    'room_charge_tax_ec' => $query2->b_room_charge_tax,
                    'meal_charge_ec' => $query2->b_meal_charge,
                    'meal_charge_tax_ec' => $query2->b_meal_charge_tax
                );
            }


            if ($extra_adult > 0 && $occu < $no_of_guest) {

                $queryextra = $this->rate_plan_model->get_rmCh($food_plans, $booking_source, $rate_plan_id, $unitID, 5);
                if ($room_rent_type == 'Weekend') {

                    $data['extra_person_room_rent'] = $data['room_charge_ea'];
                    $data['extra_person_rate_price'] = $data['meal_charge_ea'];
                    $data['room_charge_tax_exp'] = $data['room_charge_tax_ea'];
                    $data['meal_charge_tax_exp'] = $data['meal_charge_tax_ea'];
                } else if ($room_rent_type == 'Seasonal') {

                    $data['extra_person_room_rent'] = $data['room_charge_ea'];
                    $data['extra_person_rate_price'] = $data['meal_charge_ea'];
                    $data['room_charge_tax_exp'] = $data['room_charge_tax_ea'];
                    $data['meal_charge_tax_exp'] = $data['meal_charge_tax_ea'];
                } else {

                    $data['extra_person_room_rent'] = $data['room_charge_ea'];
                    $data['extra_person_rate_price'] = $data['meal_charge_ea'];
                    $data['room_charge_tax_exp'] = $data['room_charge_tax_ea'];
                    $data['meal_charge_tax_exp'] = $data['meal_charge_tax_ea'];
                }
            } else if ($extra_child > 0 && $defaultoccu < $no_of_guest) {

                $queryextra = $this->rate_plan_model->get_rmCh($food_plans, $booking_source, $rate_plan_id, $unitID, 6);
                if ($room_rent_type == 'Weekend') {

                    $data['extra_person_room_rent'] = $data['room_charge_ec'];
                    $data['extra_person_rate_price'] = $data['meal_charge_ec'];
                    $data['room_charge_tax_exp'] = $data['room_charge_tax_ec'];
                    $data['meal_charge_tax_exp'] = $data['meal_charge_tax_ec'];
                } else if ($room_rent_type == 'Seasonal') {

                    $data['extra_person_room_rent'] = $data['room_charge_ec'];
                    $data['extra_person_rate_price'] = $data['meal_charge_ec'];
                    $data['room_charge_tax_exp'] = $data['room_charge_tax_ec'];
                    $data['meal_charge_tax_exp'] = $data['meal_charge_tax_ec'];
                } else {

                    $data['extra_person_room_rent'] = $data['room_charge_ec'];
                    $data['extra_person_rate_price'] = $data['meal_charge_ec'];
                    $data['room_charge_tax_exp'] = $data['room_charge_tax_ec'];
                    $data['meal_charge_tax_exp'] = $data['meal_charge_tax_ec'];
                }
            } else {

                $data['extra_person_room_rent'] = 0;
                $data['extra_person_rate_price'] = 0;
                $data['room_charge_tax_exp'] =  0;
                $data['meal_charge_tax_exp'] = 0;
            }


            //	$price1 = $data['meal_charge'] + $data['extra_person_rate_price'];
            if (isset($meal_price_modifier) && $meal_price_modifier != '') {

                if (isset($food_plan_mod_type) && $food_plan_mod_type == 'add') {

                    //$meal_plan_chrg_line = $meal_price + $meal_price_modifier;
                    $price1 = $data['meal_charge'] + $data['extra_person_rate_price'] + $meal_price_modifier;
                } else {

                    $price1 = $data['meal_charge'] + $data['extra_person_rate_price'] - $meal_price_modifier;
                }
            } else {

                $price1 = $data['meal_charge'] + $data['extra_person_rate_price'];
            }

            if (isset($room_price_modifier) && $room_price_modifier != '') {

                if (isset($room_plan_mod_type) && $room_plan_mod_type == 'add') {

                    //$rent_plan_chrg_line = $meal_price + $meal_price_modifier;
                    $price = $data['room_charge'] + $data['extra_person_room_rent'] + $room_price_modifier;
                } else {

                    $price = $data['room_charge'] + $data['extra_person_room_rent'] - $room_price_modifier;
                }
            } else {

                $price = $data['room_charge'] + $data['extra_person_room_rent'];
            }


            $args = 'a';

            $lineitemdetails = $this->bookings_model->booking_line_item_details($booking_id);
            $tax = array();
            $tax1 = array();
            if ($lineitemdetails->rr_total_tax > 0) {

                $times = $this->dashboard_model->getTaxElements($price, $cust_from_date_actual, 'Booking', $args, $g_state);

                if ($times != 0) {
                    $i = 0;
                    $tot = 0;
                    $tax['r_id'] = $times[0]->r_id;
                    $tax['planName'] = $times[0]->r_name;         // Assumed, plan name will be same for all returned rows
                    if ($args == 'a') {


                        foreach ($times as $res) {
                            if ($times[$i]->calculated_on == 'n_r') {
                                $taxAmt = ($times[$i]->tax_mode == 'p' ? ($times[$i]->tax_value * $price / 100) : ($times[$i]->tax_value));
                                $tax[$times[$i]->tax_name] = $taxAmt;
                            } else if ($times[$i]->calculated_on == 'n_p' && ($i > 0)) {
                                $taxAmt = ($times[$i]->tax_mode == 'p' ? ($times[$i]->tax_value * ($price + $tot) / 100) : ($times[$i]->tax_value));
                                $data[$times[$i]->tax_name] = $taxAmt;
                            }

                            $tot = $tot + $taxAmt;
                            $i++;
                        } // End foreach
                    } else
                        $tax = $times;
                } else {
                    $tot = 0.00;
                }

                $data['total'] = $tot;
                $tax['total'] = $tot;         //add total tax in array

            } else {

                $data['total'] = 0.00;
            }


            $data['rr_tax_response'] = $tax;

            if ($lineitemdetails->mp_total_tax > 0) {

                //echo "price1".$price1;exit;
                $times1 = $this->dashboard_model->getTaxElements($price1, $cust_from_date_actual, 'Meal Plan', $args, $g_state);


                if ($times1 != 0) {
                    $i1 = 0;
                    $tot1 = 0;
                    $tax1['r_id'] = $times1[0]->r_id;
                    $tax1['planName'] = $times1[0]->r_name;         // Assumed, plan name will be same for all returned rows
                    if ($args == 'a') {


                        foreach ($times1 as $res) {
                            if ($times1[$i1]->calculated_on == 'n_r') {
                                $taxAmt1 = ($times1[$i1]->tax_mode == 'p' ? ($times1[$i1]->tax_value * $price1 / 100) : ($times1[$i1]->tax_value));
                                $tax1[$times1[$i1]->tax_name] = $taxAmt1;
                            } else if ($times1[$i1]->calculated_on == 'n_p' && ($i1 > 0)) {
                                $taxAmt1 = ($times1[$i1]->tax_mode == 'p' ? ($times1[$i1]->tax_value * ($price1 + $tot1) / 100) : ($times1[$i1]->tax_value));
                                $data[$times1[$i1]->tax_name] = $taxAmt1;
                                $tax1[$times1[$i1]->tax_name] = $taxAmt1;
                            }


                            $tot1 = $tot1 + $taxAmt1;
                            $i1++;
                        } // End foreach
                    } else
                        $tax1 = $times1;
                } else {
                    $tot1 = 0.00;
                }

                $data['total'] = $tot1;
                $tax1['total'] = $tot1;         //add total tax in array

                //header('Content-Type: application/json');
                // $data['rr_tax_response'] = json_encode($tax);

            } else {

                $data['total'] = 0.00;
            }

            $data['mp_tax_response'] = $tax1;

            /*	echo "<pre>";
		print_r($data);
		echo "</pre>";
		exit;
		*/
            //checking start
            date_default_timezone_set('Asia/Kolkata');
            $current_date = date("Y-m-d H:i:s");
            if ($details->booking_status_id == "5") {
                $start = $details->cust_from_date;
            } else if ($details->booking_status_id == "6") {
                $checkout = 1;
            }

            $data['newStart'] = $newStart;
            $data['newEnd'] = $newEnd;
            $data['newResource'] = $newResource;
            $data['id'] = $booking_id;
            $data['group_id'] = $group_id;
            $data['rooms'] = $rooms;
            $data['details'] = $details;
            $data['events'] = $this->dashboard_model->all_events_limit();
            $data['currentbookedRoom']  = $currentbookedRoom;
        } // end of new else..

        if ($details->booking_status_id == "5" && strtotime($newStart) != strtotime($start)) {   // This is previously written 

            $dataerror['checkout'] = 1;

            $this->load->view("backend/dashboard/hotel_move_error_msg", $dataerror);
        } else if ($details->booking_status_id == "6") {

            $dataerror['checkout'] = 1;

            $this->load->view("backend/dashboard/hotel_move_error_msg", $dataerror);
        } else if ($occupancyfail == '1') {

            $dataerror['occupancyfailed'] = 1;
            //	$this->hotel_move_error_message($dataerror);
            $this->load->view("backend/dashboard/hotel_move_error_msg", $dataerror);
        } else if ($sameroom == '1') {

            $dataerror['sameroom'] = 1;
            $this->load->view("backend/dashboard/hotel_move_error_msg", $dataerror);
        } else if ($grp_move == '1') {

            $dataerror['grp_move'] = 1;

            $this->load->view("backend/dashboard/hotel_move_error_msg", $dataerror);
        } else {

            /* echo "<pre>";
			print_r($data);
			exit;
		*/
            $this->load->view("backend/dashboard/hotel_booking_move", $data);
        }
    }

    function roomDeleteFromGroup()
    {
        $bookingID = $this->input->post("bookingID");
        $groupdetailsID = $this->input->post("groupdetailsID");
        $groupID = $this->input->post("groupID");
        $guest = $this->input->post("guest");
        //exit;
        $this->bookings_model->roomDeleteFromGroup($bookingID, $groupdetailsID, $groupID, $guest);
    }

    function meal_update_modifier()
    {

        $bookingID = $this->input->post("bookingId");
        $modifier_array = array(
            'food_plan_mod_type' => $this->input->post("food_plan_mod_type"),
            'food_plan_mod_rent' => $this->input->post("food_plan_mod_rent")

        );
        $result = $this->bookings_model->update_modifier($bookingID, $modifier_array);
        echo $result;
    }

    function rent_update_modifier()
    {

        $bookingID = $this->input->post("bookingId");
        $modifier_array = array(
            'rent_mode_type' => $this->input->post("rent_mode_type"),
            'mod_room_rent' => $this->input->post("mod_room_rent")

        );
        $result = $this->bookings_model->update_modifier($bookingID, $modifier_array);
        echo $result;
    }


    function group_booking_backend_move()
    {
        $start = $this->input->post('newStart');
        $end = $this->input->post('newEnd');
        $start_ac = date("Y-m-d", strtotime($start));
        $end_ac = date("Y-m-d", strtotime($end));
        $booking_id = $this->input->post('id');
        $groupID = $this->input->post('groupID');
        $room_id = $this->input->post('newResource');
        $overlaps = $this->bookings_model->count_rooms_bookings_event($booking_id, $room_id, $start, $end) > 0;

        if ($overlaps) {
            $response = new Result();
            $response->result = 'Error';
            $response->message = 'This reservation overlaps with an existing reservation.';

            header('Content-Type: application/json');
            echo json_encode($response);
            exit;
        }

        //get the exact checkin and checkout date

        $details = $this->bookings_model->get_booking_details_row($booking_id);
        $times = $this->dashboard_model->checkin_time();
        foreach ($times as $time) {
            if ($time->hotel_check_in_time_fr == 'PM' && $time->hotel_check_in_time_hr != "12") {
                $modifier = ($time->hotel_check_in_time_hr + 12) + $time->hotel_check_in_time_mm / 60;
            } else {
                $modifier = ($time->hotel_check_in_time_hr) + $time->hotel_check_in_time_mm / 60;
            }
        }

        $times2 = $this->dashboard_model->checkout_time();

        foreach ($times2 as $time2) {
            if ($time2->hotel_check_out_time_fr == 'PM' && $time2->hotel_check_out_time_hr != "12") {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12) + $time->hotel_check_out_time_mm / 60;
            } else {
                $modifier2 = ($time2->hotel_check_out_time_hr) + $time->hotel_check_out_time_mm / 60;
            }
        }
        $checkin_date = date("Y-m-d H:i:s", strtotime($details->cust_from_date) + 60 * 60 * $modifier);
        $checkout_date = date("Y-m-d H:i:s", strtotime($details->cust_end_date) + 60 * 60 * $modifier2);
        //ending


        //checking start
        date_default_timezone_set('Asia/Kolkata');
        $current_date = date("Y-m-d H:i:s");

        if ($details->booking_status_id == "5") {
            $start = $details->cust_from_date;
            //exit();
        }

        if ($details->booking_status_id == "6") {
            $end = $details->cust_end_date;
            exit();
        }

        //checkin end



        $result = $this->bookings_model->update_group_bookings_move($booking_id, $groupID, $start, $end, $start_ac, $end_ac);

        $response = new Result();
        $response->result = 'OK';
        $response->message = 'Update successful';

        // header('Content-Type: application/json');
        //echo json_encode($response);
        $data['message'] = $response->message;
        $this->load->view("backend/dashboard/hotel_booking_move", $data);
    }

    function hotel_booking_move_message()
    {
        $this->load->view("backend/dashboard/hotel_booking_move_message");
    }

    function hotel_move_error_message($dataerror)
    {
        $this->load->view("backend/dashboard/hotel_move_error_msg", $dataerror);
    }

    function roomRemoveFromGroup()
    {
        $bookingID = $this->input->post("bookingID");
        $groupdetailsID = $this->input->post("groupdetailsID");
        $groupID = $this->input->post("groupID");
        $guest = $this->input->post("guest");
        $trr = $this->input->post("trr");
        $tfi = $this->input->post("tfi");
        $totalTax = $this->input->post("totalTax");
        $st = $this->input->post("st");
        $sc = $this->input->post("sc");
        $lt = $this->input->post("lt");
        $sd = $this->input->post("sd");
        //exit;
        $this->bookings_model->roomRemoveFromGroup($bookingID, $groupdetailsID, $groupID, $guest, $trr, $tfi, $totalTax, $st, $sc, $lt, $sd);
    }



    function check_booking()
    {
        $chkin_time = $_POST['chkin_time'];
        $chkout_time = $_POST['chkout_time'];
        //$chkout_time='12:00:00';
        //$chkin_time='16:10:23';
        $start_dt = $_POST['start_dt'];
        //$start_dt='20-09-2016';
        $end_dt = $_POST['end_dt'];
        //$end_dt='21-09-2016';
        $qry = $this->dashboard_model->check_booking($start_dt, $end_dt, $chkin_time, $chkout_time);
        //print_r($qry);
        if ($qry > 0) {
            $data = array(
                'data' => '1',
            );
        } else {
            $data = array(
                'data' => '0',
            );
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }


    function change_password()
    {
        $change = $this->bookings_model->change_password($this->input->post('opwd'), $this->input->post('npwd'));
        echo $change;
        /*if ($change) {
			$this->session->set_flashdata('succ_msg', "Password updated Successfully!");
			redirect('dashboard/profile');
		} else {
			$this->session->set_flashdata('err_msg', "Database Error. Try again later!");
			redirect('dashboard/profile');
		}*/
    }

    function checkUser()
    {
        $val = $_POST["val"];
        echo $val = $this->bookings_model->checkUser($val);
    }


    function cancel_booking()
    {
        $booking_id = $this->input->post('booking_id');
        $this->dashboard_model->delete_booking($booking_id);
        $this->dashboard_model->delete_stay_guest($booking_id);
    }



    function bookingChargeLineItem_insert()
    {

        $booking_id = $this->input->post('booking_id');
        $room_rent = $this->input->post('room_rent');
        $room_rent_tax = $this->input->post('room_rent_tax');
        $adlt = $this->input->post('adult');
        $kid = $this->input->post('kid');

        if ($this->input->post('extra_parsen_rom_rent') == NULL)
            $extra_parsen_rom_rent = 0;
        else
            $extra_parsen_rom_rent = $this->input->post('extra_parsen_rom_rent');

        $extra_parsen_rom_rent_tax = $this->input->post('extra_parsen_rom_rent_tax');
        $meal_plan = $this->input->post('meal_plan');
        $meal_plan_tax = $this->input->post('meal_plan_tax');

        if ($this->input->post('extra_meal_plan') == NULL)
            $extra_meal_plan = 0;
        else
            $extra_meal_plan = $this->input->post('extra_meal_plan');

        $extra_meal_plan_tax = $this->input->post('extra_meal_plan_tax');
        $BookingResponse = $this->input->post('BookingResponse');
        $mealResponse = $this->input->post('mealResponse');

        $total_meal_tax = $this->input->post('meal_plan_tax');
        $date11 = $this->input->post('date11');


        $group_id = 0;
        if (isset($group_id)) {
            $group_id = '';
        }

        // Passing data to Booking line item table
        $data = array(
            'booking_id' => $booking_id,
            'room_rent' => $room_rent,
            'rr_total_tax' => $room_rent_tax,
            'group_id' => $group_id,
            'exrr_chrg' => $extra_parsen_rom_rent,
            'exmp_chrg' => $extra_meal_plan,
            'exrr_total_tax' => $extra_parsen_rom_rent_tax,
            'exmp_total_tax' => $extra_meal_plan_tax,
            'meal_plan_chrg' => $meal_plan,
            'adult' => $adlt,
            'kid' => $kid,
            'mp_total_tax' => $meal_plan_tax,
            'hotel_id' => $this->session->userdata('user_hotel'),
            'rr_tax_response' => $BookingResponse,
            'mp_tax_response' => $mealResponse,
            'date' => $date11
        );

        $query = $this->bookings_model->bookingChargeLineItem_insert($data);
        if ($query > 0) {
            $msg = 1;
        } else {
            $msg = 0;
        }

        $data = array(
            'data' => $query,
            'status' => $msg,
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function getBcliID()
    {

        $booking_id = $this->input->post('booking_id');
        $index = $this->input->post('index');

        $query = $this->bookings_model->bookingChargeIineItem_fetch_bcli($booking_id, $index);

        if ($query > 0) {
            $data = array(
                'bcli_id' => $query->bcli_id,
                'status' => 'success',
            );
        } else {
            $data = array(
                'status' => 'fail',
            );
        }

        header('Content-Type: application/json');
        echo json_encode($data);
    }


    function bookingChargeLineItem_update()
    {

        $bcli = $this->input->post('bcli');
        $booking_id = $this->input->post('booking_id');
        $room_rent = $this->input->post('room_rent');
        $room_rent_tax = $this->input->post('room_rent_tax');
        $extra_parsen_rom_rent = $this->input->post('extra_parsen_rom_rent');
        $extra_parsen_rom_rent_tax = $this->input->post('extra_parsen_rom_rent_tax');
        $meal_plan = $this->input->post('meal_plan');
        $meal_plan_tax = $this->input->post('meal_plan_tax');
        $extra_meal_plan = $this->input->post('extra_meal_plan');
        $extra_meal_plan_tax = $this->input->post('extra_meal_plan_tax');
        $BookingResponse = $this->input->post('BookingResponse');
        $mealResponse = $this->input->post('mealResponse');
        $total_meal_tax = $this->input->post('meal_plan_tax');
        $date11 = $this->input->post('date11');
        $group_id;

        if (isset($group_id)) {
            $group_id = '';
        }

        $data = array(
            'booking_id' => $booking_id,
            'room_rent' => $room_rent,
            'rr_total_tax' => $room_rent_tax,
            'group_id' => $group_id,
            'exrr_chrg' => $extra_parsen_rom_rent,
            'exmp_chrg' => $extra_meal_plan,
            'exrr_total_tax' => $extra_parsen_rom_rent_tax,
            'exmp_total_tax' => $extra_meal_plan_tax,
            'meal_plan_chrg' => $meal_plan,
            'mp_total_tax' => $meal_plan_tax,
            'hotel_id' => $this->session->userdata('user_hotel'),
            'rr_tax_response' => $BookingResponse,
            'mp_tax_response' => $mealResponse,
            'date' => $date11
        );

        $query = $this->bookings_model->bookingChargeLineItem_updateBy_bcli($booking_id, $bcli, $data);
        $data = array(
            'data' => "sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }



    function bookingChargeLineItem_update_dynamic()
    {

        $bcli = $this->input->post('bcli');
        $booking_id = $this->input->post('booking_id');
        $room_rent = $this->input->post('room_rent');
        $room_rent_tax = $this->input->post('room_rent_tax');
        $extra_parsen_rom_rent = $this->input->post('extra_parsen_rom_rent');
        $extra_parsen_rom_rent_tax = $this->input->post('extra_parsen_rom_rent_tax');
        $meal_plan = $this->input->post('meal_plan');
        $meal_plan_tax = $this->input->post('meal_plan_tax');
        $extra_meal_plan = $this->input->post('extra_meal_plan');
        $extra_meal_plan_tax = $this->input->post('extra_meal_plan_tax');
        $BookingResponse = $this->input->post('BookingResponse');
        $mealResponse = $this->input->post('mealResponse');
        $total_meal_tax = $this->input->post('meal_plan_tax');
        $date11 = $this->input->post('date11');
        $adlt = $this->input->post('adult');
        $kid = $this->input->post('kid');
        $mod_room_rent = $this->input->post('mod_room_rent');
        $group_id;

        if (isset($group_id)) {
            $group_id = '';
        }

        $data = array(
            'booking_id' => $booking_id,
            'room_rent' => $room_rent,
            'rr_total_tax' => $room_rent_tax,
            'group_id' => $group_id,
            'exrr_chrg' => $extra_parsen_rom_rent,
            'exmp_chrg' => $extra_meal_plan,
            'exrr_total_tax' => $extra_parsen_rom_rent_tax,
            'exmp_total_tax' => $extra_meal_plan_tax,
            'adult' => $adlt,
            'kid' => $kid,
            'modifier_amount' => $mod_room_rent,
            'meal_plan_chrg' => $meal_plan,
            'mp_total_tax' => $meal_plan_tax,
            'hotel_id' => $this->session->userdata('user_hotel'),
            'rr_tax_response' => $BookingResponse,
            'mp_tax_response' => $mealResponse,
            'date' => $date11
        );

        $query = $this->bookings_model->bookingChargeLineItem_updateBy_bcli_dynamic($booking_id, $bcli, $data);
        $data = array(
            'data' => "sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }



    function hotel_backend_onchange_unitCategory()
    {

        $hotel_id = $this->session->userdata('user_hotel');
        $unit_selector = $this->input->post('unit_id');
        //$id = $this->session->userdata('user_id');
        //$data = $this->dashboard_model->admin_status($id);
        //if(isset($data) && $data){
        $hotel_id = $this->session->userdata('user_hotel');
        /*}else{
        $hotel_id = '';
        }*/

        $rooms = $this->bookings_model->all_rooms_hotelwise($hotel_id);
        if ($unit_selector == 'all') {
            $units = $this->bookings_model->all_units();
        } else {
            $units = $this->bookings_model->all_units_limit($unit_selector);
        }
        $result = array();
        $i = 0;
        $j = 0;
        foreach ($units as $unit) {
            $a = array();
            $x = new Room();
            $x->id = "unit";
            $x->name = "<div style=\"color: white; font-weight: 800; \">" . $unit->unit_name . "</div>";
            $x->unit_id = $unit->id;
            $x->expanded = true;
            $x->eventHeight = 20;
            $x->backColor = "#A6A6A6";


            foreach ($rooms as $room) {

                if ($room->unit_id == $unit->id) {

                    $unit_id = $room->unit_id;
                    $unit = $this->bookings_model->get_unit_name($unit_id);
                    $unit_name = $unit->unit_name;
                    $unit_id = $unit->id;


                    $r = new Room();
                    $r->id = $room->room_id;
                    $r->eventHeight = 50;
                    $clean = $room->clean_id;
                    if ($clean == 1) {
                        $color = "#1BA39C";
                    } else if ($clean == 2) {
                        $color = "#F7CA18";
                    } else if ($clean == 3) {
                        $color = "#5E738B";
                    } else if ($clean == 4) {
                        $color = "#555555";
                    } else {
                        $color = "red";
                    }
                    $bgColor = $this->dashboard_model->get_color_by_houskeeping_status_id($clean);
                    if (isset($bgColor) && $bgColor) {
                        $color = $bgColor->color_primary;
                    } else {
                        $color = "#666";
                    }


                    date_default_timezone_set('Asia/Kolkata');
                    $d = date('Y-m-d');
                    $flg = $this->bookings_model->OTA_Avail($room->room_id, $d); //flg Active = 1 inactive = 0
                    $rm_Avail = $this->bookings_model->rm_Avail($room->room_id);

                    if ($rm_Avail == 1) {
                        $av = "active";
                    }
                    $stat = '';
                    $ot = '';
                    $av = '';
                    if ($flg) {
                        $stat = 'Ota';
                        $ot = " active";
                    } else {
                        $stat = 'Avail';
                    }
                    $s = " border-right: 7px solid #f9ba25;";
                    $style = "<div style=" . $s . "></div>";
                    //$r->name = $room->room_no . "<br/>" . $room->room_bed . " Bed" . $style;
                    $r->capacity = $room->room_bed;
                    $r->status = $room->room_status;
                    //$room->unit_id
                    $roomName = $this->unit_class_model->unit_type_fetch_id1($room->unit_id);
                    $unit_type_name = $roomName->name;
                    //$ac=$room->room_features;
                    $ac = (explode(',', $room->room_features));
                    if ($ac[0] == '1') {
                        $AC = " active";
                    } else {
                        $AC = "";
                    }
                    $r->html = "<div style=\"font-size:14px; border-right: 7px solid " . $color . " ; padding:6px; cursor:pointer; margin-left: 40px;\"><div class='ot-av'><span class='ota'>Ota</span><span><small class='ac" . $AC . "'>AC</small><small class='ava" . $av . "'>AV</small><span></div><div style='color:" . $color . "' class='room-noo'>" . $room->room_no . "<span>" . $unit_type_name . "</span></div><div class='bed'><span>" . $room->room_bed . " </span><span>(" . $room->max_occupancy . ")</span></div></div>";
                    $r->css = "resource_action_menu";
                    $r->borderColor = "green";
                    $a[] = $r;
                    $unit_id_prev = $unit_id;
                    $j++;
                }
            }
            $i++;
            $x->children = $a;
            $result[] = $x;
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    function get_source_type_id_by_name_source()
    {

        $source = $this->input->post('source');
        $query = $this->bookings_model->get_source_type_id_by_name_source($source);


        $data = array(
            'id' => $query->bk_source_type_id

        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function send_Mail_user()
    {
        $mail_settings = $this->dashboard_model->get_template_settings($this->session->userdata('user_hotel'));


        if (isset($mail_settings->confirmation_mail_from) &&  $mail_settings->confirmation_mail_from && $mail_settings->send_confirmation_email == 'yes') {
            $hotel = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $mail = $_POST['mail_id'];
            $booking_id = $_POST['booking_id'];
            $subject = "Booking confirmation mail (1 Room) | " . $hotel->hotel_name;
            $header = "From:" . $mail_settings->confirmation_mail_from . "\r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-type: text/html\r\n";

            $hotel_id = $this->session->userdata('user_hotel');

            $type = "sb";
            $hotel_id = $this->session->userdata('user_hotel');
            $data['booking_details']  = $this->bookings_model->get_booking_details($booking_id);
            foreach ($data['booking_details'] as $key) {
                $data['group_id'] = $key->group_id;
                $bk_date = $key->booking_taking_date;
            }

            $data['hotel_name'] = $this->dashboard_model->get_hotel($data['booking_details'][0]->hotel_id);
            $data['hotel_contact']  = $this->bookings_model->get_hotel_contact_details($data['booking_details'][0]->hotel_id);


            $inv_name = "BK0" . $hotel_id . "_" . $bk_date . "_" . $booking_id;
            $data['tax'] = $this->bookings_model->get_tax_details($hotel_id);
            $data['adjustment'] = $this->dashboard_model->get_adjuset_amt($booking_id);
            $data['all_adjustment'] = $this->dashboard_model->all_adjst($booking_id);
            $data['chagr_tax'] = $this->bookings_model->line_charge_item_tax($type, $booking_id);
            $data['transaction_details']  = $this->bookings_model->get_transaction_details($booking_id);
            $data['payment_details']  = $this->bookings_model->get_payment_details($booking_id);
            $data['total_payment']  = $this->bookings_model->get_total_payment($booking_id);
            $data['pos']  = $this->dashboard_model->all_pos_booking($booking_id);
            $data['printer']  = $this->dashboard_model->get_printer($hotel_id);
            $this->load->view("backend/dashboard/invoice_pdf_mail", $data);
            $html = $this->output->get_output();

            $a = @mail($mail, $subject, $html, $header);
        } else {
            $a = 1;
        }
        return 1;
    }

    function check_status()
    {
        $resource = $_GET["resource"];
        $clean = $this->bookings_model->get_clean_status($resource);
        $clean_id = $clean->clean_id;
        if ($clean_id) {
            $data = array(
                'data' => $clean_id,
            );
        }

        header('Content-Type: application/json');
        echo json_encode($data);
    }


    /* end  */
}
