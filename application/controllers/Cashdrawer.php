<?php
/*
  Created by PhpStorm.
 User: subhabrata
 Date: 11/11/2015
 Time: 12:08 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Cashdrawer extends CI_Controller
{
 public function __construct()
    {
        parent::__construct();
		$Gid=0;
		if (!$this->session->userdata('is_logged_in')) {
            redirect('login');
        }
		
		/*create dynamic array*/
		  $db['superadmin'] = array(
					'dsn'   => '',
					'hostname' => 'localhost',
					'username' => $this->session->userdata('username'),
					'password' => $this->session->userdata('password'),
					'database' => $this->session->userdata('database'),
					'dbdriver' => 'mysqli',
					'dbprefix' => '',
					'pconnect' => FALSE,
					'db_debug' => TRUE,
					'cache_on' => FALSE,
					'cachedir' => '',
					'char_set' => 'utf8',
					'dbcollat' => 'utf8_general_ci',
					'swap_pre' => '',
					'encrypt' => FALSE,
					'compress' => FALSE,
					'stricton' => FALSE,
					'failover' => array(),
					'save_queries' => TRUE
				);
  //echo "<pre>";
       //    print_r($db1['default']);exit;
            $CI = &get_instance();
        $this->db1 = $CI->load->database($db['superadmin'],true);
		
        //$this->load->database();
		
		/* end of dynamic array creation */
		$this->arraay =$this->session->userdata('per');
                //$login_id=$this->session->userdata('admin_id');
                //if(!$login_id>0){redirect(base_url('admin/login')); }
        $this->load->model('mod_common');
        $this->load->model('bookings_model');
        $this->load->model('dashboard_model');
        $this->load->library('grocery_CRUD');
		$this->load->library('table');
		if (!$this->session->userdata('is_logged_in')) {
            redirect('login');
        }
		
    }
    function index()
    {
		
	//	echo $this->session->userdata('user_id'); exit;
        $data['hotels'] = $this->dashboard_model->total_hotels_hotel_id(); 
	//	echo "<pre>";
	//	print_r($data['hotels']);exit;
		  $this->load->view('backend/dashboard/index_2',$data);
		
		//$data['hotel_id']=$this->session->userdata('user_hotel');
	   //$this->load->view('backend/cashdrawer/index', $data);
	  
    }
	function index2(){
		
		/*echo "<pre>";
		echo $this->db->database;
		echo "<br>".$this->db1->database;
		echo "<pre>";
			print_r($query->result());
			exit;
			echo "<br>".$hid; */
		
			if($this->input->post()){
		$hid=$_POST['hotel_id_session'];
		//echo $hid; exit;
		$this->session->set_userdata(array( 'user_hotel' =>$hid ));
			}
			
	
		
			
			
		 $data['hotels'] = $this->dashboard_model->total_hotels();      
		 $data['hotel_id']=$this->session->userdata('user_hotel');
		 
		 
		 
		 //$this->session->set_userdata(array( 'user_hotel' =>$query ))
		// echo "<pre>";
		// print_r($this->session->userdata());exit;    
	   $this->load->view('backend/cashdrawer/index', $data);
	   
	   //$data['hotels'] = $this->dashboard_model->total_hotels();       
	   // $this->load->view('backend/dashboard/index_2',$data);
	}

    function redirect(){

        /*$alls=$this->dashboard_model->total_hotels();
        foreach($alls as $hot){

            if(isset($this->input->post('user_hotel'.$hot->hotel_id))){
                $target=$hot->hotel_id;

            }
        }*/


        $userdata=array(
            //'user_type'=> $user_type_data->user_type_name,

            'user_hotel'=>$this->input->post('user_hotel'),

        );
       // print_r($userdata);
       // exit();
        $this->session->set_userdata($userdata);
        redirect('dashboard');
    }

    function new_session(){
			$data['heading'] = " ";
            $data['description'] ='';
            $data['active'] = '';
        date_default_timezone_set('Asia/Kolkata');
        $data=array(

            'opening_datetime' => date("Y-m-d H:i:s"),
            'user_id' => $this->session->userdata('user_id'),
            'hotel_id' => $this->session->userdata('user_hotel'),
					
        );
		 //echo "<pre>";
		 //print_r($data);exit;
        $cd_key=$this->dashboard_model->insert_cash_drawer($data);
		/*echo $cd_key;
		echo '<br>'.$this->session->userdata('user_hotel');
		exit;*/
		 $opening_cash=$this->unit_class_model->set_opening_cash($cd_key);
	// echo "<pre>";
		//print_r( $opening_cash);exit;
		$cash=$opening_cash->opening_cash;		
		$deposit=$opening_cash->deposit;
		$withdraw=$opening_cash->withdraw;
		$clossing=$opening_cash->closing_cash;
		
		$cash=$this->unit_class_model->update_opening_balance($cash,$cd_key,$deposit,$withdraw,$clossing);
		
        $this->session->set_userdata(array( 'cd_key' =>$cd_key ));

      /*  if($_GET['id'] && isset($_GET['id'])){
            $this->dashboard_model->update_drawer($_GET['id']);
        }*/
		 
		 $data1['id']=$_GET['id'];
		 $data1['page'] = 'backend/cashdrawer/open_register';
       $this->load->view('backend/cashdrawer/open_register', $data1);
		//redirect('dashboard');
		
    }
function new_session1(){
$id=$_POST['id'];
        date_default_timezone_set('Asia/Kolkata');
        $data=array(
            'opening_datetime' => date("Y-m-d H:i:s"),
            'user_id' => $this->session->userdata('user_id'),
            'hotel_id' => $this->session->userdata('user_hotel')
        );
        $cd_key=$this->dashboard_model->insert_cash_drawer($data);
		$opening_cash=$this->unit_class_model->set_opening_cash($cd_key);
		$cash=$opening_cash->clossing_cash;
		$deposit=$opening_cash->deposit;
		$withdraw=$opening_cash->withdraw;
		$clossing=$opening_cash->clossing_cash;
		$cash=$this->unit_class_model->update_opening_balance($cash,$cd_key,$deposit,$withdraw,$clossing);
        $this->session->set_userdata(array( 'cd_key' =>$cd_key ));

        if($id && isset($id)){
            $this->dashboard_model->update_drawer($id);
        }
		 
		// $data1['id']=$_GET['id'];
		// $data1['page'] = 'backend/cashdrawer/open_register';
        //$this->load->view('backend/cashdrawer/open_register', $data1);
		//redirect('dashboard');
		echo $data=1;
		//header('Content-Type: application/json');
      //  echo json_encode($data);
		
    }
	function cashDrawerLog($id){
		
		date_default_timezone_set('Asia/Kolkata');
		$data=array(
		'shift_id'=>$this->session->userdata('new_shift'),
		'cash_drawer_id'=>$id,		
		'enterDateTime'=>date('Y-m-d H:i:s'),
		'user_id'=>$this->session->userdata('user_id'),
		'hotel_id'=>$this->session->userdata('user_hotel')
		);
		$log_id=$this->unit_class_model->save_cash_drawer_log($data);
		$Gid=$log_id;		//echo 'pre_log_key_key'.$Gid;exit;
		$this->session->set_userdata(array('pre_log_key_key' =>$log_id ));
		$data1=array(
		'opening_datetime'=>date('Y-m-d H:i:s'),
		
		);
		//$this->unit_class_model->update_cash_drawer($data1,$id);
		return $log_id;
	}
    function old_session(){

        $id=$_GET['id'];
		
        $this->session->set_userdata(array( 'cd_key' =>$id ));
		$logID=$this->cashDrawerLog($id);
		//$Gid=$id;
		$this->session->set_userdata(array( 'pre_log_key' =>$logID ));
		$data=array(
		'cashdrawer'=>$this->session->userdata('cd_key')
		);
		$this->login_model->update_login_session1($data,$this->session->userdata('login_session_id'),$this->session->userdata('user_hotel'));
	
	/*	date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
		$data1=array(
		'drawer_id'=>$this->session->userdata('cd_key'),
		'closing_datetime'=>$d
		);
		//print_r($data1);exit;
		//$this->unit_class_model->update_cash_drawer($this->session->userdata('cd_key'),$data1);
		
		
		$this->session->userdata('cd_key');
		$bb=$this->session->userdata('pre_log_key_key');
        $id=$_GET['id'];
		//echo $id;exit;
        $this->session->set_userdata(array( 'cd_key' =>$id ));
        $data=1;
		$ab=$this->cashDrawerLog($id);
		$this->session->set_userdata(array( 'pre_log_key' =>$ab ));
		$data2=array(
		'leaveDateTime'=>$d,		
		);
		if($bb!=$this->session->userdata('pre_log_key')){
		$this->unit_class_model->update_cash_drawer_log($data2,$bb);
		}*/
		
		
		function getBrowser() {
                $u_agent = $_SERVER['HTTP_USER_AGENT'];
                $bname = 'Unknown';
                $platform = 'Unknown';
                $version = "";

                //First get the platform?
                if (preg_match('/linux/i', $u_agent)) {
                    $platform = 'Linux';
                } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
                    $platform = 'Mac';
                } elseif (preg_match('/windows|win32/i', $u_agent)) {
                    $platform = 'Windows';
                }

                // Next get the name of the useragent yes seperately and for good reason
                if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
                    $bname = 'Internet Explorer';
                    $ub = "MSIE";
                } elseif (preg_match('/Firefox/i', $u_agent)) {
                    $bname = 'Mozilla Firefox';
                    $ub = "Firefox";
                } elseif (preg_match('/Chrome/i', $u_agent)) {
                    $bname = 'Google Chrome';
                    $ub = "Chrome";
                } elseif (preg_match('/Safari/i', $u_agent)) {
                    $bname = 'Apple Safari';
                    $ub = "Safari";
                } elseif (preg_match('/Opera/i', $u_agent)) {
                    $bname = 'Opera';
                    $ub = "Opera";
                } elseif (preg_match('/Netscape/i', $u_agent)) {
                    $bname = 'Netscape';
                    $ub = "Netscape";
                }

                // finally get the correct version number
                $known = array('Version', $ub, 'other');
                $pattern = '#(?<browser>' . join('|', $known) .
                        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
                if (!preg_match_all($pattern, $u_agent, $matches)) {
                    // we have no matching number just continue
                }

                // see how many we have
                $i = count($matches['browser']);
                if ($i != 1) {
                    //we will have two since we are not using 'other' argument yet
                    //see if version is before or after the name
                    if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                        $version = $matches['version'][0];
                    } else {
                        $version = $matches['version'][1];
                    }
                } else {
                    $version = $matches['version'][0];
                }

                // check if we have a number
                if ($version == null || $version == "") {
                    $version = "?";
                }

                return array(
                    'userAgent' => $u_agent,
                    'name' => $bname,
                    'version' => $version,
                    'platform' => $platform,
                    'pattern' => $pattern
                );
            }

			$ua = getBrowser();
                $yourbrowser = $ua['name'] . " " . $ua['version'] . " on " . $ua['platform'];

                //---------------------------------------------------------------------------
                //echo 'Browser - '.$browser;
                $session_data = array(
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'browser_info' => $yourbrowser,
                    'cashdrawer' => $id,
					
                    'status' => 1,
                );
                $login_session_data = $this->login_model->login_session($session_data);
			$this->session->set_userdata(array( 'mach_id' =>$login_session_data ));
			$this->session->set_userdata(array('login_session_id' => $login_session_data));
			
			
		
		
		
		
		
		
        redirect('dashboard');
    }
	function enter_cashdrawer(){
			date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
		$data1=array(
		'drawer_id'=>$this->session->userdata('cd_key'),
		'closing_datetime'=>$d
		);
		//print_r($data1);exit;
		//$this->unit_class_model->update_cash_drawer($this->session->userdata('cd_key'),$data1);
		
		
		$this->session->userdata('cd_key');
		$bb=$this->session->userdata('pre_log_key_key');
        $id=$_POST['id'];
		//echo $id;exit;
        $this->session->set_userdata(array( 'cd_key' =>$id ));
        $data=1;
		$ab=$this->cashDrawerLog($id);
		$this->session->set_userdata(array( 'pre_log_key' =>$ab ));
		$data2=array(
		'leaveDateTime'=>$d,		
		);
		if($bb!=$this->session->userdata('pre_log_key')){
		$this->unit_class_model->update_cash_drawer_log($data2,$bb);
		}
//	print_r($data2);
		header('Content-Type: application/json');
        echo json_encode($data);
    }
	
	//loader HOTEL add section
	function add_cash(){
		date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
		 //$found = in_array("10",$this->arraay);
		 $found ='1';
		if($found) {
		$this->load->model('unit_class_model');
		
        if($this->input->post())
                {   
			
					$opening=$this->input->post('hid1');
					$withdraw=$this->input->post('withdraw');
					$deposit=$this->input->post('deposit');
					$name=$this->input->post('name');
					$profitcenter=$this->input->post('p_center');

					$closing_cash=($opening+$deposit)-$withdraw;
					
					$hotel_id=$this->session->userdata('user_hotel');
					$user_id=$this->session->userdata('user_id');
					if($user_id > 0){
						$user = $this->dashboard_model->get_admin($user_id);
					
						if(isset($user)){
							foreach($user as $user){
								$nam=$user->admin_first_name;
								//print_r($user);
							}
						}
					}
						 $data=array(
								'closing_cash'=>$closing_cash,						 
								'withdraw'=>$withdraw,
								'deposit'=>$deposit,
								'hotel_id'=>$hotel_id,
								'user_id'=>$user_id,
								//'opening_datetime'=>$d,
								'cashDrawerName'=>$nam.'-'.date('d-m-y').'-'.$name,
								'profitCenter'=>$profitcenter,
								'is_ended'=>0,
                            );
					$query=$this->unit_class_model->add_cash($data);
					
					}
		}
	
	redirect('dashboard');
	}
	function add_cash_index(){
		date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
		 //$found = in_array("10",$this->arraay);
		 $found ='1';
		if($found) {
		$this->load->model('unit_class_model');
        if($this->input->post())
                {   
			
					$name=$this->input->post('name');
					$profitcenter=$this->input->post('profitcenter');
					$opening=$this->input->post('opening');
					//$withdraw=$this->input->post('withdraw');
					$deposit=$this->input->post('deposit');

					$closing_cash=($opening+$deposit);
					
					$hotel_id=$this->session->userdata('user_hotel');
					$user_id=$this->session->userdata('user_id');
					
					if($user_id > 0){
						$user = $this->dashboard_model->get_admin($user_id);
					
						if(isset($user)){
							foreach($user as $user){
								$nam=$user->admin_first_name;
								//print_r($user);
							}
						}
					}
					
						 $data=array(
								'closing_cash'=>$closing_cash,						 
								//'withdraw'=>$withdraw,
								'deposit'=>$deposit,
								'hotel_id'=>$hotel_id,
								'user_id'=>$user_id,
								'opening_datetime'=>$d,
								'cashDrawerName'=>$nam.'-'.date('d-m-y').'-'.$name,
								'profitCenter'=>$profitcenter,
								'is_ended'=>0,
								
								//'user_id'=>$user_id,
                            );
					$query=$this->unit_class_model->add_cash($data);
					
					}
		}
	if($query){
		$data=1;
		header('Content-Type: application/json');
        echo json_encode($data);
	}
	
	}
	function edit_cash_drawer(){
		
		date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
		 $found ='1';
		if($found) {
		$this->load->model('unit_class_model');
        if($this->input->post())
                {   
			
					$name=$this->input->post('name');
					$profitcenter=$this->input->post('profitcenter');					
					$deposit=$this->input->post('deposit');
					$id=$this->input->post('id');
				
					/*$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				   $this->db1->where('drawer_id',$id);
				   $query1=$this->db1->get(TABLE_PRE.'cash_drawer');
				   $query1=$query1->row();
	   
	   
	   $clossing=$query1->cash_collection+$deposit;*/
					
					
					$hotel_id=$this->session->userdata('user_hotel');
					$user_id=$this->session->userdata('user_id');
					
					if($user_id > 0){
						$user = $this->dashboard_model->get_admin($user_id);
					
						if(isset($user)){
							foreach($user as $user){
								$nam=$user->admin_first_name;
								//print_r($user);
							}
						}
					}
					
					
						 $data=array(
							//	'closing_cash'=>$clossing,						 
								//'withdraw'=>$withdraw,
								'deposit'=>$deposit,
								'hotel_id'=>$hotel_id,
								'user_id'=>$user_id,
								//'opening_datetime'=>$d,
								'cashDrawerName'=>$nam.'-'.date('d-m-y').'-'.$name,
								'profitCenter'=>$profitcenter,
								'is_ended'=>0,
								
								//'user_id'=>$user_id,
                            );
					$query=$this->unit_class_model->edit_cash($data,$id);
					
					}
		}
	if($query){
		$data=1;
		header('Content-Type: application/json');
        echo json_encode($data);
	}
	}
	
	function add_cash_index1(){
		date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
		 //$found = in_array("10",$this->arraay);
		 $found ='1';
		if($found) {
		$this->load->model('unit_class_model');
        if($this->input->post())
                {   
			
					$name=$this->input->post('name');
					$profitcenter=$this->input->post('profitcenter');
					$opening=$this->input->post('opening');
					//$withdraw=$this->input->post('withdraw');
					$deposit=$this->input->post('deposit');

					$closing_cash=($opening+$deposit);
					
					$hotel_id=$this->session->userdata('user_hotel');
					$user_id=$this->session->userdata('user_id');
					
					if($user_id > 0){
						$user = $this->dashboard_model->get_admin($user_id);
					
						if(isset($user)){
							foreach($user as $user){
								$nam=$user->admin_first_name;
								//print_r($user);
							}
						}
					}
					
						 $data=array(
								'closing_cash'=>$closing_cash,						 
								//'withdraw'=>$withdraw,
								'deposit'=>$deposit,
								'hotel_id'=>$hotel_id,
								'user_id'=>$user_id,
								'opening_datetime'=>$d,
								'cashDrawerName'=>$nam.'-'.date('d-m-y').'-'.$name,
								'profitCenter'=>$profitcenter,
								'is_ended'=>0,
								
								//'user_id'=>$user_id,
                            );
					$query=$this->unit_class_model->add_cash($data);
					$this->session->set_userdata(array( 'cd_key' =>$query ));
					}
		}
	if($query){
		$data=1;
		header('Content-Type: application/json');
        echo json_encode($data);
	}
	
	}
	
	function add_cash_index2(){
		date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
		 //$found = in_array("10",$this->arraay);
		 $found ='1';
		if($found) {
		$this->load->model('unit_class_model');
        if($this->input->post())
                {   
			
					$name=$this->input->post('name');
					$profitcenter=$this->input->post('profitcenter');
					$opening=$this->input->post('opening');//previous clossion cash
					//$withdraw=$this->input->post('withdraw');
					$deposit=$this->input->post('deposit');
					$p_center1=$this->input->post('p_center1');
					
					
					
					$hotel_id=$this->session->userdata('user_hotel');
					$user_id=$this->session->userdata('user_id');
					
					if($user_id > 0){
						$user = $this->dashboard_model->get_admin($user_id);
					
						if(isset($user) && $user){
							foreach($user as $user){
								$nam=$user->admin_first_name;
								//print_r($user);
							}
						}else{							$nam='';						}
					}
					
					if(isset($p_center1) && $p_center1){	 
							$data1=array(
							'hotel_id'=>$hotel_id,
							'user_id'=>$user_id,
							'profit_center_location'=>$profitcenter,
							'profit_center_des'=>'Default',
							'status'=>'1',
							
							);
					$query1=$this->unit_class_model->add_profitcenter($data1);
					$data=array(
								'closing_cash'=>0,						 
								//'withdraw'=>$withdraw,
								'deposit'=>$deposit,
								'opening_cash'=>$deposit,
								'hotel_id'=>$hotel_id,
								'user_id'=>$user_id,
								'opening_datetime'=>$d,
								'cashDrawerName'=>$nam.'-'.date('d-m-y').'-'.$name,
								'profitCenter'=>$query1,
								'is_ended'=>0,
								
								//'user_id'=>$user_id,
                            );
					}else{
						
					$data=array(
								'closing_cash'=>0,						 
								//'withdraw'=>$withdraw,
								'deposit'=>$deposit,
								'opening_cash'=>$deposit,
								'hotel_id'=>$hotel_id,
								'user_id'=>$user_id,
								'opening_datetime'=>$d,
								'cashDrawerName'=>$nam.'-'.date('d-m-y').'-'.$name,
								'profitCenter'=>$profitcenter,
								'is_ended'=>0,
								
								//'user_id'=>$user_id,
                            );
						
					}
					//echo "<pre>";
					//print_r($data);
					
					$query=$this->unit_class_model->add_cash($data);
				///	echo $query;
					$this->session->set_userdata(array( 'cd_key' =>$query ));
					//$this->session->set_userdata(array( 'user_hotel' =>$hotel_id ));
					
					}
		}
		$data=array(
		'cashdrawer'=>$this->session->userdata('cd_key')
		);
		//$this->login_model->update_login_session1($data,$this->session->userdata('login_session_id'),$this->session->userdata('user_hotel'));
		function getBrowser() {
                $u_agent = $_SERVER['HTTP_USER_AGENT'];
                $bname = 'Unknown';
                $platform = 'Unknown';
                $version = "";

                //First get the platform?
                if (preg_match('/linux/i', $u_agent)) {
                    $platform = 'Linux';
                } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
                    $platform = 'Mac';
                } elseif (preg_match('/windows|win32/i', $u_agent)) {
                    $platform = 'Windows';
                }

                // Next get the name of the useragent yes seperately and for good reason
                if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
                    $bname = 'Internet Explorer';
                    $ub = "MSIE";
                } elseif (preg_match('/Firefox/i', $u_agent)) {
                    $bname = 'Mozilla Firefox';
                    $ub = "Firefox";
                } elseif (preg_match('/Chrome/i', $u_agent)) {
                    $bname = 'Google Chrome';
                    $ub = "Chrome";
                } elseif (preg_match('/Safari/i', $u_agent)) {
                    $bname = 'Apple Safari';
                    $ub = "Safari";
                } elseif (preg_match('/Opera/i', $u_agent)) {
                    $bname = 'Opera';
                    $ub = "Opera";
                } elseif (preg_match('/Netscape/i', $u_agent)) {
                    $bname = 'Netscape';
                    $ub = "Netscape";
                }

                // finally get the correct version number
                $known = array('Version', $ub, 'other');
                $pattern = '#(?<browser>' . join('|', $known) .
                        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
                if (!preg_match_all($pattern, $u_agent, $matches)) {
                    // we have no matching number just continue
                }

                // see how many we have
                $i = count($matches['browser']);
                if ($i != 1) {
                    //we will have two since we are not using 'other' argument yet
                    //see if version is before or after the name
                    if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                        $version = $matches['version'][0];
                    } else {
                        $version = $matches['version'][1];
                    }
                } else {
                    $version = $matches['version'][0];
                }

                // check if we have a number
                if ($version == null || $version == "") {
                    $version = "?";
                }

                return array(
                    'userAgent' => $u_agent,
                    'name' => $bname,
                    'version' => $version,
                    'platform' => $platform,
                    'pattern' => $pattern
                );
            }

			$ua = getBrowser();
                $yourbrowser = $ua['name'] . " " . $ua['version'] . " on " . $ua['platform'];

                //---------------------------------------------------------------------------
                //echo 'Browser - '.$browser;
                $session_data = array(
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'browser_info' => $yourbrowser,
                    'cashdrawer' => $this->session->userdata('cd_key'),
                    'status' => 1,
                );
				
		$logID=$this->cashDrawerLog($this->session->userdata('cd_key'));
                $login_session_data = $this->login_model->login_session($session_data);
			$this->session->set_userdata(array( 'mach_id' =>$login_session_data ));
			$this->session->set_userdata(array('login_session_id' => $login_session_data));
			
			
		
		
		
		
		
		
		
		
		
		
	if($query){
		$data=1;
		header('Content-Type: application/json');
       echo json_encode($data);
	}
	
	
	
	}
	function get_clossing_cash_drawer_info($id){
		
		$res=$this->unit_class_model->get_clossing_cash_drawer_info($id);
		if($res){
			echo 'Do you want to change default profit center';
			
		}
		$data=array(
		'id'=>$res->drawer_id,		
		
		);
		
		header('Content-Type: application/json');
        echo json_encode($data);
	
	}
	
	function close_cashdrawer(){ 
		$id=$this->input->post("id");		
		$inputValue=$this->input->post("inputValue");		
		$note=$this->input->post("note");		
		date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
		
	    
					   $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					   $this->db1->where('drawer_id',$id);
					   $query1=$this->db1->get(TABLE_PRE.'cash_drawer');
					   $query1=$query1->row();
$ne=$inputValue+$query1->withdraw;
$query=$this->unit_class_model->close_cashdrawer($id,$ne,$d);
		

					
					   
	   
	   $wid=$query1->withdraw+$inputValue;
					
						 $data2=array(														 
								'withdraw'=>$wid,
								
                            );
							$sd=$query1->cash_collection+$query1->deposit-$query1->withdraw;
							$data1=array(														 
								'timeAdded'=>date('Y-m-d H:i:s'),
								'cd_id'=>$id,
								'prevAmt'=>$sd,
								'wAmt'=>$inputValue,
								'cdLog_id'=>$this->session->userdata('pre_log_key'),
								'reason'=>$note,
								'user_id'=>$this->session->userdata('user_id'),
								'hotel_id'=>$this->session->userdata('user_hotel')
								
                            );
					
						
					$query1=$this->unit_class_model->cd_withdraw($data1);

















		$cl='leaveDateTime';
		if($query){
			$this->cashDrawerLog($id,$cl);
		$data=array(
		'msg'=>"Cashdrawer Close",
		'status'=>1
			
		);
		}else{
			$data=array(
		'msg'=>"Cashdrawer Not Close",
		'status'=>0
		);
		}
		header('Content-Type: application/json');
        echo json_encode($data);
	}
	
	 function all_cashdrawer_log()
    {

			 $found = in_array("25",$this->arraay);
		if($found) {
            $data['heading'] = 'Cashdrawer';
            $data['sub_heading'] = 'All Cashdrawer';
            $data['description'] = 'Cashdrawer Log';
            $data['active'] = 'setting';
			$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			
            //$data['bookings'] = $this->dashboard_model->all_bookings($config['per_page'], $segment);
			$data['cashdrawer'] = $this->unit_class_model->all_cashdrawer_log();
            $data['transactions'] = $this->dashboard_model->all_transactions();
            //$data['pagination'] = $this->pagination->create_links();
			//echo "<pre>";
			//print_r($this->session->userdata());
            $data['page'] = 'backend/dashboard/all_cashdrawer_log';
            $this->load->view('backend/common/index', $data);

        } else {
            redirect('dashboard');
        }
    }
	
	function all_cashdrawer()
    {

			 $found = in_array("25",$this->arraay);
		if($found) {
            $data['heading'] = 'Cashdrawer';
            $data['sub_heading'] = 'All Cashdrawer';
            $data['description'] = 'Cashdrawer ';
            $data['active'] = 'setting';
			$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			
            //$data['bookings'] = $this->dashboard_model->all_bookings($config['per_page'], $segment);
			$data['drawers'] = $this->unit_class_model->all_cashdrawer();
			//print_r($data['drawers']);exit;
            $data['transactions'] = $this->dashboard_model->all_transactions();
            //$data['pagination'] = $this->pagination->create_links();

            $data['page'] = 'backend/dashboard/all_cashdrawer';
            $this->load->view('backend/common/index', $data);

        } else {
            redirect('dashboard');
        }
    }
	
	
	function withdraw_cashdrawer(){
		
		date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
		 $found ='1';
		if($found) {
		$this->load->model('unit_class_model');
        if($this->input->post())
                {   
			
					$withdraw=$this->input->post('withdraw');
					$note=$this->input->post('note');
					$id=$this->input->post('id');
				
					$hotel_id=$this->session->userdata('user_hotel');
					$user_id=$this->session->userdata('user_id');
					
					if($user_id > 0){
						$user = $this->dashboard_model->get_admin($user_id);
					
						if(isset($user)){
							foreach($user as $user){
								$nam=$user->admin_first_name;
								//print_r($user);
							}
						}
					}
					 $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					   $this->db1->where('drawer_id',$id);
					   $query1=$this->db1->get(TABLE_PRE.'cash_drawer');
					   $query1=$query1->row();
					   
	   
	   $wid=$query1->withdraw+$withdraw;
					
						 $data=array(														 
								'withdraw'=>$wid,
								
                            );
							$sd=$query1->cash_collection+$query1->deposit-$query1->withdraw;
							$data1=array(														 
								'timeAdded'=>date('Y-m-d H:i:s'),
								'cd_id'=>$id,
								'prevAmt'=>$sd,
								'wAmt'=>$withdraw,
								'cdLog_id'=>$this->session->userdata('pre_log_key'),
								'reason'=>$note,
								'user_id'=>$this->session->userdata('user_id'),
								'hotel_id'=>$this->session->userdata('user_hotel')
								
                            );
					
						
					$query1=$this->unit_class_model->cd_withdraw($data1);
					$query=$this->unit_class_model->edit_cash($data,$id);
					
					}
		}
	if($query){
		$data=1;
		header('Content-Type: application/json');
        echo json_encode($data);
	}
	}
	
	
	
	

}