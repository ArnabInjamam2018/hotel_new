<?php

/*
 Created by The Infinity Tree.
 User: samB
*/

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        /* Session Checking Start */
        parent::__construct();
		 if (!$this->session->userdata('is_logged_in')) {
            redirect('login');
        }
		//$this->load->library('multiple');
		$db['superadmin'] = array(
			'dsn'   => '',
			'hostname' => 'localhost',
			'username' => $this->session->userdata('username'),
			'password' => $this->session->userdata('password'),
			'database' => $this->session->userdata('database'),
			'dbdriver' => 'mysqli',
			'dbprefix' => '',
			'pconnect' => FALSE,
			'db_debug' => TRUE,
			'cache_on' => FALSE,
			'cachedir' => '',
			'char_set' => 'utf8',
			'dbcollat' => 'utf8_general_ci',
			'swap_pre' => '',
			'encrypt' => FALSE,
			'compress' => FALSE,
			'stricton' => FALSE,
			'failover' => array(),
			'save_queries' => TRUE 
		);

        /*echo "<pre>";
		echo $this->db1->database;
        print_r($db1['default']);exit;*/

        $CI = &get_instance();
        $this->db1 = $CI->load->database($db['superadmin'],true);

        $this->arraay = $this->session->userdata('per');
        $this->load->model('superadminmodel/account_model');
        $this->load->model('bookings_model');
        $this->load->model('unit_class_model');
        $this->load->model('setting_model');
        $this->load->model('reports_model');
        $this->load->model('setting_model');

        /* Session Checking End */

        /* URL Value Encryption Start */

        function base64url_encode($data) {
            return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
        }

        /* URL Value Encryption End */

        /* URL Value Decryption Start */

        function base64url_decode($data) {
            return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
        }

        /* URL Value Decryption End */
    }

    /* Dashboard Page Start */

    function index() {
		
		//echo "<pre>";
		
        $id = $this->session->userdata('user_id');
        $hotel_id = $this->session->userdata('user_hotel');
        //echo $hotel_id.''.$id;exit;
        $data['admin_status'] = $this->dashboard_model->admin_status($id);
        //echo $id."=====".$this->session->userdata('user_name');exit;
        $hotelnames = $this->dashboard_model->get_hotel_name1($this->session->userdata('user_hotel'), 1);
        if (isset($hotelnames->hotel_name)) {
            $hot_name = $hotelnames->hotel_name;
        } else {
            $hot_name = '';
        }
        //$hot_name = 
        $data['heading'] = "Dashboard ";
        $data['description'] = $hot_name . '<div class="pull-right">Welcome ' . $this->session->userdata('user_name') . ' </div> ';
        $data['active'] = 'dashboard';
        //print_r($this->session->userdata);exit;
        $data['bookings'] = $this->dashboard_model->all_bookings($hotel_id);
        //print_r($data['bookings']);exit;
		
        $data['recent_bookings'] = $this->dashboard_model->all_recent_bookings($hotel_id);
		//echo "<pre>"; print_r($data['tasks_pending']); exit();
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
      //  $data['unique_room_bookings'] = $this->dashboard_model->all_bookings_unique($hotel_id);  
          $data['unique_room_bookings'] = $this->dashboard_model->all_bookings_unique_Today($hotel_id);
          
          if(isset($data['unique_room_bookings']) && $data['unique_room_bookings']){
			
		foreach($data['unique_room_bookings'] as $row){
			/*$Tot_Rev+=$row->Totsum;
			 $cashP+=$row->CashP;
			$cardP+=$row->CardP;
			$FundP+=$row->FundP;
			$chqP+=$row->chqP;
			$ewaP+=$row->ewaP;
			$draP+=$row->draP;*/
			$data['Tot_Rev']+=$row->Totsum;
		$data['cashP']+=$row->CashP;
		$data['bankP']+=$row->CardP+$row->FundP+$row->chqP+$row->ewaP+$row->draP;
		
		}
		}
          
          
        $data['all_rooms'] = $this->dashboard_model->count_rooms($hotel_id);
		$data['tasks_pending'] = $this->dashboard_model->tasks_pending($hotel_id);
		
        //$data['guests'] = $this->dashboard_model->all_guest_limit_view();
        //$data['tasks_assign'] = $this->dashboard_model->task_assiger($hotel_id);
        //$data['tasks_assigee'] = $this->dashboard_model->task_assiger($hotel_id);
		
				/*echo "<pre>";
                print_r($data['guests']);
				if ( isset( $data['tasks_assign'] ) && $data['tasks_assign'] ){
					foreach ( $data['tasks_assign'] as $tasks_assign ){
																			
						echo $tasks_assign->admin_id.'. '.$tasks_assign->admin_first_name .' '. $tasks_assign->admin_middle_name .' '. $tasks_assign->admin_last_name.' </br>'; 
					}
			    }
                exit();*/
				
		//echo "<pre>";		
		//print_r($data['tasks_pending']);
		//exit();
		
		$data['page'] = 'backend/dashboard/index';
		
        if ($this->input->post('hotel_image_upload')) {
            $this->upload->do_upload('hotel_image_upload');
            $this->upload->display_errors();
            //exit();
            $this->upload->initialize($this->set_upload_options());
            if ($this->upload->do_upload('hotel_image_upload')) {
                $s_image = $this->upload->data('file_name');
                $filename = $s_image;


                $source_path = './upload/' . $filename;
                $target_path = './upload/hotel/admin/';

                $config_manip = array(
                    //'file_name' => 'myfile',
                    'image_library' => 'gd2',
                    'source_image' => $source_path,
                    'new_image' => $target_path,
                    'maintain_ratio' => TRUE,
                    'create_thumb' => TRUE,
                    'thumb_marker' => '_thumb',
                    'width' => 100,
                    'height' => 100
                );
                $this->load->library('image_lib', $config_manip);

                if (!$this->image_lib->resize()) {
                    //$this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                    echo $this->image_lib->display_errors();
                    exit();
                    // redirect('common/index');
                } else {
                    $thumb_name = explode(".", $filename);
                    $hotel_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();


                    $data = array(
                        'hotel_logo_images_thumb' => $filename,
                    );
                }

                $query = $this->dashboard_model->edit_hotel_details($data, $this->session->userdata('user_hotel'));
                if ($query) {
                    $this->session->set_flashdata('succ_msg', 'Upload Sucessfully');
                } else {
                    $this->session->set_flashdata('err_msg', 'Database Error');
                }
                $data['page'] = 'backend/dashboard/index';
                $id = $this->session->userdata('user_id');
                $data['admin_status'] = $this->dashboard_model->admin_status($id);

                $data['heading'] = "Dashboard";
                $data['description'] = 'Welcome2 ' . $this->session->userdata('user_name') . ' to Hotel Objects Dashboard';
                $data['active'] = 'dashboard';
                $data['page'] = 'backend/dashboard/index';

                $data['bookings'] = $this->dashboard_model->all_bookings($hotel_id);
                $data['recent_bookings'] = $this->dashboard_model->all_recent_bookings($hotel_id);
                $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
                $data['unique_room_bookings'] = $this->dashboard_model->all_bookings_unique($hotel_id);
                $data['tasks_assign'] = $this->dashboard_model->task_assiger($hotel_id);

                
                $data['all_rooms'] = $this->dashboard_model->all_rooms($hotel_id);
               // echo "<pre>";
               // print_r($data);
                //echo $data['unique_room_bookings'];
               // exit();
                $data['guests'] = $this->dashboard_model->all_guest_limit_view();
                $this->load->view('backend/common/index', $data);
            }
        } else {
            
		       // echo "<pre>";
               // print_r($data);
               // echo $data['unique_room_bookings'];
               // exit();
            $this->load->view('backend/common/index', $data);
        }
    }
	
	function minimal() {
		
		//echo "<pre>";
		//print_r($this->session->userdata);exit;
        $id = $this->session->userdata('user_id');
        $hotel_id = $this->session->userdata('user_hotel');
        //echo $hotel_id.''.$id;exit;
        $data['admin_status'] = $this->dashboard_model->admin_status($id);
        //echo $id."=====".$this->session->userdata('user_name');exit;
        $hotelnames = $this->dashboard_model->get_hotel_name1($this->session->userdata('user_hotel'), 1);
        if (isset($hotelnames->hotel_name)) {
            $hot_name = $hotelnames->hotel_name;
        } else {
            $hot_name = '';
        }
        //$hot_name = 
        $data['heading'] = "Dashboard ";
        $data['description'] = $hot_name . '<div class="pull-right">Welcome ' . $this->session->userdata('user_name') . ' </div> ';
        $data['active'] = 'dashboard';
        $data['page'] = 'backend/dashboard/index_backup';
        
        $this->load->view('backend/common/index', $data);

    }

    /* Dashboard Page End */

    /* Logout Start */

    function logout() {	//	session_destroy();		
        $this->dashboard_model->remove_online($this->session->userdata('user_id'));
        $this->dashboard_model->remove_log($this->session->userdata('shift_id'));
        //UPDATE LOGIN_SESSION TABLE						
        $t = 1;
		if($t == 1){
			$session_data = array(
            'user_id' => $this->session->userdata('user_id'),
            'logout_dateTime' => date('Y-m-d H:i:s'),
			'status' => 'Logout'
			//'hls'=>$_SESSION['mach_id']
			);
		}
		else{
			$session_data = array(
            'user_id' => $this->session->userdata('user_id'),
            'logout_dateTime' => date('Y-m-d H:i:s'),
			'status' => 'Logout',
			'dubLogout' => '1',
			//'hls'=>$_SESSION['mach_id']
			);
		}
		
        if (isset($_SESSION['mach_id'])) { 
            $login_session_data = $this->login_model->update_login_session($session_data, $_SESSION['mach_id']);
            date_default_timezone_set('Asia/Kolkata');
            $d = date('Y-m-d H:i:s');
            $data2 = array(
                'leaveDateTime' => $d,
                    
            );
			
            //$this->unit_class_model->update_cash_drawer_log($data2, $this->session->userdata('pre_log_key_key'));
        }

        $this->session->sess_destroy();
        redirect('login/logged_out');
    }
	
	
	
	function logoutD() {

        $this->dashboard_model->remove_online($this->session->userdata('user_id'));
        $this->dashboard_model->remove_log($this->session->userdata('shift_id'));

        //UPDATE LOGIN_SESSION TABLE						
        $t = 0;
		if($t == 1){
			$session_data = array(
            'user_id' => $this->session->userdata('user_id'),
            'logout_dateTime' => date('Y-m-d H:i:s'),
			'status' => 'Logout'
			);
		}
		else{
			$session_data = array(
            'user_id' => $this->session->userdata('user_id'),
            'logout_dateTime' => date('Y-m-d H:i:s'),
			'status' => 'Logout',
			'dubLogout' => '1'
			);
		}
		//echo "<pre>";					
          //  print_r($this->session->userdata());exit;
        if (isset($_SESSION['mach_id'])) {
            $login_session_data = $this->login_model->update_login_session($session_data, $_SESSION['mach_id']);
            date_default_timezone_set('Asia/Kolkata');
            $d = date('Y-m-d H:i:s');
            $data2 = array(
                'leaveDateTime' => $d,
                    //'id'=>$this->session->userdata('pre_log_key')	
            );
            
            $this->unit_class_model->update_cash_drawer_log($data2, $this->session->userdata('pre_log_key'));
        }

        $this->session->sess_destroy();
        redirect('login/logged_out');
		header("Refresh:0");
		echo 'Refresh the page!';
    }

    /* Logout End */

    function add_task() {
        if ($this->input->post()) {
            $task_entry = array(
                'title' => $this->input->post('title'),
                'from_id' => $this->input->post('asign_from'),
                'to_id' => $this->input->post('asign_to'),
                'task' => $this->input->post('task_desc'),
                'status' => '0',
                'priority' => $this->input->post('task_priority'),
                'due_date' => date("y-m-d", strtotime($this->input->post('due_date'))),
                'added_date' => date("Y-m-d H:i:s"),
                'hotel_id' => $this->session->userdata('user_hotel'),
                'user_id' => $this->session->userdata('user_id')
            );

            $query = $this->dashboard_model->task_add($task_entry);
            redirect('dashboard/all_tasks');
        }
    }

    function complete_task() {
        $task_id = $_GET['task_id'];
        $query = $this->dashboard_model->task_complete($task_id);
        redirect('dashboard/task_details?task_id='.$task_id);
    }
	
	function reopen_task() {
        $task_id = $_GET['task_id'];
        $query = $this->dashboard_model->task_reopen($task_id);
        redirect('dashboard/task_details?task_id='.$task_id);
    }

    /* Add Admin Start */

    function add_admin() {

        $found = in_array("1", $this->arraay);
        if ($found) {
            $data['heading'] = 'Admin';
            $data['sub_heading'] = 'Add Admin';
            $data['description'] = 'Add Admin Here';
            $data['active'] = 'add_admin';
            $data['hotel'] = $this->dashboard_model->all_hotel_list();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $permission_users = '2';
            $data['permission'] = $this->dashboard_model->admin_permission_label($permission_users);
            if ($this->input->post()) {
                $table = TABLE_PRE . 'admin_details';
                $table1 = TABLE_PRE . 'login';
                //  $this->form_validation->set_rules('admin_hotel', 'Hotel Name', 'trim|required|xss_clean');
                // $this->form_validation->set_rules('admin_first_name', 'First Name', 'trim|required|xss_clean');
                // $this->form_validation->set_rules('admin_middle_name', 'Middle Name', 'trim|xss_clean');
                // $this->form_validation->set_rules('admin_last_name', 'Last Name', 'trim|required|xss_clean');
                // $this->form_validation->set_rules('admin_email', 'Email Address', 'trim|required|valid_email|xss_clean|is_unique[' . $table . '.admin_email]');
                //$this->form_validation->set_rules('admin_username', 'Username', 'trim|required|xss_clean|min_length[5]|max_length[20]|is_unique[' . $table1 . '.user_name]');
                // $this->form_validation->set_rules('admin_password', 'Password', 'trim|required|xss_clean|min_length[5]|max_length[50]');
                // $this->form_validation->set_rules('admin_retype_password', 'Retype Password', 'trim|required|xss_clean|min_length[5]|max_length[50]|matches[admin_password]');
                // $this->form_validation->set_rules('permission[]', 'Permission', 'required');

                date_default_timezone_set("Asia/Kolkata");
                if ($this->session->userdata('user_type_slug') == "SUPA") {
                    $admin_hotel = $this->input->post('admin_hotel');
                } else {

                    $admin_hotel = $this->session->userdata('user_hotel');
                }

                if ($this->session->userdata('user_type_slug') == "SUPA") {
                    $type1 = 2;
                } elseif ($this->session->userdata('user_type_slug') == "AD") {
                    $type1 = 3;
                }
                $admin = array(
                    'admin_hotel' => $admin_hotel,
                    'admin_first_name' => $this->input->post('admin_first_name'),
                    'admin_middle_name' => $this->input->post('admin_middle_name'),
                    'admin_last_name' => $this->input->post('admin_last_name'),
                    'admin_email' => $this->input->post('admin_email'),
                    'admin_user_type' => $type1,
                    'admin_create_date' => date("Y-m-d H:i:s"),
                    'admin_modification_date' => date("Y-m-d H:i:s"),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
                $query1 = $this->dashboard_model->add_admin($admin);

                if ($this->session->userdata('user_type_slug') == "SUPA") {
                    $type = 2;
                } elseif ($this->session->userdata('user_type_slug') == "AD") {
                    $type = 3;
                }

                if (isset($query1) && $query1) {
                    $user = array(
                        'user_id' => $query1,
                        'hotel_id' => $admin_hotel,
                        'user_name' => $this->input->post('admin_username'),
                        'user_password' => sha1(md5($this->input->post('admin_password'))),
                        'user_email' => $this->input->post('admin_email'),
                        'user_type_id' => $type,
                        'user_status' => 'A',
                        'hotel_id' => $this->session->userdata('user_hotel'),
                    );
                    $query2 = $this->dashboard_model->add_user($user);

                    $hotel = array(
                        'hotel_id' => $this->input->post('admin_hotel'),
                        'hotel_status' => 'A'
                    );
                    $update = $this->dashboard_model->update_hotel($hotel);

                    $permission_id = implode(",", $this->input->post('permission'));
                    if ($this->session->userdata('user_type_slug') == "SUPA") {
                        $type3 = "AD";
                    } elseif ($this->session->userdata('user_type_slug') == "AD") {
                        $type3 = "SUB";
                    }
                    $permission = array(
                        'user_id' => $query1,
                        'user_type' => $type3,
                        'user_permission_type' => $permission_id
                    );
                    $user_permission = $this->dashboard_model->add_user_permission($permission);
                }

                if (isset($query2) && $query2) {
                    $this->session->set_flashdata('succ_msg', "Admin Added Successfully!");
                    $msg = "<p>Hello</p>
							<p>You are registered with Hotel Objects. Please click here to activate your account:-" . base_url() . "login/activate_admin/" . base64url_encode($query2) . "</p>
							<p>Username:" . $this->input->post('admin_username') . "</p>
							<p>Password:" . $this->input->post('admin_password') . "</p>
							<p>--Hotel Object Team</p>";
                    $config['protocol'] = 'smtp';
                    $config['smtp_host'] = 'smtp.webartmedialabs.com';
                    $config['mailtype'] = 'html';
                    $this->email->initialize($config);
                    $this->email->from('sayak@webartmedialabs.com', 'Hotel Objects');
                    $this->email->to($this->input->post('admin_email'), $this->input->post('admin_first_name'));
                    $this->email->subject('Registering With Hotel Objects');
                    $this->email->message($msg);
                    if ($this->email->send()) {
                        redirect('dashboard/add_admin');
                    } else {
                        $this->session->set_flashdata('err_msg', $this->email->print_debugger());
                        redirect('dashboard/add_admin');
                    }
                    redirect('dashboard/add_admin');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_admin');
                }
            } else {
                $data['page'] = 'backend/dashboard/add_admin';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Add Admin End */

    /* Ajax Username Checking During User Addition Start */

    function username_check() {
        $username['user_name'] = $this->input->post('username');
        $query = $this->dashboard_model->username_check($username);
        if (isset($query) && $query) {
            die('<img src=' . base_url() . 'assets/dashboard/images/dislike-icon.png style="height:40px;">');
        } else {
            die('<img src=' . base_url() . 'assets/dashboard/images/like-icon.png style="height:40px;">');
        }
    }

    /* Ajax Username Checking During User Addition End */

    /* Ajax Email Checking During User Addition Start */

    function email_check() {
        $email['user_email'] = $this->input->post('email');
        $query = $this->dashboard_model->email_check($email);
        if (isset($query) && $query) {
            die('<img src=' . base_url() . 'assets/dashboard/images/dislike-icon.png style="height:40px;">');
        } else {
            die('<img src=' . base_url() . 'assets/dashboard/images/like-icon.png style="height:40px;">');
        }
    }

    /* Ajax Email Checking During User Addition End */

    /* Ajax Email Checking During Admin Registration Start */

    function admin_email_check() {
        $email['user_email'] = $this->input->post('email');
        if ($this->input->post('email') == $this->input->post('hemail')) {
            die('<img src=' . base_url() . 'assets/dashboard/images/like-icon.png style="height:40px;">');
        } else {
            $query = $this->dashboard_model->email_check($email);
            if (isset($query) && $query) {
                die('<img src=' . base_url() . 'assets/dashboard/images/dislike-icon.png style="height:40px;">');
            } else {
                die('<img src=' . base_url() . 'assets/dashboard/images/like-icon.png style="height:40px;">');
            }
        }
    }

    /* Ajax Email Checking During Admin Registration End */

    /* Admin Registration Start */

    function admin_registration() {
        if ($this->session->userdata('admin') == '1') {
            $table = TABLE_PRE . 'login';
            $this->form_validation->set_rules('admin_first_name', 'First Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('admin_first_name', 'Middle Name', 'trim|xss_clean');
            $this->form_validation->set_rules('admin_last_name', 'Last Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('admin_address', 'Address', 'trim|required|xss_clean');
            $this->form_validation->set_rules('admin_city', 'City', 'trim|required|xss_clean');
            $this->form_validation->set_rules('admin_district', 'District', 'trim|required|xss_clean');
            $this->form_validation->set_rules('admin_state', 'State', 'trim|required|xss_clean');
            $this->form_validation->set_rules('admin_country', 'Country', 'trim|required|xss_clean');
            $this->form_validation->set_rules('admin_pin', 'Pin', 'trim|required|xss_clean');
            if ($this->input->post('admin_email') == $this->input->post('admin_email_hidden')) {
                $this->form_validation->set_rules('admin_email', 'Email Address', 'trim|required|valid_email|xss_clean');
            } else {
                $this->form_validation->set_rules('admin_email', 'Email Address', 'trim|required|valid_email|xss_clean|is_unique[' . $table . '.user_email]');
            }
            $this->form_validation->set_rules('admin_phone1', 'Phone Number', 'trim|required|numeric|max_length[10]|xss_clean');
            $this->form_validation->set_rules('admin_phone2', 'Alternative Phone Number', 'trim|numeric|max_length[10]|xss_clean');
            $this->form_validation->set_rules('admin_dob', 'Date of Birth', 'trim|required|xss_clean');
            $this->form_validation->set_rules('admin_mother_tongue', 'Mother Tongue', 'trim|required|xss_clean');
            $this->form_validation->set_rules('admin_comm_language', 'Mother Tongue', 'trim|required|xss_clean');
            $this->form_validation->set_rules('admin_idproof1_type', 'ID Proof 1', 'trim|xss_clean');
            $this->form_validation->set_rules('admin_idproof2_type', 'ID Proof 2', 'trim|xss_clean');
            $this->form_validation->set_rules('admin_idproof3_type', 'ID Proof 3', 'trim|xss_clean');

            $admin_idproof1_path = $admin_idproof2_path = $admin_idproof3_path = '';

            $this->upload->initialize($this->set_upload_options());

            if ($this->input->post('admin_idproof1_type')) {
                if ($this->upload->do_upload('userdata1')) {
                    $admin_idproof1_path = $this->upload->data('file_name');
                } else {
                    $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    redirect('dashboard');
                }
            }

            if ($this->input->post('admin_idproof2_type')) {
                if ($this->upload->do_upload('userdata2')) {
                    $admin_idproof2_path = $this->upload->data('file_name');
                } else {
                    $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    redirect('dashboard');
                }
            }

            if ($this->input->post('admin_idproof3_type')) {
                if ($this->upload->do_upload('userdata3')) {
                    $admin_idproof3_path = $this->upload->data('file_name');
                } else {
                    $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    redirect('dashboard');
                }
            }

            if ($this->upload->do_upload('image')) {
                $admin_image = $this->upload->data('file_name');
            } else {
                $admin_image = "user.png";
            }

            date_default_timezone_set("Asia/Kolkata");

            $var = $this->input->post('admin_dob');
            if (strpos(".", $var)) {
                $date = str_replace('.', '-', $var);
            } else {
                if (strpos("/", $var)) {
                    $date = str_replace('/', '-', $var);
                } else {
                    $date = $var;
                }
            }
            $date = date('Y-m-d', strtotime($date));

            $admin = array(
                'admin_id' => $this->session->userdata('user_id'),
                'admin_first_name' => $this->input->post('admin_first_name'),
                'admin_middle_name' => $this->input->post('admin_middle_name'),
                'admin_last_name' => $this->input->post('admin_last_name'),
                'admin_email' => $this->input->post('admin_email'),
                'admin_address' => $this->input->post('admin_address'),
                'admin_city' => $this->input->post('admin_city'),
                'admin_district' => $this->input->post('admin_district'),
                'admin_state' => $this->input->post('admin_state'),
                'admin_country' => $this->input->post('admin_country'),
                'admin_pin' => $this->input->post('admin_pin'),
                'admin_phone1' => $this->input->post('admin_phone1'),
                'admin_phone2' => $this->input->post('admin_phone2'),
                'admin_dob' => $date,
                'admin_mother_tongue' => $this->input->post('admin_mother_tongue'),
                'admin_comm_language' => $this->input->post('admin_comm_language'),
                'admin_idproof1_type' => $this->input->post('admin_idproof1_type'),
                'admin_idproof1_path' => $admin_idproof1_path,
                'admin_idproof2_type' => $this->input->post('admin_idproof2_type'),
                'admin_idproof2_path' => $admin_idproof2_path,
                'admin_idproof3_type' => $this->input->post('admin_idproof3_type'),
                'admin_idproof3_path' => $admin_idproof3_path,
                'admin_image' => $admin_image,
                'admin_modification_date' => date("Y-m-d H:i:s"),
                'admin_status' => 'A'
            );

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('dashboard');
            } else {
                $update = $this->dashboard_model->admin_registration($admin);
                $login_data = array(
                    'login_id' => $this->session->userdata('login_id'),
                    'user_email' => $this->input->post('admin_email')
                );
                $login = $this->dashboard_model->update_login($login_data);
                if (isset($update) && $update && isset($login) && $login) {
                    $this->session->set_flashdata('succ_msg', "Your registration completed successfully.");
                } else {
                    $this->session->set_flashdata('err_msg', "Your registration is incomplete. Try again!");
                }
                redirect('dashboard');
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Admin Registration End */

    /* Add Hotel Start */

    function add_hotel_m() {

        $found = in_array("37", $this->arraay);
        if ($found) {
            $data['heading'] = 'Setting';
            $data['sub_heading'] = 'Hotel Management';
            $data['description'] = 'Add Hotel Here';
            $data['active'] = 'add_hotel_m';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                $hotel_type = $this->input->post('hotel_type');
                $hotel_type_string = "";
                $hotel_type_string = $hotel_type_string . implode(',', $hotel_type);

                $guest_option = $this->input->post('guest');
                $guest_option_string = "";
                $guest_option_string = $guest_option_string . implode(',', $guest_option);

                $broker_option = $this->input->post('broker');
                $broker_option_string = "";
                $broker_option_string = $broker_option_string . implode(',', $broker_option);
                /* Start Logo Upload */

                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('image_photo')) {
                    //echo "uploaded"; exit();
                    $hotel_logo = $this->upload->data('file_name');
                    $filename = $hotel_logo;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/hotel/';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 100,
                        'height' => 100
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_hotel_m');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $logo_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                } else {
                    redirect('dashboard/add_hotel_m');
                }
                /* End Logo Upload */
                //exit();
                date_default_timezone_set("Asia/Kolkata");
                $hotel = array(
                    'hotel_name' => $this->input->post('hotel_name'),
                    'hotel_year_established' => $this->input->post('hotel_year_established'),
                    'hotel_ownership_type' => $this->input->post('hotel_ownership_type'),
                    'hotel_type' => $hotel_type_string,
                    'hotel_rooms' => $this->input->post('hotel_rooms'),
                    'hotel_floor' => $this->input->post('hotel_floor'),
                    'hotel_logo_images' => $hotel_logo,
                    'hotel_logo_images_thumb' => $logo_thumb,
                    'hotel_logo_text' => $this->input->post('images_text'),
                    'hotel_status' => 'A',
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );

                $hotel_contact_details = array(
                    'hotel_street1' => $this->input->post('hotel_street1'),
                    'hotel_street2' => $this->input->post('hotel_street2'),
                    'hotel_landmark' => $this->input->post('hotel_landmark'),
                    'hotel_district' => $this->input->post('hotel_district'),
                    'hotel_pincode' => $this->input->post('hotel_pincode'),
                    'hotel_state' => $this->input->post('hotel_state'),
                    'hotel_country' => $this->input->post('hotel_country'),
                    'hotel_branch_street1' => $this->input->post('hotel_branch_street1'),
                    'hotel_branch_street2' => $this->input->post('hotel_branch_street2'),
                    'hotel_branch_landmark' => $this->input->post('hotel_branch_landmark'),
                    'hotel_branch_district' => $this->input->post('hotel_branch_district'),
                    'hotel_branch_pincode' => $this->input->post('hotel_branch_pincode'),
                    'hotel_branch_state' => $this->input->post('hotel_branch_state'),
                    'hotel_branch_country' => $this->input->post('hotel_branch_country'),
                    'hotel_booking_street1' => $this->input->post('hotel_booking_street1'),
                    'hotel_booking_street2' => $this->input->post('hotel_booking_street2'),
                    'hotel_booking_landmark' => $this->input->post('hotel_booking_landmark'),
                    'hotel_booking_district' => $this->input->post('hotel_booking_district'),
                    'hotel_booking_pincode' => $this->input->post('hotel_booking_pincode'),
                    'hotel_booking_state' => $this->input->post('hotel_booking_state'),
                    'hotel_booking_country' => $this->input->post('hotel_booking_country'),
                    'hotel_frontdesk_name' => $this->input->post('hotel_frontdesk_name'),
                    'hotel_frontdesk_mobile' => $this->input->post('hotel_frontdesk_mobile'),
                    'hotel_frontdesk_mobile_alternative' => $this->input->post('hotel_frontdesk_mobile_alternative'),
                    'hotel_frontdesk_email' => $this->input->post('hotel_frontdesk_email'),
                    'hotel_owner_name' => $this->input->post('hotel_owner_name'),
                    'hotel_owner_mobile' => $this->input->post('hotel_owner_mobile'),
                    'hotel_owner_mobile_alternative' => $this->input->post('hotel_owner_mobile_alternative'),
                    'hotel_owner_email' => $this->input->post('hotel_owner_email'),
                    'hotel_hr_name' => $this->input->post('hotel_hr_name'),
                    'hotel_hr_mobile' => $this->input->post('hotel_hr_mobile'),
                    'hotel_hr_mobile_alternative' => $this->input->post('hotel_hr_mobile_alternative'),
                    'hotel_hr_email' => $this->input->post('hotel_hr_email'),
                    'hotel_accounts_name' => $this->input->post('hotel_accounts_name'),
                    'hotel_accounts_mobile' => $this->input->post('hotel_accounts_mobile'),
                    'hotel_accounts_mobile_alternative' => $this->input->post('hotel_accounts_mobile_alternative'),
                    'hotel_accounts_email' => $this->input->post('hotel_accounts_email'),
                    'hotel_near_police_name' => $this->input->post('hotel_near_police_name'),
                    'hotel_near_police_mobile' => $this->input->post('hotel_near_police_mobile'),
                    'hotel_near_police_mobile_alternative' => $this->input->post('hotel_near_police_mobile_alternative'),
                    'hotel_near_police_email' => $this->input->post('hotel_near_police_email'),
                    'hotel_near_medical_name' => $this->input->post('hotel_near_medical_name'),
                    'hotel_near_medical_mobile' => $this->input->post('hotel_near_medical_mobile'),
                    'hotel_near_medical_mobile_alternative' => $this->input->post('hotel_near_medical_mobile_alternative'),
                    'hotel_near_medical_email' => $this->input->post('hotel_near_medical_email'),
                    'hotel_fax' => $this->input->post('hotel_fax'),
                    'hotel_website' => $this->input->post('hotel_website'),
                    'hotel_working_from' => '',
                    'hotel_working_upto' => '',
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );

                $hotel_tax_details = array(
                    'hotel_tax_applied' => $this->input->post('hotel_tax_applied'),
                    'hotel_service_tax' => $this->input->post('hotel_service_tax'),
                    'hotel_luxury_tax' => $this->input->post('hotel_luxury_tax'),
                    'hotel_service_charge' => $this->input->post('hotel_service_charge'),
                    'hotel_stander_tac' => $this->input->post('hotel_stander_tac'),
                    'hotel_vat_tax' => $this->input->post('hotel_vat_tax'),
                    'hotel_tax_food_vat' => $this->input->post('hotel_tax_food_vat'),
                    'hotel_tax_liquor_vat' => $this->input->post('hotel_tax_liquor_vat'),
                    'luxury_tax_slab1_range_to' => $this->input->post('luxury_tax_slab1_range_to'),
                    'luxury_tax_slab1_range_from' => $this->input->post('luxury_tax_slab1_range_from'),
                    'luxury_tax_slab1' => $this->input->post('luxury_tax_slab1'),
                    'luxury_tax_slab2_range_to' => $this->input->post('luxury_tax_slab2_range_to'),
                    'luxury_tax_slab2_range_from' => $this->input->post('luxury_tax_slab2_range_from'),
                    'luxury_tax_slab2' => $this->input->post('luxury_tax_slab2'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );

                $hotel_billing_settings = array(
                    'billing_name' => $this->input->post('billing_name'),
                    'billing_street1' => $this->input->post('billing_street1'),
                    'billing_street2' => $this->input->post('billing_street2'),
                    'billing_landmark' => $this->input->post('billing_landmark'),
                    'billing_district' => $this->input->post('billing_district'),
                    'billing_pincode' => $this->input->post('billing_pincode'),
                    'billing_state' => $this->input->post('billing_state'),
                    'billing_country' => $this->input->post('billing_country'),
                    'billing_email' => $this->input->post('billing_email'),
                    'billing_phone' => $this->input->post('billing_phone'),
                    'billing_fax' => $this->input->post('billing_fax'),
                    'billing_vat' => $this->input->post('billing_vat'),
                    'billing_bank_name' => $this->input->post('billing_bank_name'),
                    'billing_account_no' => $this->input->post('billing_account_no'),
                    'billing_ifsc_code' => $this->input->post('billing_ifsc_code'),
                    'luxury_tax_reg_no' => $this->input->post('luxury_tax_reg_no'),
                    'service_tax_no' => $this->input->post('service_tax_no'),
                    'vat_reg_no' => $this->input->post('vat_reg_no'),
                    'cin_no' => $this->input->post('cin_no'),
                    'tin_no' => $this->input->post('tin_no'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
                $hotel_general_preference = array(
                    'hotel_check_in_time_hr' => $this->input->post('hotel_check_in_time_hr'),
                    'hotel_check_in_time_mm' => $this->input->post('hotel_check_in_time_mm'),
                    'hotel_check_in_time_fr' => $this->input->post('hotel_check_in_time_fr'),
                    'hotel_check_out_time_hr' => $this->input->post('hotel_check_out_time_hr'),
                    'hotel_check_out_time_mm' => $this->input->post('hotel_check_out_time_mm'),
                    'hotel_check_out_time_fr' => $this->input->post('hotel_check_out_time_fr'),
                    'hotel_guest' => $guest_option_string,
                    'hotel_broker' => $broker_option_string,
                    //'hotel_buffer_hour' => $this->input->post('buffer_hour'),
                    'hotel_broker_commission' => 0,
                    'hotel_date_format' => $this->input->post('hotel_date_format'),
                    'hotel_time_format' => $this->input->post('hotel_time_format'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );


                $query = $this->dashboard_model->add_hotel($hotel, $hotel_contact_details, $hotel_tax_details, $hotel_billing_settings, $hotel_general_preference);
                if ($query) {
                    $data['page'] = 'backend/dashboard/add_hotel_m';
                    $this->load->view('backend/common/index', $data);
                } else {
                    echo "php error";
                }
            } else {
                $data['country'] = $this->dashboard_model->get_country();
                $data['star'] = $this->dashboard_model->get_star_rating();
                $data['page'] = 'backend/dashboard/add_hotel_m';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Add Hotel End */

    /* Ajax Email Checking During Hotel Addition Start */

    function get_tax_details() {

        $data['tax'] = $this->dashboard_model->get_tax();
    }

    function hotel_email_check() {
        if ($this->session->userdata('admin') == '1') {
            $email['hotel_email'] = $this->input->post('email');
            $query = $this->dashboard_model->hotel_email_check($email);
            if (isset($query) && $query) {
                die('<img src=' . base_url() . 'assets/dashboard/images/dislike-icon.png style="height:40px;">');
            } else {
                die('<img src=' . base_url() . 'assets/dashboard/images/like-icon.png style="height:40px;">');
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Ajax Email Checking During Hotel Addition End */

    /* Ajax Phone No. Checking During Hotel Addition Start */

    function hotel_phone_check() {
        if ($this->session->userdata('admin') == '1') {
            $phone['hotel_phone'] = $this->input->post('phone');
            $query = $this->dashboard_model->hotel_phone_check($phone);
            if (isset($query) && $query) {
                die('<img src=' . base_url() . 'assets/dashboard/images/dislike-icon.png style="height:40px;">');
            } else {
                die('<img src=' . base_url() . 'assets/dashboard/images/like-icon.png style="height:40px;">');
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Ajax Phone No. Checking During Hotel Addition End */

    /* All Hotel Start */

    function all_hotel_m() {

        $found = in_array("37", $this->arraay);
        if ($found) {
            $data['heading'] = 'Setting';
            $data['sub_heading'] = 'Hotel Management';
            $data['description'] = 'Added Hotel List';
            $data['active'] = 'all_hotel_m';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            /* if ($this->input->post('pages')) {
              $pages = $this->input->post('pages');
              } else {
              if ($this->session->flashdata('pages')) {
              $pages = $this->session->flashdata('pages');
              } else {
              $pages = 5;
              }
              }

              $this->session->set_flashdata('pages', $pages);

              $config['base_url'] = base_url() . 'dashboard/all_hotel_m';
              $config['total_rows'] = $this->dashboard_model->all_hotels_row();
              $config['per_page'] = $pages;
              $config['uri_segment'] = 3;
              $config['full_tag_open'] = "<ul class='pagination'>";
              $config['full_tag_close'] = "</ul>";
              $config['num_tag_open'] = '<li>';
              $config['num_tag_close'] = '</li>';
              $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
              $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
              $config['next_tag_open'] = "<li>";
              $config['next_tagl_close'] = "</li>";
              $config['prev_tag_open'] = "<li>";
              $config['prev_tagl_close'] = "</li>";
              $config['first_tag_open'] = "<li>";
              $config['first_tagl_close'] = "</li>";
              $config['last_tag_open'] = "<li>";
              $config['last_tagl_close'] = "</li>";


              $this->pagination->initialize($config);

              $segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; */

            //$data['hotels'] = $this->dashboard_model->all_hotels($config['per_page'], $segment);
            //$data['pagination'] = $this->pagination->create_links();
            $data['hotels'] = $this->dashboard_model->all_hotels();
            //print_r($data);
            //exit();
            $data['page'] = 'backend/dashboard/all_hotel_m';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    /* All Hotel End */


    /* Delete Hotel Start */

    function hotel_delete() {
        if ($this->session->userdata('hotel_m') == '1') {
            $this->form_validation->set_rules('delete[]', 'Select Hotel', 'trim|required|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', validation_errors());
                redirect('dashboard/all_hotels');
            } else {
                $hotel_id = $this->input->post('delete');
                $hotel = array(
                    'hotel_status' => 'D'
                );
                $update = $this->dashboard_model->delete_hotel($hotel_id, $hotel);
                if (isset($update) && $update) {
                    $this->session->set_flashdata('succ_msg', "Hotels Deleted!");
                    redirect('dashboard/all_hotels');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try Again Later!");
                    redirect('dashboard/all_hotels');
                }
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Delete Hotel End */

    /* Ajax Hotel Update Status Start */

    function hotel_update_status() {

        $found = in_array("37", $this->arraay);
        if ($found) {
            $status = array(
                'hotel_id' => $this->input->post('id'),
                'hotel_status' => $this->input->post('value'),
            );
            $query = $this->dashboard_model->hotel_status_update($status);
            if (isset($query) && $query) {
                echo '<div class="alert alert-success alert-dismissible text-center" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Status Updated Successfully!</strong>
                                </div>';
            } else {
                echo '<div class="alert alert-danger alert-dismissible text-center" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Database Error. Try Again Later!</strong>
                                </div>';
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Ajax Hotel Update Status End */

    /* Add Permission Start */

    function add_permission() {
        if ($this->session->userdata('user_type_slug') !== 'SUPA') {
            $data['heading'] = 'Permissions';
            $data['sub_heading'] = 'Add Permission';
            $data['description'] = 'Add Access Permission Here';
            $data['active'] = 'add_permission';
            $data['users'] = $this->dashboard_model->get_user_type();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                $table = TABLE_PRE . 'permission_types';
                $this->form_validation->set_rules('permission_label', 'Permission Label', 'trim|required|xss_clean|is_unique[' . $table . '.permission_label]');
                $this->form_validation->set_rules('permission_description', 'Permission Description', 'trim|xss_clean');
                $this->form_validation->set_rules('permission_users[]', 'Permission Users', 'trim|required|xss_clean');
                $this->form_validation->set_rules('permission_val', 'Permission Values', 'trim|required|xss_clean');
                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('err_msg', validation_errors());
                    redirect('dashboard/add_permission');
                } else {
                    $permission_users = implode(",", $this->input->post('permission_users'));
                    $permission_val = explode(",", $this->input->post('permission_val'));
                    $i = 0;
                    foreach ($permission_val as $per) {
                        $permission[$i] = array(
                            'permission_label' => $this->input->post('permission_label'),
                            'permission_description' => $this->input->post('permission_description'),
                            'permission_users' => $permission_users,
                            'permission_value' => $per
                        );
                        $i++;
                    }

                    $query = $this->dashboard_model->add_permission($permission);

                    if (isset($query) && $query) {
                        $this->session->set_flashdata('succ_msg', "Permission Added Successfully!");
                        redirect('dashboard/add_permission');
                    } else {
                        $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                        redirect('dashboard/add_permission');
                    }
                }
            } else {
                $data['page'] = 'backend/dashboard/add_permission';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Add Permission End */

    /* All Permission Start */

    function all_permissions() {
        if ($this->session->userdata('user_type_slug') !== 'SUPA') {
            $data['heading'] = 'Permissions';
            $data['sub_heading'] = 'All Permissions';
            $data['description'] = 'Added Permissions List';
            $data['active'] = 'all_permissions';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post('pages')) {
                $pages = $this->input->post('pages');
            } else {
                if ($this->session->flashdata('pages')) {
                    $pages = $this->session->flashdata('pages');
                } else {
                    $pages = 5;
                }
            }

            $this->session->set_flashdata('pages', $pages);

            $config['base_url'] = base_url() . 'dashboard/all_permissions';
            $config['total_rows'] = $this->dashboard_model->all_permissions_row();
            $config['per_page'] = $pages;
            $config['uri_segment'] = 3;
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] = "</ul>";
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";


            $this->pagination->initialize($config);

            $segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            $data['permissions'] = $this->dashboard_model->all_permissions($config['per_page'], $segment);
            $data['pagination'] = $this->pagination->create_links();

            $data['page'] = 'backend/dashboard/all_permissions';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    /* All Permission End */

    /* Edit Permission Start */

    function edit_permission() {
        if ($this->session->userdata('user_type_slug') !== 'SUPA') {
            $data['heading'] = 'Permissions';
            $data['sub_heading'] = 'Edit Permission';
            $data['description'] = 'Edit Permission Here';
            $data['active'] = 'edit_permission';
            $data['users'] = $this->dashboard_model->get_user_type();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                $table = TABLE_PRE . 'permission_types';

                $this->form_validation->set_rules('permission_description', 'Permission Description', 'trim|xss_clean');
                $this->form_validation->set_rules('permission_users[]', 'Permission Users', 'trim|required|xss_clean');
                $this->form_validation->set_rules('permission_val', 'Permission Values', 'trim|required|xss_clean');
                if ($this->input->post('permission_label_hidden') != $this->input->post('permission_label')) {
                    $this->form_validation->set_rules('permission_label', 'Permission Label', 'trim|required|xss_clean|is_unique[' . $table . '.permission_label]');
                } else {
                    $this->form_validation->set_rules('permission_label', 'Permission Label', 'trim|required|xss_clean');
                }
                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('err_msg', validation_errors());
                    redirect('dashboard/edit_permission/' . base64url_encode($this->input->post('permission_id')));
                } else {
                    $permission_id = explode(",", $this->input->post('permission_id'));
                    $permission_users = implode(",", $this->input->post('permission_users'));
                    $permission_val = explode(",", $this->input->post('permission_val'));
                    $i = 0;
                    foreach ($permission_id as $per) {
                        $permission[$i] = array(
                            'permission_id' => $per,
                            'permission_label' => $this->input->post('permission_label'),
                            'permission_description' => $this->input->post('permission_description'),
                            'permission_users' => $permission_users,
                            'permission_value' => $permission_val[$i]
                        );
                        $i++;
                    }
                    $update = $this->dashboard_model->update_permission($permission);
                    $data['permission'] = $this->dashboard_model->get_permission($per);
                    $this->session->set_flashdata('permission', $data['permission']);
                    if (isset($update) && $update) {
                        $this->session->set_flashdata('succ_msg', "Updated Successfully!");
                        redirect('dashboard/edit_permission/' . base64url_encode($permission_id[0]));
                    } else {
                        $this->session->set_flashdata('err_msg', "Database Error. Try Again Later!");
                        redirect('dashboard/edit_permission/' . base64url_encode($permission_id[0]));
                    }
                }
            }
            $permission_id = (isset($permission) && $permission) ? $permission['permission_id'] : base64url_decode($this->uri->segment(3));
            $data['permission'] = $this->dashboard_model->get_permission($permission_id);
            if (!$data['permission'] || !$permission_id) {
                redirect('dashboard/all_permissions');
            }
            $data['page'] = 'backend/dashboard/edit_permission';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    /* Edit Permission End */

    /* Start Lock Screen */

    function lock_screen() {
        
    }

    /* Add Room Start */

    function add_room() {
		$this->load->library('upload');
		$this->load->library('image_lib');
			
        $found = in_array("10", $this->arraay);
        if ($found) {
            $data['heading'] = 'Room';
            $data['sub_heading'] = 'Add Room';
            $data['description'] = 'Add Room Here';
            $data['active'] = 'all_rooms';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {

                $room_feature = $this->input->post('room_feature');
                //echo "<pre>";
                //print_r($room_feature);
                foreach ($room_feature as $key => $val) {
                    $ft[$val][0] = $this->input->post('uamt_' . $val);
                    if ($this->input->post('uamt_' . $val) == '1') {
                        $charge = $this->input->post('amt_' . $val);
                    } else {
                        $charge = 0;
                    }
                    $ft[$val][1] = $charge;
                }



                //print_r($ft);
                //exit;
                $room_feature_string = "";
                $room_feature_string = $room_feature_string . implode(',', $room_feature);

                $room_image = "";
                $room_thumb = "";
				$room_image_2="";
				$room_image_2_thumb="";
				$room_image_3="";
				$room_image_3_thumb="";
				$room_image_4="";
				$room_image_4_thumb="";
                $this->upload->initialize($this->set_upload_unit_image1());
                if ($this->upload->do_upload('image1')) {
                    $room_image = $this->upload->data('file_name');
                    $filename = $room_image;
                    $source_path = './upload/unit/original/image1/'. $filename;
                    $target_path = './upload/unit/thumb/image1/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                   // $this->load->library('image_lib', $config_manip);
				   $this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_room');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $room_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                }
				$this->upload->initialize($this->set_upload_unit_image2());
				if ($this->upload->do_upload('image2')) {
                    $room_image_2 = $this->upload->data('file_name');
                    $filename2 = $room_image_2;
                    $source_path = './upload/unit/original/image2/' . $filename2;
                    $target_path = './upload/unit/thumb/image2/';
                    $config_manip2 = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                   // $this->load->library('image_lib', $config_manip);
				   $this->image_lib->initialize($config_manip2);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_room');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename2);
                    $room_image_2_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                }
				$this->upload->initialize($this->set_upload_unit_image3());
				if ($this->upload->do_upload('image3')) {
                    $room_image_3 = $this->upload->data('file_name');
                    $filename3 = $room_image_3;
                    $source_path = './upload/unit/original/image3/' . $filename3;
                    $target_path = './upload/unit/thumb/image3/';
                    $config_manip3 = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                   // $this->load->library('image_lib', $config_manip);
				   $this->image_lib->initialize($config_manip3);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_room');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename3);
                    $room_image_3_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                }
				$this->upload->initialize($this->set_upload_unit_image4());
				if ($this->upload->do_upload('image4')) {
                    $room_image_4 = $this->upload->data('file_name');
                    $filename4 = $room_image_4;
                    $source_path = './upload/unit/original/image4/' . $filename4;
                    $target_path = './upload/unit/thumb/image4/';
                    $config_manip4 = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                   // $this->load->library('image_lib', $config_manip);
				   $this->image_lib->initialize($config_manip4);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_room');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename4);
                    $room_image_4_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                }
                date_default_timezone_set("Asia/Kolkata");
                if ($this->input->post('rate_type') == '1') {
                    $room = array(
                        'hotel_id' => $this->session->userdata('user_hotel'),
                        //'unit_type' => $this->input->post('unit_type'),
                        'unit_id' => $this->input->post('unit_name'),
                        //'unit_class' => $this->input->post('unit_class'),
                        'floor_no' => $this->input->post('floor_no'),
                        'room_name' => $this->input->post('room_name'),
                        'room_no' => $this->input->post('room_no'),
                        'clean_id' => 1,
                        'room_bed' => $this->input->post('room_bed'),
                        'room_features' => $room_feature_string,
                        'room_rent' => $this->input->post('room_rent'),
                        'room_rent_seasonal' => $this->input->post('room_rent_seasonal'),
                        'room_rent_weekend' => $this->input->post('room_rent_weekend'),
                        'max_discount' => $this->input->post('max_discount'),
                        'room_image' => $room_image,
                        'room_image_thumb' => $room_thumb,
						'room_image_2' => $room_image_2,
                        'room_image_2_thumb' => $room_image_2_thumb,
						'room_image_3' => $room_image_3,
                        'room_image_3_thumb' => $room_image_3_thumb,
						'room_image_4' => $room_image_4,
                        'room_image_4_thumb' => $room_image_4_thumb,
                        'room_add_date' => date("Y-m-d H:i:s"),
                        'room_modification_date' => date("Y-m-d H:i:s"),
                        'max_occupancy' => $this->input->post('max_occupancy'),
                        'rate_type' => $this->input->post('rate_type'),
                        'adult_rate_type' => $this->input->post('adult_radio'),
                        'adult_rate' => $this->input->post('adult_rate'),
                        'kid_rate_type' => $this->input->post('kid_radio'),
                        'kid_rate' => $this->input->post('kid_rate'),
                        'note' => $this->input->post('note'),
                        'hotel_id' => $this->session->userdata('user_hotel'),
                        'user_id' => $this->session->userdata('user_id')
                    );
                } else {
                    $room = array(
                        'hotel_id' => $this->session->userdata('user_hotel'),
                        //'unit_type' => $this->input->post('unit_type'),
                        'unit_id' => $this->input->post('unit_name'),
                        //'unit_class' => $this->input->post('unit_class'),
                        'floor_no' => $this->input->post('floor_no'),
                        'room_name' => $this->input->post('room_name'),
                        'room_no' => $this->input->post('room_no'),
                          'clean_id' => 1,
                        'room_bed' => $this->input->post('room_bed'),
                        'room_features' => $room_feature_string,
                        'room_rent' => $this->input->post('room_rent'),
                        'room_rent_seasonal' => $this->input->post('room_rent_seasonal'),
                        'room_rent_weekend' => $this->input->post('room_rent_weekend'),
                        'max_discount' => $this->input->post('max_discount'),
                        'room_image' => $room_image,
                        'room_image_thumb' => $room_thumb,
						'room_image_2' => $room_image_2,
                        'room_image_2_thumb' => $room_image_2_thumb,
						'room_image_3' => $room_image_3,
                        'room_image_3_thumb' => $room_image_3_thumb,
						'room_image_4' => $room_image_4,
                        'room_image_4_thumb' => $room_image_4_thumb,
                        'room_add_date' => date("Y-m-d H:i:s"),
                        'room_modification_date' => date("Y-m-d H:i:s"),
                        'max_occupancy' => $this->input->post('max_occupancy'),
                        'rate_type' => $this->input->post('rate_type'),
                        'note' => $this->input->post('note'),
                        'user_id' => $this->session->userdata('user_id')
                    );
                }
                // print_r($room);
                //exit();
                $dt['room'] = $room;
                $dt['feature'] = $ft;
                //$dt['amt'] = $charge;
                $query = $this->dashboard_model->add_room($dt);

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Room Added Successfully!");
                    redirect('dashboard/all_rooms');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_room');
                }
            } else {
                $data['room_feature'] = $this->dashboard_model->get_all_room_feature();
                $data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
                $data['page'] = 'backend/dashboard/add_room';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Add Room End */

    /* All Rooms Start */

    function all_rooms() {

        $found = in_array("10", $this->arraay);
        if ($found) {
            $data['heading'] = 'Room';
            $data['sub_heading'] = 'All Rooms';
            $data['description'] = 'Added Rooms List';
            $data['active'] = 'all_rooms';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));


            //$data['rooms'] = $this->dashboard_model->all_rooms($config['per_page'], $segment);
            $data['rooms'] = $this->dashboard_model->all_rooms();
            //$data['pagination'] = $this->pagination->create_links();
            /* echo "<pre>";
              print_r($data);
              exit;
             */
            $data['page'] = 'backend/dashboard/all_rooms';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    /* All Rooms End */

    /* Upload Start */
	function set_upload_options() {
        $config = array(
            'upload_path' => './upload/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
        return $config;
    }
	
	function set_upload_channel() {
        $config = array(
            'upload_path' => './upload/channel/image/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
        return $config;
    }
	
	function set_upload_channel_doc() {
        $config = array(
            'upload_path' => './upload/channel/document/',
            'allowed_types' => 'gif|jpg|png|jpeg|pdf|doc|docx',
            'overwrite' => FALSE
        );
        return $config;
    }
	
	function set_upload_broker() {
        $config = array(
            'upload_path' => './upload/broker/image/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
        return $config;
    }
	
	function set_upload_broker_doc() {
        $config = array(
            'upload_path' => './upload/broker/document/',
            'allowed_types' => 'gif|jpg|png|jpeg|pdf|doc|docx',
            'overwrite' => FALSE
        );
        return $config;
    }
	
	function set_upload_unit_image1() {
        $config = array(
            'upload_path' => './upload/unit/original/image1/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
        return $config;
    }
	
	function set_upload_unit_image2() {
        $config = array(
            'upload_path' => './upload/unit/original/image2',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
        return $config;
    }
	
	function set_upload_unit_image3() {
        $config = array(
            'upload_path' => './upload/unit/original/image3',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
        return $config;
    }
	
	function set_upload_unit_image4() {
        $config = array(
            'upload_path' => './upload/unit/original/image4',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
        return $config;
    }

    function set_upload_optionscer() {
        $config = array(
            'upload_path' => './upload/certificate/original/cer_1/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
        return $config;
    }
	
	function set_upload_optionscer2() {
        $config = array(
            'upload_path' => './upload/certificate/original/cer_2/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
        return $config;
    }
	  function set_upload_optionsGst() {
        $config = array(
            'upload_path' => './upload/guest/originals/photo/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
		
        return $config;
    } 
	function set_upload_optionsGstId() {
        $config = array(
            'upload_path' => './upload/guest/originals/id_proof/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
		
        return $config;
    }
	 function set_upload_optionsAsset() {
        $config = array(
            'upload_path' => './upload/asset/originals/asset_image/',
            'allowed_types' => 'gif|jpg|png|jpeg',
			
            'overwrite' => FALSE
        );
		
        return $config;
    }
	function set_upload_optionsA_Bill1() {
        $config = array(
            'upload_path' => './upload/asset/originals/Proc_bill_1/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
        return $config;
    }
	function set_upload_optionsA_Bill2() {
        $config = array(
            'upload_path' => './upload/asset/originals/Proc_bill_2/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
        return $config;
    }
 function set_upload_purchase() {
        $config = array(
            'upload_path' => './upload/payments/orginal/purchase/',
            'allowed_types' => 'gif|jpg|png|jpeg|pdf',
            'overwrite' => FALSE
        );
		
        return $config;
    } 
 function set_upload_otherDoc() {
        $config = array(
            'upload_path' => './upload/payments/orginal/other/',
            'allowed_types' => 'gif|jpg|png|jpeg|pdf',
            'overwrite' => FALSE
        );
		
        return $config;
    }	
    /* Upload End */

    function add_booking() {

        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['heading'] = 'Booking';
            $data['sub_heading'] = 'Add Booking';
            $data['description'] = 'Add Booking Here';
            $data['active'] = 'add_booking';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {


                $rooms = $this->dashboard_model->get_room_id($this->session->userdata('user_hotel'), $this->input->post('room_no'));
                if (!$rooms) {

                    $this->session->set_flashdata('err_msg', "Room Number for this hotel not found. Please add new room. ");
                    $data['page'] = 'backend/dashboard/add_booking';
                    $this->load->view('backend/common/index', $data);
                } else {

                    foreach ($rooms as $room) {

                        $room_id = $room->room_id;
                    }

                    date_default_timezone_set("Asia/Kolkata");
                    $bookings = array(
                        'room_id' => $room_id,
                        'cust_name' => $this->input->post('cust_name'),
                        'cust_address' => $this->input->post('cust_address'),
                        'cust_contact_no' => $this->input->post('cust_contact_no'),
                        'cust_from_date' => $this->input->post('cust_from_date'),
                        'cust_end_date' => $this->input->post('cust_end_date'),
                        'cust_booking_status' => $this->input->post('cust_booking_status'),
                        'cust_payment_initial' => $this->input->post('cust_payment_initial'),
                    );
                    $query = $this->dashboard_model->add_booking($bookings);

                    if (isset($query) && $query) {
                        $this->session->set_flashdata('succ_msg', "Booking Added Successfully!");
                        redirect('dashboard/add_booking');
                    } else {
                        $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                        redirect('dashboard/add_booking');
                    }
                }
            } else {
                $data['page'] = 'backend/dashboard/add_booking';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function add_booking_calendar() {


        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['heading'] = 'Booking';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Booking Here';
            $data['active'] = 'add_booking_calendar';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $this->load->model('bookings_model');
            $data['units'] = $this->bookings_model->all_units();
            $data['page'] = 'backend/dashboard/add_booking_calendar';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function calendar_load_2() {


        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['heading'] = 'Booking';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Booking Here';
            $data['active'] = 'add_booking_calendar';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $this->load->model('bookings_model');
            $data['units'] = $this->bookings_model->all_units();
            $data['page'] = 'backend/dashboard/calendar_load_2';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function snapshot_load() {


        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['heading'] = 'Booking';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Booking Here';
            $data['active'] = 'add_booking_calendar';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $this->load->model('bookings_model');
            $data['units'] = $this->bookings_model->all_units();
            $data['page'] = 'backend/dashboard/snapshot_load';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function all_bookings() {
        $found = in_array("25", $this->arraay);
        if ($found) {
			
			
            $data['heading'] = 'Booking';
            $data['sub_heading'] = 'All Bookings';
            $data['description'] = 'Bookings List';
            $data['active'] = 'all_bookings';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            //$data['bookings'] = $this->dashboard_model->all_bookings($config['per_page'], $segment);
            $data['bookings'] = $this->dashboard_model->all_bookings();
            $data['transactions'] = $this->dashboard_model->all_transactions();
            //$data['pagination'] = $this->pagination->create_links();
				
            $data['page'] = 'backend/dashboard/all_bookings';
            $this->load->view('backend/common/index', $data);
			
        } else {
            redirect('dashboard');
        }
    }

    function cancel_report() {

        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['heading'] = 'Report';
            $data['sub_heading'] = 'All Cancelled Bookings';
            $data['description'] = 'Cancelled Bookings List';
            $data['active'] = 'all_bookings';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            //$data['bookings'] = $this->dashboard_model->all_bookings($config['per_page'], $segment);
            $data['bookings'] = $this->setting_model->cancel_bookings();
            //   $data['transactions'] = $this->dashboard_model->all_transactions();
            //$data['pagination'] = $this->pagination->create_links();.........

            /* echo "<pre>";
              print_r($data);
              echo "</pre>";
              exit; */
            $data['page'] = 'backend/dashboard/cancel_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function cancel_report_by_date() {

        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['heading'] = 'Report';
            $data['sub_heading'] = 'All Cancelled Bookings';
            $data['description'] = 'Cancelled Bookings List';
            $data['active'] = 'all_bookings';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $cust_from_date_actual = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $cust_end_date_actual = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
            //$data['bookings'] = $this->dashboard_model->all_bookings($config['per_page'], $segment);
            $data['bookings'] = $this->setting_model->cancel_bookings_by_date($cust_from_date_actual, $cust_end_date_actual);
            //   $data['transactions'] = $this->dashboard_model->all_transactions();
            //$data['pagination'] = $this->pagination->create_links();
            $data['start_date'] = $cust_from_date_actual;
            $data['end_date'] = $cust_end_date_actual;
            $data['page'] = 'backend/dashboard/cancel_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function all_group_booking() {

        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['heading'] = 'Booking';
            $data['sub_heading'] = 'All Group Bookings';
            $data['description'] = 'Group Bookings List';
            $data['active'] = 'all_group_booking';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            //$data['line_items']  =$this->bookings_model->line_charge_item_tax('gb',$group_id);
            // $data['details']=$this->dashboard_model->get_group_details($group_id);
            // $data['details2']=$this->dashboard_model->get_group($group_id);
            // $data['details3']=$this->dashboard_model->get_booking_details_grp($group_id);
            // $data['transaction'] = $this->dashboard_model->all_transaction_group($group_id);
            $data['groups'] = $this->dashboard_model->all_group();
            //echo "<pre>";
            //print_r($data['groups']);
            //exit();
            $data['page'] = 'backend/dashboard/all_group_bookings';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function all_group_booking_by_date() {
        $start_date = date("m/d/Y");
        $end_date = date("m/d/Y");
        $data['heading'] = 'Group Bookings';
        $data['sub_heading'] = 'All Group Bookings';
        $data['description'] = 'Group Bookings List';
        $data['active'] = 'all_bookings';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        if ($this->input->post()) {
            $start_date = date("m/d/Y", strtotime($this->input->post('t_dt_frm')));
            $end_date = date("m/d/Y", strtotime($this->input->post('t_dt_to')));
            $data['start_date'] = $this->input->post('t_dt_frm');
            $data['end_date'] = $this->input->post('t_dt_to');
        }

        $data['groups'] = $this->dashboard_model->all_group_by_date($start_date, $end_date);
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;

        // $data['types'] = $this->dashboard_model->all_f_types();
        //$data['entity'] = $this->dashboard_model->all_f_entity();
        $data['page'] = 'backend/dashboard/all_group_bookings';
        $this->load->view('backend/common/index', $data);
    }

    /* All Bookings End */

    // Transaction starts (Add Transaction)

    function add_transaction() {


        $found = in_array("28", $this->arraay);
        if ($found) {
            $data['heading'] = 'Transaction';
            $data['sub_heading'] = 'Add Transaction';
            $data['description'] = 'Add Transaction Here';
            $data['active'] = 'add_transaction';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            //19.11.2015
            /* if($this->input->post('term'))
              {
              echo $this->input->post('term');
              } */

            if ($this->input->post()) {

                date_default_timezone_set("Asia/Kolkata");
                $transactions = array(
                    't_booking_id' => $this->input->post('t_booking_id'),
                    't_amount' => $this->input->post('t_amount'),
                    //'t_date' => date("Y-m-d H:i:s"),
                    't_payment_mode' => $this->input->post('t_payment_mode'),
                    't_status' => $this->input->post('t_payment_status'),
                        //'hotel_id'=>$this->session->userdata('user_hotel'),
                        //'user_id'=>$this->session->userdata('user_id')
                );
                $query = $this->dashboard_model->add_transaction($transactions);

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Transaction Added Successfully!");
                    redirect('dashboard/add_transaction');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_transaction');
                }
            } else {

                //$this->load->model('Dashboard_model');
                //echo $book_id;
                $data['booking'] = $this->dashboard_model->all_booking_id();
                $data['page'] = 'backend/dashboard/add_transaction';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function all_transactions() {

        $found = in_array("28", $this->arraay);
        if ($found) {
            $data['heading'] = 'Finance';
            $data['sub_heading'] = 'Transactions';
            $data['description'] = 'Transaction List';
            $data['active'] = 'all_transactions';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['transactions'] = $this->dashboard_model->all_transactions_limit();
            $data['page'] = 'backend/dashboard/all_transactions';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    /* Edit Hotel Start */

    function edit_hotel() {

        $found = in_array("37", $this->arraay);
        if ($found) {
            $data['heading'] = 'Hotel';
            $data['sub_heading'] = 'Edit Hotel';
            $data['description'] = 'Edit Hotel Here';
            $data['active'] = 'edit_hotel';
            $data['country'] = $this->dashboard_model->get_country();
            $data['star'] = $this->dashboard_model->get_star_rating();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            // echo '<pre>';
            //print_r($data['hotel_name']);// exit;
            $hotel_contact_details_edit = array();

            if ($this->input->post()) {
                date_default_timezone_set("Asia/Kolkata");
                $hotel_type = $this->input->post('hotel_type');
                $hotel_type_string = "";
                $hotel_type_string = $hotel_type_string . implode(',', $hotel_type);

                $guest_option = $this->input->post('guest');
                $guest_option_string = "";
                $guest_option_string = $guest_option_string . implode(',', $guest_option);

                $broker_option = $this->input->post('broker');
                $broker_option_string = "";
                $broker_option_string = $broker_option_string . implode(',', $broker_option);

                //Image Upload Start Now

                $this->upload->initialize($this->set_upload_options());

                if ($this->upload->do_upload('image_photo')) {
                    $hotel_image = $this->upload->data('file_name');
                    $filename = $hotel_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/hotel/';

                    $config_manip = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 100,
                        'height' => 75
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_compliance');
                    } else {
                        $thumb_name = explode(".", $filename);
                        $hotel_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                        $this->image_lib->clear();


                        $hotel_edit = array(
                            'hotel_name' => $this->input->post('hotel_name'),
                           'hotel_gpname' => $this->input->post('hotel_gpname'),
                            'hotel_year_established' => $this->input->post('hotel_year_established'),
                            'hotel_ownership_type' => $this->input->post('hotel_ownership_type'),
                            'hotel_type' => $hotel_type_string,
                            'hotel_rooms' => $this->input->post('hotel_rooms'),
                            'hotel_floor' => $this->input->post('hotel_floor'),
                            'hotel_logo_images' => $hotel_image,
                            'hotel_logo_images_thumb' => $hotel_thumb,
                            'hotel_logo_text' => $this->input->post('images_text')
                        );
                    }
                }

                //Image Upload Section Closed
                else {
                    $hotel_edit = array(
                        'hotel_name' => $this->input->post('hotel_name'),
                        'hotel_gpname' => $this->input->post('hotel_gpname'),
                        'hotel_year_established' => $this->input->post('hotel_year_established'),
                        'hotel_ownership_type' => $this->input->post('hotel_ownership_type'),
                        'hotel_type' => $hotel_type_string,
                        'hotel_rooms' => $this->input->post('hotel_rooms'),
                        'hotel_floor' => $this->input->post('hotel_floor'),
                        'hotel_logo_text' => $this->input->post('images_text')
                    );
                }


                $hotel_contact_details_edit = array(
                    'hotel_street1' => $this->input->post('hotel_street1'),
                    'hotel_street2' => $this->input->post('hotel_street2'),
                    'hotel_landmark' => $this->input->post('hotel_landmark'),
                    'hotel_district' => $this->input->post('hotel_district'),
                    'hotel_pincode' => $this->input->post('hotel_pincode'),
                    'hotel_state' => $this->input->post('hotel_state'),
                    'hotel_country' => $this->input->post('hotel_country'),
                    'hotel_branch_street1' => $this->input->post('hotel_branch_street1'),
                    'hotel_branch_street2' => $this->input->post('hotel_branch_street2'),
                    'hotel_branch_landmark' => $this->input->post('hotel_branch_landmark'),
                    'hotel_branch_district' => $this->input->post('hotel_branch_district'),
                    'hotel_branch_pincode' => $this->input->post('hotel_branch_pincode'),
                    'hotel_branch_state' => $this->input->post('hotel_branch_state'),
                    'hotel_branch_country' => $this->input->post('hotel_branch_country'),
                    'hotel_booking_street1' => $this->input->post('hotel_booking_street1'),
                    'hotel_booking_street2' => $this->input->post('hotel_booking_street2'),
                    'hotel_booking_landmark' => $this->input->post('hotel_booking_landmark'),
                    'hotel_booking_district' => $this->input->post('hotel_booking_district'),
                    'hotel_booking_pincode' => $this->input->post('hotel_booking_pincode'),
                    'hotel_booking_state' => $this->input->post('hotel_booking_state'),
                    'hotel_booking_country' => $this->input->post('hotel_booking_country'),
                    'hotel_frontdesk_name' => $this->input->post('hotel_frontdesk_name'),
                    'hotel_frontdesk_mobile' => $this->input->post('hotel_frontdesk_mobile'),
                    'hotel_frontdesk_mobile_alternative' => $this->input->post('hotel_frontdesk_mobile_alternative'),
                    'hotel_frontdesk_email' => $this->input->post('hotel_frontdesk_email'),
                    'hotel_owner_name' => $this->input->post('hotel_owner_name'),
                    'hotel_owner_mobile' => $this->input->post('hotel_owner_mobile'),
                    'hotel_owner_mobile_alternative' => $this->input->post('hotel_owner_mobile_alternative'),
                    'hotel_owner_email' => $this->input->post('hotel_owner_email'),
                    'hotel_hr_name' => $this->input->post('hotel_hr_name'),
                    'hotel_hr_mobile' => $this->input->post('hotel_hr_mobile'),
                    'hotel_hr_mobile_alternative' => $this->input->post('hotel_hr_mobile_alternative'),
                    'hotel_hr_email' => $this->input->post('hotel_hr_email'),
                    'hotel_accounts_name' => $this->input->post('hotel_accounts_name'),
                    'hotel_accounts_mobile' => $this->input->post('hotel_accounts_mobile'),
                    'hotel_accounts_mobile_alternative' => $this->input->post('hotel_accounts_mobile_alternative'),
                    'hotel_accounts_email' => $this->input->post('hotel_accounts_email'),
                    'hotel_near_police_name' => $this->input->post('hotel_near_police_name'),
                    'hotel_near_police_mobile' => $this->input->post('hotel_near_police_mobile'),
                    'hotel_near_police_mobile_alternative' => $this->input->post('hotel_near_police_mobile_alternative'),
                    'hotel_near_police_email' => $this->input->post('hotel_near_police_email'),
                    'hotel_near_medical_name' => $this->input->post('hotel_near_medical_name'),
                    'hotel_near_medical_mobile' => $this->input->post('hotel_near_medical_mobile'),
                    'hotel_near_medical_mobile_alternative' => $this->input->post('hotel_near_medical_mobile_alternative'),
                    'hotel_near_medical_email' => $this->input->post('hotel_near_medical_email'),
                    'hotel_fax' => $this->input->post('hotel_fax'),
                    'hotel_website' => $this->input->post('hotel_website'),
                    'hotel_working_from' => '',
                    'hotel_working_upto' => ''
                );

                $hotel_tax_details_edit = array(
                    'hotel_tax_applied' => $this->input->post('hotel_tax_applied'),
                    'hotel_service_tax' => $this->input->post('hotel_service_tax'),
                    'hotel_luxury_tax' => $this->input->post('hotel_luxury_tax'),
                    'hotel_service_charge' => $this->input->post('hotel_service_charge'),
                    'hotel_stander_tac' => $this->input->post('hotel_stander_tac'),
                    'hotel_vat_tax' => $this->input->post('hotel_vat_tax'),
                    'hotel_tax_food_vat' => $this->input->post('hotel_tax_food_vat'),
                    'hotel_tax_liquor_vat' => $this->input->post('hotel_tax_liquor_vat'),
                    'luxury_tax_slab1_range_to' => $this->input->post('luxury_tax_slab1_range_to'),
                    'luxury_tax_slab1_range_from' => $this->input->post('luxury_tax_slab1_range_from'),
                    'luxury_tax_slab1' => $this->input->post('luxury_tax_slab1'),
                    'luxury_tax_slab2_range_to' => $this->input->post('luxury_tax_slab2_range_to'),
                    'luxury_tax_slab2_range_from' => $this->input->post('luxury_tax_slab2_range_from'),
                    'luxury_tax_slab2' => $this->input->post('luxury_tax_slab2')
                );
                //print_r($hotel_tax_details_edit);exit;
                $hotel_billing_settings_edit = array(
                    'billing_name' => $this->input->post('billing_name'),
                    'billing_street1' => $this->input->post('billing_street1'),
                    'billing_street2' => $this->input->post('billing_street2'),
                    'billing_landmark' => $this->input->post('billing_landmark'),
                    'billing_district' => $this->input->post('billing_district'),
                    'billing_pincode' => $this->input->post('billing_pincode'),
                    'billing_state' => $this->input->post('billing_state'),
                    'billing_country' => $this->input->post('billing_country'),
                    'billing_email' => $this->input->post('billing_email'),
                    'billing_phone' => $this->input->post('billing_phone'),
                    'billing_fax' => $this->input->post('billing_fax'),
                    'billing_vat' => $this->input->post('billing_vat'),
                    'billing_bank_name' => $this->input->post('billing_bank_name'),
                    'billing_account_no' => $this->input->post('billing_account_no'),
                    'billing_ifsc_code' => $this->input->post('billing_ifsc_code'),
                    'luxury_tax_reg_no' => $this->input->post('luxury_tax_reg_no'),
                    'service_tax_no' => $this->input->post('service_tax_no'),
                    'vat_reg_no' => $this->input->post('vat_reg_no'),
                    'cin_no' => $this->input->post('cin_no'),
                    'tin_no' => $this->input->post('tin_no')
                );
                $hotel_general_preference_edit = array(
                    'hotel_check_in_time_hr' => $this->input->post('hotel_check_in_time_hr'),
                    'hotel_check_in_time_mm' => $this->input->post('hotel_check_in_time_mm'),
                    'hotel_check_in_time_fr' => $this->input->post('hotel_check_in_time_fr'),
                    'hotel_check_out_time_hr' => $this->input->post('hotel_check_out_time_hr'),
                    'hotel_check_out_time_mm' => $this->input->post('hotel_check_out_time_mm'),
                    'hotel_check_out_time_fr' => $this->input->post('hotel_check_out_time_fr'),
                    'hotel_guest' => $guest_option_string,
                    'hotel_broker' => $broker_option_string,
                    'hotel_buffer_hour' => $this->input->post('buffer_hour'),
                    'hotel_broker_commission' => 0,
                    'hotel_date_format' => $this->input->post('hotel_date_format'),
                    'hotel_time_format' => $this->input->post('hotel_time_format')
                );








                //$query = $this->dashboard_model->edit_hotel_1($hotel_edit,$this->input->post('h_id'));
                $query = $this->dashboard_model->edit_hotel_details($hotel_edit, $this->input->post('h_id'));
                $query = $this->dashboard_model->edit_hotel_contact($hotel_contact_details_edit, $this->input->post('h_id'));
                $query = $this->dashboard_model->edit_hotel_tax($hotel_tax_details_edit, $this->input->post('h_id'));
                $query = $this->dashboard_model->edit_hotel_billing($hotel_billing_settings_edit, $this->input->post('h_id'));
                $query = $this->dashboard_model->edit_hotel_preferences($hotel_general_preference_edit, $this->input->post('h_id'));


                $hotel_id = $this->input->post('h_id');

                if (isset($query) && $query) {

                    $this->session->set_flashdata('succ_msg', "Hotel Accounts Info Updated Successfully!");
                    //$data['value'] = $this->dashboard_model->get_c_details($room_id );
                    //$hotel_id = $_GET['hotel_id'];

                    $data['value'] = $this->dashboard_model->get_hotel_details($hotel_id);

                    $data['hotels'] = $this->dashboard_model->all_hotels();
                    $data['page'] = 'backend/dashboard/all_hotel_m';
                    $this->load->view('backend/common/index', $data);
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    //$data['value'] = $this->dashboard_model->get_c_details($c_id);

                    $hotel_id = $_GET['hotel_id'];
                    $data['hotels'] = $this->dashboard_model->all_hotels();
                    $data['value'] = $this->dashboard_model->get_hotel_details($hotel_id);
                    $data['page'] = 'backend/dashboard/all_hotel_m';
                    $this->load->view('backend/common/index', $data);
                }
            } else {

                $hotel_id = $_GET['hotel_id'];
                $data['value'] = $this->dashboard_model->get_hotel_details($hotel_id);
                // echo "hotel".$hotel_id ;
                //print_r($data['value']); exit;
                $data['hotels'] = $this->dashboard_model->all_hotels();
                $data['page'] = 'backend/dashboard/edit_hotel';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Edit Hotel End */

    function tasks_add() {
        
    }

    function tasks_delete() {
        
    }

    function edit_room() {
		$this->load->library('upload');
		$this->load->library('image_lib');
        $found = in_array("11", $this->arraay);
        if ($found) {
            $data['heading'] = 'Room';
            $data['sub_heading'] = 'Edit Room';
            $data['description'] = 'Edit Room Here';
            $data['active'] = 'all_rooms';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                //print_r($this->input->post());
                //exit;
                date_default_timezone_set("Asia/Kolkata");
				$room_id = $this->input->post('room_id');

                $room_feature = $this->input->post('room_feature');
                //echo "<pre>";
                //print_r($room_feature);
                foreach ($room_feature as $key => $val) {
                    $ft[$val][0] = $this->input->post('uamt_' . $val);
                    if ($this->input->post('uamt_' . $val) == '1') {
                        $charge = $this->input->post('amt_' . $val);
                    } else {
                        $charge = 0;
                    }
                    $ft[$val][1] = $charge;
                }



                /*   $room_feature = $this->input->post('room_feature');
                  foreach($room_feature as $key => $value){
                  $ft[$value] = $this->input->post('feature_type_'.$value);
                  } */

                $room_feature_string = "";
                $room_feature_string = $room_feature_string . implode(',', $room_feature);
				$room_image1 ="";
				$room_thumb1 ="";
				$room_image2 ="";
				$room_thumb2 ="";
				$room_image3 ="";
				$room_thumb3 ="";
				$room_image4 ="";
				$room_thumb4 ="";
				$room_data=$this->dashboard_model->get_room_details($room_id);
				/*echo "<pre>";
				print_r($room_data);
				exit();*/
				foreach($room_data as $rm_data){
					$room_image1=$rm_data->room_image;
					$room_thumb1 =$rm_data->room_image_thumb;
					$room_image2 =$rm_data->room_image_2;
					$room_thumb2 =$rm_data->room_image_2_thumb;
					$room_image3 =$rm_data->room_image_3;
					$room_thumb3 =$rm_data->room_image_3_thumb;
					$room_image4 =$rm_data->room_image_4;
					$room_thumb4 =$rm_data->room_image_4_thumb;
				}
				//echo $rm;
				//exit();
                $this->upload->initialize($this->set_upload_unit_image1());
                
				
					if ($this->upload->do_upload('image1')) {

                    /*  Image Upload 18.11.2015 */
                    $room_image1 = $this->upload->data('file_name');
                    $filename1 = $room_image1;
                    $source_path = './upload/unit/original/image1/' . $filename1;
                    $target_path = './upload/unit/thumb/image1/';
                    $config_manip1 = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 800,
                        'height' => 800
                    );
                   // $this->load->library('image_lib', $config_manip1);
				   $this->image_lib->initialize($config_manip1);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_compliance');
                    } else {
                        // clear //
                        $thumb_name = explode(".", $filename1);
                        $room_thumb1 = $thumb_name[0] . $config_manip1['thumb_marker'] . "." . $thumb_name[1];
                        $this->image_lib->clear();
                        /* Images Upload  18.11.2015 */
							} 
					}
					$this->upload->initialize($this->set_upload_unit_image2());
					if ($this->upload->do_upload('image2')) {

                    /*  Image Upload 18.11.2015 */
                    $room_image2 = $this->upload->data('file_name');
                    $filename2 = $room_image2;
                    $source_path = './upload/unit/original/image2/' . $filename2;
                    $target_path = './upload/unit/thumb/image2/';
                    $config_manip2 = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 800,
                        'height' => 800
                    );
                   // $this->load->library('image_lib', $config_manip1);
				   $this->image_lib->initialize($config_manip2);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_room');
                    } else {
                        // clear //
                        $thumb_name = explode(".", $filename2);
                        $room_thumb2 = $thumb_name[0] . $config_manip2['thumb_marker'] . "." . $thumb_name[1];
                        $this->image_lib->clear();
                        /* Images Upload  18.11.2015 */
							} 
					}
					$this->upload->initialize($this->set_upload_unit_image3());
					if ($this->upload->do_upload('image3')) {

                    /*  Image Upload 18.11.2015 */
                    $room_image3 = $this->upload->data('file_name');
                    $filename3 = $room_image3;
                    $source_path = './upload/unit/original/image3/' . $filename3;
                    $target_path = './upload/unit/thumb/image3/';
                    $config_manip3 = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 800,
                        'height' => 800
                    );
                   // $this->load->library('image_lib', $config_manip1);
				   $this->image_lib->initialize($config_manip3);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_room');
                    } else {
                        // clear //
                        $thumb_name = explode(".", $filename3);
                        $room_thumb3 = $thumb_name[0] . $config_manip3['thumb_marker'] . "." . $thumb_name[1];
                        $this->image_lib->clear();
                        /* Images Upload  18.11.2015 */
							} 
					}
					$this->upload->initialize($this->set_upload_unit_image4());
					if ($this->upload->do_upload('image4')) {

                    /*  Image Upload 18.11.2015 */
                    $room_image4 = $this->upload->data('file_name');
                    $filename4 = $room_image4;
                    $source_path = './upload/unit/original/image4/' . $filename4;
                    $target_path = './upload/unit/thumb/image4/';
                    $config_manip4 = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 800,
                        'height' => 800
                    );
                   // $this->load->library('image_lib', $config_manip1);
				   $this->image_lib->initialize($config_manip4);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_room');
                    } else {
                        // clear //
                        $thumb_name = explode(".", $filename4);
                        $room_thumb4 = $thumb_name[0] . $config_manip4['thumb_marker'] . "." . $thumb_name[1];
                        $this->image_lib->clear();
                        /* Images Upload  18.11.2015 */
							} 
					}
                        $room_edit = array(
                            'hotel_id' => $this->session->userdata('user_hotel'),
                            'unit_id' => $this->input->post('unit_name'),
                            'floor_no' => $this->input->post('floor_no'),
                            'room_name' => $this->input->post('room_name'),
                            'room_no' => $this->input->post('room_no'),
                            'room_bed' => $this->input->post('room_bed'),
                            'room_features' => $room_feature_string,
                            'room_rent' => $this->input->post('room_rent'),
                            'room_rent_seasonal' => $this->input->post('room_rent_seasonal'),
                            'room_rent_weekend' => $this->input->post('room_rent_weekend'),
                            'room_image' => $room_image1,
                            'room_image_thumb' => $room_thumb1,
							'room_image_2' => $room_image2,
                            'room_image_2_thumb' => $room_thumb2,
							'room_image_3' => $room_image3,
                            'room_image_3_thumb' => $room_thumb3,
							'room_image_4' => $room_image4,
                            'room_image_4_thumb' => $room_thumb4,
                            'room_add_date' => date("Y-m-d H:i:s"),
                            'room_modification_date' => date("Y-m-d H:i:s"),
                            //'max_occupancy'=>$this->input->post('max_occupancy'),
                            'max_discount' => $this->input->post('max_discount'),
                            'rate_type' => $this->input->post('rate_type'),
                            'adult_rate' => $this->input->post('adult_rate'),
                            'adult_rate_type' => $this->input->post('adult_radio'),
                            'kid_rate' => $this->input->post('kid_rate'),
                            'kid_rate_type' => $this->input->post('kid_radio'),
                            'note' => $this->input->post('note'),
                        );
                    
                


                $dt['room'] = $room_edit;
                $dt['feature'] = $ft;
				//echo "<pre>";
               // print_r($room_edit);
                //print_r($dt['feature']);
               // exit();
                $query = $this->dashboard_model->edit_room($dt, $this->input->post('room_id'));

                

                if (isset($query) && $query) {

                    $this->session->set_flashdata('succ_msg', "Rooms  Updated Successfully!");
                    //$data['value'] = $this->dashboard_model->get_c_details($room_id );

                    redirect('dashboard/all_rooms');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    //$data['value'] = $this->dashboard_model->get_c_details($c_id);

                    $data['rooms'] = $this->dashboard_model->all_rooms();
                    $data['page'] = 'backend/dashboard/all_rooms';
                    $this->load->view('backend/common/index', $data);
                }
            } else {

                $room_id = $_GET['room_id'];
                $data['value'] = $this->dashboard_model->get_room_details($room_id);
                $data['room_feature'] = $this->dashboard_model->get_all_room_feature();
                $data['rf'] = $this->dashboard_model->get_all_room_feature_id($room_id);
                $data['page'] = 'backend/dashboard/edit_room';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Add Compliance */

  function add_compliance() {
		$this->load->library('upload');
		$this->load->library('image_lib');
		date_default_timezone_set("Asia/Kolkata");
		$found = in_array("13", $this->arraay);
        if ($found) {
            $data['heading'] = 'Compliance';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Compliance Here';
            $data['active'] = 'all_compliance';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['cirtificate'] = $this->dashboard_model->all_cirtificate();
            if ($this->input->post()) {

                /* 20.11.2015 _ Images Upload */
                $this->upload->initialize($this->set_upload_optionscer());
                if ($this->upload->do_upload('image_certificate')) {

                    $certificate_image1 = $this->upload->data('file_name');
					//echo $certificate_image1;
					//exit();
                    $filename = $certificate_image1;
					//echo $filename;
					//exit();
                    $source_path = './upload/certificate/original/cer_1' . $filename;
                    $target_path = './upload/certificate/thumb/cer_1';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 100,
                        'height' => 100
                    );
                   $this->load->library('image_lib', $config_manip);
				//	$this->image_lib->initialize($config_manip);
					//$this->image_lib->resize();
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_compliance');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $certificate_thumb1 = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                   // $this->image_lib->clear();
                } else {
                    $certificate_image1 = "";
                    $certificate_thumb1 = "";
                    // $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    //redirect('dashboard/add_compliance');
                }

                /* 20.11.2015 _ Images Upload */
				
				$this->upload->initialize($this->set_upload_optionscer2());
                if ($this->upload->do_upload('image_certificate2')) {

                    $certificate_image2 = $this->upload->data('file_name');
					//echo $certificate_image2;
					//exit();
                    $filename1 = $certificate_image2;
					//echo $filename1;
					//exit();
                    $source_path1 = './upload/certificate/original/cer_2' . $filename1;
                    $target_path1= './upload/certificate/thumb/cer_2';

                    $config_manip1 = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path1,
                        'new_image' => $target_path1,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 100,
                        'height' => 100
                    );
                    $this->load->library('image_lib', $config_manip1);
				//	$this->image_lib->initialize($config_manip1);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_compliance');
                    }
                    // clear //
                    $thumb_name1 = explode(".", $filename1);
                    $certificate_thumb2 = $thumb_name1[0] . $config_manip1['thumb_marker'] . "." . $thumb_name1[1];
                    $this->image_lib->clear();
                } else {
                    $certificate_image2 = "";
                    $certificate_thumb2 = "";
                    // $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    //redirect('dashboard/add_compliance');
                }

                $a = $this->input->post('c_type');
                if ($a == '' || $a == NULL) {
                    $b = $this->input->post('c_type1');
                } else {
                    $b = $this->input->post('c_type');
                }

                
                $compliance = array(
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'c_valid_for' => $this->input->post('c_valid_for'),
                    'c_name' => $this->input->post('c_name'),
                    'c_owner' => $this->input->post('c_owner'),
                    'c_description' => $this->input->post('c_description'),
                    'c_type' => $b,
                    'c_importance' => $this->input->post('c_importance'),
                    'c_valid_from' => $this->input->post('c_valid_from'),
                    'c_valid_upto' => $this->input->post('c_valid_upto'),
                    'c_renewal' => $this->input->post('c_renewal'),
                    'c_file' => $certificate_image1,
                    'c_file_thumb' => $certificate_thumb1,
					'c_file_2' => $certificate_image2,
                    'c_file_2_thumb' => $certificate_thumb2,
                    'c_primary_notif_period' => $this->input->post('c_primary'),
                    'c_secondary_notif_period' => $this->input->post('c_secondary'),
                    'c_authority' => $this->input->post('c_authority'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
                /* echo "<pre>";
                  print_r($compliance);
                  exit(); */
                $query = $this->dashboard_model->add_compliance($compliance);

                if ($query) {
                    $this->session->set_flashdata('succ_msg', "Compliance Added Successfully!");
                    redirect('dashboard/all_compliance');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_compliance');
                }
            } else {
                $data['page'] = 'backend/dashboard/add_compliance';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function all_compliance() {

        $found = in_array("13", $this->arraay);
        if ($found) {
            $data['heading'] = 'Compliance';
            $data['sub_heading'] = 'All compliance';
            $data['description'] = 'Compliance List';
            $data['active'] = 'all_compliance';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));


            //$data['compliance'] = $this->dashboard_model->all_compliance_limit($config['per_page'], $segment);
            //$data['pagination'] = $this->pagination->create_links();

            $data['compliance'] = $this->dashboard_model->all_compliance_limit();
            $data['page'] = 'backend/dashboard/all_compliance';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function add_hrm() {

        $data['page'] = 'backend/dashboard/add_hrm';
        $this->load->view('backend/common/index', $data);
    }

    function all_hrm() {
        $data['page'] = 'backend/dashboard/all_hrm';
        $this->load->view('backend/common/index', $data);
    }

    function add_guest() {
         $this->load->library('upload');
         $this->load->library('image_lib');

        $found = in_array("7", $this->arraay);
        if ($found) {
            $corporate = $this->dashboard_model->fetch_corporate();
            //print_r($corporate);
            //exit();
            $data['corporate'] = $corporate;
            $data['heading'] = 'Guests';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Guest Here';
            $data['active'] = 'all_guest';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {
                $g_type = $this->input->post('g_type');
                if ($g_type !== "Military") {
                    $retired = "N/A";
                    $rank = "N/A";
                } else {
                    $retired = $this->input->post('retired');
                    $rank = $this->input->post('rank');
                }
               // $this->upload->initialize($this->set_upload_options());
                $this->upload->initialize($this->set_upload_optionsGst());
                if ($this->upload->do_upload('image_photo'))   {
                    /*  Image Upload 18.11.2015 */
					
                    $room_image = $this->upload->data('file_name');
                    $filename = $room_image;
                    $source_path = './upload/guest/originals/photo/' . $filename;
                    $target_path = './upload/guest/thumbnail/photo/';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                   // $this->load->library('image_lib', $config_manip);
                         $this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_guest');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $room_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					
					
                } else {
                    $room_image = "";
                    $room_thumb = "";
                } 
            $this->upload->initialize($this->set_upload_optionsGstId());
                if ($this->upload->do_upload('image_idpf')) {
			//	print_r($this->upload->data()); exit;
                    /*  Image Upload 18.11.2015 */
					
                    $room_image1 = $this->upload->data('file_name');
                    $filename1 = $room_image1;
                    $source_path1 = './upload/guest/originals/id_proof/'.$filename1;
                    $target_path1 = './upload/guest/thumbnail/id_proof/';

                    $config_manip1 = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path1,
                        'new_image' => $target_path1,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                   // $this->load->library('image_lib', $config_manip1);
                     $this->image_lib->initialize($config_manip1);
					//print_r($config_manip1); exit;
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_guest');
                    }
                    // clear //
                    $thumb_name1 = explode(".", $filename1);
                    $room_thumb1 = $thumb_name1[0] . $config_manip1['thumb_marker'] . "." . $thumb_name1[1];
                    $this->image_lib->clear();
                } else {
                    $room_image1 = "";
                    $room_thumb1 = "";
					
                }

                if ($this->input->post('g_dob')) {
                    $dob = date("Y-m-d", strtotime($this->input->post('g_dob')));
					$age=date_diff(date_create($dob), date_create('today'))->y;
					//$age++;
                } else {
                    $dob = '';
					$age='';
                }
				$other=$this->input->post('other_id');
				if(isset($other) && $other){
					$typ=$this->input->post('other_id');
				}else{
					$typ=$this->input->post('g_id_type');
				}
				
				
				if($this->input->post('highestQualification') == NULL){
					$hql_guest = '9';
				} else {
					$hql_guest = $this->input->post('highestQualification');
				}
				
				if(is_array($this->input->post('discount_rule'))){
					$disc_rl_guest = implode(",", $this->input->post('discount_rule'));
				} else {
					$disc_rl_guest = NULL;
				}
			
				
                $guest = array(
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'g_name' => $this->input->post('g_name'),
                    'g_place' => '',
					'g_age'=>$age,
                    'g_address' => $this->input->post('g_address'),
                    'g_contact_no' => $this->input->post('g_contact_no'),
                    'g_email' => $this->input->post('g_email'),
                    'g_gender' => $this->input->post('g_gender'),
                    'g_dob' => $dob,
                    'g_occupation' => $this->input->post('g_occupation'),
                    'g_married' => $this->input->post('g_married'),
                    'g_id_type' =>$typ,
                    'g_id_number' => $this->input->post('g_id_number'),
                    'g_pincode' => $this->input->post('g_place'),
                    'g_city' => $this->input->post('g_city'),
                    'g_photo' => $room_image,
                    'g_photo_thumb' => $room_thumb,
                    'g_id_proof' => $room_image1,
                    'g_id_proof_thumb' => $room_thumb1,
                    'g_state' => $this->input->post('g_state'),
                    'g_country' => $this->input->post('g_country'),
                    'g_type' => $this->input->post('g_type'),
                    'type' => $this->input->post('type'),
                    'f_size' => $this->input->post('f_size'),
                    'class' => $this->input->post('class'),
                    'retired' => $retired,
                    'rank' => $rank,
                    'corporate_id' => $this->input->post('c_name'),
                    'dep' => $this->input->post('dep'),
                    'c_des' => $this->input->post('c_des'),
                    'g_anniv' => $this->input->post('g_anniv'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                    'corporate_id' => $this->input->post('c_name'),
                    'g_highest_qual_name' => $this->input->post('qualificationName'),
                    'g_highest_qual_level' => $hql_guest,
                    'cp_discount_rule_id' =>  $disc_rl_guest,
                    'gstin' =>  $this->input->post('gstin')
                );
				
				//echo "<pre>";
				//print_r($guest); exit;

                $query = $this->dashboard_model->add_guest($guest);

                if ($query) {
                    $this->session->set_flashdata('succ_msg', "Guest Added Successfully!");
                    redirect('dashboard/all_guest');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_guest');
                }
            } else {
                $data['page'] = 'backend/dashboard/add_guest';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    } // End function add_guest

    function all_guest() {

        $found = in_array("8", $this->arraay);
        if ($found) {
            $corporate = $this->dashboard_model->fetch_corporate();
            //print_r($corporate);
            //exit();
            $data['corporate'] = $corporate;
            $data['heading'] = 'Guests';
            $data['sub_heading'] = 'All Guest';
            $data['description'] = 'Guest List';
            $data['active'] = 'all_guest';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['guest'] = $this->dashboard_model->all_guest_limit_view();
			//echo "<pre>";
			//print_r($data['guest']);exit;
            $data['page'] = 'backend/dashboard/all_guest';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function add_broker() {
		$this->load->library('upload');
		$this->load->library('image_lib');

        $found = in_array("19", $this->arraay);
        if ($found) {
            $data['heading'] = 'Broker & Channel';
            $data['sub_heading'] = 'Broker';
            $data['description'] = 'Add Broker Here';
            $data['active'] = 'all_broker';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                //$id_b = $this->input->post('b_contact');
                /* Images Upload */
				$broker_image="";
				$broker_thumb="";
				$broker_doc="";
                $this->upload->initialize($this->set_upload_broker());
                if ($this->upload->do_upload('image_photo')) {
                    /*  Image Upload 18.11.2015 */
                    $broker_image = $this->upload->data('file_name');
                    $filename = $broker_image;
                    $source_path = './upload/broker/image/' . $filename;
                    $target_path = './upload/broker/image/';

                    $config_manip = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                   // $this->load->library('image_lib', $config_manip);
				  $this->image_lib->initialize($config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/all_broker');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $broker_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                    /* Images Upload  18.11.2015 */
                } else {
                    $broker_thumb = "";
                    $broker_image = "";
                    //$this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    //redirect('dashboard/add_broker');
                }
				$this->upload->initialize($this->set_upload_broker_doc());
				if ($this->upload->do_upload('doc_photo')){
					$broker_doc=$this->upload->data('file_name');
					$broker_ext=explode(".", $broker_doc);
					if($broker_ext[1]=="jpg" || $broker_ext[1]=="png" || $broker_ext[1]=="gif" || $broker_ext[1]=="jpeg" ){
						$filename = $broker_doc;
						$source_path = './upload/broker/document/' . $filename;
						$target_path = './upload/broker/image/';

						$config_manip1 = array(
							//'file_name' => 'myfile',
							'image_library' => 'gd2',
							'source_image' => $source_path,
							'new_image' => $target_path,
							
						);
					   // $this->load->library('image_lib', $config_manip);
					  $this->image_lib->initialize($config_manip1);
					  if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_broker');
						
                    }
					$delete_doc= './upload/broker/document/' . $broker_doc;
						
						unlink($delete_doc);
					}
				}

                date_default_timezone_set("Asia/Kolkata");
                /* $files = $_FILES['image_photo'];
                  print_r($files);
                  $this->load->library('upload');
                  $new_name = str_replace(".","",microtime());
                  $config1['upload_path'] =FCPATH . 'upload/';
                  $config1['allowed_types'] = 'gif|jpg|png|jpeg';
                  $config1['file_name']=$new_name; */
                $broker = array(
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'b_agency' => $this->input->post('b_agency'),
                    'b_agency_name' => $this->input->post('b_agency_name'),
                    'b_name' => $this->input->post('b_name'),
                    'b_address' => $this->input->post('b_address'),
                    'b_contact' => $this->input->post('b_contact'),
                    'b_email' => $this->input->post('b_email'),
                    'b_website' => $this->input->post('b_website'),
                    'b_pan' => $this->input->post('b_pan'),
                    'b_bank_acc_no' => $this->input->post('b_bank_acc_no'),
                    'b_bank_ifsc' => $this->input->post('b_bank_ifsc'),
                    'b_photo' => $broker_image,
                    'b_photo_thumb' => $broker_thumb,
					'b_doc' => $broker_doc,
                    'broker_commission' => $this->input->post('b_commission'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
				/*echo "<pre>";
                print_r($broker); 
				exit();*/
                $query = $this->dashboard_model->add_broker($broker);

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Broker Added Successfully!");
                    redirect('dashboard/all_broker');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/all_broker');
                }
            } else {
                $data['page'] = 'backend/dashboard/add_broker';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function all_broker() {

        $found = in_array("19", $this->arraay);
        if ($found) {
            $data['heading'] = 'Broker & Channel';
            $data['sub_heading'] = 'Broker';
            $data['description'] = 'Broker List';
            $data['active'] = 'all_broker';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            /* if ($this->input->post('pages')) {
              $pages = $this->input->post('pages');
              } else {
              if ($this->session->flashdata('pages')) {
              $pages = $this->session->flashdata('pages');
              } else {
              $pages = 5;
              }
              }

              $this->session->set_flashdata('pages', $pages);

              $config['base_url'] = base_url() . 'dashboard/all_broker';
              $config['total_rows'] = $this->dashboard_model->all_broker_row();
              $config['per_page'] = $pages;
              $config['uri_segment'] = 3;
              $config['full_tag_open'] = "<ul class='pagination'>";
              $config['full_tag_close'] = "</ul>";
              $config['num_tag_open'] = '<li>';
              $config['num_tag_close'] = '</li>';
              $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
              $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
              $config['next_tag_open'] = "<li>";
              $config['next_tagl_close'] = "</li>";
              $config['prev_tag_open'] = "<li>";
              $config['prev_tagl_close'] = "</li>";
              $config['first_tag_open'] = "<li>";
              $config['first_tagl_close'] = "</li>";
              $config['last_tag_open'] = "<li>";
              $config['last_tagl_close'] = "</li>";


              $this->pagination->initialize($config);

              $segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

              $data['broker'] = $this->dashboard_model->all_broker_limit($config['per_page'], $segment);
              $data['pagination'] = $this->pagination->create_links(); */
            $data['broker'] = $this->dashboard_model->all_broker_limit();
            $data['page'] = 'backend/dashboard/all_broker';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function broker_payments() {

        $found = in_array("19", $this->arraay);
        if ($found) {
            if ($this->input->post() && $this->input->post('month') != '') {
                $data['m'] = $this->input->post('month');
                $data['y'] = $this->input->post('year');
                $data['maxDays'] = cal_days_in_month(CAL_GREGORIAN, $data['m'], $data['y']);
            } else {
                $data['y'] = date("Y");
                $data['m'] = date("m");
                $data['maxDays'] = date('t');
            }

            $data['heading'] = 'Broker & Channel';
            $data['sub_heading'] = 'Broker';
            $data['description'] = 'Broker Payments';
            $data['active'] = 'broker_payments';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $data['broker'] = $this->dashboard_model->all_broker_limit();
            if ($this->input->post()) {


                date_default_timezone_set("Asia/Kolkata");
                $commission = $this->input->post('b_amount');
                $id = $this->input->post('b_id');
                $query = $this->dashboard_model->pay_broker($id, $commission);

                $data = array(
                    't_amount' => $commission,
                    'transaction_type_id' => 3,
                    'details_id' => $this->input->post('b_id'),
                    'details_type' => 'Broker payment',
                    'transaction_type' => 'Broker payment',
                    't_date' => date("Y-m-d H:i:s"),
                    't_payment_mode' => $this->input->post('b_payment_mode'),
                    't_bank_name' => $this->input->post('b_bank_name'),
                    'transaction_releted_t_id' => 3,
                    'transaction_from_id' => 4,
                    'transaction_to_id' => 3,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
                $query2 = $this->dashboard_model->add_transaction1($data);

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Broker Paid Successfully!");
                    redirect('dashboard/broker_payments');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Kindly try again later!");
                    redirect('dashboard/broker_payments');
                }
            } else {
                $data['page'] = 'backend/dashboard/broker_payments';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function return_commission_broker() {
        //  $id=$_GET["b_id"];

        $id = $this->input->post('b_id');

        $array = $this->dashboard_model->get_brokerCommissionByID($id);

        // echo $array->broker_commission;
        // print_r($array);
        // exit();

        $data = array(
            'commission' => $array->broker_commission,
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function return_commission_channel() {
        //  $id=$_GET["b_id"];

        $id = $this->input->post('b_id');

        $array = $this->dashboard_model->get_channelCommissionByID($id);

        // echo $array->broker_commission;
        // print_r($array);
        // exit();

        $data = array(
            'commission' => $array->channel_commission,
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function broker_booking() {




        date_default_timezone_set("Asia/Kolkata");

        /* $commission = array(


          'broker_commssion_payed' => $this->input->post('b_amount'),



          ); */
        //$commission=$this->input->get('broker_amount');
        $commission = $_GET["broker_amount"];
        $b_id = $_GET["broker_id"];
        $booking_id = $_GET["booking_id"];

        //echo  $commission;
        // echo $b_id;
        //echo $booking_id;
        //exit();
        //$b_id=$this->input->get('broker_id');
        //$booking_id=$this->input->get('booking_id');
        $query = $this->dashboard_model->update_broker_commission($b_id, $commission);

        $data = array(
            'booking_id' => intval(substr($booking_id, -4)),
            'broker_id' => $b_id,
            'broker_commission' => $commission,
            'booking_source' => 'Broker',
        );

        $query2 = $this->dashboard_model->update_broker_booking($data);

        if (isset($query) && $query && $query2 && isset($query2)) {
            $this->session->set_flashdata('succ_msg', "Broker Booking Added Successfully!");
            // redirect('dashboard/broker_payments');
            echo "success";
        } else {
            $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
            //redirect('dashboard/broker_payments');
            echo "failure";
        }
    }

    function broker_booking2() {




        date_default_timezone_set("Asia/Kolkata");

        /* $commission = array(


          'broker_commssion_payed' => $this->input->post('b_amount'),



          ); */
        //$commission=$this->input->get('broker_amount');
        $commission = $_GET["broker_amount"];
        $b_id = $_GET["broker_id"];
        $booking_id = $_GET["booking_id"];
        //$b_id=$this->input->get('broker_id');
        //$booking_id=$this->input->get('booking_id');
        $query = $this->dashboard_model->update_broker_commission($b_id, $commission);

        $data = array(
            'booking_id' => intval(substr($booking_id, -4)),
            'broker_id' => $b_id,
            'broker_commission' => $commission,
            'booking_source' => 'Broker',
        );

        $query2 = $this->dashboard_model->update_broker_booking($data);

        if (isset($query) && $query && $query2 && isset($query2)) {
            $this->session->set_flashdata('succ_msg', "Broker Booking Added Successfully!");
            // redirect('dashboard/broker_payments');
            echo "success";
        } else {
            $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
            //redirect('dashboard/broker_payments');
            echo "failure";
        }
    }

    function showAmount() {
        $a = $this->input->post('term');
        //$this->load->model('Dashboard);
        $book = $this->dashboard_model->all_t_amount($a);
        //echo $book;
        foreach ($book as $row1) {

            $data1 = $row1->base_room_rent;
            $data2 = $row1->rent_mode_type;
            $data3 = $row1->mod_room_rent;
        }

        if ($data2 == 'P' || $data2 == 'p') {
            echo "Hello";
            exit();
        }

        $data = array(
            'base_room' => $data1,
            'aa' => $data2,
            'bb' => $data3
        );
        header('Content-Type: application/json');
        echo json_encode($data1);
        //print_r($book);
        //$this->load->view('backend/dashboard/add_transaction',$book);
        //echo $a;
    }

    function all_reports() {
        $found = in_array("38", $this->arraay);
        if ($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Guest Report';
            $data['description'] = 'Guest Reports';
            $data['active'] = 'all_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['bookings'] = $this->dashboard_model->all_bookings_report();
            $data['transactions'] = $this->dashboard_model->all_transactions();
            $data['pagination'] = $this->pagination->create_links();

            $data['page'] = 'backend/dashboard/all_reports';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function report_search() {
        $data['heading'] = 'Hotel Reports';
        $data['sub_heading'] = 'Guest Report';
        $data['description'] = 'Guest Report';
        $data['active'] = 'all_reports';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $visit = '';
        $date_1 = '';
        $date_2 = '';
        if ($this->input->post()) {

            //$room=$this->input->post('r_room');
            //$status=$this->input->post('checked_in_guest');
            $r_hotel = $this->input->post('r_hotel');


            //$data['r_hotel']=$this->input->post('r_hotel');
            $data['r_visit'] = $this->input->post('r_visit');
            $data['date_1'] = $this->input->post('r_date_1');
            $data['date_2'] = $this->input->post('r_date_2');

            if ($this->input->post('r_date_1') && $this->input->post('r_date_2')) {

                $date_1 = date('Y-m-d', strtotime($this->input->post('r_date_1')));
                $date_2 = date('Y-m-d', strtotime($this->input->post('r_date_2')));
            }
            $visit = $this->input->post('r_visit');
            //echo $visit . $date_1.$date_2;
            //exit;
        }
        $data['bookings'] = $this->dashboard_model->report_search($visit, $date_1, $date_2);
        $data['transactions'] = $this->dashboard_model->all_transactions();



        //print_r($data['bookings']);
        //exit;

        $data['page'] = 'backend/dashboard/all_reports';
        $this->load->view('backend/common/index', $data);



        /* }


          else {


          $data['bookings'] = $this->dashboard_model->all_bookings_report();
          $data['transactions'] = $this->dashboard_model->all_transactions();
          //$data['pagination'] = $this->pagination->create_links();

          $data['page'] = 'backend/dashboard/all_reports';
          $this->load->view('backend/common/index', $data);

          } */
    }

    function all_f_reports() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Financial Reports';
            $data['description'] = 'Report List';
            $data['active'] = 'all_f_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));



            //$data['transactions'] = $this->dashboard_model->all_transactions_limit($config['per_page'], $segment);
            //$data['pagination'] = $this->pagination->create_links();
            $data['transactions'] = $this->dashboard_model->all_f_reports();
            //print_r($data['transactions']); exit;

            $data['types'] = $this->dashboard_model->all_f_types();
            $data['entity'] = $this->dashboard_model->all_f_entity();
            $data['page'] = 'backend/dashboard/all_f_reports';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function financial_report() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $data['heading'] = 'Reports';
            $data['sub_heading'] = 'Transaction Report';
            $data['description'] = 'Report List';
            $data['active'] = 'all_f_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));



            //$data['transactions'] = $this->dashboard_model->all_transactions_limit($config['per_page'], $segment);
            //$data['pagination'] = $this->pagination->create_links();
            $data['transactions'] = $this->dashboard_model->all_f_reports();
            $data['types'] = $this->dashboard_model->all_f_types();
            $data['entity'] = $this->dashboard_model->all_f_entity();
            $data['page'] = 'backend/dashboard/financial_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function reservation_summary() {

        $found = in_array("38", $this->arraay);
        if ($found) {

            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Reservation Reports';
            $data['description'] = 'Reservation summary';
            $data['active'] = 'reservation_summary';

            // $data['usage_log'] = $this->dashboard_model->all_usage_log();


            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['summary'] = $this->dashboard_model->fetch_r_summary();
            $data['page'] = 'backend/dashboard/reservation_summary';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function group_summary() {

        $data['heading'] = 'Hotel Reports';
        $data['sub_heading'] = 'Reservation Reports';
        $data['description'] = 'Reservation summary';
        $data['active'] = 'group_summary';

        // $data['usage_log'] = $this->dashboard_model->all_usage_log();
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/guest_report';
        $this->load->view('backend/common/index', $data);
    }

    function group_report() {
        $start_date = date("Y-m-d");
        $end_date = date("Y-m-d");
        $data['heading'] = 'Reports';
        $data['sub_heading'] = 'Transaction Report';
        $data['description'] = 'Report List';
        $data['active'] = 'all_f_reports';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        if ($this->input->post()) {
            $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
        }


        $data['reports'] = $this->dashboard_model->all_guest_by_date($start_date, $end_date);
        $data['page'] = 'backend/dashboard/guest_report';
        $this->load->view('backend/common/index', $data);
    }

    function reservation_report() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Reservation Reports';
            $data['description'] = 'Reservation reports';
            $data['active'] = 'reservation_report';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['reports'] = $this->dashboard_model->all_bookings();
            $data['page'] = 'backend/dashboard/reservation_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function note_preference_report() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Reservation Reports';
            $data['description'] = 'Note & Preference Report  ';
            $data['active'] = 'note_preference_report';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            //$data['reports'] = $this->dashboard_model->all_note_preference();
            $data['bookings'] = $this->dashboard_model->all_bookings();
            $data['page'] = 'backend/dashboard/note_preference_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function note_preference_report_datefilter() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $start_date = date("Y-m-d");
            $end_date = date("Y-m-d");
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $data['heading'] = 'Reports';
            $data['sub_heading'] = 'Transaction Report';
            $data['description'] = 'Report List';
            $data['active'] = 'all_f_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
                $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
                $data['start_date'] = $this->input->post('t_dt_frm');
                $data['end_date'] = $this->input->post('t_dt_to');
            }

            $data['bookings'] = $this->dashboard_model->note_preference_by_date_datefilter($start_date, $end_date);

            $data['page'] = 'backend/dashboard/note_preference_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function tax_report() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Tax Reports';
            $data['description'] = 'Tax Report';
            $data['active'] = 'tax_report';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
                $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));


                $data['start_date'] = $this->input->post('t_dt_frm');
                $data['end_date'] = $this->input->post('t_dt_to');
                $data['line_item'] = $this->dashboard_model->all_tax_report_by_date($start_date, $end_date);
            } else {
                $data['line_item'] = $this->dashboard_model->get_All_booking_details();
            }

            $data['page'] = 'backend/dashboard/tax_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function tax_filter() {
        $start_date = date("Y-m-d", strtotime($this->input->post('start_date')));
        $end_date = date("Y-m-d", strtotime($this->input->post('end_date')));
        $type = $this->input->post('type');
        if ($type == 'mp_tax') {
            $line_item = $this->dashboard_model->all_tax_report_by_date($start_date, $end_date);
            $output = '<table class="table table-striped table-hover table-bordered" id="sample_10">
            <thead>
              <tr> 
                <th> # </th>
				<th>Booking ID </th>
				<th> Guest Name </th>
				<th> Room No</th>
                 <th> Booking Date</th>';

            $f = 0;
            $cm = 0;
            $as = array();
            $sa = array();
            $taxType = $this->dashboard_model->get_distinct_tax_type();
            //echo "<pre>";
            //print_r($taxType);
            if (isset($taxType) && $taxType) {

                foreach ($taxType as $tax) {

                    array_push($as, $tax->tax_name);

                    $output .= '<th>' . $tax->tax_name . '</th>';
                }

                $output .= ' <th class="">POS Tax</th>
                <th class="">Extra Tax</th>
              	<th class="">Total Tax</th>
				 </tr></thead><tbody>';
                $sl = 0;
                $mf = 0;
                $co = 0;
                $pos_tax1 = 0;
                $pos_tax = 0;
                $pos_tot_amount1 = 0;
                $ch_tax = 0.00;
                $s_tax = 0.00;
                $charge_details = 0;
                if (isset($line_item) && $line_item) {
                    //$id=$item->booking_id;

                    foreach ($line_item as $item) {
                        //if(isset($item->booking_extra_charge_id) && $item->booking_extra_charge_id>0){
                        $charge_id_array = explode(",", $item->booking_extra_charge_id);
                        $ch_tax = 0.00;
                        for ($i = 1; $i < sizeof($charge_id_array); $i++) {

                            $charge_details = $this->dashboard_model->get_charge_details($charge_id_array[$i]);
                            $ch_qty = $charge_details->crg_quantity;
                            $ch_price = $charge_details->crg_unit_price;
                            $ch_amt = $ch_price * ($charge_details->crg_tax / 100) * $ch_qty;
                            $ch_tax = $ch_tax + $ch_amt;
                            //echo " ch tax ".$ch_tax." id ".$item->booking_id;
                        }

                        //}	
                        if ($item->booking_id_actual != '') {

                            $id = $item->booking_id;
                            $extra_ser = $this->dashboard_model->get_service_details2($id);
                            if (isset($extra_ser) && $extra_ser) {
                                foreach ($extra_ser as $service_tax) {
                                    //$s_tax+=$service_tax->tax;
                                    $sp = $service_tax->s_price;
                                    $qty = $service_tax->qty;
                                    $stax_amt = $sp * ($service_tax->tax / 100) * $qty;
                                    $s_tax = $stax_amt;
                                }
                            } else {
                                $s_tax = 0;
                            }
                        } else {
                            $id = $item->booking_id;
                            $extra_ser = $this->dashboard_model->get_service_details3($id);
                            if (isset($extra_ser) && $extra_ser) {

                                foreach ($extra_ser as $service_tax) {
                                    $sp = $service_tax->s_price;
                                    $qty = $service_tax->qty;
                                    $stax_amt = $sp * ($service_tax->tax / 100) * $qty;
                                    $s_tax = $stax_amt;
                                    //echo $stax_amt."rtyhtf";
                                }
                            } else {
                                $s_tax = 0;
                            }
                        }

                        $extra_tax_amt = $ch_tax + $s_tax;

                        $output .= '<td>' . ++$sl . '</td>';
                        $output .= '<td>';
                        if ($item->booking_id_actual != '') {
                            $output .= $item->booking_id_actual;
                        } else {
                            $output .= "HM0GRP600" . $item->group_id;
                        }

                        $output .= '</td>';
                        $output .= '<td>' . $item->cust_name . '</td>';
                        $output .= '<td>' . $item->room_no . '</td>';
                        /* echo date("dS-M-Y",strtotime($item->cust_from_date));?>
                          <div style="color:#1F897F;">-to-</div>
                          <?php

                          echo date("dS-M-Y",strtotime($item->cust_end_date));?> */
                        $output .= '<td>' . date("dS-M-Y", strtotime($item->cust_from_date_actual)) . '<div style="color:#1F897F;">-to-</div>' . date("dS-M-Y", strtotime($item->cust_end_date_actual)) . '</td>';


                        if ($item->group_id == 0) {
                            $poses = $this->dashboard_model->all_pos_booking($item->booking_id);


                            if ($poses) {
                                foreach ($poses as $key) {
                                    $pos_tax = $key->tax;
                                    $pos_tot_amount1 = $key->total_amount;
                                }
                            }
                            $type1 = 'sb';
                            $bid = $item->booking_id;
                        } else {
                            $type1 = 'gb';
                            $bid = $item->group_id;
                            $poses_gb = $this->dashboard_model->all_pos_booking_group($item->group_id);
                            $pos_amount1 = 0;

                            if ($poses_gb) {
                                foreach ($poses_gb as $key1) {
                                    $pos_tax1 = $key1->tax;
                                    $pos_tot_amount1 = $key1->total_amount;
                                }
                            }
                        }

                        $taxData = $this->bookings_model->line_charge_item_tax($type1, $bid);

                        if (isset($taxData) && $taxData) {
                            //unset($taxData['mp_tax']['total']);
                            unset($taxData['mp_tax']['r_id']);
                            unset($taxData['mp_tax']['planName']);
                            //	unset($taxData['mp_tax']['Service Charge']);

                            unset($taxData['rr_tax']['r_id']);
                            unset($taxData['rr_tax']['planName']);
                            unset($taxData['rr_tax']['total']);
                            $taxData['booking_id'] = $bid;
                            $a1 = $taxData['rr_tax'];
                            $a2 = $taxData['mp_tax']; //print_r($taxData);
                            $sa = $taxData['mp_tax'];
                            //$sa = array_merge($taxData['rr_tax'],$taxData['mp_tax']);
                            //print_r($sa);
                            if ($taxData['mp_tax']) {
                                $f = count($taxData['mp_tax']);
                            } else {//echo ;
                            }

                            foreach ($as as $key => $val1) {

                                if (array_key_exists($val1, $sa)) {
                                    $output .= '<td>' . $sa[$val1] . '</td>';
                                } else {
                                    $output .= '<td>0.00</td>';
                                }
                            }
                        } else {
                            if (isset($taxType) && $taxType) {

                                foreach ($taxType as $tax) {

                                    $output .= '<td>0.00</td>';
                                }
                            }
                        }

                        $output .= '<td>';
                        if ($item->group_id != '' && $item->group_id != 0) {
                            $output .= $pos_tax1;
                        } else {
                            $output .= $pos_tax;
                        }
                        $output .= '</td>';

                        $output .= '<td>';

                        $output .= number_format($extra_tax_amt, 2, '.', ',');

                        $output .= '</td>';



                        $output .= '<td>';
                        $output .= $item->mp_tax + $pos_tax + $pos_tax1 + $extra_tax_amt;

                        $output .= '</td>';

                        $output .= '</tr>';
                    }
                }

                $output .= '</tr>';

                $output .= '</tbody></table>';
            }
            echo $output;
        } elseif ($type == 'rm_tax') {
            $line_item = $this->dashboard_model->all_tax_report_by_date($start_date, $end_date);
            $output = '<table class="table table-striped table-hover table-bordered" id="sample_10">
            <thead>
              <tr> 
                <th> # </th>
				<th>Booking ID </th>
				<th> Guest Name </th>
				<th> Room No</th>
                 <th> Booking Date</th>';

            $f = 0;
            $cm = 0;
            $as = array();
            $sa = array();
            $taxType = $this->dashboard_model->get_distinct_tax_type();
            //echo "<pre>";
            //print_r($taxType);
            if (isset($taxType) && $taxType) {

                foreach ($taxType as $tax) {
                    if ($tax->tax_name != 'VAT') {
                        array_push($as, $tax->tax_name);

                        $output .= '<th>' . $tax->tax_name . '</th>';
                    }
                }

                $output .= ' <th class="">POS Tax</th>
                <th class="">Extra Tax</th>
              	<th class="">Total Tax</th>
				 </tr></thead><tbody>';
                $sl = 0;
                $mf = 0;
                $co = 0;
                $pos_tax1 = 0;
                $pos_tax = 0;
                $pos_tot_amount1 = 0;
                $ch_tax = 0.00;
                $s_tax = 0.00;
                $charge_details = 0;
                if (isset($line_item) && $line_item) {
                    //$id=$item->booking_id;

                    foreach ($line_item as $item) {
                        //if(isset($item->booking_extra_charge_id) && $item->booking_extra_charge_id>0){
                        $charge_id_array = explode(",", $item->booking_extra_charge_id);
                        $ch_tax = 0.00;
                        for ($i = 1; $i < sizeof($charge_id_array); $i++) {

                            $charge_details = $this->dashboard_model->get_charge_details($charge_id_array[$i]);
                            $ch_qty = $charge_details->crg_quantity;
                            $ch_price = $charge_details->crg_unit_price;
                            $ch_amt = $ch_price * ($charge_details->crg_tax / 100) * $ch_qty;
                            $ch_tax = $ch_tax + $ch_amt;
                            //echo " ch tax ".$ch_tax." id ".$item->booking_id;
                        }

                        //}	
                        if ($item->booking_id_actual != '') {

                            $id = $item->booking_id;
                            $extra_ser = $this->dashboard_model->get_service_details2($id);
                            if (isset($extra_ser) && $extra_ser) {
                                foreach ($extra_ser as $service_tax) {
                                    //$s_tax+=$service_tax->tax;
                                    $sp = $service_tax->s_price;
                                    $qty = $service_tax->qty;
                                    $stax_amt = $sp * ($service_tax->tax / 100) * $qty;
                                    $s_tax = $stax_amt;
                                }
                            } else {
                                $s_tax = 0;
                            }
                        } else {
                            $id = $item->booking_id;
                            $extra_ser = $this->dashboard_model->get_service_details3($id);
                            if (isset($extra_ser) && $extra_ser) {

                                foreach ($extra_ser as $service_tax) {
                                    $sp = $service_tax->s_price;
                                    $qty = $service_tax->qty;
                                    $stax_amt = $sp * ($service_tax->tax / 100) * $qty;
                                    $s_tax = $stax_amt;
                                    //echo $stax_amt."rtyhtf";
                                }
                            } else {
                                $s_tax = 0;
                            }
                        }

                        $extra_tax_amt = $ch_tax + $s_tax;

                        $output .= '<td>' . ++$sl . '</td>';
                        $output .= '<td>';
                        if ($item->booking_id_actual != '') {
                            $output .= $item->booking_id_actual;
                        } else {
                            $output .= "HM0GRP600" . $item->group_id;
                        }

                        $output .= '</td>';
                        $output .= '<td>' . $item->cust_name . '</td>';
                        $output .= '<td>' . $item->room_no . '</td>';
                        $output .= '<td>' . date("dS-M-Y", strtotime($item->cust_from_date_actual)) . '<div style="color:#1F897F;">-to-</div>' . date("dS-M-Y", strtotime($item->cust_end_date_actual)) . '</td>';


                        if ($item->group_id == 0) {
                            $poses = $this->dashboard_model->all_pos_booking($item->booking_id);


                            if ($poses) {
                                foreach ($poses as $key) {
                                    $pos_tax = $key->tax;
                                    $pos_tot_amount1 = $key->total_amount;
                                }
                            }
                            $type1 = 'sb';
                            $bid = $item->booking_id;
                        } else {
                            $type1 = 'gb';
                            $bid = $item->group_id;
                            $poses_gb = $this->dashboard_model->all_pos_booking_group($item->group_id);
                            $pos_amount1 = 0;

                            if ($poses_gb) {
                                foreach ($poses_gb as $key1) {
                                    $pos_tax1 = $key1->tax;
                                    $pos_tot_amount1 = $key1->total_amount;
                                }
                            }
                        }

                        $taxData = $this->bookings_model->line_charge_item_tax($type1, $bid);

                        if (isset($taxData) && $taxData) {
                            //unset($taxData['mp_tax']['total']);
                            unset($taxData['mp_tax']['r_id']);
                            unset($taxData['mp_tax']['planName']);
                            unset($taxData['mp_tax']['Service Charge']);

                            unset($taxData['rr_tax']['r_id']);
                            unset($taxData['rr_tax']['planName']);
                            unset($taxData['rr_tax']['total']);
                            $taxData['booking_id'] = $bid;
                            $a1 = $taxData['rr_tax'];
                            $a2 = $taxData['mp_tax']; //print_r($taxData);
                            //$sa = $taxData['rr_tax'];
                            $sa = array_merge($taxData['rr_tax'], $taxData['mp_tax']);

                            if ($taxData['mp_tax']) {
                                $f = count($taxData['mp_tax']);
                            } else {//echo ;
                            }

                            foreach ($as as $key => $val1) {
                                if ($val1 != 'VAT') {
                                    if (array_key_exists($val1, $sa)) {
                                        $output .= '<td>' . $sa[$val1] . '</td>';
                                    } else {
                                        $output .= '<td>0.00</td>';
                                    }
                                }
                            }
                        } else {
                            if (isset($taxType) && $taxType) {

                                foreach ($taxType as $tax) {
                                    if ($tax->tax_name != 'VAT') {
                                        $output .= '<td>0.00</td>';
                                    }
                                }
                            }
                        }

                        $output .= '<td>';
                        if ($item->group_id != '' && $item->group_id != 0) {
                            $output .= $pos_tax1;
                        } else {
                            $output .= $pos_tax;
                        }
                        $output .= '</td>';

                        $output .= '<td>';

                        $output .= number_format($extra_tax_amt, 2, '.', ',');

                        $output .= '</td>';



                        $output .= '<td>';
                        $output .= $item->rr_tot_tax + $pos_tax + $pos_tax1 + $extra_tax_amt;

                        $output .= '</td>';

                        $output .= '</tr>';
                    }
                }

                $output .= '</tr>';

                $output .= '</tbody></table>';
            }
            echo $output;
        } elseif ($type == "both_tax") {

            $output = '<table class="table table-striped table-hover table-bordered" id="sample_10">
            <thead>
              <tr> 
                <th> # </th>
				<th>Booking ID </th>
				<th> Guest Name </th>
				<th> Room No</th>
                 <th> Booking Date</th>';

            $f = 0;
            $cm = 0;
            $as = array();
            $sa = array();
            $taxType = $this->dashboard_model->get_distinct_tax_type();
            //echo "<pre>";
            //print_r($taxType);
            if (isset($taxType) && $taxType) {

                foreach ($taxType as $tax) {

                    array_push($as, $tax->tax_name);

                    $output .= '<th>' . $tax->tax_name . '</th>';
                }

                $output .= ' <th class="">POS Tax</th>
                <th class="">Extra Tax</th>
              	<th class="">Total Tax</th>
				 </tr></thead><tbody>';
                $sl = 0;
                $mf = 0;
                $co = 0;
                $pos_tax1 = 0;
                $pos_tax = 0;
                $pos_tot_amount1 = 0;
                $ch_tax = 0.00;
                $s_tax = 0.00;
                $charge_details = 0;
                $line_item = $this->dashboard_model->all_tax_report_by_date($start_date, $end_date);
                if (isset($line_item) && $line_item) {
                    //$id=$item->booking_id;

                    foreach ($line_item as $item) {
                        //if(isset($item->booking_extra_charge_id) && $item->booking_extra_charge_id>0){
                        $charge_id_array = explode(",", $item->booking_extra_charge_id);
                        $ch_tax = 0.00;
                        for ($i = 1; $i < sizeof($charge_id_array); $i++) {

                            $charge_details = $this->dashboard_model->get_charge_details($charge_id_array[$i]);
                            $ch_qty = $charge_details->crg_quantity;
                            $ch_price = $charge_details->crg_unit_price;
                            $ch_amt = $ch_price * ($charge_details->crg_tax / 100) * $ch_qty;
                            $ch_tax = $ch_tax + $ch_amt;
                            //echo " ch tax ".$ch_tax." id ".$item->booking_id;
                        }

                        //}	
                        if ($item->booking_id_actual != '') {

                            $id = $item->booking_id;
                            $extra_ser = $this->dashboard_model->get_service_details2($id);
                            if (isset($extra_ser) && $extra_ser) {
                                foreach ($extra_ser as $service_tax) {
                                    //$s_tax+=$service_tax->tax;
                                    $sp = $service_tax->s_price;
                                    $qty = $service_tax->qty;
                                    $stax_amt = $sp * ($service_tax->tax / 100) * $qty;
                                    $s_tax = $stax_amt;
                                }
                            } else {
                                $s_tax = 0;
                            }
                        } else {
                            $id = $item->booking_id;
                            $extra_ser = $this->dashboard_model->get_service_details3($id);
                            if (isset($extra_ser) && $extra_ser) {

                                foreach ($extra_ser as $service_tax) {
                                    $sp = $service_tax->s_price;
                                    $qty = $service_tax->qty;
                                    $stax_amt = $sp * ($service_tax->tax / 100) * $qty;
                                    $s_tax = $stax_amt;
                                    //echo $stax_amt."rtyhtf";
                                }
                            } else {
                                $s_tax = 0;
                            }
                        }

                        $extra_tax_amt = $ch_tax + $s_tax;

                        $output .= '<td>' . ++$sl . '</td>';
                        $output .= '<td>';
                        if ($item->booking_id_actual != '') {
                            $output .= $item->booking_id_actual;
                        } else {
                            $output .= "HM0GRP600" . $item->group_id;
                        }

                        $output .= '</td>';
                        $output .= '<td>' . $item->cust_name . '</td>';
                        $output .= '<td>' . $item->room_no . '</td>';
                        $output .= '<td>' . date("dS-M-Y", strtotime($item->cust_from_date_actual)) . '<div style="color:#1F897F;">-to-</div>' . date("dS-M-Y", strtotime($item->cust_end_date_actual)) . '</td>';


                        if ($item->group_id == 0) {
                            $poses = $this->dashboard_model->all_pos_booking($item->booking_id);


                            if ($poses) {
                                foreach ($poses as $key) {
                                    $pos_tax = $key->tax;
                                    $pos_tot_amount1 = $key->total_amount;
                                }
                            }
                            $type1 = 'sb';
                            $bid = $item->booking_id;
                        } else {
                            $type1 = 'gb';
                            $bid = $item->group_id;
                            $poses_gb = $this->dashboard_model->all_pos_booking_group($item->group_id);
                            $pos_amount1 = 0;

                            if ($poses_gb) {
                                foreach ($poses_gb as $key1) {
                                    $pos_tax1 = $key1->tax;
                                    $pos_tot_amount1 = $key1->total_amount;
                                }
                            }
                        }

                        $taxData = $this->bookings_model->line_charge_item_tax($type1, $bid);

                        if (isset($taxData) && $taxData) {
                            //unset($taxData['mp_tax']['total']);
                            unset($taxData['mp_tax']['r_id']);
                            unset($taxData['mp_tax']['planName']);
                            //unset($taxData['mp_tax']['Service Charge']);

                            unset($taxData['rr_tax']['r_id']);
                            unset($taxData['rr_tax']['planName']);
                            unset($taxData['rr_tax']['total']);
                            $taxData['booking_id'] = $bid;
                            $a1 = $taxData['rr_tax'];
                            $a2 = $taxData['mp_tax']; //print_r($taxData);
                            $sum_tax = array();
                            /* for($i=1;$i<=2;$i++){
                              $sum_tax[$i]=$a1+$a2;
                              } */
                            //print_r($sum_tax[$i]);
                            //$sa = $taxData['rr_tax'];
                            //$sa = array_merge($taxData['rr_tax'],$taxData['mp_tax']);
                            //...............NB


                            $keys = array_fill_keys(array_keys($a1 + $a2), 0);

                            $sum_array = array();
                            //................NB
                            $sa = array_merge($keys, $a1);
                            $sb = array_merge($keys, $a2);




                            $sc = array();
                            foreach (array_keys($sa + $sb) as $key) {
                                $sc[$key] = $sa[$key] + $sb[$key];
                            }
                            if ($taxData['mp_tax']) {
                                $f = count($taxData['mp_tax']);
                            } else {//echo ;
                            }

                            foreach ($as as $key => $val1) {

                                if (array_key_exists($val1, $sc)) {
                                    $output .= '<td>' . $sc[$val1] . '</td>';
                                } else {
                                    $output .= '<td>0.00</td>';
                                }
                            }
                        } else {
                            if (isset($taxType) && $taxType) {

                                foreach ($taxType as $tax) {

                                    $output .= '<td>0.00</td>';
                                }
                            }
                        }

                        $output .= '<td>';
                        if ($item->group_id != '' && $item->group_id != 0) {
                            $output .= $pos_tax1;
                        } else {
                            $output .= $pos_tax;
                        }
                        $output .= '</td>';

                        $output .= '<td>';

                        $output .= number_format($extra_tax_amt, 2, '.', ',');

                        $output .= '</td>';



                        $output .= '<td>';
                        $output .= $item->rr_tot_tax + $item->mp_tax + $pos_tax + $pos_tax1 + $extra_tax_amt;

                        $output .= '</td>';

                        $output .= '</tr>';
                    }
                }

                $output .= '</tr>';

                $output .= '</tbody></table>';
            }
            echo $output;
        }
        // $data['line_item'] = $this->dashboard_model->all_tax_report_by_date($start_date,$end_date);
    }

    function rr_tax_report() {
        $line_item = $this->dashboard_model->get_All_booking_details();
        $output = '<table class="table table-striped table-hover table-bordered" id="sample_10">
            <thead>
              <tr> 
                <th> # </th>
				<th>Booking ID </th>
				<th> Guest Name </th>
				<th> Room No</th>
                 <th> Booking Date</th>';

        $f = 0;
        $cm = 0;
        $as = array();
        $sa = array();
        $taxType = $this->dashboard_model->get_distinct_tax_type();
        //echo "<pre>";
        //print_r($taxType);
        if (isset($taxType) && $taxType) {

            foreach ($taxType as $tax) {
                if ($tax->tax_name != 'VAT') {
                    array_push($as, $tax->tax_name);

                    $output .= '<th>' . $tax->tax_name . '</th>';
                }
            }

            $output .= ' <th class="">POS Tax</th>
                <th class="">Extra Tax</th>
              	<th class="">Total Tax</th>
				 </tr></thead><tbody>';
            $sl = 0;
            $mf = 0;
            $co = 0;
            $pos_tax1 = 0;
            $pos_tax = 0;
            $pos_tot_amount1 = 0;
            $ch_tax = 0.00;
            $s_tax = 0.00;
            $charge_details = 0;
            if (isset($line_item) && $line_item) {
                //$id=$item->booking_id;

                foreach ($line_item as $item) {
                    //if(isset($item->booking_extra_charge_id) && $item->booking_extra_charge_id>0){
                    $charge_id_array = explode(",", $item->booking_extra_charge_id);
                    $ch_tax = 0.00;
                    for ($i = 1; $i < sizeof($charge_id_array); $i++) {

                        $charge_details = $this->dashboard_model->get_charge_details($charge_id_array[$i]);
                       if(isset($charge_details->crg_quantity) && $charge_details->crg_quantity){
					   $ch_qty = $charge_details->crg_quantity;
					   }else{
						    $ch_qty =0;
					   }
					   if(isset($charge_details->crg_unit_price) && $charge_details->crg_unit_price){
                        $ch_price = $charge_details->crg_unit_price;
					   }else{
						    $ch_price = 0;
					   }
					   if(isset($charge_details->crg_tax) && $charge_details->crg_tax){
						   $crg=$charge_details->crg_tax;
					   }else{
						   $crg=0;
						   
					   }
                        $ch_amt = $ch_price * ($crg / 100) * $ch_qty;
                        $ch_tax = $ch_tax + $ch_amt;
                        //echo " ch tax ".$ch_tax." id ".$item->booking_id;
                    }

                    //}	
                    if ($item->booking_id_actual != '') {

                        $id = $item->booking_id;
                        $extra_ser = $this->dashboard_model->get_service_details2($id);
                        if (isset($extra_ser) && $extra_ser) {
                            foreach ($extra_ser as $service_tax) {
                                //$s_tax+=$service_tax->tax;
                                $sp = $service_tax->s_price;
                                $qty = $service_tax->qty;
                                $stax_amt = $sp * ($service_tax->tax / 100) * $qty;
                                $s_tax = $stax_amt;
                            }
                        } else {
                            $s_tax = 0;
                        }
                    } else {
                        $id = $item->booking_id;
                        $extra_ser = $this->dashboard_model->get_service_details3($id);
                        if (isset($extra_ser) && $extra_ser) {

                            foreach ($extra_ser as $service_tax) {
                                $sp = $service_tax->s_price;
                                $qty = $service_tax->qty;
                                $stax_amt = $sp * ($service_tax->tax / 100) * $qty;
                                $s_tax = $stax_amt;
                                //echo $stax_amt."rtyhtf";
                            }
                        } else {
                            $s_tax = 0;
                        }
                    }

                    $extra_tax_amt = $ch_tax + $s_tax;

                    $output .= '<td>' . ++$sl . '</td>';
                    $output .= '<td>';
                    if ($item->booking_id_actual != '') {
                        $output .= $item->booking_id_actual;
                    } else {
                        $output .= "HM0GRP600" . $item->group_id;
                    }

                    $output .= '</td>';
                    $output .= '<td>' . $item->cust_name . '</td>';
                    $output .= '<td>' . $item->room_no . '</td>';
                    $output .= '<td>' . date("dS-M-Y", strtotime($item->cust_from_date_actual)) . '<div style="color:#1F897F;">-to-</div>' . date("dS-M-Y", strtotime($item->cust_end_date_actual)) . '</td>';


                    if ($item->group_id == 0) {
                        $poses = $this->dashboard_model->all_pos_booking($item->booking_id);


                        if ($poses) {
                            foreach ($poses as $key) {
                                $pos_tax = $key->tax;
                                $pos_tot_amount1 = $key->total_amount;
                            }
                        }
                        $type1 = 'sb';
                        $bid = $item->booking_id;
                    } else {
                        $type1 = 'gb';
                        $bid = $item->group_id;
                        $poses_gb = $this->dashboard_model->all_pos_booking_group($item->group_id);
                        $pos_amount1 = 0;

                        if ($poses_gb) {
                            foreach ($poses_gb as $key1) {
                                $pos_tax1 = $key1->tax;
                                $pos_tot_amount1 = $key1->total_amount;
                            }
                        }
                    }

                    $taxData = $this->bookings_model->line_charge_item_tax($type1, $bid);

                    if (isset($taxData) && $taxData) {
                        //unset($taxData['mp_tax']['total']);
                        unset($taxData['mp_tax']['r_id']);
                        unset($taxData['mp_tax']['planName']);
                        unset($taxData['mp_tax']['Service Charge']);

                        unset($taxData['rr_tax']['r_id']);
                        unset($taxData['rr_tax']['planName']);
                        unset($taxData['rr_tax']['total']);
                        $taxData['booking_id'] = $bid;
                        $a1 = $taxData['rr_tax'];
                        $a2 = $taxData['mp_tax']; //print_r($taxData);
                        //$sa = $taxData['rr_tax'];
                        $sa = array_merge($taxData['rr_tax'], $taxData['mp_tax']);

                        if ($taxData['mp_tax']) {
                            $f = count($taxData['mp_tax']);
                        } else {//echo ;
                        }

                        foreach ($as as $key => $val1) {
                            if ($val1 != 'VAT') {
                                if (array_key_exists($val1, $sa)) {
                                    $output .= '<td>' . $sa[$val1] . '</td>';
                                } else {
                                    $output .= '<td>0.00</td>';
                                }
                            }
                        }
                    } else {
                        if (isset($taxType) && $taxType) {

                            foreach ($taxType as $tax) {
                                if ($tax->tax_name != 'VAT') {
                                    $output .= '<td>0.00</td>';
                                }
                            }
                        }
                    }

                    $output .= '<td>';
                    if ($item->group_id != '' && $item->group_id != 0) {
                        $output .= $pos_tax1;
                    } else {
                        $output .= $pos_tax;
                    }
                    $output .= '</td>';

                    $output .= '<td>';

                    $output .= number_format($extra_tax_amt, 2, '.', ',');

                    $output .= '</td>';



                    $output .= '<td>';
                    $output .= $item->rr_tot_tax + $pos_tax + $pos_tax1 + $extra_tax_amt;

                    $output .= '</td>';

                    $output .= '</tr>';
                }
            }

            $output .= '</tr>';

            $output .= '</tbody></table>';
        }
        echo $output;
    }

    function mp_tax_report() {
        $line_item = $this->dashboard_model->get_All_booking_details();
        $output = '<table class="table table-striped table-hover table-bordered" id="sample_10">
            <thead>
              <tr> 
                <th> # </th>
				<th>Booking ID </th>
				<th> Guest Name </th>
				<th> Room No</th>
                 <th> Booking Date</th>';

        $f = 0;
        $cm = 0;
        $as = array();
        $sa = array();
        $taxType = $this->dashboard_model->get_distinct_tax_type();
        //echo "<pre>";
        //print_r($taxType);
        if (isset($taxType) && $taxType) {

            foreach ($taxType as $tax) {

                array_push($as, $tax->tax_name);

                $output .= '<th>' . $tax->tax_name . '</th>';
            }

            $output .= ' <th class="">POS Tax</th>
                <th class="">Extra Tax</th>
              	<th class="">Total Tax</th>
				 </tr></thead><tbody>';
            $sl = 0;
            $mf = 0;
            $co = 0;
            $pos_tax1 = 0;
            $pos_tax = 0;
            $pos_tot_amount1 = 0;
            $ch_tax = 0.00;
            $s_tax = 0.00;
            $charge_details = 0;
            if (isset($line_item) && $line_item) {
                //$id=$item->booking_id;

                foreach ($line_item as $item) {
                    //if(isset($item->booking_extra_charge_id) && $item->booking_extra_charge_id>0){
                    $charge_id_array = explode(",", $item->booking_extra_charge_id);
                    $ch_tax = 0.00;
                    for ($i = 1; $i < sizeof($charge_id_array); $i++) {

                        $charge_details = $this->dashboard_model->get_charge_details($charge_id_array[$i]);
                        if(isset($charge_details->crg_quantity) && $charge_details->crg_quantity){
							$ch_qty=$charge_details->crg_quantity;
						}else{
							$ch_qty=0;
						}
						//$ch_qty = $charge_details->crg_quantity;
                        if(isset($charge_details->crg_unit_price) && $charge_details->crg_unit_price){
						$ch_price = $charge_details->crg_unit_price;
						}else{
							$ch_price = 0;
						}
						if(isset($charge_details->crg_tax) && $charge_details->crg_tax){
							$crt=$charge_details->crg_tax;
						}else{
							$crt=0;
						}
                        $ch_amt = $ch_price * ($crt / 100) * $ch_qty;
                        $ch_tax = $ch_tax + $ch_amt;
                        //echo " ch tax ".$ch_tax." id ".$item->booking_id;
                    }

                    //}	
                    if ($item->booking_id_actual != '') {

                        $id = $item->booking_id;
                        $extra_ser = $this->dashboard_model->get_service_details2($id);
                        if (isset($extra_ser) && $extra_ser) {
                            foreach ($extra_ser as $service_tax) {
                                //$s_tax+=$service_tax->tax;
                                $sp = $service_tax->s_price;
                                $qty = $service_tax->qty;
                                $stax_amt = $sp * ($service_tax->tax / 100) * $qty;
                                $s_tax = $stax_amt;
                            }
                        } else {
                            $s_tax = 0;
                        }
                    } else {
                        $id = $item->booking_id;
                        $extra_ser = $this->dashboard_model->get_service_details3($id);
                        if (isset($extra_ser) && $extra_ser) {

                            foreach ($extra_ser as $service_tax) {
                                $sp = $service_tax->s_price;
                                $qty = $service_tax->qty;
                                $stax_amt = $sp * ($service_tax->tax / 100) * $qty;
                                $s_tax = $stax_amt;
                                //echo $stax_amt."rtyhtf";
                            }
                        } else {
                            $s_tax = 0;
                        }
                    }

                    $extra_tax_amt = $ch_tax + $s_tax;

                    $output .= '<td>' . ++$sl . '</td>';
                    $output .= '<td>';
                    if ($item->booking_id_actual != '') {
                        $output .= $item->booking_id_actual;
                    } else {
                        $output .= "HM0GRP600" . $item->group_id;
                    }

                    $output .= '</td>';
                    $output .= '<td>' . $item->cust_name . '</td>';
                    $output .= '<td>' . $item->room_no . '</td>';
                    $output .= '<td>' . date("dS-M-Y", strtotime($item->cust_from_date_actual)) . '<div style="color:#1F897F;">-to-</div>' . date("dS-M-Y", strtotime($item->cust_end_date_actual)) . '</td>';


                    if ($item->group_id == 0) {
                        $poses = $this->dashboard_model->all_pos_booking($item->booking_id);


                        if ($poses) {
                            foreach ($poses as $key) {
                                $pos_tax = $key->tax;
                                $pos_tot_amount1 = $key->total_amount;
                            }
                        }
                        $type1 = 'sb';
                        $bid = $item->booking_id;
                    } else {
                        $type1 = 'gb';
                        $bid = $item->group_id;
                        $poses_gb = $this->dashboard_model->all_pos_booking_group($item->group_id);
                        $pos_amount1 = 0;

                        if ($poses_gb) {
                            foreach ($poses_gb as $key1) {
                                $pos_tax1 = $key1->tax;
                                $pos_tot_amount1 = $key1->total_amount;
                            }
                        }
                    }

                    $taxData = $this->bookings_model->line_charge_item_tax($type1, $bid);

                    if (isset($taxData) && $taxData) {
                        //unset($taxData['mp_tax']['total']);
                        unset($taxData['mp_tax']['r_id']);
                        unset($taxData['mp_tax']['planName']);
                        //	unset($taxData['mp_tax']['Service Charge']);

                        unset($taxData['rr_tax']['r_id']);
                        unset($taxData['rr_tax']['planName']);
                        unset($taxData['rr_tax']['total']);
                        $taxData['booking_id'] = $bid;
                        $a1 = $taxData['rr_tax'];
                        $a2 = $taxData['mp_tax']; //print_r($taxData);
                        $sa = $taxData['mp_tax'];
                        //$sa = array_merge($taxData['rr_tax'],$taxData['mp_tax']);
                        //print_r($sa);
                        if ($taxData['mp_tax']) {
                            $f = count($taxData['mp_tax']);
                        } else {//echo ;
                        }

                        foreach ($as as $key => $val1) {

                            if (array_key_exists($val1, $sa)) {
                                $output .= '<td>' . $sa[$val1] . '</td>';
                            } else {
                                $output .= '<td>0.00</td>';
                            }
                        }
                    } else {
                        if (isset($taxType) && $taxType) {

                            foreach ($taxType as $tax) {

                                $output .= '<td>0.00</td>';
                            }
                        }
                    }

                    $output .= '<td>';
                    if ($item->group_id != '' && $item->group_id != 0) {
                        $output .= $pos_tax1;
                    } else {
                        $output .= $pos_tax;
                    }
                    $output .= '</td>';

                    $output .= '<td>';

                    $output .= number_format($extra_tax_amt, 2, '.', ',');

                    $output .= '</td>';



                    $output .= '<td>';
                    $output .= $item->mp_tax + $pos_tax + $pos_tax1 + $extra_tax_amt;

                    $output .= '</td>';

                    $output .= '</tr>';
                }
            }

            $output .= '</tr>';

            $output .= '</tbody></table>';
        }
        echo $output;
    }

    function lodging_tax_report() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Tax Reports';
            $data['description'] = 'Tax Report';
            $data['active'] = 'lodging_tax_report';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            // $data['reports'] = $this->dashboard_model->all_note_preference();
            $data['bookings'] = $this->dashboard_model->all_bookings();
            $data['page'] = 'backend/dashboard/lodging_tax_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function cancelled_reservation() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Reservation Reports';
            $data['description'] = 'Reservation reports';
            $data['active'] = 'cancelled_reservation';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/cancelled_reservation';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function get_r_report_by_date() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $start_date = date("Y-m-d");
            $end_date = date("Y-m-d");
            $data['heading'] = 'Reports';
            $data['sub_heading'] = 'Transaction Report';
            $data['description'] = 'Report List';
            $data['active'] = 'all_f_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
                $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
                $data['start_date'] = $this->input->post('t_dt_frm');
                $data['end_date'] = $this->input->post('t_dt_to');
            }

            $data['reports'] = $this->dashboard_model->all_r_by_date($start_date, $end_date);
            $data['page'] = 'backend/dashboard/reservation_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function get_r_summary_by_date() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $start_date = date("Y-m-d");
            $end_date = date("Y-m-d");
            $data['heading'] = 'Reports';
            $data['sub_heading'] = 'Transaction Report';
            $data['description'] = 'Report List';
            $data['active'] = 'all_f_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
                $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
                $data['start_date'] = $this->input->post('t_dt_frm');
                $data['end_date'] = $this->input->post('t_dt_to');
                $data['summary'] = $this->dashboard_model->all_r_by_date($start_date, $end_date);
            } else {


                $data['summary'] = $this->dashboard_model->all_r_by_date($start_date, $end_date);
            }
            $data['page'] = 'backend/dashboard/reservation_summary';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function get_f_report_by_date() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $start_date = date("Y-m-d");
            $end_date = date("Y-m-d");
            $data['start_date'] = date("m/d/Y");
            $data['end_date'] = date("m/d/Y");
            $data['heading'] = 'Reports';
            $data['sub_heading'] = 'Transaction Report';
            $data['description'] = 'Report List';
            $data['active'] = 'all_f_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) { //print_r($this->input->post());exit;
                $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
                $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
                $data['start_date'] = $this->input->post('t_dt_frm');
                $data['end_date'] = $this->input->post('t_dt_to');
            }
            $data['transactions'] = $this->dashboard_model->all_transactions_by_date($start_date, $end_date);
            //print_r($data['transactions']); exit;
            //print_r($data['start_date']); exit;
            //$data['transactions'] = $this->dashboard_model->all_f_reports();
            $data['types'] = $this->dashboard_model->all_f_types();
            $data['entity'] = $this->dashboard_model->all_f_entity();
            $data['page'] = 'backend/dashboard/all_f_reports';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function get_tax_report() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $start_date = date("Y-m-d");
            $end_date = date("Y-m-d");
            $data['heading'] = 'Reports';
            $data['sub_heading'] = 'Tax Report';
            $data['description'] = 'Report List';
            $data['active'] = 'all_tax_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
                $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
                $data['start_date'] = $this->input->post('t_dt_frm');
                $data['end_date'] = $this->input->post('t_dt_to');
            }

            $data['bookings'] = $this->dashboard_model->all_tax_report_by_date($start_date, $end_date);
            // $data['types'] = $this->dashboard_model->all_f_types();
            // $data['entity'] = $this->dashboard_model->all_f_entity();
            $data['page'] = 'backend/dashboard/tax_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function get_lodging_tax_report() {


        $found = in_array("38", $this->arraay);
        if ($found) {
            $start_date = date("Y-m-d");
            $end_date = date("Y-m-d");
            $data['heading'] = 'Reports';
            $data['sub_heading'] = 'lodging Report';
            $data['description'] = 'Report List';
            $data['active'] = 'all_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
                $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
                $data['start_date'] = $this->input->post('t_dt_frm');
                $data['end_date'] = $this->input->post('t_dt_to');
            }

            $data['bookings'] = $this->dashboard_model->all_tax_report_by_date($start_date, $end_date);
            // $data['types'] = $this->dashboard_model->all_f_types();
            // $data['entity'] = $this->dashboard_model->all_f_entity();
            $data['page'] = 'backend/dashboard/lodging_tax_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function get_booking_report_by_date() {
        $start_date = ''; // date("Y-m-d");
        $end_date = ''; //date("Y-m-d");
        $admin = "";
        $data['heading'] = 'Booking';
        $data['sub_heading'] = 'All Bookings';
        $data['description'] = 'Bookings List';
        $data['active'] = 'all_bookings';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        if ($this->input->post()) {
            if ($this->input->post('t_dt_frm') && $this->input->post('t_dt_frm')) {
                $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
                $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
                $data['start_date'] = $this->input->post('t_dt_frm');
                $data['end_date'] = $this->input->post('t_dt_to');
            }
            if ($this->input->post('admins') != null) {
                $admin = $this->input->post('admins');
            }
            //
            //print_r($this->input->post());
            //exit();
        }

        $data['bookings'] = $this->dashboard_model->all_bookings_by_date($start_date, $end_date, $admin);
        $data['transactions'] = $this->dashboard_model->all_transactions();
        $data['page'] = 'backend/dashboard/all_bookings';
        $this->load->view('backend/common/index', $data);
    }

    function get_grp_booking_report_by_date() {
        $start_date = date("Y-m-d");
        $end_date = date("Y-m-d");
        $data['heading'] = 'Booking';
        $data['sub_heading'] = 'All Bookings';
        $data['description'] = 'Bookings List';
        $data['active'] = 'all_bookings';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        if ($this->input->post()) {
            $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
        }
        $data['groups'] = $this->dashboard_model->get_grp_booking_report_by_date($start_date, $end_date);
        //print_r($data['groups']);exit;
        $data['transactions'] = $this->dashboard_model->all_transactions();
        $data['page'] = 'backend/dashboard/all_group_bookings';
        $this->load->view('backend/common/index', $data);
    }

    function cust_police_reports() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Guest Report';
            $data['description'] = 'Report List';
            $data['active'] = 'cust_police_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {

                $date_1 = date('Y-m-d', strtotime($this->input->post('t_dt_frm')));
                $date_2 = date('Y-m-d', strtotime($this->input->post('t_dt_to')));
                $data['start_date'] = $this->input->post('t_dt_frm');
                $data['end_date'] = $this->input->post('t_dt_to');
                $data['bookings'] = $this->dashboard_model->get_cust_report_by_date($date_1, $date_2);
            } else {
                $data['bookings'] = $this->dashboard_model->all_bookings();
            }


            $data['page'] = 'backend/dashboard/cust_police_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function generate_excel() {
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Reports');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'All Report');
        $this->excel->getActiveSheet()->setCellValue('A4', 'S.No.');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Booking Id');
        $this->excel->getActiveSheet()->setCellValue('C4', 'Name');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Room Number');
        $this->excel->getActiveSheet()->setCellValue('E4', 'From And Upto Date');
        $this->excel->getActiveSheet()->setCellValue('F4', 'Address');
        $this->excel->getActiveSheet()->setCellValue('G4', 'Contact Number');
        $this->excel->getActiveSheet()->setCellValue('H4', 'Total Amount');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Amount Dus');
        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:I1');
        //set aligment to center for that merged cell (A1 to C1)

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');

        for ($col = ord('A'); $col <= ord('I'); $col++) {
            //set column dimension
            $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
            //change the font size
            $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
            $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $rs = $this->dashboard_model->get_data_for_excel();

        //print_r($rs);
        //exit();

        $exceldata = "";
        foreach ($rs as $row) {
            $exceldata[] = $row;
        }

        $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A4');

        $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('H4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('I4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


        $filename = 'PHPExcelDemo.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function edit_guest() {
		 $this->load->library('upload');
         $this->load->library('image_lib');
        $found = in_array("38", $this->arraay);
        if ($found) {
            $corporate = $this->dashboard_model->fetch_corporate();
            //print_r($corporate);
            //exit();
            $data['corporate'] = $corporate;
            $data['heading'] = 'Guests';
            $data['sub_heading'] = 'Edit Guest';
            $data['description'] = 'Edit Guest Here';
            $data['active'] = 'edit_guest';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            date_default_timezone_set("Asia/Kolkata");
			 $this->load->library('upload');
			$this->load->library('image_lib');
            if ($this->input->post()) {
				
               if ($this->input->post('g_dob')) {
                        $dob = date("Y-m-d", strtotime($this->input->post('g_dob')));
						$age=date_diff(date_create($dob), date_create('today'))->y;
                    } else {
                        $dob = '';
						$age='';
                    }
					$other=$this->input->post('other_id');
				if(isset($other) && $other){
					$typ=$this->input->post('other_id');
				}else{
					$typ=$this->input->post('g_id_type');
				}
				
				if(is_array($this->input->post('discount_rule'))){
					$disc_rl_guest = implode(",", $this->input->post('discount_rule'));
				} else {
					$disc_rl_guest = NULL;
				}
				
               // $this->upload->initialize($this->set_upload_optionsGstId());
				$this->upload->initialize($this->set_upload_optionsGst());
                if (($this->upload->do_upload('image_photo')) && ($this->upload->do_upload('image_idpf')) ) {
				//	 echo "zcsdsfds"; exit;
                    if ($this->upload->do_upload('image_photo')) {
						
                        $guest_image = $this->upload->data('file_name');
                        $filename = $guest_image;
                         $source_path = './upload/guest/originals/photo/'.$filename;
						$target_path = './upload/guest/thumbnail/photo/';

                        $config_manip = array(
                            'image_library' => 'gd2',
                            'source_image' => $source_path,
                            'new_image' => $target_path,
                            'maintain_ratio' => TRUE,
                            'create_thumb' => TRUE,
                            'thumb_marker' => '_thumb',
                            'width' => 250,
                            'height' => 250
                        );

                      //  $this->load->library('image_lib', $config_manip);
						 $this->image_lib->initialize($config_manip);
                        if (!$this->image_lib->resize()) {
                            $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                            redirect('dashboard/edit_guest');
                        }

                        // clear //
                        $thumb_name = explode(".", $filename);
                        $guest_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                        $this->image_lib->clear();
                    }

                    if ($this->input->post('g_dob')) {
                        $dob = date("Y-m-d", strtotime($this->input->post('g_dob')));
						$age=date_diff(date_create($dob), date_create('today'))->y;
                    } else {
                        $dob = '';
						$age='';
                    }
					$other=$this->input->post('other_id');
				if(isset($other) && $other){
					$typ=$this->input->post('other_id');
				}else{
					$typ=$this->input->post('g_id_type');
				}

                    $g_type = $this->input->post('g_type');
                    if ($g_type !== "Military") {
                        $retired = "N/A";
                        $rank = "N/A";
                    } else {
                        $retired = $this->input->post('retired');
                        $rank = $this->input->post('rank');
                    }
					 $this->upload->initialize($this->set_upload_optionsGstId());
                    if ($this->upload->do_upload('image_idpf')) {

                        $guest_id = $this->upload->data('file_name');
                        $filename1 = $guest_id;
                          $source_path1 = './upload/guest/originals/id_proof/'.$filename1;
                    $target_path1 = './upload/guest/thumbnail/id_proof/';

                        $config_manip1 = array(
                            'image_library' => 'gd2',
                            'source_image' => $source_path1,
                            'new_image' => $target_path1,
                            'maintain_ratio' => TRUE,
                            'create_thumb' => TRUE,
                            'thumb_marker' => '_thumb',
                            'width' => 250,
                            'height' => 250
                        );

                     //   $this->load->library('image_lib', $config_manip1);
						 $this->image_lib->initialize($config_manip1);
                        if (!$this->image_lib->resize()) {
                            $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                            redirect('dashboard/edit_guest');
                        }
                        // clear //
                        $thumb_name1 = explode(".", $filename1);
                        $guest_thumb1 = $thumb_name1[0] . $config_manip1['thumb_marker'] . "." . $thumb_name1[1];
                        $this->image_lib->clear();
                    }
                    $g_type = $this->input->post('g_type');
                    if ($g_type !== "Military") {
                        $retired = "N/A";
                        $rank = "N/A";
                    } else {
                        $retired = $this->input->post('retired');
                        $rank = $this->input->post('rank');
                    }
					
				
                    $guest_edit = array(
                        'g_type' => $this->input->post('g_type'),
						'g_id_type'=>$typ,
						'g_id_number'=>$this->input->post('g_id_number'),
						'g_age'=>$age,
                        'corporate_id' => $this->input->post('c_name'),
                        'c_des' => $this->input->post('c_des'),
                        'g_name' => $this->input->post('g_name'),
                        'g_address' => $this->input->post('g_address'),
                        'g_contact_no' => $this->input->post('g_contact_no'),
                        'g_email' => $this->input->post('g_email'),
                        'g_gender' => $this->input->post('g_gender'),
                        'g_dob' => date("Y-m-d", strtotime($this->input->post('g_dob'))),
                        'g_occupation' => $this->input->post('g_occupation'),
                        'g_married' => $this->input->post('g_married'),
                        'g_pincode' => $this->input->post('g_place'),
                        'g_city' => $this->input->post('g_city'),
                        'g_state' => $this->input->post('g_state'),
                        'g_country' => $this->input->post('g_country'),
                        'g_photo' => $guest_image,
                        'g_id_proof' => $guest_id,
                        'g_photo_thumb' => $guest_thumb,
                        'g_id_proof_thumb' => $guest_thumb1,
                        'g_type' => $this->input->post('g_type'),
                        'type' => $this->input->post('type'),
                        'f_size' => $this->input->post('f_size'),
                        'class' => $this->input->post('class'),
                        'retired' => $retired,
                        'rank' => $rank,
                        'corporate_id' => $this->input->post('c_name'),
                        'dep' => $this->input->post('dep'),
                        'c_des' => $this->input->post('c_des'),
                        'g_anniv' => $this->input->post('g_anniv'),
                        //'corporate_id' => $this->input->post('corporateId'),
                        'g_highest_qual_name' => $this->input->post('qualificationName'),
                        //'g_highest_qual_level' => $this->input->post('highestQualification'),
                        'g_highest_qual_level' => $this->input->post('qualificationLevel'),
                        'cp_discount_rule_id' => $disc_rl_guest,
						'gstin' =>  $this->input->post('gstin')
                    );
					
                } 
				 
				elseif ($this->upload->do_upload('image_photo')) {
					$this->upload->initialize($this->set_upload_optionsGst());
					if($this->upload->do_upload('image_photo')){
					
                    /*  Image Upload 18.11.2015 */
                    $guest_image = $this->upload->data('file_name');
                    $filename = $guest_image;
                    $source_path = './upload/guest/originals/photo/'.$filename;
						$target_path = './upload/guest/thumbnail/photo/';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                   // $this->load->library('image_lib', $config_manip);
					 $this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/edit_guest');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $guest_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();

                    $g_type = $this->input->post('g_type');
                    if ($g_type !== "Military") {
                        $retired = "N/A";
                        $rank = "N/A";
                    } else {
                        $retired = $this->input->post('retired');
                        $rank = $this->input->post('rank');
                    }
                    $guest_edit = array(
                        'g_type' => $this->input->post('g_type'),
						'g_id_type'=>$typ,
						'g_id_number'=>$this->input->post('g_id_number'),
						'g_age'=>$age,
                        'corporate_id' => $this->input->post('c_name'),
                        'c_des' => $this->input->post('c_des'),
                        'g_name' => $this->input->post('g_name'),
                        'g_address' => $this->input->post('g_address'),
                        'g_contact_no' => $this->input->post('g_contact_no'),
                        'g_email' => $this->input->post('g_email'),
                        'g_gender' => $this->input->post('g_gender'),
                        'g_dob' => date("Y-m-d", strtotime($this->input->post('g_dob'))),
                        'g_occupation' => $this->input->post('g_occupation'),
                        'g_married' => $this->input->post('g_married'),
                        'g_pincode' => $this->input->post('g_place'),
                        'g_city' => $this->input->post('g_city'),
                        'g_state' => $this->input->post('g_state'),
                        'g_country' => $this->input->post('g_country'),
                        'g_photo' => $guest_image,
                        'g_photo_thumb' => $guest_thumb,
                        'g_type' => $this->input->post('g_type'),
                        'type' => $this->input->post('type'),
                        'f_size' => $this->input->post('f_size'),
                        'class' => $this->input->post('class'),
                        'retired' => $retired,
                        'rank' => $rank,
                        'corporate_id' => $this->input->post('c_name'),
                        'dep' => $this->input->post('dep'),
                        'c_des' => $this->input->post('c_des'),
                        'g_anniv' => $this->input->post('g_anniv'),
                       // 'corporate_id' => $this->input->post('corporateId'),
                        'g_highest_qual_name' => $this->input->post('qualificationName'),
                        //'g_highest_qual_level' => $this->input->post('highestQualification'),
                        'g_highest_qual_level' => $this->input->post('qualificationLevel'),
                        'cp_discount_rule_id' => $disc_rl_guest,
						'gstin' =>  $this->input->post('gstin')
                    );
                }
				}
				
				elseif ($this->upload->do_upload('image_idpf')) {
					
					$this->upload->initialize($this->set_upload_optionsGstId());
					if($this->upload->do_upload('image_idpf')){
                    $guest_id = $this->upload->data('file_name');
                    $filename1 = $guest_id;
                    $source_path1 = './upload/guest/originals/id_proof/'.$filename1;
                    $target_path1 = './upload/guest/thumbnail/id_proof/';

                    $config_manip1 = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path1,
                        'new_image' => $target_path1,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );

                  //  $this->load->library('image_lib', $config_manip1);
					 $this->image_lib->initialize($config_manip1);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/edit_guest');
                    }
                    // clear //
                    $thumb_name1 = explode(".", $filename1);
                    $guest_thumb1 = $thumb_name1[0] . $config_manip1['thumb_marker'] . "." . $thumb_name1[1];
                    $this->image_lib->clear();
                    $g_type = $this->input->post('g_type');
                    if ($g_type !== "Military") {
                        $retired = "N/A";
                        $rank = "N/A";
                    } else {
                        $retired = $this->input->post('retired');
                        $rank = $this->input->post('rank');
                    }
                    $guest_edit = array(
                        'g_type' => $this->input->post('g_type'),
						'g_id_type'=>$typ,
						'g_id_number'=>$this->input->post('g_id_number'),
						'g_age'=>$age,
                        'corporate_id' => $this->input->post('c_name'),
                        'c_des' => $this->input->post('c_des'),
                        'g_name' => $this->input->post('g_name'),
                        'g_address' => $this->input->post('g_address'),
                        'g_contact_no' => $this->input->post('g_contact_no'),
                        'g_email' => $this->input->post('g_email'),
                        'g_gender' => $this->input->post('g_gender'),
                        'g_dob' => date("Y-m-d", strtotime($this->input->post('g_dob'))),
                        'g_occupation' => $this->input->post('g_occupation'),
                        'g_married' => $this->input->post('g_married'),
                        'g_pincode' => $this->input->post('g_place'),
                        'g_city' => $this->input->post('g_city'),
                        'g_state' => $this->input->post('g_state'),
                        'g_country' => $this->input->post('g_country'),
                        'g_id_proof' => $guest_id,
                        'g_id_proof_thumb' => $guest_thumb1,
                       // 'g_type' => $this->input->post('g_type'),
                        'type' => $this->input->post('type'),
                        'f_size' => $this->input->post('f_size'),
                        'class' => $this->input->post('class'),
                        'retired' => $retired,
                        'rank' => $rank,
                        'corporate_id' => $this->input->post('c_name'),
                        'dep' => $this->input->post('dep'),
                        'c_des' => $this->input->post('c_des'),
                        'g_anniv' => $this->input->post('g_anniv'),
                        //'corporate_id' => $this->input->post('corporateId'),
                        'g_highest_qual_name' => $this->input->post('qualificationName'),
                        //'g_highest_qual_level' => $this->input->post('highestQualification'),
                        'g_highest_qual_level' => $this->input->post('qualificationLevel'),
                        'cp_discount_rule_id' => $disc_rl_guest,
						'gstin' =>  $this->input->post('gstin')
                    );
					}
                } else {
                    $g_type = $this->input->post('g_type');
                    if ($g_type !== "Military") {
                        $retired = "N/A";
                        $rank = "N/A";
                    } else {
                        $retired = $this->input->post('retired');
                        $rank = $this->input->post('rank');
                    }
                    $guest_edit = array(
                        'g_type' => $this->input->post('g_type'),
						'g_id_type'=>$typ,
						'g_id_number'=>$this->input->post('g_id_number'),
						'g_age'=>$age,
                        //'corporate_id' => $this->input->post('c_name'),
                        'c_des' => $this->input->post('c_des'),
                        'g_name' => $this->input->post('g_name'),
                        'g_address' => $this->input->post('g_address'),
                        'g_contact_no' => $this->input->post('g_contact_no'),
                        'g_email' => $this->input->post('g_email'),
                        'g_gender' => $this->input->post('g_gender'),
                        'g_dob' => date("Y-m-d", strtotime($this->input->post('g_dob'))),
                        'g_occupation' => $this->input->post('g_occupation'),
                        'g_married' => $this->input->post('g_married'),
                        'g_pincode' => $this->input->post('g_place'),
                        'g_city' => $this->input->post('g_city'),
                        'g_state' => $this->input->post('g_state'),
                        'g_country' => $this->input->post('g_country'),
                        'g_type' => $this->input->post('g_type'),
                        'type' => $this->input->post('type'),
                        'f_size' => $this->input->post('f_size'),
                        'class' => $this->input->post('class'),
                        'retired' => $retired,
                        'rank' => $rank,
                        'corporate_id' => $this->input->post('c_name'),
                        'dep' => $this->input->post('dep'),
                        'c_des' => $this->input->post('c_des'),
                        'g_anniv' => $this->input->post('g_anniv'),
                        //'corporate_id' => $this->input->post('corporateId'),
                        'g_highest_qual_name' => $this->input->post('qualificationName'),
                        //'g_highest_qual_level' => $this->input->post('highestQualification'),
                        'g_highest_qual_level' => $this->input->post('qualificationLevel'),
                        'cp_discount_rule_id' => $disc_rl_guest,
						'gstin' =>  $this->input->post('gstin')
                    );
                }

				/*
				echo "<pre>";
				echo $this->input->post('guest_id');
				print_r($guest_edit);exit;*/
				//echo "<pre>";
				//print_r($guest_edit);exit;
                $query = $this->dashboard_model->edit_guest($guest_edit, $this->input->post('guest_id'));
                $g_id = $this->input->post('guest_id');
                if (isset($query) && $query) {
					$this->session->set_flashdata('succ_msg', "Guest Updated Successfully!");
                    redirect('dashboard/all_guest');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    $data['value'] = $this->dashboard_model->get_guest_details($g_id);
                    $data['page'] = 'backend/dashboard/edit_guest';
                    $this->load->view('backend/common/index', $data);
                }
				
            } else {
                date_default_timezone_set("Asia/Kolkata");
                $g_id = $_GET['g_id'];
                $data['value'] = $this->dashboard_model->get_guest_details($g_id);
                $data['page'] = 'backend/dashboard/edit_guest';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function edit_broker() {
		$this->load->library('upload');
		$this->load->library('image_lib');

        $found = in_array("20", $this->arraay);
        if ($found) {
            $data['heading'] = 'Broker & Channel';
            $data['sub_heading'] = 'Broker';
            $data['description'] = 'Edit Broker Here';
            $data['active'] = 'all_broker';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                date_default_timezone_set("Asia/Kolkata");
				$broker_id=$this->input->post('broker_id');
				$broker_image="";
				$broker_thumb="";
				$broker_doc="";
				$broker_data=$this->dashboard_model->get_broker_details($broker_id);
				foreach($broker_data as $b_data){
					$broker_image=$b_data->b_photo;
					$broker_thumb=$b_data->b_photo_thumb;
					$broker_doc=$b_data->b_doc;
				}
				
                $this->upload->initialize($this->set_upload_broker());
                if ($this->upload->do_upload('image_photo')) {
                    /*  Image Upload 18.11.2015 */
                    $broker_image = $this->upload->data('file_name');
                    $filename = $broker_image;
                    $source_path = './upload/broker/image/' . $filename;
                    $target_path = './upload/broker/image/';

                    $config_manip = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                   // $this->load->library('image_lib', $config_manip);
						$this->image_lib->initialize($config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/all_broker');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $broker_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                    /* Images Upload  18.11.2015 */
				}
				$this->upload->initialize($this->set_upload_broker_doc());
				if ($this->upload->do_upload('doc_photo')){
					$broker_doc=$this->upload->data('file_name');
					$broker_ext=explode(".", $broker_doc);
					if($broker_ext[1]=="jpg" || $broker_ext[1]=="png" || $broker_ext[1]=="gif" || $broker_ext[1]=="jpeg" ){
						$filename = $broker_doc;
						$source_path = './upload/broker/document/' . $filename;
						$target_path = './upload/broker/image/';

						$config_manip1 = array(
							//'file_name' => 'myfile',
							'image_library' => 'gd2',
							'source_image' => $source_path,
							'new_image' => $target_path,
							
						);
					   // $this->load->library('image_lib', $config_manip);
					  $this->image_lib->initialize($config_manip1);
					  if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_broker');
						
                    }
					$delete_doc= './upload/broker/document/' . $broker_doc;
						
						unlink($delete_doc);
					}
				}

                    $broker_edit = array(
                        'b_agency' => $this->input->post('b_agency'),
                        'b_agency_name' => $this->input->post('b_agency_name'),
                        'b_name' => $this->input->post('b_name'),
                        'b_address' => $this->input->post('b_address'),
                        'b_contact' => $this->input->post('b_contact'),
                        'b_email' => $this->input->post('b_email'),
                        'b_website' => $this->input->post('b_website'),
                        'b_pan' => $this->input->post('b_pan'),
                        'b_bank_acc_no' => $this->input->post('b_bank_acc_no'),
                        'b_bank_ifsc' => $this->input->post('b_bank_ifsc'),
                        'b_photo' => $broker_image,
                        'b_photo_thumb' => $broker_thumb,
						'b_doc' => $broker_doc ,
                        'broker_commission' => $this->input->post('b_commission')
                    );
                
					/*echo "<pre>";
					print_r($broker_edit);
					exit();*/

                $query = $this->dashboard_model->edit_broker($broker_edit, $this->input->post('broker_id'));

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Broker  Update Successfully!");
                    //$data['value'] = $this->dashboard_model->get_broker_details($this->input->post('broker_id'));
                    //$data['page'] = 'backend/dashboard/all_broker';
                    //$this->load->view('backend/common/index', $data);
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    //$data['value'] = $this->dashboard_model->get_broker_details($b_id);
                    //$data['page'] = 'backend/dashboard/all_broker';
                    //$this->load->view('backend/common/index', $data);
                }

                redirect(base_url() . 'dashboard/all_broker');
            } else {
                $b_id = $_GET['b_id'];
                $data['value'] = $this->dashboard_model->get_broker_details($b_id);
                $data['page'] = 'backend/dashboard/edit_broker';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function add_hotel_submit() {
        $booking_id = $this->input->post('booking_id');
        $add_amount = $this->input->post('add_amount');
        $pay_mode = $this->input->post('pay_mode');
        $bankname = $this->input->post('bankname');
        $payment_status = $this->input->post('bankname');

        echo $booking_id, $add_amount, $pay_mode, $bankname, $payment_status;
    }

    function pdf_reports() {

        $found = in_array("38", $this->arraay);
        if ($found) {

            $data['bookings'] = $this->dashboard_model->all_bookings();
            $data['transactions'] = $this->dashboard_model->all_transactions();
            //print_r($data);
            //exit();
            $this->load->view("backend/dashboard/all_reports_pdf", $data);
            $html = $this->output->get_output();
            $this->load->library('dompdf_gen');
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("all_report.pdf");
        } {
            redirect('dashboard');
        }
    }

    function pdf_reports_police() {


        $found = in_array("38", $this->arraay);
        if ($found) {
            if (($this->input->get('s_d') == "") && ($this->input->get('e_d') == "")) {
                $data['bookings'] = $this->dashboard_model->all_bookings();
                $data['hotel_contact'] = $this->bookings_model->get_hotel_contact_details($data['bookings'][0]->hotel_id);
                $this->load->view("backend/dashboard/all_reports_pdf_police", $data);
            } else {
                $start_date = date("Y-m-d", strtotime($this->input->get('s_d')));
                $end_date = date("Y-m-d", strtotime($this->input->get('e_d')));
                $data['start_date'] = $start_date;
                $data['end_date'] = $end_date;
                $data['bookings'] = $this->dashboard_model->get_cust_report_by_date($start_date, $end_date);
                //echo "<pre>";
                //print_r($data['bookings']);
                //exit;
                $data['hotel_contact'] = $this->bookings_model->get_hotel_contact_details($data['bookings'][0]->hotel_id);
                $this->load->view("backend/dashboard/all_reports_pdf_police", $data);
            }

            $html = $this->output->get_output();
            $this->load->library('dompdf_gen');
            $this->dompdf->set_paper('A3', 'landscape');
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("all_report.pdf");
        } else {
            redirect('dashboard');
        }
    }

    function pdf_reports_financial() {


        $found = in_array("38", $this->arraay);
        if ($found) {
            if (($this->input->get('s_d') == "") && ($this->input->get('e_d') == "")) {
                //$data['bookings'] = $this->dashboard_model->all_bookings();
                $data['transactions'] = $this->dashboard_model->all_f_reports();
                $data['types'] = $this->dashboard_model->all_f_types();
                $data['entity'] = $this->dashboard_model->all_f_entity();
            } else {
                $start_date = date("Y-m-d", strtotime($this->input->get('s_d')));
                $end_date = date("Y-m-d", strtotime($this->input->get('e_d')));
                $data['start_date'] = $start_date;
                $data['end_date'] = $end_date;
                //$data['bookings'] = $this->dashboard_model->all_bookings();
                $data['transactions'] = $this->dashboard_model->all_transactions_by_date($start_date, $end_date);
                $data['types'] = $this->dashboard_model->all_f_types();
                $data['entity'] = $this->dashboard_model->all_f_entity();
            }



            $this->load->view("backend/dashboard/pdf_reports_financial", $data);
            $html = $this->output->get_output();
            $this->load->library('dompdf_gen');
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("all_report.pdf");
        } else {
            redirect('dashboard');
        }
    }

    function checkout_submission() {
        //echo "hello";
        //$a = $_POST['booking_id'];
        $booking_id = $this->input->post('booking_id');
        $date_checkout = $this->input->post('date_checkout');
        $dateCheckout = $this->input->post('dateCheckout');
		$dateCheckin = $this->input->post('dateCheckin');
        $cr_fl = $this->input->post('cr_fl');
        //$dateCheckout  = '2016-06-14';
        $booking_details = $this->dashboard_model->get_booking_details($booking_id);
        //group booking checkin
        $group_id = $booking_details->group_id;
        //$group_id= '26';
        if ($group_id == 0) {
            $this->dashboard_model->update_checkout_status($booking_id, $date_checkout, $dateCheckout,$dateCheckin);
			$this->dashboard_model->update_checkout_status_stay_guest($booking_id,'checkout');
        }
        if ($group_id != 0) {
            $details2 = $this->dashboard_model->get_group($group_id);
		//	print_r($details2);exit;
            foreach ($details2 as $key) {
                # code...
                $indi_due = $key->indi_due;
            }
            //echo $indi_due;
            //exit;
            if ($cr_fl != '1') {
                if ($indi_due != "0") {
                    exit();
                }
            }
            $this->dashboard_model->update_checkout_status_grp($group_id, $date_checkout, $dateCheckout);
			
        }
        //housekeeping part
        $room_id = $booking_details->room_id;
        $this->dashboard_model->make_room_dirty($room_id);

        //sms part
        $guest_id = $booking_details->guest_id;
        $guest_details = $this->dashboard_model->get_guest_details($guest_id);
        foreach ($guest_details as $guest) {
            $g_name = $guest->g_name;
            $g_number = $guest->g_contact_no;
            $g_email = $guest->g_email;
        }
        $hotel_name = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $hot_name = $hotel_name->hotel_name;
        $text = $g_name . " has checked out from " . $hot_name;
        $text_guest = "You have checked out from " . $hot_name.', do visit us again in the future';
        $this->load->model('bookings_model');
        $this->bookings_model->send_sms($text);
        $this->bookings_model->send_sms_guest($g_number, $text_guest);
        if (isset($g_email) && $g_email != "") {
            $this->bookings_model->send_mail($g_email, $text_guest, $hot_name);
        }
    }
	
	
	function checkout_submission_single_under_grp() {
        //echo "hello bunty";
        //$a = $_POST['booking_id'];
        $booking_id = $this->input->post('booking_id');
        $date_checkout = $this->input->post('date_checkout');
        $dateCheckout = $this->input->post('dateCheckout');
        $cr_fl = $this->input->post('cr_fl');
        //$dateCheckout  = '2016-06-14';
        $booking_details = $this->dashboard_model->get_booking_details($booking_id);
        //group booking checkin
      //  $group_id = $booking_details->group_id;
        //$group_id= '26';
       
            $this->dashboard_model->update_checkout_status($booking_id, $date_checkout, $dateCheckout);
			$this->dashboard_model->update_checkout_status_stay_guest($booking_id,'checkout');
      
      
        //housekeeping part
        $room_id = $booking_details->room_id;
        $this->dashboard_model->make_room_dirty($room_id);

        //sms part
        $guest_id = $booking_details->guest_id;
        $guest_details = $this->dashboard_model->get_guest_details($guest_id);
        foreach ($guest_details as $guest) {
            $g_name = $guest->g_name;
            $g_number = $guest->g_contact_no;
            $g_email = $guest->g_email;
        }
        $hotel_name = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $hot_name = $hotel_name->hotel_name;
        $text = $g_name . " has checked out from " . $hot_name;
        $text_guest = "You have checked out from " . $hot_name.', do visit us again in the future';
        $this->load->model('bookings_model');
        $this->bookings_model->send_sms($text);
        $this->bookings_model->send_sms_guest($g_number, $text_guest);
        if (isset($g_email) && $g_email != "") {
            $this->bookings_model->send_mail($g_email, $text_guest, $hot_name);
        }
    }
	
	
	
	
	function checkout_single_under_grp(){
		
		
		$booking_id = $this->input->post('booking_id');
        $date_checkout = $this->input->post('date_checkout');
        $dateCheckout = $this->input->post('dateCheckout');
        $cr_fl = $this->input->post('cr_fl');
        //$dateCheckout  = '2016-06-14';
        $booking_details = $this->dashboard_model->get_booking_details($booking_id);
       
		$this->dashboard_model->update_checkout_status($booking_id, $date_checkout, $dateCheckout);
		$this->dashboard_model->update_checkout_status_stay_guest($booking_id,'checkout');
        
     
        //housekeeping part
        $room_id = $booking_details->room_id;
        $this->dashboard_model->make_room_dirty($room_id);

        //sms part
        $guest_id = $booking_details->guest_id;
        $guest_details = $this->dashboard_model->get_guest_details($guest_id);
        foreach ($guest_details as $guest) {
            $g_name = $guest->g_name;
            $g_number = $guest->g_contact_no;
            $g_email = $guest->g_email;
        }
        $hotel_name = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $hot_name = $hotel_name->hotel_name;
        $text = $g_name . " has checked out from " . $hot_name;
        $text_guest = "You have checked out from " . $hot_name.', do visit us again in the future';
        $this->load->model('bookings_model');
        $this->bookings_model->send_sms($text);
        $this->bookings_model->send_sms_guest($g_number, $text_guest);
        if (isset($g_email) && $g_email != "") {
            $this->bookings_model->send_mail($g_email, $text_guest, $hot_name);
        }
		
		
		
		
		
		
		
		
	}
	

    function cancel_submission() {
        //echo "hello bunty";
        $reason = $_POST['reason'];
        $type = $_POST['note'];
        $booking_id = $this->input->post('booking_id');
        $cancel_date = $this->input->post('cancel_date');
        //	$this->dashboard_model->update_cancel_status($booking_id,$reason,$type);
        $chk_gb = $this->dashboard_model->chk_gb($booking_id);
        //print_r($chk_gb);
        //print_r(); //exit;
        if (isset($chk_gb) && $chk_gb->group_id != 0 && $chk_gb->group_id != '') {
            $group_id = $chk_gb->group_id;
            $allBk = $this->dashboard_model->get_all_sb_by_group_id($group_id);

            foreach ($allBk as $bk) {
                $bk_id = $bk->booking_id;
                $getstatus = $this->dashboard_model->update_cancel_status($bk_id, $reason, $type, $cancel_date);
            }


            $this->dashboard_model->GBupdate_cancel_status($group_id, $reason, $type, $cancel_date);
        } else {
            $getstatus = $this->dashboard_model->update_cancel_status($booking_id, $reason, $type, $cancel_date);
        }
		$this->dashboard_model->update_cancel_status_stay_guest($booking_id,'cancelled');
    }

    /* function cancel_submission_retention()
      {
      //echo "hello bunty";
      //$a = $_POST['booking_id'];
      //date_default_timezone_set('America/Los_Angeles');
      //	$hotel_id=$this->session->userdata('user_hotel');
      //	$user_id=$this->session->userdata('user_id');
      $booking_id  = $this->input->post('booking_id');
      $amount=$this->input->post('amount');
      $mode=$this->input->post('mode');
      $bank=$this->input->post('bank');
      $cancel_date = $this->input->post('cancel_date');

      $cancel_amount=$this->input->post('cancel_amount');
      $reason_type = $this->input->post('reason_type');
      $reason_note = $this->input->post('reason_note');

      //$chk_lasltransaction=$this->dashboard_model->chk_lasltransaction();
      //$chk_lasltransaction=$chk_lasltransaction+1;
      //$transaction_id='TA0'.$hotel_id."-".$user_id."/".date("d").date("m").date("y")."/".$chk_lasltransaction;
      //echo $booking_id."a".$amount."md".$mode."bnk".$bank."cn".$cancel_date."ca".$cancel_amount."rt".$reason_type."rn".$reason_note."tt".$transaction_id;


      $data=array(

      't_booking_id'=>$booking_id,
      //	'actual_id' => $transaction_id,
      't_amount' => $amount,
      // 't_date' =>  date("Y-m-d H:i:s"),
      't_payment_mode' => $mode,
      't_bank_name' => $bank,
      't_status' => 'Done',
      'transaction_type' => 'Guest refund',
      'details_id' =>  $booking_id,
      'details_type' => 'single booking',
      'transaction_type_id' => 2,
      'transaction_from_id' => 4,
      'transaction_to_id' => 4,
      'transaction_releted_t_id' => 2,
      //	'user_id' => $user_id,
      //	'hotel_id' => $hotel_id

      );


      $data_cancel_transcation=array(


      't_booking_id'=>$booking_id,
      //'actual_id' => $transaction_id,
      't_amount' => $cancel_amount,
      // 't_date' =>  date("Y-m-d H:i:s"),
      't_payment_mode' => $mode,
      't_bank_name' => $bank,
      't_status' => 'Done',
      'transaction_type' => 'Cancellation Fees',
      'details_id' =>  $booking_id,
      'details_type' => 'single booking',
      'transaction_type_id' => 6,
      'transaction_from_id' => 2,
      'transaction_to_id' => 4,
      'transaction_releted_t_id' => 6,
      //	'user_id' => $user_id,
      //	'hotel_id' => $hotel_id

      );
      //	print_r($data_cancel_transcation) ;

      //	 $query3= $this->dashboard_model->remove_transaction($data['t_booking_id']);
      $query= $this->dashboard_model->add_transaction($data,$data_cancel_transcation);

      /*echo "<pre>";
      print_r($query);
      echo $query;
      echo "</pre>";
      exit; */

    /*      $query2= $this->dashboard_model->update_cancel_status($booking_id,$reason_type,$reason_note,$cancel_date);
      if($query){

      echo 'Retention Money Taken';
      }
      else{
      echo 'Database Error';
      }
      //echo $booking_id;
      }
     */

    function cancel_submission_retention() {
        //echo "hello bunty";
        //$a = $_POST['booking_id'];
        //   date_default_timezone_set('America/Los_Angeles');


        date_default_timezone_set("Asia/Kolkata");
        $hotel_id = $this->session->userdata('user_hotel');
        $user_id = $this->session->userdata('user_id');
        $booking_id = $this->input->post('booking_id');
        $amount = $this->input->post('amount');
        $mode = $this->input->post('mode');
        $bank = $this->input->post('bank');
        $cancel_date = $this->input->post('cancel_date');
        $cancel_amount = $this->input->post('cancel_amount');
        $reason_type = $this->input->post('reason_type');
        $reason_note = $this->input->post('reason_note');

        $data = array(
            't_booking_id' => $booking_id,
            't_amount' => $amount,
            't_date' => date("Y-m-d H:i:s"),
            't_payment_mode' => $mode,
            't_bank_name' => $bank,
            't_status' => 'Done',
            'transaction_type' => 'Guest refund',
            'details_id' => $booking_id,
            'details_type' => 'single booking',
            'transaction_type_id' => 2,
            'transaction_from_id' => 4,
            'transaction_to_id' => 4,
            'transaction_releted_t_id' => 2,
            'user_id' => $user_id,
            'hotel_id' => $hotel_id
        );

        $data_cancel_transcation = array(
            't_booking_id' => $booking_id,
            't_amount' => $cancel_amount,
            't_date' => date("Y-m-d H:i:s"),
            't_payment_mode' => $mode,
            't_bank_name' => $bank,
            't_status' => 'Done',
            'transaction_type' => 'Cancellation Fees',
            'details_id' => $booking_id,
            'details_type' => 'single booking',
            'transaction_type_id' => 6,
            'transaction_from_id' => 2,
            'transaction_to_id' => 4,
            'transaction_releted_t_id' => 6,
            'user_id' => $user_id,
            'hotel_id' => $hotel_id
        );


        //	 $query3= $this->dashboard_model->remove_transaction($data['t_booking_id']);
        $query = $this->dashboard_model->add_transaction($data, $data_cancel_transcation);

        $query2 = $this->dashboard_model->update_cancel_status($booking_id, $reason_type, $reason_note, $cancel_date);
        if ($query && isset($query)) {

            echo 'Retention Money Taken';
        } else {
            echo 'Database Error';
        }
    }

    function cancel_grp_submission_retention() {
        //echo "hello bunty";
        //$a = $_POST['booking_id'];
        //  date_default_timezone_set('America/Los_Angeles');

        date_default_timezone_set("Asia/Kolkata");
        $hotel_id = $this->session->userdata('user_hotel');
        $user_id = $this->session->userdata('user_id');
        $booking_id = $this->input->post('booking_id');
        $amount = $this->input->post('amount');
        $mode = $this->input->post('mode');
        $bank = $this->input->post('bank');
        $cancel_date = $this->input->post('cancel_date');
        $group_id = $this->input->post('group_id');
        $cancel_amount = $this->input->post('cancel_amount');
        $reason_type = $this->input->post('reason_type');
        $reason_note = $this->input->post('reason_note');

        $chk_gb = $this->dashboard_model->chk_gb($booking_id);

        //print_r($chk_gb);

        if (isset($chk_gb) && $chk_gb->group_id != 0 && $chk_gb->group_id != '') {
            $group_id = $chk_gb->group_id;
            $allBk = $this->dashboard_model->get_all_sb_by_group_id($group_id);
            //  print_r($allBk);
            //  exit;
            foreach ($allBk as $bk) {
                $bk_id = $bk->booking_id;
                $getstatus = $this->dashboard_model->update_cancel_status($bk_id, $reason_type, $reason_note, $cancel_date);
            }


            $GBupdatestatus = $this->dashboard_model->GBupdate_cancel_status($group_id, $reason_type, $reason_note, $cancel_date);
        }


        /* $chk_lasltransaction=$this->dashboard_model->chk_lasltransaction();
          $chk_lasltransaction=$chk_lasltransaction+1;
          $transaction_id='TA0'.$this->session->userdata('user_hotel')."-".$this->session->userdata('user_id')."/".date("d").date("m").date("*y")."/".$chk_lasltransaction; */

        $data = array(
            't_group_id' => $group_id,
            't_amount' => $amount,
            //   't_date' =>  date("Y-m-d H:i:s"),
            't_payment_mode' => $mode,
            't_bank_name' => $bank,
            't_status' => 'Done',
            'transaction_type' => 'Guest refund',
            'details_id' => $group_id,
            'details_type' => 'grp booking',
            'transaction_type_id' => 2,
            'transaction_from_id' => 4,
            'transaction_to_id' => 4,
            'transaction_releted_t_id' => 2,
            'user_id' => $user_id,
            'hotel_id' => $hotel_id
        );


        $data_cancel_transcation = array(
            't_group_id' => $group_id,
            't_amount' => $cancel_amount,
            //   't_date' =>  date("Y-m-d H:i:s"),
            't_payment_mode' => $mode,
            't_bank_name' => $bank,
            't_status' => 'Done',
            'transaction_type' => 'Cancellation Fees',
            'details_id' => $group_id,
            'details_type' => 'grp booking',
            'transaction_type_id' => 6,
            'transaction_from_id' => 2,
            'transaction_to_id' => 4,
            'transaction_releted_t_id' => 6,
            'user_id' => $user_id,
            'hotel_id' => $hotel_id
        );
        /* echo "<pre>";
          print_r($data);
          echo "</pre>";
          exit;
         */
        //$query3= $this->dashboard_model->remove_transaction_grp($data['t_booking_id']);
        $query = $this->dashboard_model->add_transaction_grp($data, $data_cancel_transcation);

        // $query2= $this->dashboard_model->update_cancel_status($booking_id,'cancel retention','cancel retention',$cancel_date);
        if ($query) {

            echo 'Retention Money Taken';
        } else {
            echo 'Database Error';
        }
    }

    function checkin_submission() {
        $booking_id = $this->input->post('booking_id');
		$group_id = $this->input->post('group_id');
		$checkin_time = $this->input->post('chk_tym');
		$custom_chk_tym = $this->input->post('custom_chk_tym');
		$cnfrm_chk_date = $this->input->post('cnfrm_chk_date');
        $booking_details = $this->dashboard_model->get_booking_details($booking_id);
		$g_name=$booking_details->g_name;
		$g_number=$booking_details->g_contact_no;
		
        if ($group_id != 0) {
			
			//group booking checkin
			
			// _sb
			
			// _sb
            
			if($checkin_time){
			
				$this->dashboard_model->update_confirmcheckin_status($booking_id,$checkin_time,$group_id);
			}
			else if($custom_chk_tym){
			
				$this->dashboard_model->update_customcheckin_status($booking_id,$custom_chk_tym,$cnfrm_chk_date,$group_id);
			}
			else{

				$this->dashboard_model->update_checkin_status_grp_new($group_id,$booking_id);
			}
			
        } else { 

		// Single Booking Check in
		
		if($checkin_time){
			
			$this->dashboard_model->update_confirmcheckin_status($booking_id,$checkin_time);
		}
		else if($custom_chk_tym){
			
			$this->dashboard_model->update_customcheckin_status($booking_id,$custom_chk_tym,$cnfrm_chk_date);
		}
			else{
				
				$this->dashboard_model->update_checkin_status($booking_id);
			}
		
            // kit allotment in single booking...
            $ho = $this->session->userdata('user_hotel');
            $this->dashboard_model->allot_kit_booking($booking_id, $ho, "sb");
            //exit;
            //...end	cust_from_date            
			
            $this->dashboard_model->update_checkin_status_stay_guest($id,'checkin');
			
			
        }
		    $hotel_name = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$hot_name=$hotel_name->hotel_name;
			$text = $g_name . " has just checked-in at " . $hot_name;
			$text_guest = "You have checked in at " . $hot_name.', Welcome & Enjoy your stay!';
			$this->bookings_model->send_sms($text);
		
		if(isset($g_number) && $g_number){
			$this->bookings_model->send_sms_guest($g_number, $text_guest);
		}
    } // End function checkin_submission

    function send_sms_guest() {

        $g_number = '8981156030';
        $text_guest = 'Color Palettes Color Schemes';

        $this->bookings_model->send_sms_guest('8981156030', $text_guest);
    }

    function myprofile() {

        $data['heading'] = 'My Profile';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'myprofile';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));




        $data['page'] = 'backend/dashboard/profile';
        $this->load->view('backend/common/index', $data);
    }

    function lockscreen() {


        $data['heading'] = 'Lockscreen';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'myprofile';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));


        $data['page'] = 'backend/dashboard/lockscreen';
        $this->load->view('backend/common/lockscreen', $data);
    }

    function profile() {


        $data['heading'] = 'Profile';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'myprofile';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['transactions'] = $this->dashboard_model->all_transactions_limit_10();
        $data['transactions_date'] = $this->dashboard_model->all_transactions_limit_10_group_by_date();
        $data['booking'] = $this->dashboard_model->get_last_10_booking();
        $data['booking1'] = $this->dashboard_model->get_last_10_booking();

        $data['page'] = 'backend/dashboard/profile';
        $this->load->view('backend/common/index', $data);
    }

    function unlock() {

        if ($this->dashboard_model->check_unlock($this->input->post('p_lock'))) {

            header('Location:  ' . base_url() . 'dashboard');
        } else {
            header('Location:  ' . base_url() . 'dashboard/lockscreen');
        }
    }

    function delete_room() {
        $found = in_array("12", $this->arraay);
        if ($found) {
            $data['heading'] = '';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $room_id = $_GET['room_id'];
            $query = $this->dashboard_model->delete_room($room_id);
            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function delete_charge() {
        $found = in_array("7", $this->arraay);
        if ($found) {

            $s_id = $_GET['c_id'];
            $booking_id = $_GET['booking_id'];

            $price_data = $this->dashboard_model->get_charge_details($s_id);
            $price = $price_data->crg_total;

            $service_array = $this->dashboard_model->get_booking_details2($booking_id);

            foreach ($service_array as $key) {
                # code...

                $service_string = $key->booking_extra_charge_id;
                $service_price_total = $key->booking_extra_charge_amount;
            }

            $new_price = $service_price_total - $price;

            $array = explode(",", $service_string);

            //  print_r($array);
            //exit();
            $new_service_array = array();

            for ($i = 1; $i < sizeof($array); $i++) {
                # code...

                if ($array[$i] == $s_id) {

                    // echo "yes";
                    unset($array[$i]);
                    //var_dump($array);
                    break;
                }
            }

            $service_new_string = implode(",", $array);

            //echo $service_new_string;
            //exit();

            $new_service_array = array('booking_extra_charge_id' => $service_new_string, 'booking_extra_charge_amount' => $new_price);


            $query = $this->dashboard_model->delete_charge($booking_id, $new_service_array);


            redirect("dashboard/booking_edit?b_id=" . $booking_id);

            exit();

            if (isset($query) && $query) {
                $this->session->set_flashdata('succ_msg', "Service Deleted Successfully!");
                $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

                //$data['bookings'] = $this->dashboard_model->all_bookings($config['per_page'], $segment);
                $data['bookings'] = $this->dashboard_model->all_bookings();
                $data['transactions'] = $this->dashboard_model->all_transactions();
                $data['page'] = 'backend/dashboard/all_bookings';
                $this->load->view('backend/common/index', $data);
            } else {
                $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

                //$data['bookings'] = $this->dashboard_model->all_bookings($config['per_page'], $segment);
                $data['bookings'] = $this->dashboard_model->all_bookings();
                $data['transactions'] = $this->dashboard_model->all_transactions();
                $data['page'] = 'backend/dashboard/all_bookings';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function delete_service() {
        $s_id = $_GET['s_id'];
        $booking_id = $_GET['booking_id'];

        $price_data = $this->dashboard_model->get_service_details($s_id, $booking_id);
        $price1 = $price_data->s_price;
        $qty = $price_data->qty;
        $price = $price1 * $qty;
        $service_array = $this->dashboard_model->get_booking_details2($booking_id);

        foreach ($service_array as $key) {
            # code...

            $service_string = $key->service_id;
            $service_price_total = $key->service_price;
        }

        $new_price = $service_price_total - $price;

        $array = explode(",", $service_string);

        //  print_r($array);
        //exit();
        $new_service_array = array();

        for ($i = 1; $i < sizeof($array); $i++) {
            # code...

            if ($array[$i] == $s_id) {

                // echo "yes";
                unset($array[$i]);
                //var_dump($array);
                break;
            }
        }

        $service_new_string = implode(",", $array);

        //echo $service_new_string;
        //exit();

        $new_service_array = array('service_id' => $service_new_string, 'service_price' => $new_price);


        $query = $this->dashboard_model->delete_service($booking_id, $new_service_array);
        $query1 = $this->dashboard_model->delete_service_mapping($booking_id, $s_id);


        redirect("dashboard/booking_edit?b_id=" . $booking_id);

        exit();

        if (isset($query) && $query) {
            $this->session->set_flashdata('succ_msg', "Service Deleted Successfully!");
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            //$data['bookings'] = $this->dashboard_model->all_bookings($config['per_page'], $segment);
            $data['bookings'] = $this->dashboard_model->all_bookings();
            $data['transactions'] = $this->dashboard_model->all_transactions();
            $data['page'] = 'backend/dashboard/all_bookings';
            $this->load->view('backend/common/index', $data);
        } else {
            $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            //$data['bookings'] = $this->dashboard_model->all_bookings($config['per_page'], $segment);
            $data['bookings'] = $this->dashboard_model->all_bookings();
            $data['transactions'] = $this->dashboard_model->all_transactions();
            $data['page'] = 'backend/dashboard/all_bookings';
            $this->load->view('backend/common/index', $data);
        }
    }

    function delete_guest() {
        $found = in_array("9", $this->arraay);
        if ($found) {

            $data['heading'] = '';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $g_id = $_GET['g_id'];

            $query = $this->dashboard_model->delete_guest($g_id);

            $data = array(
                'data' => "sucess",
                'type' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
                'type' => "warning",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function delete_broker() {

        $found = in_array("21", $this->arraay);
        if ($found) {
            $data['heading'] = '';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $b_id = $_GET['b_id'];

            $query = $this->dashboard_model->delete_broker($b_id);

            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function delete_compliance() {
        $found = in_array("15", $this->arraay);
        if ($found) {
            $data['heading'] = '';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $c_id = $_GET['c_id'];
            //exit;
            $query = $this->dashboard_model->delete_compliance($c_id);

            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function delete_event() {
        $found = in_array("47", $this->arraay);
        if ($found) {
            $data['heading'] = '';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $e_id = $_GET['e_id'];

            $query = $this->dashboard_model->delete_event($e_id);

            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    /* 23/12/2015 subha */

    function edit_compliance() {
		$this->load->library('upload');
		$this->load->library('image_lib');

        $found = in_array("14", $this->arraay);
        if ($found) {
            $data['heading'] = 'Compliance';
            $data['sub_heading'] = '';
            $data['description'] = 'Edit Compliance Here';
            $data['active'] = 'all_compliance';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['cirtificate'] = $this->dashboard_model->all_cirtificate();
            if ($this->input->post()) {
                date_default_timezone_set("Asia/Kolkata");


                $this->upload->initialize($this->set_upload_optionscer());
                if (($this->upload->do_upload('image_compliance')) && ($this->upload->do_upload('image_compliance2'))) {
                    /*  Image Upload 18.11.2015 */
					if($this->upload->do_upload('image_compliance')){
                    $compliance_img = $this->upload->data('file_name');
                    $filename = $compliance_img;
                    $source_path = './upload/certificate/original/cer_1/' . $filename;
                    $target_path = './upload/certificate/thumb/cer_1/';

                    $config_manip = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 200,
                        'height' => 200
                    );
                   // $this->load->library('image_lib', $config_manip);
				   $this->image_lib->initialize($config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_compliance');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $compliance_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                    /* Images Upload  18.11.2015 */

					}
					
					$this->upload->initialize($this->set_upload_optionscer2());
					if($this->upload->do_upload('image_compliance2')){
                    $compliance_img2 = $this->upload->data('file_name');
                    $filename2 = $compliance_img2;
                    $source_path = './upload/certificate/original/cer_2/' . $filename2;
                    $target_path = './upload/certificate/thumb/cer_2/';

                    $config_manip1 = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 200,
                        'height' => 200
                    );
                   // $this->load->library('image_lib', $config_manip);
				   $this->image_lib->initialize($config_manip1);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_compliance');
                    }
                    // clear //
                    $thumb_name1 = explode(".", $filename2);
                    $compliance_thumb2 = $thumb_name1[0] . $config_manip1['thumb_marker'] . "." . $thumb_name1[1];
                    $this->image_lib->clear();
                    /* Images Upload  18.11.2015 */

					}


                    $compliance_edit = array(
                        'c_valid_for' => $this->input->post('c_valid_for'),
                        'c_name' => $this->input->post('c_name'),
                        'c_owner' => $this->input->post('c_owner'),
                        'c_description' => $this->input->post('c_description'),
                        'c_type' => $this->input->post('c_type'),
                        'c_importance' => $this->input->post('c_importance'),
                        'c_valid_from' => $this->input->post('c_valid_from'),
                        'c_valid_upto' => $this->input->post('c_valid_upto'),
                        'c_renewal' => $this->input->post('c_renewal'),
                        'c_file' => $compliance_img,
                        'c_file_thumb' => $compliance_thumb,
						'c_file_2' => $compliance_img2,
                        'c_file_2_thumb' => $compliance_thumb2,
                        'c_primary_notif_period' => $this->input->post('c_primary'),
                        'c_secondary_notif_period' => $this->input->post('c_secondary'),
                        'c_authority' => $this->input->post('c_authority')
                    );
			} elseif($this->upload->do_upload('image_compliance')) {
                    $this->upload->initialize($this->set_upload_optionscer());
					if($this->upload->do_upload('image_compliance')) {
					$compliance_img = $this->upload->data('file_name');
                    $filename = $compliance_img;
                    $source_path = './upload/certificate/original/cer_1/' . $filename;
                    $target_path = './upload/certificate/thumb/cer_1/';

                    $config_manip = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 200,
                        'height' => 200
                    );
                   // $this->load->library('image_lib', $config_manip);
				   $this->image_lib->initialize($config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_compliance');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $compliance_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					}
					 $compliance_edit = array(
                        'c_valid_for' => $this->input->post('c_valid_for'),
                        'c_name' => $this->input->post('c_name'),
                        'c_owner' => $this->input->post('c_owner'),
                        'c_description' => $this->input->post('c_description'),
                        'c_type' => $this->input->post('c_type'),
                        'c_importance' => $this->input->post('c_importance'),
                        'c_valid_from' => $this->input->post('c_valid_from'),
                        'c_valid_upto' => $this->input->post('c_valid_upto'),
                        'c_renewal' => $this->input->post('c_renewal'),
                        'c_file' => $compliance_img,
                        'c_file_thumb' => $compliance_thumb,
						//'c_file_2' => $compliance_img2,
                        //'c_file_2_thumb' => $compliance_thumb2,
                        'c_primary_notif_period' => $this->input->post('c_primary'),
                        'c_secondary_notif_period' => $this->input->post('c_secondary'),
                        'c_authority' => $this->input->post('c_authority')
                    );
                } elseif($this->upload->do_upload('image_compliance2')){
					$this->upload->initialize($this->set_upload_optionscer2());
					if($this->upload->do_upload('image_compliance2')){
                    $compliance_img2 = $this->upload->data('file_name');
                    $filename2 = $compliance_img2;
                    $source_path = './upload/certificate/original/cer_2/' . $filename2;
                    $target_path = './upload/certificate/thumb/cer_2/';

                    $config_manip1 = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 200,
                        'height' => 200
                    );
                   // $this->load->library('image_lib', $config_manip);
				   $this->image_lib->initialize($config_manip1);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_compliance');
                    }
                    // clear //
                    $thumb_name1 = explode(".", $filename2);
                    $compliance_thumb2 = $thumb_name1[0] . $config_manip1['thumb_marker'] . "." . $thumb_name1[1];
                    $this->image_lib->clear();
                    /* Images Upload  18.11.2015 */

					}
					$compliance_edit = array(
                        'c_valid_for' => $this->input->post('c_valid_for'),
                        'c_name' => $this->input->post('c_name'),
                        'c_owner' => $this->input->post('c_owner'),
                        'c_description' => $this->input->post('c_description'),
                        'c_type' => $this->input->post('c_type'),
                        'c_importance' => $this->input->post('c_importance'),
                        'c_valid_from' => $this->input->post('c_valid_from'),
                        'c_valid_upto' => $this->input->post('c_valid_upto'),
                        'c_renewal' => $this->input->post('c_renewal'),
                      //  'c_file' => $compliance_img,
                       // 'c_file_thumb' => $compliance_thumb,
						'c_file_2' => $compliance_img2,
                        'c_file_2_thumb' => $compliance_thumb2,
                        'c_primary_notif_period' => $this->input->post('c_primary'),
                        'c_secondary_notif_period' => $this->input->post('c_secondary'),
                        'c_authority' => $this->input->post('c_authority')
                    );
					
				}else {
					$compliance_edit = array(
                        'c_valid_for' => $this->input->post('c_valid_for'),
                        'c_name' => $this->input->post('c_name'),
                        'c_owner' => $this->input->post('c_owner'),
                        'c_description' => $this->input->post('c_description'),
                        'c_type' => $this->input->post('c_type'),
                        'c_importance' => $this->input->post('c_importance'),
                        'c_valid_from' => $this->input->post('c_valid_from'),
                        'c_valid_upto' => $this->input->post('c_valid_upto'),
                        'c_renewal' => $this->input->post('c_renewal'),
                      //  'c_file' => $compliance_img,
                       // 'c_file_thumb' => $compliance_thumb,
						//'c_file_2' => $compliance_img2,
                      //  'c_file_2_thumb' => $compliance_thumb2,
                        'c_primary_notif_period' => $this->input->post('c_primary'),
                        'c_secondary_notif_period' => $this->input->post('c_secondary'),
                        'c_authority' => $this->input->post('c_authority')
                    );
				}

			/*	echo "<pre>";
				print_r($compliance_edit);
				exit();
				*/
                $query = $this->dashboard_model->edit_compliance($compliance_edit, $this->input->post('compliance_id'));

                $c_id = $this->input->post('c_id');

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Compliances  Update Successfully!");
                   /* $data['value'] = $this->dashboard_model->get_c_details($c_id);
                    $data['compliance'] = $this->dashboard_model->all_compliance_limit();
                    $data['page'] = 'backend/dashboard/all_compliance';
                    $this->load->view('backend/common/index', $data);*/
					redirect('dashboard/all_compliance');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    $data['value'] = $this->dashboard_model->get_c_details($c_id);
                    $data['compliance'] = $this->dashboard_model->all_compliance_limit();

                    $data['page'] = 'backend/dashboard/all_compliance';
                    $this->load->view('backend/common/index', $data);
                }
            } else {
                $c_id = $_GET['c_id'];
                $data['value'] = $this->dashboard_model->get_c_details($c_id);
                $data['page'] = 'backend/dashboard/edit_compliance';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function add_event() {


        $found = in_array("44", $this->arraay);
        if ($found) {
            $data['heading'] = 'Events';
            $data['sub_heading'] = 'Add Event';
            $data['description'] = 'Add Event Here';
            $data['active'] = 'add_event';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                date_default_timezone_set("Asia/Kolkata");

                if ($this->input->post('e_seasonal') != null) {
                    $seasonal = $this->input->post('e_seasonal');
                } else {
                    $seasonal = '0';
                }


                $event = array(
                    'e_name' => $this->input->post('e_name'),
                    'e_from' => date("y-m-d", strtotime($this->input->post('e_from'))),
                    'e_upto' => date("y-m-d", strtotime($this->input->post('e_upto'))),
                    'e_notify' => $this->input->post('e_notify'),
                    'e_seasonal' => $seasonal,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'event_color' => $this->input->post('e_event_color'),
                    'event_text_color' => $this->input->post('e_event_text_color'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );

                //print_r($event); exit;
                $query = $this->dashboard_model->add_event($event);

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Event Added Successfully!");
                    redirect('dashboard/all_events');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_event');
                }
            } else {
                $data['page'] = 'backend/dashboard/add_event';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function edit_event() {
        $e_id = $_POST['e_id'];
        //$e_id=14;
        $event = $this->dashboard_model->fetch_e_id($e_id);
        $data = array(
            'e_id' => $event->e_id,
            'e_name' => $event->e_name,
            'e_from' => $event->e_from,
            'e_upto' => $event->e_upto,
            'e_notify' => $event->e_notify,
            'e_seasonal' => $event->e_seasonal,
            'hotel_id' => $event->hotel_id,
            'event_color' => $event->event_color,
            'event_text_color' => $event->event_text_color
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function update_events() {
        echo $id = $_POST['e_id'];
        exit;
        $data = array(
            'e_id' => $_POST['e_id'],
            'e_name' => $_POST['e_name'],
            'e_from' => $_POST['e_from'],
            'e_upto' => $_POST['c_vali_upto'],
            'e_notify' => $_POST['e_notify'],
            'e_seasonal' => $_POST['e_seasonal'],
            'event_color' => $_POST['e_event_color'],
            'event_text_color' => $_POST['e_event_text_color'],
        );
        $query = $this->dashboard_model->update_event($data);
        if ($query) {
            $data = array(
                'data' => '1');
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function all_events() {

        $found = in_array("14", $this->arraay);
        if ($found) {
            $data['heading'] = 'Events';
            $data['sub_heading'] = 'All Events';
            $data['description'] = 'Events List';
            $data['active'] = 'all_events';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $data['events'] = $this->dashboard_model->all_events_limit();
            $data['page'] = 'backend/dashboard/all_events';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function add_feedback() {


        $found = in_array("48", $this->arraay);
        if ($found) {
            $data['heading'] = 'Feedback';
            $data['sub_heading'] = 'Add Feedback';
            $data['description'] = 'Add Feedback Here';
            $data['active'] = 'all_feedback';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                //print_r($_POST);
                //exit;
                if ($this->input->post('booking_id')) {
                    $booking_id = $this->input->post('booking_id');
                } else {
                    $booking_id = '0';
                }
                $feedback = array(
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'guest_name' => $this->input->post('gName'),
                    'booking_id' => $booking_id,
                    'ease_booking' => $this->input->post('w1'),
                    'reception' => $this->input->post('w2'),
                    'staff' => $this->input->post('w3'),
                    'cleanliness' => $this->input->post('w4'),
                    'ambience' => $this->input->post('w5'),
                    'sleep_quality' => $this->input->post('w6'),
                    'room_quality' => $this->input->post('w7'),
                    'food_quality' => $this->input->post('w8'),
                    'env_quality' => $this->input->post('w9'),
                    'service' => $this->input->post('w10'),
                    'package' => $this->input->post('w11'),
                    'extra' => $this->input->post('w12'),
                    'come_back' => $this->input->post('or1'),
                    'refer_friend' => $this->input->post('or2'),
                    'reasonable_cost' => $this->input->post('or3'),
                    'suggestion' => $this->input->post('or4'),
                    'comment' => $this->input->post('comment'),
                    'add_social_media' => $this->input->post('or5'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
                $query = $this->dashboard_model->add_feedback($feedback);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Feedback Added Successfully!");
                    if ($booking_id > 0) {
                        redirect('dashboard/booking_edit?b_id=' . $booking_id);
                    } else {
                        redirect('dashboard/add_feedback');
                    }
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    if ($booking_id > 0) {
                        redirect('dashboard/booking_edit?b_id=' . $booking_id);
                    } else {
                        redirect('dashboard/add_feedback');
                    }
                }
            } else {
                if (isset($_GET['bID'])) {
                    $data['guest'] = $this->dashboard_model->get_booking_details($_GET['bID']);
                    $data['page'] = 'backend/dashboard/add_feedback';
                    $this->load->view('backend/common/index', $data);
                } else {
                    $data['page'] = 'backend/dashboard/add_feedback';
                    $this->load->view('backend/common/index', $data);
                }
            }
        } else {
            redirect('dashboard');
        }
    }

    function fetch_gst_by_id() {
        $id = $_GET['fdback_id'];
        $data['heading'] = 'Feedback';
        $data['sub_heading'] = 'Add Feedback';
        $data['description'] = 'Add Feedback Here';
        $data['active'] = 'all_feedback';
        // $query=$this->dashboard_model->fetch_e_id($id);
        //$data['admin']=$this->dashboard_model->fetch_admin();
        $data['fdback'] = $this->dashboard_model->fetch_fdback_id($id);
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/edit_feedback';

        $this->load->view('backend/common/index', $data);
    }

    function fetch2_gst_by_id() {
        $id = $_GET['fdback_id'];
        $data['heading'] = 'Feedback';
        $data['sub_heading'] = 'Add Feedback';
        $data['description'] = 'Add Feedback Here';
        $data['active'] = 'all_feedback';
        // $query=$this->dashboard_model->fetch_e_id($id);
        //$data['admin']=$this->dashboard_model->fetch_admin();
        $data['fdback'] = $this->dashboard_model->fetch_fdback_id($id);
        $data['feedback'] = $this->dashboard_model->all_feedback();
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/view_feedback';

        $this->load->view('backend/common/index', $data);
    }

    function edit_feedback() {

        $found = in_array("49", $this->arraay);
        if ($found) {
            $data['heading'] = 'Feedback';
            $data['sub_heading'] = 'update Feedback';
            $data['description'] = 'update Feedback Here';
            $data['active'] = 'all_feedback';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                //print_r($_POST);
                //exit;
                if ($this->input->post('booking_id')) {
                    $booking_id = $this->input->post('booking_id');
                } else {
                    $booking_id = '0';
                }
                $feedback = array(
                    'id' => $this->input->post('f_id'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'guest_name' => $this->input->post('gName'),
                    'booking_id' => $booking_id,
                    'ease_booking' => $this->input->post('w1'),
                    'reception' => $this->input->post('w2'),
                    'staff' => $this->input->post('w3'),
                    'cleanliness' => $this->input->post('w4'),
                    'ambience' => $this->input->post('w5'),
                    'sleep_quality' => $this->input->post('w6'),
                    'room_quality' => $this->input->post('w7'),
                    'food_quality' => $this->input->post('w8'),
                    'env_quality' => $this->input->post('w9'),
                    'service' => $this->input->post('w10'),
                    'package' => $this->input->post('w11'),
                    'extra' => $this->input->post('w12'),
                    'come_back' => $this->input->post('or1'),
                    'refer_friend' => $this->input->post('or2'),
                    'reasonable_cost' => $this->input->post('or3'),
                    'suggestion' => $this->input->post('or4'),
                    'comment' => $this->input->post('comment'),
                    'add_social_media' => $this->input->post('or5')
                );
                //echo "<pre>";
                //print_r($feedback);
                //exit();
                $query = $this->dashboard_model->update_feedback($feedback);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Feedback Update Successfully!");
                    if ($booking_id > 0) {
                        redirect('dashboard/booking_edit?b_id=' . $booking_id);
                    } else {
                        redirect('dashboard/all_feedback');
                    }
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    if ($booking_id > 0) {
                        redirect('dashboard/booking_edit?b_id=' . $booking_id);
                    } else {
                        redirect('dashboard/all_feedback');
                    }
                }
            } else {
                if (isset($_GET['bID'])) {
                    $data['guest'] = $this->dashboard_model->get_booking_details($_GET['bID']);
                    $data['page'] = 'backend/dashboard/add_feedback';
                    $this->load->view('backend/common/index', $data);
                } else {
                    $data['page'] = 'backend/dashboard/add_feedback';
                    $this->load->view('backend/common/index', $data);
                }
            }
        } else {
            redirect('dashboard');
        }
    }

    function all_feedback() {

        $found = in_array("48", $this->arraay);
        if ($found) {
            $data['heading'] = 'Feedback';
            $data['sub_heading'] = 'All Feedbacks';
            $data['description'] = 'Feedbacks List';
            $data['active'] = 'all_feedback';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['feedback'] = $this->dashboard_model->all_feedback();
            $data['page'] = 'backend/dashboard/all_feedback';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function delete_feedback() {

        $found = in_array("50", $this->arraay);
        if ($found) {
            $id = $_GET['f_id'];
            $data['heading'] = '';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $query = $this->dashboard_model->delete_feedback($id);

            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function get_guest_name($query) {
        $data = $this->dashboard_model->get_guest_name($query);
        foreach ($data as $row) {
            $val[] = implode(',', $row);
        }
        $val = '[' . '"' . implode('","', $val) . '"' . ']';
        print_r($val);
        exit;
    }

    function pay_broker() {


        $found = in_array("19", $this->arraay);
        if ($found) {

            $data['heading'] = 'Pay Broker';
            $data['sub_heading'] = 'Pay Broker';
            $data['description'] = 'Pay Broker Here';
            $data['active'] = 'add_broker';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                
                date_default_timezone_set("Asia/Kolkata");
                
                $event = array(
                    'e_name' => $this->input->post('e_name'),
                    'e_from' => date("y-m-d", strtotime($this->input->post('e_from'))),
                    'e_upto' => date("y-m-d", strtotime($this->input->post('e_upto'))),
                    'e_notify' => $this->input->post('e_notify'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                );
                $query = $this->dashboard_model->add_event($event);

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Event Added Successfully!");
                    redirect('dashboard/add_event');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_event');
                }
            } else {
                $data['page'] = 'backend/dashboard/add_event';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function send_message() {

        $from = $_GET["from"];
        $to = $_GET["to"];
        $message = $_GET["message"];





        $data = array(
            'u_from_id' => $from,
            'u_to_id' => $to,
            'm_body' => $message,
        );


        $query = $this->dashboard_model->send_message($data);

        if ($query) {
            echo $query;
        } else {
            echo $query;
        }
    }

    function get_message() {

        $from = $_GET["from"];
        $to = $_GET["to"];
        //$message=$_GET["message"];

        if ($from != "" && $to != "") {

            $msgs = $this->dashboard_model->get_message($from, $to);
            if (isset($msgs) && $msgs) {
                foreach ($msgs as $msg) {
                    $text = $msg->m_body;
                    $id = $msg->m_id;
                }
            } else {
                $id = 0;
                $text = "";
            }
            if ($id != 0) {
                $this->dashboard_model->update_message($from, $to, $id);
            }

            if ($text != "") {
                echo $text;
            } else {
                echo "";
            }
        }
    }

    function unit_type() {
        $found = in_array("7", $this->arraay);
        if ($found) {
            $data['heading'] = 'Room';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['active'] = 'unit_type';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $data['unit_class'] = $this->dashboard_model->get_unit_class_list();
            //	echo "<pre>";
            //	print_r($data['unit_class']);exit;
            $data['kit'] = $this->dashboard_model->get_all_kit();
            $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
            $data['unit'] = $this->dashboard_model->all_unit_type();
            //$data['type'] = $this->dashboard_model->all_unit_type();
            $data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
			//echo "<pre>";
			//print_r($this->session->userdata());
			//print_r($data['unitTypeName']);exit;
			
            $data['page'] = 'backend/dashboard/unit_type';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function add_unit_type() {

        $found = in_array("10", $this->arraay);
        if ($found) {
            $data['heading'] = 'Unit Type';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['active'] = 'room_type';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
            if ($this->input->post()) {

                $utype = $this->input->post('unit_type');
                $uclass = $this->input->post('unit_class');

                $type = (explode("/", $utype));
                $uclass = (explode("/", $uclass));

                $unit = array(
                    'unit_cate_id' => $type[1],
                    'unit_class_id' => $uclass[1],
                    'unit_type' => $type[0],
                    'unit_name' => $this->input->post('unit_name'),
                    'unit_class' => $uclass[0],
                    'unit_desc' => $this->input->post('desc'),
                    'kit' => $this->input->post('kit'),
                    'default_occupancy' => $this->input->post('default_occ'),
                    'max_occupancy' => $this->input->post('max_occ'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
                //echo "<pre>";
                //print_r($unit);exit;
                $kit_id = $this->input->post('kit');
                $query = $this->dashboard_model->add_unit_type($unit, $kit_id);

                //echo "query".$query;

                $rate_plan_index_value = $this->dashboard_model->get_rate_plan_value($this->session->userdata('user_hotel'));
                //POPULATE RATE PLAN AT THE TIME OF UNIT TYPE INSERT
                //echo "rate_plan_index_value".$rate_plan_index_value;"<br>";
                //	if ((isset($query) && $query) && ($rate_plan_index_value!='' && isset($rate_plan_index_value))){
                //	function add_rate_plan()
                //{


                $plan_rate_type = $this->rate_plan_model->all_rate_plan_type();
                $booking_source = $this->rate_plan_model->all_booking_source_type();
                $all_unit_type = $this->rate_plan_model->all_units2($query);
                $all_meal_plan = $this->rate_plan_model->all_meal_plan();
                $occupancy = $this->rate_plan_model->all_occupancy();



                /* 	echo "<pre>";
                  print_r($plan_rate_type);
                  echo "<pre>";
                  print_r($booking_source);
                  echo "<pre>";
                  print_r($all_unit_type);
                  echo "<pre>";
                  print_r($all_meal_plan);
                  echo "<pre>";
                  print_r($occupancy);
                  echo "Finished *********"; */


                foreach ($plan_rate_type as $rate_type) {
                    foreach ($booking_source as $bs) {
                        foreach ($all_unit_type as $u_type) {
                            foreach ($all_meal_plan as $m_c) {
                                foreach ($occupancy as $oc) {

                                    $data = array(
                                        'rate_plan_type_id' => $rate_type->rate_plan_type_id,
                                        'rate_plan_type_name' => $rate_type->name,
                                        'source_type_id' => $bs->bst_id,
                                        'source_type_name' => $bs->bst_name,
                                        'unit_type_id' => $u_type->id,
                                        'unit_type_name' => $u_type->unit_name,
                                        'meal_plan_id' => $m_c->hotel_meal_plan_cat_id,
                                        'meal_plan' => $m_c->name,
                                        'occupancy_type' => $oc->name,
                                        'occupancy_id' => $oc->hotel_occupancy_id,
                                        //'user_id'=>$this->session->userdata('user_id'),
                                        'hotel_id' => $this->session->userdata('user_hotel')
                                    );

                                    /* 	   echo "<pre>";
                                      print_r($data);
                                      echo "</pre>";
                                      echo "array  ************"; */

                                    $this->db1->insert('rate_plan', $data);
                                }
                            }
                        }
                    }
                }





                //	}
                //}
                /* else{

                  echo "value".$rate_plan_index_value;exit;
                  }
                 */
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Unit Type Added Successfully!");
                    redirect('dashboard/unit_type');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/unit_type');
                }
            }
        } else {
            redirect('dashboard');
        }
    }

    function delete_unit_type() {

        $found = in_array("12", $this->arraay);
        if ($found) {
            $id = $_GET['f_id'];
            $data['heading'] = '';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));

            $room_exists = $this->dashboard_model->get_room_by_unittype($id, $this->session->userdata('user_hotel'));
            if ($room_exists) {


                $data = array(
                    'data' => "This Unit type all ready has rooms",
                );
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }
            $query = $this->dashboard_model->delete_unit_type($id);

            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function get_amount_guest() {


        $booking_id = $_GET["booking_id"];

        $booking_id_actual = substr($booking_id, -4);
        $datas = $this->dashboard_model->particular_booking(intval($booking_id_actual));
        if (isset($datas) && $datas) {
            foreach ($datas as $data) {

                $amount = $data->room_rent_sum_total;
            }
        } else {
            $amount = 0;
        }

        echo $amount;
    }

    function get_unit_name() {
		
		$unit_npt ='';
        $unit_type = $this->input->post('val');
        $query = $this->dashboard_model->get_unit_name($unit_type);
        //echo "<pre>";
        if ($query != 0) {
           $unit_npt = '<select class="form-control bs-select" id="unit_type" name="unit_name" required="required" onchange="get_unit_class1(this.value)">';
             $unit_npt .= '<option value="" disabled selected="selected">Unit Type</option>';
            foreach ($query as $qry) {
               $unit_npt .= '<option value="' . $qry["id"] . '">' . $qry["unit_name"] . '</option>';
            }
            $unit_npt .= '</select><label></label><span class="help-block">Unit Type *</span>';
            
			echo $unit_npt;
        } else {
            echo $a = 0;
        }
    }

    function get_unit_class() {
        $unitID = $this->input->post('val');
        $qry = $this->dashboard_model->get_unit_class($unitID);
        //echo "<pre>";
        //print_r($qry);
        echo '<input type="text" autocomplete="off" class="form-control input-sm" readonly id="unit_class" name="unit_class" placeholder="Unit Class *" required="required" value="' . $qry['unit_class'] . '">
			<input type="hidden" name="max_occupancy" id="max_occupancy" value="' . $qry['max_occupancy'] . '"><input type="hidden" name="room_bed" id="room_bed" value="' . $qry['default_occupancy'] . '"> ';

        //exit;
    }

    function booking_edit() {

        $found = in_array("26", $this->arraay);
        if ($found) {
            $data['heading'] = 'Booking View';
            $data['sub_heading'] = 'Booking Details Page';
            $data['description'] = 'Bookings Details';
            $data['active'] = 'booking_edit';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['Tsettings'] = $this->dashboard_model->get_Tsettings($this->session->userdata('user_hotel'));
            //echo $this->uri->segment(3);
            /* $bookings=$this->unit_class_model->all_laudry_id($bookingID,$this->session->userdata('user_hotel'));
              echo "<pre>";
              print_r($bookings);
              exit; */
            $bookingID = $_GET['b_id'];
            $services = $this->dashboard_model->all_services();
            if (isset($services) && $services) {
                foreach ($services as $service) {
                    $match = $this->dashboard_model->service_booking_match($bookingID, $service->s_rules);
                    if (isset($match) && $match) {
                        $data['s_id'] = $service->s_id;
                        $data['s_name'] = $service->s_name;
                        $data['s_category'] = $service->s_category;
                        $data['s_price'] = $service->s_price;
                        $data['s_tax'] = $service->s_tax;
                        $data['s_discount'] = $service->s_discount;
                    } else {
                        $data['s_id'] = "";
                    }
                }
            }
            $data['bookingDetails'] = $this->dashboard_model->get_booking_details($bookingID);
            $data['lineItemDetails'] = $this->bookings_model->bookingChargeIineItem_fetch($bookingID);
            $data['guestDetails'] = $this->unit_class_model->get_hotel_stay_guest($bookingID);
            $data['bookingLineItem'] = $this->dashboard_model->get_bookingLineItem($bookingID);
            $data['feedback'] = $this->dashboard_model->all_feedbackByID($bookingID);
            $data['transaction'] = $this->dashboard_model->all_transactions_byID('sb',$bookingID);
            $data['all_adjst'] = $this->dashboard_model->all_adjst($bookingID);

            $data['page'] = 'backend/dashboard/booking_edit';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function add_service_to_booking() {

        $found = in_array("25", $this->arraay);
        if ($found) {

            $s_id = $this->input->post("service_id");
            $s_price = $this->input->post("service_price");
            $s_tax = $this->input->post("service_tax");
            $booking_id = $this->input->post("booking_id");
            $sevice_total = $this->input->post("service_total");
            $qty = $this->input->post("qty");



            $data = array(
                'booking_id' => $booking_id,
                'service_id' => ',' . $s_id,
                'service_price' => $sevice_total
            );
            $query = $this->dashboard_model->add_service_to_booking_db($data);

            $services = array(
                'booking_id' => $booking_id,
                'service_id' => $s_id,
                'qty' => $qty,
                's_price' => $s_price,
                'tax' => $s_tax,
                'total' => $sevice_total,
                'hotel_id' => $this->session->userdata('user_hotel'),
                'user_id' => $this->session->userdata('user_id')
            );
            $query1 = $this->dashboard_model->add_service_to_mapping($services);
            $data = array(
                'return' => $query
            );



            header('Content-Type: application/json');
            echo json_encode($data);

            return $query;
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function add_kit_to_booking() {
        $type = "sb";
        $sub_stock_update_array1 = array();
        $kit_name = $this->input->post("kit_name");
        $kit_qty = $this->input->post("kit_qty");
        $kit_type = $this->input->post("kitid");
        $booking_id = $this->input->post("booking_id");
        $kit_price = $this->input->post("kit_price");
        $warehouse = $this->input->post("warehouse");
        $grp_bk = $this->input->post("grp_bk");

        $kit_services = array(
            'booking_id' => $booking_id,
            'kit_id' => $kit_type,
            'item_id' => $kit_name,
            'qty' => $kit_qty,
            'price' => $kit_price,
            'warehouse_id' => $warehouse,
            'hotel_id' => $this->session->userdata('user_hotel'),
            'user_id' => $this->session->userdata('user_id'),
            'booking_type' => 'sb',
            'group_id' => $grp_bk
        );

        //print_r($kit_services);exit;
        $this->db1->select('*');
        $this->db1->where('item_id', $kit_name);
        $this->db1->where('hotel_id', $this->session->userdata('user_hotel'));
        $this->db1->where('warehouse_id', $warehouse);
        $stock_qty_query1 = $this->db1->get('hotel_stock_qty_mapping');
        if ($stock_qty_query1->num_rows() > 0) {


            // update stock inventory ..

            $this->db1->select('*');
            $this->db1->where('hotel_stock_inventory_id', $kit_name);
            $this->db1->where('hotel_id', $this->session->userdata('user_hotel'));
            $this->db1->where('warehouse', $warehouse);
            $stock_query = $this->db1->get('hotel_stock_inventory');
            if ($stock_query->num_rows() > 0) {

                $stock_update_array['qty'] = $stock_query->row()->qty - $kit_qty;
                $stock_update_array['closing_qty'] = $stock_query->row()->closing_qty - $kit_qty;
                $stock_update_array['user_id'] = $this->session->userdata('user_id');
                $stock_update_array['note'] = "item  update successfully on" . date('Y-m-d') . "by" . $this->session->userdata('user_id');
                $stock_update_array['date'] = date('Y-m-d');


                $result1 = $this->dashboard_model->update_stock_by_item($stock_update_array, $kit_name, $warehouse);

                $result2 = $this->dashboard_model->update_stock_qty_mapping($kit_name, $warehouse, $kit_qty);

                $result = $this->dashboard_model->add_kit_to_booking($kit_services);

                $sub_stock_update_array1['hotel_stock_inventory_id'] = $kit_name;
                //$sub_stock_update_array1['date']  =  date('Y-m-d');
                $sub_stock_update_array1['a_type'] = $stock_query->row()->item_type;
                $sub_stock_update_array1['a_category'] = $stock_query->row()->a_category;
                $sub_stock_update_array1['item'] = $kit_name;
                $sub_stock_update_array1['vendor'] = '';
                $sub_stock_update_array1['warehouse'] = $warehouse;
                $sub_stock_update_array1['link_purchase'] = $booking_id;
                $sub_stock_update_array1['qty'] = $kit_qty;
                $sub_stock_update_array1['opening_qty'] = $stock_query->row()->opening_qty;
                $sub_stock_update_array1['closing_qty'] = $stock_query->row()->closing_qty - $kit_qty;
                $sub_stock_update_array1['price'] = $kit_price;
                $sub_stock_update_array1['operation'] = "consumption single item";
                $sub_stock_update_array1['note'] = "consumption single item by" . '' . $booking_id . '' . $type;
                $sub_stock_update_array1['user_id'] = $this->session->userdata('user_id');
                $sub_stock_update_array1['hotel_id'] = $this->session->userdata('user_hotel');

                //print_r($sub_stock_update_array1);exit;
                $this->dashboard_model->insert_sub_stock_by_item($sub_stock_update_array1);


                //}		
                $data_array = array(
                    'data' => "kit added successfully",
                );
            }
        } else {

            $data_array = array(
                'data' => "Stock is empty for this item",
            );
        }
        header('Content-Type: application/json');
        echo json_encode($data_array);
    }

    function add_charge_to_booking() {
		date_default_timezone_set("Asia/Kolkata");
        $found = in_array("25", $this->arraay);
        if ($found) {
            $name = $this->input->post("name");
            $hsn_sac = $this->input->post("hsn_sac");
            $quantity = $this->input->post("quantity");
            $unit_price = $this->input->post("unit_price");
            $sgst = $this->input->post("sgst");
            $cgst = $this->input->post("cgst");
            $igst = $this->input->post("igst");
            $utgst = $this->input->post("utgst");
            $tax = $this->input->post("tax");
            $total = $this->input->post("total");
            $booking_id = $this->input->post('booking_id');
            $booking_type = $this->input->post('booking_type');
            $extra_charges_id = $this->input->post('extra_charges_id');


            $data = array(
                'extra_charge_id' => $extra_charges_id,
                'booking_type' => $booking_type,
                'crg_booking_id' => $booking_id,
                'crg_description' => $name,
                'hsn_sac' => $hsn_sac,
                'crg_quantity' => $quantity,
                'crg_unit_price' => $unit_price,
                'sgst' => $sgst,
                'cgst' => $cgst,
                'igst' => $igst,
                'utgst' => $utgst,
                'crg_tax' => $tax,
                'crg_total' => $total,
                'aded_on' => date('y-m-d h:i:s'),
                'user_id' => $this->session->userdata('user_id'),
                'hotel_id' => $this->session->userdata('user_hotel')
            );

            $c_price = $unit_price * $quantity;

            if ($extra_charges_id == null) {
                $data1 = array(
                    'charge_name' => $name,
					'hsn_sac' => $hsn_sac,
                    'charge_des' => $name,
                    'unit_price' => $unit_price,
                    'charge_sgst' => $sgst,
                    'charge_cgst' => $cgst,
                    'charge_igst' => $igst,
                    'charge_utgst' => $utgst,
                    'charge_tax' => $tax,
                    'user_id' => $this->session->userdata('user_id'),
                    'hotel_id' => $this->session->userdata('user_hotel')
                );

                //...newly added on 050917..
          $chkextraCh=$this->dashboard_model->ch_extra_charge($name);
            if($chkextraCh==0){
                $query6 = $this->dashboard_model->insert_extra_charge($data1);
            }
               // $query6 = $this->dashboard_model->insert_extra_charge($data1);
            }

            $query = $this->dashboard_model->insert_charge($data);

            $data2 = array(
                'booking_id' => $booking_id,
                'booking_extra_charge_id' => ',' . $query,
                'booking_extra_charge_amount' => $total,
                'aded_on' => date('y-m-d h:i:s'),
            );

            $query2 = $this->dashboard_model->add_charge_to_booking_db($data2);

            $data_array = array(
                'return' => $query,
                'return2' => $query2
            );

            header('Content-Type: application/json');
            echo json_encode($data_array);

            // return $query;
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function add_charge_to_booking_group() {

        $found = in_array("25", $this->arraay);
        if ($found) {
            $name = $this->input->post("name");
            $hsn_sac = $this->input->post("hsn_sac");
            $quantity = $this->input->post("quantity");
            $unit_price = $this->input->post("unit_price");
			$sgst = $this->input->post("sgst");
            $cgst = $this->input->post("cgst");
            $igst = $this->input->post("igst");
            $utgst = $this->input->post("utgst");
            $tax = $this->input->post("tax");
            $total = $this->input->post("total");
            $booking_id = $this->input->post('booking_id');


            $data = array(
                'crg_description' => $name,
                'hsn_sac' => $hsn_sac,
                'booking_type' => 'gb',
                'crg_booking_id' => $booking_id,
                'crg_quantity' => $quantity,
                'crg_unit_price' => $unit_price,
				'sgst' => $sgst,
                'cgst' => $cgst,
                'igst' => $igst,
                'utgst' => $utgst,
                'crg_tax' => $tax,
                'crg_total' => $total,
				'aded_on' => date('y-m-d h:i:s'),
                'hotel_id' => $this->session->userdata('user_hotel'),
                'user_id' => $this->session->userdata('user_id')
            );
			
			//print_r($data);
			/*echo json_encode($data);
			exit;*/
			
			 $data1 = array(
                    'charge_name' => $name,
					'hsn_sac' => $hsn_sac,
                    'charge_des' => $name,
                    'unit_price' => $unit_price,
                    'charge_sgst' => $sgst,
                    'charge_cgst' => $cgst,
                    'charge_igst' => $igst,
                    'charge_utgst' => $utgst,
                    'charge_tax' => $tax,
                    'user_id' => $this->session->userdata('user_id'),
                    'hotel_id' => $this->session->userdata('user_hotel')
                );
			
			
			

            $c_price = $unit_price * $quantity;
           
           $chkextraCh=$this->dashboard_model->ch_extra_charge($name);
            if($chkextraCh==0){
                $query6 = $this->dashboard_model->insert_extra_charge($data1);
                
                
            }
           $query = $this->dashboard_model->insert_charge($data); 

            $data2 = array(
                'booking_id' => $booking_id,
                'booking_extra_charge_id' => ',' . $query,
                'booking_extra_charge_amount' => $total,
                'aded_on' => date('y-m-d h:i:s'),
            );

            $query2 = $this->dashboard_model->add_charge_to_booking_db_group($data2);

            $data_array = array(
                'return' => $query,
                'return2' => $query2
            );



            header('Content-Type: application/json');
            echo json_encode($data_array);

            // return $query;
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function add_stay_details() {
        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['heading'] = '';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['active'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                $booking_id = $this->input->post('booking_id');
				
				if(isset($bookingDetails->cust_end_date)){
					$dc = (date('d', strtotime($bookingDetails->cust_end_date)) - date('d', strtotime($this->input->post('from_date1'))));
				}
                if ($this->input->post('cust_end_date' != NULL))
                    $cod = date('Y-m-d h:i:s', strtotime($this->input->post('cust_end_date')));
                else
                    $cod = NULL;
				
				if ($this->input->post('cust_from_date' != NULL))
                    $csd = date('Y-m-d h:i:s', strtotime($this->input->post('cust_from_date')));
                else
                    $csd = NULL;
				
                $chkt = '';
                $x = $this->input->post('checkin_time');
                if (isset($x))
                    $chkt = 'checkin_time';
				

                $stay = array(
                    'checkin_date' => $csd,
                    'confirmed_checkin_time' => $this->input->post('confirmed_checkin_time'),
                    'checkin_time' => $this->input->post('checkin_time'),
                    'arrival_mode' => $this->input->post('arrival_mode'),
                    'coming_from' => $this->input->post('coming_from'),
                    'checkout_date' => $cod,
                    'confirmed_checkout_time' => $this->input->post('confirmed_checkout_time'),
                    'checkout_time' => $this->input->post('checkout_time'),
                    'dept_mode' => $this->input->post('dept_mode'),
                    'departure_details' => $this->input->post('departure_details'),
                    'arrival_details' => $this->input->post('arrival_details'),
                    'next_destination' => $this->input->post('next_destination')
                );
                /*print_r($stay);
                exit;*/
                $query = $this->dashboard_model->add_stay_details($stay, $booking_id);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Stay Details updated Successfully!");
                    redirect('dashboard/booking_edit?b_id=' . $booking_id);
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/booking_edit?b_id=' . $booking_id);
                }
            } else {
                $data['page'] = 'backend/dashboard/booking_edit';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function add_pay_info() {


        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                //print_r($_POST);
                //exit;
                //base_room_rent
                if ($this->input->post('modifier_type') == 'add') {
                    $val = $this->input->post('actual_room_rent') + $this->input->post('modifier_amount');
                } else {
                    $val = $this->input->post('actual_room_rent') - $this->input->post('modifier_amount');
                }
                $val = $this->input->post("actual_room_rent");
                //$tot_amt =$this->input->post('room_rent_amount');
                $tot_amt = $this->input->post('room_rent_total_amount');
                $tax_amt = $this->input->post("room_rent_tax_amount");
                $tm = $this->input->post("room_rent_sum_total");
                $booking_id = $this->input->post('booking_id');
                $discount = $this->input->post('discount');
                $payInfo = array(
                    //'base_room_rent' => $val,
                    'rent_mode_type' => $this->input->post('modifier_type'),
                    'mod_room_rent' => $this->input->post('modifier_amount'),
                    'room_rent_total_amount' => $tot_amt,
                    'room_rent_tax_amount' => $tax_amt,
                    'room_rent_sum_total' => $tot_amt + $tax_amt,
                    'room_rent_tax_details' => $this->input->post('hid'),
                    'discount' => $this->input->post('discount'),
                        //'service_tax'=>$this->input->post('tax_type'),
                        //'luxury_tax'=>$this->input->post('tax_type'),
                        //'service_charge'=>$this->input->post('tax_type'),
                );
                // print_r($payInfo);
                // exit;
                $query = $this->dashboard_model->add_pay_info($payInfo, $booking_id);
                if ($query == true) {
                    $this->session->set_flashdata('succ_msg', "Payment Information Added Successfully!");
                    redirect('dashboard/booking_edit?b_id=' . $booking_id);
                }
                /* else
                  {
                  $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                  redirect('dashboard/booking_edit?b_id='.$booking_id);
                  } */
            } else {
                $data['page'] = 'backend/dashboard/booking_edit';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            rediect('dashboard');
        }
    }

    function add_guest_info() {

        $found = in_array("7", $this->arraay);
        if ($found) {
            $data['heading'] = '';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['active'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                //print_r($_POST);
                //exit;
                $address = $this->dashboard_model->fetch_all_address($this->input->post('g_pincode'));
                //print_r($address);
                //echo $address[0]->area_0;
                //exit;
                $booking_id = $this->input->post('booking_id');
                $guest_id = $this->input->post('guest_id');
                $charge = $this->input->post('g_charge');
                $check_in = date("Y-m-d", strtotime($this->input->post('check_in')));
                $check_out = date("Y-m-d", strtotime($this->input->post('check_out')));

                $guest = array(
                    'g_name' => $this->input->post('g_name'),
                    'g_gender' => $this->input->post('g_gender'),
                    'g_contact_no' => $this->input->post('g_contact_no'),
                    'g_pincode' => $this->input->post('g_pincode'),
                    'g_id_type' => $this->input->post('g_id_type'),
                    'g_id_number' => $this->input->post('g_id_number'),
                        //'g_city' => $address[0]->area_4,
                        //'g_state' => $address[0]->area_1,
                        //'g_country' => $address[0]->area_0
                );
                
                $preference = array(
                    'preference' => $this->input->post('preference')
                );

                $guest2 = array(
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'g_name' => $this->input->post('g_name'),
                    'g_gender' => $this->input->post('g_gender'),
                    'g_contact_no' => $this->input->post('g_contact_no'),
                    'g_pincode' => $this->input->post('g_pincode'),
                    'g_city' => $address[0]->area_4,
                    'g_state' => $address[0]->area_1,
                    'g_country' => $address[0]->area_0,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );

                //print_r( $guest2);exit;
                $extra_guest = array(
                    'booking_id' => $booking_id,
                    'g_id' => $this->input->post('guest_id'),
                    'check_in_date' => $check_in,
                    'check_out_date' => $check_out,
                    'charge_applicable' => $charge,
                    'booking_type' => '',
                );

                //print_r($extra_guest);exit;
                $query = $this->dashboard_model->add_guest_info($guest, $guest_id, $preference, $booking_id, $guest2, $extra_guest);





                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Guest Details Added Successfully!");
                    redirect('dashboard/booking_edit?b_id=' . $booking_id);
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/booking_edit?b_id=' . $booking_id);
                }
            } else {
                $data['page'] = 'backend/dashboard/booking_edit';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function add_channel() {
		$this->load->library('upload');
		$this->load->library('image_lib');

        $found = in_array("51", $this->arraay);
        if ($found) {
            $data['heading'] = 'Broker & Channel';
            $data['sub_heading'] = 'Channel';
            $data['description'] = 'Add Channel Here';
            $data['active'] = 'all_channel';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {
				
				$channel_image="";
				$channel_thumb="";
				$channel_doc="";

                //$id_b = $this->input->post('b_contact');
                /* Images Upload */
                $this->upload->initialize($this->set_upload_channel());
                if ($this->upload->do_upload('image_photo')) {
                    $channel_image = $this->upload->data('file_name');
                    $filename = $channel_image;
                    $source_path = './upload/channel/image/' . $filename;
                    $target_path = './upload/channel/image/';

                    $config_manip = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                    //$this->load->library('image_lib', $config_manip);
					$this->image_lib->initialize($config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_channel');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $channel_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                } 
				$this->upload->initialize($this->set_upload_channel_doc());
				if ($this->upload->do_upload('doc_photo')){
					$channel_doc=$this->upload->data('file_name');
					$channel_ext=explode(".", $channel_doc);
					if($channel_ext[1]=="jpg" || $channel_ext[1]=="png" || $channel_ext[1]=="gif" || $channel_ext[1]=="jpeg" ){
						$filename = $channel_doc;
						$source_path = './upload/channel/document/' . $filename;
						$target_path = './upload/channel/image/';

						$config_manip1 = array(
							//'file_name' => 'myfile',
							'image_library' => 'gd2',
							'source_image' => $source_path,
							'new_image' => $target_path,
							
						);
					   // $this->load->library('image_lib', $config_manip);
					  $this->image_lib->initialize($config_manip1);
					  if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_channel');
						
                    }
					$delete_doc= './upload/channel/document/' . $channel_doc;
						
						unlink($delete_doc);
					}
				}
                date_default_timezone_set("Asia/Kolkata");
                $channel = array(
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'channel_name' => $this->input->post('channel_name'),
                    'channel_contact_name' => $this->input->post('channel_contact_name'),
                    'channel_address' => $this->input->post('channel_address'),
                    'channel_contact' => $this->input->post('channel_contact'),
                    'channel_email' => $this->input->post('channel_email'),
                    'channel_website' => $this->input->post('channel_website'),
                    'channel_pan' => $this->input->post('channel_pan'),
                    'channel_bank_acc_no' => $this->input->post('channel_bank_acc_no'),
                    'channel_bank_ifsc' => $this->input->post('channel_bank_ifsc'),
                    'channel_commission' => $this->input->post('channel_commission'),
                    'channel_photo' => $channel_image,
                    'channel_photo_thumb' => $channel_thumb,
					'channel_doc' => $channel_doc,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
				/*echo "<pre>";
				print_r($channel);
				exit();*/
                $query = $this->dashboard_model->add_channel($channel);

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Channel Added Successfully!");
                    redirect('dashboard/all_channel');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_channel');
                }
            } else {
                $data['page'] = 'backend/dashboard/add_channel';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function all_channel() {
        $found = in_array("51", $this->arraay);
        if ($found) {
            $data['heading'] = 'Broker & Channel';
            $data['sub_heading'] = 'Channel';
            $data['description'] = 'Channel List';
            $data['active'] = 'all_channel';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $data['channel'] = $this->dashboard_model->all_channels();
            $data['page'] = 'backend/dashboard/all_channel';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function channel_payments() {

        $found = in_array("51", $this->arraay);
        if ($found) {
            $data['heading'] = 'Broker & Channel';
            $data['sub_heading'] = 'Channel';
            $data['description'] = '';
            $data['active'] = 'channel_payments';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['channel'] = $this->dashboard_model->all_channel_limit();

            if ($this->input->post()) {


                date_default_timezone_set("Asia/Kolkata");

                $commission = $this->input->post('b_amount');
                $channel_id = $this->input->post('b_id');
                $query = $this->dashboard_model->pay_channel($channel_id, $commission);

                $data = array(
                    't_amount' => $commission,
                    't_date' => date("Y-m-d H:i:s"),
                    't_payment_mode' => $this->input->post('b_payment_mode'),
                    't_bank_name' => $this->input->post('b_bank_name'),
                    'transaction_releted_t_id' => 10,
                    'transaction_from_id' => 4,
                    'transaction_to_id' => 10,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
                $query2 = $this->dashboard_model->add_transaction1($data);

                if (isset($query) && $query && $query2 && isset($query2)) {
                    $this->session->set_flashdata('succ_msg', "Channel Payed Successfully!");
                    redirect('dashboard/channel_payments');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/channel_payments');
                }
            } else {
                $data['channel'] = $this->dashboard_model->all_channel_limit();
                $data['page'] = 'backend/dashboard/channel_payments';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function channel_booking() {

        date_default_timezone_set("Asia/Kolkata");
        $commission = $_GET["channel_amount"];
        $channel_id = $_GET["channel_id"];
        $booking_id = $_GET["booking_id"];
        $query = $this->dashboard_model->update_channel_commission($channel_id, $commission);

        $data = array(
            'booking_id' => intval(substr($booking_id, -4)),
            'channel_id' => $channel_id,
            'channel_commission' => $commission,
            'booking_source' => 'Channel',
        );

        $query2 = $this->dashboard_model->update_channel_booking($data);

        if (isset($query) && $query && $query2 && isset($query2)) {
            $this->session->set_flashdata('succ_msg', "Channel Booking Added Successfully!");
            // redirect('dashboard/broker_payments');
            echo "success";
        } else {
            $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
            //redirect('dashboard/broker_payments');
            echo "failure";
        }
    }

    function edit_channel() {
		$this->load->library('upload');
		$this->load->library('image_lib');

        $found = in_array("52", $this->arraay);
        if ($found) {
            $data['heading'] = 'Broker & Channel';
            $data['sub_heading'] = 'Channel';
            $data['description'] = 'Edit Channel Here';
            $data['active'] = 'all_channel';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                date_default_timezone_set("Asia/Kolkata");
				$channel_image="";
				$channel_thumb="";
				$channel_doc="";
				$channel_id=$this->input->post('channel_id');
				$channel_data=$this->dashboard_model->get_channel_details($channel_id);
				foreach($channel_data as $c_data){
					$channel_image=$c_data->channel_photo;
					$channel_thumb=$c_data->channel_photo_thumb;
					$channel_doc=$c_data->channel_doc;
				}


                $this->upload->initialize($this->set_upload_channel());
                if ($this->upload->do_upload('image_photo')) {
                    /*  Image Upload 18.11.2015 */
                    $channel_image = $this->upload->data('file_name');
                    $filename = $channel_image;
                    $source_path = './upload/channel/image/' . $filename;
                    $target_path = './upload/channel/image/';

                    $config_manip = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                    //$this->load->library('image_lib', $config_manip);
					$this->image_lib->initialize($config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_channel');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $channel_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                    /* Images Upload  18.11.2015 */
				}
				$this->upload->initialize($this->set_upload_channel_doc());
				if ($this->upload->do_upload('doc_photo')){
					$channel_doc=$this->upload->data('file_name');
					$channel_ext=explode(".", $channel_doc);
					if($channel_ext[1]=="jpg" || $channel_ext[1]=="png" || $channel_ext[1]=="gif" || $channel_ext[1]=="jpeg" ){
						$filename = $channel_doc;
						$source_path = './upload/channel/document/' . $filename;
						$target_path = './upload/channel/image/';

						$config_manip1 = array(
							//'file_name' => 'myfile',
							'image_library' => 'gd2',
							'source_image' => $source_path,
							'new_image' => $target_path,
							
						);
					   // $this->load->library('image_lib', $config_manip);
					  $this->image_lib->initialize($config_manip1);
					  if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_channel');
						
                    }
					$delete_doc= './upload/channel/document/' . $channel_doc;
						
						unlink($delete_doc);
					}
				}
                    $channel = array(
                        'hotel_id' => $this->session->userdata('user_hotel'),
                        'channel_name' => $this->input->post('channel_name'),
                        'channel_contact_name' => $this->input->post('channel_contact_name'),
                        'channel_address' => $this->input->post('channel_address'),
                        'channel_contact' => $this->input->post('channel_contact'),
                        'channel_email' => $this->input->post('channel_email'),
                        'channel_website' => $this->input->post('channel_website'),
                        'channel_pan' => $this->input->post('channel_pan'),
                        'channel_bank_acc_no' => $this->input->post('channel_bank_acc_no'),
                        'channel_bank_ifsc' => $this->input->post('channel_bank_ifsc'),
                        'channel_commission' => $this->input->post('channel_commission'),
                        'channel_photo' => $channel_image,
                        'channel_photo_thumb' => $channel_thumb,
						'channel_doc' => $channel_doc
                    );
                


                $query = $this->dashboard_model->edit_channel($channel, $this->input->post('channel_id'));

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Channel  Update Successfully!");
                    //$data['value'] = $this->dashboard_model->get_channel_details($this->input->post('channel_id'));
                    //$data['page'] = 'backend/dashboard/edit_channel';
                    //$this->load->view('backend/common/index', $data);
                    redirect('dashboard/all_channel');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    $data['value'] = $this->dashboard_model->get_channel_details($this->input->post('channel_id'));
                    $data['page'] = 'backend/dashboard/edit_channel';
                    $this->load->view('backend/common/index', $data);
                }
            } else {
                $channel_id = $_GET['channel_id'];
                $data['channel'] = $this->dashboard_model->get_channel_details($channel_id);
                $data['page'] = 'backend/dashboard/edit_channel';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function delete_channel() {

        $found = in_array("53", $this->arraay);
        if ($found) {
            $data['heading'] = '';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $c_id = $_GET['c_id'];
            $query = $this->dashboard_model->delete_channel($c_id);

            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function add_service() {

        $found = in_array("54", $this->arraay);
        if ($found) {
            $data['heading'] = 'Service';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Service Here';
            $data['active'] = 'all_service';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                //  echo $this->input->post("rule_list_val");
                // exit();

                $rule = $this->input->post('rule_list_val');
                //echo $rule;
                //exit();

                if ($rule == '' || $rule == NULL) {
                    $guest_all = '1';
                } else {
                    $guest_all = '0';
                }


                //Image Upload Start Now

                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('s_image')) {
                    $s_image = $this->upload->data('file_name');
                    $filename = $s_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/service/';

                    $config_manip = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_service');
                    } else {
                        $thumb_name = explode(".", $filename);
                        $hotel_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                        $this->image_lib->clear();


                        $service = array(
                            's_name' => $this->input->post('s_name'),
                            's_category' => $this->input->post('s_category'),
                            's_description' => $this->input->post('s_description'),
                            's_no_member' => $this->input->post('s_no_member'),
                            's_periodicity' => $this->input->post('s_periodicity'),
                            's_image' => $hotel_thumb,
                            's_rules' => $rule,
                            's_all_guest' => $guest_all,
                            's_price' => $this->input->post('s_price'),
                            's_price_weekend' => $this->input->post('s_price_weekend'),
                            's_price_holiday' => $this->input->post('s_price_holiday'),
                            's_price_special' => $this->input->post('s_price_special'),
                            's_tax_applied' => $this->input->post('s_tax_applied'),
                            's_tax' => $this->input->post('s_tax'),
                            's_discount_applied' => $this->input->post('s_discount_applied'),
                            's_discount' => $this->input->post('s_discount'),
                            'hotel_id' => $this->session->userdata('user_hotel'),
                            'user_id' => $this->session->userdata('user_id'),
                            'hotel_id' => $this->session->userdata('user_hotel'),
                            'user_id' => $this->session->userdata('user_id')
                        );
                    }

                    $query = $this->dashboard_model->insert_service($service);
                    if ($query) {
                        $this->session->set_flashdata('succ_msg', 'Service Created Sucessfully');
                    } else {
                        $this->session->set_flashdata('err_msg', 'Database Error');
                    }
                    $data['page'] = 'backend/dashboard/add_service';
                    $this->load->view('backend/common/index', $data);
                }

                //Image Upload Section Closed
                else {
                    $service = array(
                        's_name' => $this->input->post('s_name'),
                        's_category' => $this->input->post('s_category'),
                        's_description' => $this->input->post('s_description'),
                        's_no_member' => $this->input->post('s_no_member'),
                        's_periodicity' => $this->input->post('s_periodicity'),
                        's_rules' => $rule,
                        's_price' => $this->input->post('s_price'),
                        's_price_weekend' => $this->input->post('s_price_weekend'),
                        's_price_holiday' => $this->input->post('s_price_holiday'),
                        's_price_special' => $this->input->post('s_price_special'),
                        's_tax_applied' => $this->input->post('s_tax_applied'),
                        's_tax' => $this->input->post('s_tax'),
                        's_discount_applied' => $this->input->post('s_discount_applied'),
                        's_discount' => $this->input->post('s_discount'),
                        'hotel_id' => $this->session->userdata('user_hotel'),
                        'user_id' => $this->session->userdata('user_id')
                    );
                    $query = $this->dashboard_model->insert_service($service);
                    if ($query) {
                        $this->session->set_flashdata('succ_msg', 'Service Created Sucessfully');
                    } else {
                        $this->session->set_flashdata('err_msg', 'Database Error');
                    }
                    $data['page'] = 'backend/dashboard/add_service';
                    $this->load->view('backend/common/index', $data);
                }

                $data['page'] = 'backend/dashboard/add_service';
                $this->load->view('backend/common/index', $data);
            } else {
                $data['page'] = 'backend/dashboard/add_service';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function add_rule() {

        //echo $this->input->post();
        $data = array(
            /* General Rule */
            'r_name' => $this->input->post('r_name'),
            'r_apply_person' => $this->input->post('r_apply_person'),
            'r_apply_hotel' => $this->input->post('r_apply_hotel'),
            /* Guest Rule */
            'r_guest_name' => $this->input->post('r_guest_name'),
            'r_guest_gender' => $this->input->post('r_guest_gender'),
            'r_guest_age' => $this->input->post('r_guest_age'),
            'r_guest_age_op' => $this->input->post('r_guest_age_op'),
            'r_guest_type' => $this->input->post('r_guest_type'),
            'r_guest_state' => $this->input->post('r_guest_state'),
            'r_guest_country' => $this->input->post('r_guest_country'),
            'r_guest_vip' => $this->input->post('r_guest_vip'),
            'r_guest_visit_count' => $this->input->post('r_guest_visit_count'),
            'r_guest_visit_count_op' => $this->input->post('r_guest_visit_count_op'),
            'r_guest_spent' => $this->input->post('r_guest_spent'),
            'r_guest_spent_op' => $this->input->post('r_guest_spent_op'),
            'r_guest_preffered' => $this->input->post('r_guest_preffered'),
            'r_guest_new' => $this->input->post('r_guest_new'),
            /* Room and Booking Rule */
            'r_room_no' => $this->input->post('r_room_no'),
            'r_room_bed' => $this->input->post('r_room_bed'),
            'r_room_floor' => $this->input->post('r_room_floor'),
            'r_room_category' => $this->input->post('r_room_category'),
            'r_room_bed' => $this->input->post('r_room_bed'),
            'r_booking_from' => $this->input->post('r_booking_from'),
            'r_booking_to' => $this->input->post('r_booking_to'),
            'r_booking_no_guest' => $this->input->post('r_booking_no_guest'),
            'r_booking_no_guest_op' => $this->input->post('r_booking_no_guest_op'),
            'r_booking_nature_visit' => $this->input->post('r_booking_nature_visit'),
            'r_booking_source' => $this->input->post('r_booking_source'),
            'r_booking_stay_days' => $this->input->post('r_booking_stay_days'),
            'r_booking_stay_days_op' => $this->input->post('r_booking_stay_days_op'),
            'r_booking_total' => $this->input->post('r_booking_total'),
            'r_booking_total_op' => $this->input->post('r_booking_total_op'),
            'hotel_id' => $this->session->userdata('user_hotel'),
            'user_id' => $this->session->userdata('user_id')
        );
        /* $data=array(
          'r_name' =>'Test',

          ); */
        $query = $this->dashboard_model->rule_create($data);
       
    }

    function all_service() {

        $found = in_array("54", $this->arraay);
        if ($found) {
            $data['heading'] = 'Service';
            $data['sub_heading'] = '';
            $data['description'] = 'Service List';
            $data['active'] = 'all_service';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['service_list'] = $this->dashboard_model->all_service_result();

            $data['page'] = 'backend/dashboard/all_service';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function all_laundry_service() {

        $found = in_array("54", $this->arraay);
        if ($found) {
            $data['heading'] = 'Service';
            $data['sub_heading'] = 'Laundry Services';
            $data['description'] = 'Laundry Services';
            $data['active'] = 'all_laundry_service';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['laundry_service'] = $this->dashboard_model->all_laundry_service();

            $data['page'] = 'backend/dashboard/all_laundry_service';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function add_asset() {
				$this->load->library('upload');
				$this->load->library('image_lib');
        $found = in_array("57", $this->arraay);
        if ($found) {
            $data['heading'] = 'Assets';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Asset Here';
            $data['active'] = 'all_assets';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            date_default_timezone_set("Asia/Kolkata");

            if ($this->input->post()) {
				 
                // $this->upload->initialize($this->set_upload_options());

					
               $this->upload->initialize($this->set_upload_optionsAsset());
               
                if ($this->upload->do_upload('a_asset_image')) {
					 
                    $asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/asset/originals/asset_image/' . $filename;
                    $target_path = './upload/asset/thumbnail/asset_image/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                    //$this->load->library('image_lib', $config_manip);
					$this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_asset');
                    }
                    $thumb_name = explode(".", $filename);
                    $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                } else {
					 $thumb_name = '';
                     $asset_thumb='';
					 $asset_image='';
					
                  
                }

               // $this->upload->initialize($this->set_upload_options());
			   $this->upload->initialize($this->set_upload_optionsA_Bill1());
               if ($this->upload->do_upload('a_proc_bill_1_imag')) {
						
                    $procurement_bill_1_image = $this->upload->data('file_name');
                    $filename1 = $procurement_bill_1_image; //exit;
                    $source_path1 = './upload/asset/originals/Proc_bill_1/' . $filename1;
                    $target_path1 = './upload/asset/thumbnail/Proc_bill_1/';
                    $config_manip_bill_1 = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path1,
                        'new_image' => $target_path1,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 100,
                        'height' => 100
                    );
                 //   $this->load->library('image_lib', $config_manip_bill_1);
					$this->image_lib->initialize($config_manip_bill_1);
					//$this->image_lib->resize();
                    if (!$this->image_lib->resize()) {
						//echo "bill 1";exit;
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/all_assets');
                    }
                    $thumb_name1 = explode(".", $filename1);
                    $procurement_bill_1_thumb = $thumb_name1[0] . $config_manip_bill_1['thumb_marker'] . "." . $thumb_name1[1];
                    $this->image_lib->clear();
					
					
					
                }else {
					 $thumb_name1 = '';
                     $procurement_bill_1_thumb='';
					 $procurement_bill_1_image='';
					
                  //  $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    //redirect('dashboard/add_asset');
                }

				 $this->upload->initialize($this->set_upload_optionsA_Bill2());	
                if ($this->upload->do_upload('a_proc_bill_2_imag')) {
					
					  //$this->image_lib->clear();
                    //$this->upload->initialize($this->set_upload_options());
                   
				    $procurement_bill_2_image = $this->upload->data('file_name');
                    $filename1 = $procurement_bill_2_image; //exit;
                    $source_path1 = './upload/asset/originals/Proc_bill_2/' . $filename1;
                    $target_path1 = './upload/asset/thumbnail/Proc_bill_2/';
                    $config_manip_bill_1 = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path1,
                        'new_image' => $target_path1,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 100,
                        'height' => 100
                    );
                 //   $this->load->library('image_lib', $config_manip_bill_1);
					$this->image_lib->initialize($config_manip_bill_1);
					//$this->image_lib->resize();
                    if (!$this->image_lib->resize()) {
						//echo "bill 1";exit;
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/all_assets');
                    }
                    $thumb_name1 = explode(".", $filename1);
                    $procurement_bill_2_thumb = $thumb_name1[0] . $config_manip_bill_1['thumb_marker'] . "." . $thumb_name1[1];
                    $this->image_lib->clear();
				   
				   /* $procurement_bill_2_image = $this->upload->data('file_name');
                 $filename2 = $procurement_bill_2_image;
                    $source_path2 = './upload/asset/originals/Proc_bill_2/' . $filename2;
                    $target_path2 = './upload/asset/thumbnail/Proc_bill_2/';
					
                    $config_manip_bill_2 = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path2,
                        'new_image' => $target_path2,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 100,
                        'height' => 100
                    );
					$this->image_lib->initialize($config_manip_bill_2);
					//print_r($config_manip_bill_2); exit;
                   $thumb_name2 = explode(".", $filename2);
                    $procurement_bill_2_thumb = $thumb_name2[0] . $config_manip_bill_2['thumb_marker'] . "." . $thumb_name2[1];
                    $this->image_lib->clear();*/
					 
			   } 
				  // echo "bill 2";exit;
                   else {
					 $thumb_name2 = '';
                     $procurement_bill_2_thumb='';
                     $procurement_bill_2_image='';
					
                  //  $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                   
                }


                $asset = array(
                    'a_hotel_id' => $this->session->userdata('user_hotel'),
                    'a_type' => $this->input->post('a_type'),
                    'a_category' => $this->input->post('a_category'),
                    'a_name' => $this->input->post('a_name'),
                    'a_first_hand' => $this->input->post('a_first_hand'),
                    'a_bought_date' => date("Y-m-d", strtotime($this->input->post('a_bought_date'))),
                    'a_description' => $this->input->post('a_description'),
                    'a_reg_number' => $this->input->post('a_reg_number'),
                    'a_purchased_from' => $this->input->post('a_purchased_from'),
                    'a_seller_contact_no' => $this->input->post('a_seller_contact_no'),
                    'a_service_contact_no' => $this->input->post('a_service_contact_no'),
                    'a_incharge' => $this->input->post('a_incharge'),
                    'a_cost' => $this->input->post('a_cost'),
                    'a_annual_depreciation' => $this->input->post('a_annual_depreciation'),
                    'a_amc' => $this->input->post('a_amc'),
                    'a_amc_agency_name' => $this->input->post('a_amc_agency_name'),
                    'a_amc_reg_contact_no' => $this->input->post('a_amc_reg_contact_no'),
                    'a_amc_renewal_date' => date("Y-m-d", strtotime($this->input->post('a_amc_renewal_date'))),
                    'a_amc_charge' => $this->input->post('a_amc_charge'),
                    'a_decomission_date' => date("Y-m-d", strtotime($this->input->post('a_decomission_date'))),
                    'a_asset_image' => $asset_image,
                    'a_asset_image_thumb' => $asset_thumb,
                    'a_proc_bill_1_imag' => $procurement_bill_1_image,
                    'a_proc_bill_1_imag_thumb' => $procurement_bill_1_thumb,
                    'a_proc_bill_2_imag' => $procurement_bill_2_image,
                    'a_proc_bill_2_imag_thumb' => $procurement_bill_2_thumb,
                    'hotel_id' => $this->session->userdata('user_hotel')
                        //'user_id'=>$this->session->userdata('user_id')
                );
					//print_r($assets); exit;


                $query = $this->dashboard_model->add_asset($asset);
                if ($query) {
                    $this->session->set_flashdata('succ_msg', "Asset Added Successfully!");
                    $data['asset_type'] = $this->dashboard_model->all_assets_types();
                    $data['page'] = 'backend/dashboard/add_asset';
                    $this->load->view('backend/common/index', $data);
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    $data['asset_type'] = $this->dashboard_model->all_assets_types();
                    $data['page'] = 'backend/dashboard/add_asset';
                    $this->load->view('backend/common/index', $data);
                }
            } else {
                $data['asset_type'] = $this->dashboard_model->all_assets_types();
                $data['page'] = 'backend/dashboard/add_asset';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function all_assets() {

        $found = in_array("57", $this->arraay);
        if ($found) {
            $data['heading'] = 'Assets';
            $data['sub_heading'] = 'All Assets';
            $data['description'] = 'Assets List';
            $data['active'] = 'all_assets';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $data['assets'] = $this->dashboard_model->all_assets_limit();
            $data['page'] = 'backend/dashboard/all_assets';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function Edit_asset() {
        $data['heading'] = 'Assets';
        $data['sub_heading'] = '';
        $data['description'] = 'Edit Assets';
        $data['active'] = 'all_assets';
        $id = $_GET['a_id'];
        $data['assets'] = $this->dashboard_model->fetch_assets_by_id($id);
        $data['type'] = $this->dashboard_model->all_assets_types();
        $data['category'] = $this->dashboard_model->all_assets_category();
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/edit_asset';
        $this->load->view('backend/common/index', $data);
    }

    function view_asset() {
        $id = $_GET['a_id'];
        $data['heading'] = 'Assets';
        $data['sub_heading'] = '';
        $data['description'] = 'Edit Assets';
        $data['active'] = 'all_assets';
        $data['assets'] = $this->dashboard_model->fetch_assets_by_id($id);
        //$data['type']=$this->dashboard_model->all_assets_types();
        //$data['category']=$this->dashboard_model->all_assets_category();
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        $data['page'] = 'backend/dashboard/view_asset';
        $this->load->view('backend/common/index', $data);
    }

    function edit_asset1() {
		$this->load->library('upload');
		$this->load->library('image_lib');
        $found = in_array("58", $this->arraay);
        if ($found) {
            $data['heading'] = 'Assets';
            $data['sub_heading'] = 'Edit Asset';
            $data['description'] = 'Edit Asset Here';
            $data['active'] = 'edit_asset';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            date_default_timezone_set("Asia/Kolkata");

            if ($this->input->post()) {
				
                // $this->upload->initialize($this->set_upload_options());


               $this->upload->initialize($this->set_upload_optionsAsset());
                if (($this->upload->do_upload('a_asset_image')) && ($this->upload->do_upload('a_proc_bill_1_imag')) && ($this->upload->do_upload('a_proc_bill_2_imag'))) {
				//	echo "asset"; exit;
				/*image upload 1 start*/
					
                    if ($this->upload->do_upload('a_asset_image')){
					$asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/asset/originals/asset_image/' . $filename;
                    $target_path = './upload/asset/thumbnail/asset_image/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                  
					$this->image_lib->initialize($config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_asset');
                    }
                    $thumb_name = explode(".", $filename);
                    $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					}
		/*Image upload 1 end*/
             $this->upload->initialize($this->set_upload_optionsA_Bill1());
                if ($this->upload->do_upload('a_proc_bill_1_imag')) {
						
                    $procurement_bill_1_image = $this->upload->data('file_name');
                    $filename1 = $procurement_bill_1_image; //exit;
                    $source_path1 = './upload/asset/originals/Proc_bill_1/' . $filename1;
                    $target_path1 = './upload/asset/thumbnail/Proc_bill_1/';
                    $config_manip_bill_1 = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path1,
                        'new_image' => $target_path1,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 100,
                        'height' => 100
                    );
                 //   $this->load->library('image_lib', $config_manip_bill_1);
					$this->image_lib->initialize($config_manip_bill_1);

                    if (!$this->image_lib->resize()) {
					//	echo "bill 1";exit;
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/all_assets');
                    }
                    $thumb_name = explode(".", $filename);
                    $procurement_bill_1_thumb = $thumb_name[0] . $config_manip_bill_1['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                }
				  $this->upload->initialize($this->set_upload_optionsA_Bill2());
                if ($this->upload->do_upload('a_proc_bill_2_imag')) {
					
                    //$this->upload->initialize($this->set_upload_options());
                   $procurement_bill_2_image = $this->upload->data('file_name');
                    $filename1 = $procurement_bill_2_image; //exit;
                    $source_path1 = './upload/asset/originals/Proc_bill_2/' . $filename1;
                    $target_path1 = './upload/asset/thumbnail/Proc_bill_2/';
                    $config_manip_bill_1 = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path1,
                        'new_image' => $target_path1,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 100,
                        'height' => 100
                    );
                 //   $this->load->library('image_lib', $config_manip_bill_1);
					$this->image_lib->initialize($config_manip_bill_1);
					
					
					
					


                    $thumb_name = explode(".", $filename);
                    $procurement_bill_2_thumb = $thumb_name[0] . $config_manip_bill_1['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					 
			   } 


                $asset = array(
                    'a_id' => $this->input->post('a_id'),
                    'a_hotel_id' => $this->session->userdata('user_hotel'),
                    'a_type' => $this->input->post('a_type'),
                    'a_category' => $this->input->post('a_category'),
                    'a_name' => $this->input->post('a_name'),
                    'a_first_hand' => $this->input->post('a_first_hand'),
                    'a_bought_date' => date("Y-m-d", strtotime($this->input->post('a_bought_date'))),
                    'a_description' => $this->input->post('a_description'),
                    'a_reg_number' => $this->input->post('a_reg_number'),
                    'a_purchased_from' => $this->input->post('a_purchased_from'),
                    'a_seller_contact_no' => $this->input->post('a_seller_contact_no'),
                    'a_service_contact_no' => $this->input->post('a_service_contact_no'),
                    'a_incharge' => $this->input->post('a_incharge'),
                    'a_cost' => $this->input->post('a_cost'),
                    'a_annual_depreciation' => $this->input->post('a_annual_depreciation'),
                    'a_amc' => $this->input->post('a_amc'),
                    'a_amc_agency_name' => $this->input->post('a_amc_agency_name'),
                    'a_amc_reg_contact_no' => $this->input->post('a_amc_reg_contact_no'),
                    'a_amc_renewal_date' => date("Y-m-d", strtotime($this->input->post('a_amc_renewal_date'))),
                    'a_amc_charge' => $this->input->post('a_amc_charge'),
                    'a_decomission_date' => date("Y-m-d", strtotime($this->input->post('a_decomission_date'))),
                    'a_asset_image' => $asset_image,
                    'a_asset_image_thumb' => $asset_thumb,
                    'a_proc_bill_1_imag' => $procurement_bill_1_image,
                    'a_proc_bill_1_imag_thumb' => $procurement_bill_1_thumb,
                    'a_proc_bill_2_imag' => $procurement_bill_2_image,
                    'a_proc_bill_2_imag_thumb' => $procurement_bill_2_thumb,
                );
				} elseif($this->upload->do_upload('a_asset_image')) {
					$this->upload->initialize($this->set_upload_optionsAsset());
					if($this->upload->do_upload('a_asset_image')){
					$asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/asset/originals/asset_image/' . $filename;
                    $target_path = './upload/asset/thumbnail/asset_image/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                  
					$this->image_lib->initialize($config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_asset');
                    }
                    $thumb_name = explode(".", $filename);
                    $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					}
				$asset = array(
                    'a_id' => $this->input->post('a_id'),
                    'a_hotel_id' => $this->session->userdata('user_hotel'),
                    'a_type' => $this->input->post('a_type'),
                    'a_category' => $this->input->post('a_category'),
                    'a_name' => $this->input->post('a_name'),
                    'a_first_hand' => $this->input->post('a_first_hand'),
                    'a_bought_date' => date("Y-m-d", strtotime($this->input->post('a_bought_date'))),
                    'a_description' => $this->input->post('a_description'),
                    'a_reg_number' => $this->input->post('a_reg_number'),
                    'a_purchased_from' => $this->input->post('a_purchased_from'),
                    'a_seller_contact_no' => $this->input->post('a_seller_contact_no'),
                    'a_service_contact_no' => $this->input->post('a_service_contact_no'),
                    'a_incharge' => $this->input->post('a_incharge'),
                    'a_cost' => $this->input->post('a_cost'),
                    'a_annual_depreciation' => $this->input->post('a_annual_depreciation'),
                    'a_amc' => $this->input->post('a_amc'),
                    'a_amc_agency_name' => $this->input->post('a_amc_agency_name'),
                    'a_amc_reg_contact_no' => $this->input->post('a_amc_reg_contact_no'),
                    'a_amc_renewal_date' => date("Y-m-d", strtotime($this->input->post('a_amc_renewal_date'))),
                    'a_amc_charge' => $this->input->post('a_amc_charge'),
                    'a_decomission_date' => date("Y-m-d", strtotime($this->input->post('a_decomission_date'))),
                    'a_asset_image' => $asset_image,
                    'a_asset_image_thumb' => $asset_thumb,
                   // 'a_proc_bill_1_imag' => $procurement_bill_1_image,
                   // 'a_proc_bill_1_imag_thumb' => $procurement_bill_1_thumb,
                   // 'a_proc_bill_2_imag' => $procurement_bill_2_image,
                   // 'a_proc_bill_2_imag_thumb' => $procurement_bill_2_thumb,
                );
					
				} elseif ($this->upload->do_upload('a_proc_bill_1_imag')){
					
					
					$this->upload->initialize($this->set_upload_optionsA_Bill1());
					if ($this->upload->do_upload('a_proc_bill_1_imag')){
					$procurement_bill_1_image = $this->upload->data('file_name');
                    $filename1 = $procurement_bill_1_image; //exit;
                    $source_path1 = './upload/asset/originals/Proc_bill_1/' . $filename1;
                    $target_path1 = './upload/asset/thumbnail/Proc_bill_1/';
                    $config_manip_bill_1 = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path1,
                        'new_image' => $target_path1,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 100,
                        'height' => 100
                    );
                 //   $this->load->library('image_lib', $config_manip_bill_1);
					$this->image_lib->initialize($config_manip_bill_1);

                    if (!$this->image_lib->resize()) {
						//echo "bill 1";exit;
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/all_assets');
                    }
                    $thumb_name = explode(".", $filename);
                    $procurement_bill_1_thumb = $thumb_name[0] . $config_manip_bill_1['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					}
					$asset = array(
                    'a_id' => $this->input->post('a_id'),
                    'a_hotel_id' => $this->session->userdata('user_hotel'),
                    'a_type' => $this->input->post('a_type'),
                    'a_category' => $this->input->post('a_category'),
                    'a_name' => $this->input->post('a_name'),
                    'a_first_hand' => $this->input->post('a_first_hand'),
                    'a_bought_date' => date("Y-m-d", strtotime($this->input->post('a_bought_date'))),
                    'a_description' => $this->input->post('a_description'),
                    'a_reg_number' => $this->input->post('a_reg_number'),
                    'a_purchased_from' => $this->input->post('a_purchased_from'),
                    'a_seller_contact_no' => $this->input->post('a_seller_contact_no'),
                    'a_service_contact_no' => $this->input->post('a_service_contact_no'),
                    'a_incharge' => $this->input->post('a_incharge'),
                    'a_cost' => $this->input->post('a_cost'),
                    'a_annual_depreciation' => $this->input->post('a_annual_depreciation'),
                    'a_amc' => $this->input->post('a_amc'),
                    'a_amc_agency_name' => $this->input->post('a_amc_agency_name'),
                    'a_amc_reg_contact_no' => $this->input->post('a_amc_reg_contact_no'),
                    'a_amc_renewal_date' => date("Y-m-d", strtotime($this->input->post('a_amc_renewal_date'))),
                    'a_amc_charge' => $this->input->post('a_amc_charge'),
                    'a_decomission_date' => date("Y-m-d", strtotime($this->input->post('a_decomission_date'))),
                 //   'a_asset_image' => $asset_image,
                  //  'a_asset_image_thumb' => $asset_thumb,
                    'a_proc_bill_1_imag' => $procurement_bill_1_image,
                    'a_proc_bill_1_imag_thumb' => $procurement_bill_1_thumb,
                   // 'a_proc_bill_2_imag' => $procurement_bill_2_image,
                   // 'a_proc_bill_2_imag_thumb' => $procurement_bill_2_thumb,
                );
				} elseif ($this->upload->do_upload('a_proc_bill_2_imag')){
					
					 $this->upload->initialize($this->set_upload_optionsA_Bill2());
					 if ($this->upload->do_upload('a_proc_bill_2_imag')){
					 $procurement_bill_2_image = $this->upload->data('file_name');
                    $filename1 = $procurement_bill_2_image; //exit;
                    $source_path1 = './upload/asset/originals/Proc_bill_2/' . $filename1;
                    $target_path1 = './upload/asset/thumbnail/Proc_bill_2/';
                    $config_manip_bill_1 = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path1,
                        'new_image' => $target_path1,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 100,
                        'height' => 100
                    );
                 //   $this->load->library('image_lib', $config_manip_bill_1);
					$this->image_lib->initialize($config_manip_bill_1);
					
			

                    $thumb_name = explode(".", $filename);
                    $procurement_bill_2_thumb = $thumb_name[0] . $config_manip_bill_1['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					 }
					$asset = array(
                    'a_id' => $this->input->post('a_id'),
                    'a_hotel_id' => $this->session->userdata('user_hotel'),
                    'a_type' => $this->input->post('a_type'),
                    'a_category' => $this->input->post('a_category'),
                    'a_name' => $this->input->post('a_name'),
                    'a_first_hand' => $this->input->post('a_first_hand'),
                    'a_bought_date' => date("Y-m-d", strtotime($this->input->post('a_bought_date'))),
                    'a_description' => $this->input->post('a_description'),
                    'a_reg_number' => $this->input->post('a_reg_number'),
                    'a_purchased_from' => $this->input->post('a_purchased_from'),
                    'a_seller_contact_no' => $this->input->post('a_seller_contact_no'),
                    'a_service_contact_no' => $this->input->post('a_service_contact_no'),
                    'a_incharge' => $this->input->post('a_incharge'),
                    'a_cost' => $this->input->post('a_cost'),
                    'a_annual_depreciation' => $this->input->post('a_annual_depreciation'),
                    'a_amc' => $this->input->post('a_amc'),
                    'a_amc_agency_name' => $this->input->post('a_amc_agency_name'),
                    'a_amc_reg_contact_no' => $this->input->post('a_amc_reg_contact_no'),
                    'a_amc_renewal_date' => date("Y-m-d", strtotime($this->input->post('a_amc_renewal_date'))),
                    'a_amc_charge' => $this->input->post('a_amc_charge'),
                    'a_decomission_date' => date("Y-m-d", strtotime($this->input->post('a_decomission_date'))),
                 //   'a_asset_image' => $asset_image,
                  //  'a_asset_image_thumb' => $asset_thumb,
                   // 'a_proc_bill_1_imag' => $procurement_bill_1_image,
                   // 'a_proc_bill_1_imag_thumb' => $procurement_bill_1_thumb,
                    'a_proc_bill_2_imag' => $procurement_bill_2_image,
                    'a_proc_bill_2_imag_thumb' => $procurement_bill_2_thumb,
                );
					
				} else {
					
					$asset = array(
                    'a_id' => $this->input->post('a_id'),
                    'a_hotel_id' => $this->session->userdata('user_hotel'),
                    'a_type' => $this->input->post('a_type'),
                    'a_category' => $this->input->post('a_category'),
                    'a_name' => $this->input->post('a_name'),
                    'a_first_hand' => $this->input->post('a_first_hand'),
                    'a_bought_date' => date("Y-m-d", strtotime($this->input->post('a_bought_date'))),
                    'a_description' => $this->input->post('a_description'),
                    'a_reg_number' => $this->input->post('a_reg_number'),
                    'a_purchased_from' => $this->input->post('a_purchased_from'),
                    'a_seller_contact_no' => $this->input->post('a_seller_contact_no'),
                    'a_service_contact_no' => $this->input->post('a_service_contact_no'),
                    'a_incharge' => $this->input->post('a_incharge'),
                    'a_cost' => $this->input->post('a_cost'),
                    'a_annual_depreciation' => $this->input->post('a_annual_depreciation'),
                    'a_amc' => $this->input->post('a_amc'),
                    'a_amc_agency_name' => $this->input->post('a_amc_agency_name'),
                    'a_amc_reg_contact_no' => $this->input->post('a_amc_reg_contact_no'),
                    'a_amc_renewal_date' => date("Y-m-d", strtotime($this->input->post('a_amc_renewal_date'))),
                    'a_amc_charge' => $this->input->post('a_amc_charge'),
                    'a_decomission_date' => date("Y-m-d", strtotime($this->input->post('a_decomission_date'))),
                 //   'a_asset_image' => $asset_image,
                  //  'a_asset_image_thumb' => $asset_thumb,
                   // 'a_proc_bill_1_imag' => $procurement_bill_1_image,
                   // 'a_proc_bill_1_imag_thumb' => $procurement_bill_1_thumb,
                    //'a_proc_bill_2_imag' => $procurement_bill_2_image,
                   // 'a_proc_bill_2_imag_thumb' => $procurement_bill_2_thumb,
                );
				}
				
				

                $query = $this->dashboard_model->update_asset($asset);
                if ($query) {
                    $this->session->set_flashdata('succ_msg', "Asset Updated Successfully!");
                    redirect('dashboard/all_assets');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/all_assets');
                }
            } else {
                $data['asset_type'] = $this->dashboard_model->all_assets_types();
                $data['page'] = 'backend/dashboard/all_assets';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function edit_asset_stock() {


        $found = in_array("57", $this->arraay);
        if ($found) {

            $id = $_GET['a_id'];
            $data['heading'] = 'Stock Inventory';
            $data['sub_heading'] = '';
            $data['description'] = 'Edit Stock & Inventory';
            $data['active'] = 'all_stock_invent';
            $data['assets'] = $this->dashboard_model->fetch_asset_by_id($id);
            $data['type'] = $this->dashboard_model->all_assets_types();
            $data['category'] = $this->dashboard_model->all_stock_invent_cat();
            //echo "<pre>";
            //print_r($data);
            //exit;
            $data['warehouse'] = $this->dashboard_model->all_warehouse();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/edit_stock_inventory';
            $this->load->view('backend/common/index', $data);
        } else {
            rediect('dashboard');
        }
    }

    function asset_type() {


        $found = in_array("57", $this->arraay);
        if ($found) {
            $data['heading'] = 'Assets';
            $data['sub_heading'] = 'All Assets';
            $data['description'] = 'Assets List';
            $data['active'] = 'asset_type';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('asset_type')) {

                    $asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/asset/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/asset_type');
                    }
                    $thumb_name = explode(".", $filename);
                    $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                } else {
                    //$this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    //redirect('dashboard/asset_type');
                    $source_path = './upload/' . 'asdf';
                    $target_path = './upload/asset/';
                    $asset_thumb = "no_images.png";
                }





                $asset_type = array(
                    'asset_type_name' => $this->input->post('asset_type_name'),
                    'asset_type_description' => $this->input->post('asset_type_description'),
                    'asset_icon' => $asset_thumb,
                    'hotel_id' => $this->session->userdata('user_hotel')
                );

                //print_r($asset_type);exit;

                $query = $this->dashboard_model->add_asset_type($asset_type);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Asset Type Added Successfully!");
                    redirect('dashboard/asset_type');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/asset_type');
                }
            }

            $data['asset'] = $this->dashboard_model->all_assets_tasks();

            //print_r($data['asset']);exit;
            $data['page'] = 'backend/dashboard/assets_type';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function edit_Atype() {
        $data['master_log'] = $this->dashboard_model->get_master_fuel_stock();
        $id = $_GET['a_id'];
        $data['rating'] = $this->dashboard_model->get_dg_rating();
        ///$query=$this->dashboard_model->fetch_dgset($id);

        $data['type'] = $this->dashboard_model->fetch_type_by_id($id);
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/edit_a_type';

        $this->load->view('backend/common/index', $data);
    }

    function update_Atype() {



        $found = in_array("57", $this->arraay);
        if ($found) {
            $data['heading'] = 'Assets';
            $data['sub_heading'] = 'All Assets';
            $data['description'] = 'Assets List';
            $data['active'] = 'asset_type';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            //$data['type']=$this->dashboard_model->fetch_type_by_id($id);
            if ($this->input->post()) {
                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('asset_type1')) {

                    $asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/asset/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/asset_type');
                    }
                    $thumb_name = explode(".", $filename);
                    $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                } else {
                    //$this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    //redirect('dashboard/asset_type');
                    $target_path = './upload/section/';
                    $asset_thumb = 'no_images.png';
                }



                $asset_type = array(
                    'asset_type_id' => $this->input->post('hid1'),
                    'asset_type_name' => $this->input->post('unit_name1'),
                    'asset_type_description' => $this->input->post('asetdesc1'),
                    'asset_icon' => $asset_thumb
                );

                //print_r($asset_type);
                //exit;
                $query = $this->dashboard_model->update_asset_type($asset_type);


                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Asset Type Added Successfully!");
                    redirect('dashboard/asset_type');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/asset_type');
                }
            }

            $data['asset'] = $this->dashboard_model->all_assets_tasks();
            $data['page'] = 'backend/dashboard/edit_a_type';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function delete_a_type() {
        $id = $_GET['a_id'];
        $details = $this->dashboard_model->delete_a_type($id);
        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function def_stock_invent() {

        $found = in_array("60", $this->arraay);
        if ($found) {
            $data['heading'] = 'Stock Inventory';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Asset Here';
            $data['active'] = 'all_stock_invent';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['assets'] = $this->dashboard_model->all_assets_limit();
            $data['type'] = $this->dashboard_model->all_assets_types();
            $data['warehouse'] = $this->dashboard_model->all_warehouse();
            $data['category'] = $this->dashboard_model->all_stock_invent_cat();
            //$data['category']=$this->dashboard_model->all_assets_category();
            date_default_timezone_set("Asia/Kolkata");

            if ($this->input->post()) {


                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('a_image')) {

                    $asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/asset/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250,
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/all_stock_invent');
                    }
                    $thumb_name = explode(".", $filename);
                    $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                } else {
                    //$asset_thumb ='empty';
                    //$this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    //redirect('dashboard/def_stock_invent');
                    $source_path = './upload/' . 'asdf';
                    $target_path = './upload/asset/';
                    $asset_thumb = "no_images.png";
                }


                $asset = array(
                    //'a_hotel_id' => $this->session->userdata('user_hotel'),
                    'date' => date("Y/m/d"),
                    'item_type' => $this->input->post('a_type'),
                    'a_category' => $this->input->post('a_category'),
                    'a_name' => $this->input->post('a_name'),
                    'make' => $this->input->post('make'),
                    'item_des' => $this->input->post('i_des'),
                    'u_price' => $this->input->post('u_price'),
                    'selling_price' => $this->input->post('s_price'),
                    'unit' => $this->input->post('unit'),
                    'note' => $this->input->post('note'),
                    'importance' => $this->input->post('importance'),
                    'qty' => "0",
                    'opening_qty' => "0",
                    'closing_qty' => "0",
                    'warehouse' => $this->input->post('warehouse'),
                    'l_stock' => $this->input->post('l_stock'),
                    'a_image' => $asset_thumb,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );

                //print_r($asset);
                //exit;

                $query = $this->dashboard_model->add_stock_invent($asset);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Asset Added Successfully!");
                    redirect('dashboard/all_stock_invent');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    $data['asset_type'] = $this->dashboard_model->all_assets_types();
                    $data['page'] = 'backend/dashboard/def_stock_invent';
                    $this->load->view('backend/common/index', $data);
                }
            } else {
                $data['warehouse'] = $this->dashboard_model->all_warehouse();
                $data['category'] = $this->dashboard_model->all_stock_invent_cat();
                $data['asset_type'] = $this->dashboard_model->all_assets_types();
                $data['page'] = 'backend/dashboard/def_stock_invent';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function update_asset() {
		$this->load->library('upload');
		$this->load->library('image_lib');
        $found = in_array("60", $this->arraay);
        if ($found) {
            $data['heading'] = 'Assets';
            $data['sub_heading'] = 'Add Asset';
            $data['description'] = 'Add Asset Here';
            $data['active'] = 'add_asset';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['assets'] = $this->dashboard_model->all_assets_limit();
            $data['type'] = $this->dashboard_model->all_assets_types();
            $data['category'] = $this->dashboard_model->all_assets_category();
            $data['warehouse'] = $this->dashboard_model->all_warehouse();
            date_default_timezone_set("Asia/Kolkata");

            if ($this->input->post()) {
                $mop = $this->input->post('mop');
                $checkno = $this->input->post('checkno');
                $check_bank_name = $this->input->post('check_bank_name');
                $draft_no = $this->input->post('draft_no');
                $draft_bank_name = $this->input->post('draft_bank_name');
                $ft_bank_name = $this->input->post('ft_bank_name');
                $ft_account_no = $this->input->post('ft_account_no');
                $ft_ifsc_code = $this->input->post('ft_ifsc_code');
			
                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('a_image')) {

                    $asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/asset/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                  //  $this->load->library('image_lib', $config_manip);
					$this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/edit_stock_invent');
                    }
                    $thumb_name = explode(".", $filename);
                    $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					
					 $asset = array(
                    'hotel_stock_inventory_id' => $this->input->post('id'),
                    //'stock_inventory_id'=>$this->input->post('stock_inventory_id'),
                    //'a_hotel_id' => $this->session->userdata('user_hotel'),
                    'item_type' => $this->input->post('a_type'),
                    'a_category' => $this->input->post('a_category'),
                    'a_name' => $this->input->post('a_name'),
                    'make' => $this->input->post('make'),
                    'item_des' => $this->input->post('i_des'),
                    'u_price' => $this->input->post('u_price'),
                    'unit' => $this->input->post('unit'),
                    'mode_of_payment' => $mop,
                    'check_no' => $checkno,
                    'check_bank_name' => $check_bank_name,
                    'draft_no' => $draft_no,
                    'draft_bank_name' => $draft_bank_name,
                    'ft_bank_name' => $ft_bank_name,
                    'ft_account_no' => $ft_account_no,
                    'ft_ifsc_code' => $ft_ifsc_code,
                    'qty' => $this->input->post('qty'),
                    'opening_qty' => $this->input->post('opening_qty'),
                    'closing_qty' => $this->input->post('closing_qty'),
                    'warehouse' => $this->input->post('warehouse'),
                    'note' => $this->input->post('note'),
                    'importance' => $this->input->post('importance'),
                    'l_stock' => $this->input->post('l_stock'),
                    'a_image' => $asset_thumb,
					);
               
			   } else {
                   // $asset_thumb = "";
				    $asset = array(
                    'hotel_stock_inventory_id' => $this->input->post('id'),
                    //'stock_inventory_id'=>$this->input->post('stock_inventory_id'),
                    //'a_hotel_id' => $this->session->userdata('user_hotel'),
                    'item_type' => $this->input->post('a_type'),
                    'a_category' => $this->input->post('a_category'),
                    'a_name' => $this->input->post('a_name'),
                    'make' => $this->input->post('make'),
                    'item_des' => $this->input->post('i_des'),
                    'u_price' => $this->input->post('u_price'),
                    'unit' => $this->input->post('unit'),
                    'mode_of_payment' => $mop,
                    'check_no' => $checkno,
                    'check_bank_name' => $check_bank_name,
                    'draft_no' => $draft_no,
                    'draft_bank_name' => $draft_bank_name,
                    'ft_bank_name' => $ft_bank_name,
                    'ft_account_no' => $ft_account_no,
                    'ft_ifsc_code' => $ft_ifsc_code,
                    'qty' => $this->input->post('qty'),
                    'opening_qty' => $this->input->post('opening_qty'),
                    'closing_qty' => $this->input->post('closing_qty'),
                    'warehouse' => $this->input->post('warehouse'),
                    'note' => $this->input->post('note'),
                    'importance' => $this->input->post('importance'),
                    'l_stock' => $this->input->post('l_stock'),
                   // 'a_image' => $asset_thumb,
                );
                }


               
			

                //print_r($asset);
                //exit;

                $query = $this->dashboard_model->update_stock_invent($asset);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Asset Added Successfully!");
                    redirect('dashboard/all_stock_invent');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    // $data['asset_type'] = $this->dashboard_model->all_assets_types();
                    $data['page'] = 'backend/dashboard/def_stock_invent';
                    $this->load->view('backend/common/index', $data);
                }
            } else {
                $data['asset_type'] = $this->dashboard_model->all_assets_types();
                $data['page'] = 'backend/dashboard/def_stock_invent';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function all_stock_invent() {

        $found = in_array("57", $this->arraay);
        if ($found) {
            $data['heading'] = 'Stock Inventory';
            $data['sub_heading'] = '';
            $data['description'] = 'All S/I';
            $data['active'] = 'all_stock_invent';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['logs'] = $this->dashboard_model->all_stock_invent();
            $data['assets'] = $this->dashboard_model->all_assets_limit();
            $data['type'] = $this->dashboard_model->all_assets_types();
            $data['category'] = $this->dashboard_model->all_assets_category();
            $data['warehouse'] = $this->dashboard_model->all_warehouse();
            //$data['new_log']=$this->dashboard_model->all_new_stock_invent();
            $data['page'] = 'backend/dashboard/all_stock_invent';
            $this->load->view('backend/common/index', $data);
        } else {
            rediect('dashboard');
        }
    }

    function chk_duplicate_name() {
        $name = $this->input->post('data');
        $query = $this->dashboard_model->chk_duplicate_name($name);
        if ($query > 0) {
            $data = array(
                'data' => "1"
            );
        } else {
            $data = array(
                'data' => "0"
            );
        }

        header('Content-Type: application/json');
        echo json_encode($data);
        //print_r($query);
        //exit;
    }

    function delete_stock_invent() {

        $found = in_array("58", $this->arraay);
        if ($found) {
            $id = $_GET['a_id'];
            $details = $this->dashboard_model->delete_stock_invent($id);

            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function delete_transfer_asset() {

        $found = in_array("59", $this->arraay);
        if ($found) {
            $id = $_GET['id'];
            $details = $this->dashboard_model->delete_transfer_asset($id);

            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function transfer_asset() {
		$this->load->library('upload');
		$this->load->library('image_lib');
		$found = in_array("63", $this->arraay);
        if ($found) {
            $data['heading'] = 'Stock Inventory';
            $data['sub_heading'] = '';
            $data['description'] = 'Transfer Asset';
            $data['active'] = 'transfer_asset';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['assets'] = $this->dashboard_model->all_stock_invent();
            $data['type'] = $this->dashboard_model->all_assets_types();
            $data['warehouse'] = $this->dashboard_model->all_warehouse();
            $data['category'] = $this->dashboard_model->all_stock_invent_cat();
            $data['admin'] = $this->dashboard_model->fetch_user();
            $data['transfer_asset'] = $this->dashboard_model->all_transfer_asset();
            if ($this->input->post()) {
                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('grn_image')) {

                    $asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/asset/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                   // $this->load->library('image_lib', $config_manip);
					$this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/transfer_asset');
                    }
                    $thumb_name = explode(".", $filename);
                    $grn_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                } else {

                    //$source_path = './upload/' . 'asdf';
                    // $target_path = './upload/asset/';
                    $grn_thumb = "no_images.png";
                }

                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('transfr_vou_image')) {

                    $asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/asset/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                    //$this->load->library('image_lib', $config_manip);
					$this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/transfer_asset');
                    }
                    $thumb_name = explode(".", $filename);
                    $transfr_vou_image_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                } else {


                    $transfr_vou_image_thumb = "no_images.png";
                }




                $transfer_asset = array(
                    //'stock_category_id'=>$this->input->post('stock_category_id'),
                    'item_id' => $this->input->post('a_name'),
                    'from_warehouse_id' => $this->input->post('from_warehouse'),
                    'to_warehouse_id' => $this->input->post('to_warehouse'),
                    'qty' => $this->input->post('qty'),
                    'note' => $this->input->post('note1'),
                    'grn_image' => $grn_thumb,
                    'transfr_vou_image' => $transfr_vou_image_thumb,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'approv_admin_id' => $this->input->post('admin_id'),
                    'admin_id' => $this->session->userdata('user_id'),
                    'user_id' => $this->session->userdata('user_id'),
                );

                //print_r($transfer_log);exit;
                //print_r($transfer_asset);exit;
                $query = $this->dashboard_model->transfer_asset($transfer_asset);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Add  Successfully!");
                    redirect('dashboard/transfer_asset');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/transfer_asset');
                }
            }




            $data['page'] = 'backend/dashboard/transfer_asset';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function transfer_asset_date() {

        $start_date = date("Y-m-d", strtotime($this->input->post('fdate')));
        $end_date = date("Y-m-d", strtotime($this->input->post('edate')));
        //echo  $start_date;
        //echo  $end_date;
        //exit;
        $transfer_asset = $this->dashboard_model->all_transfer_asset_by_date($start_date, $end_date);

        $output = ' <table class="table table-striped table-hover table-bordered table-scrollable" id="dataTable2">
          <thead>
            <tr>

              <th scope="col">Serial No </th>
              <th scope="col">Upload GRN/ Transfer Voucher image </th>
              <th scope="col">Date </th>
              <th scope="col">item</th>
              <th scope="col">From Warehouse </th>
              <th scope="col">To Warehouse</th>
              <th scope="col">Qty</th>
              <th scope="col">Note</th>
              <th scope="col">Admin Name</th>
              <th scope="col"> Action </th>

            </tr>
          </thead>
          <tbody>';
        $srl_no = 0;
        if (isset($transfer_asset) && $transfer_asset) {

            foreach ($transfer_asset as $asset) {
                if ($asset->grn_image == '') {
                    $grn = "no_images.png";
                } else {
                    $grn = $asset->grn_image;
                }

                if ($asset->transfr_vou_image == '') {
                    $vou_img = "no_images.png";
                } else {
                    $vou_img = $asset->transfr_vou_image;
                }

                $srl_no++;
                $output .= '<tr>';
                $output .= '<td>' . $srl_no . '</td>';
                $output .= '<td><img  width="60px" height="60px" src="' . base_url() . 'upload/asset/' . $grn . '" alt=""/>
		   <img  width="60px" height="60px" src="' . base_url() . 'upload/asset/' . $vou_img . '" alt=""/>
		   </td>';
                $output .= '<td>' . $asset->date . '</td>';

                if (isset($asset->item_id)) {
                    $item_name = $this->dashboard_model->get_stock_item($asset->item_id);
                    $item_name->a_name;
                }

                $output .= '<td>' . $item_name->a_name . '</td>';


                if (isset($asset->from_warehouse_id)) {
                    $warehouse_id = $asset->from_warehouse_id;
                    $wharehouse = $this->dashboard_model->get_warehouse($warehouse_id);
                    $wharehouse->name;
                }
                $output .= '<td>' . $wharehouse->name . '</td>';

                if (isset($asset->to_warehouse_id)) {
                    $warehouse_id = $asset->to_warehouse_id;
                    $wharehouse = $this->dashboard_model->get_warehouse($warehouse_id);
                    $wharehouse->name;
                }
                $output .= '<td>' . $wharehouse->name . '</td>';
                $output .= '<td>' . $asset->qty . '</td>';
                $output .= '<td>' . $asset->Note . '</td>';

                $admin_id = $asset->approv_admin_id;
                $admin_name = $this->dashboard_model->get_admin($admin_id);
                if (isset($admin_name) && $admin_name) {
                    foreach ($admin_name as $name) {
                        $name = $name->admin_first_name . $name->admin_middle_name . $name->admin_last_name;
                    }
                } else {
                    $name = "";
                }
                $output .= '<td>' . $name . '</td>';
                $output .= '<td>';
                $output .= '<table width="100%"><tr><td><a onclick="soft_delete(' . $asset->id . ')"> <button data-toggle="modal"  class="btn red pull-right" ><i class="fa fa-trash"></i></button></a>
		   </tr></table>';
                $output .= '</tr>';
            }
            $output .= '</tbody></table>';
            echo $output;
        } else {
            echo $output = 0;
        }
    }

    function pdf_transfer_asset() {



        $found = in_array("38", $this->arraay);
        if ($found) {
            if (($this->input->get('s_d') == "") && ($this->input->get('e_d') == "")) {
                $data['admin'] = $this->dashboard_model->fetch_user();
                $data['transfer_asset'] = $this->dashboard_model->all_transfer_asset();
                $data['page'] = 'backend/dashboard/pdf_transfer_asset';
            } else {
                $start_date = date("Y-m-d", strtotime($this->input->get('s_d')));
                $end_date = date("Y-m-d", strtotime($this->input->get('e_d')));
                $data['start_date'] = $start_date;
                $data['end_date'] = $end_date;
                $data['transfer_asset'] = $this->dashboard_model->all_transfer_asset_date($start_date, $end_date);
                //$data['hotel_contact']  = $this->bookings_model->get_hotel_contact_details($data['bookings'][0]->hotel_id);
            }
            $this->load->view("backend/dashboard/pdf_transfer_asset", $data);
            $html = $this->output->get_output();
            $this->load->library('dompdf_gen');
            //  $this->dompdf->set_paper('A3','landscape');
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("all_transfer_asset.pdf");
        } else {
            redirect('dashboard');
        }
    }
    
    function update_stock_invent() {

        $found = in_array("60", $this->arraay);
        if ($found) {
            $data['heading'] = 'Stock Inventory';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Stock & Inventory';
            $data['active'] = 'all_stock_invent';
            $data['assets'] = $this->dashboard_model->all_stock_invent();
            $data['type'] = $this->dashboard_model->all_assets_types();
            $data['warehouse'] = $this->dashboard_model->all_warehouse();
            $data['category'] = $this->dashboard_model->all_stock_invent_cat();
            $data['vendor'] = $this->dashboard_model->all_vendors();
            //$data['category']=$this->dashboard_model->all_assets_category();
            //$data['usage_log'] = $this->dashboard_model->all_usage_log();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			 $bil_no = $this->dashboard_model->get_lastPay();
			 $bill="HM0".$this->session->userdata('user_hotel')."-".++$bil_no; 	
           
		   if ($this->input->post()) {
			   
				$a_id=preg_split("/[\s,~]+/", $this->input->post('a_name'));
				/*$a_ids=split('~', $this->input->post('a_name'));
				echo 'a_name - '.$this->input->post('a_name');
				echo '</br>';
				echo 'a_id - ';
				print_r($a_id);
				echo '</br>';
				echo 'a_ids - ';
				print_r($a_ids);
				exit;*/
				
                /* Images Upload */
                date_default_timezone_set("Asia/Kolkata");
                $mop = $this->input->post('mop');
                $checkno = $this->input->post('checkno');
                $check_bank_name = $this->input->post('check_bank_name');
                $draft_no = $this->input->post('draft_no');
                $draft_bank_name = $this->input->post('draft_bank_name');
                $ft_bank_name = $this->input->post('ft_bank_name');
                $ft_account_no = $this->input->post('ft_account_no');
                $ft_ifsc_code = $this->input->post('ft_ifsc_code');
                $unit = $this->input->post('unit');
                $item_name = $a_id[1];
                $date = $this->input->post('date');
                $vendor_id = $this->input->post('vendor_id');
                $vendor_name = $this->input->post('vendor[query]');
                $bill_date =  date("Y-m-d H:i:s", strtotime($this->input->post('bill_date') . ' ' . date('H:i:s')));
                $bill_due_date = $this->input->post('bill_due_date') ;
                $payments_term = $this->input->post('payment_term');
               // $payments_status = $this->input->post('payment_status');
                $delivery_date = $this->input->post('delivery_date');
                $specific_date = $this->input->post('specific_date');
                
				if($payments_term=='cod'){
					$bill_due_date= $delivery_date;
				}elseif($payments_term=='other'){
					$bill_due_date= $specific_date;
				}
				
				if($vendor_id==''){
					$newVendor=array(
					'hotel_vendor_name'=>$vendor_name,
					'user_id'=>$this->session->userdata('user_id'),
					'hotel_id'=>$this->session->userdata('user_hotel'),
					);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$this->db1->insert('hotel_vendor',$newVendor);
				$vendor_id=$this->db1->insert_id();
				}	 
			//	echo " vendor iD : ".$vendor_id; exit;
                $item = array(
                 
                    'hotel_stock_inventory_id' => $a_id[0],
                   // 'a_type' => $this->input->post('a_type'),
                    'a_category' => $this->input->post('a_category'),
                    'item' => $a_id[0],
                    'vendor' => $vendor_id,
                    'warehouse' => $this->input->post('warehouse'),
                  //  'link_purchase' => $this->input->post('link_purchase'),
                    'qty' => $this->input->post('qty'),
                    'opening_qty' => +$this->input->post('qty1'),
                    'closing_qty' => $this->input->post('qty') + $this->input->post('qty1'),
                    'price' => $this->input->post('price'),
                    'note' => $this->input->post('note'),
                    'operation' => "add"
                );
          
                if ($check_bank_name != "") {
                    $t_bank_name = $check_bank_name;
                } else if ($draft_bank_name != "") {
                    $t_bank_name = $draft_bank_name;
                } else {
                    $t_bank_name = $ft_bank_name;
                }

                $line_itmes = array(
                    'product' => $this->input->post('item_name'),
                    'qty' => $this->input->post('qty'),
                    'price' => $this->input->post('price')/$this->input->post('qty'),
                    'tax' => 0,
                    'discount' => 0,
                    'total' => $this->input->post('price'),
                    'class' => '',
                    'note' => '',
                );

                $add_pay = array(
                    'date' => date("Y-m-d H:i:s"),
                    'type_of_payment' => "Bill payments",
                    'vendor_id' => $vendor_id,
                    'bill_month' => "",
                    'bill_other' => " For item purchase",
                    'person_name' => "",
                    'designation' => "",
                    'sal_month' => "",
                    'sal_other' => "",
                    'payment_to' => "",
                    'misc_month' => "",
                    'misc_other' => "",
                    'tax_month' => "",
                    'tax_other' => "",
                    'tax_type' => "",
                    'description' => "",                    
                    'approved_by' => "a",
                    'paid_by' => "a",
                    'recievers_name' => "a",
                    'recievers_desig' => "a",                  
                    'admin' => $this->session->userdata('user_id'),                    
                    'profit_center' => '',
                    'note' => 'bill payment',
                    'bill_date' => date("Y-m-d H:i:s", strtotime($this->input->post('bill_date') . ' ' . date('H:i:s'))),
					'bill_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                    'payment_term_date' =>date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
					'payment_term' => $payments_term,	
				    'ref_no' => 123,
                    'payments_due' => $this->input->post('price'),
                    'payments_status' => '0',
                    'payments_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                    'delivery_date' => date("Y-m-d H:i:s", strtotime($this->input->post('delivery_date') . ' ' . date('H:i:s'))),
                    'hotel_id' => $this->session->userdata('user_hotel'),
					'user_id' => $this->session->userdata('user_id'),
                    'bill_no' => $bill,
                );

            //  print_r($add_pay); exit;
                $item_id = $item_name;
                $i_name = $this->dashboard_model->get_item_name1($item_id);
                
                $stock_qty = array(
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                    'warehouse_id' => $this->input->post('warehouse'),
                    'qty' => $this->input->post('qty'),
                    'item_id' => $a_id[0],
                    'note' => $this->input->post('note'),
                );
             //   print_r($stock_qty);
			//	exit;
                $check_stock = $this->dashboard_model->get_stock($a_id[0]);
               // print_r($check_stock);
               // exit;
                if (isset($check_stock)) {
                    $flg = 0;
                    foreach ($check_stock as $chk_stk) {

                        if ($chk_stk->warehouse_id == $this->input->post('warehouse') && $chk_stk->item_id ==$a_id[0]) {
                          
							$flg++;
                        }
                    }

                    if ($flg > 0) {
						//echo "test"; exit;  
                        $update_warehouse_stock = $this->dashboard_model->update_warehouse_stock($stock_qty);
                    } else { //echo "not have";exit;
                        $query3 = $this->dashboard_model->insert_warehouse_stock($stock_qty);
                    }
                }
                $query = $this->dashboard_model->new_stock($item, $stock_qty, $add_pay, $line_itmes);


                if ($query) {
                    $this->session->set_flashdata('succ_msg', "Updated  Successfully!");
                    redirect('dashboard/stock_invent_log');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/update_stock_invent');
                }
            }
            $data['page'] = 'backend/dashboard/add_stock_invent';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    // end function update_stock_invent

  /*  function get_item_name() {
        $cat_type = $this->input->post('data');
        $cat_name = $this->input->post('data1');
        $query = $this->dashboard_model->get_item_name($cat_name, $cat_type);
        $val = '<select class="form-control" id="a_name" name="a_name" required="required" onchange="get_item_price(this.value)"><option value="">Select Item</option>';


        foreach ($query as $qry) {
            //echo $qry->hotel_stock_inventory_id;


            $val .= '<option value="' . $qry->hotel_stock_inventory_id . '~' . $qry->a_name . '">' . $qry->a_name . '</option>';
        }
        $val .= '</select><label></label><span class="help-block">Select Item *</span>';
        echo $val;
    }*/
    
    
    function get_item_name() {
      //  $cat_type = $this->input->post('data');
        $cat_name = $this->input->post('data1');
        $query = $this->dashboard_model->get_item_name($cat_name);
        $val = '<select class="form-control" id="a_name" name="a_name" required="required" onchange="get_item_price(this.value)"><option value="">Select Item</option>';


        foreach ($query as $qry) {
            //echo $qry->hotel_stock_inventory_id;


            $val .= '<option value="' . $qry->hotel_stock_inventory_id . '~' . $qry->a_name . '">' . $qry->a_name . '</option>';
        }
        $val .= '</select><label></label><span class="help-block">Select Item *</span>';
        echo $val;
    }

    function get_item_name_by_wh_id() {
        $wh_id = $this->input->post('data');
        $query = $this->dashboard_model->get_item_name_by_wh_id($wh_id);
        //print_r($query);exit;
        $val = '<select class="form-control" id="a_name" name="a_name" required="required" onchange="get_item_qty(this.value)">
                <option value="">Select Item</option>';

        //$query1=$this->dashboard_model->get_item_name1($qry->item_id);
        //print_r($query1);exit;
        foreach ($query as $qry) {
            $item_id = $qry->item_id;

            $query1 = $this->dashboard_model->get_item_name1($item_id);
            //print_r($query1);
            //exit;
            if (isset($query1) && $query1) {
                $name = $query1->a_name;

                $val .= '<option value="' . $qry->item_id . '">' . $name . '</option>';
            }
        }
        $val .= '</select><label></label><span class="help-block">Select Item *</span>';
        echo $val;
    }

    function get_item_price() {
        $id = $this->input->post('data');
        // $id=53;
        //$itm="Toothbrush";
        $var = "";
        $query = $this->dashboard_model->get_item_price($id);
        $id1 = $query->warehouse; //exit;
        foreach ($query as $wh_id) {
            $query1 = $this->dashboard_model->get_wh_id_from_mpping($query->hotel_stock_inventory_id);
        }


        $var .= "<option value=''>Select Warehouse</option>";
        // print_r($query1); exit;
        foreach ($query1 as $qr1) {
            $name_query = $this->dashboard_model->get_warehouse($qr1->warehouse_id);

            $name = $name_query->name;

            $var .= "<option value='" . $qr1->warehouse_id . "'>" . $name . "</option>";
        }

        $val = $query->u_price . "_" . $query->hotel_stock_inventory_id . "_" . $query->qty . "_" . $query->opening_qty . "_" . $query->closing_qty . "_" . $query->l_stock;
        echo $var . "**" . $val;
    }

    function get_qty() {
        $w_id = $this->input->post('data');

        $item_id = $this->input->post('item_id');
        $query = $this->dashboard_model->get_qty($w_id, $item_id);

        $value = $query->qty;
        echo $value;
    }

    function update1_stock_invent() {

        $found = in_array("60", $this->arraay);
        if ($found) {
            $data['heading'] = 'Stock Inventory';
            $data['sub_heading'] = '';
            $data['description'] = 'Sub Stock & Inventory';
            $data['active'] = 'all_stock_invent';
            $data['assets'] = $this->dashboard_model->all_stock_invent();
            $data['type'] = $this->dashboard_model->all_assets_types();
            $data['warehouse'] = $this->dashboard_model->all_warehouse();
            $data['category'] = $this->dashboard_model->all_stock_invent_cat();
            //$data['category']=$this->dashboard_model->all_assets_category();
            $data['usage_log'] = $this->dashboard_model->all_usage_log();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/sub_stock_invent';
            $this->load->view('backend/common/index', $data);

            if ($this->input->post()) {


                /* Images Upload */
                date_default_timezone_set("Asia/Kolkata");




                $item = array(
                    //'date'=>$this->input->post('date'),
                    'hotel_stock_inventory_id' => $this->input->post('a_name'),
                 //   'a_type' => $this->input->post('a_type'),
                    'a_category' => $this->input->post('a_category'),
                    'item' => $this->input->post('a_name'),
                    'warehouse' => $this->input->post('warehouse'),
                    //'link_purchase'=>$this->input->post('link_purchase'),
                    'qty' => $this->input->post('qty'),
                    'opening_qty' => $this->input->post('qty1'),
                    'closing_qty' => $this->input->post('qty1') - $this->input->post('qty'),
                    'operation' => "sub",
                    'note' => $this->input->post('Reason')
                );
            //    print_r($item);
              //  exit;
                $stock_qty = array(
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'warehouse_id' => $this->input->post('warehouse'),
                    'qty' => $this->input->post('qty'),
                    'item_id' => $this->input->post('a_name'),
                    'note' => $this->input->post('note'),
                );
                //print_r($stock_qty);
                //exit;

                $query = $this->dashboard_model->update_stock($item, $stock_qty);


                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Updated Successfully!");
                    redirect('dashboard/stock_invent_log');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/update1_stock_invent');
                }
            }
        } else {
            redirect('dashboard');
        }
    }

    function stock_invent_log() {

        $found = in_array("60", $this->arraay);
        if ($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'All Stock & Inventory Log';
            $data['description'] = 'All S/I Log';
            $data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if (($this->input->post('t_dt_frm')) && ($this->input->post('t_dt_to'))) {
                $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
                $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
                $data['start_date'] = $this->input->post('t_dt_frm');
                $data['end_date'] = $this->input->post('t_dt_to');
                $data['logs'] = $this->dashboard_model->all_stock_invent_by_date($start_date, $end_date);
                $data['new_log'] = $this->dashboard_model->all_new_stock_invent_by_date($start_date, $end_date);
            } else {
                $data['logs'] = $this->dashboard_model->all_stock_invent();
                $data['new_log'] = $this->dashboard_model->all_new_stock_invent();
            }
            $data['page'] = 'backend/dashboard/stock_invent_logs';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function add_kit() {

        $found = in_array("14", $this->arraay);
        if ($found) {
            $data['heading'] = 'Room';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Kit';
            $data['active'] = 'kit_card';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {
                //print_r($this->input->post());

                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('image')) {

                    $asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/asset/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 400,
                        'height' => 400,
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_kit');
                    }
                    $thumb_name = explode(".", $filename);
                    $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();


                    $kit_defination_array = array(
                        'kit_name' => $this->input->post('kit_name'),
                        'kit_des' => $this->input->post('kit_des'),
                        'kit_type' => $this->input->post('kit_type'),
                        'kit_extra_cost' => $this->input->post('kit_extra_cost'),
                        'is_sellable' => $this->input->post('is_sellable'),
                        'kit_sel_price' => $this->input->post('kit_sel_price'),
                        //'default_kit'=>$this->input->post('kit_default'),
                        'image_name' => $asset_thumb,
                        'hotel_id' => $this->session->userdata('user_hotel'),
                        'user_id' => $this->session->userdata('user_id')
                    );
                    $last_kit_id = $this->dashboard_model->add_kit($kit_defination_array);


                    if (isset($last_kit_id)) {

                        for ($i = 0; $i < $this->input->post('count'); $i++) {
                            if (($this->input->post('i_name_' . $i) != "" ) && ($this->input->post('qty_' . $i) != "" ) && ($this->input->post('unit_cost_' . $i) != "" ) && ($this->input->post('qty_for_' . $i) != "" )) {
                                $kit_maping_array = array(
                                    'item_name' => $this->input->post('i_name_' . $i),
                                    'qty' => $this->input->post('qty_' . $i),
                                    'unit_price' => $this->input->post('unit_cost_' . $i),
                                    'qty_for_apply' => $this->input->post('qty_for_' . $i),
                                    'kit_id' => $last_kit_id,
                                    'hotel_id' => $this->session->userdata('user_hotel'),
                                    'user_id' => $this->session->userdata('user_id')
                                );
                                $last_kit_mapping_id = $this->dashboard_model->add_kit_mapping($kit_maping_array);
                            }
                        }

                        $this->session->set_flashdata('succ_msg', "Kit Defined Succeffully. ");
                        redirect('dashboard/add_kit');
                    }
                } else {
                    $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    redirect('dashboard/add_kit');
                }
            } else {
                $data['page'] = 'backend/dashboard/add_kit';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function housekeeping_staff() {

        $found = in_array("10", $this->arraay);
        if ($found) {
            $data['heading'] = 'Housekeeping';
            $data['sub_heading'] = '';
            $data['description'] = 'House Keeping Staff';
            $data['active'] = 'housekeeping_staff';

            // $data['usage_log'] = $this->dashboard_model->all_usage_log();
            $data['maids'] = $this->dashboard_model->all_maid();

            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/housekeeping_staff';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function edit_housekeeping_staff() {
        $data['heading'] = 'Housekeeping';
        $data['sub_heading'] = 'Housekeeping Staff';
        $data['description'] = 'Housekeeping Staff';
        $data['active'] = 'Housekeeping Staff';
    }

    function delete_housekeeping_staff() {

        $found = in_array("12", $this->arraay);
        if ($found) {
            $id = $_GET['m_id'];
            $details = $this->dashboard_model->delete_assets($id);
            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function delete_assets() {

        $found = in_array("59", $this->arraay);
        if ($found) {
            $id = $_GET['a_id'];
            $details = $this->dashboard_model->delete_assets($id);
            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function add_housekeeping() {

        $found = in_array("10", $this->arraay);
        if ($found) {
            $data['heading'] = 'Housekeeping';
            $data['sub_heading'] = 'Add Housekeeping';
            $data['description'] = 'Add Housekeeping here';
            //$data['active'] = 'asset_type';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                
            }

            // $data['asset'] = $this->dashboard_model->all_assets_tasks();
            $data['page'] = 'backend/dashboard/add_housekeeping';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function housekeeping() {

        $found = in_array("10", $this->arraay);
        if ($found) {
            $data['heading'] = 'Housekeeping';
            $data['sub_heading'] = 'Dashboard';
            $data['description'] = 'Housekeeping Dashboard';
            $data['active'] = 'housekeeping';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                
            }

            // $data['asset'] = $this->dashboard_model->all_assets_tasks();
            $data['page'] = 'backend/dashboard/housekeeping';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function assingment() {
        if ($this->session->userdata('user_type_slug') != 'G') {
            $data['heading'] = 'Housekeeping';
            $data['sub_heading'] = 'assingment';
            $data['description'] = 'assingment Status here';
            $data['active'] = 'assingment';
            $data['rooms'] = $this->dashboard_model->all_rooms();

            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if ($this->input->post()) {
                
            }

            // $data['asset'] = $this->dashboard_model->all_assets_tasks();
            $data['page'] = 'backend/dashboard/assingment';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function room_status() {



        $this->load->view('backend/dashboard/room_status');
    }

    function calendar_load() {


        $this->load->view('backend/dashboard/calendar_load');
    }

    /* function snapshot_load(){


      $this->load->view('backend/dashboard/snapshot-load');


      }
      function calendar_load_2(){


      $this->load->view('backend/dashboard/calendar_load_2');


      } */

    function maid_matrix_load() {


        $this->load->view('backend/dashboard/maid_matrix');
    }

    function send_mail() {
         
      $booking_id =$this->input->post('booking_id');
       
    //   $booking_id=1497;
     //  echo $booking_id; die();
		$type="sb";
		$hotel_id = $this->session->userdata('user_hotel');
        $data['booking_details']  = $this->bookings_model->get_booking_details($booking_id);
        foreach ($data['booking_details'] as $key) {
            $data['group_id'] = $key->group_id;
            $bk_date = $key->booking_taking_date;
         
        }
        
        $data['hotel_name'] = $this->dashboard_model->get_hotel($data['booking_details'][0]->hotel_id);
        $data['hotel_contact']  = $this->bookings_model->get_hotel_contact_details($data['booking_details'][0]->hotel_id);
       	$inv_name="BK0".$hotel_id."_".$bk_date."_".$booking_id;
		$data['tax']=$this->bookings_model->get_tax_details($hotel_id);
		$data['adjustment']=$this->dashboard_model->get_adjuset_amt($booking_id);
		$data['all_adjustment']=$this->dashboard_model->all_adjst($booking_id);
		$data['chagr_tax']=$this->bookings_model->line_charge_item_tax($type,$booking_id);
	    $data['transaction_details']  = $this->bookings_model->get_transaction_details($booking_id);
        $data['payment_details']  = $this->bookings_model->get_payment_details($booking_id);
        $data['total_payment']  = $this->bookings_model->get_total_payment($booking_id);
		$data['pos']  =$this->dashboard_model->all_pos_booking($booking_id);
		$data['printer']  =$this->dashboard_model->get_printer($hotel_id);
		$data['bid']  =$booking_id;
		
		 $booking_details = $this->dashboard_model->get_booking_details($booking_id);
		$guest_id = $booking_details->guest_id;

        $guest_details = $this->dashboard_model->get_guest_details($guest_id);

        foreach ($guest_details as $guest) {
            if ($guest->g_email != "") {
                $email = $guest->g_email;
                $name = $guest->g_name;
            } else {
                $email = "smallick415@gmail.com";
                $name = "No Email Address";
            }
        }


  $this->load->library('email');

    $config['smtp_host'] = 'ssl://smtp.gmail.com';
    $config['smtp_user'] = 'smallick415@gmail.com';
        $config['smtp_pass'] = 'inttls@2020';
    $config['smtp_port'] = 465;
 $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE;
    $this->email->initialize($config);
     $this->email->from('smallick415@gmail.com', 'Royalpms');
  //  $this->email->reply_to('myemail@gmail.com', 'KeyStroke');
     $this->email->to($email);
    $this->email->subject('Thanks ' . $name . ' for  your Stay');
    $format = $this->load->view("backend/dashboard/invoice_pdf", $data, TRUE);
   $this->email->message($format);

       if($this->email->send()){
           echo '1';
       } 

    
    }

    function send_sms($r_message) {

        $details = $this->dashboard_model->get_admin('0');
        foreach ($details as $det) {
            echo $receiver = $det->admin_phone1;
            echo $r_message;
        }
        exit();


        // Authorisation details.
        $username = "samrat360@gmail.com";
        $hash = "2c806042ceba707a9ba592295dd8a6aceda01083";

        // Config variables. Consult http://api.textlocal.in/docs for more info.
        $test = "0";

        // Data for text message. This is the text message data.
        $sender = "TXTLCL"; // This is who the message appears to be from.
        $numbers = $receiver; // A single number or a comma-seperated list of numbers
        //$message = "This is a test message from the HO. Hyegeche configure. Do Anondo. ";
        // 612 chars or less
        // A single number or a comma-seperated list of numbers
        $message = urlencode($r_message);
        $data = "username=" . $username . "&hash=" . $hash . "&message=" . $message . "&sender=" . $sender . "&numbers=" . $numbers . "&test=" . $test;
        $ch = curl_init('http://api.textlocal.in/send/?');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        echo $result = curl_exec($ch); // This is the result from the API
        curl_close($ch);
    }

    function test2() {
       // $this->load->model('bookings_model');
       // $this->bookings_model->send_sms('abc');
	   $cash= $this->unit_class_model->get_mode_cash('ewallet',606);
		print_r($cash);	
		//echo $cash;
		//a=array();
		foreach($cash as $key => $value){
			echo $key."= ".$value;
		}
		}

    function change_room_status() {

        $room_id = $this->input->post('room_id');
        $clean_status = $this->input->post('clean_status');
        $prop_type = $this->input->post('prop_type');
        if ($prop_type == 'room') {
            $data = array(
                'room_id' => $room_id,
                'clean_id' => $clean_status,
            );
        }
        if ($prop_type == 'section') {
            $data = array(
                'sec_id' => $room_id,
                'sec_housekeeping_status' => $clean_status,
            );
        }

        $this->dashboard_model->update_room($data, $prop_type);
        if ($clean_status == 1) {
            $outer = $this->dashboard_model->get_housekeeping_pending_asso_staff($room_id, 'Maid', 'room');
            foreach ($outer as $to) {
                $maid_id = $to->staff_id;
            }
            if (isset($maid_id) && $maid_id) {
                $qry = $this->dashboard_model->update_maid_alo_unit($maid_id, "-1");
                $this->dashboard_model->update_housekeeping_log_status_by_staff_type($room_id, 'Maid', $prop_type);
            }
        }
        /* if($clean_status==1) {
          $get_maid_details_row = $this->dashboard_model->get_maid_row($room_id);
          $staff_avlabl = $get_maid_details_row->staff_total_availability;
          $maid_id = $get_maid_details_row->maid_id;
          $md_avilibility_count = $staff_avlabl-1;

          if ($staff_avlabl == '1') {
          $stats = '0';
          }
          else{
          $stats = '1';
          }

          $this->dashboard_model->remove_maid($room_id);

          date_default_timezone_set("Asia/Kolkata");

          $cleared_time = date("Y-m-d H:i:s");

          $this->dashboard_model->generate_housekeeping_log_update($cleared_time,$room_id,$maid_id);

          $data2=array(
          'maid_id' =>$maid_id,
          'maid_status' =>$stats,
          'staff_total_availability' => $md_avilibility_count
          );
          $query2=$this->dashboard_model->update_maid($data2);
          } */


        header('Content-Type: application/json');
        echo json_encode("Room Status Updated");
    }

    function add_maid() {

        $data = array(
            'maid_name' => $this->input->post('maid_name'),
            'maid_address' => $this->input->post('maid_address'),
            'maid_contact' => $this->input->post('maid_contact'),
            'maid_status' => '0',
            'staff_availability' => $this->input->post('status'),
            'type' => $this->input->post('type'),
            'section' => $this->input->post('maid_sec'),
            'max_alo_unit' => $this->input->post('max_alo_unit'),
            'hotel_id' => $this->session->userdata('user_hotel'),
            'user_id' => $this->session->userdata('user_id')
        );

        $query = $this->dashboard_model->add_maid($data);
        if (isset($query) && $query) {
            $this->session->set_flashdata('succ_msg', "Maid Added Successfully!");
            redirect('dashboard/housekeeping_staff');
        } else {
            $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
            redirect('dashboard/housekeeping');
        }
    }

    function all_maids() {
        if ($this->session->userdata('feedback') == '1') {
            $data['heading'] = 'View All Tasks';
            $data['sub_heading'] = 'View All Tasks';
            $data['description'] = 'All Tasks Details';
            $data['active'] = 'all_tasks';
            $data['maids'] = $this->dashboard_model->all_maids();
            // $data['tasks_assigee'] = $this->dashboard_model->task_assiger();
            //$data['tasks_pending'] = $this->dashboard_model->tasks_all();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $data['page'] = 'backend/dashboard/maids';
            $this->load->view('backend/common/index', $data);
        }
    }

    function assign_maid() {

        date_default_timezone_set("Asia/Kolkata");
        $maidId = $this->input->post('maid_id');
        $roomId = $this->input->post('room_id');
        //$roomId = 222;
        //$maidId = 19;

        $data = array(
            'maid_id' => $maidId,
            'room_id' => $roomId,
            'auditor_id' => $this->input->post('auditor_id'),
            'technician_id' => '0',
            'supervisor_id' => '0',
            'status' => 0,
            'date' => date("Y-m-d H:i:s")
        );



        $rum_id = $roomId;

        $room_check = $this->dashboard_model->check_if_room_ok($rum_id);

        if (isset($room_check) && $room_check) {
            $query = $this->dashboard_model->assigned_maid_update($rum_id, $data);
        } else {
            $query = $this->dashboard_model->assign_maid($data);
        }

        $md_id = $maidId;
        $maid_avaibility = $this->dashboard_model->maid_is_available($md_id);
        $md_avilibility_count = $maid_avaibility->staff_total_availability;
        $md_avilibility_count = $md_avilibility_count + 1;

        $data2 = array(
            'maid_id' => $maidId,
            'maid_status' => 1,
            'staff_total_availability' => $md_avilibility_count
        );

        $query2 = $this->dashboard_model->update_maid($data2);

        $data4 = array(
            'maid_id' => $maidId,
            'room_id' => $roomId,
            'maid_assigned_on' => date("Y-m-d H:i:s"),
            'auditor_assigned_on' => date("Y-m-d H:i:s"),
            'auditor_id' => '0',
            'room_cleared_on' => date("Y-m-d H:i:s"),
            'audited_on' => date("Y-m-d H:i:s")
        );

        $this->dashboard_model->generate_housekeeping_log_entry($data4);

        /* $data3=array(

          'room_id' => $this->input->post('room_id'),

          'clean_id' =>3,

          );

          $query3=$this->dashboard_model->update_room($data3); */

        if (isset($query) && $query && $query2) {
            header('Content-Type: application/json');
            echo json_encode("Maid Assigned to the Room");
        } else {
            header('Content-Type: application/json');
            echo json_encode("Database Error");
        }
    }

// End assign_maid

    function all_tasks() {

        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['heading'] = 'View All Tasks';
            $data['sub_heading'] = 'View All Tasks';
            $data['description'] = 'All Tasks Details';
            $data['active'] = 'all_tasks';
            $data['tasks_assign'] = $this->dashboard_model->task_assiger();
            $data['tasks_assigee'] = $this->dashboard_model->task_assiger();
            $data['tasks_pending'] = $this->dashboard_model->tasks_all();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $data['page'] = 'backend/dashboard/all_tasks';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function task_details() {

        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['heading'] = 'View Task Details';
            $data['sub_heading'] = 'View Task Details';
            $data['description'] = 'Task Details';
            $data['active'] = 'booking_edit';
            $data['tasks_pending'] = $this->dashboard_model->tasks_all();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $task_id = $_REQUEST['task_id'];
            $assignee_id = $_REQUEST['task_asignee'];
            $data['get_task'] = $this->dashboard_model->get_task_row($task_id);
            $data['get_other_tasks'] = $this->dashboard_model->get_assignee_task($assignee_id);
            $data['page'] = 'backend/dashboard/task_details';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function other_task_details() {

        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['heading'] = 'View Task Details';
            $data['sub_heading'] = 'View Task Details';
            $data['description'] = 'Task Details';
            $data['active'] = 'booking_edit';
            $data['tasks_pending'] = $this->dashboard_model->tasks_all();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $task_id = $_REQUEST['task_id'];
            $assignee_id = $_REQUEST['task_asignee'];
            $get_task = $this->dashboard_model->get_task_row($task_id);
            $get_other_tasks = $this->dashboard_model->get_assignee_task($assignee_id);
            $from = $get_task->from_id;
            $to = $get_task->to_id;

            $from_details = $this->dashboard_model->get_task_user($from);
            $to_details = $this->dashboard_model->get_task_user($to);
            $get_status = $get_task->status;
            $assign = '';
            $assign_to = '';
            foreach ($from_details as $from_details) {
                $assign = $from_details->admin_first_name . ' ' . $from_details->admin_middle_name . ' ' . $from_details->admin_last_name;
            }
            foreach ($to_details as $to_details) {
                $assign_to = $to_details->admin_first_name . ' ' . $to_details->admin_middle_name . ' ' . $to_details->admin_last_name;
            }
            if ($get_status == "0") {
                $task_status_desc = "Pending";
                $cmps = '#EA5B5B';
            } else {
                $task_status_desc = "Completed";
                $cmps = '#4DA14D';
            }
            $get_prority = $get_task->priority;
            if ($get_prority == '1') {
                $color = "#06050B";
                $background = "#ebeaf1";
            } else if ($get_prority == '2') {
                $color = "#180f01";
                $background = "#fef4e6";
            } else {
                $color = "#081314";
                $background = "#edf9f9";
            }
            //echo $cmps;
            //exit();
            //print_r($data);
            //	if(isset(get_task)){

            $out_put = '<div class="portlet box green">
		    <div class="portlet-title" style="background-color:' . $cmps . '">
                <div class="caption">' . $get_task->title . '</div></div>
            
            <div class="portlet-body">
               <ul class="list-group" style="margin-bottom:0;"> 
                    <li class="list-group-item" style="background:' . $background . '">
                        <label style="font-weight:bold">Assignee:</label>
                        <label style="color:$color">' . $assign . '
                     </label>
                    </li>';
            $out_put .= '<li class="list-group-item" style="background:' . $background . '">
                        <label style="font-weight:bold">Asigned to:</label>
                        <label style="color:$color">' . $assign_to . '
                        
                        </label>
                    </li>';
            $out_put .= '<li class="list-group-item" style="background:' . $background . '">
                        <label style="font-weight:bold">Task Details:</label>
                        <label style="color:$color">' . $get_task->task . '

                        </label>
                    </li>';
            $out_put .= '<li class="list-group-item" style="background:' . $background . '">
                        <label style="font-weight:bold">Task Status:</label>
                        <label style="color:$color">' . $task_status_desc . '
                        </label>
                    </li>';
            $out_put .= '<li class="list-group-item" style="background:' . $background . '">
                        <label style="font-weight:bold">Task Due Date:</label>
                        <label style="color:$color">' .date("l jS F Y", strtotime($get_task->due_date)). '
                        </label>
                    </li>';
            $out_put .= '<li class="list-group-item" style="background:' . $background . '">
                        <label style="font-weight:bold">Task Given on:</label>
                        <label style="color:$color">' . date("g:i A \- l jS F Y", strtotime($get_task->added_date)). '
                        </label>
						</li>';
									
			if($task_status_desc == 'Pending') {
				$out_put .= '<li class="list-group-item" style="background:' . $background . '">
                        <div class="task-config-btn btn-group"> 
							<a href="'.base_url().'dashboard/complete_task?task_id='.$get_task->id.' class="btn green btn-sm" style="padding: 4px 3px 4px;"> Mark this task Complete <i class="fa fa-check"></i></a> 
						</div>
						</li>';
                        
			} else if ($task_status_desc == 'Completed'){
				$out_put .= '<li class="list-group-item" style="background:' . $background . '">
                        <div class="task-config-btn btn-group"> 
							<a href="'.base_url().'dashboard/reopen_task?task_id='.$get_task->id.' class="btn green btn-sm" style="padding: 4px 3px 4px;"> Reopen this task <i class="fa fa-check"></i></a> 
						</div>
						</li>';
			}
						
			$out_put .='
                        
            	</ul>    
            </div>
         </div>';

            echo $out_put;
        } else {
            redirect('dashboard');
        }
    }

    //  END OF METHOD..

    /* function test(){
      $string="5Mota Mai12Chokua Jolwala";
      preg_match_all('!\d+!', $string, $matches);
      //print_r($matches);
      echo $matches[0][1];
      } */

    function matrix_maid_add() {

        $array = array();

        $array = $this->input->post('table');

        $data = "In " . $array[0][0] . "  assinged " . intval(preg_replace('/[^0-9]+/', '', $array[0][1]), 10);

        $length = count($array);

        $l = 0;

        while ($l < $length) {
            if (intval(preg_replace('/[^0-9]+/', '', $array[$l][1]), 10) != 0) {


                preg_match_all('!\d+!', $array[$l][1], $maid_ids);

                $length2 = count($maid_ids[0]);

                $l2 = 0;

                while ($l2 < $length2) {

                    $matrix = array(
                        'room_id' => $array[$l][0],
                        'maid_id' => $maid_ids[0][$l2],
                        'status' => 0
                    );

                    $this->dashboard_model->assign_maid($matrix);



                    $l2++;
                }
            } $l++;
        }

        header('Content-Type: application/json');
        echo json_encode("Maids Assigned");
    }

    function matrix_maid_auto() {

        $rooms = $this->dashboard_model->all_rooms();

        foreach ($rooms as $room) {

            $this->dashboard_model->remove_maid($room->room_id);
            $maids = $this->dashboard_model->all_maids_shuffle();
            // shuffle ($maids);
            foreach ($maids as $maid) {

                $data = array(
                    'room_id' => $room->room_id,
                    'maid_id' => $maid->maid_id,
                    'status' => 0
                );
                $this->dashboard_model->assign_maid($data);
                break;
            }
        }
        header('Content-Type: application/json');
        echo json_encode("Maids Auto Assigned");
    }

    function add_gi_details() {

        $data['heading'] = '';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = '';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        if ($this->input->post()) {

            //print_r($_POST);
            //exit;

            $booking_id = $this->input->post('booking_id');

            $stay = $this->input->post();
            unset($stay['booking_id']);
            //print_r($stay);
            //exit;
            $query = $this->dashboard_model->add_gi_details($stay, $booking_id);

            //print_r($stay);exit;
            if (isset($query) && $query) {
                $this->session->set_flashdata('succ_msg', "General information has been updated successfully!");
                redirect('dashboard/booking_edit?b_id=' . $booking_id);
            } else {
                $this->session->set_flashdata('err_msg', "Database Error.Please Try again later!");
                redirect('dashboard/booking_edit?b_id=' . $booking_id);
            }
        } else {
            $data['page'] = 'backend/dashboard/booking_edit';
            $this->load->view('backend/common/index', $data);
        }
    }

    function all_bookings_date() {
        $date = $this->input->post('date');
        $times = $this->dashboard_model->checkin_time();
        foreach($times as $time) {
            if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
                $modifier = ($time->hotel_check_in_time_hr + 12) + $time->hotel_check_in_time_mm/60;
            }
            else{
                $modifier = ($time->hotel_check_in_time_hr) + $time->hotel_check_in_time_mm/60;
            }
        }

        $times2=$this->dashboard_model->checkout_time();
		
        foreach($times2 as $time2) {
            if($time2->hotel_check_out_time_fr=='PM' && $time2->hotel_check_out_time_hr !="12") {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12) + $time->hotel_check_out_time_mm/60;
            }
            else{
                $modifier2 = ($time2->hotel_check_out_time_hr) + $time->hotel_check_out_time_mm/60;
            }
        }

        $date = $this->input->post('date');


        $parts = explode('/', $date);

        $date_final = $parts[2] . '-' . $parts[0] . '-' . $parts[1];
        // $date_final=date('Y-m-d H:i:s',strtotime($date_final)+$modifier1);

        $av = 0;
        $co = 0;
        $ci = 0;
        $ad = 0;
        $con = 0;
        $th = 0;
        $bookings = $this->dashboard_model->all_bookings();
        if ($bookings) {
            foreach ($bookings as $booking) {

                $cust_from_date = date('Y-m-d', strtotime($booking->cust_from_date) + $modifier1);
                $cust_end_date = date('Y-m-d', strtotime($booking->cust_end_date) + $modifier2);

                if ($cust_from_date <= $date_final && $cust_end_date >= $date_final) {

                    //echo 1;

                    if ($booking->booking_status_id == 1) {
                        $th++;
                    } elseif ($booking->booking_status_id == 2) {
                        $ad++;
                    } elseif ($booking->booking_status_id == 4) {
                        $con++;
                    } elseif ($booking->booking_status_id == 5) {
                        $ci++;
                    } elseif ($booking->booking_status_id == 6) {
                        $co++;
                    }
                }
            }
        }


        $no_room = $this->dashboard_model->all_rooms_row();
        $av = $no_room - ($th + $ad + $con + $ci + $co);



        $data = array(
            'av' => $av,
            'cho' => $co,
            'chi' => $ci,
            'advnce' => $ad,
            'con' => $con,
            'thld' => $th,
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function all_bookings_date_today() {
        //$date=$this->input->post('date');
        $times = $this->dashboard_model->checkin_time();
        foreach($times as $time) {
            if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
                $modifier = ($time->hotel_check_in_time_hr + 12) + $time->hotel_check_in_time_mm/60;
            }
            else{
                $modifier = ($time->hotel_check_in_time_hr) + $time->hotel_check_in_time_mm/60;
            }
        }

        $times2=$this->dashboard_model->checkout_time();
		
        foreach($times2 as $time2) {
            if($time2->hotel_check_out_time_fr=='PM' && $time2->hotel_check_out_time_hr !="12") {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12) + $time->hotel_check_out_time_mm/60;
            }
            else{
                $modifier2 = ($time2->hotel_check_out_time_hr) + $time->hotel_check_out_time_mm/60;
            }
        }


        // $parts = explode('/',$date);
        date_default_timezone_set('Asia/Kolkata');
        $date_final = date('Y-m-d');
        // $date_final=date('Y-m-d H:i:s',strtotime($date_final)+$modifier1);

        $av = 0;
        $co = 0;
        $ci = 0;
        $ad = 0;
        $con = 0;
        $th = 0;
        $bookings = $this->dashboard_model->all_bookings();
        if ($bookings) {
            foreach ($bookings as $booking) {

                /*   if ($booking->booking_status_id == 1) {
                  $th++;
                  } elseif ($booking->booking_status_id == 2) {
                  $ad++;
                  } elseif ($booking->booking_status_id == 4) {
                  $con++;
                  } elseif ($booking->booking_status_id == 5) {
                  $ci++;
                  } elseif ($booking->booking_status_id == 6) {
                  $co++;
                  } */

                $cust_from_date = date('Y-m-d', strtotime($booking->cust_from_date) + $modifier1);
                $cust_end_date = date('Y-m-d', strtotime($booking->cust_end_date) + $modifier2);

                if ($cust_from_date <= $date_final && $cust_end_date >= $date_final) {

                    //echo 1;

                    if ($booking->booking_status_id == 1) {
                        $th++;
                    } elseif ($booking->booking_status_id == 2) {
                        $ad++;
                    } elseif ($booking->booking_status_id == 4) {
                        $con++;
                    } elseif ($booking->booking_status_id == 5) {
                        $ci++;
                    } elseif ($booking->booking_status_id == 6) {
                        $co++;
                    }
                }
            }
        }


        $no_room = $this->dashboard_model->all_rooms_row();
        $av = $no_room - ($th + $ad + $con + $ci + $co);



        $data = array(
            'av' => $av,
            'cho' => $co,
            'chi' => $ci,
            'advnce' => $ad,
            'con' => $con,
            'thld' => $th,
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function ten_days_revenue_bookings() {

        $data = array(
        );

        date_default_timezone_set('Asia/Kolkata');
        $date_end = date("Y/m/d");

        $date_start = date("Y/m/d", strtotime("-10 days", strtotime($date_end)));

        // echo $date_end;
        // echo $date_start;
        $i = 0;
        while ($date_start <= $date_end) {

            $bookings = $this->dashboard_model->revenue_by_date($date_start);
            //print_r($bookings);
            //  exit;
            foreach ($bookings as $row) {
                if ($row->t_amount != '' && $row->t_amount > 0) {
                    //echo intval($row->t_amount)."\n";

                    $bookings_count = $this->dashboard_model->bookings_by_date($date_start);

                    $data1 = array(
                        'year' => $date_start,
                        'income' => ($row->t_amount / 1000),
                        'expenses' => $bookings_count
                    );
                    $data[$i] = $data1;
                } else {

                    $bookings_count = $this->dashboard_model->bookings_by_date($date_start);

                    $data1 = array(
                        'year' => $date_start,
                        'income' => 0,
                        'expenses' => $bookings_count
                    );
                    $data[$i] = $data1;
                }
            }

            $date_start = date("Y/m/d", strtotime("+1 day", strtotime($date_start)));

            $i++;
        }



        /* $data1=array(

          'year' => 2017,
          'income' =>10,
          'expenses' =>25


          ); */



        //	print_r($data);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function current_day_revenue_bookings() {

        $t_date = $this->input->post('t_date');
        date_default_timezone_set('Asia/Kolkata');


        $i = 0;
        $results = $this->setting_model->current_day_revenue_bookings($t_date);
        if (isset($results) && $results != '') {

            $output = '<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
		  
			<table class="table table-striped table-hover">
                            <thead>
                                <tr style="background-color:#F9F9F9 !important;">
                                    <th> ID </th>
                                    <th> Date - Time </th>
                                    <th> Type </th>
                                    <th> Mode </th>
                                    <th> Amount </th>
                                    <th>  </th>
                                </tr>
                            </thead>
						<tbody>';

            foreach ($results as $result) {

                if ($result->t_id) {

                    $tran_id = "TA0" . $this->session->userdata('user_hotel') . "/" . $this->session->userdata('user_id') . "/" . $result->t_id;
								
					$dec = 'none';
					if($result->t_status == 'Cancel')
						$dec = 'line-through';
					//$d = date("d",strtotime($transaction->t_date)).date("m",strtotime($transaction->t_date)).date("y",strtotime($transaction->t_date));
                   
					$t_id = '<span style="text-decoration:'.$dec.'">'.$tran_id.'</span>';
					if($result->t_status == 'Done'){
						$t_id .= ' <i style="color:#9BCA3B;" class="fa fa-check" aria-hidden="true"></i>';
						//$tpa = $tpa + $transaction->t_amount;
					}
					if($result->t_status == 'Pending')
						$t_id .= ' <i style="color:#FFAF4A;" class="fa fa-hourglass-start" aria-hidden="true"></i>';
					if($result->t_status == 'Cancel')
						$t_id .= ' <i style="color:#F5695E;" class="fa fa-times" aria-hidden="true"></i>';
					
					
					
					
                }
				
				if(isset($result->transaction_type_id) && $result->transaction_type_id!=''){
								
								$transaction_type_status = $this->setting_model->get_transaction_type_cat($result->transaction_type_id);
								if(isset($transaction_type_status) && $transaction_type_status!=''){
									//$transaction_type_text = 
									if($transaction_type_status->hotel_transaction_type_cat == 'Income'){
										
										$tran_type ='<span style="color:#127561;">'.$result->transaction_type;
									}
									else if($transaction_type_status->hotel_transaction_type_cat == 'Expense'){
										
										$tran_type ='<span style="color:#A40C0C;">'.$result->transaction_type;
									}
									else if($transaction_type_status->hotel_transaction_type_cat == 'Investment'){
										
										$tran_type =$result->transaction_type.""."(INV)";
									}
									else{
										$tran_type =$result->transaction_type.""."(EQU)";
									}
								}
								else{
											$tran_type ='No info';
								}
							}
							else{
								
								$tran_type ='N/A';
							}
							
                $output .= '<tr>							
														<td><a>' . $t_id . '</a></td>											
														<td>' . date("jS F Y h:i:s", strtotime($result->t_date)) . '</td>
														<td>' . $tran_type . '</td>
														<td>' . $result->t_payment_mode . '</td>
														<td>' . $result->t_amount . '</td>																			</tr>';
            }





            $output .= '</tbody></table></div>';
        }



        echo $output;
    }

    function get_weather() {

        $pincodes = $this->dashboard_model->get_hotel_pincode();

        foreach ($pincodes as $pincode) {
            $pincode = $pincode->hotel_pincode;
        }

        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $pincode . "&sensor=false";
        $details = file_get_contents($url);
        $result = json_decode($details, true);

        $lat = $result['results'][0]['geometry']['location']['lat'];

        $lng = $result['results'][0]['geometry']['location']['lng'];


        $json = file_get_contents("http://api.openweathermap.org/data/2.5/weather?lat=" . $lat . "&lon=" . $lng . "&appid=5eadfb5f0a2900ea7f1980c75cba3ba3");


        //  $json = file_get_contents("http://api.openweathermap.org/data/2.5/weather?zip=".$pincode.",in&appid=44db6a862fba0b067b1930da0d769e98");

        $obj = json_decode($json);
//echo $obj->access_token;
        // print_r($obj);
        // echo $obj['weather']['temp_min'];

        $main = $obj->main;

        $weather = $obj->weather;

        $min_temp = ($main->temp_min - 273) . " °C";
        $max_temp = ($main->temp_max - 273) . " °C";
        $temp = number_format((float) $main->temp - 273, 2, '.', '') . " °C";


        $data = array(
            'min_temp' => $min_temp,
            'max_temp' => $max_temp,
            'temp' => $temp,
            'description' => $weather[0]->description,
            'icon' => $weather[0]->icon,
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function add_purchase() {

        $found = in_array("82", $this->arraay);
        if ($found) {
            $last_pur_id = $this->dashboard_model->get_last_pur_id();
            if ($last_pur_id && isset($last_pur_id)) {
                $data['id'] = $last_pur_id;
            } else {
                $data['id'] = 0;
            }


            $data['heading'] = 'Finance';
            $data['sub_heading'] = 'Purchase';
            $data['description'] = 'Add Purchase Here';
            $data['active'] = 'all_purchase';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['vendor'] = $this->dashboard_model->all_vendor();
            if ($this->input->post()) {


                $purchase = array(
                    'p_id' => $this->input->post('p_id'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'a_id' => $this->input->post('a_id'),
                    'p_date' => date("Y-m-d H:i:s", strtotime($this->input->post('p_date'))),
                    'p_supplier' => $_POST['p_supplier']['query'],
                    'p_i_name' => $this->input->post('p_i_name'),
                    'p_i_description' => $this->input->post('p_i_description'),
                    'p_i_price' => $this->input->post('p_i_price'),
                    'p_i_quantity' => $this->input->post('p_i_quantity'),
                    'p_unit' => $this->input->post('p_unit'),
                    'p_price' => $this->input->post('p_price'),
                    'p_tax' => $this->input->post('p_tax'),
                    'p_discount' => $this->input->post('p_discount'),
                    'profit_center' => $this->input->post('profit_center'),
                    'p_total_price' => $this->input->post('p_total_price'),
                );

                date_default_timezone_set('Asia/Kolkata');

				 $line_itmes = array(
                    'product' => $this->input->post('p_i_name'),
                    'qty' => $this->input->post('p_i_quantity'),
                    'price' => $this->input->post('p_i_price'),
                    'tax' => $this->input->post('p_tax'),
                    'discount' => $this->input->post('p_discount'),
                    'total' => $this->input->post('p_total_price'),
                    'class' => '',
                    'note' => '',
                );
				
                $add_pay = array(
                    'date' => date("Y-m-d H:i:s"),
                    'type_of_payment' => "Bill payments",
                    'vendor_id' => $this->input->post("vendor_id"),
                    //'amount' => $this->input->post("amount"),
                    'bill_month' => "",
                     'bill_other' => " For Miscellaneous purchase",
                    //'note_bill' =>$note_bill,
                    'person_name' => "",
                    'designation' => "",
                    'sal_month' => "",
                    'sal_other' => "",
                    // 'note_sal' => $note_sal,
                    'payment_to' => "",
                    //'note_misc' => $note_misc,
                    'misc_month' => "",
                    'misc_other' => "",
                    'tax_month' => "",
                    'tax_other' => "",
                    'tax_type' => "",
                    //'tax_payment_to' => $tax_payment_to,
                    'description' => "",
                    //'mode_of_payment' => "",
                    //CARD
                    //$card_type,$bankname,$card_no,$card_name,$card_exmp,$card_expy,$card_cvv,$card_payment_status
                    //'tax_type' => $tax_type,
                    //CHECK NO 
                    //$che_bank_name, $che_bank_cno,$che_bank_drwer_name,$che_bank_payment_status
                    //'check_no' => "",
                    //'check_bank_name' =>"",
                    //'dd'=>$che_bank_drwer_name,
                    //'dd'=>$che_bank_payment_status,
                    //DRAFT
                    //'draft_no' => "",
                    //'draft_bank_name' => "",
                    //fund transfer
                    //$fund_bank_name,$fund_bank_ac_no,$fund_bank_ifsc,$fund_bank_payment_status
                    //'ft_bank_name' =>"",
                    //'ft_account_no' => "",
                    //'ft_ifsc_code' => "",
                    //''=>$fund_bank_payment_status,
                    'approved_by' => "a",
                    'paid_by' => "a",
                    'recievers_name' => "a",
                    'recievers_desig' => "a",
                    /* 'approved_by' => 'a',
                      'paid_by' =>  'a',
                      'recievers_name' => 'a',
                      'recievers_desig' =>  'a', */
                    'admin' => $this->session->userdata('user_id'),
                    
                    'profit_center' => '',
                    'note' => 'bill payment',
                    'bill_date' => date("Y-m-d H:i:s"),
                    'bill_due_date' => '',
                    'payment_term_date' => '',
                    'ref_no' => 123,
                    'payments_due' => $this->input->post('p_total_price'),
                    'payments_status' =>'0',
                    'payments_due_date' => '',
                    'hotel_id' => $this->session->userdata('user_hotel'),
					'user_id' => $this->session->userdata('user_id'),
                    'bill_no' => '1',
                );
				
				//print_r($add_pay); exit;
               /* $transaction = array(
                    'transaction_type' => "Vendor payment",
                    'transaction_type_id' => "5",
                    't_amount' => $this->input->post('p_total_price'),
                    't_date' => date("Y-m-d H:i:s"),
                    'transaction_releted_t_id' => "5",
                    't_status' => "Done",
                    'transaction_from_id' => "4",
                    'transaction_to_id' => "6",
                    't_payment_mode' => "cash",
                    'user_id' => $this->session->userdata('user_id'),
                    'p_center' => $this->input->post('profit_center'),
                    'transactions_detail_notes' => "( Purchase payments for " . $this->input->post('p_total_price') . " from " . $this->input->post('p_supplier') . " Rs: " . $this->input->post('p_total_price') . " )",
                );*/
                //$query2 = $this->dashboard_model-> add_transaction($transaction);
                $query = $this->dashboard_model->add_purchase($purchase,$add_pay,$line_itmes);
                $data['id'] = $this->dashboard_model->get_last_pur_id();
                // print_r($query);
                //exit;

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Purchase Added Successfully!");
                    redirect('dashboard/all_purchase', $data);
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/all_purchase');
                }
            } else {
                $data['page'] = 'backend/dashboard/add_purchase';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function all_purchase() {

        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['heading'] = 'Finance';
            $data['sub_heading'] = 'Purchase';
            $data['description'] = 'All Purchase List';
            $data['active'] = 'all_purchase';
            // $data['all_purchase'] = $this->dashboard_model->get_hotel($this->session->userdata('all_purchase'));

            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $data['purchases'] = $this->dashboard_model->all_purchase_limit();
            $data['page'] = 'backend/dashboard/all_purchases';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function delete_purchase() {

        $found = in_array("84", $this->arraay);
        if ($found) {
            $id = $_GET['p_id'];

            $details = $this->dashboard_model->delete_purchase($id);
            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function add_shift() {

        // if($this->session->userdata('user_type_slug') =='SUPA'){
        $found = in_array("252", $this->arraay);
        if ($found) {
            $data['heading'] = 'Shift';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Shift Here';
            $data['active'] = 'add_shift';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {


                $shift = array(
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    //'user_id'=>$this->session->userdata('user_id')
                    'shift_name' => $this->input->post('shift_name'),
                    'shift_opening_time' => $this->input->post('shift_opening_time'),
                    'shift_closing_time' => $this->input->post('shift_closing_time'),
                );

                $query = $this->dashboard_model->add_shift($shift);

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Shift Added Successfully!");
                    redirect('dashboard/all_shifts');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_shift');
                }
            } else {
                $data['page'] = 'backend/dashboard/add_shift';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function check_shift_expire() {
        $id = $this->input->post("id");

        date_default_timezone_set("Asia/Kolkata");
        $shifts = $this->dashboard_model->all_shift_limit();

        foreach ($shifts as $shift) {
            $time = date("H:i:s");
            if ($time < $shift->shift_closing_time && $time > $shift->shift_opening_time) {



                if ($time == date("H:i:s", strtotime($shift->shift_closing_time) - 60 * 5)) {

                    $expire = 1;
                } else if ($time > date("H:i:s", strtotime($shift->shift_closing_time))) {

                    $expire = 3;
                } else {

                    $expire = 0;
                }
            }
        }

        $data = array(
            'id' => $id,
            'expire' => $expire,
        );

        //ob_start('ob_gzhandler');

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function all_shifts() {
        //if($this->session->userdata('user_type_slug') =='SUPA') {
        $found = in_array("253", $this->arraay);
        if ($found) {
            $data['heading'] = 'Shift';
            $data['sub_heading'] = '';
            $data['description'] = 'Shifts List';
            $data['active'] = 'all_shifts';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $data['shifts'] = $this->dashboard_model->all_shift_limit();
            $data['shifts1'] = $this->dashboard_model->all_shift_limit1();
            $data['page'] = 'backend/dashboard/all_shifts';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function log_shift() {
        // if($this->session->userdata('user_type_slug') =='SUPA') {
        $found = in_array("253", $this->arraay);
        if ($found) {
            $data['heading'] = 'Shift';
            $data['sub_heading'] = '';
            $data['description'] = 'Shifts List';
            $data['active'] = 'log_shift';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $data['shifts'] = $this->dashboard_model->all_shift_limit();
            $data['page'] = 'backend/dashboard/log_shift';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function soft_delete_booking() {

       $found = in_array("27", $this->arraay);
      
        if ($found) {
            $id = $_GET['id'];

            $details = $this->dashboard_model->get_booking_details2($id);
            // $details->booking_extra_charge_id;
            $details1 = $this->dashboard_model->get_transaction_details($id);
            $line_items = $this->dashboard_model->get_line_items_details($id);

            if (isset($line_items)) {
                foreach ($line_items as $lm) {
                    $line = $lm;
                    $this->dashboard_model->insert_shadow_line_items($line);
                }
                $this->dashboard_model->delete_line_items($id);
            }
            //$pos_count=$this->dashboard_model->get_pos_count($id);
            /*  if($details->booking_extra_charge_id){
              $extra_ch_id = explode(",",$details->booking_extra_charge_id);
              if($extra_ch_id){
              for($i=0;$i<count($extra_ch_id);$i++)
              {
              $extra_charge=$this->dashboard_model->get_extra_charge_by_id($extra_ch_id[$i]);
              print_r($extra_charge); exit;
              $this->dashboard_model->move_extra_charge_shadow($extra_charge);
              $this->dashboard_model->delete_extra_charge_by_id($extra_ch_id[$i]);

              }
              }

              } */

            foreach ($details as $detail) {

                $data = $detail;
            }


            $pos = $this->dashboard_model->get_pos_details($id);
            if ($pos != false) {
                foreach ($pos as $ps) {
                    $data1 = $ps;
                    $this->dashboard_model->move_pos_shadow($data1);
                }
            }

            if ($data) {
                $query = $this->dashboard_model->insert_shadow_booking($data);
            }
            if ($details1) {
                $query4 = $this->dashboard_model->insert_shadow_transaction($details1);
            }
            $query2 = $this->dashboard_model->delete_booking($id);
            $query3 = $this->dashboard_model->delete_transaction($id);
            $this->dashboard_model->delete_pos($id);




            $data1 = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data1);
        } else {
            //redirect('dashboard');
        $data1 = array(
                'data' => "0",
            );
            header('Content-Type: application/json');
            echo json_encode($data1);
        }
    }

    function soft_delete_group_booking() {

        $found = in_array("27",$this->arraay);
        if($found)
        {

        $id = $_GET['id'];

        $group_details = $this->dashboard_model->get_group_details($id);
        foreach ($group_details as $group_detail) {
            $detail = $group_detail;

            $this->dashboard_model->move_group_details_shadow($detail);
        }

        $group_transaction = $this->dashboard_model->get_group_transaction_details($id);

        foreach ($group_transaction as $group_transaction) {
            $transaction = $group_transaction;

            $this->dashboard_model->move_group_transaction_shadow($transaction);
        }



        $groups = $this->dashboard_model->get_group($id);

        foreach ($groups as $grp) {
            $datas = $grp;
            $this->dashboard_model->move_group_shadow($datas);
        }



        $bookings = $this->dashboard_model->get_booking_details_grp($id);

        foreach ($bookings as $booking) {

            $booking_id = $booking->booking_id;

            $pos = $this->dashboard_model->get_pos_details($booking_id);
            $bookings_details = $this->dashboard_model->get_booking_details2($booking_id);

            if ($bookings_details != false) {
                foreach ($bookings_details as $bk_details) {
                    $bk_data = $bk_details;
                    $query = $this->dashboard_model->insert_shadow_booking($bk_data);
                }
            }

            if (isset($pos) && $pos != false) {
                foreach ($pos as $ps) {
                    $datas1 = $ps;
                    $this->dashboard_model->move_pos_shadow($datas1);
                }

                $this->dashboard_model->delete_pos($booking_id);
            }

            $query2 = $this->dashboard_model->delete_booking($booking_id);
            // $query3=$this->dashboard_model->delete_transaction($booking_id);
        }

        $this->dashboard_model->delete_group($id);
        $this->dashboard_model->delete_group_details($id);
        $this->dashboard_model->delete_group_transaction($id);

        $data1 = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data1);


         }else{
         
          $data1 = array(
                'data' => "0",
            );
            header('Content-Type: application/json');
            echo json_encode($data1);
         
          } 
    }

    function add_dgset() {
        $data['master_log'] = $this->dashboard_model->get_master_fuel_stock();
        $data['rating'] = $this->dashboard_model->get_dg_rating();

        $found = in_array("69", $this->arraay);
        if ($found) {
            $data['heading'] = 'DG set';
            $data['sub_heading'] = 'Add dgset';
            $data['description'] = 'Add DG set here';
            $data['active'] = 'add_dgset';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {
                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('dg_image')) {

                    $asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/dg_set/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_asset');
                    }
                    $thumb_name = explode(".", $filename);
                    $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                } else {
                    //$this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    //redirect('dashboard/add_asset');
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/dg_set/';
                    $asset_thumb = "no_images.png";
                }






                /* Images Upload */
                date_default_timezone_set("Asia/Kolkata");
                $dgset_id = $this->input->post('dgset_id');
                $genname = $this->input->post('gen_name');
                $asset_code = $this->input->post('asset_code');
                $make = $this->input->post('make');
                $model = $this->input->post('model');
                $ratings = $this->input->post('ratings');
                $fuel = $this->input->post('fuel');
                $cylinder = $this->input->post('cylinder_choice');
                $rotation = $this->input->post('Rotation_choice');
                $design = $this->input->post('Design_choice');
                $alternator_make = $this->input->post('alternator_make');
                $fuel_consumption = $this->input->post('fuel_consumption');
                $status = $this->input->post('status');
                $amc = $this->input->post('amc');
                $cooling = $this->input->post('Design_choice');


                $dgset = array(
                    //'hotel_id' => $this->session->userdata('user_hotel'),
                    'dgset_id' => $dgset_id,
                    'gen_name' => $genname,
                    'asset_code' => $asset_code,
                    'make' => $make,
                    'model' => $model,
                    'ratings' => $ratings,
                    'fuel' => $fuel,
                    'cylinders' => $cylinder,
                    'rotation' => $rotation,
                    'design' => $design,
                    'alternator_make' => $alternator_make,
                    'fuel_consumption' => $fuel_consumption,
                    'status' => $status,
                    'amc' => $amc,
                    'cooling' => $cooling,
                    'img' => $asset_thumb,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                        //'user_id'=>$this->session->userdata('user_id')
                );

                //print_r($dgset);
                //exit();
                $query = $this->dashboard_model->add_dgset($dgset);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "DGset Added Successfully!");
                    redirect('dashboard/all_dgset');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_dgset');
                }
            } else {
                $data['page'] = 'backend/dashboard/add_dgset';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function all_dgset() {

        $data['heading'] = 'DG set';
        $data['sub_heading'] = 'All DG sets';
        $data['description'] = 'DG set List';
        $data['active'] = 'all_dgset';
        // $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        /* if ($this->input->post('pages')) {
          $pages = $this->input->post('pages');
          } else {
          if ($this->session->flashdata('pages')) {
          $pages = $this->session->flashdata('pages');
          } else {
          $pages = 5;
          }
          }

          $this->session->set_flashdata('pages', $pages);

          $config['base_url'] = base_url() . 'dashboard/all_guest';
          $config['total_rows'] = $this->dashboard_model->all_guest_row();
          $config['per_page'] = $pages;
          $config['uri_segment'] = 3;
          $config['full_tag_open'] = "<ul class='pagination'>";
          $config['full_tag_close'] = "</ul>";
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
          $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
          $config['next_tag_open'] = "<li>";
          $config['next_tagl_close'] = "</li>";
          $config['prev_tag_open'] = "<li>";
          $config['prev_tagl_close'] = "</li>";
          $config['first_tag_open'] = "<li>";
          $config['first_tagl_close'] = "</li>";
          $config['last_tag_open'] = "<li>";
          $config['last_tagl_close'] = "</li>";


          $this->pagination->initialize($config);

          $segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

          $data['guest'] = $this->dashboard_model->all_guest_limit($config['per_page'], $segment);
          $data['pagination'] = $this->pagination->create_links(); */
        $data['master_log'] = $this->dashboard_model->get_master_fuel_stock();
        //	echo "<pre>";
        //print_r($data['master_log']);exit;

        $data['rating'] = $this->dashboard_model->get_dg_rating(); //print_r($data['rating']);exit;
        $data['dgset'] = $this->dashboard_model->all_dgset();
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/all_dgset';
        $this->load->view('backend/common/index', $data);
    }

    function delete_dgset() {

        $found = in_array("71", $this->arraay);
        if ($found) {
            $id = $_GET['dgset_id'];
            $details = $this->dashboard_model->delete_dgset($id);
            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function edit_dgset() {

        $found = in_array("70", $this->arraay);
        if ($found) {
            $data['master_log'] = $this->dashboard_model->get_master_fuel_stock();
            $id = $_POST['dgset_id1'];
            $data['rating'] = $this->dashboard_model->get_dg_rating();
            ///$query=$this->dashboard_model->fetch_dgset($id);

            $data['d'] = $this->dashboard_model->fetch_dgset($id);
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/edit_dgset';

            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function update_dgset() {

        $found = in_array("70", $this->arraay);
        if ($found) {
            $data['heading'] = 'DG set';
            $data['sub_heading'] = 'edit dgset';
            $data['description'] = 'Edit DG set here';
            $data['active'] = 'update_dgset';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {
                //$id_g = $this->input->post('g_contact_no');
                /* Images Upload */

                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('dg_image')) {

                    $asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/dg_set/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_asset');
                    }
                    $thumb_name = explode(".", $filename);
                    $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                }
                //echo $asset_thumb;
                //exit;
                /*  else
                  {
                  $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                  redirect('dashboard/add_asset');
                  } */

                /* Images Upload */
                date_default_timezone_set("Asia/Kolkata");
                $dgset_id = $this->input->post('dgset_id');

                $genname = $this->input->post('gen_name');
                $asset_code = $this->input->post('asset_code');
                $make = $this->input->post('make');
                $model = $this->input->post('model');
                $ratings = $this->input->post('ratings');
                $fuel = $this->input->post('fuel');
                $cylinder = $this->input->post('cylinder_choice');
                $rotation = $this->input->post('Rotation_choice');
                $design = $this->input->post('Design_choice');
                $alternator_make = $this->input->post('alternator_make');
                $fuel_consumption = $this->input->post('fuel_consumption');
                $status = $this->input->post('status');
                $amc = $this->input->post('amc');
                $cooling = $this->input->post('Design_choice');
                if ($asset_thumb) {
                    $image_record = $asset_thumb;
                } else {
                    $image_record = $this->input->post('image_record');
                }
                $dgset = array(
                    //'hotel_id' => $this->session->userdata('user_hotel'),
                    'dgset_id' => $dgset_id,
                    'gen_name' => $genname,
                    'asset_code' => $asset_code,
                    'make' => $make,
                    'model' => $model,
                    'ratings' => $ratings,
                    'fuel' => $fuel,
                    'cylinders' => $cylinder,
                    'rotation' => $rotation,
                    'design' => $design,
                    'alternator_make' => $alternator_make,
                    'fuel_consumption' => $fuel_consumption,
                    'status' => $status,
                    'amc' => $amc,
                    'cooling' => $cooling,
                    'img' => $image_record
                );

                //print_r($dgset);
                //exit();
                $query = $this->dashboard_model->update_dgset($dgset);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "DGset Updated Successfully!");
                    redirect('dashboard/all_dgset');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/all_dgset');
                }
            } else {
                $data['page'] = 'backend/dashboard/all_dgset';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function roomOccu() {


        $rid = $this->input->post('roomid');
        $rno = $this->input->post('roomno');
        $query = $this->dashboard_model->getOccu($rid, $rno);
        if ($query) {

            //echo "helloooo";
            //exit();
            //print_r($query);
            header('Content-Type: application/json');
            echo json_encode($query);
        } else {

            echo "alert";
        }
    }

    function usage_log() {
        $data['heading'] = 'DG set';
        $data['sub_heading'] = 'Add dgset Usage Log';
        $data['description'] = 'Add DG set Usage Log here';
        $data['active'] = 'all_usage_log';
        $query = $this->dashboard_model->fetch_gen('active');
        $usertypes = $this->dashboard_model->fetch_user();
        //print_r($value);
        //exit();
        $data['usertypes'] = $usertypes;
        $data['datas'] = $query;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/usage_log';

        $this->load->view('backend/common/index', $data);
    }

    function add_usage_log() {
        $data['datas'] = $this->dashboard_model->all_dgset('active');
        $data['usertypes'] = $this->dashboard_model->fetch_user();

        $found = in_array("74", $this->arraay);
        if ($found) {
            $data['heading'] = 'DG set';
            $data['sub_heading'] = 'Add dgset Usage Log';
            $data['description'] = 'Add DG set Usage Log here';
            $data['active'] = 'usage_log';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {
                $id_g = $this->input->post('g_contact_no');
                /* Images Upload */



                /* Images Upload */
                date_default_timezone_set("Asia/Kolkata");
                $nm_txt_val = $this->input->post('nm_txt_val');
                //$genname=$this->input->post('gen_name');
                $startdate = $this->input->post('startdate');
                $starttime = $this->input->post('starttime');
                $stopdate = $this->input->post('stopdate');
                $stoptime = $this->input->post('stoptime');
                $admin = $this->session->userdata('user_id');
                $totalhours = $this->input->post('totalhours');
                $fuelconsumption = number_format($this->input->post('fuelconsumption'), 2, '.', '');
                $o_fuel = $this->input->post('o_fuel');
                $c_fuel = $this->input->post('c_fuel');
                $differfuel = $this->input->post('differfuel');
                $fuel_name = $this->input->post('fuel_type');
                //  $vendor_id=$this->input->post('vendor_id');


                $usagelog = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'gen_name' => $nm_txt_val,
                    'startdate' => $startdate,
                    'starttime' => $starttime,
                    'stopdate' => $stopdate,
                    'stoptime' => $stoptime,
                    'admin' => $admin,
                    'totalhours' => number_format($totalhours, 2),
                    'fuelconsumption' => $fuelconsumption,
                    'openingfuel' => $o_fuel,
                    'closingfuel' => $c_fuel,
                    'differfuel' => $differfuel,
                        //'vendor_id' => $vendor_id,
                );




                //print_r($dgset);
                //exit();
                //$query = $this->dashboard_model->add_usage_log($usagelog);
                $query = $this->dashboard_model->add_usage_log($usagelog);
                if (isset($query) && $query) {

                    $add_sub = array(
                        'fuel_name' => $fuel_name,
                        'in_out_type' => 'Cons',
                        'quantity' => $fuelconsumption,
                        'o_time' => $stoptime,
                        'o_date' => $stopdate,
                        'price' => '',
                        'seller' => '',
                        'note' => 'Consumed by ' . $nm_txt_val . ' on ' . $stopdate . " For " . number_format($totalhours, 2) . " hours",
                        'pur_id' => '',
                        'sub_account' => $nm_txt_val,
                        'usage_log_id' => $query,
                    );
                    $up_array = array(
                        'fuel_name' => $fuel_name,
                        'quantity' => $fuelconsumption,
                        'in_out_type' => 'Cons',
                    );
                    //print_r( $add_sub); exit;
                    $query1 = $this->dashboard_model->add_sub_fuel($add_sub);
                    $this->dashboard_model->update_master_fuel($up_array);
                }
                if ($query1) {
                    $this->session->set_flashdata('succ_msg', "Usage Log Added Successfully!");
                    redirect('dashboard/all_usage_log');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/usage_log');
                }
            } else {
                $data['page'] = 'backend/dashboard/usage_log';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function all_usage_log() {
        $data['heading'] = 'DG set';
        $data['sub_heading'] = 'All usage log';
        $data['description'] = 'usage log  List';
        $data['active'] = 'all_usage_log';

        $data['usage_log'] = $this->dashboard_model->all_usage_log();
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/all_usage_log';
        $this->load->view('backend/common/index', $data);
    }

    function delete_usage_log() {
        $found = in_array("75", $this->arraay);
        if ($found) {
            $id = $_GET['u_id'];
            $det_type_qty = $this->dashboard_model->get_fuel_qty_type($id);
            //echo $id;
            //print_r ($det_type_qty);
            //echo $det_type_qty->quantity;
            //echo $det_type_qty->fuel_name;
            //echo $det_type_qty->in_out_type;
            //exit();
            if (($det_type_qty->in_out_type == 'sub') || ($det_type_qty->in_out_type == 'Cons')) {
                $type = 'add';
            } else {
                $type = 'sub';
            }
            //echo $type;
            //exit();

            $up_array = array(
                'fuel_name' => $det_type_qty->fuel_name,
                'quantity' => $det_type_qty->quantity,
                'in_out_type' => $type,
            );
            $this->dashboard_model->update_master_fuel($up_array);


            $res = $this->dashboard_model->delete_fuel_log_by_usage_id($id);
            $details = $this->dashboard_model->delete_usage_log($id);

            //print_r ($detail);
            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            redirect('dashboard');
        }
    }

    function stopwatch_usage_log() {
        $data['heading'] = 'DG set';
        $data['sub_heading'] = 'All usage log';
        $data['description'] = 'usage log  List';
        $data['active'] = 'stopwatch_usage_log';
        $query = $this->dashboard_model->fetch_gen('active');
        $usertypes = $this->dashboard_model->fetch_user();
        //print_r($value);
        //exit();
        $data['usertypes'] = $usertypes;
        $data['datas'] = $query;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/stopwatch_usage_log';

        $this->load->view('backend/common/index', $data);
    }

    function add_stopwatch_usage_log() { {
            if ($this->session->userdata('add_dgset') != '1') {
                $data['heading'] = ' DG set';
                $data['sub_heading'] = 'Add dgset Stopwatch Usage Log';
                $data['description'] = 'Add DG set Stopwatch Usage Log here';
                $data['active'] = 'stopwatch_usage_log';
                $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
                if ($this->input->post()) {
                    $id_g = $this->input->post('g_contact_no');
                    /* Images Upload */



                    /* Images Upload */
                    date_default_timezone_set("Asia/Kolkata");
                    $genname = $this->input->post('gen_name');
                    $startdate = $this->input->post('startdate');
                    $starttime = $this->input->post('starttime');
                    $stopdate = $this->input->post('stopdate');
                    $stoptime = $this->input->post('stoptime');
                    $admin = $this->input->post('admin');
                    $totalhours = $this->input->post('totalhours');
                    $fuelconsumption = $this->input->post('fuelconsumption');
                    $o_fuel = $this->input->post('o_fuel');
                    $c_fuel = $this->input->post('c_fuel');
                    $differfuel = $this->input->post('differfuel');



                    $usagelog = array(
                        //'hotel_id' => $this->session->userdata('user_hotel'),
                        'gen_name' => $genname,
                        'startdate' => $startdate,
                        'starttime' => $starttime,
                        'stopdate' => $stopdate,
                        'stoptime' => $stoptime,
                        'admin' => $admin,
                        'totalhours' => $totalhours,
                        'fuelconsumption' => $fuelconsumption,
                        'openingfuel' => $o_fuel,
                        'closingfuel' => $c_fuel,
                        'differfuel' => $differfuel,
                        'hotel_id' => $this->session->userdata('user_hotel'),
                        'user_id' => $this->session->userdata('user_id')
                    );

                    //print_r($dgset);
                    //exit();
                    $query = $this->dashboard_model->add_usage_log($usagelog);
                    if (isset($query) && $query) {
                        $this->session->set_flashdata('succ_msg', "Usage Log Added Successfully!");
                        redirect('dashboard/stopwatch_usage_log');
                    } else {
                        $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                        redirect('dashboard/stopwatch_usage_log');
                    }
                } else {
                    $data['page'] = 'backend/dashboard/usage_log';
                    $this->load->view('backend/common/index', $data);
                }
            } else {
                redirect('dashboard');
            }
        }
    }

    function chk_duplicate_fuel_type() {
        $type = $_POST['data'];
        //$type='test';
        $query = $this->dashboard_model->chk_duplicate_fuel_type($type);
        if ($query > 0) {
            $data = array(
                'data' => "1"
            );
        } else {
            $data = array(
                'data' => "0"
            );
        }

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function fetch_quantity() {
        $f_id = $_POST['fuel_id'];
        $this->db1->where('user_id', $this->session->userdata('user_id'));
        $this->db1->where('hotel_id', $this->session->userdata('user_hotel'));
        $query = $this->dashboard_model->fetch_quantity($f_id);
        if ($query) {
            $data = array(
                'reorder_level' => $query->reorder_level,
                'cur_stock' => $query->cur_stock,
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function add_sub_fuel_log() {
        if ($this->session->userdata('add_dgset') != '1') {
            $f_id = '';
            $usertypes = $this->dashboard_model->fetch_purchase_id();
            //print_r($value);
            //exit();
            $data['p_id'] = $usertypes;

            //print_r($value);
            //exit();
            $data['master_log'] = $this->dashboard_model->get_master_fuel_stock();
            //print_r($data);
            //exit;

            $data['heading'] = 'DG set';
            $data['sub_heading'] = 'Add/Sub fuel log';
            $data['description'] = '';
            $data['active'] = 'view_fuel_log';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['vendor'] = $this->dashboard_model->all_vendor();
			 $bil_no = $this->dashboard_model->get_lastPay();
			 $bill="HM0".$this->session->userdata('user_hotel')."-".++$bil_no; 	
            if ($this->input->get()) {
                $data['f_id'] = $_GET['f_id'];
            }



            if ($this->input->post()) {

                //$id_g = $this->input->post('g_contact_no');
                /* Images Upload */

				
                /* Images Upload */
                date_default_timezone_set("Asia/Kolkata");

                $fuel = split('~', $this->input->post('fuel'));

                $fuel_name = $fuel[0];
                $fuel_namenew = $fuel[1];
                //print_r($fuel_name); exit;
                $in_out_type = $this->input->post('mode');
                $quantity = $this->input->post('quantity');
                $o_time = $this->input->post('o_time');
                $o_date = $this->input->post('o_date');
                $price = $this->input->post('price');
                $seller = $_POST['vendor']['query'];
                $note = $this->input->post('note');
                $pu_id = $this->input->post('purchaseid');
                $sub_account = $this->input->post('sub_account');
                $vendor_id = $this->input->post('vendor_id');

                $unit_price = $price / $quantity;
				
				$bill_date =  date("Y-m-d H:i:s", strtotime($this->input->post('bill_date') . ' ' . date('H:i:s')));
                $bill_due_date = $this->input->post('bill_due_date') ;
                $payments_term = $this->input->post('payment_term');
                $delivery_date = $this->input->post('delivery_date');
                $specific_date = $this->input->post('specific_date');
                //	echo  "5456".$vendor_name; exit;
				if($payments_term=='cod'){
					$bill_due_date= $delivery_date;
				}elseif($payments_term=='other'){
					$bill_due_date= $specific_date;
				}

                $add_sub = array(
                    'fuel_name' => $fuel_name,
                    'in_out_type' => $in_out_type,
                    'quantity' => $quantity,
                    'o_time' => $o_time,
                    'o_date' => $o_date,
                    'price' => $price,
                    'seller' => $seller,
                    'note' => $note,
                    'pur_id' => $pu_id,
                    'sub_account' => $sub_account,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );

                $up_array = array(
                    'fuel_name' => $fuel_name,
                    'quantity' => $quantity,
                    'in_out_type' => $in_out_type,
                );

                $line_itmes = array(
                    'product' => $fuel_namenew,
                    'qty' => $quantity,
                    'price' => $unit_price,
                    'tax' => 0,
                    'discount' => 0,
                    'total' => $price,
                    'class' => '',
                    'note' => '',
                );
				//print_r($line_itmes);
                $add_pay = array(
                    'date' => date("Y-m-d H:i:s"),
                     'type_of_payment' => "Bill payments",
                    'hotel_id' => $this->session->userdata('user_hotel'),
					'user_id' => $this->session->userdata('user_id'),
                     'vendor_id' => $vendor_id,
					//'amount' => $price,
                    'bill_month' => "",
                     'bill_other' => " For Fuel purchase",
                    //'note_bill' =>$note_bill,
                    'person_name' => "",
                    'designation' => "",
                    'sal_month' => "",
                    'sal_other' => "",
                    'note_sal' => "",
                    'payment_to' => "",
                    'note_misc' => "",
                    'misc_month' => "",
                    'misc_other' => "",
                    'tax_month' => "",
                    'tax_other' => "",
                    'tax_type' => "",
                    //'tax_payment_to' => $tax_payment_to,
                    'description' => "",
                    'approved_by' => "",
                    'paid_by' => "",
                    'recievers_name' => "",
                    'recievers_desig' => "",
                    'admin' => $this->session->userdata('user_id'),
                    'profit_center' => "",
                    'note' => "",
                     'bill_date' => date("Y-m-d H:i:s", strtotime($this->input->post('bill_date') . ' ' . date('H:i:s'))),

					'bill_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                    'payment_term_date' =>date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
					'payment_term' => $payments_term,	
				    'ref_no' => 123,
                    'payments_due' => $this->input->post('price'),
                    'payments_status' => '0',
                    'payments_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                    'delivery_date' => date("Y-m-d H:i:s", strtotime($this->input->post('delivery_date') . ' ' . date('H:i:s'))),
                    'hotel_id' => $this->session->userdata('user_hotel'),
					'user_id' => $this->session->userdata('user_id'),
                    'bill_no' => $bill,
                );


              


                $query = $this->dashboard_model->add_sub_fuel($add_sub, $add_pay, $line_itmes,$in_out_type);
                $this->dashboard_model->update_master_fuel($up_array);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Fuel adjused successfully!");
                    redirect('dashboard/view_fuel_log');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_sub_fuel_log');
                }
            } else {

                $data['page'] = 'backend/dashboard/add_sub_fuel_log';

                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function fetch_purchase_id() {
        $usertypes = $this->dashboard_model->fetch_purchase_id();
        $data['p_id'] = $usertypes;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/add_sub_fuel_log';
        $this->load->view('backend/common/index', $data);
    }
	
	

    function add_bill_payments() {

		$this->load->library('upload');
        $this->load->library('image_lib');
        $data['heading'] = 'Finance';
        $data['description'] = 'Add Bill Payments';
        $data['active'] = 'all_billpayments';
        $usertypes = $this->dashboard_model->fetch_user();
        $data['usertypes'] = $usertypes;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['bil_no'] = $this->dashboard_model->get_lastPay();
		
        if ($this->input->post()) {
			$this->upload->initialize($this->set_upload_purchase());
                if ($this->upload->do_upload('purchase_doc')) {
                    $purchase = $this->upload->data('file_name');
					$type=explode(".", $purchase); 

					if($type[1]!='pdf'){
						
						$this->upload->initialize($this->set_upload_purchase());
						$filename = $purchase; 
                    $source_path = './upload/payments/orginal/purchase/' . $filename;
                    $target_path = './upload/payments/thumbnail/purchase/';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
				 $this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_bill_payments');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename); 
                    $purchase_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					} else {
						$config = array('upload_path' => './upload/upload/payments/orginal/purchase/',
										'allowed_types' => "pdf",
										'overwrite' => TRUE,);
						$this->upload->initialize($config);
						$this->upload->do_upload('purchase_doc');
						$up_file = $this->upload->data();
						$purchase_thumb = "";
					}	
				} else {
                    $purchase = "";
                    $purchase_thumb = "";
                }
				$this->upload->initialize($this->set_upload_otherDoc());
				if($this->upload->do_upload('other_doc')){
					 $other_doc = $this->upload->data('file_name');
					$type1=explode(".", $other_doc); 
						
					if($type1[1]!='pdf'){
						
						$this->upload->initialize($this->set_upload_purchase());
						$filename = $other_doc; 
                    $source_path = './upload/payments/orginal/other/' . $filename;
                    $target_path = './upload/payments/thumbnail/other/';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
				 $this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_bill_payments');
                    }
                   
                    $thumb_name = explode(".", $filename); 
                    $other_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					}else{
						$config = array('upload_path' => './upload/upload/payments/orginal/other/',
										'allowed_types' => "pdf",
										'overwrite' => TRUE,);
						$this->upload->initialize($config);
						$this->upload->do_upload('other_doc');
						$up_file = $this->upload->data();
						$other_thumb = "";
					}
				}else{
					$other_doc="";
					$other_thumb = "";
				}		
				
				
            date_default_timezone_set("Asia/Kolkata");
            $draft_bank_name = '';
            $ft_bank_name = '';
            $salpayfor='';
            $sal_month='';
            $sal_other='';
            $billother='';
            $bill_month='';
			$miscpayfor='';
			$payfortax='';
			 $misc_month='';
			 $misc_other='';
			 $sal_month='';
			 $tax_other='';
			 $tax_month='';
          $top = $this->input->post('top');
            $vendor_id = $this->input->post('vendor_id');
             $salpayfor=$this->input->post('salpayfor');
             $billpayfor=$this->input->post('billpayfor');
             $miscpayfor=$this->input->post('miscpayfor');
             $payfortax=$this->input->post('payfortax');
           if($billpayfor=='month'){
            $bill_month = $this->input->post('bill_month');
			 $bil_for = $this->input->post("bill_month");
		   }
			 if($billpayfor=='other'){	
            $billother = $this->input->post('billother');
			 $bil_for = $this->input->post("billother");
			 }
            $note_bill = $this->input->post('note_bill');
            $person_name = $this->input->post('person_name');
            $designation = $this->input->post('designation');
          
            if($salpayfor=='month'){
            $sal_month = $this->input->post('sal_month');
            }
             if($salpayfor=='other'){
             $sal_other = $this->input->post('sal_other');
             }
            $note_sal = $this->input->post('note_sal');
          $payment_to = $this->input->post('payment_to');
            $note_misc = $this->input->post('note_misc');
            if($miscpayfor=='month'){
			 $misc_month = $this->input->post('misc_month');
            }
			if($miscpayfor=='other'){
			$misc_other = $this->input->post('miscother');
            }
			 if($payfortax=='month'){
			// $misc_month = $this->input->post('misc_month');
			  $tax_month = $this->input->post('tax_month');
           
            }
			if($payfortax=='other'){
		//	$misc_other = $this->input->post('miscother');
		 $tax_other = $this->input->post('tax_other');
            }
			$payment_to1 = $this->input->post('payment_to1');
            $tax_type = $this->input->post('tax_type');
          //  $tax_payment_to = $this->input->post('tax_payment_to');
            $description = $this->input->post('description');
            $mop = $this->input->post('mop');

           $amount = $this->input->post('pay_amount');
		 //exit;
            $check_bank_name = $this->input->post('p_center');
            $draft_no = $this->input->post('pay_mode');

            //$card_type,$bankname,$card_no,$card_name,$card_exmp,$card_expy,$card_cvv,$card_payment_status
            //CARD INFO
            $chk_bank_name=$this->input->post('chk_bank_name');
						  $checkno=$this->input->post('checkno');
						  $chk_drawer_name=$this->input->post('chk_drawer_name');
						   $card_type=$this->input->post('card_type');
						  $card_no=$this->input->post('card_no');
						  $card_name=$this->input->post('card_name');
						  $card_expy=$this->input->post('card_expy');
						  $card_expm=$this->input->post('card_expm');
						  $card_czz=$this->input->post('card_cvv');
						  $card_exp_date=$card_expm.$card_expy;
						  $card_bank_name=$this->input->post('card_bank_name');
						  // draft  done
						  $draft_no=$this->input->post('draft_no');
						  $draft_bank_name=$this->input->post('draft_bank_name');
						  $draft_drawer_name=$this->input->post('draft_drawer_name');
						  
						  // fund  done
						  $ft_bank_name=$this->input->post('ft_bank_name');
						  $ft_account_no=$this->input->post('ft_account_no');
						  $ft_ifsc_code=$this->input->post('ft_ifsc_code');
						  
						  //wallet 
						    $wallet_name=$this->input->post('wallet_name');
						    $wallet_tr_id=$this->input->post('wallet_tr_id');
						    $wallet_rec_acc=$this->input->post('wallet_rec_acc');
						 
						  $ft_approved_by=$this->input->post('ft_approved_by');
						  $ft_recievers_name=$this->input->post('ft_recievers_name');
						  $ft_paid_by=$this->input->post('ft_paid_by');
						  $ft_recievers_desig=$this->input->post('ft_recievers_desig');
						  $profit_center1=$this->input->post('p_center');
						 $p_status=$this->input->post('p_status_w');
						  
						  if(isset($card_bank_name)){
							   $t_bank_name=$card_bank_name;
						  }else if(isset($draft_bank_name)){
							  $t_bank_name=$draft_bank_name;
						  }else if(isset($ft_bank_name)){
							  $t_bank_name=$ft_bank_name;
						  }else if(isset($chk_bank_name)){
							  $t_bank_name=$chk_bank_name;
						  }

            $approved_by = $this->input->post('approved_by');
            $paid_by = $this->input->post('paid_by');
            $recievers_name = $this->input->post('recievers_name');
            $recievers_desig = $this->input->post('recievers_desig');
            $admin = $this->session->userdata('user_id');
            $profit_center = $this->input->post('profit_center');
            $profit_center1 = $this->input->post('profit_center1');
            $pay_amt = $this->input->post("pay_amount");
            $payments_due1 = $this->input->post('payments_due');
		
            if ($payments_due1 == 0) {
                $pay_status = '1';
                $due_date = '';
            } else if ($pay_amt == 0) {
                $pay_status = '0';
                $due_date = date("Y-m-d H:i:s", strtotime($this->input->post('payments_due_date') . ' ' . date('H:i:s')));
            } else {
                $due_date = date("Y-m-d H:i:s", strtotime($this->input->post('payments_due_date') . ' ' . date('H:i:s')));
                $pay_status = '2';
            }
			

          // $payments_term = $this->input->post('payment_term');
			// $p_date=$this->input->post('bill_due_date');
			  $bill_date =  date("Y-m-d H:i:s", strtotime($this->input->post('bill_date') . ' ' . date('H:i:s')));
                $bill_due_date = $this->input->post('bill_due_date') ;
                $payments_term = $this->input->post('payment_term');
              
                $specific_date = $this->input->post('specific_date');
                //	echo  "5456".$vendor_name; exit;
				if($payments_term=='cod'){
					$bill_due_date= $delivery_date;
				}elseif($payments_term=='other'){
					$bill_due_date= $specific_date;
				}
			 if($top=="Bill payments"){
				  $delivery_date = date("Y-m-d H:i:s", strtotime($this->input->post('delivery_date'). ' ' . date('H:i:s')));
				
			}else{
				 $delivery_date="";
			}
        


            if ($billother != '') {
                $bil_for = $this->input->post("billother");
            } else {
                $bil_for = $this->input->post("bill_month");
            }

            if ($sal_other != '') {
                $sal_for = $sal_other;
				$sal_month='';
            } else {
                $sal_for = $sal_month;
				$sal_other='';
            }

            if ($misc_other != '') {
                $misc_for = $misc_other;
				$misc_month='';
            } else {
                $misc_for = $misc_month;
				$misc_other='';
            }
            if ($tax_other != '') {
                $tax_for = $tax_other;
				$tax_month='';
            } else {
                $tax_for = $tax_month;
				$tax_other='';
            }
            $line_itmes = array(
                'product' => $this->input->post("product1"),
                'qty' => $this->input->post("qty1"),
                'price' => $this->input->post("price1"),
                'tax' => $this->input->post("tax1"),
                'disc' => $this->input->post("disc1"),
                'total' => $this->input->post("total1"),
                'cls' => $this->input->post("cls1"),
                'note' => $this->input->post("note11"),
            );
		 $pay_details = array(
                //'amount'=>$this->input->post("pay_amount"),
                'amount' => $amount,
                'account' => '',
                'des' => '',
                //	'approved_by'=>'a',
                //	'paid_by'=>'a',
                'approved_by' => $approved_by,
                'paid_by' => $paid_by,
                'reciver_name' => $recievers_name,
                //'reciver_name'=>'a',
                //'designation'=>'a',
                'designation' => $recievers_desig,
                'due_amount' => $this->input->post("due_amount"),
                'hotel_id' => $this->session->userdata('user_hotel'),
                'user_id' => $this->session->userdata('user_id')
            );
            //	print_r($pay_details); exit;



            $add_pay = array(
                'hotel_id' => $this->session->userdata('user_hotel'),
                'user_id' => $this->session->userdata('user_id'),
              //  'date' => date("Y-m-d H:i:s", strtotime($this->input->post('date') . ' ' . date('H:i:s'))),
                'type_of_payment' => $top,
                'vendor_id' => $vendor_id,
                //'amount' => $this->input->post("amount"),
                'bill_month' => $bill_month,
                'bill_other' => $billother,
                //'note_bill' =>$note_bill,
                'person_name' => $person_name,
                'designation' => $designation,
                'sal_month' => $sal_month,
                'sal_other' => $sal_other,
                'note_sal' => "",
                'payment_to' => $payment_to1,
                'note_misc' => "",
                'misc_month' => $misc_month,
                'misc_other' => $misc_other,
                'tax_month' => $tax_month,
                'tax_other' => $tax_other,
                'tax_type' => $tax_type,
                //'tax_payment_to' => $tax_payment_to,
                'description' => $description,
                'approved_by' => $approved_by,
                'paid_by' => $paid_by,
                'recievers_name' => $recievers_name,
                'recievers_desig' => $recievers_desig,
                'admin' => $admin,
                'profit_center' => $profit_center,
                'note' => $this->input->post('memo'),
                'bill_date' => date("Y-m-d H:i:s", strtotime($this->input->post('bill_date') . ' ' . date('H:i:s'))),
                'bill_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                 'payment_term_date' =>date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                'payment_term' => $payments_term,
                'ref_no' => $this->input->post('ref_no'),
                'bill_no' => $this->input->post('bill_no'),
                'payments_due' => $this->input->post('payments_due'),
                'payments_status' => $pay_status,
                'payments_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                'delivery_date' => $delivery_date,
                'purchase_doc' => $purchase,
                'purchase_thumb' => $purchase_thumb,
                'other_doc' => $other_doc,
                'other_doc_thumb' => $other_thumb,
            );
            	
            $chk_lasltransaction = $this->dashboard_model->chk_lasltransaction();
            $chk_lasltransaction = $chk_lasltransaction + 1;
            $transaction_id = 'TA0' . $this->session->userdata('user_hotel') . "-" . $this->session->userdata('user_id') . "/" . date("d") . date("m") . date("y") . "/" . $chk_lasltransaction;

            if ($check_bank_name != "") {
                $t_bank_name = $check_bank_name;
                $draft_bank_name = "";
            } else if ($draft_bank_name != "") {
                $t_bank_name = $draft_bank_name;
                $check_bank_name = "";
            } else {
                $t_bank_name = $ft_bank_name;
            }
            if ($top == "Bill payments") {
                if ($vendor_id) {
                    $vendor_name = $this->dashboard_model->get_vendor_name($vendor_id);
                }
                if ($this->input->post('note') != '') {
                    $note = '( Payment To ' . $vendor_name->hotel_vendor_name . ' For ' . $bil_for . " Rs: " . $this->input->post("pay_amount") . ") -" . $this->input->post('note');
                } else {
                    $note = '( Payment To ' . $vendor_name->hotel_vendor_name . ' For ' . $bil_for . " Rs: " . $this->input->post("pay_amount") . ')';
                }

                //$t_transaction = array(




                $t_pay = array(
                    'actual_id' => $transaction_id,
                    'transaction_type' => "Bill Payments",
                    'transaction_type_id' => 5,
					
                    't_date' => date("Y-m-d H:i:s"),
                    'details_type' => "Bill Payments",
                    't_amount' => $amount,
                    't_payment_mode' => $mop,
                    't_card_type'=>$card_type,
						't_amount'=>$amount,
						't_payment_mode'=>$mop,
						
						'checkno'=>$checkno,
						't_bank_name'=>$t_bank_name,
						't_drw_name'=>$chk_drawer_name,
						
						't_card_no'=>$card_no,
						't_card_name'=>$card_name,
						't_card_exp_date'=>$card_exp_date,
						
						't_status'=>$p_status,
						//'t_card_cvv'=>$card_cvv,
						
						'draft_no'=>$draft_no,
						'draft_bank_name'=>$draft_bank_name,
						
						'ft_bank_name'=>$ft_bank_name,						
						'ft_account_no'=>$ft_account_no,
						'ft_ifsc_code'=>$ft_ifsc_code,
							 
						't_w_name'=>$wallet_name,
						't_tran_id'=>$wallet_tr_id,
						't_recv_acc'=>$wallet_rec_acc,
						
                    'transaction_releted_t_id' => "5",
                    'transaction_from_id' => "4",
                    'transaction_to_id' => "6",
                    'hotel_id' => $this->session->userdata('user_hotel'),
					  'user_id' => $this->session->userdata('user_id'),
                  //  't_status' => "Done",
                    'p_center' => $profit_center1,
                    'transactions_detail_notes' => $note
                ); 
				//print_r($t_pay); exit;
            } else if ($top == "Salary/Wage Payments") {

                if ($this->input->post('note') != '') {
                    $note = '( Payment To ' . $person_name . ' For ' . $sal_for . " Rs: " . $this->input->post("pay_amount") . ") -" . $this->input->post('note');
                } else {
                    $note = '( Payment To ' . $person_name . ' For ' . $sal_for . " Rs: " . $this->input->post("pay_amount") . ')';
                }

                $t_pay = array(
                    'actual_id' => $transaction_id,
                    'transaction_type' => "Salary/Wage Payments",
                    'transaction_type_id' => 7,
                    't_date' => date("Y-m-d H:i:s"),
                    'details_type' => "Salary/Wage Payments",
                    't_amount' => $this->input->post("pay_amount"),
                    't_payment_mode' => $mop,
                    't_card_type'=>$card_type,
						't_amount'=>$amount,
						't_payment_mode'=>$mop,
						
						'checkno'=>$checkno,
						't_bank_name'=>$t_bank_name,
						't_drw_name'=>$chk_drawer_name,
						
						't_card_no'=>$card_no,
						't_card_name'=>$card_name,
						't_card_exp_date'=>$card_exp_date,
						
						't_status'=>$p_status,
						//'t_card_cvv'=>$card_cvv,
						
						'draft_no'=>$draft_no,
						'draft_bank_name'=>$draft_bank_name,
						
						'ft_bank_name'=>$ft_bank_name,						
						'ft_account_no'=>$ft_account_no,
						'ft_ifsc_code'=>$ft_ifsc_code,
							 
						't_w_name'=>$wallet_name,
						't_tran_id'=>$wallet_tr_id,
						't_recv_acc'=>$wallet_rec_acc,
                    'transaction_releted_t_id' => "7",
                    'transaction_from_id' => "4",
                    'transaction_to_id' => "7",
                    'hotel_id' => $this->session->userdata('user_hotel'),
					  'user_id' => $this->session->userdata('user_id'),
                 //   't_status' => "Done",
                    'p_center' => $profit_center1,
                    'transactions_detail_notes' => $note
                );
            } else if ($top == "Miscellaneous Payments") {
                if ($this->input->post('note') != '') {
                    $note = '( Payment To ' . $payment_to . ' For ' . $misc_for . " Rs: " . $this->input->post("pay_amount") . ") -" . $this->input->post('note');
                } else {
                    $note = '( Payment To ' . $payment_to . ' For ' . $misc_for . " Rs: " . $this->input->post("pay_amount") . ')';
                }
                $t_pay = array(
                    'actual_id' => $transaction_id,
                    'transaction_type' => "Miscellaneous Payments",
                    'transaction_type_id' => 12,
                    't_date' => date("Y-m-d H:i:s"),
                    'details_type' => "Miscellaneous Payments",
                    't_amount' => $this->input->post("pay_amount"),
                    't_payment_mode' => $mop,
                    't_card_type'=>$card_type,
						't_amount'=>$this->input->post("pay_amount"),
						't_payment_mode'=>$mop,
						
						'checkno'=>$checkno,
						't_bank_name'=>$t_bank_name,
						't_drw_name'=>$chk_drawer_name,
						
						't_card_no'=>$card_no,
						't_card_name'=>$card_name,
						't_card_exp_date'=>$card_exp_date,
						
						't_status'=>$p_status,
						//'t_card_cvv'=>$card_cvv,
						
						'draft_no'=>$draft_no,
						'draft_bank_name'=>$draft_bank_name,
						
						'ft_bank_name'=>$ft_bank_name,						
						'ft_account_no'=>$ft_account_no,
						'ft_ifsc_code'=>$ft_ifsc_code,
							 
						't_w_name'=>$wallet_name,
						't_tran_id'=>$wallet_tr_id,
						't_recv_acc'=>$wallet_rec_acc,
                    'transaction_releted_t_id' => "12",
                    'transaction_from_id' => "4",
                    'transaction_to_id' => "11",
                    'hotel_id' => $this->session->userdata('user_hotel'),
					  'user_id' => $this->session->userdata('user_id'),
                   // 't_status' => "Done",
                    'p_center' => $profit_center1,
                    'transactions_detail_notes' => '( Payment To ' . $payment_to . ' For ' . $misc_for . ' Rs: ' . $this->input->post("pay_amount") . ") -" . $this->input->post('note')
                );
            } else if ($top == "Tax/Compliance Payment") {
                if ($this->input->post('note') != '') {
                    $note = '( Payment To ' . $tax_type . ' For ' . $tax_for . " Rs: " . $this->input->post("pay_amount") . ") -" . $this->input->post('note');
                } else {
                    $note = '( Payment To ' . $tax_type . ' For ' . $tax_for . " Rs: " . $this->input->post("pay_amount") . ')';
                }
                $t_pay = array(
                    'actual_id' => $transaction_id,
                    'transaction_type' => "Tax/Compliance Payment",
                    'transaction_type_id' => 4,
                    't_date' => date("Y-m-d H:i:s"),
                    'details_type' => "Tax/Compliance Payment",
                    't_amount' => $this->input->post("pay_amount"),
                    't_payment_mode' => $mop,
                    't_card_type'=>$card_type,
						't_amount'=>$amount,
						't_payment_mode'=>$mop,
						
						'checkno'=>$checkno,
						't_bank_name'=>$t_bank_name,
						't_drw_name'=>$chk_drawer_name,
						
						't_card_no'=>$card_no,
						't_card_name'=>$card_name,
						't_card_exp_date'=>$card_exp_date,
						
						't_status'=>$p_status,
					//	't_card_cvv'=>$card_cvv,
						
						'draft_no'=>$draft_no,
						'draft_bank_name'=>$draft_bank_name,
						
						'ft_bank_name'=>$ft_bank_name,						
						'ft_account_no'=>$ft_account_no,
						'ft_ifsc_code'=>$ft_ifsc_code,
							 
						't_w_name'=>$wallet_name,
						't_tran_id'=>$wallet_tr_id,
						't_recv_acc'=>$wallet_rec_acc,
                    'transaction_releted_t_id' => "4",
                    'transaction_from_id' => "4",
                    'transaction_to_id' => "5",
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                    //'t_status' => "done",
                    'p_center' => $profit_center1,
                    'transactions_detail_notes' => '( Payment To ' . $tax_type . ' For ' . $tax_for . ' Rs: ' . $this->input->post("pay_amount") . ") " . "-" . $this->input->post('note')
                );
            }
           

            $query = $this->dashboard_model->add_payments($add_pay, $t_pay, $line_itmes, $pay_details);

            if (isset($query) && $query) {
                $this->session->set_flashdata('succ_msg', "Bills Added Successfully!");
                redirect('dashboard/all_billpayments');
            } else {
                $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                redirect('dashboard/add_bill_payments');
            }
        } else {
            $data['page'] = 'backend/dashboard/add_bill_payments';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $this->load->view('backend/common/index', $data);
        }
    }

    function edit_bill_payments() {
        $data['heading'] = 'Finance';
        $data['description'] = 'Edit Bill Payments';
        $data['active'] = 'all_billpayments';
        //$data['transaction'] = $this->dashboard_model->all_transactions();
        $usertypes = $this->dashboard_model->fetch_user();
        $data['usertypes'] = $usertypes;
        // $data['vendor'] = $this->dashboard_model->all_vendor();
		$this->load->library('upload');
         $this->load->library('image_lib');
        if ($this->input->post()) {
           				
            date_default_timezone_set("Asia/Kolkata");
			$salpayfor='';
            $sal_month='';
            $sal_other='';
            $billother='';
            $bill_month='';
			$miscpayfor='';
			$payfortax='';
			 $misc_month='';
			 $misc_other='';
			 $sal_month='';
			 $sal_other='';
			 $tax_other='';
			 $tax_month='';
            $top = $this->input->post('hidVal');
            $pay_id = $this->input->post('payment_id');
			
            $vendor_id = $this->input->post('vendor_id');
		   $salpayfor=$this->input->post('salpayfor');
			$billpayfor=$this->input->post('billpayfor');
              $miscpayfor=$this->input->post('miscpayfor');
           $payfortax=$this->input->post('payfortax');
           if($billpayfor=='month'){
            $bill_month = $this->input->post('bill_month');
			 $bil_for = $this->input->post("bill_month");
		   }
			 if($billpayfor=='other'){	
            $billother = $this->input->post('billother');
			 $bil_for = $this->input->post("billother");
			 }
            $note_bill = $this->input->post('note_bill');
            $person_name = $this->input->post('person_name');
            $designation = $this->input->post('designation');
          
            if($salpayfor=='month'){
				$sal_month = $this->input->post('sal_month');
            }
             if($salpayfor=='other'){
				$sal_other = $this->input->post('sal_other'); 
             }
            $note_sal = $this->input->post('note_sal');
            $payment_to = $this->input->post('payment_to');
            $note_misc = $this->input->post('note_misc');
            if($miscpayfor=='month'){
			 $misc_month = $this->input->post('misc_month');
            }
			if($miscpayfor=='other'){
			$misc_other = $this->input->post('miscother');
            }
			 if($payfortax=='month'){
			// $misc_month = $this->input->post('misc_month');
			  $tax_month = $this->input->post('tax_month');
           
            }
			if($payfortax=='other'){
			//	$misc_other = $this->input->post('miscother');
		 $tax_other = $this->input->post('tax_other');
            }

            $note_sal = $this->input->post('note_sal');
            $payment_to = $this->input->post('payment_to');
            $note_misc = $this->input->post('note_misc');
			/*  $misc_month = $this->input->post('misc_month');
            $misc_other = $this->input->post('miscother');*/
           
            $tax_type = $this->input->post('tax_type');
			//  $tax_payment_to = $this->input->post('payment_to');
            $description = $this->input->post('description');
            $mop = $this->input->post('mop');
            $chk_bank_name=$this->input->post('chk_bank_name');
						  $checkno=$this->input->post('checkno');
						  $chk_drawer_name=$this->input->post('chk_drawer_name');
						  
						  // fund  done
						   $card_type=$this->input->post('card_type');
						  $card_no=$this->input->post('card_no');
						  $card_name=$this->input->post('card_name');
						  $card_expy=$this->input->post('card_expy');
						  $card_expm=$this->input->post('card_expm');
						  $card_czz=$this->input->post('card_cvv');
						  $card_exp_date=$card_expm.$card_expy;
						  $card_bank_name=$this->input->post('card_bank_name');
						  // draft  done
						  $draft_no=$this->input->post('draft_no');
						  $draft_bank_name=$this->input->post('draft_bank_name');
						  $draft_drawer_name=$this->input->post('draft_drawer_name');
						  
						  // fund  done
						  $ft_bank_name=$this->input->post('ft_bank_name');
						  $ft_account_no=$this->input->post('ft_account_no');
						  $ft_ifsc_code=$this->input->post('ft_ifsc_code');
						  
						  //wallet 
						    $wallet_name=$this->input->post('wallet_name');
						    $wallet_tr_id=$this->input->post('wallet_tr_id');
						    $wallet_rec_acc=$this->input->post('wallet_rec_acc');
						 
						  $ft_approved_by=$this->input->post('ft_approved_by');
						  $ft_recievers_name=$this->input->post('ft_recievers_name');
						  $ft_paid_by=$this->input->post('ft_paid_by');
						  $ft_recievers_desig=$this->input->post('ft_recievers_desig');
						  $profit_center1=$this->input->post('p_center');
						  $p_status=$this->input->post('p_status_w');
						  
						  if(isset($card_bank_name)){
							   $t_bank_name=$card_bank_name;
						  }else if(isset($draft_bank_name)){
							  $t_bank_name=$draft_bank_name;
						  }else if(isset($ft_bank_name)){
							  $t_bank_name=$ft_bank_name;
						  }else if(isset($chk_bank_name)){
							  $t_bank_name=$chk_bank_name;
						  }
		   
		   
            $approved_by = $this->input->post('approved_by');
            $paid_by = $this->input->post('paid_by');
            $recievers_name = $this->input->post('recievers_name');
            $recievers_desig = $this->input->post('recievers_desig');
            $admin = $this->session->userdata('user_id');
            $profit_center = $this->input->post('profit_center');
            $profit_center1 = $this->input->post('profit_center1');
            $pay_amt = $this->input->post("pay_amount");
            $payments_due1 = $this->input->post('payments_due');

            if ($payments_due1 == 0) {
                $pay_status = '1';
                $due_date = '';
            } else if ($pay_amt == 0) {
                $pay_status = '0';
                $due_date = date("Y-m-d H:i:s", strtotime($this->input->post('payments_due_date') . ' ' . date('H:i:s')));
            } else {
                $due_date = date("Y-m-d H:i:s", strtotime($this->input->post('payments_due_date') . ' ' . date('H:i:s')));
                $pay_status = '2';
            }
			
            $chk_lasltransaction = $this->dashboard_model->chk_lasltransaction();
            $chk_lasltransaction = $chk_lasltransaction + 1;
            $transaction_id = 'TA0' . $this->session->userdata('user_hotel') . "-" . $this->session->userdata('user_id') . "/" . date("d") . date("m") . date("y") . "/" . $chk_lasltransaction;


            $payments_term = $this->input->post('payment_term');
			$bill_date =  date("Y-m-d H:i:s", strtotime($this->input->post('bill_date') . ' ' . date('H:i:s')));
			$bill_due_date = $this->input->post('bill_due_date') ;
			
			$delivery_date = $this->input->post('delivery_date');
			$specific_date = $this->input->post('specific_date');
          if($payments_term=='cod'){
					$bill_due_date= $delivery_date;
				}elseif($payments_term=='other'){
					$bill_due_date= $specific_date;
				}
			 if($top=="Bill payments"){
				  $delivery_date = date("Y-m-d H:i:s", strtotime($this->input->post('delivery_date'). ' ' . date('H:i:s')));
				
			}else{
				 $delivery_date="";
			}

            $line_itmes = array(
                'id' => $this->input->post("line_id"),
                //'payment_id'=>$this->input->post("payment_id"),
                'product' => $this->input->post("product1"),
                'qty' => $this->input->post("qty1"),
                'price' => $this->input->post("price1"),
                'tax' => $this->input->post("tax1"),
                'disc' => $this->input->post("disc1"),
                'total' => $this->input->post("total1"),
                'cls' => $this->input->post("cls1"),
                'note' => $this->input->post("note11"),
            );
			//echo "<pre>";
			//print_r($line_itmes); exit;
            $pay_details = array(
                'amount' => $this->input->post("pay_amount"),
                'account' => '',
                'des' => '',
                'approved_by' => $this->input->post("approved_by"),
                'paid_by' => $this->input->post("paid_by"),
                'reciver_name' => $this->input->post("recievers_name"),
                'designation' => $this->input->post("recievers_desig"),
                'due_amount' => $this->input->post("due_amount"),
                'hotel_id' => $this->session->userdata('user_hotel'),
                'user_id' => $this->session->userdata('user_id')
            );
            //print_r($line_itmes); exit;
            //echo "<pre>";
            //print_r($pay_details); 
			//echo "<pre>";	
		   //print_r($line_itmes); exit;
			 $this->upload->initialize($this->set_upload_purchase());
			//$this->upload->initialize($this->set_upload_otherDoc());
                if($this->upload->do_upload('purchase_doc') && $this->upload->do_upload('other_doc')){
				if ($this->upload->do_upload('purchase_doc'))   {
                    /*  Image Upload 18.11.2015 */
					
                    $purchase = $this->upload->data('file_name');
					$type=explode(".", $purchase); 
						
					if($type[1]!='pdf'){
						//echo "heloo"; exit;
						$this->upload->initialize($this->set_upload_purchase());
						$filename = $purchase; 
                    $source_path = './upload/payments/orginal/purchase/' . $filename;
                    $target_path = './upload/payments/thumbnail/purchase/';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
				 $this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_bill_payments');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename); 
                    $purchase_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					}else{
						$config = array('upload_path' => './upload/upload/payments/orginal/purchase/',
										'allowed_types' => "pdf",
										'overwrite' => TRUE,);
						$this->upload->initialize($config);
						$this->upload->do_upload('purchase_doc');
						$up_file = $this->upload->data();
						$purchase_thumb = "";
					}
				  	 
					
					
                }
				$this->upload->initialize($this->set_upload_otherDoc());
				if($this->upload->do_upload('other_doc')){
					 $other_doc = $this->upload->data('file_name');
					$type1=explode(".", $other_doc); 
						
					if($type1[1]!='pdf'){
						//echo "heloo"; exit;
						$this->upload->initialize($this->set_upload_otherDoc());
						$filename = $other_doc; 
                    $source_path = './upload/payments/orginal/other/' . $filename;
                    $target_path = './upload/payments/thumbnail/other/';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
				 $this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_bill_payments');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename); 
                    $other_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					}else{
						$config = array('upload_path' => './upload/upload/payments/orginal/other/',
										'allowed_types' => "pdf",
										'overwrite' => TRUE,);
						$this->upload->initialize($config);
						$this->upload->do_upload('other_doc');
						$up_file = $this->upload->data();
						$other_thumb = "";
					}
				}
				$add_pay = array(
                'hotel_id' => $this->session->userdata('user_hotel'),
				'user_id' => $this->session->userdata('user_id'),
                //'date' => date("Y-m-d H:i:s", strtotime($this->input->post('date') . ' ' . date('H:i:s'))),
                'type_of_payment' =>$this->input->post("hidVal"),
                'vendor_id' => $vendor_id,
                //'amount' => $this->input->post("amount"),
                'bill_month' => $bill_month,
                'bill_other' => $billother,
                //'note_bill' =>$note_bill,
                'person_name' => $person_name,
                'designation' => $designation,
                'sal_month' => $sal_month,
                'sal_other' => $sal_other,
                // 'note_sal' => "",
                'payment_to' => $payment_to,
                //'note_misc' => "",
                'misc_month' => $misc_month,
                'misc_other' => $misc_other,
                'tax_month' => $tax_month,
                'tax_other' => $tax_other,
                'tax_type' => $tax_type,
                //'tax_payment_to' => $tax_payment_to,
                'description' => $description,
                'approved_by' => $approved_by,
                'paid_by' => $paid_by,
                'recievers_name' => $recievers_name,
                'recievers_desig' => $recievers_desig,
                'admin' => $admin,
                'profit_center' => $profit_center,
                'note' => $this->input->post('memo'),
                'bill_date' => date("Y-m-d H:i:s", strtotime($this->input->post('bill_date') . ' ' . date('H:i:s'))),
                'bill_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                 'payment_term_date' =>date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                'payment_term' => $payments_term,
                'ref_no' => $this->input->post('ref_no'),
                'bill_no' => $this->input->post('bill_no'),
                'payments_due' => $this->input->post('payments_due'),
                'payments_status' => $pay_status,
                'payments_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                'delivery_date' => $delivery_date,
				'purchase_doc' => $purchase,
                'purchase_thumb' => $purchase_thumb,
                'other_doc' => $other_doc,
                'other_doc_thumb' => $other_thumb,
            );
				}elseif ($this->upload->do_upload('purchase_doc'))   {
                    /*  Image Upload 18.11.2015 */
					
                    $purchase = $this->upload->data('file_name');
					$type=explode(".", $purchase); 
						
					if($type[1]!='pdf'){
						//echo "heloo"; exit;
						$this->upload->initialize($this->set_upload_purchase());
						$filename = $purchase; 
                    $source_path = './upload/payments/orginal/purchase/' . $filename;
                    $target_path = './upload/payments/thumbnail/purchase/';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
				 $this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_bill_payments');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename); 
                    $purchase_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					}else{
						$config = array('upload_path' => './upload/upload/payments/orginal/purchase/',
										'allowed_types' => "pdf",
										'overwrite' => TRUE,);
						$this->upload->initialize($config);
						$this->upload->do_upload('purchase_doc');
						$up_file = $this->upload->data();
						$purchase_thumb = "";
					}
				  	 
					$add_pay = array(
                'hotel_id' => $this->session->userdata('user_hotel'),
				'user_id' => $this->session->userdata('user_id'),
              //  'date' => date("Y-m-d H:i:s", strtotime($this->input->post('date') . ' ' . date('H:i:s'))),
                'type_of_payment' =>$this->input->post("hidVal"),
                'vendor_id' => $vendor_id,
                //'amount' => $this->input->post("amount"),
                'bill_month' => $bill_month,
                'bill_other' => $billother,
                //'note_bill' =>$note_bill,
                'person_name' => $person_name,
                'designation' => $designation,
                'sal_month' => $sal_month,
                'sal_other' => $sal_other,
                // 'note_sal' => "",
                'payment_to' => $payment_to,
                //'note_misc' => "",
                'misc_month' => $misc_month,
                'misc_other' => $misc_other,
                'tax_month' => $tax_month,
                'tax_other' => $tax_other,
                'tax_type' => $tax_type,
                //'tax_payment_to' => $tax_payment_to,
                'description' => $description,
                'approved_by' => $approved_by,
                'paid_by' => $paid_by,
                'recievers_name' => $recievers_name,
                'recievers_desig' => $recievers_desig,
                'admin' => $admin,
                'profit_center' => $profit_center,
                'note' => $this->input->post('memo'),
                'bill_date' => date("Y-m-d H:i:s", strtotime($this->input->post('bill_date') . ' ' . date('H:i:s'))),
                'bill_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                 'payment_term_date' =>date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                'payment_term' => $payments_term,
                'ref_no' => $this->input->post('ref_no'),
                'bill_no' => $this->input->post('bill_no'),
                'payments_due' => $this->input->post('payments_due'),
                'payments_status' => $pay_status,
                'payments_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                'delivery_date' => $delivery_date,
				'purchase_doc' => $purchase,
                'purchase_thumb' => $purchase_thumb,
                
            );
					
                }elseif($this->upload->do_upload('other_doc')){
					 $other_doc = $this->upload->data('file_name');
					$type1=explode(".", $other_doc); 
						
					if($type1[1]!='pdf'){
						//echo "heloo"; exit;
						$this->upload->initialize($this->set_upload_otherDoc());
						$filename = $other_doc; 
                    $source_path = './upload/payments/orginal/other/' . $filename;
                    $target_path = './upload/payments/thumbnail/other/';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
				 $this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_bill_payments');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename); 
                    $other_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					}else{
						$config = array('upload_path' => './upload/upload/payments/orginal/other/',
										'allowed_types' => "pdf",
										'overwrite' => TRUE,);
						$this->upload->initialize($config);
						$this->upload->do_upload('other_doc');
						$up_file = $this->upload->data();
						$other_thumb = "";
					}
					$add_pay = array(
                'hotel_id' => $this->session->userdata('user_hotel'),
				'user_id' => $this->session->userdata('user_id'),
               // 'date' => date("Y-m-d H:i:s", strtotime($this->input->post('date') . ' ' . date('H:i:s'))),
                'type_of_payment' =>$this->input->post("hidVal"),
                'vendor_id' => $vendor_id,
                //'amount' => $this->input->post("amount"),
                'bill_month' => $bill_month,
                'bill_other' => $billother,
                //'note_bill' =>$note_bill,
                'person_name' => $person_name,
                'designation' => $designation,
                'sal_month' => $sal_month,
                'sal_other' => $sal_other,
                // 'note_sal' => "",
                'payment_to' => $payment_to,
                //'note_misc' => "",
                'misc_month' => $misc_month,
                'misc_other' => $misc_other,
                'tax_month' => $tax_month,
                'tax_other' => $tax_other,
                'tax_type' => $tax_type,
                //'tax_payment_to' => $tax_payment_to,
                'description' => $description,
                'approved_by' => $approved_by,
                'paid_by' => $paid_by,
                'recievers_name' => $recievers_name,
                'recievers_desig' => $recievers_desig,
                'admin' => $admin,
                'profit_center' => $profit_center,
                'note' => $this->input->post('memo'),
                'bill_date' => date("Y-m-d H:i:s", strtotime($this->input->post('bill_date') . ' ' . date('H:i:s'))),
                'bill_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                 'payment_term_date' =>date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                'payment_term' => $payments_term,
                'ref_no' => $this->input->post('ref_no'),
                'bill_no' => $this->input->post('bill_no'),
                'payments_due' => $this->input->post('payments_due'),
                'payments_status' => $pay_status,
                'payments_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                'delivery_date' => $delivery_date,
				'other_doc' => $other_doc,
                'other_doc_thumb' => $other_thumb,
            );
				}else{
					$add_pay = array(
                'hotel_id' => $this->session->userdata('user_hotel'),
				'user_id' => $this->session->userdata('user_id'),
               // 'date' => date("Y-m-d H:i:s", strtotime($this->input->post('date') . ' ' . date('H:i:s'))),
                'type_of_payment' =>$this->input->post("hidVal"),
                'vendor_id' => $vendor_id,
                //'amount' => $this->input->post("amount"),
                'bill_month' => $bill_month,
                'bill_other' => $billother,
                //'note_bill' =>$note_bill,
                'person_name' => $person_name,
                'designation' => $designation,
                'sal_month' => $sal_month,
                'sal_other' => $sal_other,
                // 'note_sal' => "",
                'payment_to' => $payment_to,
                //'note_misc' => "",
                'misc_month' => $misc_month,
                'misc_other' => $misc_other,
                'tax_month' => $tax_month,
                'tax_other' => $tax_other,
                'tax_type' => $tax_type,
                //'tax_payment_to' => $tax_payment_to,
                'description' => $description,
                'approved_by' => $approved_by,
                'paid_by' => $paid_by,
                'recievers_name' => $recievers_name,
                'recievers_desig' => $recievers_desig,
                'admin' => $admin,
                'profit_center' => $profit_center,
                'note' => $this->input->post('memo'),
                'bill_date' => date("Y-m-d H:i:s", strtotime($this->input->post('bill_date') . ' ' . date('H:i:s'))),
                'bill_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                 'payment_term_date' =>date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                'payment_term' => $payments_term,
                'ref_no' => $this->input->post('ref_no'),
                'bill_no' => $this->input->post('bill_no'),
                'payments_due' => $this->input->post('payments_due'),
                'payments_status' => $pay_status,
                'payments_due_date' => date("Y-m-d H:i:s", strtotime($bill_due_date . ' ' . date('H:i:s'))),
                'delivery_date' => $delivery_date,
				//'other_doc' => $other_doc,
              //  'other_doc_thumb' => $other_thumb,
            );
				}

            
			//echo "<pre>";
          	//print_r($add_pay); exit;

           /* if ($check_bank_name != "") {
                $t_bank_name = $check_bank_name;
                $draft_bank_name = "";
            } else if ($draft_bank_name != "") {
                $t_bank_name = $draft_bank_name;
                $check_bank_name = "";
            } else {
                $t_bank_name = $ft_bank_name;
            }*/
            if ($top == "Bill payments") {
                if ($vendor_id) {
                    $vendor_name = $this->dashboard_model->get_vendor_name($vendor_id);
                }
                if ($this->input->post('note') != '') {
                    $note = '( Payment To ' . $vendor_name->hotel_vendor_name . ' For ' . $bil_for . " Rs: " . $this->input->post("pay_amount") . ") -" . $this->input->post('note');
                } else {
                    $note = '( Payment To ' . $vendor_name->hotel_vendor_name . ' For ' . $bil_for . " Rs: " . $this->input->post("pay_amount") . ')';
                }
                $t_pay = array(
                    'actual_id' => $transaction_id,
                    'transaction_type' => "Bill Payments",
                    'transaction_type_id' => 5,
                    't_date' => date("Y-m-d H:i:s"),
                    'details_type' => "Bill Payments",
                    't_amount' => $this->input->post("pay_amount"),
                    't_payment_mode' => $mop,
                    't_card_type'=>$card_type,
						't_amount'=>$pay_amt,
						't_payment_mode'=>$mop,
						
						'checkno'=>$checkno,
						't_bank_name'=>$t_bank_name,
						't_drw_name'=>$chk_drawer_name,
						
						't_card_no'=>$card_no,
						't_card_name'=>$card_name,
						't_card_exp_date'=>$card_exp_date,
						
						't_status'=>$p_status,
						//'t_card_cvv'=>$card_cvv,
						
						'draft_no'=>$draft_no,
						'draft_bank_name'=>$draft_bank_name,
						
						'ft_bank_name'=>$ft_bank_name,						
						'ft_account_no'=>$ft_account_no,
						'ft_ifsc_code'=>$ft_ifsc_code,
							 
						't_w_name'=>$wallet_name,
						't_tran_id'=>$wallet_tr_id,
						't_recv_acc'=>$wallet_rec_acc,
						
                    'transaction_releted_t_id' => "5",
                    'transaction_from_id' => "4",
                    'transaction_to_id' => "6",
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                  //  't_status' => "done",
                    'p_center' => $profit_center1,
                    'transactions_detail_notes' => $note
                );
              // print_r($t_pay); exit;
            } else if ($top == "Salary/Wage Payments") {

                if ($this->input->post('note') != '') {
                    $note = '( Payment To ' . $person_name . ' For ' . $sal_for . " Rs: " . $this->input->post("pay_amount") . ") -" . $this->input->post('note');
                } else {
                    $note = '( Payment To ' . $person_name . ' For ' . $sal_for . " Rs: " . $this->input->post("pay_amount") . ')';
                }

                $t_pay = array(
                    'actual_id' => $transaction_id,
                    'transaction_type' => "Salary/Wage Payments",
                    'transaction_type_id' => 7,
                    't_date' => date("Y-m-d H:i:s"),
                    'details_type' => "Salary/Wage Payments",
                   // 't_amount' => $this->input->post("pay_amount"),
                    't_card_type'=>$card_type,
						't_amount'=>$pay_amt,
						't_payment_mode'=>$mop,
						
						'checkno'=>$checkno,
						't_bank_name'=>$t_bank_name,
						't_drw_name'=>$chk_drawer_name,
						
						't_card_no'=>$card_no,
						't_card_name'=>$card_name,
						't_card_exp_date'=>$card_exp_date,
						
						't_status'=>$p_status,
						//'t_card_cvv'=>$card_cvv,
						
						'draft_no'=>$draft_no,
						'draft_bank_name'=>$draft_bank_name,
						
						'ft_bank_name'=>$ft_bank_name,						
						'ft_account_no'=>$ft_account_no,
						'ft_ifsc_code'=>$ft_ifsc_code,
							 
						't_w_name'=>$wallet_name,
						't_tran_id'=>$wallet_tr_id,
						't_recv_acc'=>$wallet_rec_acc,
                    'transaction_releted_t_id' => "7",
                    'transaction_from_id' => "4",
                    'transaction_to_id' => "7",
                    'hotel_id' => $this->session->userdata('user_hotel'),
                   // 't_status' => "done",
                    'p_center' => $profit_center1,
                    'transactions_detail_notes' => $note
                );
            } else if ($top == "Miscellaneous Payments") {
                if ($this->input->post('note') != '') {
                    $note = '( Payment To ' . $payment_to . ' For ' . $misc_for . " Rs: " . $this->input->post("pay_amount") . ") -" . $this->input->post('note');
                } else {
                    $note = '( Payment To ' . $payment_to . ' For ' . $misc_for . " Rs: " . $this->input->post("pay_amount") . ')';
                }
                $t_pay = array(
                    'actual_id' => $transaction_id,
                    'transaction_type' => "Miscellaneous Payments",
                    'transaction_type_id' => 12,
                    't_date' => date("Y-m-d H:i:s"),
                    'details_type' => "Miscellaneous Payments",
                    't_card_type'=>$card_type,
						't_amount'=>$pay_amt,
						't_payment_mode'=>$mop,
						
						'checkno'=>$checkno,
						't_bank_name'=>$t_bank_name,
						't_drw_name'=>$chk_drawer_name,
						
						't_card_no'=>$card_no,
						't_card_name'=>$card_name,
						't_card_exp_date'=>$card_exp_date,
						
						't_status'=>$p_status,
						//'t_card_cvv'=>$card_cvv,
						
						'draft_no'=>$draft_no,
						'draft_bank_name'=>$draft_bank_name,
						
						'ft_bank_name'=>$ft_bank_name,						
						'ft_account_no'=>$ft_account_no,
						'ft_ifsc_code'=>$ft_ifsc_code,
							 
						't_w_name'=>$wallet_name,
						't_tran_id'=>$wallet_tr_id,
						't_recv_acc'=>$wallet_rec_acc,
                    'transaction_releted_t_id' => "12",
                    'transaction_from_id' => "4",
                    'transaction_to_id' => "11",
                    'hotel_id' => $this->session->userdata('user_hotel'),
                 //   't_status' => "done",
                    'p_center' => $profit_center1,
                    'transactions_detail_notes' => '( Payment To ' . $payment_to . ' For ' . $misc_for . ' Rs: ' . $this->input->post("pay_amount") . ") -" . $this->input->post('note')
                );
            } else if ($top == "Tax/Compliance Payment") {
                if ($this->input->post('note') != '') {
                    $note = '( Payment To ' . $tax_type . ' For ' . $tax_for . " Rs: " . $this->input->post("pay_amount") . ") -" . $this->input->post('note');
                } else {
                    $note = '( Payment To ' . $tax_type . ' For ' . $tax_for . " Rs: " . $this->input->post("pay_amount") . ')';
                }
                $t_pay = array(
                    'actual_id' => $transaction_id,
                    'transaction_type' => "Tax/Compliance Payment",
                    'transaction_type_id' => 4,
                    't_date' => date("Y-m-d H:i:s"),
                    'details_type' => "Tax/Compliance Payment",
                    't_card_type'=>$card_type,
						't_amount'=>$pay_amt,
						't_payment_mode'=>$mop,
						
						'checkno'=>$checkno,
						't_bank_name'=>$t_bank_name,
						't_drw_name'=>$chk_drawer_name,
						
						't_card_no'=>$card_no,
						't_card_name'=>$card_name,
						't_card_exp_date'=>$card_exp_date,
						
						't_status'=>$p_status,
						//'t_card_cvv'=>$card_cvv,
						
						'draft_no'=>$draft_no,
						'draft_bank_name'=>$draft_bank_name,
						
						'ft_bank_name'=>$ft_bank_name,						
						'ft_account_no'=>$ft_account_no,
						'ft_ifsc_code'=>$ft_ifsc_code,
							 
						't_w_name'=>$wallet_name,
						't_tran_id'=>$wallet_tr_id,
						't_recv_acc'=>$wallet_rec_acc,
                    'transaction_releted_t_id' => "4",
                    'transaction_from_id' => "4",
                    'transaction_to_id' => "5",
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                //    't_status' => "done",
                    'p_center' => $profit_center1,
                    'transactions_detail_notes' => '( Payment To ' . $tax_type . ' For ' . $tax_for . ' Rs: ' . $this->input->post("pay_amount") . ") " . "-" . $this->input->post('note')
                );
            }
			//print_r($t_pay); exit;
            $query = $this->dashboard_model->update_payments($add_pay, $pay_id, $t_pay, $line_itmes, $pay_details);
            // print_r($query);die;

            if (isset($query) && $query) {
                $this->session->set_flashdata('succ_msg', "Bills Added Successfully!");
                redirect('dashboard/all_billpayments');
            }
        } else {

            $id = $_GET['p_id'];

            //echo $id;
            $data['bill_payments'] = $this->dashboard_model->fetch_bill_payments($id);
         
           if(isset($data['bill_payments'])){ 
				foreach($data['bill_payments'] as $type){
				 $type=$type->type_of_payment;
				}		   
            	
           
		   }
            //exit;
            //	print_r($data['bill_payments']); exit;
            // $data['vendor'] = $this->dashboard_model->all_vendor();
            $data['transaction'] = $this->dashboard_model->pay_transactions($id, $type);
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/edit_bill_payments';
            $this->load->view('backend/common/index', $data);
        }
    }

    function all_salpayments() {
        $data['heading'] = 'Finance';
        $data['description'] = 'Add Purchase Here';
        $data['active'] = 'all_salpayments';
        $usertypes = $this->dashboard_model->all_salpayments();
        //print_r($value);
        //exit();
        $data['salpay'] = $usertypes;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/all_salpayments';
        $this->load->view('backend/common/index', $data);
    }

    function delete_salpayments() {
        $id = $_GET['salpay_id'];
        $details = $this->dashboard_model->delete_salpayments($id);
        if (isset($details) && $details) {
            $this->session->set_flashdata('succ_msg', "Salary Deleted Successfully!");
            $usertypes = $this->dashboard_model->all_salpayments();
            //print_r($value);
            //exit();
            $data['salpay'] = $usertypes;
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/all_salpayments';
            $this->load->view('backend/common/index', $data);
        }
    }

    function all_misPayments() {

        $usertypes = $this->dashboard_model->all_mispayments();
        //print_r($value);
        //exit();
        $data['salpay'] = $usertypes;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/all_mispayments';
        $this->load->view('backend/common/index', $data);
    }

    function delete_mispayments() {
        $id = $_GET['mispay_id'];
        $details = $this->dashboard_model->delete_mispayments($id);
        if (isset($details) && $details) {
            $this->session->set_flashdata('succ_msg', "Miscelleneous payments Deleted Successfully!");
            $usertypes = $this->dashboard_model->all_mispayments();
            //print_r($value);
            //exit();
            $data['salpay'] = $usertypes;
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/all_mispayments';
            $this->load->view('backend/common/index', $data);
        }
    }

    function all_billPayments() {
        $data['heading'] = 'Finance';
        $data['description'] = 'All Bill Payments';
        $data['active'] = 'all_billpayments';
        $usertypes = $this->dashboard_model->all_billpayments();
        $data['salpay'] = $usertypes;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/all_billpayments';
        $this->load->view('backend/common/index', $data);
    }

    function delete_billpayments() {
        $id = $_GET['billpay_id'];
        $details = $this->dashboard_model->delete_billpayments($id);
        if (isset($details) && $details) {
            $this->session->set_flashdata('succ_msg', "Bills Deleted Successfully!");
            $usertypes = $this->dashboard_model->all_billpayments();
            //print_r($value);
            //exit();
            $data['salpay'] = $usertypes;
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/all_billpayments';
            $this->load->view('backend/common/index', $data);
        }
    }

    function all_taxPayments() {

        $usertypes = $this->dashboard_model->all_taxpayments();
        //print_r($value);
        //exit();
        $data['salpay'] = $usertypes;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/all_taxpayments';
        $this->load->view('backend/common/index', $data);
    }

    function all_expense_report() {
        $found = in_array("38", $this->arraay);
        if ($found) {
            $data['heading'] = 'Finance';
            $data['sub_heading'] = 'Reports';
            $data['description'] = '';
            $data['active'] = 'all_expense_report';
            $all_payments = $this->dashboard_model->all_expense_report();
            // print_r($usertypes);
            // exit();
            $data['allpayments'] = $all_payments;
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/expense_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function filter_expense_report() {
        $found = in_array("38", $this->arraay);
        if ($found) {
            $top = $this->input->post('top');
            $from_date = $this->input->post('f_date');
            $to_date = $this->input->post('t_date');

            // $filter=array(
            //         'top' => $top,
            //         'from_date' => $from_date,
            //         'to_date' => $to_date,
            //                 );
            //         print_r($filter);
            //                 exit();
            // print_r($fer);
            // die();
            ///echo 'here';
            //echo $top;
            //exit;
            $data['heading'] = 'Finance';
            $data['sub_heading'] = 'Reports';
            $data['description'] = '';
            $data['active'] = 'all_expense_report';
            $data['allpayments'] = $this->dashboard_model->filter_expense_report($top, $from_date, $to_date);
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/expense_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    // Hotel Reports





    function revpar_room_report() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            if ($this->input->post()) {
                $data['m'] = $this->input->post('month');
                $data['y'] = $this->input->post('year');
                $data['maxDays'] = cal_days_in_month(CAL_GREGORIAN, $data['m'], $data['y']);
            } else {
                $data['y'] = date("Y");
                $data['m'] = date("m");
                $data['maxDays'] = date('t');
            }
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Financial Reports';
            $data['description'] = '';
            $data['active'] = 'hotel_reports';
            //$all_payments=$this->dashboard_model->all_expense_report();
            // print_r($usertypes);
            // exit();
            //$data['allpayments']=$all_payments;
            $data['totalrooms'] = $this->dashboard_model->totalrooms();
            $data['revPar'] = $this->reports_model->revPar();
            //
            $x = count($data['totalrooms']);
            //die;
            $data['totalrooms'] = $x;

            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/rev_room_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function revpar_room_report_details() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            if ($this->input->post()) {
                $data['m'] = $this->input->post('month');
                $data['y'] = $this->input->post('year');
                $data['maxDays'] = cal_days_in_month(CAL_GREGORIAN, $data['m'], $data['y']);
            } else {
                $data['y'] = date("Y");
                $data['m'] = date("m");
                $data['maxDays'] = date('t');
            }
            $data['heading'] = 'Monthly Summary Report';
            $data['sub_heading'] = 'Reports';
            $data['description'] = '';
            $data['active'] = 'Monthly Summary Report';
            //$all_payments=$this->dashboard_model->all_expense_report();
            // print_r($usertypes);
            // exit();
            //$data['allpayments']=$all_payments;
            $data['totalrooms'] = $this->dashboard_model->totalrooms();
            //
            $x = count($data['totalrooms']);
            //die;
            $data['totalrooms'] = $x;

            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/rev_room_report_details';
            $this->load->view('backend/common/index', $data);
        } else {

            redirect('dashboard');
        }
    }

    //Deposit Report
    function deposit_report() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            if ($this->input->post()) {
                $data['m'] = $this->input->post('month');
                $data['y'] = $this->input->post('year');
                $data['maxDays'] = cal_days_in_month(CAL_GREGORIAN, $data['m'], $data['y']);
            } else {
                $data['y'] = date("Y");
                $data['m'] = date("m");
                $data['maxDays'] = date('t');
            }
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Financial Reports';
            $data['description'] = '';
            $data['active'] = 'deposit_report';
            //$all_payments=$this->dashboard_model->all_expense_report();
            // print_r($usertypes);
            // exit();
            //$data['allpayments']=$all_payments;
            $data['totalrooms'] = $this->dashboard_model->totalrooms();
            //
            $x = count($data['totalrooms']);
            //die;
            $data['totalrooms'] = $x;

            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/deposit_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    // lost item
    function add_lost_item() {



        $found = in_array("76", $this->arraay);
        if ($found) {

            $data['heading'] = 'Lost Item';
            $data['sub_heading'] = '';
            $data['description'] = 'Add lost items';
            $data['active'] = 'all_lost_item';
            $data['admin'] = $this->dashboard_model->fetch_user();
            $data['usage_log'] = $this->dashboard_model->all_usage_log();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/add_lost_item';
            $this->load->view('backend/common/index', $data);

            if ($this->input->post()) {


                /* Images Upload */
                date_default_timezone_set("Asia/Kolkata");
                $r_date = $this->input->post('reporting_date');
                $booking_id = $this->input->post('booking_id');
                $type = $this->input->post('type');
                $item_title = $this->input->post('item_title');
                $item_des = $this->input->post('item_des');
                $condition = $this->input->post('condition');
                $lost_in = $this->input->post('lost_in');
                $lost_date = $this->input->post('lost_date');
                $lost_time = $this->input->post('lost_time');
                $lost_by = $this->input->post('lost_by');
                $g_contact_no = $this->input->post('g_contact_no');
                // $admin_name=$this->input->post('admin_name');
                $uid = $this->dashboard_model->get_admin1($this->session->userdata('user_id'));

                $lost_item = array(
                    'reporting_date' => $r_date,
                    'booking_id' => $booking_id,
                    'type' => $type,
                    'item_title' => $item_title,
                    'item_des' => $item_des,
                    'condition' => $condition,
                    'lost_in' => $lost_in,
                    'lost_date' => $lost_date,
                    'lost_time' => $lost_time,
                    'lost_by' => $lost_by,
                    'g_contact_no' => $g_contact_no,
                    'admin_name' => $uid->admin_first_name . ' ' . $uid->admin_last_name,
                    'status' => 'lost',
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
                // $admin=$this->dashboard_model->fetch_admin();

                $query = $this->dashboard_model->add_lost_item($lost_item);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Lost Item Added Successfully!");
                    redirect('dashboard/all_lost_item');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_lost_item');
                }
            }
        } else {
            redirect('dashboard');
        }
    }

    function all_lost_item() {

        $found = in_array("76", $this->arraay);
        if ($found) {
            $data['heading'] = 'Lost Item';
            $data['sub_heading'] = 'All usage log';
            $data['description'] = 'usage log  List';
            $data['active'] = 'all_lost_item';

            $data['usage_log'] = $this->dashboard_model->all_usage_log();
            $data['lost_item'] = $this->dashboard_model->fetch_lost_item();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/all_lost_items';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function fetch_item_by_l_id() {

        $id = $_GET['l_id'];

        $query = $this->dashboard_model->fetch_f_id($id);
        $data['admin'] = $this->dashboard_model->fetch_admin();
        $data['lost_item'] = $this->dashboard_model->fetch_l_id($id);
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/edit_lost_item';

        $this->load->view('backend/common/index', $data);
    }

    function update_lost_item() {


        $found = in_array("77", $this->arraay);
        if ($found) {
            $data['heading'] = 'Lost_item';
            $data['sub_heading'] = 'Edit Lost Item';
            $data['description'] = 'Edit Lost here';
            $data['active'] = 'update';
            // $data['admin']=$this->dashboard_model->fetch_admin();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {
                //$id_g = $this->input->post('g_contact_no');
                /* Images Upload */


                $l_id = $this->input->post('l_id');
                $r_date = $this->input->post('reporting_date');
                $booking_id = $this->input->post('booking_id');
                $type = $this->input->post('type');
                $item_title = $this->input->post('item_title');
                $item_des = $this->input->post('item_des');
                $condition = $this->input->post('condition');
                $lost_in = $this->input->post('lost_in');
                $lost_date = $this->input->post('lost_date');
                $lost_time = $this->input->post('lost_time');
                $lost_by = $this->input->post('lost_by');
                $g_contact_no = $this->input->post('g_contact_no');
                $admin_name = $this->input->post('admin_name');

                $lost_item = array(
                    'l_id' => $l_id,
                    'reporting_date' => $r_date,
                    'booking_id' => $booking_id,
                    'type' => $type,
                    'item_title' => $item_title,
                    'item_des' => $item_des,
                    'condition' => $condition,
                    'lost_in' => $lost_in,
                    'lost_date' => $lost_date,
                    'lost_time' => $lost_time,
                    'lost_by' => $lost_by,
                    'g_contact_no' => $g_contact_no,
                    'admin_name' => $admin_name
                );



                //print_r($dgset);
                //exit();
                $query = $this->dashboard_model->update_lost_item($lost_item);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Updated Successfully!");
                    redirect('dashboard/all_lost_item');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/all_lost_item');
                }
            } else {
                $data['page'] = 'backend/dashboard/all_lost_item';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function delete_lost_item() {

        $found = in_array("78", $this->arraay);
        if ($found) {
            $id = $_GET['l_id'];
            $details = $this->dashboard_model->delete_l_item($id);
            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function found_item() {


        $found = in_array("79", $this->arraay);
        if ($found) {

            $data['heading'] = 'Lost Item';
            $data['sub_heading'] = '';
            $data['description'] = 'Add found items';
            $data['active'] = 'all_found_items';
            $data['admin'] = $this->dashboard_model->fetch_user();
            $data['usage_log'] = $this->dashboard_model->all_usage_log();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/found_item';
            $this->load->view('backend/common/index', $data);

            if ($this->input->post()) {


                /* Images Upload */
                date_default_timezone_set("Asia/Kolkata");
                $r_date = $this->input->post('reporting_date');
                // $booking_id=$this->input->post('booking_id');
                $type = $this->input->post('type');
                $item_title = $this->input->post('item_title');
                $item_des = $this->input->post('item_des');
                $condition = $this->input->post('condition');
                $found_in = $this->input->post('found_in');
                $found_date = $this->input->post('found_date');
                $found_time = $this->input->post('found_time');
                $found_by = $this->input->post('found_by');
                $g_contact_no = $this->input->post('g_contact_no');
                // $admin_name=$this->input->post('admin_name');
                $uid = $this->dashboard_model->get_admin1($this->session->userdata('user_id'));
                //echo "<pre>";
                //print_r($uid);
                $found_item = array(
                    'reporting_date' => $r_date,
                    //'booking_id'=>$booking_id,
                    'type' => $type,
                    'item_title' => $item_title,
                    'item_des' => $item_des,
                    'condition' => $condition,
                    'found_in' => $found_in,
                    'found_date' => $found_date,
                    'found_time' => $found_time,
                    'found_by' => $found_by,
                    'g_contact_no' => $g_contact_no,
                    'admin_name' => $uid->admin_first_name . ' ' . $uid->admin_last_name
                );
                //echo "<pre>";
                //	print_r($found_item);
//exit;

                $query = $this->dashboard_model->add_found_item($found_item);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Found Item Added Successfully!");
                    redirect('dashboard/all_found_items');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/found_item');
                }
            }
        } else {
            redirect('dashboard');
        }
    }

    function all_found_items() {

        $found = in_array("79", $this->arraay);
        if ($found) {
            $data['heading'] = 'Lost Item';
            $data['sub_heading'] = 'All usage log';
            $data['description'] = 'usage log  List';
            $data['active'] = 'all_found_items';

            $data['usage_log'] = $this->dashboard_model->all_usage_log();
            $data['found_item'] = $this->dashboard_model->fetch_found_item();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/all_found_items';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function fetch_item_by_id() {
        $id = $_GET['f_id'];

        $query = $this->dashboard_model->fetch_f_id($id);
        $data['admin'] = $this->dashboard_model->fetch_admin();
        $data['found_item'] = $this->dashboard_model->fetch_f_id($id);
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/edit_found_item';

        $this->load->view('backend/common/index', $data);
    }

    function delete_found_item() {

        $found = in_array("81", $this->arraay);
        if ($found) {

            $id = $_GET['f_id'];
            $details = $this->dashboard_model->delete_item($id);
            if (isset($details) && $details) {

                $data = array(
                    'data' => "sucess",
                );
                header('Content-Type: application/json');
                echo json_encode($data);
            }
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function fetch_item() {
        $item = $this->input->post("item");
        $itm_fnd = $this->dashboard_model->fetch_f_item($item);
        //print_r($itm_fnd);
        echo '<br/><br/><br/><ul>';
        if (isset($itm_fnd)) {
            foreach ($itm_fnd as $im) {
                ?>
                <li>
                    <a href="#" class="btn" style="background:#8DDCFF;" onclick="getVal('<?php echo $im['item_title'] . "-" . $im['f_id']; ?>')"><?php echo $im['item_title']; ?></a>&nbsp;
                    <b> <?php echo " Found by: " . $im['found_by']; ?></b>
                    <b> <?php echo ", on " . $im['found_date']; ?></b>
                </li>
            <?php
            }
        }
        echo '</ul>';
    }

    function fetch_lost_items() {
        $item = $this->input->post('item');


        $itm_fnd = $this->dashboard_model->fetch_lost_items($item);
        //echo "<pre>";
        //print_r($itm_fnd);
        //exit;

        if (isset($itm_fnd)) {
            echo '<ul>';
            foreach ($itm_fnd as $im) {
                ?>
                <!--<li>
                <a href="#" class="btn pink" onclick="getVal1('<?php echo $im['item_title'] . "-" . $im['l_id']; ?>')"><?php echo $im['item_title']; ?></a>

                &nbsp;<b><p> <?php echo $im['item_des'] . ", Lost by: " . $im['lost_by']; ?></b>
                <?php echo ", Lost Date: " . $im['lost_date']; ?></p>
                </li>-->
                <div class="">
                    <h2 class="search-title" style="font-size:20px">
                        <a href="#" class="search-desc" onclick="getVal1('<?php echo $im['item_title'] . "-" . $im['l_id']; ?>')"><?php echo $im['item_title']; ?></a>
                    </h2>
                    <p class="search-desc">
                        <a href="javascript:;"><?php echo $im['item_des'] . ", Lost by: " . $im['lost_by']; ?></a> -
                        <span class="font-grey-salt"><?php echo ", Lost Date: " . $im['lost_date']; ?></span>
                    </p>
                </div>
            <?php
            }
            echo '</ul>';
        } else {
            ?>

            <div class="mt-element-ribbon bg-grey-steel">
                <div class="ribbon ribbon-right ribbon-shadow ribbon-color-danger uppercase">No Items Found</div>
                <p class="ribbon-content">Please check other types or add Lost Items</p>
            </div>

            <?php
        }
    }

    function edit_found_item() {



        $found = in_array("80", $this->arraay);
        if ($found) {
            $data['heading'] = 'found_item';
            $data['sub_heading'] = 'Edit Found Item';
            $data['description'] = 'Edit Found here';
            $data['active'] = 'update';
            //$data['admin']=$this->dashboard_model->fetch_admin();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {
                //$id_g = $this->input->post('g_contact_no');
                /* Images Upload */


                $f_id = $this->input->post('f_id');
                $r_date = $this->input->post('reporting_date');
                //$booking_id=$this->input->post('booking_id');
                $type = $this->input->post('type');
                $item_title = $this->input->post('item_title');
                $item_des = $this->input->post('item_des');
                $condition = $this->input->post('condition');
                $found_in = $this->input->post('found_in');
                $found_date = $this->input->post('found_date');
                $found_time = $this->input->post('found_time');
                $found_by = $this->input->post('found_by');
                $g_contact_no = $this->input->post('g_contact_no');
                $admin_name = $this->input->post('admin_name');

                $found_item = array(
                    'f_id' => $f_id,
                    'reporting_date' => $r_date,
                    'status' => 'Not Released',
                    'type' => $type,
                    'item_title' => $item_title,
                    'item_des' => $item_des,
                    'condition' => $condition,
                    'found_in' => $found_in,
                    'found_date' => $found_date,
                    'found_time' => $found_time,
                    'found_by' => $found_by,
                    'g_contact_no' => $g_contact_no,
                    'admin_name' => $admin_name
                );



                //print_r($dgset);
                //exit();
                $query = $this->dashboard_model->update_found_item($found_item);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Found Item edited successfully!");
                    redirect('dashboard/all_found_items');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/all_found_items');
                }
            } else {
                $data['page'] = 'backend/dashboard/all_found_items';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function all_release_item() {
        $data['heading'] = 'Lost Item';
        $data['sub_heading'] = '';
        $data['description'] = 'Item release List';
        $data['active'] = 'all_release_item';

        $data['usage_log'] = $this->dashboard_model->all_usage_log();
        $data['r_item'] = $this->dashboard_model->fetch_release_item();
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/all_release_item';
        $this->load->view('backend/common/index', $data);
    }

    function release_item() {

        $found = in_array("79", $this->arraay);
        if ($found) {
            $data['heading'] = 'Lost Item';
            $data['sub_heading'] = '';
            $data['description'] = 'Release item  List';
            $data['active'] = 'all_release_item';
            $data['usage_log'] = $this->dashboard_model->all_usage_log();
            $data['found_item'] = $this->dashboard_model->fetch_found_item();
            //$data['f_type']=$this->dashboard_model->fetch_item_type()
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/release_item';
            $this->load->view('backend/common/index', $data);

            if ($this->input->post()) {


                /* Images Upload */
                date_default_timezone_set("Asia/Kolkata");
                $date = $this->input->post('date');
                $booking_id = $this->input->post('booking_id');
                $item = $this->input->post('item');
                $release_by = $this->input->post('release_by');
                $release_date = $this->input->post('release_date');
                $release_time = $this->input->post('release_time');
                $claim_by = $this->input->post('claim_by');
                $g_contact_no = $this->input->post('g_contact_no');
                $f_id = $this->input->post('select_f_itm_id');
                $l_id = $this->input->post('select_l_itm_id');

                $r_item = array(
                    'date' => $date,
                    //'booking_id'=>$booking_id,
                    'booking_id' => $booking_id,
                    'item' => $item,
                    'release_by' => $release_by,
                    'release_date' => $release_date,
                    'release_time' => $release_time,
                    'claim_by' => $claim_by,
                    'status' => "Released",
                    'g_contact_no' => $g_contact_no,
                    'f_id' => $f_id,
                    'l_id' => $l_id,
                    'hotel_id' =>$this->session->userdata('user_hotel')
                );
				//echo "<pre>";
                //print_r($r_item);
            // /  exit;
                $query = $this->dashboard_model->release_item($r_item);
                $query2 = $this->dashboard_model->update_f_item($f_id);
                $query3 = $this->dashboard_model->update_l_item($l_id);

                if (isset($query)) {
                    $this->session->set_flashdata('succ_msg', "Item Released successfully!");
                    redirect('dashboard/all_release_item');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/all_release_item');
                }
            }
        } else {
            redirect('dashboard');
        }
    }

    function search_charge() {
        $keyword = $this->input->post("keyword");
        //$keyword=$_GET["key"];
        $result = $this->dashboard_model->search_charge($keyword);

        $st = "";
        if ($result != false) {
            foreach ($result as $key) {
                $st .= "<li class='list-group-item'><a  onClick=\"selectCountry('" . $key->charge_name . "','" . $key->unit_price . "','" . $key->charge_tax . "','" . $key->unit_price_editable . "','" . $key->tax_editable . "');\">" . $key->charge_name . "</a></li>";
            }
            if ($st != "") {
                echo "<ul class='list-group'>" . $st . "</ul>";
            } else {
                echo $st;
            }
        } else {
            echo $st;
        }
    }

    function new_search_charge() {


        $keyword = $this->input->post("keyword");
        //$keyword=$_GET["key"];
        $result = $this->dashboard_model->search_charge($keyword);

        //$st= "";
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    function new_search_service() {


        $keyword = $this->input->post("keyword");
        //$keyword=$_GET["key"];
        $result = $this->dashboard_model->search_service($keyword);

        //$st= "";
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    function search_charge_autocomplete() {

        $query = (!empty($_GET['q'])) ? strtolower($_GET['q']) : null;
        $status = true;
        $result = $this->dashboard_model->search_charge_auto();
        foreach ($result as $rs) {

            $charge[] = array(
                'id' => $rs['hotel_extra_charges_id'],
                'name' => $rs['charge_name'],
                'unitprice' => $rs['unit_price'],
                'icon' => $rs['extra_charge_tag_icon'],
                'priceEditable' => $rs['unit_price_editable'],
                'taxEditable' => $rs['tax_editable'],
                'tax' => $rs['charge_tax'],	
                'cgst' => $rs['charge_cgst'],
                'sgst' => $rs['charge_sgst'],
                'igst' => $rs['charge_igst'],
                'ugst' => $rs['charge_ugst'],
                 'sac_code' => $rs['hsn_sac'],
            );
        }
        $resultProjects = [];

        foreach ($charge as $key => $oneProject) {
            if (strtolower($oneProject["name"]) !== false) {
                $resultProjects[] = $oneProject;
            }
        }

        if (empty($resultProjects)) {
            $status = false;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            "status" => $status,
            "error" => null,
            "data" => array(
                "name" => $resultProjects
            )
        ));
    }

    function search_service_autocomplete() {
        $query = (!empty($_GET['q'])) ? strtolower($_GET['q']) : null;
        $status = true;
        $result = $this->dashboard_model->search_service_auto();
        foreach ($result as $rs) {

            $service[] = array(
                'id' => $rs['s_id'],
                'name' => $rs['s_name'],
                'unitprice' => $rs['s_price'],
                'tax' => $rs['s_tax'],
                'taxApplied' => $rs['s_tax_applied'],
                'image' => base_url() . "upload/service/" . $rs['s_image'],
            );
        }
        $resultProjects = [];

        foreach ($service as $key => $oneProject) {
            if (strtolower($oneProject["name"]) !== false) {
                $resultProjects[] = $oneProject;
            }
        }

        if (empty($resultProjects)) {
            $status = false;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            "status" => $status,
            "error" => null,
            "data" => array(
                "name" => $resultProjects
            )
        ));
    }

    function search_service() {
        $keyword = $this->input->post("data");
        //$keyword=$_GET["key"];
        $result = $this->dashboard_model->search_service($keyword);
        //print_r($result); exit;
        $st = "<ul class='list-group'>";

        if ($result != false) {
            foreach ($result as $key) {
                $st .= "<li class='list-group-item'><a onClick=\"select_service('" . $key->s_name . "','" . $key->s_price . "','" . $key->s_tax . "','" . $key->s_id . "');\">" . $key->s_name . "</a></li>";
            }
        }

        echo $st . "</ul>";
    }

    function edit_release_item() {
        $id = $_GET['r_id'];
        //$query=$this->dashboard_model->fetch_f_id($id);
        //  $data['admin']=$this->dashboard_model->fetch_admin();
        $data['r_item'] = $this->dashboard_model->fetch_r_id($id);
        //print_r($data);
        //exit;

        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/edit_release_item';

        $this->load->view('backend/common/index', $data);
    }

    function update_release_item() {

        $found = in_array("80", $this->arraay);
        if ($found) {

            if ($this->input->post()) {


                /* Images Upload */
                date_default_timezone_set("Asia/Kolkata");
                $date = $this->input->post('date');
                $booking_id = $this->input->post('booking_id');
                $item = $this->input->post('item');
                $release_by = $this->input->post('release_by');
                $release_date = $this->input->post('release_date');
                $release_time = $this->input->post('release_time');
                $claim_by = $this->input->post('claim_by');
                $g_contact_no = $this->input->post('g_contact_no');


                $r_item = array(
                    'hotel_release_item_id' => $this->input->post('r_id'),
                    'date' => $date,
                    //'booking_id'=>$booking_id,
                    'booking_id' => $booking_id,
                    'item' => $item,
                    'release_by' => $release_by,
                    'release_date' => $release_date,
                    'release_time' => $release_time,
                    'claim_by' => $claim_by,
                    'g_contact_no' => $g_contact_no,
                );

                //print_r($r_item);

                $query = $this->dashboard_model->update_release_item($r_item);
                // exit;

                if (isset($query)) {
                    $this->session->set_flashdata('succ_msg', "Release Item updated successfully!");
                    redirect('dashboard/all_release_item');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/all_release_item');
                }
            }
        } else {
            redirect('dashboard');
        }
    }

    function delete_r_item() {

        $found = in_array("81", $this->arraay);
        if ($found) {
            $id = $_GET['r_id'];

            $details = $this->dashboard_model->delete_r_item($id);
            if (isset($details) && $details) {

                $data = array(
                    'data' => "sucess",
                );
                header('Content-Type: application/json');
                echo json_encode($data);
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Corporate */

    function all_corporate() {

        $data['heading'] = 'Guests';
        $data['sub_heading'] = 'All Corporate';
        $data['description'] = 'usage log  List';
        $data['active'] = 'all_corporate';

        $data['usage_log'] = $this->dashboard_model->all_usage_log();
        $data['corporate'] = $this->dashboard_model->fetch_corporate();
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/all_corporate';
        $this->load->view('backend/common/index', $data);
    }

    function add_corporate() {


        $found = in_array("7", $this->arraay);
        if ($found) {

            $data['heading'] = 'Guests';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Corporate';
            $data['active'] = 'all_corporate';
            $data['usage_log'] = $this->dashboard_model->all_usage_log();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/add_corporate';
            $this->load->view('backend/common/index', $data);

            if ($this->input->post()) {
                /* Images Upload */
                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('vendor_image')) {

                    $asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/corporate/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_corporate');
                    }
                    $thumb_name = explode(".", $filename);
                    $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                } else {
                    // $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    //redirect('dashboard/add_asset');
                    $source_path = './upload/';
                    $target_path = './upload/corporate/';
                    $asset_thumb = 'no_images.png';
                }
                date_default_timezone_set("Asia/Kolkata");
                $company_name = $this->input->post('company_name');
                //echo $company_name; exit;
                $reg_type = $this->input->post('reg_type');
                $address = $this->input->post('address');
                $pin_code = $this->input->post('pin_code');
                $city = $this->input->post('city');
                $state = $this->input->post('state');
                $country = $this->input->post('country');
                $contact_no = $this->input->post('contact_no');
                $contatc_person = $this->input->post('contatc_person');
                $manager_name = $this->input->post('manager_name');
                $manager_contact_no = $this->input->post('manager_no');
                $director_no = $this->input->post('director_no');
                $director_name = $this->input->post('director_name');
                $no_branch = $this->input->post('no_branch');
                $employees_no = $this->input->post('employees_no');
                $field = $this->input->post('field');
                $rating = $this->input->post('rating');
                $c_start_date = $this->input->post('contract_start_date');
                $c_end_date = $this->input->post('contract_end_date');
                $c_discount_start = $this->input->post('contract_discount_start');
                $c_default_apply = $this->input->post('default_rule');
                $corp_email = $this->input->post('corp_email');
                $corp_manager_email = $this->input->post('corp_manager_email');
                $corp_director_email = $this->input->post('corp_director_email');
                $industry_type = $this->input->post('industry_type');
                $gstin = $this->input->post('gstin');

                //print_r($c_default_apply);exit;
                //$c_discount_end=$this->input->post('contract_discount_end');

                $corporate_discount = array(
                    'contract_start_date' => $c_start_date,
                    'contract_end_date' => $c_end_date,
                    'contract_discount_start' => $c_discount_start,
                    'default_apply' => $c_default_apply,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                );
                /* foreach( $corporate_discount['contract_start_date'] as $index => $code ) {
                  //echo  $corporate_discount['contract_end_date'][$index] ;
                  echo $corporate_discount['default_apply'][$code];
                  }
                  exit; */

                $corporate = array(
                    'name' => $company_name,
                    'reg_type' => $reg_type,
                    'address' => $address,
                    'pin_code' => $pin_code,
                    'city' => $city,
                    'state' => $state,
                    'country' => $country,
                    'g_contact_no' => $contact_no,
                    'corp_email' => $corp_email,
                    'contact_person' => $contatc_person,
                    'manager_name' => $manager_name,
                    'manager_contact_no' => $manager_contact_no,
                    'corp_manager_email' => $corp_manager_email,
                    'director_contact_no' => $director_no,
                    'director_name' => $director_name,
                    'corp_director_email' => $corp_director_email,
                    //'industry'=>$industry,
                    'number_branch' => $no_branch,
                    'no_of_emp' => $employees_no,
                    //'no_of_booking'=>$booking_no,
                    // 'total_transaction'=>$total_transaction,
                    //'total_amt_pending'=>$amount_pending,
                    'industry_type' => $industry_type,
                    'field' => $field,
                    'rating' => $rating,
                    'gstin' => $gstin,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
                //print_r($corporate_discount);exit;

                $query = $this->dashboard_model->add_corporate($corporate, $corporate_discount);



                if (isset($query) && $query) {

                    $this->session->set_flashdata('succ_msg', "Corporate Added Successfully!");
                    redirect('dashboard/all_corporate');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Please Contact the Admin!");
                    redirect('dashboard/add_corporate');
                }
            }
        } else {
            redirect('dashboard');
        }
    }

    function fetch_item_by_c_id() {
        $id = $_GET['c_id'];

        $query = $this->dashboard_model->fetch_c_id($id);
        $data['corporate'] = $this->dashboard_model->fetch_c_id($id);
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/edit_corporate';

        $this->load->view('backend/common/index', $data);
    }

    function update_corporate() {

        $found = in_array("8", $this->arraay);
        if ($found) {
            $data['heading'] = 'corporate';
            $data['sub_heading'] = 'Edit Corporate';
            $data['description'] = 'Edit Corporate here';
            $data['active'] = 'update';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {
                //$id_g = $this->input->post('g_contact_no');
                /* Images Upload */


                $c_id = $this->input->post('c_id');
                $name = $this->input->post('name');
                //$booking_id=$this->input->post('booking_id');
                $address = $this->input->post('address');
                $g_contact_no = $this->input->post('g_contact_no');
                /// $industry=$this->input->post('industry');
                //  $no_of_emp=$this->input->post('no_of_emp');
                $contract_discount = $this->input->post('contract_discount');
                $contract_end_date = $this->input->post('contract_end_date');
                $no_of_booking = $this->input->post('no_of_booking');
                $total_transaction = $this->input->post('total_transaction');

                $corporate = array(
                    'hotel_corporate_id' => $c_id,
                    'name' => $name,
                    'address' => $address,
                    'g_contact_no' => $g_contact_no,
                    // 'industry'=>$industry,
                    //'no_of_emp'=>$no_of_emp,
                    'contract_discount' => $contract_discount,
                    'contract_end_date' => $contract_end_date,
                    'no_of_booking' => $no_of_booking,
                    'total_transaction' => $total_transaction
                );



                //print_r($corporate);
                //  exit();
                $query = $this->dashboard_model->update_corporate($corporate);


                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Corporation updated successfully!");
                    redirect('dashboard/all_corporate');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Please Try again later!");
                    redirect('dashboard/all_corporate');
                }
            } else {
                $data['page'] = 'backend/dashboard/all_found_items';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function check_duplicate_room() {
        //$room_no=$_GET["room_no"];
        $room_no = $this->input->post("room_no");
        $query = $this->dashboard_model->check_duplicate_room($this->session->userdata('user_hotel'), $room_no);

        if ($query > 0) {
            $data = array(
                'data' => "1",
            );
        } else {

            $data = array(
                'data' => "0",
            );
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function check_duplicate_room_name() {
        $room_name = $this->input->post("room_name");
        $query = $this->dashboard_model->check_duplicate_room_name($this->session->userdata('user_hotel'), $room_name);

        if ($query > 0) {
            $data = array(
                'data' => "1",
            );
        } else {

            $data = array(
                'data' => "0",
            );
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function delete_corporate() {

        $found = in_array("9", $this->arraay);
        if ($found) {
            $id = $_GET['c_id'];
            $details = $this->dashboard_model->delete_corporate($id);
            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'data' => "You do not have the previlage to do this",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function fetch_event() {
        $id = $_GET['e_id'];

        $query = $this->dashboard_model->fetch_e_id($id);
        //$data['admin']=$this->dashboard_model->fetch_admin();
        $data['event'] = $this->dashboard_model->fetch_e_id($id);
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/edit_events';

        $this->load->view('backend/common/index', $data);
    }

    function update_event() {

        $found = in_array("46", $this->arraay);
        if ($found) {
            $data['heading'] = 'Events';
            $data['sub_heading'] = 'Edit Event';
            $data['description'] = 'Add Event Here';
            $data['active'] = 'add_event';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                date_default_timezone_set("Asia/Kolkata");

                if ($this->input->post('e_seasonal') != null) {
                    $seasonal = $this->input->post('e_seasonal');
                } else {
                    $seasonal = '0';
                }
                $event = array(
                    'e_id' => $this->input->post('e_id'),
                    'e_name' => $this->input->post('e_name'),
                    'e_from' => date("y-m-d", strtotime($this->input->post('e_from'))),
                    'e_upto' => date("y-m-d", strtotime($this->input->post('e_upto'))),
                    'e_notify' => $this->input->post('e_notify'),
                    'e_seasonal' => $seasonal,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'event_color' => $this->input->post('e_event_color'),
                    'event_text_color' => $this->input->post('e_event_text_color')
                );
                //print_r($event);
                //exit();
                $query = $this->dashboard_model->update_event($event);

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Event updated successfully!");
                    redirect('dashboard/all_events');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Please Try again later!");
                    redirect('dashboard/all_events');
                }
            } else {
                $data['page'] = 'backend/dashboard/add_event';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    //1.04.2016
    function bookinglog() {
        $data['bookinglog'] = $this->dashboard_model->booking_details();

        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/bookinglog';

        $this->load->view('backend/common/index', $data);
    }

    //4.03.2016
    function guestlog($date = 0) {
        if (!empty($date)) {
            $data['guestlog'] = $this->dashboard_model->booking_details_forguest(array('booking_taking_date' => $date));
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/guest_log';
            $this->load->view('backend/common/index', $data);
        }
    }

//edit service

    function edit_service() {



        $found = in_array("55", $this->arraay);
        if ($found) {
            $data['heading'] = 'Service';
            $data['sub_heading'] = 'Other Services';
            $data['description'] = 'Edit Service Here';
            $data['active'] = 'edit_service';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                //  echo $this->input->post("rule_list_val");
                // exit();

                $rule = $this->input->post('rule_list_val');
                //echo $rule;
                //exit();
                //Image Upload Start Now

                $this->upload->initialize($this->set_upload_options());
                if ($this->upload->do_upload('s_image')) {
                    $s_image = $this->upload->data('file_name');
                    $filename = $s_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/service/';

                    $config_manip = array(
                        //'file_name' => 'myfile',
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_service');
                    } else {
                        $thumb_name = explode(".", $filename);
                        $hotel_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                        $this->image_lib->clear();


                        $service = array(
                            's_id' => $this->input->post('services_id'),
                            's_name' => $this->input->post('s_name'),
                            's_category' => $this->input->post('s_category'),
                            's_description' => $this->input->post('s_description'),
                            's_no_member' => $this->input->post('s_no_member'),
                            's_periodicity' => $this->input->post('s_periodicity'),
                            's_image' => $hotel_thumb,
                            's_rules' => $rule,
                            's_price' => $this->input->post('s_price'),
                            's_price_weekend' => $this->input->post('s_price_weekend'),
                            's_price_holiday' => $this->input->post('s_price_holiday'),
                            's_price_special' => $this->input->post('s_price_special'),
                            's_tax_applied' => $this->input->post('s_tax_applied'),
                            's_tax' => $this->input->post('s_tax'),
                            's_discount_applied' => $this->input->post('s_discount_applied'),
                            's_discount' => $this->input->post('s_discount'),
                        );
                    }

                    $query = $this->dashboard_model->update_service($service);
                    if ($query) {
                        $this->session->set_flashdata('succ_msg', 'Service updated successfully');
                    } else {
                        $this->session->set_flashdata('err_msg', 'Database Error');
                    }
                    redirect('dashboard/all_service');
                }

                //Image Upload Section Closed
                else {
                    $service = array(
                        's_id' => $this->input->post('services_id'),
                        's_name' => $this->input->post('s_name'),
                        's_category' => $this->input->post('s_category'),
                        's_description' => $this->input->post('s_description'),
                        's_no_member' => $this->input->post('s_no_member'),
                        's_periodicity' => $this->input->post('s_periodicity'),
                        's_rules' => $rule,
                        's_price' => $this->input->post('s_price'),
                        's_price_weekend' => $this->input->post('s_price_weekend'),
                        's_price_holiday' => $this->input->post('s_price_holiday'),
                        's_price_special' => $this->input->post('s_price_special'),
                        's_tax_applied' => $this->input->post('s_tax_applied'),
                        's_tax' => $this->input->post('s_tax'),
                        's_discount_applied' => $this->input->post('s_discount_applied'),
                        's_discount' => $this->input->post('s_discount'),
                    );
                    $query = $this->dashboard_model->update_service($service);
                    if ($query) {
                        $this->session->set_flashdata('succ_msg', 'Service updated successfully');
                    } else {
                        $this->session->set_flashdata('err_msg', 'Database Error');
                    }
                    redirect('dashboard/all_service');
                }
            } else {
                $id_given = $_GET["s_id"];
                $data['services'] = $this->dashboard_model->get_service_details1($id_given);
                //echo "====>>>>>>".$id_given;
                //print_r($data['services']);
                //exit();
                $data['page'] = 'backend/dashboard/service_edit';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function expense_report() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $data['heading'] = 'Expense';
            $data['sub_heading'] = 'Expense Report';
            $data['description'] = 'Detailed Expense Report';
            $data['active'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if (isset($_GET["date"])) {
                
            } else {

                $data['purchases'] = $this->dashboard_model->all_purchase_limit();
                $data['payments'] = $this->dashboard_model->all_payments();
                //print_r($payments);
                $data['page'] = 'backend/dashboard/expense_report';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function income_report() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Financial Reports';
            $data['description'] = 'Detailed Income Report';
            $data['active'] = 'income_report';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if (isset($_GET["date"])) {
                
            } else {

                //  $data['purchases']=$this->dashboard_model->all_purchase_limit();
                // $data['payments']=$this->dashboard_model->all_payments();
                //print_r($payments);
                $data['page'] = 'backend/dashboard/income_report';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function add_misc_income() {

        if ($this->session->userdata('add_misc_income') != '1') {

            $data['heading'] = 'Finance';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Misc Income';
            $data['active'] = 'misc_income';
            $data['admin'] = $this->dashboard_model->fetch_user();
            $data['usage_log'] = $this->dashboard_model->all_usage_log();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/add_misc_income';
            $this->load->view('backend/common/index', $data);

            if ($this->input->post()) {


                /* Images Upload */
                date_default_timezone_set("Asia/Kolkata");

                $mop = $this->input->post('mop');
                $checkno = $this->input->post('checkno');
                $check_bank_name = $this->input->post('check_bank_name');
                $draft_no = $this->input->post('draft_no');
                $draft_bank_name = $this->input->post('draft_bank_name');
                $ft_bank_name = $this->input->post('ft_bank_name');
                $ft_account_no = $this->input->post('ft_account_no');
                $ft_ifsc_code = $this->input->post('ft_ifsc_code');
                $profit_center = $this->input->post('profit_center');


                //	print_r($m_income);
                //exit();






                /* if($check_bank_name!="")
                  {
                  $t_bank_name=$check_bank_name;
                  }
                  else if($draft_bank_name!="")
                  {
                  $t_bank_name=$draft_bank_name;
                  }
                  else{
                  //echo "N/A";
                  $t_bank_name=$ft_bank_name;
                  } */

                if ($this->input->post("note") != '') {
                    $note = "( Recived Rs: " . $this->input->post("amount") . " from " . $this->input->post('recived_from') . " for " . $this->input->post('item') . " )" . " -" . $this->input->post("note");
                } else {
                    $note = "( Recived Rs: " . $this->input->post("amount") . " from " . $this->input->post('recived_from') . " for " . $this->input->post('item') . " )";
                }


                //PAYMENT SECTION START
                $pay_amount = $this->input->post('total');
                $mop = $this->input->post('pay_mode');
                if ($mop == '') {
                    $mop = '';
                }
                // chk  done
                $chk_bank_name = $this->input->post('chk_bank_name');
                $checkno = $this->input->post('checkno');
                $chk_drawer_name = $this->input->post('chk_drawer_name');

                // fund  done
                $card_no = $this->input->post('card_no');
                $card_name = $this->input->post('card_name');
                $card_expy = $this->input->post('card_expy');
                $card_expm = $this->input->post('card_expm');
                $card_czz = $this->input->post('card_cvv');
                $card_exp_date = $card_expm . $card_expy;
                $card_bank_name = $this->input->post('card_bank_name');
                // draft  done
                $draft_no = $this->input->post('draft_no');
                $draft_bank_name = $this->input->post('draft_bank_name');
                $draft_drawer_name = $this->input->post('draft_drawer_name');

                // fund  done
                $ft_bank_name = $this->input->post('ft_bank_name');
                $ft_account_no = $this->input->post('ft_account_no');
                $ft_ifsc_code = $this->input->post('ft_ifsc_code');

                //wallet 


                $ft_approved_by = $this->input->post('ft_approved_by');
                $ft_recievers_name = $this->input->post('ft_recievers_name');
                $ft_paid_by = $this->input->post('ft_paid_by');
                $ft_recievers_desig = $this->input->post('ft_recievers_desig');
                //$profit_center1=$this->input->post('p_center');
                $p_status = $this->input->post('p_status_w');

                if (isset($card_bank_name)) {
                    $t_bank_name = $card_bank_name;
                } else if (isset($draft_bank_name)) {
                    $t_bank_name = $draft_bank_name;
                } else if (isset($ft_bank_name)) {
                    $t_bank_name = $ft_bank_name;
                } else if (isset($chk_bank_name)) {
                    $t_bank_name = $chk_bank_name;
                }


                $card_type = $this->input->post('card_type');
                $profit_center1 = $this->input->post('profit_center');

                //$recived_from=$this->input->post('recived_from');
                $wallet_rec_acc = $this->input->post('wallet_rec_acc');
                $wallet_name = $this->input->post('wallet_name');
                $wallet_tr_id = $this->input->post('wallet_tr_id');

                $transaction = array(
                    'transaction_type' => "Misc Income",
                    'transaction_type_id' => "13",
                    't_card_type' => $card_type,
                    't_amount' => $pay_amount,
                    't_payment_mode' => $mop,
                    'checkno' => $checkno,
                    't_bank_name' => $t_bank_name,
                    't_drw_name' => $chk_drawer_name,
                    't_card_no' => $card_no,
                    't_card_name' => $card_name,
                    't_card_exp_date' => $card_exp_date,
                    't_status' => $p_status,
                    //'t_card_cvv'=>$card_cvv,
                    'draft_no' => $draft_no,
                    'draft_bank_name' => $draft_bank_name,
                    'ft_bank_name' => $ft_bank_name,
                    'ft_account_no' => $ft_account_no,
                    'ft_ifsc_code' => $ft_ifsc_code,
                    'details_type' => "Misc Income",
                    'transaction_releted_t_id' => "13",
                    'transaction_from_id' => "12",
                    'transaction_to_id' => "4",
                    'p_center' => $profit_center1,
                    //'t_booking_id'=>'99',//need to ask
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                    'transactions_detail_notes' => $note,
                    't_w_name' => $wallet_name,
                    't_recv_acc' => $wallet_rec_acc,
                    't_tran_id' => $wallet_tr_id
                );

                /* $transaction=array(
                  'transaction_type'=>"Misc Income",
                  'transaction_type_id'=>"13",
                  'user_id' => $this->session->userdata('user_id'),
                  't_date'=>date("Y-m-d H:i:s"),----
                  'details_type' => "Misc Income",
                  't_amount' => $this->input->post("amount"),
                  't_payment_mode' => $mop,
                  't_bank_name' => $t_bank_name,

                  'transaction_releted_t_id'=>"13",
                  'transaction_from_id' => "12",
                  'transaction_to_id' => "4",
                  't_hotel_id' => $this->session->userdata('user_hotel'),
                  't_status' => "Done",
                  'transactions_detail_notes' =>$note

                  );

                 */
                $m_income = array(
                    'date' => $this->input->post('date'),
                    'recived_from' => $this->input->post('recived_from'),
                    'item' => $this->input->post('item'),
                    'note' => $this->input->post('note'),
                    'recived_by' => $this->input->post('recived_by'),
                    'mode_of_payment' => $mop,
                    'check_no' => $checkno,
                    'check_bank_name' => $check_bank_name,
                    'draft_no' => $draft_no,
                    'draft_bank_name' => $draft_bank_name,
                    'ft_bank_name' => $ft_bank_name,
                    'ft_account_no' => $ft_account_no,
                    'ft_ifsc_code' => $ft_ifsc_code,
                    'tax' => $this->input->post('tax'),
                    'amount' => $this->input->post('amount'),
                    'total' => $this->input->post('total'),
                    'profit_center' => $profit_center,
                    'admin' => $this->input->post('admin_name'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                );
                /* echo "<pre>";
                  print_r($transaction);
                  print_r($m_income);

                  exit; */
                $query = $this->dashboard_model->add_misc_income($m_income, $transaction);



                //update cash
                //$mode=$this->input->post('mop');
                $mode = $mop;
                $amount = $this->input->post("amount");

                $cash = $this->unit_class_model->get_closing_stock();
                $c = $cash->clossing_cash; //get clossing cash
                $cash_collection = $cash->cash_collection; //get collect cash
                //echo $mode; exit;
                if ($mode == 'cash' || $mode == 'Cash') {
                    $query1 = $this->unit_class_model->update_cash_collection($amount, $c, $cash_collection);
                } else {
                    $cash = $this->unit_class_model->get_mode_cash($mode);
                    $c = $cash->$mode; //get mode cash
                    //$c+=$amount;
                    $c += $amount;
                    $query1 = $this->unit_class_model->update_cash_drawer_mode($c, $mode);
                }

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Misc income added successfully!");
                    redirect('dashboard/misc_income');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_misc_income');
                }
            }
        }
    }

    function edit_misc_income() {
        $id = $_GET['m_id'];

        $data['heading'] = 'Finance';
        $data['sub_heading'] = '';
        $data['description'] = 'Edit Misc Income';
        $data['active'] = 'misc_income';

        $data['m_income'] = $this->dashboard_model->fetch_m_income_id($id);

        $data['admin'] = $this->dashboard_model->fetch_admin();
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/edit_misc_income';

        $this->load->view('backend/common/index', $data);
    }

    function update_misc_income() {


        if ($this->input->post()) {


            /* Images Upload */
            date_default_timezone_set("Asia/Kolkata");
            $mop = $this->input->post('mop');
            $checkno = $this->input->post('checkno');
            $check_bank_name = $this->input->post('check_bank_name');
            $draft_no = $this->input->post('draft_no');
            $draft_bank_name = $this->input->post('draft_bank_name');
            $ft_bank_name = $this->input->post('ft_bank_name');
            $ft_account_no = $this->input->post('ft_account_no');
            $ft_ifsc_code = $this->input->post('ft_ifsc_code');
            $profit_center = $this->input->post('profit_center');


            $m_income = array(
                'hotel_misc_income_id' => $this->input->post('m_id'),
                'date' => $this->input->post('date'),
                'recived_from' => $this->input->post('recived_from'),
                'item' => $this->input->post('item'),
                'note' => $this->input->post('note'),
                'recived_by' => $this->input->post('recived_by'),
                'mode_of_payment' => $mop,
                'check_no' => $checkno,
                'check_bank_name' => $check_bank_name,
                'draft_no' => $draft_no,
                'draft_bank_name' => $draft_bank_name,
                'ft_bank_name' => $ft_bank_name,
                'ft_account_no' => $ft_account_no,
                'ft_ifsc_code' => $ft_ifsc_code,
                'profit_center' => $profit_center,
                'tax' => $this->input->post('tax'),
                'amount' => $this->input->post('amount'),
                'total' => $this->input->post('total'),
                'admin' => $this->input->post('admin_name')
            );
            //print_r($m_income);
            //exit();

            $query = $this->dashboard_model->update_misc_income($m_income);
            if (isset($query) && $query) {
                $this->session->set_flashdata('succ_msg', "Misc Income has been added successfully!");
                redirect('dashboard/misc_income');
            } else {
                $this->session->set_flashdata('err_msg', "Database Error. Please Try again later!");
                redirect('dashboard/edit_misc_income');
            }
        }
    }

    function misc_income() {

        $data['heading'] = 'Finance';
        $data['description'] = ' Miscellenious Income ';
        $data['active'] = 'misc_income';
        $data['m_income'] = $this->dashboard_model->fetch_m_income();
        //$data['active']='';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        if (isset($_GET["date"])) {
            
        } else {

            //  $data['purchases']=$this->dashboard_model->all_purchase_limit();
            // $data['payments']=$this->dashboard_model->all_payments();
            //print_r($payments);
            $data['page'] = 'backend/dashboard/all_misc_income';
            $this->load->view('backend/common/index', $data);
        }
    }

    function delete_misc_income() {
        $id = $_GET['m_id'];
        $details = $this->dashboard_model->delete_m_income($id);

        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function delete_income_report() {

        $id = $_GET['r_id'];
        $details = $this->dashboard_model->delete_e_report($id);
        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    /* Add unit block Start */

    function add_unit_block() {

        $found = in_array("10", $this->arraay);
        if ($found) {
            $data['heading'] = 'Room';
            $data['sub_heading'] = '';
            $data['description'] = 'Add unit block Here';
            $data['active'] = 'all_unit_block';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {
                $room_no = implode(",", $this->input->post('room_no'));
                //echo "<pre>";
                //print_r($room_no);
                //exit;
                date_default_timezone_set("Asia/Kolkata");
                $data = array(
                    'name' => $this->input->post('name'),
                    'desc' => $this->input->post('desc'),
                    'unit_block' => $room_no,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
                $query = $this->dashboard_model->add_unit_block($data);

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Unit added successfully!");
                    redirect('dashboard/add_unit_block');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Please Try again later!");
                    redirect('dashboard/add_unit_block');
                }
            } else {
                $data['room_feature'] = $this->dashboard_model->get_all_room_name();
                $data['page'] = 'backend/dashboard/add_unit_block';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Add unit block End */

    /* All unit block Start */

    function all_unit_block() {

        $found = in_array("10", $this->arraay);
        if ($found) {
            $data['heading'] = 'Room';
            $data['sub_heading'] = 'All unit block';
            $data['description'] = 'Added Rooms List';
            $data['active'] = 'all_unit_block';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            //$data['rooms'] = $this->dashboard_model->all_rooms($config['per_page'], $segment);
            $data['rooms'] = $this->dashboard_model->all_unit_block();
            //$data['pagination'] = $this->pagination->create_links();
            $data['page'] = 'backend/dashboard/all_unit_block';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function add_group_booking() {
		
		date_default_timezone_set("Asia/Kolkata");
	//	$start_date = date("Y-m-d", strtotime($this->input->post('start_date')));
	//	$end_date = date("Y-m-d", strtotime($this->input->post('end_date')));
		
        $found = in_array("25", $this->arraay);
        if ($found) {
            $data['heading'] = 'Booking';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Group Booking';
            $data['active'] = 'add_group_booking';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $dataNB = array();
			
            if ($this->input->post()) {
				//echo '<pre>';
				//print_r($this->input->post());
				//exit;*/
                
                //.............

                $roomID = $this->input->post('roomID');
                $startDate = $this->input->post('startDate');
                $startDateA = $this->input->post('startDateA');
                $endDate=$this->input->post('endDate');
                $endDateA=$this->input->post('endDateA');
                $days = $this->input->post('days');
			//	print_r($days);
                $mealPlan = $this->input->post('tfi');
                $vat = $this->input->post('vat');
                $trr = $this->input->post('trr');
                $tax = $this->input->post('tax');
                if ($this->input->post('charge_mode_type') == 'adult') {

                    $exrr_chrg = $this->input->post('exra');
                    $exmp_chrg = $this->input->post('exma');
                } else if ($this->input->post('charge_mode_type') == 'child') {

                    $exrr_chrg = $this->input->post('exrc');
                    $exmp_chrg = $this->input->post('exmc');
                } else {

                    $exrr_chrg = 0;
                    $exmp_chrg = 0;
                }
                //$exrr_chrg = $this->input->post('');
                //	$exmp_chrg = $this->input->post('');
                $taxresponse = $this->input->post('txrrspns');
                $mealresponse = $this->input->post('txmrspns');

                for ($i = 0; $i < count($this->input->post('roomID')); $i++) {

                    $dataNB[$i] = array(
                        // 'row_id' => $roomID[$i],
                        'date' => date('Y-m-d', strtotime($startDateA[$i])),
                        'hotel_id' => $this->session->userdata("user_hotel"),
                        // 'space' => $endDate[$i],
                        //   'days' => $days[$i],
                        'meal_plan_chrg' => $mealPlan[$i],
                        'exrr_chrg' => $exrr_chrg[$i],
                        'exmp_chrg' => $exmp_chrg[$i],
                        'room_rent' => $trr[$i],
                        'rr_total_tax' => $tax[$i],
                        'mp_total_tax' => $vat[$i],
                        'rr_tax_response' => $taxresponse[$i],
                        'mp_tax_response' => $mealresponse[$i],
                        'added_on' => date('Y-m-d H:i:s')
                    );
					
					/*if($startDate[$i] <= $endDate[$i]){
						
						if($days[$i] == 0)
							$daysi = 1;
						else
							$daysi = $days[$i];
						
						$noofdays[$i] = array(
							'row_id' => $roomID[$i],
							'dayspace' => $daysi
						);
					}*/
					
					$noofdays[$i] = array(
							'row_id' => $roomID[$i],
							'dayspace' => $days[$i]
					);
                }

                $booking_source = $this->input->post('booking_source');
                $pCenter = $this->input->post('p_center');
                $comment = $this->input->post('booking_notes');
                $preference = $this->input->post('booking_preference');
                $nature_visit = $this->input->post('nature_visit');
                $marketing_personel = $this->input->post('marketing_personel');
                $travel_agent_type = $this->input->post('travel_agent_type');

                if ($travel_agent_type == 'T') {
                    $travel_agent_type = '';
                    $travel_agent_id = 0;
                    $travel_agent_commission = 0;
                    $travel_agent_commission_actual = 0;
                    $commission_applicable = '';
                } else {
                    $travel_agent_type = $this->input->post('travel_agent_type');
                    $travel_agent_id = $this->input->post('travel_agent_id');
                    $travel_agent_commission = $this->input->post('travel_agent_commission');
                    $travel_agent_commission_actual = $this->input->post('travel_agent_commission');
                    $commission_applicable = $this->input->post('commission_applicable');
                }

                $chk_last_bk = $this->dashboard_model->chk_last_gb();
                $id_stype = $this->input->post('booking_source');

                $chk_last_bk = $chk_last_bk + 1;
                $data1 = array(
                    'actual_id' => "BKGRP0" . $this->session->userdata('user_hotel') . "-" . $this->session->userdata('user_id') . "/" . date("d") . date("m") . date("y") . "/" . $chk_last_bk,
                    'travel_agent_type' => $travel_agent_type,
                    'travel_agent_id' => $travel_agent_id,
                    'travel_agent_commission' => $travel_agent_commission,
                    'travel_agent_commission_actual' => $travel_agent_commission_actual,
                    'commission_applicable' => $commission_applicable,
                    'p_center' => $this->input->post('p_center'),
                    'name' => $this->input->post('name'),
                    'number_room' => $this->input->post('number_room'),
                    'number_guest' => $this->input->post('number_guest'),
                    'guestID' => $this->input->post('guestID'),
                    'start_date' => $this->input->post('start_date'),
                    'tax_applicable' => $this->input->post('tax_applicable'),
                    'booking_notes' => $this->input->post('booking_notes'),
                    'booking_preference' => $this->input->post('booking_preference'),
                    'booking_source' => $this->input->post('booking_source'),
                    'arrival_mode' => $this->input->post('arrival_mode'),
                    'nature_visit' => $this->input->post('nature_visit'),
                    'room_rent_type' => $this->input->post('room_rent_type'),
                    'marketing_personnel' => $marketing_personel, //8/6/2016 ab
                    'end_date' => $this->input->post('end_date'),                    
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
               // print_r( $data1); exit;

				 if($this->input->post('g_state')!=''){
					
					$state = strtoupper($this->input->post('g_state'));
				}else{
					
					$state = strtoupper($this->input->post('g_state1'));
				}
                $data2 = array(
                    'guestID' => $this->input->post('guestID'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                    'g_name' => $this->input->post('name'),
                    'g_address' => "Not Given",
                    'g_pincode' => $this->input->post('pincode'),
                    'g_contact_no' => $this->input->post('g_phn'),
                    'g_email' => $this->input->post('g_email'),
					'g_state'  =>$state ,
					'g_country'  => strtoupper($this->input->post('g_country')),
					'g_place'  => $this->input->post('g_state_code'),
					
                );
					 /*echo '</br>$startDate</br><pre>';
					 print_r($startDateA);
					 echo '</br>$endDate</br><pre>';
					 print_r($endDateA);
					 echo '</br>$days</br><pre>';
					 print_r($days);
					 echo '</br>$data1</br><pre>';
					 print_r($data1);
					 echo '</br>$data2</br><pre>';
					 print_r($data2);
					 echo '</br>$dataNB</br><pre>';
					 print_r($dataNB);
					 echo '</br>$noofdays</br><pre>';
                	 print_r($noofdays);
                	 exit();*/ 
                //---------------
                	//print_r($data2);exit;

                $hotel_id = $this->session->userdata('user_hotel');
                $taxDB = $this->bookings_model->service_tax($hotel_id);
                
                $query = $this->dashboard_model->add_group_booking($this->input->post(), $data1, $data2, $taxDB, $booking_source, $pCenter, $comment, $preference, $nature_visit, $marketing_personel, $travel_agent_type, $travel_agent_id, $travel_agent_commission, $commission_applicable, $dataNB, $noofdays, $tax);

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Great, your Group Reservation has been generated successfully!");
                    redirect('dashboard/add_group_booking/' . $query);
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_group_booking/' . $query);
                }
            } else {
                //$data['rooms'] = $this->dashboard_model->get_available_room($date1,$date2);
                //$data['events'] = $this->dashboard_model->all_events();
                $hotel_id = $this->session->userdata('user_hotel');
                $data['taxDB'] = $this->bookings_model->service_tax($hotel_id);
                $data['getNatureVisit'] = $this->dashboard_model->getNatureVisit();
                $data['page'] = 'backend/dashboard/add_group_booking';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    } // End function add_group_booking

    /* All unit block End */

    //All rooms checkedin today


    function pos_fetch_data() {

        echo $this->input->post("val");
    }

    function get_available_room() {
        //$date1=$_GET["date1"];
        //  $date2=$_GET["date2"];
        $date1 = date("Y-m-d", strtotime($this->input->post("date1")));
        $date2 = date("Y-m-d", strtotime($this->input->post("date2")));
        //$date1 = '2016-06-04';
        //$date2 = '2016-06-06';

        $rooms = $this->dashboard_model->all_rooms();
        $array1 = array();
        $array2 = array();


        $bookings_between = $this->dashboard_model->get_available_room($date1, $date2);
        /*echo '<pre>';
        print_r($bookings_between);
        exit;*/
        $i = 0;
        $j = 0;

        foreach ($rooms as $room) {
            # code...

            $array1[$i] = $room->room_id;

            $i++;
        }



        foreach ($bookings_between as $key) {
            # code...
            $array2[$j] = $key->room_id;

            $j++;
        }


        $result = array_diff($array1, $array2);



        foreach ($result as $res) {
            # code...
            $room = $this->dashboard_model->get_room_number_exact($res);
            $data = "";

            $room_number = $room->room_no;
            echo $data = ' <div class="btn-group"> <label class="btn grey-cascade" id="abc_' . $res . '"> <input id="ab_' . $res . '" style="display: none;" onclick="addRow(this.value,' . $room_number . ')" value="' . $res . '" type="checkbox" > ' . $room_number . ' </label></div>';
        }



        /*   $json = array(
          'data' => $data,

          );
          header('Content-Type: application/json');
          echo json_encode($json); */
    }

    function get_available_room2() {
        //$date1=$_GET["date1"];
        //$date2=$_GET["date2"];
        $date1 = date("Y-m-d", strtotime($this->input->post("date1")));
        $date2 = date("Y-m-d", strtotime($this->input->post("date2")));


        $rooms = $this->dashboard_model->all_rooms();
        $array1 = array();
        $array2 = array();


        $bookings_between = $this->dashboard_model->get_available_room($date1, $date2);
        $i = 0;
        $j = 0;

        foreach ($rooms as $room) {
            # code...

            $array1[$i] = $room->room_id;

            $i++;
        }



        foreach ($bookings_between as $key) {
            # code...
            $array2[$j] = $key->room_id;

            $j++;
        }


        $result = array_diff($array1, $array2);




        $k = 0;
        foreach ($result as $res) {


            $k++;
        }

        echo $k;

        /*   $json = array(
          'data' => $data,

          );
          header('Content-Type: application/json');
          echo json_encode($json); */
    }

    function add_pos_transaction() {
        $pos_id = $this->input->post("pos_id");
        $amount = $this->input->post("amount");

        $query = $this->dashboard_model->remove_amount_pos($pos_id, $amount);

        $json = array(
            'data' => $query,
        );
        header('Content-Type: application/json');
        echo json_encode($json);
    }

    function pos_grand_invoice() {

        // echo "sd";
        //exit();
        // $p_id=$_GET["p_id"];
        $b_id = $_GET["b_id"];
        $hotel_id = $this->session->userdata('user_hotel');
        $data['pos'] = $this->dashboard_model->all_pos_booking($b_id);
        $data['bookings'] = $this->dashboard_model->get_booking_details($b_id);
        $data['hotel'] = $this->dashboard_model->get_hotel_details_row($this->session->userdata('user_hotel'));
        $data['tax'] = $this->bookings_model->get_tax_details($hotel_id);
        $data['printer']  =$this->dashboard_model->get_printer($hotel_id);

        $this->load->view("backend/dashboard/invoice_pos_pdf", $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("invoice_pos.pdf");
    }

    function get_room_details() {
        $id = $_GET["id"];
        $query = $this->dashboard_model->room_details($id);
        $json = json_encode($query);
        echo $json;
    }

    function edit_purchase($id) {

        $found = in_array("83", $this->arraay);
        if ($found) {
            $data['heading'] = 'Finance';
            $data['sub_heading'] = 'Purchase';
            $data['description'] = 'Edit Purchase Here';
            $data['active'] = 'all_purchase';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {

                $purchase = array(
                    'p_id' => $this->input->post('p_id'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'a_id' => $this->input->post('a_id'),
                    'p_date' => date("Y-m-d H:i:s", strtotime($this->input->post('p_date'))),
                    'p_supplier' => $this->input->post('p_supplier'),
                    'p_i_name' => $this->input->post('p_i_name'),
                    'p_i_description' => $this->input->post('p_i_description'),
                    'p_i_price' => $this->input->post('p_i_price'),
                    'p_i_quantity' => $this->input->post('p_i_quantity'),
                    'p_unit' => $this->input->post('p_unit'),
                    'p_price' => $this->input->post('p_price'),
                    'p_tax' => $this->input->post('p_tax'),
                    'p_discount' => $this->input->post('p_discount'),
                    'profit_center' => $this->input->post('profit_center'),
                    'p_total_price' => $this->input->post('p_total_price'),
                );

                //print_r($purchase);
                // exit;
                //$query = $this->dashboard_model->add_purchase($purchase);
                date_default_timezone_set('Asia/Kolkata');

                $transaction = array(
                    'transaction_type' => "Purchase",
                    't_amount' => $this->input->post('p_total_price'),
                    't_date' => date("Y-m-d H:i:s"),
                    'transaction_releted_t_id' => "5",
                    't_status' => "Done",
                    'transaction_from_id' => "4",
                    'transaction_to_id' => "6",
                    't_payment_mode' => "cash",
                );
                //$query2 = $this->dashboard_model-> add_transaction($transaction);
                $query = $this->dashboard_model->update_purchase($purchase, $transaction, $id);
                // print_r($query);
                // die;

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Purchase updated successfully!");
                    redirect('dashboard/all_purchase');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/all_purchase');
                }
            } else {
                $data['purchase'] = $this->dashboard_model->getPurchaseById($id);
                $data['page'] = 'backend/dashboard/edit_purchase';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function booking_edit_group() {
       // print_r($_post); exit;
        $found = in_array("26", $this->arraay);
        if ($found) {
            $group_id = $_GET['group_id'];
            $data['heading'] = 'Booking';
            $data['sub_heading'] = 'Group Booking Details';
            $data['description'] = 'Group Booking Details for Group Id:' . $group_id;
            $data['active'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['details'] = $this->dashboard_model->get_group_details($group_id);
			$data['guestDetails'] = $this->unit_class_model->get_hotel_stay_guest_group($group_id);
			$data['Tsettings'] = $this->dashboard_model->get_Tsettings($this->session->userdata('user_hotel'));
			
			//echo "<pre>";
			//print_r($data['guestDetails']);exit;
            $data['details2'] = $this->dashboard_model->get_group($group_id);
            $data['details5'] = $this->dashboard_model->get_group1($group_id);
		//echo "<pre>";
			//print_r($data['details5']->guestID);exit;
            $data['details3'] = $this->dashboard_model->get_booking_details_grp($group_id);
            //$data['line_item_details'] = $this->dashboard_model->get_booking_lineitem_details_grp($group_id);
            $data['line_items'] = $this->bookings_model->line_charge_item_tax('gb', $group_id);
            $data['transaction'] = $this->dashboard_model->all_transaction_group($group_id);
            $hotel_id = $this->session->userdata('user_hotel');
            $data['taxDB'] = $this->bookings_model->service_tax($hotel_id);
            $data['page'] = 'backend/dashboard/booking_edit_group';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function booking_group_pdf() {

        $group_id = $_GET['group_id'];
        $hotel_id = $this->session->userdata('user_hotel');
        $data['heading'] = 'Booking';
        $data['sub_heading'] = 'Group Booking Details';
        $data['description'] = 'Group Booking Details for Group Id:' . $group_id;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['hotel_contact'] = $this->bookings_model->get_hotel_contact_details($this->session->userdata('user_hotel'));
        $data['tax'] = $this->bookings_model->get_tax_details($hotel_id);
        $data['details'] = $this->dashboard_model->get_group_details($group_id);
        $data['all_adjst'] = $this->dashboard_model->all_adjst_gp($group_id);
        $data['adjstAmt'] = $this->dashboard_model->get_adjuset_amt_grp($group_id);
        $data['details2'] = $this->dashboard_model->get_group($group_id);
        $data['details3'] = $this->dashboard_model->get_booking_details_grp($group_id);
        $data['transaction'] = $this->dashboard_model->all_transaction_group($group_id);
        $data['pos'] = $this->dashboard_model->all_pos_booking_group($group_id);
        $data['line_items'] = $this->bookings_model->line_charge_item_tax('gb', $group_id);
        $data['printer']  =$this->dashboard_model->get_printer($hotel_id);
		$data['bid']  =$group_id;
        $this->load->view("backend/dashboard/booking_group_pdf", $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        // $this->dompdf->render();
        // $this->dompdf->stream("taxInvoice0" . $hotel_id . "GBK0" . $group_id . ".pdf");
    }
	
	function booking_group_view() {

        $group_id = $_GET['group_id'];
        $hotel_id = $this->session->userdata('user_hotel');
        $data['heading'] = 'Booking';
        $data['sub_heading'] = 'Group Booking Details';
        $data['description'] = 'Group Booking Details for Group Id:' . $group_id;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['hotel_contact'] = $this->bookings_model->get_hotel_contact_details($this->session->userdata('user_hotel'));
        $data['tax'] = $this->bookings_model->get_tax_details($hotel_id);
        $data['details'] = $this->dashboard_model->get_group_details($group_id);
        $data['all_adjst'] = $this->dashboard_model->all_adjst_gp($group_id);
        $data['adjstAmt'] = $this->dashboard_model->get_adjuset_amt_grp($group_id);
        $data['details2'] = $this->dashboard_model->get_group($group_id);
        $data['details3'] = $this->dashboard_model->get_booking_details_grp($group_id);
        $data['transaction'] = $this->dashboard_model->all_transaction_group($group_id);
        $data['pos'] = $this->dashboard_model->all_pos_booking_group($group_id);
        $data['line_items'] = $this->bookings_model->line_charge_item_tax('gb', $group_id);
        $data['printer']  =$this->dashboard_model->get_printer($hotel_id);
		$data['bid']  =$group_id;
        $this->load->view("backend/dashboard/booking_group_pdf", $data);
        $html = $this->output->get_output();
		echo $html;
		exit;
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("taxInvoice_0" . $hotel_id . "GBK0" . $group_id . ".pdf");
    }
	
	function booking_group_gst_pdf() {

        $group_id = $_GET['group_id'];
        $hotel_id = $this->session->userdata('user_hotel');
        $data['heading'] = 'Booking';
        $data['sub_heading'] = 'Group Booking Details';
        $data['description'] = 'Group Booking Details for Group Id:' . $group_id;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['hotel_contact'] = $this->bookings_model->get_hotel_contact_details($this->session->userdata('user_hotel'));
        $data['tax'] = $this->bookings_model->get_tax_details($hotel_id);
        $data['details'] = $this->dashboard_model->get_group_details($group_id);
        $data['all_adjst'] = $this->dashboard_model->all_adjst_gp($group_id);
        $data['adjstAmt'] = $this->dashboard_model->get_adjuset_amt_grp($group_id);
        $data['details2'] = $this->dashboard_model->get_group($group_id);
        $data['details3'] = $this->dashboard_model->get_booking_details_grp($group_id);
		/*print_r($data['details3']);
		exit;*/
        $data['transaction'] = $this->dashboard_model->all_transaction_group($group_id);
        $data['pos'] = $this->dashboard_model->all_pos_booking_group($group_id);
        $data['line_items'] = $this->bookings_model->line_charge_item_tax('gb', $group_id);
        $data['printer']  = $this->dashboard_model->get_printer($hotel_id);
		$data['bid']  = $group_id;
        $this->load->view("backend/dashboard/booking_group_gst_pdf", $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("taxInvoice_0".$hotel_id."GBK0".$group_id.".pdf");
    }
	
	function booking_group_gst_view() {

        $group_id = $_GET['group_id'];
        $hotel_id = $this->session->userdata('user_hotel');
        $data['heading'] = 'Booking';
        $data['sub_heading'] = 'Group Booking Details';
        $data['description'] = 'Group Booking Details for Group Id:' . $group_id;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['hotel_contact'] = $this->bookings_model->get_hotel_contact_details($this->session->userdata('user_hotel'));
        $data['tax'] = $this->bookings_model->get_tax_details($hotel_id);
        $data['details'] = $this->dashboard_model->get_group_details($group_id);
        $data['all_adjst'] = $this->dashboard_model->all_adjst_gp($group_id);
        $data['adjstAmt'] = $this->dashboard_model->get_adjuset_amt_grp($group_id);
        $data['details2'] = $this->dashboard_model->get_group($group_id);
        $data['details3'] = $this->dashboard_model->get_booking_details_grp($group_id);
       
        $data['transaction'] = $this->dashboard_model->all_transaction_group($group_id);
        $data['pos'] = $this->dashboard_model->all_pos_booking_group($group_id);
        $data['line_items'] = $this->bookings_model->line_charge_item_tax('gb', $group_id);
        $data['printer']  = $this->dashboard_model->get_printer($hotel_id);
		$data['bid']  = $group_id;
		// echo "<pre>";
		//print_r($data);
		//exit;
        $this->load->view("backend/dashboard/booking_group_gst_view", $data);
        $html = $this->output->get_output();
		echo $html;
		exit;
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("taxInvoice_0".$hotel_id."GBK0".$group_id.".pdf");
    }

    function booking_group_pdf2() {

        $group_id = $_GET['group_id'];
        $hotel_id = $this->session->userdata('user_hotel');
        $data['heading'] = 'Booking';
        $data['sub_heading'] = 'Group Booking Details';
        $data['description'] = 'Group Booking Details for Group Id:' . $group_id;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['hotel_contact'] = $this->bookings_model->get_hotel_contact_details($this->session->userdata('user_hotel'));
		$data['adjstAmt'] = $this->dashboard_model->get_adjuset_amt_grp($group_id);
        $data['tax'] = $this->bookings_model->get_tax_details($hotel_id);
        $data['details'] = $this->dashboard_model->get_group_details($group_id);
        $data['details2'] = $this->dashboard_model->get_group($group_id);
        $data['details3'] = $this->dashboard_model->get_booking_details_grp($group_id);
        $data['transaction'] = $this->dashboard_model->all_transaction_group($group_id);
        $data['line_items'] = $this->bookings_model->line_charge_item_tax('gb', $group_id);
        $data['pos'] = $this->dashboard_model->all_pos_booking_group($group_id);
        $data['printer']  =$this->dashboard_model->get_printer($hotel_id);
		$data['bid']  =$group_id;
        $this->load->view("backend/dashboard/booking_group_pdf2", $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("taxInvoice_0".$hotel_id."GBK0".$group_id."_brief.pdf");
    }

	function booking_group_view2() {

        $group_id = $_GET['group_id'];
        $hotel_id = $this->session->userdata('user_hotel');
        $data['heading'] = 'Booking';
        $data['sub_heading'] = 'Group Booking Details';
        $data['description'] = 'Group Booking Details for Group Id:' . $group_id;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['hotel_contact'] = $this->bookings_model->get_hotel_contact_details($this->session->userdata('user_hotel'));
		$data['adjstAmt'] = $this->dashboard_model->get_adjuset_amt_grp($group_id);
        $data['tax'] = $this->bookings_model->get_tax_details($hotel_id);
        $data['details'] = $this->dashboard_model->get_group_details($group_id);
        $data['details2'] = $this->dashboard_model->get_group($group_id);
        $data['details3'] = $this->dashboard_model->get_booking_details_grp($group_id);
        $data['transaction'] = $this->dashboard_model->all_transaction_group($group_id);
        $data['line_items'] = $this->bookings_model->line_charge_item_tax('gb', $group_id);
        $data['pos'] = $this->dashboard_model->all_pos_booking_group($group_id);
        $data['printer']  =$this->dashboard_model->get_printer($hotel_id);
		$data['bid']  =$group_id;
        $this->load->view("backend/dashboard/booking_group_pdf2", $data);
        $html = $this->output->get_output();
		echo $html;
		exit;
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("taxInvoice_0".$hotel_id."GBK0".$group_id."_brief.pdf");
    }
	
	function invoice_adhoc_bill() {

        $id = $_GET['id'];
		$hotel_id = $this->session->userdata('user_hotel');
        $data['heading'] = 'Adhoc Bill Invoice';
        $data['sub_heading'] = 'Invoice for AdHoc Billing';
        $data['description'] = 'Invoice for AdHoc Billing Id:' . $id;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['hotel_contact'] = $this->bookings_model->get_hotel_contact_details($this->session->userdata('user_hotel'));
		$data['tax'] = $this->bookings_model->get_tax_details($hotel_id);
		$data['Ahb_data'] = $this->dashboard_model->get_adhoc_bill_master_byID($id);
        $data['Ahb_line_data'] = $this->dashboard_model->get_adhoc_bill_line_byID($id);
        $data['transaction'] = $this->dashboard_model->all_transaction_adhoc_byID($id);
        $data['printer']  =$this->dashboard_model->get_printer($hotel_id);
		$data['bid']  =$group_id;
        $this->load->view("backend/dashboard/invoice_adhoc_bill", $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("taxInvoice_0".$hotel_id."GBK0".$group_id."_brief.pdf");
    }
	
	function view_adhoc_bill() {
		
		$id = $_GET['id'];
		$hotel_id = $this->session->userdata('user_hotel');
        $data['heading'] = 'Adhoc Bill Invoice';
        $data['sub_heading'] = 'Invoice for AdHoc Billing';
        $data['description'] = 'Invoice for AdHoc Billing Id:' . $id;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['hotel_contact'] = $this->bookings_model->get_hotel_contact_details($this->session->userdata('user_hotel'));
		$data['tax'] = $this->bookings_model->get_tax_details($hotel_id);
		$data['Ahb_data'] = $this->dashboard_model->get_adhoc_bill_master_byID($id);
        $data['Ahb_line_data'] = $this->dashboard_model->get_adhoc_bill_line_byID($id);
        $data['transaction'] = $this->dashboard_model->all_transaction_adhoc_byID($id);
        $data['printer']  =$this->dashboard_model->get_printer($hotel_id);
		$data['bid'] = $id;
        $this->load->view("backend/dashboard/invoice_adhoc_bill", $data);
        $html = $this->output->get_output();
		echo $html;
		exit;
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("taxInvoice_0".$hotel_id."GBK0".$group_id."_brief.pdf");
    }

    function getAdditionalDiscount() {
        $discountVal = $_GET["discountVal"];
        $groupID = $_GET["groupID"];
        $query = $this->dashboard_model->getAdditionalDiscount($discountVal, $groupID);
        $json = json_encode($query);
        echo $json;
    }

    function getAdditionalDiscountSingle() {
        $discountVal = $_GET["discountVal"];
        $groupID = $_GET["groupID"];
        $query = $this->dashboard_model->getAdditionalDiscountSingle($discountVal, $groupID);
        $json = json_encode($query);
        echo $json;
    }

    function edit_unit_type() {

        $found = in_array("11", $this->arraay);
        if ($found) {
            //$data['edit_id'] = $id;
            $data['heading'] = 'Unit';
            $data['sub_heading'] = 'update unit type';
            $data['description'] = 'update unit type Here';
            $data['active'] = 'unit_type';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            //echo $id;
            if ($this->input->post()) {
                //print_r($_POST);
                //exit;
                $unit = array(
                    'id' => $this->input->post('hid1'),
                    'unit_type' => $this->input->post('unit_type1'),
                    'unit_name' => $this->input->post('unit_name1'),
                    'unit_class' => $this->input->post('unit_class1'),
                    'unit_desc' => $this->input->post('desc1'),
                    'kit' => $this->input->post('kit1'),
                    'default_occupancy' => $this->input->post('default_occ1'),
                    'max_occupancy' => $this->input->post('max_occ1'),
                );
                
                $query = $this->dashboard_model->updateUnit($unit);
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "unit type updated successfully!");
                    redirect('dashboard/unit_type');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/unit_type');
                }
            } else {
                $data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
                $data['UnitDetail'] = $this->dashboard_model->getUnitDetail($id);
                //print_r ($data['UnitDetail']);
                //exit;
                $data['page'] = 'backend/dashboard/edit_unit_type';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function finance_setup() {
        $data['heading'] = 'Finance';
        $data['sub_heading'] = 'Finance Setup';
        $data['description'] = 'Setup Your Hotel Finance Structute';
        $data['active'] = 'finance_setup';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['get_profit_center'] = $this->dashboard_model->get_profit_center();
        $data['get_finance_setting'] = $this->dashboard_model->get_setting($this->session->userdata('user_hotel'));


        if ($this->input->post()) {
            date_default_timezone_set('Asia/Kolkata');

            $flg = $this->input->post('flag');
            $tax_ids = $this->input->post('tax');

            if (!empty($tax_ids)) {
                $tax_to_db = implode(',', $tax_ids);
            } else {
                $tax_to_db = '';
            }

            if ($flg == '0') {
                $setup = array(
                    'fin_hotel_id' => $this->session->userdata('user_hotel'),
                    'fin_curr_initial' => $this->input->post('c_code'),
                    'fin_profit_center' => $this->input->post('c_pro_centr'),
                    'fin_cash_in_hand' => $this->input->post('c_in_hand'),
                    'fin_cash_bank' => $this->input->post('c_in_bank'),
                    'fin_tax_applied' => $this->input->post('c_tax'),
                    'fin_tax_amt' => "0",
                    'tax_fields' => $tax_to_db,
                    'modified_on' => date("Y-m-d H:i:s"),
                    'modified_by' => $this->session->userdata('user_name')
                );
                $query = $this->dashboard_model->insert_finance($setup);
            } else {
                $setup = array(
                    'fin_hotel_id' => $this->session->userdata('user_hotel'),
                    'fin_curr_initial' => $this->input->post('c_code'),
                    'fin_profit_center' => $this->input->post('c_pro_centr'),
                    'fin_cash_in_hand' => $this->input->post('c_in_hand'),
                    'fin_cash_bank' => $this->input->post('c_in_bank'),
                    'fin_tax_applied' => $this->input->post('c_tax'),
                    'fin_tax_amt' => "0",
                    'tax_fields' => $tax_to_db,
                    'modified_on' => date("Y-m-d H:i:s"),
                    'modified_by' => $this->session->userdata('user_name')
                );
                $query = $this->dashboard_model->update_finance($setup);
            }


            if (isset($query) && $query) {
                $this->session->set_flashdata('succ_msg', "Setup updated successfully!");
                redirect('dashboard/finance_setup');
            } else {
                $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                redirect('dashboard/finance_setup');
            }
        } else {
            $data['page'] = 'backend/dashboard/finance';
            $this->load->view('backend/common/index', $data);
        }
    }

    function get_gen_detail_by_id() {

        $query = $this->dashboard_model->fetch_gen_by_id($this->input->post('dgset_id'));
        echo $query->fuel_consumption . "-" . $query->fuel;
    }

    function housekeeping_log() {

        $found = in_array("74", $this->arraay);
        if ($found) {
            $data['heading'] = 'Housekeeping';
            $data['sub_heading'] = '';
            $data['description'] = 'Find When Your Maid Done What Activity';
            $data['active'] = 'housekeeping_log';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['logs'] = $this->dashboard_model->get_housekeeping_logs();
            $data['page'] = 'backend/dashboard/housekeeping_log';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function delete_maid() {
        $data['heading'] = '';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        $maid_id = $_GET['maid_id'];

        $query = $this->dashboard_model->delete_maid($maid_id);

        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function edit_maid() {
        $data['heading'] = 'Housekeeping';
        $data['sub_heading'] = '';
        $data['description'] = 'Change Staff Details';
        $data['active'] = 'housekeeping_staff';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['maid_info'] = $this->dashboard_model->get_maid_by_id($_GET['maid_id']);
        if ($this->input->post()) {

            $data = array(
                'maid_name' => $this->input->post('maid_name'),
                'maid_address' => $this->input->post('maid_address'),
                'maid_contact' => $this->input->post('maid_contact'),
                'staff_availability' => $this->input->post('status'),
                'type' => $this->input->post('type'),
                'section' => $this->input->post('maid_sec')
            );

            $query = $this->dashboard_model->update_maid_2($data, $_POST['hid']);
            if (isset($query) && $query) {
                $this->session->set_flashdata('succ_msg', "Maid Updated successfully!");
                redirect('dashboard/housekeeping_staff');
            } else {
                $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                redirect('dashboard/housekeeping_staff');
            }
        }
        $data['page'] = 'backend/dashboard/edit_maid';
        $this->load->view('backend/common/index', $data);
    }

    function view_fuel_log() {
        $found = in_array("74", $this->arraay);
        if ($found) {
            $data['heading'] = 'DG set';
            $data['sub_heading'] = '';
            $data['description'] = 'fuel log view';
            $data['active'] = 'view_fuel_log';

            $data['master_log'] = $this->dashboard_model->get_master_fuel_stock();
            $data['f_log'] = $this->dashboard_model->view_all_fuel_log();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/view_fuel_log';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function delete_fuel_log() {

        $found = in_array("75", $this->arraay);
        if ($found) {

            $id = $_GET['id'];
            $det_type_qty = $this->dashboard_model->get_fuel_qty_type($id);
            //echo $id;
            //print_r ($det_type_qty);
            //echo $det_type_qty->quantity;
            //echo $det_type_qty->fuel_name;
            //echo $det_type_qty->in_out_type;
            //exit();
            if (($det_type_qty->in_out_type == 'sub') || ($det_type_qty->in_out_type == 'Cons')) {
                $type = 'add';
            } else {
                $type = 'sub';
                $this->dashboard_model->delete_transaction_by_details_id($id);
            }
            //echo $type;
            //exit();

            $up_array = array(
                'fuel_name' => $det_type_qty->fuel_name,
                'quantity' => $det_type_qty->quantity,
                'in_out_type' => $type,
            );
            $this->dashboard_model->update_master_fuel($up_array);


            $details = $this->dashboard_model->delete_fuel_item($id);
            $data = array(
                'data' => "sucess",
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            redirect('dashboard');
        }
    }

    function add_tax_details() {
        $data = array(
            'tax_name' => $this->input->post('tax_name_insert'),
            'tax_type' => $this->input->post('tax_type_insert'),
            'tax_value' => $this->input->post('tax_value_insert'),
            'hotel_id' => $this->session->userdata('user_hotel'),
            'user_id' => $this->session->userdata('user_id')
        );

        $query = $this->dashboard_model->add_tax($data);

        if (isset($query) && $query) {
            $this->session->set_flashdata('succ_msg', "Tax Added successfully!");
            redirect('dashboard/finance_setup');
        } else {
            $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
            redirect('dashboard/finance_setup');
        }
    }

    function get_booking_extra_charge() {

        $room_id = $this->input->post("room_id");
        $selector = $this->input->post("selector");
        //$room_id=$_GET['room_id'];
        //$selector=$_GET['selector'];
        $rate = 0;
        if ($selector == "adult") {
            $query = $this->dashboard_model->get_booking_extra_charge_adult($room_id);
            $rate = $query->adult_rate;
        } elseif ($selector == "child") {
            $query = $this->dashboard_model->get_booking_extra_charge_child($room_id);
            $rate = $query->kid_rate;
        }

        echo $rate;
    }

    function add_fuel() {

        $found = in_array("72", $this->arraay);
        if ($found) {
            $data['heading'] = 'DG set';
            $data['sub_heading'] = '';
            $data['description'] = 'Add new fuel type here';
            $data['active'] = 'add_fuel';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {
                $id_g = $this->input->post('g_contact_no');

                date_default_timezone_set("Asia/Kolkata");
                $fuel_name = $this->input->post('fuel_name');
                $fuel_desc = $this->input->post('fuel_desc');
                $quantity = $this->input->post('quantity');
                $reorder_level = $this->input->post('reorder_level');
                $unit = $this->input->post('unit');

                $add_fuel = array(
                    //'hotel_id' => $this->session->userdata('user_hotel'),
                    'fuel_name' => $fuel_name,
                    'fuel_desc' => $fuel_desc,
                    'cur_stock' => $quantity,
                    'unit' => $unit,
                    'reorder_level' => $reorder_level,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );

                //print_r($add_fuel);
                //exit();
                //$query = $this->dashboard_model->add_usage_log($usagelog);
                $query = $this->dashboard_model->add_fuel($add_fuel);
                if (isset($query) && $query) {



                    $this->session->set_flashdata('succ_msg', "Fuel Type added successfully!");
                    redirect('dashboard/view_fuel_log');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_fuel');
                }
            } else {
                $data['page'] = 'backend/dashboard/add_fuel';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    public function mail_view() {
        if ($this->session->userdata('mail_view') != '1') {
            $data['heading'] = 'Mail Box';
            $data['sub_heading'] = 'Mail Manager';
            $data['description'] = 'Mail Managemenet system';
            $data['active'] = 'mail_view';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {
                // $id_g = $this->input->post('g_contact_no');


                if (isset($query) && $query) {



                    $this->session->set_flashdata('succ_msg', "Fuel Type added successfully!");
                    redirect('dashboard/mail_view');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/mail_view');
                }
            } else {
                $data['page'] = 'backend/dashboard/mail';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function inbox_inbox() {
        $this->load->view('backend/dashboard/inbox_inbox');
    }

    function inbox_compose() {
        $this->load->view('backend/dashboard/inbox_compose');
    }

    function inbox_reply() {
        $this->load->view('backend/dashboard/inbox_reply');
    }

    function inbox_view() {
        $this->load->view('backend/dashboard/inbox_view');
    }

    function add_vendor() {
        //die("here is the page ");

        $found = in_array("88", $this->arraay);
        if ($found) {
            $data['heading'] = 'Finance';
            $data['sub_heading'] = '';
            $data['description'] = 'Add a new vendor';
            $data['active'] = 'all_vendor';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['parent'] = $this->dashboard_model->get_parent_vendor();
            //exit();
            if ($this->input->post()) {
                //exit();
                $this->upload->initialize($this->set_upload_options());

                if ($this->upload->do_upload('vendor_image')) {

                    $asset_image = $this->upload->data('file_name');
                    $filename = $asset_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/vendor_detail/';
                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_vendor');
                    }
                    $thumb_name = explode(".", $filename);
                    $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                } else {
                    // $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                    //redirect('dashboard/add_asset');
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/vendor_detail/';
                    $asset_thumb = 'no_images.png';
                }





                $config = array('upload_path' => './upload/vendor_detail/',
                    'allowed_types' => "pdf",
                    'overwrite' => TRUE,);
                $this->upload->initialize($config);
                $this->upload->do_upload('pdf');

                // if ( $this->upload->do_upload('pdf'))
                // {
                //echo "success";
                $up_file = $this->upload->data();
                $up_array = array(
                    'hotel_vendor_name' => $this->input->post('vendor_name'),
                    'hotel_vendor_industry' => $this->input->post('industry_type'),
                    //	'hotel_vendor_name'=>$this->input->post('vendor_legal_name'),
                    //heirc =?
                    'hotel_vendor_heirc' => $this->input->post('child_supp'),
                    'hotel_vendor_contact_person' => $this->input->post('contact_person'),
                    'hotel_vendor_contact_no' => $this->input->post('contact_no'),
                    'hotel_vendor_is_contracted' => $this->input->post('is_contract'),
                    'hotel_vendor_address' => $this->input->post('address'),
                    // purchase count= ?
                    //total amount =? 
                    'hotel_vendor_image' => $asset_thumb,
                    'hotel_vendor_regtype' => $this->input->post('reg_type'),
                    'hotel_vendor_rating' => $this->input->post('rating'),
                    'hotel_vendor_field' => $this->input->post('field'),
                    //'hotel_vendor_name'=>$this->input->post('parent_supp'),
                    'hotel_vendor_image' => $asset_thumb,
                    'hotel_parent_vendor_id' => $this->input->post('parent_supp'),
                    'hotel_vendor_profit_center' => $this->input->post('profit_center'),
                    //'hotel_vendor_tax'=>$this->input->post('vendor_tax_per'),
                    //'hotel_vendor_discount'=>$this->input->post('vendor_discount_per'),
                    'hotel_vendor_reg_form' => $up_file['file_name'],
                    'hotel_vendor_purchase_count' => 0,
                    'hotel_vendor_total_amt' => 0,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                    'hotel_vendor_pin' => $this->input->post('pin_code'),
                    'hotel_vendor_city' => $this->input->post('city'),
                    'hotel_vendor_state' => $this->input->post('state'),
                    'hotel_vendor_email' => $this->input->post('contact_email'),
                    'hotel_vendor_country' => $this->input->post('country'),
                    'hotel_vendor_owner_name' => $this->input->post('director_name'),
                    'hotel_vendor_owner_no' => $this->input->post('manager_no'),
                    'hotel_vendor_owner_email' => $this->input->post('corp_manager_email'),
                    'hotel_vendor_bank' => $this->input->post('bank_name'),
                    'hotel_vendor_account' => $this->input->post('bank_account'),
                    'hotel_vendor_ifsc' => $this->input->post('bank_ifsc'),
                    'hotel_vendor_taxplan' => $this->input->post('tax_plan')
                );


                $query = $this->dashboard_model->add_vendor($up_array);
                if ($query) {
                    $this->session->set_flashdata('succ_msg', 'Data Inserted successfully');
                    redirect('dashboard/all_vendor');
                }
                // }
                /* else
                  {
                  $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                  redirect('dashboard/add_vendor');
                  } */


                //print_r($up_array);
                //die();
            }
            $data['page'] = 'backend/dashboard/add_vendor';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function add_food_plan() {


        //print_r($this->input->post());exit;
        $data['heading'] = 'Food Plan';
        $data['sub_heading'] = '';
        $data['description'] = 'Add Food Plan Here';
        $data['active'] = 'all_food_plans';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        if ($this->input->post()) {

            //print_r($this->input->post());//exit();
            //Image Upload Start Now
            //echo "<pre>";
            $fp = $this->input->post();
            $data1['s_name'] = $fp['s_name'];
            $data1['qty'] = $fp['qty'];
            $data1['s_unit_cost'] = $fp['s_unit_cost'];
            unset($fp['s_name']);
            unset($fp['qty']);
            unset($fp['s_unit_cost']);
            //print_r($fp);
            //exit;
            $this->upload->initialize($this->set_upload_options());
            if ($this->upload->do_upload('fp_image')) {
                $s_image = $this->upload->data('file_name');
                $filename = $s_image;
                $source_path = './upload/' . $filename;
                $target_path = './upload/food_plan/';

                $config_manip = array(
                    //'file_name' => 'myfile',
                    'image_library' => 'gd2',
                    'source_image' => $source_path,
                    'new_image' => $target_path,
                    'maintain_ratio' => TRUE,
                    'create_thumb' => TRUE,
                    'thumb_marker' => '_thumb',
                    'width' => 250,
                    'height' => 250
                );
                $this->load->library('image_lib', $config_manip);
                if (!$this->image_lib->resize()) {
                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                    redirect('dashboard/add_food_plan');
                } else {
                    $thumb_name = explode(".", $filename);
                    $hotel_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                }
                $fp['fp_image'] = $hotel_thumb;
                $query = $this->dashboard_model->add_food_plan($fp, $data1);
                if ($query) {
                    $this->session->set_flashdata('succ_msg', 'Food Plan created successfully');
                } else {
                    $this->session->set_flashdata('err_msg', 'Database Error');
                }
            }
            //Image Upload Section Closed
            else {
                $query = $this->dashboard_model->add_food_plan($fp, $data1);
                if ($query) {
                    $this->session->set_flashdata('succ_msg', 'Food Plan created successfully');
                } else {
                    $this->session->set_flashdata('err_msg', 'Database Error');
                }
            }

            $data['page'] = 'backend/dashboard/add_food_plan';
            $this->load->view('backend/common/index', $data);
        } else {

            //	print_r($this->input->post());exit();
            $data['service_list'] = $this->dashboard_model->all_service_result();
            $data['page'] = 'backend/dashboard/add_food_plan';
            $this->load->view('backend/common/index', $data);
        }
    }

    function all_food_plans() {
        $data['heading'] = 'Food Plan';
        $data['sub_heading'] = '';
        $data['description'] = 'Add Food Plan Here';
        $data['active'] = 'all_food_plans';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['unit'] = $this->dashboard_model->all_food_plans();
        $data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
        $data['page'] = 'backend/dashboard/all_food_plans';
        $this->load->view('backend/common/index', $data);
    }

    function all_vendor() {
        $data['heading'] = 'Finance';
        $data['sub_heading'] = '';
        $data['description'] = 'List of registered';
        $data['active'] = 'all_vendor';
        $data['vendor'] = $this->dashboard_model->all_vendors();
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/all_vendor';
        $this->load->view('backend/common/index', $data);
    }

    function delete_vendor() {


        echo $id = $_GET['vendor_id'];
        exit();
        $this->dashboard_model->check_vendor($id);

        $this->dashboard_model->delete_vendor($id);
        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function get_booked_room_on_current_date() {
        //$date1=$_GET["date1"];
        //$date2=$_GET["date2"];
        date_default_timezone_get("asia/kolkata");
        $date1 = date("Y-m-d", strtotime(date("Y-m-d")));
        $date2 = date("Y-m-d", strtotime(date("Y-m-d")));


        $rooms = $this->dashboard_model->all_rooms();
        $array1 = array();
        $array2 = array();


        $bookings_between = $this->dashboard_model->get_available_room($date1, $date2);



        $j = 0;
        foreach ($bookings_between as $key) {
            # code...
            $array2[$j] = $key->room_id;

            $j++;
        }
//print_r($array2);
        return $array2;
        /*
          $result = array_diff($array1, $array2); */
    }

    function serviceCostById() {
        $id = $_GET["id"];
        $query = $this->dashboard_model->serviceCostById($id);
        $json = json_encode($query);
        echo $json;
    }

    function get_staff_by_type() {
        $a = $this->input->post('type');

        $staff = $this->dashboard_model->get_staff_by_type($a);
        $data = "";
        if (isset($staff) && $staff) {
            $data .= "<option value='' selected='selected'>Staff Name</option>";
            foreach ($staff as $st) {

                $data .= "<option value='" . $st->maid_id . "'>" . $st->maid_name . "</option>";
            }
            echo $data;
        }
    }

    function assign_staff() {
		
        $msg = '';
        $status = '';
        $room_id = $this->input->post('room_id');
        $staff_type = $this->input->post('maid_id');
        $staff_id = $this->input->post('staff_id');
        $prop_type = $this->input->post('prop_type');
		
		/*$room_id = 55;
        $staff_type = 'maid';
        $staff_id = 33;
        $prop_type = 'room';*/
		
        $note = $this->input->post('note');
        $task_id = 0;
        $assignment_type = $this->dashboard_model->get_work_nature_id_by_staff_type($staff_type)->housekeeping_nature_work_id;
		
		if($note)
			$note = $note;
		else
			$note = $staff_type.' assigned';
		
        $up_array = array(
            'room_id' => $room_id,
            'staff_type' => $staff_type,
            'staff_id' => $staff_id,
            'assignment_type' => $assignment_type,
            //'assigned_date'=>date('Y-m-d'),
            'compleated_date' => " ",
            'task_id' => $task_id,
            'status' => 'pending',
            'notes' => $note,
            'hotel_property_type' => $prop_type,
            'hotel_housekeeping_hotel_id' => $this->session->userdata('user_hotel'),
            'hotel_id' => $this->session->userdata('user_hotel'),
        );

        $result = $this->dashboard_model->get_housekeeping_pending_asso_staff($room_id, $staff_type, $prop_type);
        $i = 0;
        foreach ($result as $res) {
            $i++;
        }
		
		//echo $i."<pre>";
        //print_r($up_array); exit;
		
        if ($i == 0) {
            $query = $this->dashboard_model->set_housekeeping_log_entry($up_array);
            $qry = $this->dashboard_model->update_maid_alo_unit($staff_id, "+1");

            if ($staff_type == 'Auditor') {
                if ($prop_type == 'room') {
                    $data = array(
                        'room_id' => $room_id,
                        'room_audit_status' => 'ongoing',
                    );
                }
                if ($prop_type == 'section') {
                    $data = array(
                        'sec_id' => $room_id,
                        'sec_audit_status' => 'ongoing',
                    );
                }

                $this->dashboard_model->update_auditing_status($data, $prop_type);
            }
            $msg = $staff_type . ' Added Sucessfully';
            $data = array(
                'msg' => $msg,
                'status' => 'success',
            );
        } else {
            $msg = $staff_type . ' Already Added '.$i.' '.$room_id.' '.$staff_type;
            $data = array(
                'msg' => $msg,
                'status' => 'warning',
            );
        }


        header('Content-Type: application/json');
        echo json_encode($data);
    } // end

    function delete_fp() {
        $id = $_GET['fp_id'];
        $details = $this->dashboard_model->delete_fp($id);
        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function delete_tax_type() {

        $tax_name = $_POST['taxname'];
        $details = $this->dashboard_model->delete_tax_type($tax_name);
        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function delete_tax_category() {

        $tax_name = $_POST['taxname'];
        $details = $this->dashboard_model->delete_tax_categ($tax_name);
        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function view_food_plan() {
        $id = $_GET['id'];
        $data['heading'] = 'Food Plan';
        $data['sub_heading'] = 'Add Food Plan';
        $data['description'] = 'Add Food Plan Here';
        $data['active'] = 'add_food_plan';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['fp'] = $this->dashboard_model->allFoodPlansById($id);
        $data['service'] = $this->dashboard_model->allServices($id);
        $data['page'] = 'backend/dashboard/view_food_plan';
        $this->load->view('backend/common/index', $data);
    }

    function edit_food_plan() {
        $data['heading'] = 'Food Plan';
        $data['sub_heading'] = 'Edit Food Plan';
        $data['description'] = 'Edit Food Plan Here';
        $data['active'] = 'all_food_plan';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        if ($this->input->post()) {
            $fp = $this->input->post();
            $data1['s_name'] = $fp['s_name'];
            $data1['qty'] = $fp['qty'];
            $data1['s_unit_cost'] = $fp['s_unit_cost'];
            unset($fp['s_name']);
            unset($fp['qty']);
            unset($fp['s_unit_cost']);
            $food_plan_id = $fp['food_plan_id'];
            unset($fp['food_plan_id']);
            $this->upload->initialize($this->set_upload_options());
            if ($this->upload->do_upload('fp_image')) {
                $s_image = $this->upload->data('file_name');
                $filename = $s_image;
                $source_path = './upload/' . $filename;
                $target_path = './upload/food_plan/';

                $config_manip = array(
                    'image_library' => 'gd2',
                    'source_image' => $source_path,
                    'new_image' => $target_path,
                    'maintain_ratio' => TRUE,
                    'create_thumb' => TRUE,
                    'thumb_marker' => '_thumb',
                    'width' => 250,
                    'height' => 250
                );
                $this->load->library('image_lib', $config_manip);
                if (!$this->image_lib->resize()) {
                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                    redirect('dashboard/edit_food_plan');
                } else {
                    $thumb_name = explode(".", $filename);
                    $hotel_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
                }
                $fp['fp_image'] = $hotel_thumb;
                $query = $this->dashboard_model->edit_food_plan($fp, $data1, $food_plan_id);
                if ($query) {
                    $this->session->set_flashdata('succ_msg', 'Food Plan updated successfully');
                } else {
                    $this->session->set_flashdata('err_msg', 'Database Error');
                }
            }
            //Image Upload Section Closed
            else {
                $query = $this->dashboard_model->edit_food_plan($fp, $data1, $food_plan_id);
                if ($query) {
                    $this->session->set_flashdata('succ_msg', 'Food Plan updated successfully');
                } else {
                    $this->session->set_flashdata('err_msg', 'Database Error');
                }
            }
            redirect('dashboard/all_food_plans', 'refresh');
            //$data['page'] = 'backend/dashboard/all_food_plans';
            //$this->load->view('backend/common/index', $data);
        } else {
            $id = $_GET['id'];
            $data['fp'] = $this->dashboard_model->allFoodPlansById($id);
            $data['service'] = $this->dashboard_model->allServices($id);
            $data['service_list'] = $this->dashboard_model->all_service_result();
            $data['page'] = 'backend/dashboard/edit_food_plan';
            $this->load->view('backend/common/index', $data);
        }
    }

    function room_housekeeping() {
        $data['heading'] = 'Housekeeping';
        $data['sub_heading'] = 'Detailed view of rooms';
        $data['description'] = 'Find room status on current date';
        $data['active'] = 'room_housekeeping';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['logs'] = $this->dashboard_model->get_room_housekeeping();
        $data['page'] = 'backend/dashboard/room_housekeeping';
        $this->load->view('backend/common/index', $data);
    }

    function maid_assignment_list() {
        $data['heading'] = 'Housekeeping';
        $data['sub_heading'] = '';
        $data['description'] = 'Room and Assigned Staffs';
        $data['active'] = 'maid_assignment_list';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['logs'] = $this->dashboard_model->get_room_housekeeping();
        $data['page'] = 'backend/dashboard/maid_assignment_list';
        $this->load->view('backend/common/index', $data);
    }

    function knockout_example() {
        $data['heading'] = 'RnD';
        $data['sub_heading'] = 'Knockout Example';
        $data['description'] = 'For RnD purpose only';
        $data['active'] = 'add_food_plan';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['data'] = $this->unit_class_model->hotel_area_code();
        //$data['fp'] = $this->dashboard_model->allFoodPlansById($id);
        //$data['service'] = $this->dashboard_model->allServices($id);
        //$data['page'] = 'backend/dashboard/view_food_plan';
        //$siteaddressAPI = "http://122.163.127.3/hotelobjects_dev/dashboard/test1/";
        //echo $data1 = file_get_contents($siteaddressAPI);
        //exit;
        $data['page'] = 'backend/reports/knockout_example';
        $this->load->view('backend/common/index', $data);
    }

    function test() {
		
        $data['heading'] = 'Test';
        $data['sub_heading'] = 'For our rough tests';
        $data['description'] = 'For our rough tests';
        $data['active'] = '';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        //$data['data'] = $this->unit_class_model->hotel_area_code();
        date_default_timezone_set('Asia/Kolkata');
        $data['page'] = 'backend/dashboard/test';
        $this->load->view('backend/common/index', $data);
    }
	
	function adhoc_bills() {
		
		date_default_timezone_set('Asia/Kolkata');
		$data['heading'] = 'AdHoc Billing';
        $data['sub_heading'] = 'All AdHoc Billing';
        $data['description'] = 'All AdHoc Billing';
        $data['active'] = 'adhoc_bills';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['Ahb_data'] = $this->dashboard_model->get_adhoc_bill_master($id='');
        $data['Ahb_line_data'] = $this->dashboard_model->get_adhoc_bill_line();
        $data['page'] = 'backend/dashboard/adhoc_bills';
        $this->load->view('backend/common/index', $data);
	}
	
	function add_adhoc_bill() {
		
		date_default_timezone_set('Asia/Kolkata');
        $data['heading'] = 'AdHoc Billing';
        $data['sub_heading'] = 'Add AdHoc Billing';
        $data['description'] = 'Add AdHoc Billing';
        $data['active'] = '';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['data'] = $this->unit_class_model->hotel_area_code();
        
		if($this->input->post()) {

			//echo 'hola amigos, welcome to add_adhoc_bill';
			$invoice_no = $this->input->post('invoice_no');
			$bill_date =  date("Y-m-d", strtotime($this->input->post('received_date')));
			$cons_date = date("Y-m-d", strtotime($this->input->post('delivery_date')));
			$service_type = $this->input->post('service_type');
			$counter = $this->input->post('counter');
			$guest_id = $this->input->post('guestID');
			$guest_name = $this->input->post('cust_name');
			$guest_name1 = $this->input->post('gn');
			$guest_type = $this->input->post('guest_type');
			$status = $this->input->post('status_type');
			$payment_status = $this->input->post('payment_status');
			$payments_due_date = $this->input->post('payments_due_date');
			$disc = $this->input->post('discount');
			$adjustment = $this->input->post('adjustment');
			$bill_note = $this->input->post('bill_note');
			
			$product = $this->input->post('product');
			$hsn_sac = $this->input->post('sac');
			$qty = $this->input->post('qty');
			$rate = $this->input->post('price');
			$cgst = $this->input->post('cgst');
			$sgst = $this->input->post('sgst');
			$igst = $this->input->post('igst');
			$utgst = $this->input->post('utgst');
			$total_tax = $this->input->post('tax');
			$discL = $this->input->post('disc');
			$adjustmentL = $this->input->post('adjst');
			$total = $this->input->post('total');
			$note = $this->input->post('note');

			$dataAhb = array(
                'hotel_id' => $this->session->userdata("user_hotel"),
                'invoice_no' => $invoice_no,
                'bill_date' => $bill_date,
                'service_type' => $service_type,
                'counter_id' => $counter,
                'guest_id' => $guest_id,
                'guest_name' => $guest_name1,
                'guest_type' => $guest_type,
                'cons_date' => $cons_date,
                'guest_id' => $guest_id,
                'status' => $status,
                'disc' => $disc,
                'adjustment' => $adjustment,
                'payment_status' => $payment_status,
                'payment_due_date' => $payments_due_date,
                'bill_note' => $bill_note,
                'addedOn' => date('Y-m-d H:i:s')
            );

			/*echo '<pre>dataAhb:</br>';
			print_r($dataAhb);*/
			

			for ($i = 0; $i < count($this->input->post('product')); $i++) {

                    $dataAhbLine[$i] = array(
                        'hotel_id' => $this->session->userdata("user_hotel"),
                        'name' => $product[$i],
                        'hsn_sac' => $hsn_sac[$i],
                        'qty' => $qty[$i],
                        'rate' => $rate[$i],
                        'sgst' => $sgst[$i],
                        'cgst' => $cgst[$i],
                        'igst' => $igst[$i],
                        'utgst' => $utgst[$i],
                        'total_tax' => $total_tax[$i],
                        'disc' => $discL[$i],
                        'adjustment' => $adjustmentL[$i],
                        'total' => $total[$i],
                        'note' => $note[$i],
                        'added_on' => date('Y-m-d H:i:s')
                    );
            }
			
			$query = $this->dashboard_model->add_adhoc_bill($dataAhb,$dataAhbLine);
			
						//PAYMENT SECTION START
						  $pay_amount=$this->input->post('pay_amount');
						  $mop=$this->input->post('pay_mode');
						  if($mop==''){
							  $mop='';
						  }
						  // chk  done
						  $chk_bank_name=$this->input->post('chk_bank_name');
						  $checkno=$this->input->post('checkno');
						  $chk_drawer_name=$this->input->post('chk_drawer_name');
						  
						  // fund  done
						  $card_no=$this->input->post('card_no');
						  $card_name=$this->input->post('card_name');
						  $card_expy=$this->input->post('card_expy');
						  $card_expm=$this->input->post('card_expm');
						  $card_czz=$this->input->post('card_cvv');
						  $card_exp_date=$card_expm.$card_expy;
						  $card_bank_name=$this->input->post('card_bank_name');
						  // draft  done
						  $draft_no=$this->input->post('draft_no');
						  $draft_bank_name=$this->input->post('draft_bank_name');
						  $draft_drawer_name=$this->input->post('draft_drawer_name');
						  
						  // fund  done
						  $ft_bank_name=$this->input->post('ft_bank_name');
						  $ft_account_no=$this->input->post('ft_account_no');
						  $ft_ifsc_code=$this->input->post('ft_ifsc_code');
						  
						  //wallet 
						    $wallet_name=$this->input->post('wallet_name');
						    $wallet_tr_id=$this->input->post('wallet_tr_id');
						    $wallet_rec_acc=$this->input->post('wallet_rec_acc');
						 
						  $ft_approved_by=$this->input->post('ft_approved_by');
						  $ft_recievers_name=$this->input->post('ft_recievers_name');
						  $ft_paid_by=$this->input->post('ft_paid_by');
						  $ft_recievers_desig=$this->input->post('ft_recievers_desig');
						  $profit_center1=$this->input->post('p_center');
						  $p_status=$this->input->post('p_status');
						  
						  if(isset($card_bank_name)){
							   $t_bank_name=$card_bank_name;
						  }else if(isset($draft_bank_name)){
							  $t_bank_name=$draft_bank_name;
						  }else if(isset($ft_bank_name)){
							  $t_bank_name=$ft_bank_name;
						  }else if(isset($chk_bank_name)){
							  $t_bank_name=$chk_bank_name;
						  }
						 
						  $card_type=$this->input->post('card_type');
						  $t_type=$this->unit_class_model->get_transaction_type(15);
						  $from=$t_type->hotel_transaction_type_from_entity_id;
						  $to=$t_type->hotel_transaction_type_to_entity_id;

						$adhoc_payment=array(
							'transaction_type' => "Misc service Income",
							't_date'=>date("Y-m-d H:i:s"),
							'details_type' => "Misc service Income",
							't_card_type'=>$card_type,
							't_amount'=>$pay_amount,
							't_payment_mode'=>$mop,
							'checkno'=>$checkno,
							't_bank_name'=>$t_bank_name,
							't_drw_name'=>$chk_drawer_name,
							't_card_no'=>$card_no,
							't_card_name'=>$card_name,
							't_card_exp_date'=>$card_exp_date,
							't_status'=>$p_status,
							'draft_no'=>$draft_no,
							'draft_bank_name'=>$draft_bank_name,
							'ft_bank_name'=>$ft_bank_name,						
							'ft_account_no'=>$ft_account_no,
							'ft_ifsc_code'=>$ft_ifsc_code,
							't_w_name'=>$wallet_name,
							't_tran_id'=>$wallet_tr_id,
							't_recv_acc'=>$wallet_rec_acc,
							'details_type'=>'Adhoc Billing',
							'details_id'=>$query,
							'transaction_releted_t_id'=>15,
							'transaction_type_id'=>15,
							'p_center'=>$profit_center1,
							't_booking_id'=>$guest_booking_id,
							'hotel_id'=>$this->session->userdata('user_hotel'),
							'user_id'=>$this->session->userdata('user_id'),
							'transaction_from_id' => $from,
							'transaction_to_id' => $to,
							't_to' => 'Hotel',
							't_from' => $guest_name,
							't_cashDrawer_id' => $this->session->userdata('cd_key'),
							'transactions_detail_notes'=>'Recieved payment of Rs.'.$pay_amount.' for Adhoc Bill no '.$query.'('.$service_type.') from '.$guest_name1.' on '.date("Y-m-d H:i:s")
						);

			/*echo '<pre>dataAhbLine:</br>';
			print_r($dataAhbLine);
			exit;*/

				$queryT = $this->dashboard_model->add_adhoc_bill_tran($adhoc_payment);
				
                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Great, your AdHoc Bill with id ".$query." has been generated successfully!");
                    redirect('dashboard/view_adhoc_bill?id='.$query);
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again again. If this repeats please contact the admin!");
                    redirect('dashboard/add_adhoc_bill');
                }

		} else {
			$data['page'] = 'backend/dashboard/add_adhoc_bill';
			$this->load->view('backend/common/index', $data);
		}

    } // End add_adhoc_bill
    
    
   // edit adhoc bill
    
    	function edit_adhoc_bill() {
		
		date_default_timezone_set('Asia/Kolkata');
        $data['heading'] = 'AdHoc Billing';
        $data['sub_heading'] = 'Add AdHoc Billing';
        $data['description'] = 'Add AdHoc Billing';
        $data['active'] = '';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['data'] = $this->unit_class_model->hotel_area_code();
        
		if($this->input->post()) {

			//echo 'hola amigos, welcome to add_adhoc_bill';
			$ahb_id = $this->input->post('ahb_id');
			$invoice_no = $this->input->post('invoice_no');
			$bill_date =  date("Y-m-d", strtotime($this->input->post('received_date')));
			$cons_date = date("Y-m-d", strtotime($this->input->post('delivery_date')));
			$service_type = $this->input->post('service_type');
			$counter = $this->input->post('counter');
			$guest_id = $this->input->post('guestID');
			$guest_name = $this->input->post('cust_name');
			$guest_name1 = $this->input->post('gn');
			$guest_type = $this->input->post('guest_type');
			$status = $this->input->post('status_type');
			$payment_status = $this->input->post('payment_status');
			$payments_due_date = $this->input->post('payments_due_date');
			$disc = $this->input->post('discount');
			$adjustment = $this->input->post('adjustment');
			$bill_note = $this->input->post('bill_note');
			
			$product = $this->input->post('product');
			$hsn_sac = $this->input->post('sac');
			$qty = $this->input->post('qty');
			$rate = $this->input->post('price');
			$cgst = $this->input->post('cgst');
			$sgst = $this->input->post('sgst');
			$igst = $this->input->post('igst');
			$utgst = $this->input->post('utgst');
			$total_tax = $this->input->post('tax');
			$discL = $this->input->post('disc');
			$adjustmentL = $this->input->post('adjst');
			$total = $this->input->post('total');
			$note = $this->input->post('note');

			$dataAhb = array(
                'hotel_id' => $this->session->userdata("user_hotel"),
                'invoice_no' => $invoice_no,
                'bill_date' => date('Y-m-d',strtotime($bill_date)),
                'service_type' => $service_type,
                'counter_id' => $counter,
                'guest_id' => $guest_id,
                'guest_name' => $guest_name1,
                'guest_type' => $guest_type,
                'cons_date' => $cons_date,
                'guest_id' => $guest_id,
                'status' => $status,
                'disc' => $disc,
                'adjustment' => $adjustment,
                'payment_status' => $payment_status,
                'payment_due_date' => date('Y-m-d',strtotime($payments_due_date)),
                'bill_note' => $bill_note,
                'addedOn' => date('Y-m-d H:i:s')
            );

			/*echo '<pre>dataAhb:</br>';
			print_r($dataAhb);*/
			

			for ($i = 0; $i < count($this->input->post('product')); $i++) {

                    $dataAhbLine[$i] = array(
                        'hotel_id' => $this->session->userdata("user_hotel"),
                        'ahb_id' => $ahb_id,
                        'name' => $product[$i],
                        'hsn_sac' => $hsn_sac[$i],
                        'qty' => $qty[$i],
                        'rate' => $rate[$i],
                        'sgst' => $sgst[$i],
                        'cgst' => $cgst[$i],
                        'igst' => $igst[$i],
                        'utgst' => $utgst[$i],
                        'total_tax' => $total_tax[$i],
                        'disc' => $discL[$i],
                        'adjustment' => $adjustmentL[$i],
                        'total' => $total[$i],
                        'note' => $note[$i],
                        'added_on' => date('Y-m-d H:i:s')
                        
                    );
                    
                    
                   // echo '<pre>$dataAhbLine:</br>';
			     //   print_r($dataAhbLine);
            
            }
			
		//	exit;
			
			
			
			
						//PAYMENT SECTION START
						  $pay_amount=$this->input->post('pay_amount');
						  $mop=$this->input->post('pay_mode');
						  if($mop==''){
							  $mop='';
						  }
						  // chk  done
						  $chk_bank_name=$this->input->post('chk_bank_name');
						  $checkno=$this->input->post('checkno');
						  $chk_drawer_name=$this->input->post('chk_drawer_name');
						  
						  // fund  done
						  $card_no=$this->input->post('card_no');
						  $card_name=$this->input->post('card_name');
						  $card_expy=$this->input->post('card_expy');
						  $card_expm=$this->input->post('card_expm');
						  $card_czz=$this->input->post('card_cvv');
						  $card_exp_date=$card_expm.$card_expy;
						  $card_bank_name=$this->input->post('card_bank_name');
						  // draft  done
						  $draft_no=$this->input->post('draft_no');
						  $draft_bank_name=$this->input->post('draft_bank_name');
						  $draft_drawer_name=$this->input->post('draft_drawer_name');
						  
						  // fund  done
						  $ft_bank_name=$this->input->post('ft_bank_name');
						  $ft_account_no=$this->input->post('ft_account_no');
						  $ft_ifsc_code=$this->input->post('ft_ifsc_code');
						  
						  //wallet 
						    $wallet_name=$this->input->post('wallet_name');
						    $wallet_tr_id=$this->input->post('wallet_tr_id');
						    $wallet_rec_acc=$this->input->post('wallet_rec_acc');
						 
						  $ft_approved_by=$this->input->post('ft_approved_by');
						  $ft_recievers_name=$this->input->post('ft_recievers_name');
						  $ft_paid_by=$this->input->post('ft_paid_by');
						  $ft_recievers_desig=$this->input->post('ft_recievers_desig');
						  $profit_center1=$this->input->post('p_center');
						  $p_status=$this->input->post('p_status');
						  
						  if(isset($card_bank_name)){
							   $t_bank_name=$card_bank_name;
						  }else if(isset($draft_bank_name)){
							  $t_bank_name=$draft_bank_name;
						  }else if(isset($ft_bank_name)){
							  $t_bank_name=$ft_bank_name;
						  }else if(isset($chk_bank_name)){
							  $t_bank_name=$chk_bank_name;
						  }
						 
						  $card_type=$this->input->post('card_type');
						  $t_type=$this->unit_class_model->get_transaction_type(15);
						  $from=$t_type->hotel_transaction_type_from_entity_id;
						  $to=$t_type->hotel_transaction_type_to_entity_id;

						$adhoc_payment=array(
							'transaction_type' => "Misc service Income",
							't_date'=>date("Y-m-d H:i:s"),
							'details_type' => "Misc service Income",
							't_card_type'=>$card_type,
							't_amount'=>$pay_amount,
							't_payment_mode'=>$mop,
							'checkno'=>$checkno,
							't_bank_name'=>$t_bank_name,
							't_drw_name'=>$chk_drawer_name,
							't_card_no'=>$card_no,
							't_card_name'=>$card_name,
							't_card_exp_date'=>$card_exp_date,
							't_status'=>$p_status,
							'draft_no'=>$draft_no,
							'draft_bank_name'=>$draft_bank_name,
							'ft_bank_name'=>$ft_bank_name,						
							'ft_account_no'=>$ft_account_no,
							'ft_ifsc_code'=>$ft_ifsc_code,
							't_w_name'=>$wallet_name,
							't_tran_id'=>$wallet_tr_id,
							't_recv_acc'=>$wallet_rec_acc,
							'details_type'=>'Adhoc Billing',
						//	'details_id'=>$query,
							'transaction_releted_t_id'=>15,
							'transaction_type_id'=>15,
							'p_center'=>$profit_center1,
							't_booking_id'=>$guest_booking_id,
							'hotel_id'=>$this->session->userdata('user_hotel'),
							'user_id'=>$this->session->userdata('user_id'),
							'transaction_from_id' => $from,
							'transaction_to_id' => $to,
							't_to' => 'Hotel',
							't_from' => $guest_name,
							't_cashDrawer_id' => $this->session->userdata('cd_key'),
							'transactions_detail_notes'=>'Recieved payment of Rs.'.$pay_amount.' for Adhoc Bill no '.$query.'('.$service_type.') from '.$guest_name1.' on '.date("Y-m-d H:i:s")
						);

		
			
		$query =  $this->dashboard_model->edit_adhoc_bill($dataAhb,$dataAhbLine,$adhoc_payment,$ahb_id);

			//	$queryT = $this->dashboard_model->add_adhoc_bill_tran($adhoc_payment);
				
                if (isset($query) && $query!='') {
                    $this->session->set_flashdata('succ_msg', "Great, your AdHoc Bill with id ".$query." has been generated successfully!");
                    redirect('dashboard/view_adhoc_bill?id='.$query);
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again again. If this repeats please contact the admin!");
                    redirect('dashboard/add_adhoc_bill');
                }

		} else {
			$data['page'] = 'backend/dashboard/add_adhoc_bill';
			$this->load->view('backend/common/index', $data);
		}

    }
	
    
    
	// end edit adhoc bill
	
	function test_offset() {
        $data['heading'] = 'Test';
        $data['sub_heading'] = 'For our rough tests';
        $data['description'] = 'For our rough tests';
        $data['active'] = '';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['data'] = $this->unit_class_model->hotel_area_code();
        date_default_timezone_set('Asia/Kolkata');

        $data['page'] = 'backend/dashboard/test_offset';
        $this->load->view('backend/common/index', $data);
    }

    // add method 03-12-16....NB...

    function showtaxplan() {
        $data['heading'] = 'Tax Plan';
        $data['sub_heading'] = 'Details of tax plan';
        $data['description'] = 'Description of tax plan elements';
        $data['active'] = 'show_tax_plan';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        //$data['data']=$this->unit_class_model->hotel_area_code();
        $price = 1500;
        $args = 'a';
        $type = 'Booking';
        $date = '2016-12-01';
        $times = $this->dashboard_model->getTaxElements($price, $date, $type, $args);  // Fetching appropiate tax plan from tax plan view based on arguments
        if (isset($times)) {
            $tax = array();
            $i = 0;
            $tot = 0;
            $tax['planName'] = $times[0]->r_name;   // Assumed, plan name will be same for all returned rows
            if ($args == 'a') {


                foreach ($times as $res) {
                    if ($times[$i]->calculated_on == 'n_r') {
                        $taxAmt = ($times[$i]->tax_mode == 'p' ? ($times[$i]->tax_value * $price / 100) : ($times[$i]->tax_value));
                        $tax[$times[$i]->tax_name] = $taxAmt;
                    } else if ($times[$i]->calculated_on == 'n_p' && ($i > 0)) {
                        $taxAmt = ($times[$i]->tax_mode == 'p' ? ($times[$i]->tax_value * ($price + $tot) / 100) : ($times[$i]->tax_value));
                        $tax[$times[$i]->tax_name] = $taxAmt;
                    }

                    $tot = $tot + $taxAmt;
                    $i++;
                } // End foreach
            } else
                $tax = $times;
        }
        echo "<pre>";
        print_r($tax);
        echo "</pre>";
        //$data['page']='backend/dashboard/test';
        //	$this->load->view('backend/common/index', $data);
    }

    function showtaxplan1() {

        date_default_timezone_set('Asia/Kolkata');

        $price = $this->input->post('price');
        $args = $this->input->post('args');
        $type = $this->input->post('type1');
        $date1 = $this->input->post('dates');
		 $g_state = strtoupper($this->input->post('g_state'));
        // echo "date1==>>>>>".$date1;
        $date2 = date("Y-m-d", strtotime($date1));
		//echo 'date is '.$date2.'<pre>';
        //echo "price".$price.$date2.$type;
        $times = $this->dashboard_model->getTaxElements($price, $date2, $type, $args,$g_state);  // Fetching appropiate tax plan from tax plan view based on arguments
		//print_r($times);
        $tax = array();
        if ($times != 0) {
            $i = 0;
            $tot = 0;
            $tax['r_id'] = $times[0]->r_id;
            $tax['planName'] = $times[0]->r_name;   // Assumed, plan name will be same for all returned rows
            if ($args == 'a') {


                foreach ($times as $res) {
                    if ($times[$i]->calculated_on == 'n_r') {
                        $taxAmt = ($times[$i]->tax_mode == 'p' ? ($times[$i]->tax_value * $price / 100) : ($times[$i]->tax_value));
                        $tax[$times[$i]->tax_name] = $taxAmt;
                    } else if ($times[$i]->calculated_on == 'n_p' && ($i > 0)) {
                        $taxAmt = ($times[$i]->tax_mode == 'p' ? ($times[$i]->tax_value * ($price + $tot) / 100) : ($times[$i]->tax_value));
                        $tax[$times[$i]->tax_name] = $taxAmt;
                    }else{
						 $taxAmt = 0;
					}

                    $tot = $tot + $taxAmt;
                    $i++;
                } // End foreach
            } else
                $tax = $times;
        }
        else {
            $tot = 0.00;
        }

        $tax['total'] = $tot;   //add total tax in array
//print_r($tax);
        header('Content-Type: application/json');
        echo json_encode($tax);
        // print_r($tax);
    }

// End function showtaxplan1()

    function showtaxplanNB() {

        //alert('hello');
        //echo ;
        /* $price = 1500;
          $args = 'a';
          $type = 'Booking';
          $date = '2016-12-01'; */
        date_default_timezone_set('Asia/Kolkata');

        $prices = $this->input->post('prices');

        $argss = $this->input->post('argss');
        $types = $this->input->post('typess');
        $dates1 = $this->input->post('datess');


        // echo "date1==>>>>>".$date1;
        $dates2 = date("Y-m-d", strtotime($dates1));
        //echo "hello".$prices.$argss.$types. $dates2;	
        //echo "price".$prices.$dates2.$types;
        $times = $this->dashboard_model->getTaxElements($prices, $dates2, $types, $argss);  
        $tax = array();
        if ($times != 0) {
            //print_r($times);
            //exit;

            $i = 0;
            $tot = 0;
            $tax['r_id'] = $times[0]->r_id;
            $tax['planName'] = $times[0]->r_name;   
            if ($args == 'a') {


                foreach ($times as $res) {
                    if ($times[$i]->calculated_on == 'n_r') {
                        $taxAmt = ($times[$i]->tax_mode == 'p' ? ($times[$i]->tax_value * $prices / 100) : ($times[$i]->tax_value));
                        $tax[$times[$i]->tax_name] = $taxAmt;
                    } else if ($times[$i]->calculated_on == 'n_p' && ($i > 0)) {
                        $taxAmt = ($times[$i]->tax_mode == 'p' ? ($times[$i]->tax_value * ($prices + $tot) / 100) : ($times[$i]->tax_value));
                        $tax[$times[$i]->tax_name] = $taxAmt;
                    }

                    $tot = $tot + $taxAmt;
                    $i++;
                } // End foreach
            } else {
                $tax = $times;
            }
        } else {
            $tot = 0.00;
        }

        $tax['total'] = $tot; //add total tax in array



        print_r($tax);
        //header('Content-Type: application/json');
        // echo json_encode($tax);
    }

    function test1() {
        $data['heading'] = 'Food Plan';
        $data['sub_heading'] = 'Add Food Plan';
        $data['description'] = 'Add Food Plan Here';
        $data['active'] = 'add_food_plan';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['unit'] = $this->dashboard_model->all_food_plans();
        $data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
        $data['page'] = 'backend/dashboard/test1';
        $this->load->view('backend/common/ajax_index1', $data);
    }

    function change_room_status_bulk() {

        $status = $this->input->post('room_status');
        //print_r ($this->input->post());
        //exit;
        $logs = $this->dashboard_model->get_room_housekeeping();
        if (isset($logs) && $logs) {
            foreach ($logs as $lg) {
                if ($this->input->post('get_' . $lg->room_id)) {
                    $room_id = $this->input->post('get_' . $lg->room_id);



                    $data = array(
                        'room_id' => $room_id,
                        'clean_id' => $status,
                    );

                    $this->dashboard_model->update_room($data, 'room');
                    if ($status == 1) {
                        $this->dashboard_model->update_housekeeping_log_status_by_staff_type($room_id, 'Maid', 'room');
                    }
                }
            }
            redirect(base_url() . "dashboard/room_housekeeping");
        }
    }

    function add_random_maid() {
        $rooms = $this->dashboard_model->all_rooms();
        $i = 0;
        $room_set = array();
        foreach ($rooms as $rm) {
            $room_set[$i] = $rm->room_id;
            $i++;
        }


        for ($k = $i - 1; $k >= 0; $k--) {
            $result = $this->dashboard_model->get_housekeeping_pending_asso_staff($room_set[$k], 'Maid', 'room');
            $len = 0;
            foreach ($result as $res) {
                $len++;
            }
            if ($len == 0) {
                $maid = $this->dashboard_model->get_staff_by_type('Maid');
                if ($maid != false) {
                    $j = 0;
                    $maid_set = array();

                    foreach ($maid as $md) {


                        $maid_set[$j] = $md->maid_id;
                        $j++;
                    }
                    $rn = rand(0, $j - 1);

                    //print_r($maid_set);
                    //echo $k."====".$room_set[$k]."=====>".$maid_set[$rn]."<br>";
                    $task_id = 0;
                    $up_array = array(
                        'room_id' => $room_set[$k],
                        'staff_type' => 'Maid',
                        'staff_id' => $maid_set[$rn],
                        'assignment_type' => $this->dashboard_model->get_work_nature_id_by_staff_type('Maid')->housekeeping_nature_work_id,
                        'assigned_date' => date('Y-m-d'),
                        'compleated_date' => " ",
                        'task_id' => $task_id,
                        'status' => 'pending',
                        'notes' => 'add Notes',
                        'hotel_property_type' => 'room',
                        'hotel_housekeeping_hotel_id' => $this->session->userdata('user_hotel'),
                        'hotel_id' => $this->session->userdata('user_hotel'),
                    );
                    $this->dashboard_model->set_housekeeping_log_entry($up_array);
                    $qry = $this->dashboard_model->update_maid_alo_unit($maid_set[$rn], "+1");
                }
            }
        }
        redirect(base_url() . 'dashboard/maid_assignment_list');
    }

    function getAvailableRoomAndEvent() {
		
        $date1 = date("Y-m-d", strtotime($this->input->post("date1")));
        $date2 = date("Y-m-d", strtotime($this->input->post("date2")));
        $rooms = $this->dashboard_model->all_rooms();
        $array1 = array();
        $array2 = array();
        $bookings_between = $this->dashboard_model->get_available_room($date1, $date2); // Getting the booked rooms
        $i = 0;
        $j = 0;
        foreach ($rooms as $room) {
            $array1[$i] = $room->room_id;
            $i++;
        }
        foreach ($bookings_between as $key) {
            $array2[$j] = $key->room_id;
            $j++;
        }
        $result = array_diff($array1, $array2);
        $ut1 = $this->dashboard_model->all_unit_type();
        
        $data1 = "";
        
        $data1 = '<div class="all-re"><div class="all-retitle uppercase"><strong style="color:#33D0E1; font-weight:800;">Available Room</strong></div><div class="portlet-body form">';

        foreach ($ut1 as $ut) {
            $data1 .= '<h3>'.$ut->unit_name.' <span style="font-size:10px; font-weight:400">('.$ut->default_occupancy.'/ '.$ut->max_occupancy.')</span></h3>';
            $z = 0;
            $data1 .= '<div class="bc">';
            foreach ($result as $res) {

                $room = $this->dashboard_model->get_room_number_exact_unit($res, $ut->id);
                //print_r($room);
                if ($room) {
                    $room_number = $room->room_no;
                    $room_numberSt = "'".$room->room_no."'";
                    $data1 .= ' <div class="btn-group"> <label class="btn grey-cascade btn-xs" id="abc_' . $res . '"> <input class="noClick" id="ab_' . $res . '" style="display: none;" onclick="addRow1(this.value,' . $room_numberSt . ')" value="' . $res . '" type="checkbox" > ' . $room_number . ' </label></div>';
                    $z++;
                }
            }
            if ($z == 0) {
                $data1 .= '<p><i class="fa fa-exclamation" aria-hidden="true"></i> No Room available.</p>';
            }
            $data1 .= '</div>';
        }
        
        $data1 .= '</div></div>';
        $data2 = "";
        $data2 .= '<div class="all-re"><div class="all-retitle uppercase"><strong style="color:#33D0E1; font-weight:800;">All Events</strong></div><div class="portlet-body form">';
        $events = $this->dashboard_model->all_events();
        if ($events && isset($events)) {
            foreach ($events as $event) {
                if ((date("Y-m-d", strtotime($event->e_from)) <= date("Y-m-d", strtotime($this->input->post("date2"))) && date("Y-m-d", strtotime($event->e_upto)) >= date("Y-m-d", strtotime($this->input->post("date1"))))) {
                    $data2 .= '<div style="font-size:13px; margin-top:7px; text-align: center;"> <span style="padding:4px 10px; text-transform: capitalize; display:inline-block; background:' . $event->event_color . '; color:' . $event->event_text_color . '; margin-right:5px;"><b> ' . $event->e_name . '</b></span> <span style="margin-right:5px; color:#2B3643">' . date("d-m-Y", strtotime($event->e_from)) . ' </span> to <span style="margin-left:5px; color:#2B3643">' . date("d-m-Y", strtotime($event->e_upto)) . '</span> </div>';
                }
            }
        }
        $data2 .= '</div></div>';
        echo $data = $data1 . '~' . $data2;
    } // End getAvailableRoomAndEvent

    function add_profit_center() {

        $found = in_array("94", $this->arraay);
        if ($found) {
            $data['heading'] = 'Setting';
            $data['sub_heading'] = 'Metadata';
            $data['description'] = 'Add Profit Center here';
            $data['active'] = 'booking_status_type';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {


                /* Images Upload */
                date_default_timezone_set("Asia/Kolkata");

                $add_prfit_center = array(
                    'profit_center_location' => $this->input->post('pc_location'),
                    'profit_center_balance' => $this->input->post('balance'),
                    'profit_center_des' => $this->input->post('des'),
                    'profit_center_default' => $this->input->post('default'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );

                $query = $this->dashboard_model->add_pc($add_prfit_center);

                $ch = $this->input->post('default');
                if ($ch == '1') {
                    $query1 = $this->dashboard_model->update_default_pc($query);
                }

                if ($query) {
                    $this->session->set_flashdata('succ_msg', 'Prfit Center added sucessfully');
                } else {
                    $this->session->set_flashdata('err_msg', 'Database Error');
                }
                //$data['page'] = 'backend/dashboard/all_profit_center';
                redirect(base_url() . 'dashboard/all_profit_center');
            }



            $data['page'] = 'backend/dashboard/add_profit_center';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function delete_extra_charge() {
        $data['heading'] = '';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        $ch_id = $_GET['a_id'];

        $query = $this->dashboard_model->delete_extra_charge($ch_id);

        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function get_unit_details($unit_id) {
        
    }

    function remove_charge_from_booking_group() {
        $s_id = $_GET['c_id'];
        $booking_id = $_GET['booking_id'];

        $price_data = $this->dashboard_model->get_charge_details($s_id);
        $price = $price_data->crg_total;

        $service_array = $this->dashboard_model->get_group_booking_details($booking_id);

        $service_string = $service_array->charges_id;
        $service_price_total = $service_array->charges_cost;


        $new_price = $service_price_total - $price;

        $array = explode(",", $service_string);

        // print_r($array);
        // exit();
        $new_service_array = array();

        for ($i = 1; $i < sizeof($array); $i++) {
            # code...

            if ($array[$i] == $s_id) {

                // echo "yes";
                unset($array[$i]);
                //var_dump($array);
                break;
            }
        }

        $service_new_string = implode(",", $array);

        //echo $service_new_string;
        //exit();

        $new_service_array = array('charges_id' => $service_new_string, 'charges_cost' => $new_price);


        $query = $this->dashboard_model->delete_group_charge($booking_id, $new_service_array);
        $q = $this->dashboard_model->delete_extra_charges($s_id);

        redirect("dashboard/booking_edit_group?group_id=" . $booking_id);

        //exit();
    }

    function extra_charge_report() {

        $found = in_array("38", $this->arraay);
        if ($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Reservation Reports';
            $data['description'] = 'Extra Charge Report';
            $data['active'] = 'extra_charge_report';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            if (($this->input->post('t_dt_frm')) && ($this->input->post('t_dt_to'))) {
                $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
                $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
                $data['start_date'] = $this->input->post('t_dt_frm');
                $data['end_date'] = $this->input->post('t_dt_to');

                $data['bookings'] = $this->dashboard_model->all_bookings_by_date($start_date, $end_date);
                $data['groups'] = $this->dashboard_model->all_group_by_date($start_date, $end_date);
            } else {
                $data['bookings'] = $this->dashboard_model->all_bookings();
                $data['groups'] = $this->dashboard_model->all_group();
            }

            $data['page'] = 'backend/dashboard/extra_charge_report';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    //edit transactions
    function edit_transaction($id) {
         $found = in_array("27", $this->arraay);
        if ($found) {
        $this->load->model('dashboard_model');
        //$this->load->model('unit_class_model');
        $data['heading'] = 'Edit Transaction';
        $data['sub_heading'] = 'Edit Transaction';
        $data['description'] = '';
        $data['active'] = 'edit_stock_inventory_category';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        //$data['data'] = $this->unit_class_model->data_warehouse($id);
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['value'] = $this->dashboard_model->data_transactions($id);



        if ($this->input->post()) {

            $id = $this->input->post('hid'); //hidden
            $transaction_type = $this->input->post('transaction_type');
            $t_booking_id = $this->input->post('t_booking_id');
            $t_group_id = $this->input->post('t_group_id');
            $purchase_id = $this->input->post('purchase_id');
            $payment_id = $this->input->post('payment_id');
            $details_id = $this->input->post('details_id');
            $t_payment_mode = $this->input->post('t_payment_mode');

            $checkno = $this->input->post('checkno');
            $check_bank_name = $this->input->post('check_bank_name');
            $draft_no = $this->input->post('draft_no');
            $draft_bank_name = $this->input->post('draft_bank_name');
            $ft_bank_name = $this->input->post('ft_bank_name');
            $ft_account_no = $this->input->post('ft_account_no');
            $ft_ifsc_code = $this->input->post('ft_ifsc_code');

            $t_status = $this->input->post('t_status');
            $transaction_from_id = $this->input->post('transaction_from_id');
            $transaction_to_id = $this->input->post('transaction_to_id');
            $t_hotel_id = $this->input->post('t_hotel_id');
            $transaction_releted_t_id = $this->input->post('transaction_releted_t_id');
            $profit_center = $this->input->post('p_center');
            $details_type = $this->input->post('details_type');
            $amount = $this->input->post('t_amount');
            $payment_mode = $this->input->post('payment_mode');
            $transactions_detail_notes = $this->input->post('transactions_detail_notes');
            date_default_timezone_set("Asia/Kolkata");
            $t_date = date('Y-m-d H:i:s');
            $data = array(
                'transaction_type' => $transaction_type,
                't_booking_id' => $t_booking_id,
                't_group_id' => $t_group_id,
                'purchase_id' => $purchase_id,
                'payment_id' => $payment_id,
                'details_id' => $details_id,
                'details_type' => $details_type,
                't_amount' => $amount,
                'last_modifies_on' => $t_date, //	date('Y-m-d H:i:s')
                't_payment_mode' => $t_payment_mode,
                'checkno' => $checkno,
                't_bank_name' => $check_bank_name,
                'draft_no' => $draft_no,
                'draft_bank_name' => $draft_bank_name,
                'ft_bank_name' => $ft_bank_name,
                'ft_account_no' => $ft_account_no,
                'ft_ifsc_code' => $ft_ifsc_code,
                't_status' => $t_status,
                'transaction_from_id' => $transaction_from_id,
                'transaction_to_id' => $transaction_to_id,
                'transaction_releted_t_id' => $transaction_releted_t_id,
                'hotel_id' => $t_hotel_id,
                'p_center' => $profit_center,
                'transactions_detail_notes' => $transactions_detail_notes,
                't_id' => $id
            );
            //print_r($data);
            //exit();
            $query = $this->dashboard_model->update_transactions($data);
            if ($query) {
                $msg = "inserted";
                $this->session->set_flashdata('succ_msg', 'Data updated Succefully');
            } else {

                $msg = " not inserted";
                $this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
            }
            redirect(base_url() . 'dashboard/all_transactions');
        }
        $data['page'] = 'backend/dashboard/edit_transactions';
        $this->load->view('backend/common/index', $data);
        }else{
        redirect('dashboard');
    }
    }

    //delete hotel transaction

     function delete_hotel_transaction() {
       $found = in_array("27", $this->arraay);
        if ($found) {
        $id = $_GET['id'];

        $this->dashboard_model->delete_hotel_transaction($id);
        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }else{
         $data = array(
            'data' => "0",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    
    }

    function pdf_reservation_report() {

        // $data['bookings'] = $this->dashboard_model->all_bookings();
        // $data['transactions'] = $this->dashboard_model->all_transactions();
        //$data['bookings'] = $this->dashboard_model->all_bookings();
        //print_r($data);
        //exit();

        $data['reports'] = $this->dashboard_model->all_bookings();
        $this->load->view("backend/dashboard/reservation_report_pdf", $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper('a4', 'landscape');
        $this->dompdf->render();
        $this->dompdf->stream("reservation_report.pdf");
    }

    function pdf_booking_report() {

        $data['bookings'] = $this->dashboard_model->all_bookings();
        $data['transactions'] = $this->dashboard_model->all_transactions();
        //$data['reports'] = $this->dashboard_model->all_bookings();
        $this->load->view("backend/dashboard/all_booking_pdf", $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper('a4', 'landscape');
        $this->dompdf->render();
        $this->dompdf->stream("all_bookings.pdf");
    }

    function pdf_reservation_summary() {

        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['summary'] = $this->dashboard_model->fetch_r_summary();
        $this->load->view("backend/dashboard/pdf_reservation_summary", $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper('a4', 'landscape');

        $this->dompdf->render();
        $this->dompdf->stream("reservation_summary.pdf");
    }

    function pdf_note_preference_report() {

        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['bookings'] = $this->dashboard_model->all_bookings();

        $this->load->view("backend/dashboard/pdf_note_preference_report", $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper('a4', 'landscape');

        $this->dompdf->render();
        $this->dompdf->stream("note_preference_report.pdf");
    }

    function pdf_monthly_summary() {
        if ($this->input->post()) {
            $data['m'] = $this->input->post('month');
            $data['y'] = $this->input->post('year');
            $data['maxDays'] = cal_days_in_month(CAL_GREGORIAN, $data['m'], $data['y']);
        } else {
            $data['y'] = date("Y");
            $data['m'] = date("m");
            $data['maxDays'] = date('t');
        }
        $data['summary'] = $this->dashboard_model->fetch_r_summary();
        $data['totalrooms'] = $this->dashboard_model->totalrooms();

        $x = count($data['totalrooms']);

        $data['totalrooms'] = $x;

        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $this->load->view("backend/dashboard/pdf_monthly_summary_report", $data);

        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html(html_entity_decode($html));
        $this->dompdf->set_paper('a4', 'landscape');
        $this->dompdf->render();
        $this->dompdf->stream("monthly_summary_report.pdf");
    }

    function tax_report_pdf() {
        $data['heading'] = 'Hotel Reports';
        $data['sub_heading'] = 'Tax Reports';
        $data['description'] = 'Tax Report';
        $data['active'] = 'tax_report';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        if (($this->input->get('s_d') == "") && ($this->input->get('e_d') == "")) {
            $data['bookings'] = $this->dashboard_model->all_bookings();
        } else {
            $start_date = date("Y-m-d", strtotime($this->input->get('s_d')));
            $end_date = date("Y-m-d", strtotime($this->input->get('e_d')));
            $data['bookings'] = $this->dashboard_model->all_tax_report_by_date($start_date, $end_date);
        }

        $this->load->view('backend/dashboard/tax_report_pdf', $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html(html_entity_decode($html));
        $this->dompdf->set_paper('a4', 'landscape');
        //$this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("tax_report.pdf");
    }

    function tax_type_settings() {
        $found = in_array("144", $this->arraay);
        if ($found) {
            $data['heading'] = 'Setting';
            $data['sub_heading'] = 'Tax Settings';
            $data['description'] = 'Tax elements list';
            $data['active'] = 'tax_type_settings';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
            $data['tax_details'] = $this->dashboard_model->tax_type_setting();
            //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
            $data['page'] = 'backend/dashboard/tax_type';
            //print_r($data);die();

            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function tax_type_add() {
        $found = in_array("141", $this->arraay);
        if ($found) {


            if ($this->input->post()) {

                date_default_timezone_set("Asia/Kolkata");


                $tax_name = $this->input->post('tax_name');
                $tax_type = $this->input->post('tax_type');
                $tax_category = $this->input->post('tax_category');
                $tax_desc = $this->input->post('tax_desc');
                $dateTime = date('Y-m-d H:i:s');




                $add_tax_type = array(
                    'tax_name' => $tax_name,
                    'tax_category' => $tax_category,
                    'tax_type_tag_id' => $tax_type,
                    'tax_desc' => $tax_desc,
                    'htl_id' => $this->session->userdata('user_hotel'),
                    'htl_user_id' => $this->session->userdata('user_id'),
                    'htl_date_time' => $dateTime,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
                //print_r($add_tax_type);
                //die();
                $insert_id = $this->dashboard_model->tax_type_add($add_tax_type);

                if ($insert_id) {

                    $msg = "inserted";
                    $this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
                    //$this->tax_type_settings();
                    redirect(base_url() . 'dashboard/tax_type_settings');
                } else {

                    $msg = " not inserted";
                    $this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
                    redirect(base_url() . 'dashboard/tax_type_settings');
                }
            } else {

                $data['heading'] = 'Setting';
                $data['sub_heading'] = 'Tax Settings';
                $data['description'] = '';
                $data['active'] = 'tax_type_settings';
                $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
                //$data['vendor'] = $this->dashboard_model->all_vendor();
                //	$data['tax_category']=$this->dashboard_model->tax_category_list();
                $data['page'] = 'backend/dashboard/tax_type_add';

                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function tax_ajax() {
        $out_put = '';
        $t_cate = $this->input->post('t_cate');
        //$t_cate = 'Indirect';
        if ($t_cate) {

            $gettax1 = $this->dashboard_model->tax_type_list($t_cate);

            $out_put .= ' <select class="form-control"  id="tax_type" name="tax_type" required="required" >';
            foreach ($gettax1 as $gettax) {

                $out_put .= '<option value="' . $gettax->tax_type_id . '">' . $gettax->tax_name . '</option>';
            }

            $out_put .= '</select>';
            $out_put .= '<span>'.'</span>';
            echo $out_put;
        }
    }

    function tax_rule_ajax() {

        $t_cate = $this->input->post('category');
        $date = $this->input->post('start_date');
        $date_range[] = explode("-", $date);
        $r_start_date = $date_range[0];
        $start = date('Y-m-d', strtotime($r_start_date[0]));
        $end = date('Y-m-d', strtotime($r_start_date[1]));
        $start_price = $this->input->post('start_price');
        $end_price = $this->input->post('end_price');

        /* $sql ="SELECT * 
          FROM `hotel_tax_rule`
          WHERE `r_cat` = '$t_cate'
          ('$end_price' >= `r_start_price`) AND (`r_end_price` >= '$start_price') AND
          ('$end' >= `r_start_date`) AND (`r_end_date` >= '$start')";
          echo $sql;
          die();
         */
        $value = $this->dashboard_model->tax_rule_ajax_specific($t_cate, $start, $end, $start_price, $end_price);
        //print_r($value);
        //die();
        if ($value == 'YES') {

            echo "EXSISTS";
        } else {

            echo "NOT EXISTS";
        }
    }

    function tax_cat_ajax() {
        $out_put = '';
        $t_cate = $this->input->post('tax_cat_mode');
        //$t_cate = 'Indirect';
        if ($t_cate) {

            $gettax1 = $this->dashboard_model->cattype_namelist($t_cate);

            $out_put .= ' <select class="form-control"  id="tax_type" name="tax_type" required="required" >';
            foreach ($gettax1 as $gettax) {

                $out_put .= '<option value="' . $gettax->tax_type_id . '">' . $gettax->tax_name . '</option>';
            }

            $out_put .= '</select>';
            echo $out_put;
        }
    }

    function tax_value_ajax() {

        $out_put = '';
        $tax_mode = $this->input->post('tax_mode');
        if ($tax_mode == 'f') {

            $out_put .= '<td class="form-group"><input type="number" min="0" max="1000" name="tax_value_5" class="form-control input-sm" placeholder="Value" id="tax_value"/></td>';
        } else {

            $out_put .= '<td class="form-group"><input type="number" min="0" max="100" name="tax_value_5" class="form-control input-sm" placeholder="Value" id="tax_value"/></td>';
        }
        echo $out_put;
    }

    function edit_tax_plan($up = '') {

        $tax_name = $this->input->post('taxid');
        if ($tax_name) {
            $getrow = $this->dashboard_model->tax_type_setting_specific($tax_name);
            //print_r($getrow);
            $data = array(
                'tax_id' => $getrow->tax_id,
                'tax_name' => $getrow->tax_name,
                'tax_category' => $getrow->tax_category,
                'tax_type_tag_id' => $getrow->tax_type_tag_id,
                'tax_desc' => $getrow->tax_desc
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            echo $this->input->post('taxname');
        }
        //echo $getrow;
    }

// new method on 11-15-11-16..

    function edit_tax_category($up = '') {

        $tax_id = $this->input->post('taxid');
        if ($tax_id) {
            $getrow = $this->dashboard_model->tax_category_setting_specific($tax_id);
            //print_r($getrow);
            //die();
            $data = array(
                'tax_id' => $getrow->tc_id,
                'tax_category_name' => $getrow->tc_name,
                'tax_category_type' => $getrow->tc_type,
                'tax_desc' => $getrow->tc_desc,
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            echo $this->input->post('taxid');
        }
        //echo $getrow;
    }

    //---------end....



    function pdf_rev_room_report() {
        if ($this->input->post()) {
            $data['m'] = $this->input->post('month');
            $data['y'] = $this->input->post('year');
            $data['maxDays'] = cal_days_in_month(CAL_GREGORIAN, $data['m'], $data['y']);
        } else {
            $data['y'] = date("Y");
            $data['m'] = date("m");
            $data['maxDays'] = date('t');
        }
        $data['heading'] = 'Hotel Reports';
        $data['sub_heading'] = 'Financial Reports';
        $data['description'] = '';
        $data['active'] = 'revpar_room_report';
        //$all_payments=$this->dashboard_model->all_expense_report();
        // print_r($usertypes);
        // exit();
        //$data['allpayments']=$all_payments;
        $data['totalrooms'] = $this->dashboard_model->totalrooms();
        //
        $x = count($data['totalrooms']);
        //die;
        $data['totalrooms'] = $x;

        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $this->load->view("backend/dashboard/pdf_rev_room_report", $data);

        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html(html_entity_decode($html));
        $this->dompdf->set_paper('a4', 'landscape');
        $this->dompdf->render();
        $this->dompdf->stream("rev_room_report.pdf");
    }

    function all_profit_center() {

        $found = in_array("94", $this->arraay);
        if ($found) {
            $data['heading'] = 'Setting';
            $data['sub_heading'] = 'Metadata';
            $data['subs_heading'] = 'Profit Center';
            $data['description'] = 'Add Profit Center here';
            $data['active'] = 'all_profit_center';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['pc'] = $this->dashboard_model->all_pc_active();
            $data['stat'] = "List of All Active Profit Center";
            $data['page'] = 'backend/dashboard/all_profit_center';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function pdf_lodging_tax_report() {


        $data['bookings'] = $this->dashboard_model->all_bookings();

        $data['transactions'] = $this->dashboard_model->all_f_reports();
        $data['types'] = $this->dashboard_model->all_f_types();
        $data['entity'] = $this->dashboard_model->all_f_entity();
        $this->load->view("backend/dashboard/pdf_lodging_tax_report", $data);
        //  $html = $this->output->get_output();
        // $this->load->library('dompdf_gen');
        // $this->dompdf->load_html(html_entity_decode($html));
        //$this->dompdf->set_paper('a4','landscape');
        // $this->dompdf->render();
        //$this->dompdf->stream("lodging_tax_report.pdf");
    }

    function delete_pc() {
        $id = $_GET['pc_id'];

        $this->dashboard_model->delete_pc($id);
        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    //edit edit_profit_center
    function edit_profit_center() {

        $found = in_array("95", $this->arraay);
        if ($found) {
            $this->load->model('dashboard_model');
            $this->load->model('unit_class_model');
            $data['heading'] = 'Edit profit center';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['active'] = 'unit_t';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
            //$data['datas'] = $this->dashboard_model->edit_profit_center($id);
            //print_r($data['datas']);exit();
            //$data['unit_class']=$this->unit_class_model->all_unit_clsss();
            //print_r($data['data']);exit();

            if ($this->input->post()) {

                $name = $this->input->post('name1');
                $balance = $this->input->post('balance1');
                $des = $this->input->post('des1');
                $default = $this->input->post('default1');
                $id = $this->input->post('hid2');

                date_default_timezone_set("Asia/Kolkata");
                $cur_date = date('Y-m-d H:i:s');
                $data = array(
                    'id' => $id,
                    'profit_center_location' => $name,
                    'profit_center_balance' => $balance,
                    'profit_center_default' => $default,
                    'profit_center_des' => $des,
                );
                
                $query = $this->dashboard_model->update_profit_center($data);
                // echo $query;exit;
                $query1 = $this->dashboard_model->edit_update_default_pc($query);
                //exit();
                if ($query) {
                    $msg = "inserted";
                    $this->session->set_flashdata('succ_msg', 'Data updated successfully');
                } else {

                    $msg = " not inserted";
                    $this->session->set_flashdata('succ_msg', 'Data not updated successfully');
                }
                redirect(base_url() . 'dashboard/all_profit_center');
            }
            $data['page'] = 'backend/dashboard/edit_profit_center';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    //fetch dat from booking nature visit
    function check_profit_center($id) {

        $this->db1->select('*');
        $this->db1->from(TABLE_PRE . 'booking_source');
        $this->db1->where('id', $id);
        $query = $this->db1->get();
        if ($query) {

            return $query->result();
        } else {

            return false;
        }
    }

    //check profit center default
    function check_profit_center_default() {

        //echo "hello";
        $a = $this->input->post('a');

        $res = $this->dashboard_model->check_profit_center_default($a);
        if ($res) {
            echo 'Do you want to change default profit center';
        }
    }

    function extrachargeNB() {

        $source = $this->input->post('booking');
        $season = $this->input->post('myseason');
        $unittype = $this->input->post('unitType');
        $date = $this->input->post('newstartDate');
        $mealplan = $this->input->post('meal');
        $occu = $this->input->post('oid');

        $query = $this->dashboard_model->extrachargeNB($source, $season, $unittype, $date, $mealplan, $occu);
        if ($query) {

            $data = array('extra_room_charge' => $query->b_room_charge,
                'extra_room_tax' => $query->b_room_charge_tax,
                'meal_charge' => $query->b_meal_charge,
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {

            return false;
        }
    }

    function pos_grand_invoice_group() {
        $b_id = $_GET["b_id"];
        $hotel_id = $this->session->userdata('user_hotel');
        $data['pos'] = $this->dashboard_model->all_pos_booking_group($b_id);
        $sbID = $this->dashboard_model->get_sbID($b_id);
        //echo $sbID->booking_id;
        //exit;
        $data['bookings'] = $this->dashboard_model->get_group_booking_guest_details($sbID->booking_id);
        $data['hotel'] = $this->dashboard_model->get_hotel_details_row($hotel_id);
        $data['tax'] = $this->bookings_model->get_tax_details($hotel_id);
        $data['printer']  =$this->dashboard_model->get_printer($hotel_id);
        $this->load->view("backend/dashboard/invoice_pos_pdf_group", $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("invoice_pos.pdf");
    }

    function extra_charge_report_pdf() {
        $data['heading'] = 'Hotel Reports';
        $data['sub_heading'] = 'Tax Reports';
        $data['description'] = 'Extra Charge Report';
        $data['active'] = 'extra_tax_report';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        if (($this->input->get('t_dt_frm')) && ($this->input->get('t_dt_to'))) {
            $start_date = date("Y-m-d", strtotime($this->input->get('t_dt_frm')));
            $end_date = date("Y-m-d", strtotime($this->input->get('t_dt_to')));
            $data['start_date'] = $this->input->get('t_dt_frm');
            $data['end_date'] = $this->input->get('t_dt_to');
            $data['bookings'] = $this->dashboard_model->all_bookings_by_date($start_date, $end_date);
        } else {
            $data['bookings'] = $this->dashboard_model->all_bookings();
        }

        //$data['page'] = 'backend/dashboard/extra_charge_report_pdf';
        $this->load->view('backend/dashboard/extra_charge_report_pdf', $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("extra_charge_report.pdf");
    }

    function fetch_fuel() {
        //$id=$_POST['f_id'];
        $id = '1';
        $fuel = $this->dashboard_model->fetch_fuel($id);
        $data = array(
            'id' => $fuel->id,
            'fs_hotel_id' => $fuel->fs_hotel_id,
            'fuel_name' => $fuel->fuel_name,
            'cur_stock' => $fuel->cur_stock,
            'fuel_desc' => $fuel->fuel_desc,
            'unit' => $fuel->unit,
            'reorder_level' => $fuel->reorder_level,
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function update_fuel() {
        $id = $_POST['f_id'];
        $f_name = $_POST['f_name'];
        $f_des = $_POST['f_des'];
        $unit = $_POST['unit'];
        $record_lvl = $_POST['record_lvl'];
        $qty = $_POST['qty'];

        $data = array(
            'fuel_name' => $f_name,
            'cur_stock' => $qty,
            'fuel_desc' => $f_des,
            'unit' => $unit,
            'reorder_level' => $record_lvl
        );
        $query = $this->dashboard_model->update_fuel($id, $data);
        if ($query) {
            $data = array(
                'data' => '1',
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    function delete_fuel($nm) {

        $found = in_array("73", $this->arraay);
        if ($found) {
            $qry = $this->dashboard_model->delete_fuel($nm);
            if ($qry == true) {
                echo "Succfully Deleted";
            } else {
                echo "Eror Occoured";
            }
        } else {
            redirect('dashboard');
        }
    }

    //edit transactions
    function edit_vendor($id) {

        //	 $found = in_array("89",$this->arraay);
        $hotel_vendor_id = $id;
        //echo $hotel_vendor_id;
        //die();

        if ($hotel_vendor_id) {
            $this->load->model('dashboard_model');
            //$this->load->model('unit_class_model');
            $data['heading'] = 'Finance';
            $data['sub_heading'] = '';
            $data['description'] = 'Edit Vendor';
            $data['active'] = 'all_vendor';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            //$data['data'] = $this->unit_class_model->data_warehouse($id);
            $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
            $data['data'] = $this->dashboard_model->data_vendor($hotel_vendor_id);
            $data['parent'] = $this->dashboard_model->get_parent_vendor();
            //print_r($data['data']);die();
            $data['page'] = 'backend/dashboard/edit_vendor';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function update_vendor() {

        if ($this->input->post()) {
            //echo "<pre>";
            //print_r($this->input->post());
            //print_r($_FILES['vendor_image']);
            //print_r($_FILES['pdf']);
            //exit();
            //
				
			 $this->upload->initialize($this->set_upload_options());
            if ($this->upload->do_upload('vendor_image')) {

                $asset_image = $this->upload->data('file_name');
                $filename = $asset_image;
                $source_path = './upload/' . $filename;
                $target_path = './upload/vendor_detail/';
                $config_manip = array(
                    'image_library' => 'gd2',
                    'source_image' => $source_path,
                    'new_image' => $target_path,
                    'maintain_ratio' => TRUE,
                    'create_thumb' => TRUE,
                    'thumb_marker' => '_thumb',
                    'width' => 250,
                    'height' => 250
                );
                $this->load->library('image_lib', $config_manip);

                if (!$this->image_lib->resize()) {
                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                    redirect('dashboard/all_vendor');
                }
                $thumb_name = explode(".", $filename);
                $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                $this->image_lib->clear();
            } else {
                $source_path = './upload/' . $filename;
                $target_path = './upload/vendor_detail/';
                $asset_thumb = 'no_images.png';
            }





            $config = array('upload_path' => './upload/vendor_detail/',
                'allowed_types' => "pdf",
                'overwrite' => TRUE,);
            $this->upload->initialize($config);
            $this->upload->do_upload('pdf');
            $up_file = $this->upload->data();
            $up_array = array(
                'hotel_vendor_id' => $this->input->post('vendor_id'),
                'hotel_vendor_name' => $this->input->post('vendor_name'),
                'hotel_vendor_industry' => $this->input->post('industry_type'),
                'hotel_vendor_heirc' => $this->input->post('child_supp'),
                'hotel_vendor_contact_person' => $this->input->post('contact_person'),
                'hotel_vendor_contact_no' => $this->input->post('contact_no'),
                'hotel_vendor_is_contracted' => $this->input->post('is_contract'),
                'hotel_vendor_address' => $this->input->post('address'),
                'hotel_vendor_regtype' => $this->input->post('reg_type'),
                'hotel_vendor_rating' => $this->input->post('rating'),
                'hotel_vendor_field' => $this->input->post('field'),
                'hotel_vendor_image' => $asset_thumb,
                'hotel_parent_vendor_id' => $this->input->post('parent_supp'),
                'hotel_vendor_profit_center' => $this->input->post('profit_center'),
                'hotel_vendor_reg_form' => $up_file['file_name'],
                'hotel_vendor_purchase_count' => 0,
                'hotel_vendor_total_amt' => 0,
                'hotel_id' => $this->session->userdata('user_hotel'),
                'user_id' => $this->session->userdata('user_id'),
                'hotel_vendor_pin' => $this->input->post('pin_code'),
                'hotel_vendor_city' => $this->input->post('city'),
                'hotel_vendor_state' => $this->input->post('state'),
                'hotel_vendor_email' => $this->input->post('contact_email'),
                'hotel_vendor_country' => $this->input->post('country'),
                'hotel_vendor_owner_name' => $this->input->post('director_name'),
                'hotel_vendor_owner_no' => $this->input->post('manager_no'),
                'hotel_vendor_owner_email' => $this->input->post('corp_manager_email'),
                'hotel_vendor_bank' => $this->input->post('bank_name'),
                'hotel_vendor_account' => $this->input->post('bank_account'),
                'hotel_vendor_ifsc' => $this->input->post('bank_ifsc'),
                'hotel_vendor_taxplan' => $this->input->post('tax_plan')
            );
            /* echo "<pre>";
              print_r($up_array);
              exit(); */
            $query = $this->dashboard_model->update_vendor($up_array);
            if ($query) {
                $this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
                redirect('dashboard/all_vendor');
            }
        }
        $data['page'] = 'backend/dashboard/edit_vendor';
        $this->load->view('backend/common/index', $data);
    }

    function pdf_group_booking_report() {

        // $data['bookings'] = $this->dashboard_model->all_bookings();
        // $data['transactions'] = $this->dashboard_model->all_transactions();
        //$data['bookings'] = $this->dashboard_model->all_bookings();
        //print_r($data);
        //exit();
        $data['bookings'] = $this->dashboard_model->all_bookings();
        $data['transactions'] = $this->dashboard_model->all_transactions();
        $data['groups'] = $this->dashboard_model->all_group();
        //$data['reports'] = $this->dashboard_model->all_bookings();
        $this->load->view("backend/dashboard/all_group_booking_pdf", $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->load_html(html_entity_decode($html));
        $this->dompdf->set_paper('a4', 'landscape');
        $this->dompdf->render();
        $this->dompdf->stream("all_group_booking_pdf.pdf");
    }

    function checkout_submission_on_credit() {
        $booking_id = $this->input->post('booking_id');
        $hotel_id = $this->session->userdata('user_hotel');
        $date_checkout = $this->input->post('date_checkout');
        $dateCheckout = $this->input->post('dateCheckout');
        $due_date = $this->input->post('due_date');
        $credit_card_no = $this->input->post('card_number');
        $cvv = $this->input->post('cvv');
        $exp_date = $this->input->post('exp_date');
        $bank_ac_no = $this->input->post('ac_no');
        $ifsc = $this->input->post('ifsc');
        $company_name = $this->input->post('c_name');
        $company_phn = $this->input->post('c_number');
        $b_type = $this->input->post('b_type');
        $total_amt = $this->input->post('total');
        //echo $booking_id."_".$hotel_id."_".$date_checkout."_".$dateCheckout."_".$due_date."_".$credit_card_no."_".$cvv."_".$exp_date."_".$bank_ac_no."_".$ifsc."_".$company_name."_".$company_phn;
        //exit;
        $data_array = array
            (
            'booking_id' => $booking_id,
            'hotel_id' => $hotel_id,
            'due_date' => $due_date,
            'credit_card_no' => $credit_card_no,
            'cvv' => $cvv,
            'exp_date' => $exp_date,
            'bank_ac_no' => $bank_ac_no,
            'ifsc' => $ifsc,
            'company_name' => $company_name,
            'company_phn' => $company_phn,
            'booking_type' => $b_type,
            'total_amt' => $total_amt,
        );
        $query = $this->dashboard_model->checkout_submission_on_credit($data_array);
		$this->dashboard_model->update_checkout_status_stay_guest($booking_id,'checkout');
        if ($query) {
            echo $query;
        } else {
            return 0;
        }
    }

    //edit shift

    function edit_shift($id) {

        $this->load->model('dashboard_model');
        $this->load->model('unit_class_model');
        $data['heading'] = 'Edit Shift';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['datas'] = $this->dashboard_model->edit_shift($id);
        

        if ($this->input->post()) {
			
            $shift_id = $this->input->post('hid');
            $shift_name = $this->input->post('shift_name');
            $shift_opening_time = $this->input->post('shift_opening_time');
            $shift_closing_time = $this->input->post('shift_closing_time');
            
            date_default_timezone_set("Asia/Kolkata");
            $cur_date = date('Y-m-d H:i:s');
            $data = array(
                'shift_id' => $shift_id,
                'hotel_id' => $this->session->userdata('user_hotel'),
                'shift_name' => $shift_name,
                'shift_opening_time' => $shift_opening_time,
                'shift_closing_time' => $shift_closing_time.':59',
            );
			
			//echo "<pre>";
            //print_r($data);exit;
			
            $query = $this->dashboard_model->update_shift($data);
            //exit();
            if ($query) {
                $msg = "inserted";
                $this->session->set_flashdata('succ_msg', 'Data updated successfully');
            } else {

                $msg = " not inserted";
                $this->session->set_flashdata('succ_msg', 'Data not updated successfully');
            }
            redirect(base_url() . 'dashboard/all_shifts');
        }
        $data['page'] = 'backend/dashboard/edit_shift';
        $this->load->view('backend/common/index', $data);
    }

    //end shift

    function delete_shift() {
        $id = $_GET['id'];
        //$id=$this->input->post('id');

        $this->dashboard_model->delete_shift($id);
        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function ceck_shift_time() {
        $start = $this->input->post('start');
        $end = $this->input->post('end');

        $a = $this->dashboard_model->ceck_shift_time($start, $end);
        //echo $a;exit();
        $data = array(
            'data' => "sucess",
                //alert('done'),
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function pending_payments() {

        $found = in_array("26", $this->arraay);
        if ($found) {
            $data['heading'] = 'Booking';
            $data['sub_heading'] = 'Pending Payments';
            $data['description'] = 'Bookings With Pending Payment';
            $data['active'] = 'pending_payments';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            //$data['bookings'] = $this->dashboard_model->all_bookings($config['per_page'], $segment);
            $data['bookings'] = $this->dashboard_model->all_bookings();
            $data['transactions'] = $this->dashboard_model->all_transactions();
            //$data['pagination'] = $this->pagination->create_links();

            $data['page'] = 'backend/dashboard/pending_payments';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function get_checkout_on_credit() {

        $booking_id = $this->input->post('id');
        $booking_type = $this->input->post('type');
        $query = $this->dashboard_model->get_checkout_on_credit($booking_id, $booking_type);
        if ($query) {
            $res = "";
            foreach ($query as $q) {
                $res = $q->total_amt . "_" . $q->due_date . "_" . $q->credit_card_no . "_" . $q->cvv . "_" . $q->exp_date . "_" . $q->bank_ac_no . "_" . $q->ifsc . "_" . $q->company_name . "_" . $q->company_phn;
            }
            if ($res != "")
                echo $res;
            else
                echo "Error";
        }
    }

    /* function invoice_settings(){
      $data['heading'] = 'Settings';
      $data['sub_heading'] = 'Settings';
      $data['description'] = 'Invoice Settings';
      $data['active'] = 'invoice_settings';
      $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

      $data['invoice_settings'] = $this->dashboard_model->invoice_sttings();

      $data['page'] = 'backend/dashboard/invoice_settings';
      $this->load->view('backend/common/index', $data);

      } */

    function invoice_settings() {


        $data['heading'] = 'Setting';
        $data['sub_heading'] = '';
        $data['description'] = 'Invoice Settings';
        $data['active'] = 'invoice_settings';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        $data['invoice_settings'] = $this->dashboard_model->get_invoice_settings($this->session->userdata('user_hotel'));

        if ($this->input->post()) {
            $config = array('upload_path' => './upload/vendor_detail/',
                'allowed_types' => "pdf",
                'overwrite' => TRUE,);
            $this->upload->initialize($config);
            $this->upload->do_upload('pdf');
            $up_file = $this->upload->data();
            $data = array(
                'gb_prefix' => $this->input->post('gb_prefix'),
                'sb_prefix' => $this->input->post('sb_prefix'),
                'hotel_id' => $this->session->userdata('user_hotel'),
                'booking_source_inv_applicable' => $this->input->post('booking_source'),
                'nature_visit_inv_applicable' => $this->input->post('nature_visit'),
                'booking_note_inv_applicable' => $this->input->post('booking_note'),
                'company_details_inv_applicable' => $this->input->post('company_details'),
                'service_tax_applicable' => $this->input->post('service_tax'),
                'service_charg_applicable' => $this->input->post('service_charg'),
                'luxury_tax_applicable' => $this->input->post('luxury_tax'),
                'authorized_signatory_applicable' => $this->input->post('authorized_signatory'),
                'invoice_pref' => $this->input->post('invoice_pref'),
                'invoice_suf' => $this->input->post('invoice_suf'),
                'unit_no' => $this->input->post('unit_no'),
                'unit_cat' => $this->input->post('unit_cat'),
                'invoice_setting_food_paln' => $this->input->post('fd_plan'),
                'invoice_setting_service' => $this->input->post('service'),
                'invoice_setting_extra_charge' => $this->input->post('extra_charge'),
                'invoice_setting_laundry' => $this->input->post('laundry'),
                'ft_text' =>  addslashes($this->input->post('ft_text')),
                'paid_pos_item_show' => $this->input->post('paid_pos_item_show'),
                'GBft_text' => $this->input->post('GBft_text'),
        		 'printer_settings' => $this->input->post('printer'),
        		 'pos_app' => $this->input->post('pos_app'),
        		 'adjust_app' => $this->input->post('adjust_app'),
        		 'disc_app' => $this->input->post('disc_app'),
        		 'pos_seg' => $this->input->post('pos_seg'),
        		 'fd_app' => $this->input->post('fd_app'),
        		 'hotel_color' => $this->input->post('hotel_color'),
        		 'font_size' => $this->input->post('font_size'),
        		 'declaration' => $this->input->post('declaration'),
        		  'invoice_no' => $this->input->post('invoice_no'),
        		   'hotel_sub_text' => $this->input->post('hotel_sub_text'),
				   'font_size_gp' => $this->input->post('font_size_gp'),
            );
			
            $query = $this->dashboard_model->update_invoice_settings($data);
            //exit();
            if ($query) {
                $msg = "inserted";
                $this->session->set_flashdata('succ_msg', 'Settings updated Succefully');
                redirect('dashboard/invoice_settings');
            } else {

                $msg = " not inserted";
                $this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
            }
        }

        $data['page'] = 'backend/dashboard/edit_invoice_settings';
        $this->load->view('backend/common/index', $data);
    }

    function update_security_details() {
        $data['heading'] = 'Settings';
        $data['sub_heading'] = 'Settings';
        $data['description'] = 'Invoice Settings';
        $data['active'] = 'invoice_settings';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        //$data['invoice_settings'] = $this->dashboard_model->invoice_sttings();

        $data['page'] = 'backend/dashboard/edit_invoice_settings';
        $this->load->view('backend/common/index', $data);
    }

    //function update
    function profit_center_status() {
        $this->load->model('dashboard_model');





        $id = $this->input->post('id');
        $data = array(
            'id' => $id,
        );
        $query = $this->unit_class_model->profit_center_status($data);
    }

    function update_credit_security_details() {

        $booking_id = $this->input->post('booking_id');
        $booking_type = $this->input->post('booking_type');
        $due_date = $this->input->post('due_date');
        $card_number = $this->input->post('card_number');
        $cvv = $this->input->post('cvv');
        $exp_date = $this->input->post('exp_date');
        $ac_no = $this->input->post('ac_no');
        $ifsc = $this->input->post('ifsc');
        $c_name = $this->input->post('c_name');
        $c_number = $this->input->post('c_number');
        //print_r($_get);

        $data = array(
            'due_date' => $due_date,
            'credit_card_no' => $card_number,
            'cvv' => $cvv,
            'exp_date' => $exp_date,
            'bank_ac_no' => $ac_no,
            'ifsc' => $ifsc,
            'company_name' => $c_name,
            'company_phn' => $c_number,
        );
        //print_r($data); exit;
        $query = $this->dashboard_model->update_credit_security_details($booking_id, $booking_type, $data);
        //echo $booking_id." ".$booking_type;
        //exit();
        if ($query) {
            echo "Success";
        }
    }

    function profit_center_active() {
        $data['heading'] = 'Setting';
        $data['sub_heading'] = 'Metadata';
        $data['subs_heading'] = 'Profit Center';
        $data['description'] = 'Add Profit Center here';
        $data['active'] = 'all_profit_center';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['pc'] = $this->dashboard_model->all_pc_active();
        $data['page'] = 'backend/dashboard/all_profit_center';
        $data['stat'] = 'List of All Active Profit Center';
        $this->load->view('backend/common/index', $data);
    }

    function profit_center_inactive() {
        $data['heading'] = 'Setting';
        $data['sub_heading'] = 'Metadata';
        $data['subs_heading'] = 'Profit Center';
        $data['description'] = 'Add Profit Center here';
        $data['active'] = 'all_profit_center';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['pc'] = $this->dashboard_model->all_pc_inactive();
        $data['page'] = 'backend/dashboard/all_profit_center';
        $data['stat'] = 'List of All Inactive Profit Center';
        $this->load->view('backend/common/index', $data);
    }

    function update_bookings_guest_id() {
        $id = $this->input->post('guest_id');
        $booikng_id = $this->input->post('booking_id');

        $query = $this->dashboard_model->update_bookings_guest_id($id, $booikng_id);
        if ($query) {
            echo "Updated";
        }
    }

    //FUNCTION IS NOT AVAILABLE IN DASHBOARD MODEL
    function add_extra_guest() {
        $g_id = $this->input->post('g_id');
        $booikng_id = $this->input->post('booking_id');
        $g_name = $this->input->post('g_name');
        $g_gender = $this->input->post('sex');
        $check_in = $this->input->post('check_in');
        $check_out = $this->input->post('check_out');
        $pin = $this->input->post('pin');
        $contact = $this->input->post('contact_no');
        $charge = $this->input->post('charge');
        $data = array(
            'g_name' => $g_name,
            'g_gender' => $g_gender,
            'g_pincode' => $pin,
            'g_contact_no' => $contact,
        );
        $query = $this->dashboard_model->add_extra_guest($id, $booikng_id);
    }

    function pending_checkin_checkout() {

        $found = in_array("26", $this->arraay);
        if ($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['bookings'] = $this->dashboard_model->all_bookings_pending();
            // $data['transactions'] = $this->dashboard_model->all_transactions();
            //	print_r($data['bookings']);exit;
            $data['page'] = 'backend/dashboard/pending_checkin_checkout';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    function get_pending_booking_report_by_date() {
        $start_date = date("Y-m-d");
        $end_date = date("Y-m-d");
        $data['heading'] = 'Booking';
        $data['sub_heading'] = 'All Bookings';
        $data['description'] = 'Bookings List';
        $data['active'] = 'all_bookings';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        if ($this->input->post()) {
            $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
            $data['start_date'] = $this->input->post('t_dt_frm');
            $data['end_date'] = $this->input->post('t_dt_to');
        }

        $data['bookings'] = $this->dashboard_model->all_bookings_by_date($start_date, $end_date);
        $data['transactions'] = $this->dashboard_model->all_transactions();
        $data['page'] = 'backend/dashboard/pending_checkin_checkout';
        $this->load->view('backend/common/index', $data);
    }

    function get_pdf() {

        $data['bookings'] = $this->dashboard_model->all_bookings();
        $data['transactions'] = $this->dashboard_model->all_transactions();
        $this->load->view("backend/dashboard/all_reports_pdf", $data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("all_report.pdf");
    }

    function search_item() {
        $keyword = $this->input->post("keyword");
        //$keyword=$_GET["key"];
        $result = $this->dashboard_model->search_item($keyword);

        $st = "";
        if ($result != false) {
            foreach ($result as $key) {
                $st .= "<li class='list-group-item' tabindex='2' onClick=\"selectitem(0,'" . $key->a_name . "','" . $key->u_price . "','" . $key->hotel_stock_inventory_id . "');\"   onkeyup=\"selectitem(event,'" . $key->a_name . "','" . $key->u_price . "','" . $key->hotel_stock_inventory_id . "');\">" . $key->a_name . "</li>";
            }
            if ($st != "") {
                echo "<ul class='list-group' style='font-size:20px;'>" . $st . "</ul>";
            } else {
                echo $st;
            }
        } else {
            echo $st;
        }
    }

    function kit_card() {
        //if($this->session->userdata('user_type_slug') !='G')
        {
            $data['heading'] = 'Room';
            $data['sub_heading'] = '';
            $data['description'] = 'Welcome Kit Details';
            $data['active'] = 'kit_card';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['kits'] = $this->dashboard_model->get_kit_details();
            $data['page'] = 'backend/dashboard/kit_card';
            $this->load->view('backend/common/index', $data);
        }
    }

    function delete_kit() {
        $id = $this->input->post('id');
        $res = $this->dashboard_model->delete_kit_items($id);
        $res1 = $this->dashboard_model->delete_kit($id);

        if (( isset($res) && $res != false) && ( isset($res1) && $res1 != false)) {
            echo true;
        } else {
            echo false;
        }
    }

    function get_kit_total_detail() {
        $id = $this->input->post('id');

        $kit_detail = $this->dashboard_model->get_kit_details_by_id($id);
        $kit_item_detail = $this->dashboard_model->get_kit_mapping_items($id);


        $st1 = $kit_detail->hotel_define_kit_id . "*" . $kit_detail->kit_name . "*" . $kit_detail->kit_des . "*" . $kit_detail->kit_type . "*" . $kit_detail->kit_extra_cost . "*" . $kit_detail->is_sellable . "*" . $kit_detail->kit_sel_price . "*" . $kit_detail->image_name;
        //echo "<br>";
        $st = "";
        $ids = "";
        if ($kit_item_detail != false) {
            foreach ($kit_item_detail as $kt) {
                $item_real_name = $this->dashboard_model->get_stock_item($kt->item_name);
                if (isset($item_real_name) && $item_real_name) {
                    $item_name = $item_real_name->a_name;
                }
                $ids .= "," . $kt->item_name;
                $st .= '<tr id="row_' . $kt->item_name . '"><td><label>' .
                        '<input   type="text" value="' . $item_name . '" class="form-control input-sm" readonly></label><label><input  id="i_name5" type="hidden" value="' . $kt->item_name . '" class="form-control input-sm" readonly></label></td><td><label><input  id="c_name1" type="text" value="' . $kt->qty . '" class="form-control input-sm" readonly></label></td><td class="hidden-480"><label><input  id="c_name4" type="text" value="' . $kt->unit_price . '" class="form-control input-sm" readonly></label></td><td class="hidden-480"><label><input  id="c_name4" type="text" value="' . $kt->qty_for_apply . '" class="form-control input-sm" readonly></label><td class="hidden-480"><label><input  id="c_name4" type="text" value="' . $kt->qty * $kt->unit_price . '" class="form-control input-sm" readonly></label></td></td><td><a href="javascript:void(0);" class="btn red btn-xs" onclick="removeRow(' . $kt->item_name . ',' . $kit_detail->hotel_define_kit_id . ')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>';
            }
        }
        $st . "";


        echo $st1 . "**" . $st . "**" . $ids;
    }

    function delete_individual_mapping_item() {
        $id = $this->input->post('id');
        $kit = $this->input->post('kit');

        $res = $this->dashboard_model->delete_individual_mapping_item($id, $kit);
        if ($res && ($res != 0)) {
            echo 1;
        } else {
            echo 0;
        }
    }

    function edit_kit() {
        $data['heading'] = 'Kit View';
        $data['sub_heading'] = 'Kit View';
        $data['description'] = 'Kit View ';
        $data['active'] = 'kit_card';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        if ($this->input->post()) {
            //print_r($this->input->post());
            //exit();
            $kit_id = $this->input->post('kit_id');
            $asset_thumb = $this->input->post('img');

            $this->upload->initialize($this->set_upload_options());
            if ($this->upload->do_upload('image')) {

                $asset_image = $this->upload->data('file_name');
                $filename = $asset_image;
                $source_path = './upload/' . $filename;
                $target_path = './upload/asset/';
                $config_manip = array(
                    'image_library' => 'gd2',
                    'source_image' => $source_path,
                    'new_image' => $target_path,
                    'maintain_ratio' => TRUE,
                    'create_thumb' => TRUE,
                    'thumb_marker' => '_thumb',
                    'width' => 250,
                    'height' => 250
                );
                $this->load->library('image_lib', $config_manip);

                if (!$this->image_lib->resize()) {
                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                    redirect('dashboard/add_kit');
                }
                $thumb_name = explode(".", $filename);
                $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                $this->image_lib->clear();
            }


            $kit_defination_array = array(
                'kit_name' => $this->input->post('kit_name'),
                'kit_des' => $this->input->post('kit_des'),
                'kit_type' => $this->input->post('kit_type'),
                'kit_extra_cost' => $this->input->post('kit_extra_cost'),
                'is_sellable' => $this->input->post('is_sellable'),
                'kit_sel_price' => $this->input->post('kit_sel_price'),
                'image_name' => $asset_thumb,
            );
            $last_kit_id = $this->dashboard_model->edit_kit($kit_defination_array, $kit_id);


            if (isset($kit_id)) {

                for ($i = 0; $i < $this->input->post('count'); $i++) {
                    if (($this->input->post('i_name_' . $i) != "" ) && ($this->input->post('qty_' . $i) != "" ) && ($this->input->post('unit_cost_' . $i) != "" ) && ($this->input->post('qty_for_' . $i) != "" )) {
                        $kit_maping_array = array(
                            'item_name' => $this->input->post('i_name_' . $i),
                            'qty' => $this->input->post('qty_' . $i),
                            'unit_price' => $this->input->post('unit_cost_' . $i),
                            'qty_for_apply' => $this->input->post('qty_for_' . $i),
                            'kit_id' => $kit_id,
                        );
                        $last_kit_mapping_id = $this->dashboard_model->add_kit_mapping($kit_maping_array);
                    }
                }

                $this->session->set_flashdata('succ_msg', "Kit Defined Succeffully. ");
                redirect('dashboard/kit_card');
            } else {
                $this->session->set_flashdata('err_msg', "Error in Uploading");
                redirect('dashboard/kit_card');
            }
        } else {
            $this->session->set_flashdata('err_msg', $this->upload->display_errors());
            redirect('dashboard/kit_card');
        }
    }

    function allot_kit($booking_id) {

        //exit();
        $booking_details = $this->dashboard_model->get_booking_details($booking_id);
        if ($booking_details != false) {
            $child = $booking_details->no_of_child;
            $adult = $booking_details->no_of_adult;

            $room_id = $booking_details->room_id;

            $room_data = $this->dashboard_model->get_room_number($room_id);
            foreach ($room_data as $rd) {
                $unit = $rd->unit_id;
            }


            $kit_id_data = $this->dashboard_model->get_unit_kit_mapping($unit);
            if ($kit_id_data != false) {
                $kit_id = $kit_id_data->primary_kit_id;

                $kit_details = $this->dashboard_model->get_kit_details_by_id($kit_id);
                $kit_item_details = $this->dashboard_model->get_kit_mapping_items($kit_id);



                $flag = 0;
                $items = "";

                $warehouse_data = $this->dashboard_model->get_distribution_warehouse();
                if ($warehouse_data != false) {
                    $warehouse = $warehouse_data->id;

                    $out = "<table border=1>";
                    foreach ($kit_item_details as $kid) {
                        if ($kid->qty_for_apply != 'Fixed') {
                            $tot = $kid->qty * ($child + $adult);
                        } else {
                            $tot = $kid->qty;
                        }

                        $item_details = $this->dashboard_model->get_kit_items_details($kid->item_name);


                        $stock_qty = $this->dashboard_model->get_qty($warehouse, $kid->item_name);
                        if ($stock_qty) {
                            $current_stock = $stock_qty->qty;
                        } else {
                            $current_stock = 0;
                        }

                        if ($current_stock <= $tot) {
                            $flag = $flag + 1;
                            $os_item_name = $this->dashboard_model->get_kit_items_details($kid->item_name);
                            $items .= "," . $os_item_name->a_name;
                        }

                        // $item_name = $this->dashboard_model->get_kit_items_details($kid->item_name);
                        //$out .="<tr><td>".$kid->item_name."</td><td>".$item_name->a_name."</td><td>".$kid->qty."</td><td>".$kid->qty_for_apply."</td><td>".$tot."</td><td>".$current_stock."</td></tr>";
                    }
                    //echo $out .="</table>";

                    if ($flag > 0) {

                        echo "Kit cannot be distributed " . $items . " Are out of stock";
                    } else {
                        date_default_timezone_set("Asia/Kolkata");

                        $qry = $this->dashboard_model->check_kit_distribution_status($booking_id);
                        if ($qry == false) {
                            $item_allot = array(
                                'allot_booking_id' => $booking_id,
                                'allot_kit_id' => $kit_id,
                                'allot_kit_status' => '1',
                                'allot_kit_date' => date('d-m-y h:i:sa'),
                            );
                            $kit_allot_id = $this->dashboard_model->kit_allotment($item_allot);
                            if (isset($kit_allot_id)) {

                                foreach ($kit_item_details as $kid) {
                                    $stock_qty = $this->dashboard_model->get_qty($warehouse, $kid->item_name);
                                    $current_stock = $stock_qty->qty;
                                    $item_name = $this->dashboard_model->get_kit_items_details($kid->item_name);

                                    if ($kid->qty_for_apply != 'Fixed') {
                                        $tot = $kid->qty * ($child + $adult);
                                    } else {
                                        $tot = $kid->qty;
                                    }

                                    $item = array(
                                        'date' => date('m-d-Y H:i:sa'),
                                        'hotel_stock_inventory_id' => $kid->item_name,
                                        'a_type' => $item_name->item_type,
                                        'a_category' => $item_name->a_category,
                                        'item' => $kid->item_name,
                                        'warehouse' => $warehouse_data->id,
                                        'qty' => $tot,
                                        'opening_qty' => $current_stock,
                                        'closing_qty' => $current_stock - $tot,
                                        'operation' => "cons",
                                        'note' => "Quantity dispatched as kit item to booking-id: " . $booking_id,
                                    );

                                    $stock_qty = array(
                                        'hotel_id' => $this->session->userdata('user_hotel'),
                                        'warehouse_id' => $warehouse_data->id,
                                        'qty' => $tot,
                                        'item_id' => $kid->item_name,
                                        'note' => "Quantity dispatched as kit item to booking-id: " . $booking_id,
                                    );
                                    $query = $this->dashboard_model->update_stock($item, $stock_qty);
                                }

                                echo "Kit distributed Successfully. kit No: " . $kit_allot_id;
                            } else {
                                echo "Database Eroor";
                            }
                        } else {
                            echo "Kit Already Distributed. Kit ID: " . $qry->allot_id;
                        }
                    }
                } else {
                    echo "Please select operational warehouse";
                }
            } else {
                echo "No kit defined for this room type";
            }
        } else {
            echo "no such booking exists";
        }
    }

    function display_user_permission() {


        print_r($this->arraay);
    }

    function add_tax_rule() {
        $found = in_array("141", $this->arraay);
        if ($found) {
            //if($this->session->userdata('user_type_slug') !='G')
            //{
            $data['heading'] = 'Setting';
            $data['sub_heading'] = 'Tax Settings';
            $data['description'] = 'Define individual tax rules';
            $data['active'] = 'all_tax_rule';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));


            if ($this->input->post()) {
                $date = $this->input->post('r_start_dates');
                //echo $date;
                $date_range[] = explode("-", $date);
                $r_start_date = $date_range[0];

                $start = date('Y-m-d', strtotime($r_start_date[0]));

                $end = date('Y-m-d', strtotime($r_start_date[1]));
                $r_cat = $this->input->post('tax_cat');
                $r_name = $this->input->post('r_name');
                $r_desc = $this->input->post('r_desc');
                $r_start_price = $this->input->post('r_start_price');
                $r_end_price = $this->input->post('r_end_price');
                $gst = $this->input->post('gst');

                $tax_data = array(
                    'r_cat' => $r_cat,
                    'r_name' => $r_name,
                    'r_desc' => $r_desc,
                    'r_start_price' => $r_start_price,
                    'r_end_price' => $r_end_price,
                    'r_start_date' => $start,
                    'r_end_date' => $end,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                    'same_state' => $gst
                );
				
				
				
                $tax_rule_id = $this->dashboard_model->add_tax_rule($tax_data);
                if (isset($tax_rule_id)) {
                    foreach ($this->input->post('tax_type') as $key => $value) {
                        $component_array = array(
                            'p_r_id' => $tax_rule_id,
                            'tax_type' => $this->input->post('tax_type')[$key],
                            'tax_mode' => $this->input->post('tax_mode')[$key],
                            'tax_value' => $this->input->post('tax_value')[$key],
                            'calculated_on' => $this->input->post('calculated_on')[$key],
                            'applied_on' => $this->input->post('applied_on')[$key],
                            'hotel_id' => $this->session->userdata('user_hotel'),
                        );
                        $component = $this->dashboard_model->add_tax_rule_component($component_array);
                    }
					
				
                    $this->session->set_flashdata('succ_msg', 'Rule Added Succfully');
                    redirect(base_url() . 'dashboard/all_tax_rule');
                } else {
                    $this->session->set_flashdata('err_msg', 'Error Occoured');
                    redirect(base_url() . 'dashboard/all_tax_rule');
                }
            }
            $data['page'] = 'backend/dashboard/add_tax_rule';
            $this->load->view('backend/common/index', $data);
			
        } else {
            redirect('dashboard');
        }
    }

    function add_tax_rule_when_update() {

        //	tax_main_id:taxid,tax_type:tax_type,tax_mode:tax_mode,tax_val:tax_value,tax_calculated:calculated_on,tax_applied:applied_on},
        $tax_main_id = $_POST['tax_main_id'];
        $tax_type = $_POST['tax_type'];
        $tax_mode = $_POST['tax_mode'];
        $tax_val = $_POST['tax_val'];
        $tax_calculated = $_POST['tax_calculated'];
        $tax_applied = $_POST['tax_applied'];
        $component_array = array(
            'p_r_id' => $tax_main_id,
            'tax_type' => $tax_type,
            'tax_mode' => $tax_mode,
            'tax_value' => $tax_val,
            'calculated_on' => $tax_calculated,
            'applied_on' => $tax_applied,
        );



        $component = $this->dashboard_model->add_tax_rule_component($component_array);
        echo $component;
    }

    function all_tax_rule($conditions = '') {

        if ($conditions == '') {
            $data['heading'] = 'Setting';
            $data['sub_heading'] = 'Tax Settings';
            $data['description'] = 'Define individual tax rules';
            $data['active'] = 'all_tax_rule';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['tax_rule'] = $this->dashboard_model->get_all_tax_rule();
            $data['page'] = 'backend/dashboard/all_tax_rules';
            $this->load->view('backend/common/index', $data);
        } else {

            $tc_name = $this->uri->segment(3);
            if ($tc_name) {

                //echo $tc_name."<br>";

                $data['heading'] = 'Settings';
                $data['sub_heading'] = 'Tax rule defination';
                $data['description'] = 'Define individual tax rules';
                $data['active'] = 'all_tax_rule';
                $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
                $data['tax_rule'] = $this->dashboard_model->get_specific_tax_rule($tc_name);
                //print_r($data);
                //die();
                $data['page'] = 'backend/dashboard/all_tax_rules';
                $this->load->view('backend/common/index', $data);
            } else {

                echo $tc_name;
            }
        }
    }

    // new method on 15-11-16....


    function all_tax_category() {
        $found = in_array("144", $this->arraay);
        if ($found) {
            $data['heading'] = 'Setting';
            $data['sub_heading'] = 'Tax Settings';
            $data['description'] = 'Define tax category and sub category';
            $data['active'] = 'all_tax_category';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['tax_category'] = $this->dashboard_model->get_all_tax_category();
            $data['page'] = 'backend/dashboard/all_tax_category';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    // end..
    function edit_tax_rule($id) {

        $data['heading'] = 'Settings';
        $data['sub_heading'] = 'Edit Tax rule defination';
        $data['description'] = 'Edit individual tax rules';
        $data['active'] = 'all_tax_rule';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['tax_rule'] = $this->dashboard_model->get_tax_rule($id);
        $data['page'] = 'backend/dashboard/edit_tax_rule';
        $this->load->view('backend/common/index', $data);
    }

    function update_tax_rule() {
        //print_r($this->input->post()); exit;
        $data['heading'] = 'Settings';
        $data['sub_heading'] = 'Tax rule defination';
        $data['description'] = 'Define individual tax rules';
        $data['active'] = 'add_tax_rule';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));


        if ($this->input->post()) {
            //print_r($this->input->post()); exit;

            $date = $this->input->post('r_start_dates');
            //echo $date."<br>"."<br>";
            //echo strlen($date)."<br>";
            //echo substr($date,11,1)."<br>"."<br>";
            //die();
            $dates = str_replace(substr($date, 11, 1), "~", $date);
            //echo $dates;
            //exit();
            $date_range[] = explode("~", $dates);
            $r_start_date = $date_range[0];
            $start = date('Y-m-d', strtotime($r_start_date[0]));

            $end = date('Y-m-d', strtotime($r_start_date[1]));

            $r_id = $this->input->post('rules_id');
            $r_cat = $this->input->post('tax_cat');
            $r_name = $this->input->post('r_name');
            $r_desc = $this->input->post('r_desc');
            $r_start_price = $this->input->post('r_start_price');
            $r_end_price = $this->input->post('r_end_price');
            $gst = $this->input->post('gst');

            $tax_data = array(
                'r_id' => $r_id,
                'r_cat' => $r_cat,
                'r_name' => $r_name,
                'r_desc' => $r_desc,
                'r_start_price' => $r_start_price,
                'r_end_price' => $r_end_price,
                'r_start_date' => $start,
                'r_end_date' => $end,
                'same_state' => $gst,
            );


            /* echo "Main Table:=<br><pre>";
              print_r($tax_data); //exit;
              echo "</pre>";
              echo "<br><br>";
              echo $tax_data;
              //print_r($date_range);
             */


            $tax_rule_id = $this->dashboard_model->update_tax_rule($r_id, $tax_data);


            if ($tax_rule_id == true) {
                //echo $tax_rule_id;
                //die();
                foreach ($this->input->post('tax_types_5') as $key => $value) {
                    $id = $this->input->post('rule_comp_id')[$key];
                    //echo $r_id;
                    //	print_r($this->input->post())rule_comp_id;
                    $component_array = array(
                        //'p_r_id' => $r_id,
                        'tax_type' => $this->input->post('tax_types_5')[$key],
                        'tax_mode' => $this->input->post('tax_modes_5')[$key],
                        'tax_value' => $this->input->post('tax_values_5')[$key],
                        'calculated_on' => $this->input->post('calculated_ons_5')[$key],
                        'applied_on' => $this->input->post('applied_ons_5')[$key],
                    );


                    $component = $this->dashboard_model->update_tax_rule_component($id, $component_array);
                }
                /* echo "Main Table:=<br><pre>";
                  print_r($component_array); //exit;
                  echo "</pre>";
                  echo "<br><br>";
                  die();
                 */

                $this->session->set_flashdata('succ_msg', 'Rule Updated Successfully');
                redirect(base_url() . 'dashboard/all_tax_rule');
            } else {
                //echo $tax_rule_id;
                $this->session->set_flashdata('err_msg', 'Error Occoured');
                redirect(base_url() . 'dashboard/all_tax_rule');
            }
        }
    }

    function delete_tax_rule() {
        $id = $this->input->post('r_id');
        $this->dashboard_model->delete_tax_rule($id);
        $data = array(
            'data' => 'success',
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function delete_tax_rule_on_update() {
        $id = $this->input->post('tax_rule_id');
        $result = $this->dashboard_model->delete_tax_rule_only($id);
    }

    function add_charges() {
        $data['heading'] = 'Setting';
        $data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'all_charges';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        if ($this->input->post()) {
            $charges = array(
                'charge_name' => $this->input->post('name'),
                'ec_tag' => $this->input->post('tag'),
                'charge_des' => $this->input->post('charge_description'),
                'unit_price' => $this->input->post('u_price'),
                'charge_sgst' => $this->input->post('c_sgst'),
                'charge_cgst' => $this->input->post('c_cgst'),
                'charge_igst' => $this->input->post('c_igst'),
                'charge_utgst' => $this->input->post('c_utgst'),
                'charge_tax' => $this->input->post('c_tax'),
                'hsn_sac' => $this->input->post('sac_code'),
                'unit_price_editable' => $this->input->post('u_p_editable'),
                'tax_editable' => $this->input->post('tax_editable'),
                'hotel_id' => $this->session->userdata('user_hotel'),
            );
            //print_r($charges);die();
            $query = $this->dashboard_model->add_extra_charges($charges);
            if ($query) {
                redirect('dashboard/all_charges');
            }
        }
        $data['page'] = 'backend/dashboard/add_extra_charges';
        $this->load->view('backend/common/index', $data);
    }

// new controller method on 15-11-16...

    function tax_type_category_add() {
        $found = in_array("141", $this->arraay);
        if ($found) {

            if ($this->input->post()) {
                $data['heading'] = 'Tax Cetgory';
                $data['sub_heading'] = 'define tax sub category';
                $data['description'] = 'Details of Tax category';
                $data['active'] = 'add_tax_category';
                $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

                $dateTime = date('Y-m-d H:i:s');

                $tax_cate = array(
                    'tc_name' => $this->input->post('tax_cate_name'),
                    'tc_type' => $this->input->post('tax_cate_type'),
                    'tc_desc' => $this->input->post('tax_cate_desc'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id'),
                    'tc_date_time' => $dateTime
                );
                //print_r($tax_cate);die();
                $query = $this->dashboard_model->add_tax_category($tax_cate);
                if ($query) {
                    redirect('dashboard/all_tax_category');
                }
            } else {

                $data['heading'] = 'Setting';
                $data['sub_heading'] = 'Tax Settings';
                $data['description'] = 'Details of Tax category';
                $data['active'] = 'all_tax_category';
                $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

                $data['page'] = 'backend/dashboard/add_tax_category';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    function get_chrage_name() {
        $name = $this->input->post('val');
        $query = $this->dashboard_model->get_charge_name($name);
        if ($query > 0) {
            $data = array(
                'data' => "1",
            );
        } else {

            $data = array(
                'data' => "0",
            );
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function all_charges() {
        $data['heading'] = 'Setting';
        $data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'all_charges';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['charges'] = $this->dashboard_model->get_charges();
        $data['page'] = 'backend/dashboard/all_extra_charges';
        $this->load->view('backend/common/index', $data);
    }

    function edit_charges() {
        $id = $this->input->post('id');
        //$id=2;
        $query = $this->dashboard_model->get_charge_by_id($id);

        $data = array(
            'hotel_extra_charges_id' => $query->hotel_extra_charges_id,
            'charge_name' => $query->charge_name,
            'charge_des' => $query->charge_des,
            'unit_price' => $query->unit_price,
            'charge_tax' => $query->charge_tax,
            'ec_tag' => $query->ec_tag,
            'unit_price_editable' => $query->unit_price_editable,
            'tax_editable' => $query->tax_editable
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function update_charges() {
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $ch_des = $this->input->post('ch_des');
        $u_price = $this->input->post('u_price');
        $charge_tax = $this->input->post('charge_tax');
        $tax_editable = $this->input->post('tax_editable');
        $u_price_editable = $this->input->post('u_price_editable');
        $extra_tag = $this->input->post('extra_tag');

        $data = array(
            'charge_name' => $name,
            'charge_des' => $ch_des,
            'ec_tag' => $extra_tag,
            'unit_price' => $u_price,
            'charge_tax' => $charge_tax,
            'unit_price_editable' => $u_price_editable,
            'tax_editable' => $tax_editable,
        );
        $query = $this->dashboard_model->update_charge($id, $data);
        if ($query) {
            $data = array(
                'data' => 'Updated Succeffully'
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            return false;
        }
    }

    function update_tax_type() {

        $id = $this->input->post('taxid');
        $name = $this->input->post('taxname');
        $tax_categ = $this->input->post('tax_categ');
        $tax_type_id = $this->input->post('tax_type_id');
        $tax_descrp = $this->input->post('tax_descrp');

        $data = array(
            'tax_name' => $name,
            'tax_category' => $tax_categ,
            'tax_type_tag_id' => $tax_type_id,
            'tax_desc' => $tax_descrp,
        );
        $query = $this->dashboard_model->update_tax($id, $data);
        if ($query) {
            $data = array(
                'data' => 'Updated Succeffully'
            );
            header('Content-Type: application/json');
            echo json_encode($data);

            //print_r($data);
        } else {
            return false;
        }
    }

    //---

    function update_tax_category() {

        $id = $this->input->post('taxid');
        $name = $this->input->post('taxname');
        $tax_categ = $this->input->post('tax_categ');
        $tax_cate_type = $this->input->post('tax_cate_type');
        $tax_descrp = $this->input->post('tax_descrp');

        $data = array(
            'tc_id' => $id,
            'tc_name' => $name,
            'tc_type' => $tax_cate_type,
            'tc_desc' => $tax_descrp,
        );
        //print_r($data);
        //die();

        $query = $this->dashboard_model->update_tax_categorys($id, $data);
        if ($query) {
            $data = array(
                'data' => 'Updated Successfully'
            );
            header('Content-Type: application/json');
            echo json_encode($data);

            //print_r($data);
        } else {
            return false;
        }
    }

    //-----
    function delete_extra_charges() {
        $id = $_GET['ch_id'];
        $this->dashboard_model->delete_extra_charge($id);

        $data = array(
            'data' => 'success',
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function check_tax_slab() {
        $tax_cat = $this->input->post('tax_cat');
        $r_start_price = $this->input->post('r_start_price');
        $r_start_date = $this->input->post('r_start_date');
        if ($tax_cat && $r_start_price && $r_start_date) {
            //echo $tax_cat."    ".$r_start_price."    ".$r_start_date ;
            $tax_slab = array(
                'tax_cat' => $tax_cat,
                'r_start_price' => $r_start_price,
                'r_start_date' => $r_start_date,
            );
            //print_r($tax_slab); exit();
            $result = $this->dashboard_model->check_tax_slab($tax_slab);

            echo $result;
        }
    }

    function select_tax_slab() {
        $date = '2016-08-14';
        $type = 'book';
        $amount = 5002.00;
        $query = $this->dashboard_model->tax_slab($date, $type, $amount);
        if ($query != false) {
            foreach ($query as $res) {

                $row = array();
                $row['tax_type'] = $res->tax_type;
                $row['tax_value'] = $res->tax_value;
                $props[] = $row;
            }
            echo json_encode($props);
        } else {
            echo "No Tax slab found";
        }
    }

    function amc_unique_date() {
        $data = "";
        $query = $this->dashboard_model->amc_unique_date();

        if ($query) {
            foreach ($query as $noti) {
                $data .= $noti->gen_name . ",";
            }
            echo $data;
        }
    }

    function fuel_for_tostr() {
        $data = '';
        $query = $this->dashboard_model->fuel_for_tostr();
    }

    /* Add Admin Roles Start */

    function add_admin_roles() {
        $found = in_array("1", $this->arraay);
        if ($found) {
            $data['heading'] = 'Admin';
            $data['sub_heading'] = 'Add Admin Roles';
            $data['description'] = 'Add Admin Roles Here';
            $data['active'] = 'add_admin_roles';
            $data['hotel'] = $this->dashboard_model->all_hotel_list();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $permission_users = '2';
            $data['permission'] = $this->dashboard_model->admin_permission_label($permission_users);
            $data['per_all'] = $this->dashboard_model->admin_permission_all();
            if ($this->input->post()) {
                if ($this->input->post('access_booking_engine') == 'on') {
                    $access_booking_engine = '1';
                } else {
                    $access_booking_engine = '0';
                }
                if ($this->input->post('take_booking') == 'on') {
                    $take_booking = '1';
                } else {
                    $take_booking = '0';
                }
                if ($this->input->post('edit_booking') == 'on') {
                    $edit_booking = '1';
                } else {
                    $edit_booking = '0';
                }
                if ($this->input->post('add_admin') == 'on') {
                    $add_admin = '1';
                } else {
                    $add_admin = '0';
                }
                if ($this->input->post('access_setting') == 'on') {
                    $access_setting = '1';
                } else {
                    $access_setting = '0';
                }
                if ($this->input->post('access_report') == 'on') {
                    $access_report = '1';
                } else {
                    $access_report = '0';
                }
                //echo $access_booking_engine;
                //print_r($this->input->post('perval'));
                //exit;
                $permission_id = implode(",", $this->input->post('permission'));
				//echo "<pre>";
			// print_r($permission_id); exit;
			   $permission = array(
                    'admin_roll_name' => $this->input->post('admin_roll_name'),
                    'max_discount' => $this->input->post('max_discount'),
                    'user_type' => $this->input->post('admin_type'),
                    'take_booking' => $take_booking,
                    'edit_booking' => $edit_booking,
                    'add_admin' => $add_admin,
                    'access_setting' => $access_setting,
                    'access_report' => $access_report,
                    'access_booking_engine' => $access_booking_engine,
                    'user_permission_type' => $permission_id,
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'user_id' => $this->session->userdata('user_id')
                );
                //echo "<pre>";
                //print_r($permission);
                //exit;
                $user_permission = $this->dashboard_model->add_user_permission($permission);
                if (isset($user_permission) && $user_permission) {
                    $this->session->set_flashdata('succ_msg', "Admin Roll Added Successfully!");
                    redirect('dashboard/add_admin_roles');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_admin_roles');
                }
            } else {

                /* echo "<pre>";
                  print_r($data['permission']);
                  exit;
                 */
                $data['page'] = 'backend/dashboard/add_admin_roles';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Add Admin Roles End */

    function all_admin_roles() {
        $found = in_array("14", $this->arraay);
        if ($found) {
            $data['heading'] = 'Admin';
            $data['sub_heading'] = 'All Admin Roles';
            $data['description'] = 'List Of All Admin Roles';
            $data['active'] = 'all_admin_roles';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['events'] = $this->dashboard_model->all_admin_roles();
            $data['page'] = 'backend/dashboard/all_admin_roles';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }

    /* Edit Admin Roles Start */

    function edit_admin_role() {
        $found = in_array("1", $this->arraay);
        if ($found) {
            $data['heading'] = 'Admin';
            $data['sub_heading'] = 'Edit Admin Roles';
            $data['description'] = 'Edit Admin Role Here';
            $data['active'] = 'edit_admin_roles';
            $data['hotel'] = $this->dashboard_model->all_hotel_list();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $permission_users = '2';
            $data['permission'] = $this->dashboard_model->admin_permission_label($permission_users);
            if ($this->input->post()) {
                //echo $access_booking_engine;
                //print_r($this->input->post('perval'));
                //exit;
                $permission_id = implode(",", $this->input->post('permission'));
                $id = $this->input->post('id');
                $permission = array(
                    'admin_roll_name' => $this->input->post('admin_roll_name'),
                    'max_discount' => $this->input->post('max_discount'),
                    'user_type' => $this->input->post('admin_type'),
                    'user_permission_type' => $permission_id
                );
                //echo "<pre>";
                //print_r($permission);
                //exit;
                $user_permission = $this->dashboard_model->update_user_permission($permission, $id);
                if (isset($user_permission) && $user_permission) {
                    $this->session->set_flashdata('succ_msg', "Admin Roll updated Successfully!");
                    redirect('dashboard/all_admin_roles');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/all_admin_roles');
                }
            } else {
                $data['page'] = 'backend/dashboard/edit_admin_roles';
                $this->load->view('backend/common/index', $data);
            }
        } else {
            redirect('dashboard');
        }
    }

    /* Edit Admin Roles End */

    function inlineEditRole() {
        $col = $_POST["column"];
        $editval = $_POST["editval"];
        $id = $_POST["id"];
        $this->dashboard_model->inlineEditRole($col, $editval, $id);
    }

    function inlineEditPermission() {
        $col = $_POST["column"];
        $editval = $_POST["editval"];
        $id = $_POST["id"];
        $this->dashboard_model->inlineEditPermission($col, $editval, $id);
    }

    function get_no_tax_booking() {
        //$query=$this->dashboard_model->get_no_tax_booking();

        $bookings = $this->dashboard_model->get_no_tax_booking();
        $transactions = $this->dashboard_model->all_transactions();
        $output = '<table class="table table-striped table-hover table-bordered table-scrollable" id="dataTable2">
          <thead>
            <tr>
             <th> Select </th>
              <th> Booking Id </th>
              <th> Booking Status </th>
              <th> Customer Name </th>
              <th> Room Number </th>
              <th> Booking Source </th>
              <th> Nature Of Visit</th>
              <th> PAX</th>
              <th> From and Upto Date </th>
              <th> Customer Address </th>
              <th> Customer Contact Number </th>
              <th> Food Plan</th>
              <th> Amount to be paid </th>
			   <th>Marketing Personel</th>
              <th> Admin </th>
              <th scope="col" class="action"> Action </th>
            </tr>
          </thead>
          <tbody>';
        if (isset($bookings) && $bookings):
            //print_r($bookings);
            $i = 1;
            $deposit = 0;
            $item_no = 0;
            $fp = 0;
            $m_p = 0;
            $msg = "";
            foreach ($bookings as $booking):
                if ($booking->service_tax == '0' && $booking->luxury_tax == '0') {
                    $msg = "No Tax";
                } else {
                    $msg = "";
                }
                $bills = $this->bookings_model->all_folio($booking->booking_id);





                if (isset($bills)) {

                    foreach ($bills as $item) {
                        # code...

                        $item_no++;
                    }
                }

                $rooms = $this->dashboard_model->get_room_number($booking->room_id);
                if (isset($rooms) && $rooms):
                    foreach ($rooms as $room):


                        if ($room->hotel_id == $this->session->userdata('user_hotel')):


                            foreach ($transactions as $transaction):

                                if ($transaction->t_booking_id == 'HM0' . $this->session->userdata('user_hotel') . '00' . $booking->booking_id):

                                    $deposit = $deposit + $transaction->t_amount;
                                endif;
                            endforeach;



                            /* Calculation start */

                            $room_number = $room->room_no;
                            $room_cost = $room->room_rent;
                            /* Find Duration */
                            $date1 = date_create($booking->cust_from_date);
                            $date2 = date_create($booking->cust_end_date);
                            $diff = date_diff($date1, $date2);
                            $cust_duration = $diff->format("%a");

                            /* Cost Calculations */
                            $total_cost = $cust_duration * $room_cost;
                            $total_cost = $total_cost + $total_cost * 0.15;
                            $food_tax = 0;
                            if ($booking->food_plans != "") {
                                $data = $this->bookings_model->fetch_food_plan($booking->food_plans);
                                if ($data)
                                    $food_tax = ($data->fp_tax_percentage / 100) * $booking->food_plan_price;
                            }
                            $due = ($booking->room_rent_sum_total + $booking->service_price + $booking->food_plan_price + $food_tax + $booking->booking_extra_charge_amount) - $booking->cust_payment_initial - $deposit;

                            $status_id = $booking->booking_status_id;

                            $status = $this->dashboard_model->get_status_by_id($status_id);

                            if (isset($status)) {
                                //print_r($status);
                                foreach ($status as $st) {

                                    /* Calculation End */


                                    $class = ($i % 2 == 0) ? "active" : "success";

                                    $booking_id = 'HM0' . $this->session->userdata('user_hotel') . '00' . $booking->booking_id;
                                    $book_id = $booking->booking_id;

                                    $output .= '<tr>';
                                    $output .= '<td><input type="checkbox" name="delete[]" value="' . $booking->booking_id . '" class="checkboxes" onclick="this.form.submit();"></td>';
                                    $output .= '<td></td>';
                                    $output .= '<td></td>';
                                    $output .= '<td></td>';



                                    $guests = $this->dashboard_model->get_guest_details($booking->guest_id);
                                    if (isset($guests) && $guests) {
                                        foreach ($guests as $key) {
                                            if ($key->g_name != "") {
                                                $g_name = $key->g_name;
                                            } else {
                                                $g_name = "na";
                                            }
                                        }
                                    }

                                    $output .= '<td>' . $g_name . '</td>';
                                    $output .= '<td>' . $room_number . '</td>';
                                    $output .= '<td>' . $booking->booking_source . '</td>';
                                    $output .= '<td>' . $booking->nature_visit . '</td>';
                                    $output .= '<td>' . $booking->no_of_guest . '</td>';
                                    $output .= ' <td>' . $booking->cust_from_date - $booking->cust_end_date . '</td>';
                                    $guest = $this->dashboard_model->get_guest_details($booking->guest_id);
                                    if (isset($guest) && $guest) {
                                        foreach ($guest as $keys) {
                                            if ($keys->g_address != "") {
                                                $g_d = $keys->g_address . "," . $keys->g_city . "- " . $keys->g_pincode . "," . $keys->g_state;
                                            } else {
                                                $g_d = "";
                                            }
                                        }
                                    }
                                    $output .= '<td>' . $g_d . '</td>';
                                    $output .= '<td>' . $booking->cust_contact_no . '</td>';
                                    $data = $this->bookings_model->fetch_food_plan($booking->food_plans);
                                    if ($data) {
                                        $fp = $data->fp_name;
                                    }
                                    $output .= '<td>' . $fp . '</td>';
                                    $output .= '<td>' . max($due, 0) . '</td>';
                                    if (isset($booking->booking_id_actual) && $booking->booking_id_actual) {
                                        $m_p = $booking->marketing_personnel;
                                    }
                                    $output .= '<td>' . $m_p . '</td>';

                                    $admin = $this->dashboard_model->get_admin($booking->user_id);

                                    if (isset($admin)) {
                                        foreach ($admin as $admin_name) {
                                            $a_name = $admin_name->admin_first_name . " " . $admin_name->admin_last_name;
                                        }
                                    }



                                    $output .= '<td>' . $a_name . '</td>';
                                    $output .= '<td class="ba">
            	<div class="btn-group">
                  <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li> <a onclick="soft_delete(' . $booking->booking_id . ')" data-toggle="modal" class="btn red btn-sm"><i class="fa fa-trash"></i></a> </li>
                    <li><a href="' . base_url() . 'dashboard/booking_edit?b_id=' . $booking->booking_id . '" data-toggle="modal" class="btn green btn-sm"><i class="fa fa-edit"></i></a></li>
                    <li> <a href="' . base_url() . 'dashboard/ if($booking->group_id !=0){echo "booking_edit_group?group_id=".$booking->group_id."" ;}else{echo "booking_edit?b_id=".$booking->booking_id."&fl=1" ;}" data-toggle="modal" class="btn blue btn-sm"><i class="fa fa-inr"></i></a></li>
                    <li><a id="dwn_link" onclick="download_pdf(' . $book_id . ');" class="btn purple btn-sm"><i class="fa fa-download"></i></a></li>
                  </ul>
                </div>
            <table width="100%">
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </table></td>';
                                    $output .= '</tr>';
                                    $i++;
                                }
                            }
                        endif;
                    endforeach;
                endif;
            endforeach;
        endif;

        $output .= '</tbody></table>';


        echo $output;
    }

    function account_settings() {
        $this->load->model('dashboard_model');
        $data['heading'] = 'Admin';
        $data['sub_heading'] = 'Account';
        $data['description'] = '';
        $data['active'] = 'account_settings';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
        $data['page'] = 'backend/dashboard/account_settings';
        $this->load->view('backend/common/index', $data);
    }

    function chart() {
        $found = 1;
        if ($found) {
            //$this->load->model('account_model');
            $data['heading'] = '';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['active'] = 'all_unit_class';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
            $data['page'] = 'backend/dashboard/chart';
            $this->load->view('backend/common/index', $data);
        }
    }

    function duplicate_product() {
        $p_name = $_POST['data'];
        $check_p = $this->dashboard_model->duplicate_product($p_name);
        if ($check_p > 0) {
            $data = array('data' => 'y');
        } else {
            $data = array('data' => 'n');
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function search_vendor() {


        $query = (!empty($_GET['q'])) ? strtolower($_GET['q']) : null;
        $status = true;
        $result = $this->dashboard_model->all_vendor();
        foreach ($result as $gt) {
            if ($gt['hotel_vendor_image'] != '') {
                $img = $gt['hotel_vendor_image'];
            } else {
                $img = 'no_user.png';
            }
            $parent = $gt['hotel_parent_vendor_id'];
            if ($parent == NULL)
                $parent = 'N/A';
            $vendor[] = array(
                'id' => $gt['hotel_vendor_id'],
                'name' => $gt['hotel_vendor_name'],
                'ph_no' => $gt['hotel_vendor_contact_no'],
                'email' => $gt['hotel_vendor_email'],
                'address' => $gt['hotel_vendor_address'],
                "image" => base_url() . "upload/vendor_detail/" . $img,
                "city" => $gt['hotel_vendor_city'],
                "industry" => $gt['hotel_vendor_industry'],
                "field" => $gt['hotel_vendor_field'],
                "contracted" => $gt['hotel_vendor_is_contracted'],
                "parent_vendor" => $parent,
            );
        }
        $resultProjects = [];

        foreach ($vendor as $key => $oneProject) {
            if (strtolower($oneProject["name"]) !== false) {
                $resultProjects[] = $oneProject;
            }
        }

        if (empty($resultProjects)) {
            $status = false;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            "status" => $status,
            "error" => null,
            "data" => array(
                "name" => $resultProjects
            )
        ));
    }

    function bill_to_com() {
        $id = $_POST['data'];
        //$id=1084;
        $query = $this->dashboard_model->bill_to_com($id);

        if ($query == '1') {
            $data = array(
                'data' => 'Inactive',
            );
        } else {
            $data = array(
                'data' => 'Active',
            );
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function bill_to_com_grp() {
        $id = $_POST['data'];
        //$id=1084;
        $query = $this->dashboard_model->bill_to_com_grp($id);

        if ($query == '1') {
            $data = array(
                'data' => 'Inactive',
            );
        } else {
            $data = array(
                'data' => 'Active',
            );
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function delete_itm() {
        $id = $_POST['i_id'];
        $i_id = $_POST['data'];
       $due_amt=$_POST['due_amt'];
        $this->dashboard_model->update_itmAmt($id, $due_amt);
        $query = $this->dashboard_model->delete_itm($id, $i_id);
        if ($query) {
            $data = array(
                'data' => 'success'
            );
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function update_g_info() {
        $g_id = $_POST['id'];
        $g_id_type = $_POST['g_id_type'];
        $id_number = $_POST['id_number'];
        $guest_number = $_POST['guest_number'];

        $data = array(
            'g_id_type' => $g_id_type,
            'g_id_number' => $id_number,
            'g_contact_no' => $guest_number,
        );
        $data1 = array(
            'g_id_type' => $g_id_type,
            'g_id_number' => $id_number,
            'cust_contact_no' => $guest_number,
        );
        $query = $this->dashboard_model->update_g_info($g_id, $data, $data1);
        if ($query) {
            $data = "success";
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function update_booking_info() {

        $id = $_POST['id'];
        $bk_date = $_POST['date'];
        $bk_sc = $_POST['bk_sc'];
        $no_ad = $_POST['no_ad'];
        $no_ch = $_POST['no_ch'];
        $bk_rent = $_POSt['bk_rent'];
        $plan_id = $_POST['plan_id'];
        $md_rm_rnt = $_POST['md_rm_rnt'];
        $md_ml_rnt = $_POST['md_ml_rnt'];
        $md_rm_type = $_POST['md_rm_type'];
        $md_ml_type = $_POST['md_ml_type'];
        $base = $_POST['base'];
        $meal_price = $_POST['meal_price'];
        $stay = $_POST['stay'];
        $mod = $_POST['modf_rr'];
        $m_mod = $_POST['modf_mp'];
        $no_of_guest = $no_ad + $no_ch;

        $data = array(
            'no_of_adult' => $no_ad,
            'no_of_child' => $no_ch,
            'base_room_rent' => $base,
            'room_rent_total_amount' => $base * $stay,
         //   'stay_days' => $stay,
            'mod_room_rent' => $mod,
            'rent_mode_type' => $md_rm_type,
            'food_plans' => $plan_id,
            'no_of_guest' => $no_of_guest,
            'food_plan_mod_type' => $md_ml_type,
            'food_plan_mod_rent' => $m_mod,
            'ex_per_price' => $_POST['ex_ch_amount'],
            'ex_ad_no' => $_POST['ex_ad_no'],
            'ex_ch_no' => $_POST['ex_ch_no']
        );

        $query = $this->dashboard_model->update_booking_info($id, $data);
        if ($query) {
            $data = array(
                'data' => 1,
            );
        } else {
            $data = array(
                'data' => 0,
            );
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function check_rm() {
        $date = $this->input->post('date');
        $room_id = $this->input->post('room_id');
        $dates = str_replace(substr($date, 11, 1), "~", $date);

        $date_range[] = explode("~", $dates);
        $r_start_date = $date_range[0];
        $start = date('Y-m-d', strtotime($r_start_date[0]));
        $end = date('Y-m-d', strtotime($r_start_date[1]));

        $get_avRm = $this->dashboard_model->get_avRm($start, $end, $room_id);
        if ($get_avRm > 0) {
            $data = array(
                'data' => 1
            );
        } else {
            $data = array(
                'data' => 0,
                'strt_date' => $start,
                'end_date' => $end,
            );
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }
	

    function test_typehead() {
        $query = (!empty($_GET['q'])) ? strtolower($_GET['q']) : null;
        $status = true;
        $get_guest = $this->dashboard_model->get_guestInfo();
        foreach ($get_guest as $gt) {
            if ($gt['g_photo_thumb'] != '') {
                $img = $gt['g_photo_thumb'];
            } else {
                $img = 'no_user.png';
            }
			if(isset($gt['g_name']) && $gt['g_name']){ 
			$name_ty=$gt['g_name'];
			}else{
				$name_ty='';
			}
			
			if(isset($gt['g_contact_no']) && $gt['g_contact_no']){ 
			$contact_ty=$gt['g_contact_no'];
			}else{
				$contact_ty='';
			}
			if(isset($gt['g_email']) && $gt['g_email']){ 
			$email_ty=$gt['g_email'];
			}else{
				$email_ty='';
			}
			
			if(isset($gt['g_address']) && $gt['g_address']){ 
			$address_ty=$gt['g_address'];
			}else{
				$address_ty='';
			}
			if(isset($gt['g_pincode']) && $gt['g_pincode']){ 
			$pincode_ty=$gt['g_pincode'];
			}else{
				$pincode_ty='';
			}
			if(isset($gt['g_id_type']) && $gt['g_id_type']){ 
			$gtype_ty=$gt['g_id_type'];
			}else{
				$gtype_ty='';
			}
			if(isset($gt['g_gender']) && $gt['g_gender']){ 
			$gender_ty=$gt['g_gender'];
			}else{
				$gender_ty='';
			}
			
			if(isset($gt['g_id_number']) && $gt['g_id_number'] ){
				$id_no=$gt['g_id_number'];
			}else{
				$id_no='';
			}
			
			if(isset($gt['g_state']) && $gt['g_state'] ){
				$state_ty=$gt['g_state'];
			}else{
				$state_ty='';
			}
			
			if(isset($gt['g_country']) && $gt['g_country'] ){
				$country_ty=$gt['g_country'];
			}else{
				$country_ty='';
			}
			
			if(isset($gt['g_place']) && $gt['g_place'] ){
				$state_code=$gt['g_place'];
			}else{
				$state_code='';
			}
			
			
            $guest[] = array(
                'id' => $gt['g_id'],
                'name' => $name_ty,
                'ph_no' => $contact_ty,
                'email' => $email_ty,
                'address' => $address_ty,
               'pincode' => $pincode_ty,
               'gender' => $gender_ty,
                'id_Type' => $gtype_ty,
				'idNO'=>$id_no,
                //'charge' => $gt['booking_extra_charge_amount'],
                "image" => base_url() . "upload/guest/thumbnail/photo/" . $img,
				"state" =>$state_ty,
				"country" =>$country_ty,
				"state_code" => $state_code
            );
        }
        $resultProjects = [];

        foreach ($guest as $key => $oneProject) {
            if (strtolower($oneProject["name"]) !== false) {
                $resultProjects[] = $oneProject;
            }
        }

        if (empty($resultProjects)) {
            $status = false;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            "status" => $status,
            "error" => null,
            "data" => array(
                "name" => $resultProjects
            )
        ));
    }

    function hotel_change() {

        $hot_id = $this->input->post('a');
        $_SESSION['user_hotel'] = $hot_id;
        //$this->session->set_userdata("user_hotel",$hot_id);
        //$this->session->set_userdata(array( 'user_hotel' =>$hot_id ));
        $chain_id = 1;
        $this->session->set_userdata("chain_id_hotel", $chain_id);
        $this->session->set_userdata("cd_key", '');
        $hotelarray = $this->dashboard_model->get_hotel_name($hot_id, $chain_id);
        //print_r($hotelarray);
        //exit;
        $data = array(
            'msg' => 'success',
            'hotel' => $hotelarray->hotel_name,
            'se' => 'fbfb',
            'id' => $_SESSION['user_hotel']
        );


        header('Content-Type: application/json');
        echo json_encode($data);


//echo "hello world";
    }

    function booking_sales_report() {
        $data['heading'] = 'Reports';
        $data['sub_heading'] = 'Transaction Report';
        $data['description'] = 'Report List';
        $data['active'] = 'all_f_reports';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        if ($this->input->post()) {
            $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
            $data['start_date'] = $this->input->post('t_dt_frm');
            $data['end_date'] = $this->input->post('t_dt_to');
            //$data['sales'] = $this->dashboard_model->all_transactions_by_date1($start_date,$end_date);
            $data['sales'] = $this->dashboard_model->all_bookings_daily_by_date($start_date, $end_date);
        } else {
            // $data['sales'] = $this->dashboard_model->all_f_reports1();
            $data['sales'] = $this->dashboard_model->all_bookings_daily_grp();
            //  $data['test'] = $this->dashboard_model->all_bookings_daily_grp();
            // print_r( $data['sales']); exit;
        }
        $data['page'] = 'backend/dashboard/daily_sales_report';
        $this->load->view('backend/common/index', $data);
    }
    

    function edit_unit_type_new() {
        $id = $_POST['u_id'];
        $result = $this->unit_class_model->edit_unit_type($id);

        $data = array(
            'max_occupancy' => $result->max_occupancy,
            'default_occupancy' => $result->default_occupancy
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function excel_tst() {
        $this->load->library('Excel');
        /* if($this->input->post()){
          $data['m']=$this->input->post('month');
          $data['y']=$this->input->post('year');
          $data['maxDays']=cal_days_in_month(CAL_GREGORIAN, $data['m'], $data['y']);
          }else{
          $data['y']= date("Y");
          $data['m']= date("m");
          $data['maxDays']=date('t'); */
        //}
        //  $data['summary'] = $this->dashboard_model->fetch_r_summary();
        // $data['totalrooms']=$this->dashboard_model->totalrooms();
        //  $x=count($data['totalrooms']);
        //  $data['totalrooms']=$x;
        // $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        // $this->load->view("backend/dashboard/all_reports_pdf",$data);



        $filename = base_url() . 'exl/Book1.xlsx'; //save our workbook as this file name
        $obj = PHPExcel_IOFactory::load($filename);

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Users list');

        // load database
        $this->load->database();

        // load model
        $this->load->model('userModel');

        // get all users in array formate
        $users = $this->dashboard_model->fetch_r_summary();

        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($users);

        $filename = 'just_some_random_name.xls'; //save our workbook as this file name

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function supportmail() {
        $data['heading'] = '';
        $data['sub_heading'] = ' ';
        $data['description'] = '';
        $data['active'] = '';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/support/support_mail';
        $this->load->view('backend/common/index', $data);
    }

    function setting() {
        $data['heading'] = '';
        $data['sub_heading'] = ' ';
        $data['description'] = '';
        $data['active'] = 'setting';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/setting';
        $this->load->view('backend/common/index', $data);
    }

    function reports() {
        $data['heading'] = '';
        $data['sub_heading'] = ' ';
        $data['description'] = '';
        $data['active'] = 'hotel_reports';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/hotel_reports';
        $this->load->view('backend/common/index', $data);
    }

    function warehouse_default() {
        $data['heading'] = 'Setting';
        $data['sub_heading'] = ' ';
        $data['description'] = '';
        $data['active'] = 'warehouse_default';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/warehouse_default';
        $this->load->view('backend/common/index', $data);
    }

    function set_default() {
        $warehouseId = $this->input->post('id');
        $hotel_id = $this->session->userdata('user_hotel');
        $data = $this->dashboard_model->set_default($warehouseId, $hotel_id);
        $data = array(
            'data' => "sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function get_default_kit() {
        $kitId = $this->input->post('id');
        $hotel_id = $this->session->userdata('user_hotel');
        $query = $this->dashboard_model->get_default_kit($kitId, $hotel_id);
        if ($query) {
            $data = array(
                'data' => 1,
            );
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function search_kit_autocomplete() {

        $query = (!empty($_GET['q'])) ? strtolower($_GET['q']) : null;
        $status = true;
        $result = $this->dashboard_model->search_kit_autocomplete();

        foreach ($result as $rs) {

            $kit[] = array(
                'id' => $rs['hotel_stock_inventory_id'],
                'name' => $rs['a_name'],
                'unitprice' => $rs['u_price'],
                'warehouse' => $rs['warehouse'],
                'image' => base_url() . "upload/asset/" . $rs['a_image'],
            );
        }
        $resultProjects = [];

        foreach ($kit as $key => $oneProject) {
            if (strtolower($oneProject["name"]) !== false) {
                $resultProjects[] = $oneProject;
            }
        }

        if (empty($resultProjects)) {
            $status = false;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            "status" => $status,
            "error" => null,
            "data" => array(
                "name" => $resultProjects
            )
        ));
    }

    function delete_alot_kit() {
        $id = $_POST['id'];
        $itemid = $_POST['itemid'];
        $qty = $_POST['qty'];
        $query = $this->dashboard_model->delete_alot_kit($id);

        $query1 = $this->dashboard_model->update_stock_qty_mapping_del($itemid, $this->session->userdata('warehouse'), $qty);
        $query2 = $this->dashboard_model->update_del_stock_inventory($itemid, $qty);
    }

    function get_item_by_kit() {
        //$kit_id=$_POST['kitid'];
        $kit_id = 25;
        $get_kit = $this->dashboard_model->get_item_by_kit($kit_id);
        echo "<pre>";
        print_r($get_kit);
    }

    function get_all_sb_by_group_id() {
        $id = $_POST['booking_id'];
        $ac_id = $_POST['bookingac_id'];

        $chk_gb = $this->dashboard_model->chk_gb($id);

        if (isset($chk_gb) && $chk_gb->group_id != 0 && $chk_gb->group_id != '') {
            $group_id = $chk_gb->group_id;
            $allBk = $this->dashboard_model->get_all_sb_by_group_id($group_id);

            foreach ($allBk as $bk) {
                $bk_id = $bk->booking_id_actual . " ,";
                echo $bk_id = $bk_id;
            }
        } else {

            echo $ac_id;
        }
    }

    function room_view() {
        $data['heading'] = '';
        $data['sub_heading'] = ' ';
        $data['description'] = '';
        $data['active'] = 'hotel_reports';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['unit'] = $this->dashboard_model->all_unit_type();
        $data['rooms'] = $this->dashboard_model->all_rooms();
        //$data['rooms'] = $this->unit_class_model->room_view_data($this->session->userdata('user_hotel'));
        //print_r($data['unit']);exit;

        $data['page'] = 'backend/dashboard/room_view';
        $this->load->view('backend/common/index', $data);
    }
	
	function room_view_test() {
        $data['heading'] = '';
        $data['sub_heading'] = ' ';
        $data['description'] = '';
        $data['active'] = 'hotel_reports';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        //$data['unit'] = $this->bookings_model->room_view();
        // $data['rooms'] = $this->dashboard_model->all_rooms();
        $data['rooms'] = $this->bookings_model->room_view();
        //print_r($data['unit']);exit;

        $data['page'] = 'backend/dashboard/room_view_test';
        $this->load->view('backend/common/index', $data);
    }

    function add_adjustment() {
        $id = $_POST['id'];
        $type = $_POST['type'];
        $amt = $_POST['amount'];
        $reason = $_POST['ad_res'];
        $note = $_POST['note'];

        switch ($reason) {
            case 'Adjustment(-)':
                $dr = 'Cr';
                $amt = -$amt;
                break;
            case 'Adjustment(+)':
                $dr = 'Dr';

                break;
            case 'Extra Amount Charged':
                $dr = 'Dr';

                break;
            case 'Less Amount Charged':
                $dr = 'Cr';
                $amt = -$amt;
                break;
            case 'Guest Refused to Pay':
                $dr = 'Cr';
                $amt = -$amt;
                break;
            case 'Guest Absconding':
                $dr = 'Cr';
                $amt = -$amt;
                break;
            case 'Damage/ Theft':
                $dr = 'Dr';
                break;
            case 'Internal Staff':
                $dr = 'Cr';
                $amt = -$amt;
                break;
            case 'Debit Note':
                $dr = 'Dr';
            case 'Credit Note':
                $dr = 'Cr';
                $amt = -$amt;
                break;
            /* case 'Dr':
              $dr = 'Dr';
              break;
              case 'Cr':
              $dr = 'Cr';
              $amt=-$amt;
              break; */
            default:
                $dr = '';
        }

        if ($type == 'gb') {
            $data = array(
                'group_id' => $id,
                'amount' => $amt,
                'dr_cr' => $dr,
                'reason' => $reason,
                'note' => $note,
                'user_id' => $this->session->userdata('user_id'),
                'hotel_id' => $this->session->userdata('user_hotel'),
            );
        } else {
            $data = array(
                'booking_id' => $id,
                'amount' => $amt,
                'dr_cr' => $dr,
                'reason' => $reason,
                'note' => $note,
                'user_id' => $this->session->userdata('user_id'),
                'hotel_id' => $this->session->userdata('user_hotel'),
            );
        }


        $query = $this->dashboard_model->add_adjustment($data);
        if ($query) {
            echo 1;
        } else {
            echo 0;
        }
    }

    function delete_Adjst() {
        $id = $_POST['id'];

        $query = $this->dashboard_model->delete_Adjst($id);
        if ($query) {
            echo 1;
        } else {
            echo 0;
        }
    }

    function get_group_id() {
        $id = $_POST['id'];
        $result = $this->dashboard_model->get_group_id($id);

        echo $result->group_id;
    }

	function send_msg(){
		$bookings_id=$_POST['booking_id'];
		$select_bookings2 = $this->dashboard_model->get_booking_details3($bookings_id);
		$statID=0;
     //   foreach($select_bookings2 as $row2)
        	$statID=$select_bookings2->booking_status_id;
            $booking_status_id_pre = $select_bookings2->booking_status_id;
            $cust_name=$select_bookings2->cust_name;
            $cust_contact_no=$select_bookings2->cust_contact_no;
            $cust_destination=$select_bookings2->next_destination;
            $cust_from=$select_bookings2->cust_from_date;
            $cust_end=$select_bookings2->cust_end_date;
            $cust_total=$select_bookings2->rm_total + $select_bookings2->rr_tot_tax+ $select_bookings2->mp_tot+ $select_bookings2->mp_tax;
     //   }
        $this->load->model('dashboard_model');
        $hotel_name = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $hot_name=$hotel_name->hotel_name;
        $text=$cust_name." from ".$cust_destination." booked ".$hot_name." from:".$cust_from." to: ".$cust_end." , booking amount:".$cust_total;
        $text_guest="Booking in ".$hot_name." successfull!! Name: ".$cust_name." From: ".$cust_destination." Booked from : ".$cust_from." to: ".$cust_end." ,Booking Amount: ".$cust_total;
        $this->load->model('bookings_model');
        $this->bookings_model->send_sms($text);
        $this->bookings_model->send_sms_guest($cust_contact_no,$text_guest);
	}
	
function payment_itm(){
	
	$id=$_POST['id'];
	$item_details=$this->dashboard_model->get_item_details($id);
	header('Content-Type: application/json');
        echo json_encode($item_details);
	//print_r($item_details);
	}	
	function delete_lineitm(){
		$l_id=$_GET['pay_id'];
		$query=$this->db1->dashboard_model->delete_lineitm($l_id);
	}
	
	
	
	function generate_inv(){
		
		$bkng_id = $this->input->post('booking_id');
		$invoice = $this->dashboard_model->get_invoice_index($bkng_id);
		echo $invoice;
	}
	
	
function all_fuel_stock_itm(){
	 $query = (!empty($_GET['q'])) ? strtolower($_GET['q']) : null;
        $status = true;
		$result = $this->dashboard_model->all_fuel_stock_itm();;
	//echo "<pre>";
	//	print_r($result); exit;
	 foreach ($result as $gt) {
          
           
            $item[] = array(
                'id' => $gt['id'],
                'name' => $gt['item'],
                'uprice' => $gt['u_price'],
               
                
            );
        }
        $resultProjects = [];

        foreach ($item as $key => $oneProject) {
            if (strtolower($oneProject["name"]) !== false) {
                $resultProjects[] = $oneProject;
            }
        }

        if (empty($resultProjects)) {
            $status = false;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            "status" => $status,
            "error" => null,
            "data" => array(
                "name" => $resultProjects
            )
        ));
	
	
	
}	
	function guest_view($id=null){
		
		///echo $_GET['id'];exit;
		 $data['heading'] = '';
        $data['sub_heading'] = ' ';
        $data['description'] = '';
        $data['active'] = '';
		//$id=7;
		$data['guest'] = $this->unit_class_model->get_guest_details($id,$this->session->userdata('user_hotel'));
		//echo "<pre>";
		//print_r($data['guest']);exit;
		 $data['page'] = 'backend/dashboard/guest_view';
        $this->load->view('backend/common/index', $data);
	}
	
	function chain_master_dashboard(){
		 $data['heading'] = '';
        $data['sub_heading'] = ' ';
        $data['description'] = '';
        $data['active'] = '';
		 $data['page'] = 'backend/dashboard/chain_master_dashboard';
        $this->load->view('backend/common/index', $data);
	}
	
	function change_bookings(){
		$id=$_POST['id'];
		$status=$_POST['status'];
		$getbk=$this->dashboard_model->getDetailsBk($id);
		if($status==5){
		$data=array(
		
                'cust_from_date' => $getbk->cust_from_date, 
                'checkin_time' => $getbk->checkin_time,
                'checkin_date' => $getbk->checkin_date,
               'booking_status_id' => $getbk->booking_status_id,
               
		);
		}else{
			$data=array(
		
               // 'cust_from_date' => $getbk->cust_from_date, 
                //'checkin_time' => $getbk->checkin_time,
                //'checkin_date' => $getbk->checkin_date,
                'cust_end_date' => $getbk->cust_end_date,
                'cust_end_date_actual' => $getbk->cust_end_date_actual,
                'checkout_time' => $getbk->checkout_time,
                'booking_status_id' => $getbk->booking_status_id,
               
		);
		}
		$updatebk=$this->dashboard_model->changebkdetalis($id,$data);
		if($updatebk){
			echo 1;
		}
	}
	
	
function change_GPbookings(){

		$id=$_POST['id'];
		$status=$_POST['status'];

		$GBdetails=$this->dashboard_model->getDetailsGB($id);
		if(isset($GBdetails) && $GBdetails ){
		foreach($GBdetails as $getbk){
			$bk_id=$getbk->booking_id;
		if($status==5){

		$data=array(

				'cust_from_date' => $getbk->cust_from_date, 
				'checkin_time' => $getbk->checkin_time,
				'checkin_date' => '',
				'booking_status_id' => $getbk->booking_status_id,
				);

		}
		else{

			$data=array(

                'cust_end_date' => $getbk->cust_end_date,
				'cust_end_date_actual' => $getbk->cust_end_date_actual,
				'checkout_time' => $getbk->checkout_time,
				'booking_status_id' => $getbk->booking_status_id,
				);

		}

		$updatebk=$this->dashboard_model->changebkdetalis($bk_id,$data);
		$this->dashboard_model->change_GBstatus($id,$data);
		}
		
		if($updatebk){

			echo 1;

		}
		}else{
			echo 0;
		}

}	
	
function checkActiceLogin(){
		
		
		//$a_id = $this->input->post('user');
		//$h_id = $this->input->post('hotel');
		$logins = $this->dashboard_model->getActiceLogins();
		$id=$this->session->userdata('login_session_id');
		$str = 'abcd';
		$c = 0;
		$data=array(
                'status' => "fail",
                'msg'=>$id
            );
			
		if(isset($logins) && $logins){
			foreach($logins as $login){
				//$c++;
				
				/*$LoginA[]*/ $data = array(
					/*'status' => $login->status,
					'id'=> $login->hls_id,
					'login'=> $login->login_dateTime,*/
					'counter' => $login->counter,
					//'count' => $c,
					'max' => $login->max,
					
				);				
			}
		}
		
		header('Content-Type: application/json');
		echo json_encode($data);
	}
	
	
	function testing(){
	    
	    $sql="select bid,date,b_type from ((select booking_id as bid,cust_end_date as date,booking_id as b_type from hotel_bookings where hotel_id=6 and group_id=0 and cust_from_date_actual >= '2017-08-01' order by cust_end_date,booking_id) UNION (SELECT id as bid,end_date as date,NULL as b_type FROM hotel_group where `hotel_id` = 6 ORDER by str_to_date(`end_date`, '%m-%d-%Y'),id)) sanjib order by date";
	    $result = $this->db1->query($sql);
	    if($result){
	        
	        echo "<pre>";
	        print_r($result->result());
	    }
	    
	    
	}
	
	
	
function sms_test(){
    
    
    $id = "AC12f8fa1dbd590c84c6d01c7cae3d2f6b";
$token = "ee859a8d18e6b87dcfbdcc7ebe0724cb";
$url = "https://api.twilio.com/2010-04-01/Accounts/$id/SMS/Messages";
$from = "+13156057118";
$to = "+917001065632"; // twilio trial verified number
$body = "using twilio rest api from Fedrick";
$data = array (
    'From' => $from,
    'To' => $to,
    'Body' => $body,
);

$post = http_build_query($data);
$x = curl_init($url );
curl_setopt($x, CURLOPT_POST, true);
curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
curl_setopt($x, CURLOPT_POSTFIELDS, $post);
$y = curl_exec($x);
curl_close($x);

die('dcdsv');


}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
 function advance_bk_payment(){
     
      $id = "AC12f8fa1dbd590c84c6d01c7cae3d2f6b";
$token = "ee859a8d18e6b87dcfbdcc7ebe0724cb";
$url = "https://api.twilio.com/2010-04-01/Accounts/$id/Messages.json";
$from = "+13156057118";
$to = "+917001065362"; // twilio trial verified number
$body = "using twilio rest api from Fedrick";
$data = array (
    'From' => $from,
    'To' => $to,
    'Body' => $body,
);

$post = http_build_query($data);
$x = curl_init($url );
curl_setopt($x, CURLOPT_POST, true);
curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
curl_setopt($x, CURLOPT_POSTFIELDS, $post);
$y = curl_exec($x);
curl_close($x);

var_dump($y);
     
     
     
     
     
     
     
     
     
    //   $data['heading'] = '';
    //     $data['sub_heading'] = ' ';
    //     $data['description'] = '';
    //     $data['active'] = '';
    //     $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
    //     //$data['unit'] = $this->bookings_model->room_view();
    //     // $data['rooms'] = $this->dashboard_model->all_rooms();
    //   $data['ADpayments'] = $this->dashboard_model->allAdPayments();
    //     $data['page'] = 'backend/dashboard/all_advance_payments';
    //     $this->load->view('backend/common/index', $data);
 }	
	
}// end of function
?>
