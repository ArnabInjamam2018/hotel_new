<?php

class Room {}
class Result {}
class Event {}
class enquiry  extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
if (!$this->session->userdata('is_logged_in')) {            redirect('login');        }
        //$this->load->database();
		 $db['superadmin'] = array(
    'dsn'   => '',
    'hostname' => 'localhost',
    'username' => $this->session->userdata('username'),
    'password' => $this->session->userdata('password'),
    'database' => $this->session->userdata('database'),
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => TRUE,
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);
        //echo "<pre>";
       //    print_r($db['default']);exit;
            $CI = &get_instance();
        $this->db1 = $CI->load->database($db['superadmin'],true);
		$this->arraay =$this->session->userdata('per');
                //$login_id=$this->session->userdata('admin_id');
                //if(!$login_id>0){redirect(base_url('admin/login')); }
        $this->load->model('mod_common');
        $this->load->model('bookings_model');
        $this->load->model('dashboard_model');
		$this->load->model('reports_model');
        $this->load->model('setting_model');
        $this->load->library('grocery_CRUD');
		$this->load->library('table');
    }

	function index(){
        $data['heading']='Enquiry';
        $data['sub_heading']='Enquiry';
        $data['description']='Enquiry';
        $data['active']='enquiry';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['vendor'] = $this->dashboard_model->all_vendor();		
		$data['page'] = 'backend/dashboard/index_enquery';
		$this->load->view('backend/common/index', $data);
	}
	function test(){
		 $this->load->view("backend/dashboard/enq");
	}
    function getEnquiry()
    {   
	   
       // $start=$_POST['start_date'];
      //  $start=date("d-m-Y", strtotime($_POST['start_date']));
       // $end=date("d-m-Y", strtotime($_POST['end_date']));
        //$end=$_POST['end_date'];
		/*if(isset($_SESSION['unit_type']) && !empty($_SESSION['unit_type'])) {
		} else {
			list($a,$b,$c)=explode('~',$_POST['unit_type']);
			$_SESSION['unit_type'] = $a;
			$_SESSION['do'] = $b;
			$_SESSION['mo'] = $c;
		}
		if(isset($_SESSION['end']) && !empty($_SESSION['end'])) {
		} else {
			$end=$_POST['end_date'];
			$_SESSION['end'] = $end;
		}
		if(isset($_SESSION['start']) && !empty($_SESSION['start'])) {
		} else {
			$start=$_POST['start_date'];
			$_SESSION['start'] = $start;
		}*/		
		//$_SESSION['start'] = $start;
		//$_SESSION['end'] = $end;
		list($a,$b,$c)=explode('~',$_POST['unit_type']);
		$hotel_id = $this->session->userdata('user_hotel');     
        $data['resource'] = $a;
		$data['do'] = $b;
		$data['mo'] = $c;
		$data['number_of_room'] = $_POST['number_of_room'];
        //$data['rooms'] = $this->bookings_model->room_hotelwise($hotel_id,118);
        $data['events'] = $this->dashboard_model->all_events_limit();
        $data['taxes'] = $this->bookings_model->room_tax($hotel_id);
        $data['times']=$this->dashboard_model->checkin_time();
		$data['tax'] = $this->bookings_model->service_tax($hotel_id);
        $data['start'] = $_POST['start_date'];
        $data['end'] = $_POST['end_date'];
		$data['heading']='Enquiry';
		$data['sub_heading']='Enquiry';
		$data['description']='Enquiry';
		$data['active']='enquiry';
		$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
	    $data['vendor'] = $this->dashboard_model->all_vendor();		
		$data['page'] = 'backend/dashboard/enquery';
		$this->load->view('backend/common/index', $data);
    }
	   
    function hotel_backend_create()
    {
        //checkin time logic
        $times=$this->dashboard_model->checkin_time();
        foreach($times as $time) {
            if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
                $modifier = ($time->hotel_check_in_time_hr + 12);
            }
            else{
                $modifier = ($time->hotel_check_in_time_hr);
            }
        }

        $times2=$this->dashboard_model->checkout_time();
        foreach($times2 as $time2) {
            if($time->hotel_check_out_time_fr=='PM' && $time->hotel_check_out_time_hr !="12") {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12);
            }
            else{
                $modifier2 = ($time->hotel_check_out_time_hr);
            }
        }

        $id2=$this->input->post('id_guest');
        $id = $this->session->userdata('user_id');
        /*$data1 = $this->dashboard_model->admin_status($id);
            if(isset($data1) && $data1){
                $hotel_id = $data1->admin_hotel;
                }else{
                $hotel_id = '';
                }*/
        $hotel_id = $this->session->userdata('user_hotel');         
        //date_default_timezone_set("Asia/Kolkata");
        $start_time = $this->input->post('start_time');
        $end_time = $this->input->post('end_time');
       /* if($end_time<$modifier){
            exit();
        }*/

        $start_hour = round($start_time);
        $end_hour = round($end_time);
        
        $final_start_hour = $start_hour - $modifier;
        $final_end_hour = $end_hour - $modifier;
        
        $final_start_time = $final_start_hour.":00:00";
        $final_end_time = $final_end_hour.":00:00";
        
        $checkin_time = date("H:i:s", strtotime($start_time));
        $checkout_time = date("H:i:s", strtotime($end_time));
        
        /*$cust_from_date = date('Y-m-d',strtotime($this->input->post('start_dt')));
        $cust_end_date = date('Y-m-d',strtotime($this->input->post('end_dt')));*/
        
        $cust_from_date = date('Y-m-d',strtotime($this->input->post('start_dt')))."T".date("H:i:s", strtotime($final_start_time));
        $cust_end_date = date('Y-m-d',strtotime($this->input->post('end_dt')))."T".date("H:i:s", strtotime($final_end_time));
        
		/*Subha's calculation */
        $from_date = date('Y-m-d',strtotime($this->input->post('start_dt')))."T".date("H:i:s", strtotime($start_time));
        $from_date_final=date('Y-m-d H:i:s', strtotime($from_date) - 60 * 60 * $modifier);

        $end_date = date('Y-m-d',strtotime($this->input->post('end_dt')))."T".date("H:i:s", strtotime($end_time));
        $end_date_final=date('Y-m-d H:i:s', strtotime($end_date) - 60 * 60 * $modifier2);

        date_default_timezone_set('America/Los_Angeles');


        // if($from_date < date("Y-m-d H:i:s") )
        
        
        /*$today = date("Y-m-d");
        $current_date = date('Y-m-d',strtotime($this->input->post('start_dt')));
        if($today == $current_date)
        { $booking_status_id = 5; }
        else
        { $booking_status_id = 2; }*/
        $booking_status_id = 7;
		if($this->input->post('cust_full_address') != "")
		{
			$address_array=explode(",",$this->input->post('cust_full_address'));
			$cust_full_address=$this->input->post('cust_full_address');
			$g_city=$address_array[0];
			$g_state=$address_array[1];
			$g_country =$address_array[2];
		}
		else
		{
			$cust_full_address=" ";
			$g_city=" ";
			$g_state=" ";
			$g_country =" ";
		}
        $guest = array(
                //'g_id' => $this->input->post('id_guest'),
                'hotel_id' => $hotel_id,
                'g_name' => $this->input->post('cust_name'),
                //'g_place' => $this->input->post('g_place'),
                'g_pincode' => $this->input->post('cust_address'),
				'g_address' =>$cust_full_address,
				'g_city' =>$g_city,
				'g_state'=>$g_state,
				'g_country' =>$g_country,
                'g_contact_no' => $this->input->post('cust_contact_no'),
				'g_email' => $this->input->post('cust_mail'),
                //'g_gender' => $this->input->post('g_gender'),
                //'g_dob' => $this->input->post('g_dob'),
                //'g_occupation' => $this->input->post('g_occupation'),
                //'g_married' => $this->input->post('g_married'),
                //'g_id_type' => $this->input->post('g_id_type'),
                //'g_photo' => '',
                //'g_id_proof' => '',
            );                    
           
            if($id2==""){
                
                $guest_id = $this->bookings_model->add_guest($guest);
            }
            else{
                $guest_id=$id2;
            }       
                $maxID="select max(booking_id) as MID from hotel_bookings_enq";
				$query=$this->db1->query($maxID);
				$a=$query->row();
				//$a=$a+1;
		$b=$a->MID;
		$b=$b+1;
		// ...
                
                $new_book = array(
				'booking_id'=>$b,
                'hotel_id' => $hotel_id,
                'user_id' => $id,
                'room_id' => $this->input->post('room_id'),
                'guest_id' => $guest_id,
                'cust_name' => $this->input->post('cust_name'),
                'cust_address' => $this->input->post('cust_address'),
                'cust_contact_no' => $this->input->post('cust_contact_no'),
                'cust_from_date' => $from_date_final, //$cust_from_date,
                'cust_end_date' => $end_date_final,
                'cust_from_date_actual' => date("Y-m-d",strtotime($this->input->post('start_dt'))),
                'cust_end_date_actual' =>  date("Y-m-d",strtotime($this->input->post('end_dt'))) ,//$cust_end_date,
                'checkin_time' => $checkin_time,
                'confirmed_checkin_time' => $checkin_time,
                'checkout_time' => $checkout_time,
                'confirmed_checkout_time' => $checkout_time,
                'nature_visit' => $this->input->post('nature_visit'),
                'next_destination' => $this->input->post('next_destination'),
				'cust_coming_from' => $this->input->post('coming_from'),
                'booking_status_id' => $booking_status_id,
                'p_center' =>$this->input->post('p_center'),
				'marketing_personnel' =>$this->input->post('marketing_personnel'),
				'number_of_room' =>$this->input->post('number_of_room'),
                //'cust_payment_initial' => $this->input->post('cust_payment_initial')
            );
                $bookings_id = $this->bookings_model->add_new_room_enq($new_book);
            
            
            /*if (isset($query) && $query) {
                $this->session->set_flashdata('succ_msg', "Reservation Added Successfully!");
                redirect('dashboard/add_booking_calendar');
            } else {
                $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                redirect('dashboard/add_booking_calendar');
            }*/
            //$response = new Result();
            $data = array(
                'bookings_id' => $bookings_id,
                'guest_name' =>  $this->input->post('cust_name'),
                );
            
            header('Content-Type: application/json');
            echo json_encode($data);
    }

    function hotel_backend_create2()
    {
        
        /*$id = $this->session->userdata('user_id');
        $data1 = $this->dashboard_model->admin_status($id);
            if(isset($data1) && $data1){
                $hotel_id = $data1->admin_hotel;
                }else{
                $hotel_id = '';
                }*/		
			date_default_timezone_set('Asia/Kolkata');
			$perc = $this->input->post('perc1');
			$mod = $this->input->post('modf_rr');
			/*if($perc = 1){
				$mod = $this->input->post('base_room_rent1') * $mod / 100;
			}*/
			$perc2 = $this->input->post('m_perc1');
			$m_mod = $this->input->post('modf_mp');
			/*if($perc2 = "1"){
				$m_mod = $this->input->post('plan_price') * $m_mod / 100;
			}*/
			
            $bookings_id = $this->input->post('booking_1st');
            $hotel_id = $this->session->userdata('user_hotel'); 
            $no_of_guest = $this->input->post('adult') + $this->input->post('child');
            $booking_type = $this->input->post('booking_type');
            $checkin_date = '';
			$checkin_time = '';
            if($booking_type == 'current')
            {
                $booking_status_id = 5;
				$checkin_date = date("Y:m:d");
				$checkin_time = date("H:i:s");
				 $new_book = array(
									'booking_id' => $bookings_id,
									'checkin_date' => $checkin_date,
									'checkin_time' => $checkin_time
				);
				$query = $this->bookings_model->add_new_room2($new_book);
            }
            else if($booking_type=='temporaly'){
                $booking_status_id = 1;
            }
            else
            {
                $booking_status_id = 2; //Advance Booking
            }

        

        /*
        $rent=$this->input->post('booking_rent');
        $room_id=$this->input->post('room_id');
        $rents=$this->bookings_model->rate($room_id);

        foreach($rents as $r){

        if($rent=="weekend"){

            $base_room_rent=$r->room_rent_weekend;

        }
        else if($rent=="seasonal"){

            $base_room_rent=$r->room_rent_seasonal;

        }
        else{

            $base_room_rent=$r->room_rent;
        }
        }*/
            
            $new_book = array(
                'booking_id' => $bookings_id,
                'booking_id_actual' => "BK0".$this->session->userdata('user_hotel')."-".$this->session->userdata('user_id')."/".date("d").date("m").date("y")."/".$bookings_id,
                'no_of_guest' => $no_of_guest,
                'no_of_adult' => $this->input->post('adult'),
                'no_of_child' => $this->input->post('child'),
                'booking_source' => $this->input->post('booking_source'),
                'base_room_rent' => $this->input->post('base_room_rent'),
                'unit_room_rent' => $this->input->post('unit_room_rent'),
                'rent_mode_type' => $this->input->post('rent_mode_type'),
                'room_rent_total_amount' => $this->input->post('base_room_rent')*$this->input->post('stay_span'),
                'stay_days' => $this->input->post('stay_span'),
                'mod_room_rent' => $mod,
                'booking_status_id' => $booking_status_id,
                'comment' => $this->input->post('comment'),
                'booking_taking_time' => date("H:i"),
                'booking_taking_date' => date("Y-m-d"),
                'food_plans' => $this->input->post('plan_id'),
                'food_plan_price' => $this->input->post('plan_tprice'),
                'food_plan_mod_type' => $this->input->post('m_rent_mode_type'),
                'food_plan_mod_rent' => $m_mod,
                'food_plan_uprice' => $this->input->post('plan_price1'),
                'ex_per_uprice' => $this->input->post('ex_u_price'),
                'ex_per_u_mprice' => $this->input->post('ex_u_mprice'),
                'charge_name' => $this->input->post('booking_rent'),
                
            );
			
			
            $query = $this->bookings_model->add_new_room2_enq($new_book);
            
            
            /*$source = $this->bookings_model->get_booking_details($bookings_id);
            foreach($source as $row)
                {
                    $country = $row->area_0;
                }*/
            
            
            $data = array(
                'bookings_id' => $bookings_id,
                'booking_source' => $this->input->post('booking_source')
                );
            
            

            header('Content-Type: application/json');
            echo json_encode($data);
    }
    
    
    function hotel_backend_create4()
    {
        	date_default_timezone_set('Asia/Kolkata');
            $bookings_id = $this->input->post('booking_3rd');
            $rmNo = $this->input->post('booking_rm_no');
            $hotel_id = $this->session->userdata('user_hotel'); 
            $user_id = $this->session->userdata('user_id'); 
             $datetime = date("Y/m/d")." ". date("H:i:s");
            $select_bookings = $this->bookings_model->get_booking_details($bookings_id);
            foreach($select_bookings as $row)
            {
                $booking_status_id_pre = $row->booking_status_id;
                $cust_name=$row->cust_name;
                $cust_contact_no=$row->cust_contact_no;
                $cust_destination=$row->next_destination;
                $cust_from=$row->cust_from_date;
                $cust_end=$row->cust_end_date;
                $cust_total=$row->room_rent_sum_total;
            }

        if( $this->input->post('t_amount')!='') {
            if ($booking_status_id_pre == 2 || $booking_status_id_pre == 8) {
                $booking_status_id = 4;
            } else {
                $booking_status_id = 5;
            }
        }
        else{
            $booking_status_id =$booking_status_id_pre;
        }
            
            $new_book1 = array(
                'booking_id' => $bookings_id,
                'booking_status_id' => $booking_status_id
            );
            $query = $this->bookings_model->add_new_room2($new_book1);
          
            $new_book = array(
                't_booking_id' => $bookings_id,
                't_date' => $datetime,
                't_payment_mode' => $this->input->post('t_payment_mode'),
                'transaction_type' => 'Booking Payment',
                'p_center' => $this->input->post('p_center'),
                't_bank_name' => $this->input->post('t_bank_name'),
                't_amount' => $this->input->post('t_amount'),
                'transaction_from_id' => 2,
                'transaction_to_id' => 4,
                'transaction_releted_t_id' => 1,
                'user_id' => $user_id,
                'transactions_detail_notes' =>"( Booking Id: HM0".$hotel_id."00".$bookings_id." Room No : ".$rmNo." ". $name." From - ". $cust_from." To - ".$cust_end." )"	
               
            );
            $query = $this->bookings_model->add_new_room4($new_book);
            
			//update cashdrawer 
			$amount=$this->input->post('t_amount');
			$cash= $this->unit_class_model->get_closing_stock();
			$c=$cash->clossing_cash;//get clossing cash
			$cash_collection=$cash->cash_collection;//get collect cash
			$mode=$this->input->post('t_payment_mode');
			if($mode=='cash' || $mode=='Cash'){
			$query1=$this->unit_class_model->update_cash_collection($amount,$c,$cash_collection);
			}
			else{
						$cash= $this->unit_class_model->get_mode_cash($mode);
						$d=$cash->$mode;//get mode cash	
							//$c+=$amount;
							$d+=$amount;
						$query1=$this->unit_class_model->update_cash_drawer_mode($d,$mode);
						}
			
			
			
			
			
             $new_book2 = array(
                'booking_id' => $bookings_id,
				'room_rent_tax_details' => $this->input->post('booking_tax_types'),
                'room_rent_sum_total' => $this->input->post('total'),
                'room_rent_tax_amount' => $this->input->post('tax'),
                 'service_tax' => $this->input->post('servicet'),
                  'service_charge' => $this->input->post('servicec'),
                   'luxury_tax' => $this->input->post('luxuryt'),
            );
            $query = $this->bookings_model->add_new_room5_enq($new_book2);
        $select_bookings2 = $this->bookings_model->get_booking_details($bookings_id);
        foreach($select_bookings2 as $row2)
        {
            $booking_status_id_pre = $row2->booking_status_id;
            $cust_name = $row2->cust_name;
            $cust_contact_no = $row2->cust_contact_no;
            $cust_destination = $row2->next_destination;
            $cust_from = $row2->cust_from_date;
            $cust_end = $row2->cust_end_date;
            $cust_total = $row2->room_rent_total_amount;
        }

        $this->load->model('dashboard_model');
        $hotel_name = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $hot_name=$hotel_name->hotel_name;

        $text=$cust_name." FROM ".$cust_destination." BOOKED ".$hot_name."<br> FROM:".$cust_from." UPTO: ".$cust_end." ,TOTAL AMOUNT:".$cust_total;
        $text_guest="BOOKING IN ".$hot_name." SUCCESSFULL !! NAME:".$cust_name." FROM: ".$cust_destination." BOOKED FROM:".$cust_from." TO: ".$cust_end." ,TOTAL AMOUNT:".$cust_total;
        $this->load->model('bookings_model');
        $this->bookings_model->send_sms($text);
        $this->bookings_model->send_sms_guest($cust_contact_no,$text_guest);



        $data = array(
                'bookings_id' => $bookings_id,
                );

            header('Content-Type: application/json');
            echo json_encode($data);
    }

    function hotel_backend_create5(){
        $bookings_id = $this->input->post('booking_3rd');
        
        
        $new_book2 = array(
                'booking_id' => $bookings_id,
                'room_rent_sum_total' => $this->input->post('total'),
				'room_rent_tax_details' => $this->input->post('booking_tax_types'),
                'room_rent_tax_amount' => $this->input->post('tax'),
				'service_tax' => $this->input->post('servicet'),
                  'service_charge' => $this->input->post('servicec'),
                   'luxury_tax' => $this->input->post('luxuryt'),
                   'food_plan_tax' => $this->input->post('plan_m_tax'),
                   'ex_per_price' => $this->input->post('ex_per_price'),
                   'ex_per_tax' => $this->input->post('ex_per_tax'),
                   'ex_per_m_price' => $this->input->post('ex_per_m_price'),
                   'ex_per_m_tax' => $this->input->post('ex_per_m_tax'),
                   'ex_ad_no' => $this->input->post('extra_ad_no'),
                   'ex_ch_no' => $this->input->post('extra_ch_no'),
               
            );
		
			
            $query = $this->bookings_model->add_new_room5_enq($new_book2);
            $new_book5 = array(
                'booking_id' => $bookings_id,
                'room_rent_sum_total' => $this->input->post('room_rent_sum_total_5'),
				'room_rent_total_amount' => $this->input->post('room_rent_total_amount_5'),
                'room_rent_tax_amount' => $this->input->post('room_rent_tax_amount_5'),
				'ex_per_price' => $this->input->post('ex_per_price_5'),
                'ex_per_tax' => $this->input->post('ex_per_tax_5'),
                'ex_per_m_price' => $this->input->post('ex_per_m_price_5'),
                'ex_per_m_tax' => $this->input->post('ex_per_m_tax_5'),
                'food_plan_price' => $this->input->post('food_plan_price_5'),
                'food_plan_tax' => $this->input->post('food_plan_tax_5'),
               
            );	
           // print_r( $new_book5 ); exit;
            $query = $this->bookings_model->add_new_room5_enq($new_book5);
        $select_bookings2 = $this->bookings_model->get_booking_details_enq($bookings_id);
        foreach($select_bookings2 as $row2)
        {
            $booking_status_id_pre = $row2->booking_status_id;
            $cust_name=$row2->cust_name;
            $cust_contact_no=$row2->cust_contact_no;
            $cust_destination=$row2->next_destination;
            $cust_from=$row2->cust_from_date;
            $cust_end=$row2->cust_end_date;
            $cust_total=$row2->room_rent_total_amount + $row2->room_rent_tax_amount;
        }
        $this->load->model('dashboard_model');
        $hotel_name = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $hot_name=$hotel_name->hotel_name;
        $text=$cust_name." FROM ".$cust_destination." BOOKED ".$hot_name." FROM:".$cust_from." UPTO: ".$cust_end." ,TOTAL AMOUNT:".$cust_total;
        $text_guest="BOOKING IN ".$hot_name." SUCCESSFULL !! NAME:".$cust_name." FROM: ".$cust_destination." BOOKED FROM:".$cust_from." TO: ".$cust_end." ,TOTAL AMOUNT:".$cust_total;
        $this->load->model('bookings_model');
        //$this->bookings_model->send_sms($text);
        //$this->bookings_model->send_sms_guest($cust_contact_no,$text_guest);
            
            
            
            $data = array(
                'bookings_id' => $bookings_id
                );
            
            

            header('Content-Type: application/json');
            echo json_encode($data);
    }


	function mailEnquiry($bid){
        $data['heading']='Enquiry';
        $data['sub_heading']='Enquiry';
        $data['description']='Enquiry';
        $data['active']='enquiry';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['bid']= $bid;
        $data['bd'] = $this->bookings_model->get_booking_details_enq($bid); 		
		$data['page'] = 'backend/dashboard/mail_enquery';
		$this->load->view('backend/common/index', $data);
	}

    function send_mail($bid){
		 $hotel_name = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		 $bd = $this->bookings_model->get_booking_details_enq($bid);
		foreach($bd as $bd){
	//	print_r($bd); exit;
    	$guest_mailId=$this->bookings_model->getG_mailId($bd->guest_id);
    	//echo $guest_mailId->g_email; exit;
		$val = $this->dashboard_model->get_unit_exact($bd->room_id);
		$fd = $this->dashboard_model->get_meal_plan_by_id($bd->food_plans);
		}	
         $subject = "Mail from ".$hotel_name->hotel_name; 
         $header = "From:hotelobjectsapps@gmail.com \r\n";
         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";
		 $mail = $guest_mailId->g_email;
        if($bd->rent_mode_type == "add")
		{$vb = '(+)';} else { $vb = '(-)'; }
        if($bd->food_plan_mod_type == 'add')
		{$asp = '(+)';} else { $asp ='(-)'; }	
		$msg = '<table width="100%">
		  <tr>
			<td width="30%" align="left" valign="top"><br>
				<strong>Name : </strong>'.$bd->cust_name.'
			</td>
			<td width="40%" align="left" valign="top">&nbsp;</td>
			<td width="30%" align="left" valign="top"><br><strong>'.$hotel_name->hotel_name.'</strong><br>
			  <strong>DATE: </strong>'.$bd->booking_taking_date.'</td>
		  </tr>
		</table>    <span style="font-size:13px; font-weight:700; color:#009595; display:block; padding:5px 5px;">Booking Details</span>
    <hr style="background: #009595; border: none; height: 1px;">
    <table width="100%" class="table table-bordered table-hover">
    <thead>
      <tr style="background: #EDEDED; color: #1b1b1b">
        <th style="text-align:center"> Product / Service Name </th>
        <th style="text-align:center"> No of Day(s) </th>
		<th style="text-align:center"> No of Room(s) </th>
        <th style="text-align:center"> Unit Price </th>
        <th style="text-align:center"> Modifier </th>
        <th style="text-align:center"> Amount </th>
        <th style="text-align:center"> Total Tax </th>
        <th style="text-align:right"> Total </th>
      </tr>
    </thead>
    <tbody style="background: #fafafa">
    	<tr>
        <td align="center" valign="middle">'.$val->unit_name.'<br>
		<strong>From Date : </strong>'.$bd->cust_from_date_actual.'<br><strong>To Date : </strong>'.$bd->cust_end_date_actual.'
		</td>
        <td align="center" valign="middle">'.$bd->stay_days.'</td>
		<td align="center" valign="middle">'.$bd->number_of_room.'</td>
        <td align="center" valign="middle">'.$bd->unit_room_rent.'</td>
        <td align="center" valign="middle">'.$vb.' '.$bd->mod_room_rent.'</td>
        <td align="center" valign="middle">'.$bd->room_rent_total_amount * $bd->number_of_room.'</td>
        <td align="center" valign="middle">'.$bd->room_rent_tax_amount * $bd->number_of_room.'</td>
        <td align="right" valign="middle">'.($bd->room_rent_total_amount + $bd->room_rent_tax_amount) * $bd->number_of_room.'</td>
        </tr>
    	<tr>
        <td align="center" valign="middle"><strong>Meal Plan</strong><br>'.$fd->name.'
		</td>
        <td align="center" valign="middle">'.$bd->stay_days.'</td>
		<td align="center" valign="middle">'.$bd->number_of_room.'</td>
        <td align="center" valign="middle">'.$bd->food_plan_uprice.'</td>
        <td align="center" valign="middle">'.$asp.' '.$bd->food_plan_mod_rent.'</td>
        <td align="center" valign="middle">'.$bd->food_plan_price * $bd->number_of_room.'</td>
        <td align="center" valign="middle">'.$bd->food_plan_tax * $bd->number_of_room.'</td>
        <td align="right" valign="middle">'.($bd->food_plan_price + $bd->food_plan_tax) * $bd->number_of_room.'</td>
        </tr>		
        <tr>
        	<td align="right" valign="middle" colspan="8">&nbsp;</td>
        </tr>
        <tr>
      	<td align="right" valign="middle" colspan="7">Extra Person Room Rent: </td>
        <td align="right" valign="middle">'.$bd->ex_per_price * $bd->number_of_room.'</td>
        </tr>
        <tr>
      	<td align="right" valign="middle" colspan="7">Extra Person Room Rent tax: </td>
        <td align="right" valign="middle">'.$bd->ex_per_tax * $bd->number_of_room.'</td>
        </tr>
        <tr>
      	<td align="right" valign="middle" colspan="7">Extra Person meal Plan Price: </td>
        <td align="right" valign="middle">'.$bd->ex_per_m_price * $bd->number_of_room.'</td>
        </tr>
        <tr>
      	<td align="right" valign="middle" colspan="7">Extra Person meal Plan Tax: </td>
        <td align="right" valign="middle">'.$bd->ex_per_m_tax * $bd->number_of_room.'</td>
        </tr>		
        <tr>
      	<td align="right" valign="middle" colspan="7"><strong>Booking Grand Total:</strong> </td>
        <td align="right" valign="middle">INR '.$bd->room_rent_sum_total * $bd->number_of_room.'</td>
        </tr>
        </tbody>
        </table>';		 
	//	echo $msg;
       @mail($mail,$subject,$msg,$header);		
		redirect('enquiry');
	}






















	

    /* end  */
}
    




?>