<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Human_resource extends CI_Controller
{

	function __construct()
    {
        /* Session Checking Start*/
		
        parent::__construct();
		 $db['superadmin'] = array(
			'dsn'   => '',
			'hostname' => 'localhost',
			'username' => $this->session->userdata('username'),
			'password' => $this->session->userdata('password'),
			'database' => $this->session->userdata('database'),
			'dbdriver' => 'mysqli',
			'dbprefix' => '',
			'pconnect' => FALSE,
			'db_debug' => TRUE,
			'cache_on' => FALSE,
			'cachedir' => '',
			'char_set' => 'utf8',
			'dbcollat' => 'utf8_general_ci',
			'swap_pre' => '',
			'encrypt' => FALSE,
			'compress' => FALSE,
			'stricton' => FALSE,
			'failover' => array(),
			'save_queries' => TRUE 
		);

        //echo "<pre>";
		//	echo $this->db1->database;
		//    print_r($db1['default']);exit;
		
            $CI = &get_instance();
        $this->db1 = $CI->load->database($db['superadmin'],true);
		$this->arraay =$this->session->userdata('per');
		$this->load->model('unit_class_model');
		$this->load->model('bookings_model');
		$this->load->library('upload');
        if (!$this->session->userdata('is_logged_in')) {
            redirect('login');
        }
        /* Session Checking End*/

        /* URL Value Encryption Start */
        function base64url_encode($data)
        {
            return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
        }

        /* URL Value Encryption End */

        /* URL Value Decryption Start */
        function base64url_decode($data)
        {
            return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
        }
        /* URL Value Decryption End */
    }

	
	function index(){
		
		 $found = 1;
		if($found){
				 $data['heading'] = 'Human Resource Account';
				$data['sub_heading'] = 'Human Resource';
				$data['description'] = 'Add Human Resource Here';
				$data['active'] = 'misc_settings';
				$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
				$data['page'] = 'backend/dashboard/add_human_resource';
                $this->load->view('backend/common/index', $data);
	}
	
}

}

    ?>