<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Night_Audit_controller extends CI_Controller
{

	function __construct()
    {
        /* Session Checking Start*/
		
        parent::__construct();
		
		$this->arraay =$this->session->userdata('per');
		$this->load->model('unit_class_model');
		$this->load->model('bookings_model');
		$this->load->library('upload');
        if (!$this->session->userdata('is_logged_in')) {
            redirect('login');
        }
        /* Session Checking End*/

        /* URL Value Encryption Start */
        function base64url_encode($data)
        {
            return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
        }

        /* URL Value Encryption End */

        /* URL Value Decryption Start */
        function base64url_decode($data)
        {
            return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
        }
        /* URL Value Decryption End */
    }

	
function all_night_audit()
    {

			 $found = in_array("25",$this->arraay);
		if(1) {
            $data['heading'] = 'Audit';
            $data['sub_heading'] = 'All Night Audit';
            $data['description'] = 'All Night Audit ';
            $data['active'] = 'setting';
			$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			
			$data['nightAudit'] = $this->unit_class_model->all_night_audit();
            //$data['transactions'] = $this->dashboard_model->all_transactions();
            

            $data['page'] = 'backend/dashboard/all_night_audit';
            $this->load->view('backend/common/index', $data);

        } else {
            redirect('dashboard');
        }
    }
 function night_audit_settings(){
		$found = in_array("7",$this->arraay);
		if(1)
		{
        $data['heading'] = 'Night Audit Settings';
        $data['sub_heading'] = 'Night Audit Settings';
        $data['description'] = '';
        $data['active'] = 'setting';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
       if($this->input->post()){
		$chk6=$this->input->post('chk6');
		$chk7=$this->input->post('chk7');
		$time=$this->input->post('time');
		$Email_to=$this->input->post('Email_to');
		
	if($chk6=='on'){
		$chk6='1';
}else{
$chk6='0';
}
	if($chk7=='on'){
		$chk7='1';
}else{
$chk7='0';
}
		$hotel_id=$this->session->userdata('user_hotel');		
		$user_id=$this->session->userdata('user_id');	
	$data1=array(
	'night_audit_due_time'=>$time,
	'Email_to'=>$Email_to,
	'Checkin_Checkout_status'=>$chk6,
	'toggle'=>$chk7,
	'hotel_id'=>$hotel_id,
	'user_id'=>$user_id,

	);
$data1= $this->unit_class_model->update_night_audit_set($data1);
//echo "<pre>";
//print_r($data);exit;
}
        $data['page'] = 'backend/dashboard/night_audit_settings';
        $this->load->view('backend/common/index', $data);
		}
		else
		{
			redirect('dashboard');
		}

    }
	

}



    ?>