<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rate_plan extends CI_Controller
{

	public function __construct(){
        parent::__construct();if (!$this->session->userdata('is_logged_in')) {            redirect('login');        }
		
		 $db['superadmin'] = array(
    'dsn'   => '',
    'hostname' => 'localhost',
    'username' => $this->session->userdata('username'),
    'password' => $this->session->userdata('password'),
    'database' => $this->session->userdata('database'),
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => TRUE,
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);
        //echo "<pre>";
       //    print_r($db['default']);exit;
            $CI = &get_instance();
        $this->db1 = $CI->load->database($db['superadmin'],true);
		$this->arraay =$this->session->userdata('per');
		$this->load->model('rate_plan_model');
        $this->load->model('dashboard_model');
		$this->load->model('Unit_class_model');
		$this->load->model('setting_model');
		
		if (!$this->session->userdata('is_logged_in')) {
            redirect('login');
        }
		
    }

   function all_rate_plan(){
	    $found = in_array("148",$this->arraay);
		  if($found)
		    {
			$data['heading'] = 'Setting';
            $data['sub_heading'] = 'Rate Plan';
			$data['subs_heading'] = '';
            $data['description'] = 'Edit Rate Plan here';
			$data['active'] = 'setting';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['rates']=$this->rate_plan_model->all_rate_plan();
			$data['page'] = 'backend/dashboard/all_rate_plan';
            $this->load->view('backend/common/index', $data);
   }else{
	   redirect('dashboard');
   }
   }
	
	
   function add_rate_plan(){
	  
	  
	 // $found = in_array("145",$this->arraay);
	 $found = 1;
		  if($found)
		    {
				
	$query = $this->rate_plan_model->add_rate_plan();
   }else{
	   redirect('dashboard');
   }	
   }
   
   function edit_rate_plan(){
	  
	  
	 // $found = in_array("145",$this->arraay);
	 $found = 1;
		  if($found)
		    {
			//	echo "Abhishek";
	$query = $this->rate_plan_model->edit_rate_plan();
   }else{
	   redirect('dashboard');
   }	
   }
	
function get_rate_plan(){
	 $found = in_array("145",$this->arraay);
		  if($found)
		    {
	$s_id=$_POST['s_id'];
	$id=$_POST['p_id'];
	//$s_id=1;
	//$id=2;
	//$query=$this->rate_plan_model->get_rate_plan($s_id,$id);
	$out_put='<table class="table table-striped table-hover table-bordered mass-book" id="sample_3">
              <thead>
                <tr style="color:#F8681A;">
                  <th rowspan="2" width="1%" style="background-position: right 67px; padding: 58px 10px 10px; background-color:#eeeeee !important; color:#333333; !important"> # </th>
                  <th rowspan="2" style="background-position: right 67px; padding: 58px 10px 10px; background-color:#eeeeee !important; color:#333333; !important"> Unit Type </th>
                  <th rowspan="2" scope="col" style="background-position: right 67px; padding: 58px 10px 10px; background-color:#eeeeee !important; color:#333333; !important">Meal Plan</th>
                  <th rowspan="2" scope="col" style="background-position: right 67px; padding: 58px 10px 10px; border-right: 1px solid #dddddd; background-color:#eeeeee !important; color:#333333; !important">Occopancy</th>                
    			  <th scope="col" colspan="4" style="text-align:center; border-right: 1px solid #dddddd; background-color:#eeeeee !important; color:#26c281 !important; font-size:21px;">Base</th>
                  <th scope="col" colspan="4" style="text-align:center; border-right: 1px solid #dddddd; background-color:#eeeeee !important; color:#e43a45 !important; font-size:21px;">Weekend</th>
                  <th scope="col" colspan="4" style="text-align:center; background-color:#eeeeee !important; color:#337ab7 !important; font-size:21px;">Seasonal</th>
                </tr>
                <tr style="background-color: #e7ecf1;color: #333;">
					<th style="color:#26c281 !important;">Room<br/> Rent</th>
                    <th style="color:#26c281 !important;">RR <br/>Tax</th>
                    <th style="color:#26c281 !important;">Meal <br/>Plan</th>
                    <th style="border-right: 1px solid #dddddd;color:#26c281 !important;">MP <br/>Tax</th>
                    <th style="color:#e43a45 !important;">Room <br/>Rent</th>
                    <th style="color:#e43a45 !important;">RR <br/>Tax</th>
                    <th style="color:#e43a45 !important;">Meal <br/>Plan</th>
                    <th style="border-right: 1px solid #dddddd;color:#e43a45 !important;">MP <br/>Tax</th>
                    <th style="color:#337ab7 !important;">Room <br/>Rent</th>
                    <th style="color:#337ab7 !important;">RR <br/>Tax</th>
                    <th style="color:#337ab7 !important;">Meal <br/>Plan</th>
                    <th style="color:#337ab7 !important;">MP <br/>Tax</th>
                 </tr>
              </thead>
              <tbody>';
				
				$data = $this->rate_plan_model->get_rate_plan($id,$s_id);
				if(isset($data) && $data){
						$sl_no=0;
						$put = 'jtyygjytg';
						$put1 = '';
						$pmp = '';
			foreach($data as $dt){
						$sl_no++;
					$out_put.='<tr>';
					$out_put.='<td>'.$sl_no.'</td>';
					$out_put.='<td>';
					if($put != $dt->unit_type_name){
						$out_put.='<span style="font-size:15px; font-weight:bold">'.$dt->unit_type_name.'</span>';
						$put = $dt->unit_type_name;
					}
					
					$out_put.='</td>';
					/*if($put == $put1)
						$put1 = '';
					$put1 = $dt->unit_type_name;*/
				//$out_put.='<td><span style="font-size:15px; font-weight:bold">'.$put1.'</span></td>';
				$out_put.='<td>';		
				  if($pmp != $dt->meal_plan){
					  $out_put.='<span style="font-size:13px;">'.$dt->meal_plan.'</span>';
                    $pmp = $dt->meal_plan;
				  }
				  $out_put.='</td>';
					
				  
					
					$out_put.='<td><span style="font-size:12px">'.$dt->occupancy_type.'</span></td>';			
				 $out_put.='<td>';
				$out_put.='<input type="text" id="br_c'.$dt->rate_plan_id.'" class="form-control input-sm" value="'.$dt->b_room_charge.'" onblur="update_plan('.$dt->rate_plan_id.')" maxlength="10" onkeypress=" return onlyNos(event, this);">';
				 $out_put.='</td>';
                 $out_put.='<td>';
				$out_put.='<input type="text" id="br_ct'.$dt->rate_plan_id.'" class="form-control input-sm" disabled value="'.$dt->b_room_charge_tax.'" onblur="update_plan('.$dt->rate_plan_id.')" maxlength="10" onkeypress=" return onlyNos(event, this);">';
				$out_put.='</td>';
                   $out_put.='<td>';
				   $out_put.='<input type="text" id="bm_c'.$dt->rate_plan_id.'" class="form-control input-sm" value="'.$dt->b_meal_charge.'" onblur="update_plan('.$dt->rate_plan_id.')" maxlength="10" onkeypress=" return onlyNos(event, this);"></td>';
                 $out_put.='<td style="border-right: 1px solid #dddddd;">';
				 $out_put.='<input type="text" id="bm_ct'.$dt->rate_plan_id.'" class="form-control input-sm" disabled value="'.$dt->b_meal_charge_tax.'" onblur="update_plan('.$dt->rate_plan_id.')" maxlength="10" onkeypress=" return onlyNos(event, this);">';
				 $out_put.='</td>';
				 $out_put.='<td>';
				 $out_put.='<input type="text" id="wr_c'.$dt->rate_plan_id.'" class="form-control input-sm" value="'.$dt->w_room_charge.'" onblur="update_plan1('.$dt->rate_plan_id.')" maxlength="10" onkeypress=" return onlyNos(event, this);">';
				 $out_put.='</td>';
                  $out_put.='<td>';
				  $out_put.='<input type="text" id="wr_ct'.$dt->rate_plan_id.'" class="form-control input-sm" disabled value="'.$dt->w_room_charge_tax.'" onblur="update_plan1('.$dt->rate_plan_id.')" maxlength="10" onkeypress=" return onlyNos(event, this);">';
				   $out_put.='</td>';
                  $out_put.='<td>';
				  $out_put.='<input type="text" id="wm_c'.$dt->rate_plan_id.'" class="form-control input-sm" value="'.$dt->w_meal_charge.'" onblur="update_plan1('.$dt->rate_plan_id.')" maxlength="10" onkeypress=" return onlyNos(event, this);">';
				  $out_put.='</td>';
                  $out_put.='<td style="border-right: 1px solid #dddddd;">';
				   $out_put.='<input type="text" id="wm_ct'.$dt->rate_plan_id.'" class="form-control input-sm" disabled onblur="update_plan1('.$dt->rate_plan_id.')" value="'.$dt->w_meal_charge_tax.'" maxlength="10" onkeypress=" return onlyNos(event, this);">';
				   $out_put.='</td>';
                  $out_put.='<td>';
				  $out_put.='<input type="text" id="sr_c'.$dt->rate_plan_id.'" class="form-control input-sm" value="'.$dt->s_room_charge.'" onblur="update_plan2('.$dt->rate_plan_id.')" maxlength="10" onkeypress=" return onlyNos(event, this);">';
				  $out_put.='</td>';
                  $out_put.='<td>';
				  $out_put.='<input type="text" id="sr_ct'.$dt->rate_plan_id.'" class="form-control input-sm" disabled value="'.$dt->s_room_charge_tax.'" onblur="update_plan2('.$dt->rate_plan_id.')" maxlength="10" onkeypress=" return onlyNos(event, this);">';
				  $out_put.='</td>';
                  $out_put.='<td>';
				  $out_put.='<input type="text" id="sm_c'.$dt->rate_plan_id.'" class="form-control input-sm"  value="'.$dt->s_meal_charge.'" onblur="update_plan2('.$dt->rate_plan_id.')" maxlength="10" onkeypress=" return onlyNos(event, this);">';
				  $out_put.='</td>';
                   $out_put.='<td>';
				   $out_put.='<input type="text" id="sm_ct'.$dt->rate_plan_id.'" class="form-control input-sm" disabled value="'.$dt->s_meal_charge_tax.'" onblur="update_plan2('.$dt->rate_plan_id.')" maxlength="10" onkeypress=" return onlyNos(event, this);">';
				   $out_put.='</td>';
				
				 
				
                  
					
                  $out_put.= '</tr>';
				 
}

 $out_put.='</tbody></table>';
				echo $out_put;


}
}else{
	redirect('dashboard');
}
}



function update_rate_plan(){
	$found = in_array("145",$this->arraay);
		  if($found)
		    {
$r_id=$_POST['r_id'];
	$br_c=$_POST['br_c'];
	$br_ct=0;
	$bm_c=$_POST['bm_c'];
	$bm_ct=0;
	
	$data=array(
	'b_room_charge'=>$br_c,
	'b_room_charge_tax'=>$br_ct,
	'b_meal_charge'=>$bm_c,
	'b_meal_charge_tax'=>$bm_ct,
	);
	//print_r($data); exit;
	$query=$this->rate_plan_model->update_rate_plan($r_id,$data);
	$data=array(
	'data'=>success,
	);
	header('Content-Type: application/json');
     echo json_encode($data);
}else{
	redirect('dashboard');
}
}

function update_rate_plan1(){
    $r_id=$_POST['r_id'];
	$wr_c=$_POST['wr_c'];
	$wr_ct=0;
	$wm_c=$_POST['wm_c'];
	$wm_ct=0;
	
	$data=array(
	'w_room_charge'=>$wr_c,
	'w_room_charge_tax'=>$wr_ct,
	'w_meal_charge'=>$wm_c,
	'w_meal_charge_tax'=>$wm_ct,
	);
	//print_r($data); exit;
	$query=$this->rate_plan_model->update_rate_plan($r_id,$data);
	$data=array(
	'data'=>success,
	);
	header('Content-Type: application/json');
     echo json_encode($data);
}

function update_rate_plan2(){
$r_id=$_POST['r_id'];
	$sr_c=$_POST['sr_c'];
	$sr_ct=0;
	$sm_c=$_POST['sm_c'];
	$sm_ct=0;
	
	$data=array(
	's_room_charge'=>$sr_c,
	's_room_charge_tax'=>$sr_ct,
	's_meal_charge'=>$sm_c,
	's_meal_charge_tax'=>$sm_ct,
	);
	//print_r($data); exit;
	$query=$this->rate_plan_model->update_rate_plan($r_id,$data);
	$data=array(
	'data'=>success,
	);
	header('Content-Type: application/json');
     echo json_encode($data);
}

 function rate_plan_settings(){
	 	$found = in_array("145",$this->arraay);
		  if($found)
		    {
	   $data['heading'] = 'Setting';
            $data['sub_heading'] = 'Rate Plan';
			$data['subs_heading'] = '';
            $data['description'] = 'Edit Rate Plan here';
			$data['active'] = 'setting';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['rates']=$this->rate_plan_model->get_all_rate_plan();
			$data['page'] = 'backend/dashboard/rate_plan_settings';
            $this->load->view('backend/common/index', $data);
   }else{
	   redirect('dashboard');
   }
 }
   
   
  function update_r_bplan(){
    $br_c=$_POST['br_c'];
	$br_ct=0;
	$u_id=$_POST['u_id'];
	$m_id=$_POST['m_id'];
	$o_id=$_POST['o_id'];
	//$se_id=$_POST['se_id'];
	//$so_id=$_POST['so_id'];
	
	$data=array(
	'b_room_charge'=>$br_c,
	'b_room_charge_tax'=>$br_ct,
	);
	//print_r($data); exit;
	$query=$this->rate_plan_model->update_r_plan($u_id,$m_id,$o_id,$data);
	
}

function update_r_bmplan(){
    
	$bm_c=$_POST['bm_c1'];
	$bm_ct=0;
	$u_id=$_POST['u_id'];
	$m_id=$_POST['m_id'];
	$o_id=$_POST['o_id'];
	
	
	$data=array(
	'b_meal_charge'=>$bm_c,
	'b_meal_charge_tax'=>$bm_ct,
		
	);
	$query=$this->rate_plan_model->update_r_plan($u_id,$m_id,$o_id,$data);
	
}


function update_r_wrplan(){
	$wr_c=$_POST['wr_c'];
	$wr_ct=0;
	$u_id=$_POST['u_id'];
	$m_id=$_POST['m_id'];
	$o_id=$_POST['o_id'];
	
	$data=array(
	'w_room_charge'=>$wr_c,
	'w_room_charge_tax'=>$wr_ct,
	);

	$query=$this->rate_plan_model->update_r_plan($u_id,$m_id,$o_id,$data);
	/*if($query){
		echo $data1="success";
	}*/
	//echo $wr_c."wtax".$wr_ct."u_id".$u_id."m_id".$m_id."o_id".$o_id;
	
} 

function update_r_wmplan(){
	$wm_c=$_POST['wm_c'];
	$wm_ct=0;
	$u_id=$_POST['u_id'];
	$m_id=$_POST['m_id'];
	$o_id=$_POST['o_id'];
	
	$data=array(
	'w_meal_charge'=>$wm_c,
	'w_meal_charge_tax'=>$wm_ct,
	);

	$query=$this->rate_plan_model->update_r_plan($u_id,$m_id,$o_id,$data);
}

function update_r_srplan(){
	$sr_c=$_POST['sr_c'];
	$sr_ct=0;
	//$sm_c=$_POST['sm_c'];
//	$sm_ct=$_POST['sm_ct'];
	$u_id=$_POST['u_id'];
	$m_id=$_POST['m_id'];
	$o_id=$_POST['o_id'];
	
	$data=array(
	's_room_charge'=>$sr_c,
	's_room_charge_tax'=>$sr_ct,
//	's_meal_charge'=>$sm_c,
//	's_meal_charge_tax'=>$sm_ct,
	);
	//print_r($data); exit;
	$query=$this->rate_plan_model->update_r_plan($u_id,$m_id,$o_id,$data);
	
}


function update_r_smplan(){
	//$sr_c=$_POST['sr_c'];
	//$sr_ct=$_POST['sr_ct'];
	$sm_c=$_POST['sm_c'];
	$sm_ct=0;
	$u_id=$_POST['u_id'];
	$m_id=$_POST['m_id'];
	$o_id=$_POST['o_id'];
	
	$data=array(
	//'s_room_charge'=>$sr_c,
	//'s_room_charge_tax'=>$sr_ct,
	's_meal_charge'=>$sm_c,
    's_meal_charge_tax'=>$sm_ct,
	);
	//print_r($data); exit;
	$query=$this->rate_plan_model->update_r_plan($u_id,$m_id,$o_id,$data);
	
}


   function rate_plan_setting_view(){
	   $found = in_array("145",$this->arraay);
		  if($found)
		    {
	   $data['heading'] = 'Setting';
            $data['sub_heading'] = 'Rate Plan';
			$data['subs_heading'] = '';
            $data['description'] = '';
			$data['active'] = 'setting';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['rates']=$this->rate_plan_model->all_rate_plan();
			$data['page'] = 'backend/dashboard/rate_plan_setting_view';
            $this->load->view('backend/common/index', $data);
            if($this->input->post())
                {   
                	 $room_rent_tax=$this->input->post('room_rent_tax');
                	 $meal_plan_tax=$this->input->post('meal_plan_tax');
                	 $default=$this->input->post('default');

                	 $data=array(
                	 			'total_room_rent_tax'=>$room_rent_tax,
                	 			'total_meal_plan_tax'=>$meal_plan_tax,
                	 			'season_default'=>$default,
                	 			'hotel_id'=>$this->session->userdata('user_hotel'),
								'user_id'=>$this->session->userdata('user_id')
										
                	 	);

                	 $season=$this->input->post('season1');
                	 $start_date=$this->input->post('start_date1');
                	 $end_date=$this->input->post('end_date1');

                	
                	  $data1=array(
                	 			'season_name'=>$season,
                	 			'start'=>$start_date,
                	 			'end'=>$end_date
                	 	);
                	 
                	 $query = $this->rate_plan_model->rate_plan_setting_view($data,$data1);


                	 if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}

						redirect(base_url().'rate_plan/rate_plan_setting_view');
	}

}else{
	redirect('dashboard');
}
   }


function get_season_by_id(){
		$this->load->model('rate_plan_model'); 		
		$id=$this->input->post('id');		
		$query=$this->rate_plan_model->get_season_by_id($id);
			
			 if($query){
				 $data = array(
            'name' =>$query->name,

        );
			 
}
			 
        header('Content-Type: application/json');
        echo json_encode($data);

	}
	
	
	
	function delete_season_mapping_by_id(){
		if(1){
		$id=$this->input->post('id');		
		$query=$this->rate_plan_model->delete_season_mapping_by_id($id);
		if($query){
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}else
		{
			 $data = array(
            'data' =>"You do not have the previlage to do this",

        );
			header('Content-Type: application/json');
        echo json_encode($data);
		}
	}
 }

 
 	function add_season_mapping(){
		$flag=0;
		$this->load->model('unit_class_model');
		
		$season=$this->input->post('season');
		//$start=$this->input->post('start');
		//$end=$this->input->post('end');
		$start=date("y-m-d",strtotime($this->input->post('start')));
		$end=date("y-m-d",strtotime($this->input->post('end')));
		
		$presentDate=$this->rate_plan_model->currentDate();
		
		
		
		//echo $start."<br>".$end;
		//print_r($presentDate);
	//	exit;
		foreach($presentDate as $preDate){
			$start1=$preDate->start_date;
			$end1=$preDate->end_date;
			if($start >= $start1 && $start<=$end1){
				$flag=1;
			} else if($end >= $start1 && $end<=$end1){
				$flag=1;
			} 
			
			
	}  if(flag!=1){
		
		//$where = array('season_name' =>$season,'start_date' =>$start,'end_date' =>$end );
		//$this->rate_plan_model->check_season_by_date($season,$start,$end);
		
		$data=array(

						'season_name'=>$season,
						'start_date'=>$start,
						'end_date'=>$end,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
					);
				//	print_r($data);
				//	exit();
				$query=$this->rate_plan_model->add_season_mapping($data);
	}
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}
        
		redirect(base_url().'rate_plan/rate_plan_setting_view');
	}
	
	function check_season_by_date(){
		
		$season = $this->input->post('seasons');
		//$starts = $this->input->post('starts');
		$start=date("Y-m-d",strtotime($this->input->post('starts')));
		
		//echo $season."----".$start;
		$result = $this->rate_plan_model->check_season_by_condition($season,$start);
		//echo $result;
		//die();
		if($result=="true"){
			
			echo "already defined";
		}
		else{
			echo $result;
		}
	}
	
	
	
	function update_room_rent_tax(){
		$this->load->model('rate_plan_model'); 		
		$room_rent=$this->input->post('room_rent');	
			$data=array(
			'total_room_rent_tax'=>$room_rent
			);
		$query=$this->rate_plan_model->update_room_rent_tax($data);
			
			 if($query){
				 $data = array(
            'rent' =>$room_rent,

        );
			 
}
			 
        header('Content-Type: application/json');
        echo json_encode($data);

	}
	
	
	function update_meal_plan_tax(){
		$this->load->model('rate_plan_model'); 		
		$meal=$this->input->post('meal');	
			$data=array(
			'total_meal_plan_tax'=>$meal
			);
		$query=$this->rate_plan_model->update_meal_plan_tax($data);
			
			 if($query){
				 $data = array(
            'rent' =>$room_rent,

        );
			 
}
			 
        header('Content-Type: application/json');
        echo json_encode($data);

	}
	
	
	function default_season(){
		$this->load->model('rate_plan_model'); 		
		$plan=$this->input->post('plan');	
			$data=array(
			'season_default'=>$plan
			);
		$query=$this->rate_plan_model->default_season($data);
			
			 if($query){
				 $data = array(
            'plan' =>$plan,

        );
			 
}
			 
        header('Content-Type: application/json');
        echo json_encode($data);

	}
	
	
function toggle_applied_tax(){
	$flag=0;
		$this->load->model('rate_plan_model');
		$tax=$this->rate_plan_model->get_applied_tax();
		$a=$tax->tax_applied;
		if($a=='1'){
			$flag='0';
		$data=array(
		'tax_applied'=>'0'
		);
		}else{
			$flag='1';
			$data=array(
		'tax_applied'=>'1'
		);
		}
		
		$query=$this->rate_plan_model->toggle_applied_tax($data);
			
			 if($query){
				 $data = array(
            'status' =>$flag,

        );
			 
}
			 
        header('Content-Type: application/json');
        echo json_encode($data);

	}
	
	
	function toggle_applied_tax_mealplan(){
	$flag=0;
		$this->load->model('rate_plan_model');
		$tax=$this->rate_plan_model->get_applied_tax();
		$a=$tax->tax_applied_mealplan;
		if($a=='1'){
			$flag='0';
		$data=array(
		'tax_applied_mealplan'=>'0',
		'total_meal_plan_tax'=>0
		);
		}else{
			$flag='1';
			$data=array(
		'tax_applied_mealplan'=>'1'
		);
		}
		
		$query=$this->rate_plan_model->toggle_applied_tax($data);
			
			 if($query){
			if($flag){
			$data = array(
            'status' =>$flag,
			'meal'=>'1'
			);
			}else{
				$data = array(
            'status' =>$flag, 
			'meal'=>'0'
			);
			}
       
			 
}
			 
        header('Content-Type: application/json');
        echo json_encode($data);

	}
	
	function default_occupancy(){
	$flag=0;
		$this->load->model('rate_plan_model');
		$tax=$this->rate_plan_model->get_applied_tax();
		$a=$tax->default_occupancy;
		if($a=='1'){
			$flag='0';
		$data=array(
		'default_occupancy'=>'0'
		);
		}else{
			$flag='1';
			$data=array(
		'default_occupancy'=>'1'
		);
		}
		
		$query=$this->rate_plan_model->toggle_applied_tax($data);
			
			 if($query){
				 $data = array(
            'status' =>$flag,

        );
			 
}
			 
        header('Content-Type: application/json');
        echo json_encode($data);

	}
	
	
  function get_season(){
	 
	   $strt_date=date('Y-m-d', strtotime($_POST['start_dt']));
	  // $end_date=date('Y-m-d', strtotime($_POST['end_dt']));
	  $query=$this->rate_plan_model->get_season($strt_date);
	//  print_r($query);exit;
	 if($query){
		   $data=$query->season_name; 
		   $season_id=$this->rate_plan_model->get_season_id($data);
		  echo  $season_id->rate_plan_type_id;
	  }else{
		  echo $data="emty";
	  }
	
  }
  
  function get_unit_type_id(){
	  $room_id=$_POST['room_id'];
	  $unit_type_id=$this->rate_plan_model->get_unit_type_id($room_id);
	  if($unit_type_id){
		  echo $unit_type_id->unit_id;
		 
	  }else{
		  echo " ";
	  }
  }
  function get_unit_type_id1(){
	  $room_id=$_POST['room_id'];
	  $unit_type_id=$this->rate_plan_model->get_unit_type_id1($room_id);
	  if($unit_type_id){
		  echo $unit_type_id->unit_id;
		 
	  }else{
		  echo "fddf".$unit_type_id->unit_id;
	  }
   }
   
   function get_unit_type_id_rm_id(){
	  $room_id=$_POST['room_id'];
	  $unit_type_id=$this->rate_plan_model->get_unit_type_id_rm_id($room_id);
	  if($unit_type_id){
		  echo $unit_type_id->unit_id;
		 
	  }else{
		  echo "fddf".$unit_type_id->unit_id;
	  }
   }
  
   function get_unit_type_id2(){
	  $room_id=$_POST['room_id'];
	  $unit_type=$this->rate_plan_model->get_unit_type_id1($room_id);
	  if($unit_type){
		 $data=array(
		 'unit_id'=>$unit_type->unit_id,
		 'max_occupancy'=>$unit_type->max_occupancy,
		 'room_bed'=>$unit_type->room_bed,
		 );
		 header('Content-Type: application/json');
         echo json_encode($data);
	  }else{
		  echo "fddf".$unit_type_id->unit_id;
	  }
  }
   
function get_meal_plan(){
	$bc=$_POST['bc'];
	$s_id=$_POST['s_id'];
	$o_id=$_POST['o_id'];
	$u_id=$_POST['u_id'];
	
	$output='';
	$meal_plan=$this->rate_plan_model->get_meal_plan($bc,$s_id,$o_id,$u_id);
	if(isset($meal_plan)){
		 $output.='<label>Select Meal Plan</label>';
		$output.='<select name="plan_id" onchange="get_rmCh(this.value)" id="plan_id" class="form-control input-sm" required="required">';
		foreach($meal_plan as $mp){
			$output.='<option value="'.$mp->meal_plan_id.'">'.$mp->meal_plan.'</option>';		
		}
	echo $output.='</select>';
	}
	
} 


function getRateName(){
	
	$food_plans = $this->input->post('mealid');
	$query =   $this->rate_plan_model->fetch_food_plan_name($food_plans);
	if($query){
		
		echo $query->name;
	}
	else{
		
		echo "No plan selected";
	}
	
}

function get_rmCh(){
	
	$chType=$_POST['chType'];
 	 $plan=$_POST['mplan'];
 	
	  
	 $source=$_POST['source'];
	 $season=$_POST['season'];
	 $unitType=$_POST['unitType'];
	 $pax=$_POST['pax'];
	
	//echo $chType.$plan.$source.$season;
	//exit;
	 $query=$this->rate_plan_model->get_rmCh($plan,$source,$season,$unitType,$pax);
	//alert($query->w_room_charge);
	$query1=$this->rate_plan_model->get_exACh($plan,$source,$season,$unitType);
	
	$query2=$this->rate_plan_model->get_exCCh($plan,$source,$season,$unitType);
	
	if($chType == 'w_room_charge'){
		
	$data=array(
	//Weekend
		'room_charge'=>$query->w_room_charge,
		'room_charge_tax'=>$query->w_room_charge_tax,
		'meal_charge'=>$query->w_meal_charge,
		'meal_charge_tax'=>$query->w_meal_charge_tax,
		'room_charge_ea'=>$query1->w_room_charge,
		'room_charge_tax_ea'=>$query1->w_room_charge_tax,
		'meal_charge_ea'=>$query1->w_meal_charge,
		'meal_charge_tax_ea'=>$query1->w_meal_charge_tax,
		'room_charge_ec'=>$query2->w_room_charge,
		'room_charge_tax_ec'=>$query2->w_room_charge_tax,
		'meal_charge_ec'=>$query2->w_meal_charge,
		'meal_charge_tax_ec'=>$query2->w_meal_charge_tax
		);
	}
	else if($chType == 's_room_charge'){
	$data=array(
	//Seasonal
		'room_charge'=>$query->s_room_charge,
		'room_charge_tax'=>$query->s_room_charge_tax,
		'meal_charge'=>$query->s_meal_charge,
		'meal_charge_tax'=>$query->s_meal_charge_tax,
		'room_charge_ea'=>$query1->s_room_charge,
		'room_charge_tax_ea'=>$query1->s_room_charge_tax,
		'meal_charge_ea'=>$query1->s_meal_charge,
		'meal_charge_tax_ea'=>$query1->s_meal_charge_tax,
		'room_charge_ec'=>$query2->s_room_charge,
		'room_charge_tax_ec'=>$query2->s_room_charge_tax,
		'meal_charge_ec'=>$query2->s_meal_charge,
		'meal_charge_tax_ec'=>$query2->s_meal_charge_tax
		);
	}
	else{
		
	$data=array(	
	//Base
		//'room_charge'=>"hello",	
		'room_charge'=>$query->b_room_charge,	
		'room_charge_tax'=>$query->b_room_charge_tax,
		'meal_charge'=>$query->b_meal_charge,
		'meal_charge_tax'=>$query->b_meal_charge_tax,
		'room_charge_ea'=>$query1->b_room_charge,
		'room_charge_tax_ea'=>$query1->b_room_charge_tax,
		'meal_charge_ea'=>$query1->b_meal_charge,
		'meal_charge_tax_ea'=>$query1->b_meal_charge_tax,
		'room_charge_ec'=>$query2->b_room_charge,
		'room_charge_tax_ec'=>$query2->b_room_charge_tax,
		'meal_charge_ec'=>$query2->b_meal_charge,
		'meal_charge_tax_ec'=>$query2->b_meal_charge_tax
		);
	}
	
//	print_r($data);
	header('Content-Type: application/json');
     echo json_encode($data);
} 



function update_laundry_rate(){
	$id=$this->input->post('id');
	$valu=$this->input->post('valu');
	$field_name=$this->input->post('field_name');
		$data=array(
		$field_name=>$valu
		
		);
		$query=$this->rate_plan_model->update_laundry_rate($data,$id);
			
			 if($query){
				 $data1 = array(
            'status' =>'success',

        );
			 
}
			 
        header('Content-Type: application/json');
        echo json_encode($data);

	}



 function get_laundry_rates(){
	$cloth=$this->input->post('cloth');
	$fabric=$this->input->post('fabric');
	$service=$this->input->post('service');
	if($service==1){
		$service='lr_dry_cleaning_rate';
	}else if($service==2){
		$service='lr_laundry_rate';
	}
	else if($service==3){
		$service='lr_ironing_rate';
	}
		
		$query=$this->rate_plan_model->get_laundry_rates($cloth,$fabric,$service);
			
			 if($query){
				 $data1 = array(
            'status' =>'success',
			'price'=>$query->$service

        );
		//$('#price').val($query.);
			 
}
			 
        header('Content-Type: application/json');
        echo json_encode($data1);
		//print_r($data1); 

	}

}

?>