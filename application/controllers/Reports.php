<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reports extends CI_Controller
{
	public function __construct(){
        parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {            redirect('login');        }
         $db['superadmin'] = array(
			'dsn'   => '',
			'hostname' => 'localhost',
			'username' => $this->session->userdata('username'),
			'password' => $this->session->userdata('password'),
			'database' => $this->session->userdata('database'),
			'dbdriver' => 'mysqli',
			'dbprefix' => '',
			'pconnect' => FALSE,
			'db_debug' => TRUE,
			'cache_on' => FALSE,
			'cachedir' => '',
			'char_set' => 'utf8',
			'dbcollat' => 'utf8_general_ci',
			'swap_pre' => '',
			'encrypt' => FALSE,
			'compress' => FALSE,
			'stricton' => FALSE,
			'failover' => array(),
			'save_queries' => TRUE
		);
        //echo "<pre>";
       //    print_r($db['default']);exit;
            $CI = &get_instance();
        $this->db1 = $CI->load->database($db['superadmin'],true);

		$this->arraay =$this->session->userdata('per');
		$this->load->model('rate_plan_model');
        $this->load->model('dashboard_model');
		$this->load->model('Unit_class_model');
		$this->load->model('setting_model');
		$this->load->model('reports_model');
		$this->load->model('bookings_model');
		
		
    }
	function index(){
		$data['heading'] = '';
            $data['sub_heading'] = ' ';
            $data['description'] = '';
            $data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['page'] = 'backend/dashboard/hotel_reports';
            $this->load->view('backend/common/index', $data);
	}
	function reservation_summary(){

			 $found = in_array("38",$this->arraay);
		if($found){

			$data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Reservation Reports';
            $data['description'] = 'Reservation summary';
            $data['active'] = 'hotel_reports';

           // $data['usage_log'] = $this->dashboard_model->all_usage_log();


            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['summary'] = $this->dashboard_model->fetch_r_summary();
		   $data['page'] = 'backend/dashboard/reservation_summary';
           $this->load->view('backend/common/index', $data);
	}
	else
	{
		redirect('dashboard');
	}
	}
	function reservation_report(){

			 $found = in_array("38",$this->arraay);
		if($found){
			$data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Reservation Reports';
            $data['description'] = 'Reservation reports';
            $data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['reports'] = $this->dashboard_model->all_bookings();
			$data['page'] = 'backend/dashboard/reservation_report';
            $this->load->view('backend/common/index', $data);
		}
		else
		{
			redirect('dashboard');
		}


	}
	function note_preference_report(){

			 $found = in_array("38",$this->arraay);
		if($found){
			$data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Reservation Reports';
            $data['description'] = 'Note & Preference Report  ';
			$data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
           //$data['reports'] = $this->dashboard_model->all_note_preference();
			$data['bookings'] = $this->dashboard_model->all_bookings();
            $data['page'] = 'backend/dashboard/note_preference_report';
            $this->load->view('backend/common/index', $data);
		}
		else
		{
			redirect('dashboard');
		}

	}
	function extra_charge_report(){

			 $found = in_array("38",$this->arraay);
		if($found)
		{
		$data['heading'] = 'Hotel Reports';
		$data['sub_heading'] = 'Reservation Reports';
		$data['description'] = 'Extra Charge Report';
		$data['active'] = 'hotel_reports';
		$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

		if(($this->input->post('t_dt_frm')) && ($this->input->post('t_dt_to')))
		{
			$start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date  = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
			$data['start_date']=$this->input->post('t_dt_frm');
			$data['end_date']=$this->input->post('t_dt_to');
			
			$data['bookings'] = $this->dashboard_model->all_bookings_by_date($start_date,$end_date);
			$data['groups'] = $this->dashboard_model->all_group_by_date($start_date,$end_date);
		}
		else
		{
			$data['bookings'] = $this->dashboard_model->all_bookings();
			$data['groups'] = $this->dashboard_model->all_group();
		}

		$data['page'] = 'backend/dashboard/extra_charge_report';
		$this->load->view('backend/common/index', $data);
	}else{
		redirect('dashboard');
	}
	}
	function cancel_report()
    {
		
		$found = in_array("25",$this->arraay);
		if($found) {
            $data['heading'] = 'Report';
            $data['sub_heading'] = 'All Cancelled Bookings';
            $data['description'] = 'Cancelled Bookings List';
            $data['active'] = 'hotel_reports';
			$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			
            //$data['bookings'] = $this->dashboard_model->all_bookings($config['per_page'], $segment);
			$data['bookings'] = $this->setting_model->cancel_bookings();
         //   $data['transactions'] = $this->dashboard_model->all_transactions();
            //$data['pagination'] = $this->pagination->create_links();.........

			/*echo "<pre>";
			print_r($data);
			echo "</pre>";
			exit;*/
            $data['page'] = 'backend/dashboard/cancel_report';
            $this->load->view('backend/common/index', $data);

        } 
		else {
            redirect('dashboard');
        }
		
    }
	function pending_payments()
    {

			 $found = in_array("26",$this->arraay);
		if($found)
		{
            $data['heading'] = 'Booking';
            $data['sub_heading'] = 'Pending Payments';
            $data['description'] = 'Bookings With Pending Payment';
            $data['active'] = 'pending_payments';
			$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            //$data['bookings'] = $this->dashboard_model->all_bookings($config['per_page'], $segment);
			$data['bookings'] = $this->dashboard_model->all_bookings();
            $data['transactions'] = $this->dashboard_model->all_transactions();
            //$data['pagination'] = $this->pagination->create_links();

            $data['page'] = 'backend/dashboard/pending_payments';
            $this->load->view('backend/common/index', $data);

        } else {
            redirect('dashboard');
        }
    }
	function pending_checkin_checkout()
    {

			 $found = in_array("26",$this->arraay);
		if($found)
		{
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = '';
            $data['description'] = '';
            $data['active'] = 'hotel_reports';
			$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['bookings'] = $this->dashboard_model->all_bookings_pending();
           // $data['transactions'] = $this->dashboard_model->all_transactions();
		//	print_r($data['bookings']);exit;
            $data['page'] = 'backend/dashboard/pending_checkin_checkout';
            $this->load->view('backend/common/index', $data);

        } else {
            redirect('dashboard');
        }
    }
	function all_f_reports()
    {

			 $found = in_array("38",$this->arraay);
		if($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Financial Reports';
            $data['description'] = 'Report List';
            $data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));



            //$data['transactions'] = $this->dashboard_model->all_transactions_limit($config['per_page'], $segment);
            //$data['pagination'] = $this->pagination->create_links();
            $data['transactions'] = $this->dashboard_model->all_f_reports();
			//print_r($data['transactions']); exit;

			$data['types'] = $this->dashboard_model->all_f_types();
			$data['entity'] = $this->dashboard_model->all_f_entity();
            $data['page'] = 'backend/dashboard/all_f_reports';
            $this->load->view('backend/common/index', $data);

        } else {
            redirect('dashboard');
        }
    }
	function booking_sales_report(){
		$data['heading'] = 'Reports';
            $data['sub_heading'] = 'Transaction Report';
            $data['description'] = 'Report List';
            $data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

		if($this->input->post()) {	
            $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date  = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
			$data['start_date']=$this->input->post('t_dt_frm');
			$data['end_date']=$this->input->post('t_dt_to');
		//$data['sales'] = $this->dashboard_model->all_transactions_by_date1($start_date,$end_date);
		$data['sales'] = $this->dashboard_model->all_bookings_daily_by_date($start_date,$end_date);
			
			
		} else{
		  // $data['sales'] = $this->dashboard_model->all_f_reports1();
		   $data['sales'] = $this->dashboard_model->all_bookings_daily_grp();
			//  $data['test'] = $this->dashboard_model->all_bookings_daily_grp();
			// print_r( $data['sales']); exit;
			
			 // or your date as well
   
		}
		    $data['page'] = 'backend/dashboard/daily_sales_report';
            $this->load->view('backend/common/index', $data);
	}
	function booking_sales_report2(){
	   // echo'hi';exit;
		$data['heading'] = 'Reports';
            $data['sub_heading'] = 'Transaction Report';
            $data['description'] = 'Report List';
            $data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

		if($this->input->post()) {	
            $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date  = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
			$data['start_date']=$this->input->post('t_dt_frm');
			$data['end_date']=$this->input->post('t_dt_to');
		//$data['sales'] = $this->dashboard_model->all_transactions_by_date1($start_date,$end_date);
		$data['sales'] = $this->dashboard_model->all_bookings_daily_by_date($start_date,$end_date);
		//print_r( $data['sales']); exit;	
			
		} else{
		  // $data['sales'] = $this->dashboard_model->all_f_reports1();
		   $data['sales'] = $this->dashboard_model->all_bookings_daily_grp();
			//  $data['test'] = $this->dashboard_model->all_bookings_daily_grp();
			 //print_r( $data['sales']); exit;
		}
		    $data['page'] = 'backend/dashboard/daily_sales_report2';
            $this->load->view('backend/common/index', $data);
	}
	
	function all_reports() {
		
		$found = in_array("38",$this->arraay);
		if($found)
		{
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Guest Report';
            $data['description'] = 'Guest Reports';
            $data['active'] = 'hotel_reports';
			$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['bookings'] = $this->dashboard_model->all_bookings_report();
             $data['transactions'] = $this->dashboard_model->all_transactions();
             $data['pagination'] = $this->pagination->create_links();

                $data['page'] = 'backend/dashboard/all_reports';
                $this->load->view('backend/common/index', $data);

            }
			else
			{
            redirect('dashboard');
        }
    }
	
	function cust_police_reports() {

			 $found = in_array("38",$this->arraay);
		if($found) {
            $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Guest Report';
            $data['description'] = 'Report List';
            $data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

			if($this->input->post()){

			   $date_1=date('Y-m-d',strtotime($this->input->post('t_dt_frm')));
               $date_2=date('Y-m-d',strtotime($this->input->post('t_dt_to')));
				$data['start_date']=$this->input->post('t_dt_frm');
				$data['end_date']=$this->input->post('t_dt_to');
				$data['bookings'] = $this->dashboard_model->get_cust_report_by_date($date_1,$date_2);

			}
			else{
				$data['bookings'] = $this->dashboard_model->all_bookings();
			}


            $data['page'] = 'backend/dashboard/cust_police_report';
            $this->load->view('backend/common/index', $data);

        } else {
            redirect('dashboard');
        }
    }
	
	function tax_report() {
         
		$found = in_array("38",$this->arraay);
		if($found)
		{
			 $data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Tax Reports';
            $data['description'] = 'Tax Report';
			$data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			
			if($this->input->post()) {
				
				$start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
				$end_date  = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
				$data['start_date'] = $this->input->post('t_dt_frm');
				$data['end_date']  = $this->input->post('t_dt_to');
				$data['line_item'] = $this->dashboard_model->all_tax_report_by_date($start_date,$end_date);
				
			} else {
				$data['line_item'] = $this->dashboard_model->get_All_booking_details();
			}
            $data['page'] = 'backend/dashboard/tax_report';
            $this->load->view('backend/common/index', $data);
		}
		else
		{
			redirect('dashboard');
		}
	}
	
	function lodging_tax_report(){

			 $found = in_array("38",$this->arraay);
		if($found)
		{
			$data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Tax Reports';
            $data['description'] = 'Tax Report';
			$data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
           // $data['reports'] = $this->dashboard_model->all_note_preference();
			$data['bookings'] = $this->dashboard_model->all_bookings();
            $data['page'] = 'backend/dashboard/lodging_tax_report';
            $this->load->view('backend/common/index', $data);
		}
		else
		{
		redirect('dashboard');
		}
	}
	function travelagentcommission(){
		$data['heading'] = 'Reports';
            $data['sub_heading'] = 'Travel Agent Report';
            $data['description'] = 'Report List';
            $data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

			 if($this->input->post())
			{	
            $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date  = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
			$data['start_date']=$this->input->post('t_dt_frm');
			$data['end_date']=$this->input->post('t_dt_to');
			$data['travelAgent'] = $this->unit_class_model->all_travelAgentCommision_by_date1($start_date,$end_date);
		
			
			
		} else{
		   $data['travelAgent'] = $this->unit_class_model->all_travelAgentCommision();
		}
		    $data['page'] = 'backend/dashboard/travelagentcommision';
            $this->load->view('backend/common/index', $data);
	}
	function travelagentcommission_type(){
		$type=$this->input->post('type');	
		$quer = $this->unit_class_model->all_travelAgentCommision_grp_single($type);
		$output='<table class="table table-striped table-hover table-bordered" id="sample_3">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Type</th>
          <th scope="col">Type Id</th>
          <th scope="col">Booking Id</th>
          <th scope="col">Group Id</th>
          <th scope="col">Date</th>
          <th scope="col">Booking Status</th>
          <th scope="col">Applable On</th>
          <th scope="col">Applied Amount</th>
          <th scope="col">Commision Amount</th>          
        </tr>
      </thead>
      <tbody>';
	  $i=0;$totalGroup=0;$totalAppliedAmo=0;$totalcomm=0;
				if(isset($quer) && $quer){
				foreach($quer as $travel){
			$i++;
	if($travel->groupID){
	$totalGroup++;
}if($travel->applyedAmount){
	$totalAppliedAmo+=$travel->applyedAmount;
}
if($travel->amount){
	$totalcomm+=$travel->amount;
}
if($travel->taType=='agent'){
$que=$this->bookings_model->get_broker_details($travel->taID);

if(isset($que) && $que){
foreach($que as $q){
$name='<span style="color:#008822; font-weight:bold;">'.$q->b_name.'</span>';
}
}}else{
$que=$this->bookings_model->get_channel_details($travel->taID);

if(isset($que) && $que){
foreach($que as $q){
$name='<span style="color:#0277BD; font-weight:bold;">'.$q->channel_name.'</span>';
}
}
}

        $output.='<tr><td>'. $i.'</td><td>'.$travel->taType.'</td><td>'.$name.'</td><td>'.$travel->bookingID.'</td><td>'.$travel->groupID.'</td><td>'.$travel->addedDate.'</td><td>';

$stat=$this->dashboard_model->get_status($travel->bkStatus);
if(isset($stat) && $stat){foreach($stat as $sta){  $St=$sta->booking_status;}}
 $output.=$St.'</td><td>'; 
				if($travel->commAppOn=='rr'){
				$coma="Room Rent";
				}else if($travel->commAppOn=='rrf'){
                        $coma="Room Rent + Food";
					}
				else if($travel->commAppOn=='tb'){
                        $coma="Total Bill";
					}else{
$coma='';
}
			
		$output.=$coma.'</td>';
		$output.='<td>'.$travel->applyedAmount.'</td>';
		$output.='<td>'.$travel->amount.'</td></tr></tbody>';
        	
		
		
	}
	$output.='<tfoot><tr><td></td>
        	<td></td>';
        	
          $output.='<td style="font-weight:700; color:#666666; text-align:left;"> TOTAL </td>
					<td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:left;" value="'.number_format($i,2,".",",").'" /></td>
					<td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:left;" value="'.number_format($totalGroup,2,".",",").'" /></td>
					<td></td><td></td><td></td>
					<td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:left;" value="'.number_format($totalAppliedAmo,2,".",",").'" /></td>
					<td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:left;" value="'.number_format($totalcomm,2,".",",").'" /></td>
        </tr>
          
        </tfoot>
    </table>';
      
}
	
	echo $output;

}

	function admin_transaction_report(){
		
			$data['heading'] = 'Reports';
            $data['sub_heading'] = 'Admin Transaction Report';
			$data['subs_heading'] = 'Transaction Reports';
            $data['description'] = 'Admin Transaction Report';
			$data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));			
			$data['page'] = 'backend/dashboard/admin_transaction_report';
            $this->load->view('backend/common/index', $data);
   }
   
   
   function admin_transaction_summary(){
	   
			$data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Financial Reports';
			$data['subs_heading'] = '';
            $data['description'] = 'Admin Transaction Report';
			$data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			//echo "<pre>";
			//print_r($data['hotel_name']);exit;
			$data['transaction_report']=$this->reports_model->admin_transaction_summary();
			$data['page'] = 'backend/dashboard/admin_transaction_report';
            $this->load->view('backend/common/index', $data);
   }
   
   
   function discount_report($condition=''){
		
		if($condition==''){
			$data['heading'] = 'Hotel Reports';
            $data['sub_heading'] = 'Reservation Reports';
            $data['description'] = 'Discount summary report';
            $data['active'] = 'hotel_reports';
            $data['booking'] = $this->dashboard_model->get_booking_report();
			$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['page']='backend/dashboard/booking_report';
			$this->load->view('backend/common/index', $data);
		}
		else{
		
			$date = $this->input->post('search_val');
			$date_range[]= explode("-",$date);
			$r_start_date = $date_range[0];	
			$start =  date('Y-m-d', strtotime($r_start_date[0]));
			$end =   date('Y-m-d', strtotime($r_start_date[1]));
			//echo $start.'===='.$end;
			//exit();
			
			$booking = $this->dashboard_model->get_booking_report_by_condition($start,$end);
			$out_put='<table class="table table-striped table-bordered table-hover" id="sample_1">
						<thead>
						  <tr>
							<th width="2%" scope="col">Sl </th>
							<th style="width:10%;" scope="col">Discount Date </th>
							<th scope="col">Admin Name </th>
							<th scope="col">Hotel Name </th>
							<th scope="col">Discount Given to </th>
							<th scope="col">Guest Name </th>
							<th scope="col">Discount plan </th>	
							<th scope="col">Discount For </th>
							<th scope="col">Discount % </th>
							<th scope="col">Discount Amount </th>
							<th scope="col">Amount Due </th>
							<th scope="col">Total Amount </th>
							<th scope="col">IP Address </th>
							<!--<th scope="col"> Unit Type </th>
							<th scope="col"> Unit Class </th>
							<th scope="col" width="400"> Unit Description </th>-->
							
						  </tr>
						</thead>		
					  <tbody>';
					  
					if(isset($booking) && $booking ){
					 $sl =0;
					 $d = '';
					 $span='';
					 $span2='';
					 $span3='';
					 $span4 = '';
					  foreach($booking as $book){
						$sl++;  
						  
						  $out_put.='<tr id=row_'.$sl.'>';
						  $out_put.='<td>'.$sl.'</td>';
						   
					if($d != date("jS M Y",strtotime($book->add_date))){
						$span = '<span style="color:#0277BD; font-weight:bold;">'.date("jS M Y",strtotime($book->add_date)).'</span>';
					}
					else{
						$span = '<span style="color:#0277BD;">'.date("jS M Y",strtotime($book->add_date)).'</span>';
					}
					
					$d = date("jS M Y",strtotime($book->add_date));
				
						  
						  $out_put.='<td style="width:10%;">'.$span.'</td>';
						  $out_put.='<td>'.$book->admin_name.'</td>';
						  $out_put.='<td>'.$book->hotel_name.'</td>';
						  
						   
					if($book->related_type == 'group_booking'){
						$span2 = '<span style="color:#F8681A;"><i class="fa fa-users" aria-hidden="true"></i> Group Booking</span>';
					}
					else if($book->related_type == 'single_booking'){
						$span2 = '<span style="color:#023358;"><i class="fa fa-user" aria-hidden="true"></i> Single Booking</span>';
					}
					else
						$span2 = $book->related_type;
					
					$span2 .= '<br><span style="font-style:italic;"> - '.$book->related_id.'</span>';
				
						  
						  $out_put.='<td>'.$span2.'</td>';
						  $out_put.='<td>'.$book->g_name.'</td>';
						  
						  
					if($book->discount_plan_id == 0)
						$span3 =  '<span style="color:#AAAAAA;">None</span>';
					else
						$span3 = $book->rule_name; 
				
						  $out_put.='<td>'.$span.'</td>';
						  $out_put.='<td>'.$book->discount_for.'</td>';
						  
						  
					if($book->discount_percentage > 25)						
					$span4 = '<span style="color:#E52828;">'.$book->discount_percentage.' <i style="color:#E52828;" class="fa fa-percent" aria-hidden="true"></i>';
					else if($book->discount_percentage > 10)						
						$span4 = '<span style="color:#3E3274;">'.$book->discount_percentage.' <i style="color:#3E3274;" class="fa fa-percent" aria-hidden="true"></i>';
					else
						$span4 = $book->discount_percentage.' <i style="color:#AAAAAA;" class="fa fa-percent" aria-hidden="true"></i>'; 
				
						  $out_put.='<td>'.$span4.'</td>';
						  $out_put.='<td><i class="fa fa-inr" aria-hidden="true"></i>'.$book->discount_amount.'</td>';
						  $out_put.='<td>'.$book->amt_due.'</td>';
						  $out_put.='<td>'.$book->totalAmt.'</td>';
						   $out_put.='<td>'.$book->ip_address.'</td>';
						 $out_put.='<tr>';
	
					  }
					}
					$out_put.='</tbody>';	
					 
			echo $out_put;
		}
		
		
	}


	 function all_login_reports(){
		 
			$data['heading'] = 'Reports';
            $data['sub_heading'] = 'All Login Details';
			$data['subs_heading'] = 'All Login Details';
            $data['description'] = 'All Login Details';
			$data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			
			$data['login_details'] = $this->dashboard_model->all_login_reports();
			//echo "<pre>";
			//print_r($data['login_details']);exit;
			//print_r($this->session->userdata());exit;
			$data['page'] = 'backend/dashboard/all_login_details';
            $this->load->view('backend/common/index', $data);
	 }
	 
	 function reservation_entry_summary(){
	 $found = in_array("256",$this->arraay);
		if($found == $found){
			$data['heading'] = 'Reports';
            $data['sub_heading'] = 'Reservation Entry Summary';
			$data['subs_heading'] = 'Reservation Entry Summary';
            $data['description'] = 'Reservation Entry Summary';
			$data['active'] = 'hotel_reports';
            $data['bookingDate'] = $this->dashboard_model->getBooking_byDate();
            //$data['bookingSource'] = $this->dashboard_model->getBookingSource_byDate();
			//$data['login_details'] = $this->dashboard_model->all_login_reports();

			$data['page'] = 'backend/dashboard/reservation_taken_summary';
            $this->load->view('backend/common/index', $data);
	 } else
		{
			 $this->session->set_flashdata('warning', "Inadequet permission, please contact admin to get proper permission!"); 
			 redirect('reports');
		}
	 
	 }
	 
	 function adr_report(){
		 
			$data['heading'] = 'Reports';
            $data['sub_heading'] = 'ADR Report';
			$data['subs_heading'] = 'ADR Report';
            $data['description'] = 'Average Daily Rate Report';
			$data['active'] = 'hotel_reports';
            $data['bookingDate'] = $this->dashboard_model->getBookingCustFrom_byDate();	         
			$data['page'] = 'backend/dashboard/adr_report';
            $this->load->view('backend/common/index', $data);
	 }
	 
	 function transaction_summary(){
		 
			$data['heading'] = 'Reports';
            $data['sub_heading'] = 'Transaction Summary';
			$data['subs_heading'] = 'Transaction Summary';
            $data['description'] = 'Transaction Summary Report';
			$data['active'] = 'hotel_reports';
            $data['bookingDate'] = $this->dashboard_model->getBookingCustFrom_byDate();	         
			$data['page'] = 'backend/dashboard/transaction_summary';
            $this->load->view('backend/common/index', $data);
	 }
	 
	  function monthly_summary_report(){

		$data['heading']='Hotel Reports';
        $data['sub_heading']='Financial Reports';
        $data['description']='';
        $data['active']='hotel_reports';
        $data['totalrooms']=$this->dashboard_model->totalrooms();
		//print_r( $data['totalrooms']); exit;
		$x=count($data['totalrooms']);
        $data['totalrooms']=$x;
		 //$data['bookingDate'] = $this->dashboard_model->getBookingLineItem_byDate1();	   
		//exit;
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/monthly_summary_report';
        $this->load->view('backend/common/index', $data);
    
	}
	
	 function monthly_summ_r2(){
        
		$data['heading']='Hotel Reports';
        $data['sub_heading']='Financial Reports';
        $data['description']='';
        $data['active']='hotel_reports';
        $data['totalrooms']=$this->dashboard_model->totalrooms();
		
		$x=count($data['totalrooms']);
        $data['totalrooms']=$x;
        $month=$_GET['m'];
					if($month==1){
					 $start= date("Y-01-01");
					 $end= date("Y-01-t");
					
					}else if($month==2){
						 $start= date("Y-02-01");
					 $end= date("Y-02-t");
					 $m=date("t", strtotime($start));
					}else if($month==3){
						 $start= date("Y-03-01");
					     $end= date("Y-03-t");
						 $m=date("t", strtotime($start));
					}else if($month==4){
						 $start= date("Y-04-01");
					     $end= date("Y-04-t");
						 $m=date("t", strtotime($start));
					}else if($month==5){
						 $start= date("Y-05-01");
					     $end= date("Y-05-t");
						$m=date("t", strtotime($start));
						
					}else if($month==6){
						$start= date("Y-06-01");
					     $end= date("Y-06-t");
						 $m=date("t", strtotime($start));
					}else if($month==7){
						$start= date("Y-07-01");
					     $end= date("Y-07-t");
						 $m=date("t", strtotime($start));
					}else if($month==8){
						 $start= date("Y-08-01");
					     $end= date("Y-08-t");
						 $m=date("t", strtotime($start));
					}else if($month==9){
						 $start= date("Y-09-01");
					     $end= date("Y-09-t");
						 $m=date("t", strtotime($start));
					}else if($month==10){
						 $start= date("Y-10-01");
					     $end= date("Y-10-t");
						 $m=date("t", strtotime($start));
					}else if($month==11){
						 $start= date("Y-11-01");
					     $end= date("Y-11-t");
						 $m=date("t", strtotime($start));
					}else if($month==12){
						 $start= date("Y-12-01");
					     $end= date("Y-12-t");
						 $m=date("t", strtotime($start));
					}
		$data['bookingDate'] = $this->dashboard_model->getBookingLineItem_byDate1($start,$end);	
		$data['month'] =$month;	
		$data['start'] =$start;	
		$data['end'] =$end;	
		$data['max'] =$m;	
				
		$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/monthly_summary_report2';
        $this->load->view('backend/common/index', $data);
    
	}
	
	
	function monthly_reservation_summary_totals(){
        
			$data['heading'] = 'Reports';
            $data['sub_heading'] = 'Reservation Report';
			$data['subs_heading'] = 'Reservation Report';
            $data['description'] = 'Reservation Summary Report';
			$data['active'] = 'hotel_reports';
            $data['bookingDate'] = $this->dashboard_model->getBookingLineItem_byDate();	         
			$data['page'] = 'backend/dashboard/monthly_reservation_summary_totals';
            $this->load->view('backend/common/index', $data);
    
	}	
	
	function checkout_report(){
		$data['heading'] = 'Reports';
            $data['sub_heading'] = 'Transaction Report';
            $data['description'] = 'Report List';
            $data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

			 if($this->input->post())
			{	
            $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date  = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
			$data['start_date']=$this->input->post('t_dt_frm');
			$data['end_date']=$this->input->post('t_dt_to');
		//$data['sales'] = $this->dashboard_model->all_transactions_by_date1($start_date,$end_date);
		$data['sales'] = $this->dashboard_model->all_bookings_chkout_by_date($start_date,$end_date);
			
			
		} else{
		  // $data['sales'] = $this->dashboard_model->all_f_reports1();
		   $data['sales'] = $this->dashboard_model->all_bookings_daily_chkout();
		 //  $data['test'] = $this->dashboard_model->all_bookings_daily_grp();
       // print_r( $data['sales']); exit;
		}
		    $data['page'] = 'backend/dashboard/chkout_sales_report';
            $this->load->view('backend/common/index', $data);
	} 
	
	function transaction_summary_reports(){
		$data['heading'] = 'Reports';
            $data['sub_heading'] = 'Transaction Report';
            $data['description'] = 'Report List';
            $data['active'] = 'all_f_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

			 if($this->input->post())
			{	
            $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date  = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
			$data['start_date']=$this->input->post('t_dt_frm');
			$data['end_date']=$this->input->post('t_dt_to');
		//$data['sales'] = $this->dashboard_model->all_transactions_by_date1($start_date,$end_date);
		$data['sales'] = $this->dashboard_model->all_bookings_chkout_by_date($start_date,$end_date);
			
			
		} else{
		  // $data['sales'] = $this->dashboard_model->all_f_reports1();
		  // $data['reports'] = $this->dashboard_model->transaction_report();
		 //  $data['test'] = $this->dashboard_model->all_bookings_daily_grp();
       // print_r( $data['sales']); exit;
		}
		    $data['page'] = 'backend/dashboard/transaction_summary_reports';
            $this->load->view('backend/common/index', $data);
	}

	function checkInOut_summary(){
        
			$data['heading'] = 'Reports';
            $data['sub_heading'] = 'Checkin & Checkout Summary';
			$data['subs_heading'] = 'Checkin & Checkout Summary';
            $data['description'] = 'Checkin & Checkout Summary';
			$data['active'] = 'hotel_reports';
            $data['checkout'] = $this->reports_model->getCheckoutSummary();	         
            $data['checkin'] = $this->reports_model->getCheckinSummary();	         
			$data['page'] = 'backend/reports/checkInOut_summary';
            $this->load->view('backend/common/index', $data);
    
	}
	
	function get_r_report_by_date(){

			 $found = in_array("38",$this->arraay);
		if($found)
		{
		$start_date = date("Y-m-d");
        $end_date  = date("Y-m-d");
		$data['heading'] = 'Reports';
        $data['sub_heading'] = 'Transaction Report';
        $data['description'] = 'Report List';
        $data['active'] = 'all_f_reports';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

        if($this->input->post())
        {
            $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date  = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
			$data['start_date']=$this->input->post('t_dt_frm');
			$data['end_date']=$this->input->post('t_dt_to');
        }

		 $data['reports'] = $this->dashboard_model->all_r_by_date($start_date,$end_date);
		  $data['page'] = 'backend/dashboard/reservation_report';
        $this->load->view('backend/common/index', $data);
	} else{
	redirect('dashboard');
   }

	}
	function cancel_report_by_date(){
		
			$found = in_array("25",$this->arraay);
			if($found) {
            $data['heading'] = 'Report';
            $data['sub_heading'] = 'All Cancelled Bookings';
            $data['description'] = 'Cancelled Bookings List';
            $data['active'] = 'all_bookings';
			$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			
			$cust_from_date_actual = date("Y-m-d",strtotime($this->input->post('t_dt_frm')));
			$cust_end_date_actual = date("Y-m-d",strtotime($this->input->post('t_dt_to')));
            //$data['bookings'] = $this->dashboard_model->all_bookings($config['per_page'], $segment);
			$data['bookings'] = $this->setting_model->cancel_bookings_by_date($cust_from_date_actual,$cust_end_date_actual);
         //   $data['transactions'] = $this->dashboard_model->all_transactions();
            //$data['pagination'] = $this->pagination->create_links();
				$data['start_date'] = $cust_from_date_actual;
				$data['end_date'] = $cust_end_date_actual;
				$data['page'] = 'backend/dashboard/cancel_report';
				$this->load->view('backend/common/index', $data);

        } else {
            redirect('dashboard');
        }
			
		
		

	}
	
	 function daily_transaction_summary(){
        
		$data['heading']='Hotel Reports';
        $data['sub_heading']='Financial Reports';
        $data['description']='';
        $data['active']='hotel_reports';
        $data['totalrooms']=$this->dashboard_model->totalrooms();
		
		$x=count($data['totalrooms']);
        $data['totalrooms']=$x;
        $month=$_GET['m'];
					if($month==1){
					 $start= date("Y-01-01");
					 $end= date("Y-01-t");
					$m=date("t", strtotime($start));
					}else if($month==2){
						 $start= date("Y-02-01");
					 $end= date("Y-02-t");
					 $m=date("t", strtotime($start));
					}else if($month==3){
						 $start= date("Y-03-01");
					     $end= date("Y-03-t");
						 $m=date("t", strtotime($start));
					}else if($month==4){
						 $start= date("Y-04-01");
					     $end= date("Y-04-t");
						 $m=date("t", strtotime($start));
					}else if($month==5){
						 $start= date("Y-05-01");
					     $end= date("Y-05-t");
						$m=date("t", strtotime($start));
						
					}else if($month==6){
						$start= date("Y-06-01");
					     $end= date("Y-06-t");
						 $m=date("t", strtotime($start));
					}else if($month==7){
						$start= date("Y-07-01");
					     $end= date("Y-07-t");
						 $m=date("t", strtotime($start));
					}else if($month==8){
						 $start= date("Y-08-01");
					     $end= date("Y-08-t");
						 $m=date("t", strtotime($start));
					}else if($month==9){
						 $start= date("Y-09-01");
					     $end= date("Y-09-t");
						 $m=date("t", strtotime($start));
					}else if($month==10){
						 $start= date("Y-10-01");
					     $end= date("Y-10-t");
						 $m=date("t", strtotime($start));
					}else if($month==11){
						 $start= date("Y-11-01");
					     $end= date("Y-11-t");
						 $m=date("t", strtotime($start));
					}else if($month==12){
						 $start= date("Y-12-01");
					     $end= date("Y-12-t");
						 $m=date("t", strtotime($start));
					}
		$data['transaction_summary'] = $this->dashboard_model->transaction_report($start,$end);	
		$data['month'] =$month;	
		$data['start'] =$start;	
		$data['end'] =$end;	
		$data['max'] =$m;	
				
		$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/daily_tranc_summary_report';
        $this->load->view('backend/common/index', $data);
    
	}
	
	function daily_transactional_report(){
        
			$data['heading'] = 'Financial Reports';
            $data['sub_heading'] = 'Daily Transactional Summary';
			$data['subs_heading'] = 'Daily Transactional Summary';
            $data['description'] = 'Daily Transactional Summary';
			$data['active'] = 'hotel_reports';
            $data['transactions'] = $this->reports_model->getTransacDate();	         
            $data['transactionsMon'] = $this->reports_model->getTransacMonth();	         
            //$data['checkin'] = $this->reports_model->getCheckinSummary();	         
			$data['page'] = 'backend/reports/daily_transactional_report';
            $this->load->view('backend/common/index', $data);
	}
	
	function reservation_list(){
        
			$data['heading'] = 'Reservation';
            $data['sub_heading'] = 'Reservation List';
			$data['subs_heading'] = 'Reservation List';
            $data['description'] = 'Reservation List';
			$data['active'] = 'hotel_reports';
            $data['reservation'] = $this->reports_model->reservationList();	         
			$data['page'] = 'backend/reports/reservation_list';
            $this->load->view('backend/common/index', $data);
	}
	
	function guest_stay(){
        
			$data['heading'] = 'Guest Reports';
            $data['sub_heading'] = 'Guest Stay Report';
			$data['subs_heading'] = 'Guest Stay Report';
            $data['description'] = 'Guest Stay Report';
			$data['active'] = 'hotel_reports';
            $data['guest'] = $this->reports_model->guest_stay();	         
			$data['page'] = 'backend/reports/guest_stay';
            $this->load->view('backend/common/index', $data);
	}
	
	function adjustment_report(){
        
			$data['heading'] = 'Reservation Reports';
            $data['sub_heading'] = 'Adjustment Report';
			$data['subs_heading'] = 'Adjustment Report';
            $data['description'] = 'Adjustment Report';
			$data['active'] = 'hotel_reports';
            $data['adjustment'] = $this->reports_model->adjReport();	         
			$data['page'] = 'backend/reports/adjustment_report';
            $this->load->view('backend/common/index', $data);
	}
        
        function chart_reference(){
        
			$data['heading'] = 'Report Chart';
           		$data['active'] = 'chart_reports';
                        $data['chart_reports'] = $this->reports_model->chartList();	         
			$data['page'] = 'backend/dashboard/chart_reports';
                        $this->load->view('backend/common/index', $data);
	}
	 function chart_reference1(){
        
			$data['heading'] = 'Report Chart';
           		$data['active'] = 'chart_reports';
                        $data['chart_reports'] = $this->reports_model->chartList();	         
			$data['page'] = 'backend/dashboard/chart_reports1';
                        $this->load->view('backend/common/index', $data);
	}
	 function chart_reference2(){
        
			$data['heading'] = 'Report Chart';
           		$data['active'] = 'chart_reports';
                        $data['chart_reports'] = $this->reports_model->chartList();	         
			$data['page'] = 'backend/dashboard/chart_reports2';
                        $this->load->view('backend/common/index', $data);
	}
	 function chart_reference3(){
        
			$data['heading'] = 'Report Chart';
           		$data['active'] = 'chart_reports';
                        $data['chart_reports'] = $this->reports_model->chartList();	         
			$data['page'] = 'backend/dashboard/chart_reports3';
                        $this->load->view('backend/common/index', $data);
	}
	 function chart_reference4(){
        
			$data['heading'] = 'Report Chart';
           	$data['active'] = 'chart_reports';
            $data['chart_reports4'] = $this->reports_model->chartList4();	         
			$data['page'] = 'backend/dashboard/chart_reports4';
                        $this->load->view('backend/common/index', $data);
	}
	function chart_reference5(){
        
			$data['heading'] = 'Report Chart';
           		$data['active'] = 'chart_reports';
                        $data['chart_reports'] = $this->reports_model->chartList();	         
			$data['page'] = 'backend/dashboard/chart_reports5';
                        $this->load->view('backend/common/index', $data);
	}
	
	function multiLine_chart(){
        
			$data['heading'] = 'Report Chart';
            $data['active'] = 'chart_reports';
            //$data['chart_reports'] = $this->reports_model->chartList();	         
			$data['page'] = 'backend/reports/multiLine_chart';
            $this->load->view('backend/common/index', $data);
	}
	
	function pie_chart(){
        
			$data['heading'] = 'Report Chart';
            $data['active'] = 'chart_reports';
            //$data['chart_reports'] = $this->reports_model->chartList();	
			$data['pie_data'] = $this->reports_model->chartList4();			
			$data['page'] = 'backend/reports/pie_chart';
            $this->load->view('backend/common/index', $data);
	}
	
	function mixed_line_bar(){
        
			$data['heading'] = 'Report Chart';
            $data['active'] = 'chart_reports';
            //$data['chart_reports'] = $this->reports_model->chartList();	
			$data['pie_data'] = $this->reports_model->chartList4();			
			$data['page'] = 'backend/reports/mixed_line_bar';
            $this->load->view('backend/common/index', $data);
	}
	
	function column_chart(){
        
			$data['heading'] = 'Report Chart';
            $data['active'] = 'chart_reports';
            //$data['chart_reports'] = $this->reports_model->chartList();	
			$data['chartdata'] = $this->reports_model->chartdata();			
			$data['page'] = 'backend/reports/column_chart';
            $this->load->view('backend/common/index', $data);
	}
	
	function getTransacDate() {

        $query = $this->reports_model->getTransacDate();
        if ($query) {
            header('Content-Type: application/json');
			echo json_encode($query);
        }

    }
	
	function getBookingDetails(){
		
		$query = $this->reports_model->getBookingDetails();
        if ($query) {
            header('Content-Type: application/json');
			echo json_encode($query);
        }

		
	}
	
	function chartList4(){
		
		$query = $this->reports_model->chartList4();
        if ($query) {
            header('Content-Type: application/json');
			echo json_encode($query);
        }		
	}
	
	function chartList(){
		
		$query = $this->reports_model->chartList();
        if ($query) {
            header('Content-Type: application/json');
			echo json_encode($query);
        }		
	}
	
	function chartdata(){
		
		$query = $this->reports_model->chartdata();
        if ($query) {
            header('Content-Type: application/json');
			echo json_encode($query);
        }		
	}
	
	function bkCntTakenbyDateOnly(){
		
		$query = $this->reports_model->bkCntTakenbyDateOnly();
        if ($query) {
            header('Content-Type: application/json');
			echo json_encode($query);
        }		
	}
	
	function resTakenChart(){
	
		$query = $this->reports_model->resTakenChart();
        if ($query) {
            header('Content-Type: application/json');
			echo json_encode($query);
        }	
		
	}
	function occupied_room_report(){
		
			$data['heading'] = 'Reports';
            $data['sub_heading'] = 'Occupied Room Report';
            $data['description'] = 'Report List';
            $data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

			 if($this->input->post())
			{	
            $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date  = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
			//$data['start_date']=$start_date;
			//$data['end_date']=$end_date;
			//$data['sales'] = $this->dashboard_model->all_transactions_by_date1($start_date,$end_date);
			$data['occu_rooms'] = $this->reports_model->occupied_room_report_by_date($start_date,$end_date);
			
			
		} else{
		  
		   $data['occu_rooms'] = $this->reports_model->occupied_room_report();
		
		}
		    $data['page'] = 'backend/dashboard/occupied_room_report';
            $this->load->view('backend/common/index', $data);
		
	}
	function invoice_report(){
		
			$data['heading'] = 'Reports';
            $data['sub_heading'] = 'Invoice Report';
            $data['description'] = 'Report List';
            $data['active'] = 'hotel_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

			 if($this->input->post())
			{	
            $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date  = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
			//$data['start_date']=$start_date;
			//$data['end_date']=$end_date;
		//$data['sales'] = $this->dashboard_model->all_transactions_by_date1($start_date,$end_date);
		$data['invoice'] = $this->reports_model->invoice_report_by_date($start_date,$end_date);
			
			
		} else{
		  
		   $data['invoice'] = $this->reports_model->invoice_report();
		
		}
		    $data['page'] = 'backend/reports/invoice_report';
            $this->load->view('backend/common/index', $data);
		
	}
	
	
	function add_ADpayments(){
      $gname=$_POST['g_name'];
      $pincode=$_POST['pincode'];
      $checkin_date= date("Y-m-d ", strtotime($_POST['checkin_date']));
      $checkout_date= date("Y-m-d ", strtotime($_POST['checkout_date']));
      $room_no=$_POST['room_no'];
      $amount=$_POST['amount'];
     
       $data=array(
           'g_name'=> $gname,
           'pincode' =>$pincode,
           'checkin_date' =>$checkin_date,
           'checkout_date' =>$checkout_date,
           'room_no' =>$room_no,
           'amount' =>$amount,
           'hotel_id'=>$this->session->userdata('user_hotel'),
           'user_id'=>$this->session->userdata('user_id'),
           );
       //  print_r($data);
     $query=$this->reports_model->addAD_payments($data); 
      if($query){
          echo 1;
      }else{
          echo 0;
      }
 }


function sending_mail(){
     $pwd=mt_rand(100, 99999);
  //  $ci = get_instance();
$this->load->library('email');
$list='srsanjib.roy140@gmail.com';
$this->email->from('smartdedline@gmail.com', 'smartdedline');
$this->email->to($list);
//$this->email->cc('another@another-example.com');
//$this->email->bcc('them@their-example.com');
$msg='Your id same as your email Id and Password is - '.$pwd;
$this->email->subject('New Registration');
$this->email->message($msg);

if($this->email->send()){
    echo "Done";
}else{
    echo "Fail";
}
    
 
   
   
   
  
    
    
}


	
	
}

?>