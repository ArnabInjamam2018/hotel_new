<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Rest_server extends CI_Controller {

    public function index()
    {
        $this->load->helper('url');

        $this->load->view('rest_api/rest_server');
		
    }
	
	public function test()
	{
		$this->load->library('PHPRequests');
		$response = Requests::get('https://github.com/timeline.json');
		var_dump($response->body);
	}
}

