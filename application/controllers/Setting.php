<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Setting extends CI_Controller
{

	public function __construct(){
        parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
            redirect('login');
        }
		// DYNAMIC BATABASE 
        $db['superadmin'] = array(
			'dsn'   => '',
			'hostname' => 'localhost',
			'username' => $this->session->userdata('username'),
			'password' => $this->session->userdata('password'),
			'database' => $this->session->userdata('database'),
			'dbdriver' => 'mysqli',
			'dbprefix' => '',
			'pconnect' => FALSE,
			'db_debug' => TRUE,
			'cache_on' => FALSE,
			'cachedir' => '',
			'char_set' => 'utf8',
			'dbcollat' => 'utf8_general_ci',
			'swap_pre' => '',
			'encrypt' => FALSE,
			'compress' => FALSE,
			'stricton' => FALSE,
			'failover' => array(),
			'save_queries' => TRUE
		);
        //echo "<pre>";
       //    print_r($db['default']);exit;
        $CI = &get_instance();
        $this->db1 = $CI->load->database($db['superadmin'],true);

		$this->arraay =$this->session->userdata('per');
		//$this->load->library('multiple');
		$this->load->model('setting_model');
        $this->load->model('dashboard_model');
		$this->load->model('bookings_model');
		$this->load->model('unit_class_model');
    }

   
	function inlineEdit(){
		$col = $_POST["column"]; 
		$editval = $_POST["editval"];
		$id = $_POST["id"];
		$this->setting_model->inlineEdit($col,$editval,$id);

	}
	function index(){
		$data['heading'] = '';
            $data['sub_heading'] = ' ';
            $data['description'] = '';
            $data['active'] = 'setting';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['page'] = 'backend/dashboard/setting';
            $this->load->view('backend/common/index', $data);
	}
	function all_unit_class(){
		$found = in_array("10",$this->arraay);
		//print $found; exit;
		if($found){			 
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'setting';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['unit_class']=$this->unit_class_model->all_unit_clsss();
        $data['page'] = 'backend/dashboard/all_unit_class';
        $this->load->view('backend/common/index', $data);

    }else{
		redirect('dashboard');
	}
	}	
	function all_unit_type_category(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'setting';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->unit_class_model->all_unit_type_category_data();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->all_admin();
		$data['page']='backend/dashboard/all_unit_type_category';		
        $this->load->view('backend/common/index', $data);

    }
    
    
    function modify_unit_avail_log(){
	
	$userid =$this->session->userdata('user_id');
	$getdetails = $this->setting_model->get_status_unitavail_log();
    header('Content-Type:application/json');
	echo json_encode($getdetails);
	
	}

	function del_from_unitavaillog(){
	
	$userid =$this->session->userdata('user_id');
	$unit_id = $this->input->post('unit_id');
	$rowno = $this->input->post('obcount');
	$getdetails = $this->setting_model->del_from_unitavaillog($unit_id,$rowno);
    //header('Content-Type:application/json');
	//echo json_encode($getdetails);
	echo $getdetails;
	}
	
		function hotel_to_yummpos_setting($add =''){
		$this->load->model('dashboard_model');
		if($add==''){
			$data['heading'] = '';
            $data['sub_heading'] = ' ';
            $data['description'] = '';
            $data['active'] = 'setting';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['pos']  =      $this->setting_model->my_yummpos_setting();
			$data['page'] = 'backend/dashboard/yummpos_setting';
            $this->load->view('backend/common/index', $data);
		}
		else if($add == 'add'){
			
			$data_pos = array(
			
			"hotel_id" => $this->input->post('hotel_id'),
			"yumm_link" => $this->input->post('yumm_link'),
			"yumm_status" => $this->input->post('yumm_status'),
			"api_key" => $this->input->post('api_key'),
			"salt_key" => $this->input->post('salt_key')
			);
			
			//print_r($data_pos);exit;
			$result = $this->setting_model->hotel_to_yummpos_setting($data_pos);
			if($result == 'success'){
				
                  $this->session->set_flashdata('succ_msg', 'Yummposs added sucessfully');
                
				} else {
                    $this->session->set_flashdata('err_msg', 'Database Error');
                }
                //$data['page'] = 'backend/dashboard/all_profit_center';
                redirect(base_url() . 'setting/hotel_to_yummpos_setting');

			}
			else if($add == 'edit'){
				
				
				$hotel_id_pos = $this->input->post('hotel_id1');
				
				$data_pos = array(
			
			"yumm_link" => $this->input->post('yumm_link1'),
			"yumm_status" => $this->input->post('yumm_status1'),
			"api_key" => $this->input->post('api_key1'),
			"salt_key" => $this->input->post('salt_key1')
			);
			
			//print_r($data_pos);exit;
			$result_pos = $this->setting_model->update_hotel_to_yummpos_setting($data_pos,$hotel_id_pos);
			if($result_pos == 'updated'){
				
                  $this->session->set_flashdata('succ_msg', 'yumm poss updated sucessfully');
                
				} else {
                    $this->session->set_flashdata('err_msg', 'Database Error');
                }
                //$data['page'] = 'backend/dashboard/all_profit_center';
                redirect(base_url() . 'setting/hotel_to_yummpos_setting');

				
			}
			
		
		
	}
   
	function invoice_settings(){


		    $data['heading'] = 'Setting';
            $data['sub_heading'] = '';
            $data['description'] = 'Invoice Settings';
            $data['active'] = 'setting';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

            $data['invoice_settings'] = $this->dashboard_model->get_invoice_settings($this->session->userdata('user_hotel'));

		  if($this->input->post()){
		   $config = array( 'upload_path' => './upload/vendor_detail/',
                     'allowed_types' => "pdf",
                     'overwrite' => TRUE, );
			$this->upload->initialize($config);
			$this->upload->do_upload('pdf');
			$up_file=$this->upload->data();
		   $data=array(

		   'gb_prefix'=>$this->input->post('gb_prefix'),
		   'sb_prefix'=>$this->input->post('sb_prefix'),
		   'hotel_id'=>$this->session->userdata('user_hotel'),
		   'booking_source_inv_applicable'=>$this->input->post('booking_source'),
		   'nature_visit_inv_applicable'=>$this->input->post('nature_visit'),
		   'booking_note_inv_applicable'=>$this->input->post('booking_note'),
		   'company_details_inv_applicable'=>$this->input->post('company_details'),
		   'service_tax_applicable'=>$this->input->post('service_tax'),
		   'service_charg_applicable'=>$this->input->post('service_charg'),
		   'luxury_tax_applicable'=>$this->input->post('luxury_tax'),
		   'authorized_signatory_applicable'=>$this->input->post('authorized_signatory'),
		   'invoice_pref'=>$this->input->post('invoice_pref'),
		   'invoice_suf'=>$this->input->post('invoice_suf'),
		   'unit_no'=>$this->input->post('unit_no'),
		   'unit_cat'=>$this->input->post('unit_cat'),
		   'paid_pos_item_show' => $this->input->post('paid_pos_item_show'),
		   'invoice_setting_food_paln'=>$this->input->post('fd_plan'),
		   'invoice_setting_service'=>$this->input->post('service'),
		   'invoice_setting_extra_charge'=>$this->input->post('extra_charge'),
		   'invoice_setting_laundry'=>$this->input->post('laundry'),
		   );
		  // print_r($data);exit;
		   $query=$this->dashboard_model->update_invoice_settings($data);
				//exit();
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Settings updated Succefully');
						redirect('dashboard/invoice_settings');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
					}

		   }

			$data['page'] = 'backend/dashboard/edit_invoice_settings';
            $this->load->view('backend/common/index', $data);
	}
	function tax_type_settings(){
		$found = in_array("144",$this->arraay);
		  if($found)
		    {
			$data['heading']='Setting';
            $data['sub_heading']='Tax Settings';
            $data['description']='Tax elements list';
            $data['active']='setting';
			$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

			$data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
			$data['tax_details'] = $this->dashboard_model->tax_type_setting();
			//$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
			$data['page'] = 'backend/dashboard/tax_type';
			//print_r($data);die();
			
			$this->load->view('backend/common/index', $data);
			}else{
				redirect('dashboard');
			}
			}
			function all_tax_category(){
		$found = in_array("144",$this->arraay);
		  if($found)
		    {
			$data['heading'] = 'Setting';
            $data['sub_heading'] = 'Tax Settings';
            $data['description'] = 'Define tax category and sub category';
            $data['active'] = 'setting';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['tax_category']=$this->dashboard_model->get_all_tax_category();
			$data['page']='backend/dashboard/all_tax_category';
			$this->load->view('backend/common/index', $data);
		
		
	}else{
		redirect('dashboard');
	}
	}
	function all_tax_rule($conditions=''){
		
		if($conditions==''){
			$data['heading'] = 'Setting';
            $data['sub_heading'] = 'Tax Settings';
            $data['description'] = 'Define individual tax rules';
            $data['active'] = 'setting';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['tax_rule']=$this->dashboard_model->get_all_tax_rule();
			$data['page']='backend/dashboard/all_tax_rules';
			$this->load->view('backend/common/index', $data);
	}
	else{
		
		$tc_name=  $this->uri->segment(3);
		if($tc_name){
			
			//echo $tc_name."<br>";
			
				$data['heading'] = 'Settings';
				$data['sub_heading'] = 'Tax rule defination';
				$data['description'] = 'Define individual tax rules';
				$data['active'] = 'all_tax_rule';
				$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
				$data['tax_rule']=$this->dashboard_model->get_specific_tax_rule($tc_name);
				//print_r($data);
				//die();
				$data['page']='backend/dashboard/all_tax_rules';
				$this->load->view('backend/common/index', $data);
		
		}
		else{
			
			echo $tc_name;
		}
	}
}
function add_tax_rule(){
		$found = in_array("141",$this->arraay);
		  if($found)
		    {
        //if($this->session->userdata('user_type_slug') !='G')
			//{
            $data['heading'] = 'Setting';
            $data['sub_heading'] = 'Tax Settings';
            $data['description'] = 'Define individual tax rules';
            $data['active'] = 'all_tax_rule';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			
			
			if($this->input->post())
			{
				$date =  $this->input->post('r_start_dates');
				//echo $date;
				$date_range[]= explode("-",$date);
				$r_start_date = $date_range[0];
				
			$start =  date('Y-m-d', strtotime($r_start_date[0]));
			
			$end =   date('Y-m-d', strtotime($r_start_date[1]));
			
			
				$r_cat = $this->input->post('tax_cat');
				$r_name = $this->input->post('r_name');
				$r_desc = $this->input->post('r_desc');
				$r_start_price =$this->input->post('r_start_price');
				$r_end_price= $this->input->post('r_end_price');
				
				$tax_data  = array(
				'r_cat' => $r_cat,
				'r_name' => $r_name,
				'r_desc' => $r_desc,
				'r_start_price' => $r_start_price,
				'r_end_price' => $r_end_price,
				'r_start_date' => $start,
				'r_end_date' => $end,
				'hotel_id' => $this->session->userdata('user_hotel'),
				'user_id' =>$this->session->userdata('user_id')
				);
				
				//print_r($tax_data);exit;
						/*echo "Main Table:=<br><pre>";
						print_r($tax_data);
						echo "</pre>";
						echo "<br><br>";
						//echo $tax_data;
						//print_r($date_range);
						//echo "<br><pre>";
						
						//echo "</pre>";
						die();
					*/
						
				$tax_rule_id = $this->dashboard_model->add_tax_rule($tax_data); 
				if(isset($tax_rule_id))
				{
					foreach($this->input->post('tax_type') as $key => $value)
					{
						$component_array = array(
						'p_r_id' => $tax_rule_id,
						'tax_type' => $this->input->post('tax_type')[$key],
						'tax_mode' => $this->input->post('tax_mode')[$key],
						'tax_value' => $this->input->post('tax_value')[$key],
						'calculated_on' => $this->input->post('calculated_on')[$key],
						'applied_on' => $this->input->post('applied_on')[$key],
						'hotel_id' => $this->session->userdata('user_hotel'),
						);
						$component = $this->dashboard_model->add_tax_rule_component($component_array);

					}
						
					/*	echo "Sub Table:=<br><pre>";
						print_r($component_array);
						echo "</pre>";
						die();*/
					 $this->session->set_flashdata('succ_msg', 'Rule Added Succfully');
					 redirect(base_url().'setting/all_tax_rule');

				}
				else
				{
					$this->session->set_flashdata('err_msg', 'Error Occoured');
					redirect(base_url().'setting/all_tax_rule');
				}



			}
			$data['page']='backend/dashboard/add_tax_rule';
			$this->load->view('backend/common/index', $data);


        //}
    }else{
		redirect('dashboard');
	}
	}
	function all_hotel_m()
    {

			 $found = in_array("37",$this->arraay);
		if($found) {
            $data['heading'] = 'Setting';
            $data['sub_heading'] = 'Hotel Management';
            $data['description'] = 'Added Hotel List';
           // $data['active'] = 'all_hotel_m';
		    $data['active'] = 'setting';
			$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            /*if ($this->input->post('pages')) {
                $pages = $this->input->post('pages');
            } else {
                if ($this->session->flashdata('pages')) {
                    $pages = $this->session->flashdata('pages');
                } else {
                    $pages = 5;
                }
            }

            $this->session->set_flashdata('pages', $pages);

            $config['base_url'] = base_url() . 'dashboard/all_hotel_m';
            $config['total_rows'] = $this->dashboard_model->all_hotels_row();
            $config['per_page'] = $pages;
            $config['uri_segment'] = 3;
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] = "</ul>";
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";


            $this->pagination->initialize($config);

            $segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;*/

            //$data['hotels'] = $this->dashboard_model->all_hotels($config['per_page'], $segment);
            //$data['pagination'] = $this->pagination->create_links();
		    $data['hotels'] = $this->dashboard_model->all_hotels();
			//print_r($data);
			//exit();
            $data['page'] = 'backend/dashboard/all_hotel_m';
            $this->load->view('backend/common/index', $data);
        } else {
            redirect('dashboard');
        }
    }
	function unit_type(){
		$found = in_array("7",$this->arraay);
		if($found)
		{
        $data['heading'] = 'Setting';
        $data['sub_heading'] = '';
        $data['description'] = '';
        //$data['active'] = 'unit_type';
		$data['active'] = 'setting';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
	//	echo "<pre>";
	//	print_r($data['unit_class']);exit;
		$data['kit'] = $this->dashboard_model->get_all_kit();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		$data['unit'] = $this->dashboard_model->all_unit_type();
		//$data['type'] = $this->dashboard_model->all_unit_type();
        $data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
        $data['page'] = 'backend/dashboard/unit_type';
        $this->load->view('backend/common/index', $data);
		}
		else
		{
			redirect('dashboard');
		}

    }
	
	function check_avail_room(){
		
		$start = $this->input->post('std');
		$end =   $this->input->post('end');
		$rmid =  $this->input->post('rmid');
		$singleid = $this->input->post('singleid');
		$avail_room = $this->setting_model->check_avail_room($start,$end,$rmid,$singleid);
		if(isset($avail_room) && $avail_room!=''){
			echo json_encode($avail_room);	
		}
		
		
		
	}
	
	function check_avail_room_final(){
		
		$start = $this->input->post('std');
		$end =   $this->input->post('end');
		$rmid =  $this->input->post('rmid');
		//$singleid = $this->input->post('singleid');
		$avail_room = $this->setting_model->check_avail_room_final($start,$end,$rmid);
		$avail_room_grp = $this->setting_model->check_avail_room_final_grp2($start,$end,$rmid);
		//	echo json_encode($avail_room_grp);
		
		//exit;
		if(isset($avail_room) && $avail_room){
			
			if(isset($avail_room_grp) && $avail_room_grp){
				
				echo json_encode($avail_room_grp);	
			}
			else{
				
				echo json_encode($avail_room);	
			}
		}
		else if(isset($avail_room_grp) && $avail_room_grp){
			
			if(isset($avail_room) && $avail_room){
				
				echo json_encode($avail_room);	
			}
			else{
				
				echo json_encode($avail_room_grp);	
			}
		}
	}
	
	
	function check_avail_room_final1(){
		
		$start = $this->input->post('std');
		$end =   $this->input->post('end');
		$rmid =  $this->input->post('rmid');
		//$singleid = $this->input->post('singleid');
		$avail_room = $this->setting_model->check_avail_room_final($start,$end,$rmid);
		//$avail_room_grp = $this->setting_model->check_avail_room_final_grp2($start,$end,$rmid);
		header('Content-Type:application/json');
		echo json_encode($avail_room);
	
		
		
		
	}
	
	
	
	
	function check_avail_room_final_grp(){
		
		$start = date('Y-m-d', strtotime($this->input->post('std')));
		$end =   date('Y-m-d', strtotime($this->input->post('end')));
		$rmid =  $this->input->post('rmid');
		//$singleid = $this->input->post('singleid');
		$avail_room = $this->setting_model->check_avail_room_final_grp($start,$end,$rmid);
	//	print_r($avail_room); exit;
		if(isset($avail_room) && $avail_room!=''){
			
				
				echo json_encode($avail_room);	
		
			
		}
		else{
			
					$success = $this->setting_model->insert_avail_room_final_grp($start,$end,$rmid);
			
			
		}
		
		
		
	} 
	
	
	function check_avail_room_delete_grp(){
		
		$start = date('Y-m-d', strtotime($this->input->post('std')));
		$end =   date('Y-m-d', strtotime($this->input->post('end')));
		$rmid =  $this->input->post('rmid');
		$delvalue = $this->setting_model->check_avail_room_delete_grp($start,$end,$rmid);
		$succes_array = array(
		
		"success" => $delvalue,
		
		);
		header('Content-Type: application/json');
		echo json_encode($succes_array);
		
	}
	
	
	function update_guest_details_booking(){
		
		$myguest_name = $this->input->post('myguest_name');
		$myguest_address =   $this->input->post('myguest_address');
		$myguest_mobile =  $this->input->post('myguest_mobile');
		$myguest_mail =  $this->input->post('myguest_mail');
		$bookingid =  $this->input->post('bookingid');
		$guestId =  $this->input->post('guestId');
		
		$guest_details_bookings = array(
		
		"cust_name" => $myguest_name,
		"cust_address" => $myguest_address,
		"cust_contact_no" => $myguest_mobile,
		//"cust_contact_no" => $myguest_mobile,
		
		);
		
		$guest_details_info = array(
					
					"g_name" => $myguest_name,
					"g_pincode" => $myguest_address,
					"g_contact_no" => $myguest_mobile,
					"g_email" => $myguest_mail,
					
					);
		
		$getresult = $this->setting_model->update_guest($guestId,$guest_details_info,$bookingid,$guest_details_bookings);
		
			$resultarray = array(
			
			"succes_result" => $getresult,
			);
		
		header('Content-Type: application/json');
		echo json_encode($resultarray);
		
	}
	
	
	
	function update_guest_details_booking2(){
		
		$myguest_name = $this->input->post('myguest_name');
		$myguest_address =   $this->input->post('myguest_address');
		$myguest_mobile =  $this->input->post('myguest_mobile');
		$myguest_mail =  $this->input->post('myguest_mail');
		$bookingid =  $this->input->post('bookingid');
		$guestId =  $this->input->post('guestId');
		//$start = date('Y-m-d H:i:s', strtotime($this->input->post('std')));
		//$end =   date('Y-m-d H:i:s', strtotime($this->input->post('end')));
		//$start_actual = date('Y-m-d H:i:s', strtotime($this->input->post('std')));
		//$end_actual =   date('Y-m-d H:i:s', strtotime($this->input->post('end')));
		
		
		
		
		
		$times=$this->dashboard_model->checkin_time();
        foreach($times as $time) {
            if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
                $modifier = ($time->hotel_check_in_time_hr + 12) + $time->hotel_check_in_time_mm/60;
            }
            else{
                $modifier = ($time->hotel_check_in_time_hr) + $time->hotel_check_in_time_mm/60;
            }			
        }
		
        $times2=$this->dashboard_model->checkout_time();
        foreach($times2 as $time2) {
            if($time2->hotel_check_out_time_fr=='PM' && $time2->hotel_check_out_time_hr !="12") {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12) + $time->hotel_check_out_time_mm/60;
            }
            else{
                $modifier2 = ($time2->hotel_check_out_time_hr) + $time->hotel_check_out_time_mm/60;
            }
			$hotel_check_out_time_mm = $time2->hotel_check_out_time_mm;
			$hotel_check_in_time_mm = $time2->hotel_check_in_time_mm;
        }

          
        date_default_timezone_set("Asia/Kolkata");
		
        $start_time = $this->input->post('guest_checkin_time'); //'14:15:19';
        $end_time = $this->input->post('guest_checkout_time');
       
        $start_hour = round($start_time);
        $end_hour = round($end_time);
        
        $final_start_hour = $start_hour - $modifier;
        $final_end_hour = $end_hour - $modifier;
        
        $final_start_time = $final_start_hour.":00:00";
        $final_end_time = $final_end_hour.":00:00";
        
        $checkin_time = date("H:i:s", strtotime($start_time));
        $checkout_time = date("H:i:s", strtotime($end_time));
		
		$start_dt = $this->input->post('std'); //'2017-05-26';
        $end_dt = $this->input->post('end'); //'2017-05-27';
		
        $cust_from_date = date('Y-m-d',strtotime($start_dt))."T".date("H:i:s", strtotime($final_start_time));
        $cust_end_date = date('Y-m-d',strtotime($end_dt))."T".date("H:i:s", strtotime($final_end_time));
        
        $from_date = date('Y-m-d',strtotime($start_dt))."T".date("H:i:s", strtotime($start_time));
        $from_date_final=date('Y-m-d H:i:s', strtotime($from_date) - 60 * 60 * $modifier);

        $end_date = date('Y-m-d',strtotime($end_dt))."T".date("H:i:s", strtotime($end_time));
        $end_date_final=date('Y-m-d H:i:s', strtotime($end_date) - 60 * 60 * $modifier);
		
		
		
		
		
		$guest_details_bookings = array(
		
		"cust_name" => $myguest_name,
		"cust_address" => $myguest_address,
		"cust_contact_no" => $myguest_mobile,
		'cust_from_date' => $from_date_final, //$cust_from_date,
		'cust_end_date' => $end_date_final,
		'cust_from_date_actual' => date("Y-m-d",strtotime($this->input->post('std'))),
		'cust_end_date_actual' =>  date("Y-m-d",strtotime($this->input->post('end'))) ,//$cust_end_date,
		'checkin_time' => $checkin_time,
		'confirmed_checkin_time' => $checkin_time,
		'checkout_time' => $checkout_time,
		'confirmed_checkout_time' => $checkout_time,
		//"cust_contact_no" => $myguest_mobile,
		
		);
		
		
		
		$guest_details_info = array(
					
					"g_name" => $myguest_name,
					"g_pincode" => $myguest_address,
					"g_contact_no" => $myguest_mobile,
					"g_email" => $myguest_mail,
					
					);
		
		$getresult = $this->setting_model->update_guest($guestId,$guest_details_info,$bookingid,$guest_details_bookings);
		
			$resultarray = array(
			
			"succes_result" => $getresult,
			);
		
		header('Content-Type: application/json');
		echo json_encode($resultarray);
		
	}
	
	
	function update_guest_details_booking3(){
		
		$myguest_name = $this->input->post('myguest_name');
		$myguest_address =   $this->input->post('myguest_address');
		$myguest_mobile =  $this->input->post('myguest_mobile');
		$myguest_mail =  $this->input->post('myguest_mail');
		$bookingid =  $this->input->post('bookingid');
		$guestId =  $this->input->post('guestId');
		//$start = date('Y-m-d H:i:s', strtotime($this->input->post('std')));
		//$end =   date('Y-m-d H:i:s', strtotime($this->input->post('end')));
		//$start_actual = date('Y-m-d H:i:s', strtotime($this->input->post('std')));
		//$end_actual =   date('Y-m-d H:i:s', strtotime($this->input->post('end')));
		
		
		
		
		
		$times=$this->dashboard_model->checkin_time();
        foreach($times as $time) {
            if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
                $modifier = ($time->hotel_check_in_time_hr + 12) + $time->hotel_check_in_time_mm/60;
            }
            else{
                $modifier = ($time->hotel_check_in_time_hr) + $time->hotel_check_in_time_mm/60;
            }			
        }
		
        $times2=$this->dashboard_model->checkout_time();
        foreach($times2 as $time2) {
            if($time2->hotel_check_out_time_fr=='PM' && $time2->hotel_check_out_time_hr !="12") {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12) + $time->hotel_check_out_time_mm/60;
            }
            else{
                $modifier2 = ($time2->hotel_check_out_time_hr) + $time->hotel_check_out_time_mm/60;
            }
			$hotel_check_out_time_mm = $time2->hotel_check_out_time_mm;
			$hotel_check_in_time_mm = $time2->hotel_check_in_time_mm;
        }

          
        date_default_timezone_set("Asia/Kolkata");
		
        $start_time = $this->input->post('guest_checkin_time'); //'14:15:19';
        $end_time = $this->input->post('guest_checkout_time');
       
        $start_hour = round($start_time);
        $end_hour = round($end_time);
        
        $final_start_hour = $start_hour - $modifier;
        $final_end_hour = $end_hour - $modifier;
        
        $final_start_time = $final_start_hour.":00:00";
        $final_end_time = $final_end_hour.":00:00";
        
        $checkin_time = date("H:i:s", strtotime($start_time));
        $checkout_time = date("H:i:s", strtotime($end_time));
		
		$start_dt = $this->input->post('std'); //'2017-05-26';
        $end_dt = $this->input->post('end'); //'2017-05-27';
		
        $cust_from_date = date('Y-m-d',strtotime($start_dt))."T".date("H:i:s", strtotime($final_start_time));
        $cust_end_date = date('Y-m-d',strtotime($end_dt))."T".date("H:i:s", strtotime($final_end_time));
        
        $from_date = date('Y-m-d',strtotime($start_dt))."T".date("H:i:s", strtotime($start_time));
        $from_date_final=date('Y-m-d H:i:s', strtotime($from_date) - 60 * 60 * $modifier);

        $end_date = date('Y-m-d',strtotime($end_dt))."T".date("H:i:s", strtotime($end_time));
        $end_date_final=date('Y-m-d H:i:s', strtotime($end_date) - 60 * 60 * $modifier);
		
		
		
		
		
		$guest_details_bookings = array(
		
		"cust_name" => $myguest_name,
		"cust_address" => $myguest_address,
		"cust_contact_no" => $myguest_mobile,
		'cust_from_date' => $from_date_final, //$cust_from_date,
		'cust_end_date' => $end_date_final,
		'cust_from_date_actual' => date("Y-m-d",strtotime($this->input->post('std'))),
		'cust_end_date_actual' =>  date("Y-m-d",strtotime($this->input->post('end'))) ,//$cust_end_date,
		'checkin_time' => $checkin_time,
		'confirmed_checkin_time' => $checkin_time,
		'checkout_time' => $checkout_time,
		'confirmed_checkout_time' => $checkout_time,
		//"cust_contact_no" => $myguest_mobile,
		
		);
		
		
		
		$guest_details_info = array(
					
					"g_name" => $myguest_name,
					"g_pincode" => $myguest_address,
					"g_contact_no" => $myguest_mobile,
					"g_email" => $myguest_mail,
					
					);
		
		$getresult = $this->setting_model->update_guest($guestId,$guest_details_info,$bookingid,$guest_details_bookings);
		
			$resultarray = array(
			
			"succes_result" => $getresult,
			);
		
		header('Content-Type: application/json');
		echo json_encode($resultarray);
		
	}
	
	
	
	
	function edit_hotel()
    {

			 $found = in_array("37",$this->arraay);
		if($found) {
            $data['heading'] = 'Hotel';
            $data['sub_heading'] = 'Edit Hotel';
            $data['description'] = 'Edit Hotel Here';
            $data['active'] = 'edit_hotel';
            $data['country'] = $this->dashboard_model->get_country();
            $data['star'] = $this->dashboard_model->get_star_rating();
            $data['hotel_name'] =  $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
           // echo '<pre>';
			//print_r($data['hotel_name']);// exit;
			$hotel_contact_details_edit = array();

            if($this->input->post()){
                date_default_timezone_set("Asia/Kolkata");
                $hotel_type = $this->input->post('hotel_type');
                $hotel_type_string = "";
               $hotel_type_string =  $hotel_type_string.implode(',' , $hotel_type);

                $guest_option = $this->input->post('guest');
                $guest_option_string = "";
                $guest_option_string =$guest_option_string . implode(',' , $guest_option);

                $broker_option = $this->input->post('broker');
                $broker_option_string = "";
                $broker_option_string = $broker_option_string . implode(',' , $broker_option);

                //Image Upload Start Now

                $this->upload->initialize($this->set_upload_options());

                if ($this->upload->do_upload('image_photo') )
                {
                    $hotel_image = $this->upload->data('file_name');
                    $filename = $hotel_image;
                    $source_path = './upload/' . $filename;
                    $target_path = './upload/hotel/';

                    $config_manip = array(
                    //'file_name' => 'myfile',
                    'image_library' => 'gd2',
                    'source_image' => $source_path,
                    'new_image' => $target_path,
                    'maintain_ratio' => TRUE,
                    'create_thumb' => TRUE,
                    'thumb_marker' => '_thumb',
                    'width' => 100,
                    'height' => 75
                    );
                    $this->load->library('image_lib', $config_manip);

                    if (!$this->image_lib->resize())
                    {
                            $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                            redirect('dashboard/add_compliance');
                    }
                    else
                    {
                            $thumb_name = explode(".", $filename);
                            $hotel_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                            $this->image_lib->clear();


                            $hotel_edit = array(
                                'hotel_name' => $this->input->post('hotel_name'),
                                'hotel_year_established' => $this->input->post('hotel_year_established'),
                                'hotel_ownership_type'  => $this->input->post('hotel_ownership_type'),
                                'hotel_type' => $hotel_type_string,
                                'hotel_rooms' => $this->input->post('hotel_rooms'),
                                'hotel_floor' => $this->input->post('hotel_floor'),
                                'hotel_logo_images' => $hotel_image,
                                'hotel_logo_images_thumb' => $hotel_thumb,
                                'hotel_logo_text' => $this->input->post('images_text')
                            );

                    }

                }

                //Image Upload Section Closed
                else
                {
                    $hotel_edit = array(
                        'hotel_name' => $this->input->post('hotel_name'),
                        'hotel_year_established' => $this->input->post('hotel_year_established'),
                        'hotel_ownership_type'  => $this->input->post('hotel_ownership_type'),
                        'hotel_type' => $hotel_type_string,
                        'hotel_rooms' => $this->input->post('hotel_rooms'),
                        'hotel_floor' => $this->input->post('hotel_floor'),
                        'hotel_logo_text' => $this->input->post('images_text')
                    );
                }


                    $hotel_contact_details_edit = array(
                        'hotel_street1' => $this->input->post('hotel_street1'),
                        'hotel_street2' => $this->input->post('hotel_street2'),
                        'hotel_landmark' => $this->input->post('hotel_landmark'),
                        'hotel_district' => $this->input->post('hotel_district'),
                        'hotel_pincode' => $this->input->post('hotel_pincode'),
                        'hotel_state'=> $this->input->post('hotel_state'),
                        'hotel_country' => $this->input->post('hotel_country'),
                        'hotel_branch_street1' => $this->input->post('hotel_branch_street1'),
                        'hotel_branch_street2' => $this->input->post('hotel_branch_street2'),
                        'hotel_branch_landmark' => $this->input->post('hotel_branch_landmark'),
                        'hotel_branch_district' => $this->input->post('hotel_branch_district'),
                        'hotel_branch_pincode' => $this->input->post('hotel_branch_pincode'),
                        'hotel_branch_state' => $this->input->post('hotel_branch_state'),
                        'hotel_branch_country' => $this->input->post('hotel_branch_country'),
                        'hotel_booking_street1' => $this->input->post('hotel_booking_street1'),
                        'hotel_booking_street2' => $this->input->post('hotel_booking_street2'),
                        'hotel_booking_landmark' => $this->input->post('hotel_booking_landmark'),
                        'hotel_booking_district' => $this->input->post('hotel_booking_district'),
                        'hotel_booking_pincode' => $this->input->post('hotel_booking_pincode'),
                        'hotel_booking_state' => $this->input->post('hotel_booking_state'),
                        'hotel_booking_country' => $this->input->post('hotel_booking_country'),
                        'hotel_frontdesk_name' => $this->input->post('hotel_frontdesk_name'),
                        'hotel_frontdesk_mobile' => $this->input->post('hotel_frontdesk_mobile'),
                        'hotel_frontdesk_mobile_alternative' => $this->input->post('hotel_frontdesk_mobile_alternative'),
                        'hotel_frontdesk_email' => $this->input->post('hotel_frontdesk_email'),
                        'hotel_owner_name' => $this->input->post('hotel_owner_name'),
                        'hotel_owner_mobile' => $this->input->post('hotel_owner_mobile'),
                        'hotel_owner_mobile_alternative' => $this->input->post('hotel_owner_mobile_alternative'),
                        'hotel_owner_email' => $this->input->post('hotel_owner_email'),
                        'hotel_hr_name' => $this->input->post('hotel_hr_name'),
                        'hotel_hr_mobile' => $this->input->post('hotel_hr_mobile'),
                        'hotel_hr_mobile_alternative' => $this->input->post('hotel_hr_mobile_alternative'),
                        'hotel_hr_email' => $this->input->post('hotel_hr_email'),
                        'hotel_accounts_name' => $this->input->post('hotel_accounts_name'),
                        'hotel_accounts_mobile' => $this->input->post('hotel_accounts_mobile'),
                        'hotel_accounts_mobile_alternative' => $this->input->post('hotel_accounts_mobile_alternative'),
                        'hotel_accounts_email' => $this->input->post('hotel_accounts_email'),
                        'hotel_near_police_name' => $this->input->post('hotel_near_police_name'),
                        'hotel_near_police_mobile' => $this->input->post('hotel_near_police_mobile'),
                        'hotel_near_police_mobile_alternative' => $this->input->post('hotel_near_police_mobile_alternative'),
                        'hotel_near_police_email' => $this->input->post('hotel_near_police_email'),
                        'hotel_near_medical_name' => $this->input->post('hotel_near_medical_name'),
                        'hotel_near_medical_mobile' => $this->input->post('hotel_near_medical_mobile'),
                        'hotel_near_medical_mobile_alternative' => $this->input->post('hotel_near_medical_mobile_alternative'),
                        'hotel_near_medical_email' => $this->input->post('hotel_near_medical_email'),
                        'hotel_fax' => $this->input->post('hotel_fax'),
                        'hotel_website' => $this->input->post('hotel_website'),
                        'hotel_working_from' => '',
                        'hotel_working_upto' => ''
                    );

                    $hotel_tax_details_edit = array(
                        'hotel_tax_applied' => $this->input->post('hotel_tax_applied'),
                        'hotel_service_tax' => $this->input->post('hotel_service_tax'),
                        'hotel_luxury_tax' => $this->input->post('hotel_luxury_tax'),
                        'hotel_service_charge' =>$this->input->post('hotel_service_charge'),
                        'hotel_stander_tac' => $this->input->post('hotel_stander_tac'),
						'hotel_vat_tax' => $this->input->post('hotel_vat_tax'),
						'hotel_tax_food_vat' => $this->input->post('hotel_tax_food_vat'),
						'hotel_tax_liquor_vat' => $this->input->post('hotel_tax_liquor_vat'),
						'luxury_tax_slab1_range_to' => $this->input->post('luxury_tax_slab1_range_to'),
						'luxury_tax_slab1_range_from' => $this->input->post('luxury_tax_slab1_range_from'),
						'luxury_tax_slab1' => $this->input->post('luxury_tax_slab1'),
						'luxury_tax_slab2_range_to' => $this->input->post('luxury_tax_slab2_range_to'),
						'luxury_tax_slab2_range_from' => $this->input->post('luxury_tax_slab2_range_from'),
						'luxury_tax_slab2' => $this->input->post('luxury_tax_slab2')
                    );
                   //print_r($hotel_tax_details_edit);exit;
                    $hotel_billing_settings_edit = array(
                        'billing_name' => $this->input->post('billing_name'),
                        'billing_street1' => $this->input->post('billing_street1'),
                        'billing_street2' => $this->input->post('billing_street2'),
                        'billing_landmark' => $this->input->post('billing_landmark'),
                        'billing_district' => $this->input->post('billing_district'),
                        'billing_pincode' => $this->input->post('billing_pincode'),
                        'billing_state' => $this->input->post('billing_state'),
                        'billing_country' => $this->input->post('billing_country'),
                        'billing_email' => $this->input->post('billing_email'),
                        'billing_phone' => $this->input->post('billing_phone'),
                        'billing_fax' => $this->input->post('billing_fax'),
                        'billing_vat' => $this->input->post('billing_vat'),
                        'billing_bank_name' => $this->input->post('billing_bank_name'),
                        'billing_account_no'=> $this->input->post('billing_account_no'),
                        'billing_ifsc_code' => $this->input->post('billing_ifsc_code'),
						'luxury_tax_reg_no' => $this->input->post('luxury_tax_reg_no'),
						'service_tax_no' => $this->input->post('service_tax_no'),
						'vat_reg_no' => $this->input->post('vat_reg_no'),
						'cin_no' => $this->input->post('cin_no'),
						'tin_no' => $this->input->post('tin_no')


                    );
                    $hotel_general_preference_edit = array(

                        'hotel_check_in_time_hr' => $this->input->post('hotel_check_in_time_hr'),
                        'hotel_check_in_time_mm' => $this->input->post('hotel_check_in_time_mm'),
                        'hotel_check_in_time_fr' => $this->input->post('hotel_check_in_time_fr'),
                        'hotel_check_out_time_hr' => $this->input->post('hotel_check_out_time_hr'),
                        'hotel_check_out_time_mm' => $this->input->post('hotel_check_out_time_mm'),
                        'hotel_check_out_time_fr' => $this->input->post('hotel_check_out_time_fr'),
                        'hotel_guest' => $guest_option_string,
                        'hotel_broker'=> $broker_option_string,
						'hotel_buffer_hour' => $this->input->post('buffer_hour'),

                        'hotel_broker_commission' => 0,
                        'hotel_date_format' => $this->input->post('hotel_date_format'),
                        'hotel_time_format' => $this->input->post('hotel_time_format')
                    );




					
					


                //$query = $this->dashboard_model->edit_hotel_1($hotel_edit,$this->input->post('h_id'));
                $query = $this->dashboard_model->edit_hotel_details($hotel_edit,$this->input->post('h_id'));
                $query = $this->dashboard_model->edit_hotel_contact($hotel_contact_details_edit,$this->input->post('h_id'));
                $query = $this->dashboard_model->edit_hotel_tax($hotel_tax_details_edit,$this->input->post('h_id'));
                $query = $this->dashboard_model->edit_hotel_billing($hotel_billing_settings_edit,$this->input->post('h_id'));
                $query = $this->dashboard_model->edit_hotel_preferences($hotel_general_preference_edit,$this->input->post('h_id'));


                $hotel_id = $this->input->post('h_id');

                if (isset($query) && $query) {

                    $this->session->set_flashdata('succ_msg', "Hotel Accounts Info Updated Successfully!");
                    //$data['value'] = $this->dashboard_model->get_c_details($room_id );

                    //$hotel_id = $_GET['hotel_id'];

                    $data['value'] = $this->dashboard_model->get_hotel_details($hotel_id);
					
                    $data['hotels'] = $this->dashboard_model->all_hotels();
                    $data['page'] = 'backend/dashboard/all_hotel_m';
                    $this->load->view('backend/common/index', $data);

                }
                else{
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    //$data['value'] = $this->dashboard_model->get_c_details($c_id);

                   $hotel_id = $_GET['hotel_id'];
                    $data['hotels'] = $this->dashboard_model->all_hotels();
                    $data['value'] = $this->dashboard_model->get_hotel_details($hotel_id);
                    $data['page'] = 'backend/dashboard/all_hotel_m';
                    $this->load->view('backend/common/index', $data);
                }
            }
            else{

                $hotel_id = $_GET['hotel_id'];
                $data['value'] = $this->dashboard_model->get_hotel_details($hotel_id);
              //  echo "hotel".$hotel_id ;
				//print_r($data['value']); exit;
				$data['hotels'] = $this->dashboard_model->all_hotels();
                $data['page'] = 'backend/dashboard/edit_hotel';
                $this->load->view('backend/common/index', $data);

            }

        }
        else {
            redirect('dashboard');
        }
    }
	function hotel_booking_source(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
        $data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'setting';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->get_booking_source();
        $data['page'] = 'backend/dashboard/hotel_booking_source';
		$data['stat']='List of All Active Booking Source';
        $this->load->view('backend/common/index', $data);

    }
	function all_profit_center(){

			 $found = in_array("94",$this->arraay);
		if($found)
		{
			$data['heading'] = 'Setting';
            $data['sub_heading'] = 'Metadata';
			$data['subs_heading'] = 'Profit Center';
            $data['description'] = 'Add Profit Center here';
			$data['active'] = 'setting';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['pc']=$this->dashboard_model->all_pc_active();
			$data['stat']="List of All Active Profit Center";
			$data['page'] = 'backend/dashboard/all_profit_center';
            $this->load->view('backend/common/index', $data);
	}else{
		redirect('dashboard');
	}
	}
	function hotel_area_code(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
        $data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'setting';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->hotel_area_code();
        $data['page'] = 'backend/dashboard/hotel_area_code';
		$data['stat']='List of All Active Booking Source';
        $this->load->view('backend/common/index', $data);

    }
	function booking_status_type(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
		$data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'setting';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));		
		$data['data']=$this->unit_class_model->booking_status_type();
        $data['page'] = 'backend/dashboard/booking_status_type';
        $this->load->view('backend/common/index', $data);

    }
	function housekeeping_status(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
		$data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'setting';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));		
		$data['data']=$this->unit_class_model->housekeeping_status();
        $data['page'] = 'backend/dashboard/housekeeping_status';
        $this->load->view('backend/common/index', $data);

    }
	function marketing_personnel(){
		 //$found = in_array("91",$this->arraay);
		
		//if($found)
		{		
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
        $data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'setting';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->all_marketing_personnel();
        $data['page'] = 'backend/dashboard/marketing_personnel';
		$data['stat']='List of Marketing Personel';
        $this->load->view('backend/common/index', $data);

    }
	/*else{
		redirect('dashboard');
	}*/
	}
	function all_charges(){
			 $data['heading'] = 'Setting';
			$data['sub_heading'] = 'Metadata';
            $data['description'] = '';
            $data['active'] = 'setting';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['charges']=$this->dashboard_model->get_charges();
			$data['page']='backend/dashboard/all_extra_charges';
			$this->load->view('backend/common/index', $data);
}
function add_charges(){
	        $data['heading'] = 'Setting';
            $data['sub_heading'] = 'Metadata';
            $data['description'] = '';
            $data['active'] = 'all_charges';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			 $data['tax_rule_extra'] = $this->dashboard_model->tax_rule_extraCharge($this->session->userdata('user_hotel'));
			if($this->input->post()){
				$charges=array(
				'charge_name'=>$this->input->post('name'),
				'ec_tag'=>$this->input->post('tag'),
				'charge_des'=>$this->input->post('charge_description'),
				'unit_price'=>$this->input->post('u_price'),
				'c_sgst'=>$this->input->post('c_sgst'),
				'c_cgst'=>$this->input->post('c_cgst'),
				'c_igst'=>$this->input->post('c_igst'),
				'c_utgst'=>$this->input->post('c_utgst'),
				'hsn_sac'=>$this->input->post('sac_code'),
				'charge_tax'=>$this->input->post('c_tax'),
				'unit_price_editable'=>$this->input->post('u_p_editable'),
				'tax_editable'=>$this->input->post('tax_editable'),
				'hotel_id'=>$this->session->userdata('user_hotel'),
				
				);
				//print_r($charges);die();
			$query=$this->dashboard_model->add_extra_charges($charges);
			if($query){
				redirect('dashboard/all_charges');
			}
			}
			$data['page']='backend/dashboard/add_extra_charges';
			$this->load->view('backend/common/index', $data);
}
function edit_charges(){
	$id=$this->input->post('id');
	//$id=2;
	$query=$this->dashboard_model->get_charge_by_id($id);

		$data=array(
		'hotel_extra_charges_id'=>$query->hotel_extra_charges_id,
		'charge_name'=>$query->charge_name,
		'charge_des'=>$query->charge_des,
		'unit_price'=>$query->unit_price,
		'charge_tax'=>$query->charge_tax,
		'ec_tag'=>$query->ec_tag,
		'unit_price_editable'=>$query->unit_price_editable,
		'tax_editable'=>$query->tax_editable

		);
	  header('Content-Type: application/json');
        echo json_encode($data);


}
function update_charges(){
	$id=$this->input->post('id');
	$name=$this->input->post('name');
	$ch_des=$this->input->post('ch_des');
	$u_price=$this->input->post('u_price');
	$charge_tax=$this->input->post('charge_tax');
	$tax_editable=$this->input->post('tax_editable');
	$u_price_editable=$this->input->post('u_price_editable');
	$extra_tag = $this->input->post('extra_tag');
	
	$data=array(
	'charge_name'=>$name,
	'charge_des'=>$ch_des,
	'ec_tag'=>$extra_tag,
	'unit_price'=>$u_price,
	'charge_tax'=>$charge_tax,
	'unit_price_editable'=>$u_price_editable,
	'tax_editable'=>$tax_editable,
	);
	$query=$this->dashboard_model->update_charge($id,$data);
	if($query){
		$data=array(
		'data'=>'Updated Succeffully'
		);
	  header('Content-Type: application/json');
	 echo json_encode($data);
	}else{
		return false;
	}

	}
	function delete_extra_charges(){
	$id=$_GET['ch_id'];
	$this->dashboard_model->delete_extra_charge($id);

		$data=array(
		'data'=>'success',
		);

	header('Content-Type: application/json');
	echo json_encode($data);
}
function warehouse_default(){
		$data['heading'] = 'Setting';
            $data['sub_heading'] = ' ';
            $data['description'] = '';
            $data['active'] = 'setting';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['page'] = 'backend/dashboard/warehouse_default';
            $this->load->view('backend/common/index', $data);
	}
	function set_default(){
		$warehouseId=$this->input->post('id');
		$hotel_id=$this->session->userdata('user_hotel');
		$data=$this->dashboard_model->set_default($warehouseId,$hotel_id);                                                                                                         
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	function booking_nature_visit(){
		 $found = in_array("91",$this->arraay);
		
		if($found)
		{		
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
        $data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'setting';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->active_booking_nature_visit($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/booking_nature_visit';
		$data['stat']='List of All Booking Nature Visit';
        $this->load->view('backend/common/index', $data);

    }else{
		redirect('dashboard');
	}
	}
 function edit_group_booking_test(){
	/*echo "Here";
	//echo "<pre>";
	//print_r($_POST);
	echo "</pre>";
	exit;
	*/
	date_default_timezone_set("Asia/Kolkata");
	$gbID = 317;//$this->input->post('groupbookingID');
	$gbDetails = $this->setting_model->get_gbDetails($gbID);
	$guestID =$gbDetails->guestID;
	//print_r($guestID);
	//exit;
	
	$data2 = array(
		'booking_source' => $gbDetails->booking_source,
		'comment' => $gbDetails->booking_notes,
		'preference' => $gbDetails->booking_preference,
		'p_center' => $gbDetails->p_center,
		'nature_visit' => $gbDetails->nature_visit,
	);
	
	$adultRoomRent=$this->input->post('adultRoomRent');
	$afi=$this->input->post('afi');
	$cfi=$this->input->post('cfi');
	$childRoomRent=$this->input->post('childRoomRent');
	
	$hid_group_status=$this->input->post('hid_group_status');
	
	//print_r($dataRoom);
	/*$ab=$this->input->post();
	foreach($ab['sbID'] as $key => $val){
		
		echo '<br>'.$ab['sbID'][$key];
	}*/
	
	//exit;
	$room_st=$this->input->post('room_st');
	$room_vat=$this->input->post('room_vat');
	$room_sc=$this->input->post('room_sc');
	$vat=$this->input->post('vat');
	$tax_status=$this->input->post('tax_status');
	
	$tax=array(
		'room_st'=>$room_st,
		'room_vat'=>$room_vat,
		'room_sc'=>$room_sc,
		'vat'=>$vat,
		'tax_apply'=>$tax_status,
	);
	
	
	
	$hotel_id = $this->session->userdata('user_hotel');
	$taxDB = $this->bookings_model->service_tax($hotel_id);
	$query = $this->setting_model->edit_group_booking($this->input->post(),$data2,$taxDB,$gbID,$guestID,$tax,$hid_group_status);
	

 }


 function edit_group_booking(){
	date_default_timezone_set("Asia/Kolkata");
	$gbID = $this->input->post('groupbookingID');
	$gbDetails = $this->setting_model->get_gbDetails($gbID);
	$guestID =$gbDetails->guestID;
	$data2 = array(
		'booking_source' => $gbDetails->booking_source,		
		'comment' => $gbDetails->booking_notes,
		'preference' => $gbDetails->booking_preference,
		'p_center' => $gbDetails->p_center,
		'nature_visit' => $gbDetails->nature_visit,
	);
	
	$adultRoomRent=$this->input->post('adultRoomRent');
	$afi=$this->input->post('afi');
	$cfi=$this->input->post('cfi');
	$childRoomRent=$this->input->post('childRoomRent');
	
	$hid_group_status=$this->input->post('hid_group_status');
	
	$room_st=$this->input->post('room_st');
	$room_vat=$this->input->post('room_vat');
	$room_sc=$this->input->post('room_sc');
	$vat=$this->input->post('vat');
	$tax_status=$this->input->post('tax_status');
	
	$tax=array(
		'room_st'=>$room_st,
		'room_vat'=>$room_vat,
		'room_sc'=>$room_sc,
		'vat'=>$vat,
		'tax_apply'=>$tax_status,
	);
	
	
	
	$hotel_id = $this->session->userdata('user_hotel');
	$taxDB = $this->bookings_model->service_tax($hotel_id);
	$query = $this->setting_model->edit_group_booking($this->input->post(),$data2,$taxDB,$gbID,$guestID,$tax,$hid_group_status);
	if (isset($query) && $query) {
		$this->session->set_flashdata('succ_msg', "Group Booking updated successfully!");
		$this->session->keep_flashdata('succ_msg','Group Booking updated successfully!');
		redirect('dashboard/booking_edit_group?&group_id='.$gbID );
	} else {
		$this->session->set_flashdata('err_msg', "Database Error. Try again later!");
		$this->session->keep_flashdata('err_msg','Database Error. Try again later!');
		redirect('dashboard/booking_edit_group?&group_id='.$gbID);
	}

 }


    /* Add Admin user Start */
   function add_admin_user(){	
            $data['heading'] = 'Admin';
            $data['sub_heading'] = 'Add Admin User';
            $data['description'] = 'Add Admin User Here';
            $data['active'] = 'add_admin_user';			//echo 'add_admin_user';exit;
            $data['hotel'] = $this->dashboard_model->all_hotel_list();			
			$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));			//print_r( $data['hotel_name']);exit;
            if ($this->input->post()) {
				//echo "<pre>";
				//print_r($this->input->post());
				
				
				//jhexit;
					$admin_details = array(
					   // 'admin_hotel' => implode(",", $this->input->post('hotel_id')),
					    'admin_hotel' =>$this->input->post('hotel_id'),
						'admin_first_name' => $this->input->post('fName'),
						'admin_last_name' => $this->input->post('lName'),
						'admin_email' => $this->input->post('email'),
						'admin_phone1' => $this->input->post('contact'),
						'admin_phone2' => $this->input->post('alt_contact'),
						'admin_user_type' => $this->input->post('admin_type'),
						'admin_status' => 'I',
						'dept' => $this->input->post('dept'),
						'dept_role' => $this->input->post('dept_role'),
						'profit_center' => implode(",", $this->input->post('profit_center')),
						'class' => $this->input->post('class'),
						'employee_id' => $this->input->post('employee_id'),
						'about_me' => $this->input->post('about_me'),	
						
					);
					$login = array(
					    'hotel_id' => implode(",", $this->input->post('hotel_id')),
					    //'user_id' =>$this->session->userdata('user_id'),
						'user_name' => $this->input->post('uName'),
						'user_password' => sha1(md5($this->input->post('password'))),
						'user_email' => $this->input->post('email'),
						'user_type_id' => $this->input->post('admin_type'),
						'admin_role_id' => $this->input->post('admin_role_id'),
						'user_status' => 'I',
						'chain_id' =>$this->session->userdata('chain_id')
					);
					
					$contact = $this->input->post('contact');
                    $text=" New admin created"." "." User Name: ".$this->input->post('uName')." Password:".$this->input->post('password');												//echo "<pre>";						//print_r($admin_details);						//print_r($login);exit;
					$chk = $this->setting_model->add_admin_user1($admin_details,$login);//print_r($chk);
                    if (isset($chk) && $chk) {
						//$this->setting_model->send_sms($text,$contact);
                        $this->session->set_flashdata('succ_msg', "Admin User Added Successfully!");
                        redirect('setting/all_admin_user');
                    } else {
                        $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                        redirect('setting/all_admin_user');
                    }

            } else {
                $data['page'] = 'backend/dashboard/add_admin_user';
                $this->load->view('backend/common/index', $data);
            }

    }
    /* Add Admin user End */



	function checkUser(){
		$val = $_POST["val"];
		echo $val = $this->setting_model->checkUser($val);
	}



    function all_admin_user(){
		
		//echo "<pre>";
		//print_r($this->session->all_userdata());exit;
		$data['heading'] = 'Admin';
		$data['sub_heading'] = 'All Admin User';
		$data['description'] = 'All Admin User Here';
		$data['active'] = 'all_admin_user';
		$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['ad'] = $this->setting_model->all_admin_user();
		/*echo "<pre>";
		print_r($data['ad']);
		exit();*/
		$data['page'] = 'backend/dashboard/view_admin_user';
		$this->load->view('backend/common/index', $data); 
    }
	
	function backup_database(){
		
		//echo "<pre>";
		//print_r($this->session->all_userdata());exit;
		$data['heading'] = 'Setting';
		$data['sub_heading'] = 'Databse Backup';
		$data['description'] = 'Databse Backup';
		$data['active'] = 'setting';
		$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		/*echo "<pre>";
		print_r($data['ad']);
		exit();*/
		$data['page'] = 'backend/dashboard/backup';
		$this->load->view('backend/common/index', $data); 
    }

    /* Update Admin user Start */
    function edit_admin_user(){	
            $data['heading'] = 'Admin';
            $data['sub_heading'] = 'Update Admin User';
            $data['description'] = 'Update Admin User Here';
            $data['active'] = 'edit_admin_user';
            $data['hotel'] = $this->dashboard_model->all_hotel_list();
			$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if ($this->input->post()) {
				//echo "<pre>";
				//print_r($this->input->post());
				//exit;
					$admin_details = array(
					    'admin_hotel' =>$this->input->post('hotel_id'),
						'admin_first_name' => $this->input->post('fName'),
						'admin_last_name' => $this->input->post('lName'),
						'admin_email' => $this->input->post('email'),
						'admin_phone1' => $this->input->post('contact'),
						'admin_phone2' => $this->input->post('alt_contact'),
						'admin_user_type' => $this->input->post('admin_type'),
						'admin_status' => $this->input->post('admin_status'),
						'dept' => $this->input->post('dept'),
						'dept_role' => $this->input->post('dept_role'),
						'profit_center' => implode(",", $this->input->post('profit_center')),
						'class' => $this->input->post('class'),
						'employee_id' => $this->input->post('employee_id'),
						'about_me' => $this->input->post('about_me'),												
					);
					$login = array(
					    'hotel_id' =>$this->input->post('hotel_id'),
						'user_email' => $this->input->post('email'),
						'user_type_id' => $this->input->post('admin_type'),
						'admin_role_id' => $this->input->post('admin_role_id'),
						'user_status' => $this->input->post('admin_status')
					);
					$admin_id = $this->input->post('admin_id');
					$login_id = $this->input->post('login_id');	
					$chk = $this->setting_model->edit_admin_user($admin_details,$login,$admin_id,$login_id);
                    if (isset($chk) && $chk) {
                        $this->session->set_flashdata('succ_msg', "Admin User updated Successfully!");
                        redirect('setting/all_admin_user');
                    } else {
                        $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                        redirect('setting/all_admin_user');
                    }

            } else {
				$adUser = $_GET['id'];
				$data['ad'] = $this->setting_model->getAdminUserById($adUser);
                $data['page'] = 'backend/dashboard/edit_admin_user';
                $this->load->view('backend/common/index', $data);
            }

    }
    /* Update Admin user End */


    function delete_admin_user(){
        $admin_id = $_POST['admin_id'];
		$login_id = $_POST['login_id'];
        $query = $this->setting_model->delete_admin_user($admin_id,$login_id);
         $data= array(
            'data' =>"sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    
	}
	
	function showAgentDetails(){
            $agent_type = $this->input->post('val');			
             $query = $this->setting_model->showAgentDetails($agent_type);
			//echo "<pre>";
			if($query!=0){
			if ($agent_type == 'B'){	
				echo '<option value="0" selected="">Select Broker Name</option>';
					foreach($query as $qry){
						echo '<option value="'.$qry["b_id"].'">'.$qry["b_name"].'</option>';
					}
				echo '<label></label><span class="help-block">Select Broker Name *</span>';
			} else {
				echo '<option value="0" selected="">Select OTA Name</option>';
					foreach($query as $qry){
						echo '<option value="'.$qry["channel_id"].'">'.$qry["channel_name"].'</option>';
			        }
				echo '<label></label><span class="help-block">Select OTA Name *</span>';
			}
			}else{
				echo $a=0;
			}

    }	
	
    function showAgentCommission(){
		$id = $this->input->post('id');
        $agent_type = $this->input->post('agent_type');
        $agent = $this->setting_model->showAgentCommission($agent_type,$id);
        foreach($agent as $row){
			if ($agent_type == 'B'){
				$commission = $row->broker_commission;
			} else {
				$commission = $row->channel_commission;
			}	
        }
        $data = array(
            'commission' => round($commission)
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }	
	
    function updateAgentCommission(){
		$gid = $this->input->post('gID');
        $cr = $this->input->post('cr');
		$ca = $this->input->post('ca');
		//$amt = $this->input->post('amt');
        $agent = $this->setting_model->updateAgentCommission($cr,$ca,$gid);
        $data = array(
            'val' => $agent
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }	
	
	function get_arrival(){
		
		$b_source = $this->input->post('b_source');
        $b_stat = $this->input->post('b_stat');
		$status_text = $this->input->post('status_text');
		$date_status = $this->input->post('date_status');
		$recent_bookings = $this->setting_model->get_arrival($b_source,$b_stat,$status_text,$date_status);
		if(isset($recent_bookings) && $recent_bookings!=''){
			
			//print_r($recent_bookings);
			//exit;
			$output = '<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
			
                                        
										
					<table class="table table-striped table-hover">
                                            <thead>
                                                <tr style="background-color:#F9F9F9 !important;">
                                                    <th> Guest </th>
                                                    <th> Status </th>
                                                    <th> Checkin/Checkout Date </th>
                                                    <th> Room </th>
                                                    <th> Days </th>
                                                    <th>  </th>
                                                </tr>
                                            </thead>
                                            <tbody>';
                                                
                                                
                                        
											 if(isset($recent_bookings) && $recent_bookings){
													$k=0;
												
													$deposit=0;
					
												foreach($recent_bookings as $recent_bookings){
													
												if($recent_bookings->group_id !="0"){
						$booking_id = '<span class="fa fa-group" ><span style="font-family:Open Sans,sans-serif; font-size:11px;padding-left:6px; display: inline-block;">'.'HM0'.$this->session->userdata('user_hotel').'0'.$recent_bookings->booking_id;
						
						$hid_grp_id = '<input type="hidden" id="dash_grp_id" class="dash_grp_id" value=' . $recent_bookings->group_id . '>';

						$hid_bkng_id = '<input type="hidden" id="dash_booking_id" class="dash_booking_id" value=' . $recent_bookings->booking_id . '>';
						
						
					} 
					else
					{
						$booking_id = '<span class="fa fa-user" ><span style="font-family:Open Sans,sans-serif; font-size:11px;padding-left:6px; display: inline-block;">'.'HM0'.$this->session->userdata('user_hotel').'0'.$recent_bookings->booking_id;	


						
						$hid_bkng_id = '<input type="hidden" id="dash_booking_id" class="dash_booking_id" value=' . $recent_bookings->booking_id . '>';

						$hid_grp_id = '<input type="hidden" id="dash_grp_id" class="dash_grp_id" value=' . $recent_bookings->group_id . '>';
						
					}
												
												
													
													
													$k++;
													
													$room_number=$this->dashboard_model->get_room_number($recent_bookings->room_id);
													
													foreach($room_number as $r){ 
														$room = $r->room_no; 
													}
												
													$stl = '';
													$s_id = $recent_bookings->booking_status_id;
													$color = $this->unit_class_model->status_color($s_id);
													
													if($recent_bookings->group_id != 0)
														$stl = 'color:orange;';
													
													if(1 == 1){
													
														$output .= '<tr>							
														<td><a>'.$recent_bookings->cust_name.'</a> <span style="color:#AAAAAA;">('.$recent_bookings->no_of_guest.')<br>'.$booking_id.'</span></td>
														<td> <label class="" style="color:'.$color->bar_color_code.'">'.$color->booking_status.'</label></td>
														<td>'.date("jS F Y",strtotime($recent_bookings->cust_from_date)).'/'.date("jS F Y",strtotime($recent_bookings->cust_end_date)).'</td>
														<td style="'.$stl.'"><label class="" style="'.$stl.'">'.$room.'</label></td>
														<td> '.$recent_bookings->stay_days.'</td>
														<td>' . $hid_bkng_id . $hid_grp_id . '<a class="click_print" id="click_print_ajax_id"><i style="color:#666666; font-size:14px" class="fa fa-print"  aria-hidden="true"></i></a>' . ' <a class="go_to_details"> <i style="color:#666666; font-size:14px" class="fa fa-sign-in" aria-hidden="true"></i></a>  <i style="color:#666666; font-size:14px" class="fa fa-arrow-circle-left" aria-hidden="true"></i>  <i style="color:#666666; font-size:14px" class="fa fa-times" aria-hidden="true"></i>
                                                        </td>																</tr>';	
													}
													
												}
											 }
										$output .='</tbody></table></div>';					
          
		}
       
	   echo $output;
		
	}// end of function 
	
	
    function get_day_revenue_bookings(){

       $t_date = $this->input->post('trandate');
	   $t_type = $this->input->post('t_stat');
	    $t_modes = $this->input->post('tran_mode');
	   
        date_default_timezone_set('Asia/Kolkata');
       

		$i=0;
		$results = $this->setting_model->get_day_revenue_bookings($t_date,$t_type,$t_modes);
		$output =  '<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
		  
			<table class="table table-striped table-hover">
                            <thead>
                                <tr style="background-color:#F9F9F9 !important;">
                                    <th> ID </th>
                                    <th> Date - Time </th>
                                    <th> Type </th>
                                    <th> Mode </th>
                                    <th> Amount </th>
                                    <th>  </th>
                                </tr>
                            </thead>
						<tbody>';
		if(isset($results) && $results!=''){
			$transaction_type_text = '';
			
                             
						foreach($results as $result){
							
							if($result->t_id){
								
								$tran_id = "TA0".$this->session->userdata('user_hotel')."/".$this->session->userdata('user_id')."/".$result->t_id;
								
					$dec = 'none';
					if($result->t_status == 'Cancel')
						$dec = 'line-through';
					//$d = date("d",strtotime($transaction->t_date)).date("m",strtotime($transaction->t_date)).date("y",strtotime($transaction->t_date));
                   
					$t_id = '<span style="text-decoration:'.$dec.'">'.$tran_id.'</span>';
					if($result->t_status == 'Done'){
						$t_id .= ' <i style="color:#9BCA3B;" class="fa fa-check" aria-hidden="true"></i>';
						//$tpa = $tpa + $transaction->t_amount;
					}
					if($result->t_status == 'Pending')
						$t_id .= ' <i style="color:#FFAF4A;" class="fa fa-hourglass-start" aria-hidden="true"></i>';
					if($result->t_status == 'Cancel')
						$t_id .= ' <i style="color:#F5695E;" class="fa fa-times" aria-hidden="true"></i>';
					
							}
							
							
							
							
							if(isset($result->transaction_type_id) && $result->transaction_type_id!=''){
								
								$transaction_type_status = $this->setting_model->get_transaction_type_cat($result->transaction_type_id);
								if(isset($transaction_type_status) && $transaction_type_status!=''){
									//$transaction_type_text = 
									if($transaction_type_status->hotel_transaction_type_cat == 'Income'){
										
										$tran_type ='<span style="color:#127561;">'.$result->transaction_type;
									}
									else if($transaction_type_status->hotel_transaction_type_cat == 'Expense'){
										
										$tran_type ='<span style="color:#A40C0C;">'.$result->transaction_type;
									}
									else if($transaction_type_status->hotel_transaction_type_cat == 'Investment'){
										
										$tran_type =$result->transaction_type.""."(INV)";
									}
									else{
										$tran_type =$result->transaction_type.""."(EQU)";
									}
								}
								else{
											
											$tran_type ='<span style="color:#aaaaaa;">'.'N/A';
								}
							}
							else{
								
								$tran_type ='<span style="color:#aaaaaa;">'.'N/A';
							}
							
							
							$output .= '<tr>							
														<td><a>'.$t_id.'</a></td>											
														<td>'.date("jS F Y h:i:s",strtotime($result->t_date)).'</td>
														<td>'.$tran_type.'</td>
														<td>'.$result->t_payment_mode.'</td>
														<td>'.$result->t_amount.'</td>																			</tr>';	
						}				
						
							
                            
                                
								
							$output .=	'</tbody></table></div>';
                        
          echo $output;
							
			
		}
		else{
			 echo $output;
			
		}

      
     

    }
	
	
	
	
	   function get_revenue_specific_date(){

       $start_date_tran = $this->input->post('start_date_tran');
	   $end_date_tran = $this->input->post('end_date_tran');
	   $t_type = $this->input->post('t_stat');
	   $t_modes = $this->input->post('tran_mode');
	   
        date_default_timezone_set('Asia/Kolkata');
       

		$i=0;
		$results = $this->setting_model->get_day_revenue_bookings($start_date_tran,$end_date_tran,$t_type,$t_modes);
		$output =  '<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
		  
			<table class="table table-striped table-hover">
                            <thead>
                                <tr style="background-color:#F9F9F9 !important;">
                                    <th> ID </th>
                                    <th> Date - Time </th>
                                    <th> Type </th>
                                    <th> Mode </th>
                                    <th> Amount </th>
                                    <th>  </th>
                                </tr>
                            </thead>
						<tbody>';
		if(isset($results) && $results!=''){
			$transaction_type_text = '';
			
                             
						foreach($results as $result){
							
							if($result->t_id){
								
								$tran_id = "TA0".$this->session->userdata('user_hotel')."/".$this->session->userdata('user_id')."/".$result->t_id;
								
					$dec = 'none';
					if($result->t_status == 'Cancel')
						$dec = 'line-through';
					//$d = date("d",strtotime($transaction->t_date)).date("m",strtotime($transaction->t_date)).date("y",strtotime($transaction->t_date));
                   
					$t_id = '<span style="text-decoration:'.$dec.'">'.$tran_id.'</span>';
					if($result->t_status == 'Done'){
						$t_id .= ' <i style="color:#9BCA3B;" class="fa fa-check" aria-hidden="true"></i>';
						//$tpa = $tpa + $transaction->t_amount;
					}
					if($result->t_status == 'Pending')
						$t_id .= ' <i style="color:#FFAF4A;" class="fa fa-hourglass-start" aria-hidden="true"></i>';
					if($result->t_status == 'Cancel')
						$t_id .= ' <i style="color:#F5695E;" class="fa fa-times" aria-hidden="true"></i>';
					
							}
							
							
							
							
							if(isset($result->transaction_type_id) && $result->transaction_type_id!=''){
								
								$transaction_type_status = $this->setting_model->get_transaction_type_cat($result->transaction_type_id);
								if(isset($transaction_type_status) && $transaction_type_status!=''){
									//$transaction_type_text = 
									if($transaction_type_status->hotel_transaction_type_cat == 'Income'){
										
										$tran_type ='<span style="color:#127561;">'.$result->transaction_type;
									}
									else if($transaction_type_status->hotel_transaction_type_cat == 'Expense'){
										
										$tran_type ='<span style="color:#A40C0C;">'.$result->transaction_type;
									}
									else if($transaction_type_status->hotel_transaction_type_cat == 'Investment'){
										
										$tran_type =$result->transaction_type.""."(INV)";
									}
									else{
										$tran_type =$result->transaction_type.""."(EQU)";
									}
								}
								else{
											
											$tran_type ='<span style="color:#aaaaaa;">'.'N/A';
								}
							}
							else{
								
								$tran_type ='<span style="color:#aaaaaa;">'.'N/A';
							}
							
							
							$output .= '<tr>							
														<td><a>'.$t_id.'</a></td>											
														<td>'.date("jS F Y h:i:s",strtotime($result->t_date)).'</td>
														<td>'.$tran_type.'</td>
														<td>'.$result->t_payment_mode.'</td>
														<td>'.$result->t_amount.'</td>																			</tr>';	
						}				
						
							
                            
                                
								
							$output .=	'</tbody></table></div>';
                        
          echo $output;
							
			
		}
		else{
			 echo $output;
			
		}

      
     

    }
	

	
function ftEdit(){
	$ft=$_POST['footer'];
	$hotel_id=$this->session->userdata('user_hotel');
	$data=array(
	'ft_text'=>$ft,
	'hotel_id'=>$hotel_id,
	);
	$query=$this->dashboard_model->ft_invoice_settings($data);

}
function ftEditGB(){
	$ft=$_POST['footer'];
	$hotel_id=$this->session->userdata('user_hotel');
	$data=array(
	'GBft_text'=>$ft,
	'hotel_id'=>$hotel_id,
	);
	$query=$this->dashboard_model->ft_invoice_settings($data);

}


function get_guest_in_house(){
	
	$guest_date = $this->input->post('guest_date');
	$g_type = $this->input->post('g_type');
	$g_source = $this->input->post('g_source');
	
	$guestdetails =  $this->setting_model->guest_in_house_ajax($guest_date,$g_type,$g_source);
	//print_r($get_guest_result);
	if(isset($guestdetails) && $guestdetails!=''){
		
		 $output = '<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">';
			
            
			
							
										//print_r($guestdetails);
											 if(isset($guestdetails) && $guestdetails!=''){
													$k=0;
												
													$deposit=0;
													//;
					
												foreach($guestdetails as $guestdetails1){
													$k++;
													//print_r($recent_bookings1);
													//echo ' <br></br> ';
													if(isset($guestdetails1->room_no) && $guestdetails1->room_no!=''){
														
														$room = $guestdetails1->room_no;
													}
													else{
														$room ='No Room';
													}
													

												
													$stl = '';
													if(isset($guestdetails1->booking_status_id) && $guestdetails1->booking_status_id!=''){
														
														$s_id = $guestdetails1->booking_status_id;
														$color = $this->unit_class_model->status_color($s_id);
													}
													else{
														$s_id = '';
														$color = '';
													}
												if(isset($guestdetails1->group_id) && $guestdetails1->group_id!=''){	
													
													if($guestdetails1->group_id != 0)
														$stl = 'color:orange;';
													else{
														$stl = 'color:yellow;';
													}
												}
												else{
													$stl = 'color:yellow;';
													
												}
												
												if(isset($guestdetails1->g_city) && $guestdetails1->g_city!=''){
													
													$g_city = $guestdetails1->g_city;
												}
												else
												{
													$g_city = 'No Info';
												}
												if(isset($guestdetails1->g_state) && $guestdetails1->g_state!=''){
													
													$g_state = $guestdetails1->g_state;
												}
												else
												{
													$g_state = 'No Info';
												}
												if(isset($guestdetails1->g_contact_no) && $guestdetails1->g_contact_no!=''){
													
													$g_contact_no = $guestdetails1->g_contact_no;
												}
												else
												{
													$g_contact_no = 'No Info';
												}
												if(isset($guestdetails1->g_email) && $guestdetails1->g_email!=''){
													
													$g_email = $guestdetails1->g_email;
												}
												else
												{
													$g_email = 'No Info';
												}
												
													if(isset($guestdetails1->g_name) && $guestdetails1->g_name!=''){
														
														$cust_name = $guestdetails1->g_name;
													//	$color = $this->unit_class_model->status_color($s_id);
													}
													else{
														$cust_name = 'No info';
													}
													if(isset($guestdetails1->no_of_guest) && $guestdetails1->no_of_guest!=''){
														
														$nog = $guestdetails1->no_of_guest;
													//	$color = $this->unit_class_model->status_color($s_id);
													}
													else{
														$nog = 'No info';
													}
													
													
													if($guestdetails1->docVerif == 1){
                                                            $docverification = '<span class="mt-action-dot"><i style="color:red;" class="fa fa-circle" aria-hidden="true"></i></span>';
                                                            
                                                        }	
                                                        
                                                        else{
                                                            $docverification =  '<span class="mt-action-dot"><i style="color:#7BCD8A;" class="fa fa-circle" aria-hidden="true"></i></span>';
                                                        }
													
													
													
												if(isset($guestdetails1->group_id) && $guestdetails1->group_id!=''){	
													
													if($guestdetails1->group_id != 0)
														$bktypes = '<div class="mt-comment-text "> <i class ="fa fa-group" style="font-weight:400;"></i> BK'.$guestdetails1->hotel_id.'/0'.$guestdetails1->user_id.'/'.$guestdetails1->booking_id.' (GBK0'.$guestdetails1->hotel_id.'/0'.$guestdetails1->user_id.'/'.$guestdetails1->group_id.') | ';
													else{
														$bktypes = '<div class="mt-comment-text"> <i class ="fa fa-user" style="font-weight:400;"></i> BK0'.$guestdetails1->hotel_id.'/0'.$guestdetails1->user_id.'/'.$guestdetails1->booking_id.' | ';
													}
												}
												else{
													$bktypes = '<div class="mt-comment-text"> <i style="font-weight:400;" class ="fa fa-user"></i> BK'.$guestdetails1->hotel_id.'/0'.$guestdetails1->user_id.'/'.$guestdetails1->booking_id.' | ';
													
												}
													
													if(isset($guestdetails1->group_id) && $guestdetails1->group_id!=''){	
													
													if($guestdetails1->group_id != 0)
														$stl = 'color:orange;';
													else{
														$stl = 'color:#3D4B76;';
													}
												}
												else{
													$stl = 'color:#3D4B76;';
													
												}
													
													if(1 == 1){
													
														$output .= '<div class="mt-comments">
																<div class="mt-comment">';
																
						$output .= '<div class="mt-comment-img">';
					//upload/guest/thumbnail/photo/
						if(isset($guestdetails1->g_photo_thumb) && $guestdetails1->g_photo_thumb!=''){
							$img = base_url().'upload/guest/thumbnail/photo/'.$guestdetails1->g_photo_thumb; 
							
							$output .= "<img src='".$img."'>";
							
						}
						else{
							$img =  base_url().'assets/pages/media/users/avatar1.png';
							$output .= "<img src='".$img."'>";
						}
						
														$output .='</div>';
													
												 $output .='<div class="mt-comment-body">
                                                        <div class="mt-comment-info">';
														
														$output .='<span class="mt-comment-author">'.$cust_name.' </span><span style="color:#AAAAAA;"> &nbsp('.$nog.')</span>';
														
														$output .= '&nbsp
															<label class="mt-action-desc">'.$guestdetails1->g_type.'</label>
															&nbsp';
															
														$output .= $docverification.'
                                                            <span class="mt-comment-date">'.date("jS F Y H:i:s",strtotime($guestdetails1->check_in_date)).' '.' - '.' '.date("jS F Y H:i:s",strtotime($guestdetails1->check_out_date)).'</span>';
															
														$output .= '</div>';
														
														$output .= $bktypes.'</span>RNO'.'-'.'<span style='.$stl.'>'.$room .'</span> | '. ' '.$g_city. ' | '. $g_state. ' | '. $g_contact_no.' | '.$g_email .'</div>';
														
														if(isset($guestdetails1->status) && $guestdetails1->status != ''){
															$statusG = $guestdetails1->status;
															if($statusG == 'checkin')
																$colS = '#0AA547';
															else if($statusG == 'checkout')
																$colS = '#62ADB1';
															else
																$colS = '#19B5F0';
														}
														else{
															$statusG = 'n/a - Please check';
															$colS = 'red';
														}
														
														$output .= '<div class="mt-comment-details">
                                                            <span style="color:'.$colS.';" class="mt-comment-status mt-comment-status-pending">'.$statusG.' </span>
															<span style="color:#AAAAAA"> &nbsp'.$guestdetails1->stayPurpose.'</span>
                                                            <ul class="mt-comment-actions">
                                                                <li>
                                                                    <a href="#">Quick Edit</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">Go to Booking</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">Check out Guest</a>
                                                                </li>
                                                            </ul>
                                                        </div>';
													
														
														$output .= '
																</div>
														';
														
														$output .= '</div>
																</div>
														';
													}
													
												}
											 }
										$output .='</div>';
										
			 
				echo $output;		
          
		
	}
	
}// end of function 



function get_guest_specific_date(){
	
	$start_date_guest = date('Y-m-d', strtotime($this->input->post('start_date_guest')));
	$end_date_guest = date('Y-m-d', strtotime($this->input->post('end_date_guest')));
	$g_type = $this->input->post('g_type');
	$g_source = $this->input->post('g_source');
	
	$guestdetails =  $this->setting_model->guest_specific_date_ajax($start_date_guest,$end_date_guest,$g_type,$g_source);
	
	//print_r($get_guest_result);
	if(isset($guestdetails) && $guestdetails!=''){
		
		 $output = '<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">';
			
           // $daterange = "Guest details between".$start_date_guest. " to ".$end_date_guest; 
			
							
										//print_r($guestdetails);
											 if(isset($guestdetails) && $guestdetails!=''){
													$k=0;
												
													$deposit=0;
													//;
					
												foreach($guestdetails as $guestdetails1){
													$k++;
													//print_r($recent_bookings1);
													//echo ' <br></br> ';
													if(isset($guestdetails1->room_no) && $guestdetails1->room_no!=''){
														
														$room = $guestdetails1->room_no;
													}
													else{
														$room ='No Room';
													}
													

												
													$stl = '';
													if(isset($guestdetails1->booking_status_id) && $guestdetails1->booking_status_id!=''){
														
														$s_id = $guestdetails1->booking_status_id;
														$color = $this->unit_class_model->status_color($s_id);
													}
													else{
														$s_id = '';
														$color = '';
													}
												if(isset($guestdetails1->group_id) && $guestdetails1->group_id!=''){	
													
													if($guestdetails1->group_id != 0)
														$stl = 'color:orange;';
													else{
														$stl = 'color:yellow;';
													}
												}
												else{
													$stl = 'color:yellow;';
													
												}
												
												if(isset($guestdetails1->g_city) && $guestdetails1->g_city!=''){
													
													$g_city = $guestdetails1->g_city;
												}
												else
												{
													$g_city = 'No Info';
												}
												if(isset($guestdetails1->g_state) && $guestdetails1->g_state!=''){
													
													$g_state = $guestdetails1->g_state;
												}
												else
												{
													$g_state = 'No Info';
												}
												if(isset($guestdetails1->g_contact_no) && $guestdetails1->g_contact_no!=''){
													
													$g_contact_no = $guestdetails1->g_contact_no;
												}
												else
												{
													$g_contact_no = 'No Info';
												}
												if(isset($guestdetails1->g_email) && $guestdetails1->g_email!=''){
													
													$g_email = $guestdetails1->g_email;
												}
												else
												{
													$g_email = 'No Info';
												}
												
													if(isset($guestdetails1->g_name) && $guestdetails1->g_name!=''){
														
														$cust_name = $guestdetails1->g_name;
													//	$color = $this->unit_class_model->status_color($s_id);
													}
													else{
														$cust_name = 'No info';
													}
													if(isset($guestdetails1->no_of_guest) && $guestdetails1->no_of_guest!=''){
														
														$nog = $guestdetails1->no_of_guest;
													//	$color = $this->unit_class_model->status_color($s_id);
													}
													else{
														$nog = 'No info';
													}
													
												if(isset($guestdetails1->group_id) && $guestdetails1->group_id!=''){	
													
													if($guestdetails1->group_id != 0)
														$bktypes = '<div class="mt-comment-text "> <i class ="fa fa-group" style="font-weight:400;"></i> BK'.$guestdetails1->hotel_id.'/0'.$guestdetails1->user_id.'/'.$guestdetails1->booking_id.' (GBK0'.$guestdetails1->hotel_id.'/0'.$guestdetails1->user_id.'/'.$guestdetails1->group_id.') | ';
													else{
														$bktypes = '<div class="mt-comment-text"> <i class ="fa fa-user" style="font-weight:400;"></i> BK0'.$guestdetails1->hotel_id.'/0'.$guestdetails1->user_id.'/'.$guestdetails1->booking_id.' | ';
													}
												}
												else{
													$bktypes = '<div class="mt-comment-text"> <i style="font-weight:400;" class ="fa fa-user"></i> BK'.$guestdetails1->hotel_id.'/0'.$guestdetails1->user_id.'/'.$guestdetails1->booking_id.' | ';
													
												}
													
													if(isset($guestdetails1->group_id) && $guestdetails1->group_id!=''){	
													
													if($guestdetails1->group_id != 0)
														$stl = 'color:orange;';
													else{
														$stl = 'color:#3D4B76;';
													}
												}
												else{
													$stl = 'color:#3D4B76;';
													
												}
												
												if($guestdetails1->docVerif == 1){
                                                            $docverification = '<span class="mt-action-dot"><i style="color:red;" class="fa fa-circle" aria-hidden="true"></i></span>';
                                                            
                                                        }	
                                                        
                                                        else{
                                                            $docverification =  '<span class="mt-action-dot"><i style="color:#7BCD8A;" class="fa fa-circle" aria-hidden="true"></i></span>';
                                                        }
												
													
													if(1 == 1){
													
														$output .= '<div class="mt-comments">
																<div class="mt-comment">';
																
						$output .= '<div class="mt-comment-img">';
					
						if(isset($guestdetails1->g_photo_thumb) && $guestdetails1->g_photo_thumb!=''){
							$img = base_url().'upload/guest/thumbnail/photo/'.$guestdetails1->g_photo_thumb; 
							
							$output .= "<img src='".$img."'>";
							
						}
						else{
							$img =  base_url().'assets/pages/media/users/avatar1.png';
							$output .= "<img src='".$img."'>";
						}
						
														$output .='</div>';
													
												 $output .='<div class="mt-comment-body">
                                                        <div class="mt-comment-info">';
														
														$output .='<span class="mt-comment-author">'.$cust_name.' </span><span style="color:#AAAAAA;"> &nbsp('.$nog.')</span>';
														
														$output .= '&nbsp
															<label class="mt-action-desc">'.$guestdetails1->g_type.'</label>
															&nbsp';
															
														$output .= $docverification.'
                                                            <span class="mt-comment-date">'.date("jS F Y H:i:s",strtotime($guestdetails1->check_in_date)).' '.' - '.' '.date("jS F Y H:i:s",strtotime($guestdetails1->check_out_date)).'</span>';
															
														$output .= '</div>';
														
														$output .= $bktypes.'</span>RNO'.'-'.'<span style='.$stl.'>'.$room .'</span> | '. ' '.$g_city. ' | '. $g_state. ' | '. $g_contact_no.' | '.$g_email .'</div>';
														
														if(isset($guestdetails1->status) && $guestdetails1->status != ''){
															$statusG = $guestdetails1->status;
															if($statusG == 'checkin')
																$colS = '#0AA547';
															else if($statusG == 'checkout')
																$colS = '#62ADB1';
															else
																$colS = '#19B5F0';
														}
														else{
															$statusG = 'n/a - Please check';
															$colS = 'red';
														}
														
														$output .= '<div class="mt-comment-details">
                                                            <span style="color:'.$colS.';" class="mt-comment-status mt-comment-status-pending">'.$statusG.' </span>
															<span style="color:#AAAAAA"> &nbsp'.$guestdetails1->stayPurpose.'</span>
                                                            <ul class="mt-comment-actions">
                                                                <li>
                                                                    <a href="#">Quick Edit</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">Go to Booking</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">Check out Guest</a>
                                                                </li>
                                                            </ul>
                                                        </div>';
													
														
														$output .= '
																</div>
														';
														
														$output .= '</div>
																</div>
														';
													}
													
												}
											 }
										$output .='</div>';
										
			 
				echo $output;		
          
		
	}
	
}// end of function 















function current_day_notifications(){
	
	$notice_date = $this->input->post('n_date');
	
	if($notice_date == 'today'){
		
		$notifications=  $this->dashboard_model->all_compliance_limit();
		$pending =  $this->dashboard_model->all_pending_by_date($notice_date);
		
	}
	else{
		
	$notifications=  $this->dashboard_model->all_compliance();
	
	$pending =  $this->dashboard_model->all_pending();
		
	}
	
	
	
	$i=1;
	
	
	
	$output = '<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
         
              <ul class="task-list">';
	
			
			
			// for complaince
								 
                
                                if(isset($notifications) && $notifications!=''){
                                foreach($notifications as $noti){

                                if(date('Y-m-d',strtotime($noti->c_valid_upto."-".$noti->c_primary_notif_period." days"))<=date("Y-m-d")){

                                
              $output .=  '<li> <a href='.base_url().'dashboard/edit_compliance?c_id='.$noti->c_id.'> <span class="time" style="color: black">'.$noti->c_renewal.'</span> <span class="details"> <span class="label label-sm label-icon label-warning"> <i class="fa fa-hourglass-half"></i> </span>'.$noti->c_name.'requires renewal</span> </a> 
				</li>';
}
} //endforeach
				}//endif
              
            
			// for booking .. 
			
				if(isset($pending)&&$pending){

                                foreach($pending as $pend) {
                                    $booking_id_noti = 'HM0' . $this->session->userdata('user_hotel') . '00' . $pend->booking_id;
                                   
              $output .=  '<li> <a href='.base_url().'dashboard/booking_edit?b_id='.$pend->booking_id.'> <span class=time>'.$booking_id_noti.'</span> <span class="details"> <span class="label label-sm label-icon label-warning" style="background-color: #33D2E3"> <i class="fa fa-bed"></i> </span>'.$pend->cust_name.'s booking goes to pending';
                
                                        if($pend->booking_status_id_secondary ==9){
                                      $output .=   'as payment is due';
                                        }else {
                                       $output .=  'as he/she hasnt  checked in';
                                        }

                                        
                   $output .= '</span></a></li>';
              
                                }
								}
			
			
			
         $output .=  '</ul></div>';
        
			
		
			echo $output;
      
}

function auto_invoice_settings(){
    
            $data['heading'] = 'Setting';
            $data['sub_heading'] = '';
            $data['description'] = 'Invoice Settings';
            $data['active'] = 'setting';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page']='backend/dashboard/auto_invoice_setting';
			$this->load->view('backend/common/index', $data);
    
}


function auto_generate_inv_id(){
	
	
	$invoice_auto = $this->setting_model->auto_generate_inv_id();
	if(isset($invoice_auto) && $invoice_auto!=''){
//	print_r($invoice_auto);
//	 exit;
$cnt = 1;
$tab='';
foreach($invoice_auto as $item){
	
	 $arr_insrt = array(
			 
			   'user_id' =>$this->session->userdata('user_id'),
			   'hotel_id' =>$this->session->userdata('user_hotel'),
			   'invoice_type' =>$item->type,
			   'cust_name' => $item->g_name,
			   'related_id' => $item->id,
			   'date_generated' => date('Y-m-d h:i:s'),
			   'date_completed' => date('Y-m-d h:i:s'),
			   'is_cancelled' => 0
			  
			   );
			  
			  $result =  $this->setting_model->insert_auto_inv_id($arr_insrt,$cnt);
			  $cnt ++;
			  
			  if($result == "failed"){
				  
				  
			  } else {
			   
				  $tab .= '<tr><td>'.$item->id.'</td><td>'.$item->g_name.'</td><td>'.$item->type.'</td><td>'.date('Y-m-d h:i:s').'</td></tr>';
				 
			  }
	}
	
	
	
	header('Content-Type:application/json');
	echo json_encode($tab);
}
}


} // End Controller Class

?>