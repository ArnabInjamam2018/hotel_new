<?php
/*
  Created by PhpStorm.
 User: subhabrata
 Date: 11/11/2015
 Time: 12:08 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Superadmin extends CI_Controller
{
	 public function __construct()
    {
        parent::__construct();if (!$this->session->userdata('is_logged_in')) {            redirect('login');        }

       // $this->load->database();
	    // DYNAMIC BATABASE 
         $db['superadmin'] = array(
			'dsn'   => '',
			'hostname' => 'localhost',
			'username' => $this->session->userdata('username'),
			'password' => $this->session->userdata('password'),
			'database' => $this->session->userdata('database'),
			'dbdriver' => 'mysqli',
			'dbprefix' => '',
			'pconnect' => FALSE,
			'db_debug' => TRUE,
			'cache_on' => FALSE,
			'cachedir' => '',
			'char_set' => 'utf8',
			'dbcollat' => 'utf8_general_ci',
			'swap_pre' => '',
			'encrypt' => FALSE,
			'compress' => FALSE,
			'stricton' => FALSE,
			'failover' => array(),
			'save_queries' => TRUE 
		);
        //echo "<pre>";
	//	echo $this->db1->database;
       //    print_r($db1['default']);exit;
            $CI = &get_instance();
        $this->db1 = $CI->load->database($db['superadmin'],true);
		$this->arraay =$this->session->userdata('per');
                //$login_id=$this->session->userdata('admin_id');
                //if(!$login_id>0){redirect(base_url('admin/login')); }
       /* $this->load->model('mod_common');
        $this->load->model('bookings_model');
        $this->load->model('dashboard_model');
        $this->load->library('grocery_CRUD');
		$this->load->library('table');*/
    }

    function index()
    {
        $data['hotels'] = $this->dashboard_model->total_hotels();
        $this->load->view('backend/superadmin/index', $data);
    }

    function redirect(){
		
		//$alls=$this->Cashdrawer->index2();
		/*foreach($alls as $hot){
			
			if(isset($this->input->post('user_hotel'.$hot->hotel_id))){
				$target=$hot->hotel_id;
				
			}
		}*/
		

        $userdata=array(
            //'user_type'=> $user_type_data->user_type_name,

            'user_hotel'=>$this->input->post('user_hotel'),

        );
        //print_r($userdata);
        //exit();
        $this->session->set_userdata($userdata);
        redirect('dashboard');
    }

}