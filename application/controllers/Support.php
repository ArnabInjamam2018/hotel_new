<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends CI_Controller {

	 function __construct() {
        /* Session Checking Start */
		
        parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
            redirect('login');
        }
		//$this->load->library('multiple');
		$db['superadmin'] = array(
			'dsn'   => '',
			'hostname' => 'localhost',
			'username' => $this->session->userdata('username'),
			'password' => $this->session->userdata('password'),
			'database' => $this->session->userdata('database'),
			'dbdriver' => 'mysqli',
			'dbprefix' => '',
			'pconnect' => FALSE,
			'db_debug' => TRUE,
			'cache_on' => FALSE,
			'cachedir' => '',
			'char_set' => 'utf8',
			'dbcollat' => 'utf8_general_ci',
			'swap_pre' => '',
			'encrypt' => FALSE,
			'compress' => FALSE,
			'stricton' => FALSE,
			'failover' => array(),
			'save_queries' => TRUE 
		);
        /*echo "<pre>";
		echo $this->db1->database;
        print_r($db1['default']);exit;*/
	   
        $CI = &get_instance();
        $this->db1 = $CI->load->database($db['superadmin'],true);
	 }
	
	public function index()
	{
		
		 $this->load->model('setting_model');
			$data['heading'] = '';
            $data['sub_heading'] = ' ';
            $data['description'] = '';
            $data['active'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			
			$data['admin_name'] = $this->setting_model->get_admin_details($this->session->userdata('user_id'));
			
			$data['page'] = 'backend/support/support_mail';
			$this->load->view('backend/common/index', $data);
	}
	 
	
	
	

	

	
	public function camera_test()
	{
			$data['heading'] = '';
            $data['sub_heading'] = ' ';
            $data['description'] = '';
            $data['active'] = '';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['page'] = 'backend/dashboard/camera_test';
			$this->load->view('backend/common/index', $data);
	}
}
