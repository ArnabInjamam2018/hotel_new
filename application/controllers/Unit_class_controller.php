<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Unit_class_controller extends CI_Controller
{

	function __construct()
    {
        /* Session Checking Start*/
		
        parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
            redirect('login');
        }
		 // DYNAMIC BATABASE 
         $db['superadmin'] = array(
    'dsn'   => '',
    'hostname' => 'localhost',
    'username' => $this->session->userdata('username'),
    'password' => $this->session->userdata('password'),
    'database' => $this->session->userdata('database'),
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => TRUE,
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);
        //echo "<pre>";
       //    print_r($db['default']);exit;
            $CI = &get_instance();
        $this->db1 = $CI->load->database($db['superadmin'],true);
		$this->arraay =$this->session->userdata('per');
		$this->load->model('unit_class_model');
		$this->load->model('bookings_model');
		$this->load->model('setting_model');
		$this->load->model('reports_model');
		$this->load->model('dashboard_model');
		$this->load->library('upload');
        
        /* Session Checking End*/

        /* URL Value Encryption Start */
        function base64url_encode($data)
        {
            return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
        }

        /* URL Value Encryption End */

        /* URL Value Decryption Start */
        function base64url_decode($data)
        {
            return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
        }
        /* URL Value Decryption End */
    }

	function all_unit_class(){
		$found = in_array("10",$this->arraay);
		//print $found; exit;
		if($found){			 
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Room';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'all_unit_class';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['unit_class']=$this->unit_class_model->all_unit_clsss();
        $data['page'] = 'backend/dashboard/all_unit_class';
        $this->load->view('backend/common/index', $data);

    }else{
		redirect('dashboard');
	}
	}	
	function all_unit_class_add(){
		$found = in_array("10",$this->arraay);
		if($found){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
		$unit_class=$this->input->post('unit_class');
		$unit_desc=$this->input->post('desc');
		$data=array(

						'hotel_unit_class_name'=>$unit_class,
						'hotel_unit_class_desc'=>$unit_desc,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
										
					);
				$query=$this->unit_class_model->all_unit_class_add($data);


				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Successfully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Successfully');
					}
        //$this->all_unit_class();
		redirect(base_url().'unit_class_controller/all_unit_class');
	}else{
		redirect('dashboard');
	}
	}	
	
	function delete_class_type(){
		$found = in_array("12",$this->arraay);
		if($found){
		//$id=$_GET['f_id'];
		$id=$this->input->post('f_id');
		
		$this->unit_class_model->delete_class_type_model($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}else
		{
			 $data = array(
            'data' =>"You do not have the previlage to do this",

        );
			header('Content-Type: application/json');
        echo json_encode($data);
		}
}
	
	function edit_unit_class(){
		 $found = in_array("11",$this->arraay);
		if($found)
		{	
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'unit_class';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		//$data['input']=$this->unit_class_model->fetch_id($id);
		//$data['unit_class']=$this->unit_class_model->all_unit_clsss();
		
		if($this->input->post()){

				$name=$this->input->post('unit_class1');
				$desc=$this->input->post('desc1');
				$id=$this->input->post('hid1');
				$data=array(
					'hotel_unit_class_name'=>$name,
					'hotel_unit_class_desc'=>$desc,
					'hotel_unit_class_id'=>$id
					);
				$query=$this->unit_class_model->update_class($data);
					if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
					}
				redirect(base_url().'unit_class_controller/all_unit_class');

			}
        $data['page'] = 'backend/dashboard/edit_unit_class';		
        $this->load->view('backend/common/index', $data);

					
				//$this->load->view('edit_unit_class',$m);
	}else{
		redirect('dashboard');
	}
	}	


function update_data(){

			/*if($this->input->post()){

				$name=$this->input->post('class_name');
				$desc=$this->input->post('class_desc');
				$id=$this->input->post('id');
				$data=array(
					'hotel_unit_class_name'=>$name,
					'hotel_unit_class_desc'=>$desc,
					'hotel_unit_class_id'=>$id
					);
				$query=$this->unit_class_model->update_data_model($data);
				redirect('http://122.163.127.3/hotelobjects/unit_class_controller/all_unit_class');

			}*/


		}
		
		//ALL_UNIT_TYPE_CATEGORY    (loader)
		
		function all_unit_type_category(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Room';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'all_unit_type_category';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->unit_class_model->all_unit_type_category_data();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->all_admin();
		$data['page']='backend/dashboard/all_unit_type_category';		
        $this->load->view('backend/common/index', $data);

    }
	
	//add add_all_unit_type_category
	function add_all_unit_type_category(){
		
		$this->load->model('unit_class_model');
		$unit_class=$this->input->post('unit_type_category_name');
		$unit_desc=$this->input->post('unit_type_category_desc');
		//$add_admin=$this->input->post('unit_type_category_admin');
		$data=array(

						'name'=>$unit_class,
						'cat_desc'=>$unit_desc,
						//'add_admin'=>$add_admin,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
										
						
										
					);
				$query=$this->unit_class_model->all_unit_class_model_add($data);


				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Successfully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Successfully');
					}
        //$this->all_unit_class();
		redirect(base_url().'unit_class_controller/all_unit_type_category');
	}
	
	function all_unit_type_category_controller(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'unit_class';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		//$data['input']=$this->unit_class_model->unit_type_fetch_id($id);
		//$data['unit_class']=$this->unit_class_model->all_unit_clsss();
		
		date_default_timezone_set("Asia/Kolkata");
		$cur_date=date('Y-m-d H:i:s');
		
		
		if($this->input->post()){

				$name=$this->input->post('category1');
				$desc=$this->input->post('desc1');
				$date=$cur_date;
				//$admin=$this->input->post('admin1');
				$id=$this->input->post('hid1');
				$data=array(
					'name'=>$name,
					'cat_desc'=>$desc,
					'date_added'=>$date,
					//'add_admin'=>$admin,
					'id'=>$id
					);
					//print_r($data);exit;
				$query=$this->unit_class_model->update1($data);
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
					}
				redirect(base_url().'unit_class_controller/all_unit_type_category');

			}
        $data['page'] = 'backend/dashboard/all_unit_type_category';		
        $this->load->view('backend/common/index', $data);

	}
	
	
	
	
	
	//DELETE delete_unit_class_type
	
	function delete_unit_class_type(){
		//$id=$_GET['f_id'];
		 $found = in_array("12",$this->arraay);
		if($found)
		{	
		$id=$this->input->post('f_id');
		
		$this->unit_class_model->delete_unit_class_type($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}else
		{
			 $data = array(
            'data' =>"You do not have the previlage to do this",

        );
			header('Content-Type: application/json');
        echo json_encode($data);
		}
	}	
	
	//-----------------------------------------------------------------------------------
	//loader HOTEL LAUNDRY Type
	function hotel_laundry_type(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Loundty Type';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['unit_class']=$this->unit_class_model->hotel_laundry_type();
        $data['page'] = 'backend/dashboard/hotel_laundry_type';
        $this->load->view('backend/common/index', $data);

    }
	
	function add_hotel_laundry_type(){
		$this->load->model('unit_class_model');
		$name=$this->input->post('hotel_laundry_type_name');
		//$unit_desc=$this->input->post('unit_type_category_desc');
		//$add_admin=$this->input->post('unit_type_category_admin');
		$data=array(

						'laundry_type_name'=>$name,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
						
										
					);
				$query=$this->unit_class_model->model_add_hotel_laundry_type($data);


				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}
        //$this->all_unit_class();
		redirect(base_url().'unit_class_controller/hotel_laundry_type');
	}
	
	
	//delete laundry type
	
	function delete_laundry_type(){
		//$id=$_GET['f_id'];
		$id=$this->input->post('f_id');
		
		$this->unit_class_model->delete_laundry_type($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	//edit laundry type
	function edit_laundry_type($id){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Edit Laundry Type';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['input']=$this->unit_class_model->laundry_fetch_id($id);
		//$data['unit_class']=$this->unit_class_model->all_unit_clsss();
		
		if($this->input->post()){

				$name=$this->input->post('laundry_type');
				$id=$this->input->post('id');
				$data=array(
					'laundry_type_name'=>$name,					
					'laundry_type_id'=>$id
					);
				$query=$this->unit_class_model->update_laundry_type($data);
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
					}
				redirect(base_url().'unit_class_controller/hotel_laundry_type');

			}
        $data['page'] = 'backend/dashboard/hotel_laundry_edit';		
        $this->load->view('backend/common/index', $data);

	}
//------------------------------------------------------------------------------
//loader HOTEL LAUNDRY fabric Type
	function hotel_laundry_fabric_type(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Fabric Type';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['unit_class']=$this->unit_class_model->hotel_laundry_fabric_type();
        $data['page'] = 'backend/dashboard/hotel_laundry_fabric_type';
        $this->load->view('backend/common/index', $data);

    }
	
	//Add hotel laundry fabric 
	function add_hotel_laundry_fabric_type(){
		$this->load->model('unit_class_model');
		$name=$this->input->post('hotel_laundry_type_fabric_name');
		//$unit_desc=$this->input->post('unit_type_category_desc');
		//$add_admin=$this->input->post('unit_type_category_admin');
		$data=array(

						'fabric_type'=>$name,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
						
						
										
					);
				$query=$this->unit_class_model->model_add_hotel_laundry_fabric_type($data);


				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}
        //$this->all_unit_class();
		redirect(base_url().'unit_class_controller/hotel_laundry_fabric_type');
	}
			
	//delete laundry fabric type
	
	function delete_laundry_fabric_type(){
		//$id=$_GET['f_id'];
		$id=$this->input->post('f_id');
		
		$this->unit_class_model->delete_laundry_fabric_type($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	//edit laundry fabric type
	function edit_laundry_fabric_type($id){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Edit Laundry Fabric Type';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['input']=$this->unit_class_model->laundry_fetch_fabric_id($id);
		//$data['unit_class']=$this->unit_class_model->all_unit_clsss();
		
		if($this->input->post()){

				$name=$this->input->post('fabric_type_name');
				$id=$this->input->post('id');
				$data=array(
					'fabric_type'=>$name,					
					'fabric_id'=>$id
					);
				$query=$this->unit_class_model->update_laundry_fabric_type($data);
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
					}
				redirect(base_url().'unit_class_controller/hotel_laundry_fabric_type');

			}
        $data['page'] = 'backend/dashboard/edit_hotel_laundry_fabric_type';		
        $this->load->view('backend/common/index', $data);

	}
	
	//----------------------------------------------------------
	//loader HOTEL LAUNDRY INE ITEM
	function hotel_laundry_line_item(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Laundry Line Item';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		//$data['unit_class']=$this->unit_class_model->hotel_laundry_line_item();
		$data['laundry']=$this->unit_class_model->hotel_laundry_type();
		$data['fabric']=$this->unit_class_model->hotel_laundry_fabric_type();
		$data['unit_class']=$this->unit_class_model->hotel_laundry_line_item();
        //$data['page'] = 'backend/dashboard/hotel_laundry_line_item';
        $data['page'] = 'backend/dashboard/add_laundry_line_item';
        $this->load->view('backend/common/index', $data);

    }
	
	//delete HOTEL LAUNDRY INE ITEM
	
	function delete_hotel_laundry_line_item(){
		//$id=$_GET['f_id'];
		$id=$this->input->post('f_id');
		
		$this->unit_class_model->delete_hotel_laundry_line_item($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	//Add HOTEL LAUNDRY INE ITEM
	

	function add_hotel_laundry_line_item(){

		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
		// $guest_id=$this->input->post('guest_id');
		$item_name=$this->input->post('name');
		$desc=$this->input->post('description');
		$color=$this->input->post('color');
		$fabric_type=$this->input->post('fabric');
		$item_condition=$this->input->post('condition');
		$delivery_date=$this->input->post('delivery_date');
		$laundry_type=$this->input->post('laundry_type');
		
		$data=array(

						// 'guest_id'=>$guest_id,
						'item_name'=>$item_name,
						'item_desc'=>$desc,
						'color'=>$color,
						'fabric_type'=>$fabric_type,
						'item_condition'=>$item_condition,
						'delivery_date'=>$delivery_date,
						'laundry_type'=>$laundry_type,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
										
					);
				$query=$this->unit_class_model->model_add_hotel_laundry_line_item($data);
	if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}

				if($query){
						$msg="inserted";
					}

					else{

						$msg=" not inserted";
					}
        //$this->all_unit_class();
		redirect(base_url().'unit_class_controller/hotel_laundry_line_item');
	}

	//edit laundry_line_item
	function edit_hotel_laundry_line_item($id){
		echo "<script>alert('done');</script>";
		// $this->load->model('dashboard_model');
		// $this->load->model('unit_class_model');
  //       $data['heading'] = 'unit_class';
  //       $data['sub_heading'] = '';
  //       $data['description'] = '';
  //       $data['active'] = 'unit_t';
  //       $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		// $data['unit_class'] = $this->dashboard_model->get_unit_class_list();
  //       $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		// //$data['unit'] = $this->dashboard_model->all_unit_type();
  //       //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		// $data['input']=$this->unit_class_model->fetch_id($id);
		// //$data['unit_class']=$this->unit_class_model->all_unit_clsss();
		
		if($this->input->post()){

				$name=$this->input->post('a');
				echo "<script>alert('done');</script>";
				$desc=$this->input->post('b');
				$id=$this->input->post('id');
				$data=array(
					'hotel_unit_class_name'=>$name,
					'hotel_unit_class_desc'=>$desc,
					'hotel_unit_class_id'=>$id
					);
				$query=$this->unit_class_model->update_data_model($data);
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
					}
				redirect(base_url().'unit_class_controller/all_unit_class');

			}
        $data['page'] = 'backend/dashboard/edit_unit_class';		
        $this->load->view('backend/common/index', $data);

					
				$this->load->view('edit_unit_class',$m);
	}

//----------------------------------------------------------
	//loader HOTEL AMENITIES
	function hotel_amenities(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Hotel Amenities';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		//$data['unit_class']=$this->unit_class_model->hotel_laundry_line_item();
		$data['laundry']=$this->unit_class_model->hotel_laundry_type();
		$data['fabric']=$this->unit_class_model->hotel_laundry_fabric_type();
		$data['unit_class']=$this->unit_class_model->hotel_laundry_line_item();
        //$data['page'] = 'backend/dashboard/hotel_laundry_line_item';
        $data['page'] = 'backend/dashboard/hotel_amenities';
        $this->load->view('backend/common/index', $data);

    }


    
	
	
 function set_upload_options()
    {
        $config = array(
            'upload_path' => './upload/',
            'allowed_types' => 'gif|jpg|png',
            'overwrite' => FALSE
        );
        return $config;
    }
	
	function add_hotel_amenities(){
	$this->load->model('unit_class_model');
	

//upload file/image
		 if($this->input->post())
			{
			
			 $this->upload->initialize($this->set_upload_options());
                        if ($this->upload->do_upload('image')) {

                            $image = $this->upload->data('file_name');
							
                            $filename = $image;
                            $source_path = './upload/' . $filename;
                            $target_path = './upload/amenities/';                            
                            $config_manip = array(
                            'image_library' => 'gd2',
                            'source_image' => $source_path,
                            'new_image' => $target_path,
                            'maintain_ratio' => TRUE,
                            'create_thumb' => TRUE,
                            'thumb_marker' => '_thumb',
                            'width' => 250,
                            'height' => 250
                            );
                            $this->load->library('image_lib', $config_manip);
                            
                            if (!$this->image_lib->resize()) {
                                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                                    redirect('dashboard/add_asset');
                            }
                            $thumb_name = explode(".", $filename);
                            $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                            $this->image_lib->clear();
                        }
						
						
						
											
							
							// $guest_id=$this->input->post('guest_id');
							$name=$this->input->post('name');
							$desc=$this->input->post('desc');
							$general=$this->input->post('general');
							$units=$this->input->post('units');
							$type=$this->input->post('type');
							$charge=$this->input->post('charge');
							$tax=$this->input->post('tax');
	
						$data=array(
						'name'=>$name,
						'description'=>$desc,
						'hotel_general'=>$general,
						'units'=>$units,
						'type'=>$type,
						'charge'=>$charge,
						'tax'=>$tax,
						'image'=>$asset_thumb,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
						);
						//print_r($data);exit;
						$query=$this->unit_class_model->add_amenities($data);
						if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}
				redirect(base_url().'unit_class_controller/hotel_amenities');
			}
	}
	
	
	//SHOW ALL AMENITIES
	function all_Amenities()
    {
       $this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'All Amenities';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		//$data['unit_class']=$this->unit_class_model->hotel_laundry_line_item();
		//$data['laundry']=$this->unit_class_model->hotel_laundry_type();
		//$data['fabric']=$this->unit_class_model->hotel_laundry_fabric_type();
		$data['amenities']=$this->unit_class_model->all_Amenities();        
        $data['page'] = 'backend/dashboard/all_Amenities';
        $this->load->view('backend/common/index', $data);

    }
	
	
	//delete AMENITIES
	
	function delete_amenities(){
		//$id=$_GET['f_id'];
		$id=$this->input->post('g_id');
		
		$this->unit_class_model->delete_amenities($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	//edit laundry fabric type
	function hotel_amenities_edit($id){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Edit Hotel Aminities';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['input']=$this->unit_class_model->fetch_amenities($id);
		//$data['unit_class']=$this->unit_class_model->all_unit_clsss();
		
		if($this->input->post()){
			
			$this->upload->initialize($this->set_upload_options());
                        if ($this->upload->do_upload('image')) {

                            $image = $this->upload->data('file_name');
							
                            $filename = $image;
                            $source_path = './upload/' . $filename;
                            $target_path = './upload/amenities/';                            
                            $config_manip = array(
                            'image_library' => 'gd2',
                            'source_image' => $source_path,
                            'new_image' => $target_path,
                            'maintain_ratio' => TRUE,
                            'create_thumb' => TRUE,
                            'thumb_marker' => '_thumb',
                            'width' => 250,
                            'height' => 250
                            );
                            $this->load->library('image_lib', $config_manip);
                            
                            if (!$this->image_lib->resize()) {
                                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                                    redirect('dashboard/add_asset');
                            }
                            $thumb_name = explode(".", $filename);
                            $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                            $this->image_lib->clear();
                        }
						
						

				$id=$this->input->post('hid');
				$name=$this->input->post('name');
				$description=$this->input->post('desc');
				$hotel_general=$this->input->post('general');
				$units=$this->input->post('units');
				$type=$this->input->post('type');
				$charge=$this->input->post('charge');
				$tax=$this->input->post('tax');
				$data=array(
					'name'=>$name,
					'description'=>$description,					
					'hotel_general'=>$hotel_general,
					'units'=>$units,
					'type'=>$type,
					'charge'=>$charge,
					'tax'=>$tax,
					'image'=>$asset_thumb,
					'id'=>$id
					);
				$query=$this->unit_class_model->update_amenities($data);
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}
				redirect(base_url().'unit_class_controller/all_amenities');

			}
        $data['page'] =  'backend/dashboard/hotel_amenities_edit';	
        $this->load->view('backend/common/index', $data);

	}
	
	
	//-------------------------------------------------------------
	//hotel_wearehouse table

function warehouse(){
		 $found = in_array("64",$this->arraay);
		if($found)
		{	
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Stock Inventory';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'warehouse';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		//$data['unit_class']=$this->unit_class_model->hotel_laundry_line_item();
		//$data['laundry']=$this->unit_class_model->hotel_laundry_type();
		//$data['fabric']=$this->unit_class_model->hotel_laundry_fabric_type();
		$data['data']=$this->unit_class_model->all_warehouse();
        //$data['page'] = 'backend/dashboard/hotel_laundry_line_item';
        $data['page'] = 'backend/dashboard/warehouse';
        $this->load->view('backend/common/index', $data);

    }else{
		redirect('dashboard');
	}
}	
	
	//Add hotel laundry fabric 
	function add_warehouse(){
			 $found = in_array("64",$this->arraay);
		if($found)
		{	
		//$this->load->model('dashboard_model');
		$this->load->model('Unit_class_model');
		date_default_timezone_set("Asia/Kolkata");
		$name=$this->input->post('name');
		$desc=$this->input->post('desc');	
		$datas = array(
						'name'=>$name,
						'description'=>$desc,
						'change_date'=>date('Y-m-d H:i:s'),
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
					);
				$query=$this->Unit_class_model->add_warehouse($datas);
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}
		//			exit();
        //$this->all_unit_class();
		redirect(base_url().'unit_class_controller/warehouse');
	}else{
		redirect('dashboard');
	}
	}	
	
	//delete warehouse
	function delete_warehouse(){
			 $found = in_array("65",$this->arraay);
		if($found)
		{	
		//$id=$_GET['f_id'];
		$id=$this->input->post('d_id');
		
		$this->unit_class_model->delete_warehouse($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}else
		{
			 $data = array(
            'data' =>"You do not have the previlage to do this",

        );
			header('Content-Type: application/json');
        echo json_encode($data);
		}
	}	
	
	//edit warehouse
	function edit_warehouse(){
		$found = in_array("64",$this->arraay);
		if($found)
		{		
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Edit warehouse';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['data'] = $this->unit_class_model->data_warehouse($id);
        //$data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		//$data['data']=$this->unit_class_model->data_warehouse($id);
		//$data['unit_class']=$this->unit_class_model->all_unit_clsss();
		
		if($this->input->post()){

				$name=$this->input->post('name1');
				$desc=$this->input->post('desc1');
				$id=$this->input->post('hid1');
				date_default_timezone_set("Asia/Kolkata");
		$cur_date=date('Y-m-d H:i:s');
				$data=array(
					'name'=>$name,					
					'description'=>$desc,
					'id'=>$id,
					'change_date'=>$cur_date
					);
					//print_r($data);
				$query=$this->unit_class_model->update_warehouse($data);
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
					}
				redirect(base_url().'unit_class_controller/warehouse');
			}
        $data['page'] = 'backend/dashboard/edit_warehouse';		
        $this->load->view('backend/common/index', $data);
	}else{
		redirect('dashboard');
	}
	}	
	
	
	//-------------------------------------------------------------
	//stack_inventory_category

function stock_inventory_category(){
	 $found = in_array("61",$this->arraay);
		if($found)
		{		
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Stock Inventory';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'stock_inventory_category';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		
		$data['data']=$this->unit_class_model->all_stock_inventory_category();
        //$data['page'] = 'backend/dashboard/hotel_laundry_line_item';
        $data['page'] = 'backend/dashboard/stock_inventory_category';
        $this->load->view('backend/common/index', $data);

    }else{
		redirect('dashboard');
	}
}	
	
	//Add hotel stock_inventory_category
	function add_Stock_inventory_category(){
		$found = in_array("61",$this->arraay);
		if($found)
		{		
		$this->load->model('Unit_class_model');
		date_default_timezone_set("Asia/Kolkata");
		$name=$this->input->post('name');
		$desc=$this->input->post('desc');	
		$datas = array(

						'name'=>$name,
						'description'=>$desc,
						'change_date'=>date('Y-m-d H:i:s'),
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
						
										
					);
					
					//print_r ($datas);
					
				$query=$this->Unit_class_model->add_Stock_inventory_category($datas);


				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}
		//			exit();
        //$this->all_unit_class();
		redirect(base_url().'unit_class_controller/stock_inventory_category');
	}else{
		redirect('dashboard');
	}
	}	
	
	
	
	//delete stock inventory category
	
	function delete_stock_inventory_category(){
		$found = in_array("62",$this->arraay);
		if($found)
		{		
		$id=$this->input->post('f_id');
		
		$this->unit_class_model->delete_stock_inventory_category($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}else
		{
			 $data = array(
            'data' =>"You do not have the previlage to do this",

        );
			header('Content-Type: application/json');
        echo json_encode($data);
		}
	}	
	
	
	//edit warehouse
	function edit_stock_inventory_category(){
		 $found = in_array("61",$this->arraay);
		if($found)
		{		
		
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Assets';
        $data['sub_heading'] = 'Stock Inventory';
        $data['description'] = '';
        $data['active'] = 'edit_stock_inventory_category';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['data'] = $this->unit_class_model->data_warehouse($id);
        //$data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->data_hotel_stock_inventory_category($id);
		//$data['unit_class']=$this->unit_class_model->all_unit_clsss();
		
		if($this->input->post()){

				$name=$this->input->post('name1');
				$desc=$this->input->post('desc1');
				$id=$this->input->post('hid1');
				$data=array(
					'name'=>$name,					
					'description'=>$desc,
					'change_date'=>date('Y-m-d H:i:s'),
					'id'=>$id
					);
					//print_r($data);
				$query=$this->unit_class_model->update_stock_inventory_category($data);
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
					}
				redirect(base_url().'unit_class_controller/stock_inventory_category');

			}
        $data['page'] = 'backend/dashboard/edit_stock_inventory_category';		
        $this->load->view('backend/common/index', $data);

	}else{
		redirect('dashboard');
	}
	}	
	
	//---------------------------------------------------------------
	//HOTEL_SECTION loader
	//SHOW ALL HOTEL_SECTION
	function hotel_section()
    {
       $this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Room';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'hotel_section';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		//$data['unit_class']=$this->unit_class_model->hotel_laundry_line_item();
		//$data['laundry']=$this->unit_class_model->hotel_laundry_type();
		//$data['fabric']=$this->unit_class_model->hotel_laundry_fabric_type();
		$data['section']=$this->unit_class_model->all_section();        
        $data['page'] = 'backend/dashboard/all_section';
        $this->load->view('backend/common/index', $data);

    }
	
	
	
	//loader HOTEL add section
	function add_section(){
		 $found = in_array("10",$this->arraay);
		if($found) {
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Add Hotel Section';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit_class']=$this->unit_class_model->hotel_laundry_line_item();
		
        $data['page'] = 'backend/dashboard/add_section';
        $this->load->view('backend/common/index', $data);

    }
	else{
		redirect('dashboard');
	}
	}
	
	
	
	function add_hotel_section(){
	$this->load->model('unit_class_model');
	
//upload file/image
		 if($this->input->post())
			{
			//exit();
			//print_r($_FILES['image']);
			//exit();
			 $this->upload->initialize($this->set_upload_options());
                        if ($this->upload->do_upload('image')) {

                            $image = $this->upload->data('file_name');
							
                            $filename = $image;
                            $source_path = './upload/' . $filename;
                            $target_path = './upload/section/';                            
                            $config_manip = array(
                            'image_library' => 'gd2',
                            'source_image' => $source_path,
                            'new_image' => $target_path,
                            'maintain_ratio' => TRUE,
                            'create_thumb' => TRUE,
                            'thumb_marker' => '_thumb',
                            'width' => 250,
                            'height' => 250
                            );
                            $this->load->library('image_lib', $config_manip);
                            
                            if (!$this->image_lib->resize()) {
                                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                                    redirect('dashboard/add_asset');
                            }
                            $thumb_name = explode(".", $filename);
                            $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                            $this->image_lib->clear();
                        }
						else{
							$target_path = './upload/section/'; 
							 $asset_thumb ='no_images.png';
						}
						
						
						
											
							
							// $guest_id=$this->input->post('guest_id');
							$name=$this->input->post('name');
							$desc=$this->input->post('desc');
							$Housekeeping_status=$this->input->post('Housekeeping_status');
							$audit_status=$this->input->post('audit_status');
							$note=$this->input->post('note');
							$hotel_id=$this->input->post('hotel_id');
								
						$data=array(
						'sec_name'=>$name,
						'sec_desc'=>$desc,
						'sec_housekeeping_status'=>$Housekeeping_status,
						'sec_audit_status'=>$audit_status,
						'note'=>$note,
						'sec_hotel_id'=>$hotel_id,
						'sec_img'=>$asset_thumb,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
						);
						
						$query=$this->unit_class_model->add_section($data);
						if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}
				redirect(base_url().'unit_class_controller/hotel_section');
			}
	}
	
	//delete section
	
	function delete_section(){
		
		$id=$this->input->post('id');
		
		$this->unit_class_model->delete_section($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	
	//edit section
	function edit_section(){
		
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Edit Section';
        $data['sub_heading'] = 'Edit Section';
        $data['description'] = '';
        $data['active'] = 'edit_stock_inventory_category';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
				
		if($this->input->post()){
			
			$this->upload->initialize($this->set_upload_options());
                        if ($this->upload->do_upload('image')) {

                            $image = $this->upload->data('file_name');
							
                            $filename = $image;
                            $source_path = './upload/' . $filename;
                            $target_path = './upload/section/';                            
                            $config_manip = array(
                            'image_library' => 'gd2',
                            'source_image' => $source_path,
                            'new_image' => $target_path,
                            'maintain_ratio' => TRUE,
                            'create_thumb' => TRUE,
                            'thumb_marker' => '_thumb',
                            'width' => 250,
                            'height' => 250
                            );
                            $this->load->library('image_lib', $config_manip);
                            
                            if (!$this->image_lib->resize()) {
                                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                                    redirect('dashboard/add_asset');
                            }
                            $thumb_name = explode(".", $filename);
                            $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                            $this->image_lib->clear();
                        }
									

				$name=$this->input->post('name1');
				$desc=$this->input->post('desc1');
				$house_keeping_staus=$this->input->post('Housekeeping_status1');
				$audit_status=$this->input->post('audit_status1');
				$note=$this->input->post('note1');
				$hotel_id=$this->input->post('hotel_id1');				
				$id=$this->input->post('hid1');
				$data=array(
					'sec_name'=>$name,					
					'sec_desc'=>$desc,
					'sec_housekeeping_status'=>$house_keeping_staus	,
					'sec_audit_status'=>$audit_status,
					'note'=>$note,
					'sec_hotel_id'=>$hotel_id,				
					'sec_img'=>$asset_thumb,
					'sec_id'=>$id
					);
					//print_r($data);
				$query=$this->unit_class_model->update_hotel_section($data);
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
					}
				redirect(base_url().'unit_class_controller/hotel_section');

			}
        $data['page'] = 'backend/dashboard/edit_hotel_section';		
        $this->load->view('backend/common/index', $data);

	}
	
	
	//------------------------------------------------------------------------------
//loader booking_nature_visit
	function booking_nature_visit(){
		 $found = in_array("91",$this->arraay);
		
		if($found)
		{		
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
        $data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'booking_nature_visit';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->active_booking_nature_visit($this->session->userdata('user_hotel'));
        $data['page'] = 'backend/dashboard/booking_nature_visit';
		$data['stat']='List of All Booking Nature Visit';
        $this->load->view('backend/common/index', $data);

    }else{
		redirect('dashboard');
	}
	}	
	
	function active_booking_nature_visit(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Nature of Visit';
        $data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'booking_nature_visit';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->active_booking_nature_visit();
        $data['page'] = 'backend/dashboard/booking_nature_visit';
		$data['stat']='List of All Active Booking Nature Visit';
        $this->load->view('backend/common/index', $data);

    }
	function inactive_booking_nature_visit(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Nature of Visit';
        $data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'booking_nature_visit';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->inactive_booking_nature_visit();
        $data['page'] = 'backend/dashboard/booking_nature_visit';
		$data['stat']='List of All inactive Booking Nature Visit';
        $this->load->view('backend/common/index', $data);

    }
	
	
		//delete booking_nature_visit
	
	function delete_booking_nature_visit(){
		 $found = in_array("93",$this->arraay);
		if($found)
		{
		
		$id=$this->input->post('id');
		
		$this->unit_class_model->delete_booking_nature_visit($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}else
		{
			 $data = array(
            'data' =>"You do not have the previlage to do this",

        );
			header('Content-Type: application/json');
        echo json_encode($data);
		}
	}
	
	
	
		
		//edit edit_booking_nature_visit
	function edit_booking_nature_visit(){
			 $found = in_array("92",$this->arraay);
		if($found)
		{		
		 date_default_timezone_set("Asia/Kolkata");
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Edit Booking Nature Visit';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['data'] = $this->unit_class_model->data_warehouse($id);
        //$data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		//$data['datas']=$this->unit_class_model->data_booking_nature_visit($id);
		//$data['unit_class']=$this->unit_class_model->all_unit_clsss();
		//print_r($data['datas']);exit();
		if($this->input->post()){
			   date_default_timezone_set("Asia/Kolkata");
				$name=$this->input->post('name1');
				$desc=$this->input->post('desc1');
				$add_date=date('Y-m-d H:i:s');
				$id=$this->input->post('hid2');
				$def=$this->input->post('default1');
				$data=array(
					'booking_nature_visit_name'=>$name,					
					'booking_nature_visit_description'=>$desc,
					'booking_nature_visit_id'=>$id,
					'nature_visit_default'=>$def,
					'booking_nature_visit_date_added'=>$add_date
					);
					//print_r($data);
				$query=$this->unit_class_model->update_booking_naure_visit($data);
					if($def=='1'){
						$query1=$this->unit_class_model->edit_update_nature_visit($id);
					}
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
					}
				redirect(base_url().'unit_class_controller/booking_nature_visit');

			}
        $data['page'] = 'backend/dashboard/edit_booking_nature_visit';		
        $this->load->view('backend/common/index', $data);

	
			}else{
		redirect('dashboard');
	}
	}		
			
			
			function add_booking_nature_visit(){
					 $found = in_array("91",$this->arraay);
		
		if($found)
		{		
		date_default_timezone_set("Asia/Kolkata");
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
		$name=$this->input->post('name');
		$desc=$this->input->post('desc');
		$default=$this->input->post('default');
		
		$cur_date=date('Y-m-d H:i:s');
		$data=array(

						'booking_nature_visit_name'=>$name,
						'booking_nature_visit_description'=>$desc,
						'nature_visit_default'=>$default,
						'booking_nature_visit_date_added'=>$cur_date,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
										
					);
				$query=$this->unit_class_model->add_booking_nature_visit($data);
				if($default=='1'){
				$query1=$this->unit_class_model->update_booking_nature_visit_default($query);
				}
				
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}
        //$this->all_unit_class();
		redirect(base_url().'unit_class_controller/booking_nature_visit');
	}else{
		redirect('dashboard');
	}
	
}	
        
	
	//------------------------------------------------------------------------------
//loader booking_nature_visit
	function hotel_booking_source(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
        $data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'hotel_booking_source';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->get_booking_source();
        $data['page'] = 'backend/dashboard/hotel_booking_source';
		$data['stat']='List of All Active Booking Source';
        $this->load->view('backend/common/index', $data);

    }
	
	//inactive booking source
	function hotel_booking_source_inactive(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Edit Booking Source';
        $data['sub_heading'] = 'Edit Booking Source';
        $data['description'] = '';
        $data['active'] = 'hotel_booking_source';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->get_booking_source_inactive();
        $data['page'] = 'backend/dashboard/hotel_booking_source';
		$data['stat']='List of All Inactive Booking Source ';
        $this->load->view('backend/common/index', $data);

    }
	
	
		//delete booking_nature_visit
	
	function delete_booking_source(){
			 $found = in_array("99",$this->arraay);
		if($found)
		{		
		
		$id=$this->input->post('id');
		
		$this->unit_class_model->delete_booking_source($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}else
		{
			 $data = array(
            'data' =>"You do not have the previlage to do this",

        );
			header('Content-Type: application/json');
        echo json_encode($data);
		}
	}	
	
	
	function edit_category(){
		
		$id=$this->input->post('id');
		
		$query=$this->unit_class_model->fetch_category($id);
		$data = array(
            'id' =>$query->id,
			'name' =>$query->name, 
			'cat_desc' =>$query->cat_desc,
			'add_admin' =>$query->add_admin

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	function edit_unit_type(){
		
		$id=$this->input->post('id');
		
		$query=$this->unit_class_model->edit_unit_type($id);
		$data = array(
            'id' =>$query->id,
			'unit_type' =>$query->unit_type, 
			'unit_name' =>$query->unit_name,
			'unit_class' =>$query->unit_class,
			'unit_desc' =>$query->unit_desc,
			'kit'  =>   $query->kit,
			'default_occupancy' =>$query->default_occupancy,
			'max_occupancy' =>$query->max_occupancy
			
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	function fetch_section(){
		
		$id=$this->input->post('id');
		
		$query=$this->unit_class_model->fetch_section($id);
		$data = array(
            'sec_id' =>$query->sec_id,
			'sec_name' =>$query->sec_name, 
			'sec_desc' =>$query->sec_desc,
			'sec_housekeeping_status' =>$query->sec_housekeeping_status,
			'sec_audit_status' =>$query->sec_audit_status,
			'sec_img' =>$query->sec_img,
			'note' =>$query->note,
			'sec_hotel_id' =>$query->sec_hotel_id

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	
	function edit_unit_class1(){
		
		$id=$this->input->post('id');
		
		$query=$this->unit_class_model->fetch_unit_class($id);
		$data = array(
            'hotel_unit_class_id' =>$query->hotel_unit_class_id,
			'hotel_unit_class_name' =>$query->hotel_unit_class_name, 
			'hotel_unit_class_desc' =>$query->hotel_unit_class_desc
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	function get_warehouse_stock(){
		$id=$this->input->post('id');
		$query=$this->unit_class_model->get_warehouse_stock($id);
		$data = array(
            'qty' =>$query->qty,
			'item_id'=>$query->item_id,
			'warehouse_id'=>$query->warehouse_id
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
		
		//edit edit_booking_nature_visit
	function edit_booking_source(){
			 $found = in_array("98",$this->arraay);
		if($found)
		{
		//$id=$_GET['b_source_id'];
		$id=$this->input->post('hid2');
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Edit Booking source';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		$data['data']=$this->unit_class_model->data_booking_source($id);
		//$data['unit_class']=$this->unit_class_model->all_unit_clsss();
		
		if($this->input->post()){

				$name=$this->input->post('name1');
			    $desc=$this->input->post('desc1');
				$ta='0';
				$id=$this->input->post('hid2');
				$default=$this->input->post('default1');
				 $type=$this->input->post('type1');
				//exit();
				date_default_timezone_set("Asia/Kolkata");
				$cur_date=date('Y-m-d H:i:s');
				$data=array(
					'booking_source_id'=>$id,
					'booking_source_name'=>$name,					
					'booking_source_is_ta'=>$ta,
					'booking_source_hotel_id'=>$this->session->userdata('user_hotel'),
					'booking_source_date'=>$cur_date,
					'booking_default'=>$default,
					'source_type'=>$type,
					'bk_source_type_id'=>$type,
					'booking_source_description'=>$desc
					);
				/*	echo "<pre>";
					print_r($data);
					exit;
					*/
				 $query=$this->unit_class_model->update_booking_source($data,$id);
				 if($default=='1'){
				 $query1=$this->unit_class_model->edit_update_booking_source($query);
				 }
				//exit();
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
					}
				redirect(base_url().'unit_class_controller/hotel_booking_source');

			}
        $data['page'] = 'backend/dashboard/edit_booking_source';		
        $this->load->view('backend/common/index', $data);

	
			}else{
				redirect('dashboard');
			}
	}	
			
		/*
			function add_booking_source(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
		
		$name=$this->input->post('name');
		$hotel_id=$this->session->userdata('user_hotel');
		$ta=$this->input->post('ta');
		$desc=$this->input->post('desc');
		$cur_date=date('Y-m-d H:i:s');
		$data=array(

						'booking_source_name'=>$name,
						'booking_source_date'=>$cur_date,
						'booking_source_hotel_id'=>$hotel_id,
						'booking_source_is_ta'=>$ta,
						'booking_source_description'=>$desc
										
					);
				$query=$this->unit_class_model->add_booking_source($data);


				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}
        //$this->all_unit_class();
		redirect(base_url().'/unit_class_controller/hotel_booking_source');
	}*/
       
	   
	   function add_booking_source(){
		   
		   	 $found = in_array("97",$this->arraay);
		if($found)
		{
		    $data['heading'] = 'Booking source';
            $data['sub_heading'] = 'Add booking source';
            $data['description'] = 'Add booking source here';
            $data['active'] = 'hotel_booking_source';
			$cur_date=date('Y-m-d H:i:s');
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			 if($this->input->post())
                {   
			
					$name=$this->input->post('name');
					$hotel_id=$this->session->userdata('user_hotel');
					//$ta=6;
					$desc=$this->input->post('desc');
					$source=$this->input->post('type');
					date_default_timezone_set("Asia/Kolkata");
					$cur_date=date('Y-m-d H:i:s');
					$def=$this->input->post('default');
					
						
                        
                            
                        /* Images Upload*/ 
                        
                        
						 $data=array(
								'booking_source_name'=>$name,
								'booking_source_description'=>$desc,
                                'booking_default'=>$def,
                                'source_type'=>$source,
								'booking_source_date'=>$cur_date,
                                'booking_source_hotel_id'=>$hotel_id,
								'hotel_id'=>$this->session->userdata('user_hotel'),
								'user_id'=>$this->session->userdata('user_id')
								//'booking_source_is_ta'=>$ta
                            );
         
         
                    /*echo "<pre>";
                    print_r($data);
                    exit;
                    */
                    
					$query=$this->unit_class_model->add_booking_source($data);
					
					if($def=='1'){
					$query1=$this->unit_class_model->update_booking_source_default($query);
					}
                    if($query){
                        $this->session->set_flashdata('succ_msg', 'Booking Source Sucessfully');
						
                    }
                    else{
                        $this->session->set_flashdata('err_msg', 'Database Error');
                    }
					//$data['page'] = 'backend/dashboard/all_profit_center';
					redirect(base_url().'Unit_class_controller/hotel_booking_source');
				}
				  
             
				
            $data['page'] = 'backend/dashboard/add_booking_source';
            $this->load->view('backend/common/index', $data);
		
        
	
	}else{
		redirect('dashboard');
	}
	   }	


function profit_center_status(){
	$this->load->model('unit_class_model'); 		
		$id=$this->input->post('id');		
		$query=$this->unit_class_model->change_profit_center_status($id);
			
			 if($query=='1'){
				 $data = array(
            'data' =>'Inactive',

        );
			 }else{
				 $data = array(
            'data' =>'Active',

        );
			 }
        header('Content-Type: application/json');
        echo json_encode($data);
	
		/*$this->load->model('unit_class_model'); 
		
		$id=$this->input->post('id');
		$status=$this->input->post('status');
		
	
		if($status=='1'){
		$data=array(

						'status'=>'0',
											
						);
		}
		else{
						$data=array(

						'status'=>'1',
											
						);
		}
						
		
		$query=$this->unit_class_model->change_profit_center_status($id,$status,$data);
				//$query=$this->unit_class_model->profit_center_status($data);
				if($query){

               echo "wfwfe";
              }
			  else{
				   echo "wfwtrhtrfe";
			  }*/

	}
	
	
	
	/*function booking_source_default(){
	
	//echo "hello";
		  $a=$this->input->post('a');
		
		$res=$this->unit_class_model->check_booking_source_default($a);
		if($res){
			echo 'Do you want to change default profit center';
			
		}
	
}*/


/*function add_booking_source(){
			$data['heading'] = 'Setting';
            $data['sub_heading'] = 'Metadata';
			$data['subs_heading'] = 'Profit Center';
            $data['description'] = 'Add Profit Center here';
			$data['active'] = 'all_profit_center';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['pc']=$this->unit_class_model->booking_source_active();
			$data['page'] = 'backend/dashboard/hotel_booking_source';
            $this->load->view('backend/common/index', $data);
	}*/

	
	
	
	
	
	
	function check_booking_source_default(){
	
	//echo "hello";
		  $a=$this->input->post('a');
		
		$res=$this->unit_class_model->check_booking_source_default($a);
		if($res){
			echo 'Do you want to change default profit center';
			
		}
	
}


function booking_source_status(){
	$this->load->model('unit_class_model'); 		
		$id=$this->input->post('id');		
		$query=$this->unit_class_model->change_booking_source_status($id);
			
			 if($query=='1'){
				 $data = array(
            'data' =>'Inactive',

        );
			 }else{
				 $data = array(
            'data' =>'Active',

        );
			 }
        header('Content-Type: application/json');
        echo json_encode($data);
	
		/*$this->load->model('unit_class_model'); 
		
		$id=$this->input->post('id');
		$status=$this->input->post('status');
		
	
		
		$data=array(

						'status'=>'0',
											
						);
						$data1=array(

						'status'=>'1',
											
						);
						
		
		$query=$this->unit_class_model->change_booking_source_status($id,$status,$data,$data1);
				//$query=$this->unit_class_model->profit_center_status($data);
				if($query){

               echo "wfwfe";
              }
			  else{
				   echo "wfwtrhtrfe";
			  }
*/
	}
	
	
	
	function add_page_booking_nature_visit(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Add booking nature visit';
        $data['sub_heading'] = 'Edit Booking Source';
        $data['description'] = '';
        $data['active'] = 'hotel_booking_source';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->Active_booking_nature_visit();
        $data['page'] = 'backend/dashboard/add_booking_nature_visit';
		$data['stat']='List of All  Booking Nature Visit';
        $this->load->view('backend/common/index', $data);

    }
	
	
			//check profit center default
   function check_nature_visit_default(){
	
	//echo "hello";
		  $a=$this->input->post('a');
		
		$res=$this->unit_class_model->check_nature_visit_default($a);
		if($res){
			echo 'Do you want to change default profit center';
			
		}
	
}


function booking_nature_visit_status(){
	$this->load->model('unit_class_model'); 		
		$id=$this->input->post('id');		
		$query=$this->unit_class_model->booking_nature_visit_status($id);
			
			 if($query=='1'){
				 $data = array(
            'data' =>'Inactive',

        );
			 }else{
				 $data = array(
            'data' =>'Active',

        );
			 }
        header('Content-Type: application/json');
        echo json_encode($data);
	
	
	
	

	}
	
	
	//loader hotel_area_code
	function hotel_area_code(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
        $data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'hotel_area_code';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->hotel_area_code();
        $data['page'] = 'backend/dashboard/hotel_area_code';
		$data['stat']='List of All Active Booking Source';
        $this->load->view('backend/common/index', $data);

    }
	
	//loader hotel_area_code_filter
	function hotel_area_code_filter(){
		$state = $this->input->post('state');
		$data=$this->unit_class_model->hotel_area_code_filter($state);
		//$state1=$data->state;
		$output='<table class="table table-striped table-hover table-bordered" id="sample_10">
                    <thead>
                    <tr>
                       
                        <th scope="col" width="20%">
                            Pincode.
                        </th>
                        <th scope="col" width="20%">
                            Country
                        </th>
						<th scope="col" width="20%">
                            Status
                        </th>
                       
                        <th scope="col" width="20%">
                            City
                        </th>

                        <th scope="col" width="20%">
                            Locality 1
                        </th>
                        <th scope="col" width="20%">
                            Locality 2
                        </th>
						<th scope="col" width="20%">
                            Action
                        </th>
                    </tr>
                    </thead>
                    <tbody>';
		foreach($data as $datas){
			$output.='<tr>'.'<td>'.$datas->pincode.'</td>'.'<td>'.$datas->area_0.'</td>'.'<td>'.$datas->area_1.'</td>'.'<td>'.$datas->area_3.'</td>'.'<td>'.$datas->area_4.'</td>'.'<td>'.$datas->area_5.'</td>'.'<td class="ba"><div class="btn-group"><button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button><ul class="dropdown-menu pull-right" role="menu"><li><a onclick="soft_delete('.$datas->id.')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li><li><a href="edit_area_code/'.$datas->id.'" class="btn btn-xs blue" data-toggle="modal"><i class="fa fa-edit"></i></a></li></ul></div></td></tr>';
			
		}
		$output.='</tbody></table>';
		echo $output;
	  // header('Content-Type: application/json');
     //  echo json_encode($data);
    }
	
	function hotel_area_code_filter_j(){
		$state = $this->input->post('state');
		$data=$this->unit_class_model->hotel_area_code_filter($state);
		//$state1=$data->state;
		$output='<table class="table table-striped table-hover table-bordered" id="sample_10">
                    <thead>
                    <tr>
                       
                        <th scope="col" width="20%">
                            Pincode.
                        </th>
                        <th scope="col" width="20%">
                            Country
                        </th>
						<th scope="col" width="20%">
                            Status
                        </th>
                       
                        <th scope="col" width="20%">
                            City
                        </th>

                        <th scope="col" width="20%">
                            Locality 1
                        </th>
                        <th scope="col" width="20%">
                            Locality 2
                        </th>
						<th scope="col" width="20%">
                            Action
                        </th>
                    </tr>
                    </thead>
                    <tbody>';
		foreach($data as $datas){
			$output.='<tr>'.'<td>'.$datas->pincode.'</td>'.'<td>'.$datas->area_0.'</td>'.'<td>'.$datas->area_1.'</td>'.'<td>'.$datas->area_3.'</td>'.'<td>'.$datas->area_4.'</td>'.'<td>'.$datas->area_5.'</td>'.'<td><div class="btn-group"><button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button><ul class="dropdown-menu pull-right" role="menu"><li><a onclick="soft_delete('.$datas->id.')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li><li><a href="edit_area_code/'.$datas->id.'" class="btn blue btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li></ul></div></td></tr>';
			
		}
		$output.='</tbody></table>';
		echo json_encode($output);
	  // header('Content-Type: application/json');
     //  echo json_encode($data);
    }
	
	function edit_area_code($id){
		
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Edit Area Code';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'unit_t';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['datas']=$this->unit_class_model->fetch_area_code($id);
		//$data['unit_class']=$this->unit_class_model->all_unit_clsss();
		
		if($this->input->post()){

				$pincode=$this->input->post('pincode');
				$country=$this->input->post('country');
				$state=$this->input->post('state');
				$city=$this->input->post('city');
				$locality1=$this->input->post('locality1');
				$locality2=$this->input->post('locality2');
				$id=$this->input->post('hid');
				$data=array(
					'pincode'=>$pincode,					
					'area_0'=>$country,
					'area_1'=>$state,
					'area_3'=>$city,
					'area_4'=>$locality1,
					'area_5'=>$locality2,
					'id'=>$id
					);
				$query=$this->unit_class_model->update_area_code($data);
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
					}
				redirect(base_url().'unit_class_controller/hotel_area_code');

			}
        $data['page'] = 'backend/dashboard/edit_area_code';		
        $this->load->view('backend/common/index', $data);

		
	}
	

	
	function delete_hotel_area_code(){
		$code=$_GET['code'];
		
		//echo $code;exit;
		
		$this->unit_class_model->delete_hotel_area_code($code);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
	}
	
	function add_area_code(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Add area code';
        $data['sub_heading'] = 'Add area code';
        $data['description'] = '';
        $data['active'] = 'hotel_booking_source';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		//$data['data']=$this->unit_class_model->Active_booking_nature_visit();
		
		//echo 'hello';exit;
        $data['page'] = 'backend/dashboard/add_area_code';
		//$data['stat']='List of All  Booking Nature Visit';
        $this->load->view('backend/common/index', $data);

    }
	
	//add add_hotel_area_code
	function add_hotel_area_code(){
		$this->load->model('unit_class_model');
		$country=$this->input->post('country');
		$state=$this->input->post('state');
		$city=$this->input->post('city');
		$locality1=$this->input->post('locality1');
		$locality2=$this->input->post('locality2');
		$pincode=$this->input->post('pincode');
		
		$data=array(

						'pincode'=>$pincode,
						'area_0'=>$country,
						'area_1'=>$state,						
						'area_3'=>$city,
						'area_4'=>$locality1,
						'area_5'=>$locality2,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
										
					);
				$query=$this->unit_class_model->add_hotel_area_code($data);


				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}
        //$this->all_unit_class();
		redirect(base_url().'unit_class_controller/hotel_area_code');
	}
	
	function get_edit_area_code(){
		$id=$_POST['id'];
		$query=$this->unit_class_model->fetch_area_code($id);
		$data=array(

						'pincode'=>$query->pincode,
						'area_0'=>$query->area_0,
						'area_1'=>$query->area_1,						
						'area_3'=>$query->area_3,
						'area_4'=>$query->area_4,
						'area_5'=>$query->area_5,
						
					);
		 header('Content-Type: application/json');
        echo json_encode($data);
			
	}
	
	
	function all_vendor_status(){
		$this->load->model('unit_class_model'); 		
		$id=$this->input->post('id');		
		$query=$this->unit_class_model->change_all_vendor_status($id);
			
			 if($query=='1'){
				 $data = array(
            'data' =>'Inactive',

        );
			 }else{
				 $data = array(
            'data' =>'Active',

        );
			 }
        header('Content-Type: application/json');
        echo json_encode($data);

	}
	
	
	function vendor_filter(){
		 $status=$this->input->post('status'); 
		$vendor=$this->unit_class_model->vendor_filter($status);
		//$state1=$data->state;
		$output='<table class="table table-striped table-hover table-bordered table-scrollable" id="dataTable2">
                    <thead>
            <tr> 
            
			  <th scope="col">Image</th>			
              <th scope="col"> Vendor Name </th>
              <th scope="col"> Contact Person</th>
              <th scope="col"> Contact No. </th>
              <th scope="col"> Adress </th>
              <th scope="col"> Purchase Count </th>	
            
              <th scope="col"> Discount % </th>	
              <th scope="col"> Profit Center </th>
              <th scope="col"> Industry </th>
              <th scope="col"> Parent Vendor </th>
              <th scope="col"> Total Amount </th>
              <th scope="col"> Contract Status </th>
			  <th scope="col"> Status </th>
              <th scope="col"> Action </th>
			  
            </tr>
          </thead>
          <tbody>';
		if(isset($vendor) && $vendor){
			//print_r($vendor);
                            foreach($vendor as $dgs){
							//$output.='<tr><td>'.$dgs->hotel_vendor_name.'</td><tr>';
							if( $dgs->hotel_vendor_image== '') { 
							$im="no_images.png"; 
							}
							else { 
							$im=$dgs->hotel_vendor_image;
							}
							
							
			$output.='<tr id="row_'.$dgs->hotel_vendor_id.'">';
			$output.='<td><img class="single_2"   width="60px" height="60px" src="'.base_url().'upload/vendor_detail/'.$im.'" alt=""/> </td>';
			$output.='<td>'.$dgs->hotel_vendor_name.'</td>';
			$output.='<td>'.$dgs->hotel_vendor_contact_person.'</td>';
			$output.='<td>'.$dgs->hotel_vendor_contact_no.'</td>';
			$output.='<td>'.$dgs->hotel_vendor_address.'</td>';
			$output.='<td>'.$dgs->hotel_vendor_purchase_count.'</td>';
			$output.='<td>'.$dgs->hotel_vendor_discount.'</td>';
			$output.='<td>'.$dgs->hotel_vendor_profit_center.'</td>';
			$output.='<td>'.$dgs->hotel_vendor_industry.'</td>';
			$output.='<td>'.$dgs->hotel_parent_vendor_id.'</td>';
			$output.='<td>'.$dgs->hotel_vendor_total_amt.'</td>';
			$output.='<td>'.$dgs->hotel_vendor_is_contracted.'</td>';
			
			if($dgs->status == 1){
				$b=" Active";
			}
			else{
				$b=" Inactive";
			}
			$output.='<td><button class="btn green btn-xs" id="status123" value="'.$dgs->hotel_vendor_id.'" onclick="status(this.value)"><span id="demostat'. $dgs->hotel_vendor_id.'">'.$b.'</span></button></td>';
			$output.='<td class="ba">';
			$output.='<div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">';
			
			$output.='<li><a onclick="soft_delete('.$dgs->hotel_vendor_contact_no.')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>';
			$output.='<li><a href="edit_area_code/'.$dgs->hotel_vendor_purchase_count.'" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li>';
			$output.='</ul></div>';
			$output.='</td>';
			
			
			
			$output.='</tr>';
			
		}
		$output.='</tbody></table>';
		echo $output;
		$data='abcd';
		 /*$data = array(
            'data' =>'Active',

        );
	   //header('Content-Type: application/json');*/
      // echo json_encode($data);
    }
	

}

function profit_center_filter(){
		$status=$this->input->post('status');
		$pc=$this->unit_class_model->profit_center_filter($status);
		//$state1=$data->state;
		$output=' <table class="table table-striped table-hover table-bordered table-scrollable" id="sample_10">
          <thead>
            <tr>
              <th scope="col">Profit Center Location </th>
              <th scope="col">Profit Center Balance </th>
			  <th scope="col"> Status </th>
              <th scope="col"> Action </th>			 
            </tr>
          </thead>
          <tbody>';
		 if(isset($pc) && $pc){
                        //print_r($pc);//exit;    
                            $i=1;
							
                            foreach($pc as $profit_center){
                                $class = ($i%2==0) ? "active" : "success";
                                //$r_id=$items->l_id;
			$output.='<tr id="row_'.$profit_center->id.'">';
			$output.='<td>'.$profit_center->profit_center_location;
			
			
			
			
			if($profit_center->profit_center_default =='1'){
				$output.='<font color="#22FF11"> <i class="fa fa-check"  aria-hidden="true"></i></font>';
				 }
			$output.='</td>';
			$output.='<td>'.$profit_center->profit_center_balance.'</td>';						
			//$output.='<td>';
			if($profit_center->profit_center_default=='1'){
				$de="disabled "; 
					}
					else{
						$de="";
						}
			
			if($profit_center->status == 1){ $st="Active";}
else{
$st="Inactive";
}	
			$output.='<td><button '.$de.'class="btn green btn-xs"  value="'.$profit_center->id.'" onclick="status(this.value)"><span id="demostat'.$profit_center->id.'">'.$st.'</span></button></td>';
			$output.='<td align="center" class="ba"><div class="btn-group"><button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button><ul class="dropdown-menu pull-right" role="menu"><li><a onclick="soft_delete('.$profit_center->id.')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>';
			$output.='<li><a onclick="edit_profit('.$profit_center->id.')" data-toggle="modal" class="btn blue btn-xs"> <i class="fa fa-edit"></i> </a></li></ul></div>';
			
		 
			
			$output.='</td>';
			$output.='</tr>';
			
		}
		$output.='</tbody></table>';
		echo $output;
		//$data='abcd';
		 /*$data = array(
            'data' =>'Active',

        );
	   //header('Content-Type: application/json');*/
      // echo json_encode($data);
    }
	

}


function booking_source_filter(){
		$status=$this->input->post('status');
		$data=$this->unit_class_model->booking_source_filter($status);
		//$state1=$data->state;
		$output='<table class="table table-striped table-hover table-bordered table-scrollable" id="sample_10">
            <thead>
              <tr>
                <th scope="col"> Id </th>
                <th scope="col"> Booking Source Name </th>
                <th scope="col"> Booking Source Date </th>
				<th scope="col">  Booking Source is ta </th>
				<th scope="col">  Booking Source Description </th>
				<th scope="col"> Status</th>
				<th scope="col"> Action</th>
				
              </tr>
            </thead>
            <tbody>';

			if(isset($data) && $data){
                        //print_r($pc);//exit;
                            foreach($data as $gst){
								
							$output.='<tr id="row_'.$gst->booking_source_id.'">';
							$output.='<td>'.$gst->booking_source_id.'</td>';
							$output.='<td>'.$gst->booking_source_name;
							 if($gst->booking_default =='1'){
							$output.='<font color="#22FF11"> <i class="fa fa-check"  aria-hidden="true"></i></font>';
							 }
							$output.='</td>';	
							$output.='<td>'.$gst->booking_source_date.'</td>';	
							$output.='<td>'.$gst->booking_source_is_ta.'</td>';
							$output.='<td>'.$gst->booking_source_description.'</td>';	
							if($gst->booking_default == '1'){$de=" disabled ";}else{$de=" ";}
							if($gst->status == 1){ $st="Active ";}
else{
$st="Inactive ";
}	
			$output.=' <td><button '.$de.'class="btn green btn-xs"  value="'.$gst->booking_source_id.'" onclick="status(this.value)"><span id="demostat'.$gst->booking_source_id.'">'.$st.'</span></button></td>';
					
							$output.='<td align="center" class="ba"><div class="btn-group">
                  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">';
							 if(($gst->booking_source_name!='Broker') && ($gst->booking_source_name!='Channel')&& ($gst->booking_source_name!='Corporate')&& ($gst->booking_source_name!='Hotel Website'))
								{ 
			
							$output.='<li><a'.$de.'onclick="soft_delete('.$gst->booking_source_id.')" data-toggle="modal" class="btn red btn-xs" ><i class="fa fa-trash"></i></a></li>';	
							
							//$output.='</td>';
							$output.='<li><a onclick="edit_booking_source('.$gst->booking_source_id.')" data-toggle="modal" class="btn green btn-xs"> <i class="fa fa-edit"></i> </a></li>';
								}
								else{
									$output.='<li><a '.$de.' data-toggle="modal" class="btn red btn-xs" onClick="my_alert();"><i class="fa fa-trash"></i></a></li>';	
							
							$output.='<li><a data-toggle="modal" class="btn green btn-xs" onClick="my_alert();"> <i class="fa fa-edit"></i> </a></li>';
								}
							
							}
			}
								
		$output.='</ul></div></td></tr></tbody></table>';
		 
		echo $output;
		
    
		 
		 

}


function booking_nature_visit_filter(){
		$status=$this->input->post('status');
		$data=$this->unit_class_model->booking_nature_visit_filter($status);
		//$state1=$data->state;
		$output='<table class="table table-striped table-hover table-bordered table-scrollable" id="dataTable2">
            <thead>
              <tr>
                <th scope="col"> Id </th>
                <th scope="col"> Booking Nature Visit Name </th>
                <th scope="col">booking Nature Visit Description </th>
                <th scope="col"> Booking Nature Visit Date Added </th>
				<th scope="col"> Status</th>
				<th scope="col"> Action</th>
				
              </tr>
            </thead>
            <tbody>';

			if(isset($data) && $data){
                        //print_r($pc);//exit;
                            foreach($data as $gst){
								
							$output.='<tr id="row_'.$gst->booking_nature_visit_id.'">';
							$output.='<td>'.$gst->booking_nature_visit_id.'</td>';
							$output.='<td>'.$gst->booking_nature_visit_name;
							if($gst->nature_visit_default =='1'){
								$output.='<font color="#22FF11"> <i class="fa fa-check"  aria-hidden="true"></i></font>';
							}
							$output.='</td>';	
							$output.='<td>'.$gst->booking_nature_visit_description.'</td>';	
							$output.='<td>'.$gst->booking_nature_visit_date_added.'</td>';
							if($gst->nature_visit_default == '1'){$de="disabled ";}else{$de="";}
								if($gst->status == 1){ $st="Active";}
else{
$st="Inactive";
}	
			$output.='<td> <button '.$de.'class="btn green btn-xs"  value="'.$gst->booking_nature_visit_id.'" onclick="status(this.value)"><span id="demostat'.$gst->	booking_nature_visit_id.'">'.$st.'</span></button></td>';
					
							//$output.='<td>'.$gst->nature_visit_default.'</td>';	
							$output.='<td class="ba"><div class="btn-group">
                  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
				  	<li><a '.$de.'onclick="soft_delete('.$gst->booking_nature_visit_id.')" data-toggle="modal" class="btn red btn-xs" ><i class="fa fa-trash"></i></a></li>';	
							
							$output.='<li><a onclick="edit_booking_nature_visit('.$gst->booking_nature_visit_id.')" data-toggle="modal" class="btn green btn-xs"> <i class="fa fa-edit"></i> </a></li></ul></div></td>';
							
			
		
							}
			}
								
		$output.='</tbody></table>';
		 
		echo $output;
		
    
		 
		 

}

function booking_status_type(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
		$data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'booking_status_type';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));		
		$data['data']=$this->unit_class_model->booking_status_type();
        $data['page'] = 'backend/dashboard/booking_status_type';
        $this->load->view('backend/common/index', $data);

    }
	
	
	function housekeeping_status(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
		$data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'housekeeping_status';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));		
		$data['data']=$this->unit_class_model->housekeeping_status();
        $data['page'] = 'backend/dashboard/housekeeping_status';
        $this->load->view('backend/common/index', $data);

    }
	
	function booking_status_bar_color(){		
		$id=$this->input->post('id');
		$code=$this->input->post('code');
		$data = array(
            'status_id' =>$id,
			'bar_color_code'=>$code
        );
		$this->unit_class_model->booking_status_bar_color($data,$this->session->userdata('user_hotel'));
		$data = array(
            'data' =>"success",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	function change_body_color(){		
		$id=$this->input->post('id');
		$code=$this->input->post('code');
		$data = array(
            'status_id' =>$id,
			'body_color_code'=>$code
        );
		$this->unit_class_model->booking_status_bar_color($data,$this->session->userdata('user_hotel'));
		$data = array(
            'data' =>"success",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}

	function change_text_color(){		
		$id=$this->input->post('id');
		$code=$this->input->post('code');
		$data = array(
            'status_id' =>$id,
			'text_color_code'=>$code
        );
		$this->unit_class_model->booking_status_bar_color($data,$this->session->userdata('user_hotel'));
		$data = array(
            'data' =>"success",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	
	
	
	function get_stock_inventory(){
		$id=$this->input->post('id');
		
		$query=$this->unit_class_model->get_stock_inventory($id);
		$data = array(
            'hotel_stock_inventory_id' =>$query->hotel_stock_inventory_id,
			'item_type' =>$query->item_type, 
			'a_category' =>$query->a_category,
			'a_name' =>$query->a_name,
			
			'make' =>$query->make,
			'item_des' =>$query->item_des, 
			'u_price' =>$query->u_price,
			'unit' =>$query->unit,
			
			'mode_of_payment' =>$query->mode_of_payment,
			'check_no' =>$query->check_no, 
			'check_bank_name' =>$query->check_bank_name,
			'draft_no' =>$query->draft_no,
			
			'draft_bank_name' =>$query->draft_bank_name,
			'ft_bank_name' =>$query->ft_bank_name, 
			'ft_account_no' =>$query->ft_account_no,
			'ft_ifsc_code' =>$query->ft_ifsc_code,
			
			'qty' =>$query->qty,
			'opening_qty' =>$query->opening_qty, 
			'closing_qty' =>$query->closing_qty,
			'warehouse' =>$query->warehouse,
			
			'l_stock' =>$query->l_stock, 
			'note' =>$query->note,
			'importance' =>$query->importance,
			'a_image' =>$query->a_image

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	 function check_welcome_kit(){
	
		  $id=$this->input->post('id');
		  $kit=$this->input->post('kit');															
		  
		if($kit=='1'){
		$data = array(
            'id' =>$id,
			'kit'=>'0'
			);
		}else{
			$data = array(
            'id' =>$id,
			'kit'=>'1'
			);
		}
		//$update_one=$this->unit_class_model->check_welcome_kit1();	//one	
		$a=$id;
		$b=$kit;
		
		$update_status=$this->unit_class_model->check_welcome_kit($data);//
		$c=$update_status->id;
		$d=$update_status->kit;
		
		$data1 = array(
            'aid' =>$a,
			'bkit'=>$b,
			
			'cid' =>$c,
			'dkit'=>$d
			);
		header('Content-Type: application/json');
        echo json_encode($data1);
		}
		
		

function edit_dg_set(){
		
		$id=$this->input->post('id');
		//$id='3';
		
		$query=$this->unit_class_model->edit_dg_set($id);
		$data = array(
            'dgset_id' =>$query->dgset_id,
			'gen_name' =>$query->gen_name, 
			'asset_code' =>$query->asset_code,
			'make' =>$query->make,
			'model' =>$query->model,
			'ratings' =>$query->ratings,
			'fuel' =>$query->fuel, 
			'cylinders' =>$query->cylinders,
			'rotation' =>$query->rotation,
			'design' =>$query->design,
			'alternator_make' =>$query->alternator_make,
			'fuel_consumption' =>$query->fuel_consumption, 
			'status' =>$query->status,
			'amc' =>$query->amc,
			'cooling' =>$query->cooling,
			'img' =>$query->img
		);
		//echo 'fvfd';
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	 function update_dgset(){
	 $this->load->model('dashboard_model'); 
    $this->load->model('unit_class_model'); 
		 $data['master_log'] = $this->dashboard_model->get_master_fuel_stock();
		 $data['rating'] = $this->dashboard_model->get_dg_rating();
      if($this->session->userdata('add_dgset') != '1')
        {
            $data['heading']='DG set';
            $data['sub_heading']='Add dgset';
            $data['description']='Add DG set here';
            $data['active']='add_dgset';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
               if($this->input->post())
                {   
                       $this->upload->initialize($this->set_upload_options());
                        if ($this->upload->do_upload('dg_image1')) {

                            $asset_image = $this->upload->data('file_name');
                            //print_r($asset_image); exit;
							$filename = $asset_image;
                            $source_path = './upload/' . $filename;
                            $target_path = './upload/dg_set/';                            
                            $config_manip = array(
                            'image_library' => 'gd2',
                            'source_image' => $source_path,
                            'new_image' => $target_path,
                            'maintain_ratio' => TRUE,
                            'create_thumb' => TRUE,
                            'thumb_marker' => '_thumb',
                            'width' => 250,
                            'height' => 250
                            );
                            $this->load->library('image_lib', $config_manip);
                            
                            if (!$this->image_lib->resize()) {
                                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                                    redirect('dashboard/add_asset');
                            }
                            $thumb_name = explode(".", $filename);
                            $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                            $this->image_lib->clear();
                        }
                        else 
                        {
                            //$this->session->set_flashdata('err_msg', $this->upload->display_errors());
                            //redirect('dashboard/add_asset');
							//$source_path = './upload/' . $filename;
                            $target_path = './upload/dg_set/'; 
							$asset_thumb="no_images.png";							
                        }
                        
					   
					   
					   
					   
                        
                        /* Images Upload*/ 
                        date_default_timezone_set("Asia/Kolkata");
                         $dgset_id=$this->input->post('dgset_id1');
                         $genname=$this->input->post('gen_name1');
                         $asset_code=$this->input->post('asset_code1');
                         $make=$this->input->post('make1');
                         $model=$this->input->post('model1');
                         $ratings=$this->input->post('ratings1');
                         $fuel=$this->input->post('fuel1');
                         $cylinder=$this->input->post('cylinder_choice1');
                         $rotation=$this->input->post('Rotation_choice1');
                         $design=$this->input->post('Design_choice1');
                         $alternator_make=$this->input->post('alternator_make1');
                         $fuel_consumption=$this->input->post('fuel_consumption1');
                         $status=$this->input->post('status1');
                         $amc=$this->input->post('amc1');
						$cooling=$this->input->post('Design_choice1');


                        $dgset = array(

                        //'hotel_id' => $this->session->userdata('user_hotel'),
                        'dgset_id' => $dgset_id,
                        'gen_name' => $genname,
                        'asset_code' =>$asset_code,
                        'make' => $make,
                        'model' => $model,
                        'ratings' => $ratings,
                        'fuel' => $fuel,
                        'cylinders' => $cylinder,
                        'rotation' => $rotation,
                        'design' => $design,
                        'alternator_make' => $alternator_make,
                        'fuel_consumption' => $fuel_consumption,
                        'status' => $status,
                        'amc' => $amc,
						'cooling' => $cooling,
						'img'=>$asset_thumb
						);
                        
                       // print_r($dgset);
                       // exit();
                      $query = $this->unit_class_model->update_dgset($dgset);
					  //print_r($query);exit;
                        if (isset($query) && $query) 
                        {
                            $this->session->set_flashdata('succ_msg', "DGset Added Successfully!");
                            redirect('dashboard/all_dgset');
                        } 
                        else 
                        {
                            $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                            redirect('dashboard/all_dgset');
                        }
                        
                }
                else
                {
                    $data['page'] = 'backend/dashboard/all_dgset';
                    $this->load->view('backend/common/index', $data);
                }
            
        }
        else
        {
            redirect('dashboard');
        }
    }
	
	
	function edit_corporate(){
		
		$id=$this->input->post('id');
		$query=$this->unit_class_model->edit_corporate($id);
		$data = array(
            'hotel_corporate_id' =>$query->hotel_corporate_id,
			'name' =>$query->name, 
			'address' =>$query->address,
			'g_contact_no' =>$query->g_contact_no,
			'industry' =>$query->industry,
			'no_of_emp' =>$query->no_of_emp,
			'contract_discount' =>$query->contract_discount, 
			'contract_end_date' =>$query->contract_end_date,
			'no_of_booking' =>$query->no_of_booking,
			'total_transaction' =>$query->total_transaction,			
			'industry_type' =>$query->industry_type,
			'field' =>$query->field,
			'rating' =>$query->rating,
			'gstin' =>$query->gstin
        );
		//echo 'fvfd';
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	function update_corporate(){
		
	 if($this->session->userdata('update_corporate') != '1')
        {
            $data['heading']='corporate';
            $data['sub_heading']='Edit Corporate';
            $data['description']='Edit Corporate here';
            $data['active']='update';  
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
                if($this->input->post())
                {   
                        //$id_g = $this->input->post('g_contact_no');
                        /* Images Upload */
                            
                        
                         $c_id=$this->input->post('hid');
                         $name=$this->input->post('name1');
                         $address=$this->input->post('address1');
                         $g_contact_no=$this->input->post('g_contact_no1');
                         $industry=$this->input->post('industry1');
                         $no_of_emp=$this->input->post('no_of_emp1');
                         $contract_discount=$this->input->post('contract_discount1');
                         $contract_end_date=$this->input->post('contract_end_date1');
						 $industry_type=$this->input->post('industry_type');                         
						 $field=$this->input->post('field');                         
						 $rating=$this->input->post('rating'); 
						 $gstin=$this->input->post('gstin'); 
						 
                         $corporate=array(
                                'hotel_corporate_id'=>$c_id,
                                'name'=>$name,
                                'address'=>$address,
                                'g_contact_no'=>$g_contact_no,
                                'industry'=>$industry,
								'no_of_emp'=>$no_of_emp,
								'contract_discount'=>$contract_discount,
								'contract_end_date'=>$contract_end_date,
								'industry_type'=>$industry_type,
                                'field'=>$field,
                                'rating'=>$rating,
                                'gstin'=>$gstin
                         );
						 
					//print_r($corporate); exit;
					
                      $query = $this->unit_class_model->update_corporate($corporate);
                       
						
						if (isset($query) && $query) 
                        {
                            $this->session->set_flashdata('succ_msg', "Updated Successfully!");
                            redirect('dashboard/all_corporate');
                        } 
                        else 
                        {
                            $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                            redirect('dashboard/all_corporate');
                        }
                        
                }
                else
                {
                    $data['page'] = 'backend/dashboard/all_corporate';
                    $this->load->view('backend/common/index', $data);
                }
            
        }
        else
        {
            redirect('dashboard');
        }
	 
 }
 
 function edit_event(){
		
		$id=$this->input->post('id');
		
		$query=$this->unit_class_model->edit_event($id);
		$data = array(
            'e_id' =>$query->e_id,
			'e_name' =>$query->e_name, 
			'e_from' =>$query->	e_from,
			'e_upto' =>$query->	e_upto,
			'e_notify' =>$query->e_notify,
			'e_seasonal' =>$query->e_seasonal,
			'hotel_id' =>$query->hotel_id,			
			'event_color' =>$query->event_color,
			'event_text_color' =>$query->event_text_color
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	function edit_assets_type(){
		
		$id=$this->input->post('id');
		
		$query=$this->unit_class_model->edit_assets_type($id);
		$data = array(
            'asset_type_id' =>$query->asset_type_id,
			'asset_icon' =>$query->asset_icon, 
			'asset_type_name' =>$query->asset_type_name,
			'asset_type_description' =>$query->asset_type_description
			
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	function edit_stock(){
		
		$id=$this->input->post('id');
		
		$query=$this->unit_class_model->edit_stock($id);
		$data = array(
            'id' =>$query->id,
			'name' =>$query->name, 
			'description' =>$query->description,
			'change_date' =>$query->change_date
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	function edit_warehouse_aj(){
		
		$id=$this->input->post('id');
		
		$query=$this->unit_class_model->edit_warehouse_aj($id);
		date_default_timezone_set("Asia/Kolkata");
		$cur_date=date('Y-m-d H:i:s');
		
		$data = array(
            'id' =>$query->id,
			'name' =>$query->name, 
			'description' =>$query->description,
			'change_date' =>$cur_date			
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	function get_booking_nature_visit(){
		
		$id=$this->input->post('id');
		
		$query=$this->unit_class_model->fetch_booking_nature_visit($id);
		date_default_timezone_set("Asia/Kolkata");
		$cur_date=date('Y-m-d H:i:s');
		
		$data = array(
            'booking_nature_visit_id' =>$query->booking_nature_visit_id,
			'booking_nature_visit_name' =>$query->booking_nature_visit_name, 
			'booking_nature_visit_description' =>$query->booking_nature_visit_description,
			'booking_nature_visit_date_added' =>$query->booking_nature_visit_date_added,
			'nature_visit_default' =>$query->nature_visit_default,		
			'status' =>$query->status,			
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	function get_edit_booking_source(){
		
		$id=$this->input->post('id');
		
		$query=$this->unit_class_model->get_edit_booking_source($id);
		date_default_timezone_set("Asia/Kolkata");
		$cur_date=date('Y-m-d H:i:s');
		
		$data = array(
            'booking_source_id' =>$query->booking_source_id,
			'booking_source_name' =>$query->booking_source_name, 
			'booking_source_hotel_id' =>$query->booking_source_hotel_id,
			'booking_source_is_ta' =>$query->booking_source_is_ta,
			'booking_source_description' =>$query->booking_source_description,		
			'booking_default' =>$query->booking_default,
			'type'=>$query->source_type,		
			'status' =>$query->status			
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	
	function get_edit_profit_center(){
		
		$id=$this->input->post('id');
		
		$query=$this->unit_class_model->get_edit_profit_center($id);
		date_default_timezone_set("Asia/Kolkata");
		$cur_date=date('Y-m-d H:i:s');
		
		$data = array(
            'id' =>$query->id,
			'profit_center_location' =>$query->profit_center_location, 
			'profit_center_balance' =>$query->profit_center_balance,
			'profit_center_des' =>$query->profit_center_des,
			'profit_center_default' =>$query->profit_center_default,		
			'status' =>$query->status	
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	function display_user_permission()
	{
	
			
		print_r($this->arraay);
	}
	
	function change_Primary_color(){		
		$id=$this->input->post('id');
		$code=$this->input->post('code');
		$data = array(
            'status_id' =>$id,
			'color_primary'=>$code
        );
		$this->unit_class_model->change_Primary_color($data,$this->session->userdata('user_hotel'));
		$data = array(
            'data' =>"success",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	function change_Secondary_color(){		
		$id=$this->input->post('id');
		$code=$this->input->post('code');
		$data = array(
            'status_id' =>$id,
			'color_secondary'=>$code
        );
		$this->unit_class_model->change_Primary_color($data);//primary & secondary all can apply
		$data = array(
            'data' =>"success",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	
	function marketing_personnel(){
		 //$found = in_array("91",$this->arraay);
		
		//if($found)
		{		
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Setting';
        $data['sub_heading'] = 'Metadata';
        $data['description'] = '';
        $data['active'] = 'marketing_personnel';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		//$data['unit'] = $this->dashboard_model->all_unit_type();
        //$data['unitTypeName'] = $this->dashboard_model->getUnitTypeName();
		$data['data']=$this->unit_class_model->all_marketing_personnel();
        $data['page'] = 'backend/dashboard/marketing_personnel';
		$data['stat']='List of Marketing Personel';
        $this->load->view('backend/common/index', $data);

    }
	/*else{
		redirect('dashboard');
	}*/
	}
	
			function add_marketing_personel(){
					 
				
		
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
		
		$name=$this->input->post('name');
		$contact_no=$this->input->post('contact_no');
		$email_id=$this->input->post('email_id');
		$profit_center=$this->input->post('profit_center');
		$designation=$this->input->post('designation');
		
		date_default_timezone_set("Asia/Kolkata");
		$cur_date=date('Y-m-d H:i:s');
		$data=array(

						'name'=>$name,
						'contact_no'=>$contact_no,
						'email'=>$email_id,
						'profit_center'=>$profit_center,
						'designation'=>$designation,
						'created_date'=>$cur_date,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
										
					);
					print_r($data);
				$query=$this->unit_class_model->add_marketing_personel($data);
				
				
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
						redirect(base_url().'unit_class_controller/marketing_personnel');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
						redirect(base_url().'unit_class_controller/marketing_personnel');
					}
        
		
	
	
}
	
	function delete_marketing_personnel(){
		// $found = in_array("106",$this->arraay);
		//if($found)
		{
		
		$id=$this->input->post('id');
		
		$this->unit_class_model->delete_marketing_personnel($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}/*else
		{
			 $data = array(
            'data' =>"You do not have the previlage to do this",

        );
			header('Content-Type: application/json');
        echo json_encode($data);
		}*/
	}
	
	function edit_marketing_personel(){
		
		$id=$this->input->post('id');
		
		$query=$this->unit_class_model->edit_marketing_personel($id);
		
		
		$data = array(
            'id' =>$query->id,
			'name' =>$query->name, 
			'contact_no' =>$query->contact_no,
			'email' =>$query->email,
			'profit_center' =>$query->profit_center,		
			'designation' =>$query->designation			
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	function update_marketing_personel(){
		
		$hid=$this->input->post('hid');
		$name1=$this->input->post('name1');
		$contact_no1=$this->input->post('contact_no1');
		$email1=$this->input->post('email1');
		$profit_center1=$this->input->post('profit_center1');
		$designation1=$this->input->post('designation1');
		
		
		$data=array(
						'id'=>$hid,
						'name'=>$name1,
						'contact_no'=>$contact_no1,
						'email'=>$email1,
						'profit_center'=>$profit_center1,
						'designation'=>$designation1
						
										
					);
					//print_r($data);exit;
					
				$query=$this->unit_class_model->update_marketing_personel($data);
				
				
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Update Succefully');
						redirect(base_url().'unit_class_controller/marketing_personnel');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Update Not Succefully');
						redirect(base_url().'unit_class_controller/marketing_personnel');
					}
		
	}
	
	//loader booking_nature_visit
	function quick_room_add(){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Room';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'quick_room_add';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		$data['category'] = $this->unit_class_model->all_unit_type_category_data();
		$data['data']=$this->unit_class_model->get_booking_source();
        $data['page'] = 'backend/dashboard/quick_room_add';
		$data['stat']='List of All Active Booking Source';
        $this->load->view('backend/common/index', $data);

    }
	
	
	
	function add_quick_room(){
		$this->load->library('upload');
         $this->load->library('image_lib');
		//echo  base_url();exit;
		//$found = in_array("10",$this->arraay);
		//if($found){
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
		//$image=$_FILES['image']['name'];
		//$image=$this->input->post('image');
		//print_r($image);exit;
		$room_no=$this->input->post('room_no1');
		$room_name=$this->input->post('room_name1');
		$unit_type=$this->input->post('unit_type1');
		$unit_class=$this->input->post('unit_class1');
		$bed_capacity=$this->input->post('bed_capacity1');
		//$max_occupancy=$this->input->post('max_occupancy1');
		//$base_rent=$this->input->post('base_rent1');
		$floor_id=$this->input->post('floor_id1');
		$data=array(

						//'image'=>$image,
						'room_no'=>$room_no,
						'room_name'=>$room_name,
						'unit_id'=>$unit_type,
						//'room_rent'=>$base_rent,
						'room_bed'=>$bed_capacity,
						//'max_occupancy'=>$max_occupancy,
						'floor_no'=>$floor_id,
						
						
					   
					);
					//echo "<pre>";
		//			print_r($data);exit;
				$query=$this->unit_class_model->add_quick_room($data);


				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
						redirect(base_url().'unit_class_controller/quick_room_add');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
						redirect(base_url().'unit_class_controller/quick_room_add');
					}
        //$this->all_unit_class();
		
	/*}else{
		redirect('dashboard');
	}*/
	}
	
	
	function check_room_no(){
		  $a=$this->input->post('no');
		$res=$this->unit_class_model->check_room_no($a);
		if($res>0){			
		$data = array(
            'ans' =>'Not Available',
			);
		}
		else{
			$data = array(
            'ans' =>'Available',
			);
			
		}
		header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	
	
	function enquiry_management(){
		 $found = in_array("91",$this->arraay);
		
		if($found)
		{		
		$this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Enquery & Sales';
        $data['sub_heading'] = '';
        $data['description'] = '';
        $data['active'] = 'enquiry_management';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		$data['data']=$this->unit_class_model->all_enquery_list();
        $data['page'] = 'backend/dashboard/enquery_management';
		$data['enquery_management']=$this->unit_class_model->enquery_management();
		$data['unit_class']=$this->unit_class_model->all_unit_clsss();
		$data['stat']='List of All Group Booking Enquiry';
        $this->load->view('backend/common/index', $data);

    }else{
		redirect('dashboard');
	}
	}
	
	//add_enquery_management
	function add_enquery_management(){		
		$this->load->model('unit_class_model');
		$guest_name=$this->input->post('guest_name1');
		$guestId=$this->input->post('guestId');
		$guest_type=$this->input->post('guest_type1');
		$form=$this->input->post('from1');
		$to=$this->input->post('to1');
		$type=$this->input->post('type1');
		$room=$this->input->post('room1');
		$adult=$this->input->post('adult1');
		$child=$this->input->post('child1');
		$importance=$this->input->post('importance1');
		$preferrence=$this->input->post('preferrence1');
		$comment=$this->input->post('comment1');
		
		
		$address=$this->input->post('address');
		$pin=$this->input->post('pin');
		$phone=$this->input->post('phone');
		$email=$this->input->post('email');
		$comming_from=$this->input->post('comming_from');
		$nature_visit=$this->input->post('nature_visit');
		$source=$this->input->post('source1');
		$marketing=$this->input->post('marketing');
		$status=$this->input->post('status');
		
		$profit_center=$this->input->post('profit_center');
		
		
		
		$data=array(
						'guestId'=>$guestId,
						'guest_name'=>$guest_name,
						'guest_type'=>$guest_type,
						'form_date'=>$form,
						'to_date'=>$to,
						'room_type'=>$type,
						'room_no'=>$room,
						'adult_no'=>$adult,
						'child_no'=>$child,
						'importance'=>$importance,
						'preference'=>$preferrence,
						'comment'=>$comment,
						
						'address'=>$address,
						'pin'=>$pin,
						'phone'=>$phone,
						'email'=>$email,
						'comming_from'=>$comming_from,
						'nature_visit'=>$nature_visit,
						'source'=>$source,
						'marketing_executive'=>$marketing,
						'status'=>$status,
						'profit_center'=>$profit_center,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
					);
					
					//print_r($data);exit;
				$query=$this->unit_class_model->add_enquery_management($data);


				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Succefully');
					}
					else{
						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Succefully');
					}
        //$this->all_unit_class();
		redirect(base_url().'unit_class_controller/enquery_management');
	}
	
	
	//delete booking_nature_visit
	
	function delete_enquery_management(){
		/* $found = in_array("106",$this->arraay);
		if($found)
		{*/
		
		$id=$this->input->post('id');
		
		$this->unit_class_model->delete_enquery_management($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	/*}else
		{
			 $data = array(
            'data' =>"You do not have the previlage to do this",

        );
			header('Content-Type: application/json');
        echo json_encode($data);
		}*/
	}
	
	
	
	function edit_enquery_management(){		
		
		if($this->input->post()){
				$id=$this->input->post('hid');
				$guest_name=$this->input->post('guest_name2');
				$guest_type=$this->input->post('guest_type2');
				$to=$this->input->post('to2');
				$from=$this->input->post('from2');
				$room_type=$this->input->post('type2');
				$room_no=$this->input->post('room2');
				$no_adult=$this->input->post('adult2');
				$no_child=$this->input->post('child2');
				$importance=$this->input->post('importance2');
				$preference=$this->input->post('preferrence2');
				$comment=$this->input->post('comment2');
				
				
				$address=$this->input->post('address1');
				$pin=$this->input->post('pin1');
				$phone=$this->input->post('phone1');
				$email=$this->input->post('email1');
				$comming_from=$this->input->post('comming_from1');
				$nature_visit=$this->input->post('nature_visit2');
				$source=$this->input->post('source2');
				$marketing=$this->input->post('marketing2');
				$status=$this->input->post('status2');
				$profit_center=$this->input->post('profit_center1');
				
				
				
				
				
				
				$data=array(
					'guest_name'=>$guest_name,
					'guest_type'=>$guest_type,
					'to_date'=>$to,
					'form_date'=>$from,
					'room_type'=>$room_type,
					'room_no'=>$room_no,
					'adult_no'=>$no_adult,
					'child_no'=>$no_child,
					'importance'=>$importance,
					'preference'=>$preference,
					'comment'=>$comment,
					'id'=>$id,
					
					
					'address'=>$address,
					'pin'=>$pin,
					'phone'=>$phone,
					'email'=>$email,
					'comming_from'=>$comming_from,
					'nature_visit'=>$nature_visit,
					'source'=>$source,
					'marketing_executive'=>$marketing,
					'status'=>$status,
					'profit_center'=>$profit_center
					);
					//print_r($data);
					//echo $data['id'];
					//exit;
				$query=$this->unit_class_model->update_enquery_management($data);
				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Succefully');
					}

					else{

						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data updated Not Succefully');
					}
				redirect(base_url().'unit_class_controller/enquery_management');

			}
        $data['page'] = 'backend/dashboard/enquery_management';		
        $this->load->view('backend/common/index', $data);

					
				//$this->load->view('edit_unit_class',$m);
	}
	
	function get_enquery_management(){
		
		$id=$this->input->post('id');		
		$query=$this->unit_class_model->get_enquery_management($id);
		date_default_timezone_set("Asia/Kolkata");
		$cur_date=date('Y-m-d H:i:s');
		
		$data = array(
            'id' =>$query->id,
			'guest_name' =>$query->guest_name, 
			'guest_type' =>$query->guest_type,
			'form_date' =>$query->form_date,
			'to_date' =>$query->to_date,		
			'room_type' =>$query->room_type,			
			'room_no' =>$query->room_no,			
			'adult_no' =>$query->adult_no,			
			'child_no' =>$query->child_no,			
			'importance' =>$query->importance,			
			'preference' =>$query->preference,			
			'comment' =>$query->comment,
			'address'=>$query->address,
			'pin'=>$query->pin,
			'phone'=>$query->phone,
			'email'=>$query->email,
			'comming_from'=>$query->comming_from,
			'nature_visit'=>$query->nature_visit,
			'nature_visit'=>$query->nature_visit,
			'source'=>$query->source,
			'marketing_executive'=>$query->marketing_executive,
			'status'=>$query->status,
			'profit_center'=>$query->profit_center
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	function all_shift_filter(){
		$status=$this->input->post('status');
		$data=$this->unit_class_model->all_shift_filter($status);
		//$state1=$data->state;
		$output='<table class="table table-striped table-hover table-bordered table-scrollable" id="dataTable2">
            <thead>
              <tr>
				  <th scope="col"> Shift Id </th>
				  <th scope="col"> Shift Name </th>
				  <th scope="col"> Shift Start Time </th>
				  <th scope="col"> Shift End Time </th>
				  <th scope="col"> Status</th>
				  <th scope="col"> Action </th>
						
              </tr>
            </thead>
            <tbody>';

			if(isset($data) && $data){
                        //print_r($pc);//exit;
                            foreach($data as $gst){
							$output.='<tr id="row_'.$gst->shift_id.'">';
							$output.='<td>'.$gst->shift_id.'</td>';
							$output.='<td>'.$gst->shift_name.'</td>';
							$output.='<td>'.$gst->shift_opening_time.'</td>';
							$output.='<td>'.$gst->shift_closing_time.'</td>';	
							//$output.='<td>'.$gst->status.'</td>';
							if($gst->status == '0'){$de=" ";}else{$de="";}
								if($gst->status == 1){ $st="Active";}
else{
$st="Inactive";
}	
			$output.='<td> <button '.$de.'class="btn green btn-xs"  value="'.$gst->shift_id.'" onclick="status(this.value)"><span id="demostat'.$gst->	shift_id.'">'.$st.'</span></button></td>';
					
							$output.='<td class="ba" align="center"><div class="btn-group no-bgbut"><button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">';
							$output.='<li><a onclick="edit_booking_nature_visit('.$gst->shift_id.')" data-toggle="modal" class="btn blue btn-xs"> <i class="fa fa-edit"></i> </a></li>';	
							
							//$output.='<li><a disabled onclick="soft_delete('.$gst->shift_id.')" data-toggle="modal" class="btn red" ><i class="fa fa-trash"></i></a></li>';
							$output.='</ul></div></td>';
							
							}
			}
								
		$output.='</tbody></table>';
		 
		echo $output;
		
    
		 
		 

}
	
	
	
function shift_status(){
	$this->load->model('unit_class_model'); 		
		$id=$this->input->post('id');		
		$query=$this->unit_class_model->shift_status($id);
			
			 if($query=='1'){
				 $data = array(
            'data' =>'Inactive',

        );
			 }else{
				 $data = array(
            'data' =>'Active',

        );
			 }
        header('Content-Type: application/json');
        echo json_encode($data);
	}
	
	
	
	
	
	function enquery_list_filter(){
		$status=$this->input->post('status');
		$data=$this->unit_class_model->enquery_list_filter($status);
		//$state1=$data->state;
		$output='<table class="table table-striped table-hover table-bordered table-scrollable" id="dataTable2">
            <thead>
              <tr>
            <th scope="col"> Id </th>
			<th scope="col"> Guest Name </th>
			<th scope="col"> Guest type </th>
			<th scope="col">From date</th>
			<th scope="col"> To date </th>
			<th scope="col">Room Type</th>
			<th scope="col">Room no</th>
			<th scope="col">Adult</th>
			<th scope="col">Child</th>
			<th scope="col">importance</th>
			<th scope="col">Preference</th>
			<th scope="col">Comment</th>			
			<th scope="col">Status</th>			
			<th scope="col"> Action</th>
              </tr>
            </thead>
            <tbody>';

			if(isset($data) && $data){
                        //print_r($pc);//exit;
                            foreach($data as $gst){								
							$output.='<tr id="row_'.$gst->id.'">';
							$output.='<td>'.$gst->id.'</td>';
							$output.='<td>'.$gst->guest_name.'</td>';
							$output.='<td>'.$gst->guest_type.'</td>';
							$output.='<td>'.$gst->form_date.'</td>';
							$output.='<td>'.$gst->to_date.'</td>';
							$output.='<td>'.$gst->room_type.'</td>';	
							$output.='<td>'.$gst->room_no.'</td>';
							$output.='<td>'.$gst->adult_no.'</td>';
							$output.='<td>'.$gst->child_no.'</td>';						
							
							if($gst->importance=='3'){$i='#4a638a; color:#a0f3eb;';} else if($gst->importance=='2'){$i='#38ba93; color:#85f3b4';}else {$i='#438368; color:#aec1e6;';}
							if($gst->importance=='3'){ $a="High";} else if($gst->importance=='2'){$a='Medium';}else {$a='Low';}
							$output.='<td align="center"><span class="label" style="background-color:'.$i.' >'.$a.'</span></td>';
							//$output.='<td>'.$gst->importance.'</td>';
							$output.='<td>'.$gst->preference.'</td>';
							$output.='<td>'.$gst->comment.'</td>';
							$output.='<td>'.$gst->status.'</td>';
							$de='';$st='';
			$output.='<td> <button '.$de.'class="btn green btn-xs"  value="'.$gst->id.'" onclick="status(this.value)"><span id="demostat'.$gst->id.'">'.$st.'</span></button></td>';
					
							//$output.='<td>'.$gst->nature_visit_default.'</td>';	
							$output.='<td><a onclick="edit_booking_nature_visit('.$gst->id.')" data-toggle="modal" class="btn blue btn-xs"> <i class="fa fa-edit"></i> </a>';	
							
							$output.='<a '.$de.'onclick="soft_delete('.$gst->id.')" data-toggle="modal" class="btn red btn-xs" ><i class="fa fa-trash"></i></a></td>';
							
			
		
							}
			}
								
		$output.='</tbody></table>';
		 
		echo $output;
		
    
		 
		 

}
	
	
	function check_shift_time(){
		$start=$this->input->post('start');
		$end=$this->input->post('end');
		
		$a=$this->unit_class_model->check_shift_time($start,$end);
		$data = array(		
            'data' =>$a,
			'a'=>$a,
			//alert('done'),
        ); 
        header('Content-Type: application/json');
        echo json_encode($data);
	}
	
	function change_profile_pic(){
		$code=$this->input->post('code');
		$id=$this->input->post('id');
		
		//echo 'hello';exit;
		if($this->input->post())
                {   
                       $this->upload->initialize($this->set_upload_options());
                        if ($this->upload->do_upload('dg_image')) {
							
							

                            $asset_image = $this->upload->data('file_name');
                            //print_r($asset_image); exit;
							$filename = $asset_image;
                            $source_path = './upload/' . $filename;
                            $target_path = './upload/admin/';                            
                            $config_manip = array(
                            'image_library' => 'gd2',
                            'source_image' => $source_path,
                            'new_image' => $target_path,
                            'maintain_ratio' => TRUE,
                            'create_thumb' => TRUE,
                            'thumb_marker' => '_thumb',
                            'width' => 250,
                            'height' => 250
                            );
                            $this->load->library('image_lib', $config_manip);
                            
                            if (!$this->image_lib->resize()) {
                                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                                    redirect('dashboard/add_asset');
                            }
                            $thumb_name = explode(".", $filename);
                            $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                            $this->image_lib->clear();
                        }
                        else 
                        {
                            //$this->session->set_flashdata('err_msg', $this->upload->display_errors());
                            //redirect('dashboard/add_asset');
							//$source_path = './upload/' . $filename;
                            $target_path = './upload/dg_set/'; 
							$asset_thumb="no_images.png";							
                        }
                        
					   
					   
					   
					   
                        
                        /* Images Upload*/ 
                        date_default_timezone_set("Asia/Kolkata");
                         //$dgset_id=$this->input->post('dgset_id1');
                        // $genname=$this->input->post('gen_name1');
                         


                        $dgset = array(
                        'admin_image' => $asset_thumb,
                        'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
						);
                        
                       // print_r($dgset);
                       // exit();
                      $query = $this->unit_class_model->update_admin_details($dgset);
					  //print_r($query);exit;
                        if (isset($query) && $query) 
                        {
                           echo $a=1;
                        } 
                        else 
                        {
                             echo $a=0;
                        }
                       
		
		
	}
	
	/////////////////////////////

	}
function discount_rules(){
   $this->load->model('dashboard_model');
		$this->load->model('unit_class_model');
        $data['heading'] = 'Guests';
        $data['sub_heading'] = 'Discount Plan';
        $data['description'] = 'List of All Discount Plans';
        $data['active'] = 'discount_rules';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
		//$data['unit_class'] = $this->dashboard_model->get_unit_class_list();
        $data['admin_name'] = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
		
		$data['discount_rule']=$this->unit_class_model->discount_rules();
        $data['page'] = 'backend/dashboard/discount_rules';
		$data['stat']='List of All Active Booking Source';
        $this->load->view('backend/common/index', $data);


}
function add_discount_rules(){		
		$this->load->model('unit_class_model');
		
		$rule_name=$this->input->post('rule_name');
		$description=$this->input->post('description');
		$dp_discount=$this->input->post('discount');
		$discount_start_parcentage=$this->input->post('discount_Start_parcentage');
		$discount_end_parcentage=$this->input->post('discount_End_parcentage');
		$room_rent=$this->input->post('room_rent');
		$food_charge=$this->input->post('food_charge');
		
		$data=array(	
					'rule_name'=>$rule_name,
					'description'=>$description,
					'dp_discount'=>$dp_discount,
					'discount_start_percentage'=>$discount_start_parcentage,
					'discount_end_percentage'=>$discount_end_parcentage,
					'discount_room_rent_applicable'=>$room_rent,
					'food_charge_applicable'=>$food_charge,
					'applicable'=>implode(",",$this->input->post('applicable')),
					'hotel_id'=>$this->session->userdata('user_hotel'),
					'user_id'=>$this->session->userdata('user_id')
					
					);
				$query=$this->unit_class_model->add_discount_rules($data);


				if($query){
						$msg="inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Successfully');
					}
					else{
						$msg=" not inserted";
						$this->session->set_flashdata('succ_msg', 'Data Inserted Not Successfully');
					}
        //$this->all_unit_class();
		redirect(base_url().'unit_class_controller/discount_rules');
	}
	
	
	
	function delete_discount_rule(){
		
		$id=$this->input->post('id');		
		$this->unit_class_model->delete_discount_rule($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	
	}
	
	function edit_discount_rule(){
		
		$id=$this->input->post('id');		
		$query=$this->unit_class_model->get_discount_plan_by_id($id);
		date_default_timezone_set("Asia/Kolkata");
		$cur_date=date('Y-m-d H:i:s');
		
		$data = array(
            'id' =>$query->id,
			'name' =>$query->rule_name, 
			'description' =>$query->description,
			'dp_discount' =>$query->dp_discount,
			'discount_start_percentage' =>$query->discount_start_percentage,
			'discount_end_percentage' =>$query->discount_end_percentage,
			'applicable' =>$query->applicable,
			'room_rent' =>$query->discount_room_rent_applicable	,		
			'food_charge' =>$query->food_charge_applicable					
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
function update_discount_rule(){
	
                if($this->input->post())
                {  
                         $id=$this->input->post('id1');
                         $rule_name=$this->input->post('rule_name1');
                         $description=$this->input->post('description1');
						 $discount=$this->input->post('discount1');
                         $discount_start_percentage=$this->input->post('discount_start_percentage1');
                         $discount_end_percentage=$this->input->post('discount_end_percentage1');
                         //$applicable='applicable'=>implode(",",$this->input->post('applicable1')),
                         $room_rent=$this->input->post('room_rent1');
                         $food_charge=$this->input->post('food_charge1');
						
                         
                         $corporate=array(
                                'id'=>$id,
                                'rule_name'=>$rule_name,
                                'description'=>$description,
                                'dp_discount'=>$discount,
                                'applicable'=>implode(",",$this->input->post('applicable1')),
                                'discount_start_percentage'=>$discount_start_percentage,
                                'discount_end_percentage'=>$discount_end_percentage,
                                'discount_room_rent_applicable'=>$room_rent,
								'food_charge_applicable'=>$food_charge
                            );
					//print_r($corporate);exit;
                      $query = $this->unit_class_model->update_discount_rule($corporate);
                       
						
						if (isset($query) && $query) 
                        {
                            $this->session->set_flashdata('succ_msg', "Updated Successfully!");
                            //redirect('dashboard/discount_rules');
                        } 
                        else 
                        {
                            $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                           // redirect('dashboard/discount_rules');
                        }
                        redirect(base_url().'unit_class_controller/discount_rules');
                }
                else
                {
                    $data['page'] = 'backend/dashboard/discount_rules';
                    $this->load->view('backend/common/index', $data);
                }
            
        
        
 }
 
 function add_laundry_service(){
     
     
			 //$found = in_array("7",$this->arraay);
		if(1)
		{

            $data['heading'] = 'Service';
            $data['sub_heading'] = 'Laundry Services';
            $data['description'] = 'Add Laundry Service';
            $data['active'] = 'add_laundry_service';
            $data['usage_log'] = $this->dashboard_model->all_usage_log();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['transaction'] = $this->unit_class_model->transaction_laundry();
            $data['page'] = 'backend/dashboard/add_laundry_service';
            $this->load->view('backend/common/index', $data);

                 if($this->input->post())
                {   
                        
                        date_default_timezone_set("Asia/Kolkata");
						//echo "<pre>";
                        //print_r($this->input->post());exit;
						 
						 $hidTax=$this->input->post('hidTax');
						 $response=$this->input->post('taxresponse');
						 $booking_id=$this->input->post('booking_id_hid');
						 
						 $booking_type=$this->input->post('booking_type');
						 $group_booking=$this->input->post('group_booking');
						 $single_booking=$this->input->post('single_booking');
						 $billing_pref=$this->input->post('billing_pref');
						 
						 $guest_id=$this->input->post('hidden_id');
						 
						 
						 $Guest_name=$this->input->post('Guest_name_new');
						// echo  'utfuy'.$Guest_name;
						// $Guest_name=$this->input->post('g_name_hid');
						 $guest_type=$this->input->post('guest_type');
						 $received_date=date("Y-m-d h:i:s", strtotime($this->input->post('received_date')));
						 $delivery_date=date("Y-m-d h:i:s", strtotime($this->input->post('delivery_date')));
						 $special_request=$this->input->post('special_request');
						 
						 $vat=$this->input->post('vat');
						 $service_tax=$this->input->post('service_tax');
						 $service_charge=$this->input->post('service_charge');
						 $discount=$this->input->post('discount');
						 $grand_total=$this->input->post('grand_total');
						 //$total=$this->input->post('hidden_grand_total');
						// $total=$this->input->post('total_amount_hidden');
						 $total=$this->input->post('pay_amount');
						 
						 $guest_booking_id=$this->input->post('guest_booking_id');
						 $quantity=$this->input->post('quantity');
						 
						 
						 
						 $payments_due=$this->input->post('due_amount');
						if($grand_total-$total == 0){
							$stat="fullpaid";
						}
						else if($grand_total==$payments_due){
							$stat="unpaid";
						}else {
							$stat="partial";
						}
						 
						 
						 /*
pay_amount,mop,
		  checkno,check_bank_name,
		  
		  card_no,card_name,card_exp_date,card_bank_name

		  draft_no,draft_bank_name,
		  
		  ft_bank_name,ft_account_no,ft_ifsc_code,ft_approved_by,ft_recievers_name,ft_paid_by,ft_recievers_desig,profit_center1,
		  
		  
		  */
						 //PAYMENT SECTION START
						  $pay_amount=$this->input->post('pay_amount');
						  $mop=$this->input->post('pay_mode');
						  if($mop==''){
							  $mop='';
						  }
						  // chk  done
						  $chk_bank_name=$this->input->post('chk_bank_name');
						  $checkno=$this->input->post('checkno');
						  $chk_drawer_name=$this->input->post('chk_drawer_name');
						  
						  // fund  done
						  $card_no=$this->input->post('card_no');
						  $card_name=$this->input->post('card_name');
						  $card_expy=$this->input->post('card_expy');
						  $card_expm=$this->input->post('card_expm');
						  $card_czz=$this->input->post('card_cvv');
						  $card_exp_date=$card_expm.$card_expy;
						  $card_bank_name=$this->input->post('card_bank_name');
						  // draft  done
						  $draft_no=$this->input->post('draft_no');
						  $draft_bank_name=$this->input->post('draft_bank_name');
						  $draft_drawer_name=$this->input->post('draft_drawer_name');
						  
						  // fund  done
						  $ft_bank_name=$this->input->post('ft_bank_name');
						  $ft_account_no=$this->input->post('ft_account_no');
						  $ft_ifsc_code=$this->input->post('ft_ifsc_code');
						  
						  //wallet 
						    $wallet_name=$this->input->post('wallet_name');
						    $wallet_tr_id=$this->input->post('wallet_tr_id');
						    $wallet_rec_acc=$this->input->post('wallet_rec_acc');
						 
						  $ft_approved_by=$this->input->post('ft_approved_by');
						  $ft_recievers_name=$this->input->post('ft_recievers_name');
						  $ft_paid_by=$this->input->post('ft_paid_by');
						  $ft_recievers_desig=$this->input->post('ft_recievers_desig');
						  $profit_center1=$this->input->post('p_center');
						  $p_status=$this->input->post('p_status');
						  
						  if(isset($card_bank_name)){
							   $t_bank_name=$card_bank_name;
						  }else if(isset($draft_bank_name)){
							  $t_bank_name=$draft_bank_name;
						  }else if(isset($ft_bank_name)){
							  $t_bank_name=$ft_bank_name;
						  }else if(isset($chk_bank_name)){
							  $t_bank_name=$chk_bank_name;
						  }
							  
						 
						  $card_type=$this->input->post('card_type');
						  $t_type=$this->unit_class_model->get_transaction_type(16);
							$from=$t_type->hotel_transaction_type_from_entity_id;
							$to=$t_type->hotel_transaction_type_to_entity_id;
						 $laundry_payment=array(
						 'transaction_type' => "Laundry Payment",
						't_date'=>date("Y-m-d H:i:s"),
						'details_type' => "Laundry Payment",
						't_card_type'=>$card_type,
						't_amount'=>$pay_amount,
						't_payment_mode'=>$mop,
						
						'checkno'=>$checkno,
						't_bank_name'=>$t_bank_name,
						't_drw_name'=>$chk_drawer_name,
						
						't_card_no'=>$card_no,
						't_card_name'=>$card_name,
						't_card_exp_date'=>$card_exp_date,
						
						't_status'=>$p_status,
						//'t_card_cvv'=>$card_cvv,
						
						'draft_no'=>$draft_no,
						'draft_bank_name'=>$draft_bank_name,
						
						'ft_bank_name'=>$ft_bank_name,						
						'ft_account_no'=>$ft_account_no,
						'ft_ifsc_code'=>$ft_ifsc_code,
							 
						't_w_name'=>$wallet_name,
						't_tran_id'=>$wallet_tr_id,
						't_recv_acc'=>$wallet_rec_acc,
						
						'details_type'=>'Laundry',
						'transaction_releted_t_id'=>16,
						
						'p_center'=>$profit_center1,
						't_booking_id'=>$guest_booking_id,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id'),
						//'transaction_releted_t_id'=>"4",
						'transaction_from_id' => $from,
						'transaction_to_id' => $to,
						't_cashDrawer_id' => $this->session->userdata('cd_key'),
					//	't_hotel_id' => $this->session->userdata('user_hotel'),
						'transactions_detail_notes'=>$Guest_name.$received_date.'Transaction from:'.$from.'Transaction to:'.$to
						);
						 
						 //cloth_type[],fabric[],size[],condition[],service[],qty[],color[],brand[],price[],note[]
	
						 //ARRAY TABLE					
						$cloth_type=$this->input->post('h_cloth_type'); 
						$fabric=$this->input->post('h_fabric'); 
						$size=$this->input->post('size');
						$condition=$this->input->post('condition');
						
						$service=$this->input->post('h_service');
						$qty=$this->input->post('qty');
						$color=$this->input->post('h_color');
						$brand=$this->input->post('brand');
						
						$price=$this->input->post('price');
						$note=$this->input->post('note');
						
						$tax=$this->input->post('tax');
					     //print_r($c_default_apply);exit;
						
						$corporate_discount=array(
						'cloth_type_id'=>$cloth_type,
						'fabric_type_id'=>$fabric,
						'item_size'=>$size,
						'item_condition'=>$condition,						
						'laundry_service_type_id'=>$service,
						'laundry_item_qty'=>$qty,
						'laundry_item_color'=>$color,
						'laundry_item_brand'=>$brand,
						
						'item_price'=>$price,
						'item_note'=>$note,
						//'item_tax'=>$tax,
						
						);
						
						//print_r($corporate_discount);exit;
					/*foreach( $corporate_discount['fabric_type_id'] as $index => $code ) {
								//echo  $corporate_discount['contract_end_date'][$index] ;
								 echo $corporate_discount['default_apply'][$code];
							}*/
							
							if($booking_type=='1'){
								$booking=$single_booking;
								$b_type='sb';
							}else if($booking_type=='2'){
								$booking=$group_booking;
								$b_type='gb';
							}
								else{
									$booking='0';
									$b_type='n';
								}
							
                         $service=array(
                                'tax'=>$hidTax,
                                'response'=>$response,
                                'laundry_booking_type'=>$b_type,
                                'laundry_booking_id'=>$booking,
								
                                'billing_preference'=>$billing_pref,								
                                'guest_id'=>$guest_id,
                                'laundry_guest_name'=>$Guest_name,
                                'laundry_guest_type'=>$guest_type,
                                'receiv_date'=>$received_date,
                                'delivery_date'=>$delivery_date,
								
                                'laundry_special_request'=>$special_request,
								'job_status'=>$p_status,
                                'vat'=>$vat,
                                'service_tax'=>$service_tax,
                                'service_charge'=>$service_charge,
                                'discount'=>$discount,
                                'grand_total'=>$grand_total,								
                                'total_amount'=>$total,	
                                //'net_amount'=>$grand_total,	
								'payment_status'=>$stat,								
								'payment_due'=>$payments_due,								
								'booking_id'=>$guest_booking_id,								
								'total_item'=>$quantity,								
								'laundry_hotel_id'=>$this->session->userdata('user_hotel'),
								'laundry_user_id'=>$this->session->userdata('user_id'),
								'hotel_id'=>$this->session->userdata('user_hotel'),
								'user_id'=>$this->session->userdata('user_id')
                                
                            );
							
							 $extraService=array(
							 'sm_booking_type'=>$b_type,
							 'booking_id'=>$booking_id,
							 'service_id'=>0,
							 'qty'=>$quantity,
							 's_price'=>0,
							 'tax'=>0,
							 'total'=>$grand_total,
							 'hotel_id'=>$this->session->userdata('user_hotel'),
							 'user_id'=>$this->session->userdata('user_id')
							 
							 );
							//$corporate_discount='0';
						//	echo "<pre>";
							//print_r($service);exit;
							//print_r($corporate_discount);
							//print_r($laundry_payment);exit;
							if($pay_amount){
								$query = $this->unit_class_model->add_laundry_service($service,$corporate_discount,$laundry_payment);
							}else{
								$query = $this->unit_class_model->add_laundry_service($service,$corporate_discount,0);
								$query = $this->unit_class_model->add_laundry_extraservice($extraService);
								
							}
							
                        
							
						
						
						
                        if (isset($query) && $query) 
                        {
							
                            $this->session->set_flashdata('succ_msg', " Added Successfully!");
                            redirect('unit_class_controller/add_laundry_service');
                        } 
                        else 
                        {
                            $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                            redirect('unit_class_controller/add_laundry_service');
                        }  


        }

}else{
	redirect('dashboard');
}
     
     
 }
 
	function add_laundry_rates(){
     
     
			 $found = in_array("7",$this->arraay);
		if($found)
		{

            $data['heading'] = 'Add Laundry Rates';
            $data['sub_heading'] = '';
            $data['description'] = 'Add Laundry Rates';
            $data['active'] = 'all_laundry_service';
            $data['usage_log'] = $this->dashboard_model->all_usage_log();
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            $data['page'] = 'backend/dashboard/add_laundry_service';
            $this->load->view('backend/common/index', $data);

                 if($this->input->post())
                {   
                        /* Images Upload*/ 
						$this->upload->initialize($this->set_upload_options());
                        if ($this->upload->do_upload('vendor_image')) {

                            $asset_image = $this->upload->data('file_name');
                            $filename = $asset_image;
                            $source_path = './upload/' . $filename;
                            $target_path = './upload/corporate/';                            
                            $config_manip = array(
                            'image_library' => 'gd2',
                            'source_image' => $source_path,
                            'new_image' => $target_path,
                            'maintain_ratio' => TRUE,
                            'create_thumb' => TRUE,
                            'thumb_marker' => '_thumb',
                            'width' => 250,
                            'height' => 250
                            );
                            $this->load->library('image_lib', $config_manip);
                            
                            if (!$this->image_lib->resize()) {
                                    $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                                    redirect('dashboard/add_corporate');
                            }
                            $thumb_name = explode(".", $filename);
                            $asset_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                            $this->image_lib->clear();
                        }
                        else 
                        {
                          // $this->session->set_flashdata('err_msg', $this->upload->display_errors());
                            //redirect('dashboard/add_asset');
							$source_path = './upload/';
                            $target_path = './upload/corporate/';  
							$asset_thumb ='no_images.png';
                        }
                        date_default_timezone_set("Asia/Kolkata");
                         $company_name=$this->input->post('company_name');
						 //echo $company_name; exit;
						 $reg_type=$this->input->post('reg_type');
						 $address=$this->input->post('address');
						 $pin_code=$this->input->post('pin_code');
						 $city=$this->input->post('city');
						 $state=$this->input->post('state');
						 $country=$this->input->post('country');
						 $contact_no=$this->input->post('contact_no');
						 $contatc_person=$this->input->post('contatc_person');
						 $manager_name=$this->input->post('manager_name');
						 $director_no=$this->input->post('director_no');
						 $director_name=$this->input->post('director_name');
						// $industry=$this->input->post('industry');
						// $no_branch=$this->input->post('no_branch');
						// $employees_no=$this->input->post('employees_no');
					//	 $booking_no=$this->input->post('booking_no');
						// $total_transaction=$this->input->post('total_transaction');
						 $amount_pending=$this->input->post('amount_pending');     
						 
						 $industry_type=$this->input->post('industry_type');                         
						 $field=$this->input->post('field');                         
						 $rating=$this->input->post('rating');                         
                         
						 //enter data to hotel_corporate_discount_details table						
						$c_start_date=$this->input->post('contract_start_date'); 
						$c_end_date=$this->input->post('contract_end_date'); 
						$c_discount_start=$this->input->post('contract_discount_start');
						$c_default_apply=$this->input->post('default_rule');
							//print_r($c_default_apply);exit;
						//$c_discount_end=$this->input->post('contract_discount_end'); 
						
						$corporate_discount=array(
						'contract_start_date'=>$c_start_date,
						'contract_end_date'=>$c_end_date,
						'contract_discount_start'=>$c_discount_start,
						'default_apply'=>$c_default_apply,
						);
					/*foreach( $corporate_discount['contract_start_date'] as $index => $code ) {
								//echo  $corporate_discount['contract_end_date'][$index] ;
								 echo $corporate_discount['default_apply'][$code];
							}
							exit;*/
                         $corporate=array(
                                'name'=>$company_name,
                                'reg_type'=>$reg_type,
                                'address'=>$address,
                                'pin_code'=>$pin_code,
                                'city'=>$city,
                                'state'=>$state,
                                'country'=>$country,
                                'g_contact_no'=>$contact_no, 
                                'contact_person'=>$contatc_person,
								'manager_name'=>$manager_name,
                                'director_contact_no'=>$director_no,
                                'director_name'=>$director_name,
                                //'industry'=>$industry,
								//'number_branch'=>$no_branch,
								//'no_of_emp'=>$employees_no,
                                //'no_of_booking'=>$booking_no,
                               // 'total_transaction'=>$total_transaction,
                                'total_amt_pending'=>$amount_pending,
								
                                'industry_type'=>$industry_type,
                                'field'=>$field,
                                'rating'=>$rating
                                
                            );
							//print_r($corporate_discount);exit;
                   
                        $query = $this->dashboard_model->add_corporate($corporate,$corporate_discount);
						
						
						
                        if (isset($query) && $query) 
                        {
							
                            $this->session->set_flashdata('succ_msg', " Added Successfully!");
                            redirect('dashboard/all_corporate');
                        } 
                        else 
                        {
                            $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                            redirect('dashboard/add_corporate');
                        }  


        }

}else{
	redirect('dashboard');
}
     
     
 }
 function all_laundry_rates(){
		
			 $found = in_array("94",$this->arraay);
		if($found)
		{
			$data['heading'] = 'Service';
            $data['sub_heading'] = 'Laundry Services';
            $data['description'] = 'Add Laundry Rates here';
			$data['active'] = 'all_laundry_rates';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			$data['rates']=$this->unit_class_model->all_laundry_rates();
			$data['stat']="List of All Laundry Rates";
			$data['page'] = 'backend/dashboard/all_laundry_rates';
            $this->load->view('backend/common/index', $data);
	}else{
		redirect('dashboard');
	}
	}

function reset_laundry_rates(){
		$this->unit_class_model->reset_laundry_rates();
		
	}

function get_price_by_clothID_fabricId(){
		
		$cloth=$this->input->post('cloth');		
		$fab=$this->input->post('fab');	
		$service=$this->input->post('service');	
		
		$a=$this->unit_class_model->get_price_by_clothID_fabricId($cloth,$fab,$service);
		$data = array(
            'id' =>$a->lr_id,
            'laundry' =>$a->lr_laundry_rate,
            'dry' =>$a->lr_dry_cleaning_rate,
            'iron' =>$a->lr_ironing_rate,

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	
	}
	
	function get_laundrydata_by_id(){
		
		$cloth=$this->input->post('cloth_type');	
		$service=$this->input->post('service');	
		
		$color=$this->input->post('color');
		
		$fabric=$this->input->post('fabric');	
		
		$a=$this->unit_class_model->get_cloth_type_by_id($cloth);
		$b=$this->unit_class_model->get_fabric_type_by_id($fabric);
		$c=$this->unit_class_model->get_service_type_by_id($service);
		if($color!=0){	
		$d=$this->unit_class_model->get_color_by_id($color);
		$s=$d->color_name;
		}
		else{
			$s='none';
		}
		
		
		$data = array(
            'name' =>$a->laundry_ct_name,
            'fabric' =>$b->fabric_type,
            'service' =>$c->laundry_type_name,
            'color' =>$s,
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	
	}
	
	/*function get_laundrydata_by_id1(cloth_type,){
		
		$cloth=$this->input->post('cloth_type');	
		$service=$this->input->post('service');	
		
		$color=$this->input->post('color');
		
		$fabric=$this->input->post('fabric');	
		
		$a=$this->unit_class_model->get_cloth_type_by_id($cloth);
		$b=$this->unit_class_model->get_fabric_type_by_id($fabric);
		$c=$this->unit_class_model->get_service_type_by_id($service);
		if($color!=0){	
		$d=$this->unit_class_model->get_color_by_id($color);
		$s=$d->color_name;
		}
		else{
			$s='none';
		}
		
		
		$data = array(
            'name' =>$a->laundry_ct_name,
            'fabric' =>$b->fabric_type,
            'service' =>$c->laundry_type_name,
            'color' =>$s,
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	
	}*/

	function get_guest_by_booking_id(){
		
		$booking_id=$this->input->post('booking_id');	
		$booking_type=$this->input->post('booking_type');	
		if($booking_type==1){
		$a=$this->unit_class_model->get_guest_by_booking_id($booking_id);
		$b=$this->unit_class_model->get_guest_by_guest_id($a->guest_id);
		
		$data = array(
            'id' =>$a->guest_id,           
            'name' =>$b->g_name,           
            'type' =>$b->g_type,           
        );
		}
		else if($booking_type==2){
		$a=$this->unit_class_model->get_group_guest_by_booking_id($booking_id);
		$b=$this->unit_class_model->get_guest_by_guest_id($a->guestID);
		$data = array(
            'name' =>$b->g_name,           
            'type' =>$b->g_type,          
        );
		
		}
		
		
		
        header('Content-Type: application/json');
        echo json_encode($data);
		
	
	}
	
	function delete_laundry_service(){
		
		$id=$this->input->post('id');
		
		$this->unit_class_model->delete_laundry_service($id);
		$data = array(
            'data' =>"sucess",

        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	
	}
	
	function edit_laundry_service(){
	 if($this->session->userdata('update_corporate') != '1')
        {
			//if(isset($_GET['id']))
			//	$id=$_GET['id'];
			 //echo $id; exit;
            $data['heading']='corporate';
            $data['sub_heading']='Edit Corporate';
            $data['description']='Edit Corporate here';
            $data['active']='update';  
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
			// $data['laundry_service'] =$this->unit_class_model->laundry_service_by_id($id);
                if($this->input->post())
                {   
						//-------------------------------------------------------------------------
                        date_default_timezone_set("Asia/Kolkata");
                        
						 
						 $laundry_id=$this->input->post('h_g_laundry_id');
						 
						 $booking_type=$this->input->post('booking_type');
						  $group_booking=$this->input->post('group_booking1');
						   $single_booking=$this->input->post('single_booking');
						    $single_booking_hid=$this->input->post('booking_id_hid');
						    $single_booking_hid1=$this->input->post('name');
						 $billing_pref=$this->input->post('billing_pref');
						 
						 $guest_id=$this->input->post('hidden_id');
						 
						 
						 $Guest_name=$this->input->post('Guest_name');
						 $guest_type=$this->input->post('guest_type');
						 $received_date=date("Y-m-d H:i:s", strtotime($this->input->post('received_date')));
						 $delivery_date=date("Y-m-d H:i:s", strtotime($this->input->post('delivery_date')));
						 $special_request=$this->input->post('special_request');
						 
						 $vat=$this->input->post('vat');
						 $service_tax=$this->input->post('service_tax');
						 $service_charge=$this->input->post('service_charge');
						 $discount=$this->input->post('discount');
						 $grand_total=$this->input->post('grand_total');
						 $total=$this->input->post('hidden_grand_total');
						 //$total_amount_hidden=$this->input->post('total_amount_hidden');
						 
						$guest_booking_id=$this->input->post('guest_booking_id');
						$quantity=$this->input->post('quantity');
						$quantity1=$this->input->post('quantity1');
						$quantity=$quantity+$quantity1;
						
						// exit;
						 
						 
						 //$payments_due=$this->input->post('payments_due');
						 $payments_due=$this->input->post('due_amount');
						if($grand_total-$total == 0){
							$stat="fullpaid";
						}
						else if($grand_total==$payments_due){
							$stat="unpaid";
						}else {
							$stat="partial";
						}
						 
						 $pay_amount=$this->input->post('pay_amount');
						  $mop=$this->input->post('pay_mode');
						  if($mop==''){
							  $mop='';
						  }
						  // chk  done
						  $chk_bank_name=$this->input->post('chk_bank_name');
						  
						  
						  $checkno=$this->input->post('checkno');
						  $chk_drawer_name=$this->input->post('chk_drawer_name');
						  
						  // fund  done
						  $card_no=$this->input->post('card_no');
						  $card_name=$this->input->post('card_name');
						  $card_expy=$this->input->post('card_expy');
						  $card_expm=$this->input->post('card_expm');
						  $card_czz=$this->input->post('card_cvv');
						  if(isset($card_expy) && $card_expy){
						  $card_exp_date=$card_expy.'-'.$card_expm.'-00';
						  }else{
							  $card_exp_date='';
						  }
						  $card_bank_name=$this->input->post('card_bank_name');
						  
						 
						  // draft  done
						  $draft_no=$this->input->post('draft_no');
						  $draft_bank_name=$this->input->post('draft_bank_name');
						  $draft_drawer_name=$this->input->post('draft_drawer_name');
						  
						  // fund  done
						  $ft_bank_name=$this->input->post('ft_bank_name');
						  $ft_account_no=$this->input->post('ft_account_no');
						  $ft_ifsc_code=$this->input->post('ft_ifsc_code');
						  
						  //wallet 
						    $wallet_name=$this->input->post('wallet_name');
						    $wallet_tr_id=$this->input->post('wallet_tr_id');
						    $wallet_rec_acc=$this->input->post('wallet_rec_acc');
						 
						  $ft_approved_by=$this->input->post('ft_approved_by');
						  $ft_recievers_name=$this->input->post('ft_recievers_name');
						  $ft_paid_by=$this->input->post('ft_paid_by');
						  $ft_recievers_desig=$this->input->post('ft_recievers_desig');
						  $profit_center1=$this->input->post('p_center');
						  $p_status=$this->input->post('p_status');
						   
						  
						  if(isset($card_bank_name) && $card_bank_name){
							   $t_bank_name=$card_bank_name;
						  }else if(isset($draft_bank_name) && $draft_bank_name){
							  $t_bank_name=$draft_bank_name;
						  }else if(isset($ft_bank_name) && $ft_bank_name){
							  $t_bank_name=$ft_bank_name;
						  }else if(isset($chk_bank_name) && $chk_bank_name){
							  $t_bank_name=$chk_bank_name;
						  }
							  
						 
						  $card_type=$this->input->post('card_type');
						  $t_type=$this->unit_class_model->get_transaction_type(16);
							$from=$t_type->hotel_transaction_type_from_entity_id;
							$to=$t_type->hotel_transaction_type_to_entity_id;
						 $laundry_payment=array(
						 'transaction_type' => "Laundry Payment",
						't_date'=>date("Y-m-d H:i:s"),
						'details_type' => "Laundry Payment",
						't_card_type'=>$card_type,
						't_amount'=>$pay_amount,
						't_payment_mode'=>$mop,
						
						'checkno'=>$checkno,
						't_bank_name'=>$t_bank_name,
						't_drw_name'=>$chk_drawer_name,
						
						't_card_no'=>$card_no,
						't_card_name'=>$card_name,
						't_card_exp_date'=>$card_exp_date,
						
						't_status'=>$p_status,
						//'t_card_cvv'=>$card_cvv,
						
						'draft_no'=>$draft_no,
						//'draft_bank_name'=>$draft_bank_name,
						
						'ft_bank_name'=>$ft_bank_name,						
						'ft_account_no'=>$ft_account_no,
						'ft_ifsc_code'=>$ft_ifsc_code,
							 
						't_w_name'=>$wallet_name,
						't_tran_id'=>$wallet_tr_id,
						't_recv_acc'=>$wallet_rec_acc,
						
						'details_type'=>'Laundry',
						'transaction_releted_t_id'=>16,
						
						'p_center'=>$profit_center1,
						't_booking_id'=>$guest_booking_id,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id'),
						//'transaction_releted_t_id'=>"4",
						'transaction_from_id' => $from,
						'transaction_to_id' => $to,
						't_cashDrawer_id' => $this->session->userdata('cd_key'),
					//	't_hotel_id' => $this->session->userdata('user_hotel'),
						'transactions_detail_notes'=>$Guest_name.$received_date.'Transaction from:'.$from.'Transaction to:'.$to
						);
						// echo "<pre>";
						// print_r($laundry_payment);exit;

						 //PAYMENT SECTION START
						 /* $pay_amount=$this->input->post('pay_amount');
						  $mop=$this->input->post('mop');
						  if($mop==''){
							  $mop='';
						  }
						  
						  $checkno=$this->input->post('checkno');
						  $check_bank_name=$this->input->post('check_bank_name');
						  
						  $card_no=$this->input->post('card_no');
						  $card_name=$this->input->post('card_name');
						 // $card_exp_date=$this->input->post('card_exp_date');
						  $card_exp_date=$this->input->post('card_expm').'-'.$this->input->post('card_expy');
						  $card_bank_name=$this->input->post('card_bank_name');
						  
						  $draft_no=$this->input->post('draft_no');
						  $draft_bank_name=$this->input->post('draft_bank_name');
						  
						  $ft_bank_name=$this->input->post('ft_bank_name');
						  $ft_account_no=$this->input->post('ft_account_no');
						  $ft_ifsc_code=$this->input->post('ft_ifsc_code');
						  $ft_approved_by=$this->input->post('ft_approved_by');
						  $ft_recievers_name=$this->input->post('ft_recievers_name');
						  $ft_paid_by=$this->input->post('ft_paid_by');
						  $ft_recievers_desig=$this->input->post('ft_recievers_desig');
						  $profit_center1=$this->input->post('profit_center1');
						  
						 $laundry_payment=array(
						't_amount'=>$pay_amount,
						't_payment_mode'=>$mop,
						'checkno'=>$checkno,
						't_bank_name'=>$check_bank_name,
						
						't_card_no'=>$card_no,
						't_card_name'=>$card_name,
						't_card_exp_date'=>$card_exp_date,
						't_bank_name'=>$card_bank_name,
						//'t_card_cvv'=>$card_cvv,
						
						'draft_no'=>$draft_no,
						'draft_bank_name'=>$draft_bank_name,
						
						'ft_bank_name'=>$ft_bank_name,						
						'ft_account_no'=>$ft_account_no,
						'ft_ifsc_code'=>$ft_ifsc_code,
						
						//''=>$ft_approved_by,
						//''=>$ft_recievers_name,
						//''=>$ft_paid_by,
						//''=>$ft_recievers_desig,
						'p_center'=>$profit_center1,
						'hotel_id'=>$this->session->userdata('user_hotel'),
						'user_id'=>$this->session->userdata('user_id')
						);*/
						 
						 //cloth_type[],fabric[],size[],condition[],service[],qty[],color[],brand[],price[],note[],laundry_id[]
	
						 //ARRAY TABLE					
						$line_item_laundry_id=$this->input->post('laundry_id'); 
						$cloth_type=$this->input->post('h_cloth_type'); //h_service,h_fabric,h_cloth_type,h_color
						$fabric=$this->input->post('h_fabric'); 
						$size=$this->input->post('size');
						$condition=$this->input->post('condition');
						
						$service=$this->input->post('h_service');
						$qty=$this->input->post('qty');
						$color=$this->input->post('h_color');
						$brand=$this->input->post('brand');
						
						$price=$this->input->post('price');
						$note=$this->input->post('note');
						
						$tax=$this->input->post('tax');
					     //print_r($c_default_apply);exit;
						
						$corporate_discount=array(
						'laundry_id'=>$line_item_laundry_id,
						'cloth_type_id'=>$cloth_type,
						'fabric_type_id'=>$fabric,
						'item_size'=>$size,
						'item_condition'=>$condition,						
						'laundry_service_type_id'=>$service,
						'laundry_item_qty'=>$qty,
						'laundry_item_color'=>$color,
						'laundry_item_brand'=>$brand,
						
						'item_price'=>$price,
						'item_note'=>$note,
						'laundry_master_id'=>$laundry_id,
						//'item_tax'=>$tax,
						
						);
						//echo "<pre>";
					//print_r($corporate_discount);exit;
					/*foreach( $corporate_discount['fabric_type_id'] as $index => $code ) {
								//echo  $corporate_discount['contract_end_date'][$index] ;
								 echo $corporate_discount['default_apply'][$code];
							}*/
							
							if($booking_type=="1"){
								$booking=$single_booking_hid1;
								$typ='sb';
							}else if($booking_type=="2"){
								$booking=$single_booking_hid1;
								$typ='gb';
							}
								else{
									$booking='';
									$typ='n';
								}
						//	echo $booking;
						
						$minfo=$this->unit_class_model->get_info_laundryMaster($laundry_id);
						
						
						
                         $service=array(
                                'laundry_booking_type'=>$typ,
                                'laundry_booking_id'=>$booking,
                                'booking_id'=>$booking,
								'billing_preference'=>$billing_pref,								
                               'guest_id'=>$guest_id,
                                'laundry_guest_name'=>$Guest_name,
                                'laundry_guest_type'=>$guest_type,
                                'receiv_date'=>$received_date,
                                'delivery_date'=>$delivery_date,								
                                'laundry_special_request'=>$special_request,								
                                'vat'=>$vat,
                                'service_tax'=>$service_tax,
                                'service_charge'=>$service_charge,
                                'discount'=>$discount,
                                'grand_total'=>$grand_total,								
                                'total_amount'=>$pay_amount+$minfo->total_amount,	
								'payment_status'=>$stat,								
								'payment_due'=>($grand_total-($pay_amount+$minfo->total_amount)),								
								//'booking_id'=>$guest_booking_id,								
								'total_item'=>$quantity,								
								'laundry_hotel_id'=>$this->session->userdata('user_hotel'),
								'laundry_user_id'=>$this->session->userdata('user_id')
                                
                            );
							//echo $laundry_id;
						//echo "<pre>";
					//print_r($service);exit;
						//print_r($laundry_payment);exit;
							
							//$corporate_discount='0';
							
                        $query = $this->unit_class_model->update_laundry_service($service,$corporate_discount,$laundry_payment,$laundry_id,$pay_amount);
						
						
						
                       //-------------------------------------------------------------------------
						
						if (isset($query) && $query) 
                        {
                            $this->session->set_flashdata('succ_msg', "Updated Successfully!");
                            redirect('dashboard/all_laundry_service');
                        } 
                        else 
                        {
                            $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                            redirect('dashboard/all_laundry_service');
                        }
                        
                }
                else
                {
							
						$id=$_GET['id'];
						
					 //echo $id; exit;
					$data['heading'] = 'Service';
            $data['sub_heading'] = 'Laundry Services';
            $data['description'] = 'Add Laundry Rates here';
					$data['active']='update';  
					$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
					$data['laundry_service'] =$this->unit_class_model->laundry_service_by_id($id);
					$data['l_items'] =$this->unit_class_model->laundry_lineitem($id);
					//echo "<pre>";
					//print_r($data['laundry_service']);exit;
					$data['page'] = 'backend/dashboard/edit_laundry_service';
                    $this->load->view('backend/common/index', $data);
                }
            
        }
        else
        {
            redirect('dashboard');
        }
	 
 }
 
 /*function get_laundry_pdf(){
$id=$_GET['id'];

  $data['laundry_service'] =$this->unit_class_model->laundry_service_by_id($id);
 // print_r($data['laundry_service']);exit;
		$data['hotelname'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
          $data['hotel_contact']  = $this->bookings_model->get_hotel_contact_details($this->session->userdata('user_hotel'));
		 //print_r($data['hotelname']);exit;
		 
        $this->load->view("backend/dashboard/laundry_invoice_pdf",$data);
  $html = $this->output->get_output();
  $this->load->library('dompdf_gen');
  $this->dompdf->load_html($html);
  $this->dompdf->render();
  $this->dompdf->stream("laundry_report.pdf");
 }*/
 
 function get_laundry_pdf(){
        $l_id= $_GET['id'];
		$hotel_id = $this->session->userdata('user_hotel');
	//	 $data['hotel_name'] = $this->dashboard_model->get_hotel($data['booking_details'][0]->hotel_id);
       // $data['hotel_contact']  = $this->bookings_model->get_hotel_contact_details($this->session->userdata('user_hotel'));
        $data['laundry_details']  = $this->unit_class_model->laundry_service_by_id($l_id);
		$booking_id=$data['laundry_details']->booking_id;
		if(isset($booking_id) && $booking_id){
		  $data['booking_details']  = $this->bookings_model->get_booking_details($booking_id);
        foreach ($data['booking_details'] as $key) {
            $data['group_id'] = $key->group_id;
            $bk_date = $key->booking_taking_date;
         
        }
		
        
        $data['hotel_name'] = $this->dashboard_model->get_hotel($data['booking_details'][0]->hotel_id);
		$data['hotel_contact']  = $this->bookings_model->get_hotel_contact_details($data['booking_details'][0]->hotel_id);
		}
		$data['ad_hoc']  = $this->unit_class_model->get_adhoc_details($l_id);
        $data['hotel_contact']  = $this->bookings_model->get_hotel_contact_details($hotel_id);
        $data['hotel_name'] = $this->dashboard_model->get_hotel($hotel_id);
        $data['tax']=$this->bookings_model->get_tax_details($this->session->userdata('user_hotel'));
		$this->load->view("backend/dashboard/laundry_invoice_pdf",$data);
         $html = $this->output->get_output();
          $this->load->library('dompdf_gen');
          $this->dompdf->load_html($html);
          $this->dompdf->render();
          $this->dompdf->stream("laundry.pdf");
    }
	
	function get_laundry_line_item_by_id(){
		
		$id=$this->input->post('id');	
		$laundry_line_item=$this->unit_class_model->get_line_itm($id);
		foreach($laundry_line_item as $ab){
			$laundry_id[]=$ab->laundry_id;
			
			$cloth=$this->unit_class_model->get_cloth_type_by_id($ab->cloth_type_id);
			$fabric=$this->unit_class_model->get_fabric_type_by_id($ab->fabric_type_id);
			$service=$this->unit_class_model->get_service_type_by_id($ab->laundry_service_type_id);
			$color=$this->unit_class_model->get_color_by_id($ab->laundry_item_color);
			 if(isset($color) && $color){
				 $c=$color->color_name;
			 }
			 else{
				  $c='';
			 }
			 
			 if(isset($cloth) && $cloth){
				 $cloth_name=$cloth->laundry_ct_name;
			 }
			 else{
				  $cloth_name='';
			 }
			 
			  if(isset($fabric) && $fabric){
				 $fab=$fabric->fabric_type;
			 }
			 else{
				  $fab='';
			 }
			 
			if(isset($service) && $service){
				 $serv=$service->laundry_type_name;
			 }
			 else{
				   $serv='';
			 }
			 
			 if(isset($ab->laundry_item_qty) && $ab->laundry_item_qty){
				 $qty=$ab->laundry_item_qty;
			 }else{
				 $qty='';
			 }
			 
			 if($ab->laundry_item_brand!=''){
				 $brand=$ab->laundry_item_brand;
			 }else{
				 $brand='';
			 }
			 
			 
			 if($ab->item_price!=''){
				 $price=$ab->item_price;
			 }else{
				 $price=0;
			 }
			 
			 if($ab->item_note!=''){
				 $note=$ab->item_note;
			 }else{
				 $note='';
			 }
			 
			$cloth_type_id[]=$cloth_name;
			$cloth_type_id1[]=$ab->cloth_type_id;//cloth id
			$laundry_item_qty[]=$qty;
			$laundry_item_brand[]=$brand;
			$item_note[]=$note;
			$laundry_item_color1[]=$ab->laundry_item_color;//color id
			$laundry_item_color[]=$c;
			$fabric_type_id1[]=$ab->fabric_type_id;// fabric id
			$fabric_type_id[]=$fab;
			$item_condition[]=$ab->item_condition;
			$item_size[]=$ab->item_size;
			$item_price[]=$price;
			$laundry_service_type_id[]=$serv;
			$laundry_service_type_id1[]=$ab->laundry_service_type_id;// service id
			
		}
		$data = array(      		
            'laundry_id' =>$laundry_id,
            'cloth_type_id' =>$cloth_type_id,
            'cloth_type_id1' =>$cloth_type_id1,//id
            'laundry_item_qty' =>$laundry_item_qty,
            'laundry_item_brand' =>$laundry_item_brand,
            'item_note' =>$item_note,
            'laundry_item_color' =>$laundry_item_color,
            'laundry_item_color1' =>$laundry_item_color1,//id
            'fabric_type_id' =>$fabric_type_id,
            'fabric_type_id1' =>$fabric_type_id1,//id
            'item_condition' =>$item_condition,
            'item_size' =>$item_size,
            'item_price' =>$item_price,
            'laundry_service_type_id' =>$laundry_service_type_id,
            'laundry_service_type_id1' =>$laundry_service_type_id1,//id
        );
		
		
		//print_r($data);exit;
		
		
        header('Content-Type: application/json');
        echo json_encode($data);
		
	
	}


	function add_discount_mapping(){
		
		$discount_for=$this->input->post('discount_for');		
		$amount=$this->input->post('amount');		
		$discount_rule=$this->input->post('discount_rule');		
		$note=$this->input->post('note');		
		$booking_id=$this->input->post('booking_id');
		$plan_id=$this->input->post('plan_id');
		$totalAmount=$this->input->post('totalAmount');
		$totalDuee=$this->input->post('totalDuee');		
		$last = $this->unit_class_model->getLastDiscount();
		$last = $last->last;
		$last = ('disc/0'.$this->session->userdata('user_hotel').'/'.$last);
		$last++;
		$b=substr($amount,-1);
		
		//$amo=($totalAmount) - ($amount);
		if($b=='%'){
			$a=rtrim($amount, '%');
			//$a=(float)$a;			
			//$amo=((float)$a * (float)$totalAmount)/100;
			$amo=((float)$a * (float)$totalAmount)/100;
			$parsen=$amount;
		}else{
			$amo=$amount;
			$parsen=((100 *($amount))/$totalAmount);
		}

			$data=array(
				'id_alias'=>$last,
				'discount_for'=>$discount_for,
				'discount_amount'=>$amo,
				'discount_plan_id'=>$discount_rule,
				'note'=>$note,
				'booking_id'=>$booking_id,
				'related_id' => $booking_id,
				'related_type' => 'single_booking',
				'related_table' => 'hotel_bookings',
				'discount_plan_id'=>$plan_id,
				'discount_percentage'=>$parsen,
				'amt_due'=>$totalDuee,
				'ip_address'=>$_SERVER['REMOTE_ADDR'],
				'hotel_id'=>$this->session->userdata('user_hotel'),
				'user_id'=>$this->session->userdata('user_id')

			);
		$query=$this->unit_class_model->add_discount_mapping($data);
		if($query){
		$data = array(
            'data' =>"sucess",

        );
	}
        header('Content-Type: application/json');
        echo json_encode($data);
		
	
	}
	
	
	function add_groupBooing_discount_mapping(){
		
		$discount_for=$this->input->post('discount_for');		
		$amount=$this->input->post('amount');		
		//$discount_rule=$this->input->post('discount_rule');		
		$discount_rule=$this->input->post('discount_rule');	
//echo $discount_rule;exit;		
		$note=$this->input->post('note');		
		$group_booking_id=$this->input->post('group_booking_id');
		//$plan_id=$this->input->post('plan_id');
		$totalAmount=$this->input->post('totalAmount');
		$totalDuee=$this->input->post('totalDuee');		
		$last = $this->unit_class_model->getLastDiscount();
		$last = $last->last;
		$last = ('disc/0'.$this->session->userdata('user_hotel').'/'.$last);
		$last++;
		$b=substr($amount,-1);
		
		$amo=($totalAmount) - ($amount);
		if($b=='%'){
			$amo = ($amount * $totalAmount)/100;
			$parsen=$amount;
		}else{
			$amo=$amount;
			$parsen=((100 *($amount))/$totalAmount);
		}
		
			$data=array(
				'id_alias'=>$last,
				'discount_for'=>$discount_for,
				'discount_amount'=>$amo,
				'discount_plan_id'=>$discount_rule,
				'note'=>$note,
				'related_id' => $group_booking_id,
				'related_type' => 'group_booking',
				'related_table' => 'hotel_group',
				'group_id'=>$group_booking_id,
				
				'discount_percentage'=>$parsen,
				'amt_due'=>$totalDuee,
				'ip_address'=>$_SERVER['REMOTE_ADDR'],
				'hotel_id'=>$this->session->userdata('user_hotel'),
				'user_id'=>$this->session->userdata('user_id')

			);
		$query=$this->unit_class_model->add_groupBooing_discount_mapping($data);
		if($query){
		$data = array(
            'data' =>"sucess",

        );
	}
	$a="true";
        header('Content-Type: application/json');
        echo json_encode($a);
		
	
	}
	
	function delete_discount_mapping(){
		
		$mapping_id=$_POST['mapping_id'];
		
		$query=$this->unit_class_model->delete_discount_mapping($mapping_id);
		if($query){
		$data = array(
            'data' =>"Success",

        );
		}else{
			$data = array(
            'data' =>"Fail",

        );
		}
        header('Content-Type: application/json');
        echo json_encode($data);
		
	
	}
	
	function profile_pic_change($ab){
		 $dir = "./upload/admin/";
		 
move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $_FILES["image"]["name"]);
$data=$_FILES["image"]["name"];

$newData=array(
//'admin_id'=>$ab,
'admin_image'=>$data
);
$query=$this->unit_class_model->profile_pic_change($newData,$ab);

header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	function guest_doc_change($ab){
		 $dir = "./upload/guest/";
		 $flg=$_POST['flg'];
move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $_FILES["image"]["name"]);
$data=$_FILES["image"]["name"];

$newData=array(
//'admin_id'=>$ab,
'g_id_proof_thumb'=>$data
);
$query=$this->unit_class_model->guest_doc_change($newData,$ab);

header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	function guest_doc_change1(){
		
			$upload_dir="./upload/guest/thumbnail/id_proof/";
		 $dir1= "./upload/guest/originals/id_proof/";
$a1=$_FILES["image"]["tmp_name"];
$a2=$_FILES["image"]["name"];
@copy($_FILES["image"]["tmp_name"],'./upload/guest/thumbnail/id_proof/'.$_FILES["image"]["name"]);
 //create_square_image("./upload/guest/thumbnail/id_proof/".$_FILES["image"]["name"], $_FILES["image"]["name"], 250);
		 move_uploaded_file($_FILES["image"]["tmp_name"], $dir1.$_FILES["image"]["name"]);
$data=$_FILES["image"]["name"];
		 

$id_proof_type=$_POST["id_proof_type"];
$other_id=$_POST["other_id"];
if($id_proof_type=='Others'){
	$typ=$other_id;
}else{
	$typ=$id_proof_type;
}
$id_ref_number=$_POST["id_ref_number"];
$flg=$_POST["flg"];
$newData=array(
//'admin_id'=>$ab,
'g_id_number'=>$id_ref_number,
'g_id_type'=>$typ,
'g_id_proof_thumb'=>$data,
'g_id_proof'=>$data,

);
//echo $id_proof_type;
//print_r($newData); 



$query=$this->unit_class_model->guest_doc_change($newData,$flg);

		

$data=$_POST["flg"];
header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	
	
	function guest_doc_change2(){
		$flg=0;
			$upload_dir="./upload/guest/thumbnail/id_proof/";
		 $dir1= "./upload/guest/originals/id_proof/";
		 if(isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]){
$a1=$_FILES["image"]["tmp_name"];
$a2=$_FILES["image"]["name"];
@copy($_FILES["image"]["tmp_name"],'./upload/guest/thumbnail/id_proof/'.$_FILES["image"]["name"]);
 //create_square_image("./upload/guest/thumbnail/id_proof/".$_FILES["image"]["name"], $_FILES["image"]["name"], 250);
		 move_uploaded_file($_FILES["image"]["tmp_name"], $dir1.$_FILES["image"]["name"]);		 
$data=$_FILES["image"]["name"];
$flg=1;
		 }else{
			 $data='';
		 }
if(isset($_FILES["image1"]["name"]) && $_FILES["image1"]["name"]){
$dir11= "./upload/guest/originals/photo/";
$a11=$_FILES["image1"]["tmp_name"];
$a21=$_FILES["image1"]["name"];
@copy($_FILES["image1"]["tmp_name"],'./upload/guest/thumbnail/photo/'.$_FILES["image1"]["name"]);
 //create_square_image("./upload/guest/thumbnail/id_proof/".$_FILES["image"]["name"], $_FILES["image"]["name"], 250);
		 move_uploaded_file($_FILES["image1"]["tmp_name"], $dir11.$_FILES["image1"]["name"]);		 
$data1=$_FILES["image1"]["name"];
$flg=1;
}else{
	$data1='';
	
}	 


$flg=$_POST["flg"];
$newData=array(

'g_photo'=>$data1,
'g_id_proof_thumb'=>$data,
'g_id_proof'=>$data,

);
//echo $id_proof_type;
//print_r($newData); 



$query=$this->unit_class_model->guest_doc_change($newData,$flg);

		if($query){$dt=1;}else{$dt=0;}

$data=$_POST["flg"];
header('Content-Type: application/json');
        echo json_encode($dt);
		
	}
	
	
	
	function update_admin_profile(){
	
		  $id=$this->input->post('id');
		  $first_name=$this->input->post('first_name');
		  $last_name=$this->input->post('last_name');															
		  $mobile=$this->input->post('mobile');															
		  $occupation=$this->input->post('occupation');															
		  $about=$this->input->post('about');															
		  $website=$this->input->post('website');															
		  $account_class=$this->input->post('account_class');															
		  $country=$this->input->post('country');															
		  $dob=$this->input->post('dob');															
		  
		
		$data = array(
            'admin_first_name' =>$first_name,
			'admin_last_name'=>$last_name,
			'admin_phone1'=>$mobile,
			'dept'=>$occupation,
			'class'=>$account_class,
			'about_me'=>$about,
			'admin_country'=>$country,
			'admin_dob'=>$dob,
			);
		
		
		$update_status=$this->unit_class_model->update_admin_profile($data,$id);//
		if($update_status){
			$data=1;
		}else{
			$data=0;
		}
		//s$data1=$occupation;
		header('Content-Type: application/json');
        echo json_encode($data);
		}
		
		
		function showtaxplan1(){
			
		 /*$price = 1500;
		 $args = 'a';
		 $type = 'Booking';
		 $date = '2016-12-01';*/
		 // date_default_timezone_set('Asia/Kolkata');
		 
		 $price =$this->input->post('price');		
		 $args =$this->input->post('args');
		 $type =$this->input->post('type');	
		 $date1 =$this->input->post('dates');
		 
		 $date2=date("Y-m-d",strtotime($date1));
		 ///echo "price".$price.$date2.$type;

		 $times = $this->dashboard_model->getTaxElements($price,$date2,$type,$args);		// Fetching appropiate tax plan from tax plan view based on arguments
$tax = array();
	  if($times!=0){
		 
		$i = 0;
		$tot = 0;
		$tax['planName'] = $times[0]->r_name; 		// Assumed, plan name will be same for all returned rows
		if($args == 'a'){
			
			
			foreach($times as $res){
				if($times[$i]->calculated_on == 'n_r'){
					$taxAmt = ($times[$i]->tax_mode == 'p' ? ($times[$i]->tax_value * $price / 100) : ($times[$i]->tax_value));
					$tax[$times[$i]->tax_name] = $taxAmt;			
				}
				else if($times[$i]->calculated_on == 'n_p' && ($i > 0)){
					$taxAmt = ($times[$i]->tax_mode == 'p' ? ($times[$i]->tax_value * ($price + $tot) / 100) : ($times[$i]->tax_value));
					$tax[$times[$i]->tax_name] = $taxAmt;
				}
				
				$tot = $tot + $taxAmt;	
				$i++;
			} // End foreach
		}
		else
			$tax = $times;
	 }else{
$tot=0.00;
}
	 
		$tax['total']=$tot;//add total tax in array
	 
		
		//echo $tax['total'];
		//print_r($tax);
		header('Content-Type: application/json');
            echo json_encode($tax);
    }
	
	
	
	
	function check_discount_name(){
		
		$id=$this->input->post('id');	
		$name=$this->input->post('name');	
		$check=$this->unit_class_model->check_discount_name($id,$name);		
		$meg='';
			
			 if($check)
			 {	$msg='Single discount already present';
				$fl=1;
		 }
	 
		$data = array(      		
            'message' =>$msg,
            'fl' =>$fl
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	
	}
	
	function check_discount_plan(){
		
		$discount_id=$this->input->post('discount_id');	
		$booking_id=$this->input->post('booking_id');
		
		$check=$this->unit_class_model->check_discount_plan($discount_id,$booking_id);		
		$meg='';
			 if($check)
			 {	$msg='Single discount already present';
				$fl=1;
		 }else{
			 
			 $msg='Single discount already present';
				$fl=2;
		 }
		$data = array(      		
            'message' =>$msg,
            'fl' =>$fl
        );
        header('Content-Type: application/json');
        echo json_encode($data);
		
	
	}
	
	function yumpos(){
		
			 //$found = in_array("94",$this->arraay);
		if(1)
		{
			//redirect('192.168.1.3/yummpos');
			//window.location = "192.168.1.3/yumpos";
	}else{
		redirect('dashboard');
	}
	}
		
	
	
	function delete_hotel_laundry_line_item1(){		
		$id=$this->input->post('id');		
		//$quantity=$this->input->post('quantity');		
		//$lid=$this->input->post('lid');		
		$this->unit_class_model->delete_hotel_laundry_line_item($id);
		//$this->unit_class_model->update_laundry_master($lid,$quantity);
		$data = array(
            'data' =>"sucess",
        );
        header('Content-Type: application/json');
        echo json_encode($data);		
	
	}
	
	function nightAudit(){		
		$id=$this->input->post('id');
		$note=$this->input->post('note');
		$hotel_id=$this->session->userdata('user_hotel');		
		$user_id=$this->session->userdata('user_id');
		
		date_default_timezone_set('Asia/Kolkata');
		$d=date('Y-m-d H:i:s');
		$query=$this->unit_class_model->bookings_info_presentDay_single();
		//print_r($query);
		$totalUnit=0;
		$totalGuest=0;
		$single_Booking=0;
		foreach($query as $qur){
			//echo '<br>'.$qur->nRoom;
			if(is_numeric($qur->nRoom)){
			$totalUnit+=$qur->nRoom;
			}else{
				$totalUnit++;
			}
			$totalGuest+=$qur->tGuest;
			
			
			
		}
		$d1=date('Y-m-d');
		$query1=$this->unit_class_model->total_singleBooking_today($d1);
		$query2=$this->unit_class_model->total_groupBooking_today($d1);
		
		$query3=$this->unit_class_model->total_singleBooking_checkin($d1);
		$query4=$this->unit_class_model->total_singleBooking_checkout($d1);
		
		$query3_1=$this->unit_class_model->total_groupBooking_checkin($d1);
		$query4_1=$this->unit_class_model->total_groupBooking_checkout($d1);
		
		$query5=$this->unit_class_model->total_singleBooking_advance($d1);
		$query6=$this->unit_class_model->total_groupBooking_advance($d1);
		
		$query7=$this->unit_class_model->total_singleBooking_confirm($d1);
		$query8=$this->unit_class_model->total_groupBooking_confirm($d1);
		
		$query9=$this->unit_class_model->total_singleBooking_cancel($d1);
		$query10=$this->unit_class_model->total_groupBooking_cancel($d1);
		
		$query11=$this->unit_class_model->total_login($user_id,$hotel_id,$d1);
		$revenue=$this->dashboard_model->all_bookings_unique($hotel_id);
		
		$query12=$this->unit_class_model->total_stayDays($d1);
		$stayDays=$query12->stayDays;
		
		
		foreach($revenue as $rev){
			$todayRevenue=$rev->t_amount;
		}
		 
		//print_r($revenue);
		$login=$query11->login;
		$checkin_single=$query3_1->checkingroup;
		$checkout_single=$query4_1->checkoutgroup;
		
		
		$checkin_group=$query3->checkinsingle;
		$checkout_group=$query4->checkoutsingle;
		
		$checkIN=$checkin_single+$checkin_group;
		$checkOUT=$checkout_single+$checkout_group;
		$advance=$query5->singleAdvance+$query6->groupAdvance;
		$confirm=$query7->singleConfirm+$query8->groupConfirm;
		$cancel=$query9->singleCancel+$query10->groupCancel;
		$single_Booking=$query1->singletotal;
		$group_Booking=$query2->grouptotal;
		$totalUnit;//total number of units
		$totalGuest;//total Guest
		
		$single_Booking;//total single booking
		$group_Booking;//total group booking
		
		$data=array(
		'hotel_id'=>$hotel_id,
		'user_id'=>$user_id,
		'nadt_m_admin'=>$user_id,
		'nadt_m_date'=>$d,
		'nadt_m_note'=>$note,
		'nadt_m_count_s_booking'=>$single_Booking,
		'nadt_m_count_g_booking'=>$group_Booking,
		'nadt_m_total_unit'=>$totalUnit,
		'nadt_m_total_guest'=>$totalGuest,
		'nadt_m_count_checkin'=>$checkIN,
		'nadt_m_count_checkout'=>$checkOUT,
		'nadt_m_count_advance'=>$advance,
		'nadt_m_count_confirm'=>$confirm,
		'nadt_m_count_cancel'=>$cancel,
		'nadt_m_count_login'=>$login,
		'nadt_m_total_booking_revenue'=>$todayRevenue,
		'nadt_m_count_guest_staying'=>$stayDays,
		
		);
		//print_r($data);
		$query4=$this->unit_class_model->generate_nightAudit($data);
		
		
				///echo $confirm;
				//print_r($data);
				//print_r($query1);
				//print_r($query);
		if($query4){
			$status=1;
			$msg="success";
		
		}else{
			$status=0;
			$msg="Fail";
		}
		
		$data = array(
            'data' =>$msg,
            'status' =>$status
        );
        header('Content-Type: application/json');
        echo json_encode($data);
	
	}
	
	function travelagentcommission(){
		$data['heading'] = 'Reports';
            $data['sub_heading'] = 'Travel Agent Report';
            $data['description'] = 'Report List';
            $data['active'] = 'all_f_reports';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));

			 if($this->input->post())
			{	
            $start_date = date("Y-m-d", strtotime($this->input->post('t_dt_frm')));
            $end_date  = date("Y-m-d", strtotime($this->input->post('t_dt_to')));
			$data['start_date']=$this->input->post('t_dt_frm');
			$data['end_date']=$this->input->post('t_dt_to');
			$data['travelAgent'] = $this->unit_class_model->all_travelAgentCommision_by_date1($start_date,$end_date);
		
			
			
		} else{
		   $data['travelAgent'] = $this->unit_class_model->all_travelAgentCommision();
		}
		    $data['page'] = 'backend/dashboard/travelagentcommision';
            $this->load->view('backend/common/index', $data);
	}
	function travelagentcommission_type(){
		$type=$this->input->post('type');	
		$quer = $this->unit_class_model->all_travelAgentCommision_grp_single($type);
		$output='<table class="table table-striped table-hover table-bordered" id="sample_3">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Type</th>
          <th scope="col">Type Id</th>
          <th scope="col">Booking Id</th>
          <th scope="col">Group Id</th>
          <th scope="col">Date</th>
          <th scope="col">Booking Status</th>
          <th scope="col">Applable On</th>
          <th scope="col">Applied Amount</th>
          <th scope="col">Commision Amount</th>          
        </tr>
      </thead>
      <tbody>';
	  $i=0;$totalGroup=0;$totalAppliedAmo=0;$totalcomm=0;
				if(isset($quer) && $quer){
				foreach($quer as $travel){
			$i++;
	if($travel->groupID){
	$totalGroup++;
}if($travel->applyedAmount){
	$totalAppliedAmo+=$travel->applyedAmount;
}
if($travel->amount){
	$totalcomm+=$travel->amount;
}
if($travel->taType=='agent'){
$que=$this->bookings_model->get_broker_details($travel->taID);

if(isset($que) && $que){
foreach($que as $q){
$name='<span style="color:#008822; font-weight:bold;">'.$q->b_name.'</span>';
}
}}else{
$que=$this->bookings_model->get_channel_details($travel->taID);

if(isset($que) && $que){
foreach($que as $q){
$name='<span style="color:#0277BD; font-weight:bold;">'.$q->channel_name.'</span>';
}
}
}

        $output.='<tr><td>'. $i.'</td><td>'.$travel->taType.'</td><td>'.$name.'</td><td>'.$travel->bookingID.'</td><td>'.$travel->groupID.'</td><td>'.$travel->addedDate.'</td><td>';

$stat=$this->dashboard_model->get_status($travel->bkStatus);
if(isset($stat) && $stat){foreach($stat as $sta){  $St=$sta->booking_status;}}
 $output.=$St.'</td><td>'; 
				if($travel->commAppOn=='rr'){
				$coma="Room Rent";
				}else if($travel->commAppOn=='rrf'){
                        $coma="Room Rent + Food";
					}
				else if($travel->commAppOn=='tb'){
                        $coma="Total Bill";
					}else{
$coma='';
}
			
		$output.=$coma.'</td>';
		$output.='<td>'.$travel->applyedAmount.'</td>';
		$output.='<td>'.$travel->amount.'</td></tr></tbody>';
        	
		
		
	}
	$output.='<tfoot><tr><td></td>
        	<td></td>';
        	
          $output.='<td style="font-weight:700; color:#666666; text-align:left;"> TOTAL </td>
					<td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:left;" value="'.number_format($i,2,".",",").'" /></td>
					<td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:left;" value="'.number_format($totalGroup,2,".",",").'" /></td>
					<td></td><td></td><td></td>
					<td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:left;" value="'.number_format($totalAppliedAmo,2,".",",").'" /></td>
					<td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:left;" value="'.number_format($totalcomm,2,".",",").'" /></td>
        </tr>
          
        </tfoot>
    </table>';
      
}
	
	echo $output;

}

function add_hotel_stay_guest(){		
date_default_timezone_set('Asia/Kolkata');
		$Guest_name_new=$this->input->post('Guest_name_new');		
		$gd_g_contact_no=$this->input->post('gd_g_contact_no');		
		$gd_g_checkin=$this->input->post('gd_g_checkin');		
		$gd_g_checkout=$this->input->post('gd_g_checkout');		
		$gd_g_id_type=$this->input->post('gd_g_id_type');		
		$gd_g_id_charge=$this->input->post('gd_g_id_charge');		
		$gd_g_id_proof=$this->input->post('gd_g_id_proof');		
		$gd_g_document=$this->input->post('gd_g_document');		
		$Guest_hi_id=$this->input->post('Guest_hi_id');		
		$hgroup_id=$this->input->post('hgroup_id');		
		$booking_status_id_secondary=$this->input->post('booking_status_id_secondary');
		$gd_g_checkin=date_create($gd_g_checkin);
		$gd_g_checkin=date_format($gd_g_checkin,"Y-m-d H:i:s");
		$gd_g_checkin1=$gd_g_checkin;
		$Guest_hi_status=$this->input->post('Guest_hi_status');
		if($Guest_hi_status=='waiting'){			
			$sta_hi='waiting';
			}else if($Guest_hi_status=='checkin'){			
			$sta_hi='checkin';
			}else{
				$sta_hi='waiting';
			}
		$gd_g_checkout=date_create($gd_g_checkout);
		$gd_g_checkout=date_format($gd_g_checkout,"Y-m-d H:i:s");
		
			if($Guest_hi_status=='checkin'){
				$bst='after checkin';
			}else {
				 $bst='pending';
			}
			if($gd_g_id_charge){$ctype="1";}else{$ctype="0";}
		$bid=$this->input->post('bid');	

		$d=date('Y-m-d H:i:s');		
			$data=array(
			'booking_id'=>$bid,
			'group_id'=>$hgroup_id,
			'g_id'=>$Guest_hi_id,
			'addEvent'=>$bst,
			'status'=>$sta_hi,
			'stayPurpose'=>$gd_g_id_type,
			'docVerif'=>$gd_g_document,
			'isCancelled'=>0,
			'addOn'=>$d,
			'charge_applicable'=>$ctype,
			'chargeAmt'=>$gd_g_id_charge,
			'check_in_date'=>$gd_g_checkin1,
			'check_out_date'=>$gd_g_checkout,
			'hotel_id'=>$this->session->userdata('user_hotel'),
			'user_id'=>$this->session->userdata('user_id')
			
			
			
			);
			
			
			
			//print_r($data);
		$a=$this->unit_class_model->add_hotel_stay_guest($data);
		if($a){
		$data = array(
            'data' =>"sucess",
			'staid'=>$a
        );
		}else{
			$data = array(
            'data' =>"Fail",
			'staid'=>''
        );
		}
        header('Content-Type: application/json');
        echo json_encode($data);
	
	}

	
	
function add_guest_det(){		

		$guest_name=$this->input->post('guest_name');
		$guest_no=$this->input->post('guest_no');
		$guest_email=$this->input->post('guest_email');
		$guest_sex=$this->input->post('guest_sex');
		$guest_dob=$this->input->post('guest_dob');
		$guest_type=$this->input->post('guest_type');
date_default_timezone_set('Asia/Kolkata');
		$d=date('Y-m-d H:i:s');		
			$data=array(
			'g_name'=>$guest_name,
			'g_contact_no'=>$guest_no,
			'g_dob'=>$guest_dob,
			'g_type'=>$guest_type,
			'g_email'=>$guest_email,
			'g_gender'=>$guest_sex,
			'hotel_id'=>$this->session->userdata('user_hotel'),
			'user_id'=>$this->session->userdata('user_id')
			
			);
			
			//print_r($data);
		$a=$this->unit_class_model->add_guest_det($data);
		if($a){
		$data = array(
            'data' =>"sucess",
			'a'=>$a
        );
		}else{
			$data = array(
            'data' =>"Fail",
			'a'=>$a
        );
		}
        header('Content-Type: application/json');
        echo json_encode($data);	
	
	}
	
	
	function update_checkout_status_stay_guest(){		

		$booking_id=$this->input->post('booking_id');
		$stay_id=$this->input->post('stay_id');
		
		
		date_default_timezone_set('Asia/Kolkata');
		$d=date('Y-m-d H:i:s');		
			$data=array(
			'check_out_date'=>$d,
			'status'=>'checkout',
			'hotel_id'=>$this->session->userdata('user_hotel'),
			'user_id'=>$this->session->userdata('user_id')
			
			);
			
			//print_r($data);
		$a=$this->unit_class_model->update_checkout_status_stay_guest($data,$stay_id);
		if($a){
		$data = array(
            'data' =>"sucess",
			'a'=>$a
        );
		}else{
			$data = array(
            'data' =>"Fail",
			'a'=>$a
        );
		}
        header('Content-Type: application/json');
        echo json_encode($data);	
	
	}
	
	
	function check_no_guest(){		

		$booking_id=$this->input->post('booking_id');
			
		$a=$this->unit_class_model->check_no_guest($booking_id);
		if($a->total){
		$data = array(
            'data' =>"sucess",
			'a'=>$a->total
        );
		}else{
			$data = array(
            'data' =>"Fail",
			'a'=>$a->total
        );
		}
        header('Content-Type: application/json');
        echo json_encode($data);	
	
	}
	
	function delete_stay_guest(){		

		$booking_id=$this->input->post('booking_id');
		$stay_id=$this->input->post('stay_id');
			
		$a=$this->unit_class_model->delete_stay_guest($booking_id,$stay_id);
		if($a){
		$data = array(
            'data' =>"sucess",
			'a'=>$a
        );
		}else{
			$data = array(
            'data' =>"Fail",
			'a'=>$a
        );
		}
        header('Content-Type: application/json');
        echo json_encode($data);	
	
	}
	function delete_stay_guest1(){		

		$group_id=$this->input->post('group_id');
		$stay_id=$this->input->post('stay_id');
			
		$a=$this->unit_class_model->delete_stay_guest1($group_id,$stay_id);
		if($a){
		$data = array(
            'data' =>"sucess",
			'a'=>$a
        );
		}else{
			$data = array(
            'data' =>"Fail",
			'a'=>$a
        );
		}
        header('Content-Type: application/json');
        echo json_encode($data);	
	
	}
	
	function update_checkin_status_stay_guest(){		

		$booking_id=$this->input->post('booking_id');
		$stay_id=$this->input->post('stay_id');
		
		
		date_default_timezone_set('Asia/Kolkata');
		$d=date('Y-m-d H:i:s');		
			$data=array(
			'check_in_date'=>$d,
			'status'=>'checkin',
			'hotel_id'=>$this->session->userdata('user_hotel'),
			'user_id'=>$this->session->userdata('user_id')
			
			);
			
			//print_r($data);
		$a=$this->unit_class_model->update_checkout_status_stay_guest($data,$stay_id);
		if($a){
		$data = array(
            'data' =>"sucess",
			'a'=>$a
        );
		}else{
			$data = array(
            'data' =>"Fail",
			'a'=>$a
        );
		}
        header('Content-Type: application/json');
        echo json_encode($data);	
	
	}
	
	
	function update_group_master(){		

		$group_id=$this->input->post('grp');
		$id=$this->input->post('id');
		$name=$this->input->post('name');
		
		
		date_default_timezone_set('Asia/Kolkata');
		$d=date('Y-m-d H:i:s');		
			$data=array(			
			'guestID'=>$id,
			'name'=>$name
			);
			
			//print_r($data);
		$a=$this->unit_class_model->update_group_master($data,$group_id);
		if($a){
		$data = array(
            'data' =>"sucess",
			'a'=>$a
        );
		}else{
			$data = array(
            'data' =>"Fail",
			'a'=>$a
        );
		}
        header('Content-Type: application/json');
        echo json_encode($data);	
	
	}
	
	
	function roomview_dashboard(){	
	date_default_timezone_set('Asia/Kolkata');
	//	$d="date('Y-m-d')";		
		$d='2017-06-08';		
				
	if($this->input->post('id')){
		$id=$this->input->post('id');
	}
if(isset($id) && $id){
	
	$rooms= $this->bookings_model->room_view($id);
}else{
	
    $rooms= $this->bookings_model->room_view();
}
	$data='<div class="room-cat">';
	  $flg=0;
		  $typeOld = '';
		  if(isset($rooms) && $rooms){
			  foreach($rooms as $room){
				  
				  if((int)$room->group_id > 0 || (int)$room->booking_id > 0){
						$avl = '<small class="rava">BK';
						$bkd = 'bookd';
						
						if((int)$room->group_id > 0){
							$grp = '<i class="fa fa-group" aria-hidden="true" style="color:#138e9d;"></i>';
						}
						else
							$grp = '';
						
						if($room->exc_amt > 0)
							  $exc = '<i class="fa fa-plus" aria-hidden="true" style="color:#0000B3;"></i>';
						else
							  $exc = '';
						  
						if($room->pos_amt > 0)
							  $pos = '<i style="color:#FFAE04; " class="fa fa-cutlery" aria-hidden="true"></i>';
						else
							  $pos = '';
						
				  }
				  else{
								$avl = '<small class="rava active">AV';
								$bkd = '';
								$grp = '';
								$exc = '';
								$pos = '';
				  }
				  
				 if($typeOld != $room->unit_name){
					if($flg == 1){
				  
						$data.='</div>';
					} 
				 $data.='</br>';
				
				$data.='<h5><b>'.$room->unit_name.'</b>'.$room->unit_class.' | '.'Default Occupancy '.$room->default_occupancy.'/'.'Max Occupancy '.$room->max_occupancy.'</h5>';
				$data.='<div class="row">';
				 }
					$flg = 1;
				 
				 $resourc=$room->room_id;
				  if($room->pSts=="Confirmed" || $room->pSts=="Checked in" || $room->pSts=="Advance" || $room->pSts=="Temporary hold"){
				 $data.='<div class="col-md-2" onclick="pop_edit_bo('.$room->booking_id.')">';
				  }else{
					 //  $data.='<div class="col-md-2" onclick="pop_new_bo('.$start.','.$end.','.$resourc.')">';
					   $data.='<div class="col-md-2" onclick="pop_new_bo('.$resourc.')">';
				  }
                 $data.='<div class="room-v '.$bkd.'" style="border-right: 7px solid '.$room->color_primary.'; background: '.$room->pBdCC.';">';
                   $data.='<div class="room-vstatus" style="background:'.$room->pBaCC.';"></div>';
                    $data.='<div class="rot-av">';
                     $data.='<span class="rota">Ota</span>';
                      $data.='<span>';
                       $data.='<small class="rac active">AC</small>';
                        $data.=$avl;
						$data.='</small>';
						$data.='</span>';
                    $data.='</div>';					 
                    $data.='<div style="color:'.$room->color_primary.'" class="rroom-noo">';
					$data.=$room->room_no;
                    $data.='<span>'.$room->unit_type.'</span>';			
                     $data.='</div>';
                     $data.='<div class="rbed">';					  
                   	  $data.=$grp;
                       $data.=$exc;
                      $data.='<i class="fa fa-minus-circle" style="color:#E27979;"></i>';
                       $data.=$pos;              
                     $data.='</div>';
                     $data.='<div class="room-v-cl">';
                      $data.='<span>';                             
                        $data.='<dd>'.$room->g_name.'</dd>'; 
                        $data.='<small>';
						if($room->booking_id > 0) {
							
									 $data.= 'BK0'.$room->booking_id; 
						}
								
									
									 $data.='</small>';
								
                          $data.='</span><span>';
                         $data.='<small> Flr: '.$room->floor_no.'</small>';	
                            $data.=$room->pSts;
                             $data.='<small style="color:'.$room->sBaCC.';">'.$room->sSts.'</small>';
                         $data.='</span></div></div></div>';
				  
				  
				  $typeOld = $room->unit_name;
				  
				  
			 
			  } 
		  }
		  
		  
	$data.='</div>';
        header('Content-Type: application/json');
        echo json_encode($data);	
	
	}
	
	
	function roomview_dashboard_exp(){	

    $rooms= $this->bookings_model->room_view();

	$data='<div class="room-cat">';
	  $flg=0;
		  $typeOld = '';
		  if(isset($rooms) && $rooms){
			  foreach($rooms as $room){
				  
				  if((int)$room->group_id > 0 || (int)$room->booking_id > 0){
						$avl = '<small class="rava">BK';
						$bkd = 'bookd';
						
						if((int)$room->group_id > 0){
							$grp = '<i class="fa fa-group" aria-hidden="true" style="color:#138e9d;"></i>';
						}
						else
							$grp = '';
						
						if($room->exc_amt > 0)
							  $exc = '<i class="fa fa-plus" aria-hidden="true" style="color:#0000B3;"></i>';
						else
							  $exc = '';
						  
						if($room->pos_amt > 0)
							  $pos = '<i style="color:#FFAE04; " class="fa fa-cutlery" aria-hidden="true"></i>';
						else
							  $pos = '';
						
				  }
				  else{
								$avl = '<small class="rava active">AV';
								$bkd = '';
								$grp = '';
								$exc = '';
								$pos = '';
				  }
				  
				 if($typeOld != $room->unit_name){
					if($flg == 1){
				  
						$data.='</div>';
					} 
				 $data.='</br>';
				
				$data.='<h5><b>'.$room->unit_name.'</b>'.$room->unit_class.' | '.'Default Occupancy '.$room->default_occupancy.'/'.'Max Occupancy '.$room->max_occupancy.'</h5>';
				$data.='<div class="row">';
				 }
					$flg = 1;
				
				  $resourc=$room->room_id;
				 if($room->pSts=="Confirmed" || $room->pSts=="Checked in" || $room->pSts=="Advance" || $room->pSts=="Temporary hold"){
				 $data.='<div class="col-md-2" onclick="pop_edit_bo('.$room->booking_id.')">';
				  }else{
					   $data.='<div class="col-md-2" onclick="pop_new_bo('.$resourc.')">';
				  }
                 $data.='<div class="room-v '.$bkd.'" style="border-right: 7px solid '.$room->color_primary.'; background: '.$room->pBdCC.';">';
                   $data.='<div class="room-vstatus" style="background:'.$room->pBaCC.';"></div>';
                    $data.='<div class="rot-av">';
                     $data.='<span class="rota">Ota</span>';
                      $data.='<span>';
                       $data.='<small class="rac active">AC</small>';
                        $data.=$avl;
						$data.='</small>';
						$data.='</span>';
                    $data.='</div>';					 
                    $data.='<div style="color:'.$room->color_primary.'" class="rroom-noo">';
					$data.=$room->room_no;
                    $data.='<span>'.$room->unit_type.'</span>' ;                    
                     $data.='</div>';
                     $data.='<div class="rbed">';					  
                   	  $data.=$grp;
                       $data.=$exc;
                      $data.='<i class="fa fa-minus-circle" style="color:#E27979;"></i>';
                       $data.=$pos;              
                     $data.='</div>';
                     
					/* $data.='<div class="room-v-cl">';
                      $data.='<span>';                             
                        $data.='<dd>'.$room->g_name.'</dd>'; 
                         $data.='<small>Flr: '.$room->floor_no.'</small>';
                        $data.='<small>';
						if($room->booking_id > 0) {
							
									 $data.= 'BK0'.$room->booking_id; 
						}
								
									
									 $data.='</small>';
								
                          $data.='</span><span>';
                         
                            $data.=$room->pSts;
                             $data.='<small style="color:'.$room->sBaCC.';">'.$room->sSts.'</small>';
                         $data.='</span></div>';*/
						 
						$data.='</div></div>';
				  
				  
				  $typeOld = $room->unit_name;
				  
				  
			 
			  } 
		  }
		  
		  
	$data.='</div>';
        header('Content-Type: application/json');
        echo json_encode($data);	
	
	}
	
	
	function check_dublicate_guest(){		

		$booking_id=$this->input->post('bid');
		$guestid=$this->input->post('guestid');
			
		$a=$this->unit_class_model->check_dublicate_guest($booking_id,$guestid);
		if($a>0){
		$data = array(
            'data' =>1,
			
        );
		}else{
			$data = array(
            'data' =>2,
			
        );
		}
        header('Content-Type: application/json');
        echo json_encode($data);	
	
	}
	
	function guest_pic_change($ab){
		 $dir = "./upload/guest/originals/photo/";
		 
move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $_FILES["image"]["name"]);
$data=$_FILES["image"]["name"];

$newData=array(
//'admin_id'=>$ab,
'g_photo'=>$data
);
$query=$this->unit_class_model->guest_pic_change($newData,$ab);

header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	
	function guest_idproof_change($ab){
		 $dir = "./upload/guest/originals/id_proof/";
		 
move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $_FILES["image"]["name"]);
$data=$_FILES["image"]["name"];

$newData=array(
'g_id_proof'=>$data
);
$query=$this->unit_class_model->guest_pic_change($newData,$ab);

header('Content-Type: application/json');
        echo json_encode($data);
		
	}
	

function view_adhoc_bill(){
	
		$ad_id = $_GET['id'];
		date_default_timezone_set('Asia/Kolkata');
		$data['heading'] = 'AdHoc Billing';
        $data['sub_heading'] = 'All AdHoc Billing';
        $data['description'] = 'All AdHoc Billing';
        $data['active'] = 'adhoc_bills';
        $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
        $data['Ahb_data'] = $this->dashboard_model->get_adhoc_bill_master($ad_id);
        $data['Ahb_line_data'] = $this->dashboard_model->get_adhoc_bill_line_byID($ad_id);
        $data['page'] = 'backend/dashboard/edit_adhoc_bill';
        $this->load->view('backend/common/index', $data);
	
}

function delete_adhoc_line_master(){
    
    
    $adh_id = $this->input->post('adh_id');
   // echo $adh_id;
   // exit;
    $value = $this->unit_class_model->delete_adhoc_line_master($adh_id);
    if($value == "success"){
        
        echo "success";
    }else{
        
        echo "failed";
    }
    
}

	
}// end of controller



    ?>