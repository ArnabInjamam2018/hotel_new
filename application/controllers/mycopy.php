<?php 
function add_bill_payments()
	{	
		
               if($this->input->post())
                {   
                        //$id_g = $this->input->post('g_contact_no');
                        /* Images Upload */
                            
                        
                       
                        /* Images Upload*/ 
                        date_default_timezone_set("Asia/Kolkata");
                        
                          $date=$this->input->post('date');
						
                          $top=$this->input->post('top');
                          $vendor_name=$this->input->post('vendor_name');
                          $bill_month=$this->input->post('bill_month');
                          $billother=$this->input->post('billother');
                          $person_name=$this->input->post('person_name');
                          $designation=$this->input->post('designation');
						  $sal_month=$this->input->post('sal_month');
						  $sal_other=$this->input->post('sal_other');
						  $payment_to=$this->input->post('payment_to');
						  $payment_for=$this->input->post('payment_for');
						  $misc_month=$this->input->post('misc_month');
						  $misc_other=$this->input->post('miscother');
						  $description=$this->input->post('description');
						  $mop=$this->input->post('mop');
						  $checkno=$this->input->post('checkno');
						  $check_bank_name=$this->input->post('check_bank_name');
						  $draft_no=$this->input->post('draft_no');
						  $draft_bank_name=$this->input->post('draft_bank_name');
						  $ft_bank_name=$this->input->post('ft_bank_name');
						  $ft_account_no=$this->input->post('ft_account_no');
						  $ft_ifsc_code=$this->input->post('ft_ifsc_code');
						  $approved_by=$this->input->post('approved_by');
						  $recievers_name=$this->input->post('recievers_name');
						  $recievers_desig=$this->input->post('recievers_desig');
                      


                        $add_pay = array(

                        //'hotel_id' => $this->session->userdata('user_hotel'),
                        
                        
                        'date' =>$date,
                        'type_of_payment' => $top,
                        'vendor_name' => $vendor_name,
                        'amount' => $this->input->post("amount"),
                        'bill_month' => $bill_month,
                        'bill_other' => $billother,
                        'person_name' => $person_name,
						'designation' => $designation,
						'sal_month' => $sal_month,
						'sal_other' => $sal_other,
						'payment_to' => $payment_to,
						'payment_for' => $payment_for,
						'misc_month' =>  $misc_month,
						'misc_other' => $misc_other,
						'description' => $description,
						'mode_of_payment' => $mop,
						'check_no' => $checkno,
						'check_bank_name' => $check_bank_name,
						'draft_no' => $draft_no,
						'draft_bank_name' => $draft_bank_name,
						'ft_bank_name' => $ft_bank_name,
						'ft_account_no' => $ft_account_no,
						'ft_ifsc_code' => $ft_ifsc_code,
						'approved_by' => $approved_by,
						'recievers_name' => $recievers_name,
						'recievers_desig' => $recievers_desig,
						
                        
                        );
                        if($check_bank_name!="")
                        {
                            $t_bank_name=$check_bank_name;
                        }else if($draft_bank_name!="")
                        {
                            $t_bank_name=$draft_bank_name;
                        }else{
                            $t_bank_name=$ft_bank_name;
                        }
						
						$t_pay=array(
								't_date' => $date,
								't_amount' => $this->input->post("amount"),
								't_payment_mode' => $mop,
								't_bank_name' => $t_bank_name,
								
								);
                        
                        //print_r($dgset);
                        //exit();
					  //$transfer=$this->dashboard_model->transfer_payments($add_pay);
                      $query=$this->dashboard_model->add_payments($add_pay);
                        if (isset($query) && $query) 
                        {
                            $this->session->set_flashdata('succ_msg', "Bills Added Successfully!");
                            redirect('dashboard/add_bill_payments');
                        } 
                        else 
                        {
                            $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                            redirect('dashboard/add_bill_payments');
                        }
                        
                }
                else
                {
                    $data['page'] = 'backend/dashboard/add_bill_payments';
					$data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
					$this->load->view('backend/common/index', $data);
                }	
		
		
	}

	function add_purchase(){

        if($this->session->userdata('broker') =='1'){
            $data['heading']='Purchase';
            $data['sub_heading']='Add Purchase';
            $data['description']='Add Purchase Here';
            $data['active']='add_purchase';
            $data['hotel_name'] = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
            if($this->input->post()) {

                $purchase=array(
                    'p_id' => $this->input->post('p_id'),
                    'hotel_id' => $this->session->userdata('user_hotel'),
                    'a_id' => $this->input->post('a_id'),
                    'p_date' => date("Y-m-d H:i:s", strtotime($this->input->post('p_date'))),
                    'p_supplier' => $this->input->post('p_supplier'),
                    'p_i_name' => $this->input->post('p_i_name'),
                    'p_i_description' => $this->input->post('p_i_description'),
                    'p_i_price' => $this->input->post('p_i_price'),
                    'p_i_quantity' => $this->input->post('p_i_quantity'),
                    'p_unit' => $this->input->post('p_unit'),
                    'p_price' => $this->input->post('p_price'),
                    'p_tax' => $this->input->post('p_tax'),
                    'p_discount' => $this->input->post('p_discount'),
                    'p_total_price' => $this->input->post('p_total_price'),

                );

               // print_r($purchase);
               // exit;

                date_default_timezone_set('Asia/Kolkata');

                $transaction=array(

                    't_amount' =>$this->input->post('p_total_price'),
                    't_date' => date("Y-m-d H:i:s"),
                    'transaction_releted_t_id' => 5,
                    't_status' => "Done",
                    'transaction_from_id' =>4,
                    'transaction_to_id' =>6,

                );


                $query = $this->dashboard_model->add_purchase($purchase);
                $query2 = $this->dashboard_model-> add_transaction($transaction);

                if (isset($query) && $query) {
                    $this->session->set_flashdata('succ_msg', "Purchase Added Successfully!");
                    redirect('dashboard/add_purchase');
                } else {
                    $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
                    redirect('dashboard/add_purchase');
                }

            }
            else{
                $data['page'] = 'backend/dashboard/add_purchase';
                $this->load->view('backend/common/index', $data);
            }
        }else{
            redirect('dashboard');
        }
    }
	?>
    <th scope="col">
                                Date
                            </th>
                             <th scope="col">
                                Type of payment
                            </th>
                            <th scope="col">
                                 Payment To
                            </th>
                           <!--  <th scope="col">
                                Payment For
                            </th> -->
                             <th scope="col">
                                Amount
                            </th>
                          <th scope="col">
                                 Month
                            </th>
                            <th scope="col">
                                 Tax Type
                            </th>
                            <th scope="col">
                                 Others
                            </th>
                            <th scope="col">
                                 Description
                            </th>
                            <th scope="col">
                                 Mode Of Payment
                            </th>
                            <th scope="col">
                                 Check/Draft No.
                            </th>
                            <th scope="col">
                                Bank Name
                            </th>
                            <th scope="col">
                                Account Number
                            </th>
                            <th scope="col">
                                IFSC code No.
                            </th>

                            <th scope="col">
                                Approved By
                            </th>
                            <th scope="col">
                                Paid By
                            </th>
                            <th scope="col">
                                Reciever's Name
                            </th>
                            <th scope="col">
                               Reciever's Signature
                            </th>
                            <th scope="col">
                               Action
                            </th>