<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td{
	font-size:9px;
}
</style>

<div style="padding:15px 35px;">
<table>
		<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));?>
    <tr>
      <td colspan="2" align="center"><img src="upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo" class="logo-default" 
                        style="width: 82%;margin-top: 0px; margin-left: -63px;"/></td>
    </tr >
	<tr>
		<td colspan="3" align="right"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?></td>
        </tr>
    <tr>
      <td colspan="2"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%">&nbsp;</td>
    </tr>
</table>
<table class="td-pad" width="100%">
  <thead>
    <tr style="background: #e5e5e5; color: #1b1b1b">
			 <th scope="col">Total Room Inventory</th>
              <th scope="col">Rev Par (Incl. Inclusions)</th>
              <th scope="col">RevPAR(Excl. Inclusions) </th>
              <th scope="col">Total Room Rent</th>
              <th scope="col">Total Room Inclusions</th>
              <th scope="col">Avg.Daily Rate (Incl.Inclusion)</th>
              <th scope="col">Total Taxes</th>
              <th scope="col">Gross Total</th>
    </tr>
    </tr>
  </thead>
 <tbody style="background: #F2F2F2">
            <tr>
              <?php
				//Day
				/*echo $number = cal_days_in_month(CAL_GREGORIAN, 02, 2016);
                    $y= date("Y");
                    $m= date("m"); 
                    $maxDays=date('t');*/
					$sqlTotal=0;
					$sum2=0;
					$sum3=0; 
					$tax=0;
					$gross=0;
                    for($i=1; $i<=$maxDays; $i++){
                        if($i<10){
                          $val= $y.'-'.$m.'-'.'0'.$i;  
                        }else{
                          $val= $y.'-'.$m.'-'.$i; 
                        } 
                        $sql= $this->dashboard_model->roomcount($val);
                        $avgdailyrent=$this->dashboard_model->avgdailyrent($val);
						$sqlTotal +=$sql;
						
							
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$sum2=$sum2+$av->room_rent_total_amount;//exit();
								}
							}
							
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$sum3=$sum3+$av->service_price;//exit();
								}
							}
							
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$tax=$tax+$av->room_rent_tax_amount;//exit();
								}
							}
							
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$gross=$gross+$av->room_rent_tax_amount+$av->room_rent_total_amount+$av->service_price;//exit();
								}
							}
							
							
							
							
							
					}
						//$sum=0;
						//if(isset($avgdailyrent) && $avgdailyrent){
						//foreach($avgdailyrent as $avg){
							//echo $sum=$sum+$avg->room_rent_total_amount;
						
						?>
              <td align="center">
                <?php //Room Count
										$inventory=$totalrooms*$maxDays;
										echo $inventory;
										?>
               </td>
              <td align="center">
                <?php 														
										echo round(($sum2/$inventory),2);
									?>
               </td>
              <td align="center">
                <?php 
									//Room Inclusion
									echo round((($sum2+$sum3)/$inventory),2);
									?>
                </td>
              <td align="center"> <?php echo $sum2;
									?></td>
              <td align="center"> <?php echo $sum3;
									?></td>
              <td align="center">
                <?php  
						if($sqlTotal=="0"){
							echo "0";
						}else{
							echo round(($avg=$sum2/$sqlTotal),2);
						}
				?>
               </td>
              <td align="center">
                <?php 
					echo $tax;
				?>
                </td>
              <td align="center">
                <?php 
					echo $gross;
									?>
                </td>
            </tr>
            </a>
   </tbody>
</table>
</div>
