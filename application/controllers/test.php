<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Verdana, Geneva, sans-serif;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 11px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 2px;
}
</style>
<div style="padding:10px 35px;">
<?php 


if(isset($adjstAmt) && $adjstAmt){
	$adjstAmt=$adjstAmt->amount;
}else{
	$adjstAmt=0;
}


//print_r($line_items);
$glo=0;
$glo1=0;
$stat=0;
$chk=$this->bookings_model->get_invoice_settings();

if(isset($chk->booking_source_inv_applicable)){
    $booking_source = $chk->booking_source_inv_applicable;
} else {
	$booking_source ="";
}
if(isset($chk->nature_visit_inv_applicable)){
    $nature_visit = $chk->nature_visit_inv_applicable;
} else {
	$nature_visit ="";
}
if(isset($chk->booking_note_inv_applicable)){
    $booking_note = $chk->booking_note_inv_applicable;
} else {
	$booking_note ="";
}
if(isset($chk->company_details_inv_applicable)){
    $company_details = $chk->company_details_inv_applicable;
} else {
	$company_details ="";
}
if(isset($chk->service_tax_applicable)){
    $service_tax = $chk->service_tax_applicable;
} else {
	$service_tax =0;
}
if(isset($chk->service_charg_applicable)){
    $service_charg = $chk->service_charg_applicable;
} else {
	$service_charg = 0;
}
if(isset($chk->luxury_tax_applicable)){
    $luxury_tax = $chk->luxury_tax_applicable;
} else {
	$luxury_tax =0;
}

if(isset($chk->invoice_pref) && isset($chk->invoice_suf)){	
	$suf=$chk->invoice_suf;
    $pref=$chk->invoice_pref;
} else {
	$suf="";
    $pref="";	
}

if(isset($chk->unit_no)){
    $unit_no = $chk->unit_no;
} else {
	$unit_no = 0;
}
if(isset($chk->unit_cat)){
    $unit_cat = $chk->unit_cat;
} else {
	$unit_cat = 0;
}
if(isset($chk->invoice_setting_food_paln)){
    $fd_plan = $chk->invoice_setting_food_paln;
} else {
	$fd_plan =0;
}

if(isset($chk->invoice_setting_service)){
    $service = $chk->invoice_setting_service;
} else {
	$service =0;
}

if(isset($chk->invoice_setting_extra_charge)){
    $extra_charge = $chk->invoice_setting_extra_charge;
} else {
	$extra_charge =0;
}

if(isset($chk->invoice_setting_laundry)){
    $laundry = $chk->invoice_setting_laundry;
} else {
	$laundry =0;
}

foreach ($details2 as $key3 ) { ?>
<table width="100%">
  <tr>
    <td align="left" valign="middle"><img src="upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo" /></td>
    <td align="right" valign="middle"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?></td>
  </tr>
  <tr>
    <td colspan="2" width="100%"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
  </tr>
</table>
<table width="100%">
<?php 
			if(isset($tax)){
			foreach($tax as $tax_details){			
		?>
<tr>
  <td width="80%" align="left" valign="top"><?php if($company_details == '1'){ ?>
    Services Tax Reg No.:
    <?php  echo $tax_details->service_tax_no;?>
    <br />
    Vat Reg No.:
    <?php  echo $tax_details->vat_reg_no;?>
    <br />
    Luxury Tax No.:
    <?php  echo $tax_details->luxury_tax_reg_no;?>
    <br />
    CIN No:
    <?php  echo $tax_details->cin_no; } else { ?>
    &nbsp;
    <?php } ?>    
    <?php
   $guest_det=$this->dashboard_model->get_guest_details($key3->guestID);
   foreach($guest_det as $gst)
     {
       if(($key3->bill_to_com == 1) && ($gst->g_type == 'Corporate') && ($gst->corporate_id != NULL) && ($gst->corporate_id != 0)) // Check if Bill to Company is Possible
		  {
			$corporate_details = $this->dashboard_model->fetch_c_id1($gst->corporate_id); 
	   ?>
    <ul class="list-unstyled">
      <li> <strong style="color:#6B67A1;">
        <?php
					if($corporate_details->legal_name)
						echo $corporate_details->legal_name;
					else
						echo $corporate_details->name;
				?>
        </strong> </li>
      <li>
        <?php 
                    $fg = 10;
                    if(isset($corporate_details->address) && ($corporate_details->address != '' && $corporate_details->address != ' ')){
                                            echo ($corporate_details->address);
                                            $fg = 0	;
                                            //echo ', ';
                                        }
                    if(isset($corporate_details->city) && ($corporate_details->city != '' && $corporate_details->city != ' ')){
                                            if($fg == 0){
                                                echo ', ';
                                                
                                            }
                                                
                                            echo $corporate_details->city;
                                            echo '<br>';
                                            $fg = 1;
                                        }
                    else if($fg == 0)
                                            $fg = 5;
                    if(isset($corporate_details->state) && ($corporate_details->state != '' && $corporate_details->state != ' ')){
                                            if($fg == 5)
                                                echo '<br>';
                                            echo $corporate_details->state;
                                            $fg = 2;
                                            //echo ', ';
                                        }
                    if(isset($corporate_details->pin_code) && $corporate_details->pin_code!= NULL){
                                            if($fg == 2 || $fg == 5){
                                                echo ', ';
                                            }									
                                            echo 'PIN - '.$corporate_details->pin_code;
                                            $fg = 3;
                                        }
                    if(isset($corporate_details->country) && $corporate_details->country != '' && $corporate_details->country != ' '){
                                            if($fg == 3)
                                                echo ', ';
                                            echo $corporate_details->country;
                                            /*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';
                                            $fg = 4;*/
                                        }
                    if($fg == 10){
                                            echo 'No Info';
                                        }
                ?>
      </li>
      <li> <?php echo '<strong>Phone: </strong>'.$corporate_details->g_contact_no; ?> </li>
      <li> <?php echo '<strong>Email: </strong>'.$corporate_details->corp_email; ?> </li>
      <li>&nbsp; &nbsp;</li>
      <li>
        <?php 
					echo '<strong style="color:#06B4F5">Guest Name: </strong>';
					echo $gst->g_name;
					if($gst->c_des)
						echo ' - '.$gst->c_des;
												
							?>
        (Group Owner) </li>
    </ul>
    <?php  } 
	 else{
	?>
    <ul class="list-unstyled">
      <li> <strong style="color:#6B67A1; text-size:12px;">
        <?php 
					
					echo $gst->g_name;
					
							?>
        </strong>(Group Owner)</li>
      <li>
        <?php 
									
									// Logic for Guest Address
									$fg = 10;
									if(isset($gst->g_address) && ($gst->g_address != '' && $gst->g_address != ' ')){
										echo ($gst->g_address);
										$fg = 0	;
										//echo ', ';
									}
									if(isset($gst->g_city) && ($gst->g_city != '' && $gst->g_city != ' ')){
										if($fg == 0){
											echo ', ';
											
										}
											
										echo $gst->g_city;
										echo '<br>';
										$fg = 1;
									}
									else if($fg == 0)
										$fg = 5;
									if(isset($gst->g_state) && ($gst->g_state != '' && $gst->g_state != ' ')){
										if($fg == 5)
											echo '<br>';
										echo $gst->g_state;
										$fg = 2;
										//echo ', ';
									}
									if(isset($gst->g_pincode) && $gst->g_pincode!= NULL){
										if($fg == 2 || $fg == 5){
											echo ', ';
										}									
										echo 'PIN - '.$gst->g_pincode;
										$fg = 3;
									}
									if(isset($gst->g_country) && $gst->g_country != '' && $gst->g_country != ' '){
										if($fg == 3)
											echo ', ';
										echo $gst->g_country;
										/*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';
										$fg = 4;*/
									}
									if($fg == 10){
										echo 'No Info';
									}
								?>
      </li>
      <li><strong>Phone: </strong>
        <?php if($gst->g_contact_no) {echo $gst->g_contact_no;} else { echo '<span style="color:#AAAAAA;">No info</span>';} ?>
      </li>
      <li><strong>Email: </strong>
        <?php if($gst->g_email) {echo $gst->g_email;} else { echo '<span style="color:#AAAAAA;">No info</span>';} ?>
      </li>
    </ul>
    <?php	 
	 }
	 
	 
	 
	 }
		?>
    </td>
  <td width="20%" align="left" valign="top"><?php
$paid=0;
	$invoice=$this->dashboard_model->get_invoice_grp($key3->id);
	if(isset($invoice) && $invoice ){
	$invno = $invoice->invoice_no;
	$invid = $invoice->id;
	}else{
		$invno='';
		$invid='';
		
	}
?>
    <span style="color:#F8681A; font-weight:bold; font-size:12px; line-height:20px; display:block; padding-bottom:5px;">INVOICE (Group Booking)</span> <?php echo $pref.$invno.$invid.$suf; ?><br />
    <?php 
			date_default_timezone_set("Asia/Kolkata");
			echo "DATE:&nbsp;",date("g:ia dS M Y") ?>
			
			<div class="well"> <strong><?php echo  $hotel_name->hotel_name; ?></strong><br/>
      <?php echo  $hotel_contact['hotel_street1']; ?> <?php echo  $hotel_contact['hotel_street2'].', '; ?><br>
      <?php echo  $hotel_contact['hotel_district']; ?> - <?php echo  $hotel_contact['hotel_pincode']; ?> <?php echo  $hotel_contact['hotel_state']; ?> - <?php echo  $hotel_contact['hotel_country']; ?><br/>
      <strong>Phone:</strong> <?php echo  $hotel_contact['hotel_frontdesk_mobile']; ?><br/>
      <strong>Email:</strong> <?php echo  $hotel_contact['hotel_frontdesk_email']; ?><br/>
    </div>
			</td>
</tr>
<?php }}?>

    </table>
<?php } ?>
<span style="font-size:13px; font-weight:700; color:#F8681A; display:block; padding:5px 5px;">Booking Details</span>
<hr style="background: #F8681A; border: none; height: 1px;">
<table class="td-pad" width="100%">
  <thead>
    <tr style="background: #EDEDED; color: #1b1b1b">
      <th width="4%" align="center" valign="middle"> # </th>
      <th width="10%" align="center" valign="middle" width="10%"> Unit </th>
      <th width="8%" align="center" valign="middle"> Guest </th>
      <th width="14%" align="center" valign="middle"> Stay Dates </th>
      <!--<th align="center" valign="middle"> Chk Out </th>-->
      <th width="4%" align="center" valign="middle"> Adlt </th>
      <th width="4%" align="center" valign="middle"> Kid </th>
      <th align="center" valign="middle"> T_R_R </th>
	  <?php if($service_tax == '1'){ ?>
     
	  <?php } ?>
	  <?php // if($service_charg == '1'){ ?>
     <!-- <th align="center" valign="middle"> Serivce Charge </th>-->
	  <?php //}?>
	  <?php if($luxury_tax == '1'){ ?>
      <!-- <th align="center" valign="middle"> Lux Tax </th>-->
	  <?php } ?>
	  <?php if($fd_plan == '1'){ ?>
      <th align="center" valign="middle"> T_F_I </th>
      <th align="center" valign="middle">Food VAT </th>
	   <?php } ?>
      <th align="right" valign="middle"> Total </th>
    </tr>
  </thead>
  <tbody style="background: #FAFAFA">
    <?php 
	$group=$this->dashboard_model->get_bookingLineItem_group_id($_GET['group_id']); // Geting The Unit Type
			//echo '<pre>';
			//print_r($group);
		$count_r = 0;
		$total=0; 
	if($details){ foreach ($details as $key ) { 
	?>
	
    <tr>
      <td align="center" valign="middle"><?php echo $key->id; ?></td>
      <td align="center" valign="middle">
		<?php // Room Details
			$count_r++;
			$unit=$this->dashboard_model->get_unit_exact_by_unitno($key->roomNO); // Geting the Unit ID based on Room No
		
			$units=$this->dashboard_model->get_unit_exact($unit->unit_id); // Geting The Unit Type
			
			if(isset($units))
				$unit_name=$units->unit_name;
                        /*foreach ($units as $key) {
                            # code...
                                $unit_name=$units->unit_name;
                        }*/
			// Logic for checking Invoice Settings, wheather to show Unit Details _sb
			//print_r($units);
			if($unit_cat == '1'){
				echo $unit_name;
					
			}
			else{
				if($unit_no != '1')
					echo 'Unit '.$count_r;
			}
					
			if($unit_no == '1'){
				echo " (Unit No - ".$key->roomNO.") ";
					
			}
			//echo '<br>'.$unit_name;
			/*if($unit_no == '1')
				echo $key->roomNO;
			else 
				echo 'Unit'.' '.$count_r;*/
		?>
	  </td> <!--Room No-->
      <td align="center" valign="middle">
		<?php 
			echo $key->gestName; 
			//echo $guest_name;
		?>
	  </td>
      <td align="center" valign="middle">
		<?php 
			echo date("dS-M-Y",strtotime($key->startDate)); 
			echo '<br> to <br>';
			echo date("dS-M-Y",strtotime($key->endDate));
		?>
	  </td>
      <!--<td align="center" valign="middle">
		<?php 
			echo date("dS-M-Y",strtotime($key->endDate)); 
		?>
	  </td>-->
      <td align="center" valign="middle">
		<?php 
			echo $key->adultNO; 
		?>
	  </td>
      <td align="center" valign="middle">
		<?php 
			echo $key->childNO; 
		?>
	  </td>
      <td align="center" valign="middle">
		<?php 
		//total room rent
			echo $key->trr; 
		?>
	  </td>
	  <?php if($service_tax == '1'){ // Checking the Invoice Settings?>
      <!--<td align="center" valign="middle">
		<?php //Service Tax
			//echo $key->room_st; 
		?>
	  </td>-->
	  <?php } ?>
	  
	  <?php //if($service_charg == '1'){ // Checking the Invoice Settings?> 
     <!-- <td align="center" valign="middle">
		<?php // Service Charge
			//echo $key->room_sc; 
		?>
	  </td>-->
	  <?php //} ?>
	  
	  <?php if($luxury_tax == '1'){ ?>
     <!-- <td align="center" valign="middle">
		<?php // Luxury Tax
			//echo $key->room_vat; 
		?>
	  </td>-->
	  <?php } ?>
	  <?php if($fd_plan == '1'){ ?>
      <td align="center" valign="middle">
		<?php 
			echo $key->tfi; 
		?>
	  </td>
	  
      <td align="center" valign="middle">
		<?php 
			echo $key->food_vat; 
		?>
	  </td>
	  <?php }?>
      <td align="right" valign="middle">
		<?php 
			echo '<strong>'.$key->price.'</strong>'; 
		?>
	  </td>
    </tr>

    <?php $total=$total+ $key->price+ $key->service; } }?>
  </tbody>
  	 
</table>
<hr style="background: #EDEDED; border: none; height: 1px;">

<?php

                    foreach ($details2 as $groups ) {
                      # code...

                       $charge_string=$groups->charges_id;

                        $charge_total_price=$groups->charges_cost;


                    }

                     
                            $charge_sum=0;
                if($charge_string!=""){
                    ?>
<span style="font-size:13px; font-weight:700; padding:5px 5px; display:block; color:#366B9B;">Extra Charge Details</span>
<hr style="background: #366B9B; border: none; height: 1px;">
<table class="td-pad" width="100%">

  <thead>
    <tr style="background: #EDEDED; color: #1b1b1b">
      <th width="5%" align="center" valign="middle"> No </th>
      <th width="33%" align="center" valign="middle"> Product / Service Name </th>
      <!--<th class="hidden-480">
                         Amount
                    </th>-->
      <th width="10%" align="center" valign="middle"> Quantity </th>
      <th width="16%" align="center" valign="middle"> Unit Price </th>
      <th width="10%" align="center" valign="middle"> Amount </th>
      <th width="16%" align="center" valign="middle"> Total Tax </th>
      <th width="10%" align="right" valign="middle"> Total </th>
    </tr>
  </thead>
  <tbody style="background: #FAFAFA">
    <?php 

                    $charge_array=explode(",", $charge_string);

                    //print_r($service_array); 

                    for ($i=0; $i < sizeof($charge_array) ; $i++) { 
                        # code...

                        $charges=$this->dashboard_model->get_charge_details($charge_array[$i]);
                  
                        # code...
               if($charges && isset($charges)){   

               ?>
    <tr>
      <td align="center" valign="middle" ><?php echo $charges->crg_id; ?></td>
      <td align="center" valign="middle" ><?php if(isset($charges->crg_description)){ echo $charges->crg_description; }?></td>
      <td align="center" valign="middle" ><?php echo $charges->crg_quantity; ?></td>
      <td align="center" valign="middle" ><?php echo $charges->crg_unit_price; ?></td>
      <td align="center" valign="middle" ><?php $charge_total= ($charges->crg_unit_price * $charges->crg_quantity); echo number_format($charge_total, 2, '.',''); ?></td>
      <td align="center" valign="middle" ><?php echo $charge_tax= ($charges->crg_tax ); 
                        echo "%";?></td>
      <td align="right" valign="middle" ><?php echo $charge_grand_total=$charges->crg_total; ?></td>
    </tr>
    <?php  $charge_sum=$charge_sum+$charge_grand_total;  } }?>
  </tbody>
</table>
<hr style="background: #EDEDED; border: none; height: 1px;">
<?php } ?>
<!-- pos start --->
      <?php $sum_pos=0; 
	$total_bill_amount = 0 ;
$total_tax_amount = 0 ;

if($pos && isset($pos)){  
 $sum=0; $sum1=0; 	$posGAMTT=0; 
$posGAMT=0; 

foreach ($pos as $key) {
			   
				# code...
			 
			 $sum=$sum+$key->total_due;
	   $sum1=$sum1+$key->total_amount;  
	   $posGAMTT=$key->total_amount- $key->total_paid; 
	   $posGAMT=$posGAMT+$posGAMTT;
		$total_bill_amount = $total_bill_amount + $key->bill_amount ;
		
		$total_tax_amount = $total_tax_amount + $key->tax ;				   
	   } 
	   }
?>
         
      <!-- pos end ---> 
      <!--charge end--> 
	  
	  <?php if(isset($all_adjst) && $all_adjst){
		  ?>
		<span style="font-size:13px; font-weight:700; color:#D75C58; padding:5px 5px; display:block;"> Adjustment Details </span>
      <hr style="background: #D75C58; border: none; height: 1px;">  
	 
		<table width="100%" class="td-pad">
    <thead>
      <tr style="background: #EDEDED; color: #1b1b1b">
        <th > # </th>
        <th  > Date </th>
        <th > Reason </th>
        <th > Amount</th>
        <th > Note </th>
       
      </tr>
    </thead>
    <tbody style="background: #fafafa">
      <?php $i=1;
     $m=0;
	
		//print_r($all_adjustment);
           foreach($all_adjst as $adjst){
			  ++$m; 
			 ?>
      <tr>
        <td style="text-align:left"><?php  echo $i; ?></td>
         <td style="text-align:center"><?php echo date("g:ia dS M Y",strtotime($adjst->addedOn)); ?></td>
		<td style="text-align:center">
			<?php echo $adjst->reason ;?>
		</td>
        <td style="text-align:center"><?php echo $adjst->amount; ?></td>
        <td style="text-align:center"><?php echo $adjst->note; ?></td>
       
        
      </tr>
     
       <tr>
        <td colspan="7" ><hr style="background: #EEEEEE; border: none; height: 1px; "></td>
      </tr>
		   <?php $i++; }?>  
    </tbody>
  </table>	


	 <?php }?>
	  
	  
	  
	  
	  <?php   if($transaction){ ?>
	  
      <span style="font-size:13px; font-weight:700; color:#D75C58; padding:5px 5px; display:block;"> Payment Details </span>
      <hr style="background: #D75C58; border: none; height: 1px;">
      <table width="100%" class="td-pad">
        <thead>
          <tr style="background: #EDEDED; color: #1b1b1b">
            <th width="5%" align="left"> # </th>
            <th width="5%" align="left"> Status </th>
            <th width="20%"> Transaction Date </th>
            <th width="10%"> Profit Center </th>
            <th width="15%"> Payment Mode </th>
            <th width="30%" align="center"> Transaction Details </th>
            <th width="10%" align="right" style="padding-right:8px;"> Amount </th>
          </tr>
        </thead>
        
        <tbody style="background: #fafafa">
          <?php   
			  $i = 0;
			  $paid=0;
			  foreach ($transaction as $keyt ) {
				  $paid=$paid+$keyt->t_amount;
			  $i++;
			  ?>
          <tr>
            <td><?php echo $i;?></td>
            <td align="center" valign="middle"><?php 
						if($keyt->t_status == 'Cancel')
							echo '<span style="color:red;">Declined</span>';
						else if($keyt->t_status == 'Pending')
							echo '<span style="color:orange;">Processing</span>';
						else
							echo '<span style="color:green;">Recieved</span>';
					?></td>
            <td align="center" valign="middle"><?php echo date("g:ia dS M Y",strtotime($keyt->t_date)); ?></td>
            <td align="center" valign="middle"><?php echo $keyt->p_center; ?></td>
            <td align="center" valign="middle"><?php echo $keyt->t_payment_mode; ?></td>
            <td align="center" valign="middle"><?php echo $keyt->t_bank_name; ?></td>
            <td align="right" valign="middle"><?php echo $keyt->t_amount; ?></td>
          </tr>
          <?php  } ?>
          <tr>
            <td colspan="7" ><hr style="background: #EEEEEE; border: none; height: 1px; "></td>
          </tr>
          <tr>
            <td colspan="6" style="text-align:right; padding-right:16%;"><strong>Total Paid Amount:</strong></td>
            <td align="right" valign="middle"><strong>
              <?php 
	$amountPaid = $this->dashboard_model->get_amountPaidByGroupID($_GET['group_id']);
	
	if(isset($amountPaid) && $amountPaid) {
		$paid=$amountPaid->tm;
		echo $paid;
	} 
	else {
		$paid=0;
		echo "Yet to pay";
	}
	
?>
              </strong></td>
          </tr>
        </tbody>
        <?php  } ?>
      </table>
      <hr style="background: #EEEEEE; border: none; height: 1px;">
      <table class="td-pad td-fon-size" width="100%">
        <tr>
          <td align="right" width="77%" valign="middle">Room Rent Booking Amount:</td>
          <td align="right" valign="middle"><?php 
//echo number_format(($total), 2, '.','');
echo number_format(($line_items['all_price']['room_rent']), 2, '.','');
?></td>
        </tr>
        <?php
			//echo '<pre>';
			//print_r($line_items);
			unset($line_items['rr_tax']['r_id']);
			unset($line_items['rr_tax']['planName']);
			unset($line_items['rr_tax']['total']);
			foreach($line_items['rr_tax'] as $key => $value){
				
				?>
        <tr>
          <td align="right" valign="middle">Room Rent <?php echo $key;?>:</td>
          <td align="right" valign="middle"><?php 
echo number_format(($value), 2, '.','');
?></td>
        </tr>
        <?php }?>
        <tr>
          <td align="right" valign="middle"> Meal Plan Charge:</td>
          <td align="right" valign="middle"><?php 
			echo number_format(($line_items['all_price']['meal_plan_chrg']), 2, '.','');
			?></td>
        </tr>
        <?php
			//echo '<pre>';
			//print_r($line_items);
			unset($line_items['mp_tax']['total']);
			unset($line_items['mp_tax']['r_id']);
			unset($line_items['mp_tax']['planName']);
			foreach($line_items['mp_tax'] as $key => $value){
				
				?>
        <tr>
          <td align="right" valign="middle">Meal Plan <?php echo $key?>:</td>
          <td align="right" valign="middle"><?php 
echo number_format(($value), 2, '.','');
?></td>
        </tr>
        <?php }?>
        <?php if($extra_charge==1){ ?>
        <tr>
          <td align="right" valign="middle">Ex Chrage Amount:</td>
          <td align="right" valign="middle"><?php 
			//echo number_format(($charge_sum), 2, '.','');
			echo number_format(($line_items['all_price']['exrr_chrg']), 2, '.','');
			?></td>
        </tr>
        <?php }?>
        <tr>
          <td align="right" valign="middle"><strong>POS Amount:</strong></td>
          <td align="right" valign="middle"><strong><?php echo number_format($total_bill_amount, 2, '.','');?></strong></td>
        </tr>
        <tr>
          <td align="right" valign="middle"><strong>POS Service Charge:</strong></td>
          <td align="right" valign="middle"><strong><?php echo number_format($total_tax_amount, 2, '.','');?></strong></td>
        </tr>
		 <tr>
          <td align="right" valign="middle">Extra charge Amount:</td>
          <td align="right" valign="middle"><?php if($charge_sum!=0){ echo number_format($charge_sum, 2, '.','');}else{ echo number_format($charge_sum=0, 2, '.',''); }?></td>
        </tr>
        <tr>
          <td align="right" valign="middle"><strong>Total Amount:</strong></td>
          <td align="right" valign="middle"><strong>
            <?php 
if(isset($posGAMT) && $posGAMT !=""){
$grand_total_amount= $total+$charge_sum+$posGAMT;
} 
else {
$grand_total_amount= $total+$charge_sum+$adjstAmt; 
}
echo 'INR '.number_format($grand_total_amount, 2);	
?>
            </strong></td>
        </tr>
        <?php 
				$d=0;
				$discount_details=$this->unit_class_model->all_discount_group_details($_GET['group_id']);
				$lo=0;
				if(isset($discount_details)){
                foreach($discount_details as $discount){                	
                	$discount_amount=$discount->discount_amount;
					$lo+=$discount_amount;
					$glo1=$lo;
					++$d;
				}
				
				}
				 if(isset($m) && $m){
			//$sum=0;
		}else{
			$m=0;
		}
				?>
        <tr>
          <td align="right" valign="middle">Discount:</td>
          <td align="right" valign="middle"><?php echo $glo1;?><?php echo "(". $d." )" ?></td>
        </tr> 
		<tr>
          <td align="right" valign="middle">Adjustment amount:</td>
          <td align="right" valign="middle"><?php echo $adjstAmt;?> <?php echo "(". $m." )" ?></td>
        </tr>
		<tr>
          <td align="right">Total Paid Amount:</td>
          <td align="right" valign="middle"><?php //$payAmount = $grand_total_amount; 
			  echo number_format($paid, 2);?></td>
        </tr>
        <tr>
          <td align="right">Total Payable Amount:</td>
          <td align="right" valign="middle"><?php $payAmount = $grand_total_amount; 
			  echo number_format($payAmount, 2);?></td>
        </tr>
		
        <tr>
          <td align="right" valign="middle"><span style="font-size:13px; font-weight:700; color:#8876A8;">Total Due:</span></td>
          <td align="right" valign="middle"><?php 
$td = $payAmount-$paid-$glo1;
$paid_c = number_format($td, 2);
if($paid_c != 0){ ?>
            <span style="font-size:13px; font-weight:700; color:#F65656;">INR <?php echo number_format($td,2);?></span>
            <?php	
}
else{ ?>
            <span style="font-size:13px; font-weight:700; color:#7FBA67;">Full Paid</span>
            <?php }?></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td style="padding:100px 0 0;" colspan="2"></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center" width="35%">______________________________<br />
            Authorized Signature </td>
        </tr>
        <tr>
          <td width="65%">&nbsp;</td>
          <td align="center" width="35%"><?php   // Showing the Hotel Name from Session
                
        $hotel_n = $this->dashboard_model->get_hotel_name($this->session->userdata('user_hotel'));
        if(isset($hotel_n->hotel_name) && $hotel_n->hotel_name)
            echo 'For, '.$hotel_n->hotel_name;
    ?></td>
        </tr>
      </table>
      <hr style="background: #00C5CD; border: none; height: 1px; margin-top:10px; width:100%;">
      <span style="font-size:9px; font-weight:400; color:#939292; display:block;"> Powered by HOTEL OBJECTS, from Theinfinitytree Techno Solutions PVT. LTD.  Thank you for your stay! </span>
		<?php if($chk->ft_text!=NULL){
					echo "</br>"."* ".$chk->GBft_text." *";
				}
				?>
    </div>
