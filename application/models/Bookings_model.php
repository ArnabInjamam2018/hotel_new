<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bookings_model extends CI_Model {

	/* Select rooms hotel wise */
	function all_rooms_hotelwiseSql($hotel_id){
		
		$sql = "SELECT clean_id,
				  `room_features`,`room_status`,`room_bed`,hk.status_name, hk.color_primary, `room_id`,`room_no`, rm.`unit_id`, ty.unit_type, ty.unit_class, ty.unit_name, ty.default_occupancy, ty.max_occupancy
				FROM
				  `hotel_room` as rm
				  RIGHT JOIN hotel_unit_type as ty ON ty.id = rm.`unit_id`
				  LEFT JOIN hotel_housekeeping_status as hk ON hk.status_id = rm.clean_id
				WHERE
				  rm.`hotel_id` = ".$this->session->userdata('user_hotel')."
				ORDER BY 
				  ty.unit_name desc,cast(`room_no` as unsigned)";
		
		$query=$this->db1->query($sql);
		
		if($query->num_rows()>0){
			 return $query->result();
		}else{
			return false;
		}
	}
	
	function all_rooms_hotelwise($hotel_id){
		$this->db1->where('hotel_id',$hotel_id);   
			//$this->db1->where('user_id',$this->session->userdata('user_id'));
       // $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		//$this->db1->where('user_id',$this->session->userdata('user_id'));        
		$this->db1->order_by('room_no', 'ASC');
		$query = $this->db1->get(TABLE_PRE.'room');
		if($query->num_rows()>0){
			 return $query->result();
		}else{
			return false;
		}
	}
	
	function all_rooms_unitwise_new($id){
		$this->db1->where('unit_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		$this->db1->order_by('room_no', 'ASC');
		$query = $this->db1->get(TABLE_PRE.'room');
		if($query->num_rows()>0){
			return $query->result();
		}else{
			return false;
		}
	}	
	
	
	
    function all_rooms_hotelwise_limit($hotel_id,$bed){
        $this->db1->where('hotel_id',$hotel_id);
		//	$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('room_bed',$bed);
        // $this->db1->where('unit_id',0);
        $this->db1->order_by('unit_id', 'ASC');
        $query = $this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return $query->result();
        }
    }

    function all_rooms_hotelwise_price($hotel_id,$p1,$p2){
        $this->db1->where('hotel_id',$hotel_id);
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('room_rent >=', $p1);
        $this->db1->where('room_rent <=', $p2);
        // $this->db1->where('unit_id',0);
        $this->db1->order_by('unit_id', 'ASC');
        $query = $this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return $query->result();
        }
    }

    function get_unit_name($unit_id){
        $this->db1->where('id',$unit_id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		//$this->db1->where('user_id',$this->session->userdata('user_id'));  
        //$this->db1->order_by('unit_id', 'ASC');
        $query = $this->db1->get(TABLE_PRE.'unit_type');
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
    }

    function all_units(){
        
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query = $this->db1->get(TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
    function all_units_limit($id){
        $this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		//$this->db1->where('user_id',$this->session->userdata('user_id'));  
        //$this->db1->order_by('unit_id', 'ASC');
        $query = $this->db1->get(TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return $query->result();
        }
    }
    function all_unitstype_limit($id){
        $this->db1->where('unit_type',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		//$this->db1->where('user_id',$this->session->userdata('user_id'));  
        //$this->db1->order_by('unit_id', 'ASC');
        $query = $this->db1->get(TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return $query->result();
        }
    }
    function all_unitsclass_limit($id){
        $this->db1->where('unit_class',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		//$this->db1->where('user_id',$this->session->userdata('user_id'));  	
        //$this->db1->order_by('unit_id', 'ASC');
        $query = $this->db1->get(TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return $query->result();
        }
    }





    function all_rooms_onchange_hotelwise($hotel_id,$capacity){
		$this->db1->where('room_bed',$capacity);
		$this->db1->where('hotel_id',$hotel_id);
		//$this->db1->where('user_id',$this->session->userdata('user_id')); 
		$query = $this->db1->get(TABLE_PRE.'room');
		if($query->num_rows()>0){
			 return $query->result();
		}else{
			return false;
		}
	}
    function all_rooms_onchange_hotelwise_unit($hotel_id,$unit_id){
        $this->db1->where('unit_id',$unit_id);
       // $this->db1->where('hotel_id',$hotel_id);
        $query = $this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function room_hotelwise($hotel_id,$room_id){
		$this->db1->where('hotel_id',$hotel_id);
		$this->db1->where('room_id',$room_id);
		$query = $this->db1->get(TABLE_PRE.'room');
		if($query->num_rows()>0){
			 return $query->result();
		}else{
			return false;
		}
	}
	
	function broker_hotelwise($hotel_id){
		$this->db1->where('hotel_id',$hotel_id);
		$query = $this->db1->get(TABLE_PRE.'broker');
		if($query->num_rows()>0){
			 return $query->result();
		}else{
			return false;
		}
	}
    function channel_hotelwise($hotel_id){
        $this->db1->where('hotel_id',$hotel_id);
        $query = $this->db1->get(TABLE_PRE.'channel');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	function add_new_room($data,$data1){
		/*$maxID="select max(booking_id) as MID from hotel_bookings";
		$query=$this->db1->query($maxID);
		$a=$query->row();
		$data['booking_id']=$a->MID;*/
		
		//echo $query;exit;
		$this->db1->insert(TABLE_PRE.'bookingbk',$data1);
		$query=$this->db1->insert(TABLE_PRE.'bookings',$data);
		$last_id = $this->db1->insert_id();
        if($query){
            return $last_id;
        }else{
            return false;
        }
	}
	function add_new_room_enq($data){
		/*$maxID="select max(booking_id) as MID from hotel_bookings";
		$query=$this->db1->query($maxID);
		$a=$query->row();
		$data['booking_id']=$a->MID;*/
		
		//echo $query;exit;
		$query=$this->db1->insert(TABLE_PRE.'bookings_enq',$data);
		$last_id = $this->db1->insert_id();
        if($query){
            return $last_id;
        }else{
            return false;
        }
	}	
	function add_new_room2($data){
		$this->db1->where('booking_id',$data['booking_id']);
	    $query=$this->db1->update(TABLE_PRE.'bookings',$data); 
	    
        if($query){
            return true;
        }else{
            return false;
        }
	}
	function add_roomupdate($data){
		
	   $this->db1->where('booking_id',$data['booking_id']);
	    $query=$this->db1->update(TABLE_PRE.'bookingbk',$data); 
	    
        if($query){
            return true;
        }else{
            return false;
        }
	}
	function add_new_room2_enq($data){
		$this->db1->where('booking_id',$data['booking_id']);
	    $query=$this->db1->update(TABLE_PRE.'bookings_enq',$data); 
        if($query){
            return true;
        }else{
            return false;
        }
	}	
	function add_new_room4($data){
		$query=$this->db1->insert(TABLE_PRE.'transactions',$data);
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
	function add_new_room5($data,$bookings_id){
		
	/*	echo "bookings_id".$bookings_id;
		echo "<pre>";
		print_r($data);
		exit();
	*/	
		$this->db1->where('booking_id',$bookings_id);
	    $query=$this->db1->update(TABLE_PRE.'bookings',$data); 
		$abst = $this->dashboard_model->get_date_booking($bookings_id);
	if($abst->booking_status_id==5){
		$stat='checkin';
	}else if($abst->booking_status_id==2){
		$stat='waiting';
	}
		//$stat='checkin';
	$this->dashboard_model->update_checkin_status_stay_guest($bookings_id,$stat);
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
	function add_new_room5_enq($data){
		$this->db1->where('booking_id',$data['booking_id']);
	    $query=$this->db1->update(TABLE_PRE.'bookings_enq',$data); 
        if($query){
            return true;
        }else{
            return false;
        }
	}	
    function add_new_room_broker($data){
        $this->db1->where('booking_id',$data['booking_id']);
        $query=$this->db1->update(TABLE_PRE.'bookings',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }
    function add_new_room_channel($data){
        $this->db1->where('booking_id',$data['booking_id']);
        $query=$this->db1->update(TABLE_PRE.'bookings',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }
	/* For advance to Confirmed After payment */
	function change_book_id($id,$data){
		$this->db1->where('booking_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	    $query=$this->db1->update(TABLE_PRE.'bookings',$data); 
        if($query){
            return true;
        }else{
            return false;
        }
	}
	// For temp hold and advance change to pending
	function change_book_id2($id,$data){
		$this->db1->where('booking_id',$id);
	    $query=$this->db1->update(TABLE_PRE.'bookings',$data); 
        if($query){
            return true;
        }else{
            return false;
        }
	}
	function fetch_color($id){
		$this->db1->select('*');
        $this->db1->where('status_id=',$id);
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get('booking_status_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	
	function all_rooms_bookings_event($hotel_id,$start,$end)
	{
		/* $sql = "SELECT * FROM ".TABLE_PRE."bookings A
		 JOIN booking_status_type B ON A.booking_status_id = B.status_id
		 WHERE (A.cust_end_date BETWEEN '".$start."' AND '".$end."') AND (A.cust_from_date BETWEEN '".$start."' AND '".$end."') AND A.hotel_id='".$hotel_id."' AND A.booking_status_id!='7'";*/		 
		/*$sql = "SELECT * FROM ".TABLE_PRE."bookings WHERE NOT ((cust_end_date <= '".$start."') OR (cust_from_date >= '".$end."')) AND hotel_id='".$hotel_id."'";*/
		
		$sql = "SELECT A.booking_status_id,A.booking_taking_time,A.booking_id,A.cust_from_date,A.cust_end_date,A.group_id,A.room_rent_tax_details,A.guest_id,A.cust_name,A.cust_contact_no,A.cust_payment_initial,A.booking_source,A.booking_source_name,A.room_id,A.booking_status_id_secondary,A.checkin_time,A.checkout_time,A.comment,A.preference,B.bar_color_code,B.body_color_code,B.booking_status FROM ".TABLE_PRE."bookings A
		 JOIN booking_status_type B ON A.booking_status_id = B.status_id
		 WHERE (A.cust_end_date BETWEEN '".$start."' AND '".$end."') AND (A.cust_from_date BETWEEN '".$start."' AND '".$end."') AND A.hotel_id='".$hotel_id."' AND A.booking_status_id!='7'";

		$query=$this->db1->query($sql);
		 if($query->num_rows()>0){
			 return $query->result();
		}else{
			return false;
		}
	}
	
	function room_edit_hotelwise($booking_id){
		$sql = "SELECT * FROM ".TABLE_PRE."bookings A 
					JOIN ".TABLE_PRE."room B ON A.room_id = B.room_id   WHERE  A.booking_id='".$booking_id."'"; 
		 $query=$this->db1->query($sql);
		if($query->num_rows()>0){
			 return $query->result();
		}else{
			return false;
		}
	}
	
	function add_edit_room($data){
		$this->db1->where('booking_id',$data['booking_id']);
	    $query=$this->db1->update(TABLE_PRE.'bookings',$data); 
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
	function delete_booking_room($booking_id){
		
		$sql = "DELETE  FROM ".TABLE_PRE."bookings WHERE  booking_id='".$booking_id."'"; 
		 $query=$this->db1->query($sql);		
        if($query){
            return true;
        }else{
            return false;
        }
	}
	function update_rooms_bookings_event($booking_id,$start,$end,$start_ac,$end_ac,$ratio,$days_updated,$sum){
		
		$sql = "UPDATE ".TABLE_PRE."bookings SET room_rent_sum_total='".$sum."', room_rent_total_amount=room_rent_total_amount*'".$ratio."', room_rent_tax_amount=room_rent_tax_amount*'".$ratio."', stay_days='".$days_updated."' , cust_from_date='".$start."',cust_end_date='".$end."' , cust_from_date_actual='".$start_ac."' , cust_end_date_actual='".$end_ac."' WHERE  booking_id='".$booking_id."'";
		$query=$this->db1->query($sql);		
        if($query){
            return 'Booking Dates and Amount Updated';
        }else{
            return 'failed';
        }
	}
	function delete_line_item($booking_id,$tempdate){
		
		$this->db1->where('booking_id',$booking_id);
		$this->db1->where('date',$tempdate);
         $query=$this->db1->delete('booking_charge_line_item');
         if($query){
             return true;
         }else{
             return false;
         }
	}
	
	function delete_line_item_group($booking_id,$groupID){
		
		$this->db1->where('booking_id',$booking_id);
		$this->db1->where('group_id',$groupID);
         $query=$this->db1->delete('booking_charge_line_item');
         if($query){
             return true;
         }else{
             return false;
         }
	}
	
	
	function insert_line_item($lineitemdetails){
		
		$query=$this->db1->insert('booking_charge_line_item',$lineitemdetails);
        if($query){
            return true;
        }else{
            return false;
        }	

	}
    function get_stay_days($id){

        $this->db1->select('*');
        $this->db1->where('booking_id=',$id);
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get('hotel_bookings');
        if($query->num_rows()==1){
            return $query->result();
        }else{
            return false;
        }

    }
	
	function count_rooms_bookings_event($booking_id,$room_id,$start,$end){
		
		$sql = "SELECT * FROM ".TABLE_PRE."bookings WHERE NOT ((cust_end_date <= '".$start."') OR (cust_from_date >= '".$end."'))
				AND booking_id <> '".$booking_id."' AND room_id = '".$room_id."' AND booking_status_id !='7' "; 
		$query=$this->db1->query($sql);	
        if($query){
            return $query->num_rows();
        }else{
            return false;
        }
	}
	
/*	function update_rooms_bookings_move($booking_id,$room_id,$start,$end,$start_ac,$end_ac,$sum,$amt,$tax){
		$sql = "UPDATE ".TABLE_PRE."bookings SET room_rent_tax_amount='".$tax."',base_room_rent='".$amt."',room_rent_sum_total='".$sum."',room_rent_total_amount='".$sum."',cust_from_date='".$start."',cust_end_date='".$end."', room_id='".$room_id."' , cust_from_date_actual='".$start_ac."' , cust_end_date_actual='".$end_ac."' WHERE  booking_id='".$booking_id."'";
		$query=$this->db1->query($sql);		
        if($query){
            return true;
        }else{
            return false;
        }
	}
	*/
	
		function update_rooms_bookings_move($booking_id,$room_id,$start,$end,$start_ac,$end_ac,$sum,$amt,$tax,$meal_price,$meal_price_modifier){

		$sql = "UPDATE ".TABLE_PRE."bookings SET room_rent_tax_amount='".$tax."',base_room_rent='".$amt."',room_rent_sum_total='".$sum."',room_rent_total_amount='".$sum."',cust_from_date='".$start."',cust_end_date='".$end."', room_id='".$room_id."' , cust_from_date_actual='".$start_ac."' , cust_end_date_actual='".$end_ac."' , food_plan_price='".$meal_price."' , food_plan_mod_type='".$meal_price_modifier."' WHERE  booking_id='".$booking_id."'";

		$query=$this->db1->query($sql);		

        if($query){

            return true;

        }else{

            return false;

        }

	}
	function  add_guest($data){
        $query=$this->db1->insert(TABLE_PRE.'guest',$data);
		$last_id = $this->db1->insert_id();
        if($query){
            return $last_id;
        }else{
            return false;
        }
    }
	
	function get_guest_state_details($guest_id){

		 $sql = "SELECT * FROM ".TABLE_PRE."guest where g_id='".$guest_id."'";  

		 $query=$this->db1->query($sql);

		

		 if($query->num_rows()>0){

			 return $query->row();

		}else{

			return false;

		}

	

	}
	function get_guest($keyword){
		//$sql = "SELECT * FROM ".TABLE_PRE."guest where g_name like'".$keyword."%'";  
		//$query=$this->db1->query($sql);
		$this->db1->select('*');    
        $this->db1->from(TABLE_PRE.'guest');
        $this->db1->join(TABLE_PRE.'bookings', TABLE_PRE.'guest.g_id = '.TABLE_PRE.'bookings.guest_id', 'left');
        $this->db1->join(TABLE_PRE.'room', TABLE_PRE.'bookings.room_id = '.TABLE_PRE.'room.room_id', 'left');
        $this->db1->like('g_name', $keyword);
        $this->db1->distinct();
        $this->db1->group_by('g_id');
        $this->db1->order_by('booking_id', 'ASC');

		$query = $this->db1->get();
		
		 if($query->num_rows()>0){
			 return $query->result_array();
		}else{
			return false;
		}
	
	}

    function no_visit($id){
        $sql="select * from hotel_bookings where guest_id='".$id."' and booking_status_id !='7' ";
        $query=$this->db1->query($sql);
        return $query->num_rows();

    }
    function spent($id){
          $sql="select room_rent_sum_total from hotel_bookings where guest_id='".$id."' and booking_status_id !='7' ";
        $query=$this->db1->query($sql);
        return $query->row_array();


    }
	
	
	function get_guest_details($guest_id){
		 $sql = "SELECT * FROM ".TABLE_PRE."guest where g_id='".$guest_id."'";  
		 $query=$this->db1->query($sql);
		
		 if($query->num_rows()>0){
			 return $query->result();
		}else{
			return false;
		}
	
	}
	
	
	function get_broker_details($b_id){
		 $sql = "SELECT * FROM ".TABLE_PRE."broker where b_id='".$b_id."'";  
		 $query=$this->db1->query($sql);
		
		 if($query->num_rows()>0){
			 return $query->result();
		}else{
			return false;
		}
	
	}
    function get_channel_details($b_id){
        $sql = "SELECT * FROM ".TABLE_PRE."channel where channel_id='".$b_id."'";
        $query=$this->db1->query($sql);

        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

    }

	function get_booking_details($book_id)
	{
		$this->db1->where('booking_id',$book_id);
		$query = $this->db1->get(TABLE_PRE.'bookings');

		if($query->num_rows()>0){
			return $query->result();	 
		}
		else{
			return false;
		}
		
	}
	function get_booking_details_enq($book_id)
	{
		$this->db1->where('booking_id',$book_id);
		$query = $this->db1->get(TABLE_PRE.'bookings_enq');

		if($query->num_rows()>0){
			return $query->result();	 
		}
		else{
			return false;
		}
		
	}	
    function get_booking_details_row($book_id)
    {
        $this->db1->where('booking_id',$book_id);
        $query = $this->db1->get(TABLE_PRE.'bookings');

        if($query->num_rows()>0){
			
			//$dataNB['bookings_details'] = $query->row(); 
		 return $query->row(); 
			}
           //    
      
        else{
            return false;
        }
        
    }
	
	function booking_line_item_details($booking_id){
		
		$this->db1->select('*');
		$this->db1->where('booking_id',$booking_id);
		$this->db1->limit(1);
		$query = $this->db1->get('booking_charge_line_item');
		if($query->num_rows()>0){
			
		 return $query->row();
		 
			}
         
        else{
            return false;
        }
        
		
	}
	
	function get_booking_line_charge_item_details($booking_id){
		
		$this->db1->select('*');
		$this->db1->where('booking_id',$booking_id);
	//	$this->db1->limit(1);
		$query = $this->db1->get('booking_charge_line_item');
		if($query->num_rows()>0){
			
		 return $query->result_array();
		 
			}
         
        else{
            return false;
        }
        
		
	}

	function get_transaction_details($book_id)
	{
		$this->db1->where('t_booking_id',$book_id);
		$query = $this->db1->get(TABLE_PRE.'transactions');

		if($query->num_rows()>0){
			return $query->result();	 
		}
		else{
			return false;
		}
	}

	function get_transaction_total_amount($book_id)
	{
		$this->db1->select_sum('t_amount');
		$this->db1->where('t_booking_id',$book_id);
		$query = $this->db1->get(TABLE_PRE.'transactions');

		if($query->num_rows()>0){
			return $query->result();	 
		}
		else{
			return false;
		}
	}

	function get_payment_details($book_id)
	{
		$this->db1->select('*');
		$this->db1->from(TABLE_PRE.'room');
		$this->db1->join(TABLE_PRE.'bookings', 'hotel_room.room_id = hotel_bookings.room_id');
		$this->db1->where('booking_id',$book_id);
		$query = $this->db1->get();
		
		if($query->num_rows()>0){
			return $query->result();	 
		}
		else{
			return false;
		}
	}

	function get_total_payment($booking_id)
	{
		
		$this->db1->where('t_booking_id',$booking_id);
		$this->db1->where('t_status','Done');
		$this->db1->select_sum('t_amount');
		$query = $this->db1->get(TABLE_PRE.'transactions');

		if($query->num_rows()>0){
			return $query->result();	 
		}
		else{
			return false;
		}
	}

	function get_total_payment_gp($booking_id){
		$this->db1->where('t_group_id',$booking_id);
		$this->db1->select_sum('t_amount');
		$query = $this->db1->get(TABLE_PRE.'transactions');
		if($query->num_rows()>0){
			return $query->result();	 
		}
		else{
			return false;
		}
	}	


	function get_total_bill_gp($booking_id){
		$this->db1->where('group_booking_id',$booking_id);
		$this->db1->select('price');
		$query = $this->db1->get(TABLE_PRE.'groups_bookings_details');
		if($query->num_rows()>0){
			return $query->result();	 
		}
		else{
			return false;
		}
	}

	function getDetails($booking_id){
		$this->db1->where('id',$booking_id);
		$this->db1->select('*');
		$query = $this->db1->get(TABLE_PRE.'group');
		if($query->num_rows()>0){
			return $query->row();	 
		}
		else{
			return false;
		}
	}

	function all_pos_booking($booking_id , $type){
        $sql="select * from hotel_pos where booking_id='".$booking_id."' and booking_type='".$type."'";
         $query=$this->db1->query($sql);
          return $query->result();
        

    }
	
	
	
    function get_admin($admin_id)
    {
        $this->db1->where('admin_id',$admin_id);
        $query=$this->db1->get(TABLE_PRE.'admin_details');
        if($query->num_rows()==1){

            return $query->result();
        }else{
            return false;
        }



    }
    function send_sms($r_message){
			$hotel_id=$this->session->userdata('user_hotel');
        $details=$this->get_hotel_contact_details($hotel_id);
		if(isset($details) && $details)	{
	 //  foreach($details as $det){
             $receiver=$details['hotel_owner_mobile'];
            //echo $r_message;
	 
     //   }
       // exit();


        // Authorisation details.
        $username = "samrat360@gmail.com";
        $hash = "2c806042ceba707a9ba592295dd8a6aceda01083";

        // Config variables. Consult http://api.textlocal.in/docs for more info.
        $test = "0";

        // Data for text message. This is the text message data.
        $sender = "TXTLCL"; // This is who the message appears to be from.
        $numbers = $receiver; // A single number or a comma-seperated list of numbers
        //$message = "This is a test message from the HO. Hyegeche configure. Do Anondo. ";
        // 612 chars or less
        // A single number or a comma-seperated list of numbers
        $message = urlencode($r_message);
        $data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test;
        $ch = curl_init('http://api.textlocal.in/send/?');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $result = curl_exec($ch); // This is the result from the API
        curl_close($ch);
		}
    }

    function send_sms_guest($number,$r_message){
		
        // Authorisation details.
        $username = "samrat360@gmail.com";
        $hash = "2c806042ceba707a9ba592295dd8a6aceda01083";

        // Config variables. Consult http://api.textlocal.in/docs for more info.
        $test = "0";

        // Data for text message. This is the text message data.
        $sender = "TXTLCL"; // This is who the message appears to be from.
        $numbers = $number; // A single number or a comma-seperated list of numbers
        //$message = "This is a test message from the HO. Hyegeche configure. Do Anondo. ";
        // 612 chars or less
        // A single number or a comma-seperated list of numbers
        $message = urlencode($r_message);
        $data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test;
        $ch = curl_init('http://api.textlocal.in/send/?');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch); // This is the result from the API
		echo $result;
        curl_close($ch);
		
    }



    function get_total_amount($booking_id)
    {

        $this->db1->where('booking_id',$booking_id);
        $this->db1->select('*');
        $query = $this->db1->get(TABLE_PRE.'bookings');

        if($query->num_rows()>0){
            return $query->result();
        }
        else{
            return false;
        }
    }
	
	function get_total_amountNB($booking_id)
    {

        $this->db1->where('booking_id',$booking_id);
        $this->db1->select('*');
        $query = $this->db1->get(TABLE_PRE.'bookings');

        if($query->num_rows()>0){
            return $query->row();
        }
        else{
            return false;
        }
    }
	
	function rate($id){
		$this->db1->where('room_id',$id);
		 $query=$this->db1->query($sql);
		
		 if($query->num_rows()==1){
			 return $query->result();
		}else{
			return false;
		}
	}
	
	function room_tax($hotel_id){
		$this->db1->where('hotel_id',$hotel_id);
		 $query = $this->db1->get(TABLE_PRE.'hotel_tax_details');
		
		 if($query->num_rows()>=1){
			 return $query->result();
		}else{
			return false;
		}
		
	}

	function get_hotel_contact_details($hotel_id){
		$this->db1->where('hotel_id',$hotel_id);
		$query = $this->db1->get(TABLE_PRE.'hotel_contact_details');
		if($query->num_rows()>0){
			return $query->row_array();	 
		}
		else{
			return false;
		}
		
	}

	function get_hotel_logo($hotel_id){
		$this->db1->where('hotel_id',$hotel_id);
		$query = $this->db1->get(TABLE_PRE.'hotel_details');
		if($query->num_rows()>0){
			return $query->row_array();	 
		}
		else{
			return false;
		}
		
	}

    function  get_clean_status($id){
        $this->db1->where('room_id',$id);
        $query = $this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->row();
        }
        else{
            return false;
        }
    }
	
    function get_tax_details($hotel_id){
		$this->db1->select('*');
		$this->db1->where('hotel_id',$hotel_id);
		$query=$this->db1->get(TABLE_PRE.'hotel_billing_setting');
		if($query->num_rows()>0){
			return $query->result();
		} else {
			return false;
		}
	}
	
	
	function service_tax($hotel_id){
		$this->db1->select('*');
        $this->db1->where('hotel_id',$hotel_id);
		$query=$this->db1->get(TABLE_PRE.'hotel_tax_details');
		 if($query->num_rows()> 0){
			 return $query->row();
		}else{
			return false;
		}
		
	}
	
    function get_food_plan(){
        $query = $this->db1->get(TABLE_PRE.'food_plan');
        if($query){
             return $query->result();
        }else{
            return false;
        }
    }	
	 function get_meal_plan(){
		$this->db1->select('*');
        $query = $this->db1->get(TABLE_PRE.'meal_plan_cat');
        if($query){
             return $query->result();
        }else{
            return false;
        }
    }

    function fetch_food_plan($plan_id){
        $this->db1->where('id',$plan_id);
        $query = $this->db1->get(TABLE_PRE.'food_plan');
        if($query){
             return $query->row();
        }else{
            return false;
        }
    }


    function delete_invoice_previous($booking_id){

       /* $this->db1->join('invoice_master','invoice_line_itemid.master_id=invoice_master.id');
        $this->db1->where('invoice_master.related_id', $booking_id);
     return  $this->db1->delete('invoice_line_itemid'); */

    $sql="delete a,b FROM invoice_master as a left join invoice_line_itemid as b on a.id=b.master_id WHERE a.related_id='".$booking_id."'";

    $this->db1->query($sql);

    }

    function insert_invoice_master($data,$booking_id){
        $query=$this->db1->insert('invoice_master',$data);
        $last_id = $this->db1->insert_id();
        if($query){
            return $last_id;
        }else{
            return false;
        }
    }
	
	function insert_invoice_master_grp($data,$booking_id){
        $query = $this->db1->insert('invoice_master',$data);
        $last_id = $this->db1->insert_id();
        if($query){
            return $last_id;
        }
		else{
            return false;
        }
    }

    function insert_invoice_line($data,$booking_id){
        $query=$this->db1->insert('invoice_line_itemid',$data);
        $last_id = $this->db1->insert_id();
        if($query){
            return $last_id;
        }else{
            return false;
        }
    }

    function all_folio($booking_id){
        $sql="SELECT * FROM invoice_line_itemid left join invoice_master on invoice_line_itemid.master_id=invoice_master.id where invoice_master.related_id='".$booking_id."'";
        $query=$this->db1->query($sql);
        if($query){
            return $query->result();
        }
		else{
            return false;
        } 
    }

    function all_folio_grp($booking_id){
        $sql="SELECT * FROM invoice_line_itemid left join invoice_master on invoice_line_itemid.master_id=invoice_master.id where invoice_master.related_id='".$booking_id."' group by master_id";
        $query=$this->db1->query($sql);
         if($query){
            return $query->result();
        }else{
            return false;
        } 
    }
    function get_group_booking_details_by_date($date,$id){
        $this->db1->select('id,guestID');
        $this->db1->from('hotel_group');
        $this->db1->where('start_date <=',$date);
        $this->db1->where('end_date >=',$date);
        $this->db1->where('hotel_id = ',$id);
        $query=$this->db1->get();
        //echo $this->db1->last_query();exit;
        if($query->num_rows() > 0 )
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    function get_group_booking_status($g_id,$hid=NULL){
        $this->db1->select('booking_status_id,hotel_id');
        $this->db1->from('hotel_bookings');
        $this->db1->where('group_id',$g_id);
        $query=$this->db1->get();
        if($query->num_rows() > 0 ){
            return $query->row();
        } else {
            return false;
        }
    }

    function get_group_booking_room($g_id){
        $this->db1->select('roomNO');
        $this->db1->from('hotel_groups_bookings_details');
        $this->db1->where('group_booking_id',$g_id);
        $query=$this->db1->get();
        if($query->num_rows() > 0 ){
            return $query->result();
        } else {
            return false;
        }
    }

	function updatePos($id){
		$data = array(
            'total_due' => 0
        );
		$this->db1->where('booking_id',$id);
		$this->db1->where('booking_type', 'gb');
	    $query=$this->db1->update(TABLE_PRE.'pos',$data); 
        if($query){
            return true;
        }else{
            return false;
        }
	}

	function updatePosPaidAmount($val1,$val2){
		$data = array(
            'total_paid' => $val2,
			'total_paid_pos' => $val2
        );
		$this->db1->where('invoice_number',$val1);
	    $query=$this->db1->update(TABLE_PRE.'pos',$data); 
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
    function send_mail($mail,$msg,$hotelName){
         $subject = "Mail from ".$hotelName; 
         $header = "From:smallick415@gmail.com \r\n";
         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";
		 //echo $mail;
		 //exit;
         @mail($mail,$subject,$msg,$header);		
	}	
	
	function get_invoice_settings(){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query = $this->db1->get(TABLE_PRE.'settings');
			 if($query){
				 return $query->row();
			 }
			 else{
				 return false;
			 }		 
	}	
	
	function hotelWiseRoomDetails($hotel_id,$room_id){
		$this->db1->where('hotel_id',$hotel_id);
		$this->db1->where('room_id',$room_id);
		$query = $this->db1->get(TABLE_PRE.'room');
		if($query->num_rows()>0){
			 return $query->row();
		}else{
			return false;
		}
	}

	function hotelBookingsDetails($hotel_id,$room_id){
		$this->db1->where('hotel_id',$hotel_id);
		$this->db1->where('booking_id',$room_id);
		$this->db1->limit(1);
		$query = $this->db1->get('booking_charge_line_item');
		if($query->num_rows()>0){
			 return $query->row();
		}else{
			return false;
		}
	}
	
	function hotelBookingsDetails_resize($hotel_id,$room_id){
		$this->db1->where('hotel_id',$hotel_id);
		$this->db1->where('booking_id',$room_id);
		//$this->db1->limit(1);
		$query = $this->db1->get('booking_charge_line_item');
		if($query->num_rows()>0){
			 return $query->result();
		}else{
			return false;
		}
	}  
	function update_line_item($booking_id,$lineitemarray){  
		
		$this->db1->where('booking_id',$booking_id);
		$query = $this->db1->update('booking_charge_line_item', $lineitemarray);
		
		
	}
	function new_update_line_item($booking_id,$lineitemarray,$bcli_id){  
		
		$this->db1->where('booking_id',$booking_id);
		$this->db1->where('bcli_id',$bcli_id);
		$query = $this->db1->update('booking_charge_line_item', $lineitemarray);
		
		
	} 
	function update_modifier($booking_id,$lineitemarray){  
		
		$this->db1->where('booking_id',$booking_id);
		$query = $this->db1->update('hotel_bookings', $lineitemarray);
		if($query){

                return "OK";
              }
			  else{
				  
				  return "Not OK";
				  
			  }
		
	}
	
 
	function getUnitType($room_id){
		 $sql = "SELECT unit_name FROM ".TABLE_PRE."unit_type where id='".$room_id."'";  
		 $query=$this->db1->query($sql);	
		 if($query->num_rows()>0){
			 return $query->row();
		}else{
			return false;
		}
	
	}

	function roomDeleteFromGroup($bookingID,$groupdetailsID,$groupID,$guest){
		$this->db1->where('booking_id',$bookingID);
        $this->db1->delete(TABLE_PRE.'bookings');	
		$this->db1->where('id',$groupdetailsID);
        $this->db1->delete(TABLE_PRE.'groups_bookings_details');			
		$this->db1->where('booking_id',$bookingID);
		$this->db1->where('group_id',$groupID);
        $this->db1->delete('booking_charge_line_item');
		$sql = "UPDATE ".TABLE_PRE."group SET number_room = number_room - 1 , number_guest=number_guest - '".$guest."' where id='".$groupID."'";  
		$query=$this->db1->query($sql);	
        if($query){
            return true;
        }else{
            return false;
        }
	}

	function update_group_bookings_move($booking_id,$groupID,$start,$end,$start_ac,$end_ac){
		list($sy,$sm,$sd) = explode('-',$start_ac);
		list($ey,$em,$ed) = explode('-',$end_ac);
		$start_gp = $sm.'/'.$sd.'/'.$sy;
		$end_gp = $em.'/'.$ed.'/'.$ey;
		$sql = "UPDATE ".TABLE_PRE."bookings SET cust_from_date='".$start."',cust_end_date='".$end."', cust_from_date_actual='".$start_ac."' , cust_end_date_actual='".$end_ac."' WHERE  group_id='".$groupID."'";
		$query=$this->db1->query($sql);		
        if($query){
		   $sql_gp="UPDATE ".TABLE_PRE."group SET start_date='".$start_gp."',end_date='".$end_gp."' WHERE  id='".$groupID."'";
		   $this->db1->query($sql_gp);
		   $sql_gp_details="UPDATE ".TABLE_PRE."groups_bookings_details SET startDate='".$start_gp."',endDate='".$end_gp."' WHERE  group_booking_id='".$groupID."'";
		   $this->db1->query($sql_gp_details);		   
           return true;
        }else{
           return false;
        }
	}

	function roomRemoveFromGroup($bookingID,$groupdetailsID,$groupID,$guest,$trr,$tfi, $totalTax ,$st,$sc,$lt,$sd){
		$totaltrr = $trr*$sd;
		$totaltfi = $tfi*$sd;
		$tot = $trr*$sd + $totalTax*$sd;
		$totTax = $totalTax*$sd;
		$sql_gp = "UPDATE ".TABLE_PRE."bookings SET group_id = '0' , room_rent_tax_details = 'Service tax + Service Charge + Luxury Tax' , base_room_rent = '".$trr."' , room_rent_total_amount = '".$totaltrr."' , room_rent_tax_amount = '".$totTax."' , room_rent_sum_total = '".$tot."' , service_tax = '".$st."' , service_charge = '".$sc."' , luxury_tax = '".$lt."' , food_plan_price = '".$totaltfi."', food_plans='7'  where booking_id='".$bookingID."'"; 
      
        //exit;		
		$this->db1->query($sql_gp);
		$this->db1->where('id',$groupdetailsID);
        $this->db1->delete(TABLE_PRE.'groups_bookings_details');			
		$sql = "UPDATE ".TABLE_PRE."group SET number_room = number_room - 1 , number_guest=number_guest - '".$guest."' where id='".$groupID."'";  
		$query=$this->db1->query($sql);	
		
		
		// line charge item update..
		
		$data = array(
            'group_id' => 0
        );
		
		
		$this->db1->where('booking_id',$bookingID);
	    $query_grp=$this->db1->update('booking_charge_line_item',$data);
		
		
		// end of line charge update...
		
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
	
	function get_guest_id($bookings_id){
		$this->db1->select('cust_name');
		$this->db1->where('booking_id',$bookings_id);
		$query=$this->db1->get(TABLE_PRE.'bookings');
		if($query){
			return $query->row(); 
		}
	}
	
	function get_guest_gp($bookings_id){
		$this->db1->select('name');
		$this->db1->where('id',$bookings_id);
		$query=$this->db1->get(TABLE_PRE.'group');
		if($query){
			return $query->row(); 
		}
	}	
	
	function upGP($id,$booking_status_id){
        $tx = array(
            'booking_status_id' => $booking_status_id 
        );
		$this->db1->where('group_id',$id);
	    $query=$this->db1->update(TABLE_PRE.'bookings',$tx); 
        if($query){
            return "success";
        }else{
            return "failed";
        }
	}	
	

    public function change_password($old, $new){
		$id = $this->session->userdata('user_id'); 
        $query = $this->db1->select('user_password')->where('user_id',$id)->limit(1)->get('hotel_login');
        if ($query->num_rows() !== 1) {
            return FALSE;
        }
        $user = $query->row();
        $old_password = $user->user_password;
        if ($old_password == sha1(md5($old))) {       
            $data = array(
                'user_password' => sha1(md5($new))
            );
            $this->db1->where('user_id',$id);
            $successfully_changed_password_in_db = $this->db1->update('hotel_login', $data);						$this->db->where('user_id',$id);            $successfully_changed_password_in_db_static = $this->db->update('hotel_login_master', $data);            if ($successfully_changed_password_in_db && $successfully_changed_password_in_db_static) {
                $this->session->set_flashdata('succ_msg', "Password updated Successfully!");
				return 1;
            } else {
                $this->session->set_flashdata('err_msg', "Database Error. Try again later!");
				return 0;
            }
            //return $successfully_changed_password_in_db;
        } else {
			$this->session->set_flashdata('err_msg', "old_password_wrong");
        }
        $this->session->set_flashdata('succ_msg', "Password updated Successfully!");
        //return FALSE;
    }

 	function checkUser($val){
		$id = $this->session->userdata('user_id');
		$val = sha1(md5($val));
		$this->db1->select('*');
		$this->db1->where('user_id',$id);
		$this->db1->where('user_password',$val);
		$query=$this->db1->get(TABLE_PRE.'login');	
		$this->db->select('*');
		$this->db->where('user_id',$id);
		$this->db->where('user_password',$val);	
		$query1=$this->db->get(TABLE_PRE.'login_master');
		//$sta1=$query->num_rows();	
		//$sta2=$query1->num_rows();	
		if($query && $query1){
			return 1;
		}else{
			return 0;
		}	
	}

 	function pos_check($id,$val){
		$this->db1->select('*');
		$this->db1->where('booking_id',$id);
		$this->db1->where('booking_type',$val);
		$query=$this->db1->get(TABLE_PRE.'pos');
		if($query->num_rows()>0){
			return 1;
		}else{
			return 0;
		}	
	}
	
	function get_unitId($id){
       $sql='SELECT `room_id` FROM `hotel_bookings` WHERE `booking_id` = '.$id;
		 $query=$this->db1->query($sql);
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }
		
	}
	function get_rmNo($id){
		$sql='SELECT `room_no` FROM `hotel_room` WHERE `room_id` = '.$id;
		 $query=$this->db1->query($sql);
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }
	}
	function get_unitId1($id){
       $sql='SELECT `room_id` FROM `hotel_bookings` WHERE `group_id`='.$id;
		 $query=$this->db1->query($sql);
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
		
	}
	
	function get_all_single_by_group($group_id){
		
		$status_array= array('1','2','4');
		$this->db1->select('*');
		$this->db1->where('group_id',$group_id);
		$this->db1->where_in('booking_status_id',$status_array);
		$query = $this->db1->get('hotel_bookings');
		if($query->num_rows()>0){
			
			return $query->result();
		}
		
		
	}
	
	function get_rmNo1($id){
		$sql='SELECT `room_no` FROM `hotel_room` WHERE `room_id` = '.$id;
		 $query=$this->db1->query($sql);
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }
	}
	
	
	
	function bookingChargeLineItem_insert($data){
		$query=$this->db1->insert('booking_charge_line_item',$data);
		$last = $this->db1->insert_id();
		
		return $last;
	}
	
	function bookingChargeLineItem_update($id,$data){
		$this->db1->where('booking_id',$id);
		$query=$this->db1->update('booking_charge_line_item',$data);
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
	function bookingChargeLineItem_updateBy_bcli($id,$bcli,$data){
		$this->db1->where('bcli_id',$bcli);
		$this->db1->where('booking_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->update('booking_charge_line_item',$data);
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
	function bookingChargeLineItem_updateBy_bcli_dynamic($id,$bcli,$data){
		$this->db1->where('bcli_id',$bcli);
		$this->db1->where('booking_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->update('booking_charge_line_item',$data);
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
	
	function bookingChargeIineItem_fetch($id){
		$this->db1->where('booking_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->get('booking_charge_line_item');
        if($query){
            return $query->result();
        }else{
            return false;
        }
	}
	
	function bookingChargeIineItem_fetch_bcli($id,$index){
		
		$sql="SELECT
					*
				FROM
					`booking_charge_line_item`
				WHERE
					`booking_id` = ".$id." AND `hotel_id` = ".$this->session->userdata('user_hotel')." AND `isCancelled` = '0'
				LIMIT
					".$index.",1";
					
		 $query = $this->db1->query($sql);
		 
         if($query->num_rows()>0) {
             return $query->row();
         } else {
             return false;
         }
	}
	
	function get_bk_tax_details($booking_id){
		
		$this->db1->where('booking_id',$booking_id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->get('booking_charge_line_item_view');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}	
	}
	
	function get_line_items($booking_id){
		
		$this->db1->where('booking_id',$booking_id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->get('booking_charge_line_item');
		if($query->num_rows()>0){
			return $query->result();
		}else{
			return false;
		}
	}

function get_grp_line_items($booking_id){
	
	$this->db1->where('group_id',$booking_id);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->get('booking_charge_line_item');
		if($query->num_rows()>0){
			return $query->result();
		}else{
			return false;
		}
}

function line_charge_item_tax($type,$booking_id){
	
	if($type=='sb'){
		$get_line_items=$this->bookings_model->get_line_items($booking_id);
	}
	else{
		$get_line_items=$this->bookings_model->get_grp_line_items($booking_id);
	}
	
	if(isset($get_line_items) && $get_line_items != False){
		
		foreach($get_line_items as $value){

		$obj[]=json_decode($value->rr_tax_response);
		$obj1[]=json_decode($value->mp_tax_response);
			unset($value->bcli_id);	
			unset($value->booking_id);	
			unset($value->group_id);	
			unset($value->master_id);	
			unset($value->hotel_id);	
			unset($value->charge_type);	
			unset($value->rate_plan_id);	
			unset($value->rr_tax_response);	
			unset($value->mp_tax_response);	
			unset($value->added_on);	
			unset($value->date);	
			unset($value->mp_id);	
		$obj2[]=$value;		
				
			}
		$obj3=array();		
		$obj4=array();	
		
			for($i=0; $i < count($obj2); $i++){
				foreach($obj2[$i] as $key => $val){
					$obj3[$key][$i] =$val;
				}
			}
					if(isset($obj3) && $obj3!=null){
						foreach($obj3 as $key => $val){
							$obj4[$key] = array_sum($val);
						}	
					}			
				$san =array();
				$nan =array();
				$san1 =array();
				$nan1 =array();
				
				if(isset($obj) && $obj!=null){
					for($i=0; $i < count($obj); $i++){
						
						if(isset($obj[$i]) && $obj[$i]!=null){
							foreach($obj[$i] as $key => $val){
								$san[$key][$i] =$val;
							}
						}
					}
				}
				
			   if(isset($san) && $san!=null){

				foreach($san as $key => $val){
						
					$nan[$key] = array_sum($val);
				}
			   }

				for($j=0; $j < count($obj1); $j++){
					if(isset($obj1[$j]) && $obj1[$j]!=null){
				foreach($obj1[$j] as $key => $value){
					$san1[$key][$j] =$value;
				}
					}
				}

				if(isset($san1) && $san1!=null){
					foreach($san1 as $key => $value){
						$nan1[$key] = array_sum($value);
					}
				}
				
		$tax_set=array(
			'rr_tax'=>$nan,
			'mp_tax'=>$nan1,
			'all_price'=>$obj4,
		);
			return $tax_set;
	}
	else{
		return 0;
	}
	
} // End function line_charge_item_tax

	function get_cmp_name($id){
       $sql='SELECT * FROM `hotel_hotel_details` WHERE `hotel_id` = '.$id;
		 $query=$this->db1->query($sql);
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }
		
	}
	
	function get_transactionType_by_id($book_id)
	{
		$this->db1->where('hotel_transaction_type_id',$book_id);
		$query = $this->db1->get(TABLE_PRE.'transaction_type');

		if($query->num_rows()>0){
			return $query->row();	 
		}
		else{
			return false;
		}
	}
	
	function OTA_Avail($room_id,$d)
	{
		$flg=0;
		$flg1=0;
		$flg2=0;
		
		$this->db1->where('room_id',$room_id);
		$this->db1->where('room_availability','Available');
		$query = $this->db1->get(TABLE_PRE.'room');

		if($query->num_rows()>0){
			$flg=1;	 
		}
		else{
			$flg=0;
		}
		
		
		$this->db1->where('room_id',$room_id);
		$this->db1->where('status !=','Done');
		$query1 = $this->db1->get(TABLE_PRE.'housekeeping_log_detail');

		if($query1->num_rows()>0){
			$flg1=0;	 
		}
		else{
			$flg1=1;
		}
		
		function rm_Avail($id){
		$this->db1->where('room_id',$id);
		$this->db1->where('clean_id',1);
        $query=$this->db1->get('hotel_room');
		if($query->num_rows()>0){
			return 1;
		}else{
			return 0;
		}
		
	}
		
		
		
		//date_default_timezone_set('Asia/Kolkata');
		
		/*$this->db1->where('room_id',$room_id);
		$this->db1->where('cust_from_date_actual >=',$d);
		$this->db1->where('cust_end_date_actual <',$d);
		$query2 = $this->db1->get(TABLE_PRE.'bookings');

		if($query2->num_rows()>0){
			$flg2=1;	 
		}
		else{
			$flg2=0;
		}*/
		//$sql="SELECT * FROM `hotel_bookings` WHERE `room_id` = $room_id  AND $d between cust_from_date_actual AND cust_from_date_actual" ;
		$sql2="select room_id,booking_id from hotel_bookings where `room_id`= $room_id and 
		((date(cust_from_date) between '$d' and '$d' ) or
		(date(cust_end_date) between '$d' and '$d' ) or 
		(date(cust_from_date) <'$d' and date(cust_end_date) >'$d') or
		(date(cust_from_date) >'$d' and date(cust_end_date) <'$d') or 
		(date(cust_from_date) = '$d' and date(cust_end_date) >'$d')) and
		booking_status_id !='7' and booking_status_id !='6' and  date(cust_end_date) != '$d' and 
		date(cust_from_date) != cust_end_date" ;
		 $query2=$this->db1->query($sql2);
         if($query2->num_rows()>0){
              $flg2=1;
         }else{
              $flg2=0;
         }
		
		//if($flg1 && $flg2 && $flg3){
		if($flg2){
			return 1;
		}
		else{
			return 0;
		}
		
		
	}
	
	
	
	function all_bookings(){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where("booking_status_id!= ",7);
		$this->db1->where("booking_status_id!= ",6);
		//$this->db1->where("booking_status_id!= ",2);
		//$this->db1->where("booking_status_id!= ",5);
		//$this->db1->limit(100);
		$this->db1->order_by('booking_id', 'desc');
        $query=$this->db1->get('hotel_bookings');
		return $query->result();
	}

	function get_source_name($id){
		//$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('bk_source_type_id',$id);
        $query=$this->db1->get('hotel_booking_source');
		return $query->row();
	}
	function get_source_type_name($id){
		//$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('bst_id',$id);
        $query=$this->db1->get('booking_source_type');
		return $query->row();
	}
	function get_source_type_id_by_name($name){
		//$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('bst_name',$name);
        $query=$this->db1->get('booking_source_type');
		return $query->row();
	}
	
	function get_source_type_id_by_name_source($name){
		//$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('booking_source_name',$name);
        $query=$this->db1->get('hotel_booking_source');
		return $query->row();
	}
	
	function room_view($id=null){
		$sql="
		SELECT
		  `room_no`,rm.room_id, ty.`unit_name`,ty.`unit_type`,ty.`unit_class`, ty.default_occupancy, ty.max_occupancy, `floor_no`, hk.status_name, hk.color_primary, bk.booking_id, bk.group_id, gst.g_name, bk.booking_status_id, st.booking_status as pSts, st.body_color_code as pBdCC, st.bar_color_code as pBaCC, sts.booking_status as sSts, sts.body_color_code as sBdCC, sts.bar_color_code as sBaCC, SUM(exm.crg_total) as exc_amt, SUM(pos.total_amount) as pos_amt, pos.booking_type as pos_type, bk.booking_id
		FROM
		  `hotel_room` as rm 
		  RIGHT JOIN hotel_unit_type as ty ON rm.`unit_id` = ty.`id`
		  LEFT JOIN hotel_housekeeping_status as hk ON rm.`clean_id` = hk.status_id
		  LEFT JOIN hotel_bookings as bk ON bk.room_id = rm.room_id AND bk.booking_status_id IN (1,2,4,5,8) AND NOW() BETWEEN bk.cust_from_date AND bk.cust_end_date
		  LEFT JOIN hotel_extra_charges_mapping as exm ON (exm.booking_type = 'sb' AND exm.crg_booking_id = bk.booking_id) OR (exm.booking_type = 'gb' AND exm.crg_booking_id = bk.group_id)
		  LEFT JOIN hotel_pos as pos ON (pos.booking_type = 'sb' AND pos.booking_id = bk.booking_id) OR (pos.booking_type = 'gb' AND pos.booking_id = bk.group_id)
		  LEFT JOIN booking_status_type as st ON bk.booking_status_id = st.status_id
		  LEFT JOIN booking_status_type as sts ON bk.booking_status_id_secondary = sts.status_id
		  LEFT JOIN hotel_guest as gst ON gst.g_id = bk.guest_id
		WHERE 
		  rm.`hotel_id` = ".$this->session->userdata('user_hotel')."";
		  
		  if(isset($id) && $id){
			   $sql.=" and `room_no` =".$id."";
		  }
		 $sql.=" 
		GROUP BY
		  rm.`room_id`
		ORDER BY
		  `unit_id`, `room_no`";

		$query=$this->db1->query($sql);

        if($query){

            return $query->result();
        }
        else{

            return false;
        }

	}
	
	
	function getG_mailId($guest_id){
		$this->db1->select('g_email');
		$this->db1->where('g_id',$guest_id);
		$query=$this->db1->get('hotel_guest');
		if($query){
			return $query->row();
		}
	}

}

?>