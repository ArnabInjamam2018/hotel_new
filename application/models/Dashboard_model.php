<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 


 class Dashboard_model extends CI_Model {
    /* Add Hotel */
	///date_default_timezone_set('Asia/Kolkata');
	/*function __construct(){
		
		parent::__construct();
		$CI = &get_instance();
		$this->db1 = $CI->load->database('superadmin', TRUE);*/
	
	
    function add_hotel($hotel,$hotel_contact_details,$hotel_tax_details,$hotel_billing_settings,$hotel_general_preference){
		$query=$this->db1->insert(TABLE_PRE.'hotel_details',$hotel);
		$hotel_id = $this->db1->insert_id();
		$data = array(
			'hotel_id' => $hotel_id
		);
		$new_hotel_contact_details = array_merge($hotel_contact_details, $data);
		$new_hotel_tax_details = array_merge($hotel_tax_details , $data);
		$new_hotel_billing_settings = array_merge($hotel_billing_settings,$data);
		$new_hotel_general_preference = array_merge($hotel_general_preference,$data);
		$query_contact = $this->db1->insert(TABLE_PRE.'hotel_contact_details',$new_hotel_contact_details);
		$query_tax = $this->db1->insert(TABLE_PRE.'hotel_tax_details',$new_hotel_tax_details);
		$query_billing = $this->db1->insert(TABLE_PRE.'hotel_billing_setting',$new_hotel_billing_settings);
		$query_general = $this->db1->insert(TABLE_PRE.'hotel_general_preference',$new_hotel_general_preference);

        if($query){
				if($query_contact){
					if($query_tax){
						if($query_billing){
							if($query_general) {
								return true;
							} else {
								return false;
							}
						}
						else{
							return false;
						}
					}
					else{
						return false;
					}
				}
				else{
					return false;
				}

        }else{
            return false;
        }
	}

	function tax_details($hotel_id){
		$this->db1->select('hotel_id',$hotel_id);
		$query=$this->db1->get(TABLE_PRE.'hotel_tax_detail');
		if($query){
			return $query->row();
		}
		else{
			return false;
		}
	}
	
	// new function on 11-11-11-16...
	
	function tax_type_setting(){
		
		
		$this->db1->select('*');
//		$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->get('hotel_tax_type');
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
		
		
	}
	
	function tax_type_setting_specific($tax_name){

		$this->db1->select('*');
		$this->db1->where('tax_id',$tax_name);
        $query=$this->db1->get('hotel_tax_type');
	
		if($query->num_rows()>0){
            return $query->row();
        }
        else
        {
            return false;
        }
		
	}
	
	function tax_category_setting_specific($taxid){
	
		$this->db1->select('*');
		$this->db1->where('tc_id',$taxid);
        $query=$this->db1->get('hotel_tax_category');
	
		if($query->num_rows()>0){
            return $query->row();
        }
        else
        {
            return false;
        }
		
	}
	
	
	function tax_type_join($tax_type_tag_ID){
		
		$this->db1->select('tax_name');
        $this->db1->from('hotel_tax_type_tag');
		$this->db1->where('hotel_tax_type_tag.tax_type_id=',$tax_type_tag_ID);
        $query=$this->db1->get();
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows()>0 )
        {
            return $query->row();
        }
        else
        {
            return false;
        }
		
	}
	
	//end
	/* Ajax Hotel Email Check */
	function hotel_email_check($email){
		$query=$this->db1->get_where(TABLE_PRE.'hotel_details',$email);

        if($query->num_rows()==1){

			$a = $this->db1->insert_id();
			echo $a;
			exit();
            return $query->row();
        }else{
            return false;
        }

	}

    // Task Assigner Return

    function task_assiger(){
		
		$hotel_id = $this->session->userdata('user_hotel');
		// $sql="select * from hotel_admin_details where admin_user_type !='4' and hotel_id=='$hotel_id'";
		$this->db1->select('*');
        $this->db1->where('hotel_id=',$hotel_id);
        $this->db1->from('hotel_admin_details');
        //echo $this->db1->last_query();exit;
        $query=$this->db1->get();
		// $query=$this->db1->query($sql);
		if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function task_add($task_entry){
        $query=$this->db1->insert(TABLE_PRE.'tasks',$task_entry);
        if($query){
            return $this->db1->insert_id();
        }else{
            return false;
        }
    }

    function tasks_pending($hotel_id){
        $this->db1->select('*');
        $this->db1->where('status=','0');
        $this->db1->where('hotel_id=',$hotel_id);
        $this->db1->order_by('priority','asc');
        $this->db1->from(TABLE_PRE.'tasks');
        $query=$this->db1->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        else
        {
            return false;
        }
    }

     function tasks_pending_unique(){
         $this->db1->select('*');
         $this->db1->where('status=','0');
         $this->db1->where('to_id=',$this->session->userdata('user_id'));
         $this->db1->order_by('priority','asc');
         $this->db1->from(TABLE_PRE.'tasks');
         $query=$this->db1->get();
         if($query->num_rows()>0){
             return $query->result();
         }
         else
         {
             return false;
         }
     }

	function tax_category_list(){
		
		$this->db1->distinct();
		$this->db1->select('tax_category');
		$this->db1->from(TABLE_PRE.'tax_type');
		$query=$this->db1->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        else
        {
            return false;
        }
	}
	
	function tax_type_list($category=''){
		
		if($category==''){
		$this->db1->select('*');
        $this->db1->from('hotel_tax_type_tag');
		
        $query=$this->db1->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
		}
		else{
			
				$this->db1->select('*');
				$this->db1->where('tax_type',$category);
				$this->db1->from('hotel_tax_type_tag');
				$query=$this->db1->get();
				if($query->num_rows()>0){
					return $query->result();
				}
				else
				{
					return false;
				}
			
		}
		
	}
	
	function tax_type_add($add_tax_type){
		
		 $query=$this->db1->insert(TABLE_PRE.'tax_type',$add_tax_type);
        if($query){
            return true;
        }else{
            return false;
        }
		
	}


    function tasks_all(){
        $this->db1->select('*');
        $this->db1->order_by('status','asc');
        $this->db1->order_by('due_date','asc');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->from(TABLE_PRE.'tasks');
        $query=$this->db1->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    function get_task_user($from){
        $this->db1->select('*');
        $this->db1->where('admin_id=',$from);
	//	$this->db1->where('hotel_id=',$form);
        $this->db1->from(TABLE_PRE.'admin_details');
        $query=$this->db1->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    function task_complete($task_id){
        $this->db1->set('status','1');
        $this->db1->where('id', $task_id);
        $query_details = $this->db1->update(TABLE_PRE.'tasks');
        if($query_details){
            return true;
        }
        else{
            return false;
        }
    }
	
	function task_reopen($task_id){
        $this->db1->set('status','0');
        $this->db1->where('id', $task_id);
        $query_details = $this->db1->update(TABLE_PRE.'tasks');
        if($query_details){
            return true;
        }
        else{
            return false;
        }
    }

    function get_task_row($task_id){
        $this->db1->select('*');
        $this->db1->where('id=',$task_id);
        $this->db1->from(TABLE_PRE.'tasks');
        $query=$this->db1->get();
        if($query->num_rows()>0){
            return $query->row();
        }
        else
        {
            return false;
        }
    }

    function get_assignee_task($assignee_id){
        $this->db1->select('*');
        //$this->db1->where('to_id=',$assignee_id);
        $this->db1->from(TABLE_PRE.'tasks');
        $query=$this->db1->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    //End Assigner

	/* Ajax Hotel Phone Check */
	function hotel_phone_check($phone){
		$query=$this->db1->get_where(TABLE_PRE.'hotel_details',$phone);
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
	}

	/* Number of All Hotels */
	function all_hotels_row(){
		//$this->db1->where('hotel_status!=','D');
        $query=$this->db1->get(TABLE_PRE.'hotel_details');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }

	/* All Hotels With Pagination */
    function all_hotels(){
		$this->db1->select('*');
        $this->db1->from('hotel_hotel_contact_details');
        $this->db1->where('hotel_hotel_details.hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->join('hotel_hotel_details', 'hotel_hotel_contact_details.hotel_id = hotel_hotel_details.hotel_id');
        $query=$this->db1->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    /* All Hotels */
    function total_hotels()
    {
        $this->db1->where('hotel_status!=','D');
        $this->db1->order_by('hotel_id','desc');
        $query=$this->db1->get(TABLE_PRE.'hotel_details');
        if($query->num_rows()>0){
			return $query->result();
        }
        else
        {
            return false;
        }
    }
	function total_hotels_byId($hotel_id){
		  $this->db1->where('hotel_status!=','D');
		  $this->db1->where('hotel_id',$hotel_id);
        $this->db1->order_by('hotel_id','desc');
        $query=$this->db1->get(TABLE_PRE.'hotel_details');
        if($query->num_rows()>0){
            return $query->result();
        }
        else
        {
            return false;
        }
	}

	/* Get Particular Hotel Information */
	function get_hotel($hotel_id)
    {
        $this->db1->where('hotel_id',$hotel_id);
        $query=$this->db1->get(TABLE_PRE.'hotel_details');
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
    }
	function get_hotel_s($hotel_id)
    {
        $this->db->where('hotel_id',$hotel_id);
        $query=$this->db->get('hotel_details');
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }



    }

	function get_hotel_name($hotel_id)
    {
        //echo $hotel_id;exit;
        $this->db1->select('hotel_name');
		$this->db1->where('hotel_id',$hotel_id);
        $query=$this->db1->get(TABLE_PRE.'hotel_details');
        //return $this->db->last_query();
         //return $this->db1->last_query();
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }

       

    }

	/*function get_admin($admin_id)
    {
		//echo $admin_id."uuuu";
        $this->db1->where('admin_id',$admin_id);
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'admin_details');
	//	print_r($query);exit;
        if($query->num_rows()==1){
			
            return $query->result();
        }else{
            return false;
        }



    }
*/
	function get_admin($admin_id)
    {
		
        $this->db1->where('admin_id',$admin_id);		
        $query=$this->db1->get(TABLE_PRE.'admin_details');
        if($query->num_rows()==1){
			
            return $query->result();
        }else{
            return false;
        }



    }
	function get_admin_s($admin_id)
    {
		
        $this->db->where('admin_id',$admin_id);		
        $query=$this->db->get(TABLE_PRE.'admin_details');
        if($query->num_rows()==1){
			
            return $query->result();
        }else{
            return false;
        }



    }
	function get_admin1($admin_id)
    {
		
        $this->db1->where('admin_id',$admin_id);		
        $query=$this->db1->get(TABLE_PRE.'admin_details');
        if($query->num_rows()==1){
			
            return $query->row();
        }else{
            return false;
        }



    }
	
	
	
	function check_unlock($pw)
    {
        $this->db1->where('user_id',$this->session->userdata('user_id'));
		$this->db1->where('user_password',sha1(md5($pw)));

        $query=$this->db1->get(TABLE_PRE.'login');
        if($query->num_rows()==1){

            return true;
        }else{
            return false;
        }



    }



	/* Update Hotel */
	function update_hotel($hotel){
        $this->db1->where('hotel_id',$hotel['hotel_id']);
        $query=$this->db1->update(TABLE_PRE.'hotel_details',$hotel);
        if($query){
            return true;
        }else{
            return false;
        }
    }

	/* Delete Hotel (Only Status Changed) */
	function delete_hotel($hotel_id,$hotel){
        $this->db1->where_in('hotel_id',$hotel_id);
        $query=$this->db1->update(TABLE_PRE.'hotel_details',$hotel);
        if($query){
            return true;
        }else{
            return false;
        }
    }



	/* Ajax Hotel Status Update */
	function hotel_status_update($status){
		$this->db1->where('hotel_id',$status['hotel_id']);
        $query=$this->db1->update(TABLE_PRE.'hotel_details',$status);
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
	// dynamic database..
	function get_chain_database($chainid){
		
		$this->db->where('chain_id',$chainid);
       
        $query=$this->db->get('chain_master');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
		
	}
	

	/* All Hotel */
	function all_hotel_list(){
		$this->db->where('hotel_status!=','D');
        $this->db->order_by('hotel_name','asc');
        $query=$this->db->get('hotel_details');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	/* Get All User Types */
	function get_user_type(){
		$this->db1->where('user_type_slug!=','SUPA');
        $query=$this->db1->get(TABLE_PRE.'user_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	
	function get_user_type1($id){
		$this->db1->where('user_type_id',$id);		
        $query=$this->db1->get(TABLE_PRE.'user_type');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
	}
	
	

	/* Add Permission */
	function add_permission($permission){
		$query=$this->db1->insert_batch(TABLE_PRE.'permission_types',$permission);
        if($query){
            return true;
        }else{
            return false;
        }
	}

	/* Number of All Permissions */
	function all_permissions_row(){
		$this->db1->group_by('permission_label');
		$this->db1->where('permission_status!=','D');
        $query=$this->db1->get(TABLE_PRE.'permission_types');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }

	/* All Permissions */
    function all_permissions(){
		$this->db1->group_by('permission_label');
		$this->db1->where('permission_status!=','D');
        $this->db1->order_by('permission_label','asc');
		//$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'permission_types');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	/* Particular User Type Information */
	function get_user_type_by_id($user){
		$this->db1->where('user_type_id',$user);
		 $query=$this->db1->get(TABLE_PRE.'user_type');
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
	}

	/* Particular Permission */
	function get_permission($permission_id){
        $this->db1->where('permission_id',$permission_id);
        $query=$this->db1->get(TABLE_PRE.'permission_types');
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
    }

	/* Get All Permission By Label */
	function get_permission_info($label){
		$this->db1->where('permission_label',$label);
        $query=$this->db1->get(TABLE_PRE.'permission_types');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}

	/* Update Permission */
	function update_permission($permission){
       	$query=$this->db1->update_batch(TABLE_PRE.'permission_types', $permission, 'permission_id');
        if($query){
            return true;
        }else{
            return false;
        }
    }

	/* Add Admin */
    function add_admin($admin){
		$this->db1->insert(TABLE_PRE.'admin_details',$admin);
        $insert_id = $this->db1->insert_id();
        return $insert_id;
	}

	/* Add User For Login */
    function add_user($user){
		$this->db1->insert(TABLE_PRE.'login',$user);
		$insert_id = $this->db1->insert_id();
		
		$this->db->insert(TABLE_PRE.'login_master',$user);
		
        return $insert_id;
        
	}

	/* Add User For Login */
    function get_country(){
		$this->db1->order_by('country_name','asc');
		$query=$this->db1->get(TABLE_PRE.'countries');
        if($query){
            return $query->result();
        }else{
            return false;
        }
	}

	/* Add User For Login */
    function get_star_rating(){
		$this->db1->order_by('star_rating_value','asc');
		$query=$this->db1->get(TABLE_PRE.'star_rating');
        if($query){
            return $query->result();
        }else{
            return false;
        }
	}

	/* Ajax Username Check */
	function username_check($username){
		$query=$this->db1->get_where(TABLE_PRE.'login',$username);
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
	}

	/* Ajax Email Check */
	function email_check($email){
		$query=$this->db1->get_where(TABLE_PRE.'login',$email);
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
	}
	
	function admin_permission_all(){
		$sql = "SELECT
				  `m_heading_id`, hd.`name`, `permission_id`, `permission_label`,`permission_value`
				FROM
				  `hotel_permission_types` as ty
				  LEFT JOIN `hotel_module_heading` as hd ON hd.`id` = ty.`m_heading_id`
				ORDER BY
				  `m_heading_id` DESC, ty.`permission_label` ASC";
		
		$query=$this->db1->query($sql);
		if($query->num_rows()>0){
			 return $query->result();
		}else{
			return false;
		}
	}

	/* All Permission Label Applicable*/
	function admin_permission_label($permission_users){
		$this->db1->like('permission_users',$permission_users,'both');
		$this->db1->group_by('permission_label');
		//$this->db->order_by('m_heading_id');
		$query=$this->db1->get(TABLE_PRE.'permission_types');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}

	/* All Permissions Depending on Label for Admin */
	function get_permission_by_label($permission_label){
		$this->db1->where('permission_label',$permission_label);
		$this->db1->order_by('m_heading_id');
		$query=$this->db1->get(TABLE_PRE.'permission_types');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	
	function get_permission_by_mheading($m_heading_id){
		$this->db1->where('m_heading_id',$m_heading_id);
		$this->db1->order_by('m_heading_id');
		$query=$this->db1->get(TABLE_PRE.'permission_types');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}

	/*Add User Permission */
	function add_user_permission($permission){
		$query=$this->db1->insert(TABLE_PRE.'user_permission',$permission);
		$query1=$this->db->insert(TABLE_PRE.'user_permission',$permission);
        if($query1){
            return true;
        }else{
            return false;
        }
	}

	/* Check Admin Status */
	function admin_status($id){
		$this->db->where('admin_id',$id);
		$query = $this->db->get(TABLE_PRE.'admin_details');
		//echo $this->db->last_query();exit;
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}
	}
	function admin_status2($id){
		$this->db1->where('admin_id',$id);
		$query = $this->db1->get(TABLE_PRE.'admin_details');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}
	}

	/* Admin Registration */
	function admin_registration($admin){
        $this->db1->where('admin_id',$admin['admin_id']);
        $query=$this->db1->update(TABLE_PRE.'admin_details',$admin);
        if($query){
            return true;
        }else{
            return false;
        }
    }

	/* Update Login Information */
	function update_login($data){
        $this->db1->where('login_id',$data['login_id']);
        $query=$this->db1->update(TABLE_PRE.'login',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }

    /*Add Booking */
    function  add_booking($data){
        $query=$this->db1->insert(TABLE_PRE.'bookings',$data);
        if($query){
            return true;
        }else{
            return false;
        }



    }

     /* */

    /* All Bookings Except cancelled */

    function all_bookings(){
		$hotel_id=$this->session->userdata('user_hotel');
        /*$this->db1->order_by('cust_from_date','desc');
		$this->db1->where('booking_status_id !=',7);
		$this->db1->where('booking_status_id !=',8);
		$this->db1->where('hotel_id',$hotel_id);*/
		
		$sql = "SELECT
					*
				FROM
					`hotel_bookings`
				WHERE
					`booking_status_id` != 7 AND
					`booking_status_id` != 6 AND 
					`hotel_id` = ".$hotel_id."
				ORDER BY cust_from_date desc, `booking_id` DESC";
		
		$query=$this->db1->query($sql);
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
		
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
        //$this->db1->limit(10);
        /*$query=$this->db1->get('booking_view');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }*/
    }
	
	function all_bookings_pending(){
		$hotel_id=$this->session->userdata('user_hotel');
        $this->db1->order_by('cust_from_date','desc');
		$this->db1->where('booking_status_id !=',7);
		$this->db1->where('booking_status_id !=',6);
		$this->db1->where('hotel_id',$hotel_id);
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->limit(100);
        $query=$this->db1->get('booking_view');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	function cancel_bookings(){
		$hotel_id=$this->session->userdata('user_hotel');
       
		/*$sql="SELECT * FROM `hotel_bookings` WHERE `booking_status_id` = 7 and `group_id` = 0 and `hotel_id` = ".$hotel_id." UNION SELECT * FROM `hotel_bookings` WHERE `booking_status_id` = 7 and `group_id` > 0 and `hotel_id` = ".$hotel_id;*/
		
	//	$sql = "SELECT * FROM `hotel_bookings` WHERE `booking_status_id` = 7 ORDER BY `cancel_date` desc";
	
	
	$this->db1->select("id AS bid,name AS cname,booking_date AS bdate,booking_source AS bsource,start_date AS fdate,end_date AS tdate,id AS type");
    $this->db1->distinct();
    $this->db1->from("hotel_group");
	$this->db1->where("date_format(booking_date,'%Y-%m-%d') =", date("Y-m-d"));
	$this->db1->where("status", 7);
  
    $query1 = $this->db1->get_compiled_select(); // It resets the query just like a get()

    $this->db1->select("booking_id AS bid,cust_name AS cname,booking_taking_date AS bdate,booking_source AS bsource,cust_from_date_actual AS fdate,cust_end_date_actual AS tdate,group_id AS type");
    $this->db1->distinct();
    $this->db1->from("hotel_bookings");
    $this->db1->where("group_id",0);
	$this->db1->where("booking_status_id",7);
	$this->db1->where("date_format(booking_taking_date,'%Y-%m-%d') =", date("Y-m-d"));
    $query2 = $this->db1->get_compiled_select(); 

    $query = $this->db1->query($query1." UNION ".$query2);

    return $query->result();	 
		 
    }
	 
	
	function cancel_bookings_by_date($cust_from_date_actual,$cust_end_date_actual){
		$hotel_id=$this->session->userdata('user_hotel');
       
		$sql="SELECT * FROM `hotel_bookings` WHERE (cancel_date BETWEEN "."'".$cust_from_date_actual."'"." and "."'".$cust_end_date_actual."') and `booking_status_id` = 7 ORDER BY cancel_date desc";

		$query=$this->db1->query($sql);
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
		
    }

	/*date filter*/
	function all_bookings_datefilter(){
		$this->db1->order_by('cust_from_date','asc');
		$this->db1->where('booking_status_id !=',7);

        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }
		else{
            return false;
        }
	}





    /* Return a particular booking under a booking id */
    function  particular_booking($booking_id){

        $this->db1->where('booking_id=',$booking_id);
        $this->db1->where('booking_status_id !=',7);

        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows()>=0){
            return $query->result();
        }else{
            return false;
        }


    }
	
	function  all_recent_bookings($hotel_id){
		
		$today = new DateTime('now-1 day');
		$today_str = $today->format('Y-m-d H:i:s');
        $this->db1->order_by('cust_from_date','asc');
		$this->db1->where('cust_from_date>=',$today_str);
		$this->db1->where('booking_status_id !=',8);
		$this->db1->where('booking_status_id !=',7);
		$this->db1->where('hotel_id =',$hotel_id);

        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function  all_recent_bookings7days($hotel_id){
		
		$today = new DateTime('now-7 day');
		$today_str = $today->format('Y-m-d H:i:s');
        $this->db1->order_by('cust_from_date','asc');
		$this->db1->where('cust_from_date>=',$today_str);
		$this->db1->where('booking_status_id !=',8);
		$this->db1->where('booking_status_id !=',7);
		$this->db1->where('hotel_id =',$hotel_id);

        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function  all_recent_bookings_today($hotel_id){
		
		$today = new DateTime('now-1 day');
		$today_str = $today->format('Y-m-d H:i:s');
        $this->db1->order_by('cust_from_date','asc');
		$this->db1->where('cust_from_date>=',$today_str);
		$this->db1->where('booking_status_id !=',8);
		$this->db1->where('booking_status_id !=',7);
		$this->db1->where('hotel_id =',$hotel_id);

        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function  get_status($id){

		$this->db1->where('status_id',$id);


        //$this->db1->limit($limit,$start);
        $query=$this->db1->get('booking_status_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	function  all_bookings_report(){

        $this->db1->order_by('cust_from_date','desc');
        $this->db1->where('hotel_guest.hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->join('hotel_guest', 'hotel_guest.g_id = hotel_bookings.guest_id');
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }



    /* Get Room Id*/
    function get_room_id($hotel_id, $room_no){

        $this->db1->where('hotel_id=',$hotel_id);
        $this->db1->where('room_no=',$room_no);
        $query=$this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	
	function getOccu($rid,$rno){
		
		$this->db1->select('*');
		$this->db1->where('room_id=',$rid);
        $this->db1->where('room_no=',$rno);
        $query=$this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return "wrong";
        }
		
	}

    /* Get Room Number */

    function get_room_number($room_id){

        $this->db1->where('room_id=',$room_id);
         $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
     function get_room_number_exact($room_id){

         $this->db1->where('room_id=',$room_id);
         $query=$this->db1->get(TABLE_PRE.'room');
         if($query->num_rows()>0){
            return $query->row();
         }else{
            return false;
         }
     }

     function room_details($room_id){
        $sql="select * from hotel_room left join hotel_unit_type on hotel_room.unit_id=hotel_unit_type.id where hotel_room.room_id='".$room_id."'";
        $query=$this->db1->query($sql);
        return $query->result();
     }

    /* Add Room */
    function add_room($data){
		//echo '<pre>';
		//print_r($data['feature']);
		//exit;
		$query=$this->db1->insert(TABLE_PRE.'room',$data['room']);
		$insert_id = $this->db1->insert_id();
		foreach($data['feature'] as $key => $value)
		{
		  $roomFeature['room_feature_id'] = $key;
		  $roomFeature['hotel_id'] = $this->session->userdata('user_hotel');
		  $roomFeature['room_feature_type'] = $value[0];
		  $roomFeature['room_feature_charge'] = $value[1];
		  $roomFeature['room_id'] = $insert_id;
		  $qry=$this->db1->insert(TABLE_PRE.'room_room_features',$roomFeature);
		}
        if($query){
            return true;
        }else{
            return false;
        }
	}

    function get_room_feature_type($room_feature_id,$room_id){
		$this->db1->select('room_feature_type');
        $this->db1->where('room_feature_id=',$room_feature_id);
		$this->db1->where('room_id=',$room_id);
        $query=$this->db1->get(TABLE_PRE.'room_room_features');
        if($query->num_rows()> '0'){
            return $query->row();
        }else{
            return false;
        }
    }
	function get_room_feature_by_id($room_feature_id){
		$this->db1->select('room_feature_name,room_feature_slug');
        $this->db1->where('room_feature_id=',$room_feature_id);
		//$this->db1->where('room_id=',$room_id);
        $query=$this->db1->get(TABLE_PRE.'room_features');
        if($query->num_rows()> '0'){
            return $query->row();
        }else{
            return false;
        }
    }
	/* Number of All Rooms */
	function all_rooms_row(){
		$this->db1->where('room_status!=','D');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }
	
	function count_rooms($id){
         $sql="select count(*) as count from hotel_room where hotel_id=".$id;
         $query=$this->db1->query($sql);
         if($query){
             return $query->result();
         }else{
             return false;
         }
     }

	/* All Hotels With Pagination */
    public function all_rooms(){
		$this->db1->where('room_status!=','D');
        $this->db1->where('hotel_room.hotel_id=',$this->session->userdata('user_hotel'));
        $this->db1->join('hotel_unit_type', 'hotel_room.unit_id = hotel_unit_type.id');
        //$this->db1->group_by('room_no');
        $this->db1->order_by('room_no','asc');
		//$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
			/*echo "<pre>";
			print_r($query->result());
			exit;
			*/
            return $query->result();
			
        }else{
            return false;
        }
    }

    /* Get Particular Hotel Information */
    function get_room($room_id){
        $this->db1->where('room_id=',$room_id);
        $query=$this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()==1){

            return $query->row();
        }else{
            return false;
        }
    }
	
	function count_room($unitid){
		
		$this->db1->select('*');
		 $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
		$this->db1->where('unit_id=',$unitid);
        $query=$this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>=0){

            return $query->num_rows();
        }else{
            return false;
        }
		
	}

    /* Update Hotel */
    function update_room($room,$prop_type){
		if($prop_type=='room')
		{
        $this->db1->where('room_id=',$room['room_id']);
        $query=$this->db1->update(TABLE_PRE.'room',$room);
		}
		if($prop_type=='section')
		{
        $this->db1->where('sec_id=',$room['sec_id']);
        $query=$this->db1->update(TABLE_PRE.'section',$room);
		}

        if($query){
            return true;
        }else{
            return false;
        }
    }
     function make_room_dirty($id){
         $sql="update hotel_room set clean_id='2' where room_id='".$id."' ";
         $query=$this->db1->query($sql);
         if($query){
             return true;
         }else{
             return false;
         }
     }
     function check_duplicate_room($id,$no){
         $sql="select * from hotel_room where hotel_id='".$id ."' and room_no='".$no ."' ";
         $query=$this->db1->query($sql);
         if($query){
             return $query->num_rows();
         }else{
             return false;
         }
     }

	function check_duplicate_room_name($id,$room_name){
		 $sql="select * from hotel_room where hotel_id='".$id ."' and room_name='".$room_name ."' ";
         $query=$this->db1->query($sql);
         if($query){
             return $query->num_rows();
         }else{
             return false;
         }
	}

 function add_transaction($data,$data_cancel_transcation) {
	
        $query=$this->db1->insert(TABLE_PRE.'transactions',$data);
		$query1=$this->db1->insert(TABLE_PRE.'transactions',$data_cancel_transcation);
		
        if($query){
            return true;
        }else{
            return false;
        }
 }
 
 function add_transaction1($data) {
	
        $query=$this->db1->insert(TABLE_PRE.'transactions',$data);
		
        if($query){
            return true;
        }else{
            return false;
        }
 }
 

 function  add_transaction_grp($data,$data_cancel_transcation){
	 
		
        $query=$this->db1->insert(TABLE_PRE.'transactions',$data);
        if($query){
			$query1=$this->db1->insert(TABLE_PRE.'transactions',$data_cancel_transcation);
            return true;
        }else{
            return false;
        }
     }





    function all_transactions(){
        $this->db1->order_by('t_id','desc');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		//$this->db1->where('user_id',$this->session->userdata('user_id'));  
	    $query=$this->db1->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function all_transactions_byID($type,$id){
        $this->db1->order_by('t_id','desc');
		if($type == 'sb')
			$this->db1->where('t_booking_id',$id); 
		else if($type == 'gb')
			$this->db1->where('t_group_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
	    $query=$this->db1->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        }
    }
	
	
		function all_transactions_byID_BS($type,$id,$bs){
        $this->db1->order_by('t_id','desc');
		if($type == 'sb')
			$this->db1->where('t_booking_id',$id); 
		else if($type == 'gb')
			$this->db1->where('t_group_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
	    $query=$this->db1->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        }
    }
	
	
	function all_transaction_adhoc_byID($id){
        $this->db1->order_by('t_id','desc');
		$this->db1->where('details_id',$id);
		$this->db1->where('transaction_type_id',15);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
	    $query=$this->db1->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function  all_purchase_limit(){
         //$this->db1->order_by('t_id','desc');
         //$array = array('transaction_from_id' => "6", 'transaction_releted_t_id' => "5", 'status' => $status);
         $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		 $this->db1->order_by('id','desc');
         $query=$this->db1->get(TABLE_PRE.'purchase');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }
     function  all_purchase_row(){
         $this->db1->order_by('p_id','desc');
         $query=$this->db1->get(TABLE_PRE.'purchase');
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }
     }

      function all_transactions_by_date($start_date, $end_date){
        $this->db1->order_by('t_id','asc');
		//$this->db1->group_by('t_booking_id');
		  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		//"date_format(cust_from_date,'%Y-%m-%d') >="
        $this->db1->where("date_format(t_date,'%Y-%m-%d') >=", $start_date);
        $this->db1->where("date_format(t_date,'%Y-%m-%d') <=", $end_date);
        $query=$this->db1->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	  function all_transactions_by_date1($start_date, $end_date){
        $this->db1->order_by('t_booking_id','desc');
		$this->db1->group_by('t_booking_id');
		//"date_format(cust_from_date,'%Y-%m-%d') >="
        $this->db1->where("date_format(t_date,'%Y-%m-%d') >=", $start_date);
        $this->db1->where("date_format(t_date,'%Y-%m-%d') <=", $end_date);
        $query=$this->db1->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	
	

	function note_preference_by_date_datefilter($start_date, $end_date){
        //$this->db1->order_by('t_id','asc');
        $this->db1->where('cust_from_date >=', $start_date);
        $this->db1->where('cust_from_date <=', $end_date);
        $query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	function all_tax_report_by_date($start_date, $end_date){
        $this->db1->order_by('booking_id','desc');
		$this->db1->where('booking_status_id !=',7);
        $this->db1->where('hotel_id', $this->session->userdata('user_hotel'));
        $this->db1->where('cust_from_date_actual >=', $start_date);
        $this->db1->where('cust_from_date_actual <=', $end_date);
        $this->db1->limit(100);
		$query=$this->db1->get('booking_view');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }



     function all_transactions_by_date_endless($start_date, $end_date){
         $this->db1->order_by('t_id','asc');
		 $this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
         $this->db1->where('t_date >=', $start_date);
         $this->db1->where('t_date <=', $end_date);
         $query=$this->db1->get(TABLE_PRE.'transactions');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }
	 
	 function extrachargeNB($source,$season,$unittype,$date,$mealplan,$occu){
		 
		 $this->db1->select('*');
		 $this->db1->where('source_type_name=',$source);
		 $this->db1->where('rate_plan_type_id=',$season);
		 $this->db1->where('unit_type_id=',$unittype);
		 $this->db1->where('meal_plan_id=',$mealplan);
		 $this->db1->where('occupancy_id=',$occu);
		 $query=$this->db1->get('rate_plan');
         if($query->num_rows()>0){
             return $query->row();
         }else{
             //return false;
			 echo "returning false";
         }
		 
	 }

     function opening_balance_by_date($date){
         $this->db1->order_by('t_id','asc');
         $this->db1->where('t_date <=', $date);
         // $this->db1->where('t_date <=', $end_date);
		// $this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
         $query=$this->db1->get(TABLE_PRE.'transactions');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }


     function all_bookings_by_date($start_date,$end_date,$admin=''){
		$adm_arr = explode(',',$admin);
		//print_r($admin);
		 //print_r($adm_arr);
		 //exit;
        $this->db1->order_by('cust_from_date','asc');
        $this->db1->where('booking_status_id !=',7);
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		if($start_date != "" && $end_date!="")
		{
        $this->db1->where('cust_from_date >=', $start_date);
        $this->db1->where('cust_end_date <=', $end_date);
		}
		if($admin !="")
		{
			$adm_arr = explode(',',$admin);
			$this->db1->where_in('user_id', $adm_arr);
		}

        $query=$this->db1->get(TABLE_PRE.'bookings');
		//echo  $this->db1->last_query();
		//echo "<pre>";
		//print_r($query->result());
		//exit;
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	function all_group_by_date($start_date,$end_date){
		$this->db1->order_by('booking_date','DESC');
		$this->db1->order_by('id','DESC');
        $this->db1->where('start_date >=', $start_date);
        $this->db1->where('start_date <=', $end_date);
	//	$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->get(TABLE_PRE.'group');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}

	function get_cust_report_by_date($date_1,$date_2){
		 $this->db1->order_by('booking_id','asc');
		 $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        // $this->db1->where('cust_from_date_actual >=',$date_1);
        // $this->db1->where('cust_end_date_actual <=',$date_2);
        $this->db1->where("date_format(cust_from_date,'%Y-%m-%d') >=",$date_1);
        $this->db1->where("date_format(cust_end_date,'%Y-%m-%d') >=",$date_2);
        // $this->db1->where('cust_end_date <=',$date_2);

		$query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	/*function all_transactions_report(){
        $this->db1->order_by('t_id','asc');
		join(TABLE_PRE.'guest', 'hotel_guest.g_id = credentials.cid');
        $query=$this->db1->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }*/

    /* Number of All Transactions */
    function all_transactions_row(){
        $query=$this->db1->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }

    /* All Hotels With Transactions */
    /*
    function all_transactions_limit(){
        $this->db1->order_by('t_id','desc');
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
    */

    function all_transactions_limit()
    {
        $this->db1->select('*');
        $this->db1->order_by('t_id','desc');
        $this->db1->from('transactionview_2');
		$this->db1->where('transactionview_2.hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get();

        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function find_name($get_id_name){
        $this->db1->where('hotel_entity_type_id=',$get_id_name);
        $query=$this->db1->get(TABLE_PRE.'entity_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function  add_compliance($data){
        $query=$this->db1->insert(TABLE_PRE.'compliance',$data);
        if($query){

            return true;
        }else{

            return false;
        }



    }

    function all_compliance(){
        $this->db1->order_by('c_id','asc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'compliance');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    /* Number of All Transactions */
    function all_compliance_row(){
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'compliance');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }

    /* All Hotels With Transactions */
    function all_compliance_limit(){
        $this->db1->order_by('c_id','desc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
      //  $this->db1->where('user_id',$this->session->userdata('user_id'));
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'compliance');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	
	function all_compliance_limit_by_date($n_date){
		
		if($n_date == 'today'){
			
			$today = date('Y-m-d');
			$this->db1->order_by('c_id','desc');
			$this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
			$this->db1->where('c_valid_upto >=',$today);
			//$this->db1->limit($limit,$start);
			$query=$this->db1->get(TABLE_PRE.'compliance');
			
		}
		
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	function all_dg_fuel_stock(){
        $this->db1->order_by('id','desc');
        $this->db1->where('fs_hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'master_fuel_stock');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }


	function amc_unique_date(){

		 $this->db1->select('*');
		 //$this->db1->where("date_format(amc,'%Y-%m-%d') <=",date("Y-m-d"));
		 $this->db1->where('amc <=',date("Y-m-d"));
		 $this->db1->from(TABLE_PRE.'dgset');
         $query=$this->db1->get();
         if($query->num_rows()>0){
             return $query->result();
         }
         else
         {
             return false;
         }
	}

    function  add_guest($data){
        $query=$this->db1->insert(TABLE_PRE.'guest',$data);
        if($query){
            return true;
        }else{
            return false;
        }



    }

     function  add_guest2($data){
         $query=$this->db1->insert(TABLE_PRE.'guest',$data);
         if($query){
             return $this->db1->insert_id();
         }else{
             return false;
         }



     }

    function  add_broker($data){
        $query=$this->db1->insert(TABLE_PRE.'broker',$data);
        if($query){
            return true;
        }else{
            return false;
        }



    }


    function all_broker(){
        $this->db1->order_by('b_id','asc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'broker');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    /* Number of All Transactions */
    function all_broker_row(){
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'broker');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }

    /* All Hotels With Transactions */
    function all_broker_limit(){
        $this->db1->order_by('b_id','desc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));        
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'broker');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function all_guest(){
        $this->db1->order_by('g_name','asc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'guest');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function  add_event($data){
        $query=$this->db1->insert(TABLE_PRE.'events',$data);
        if($query){
            return true;
        }else{
            return false;
        }

    }


    function all_events(){
        $this->db1->order_by('e_id','desc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db1->where('e_notify=',1);
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'events');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function all_events_limit(){
        $this->db1->order_by('e_id','desc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $this->db1->where('user_id=',$this->session->userdata('user_id'));
        //$this->db1->where('e_notify=',1);
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'events');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

     function  add_shift($data){
         $query=$this->db1->insert(TABLE_PRE.'shift',$data);
         if($query){
             return true;
         }else{
             return false;
         }

     }

     function  add_shift_log($data){
         $query=$this->db1->insert('shift_log',$data);
         if($query){
             return $this->db1->insert_id();
         }else{
             return false;
         }

     }



     function all_shift_limit(){
         $this->db1->order_by('shift_id','desc');
         $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        // $this->db1->where('status','1');
         $query=$this->db1->get(TABLE_PRE.'shift');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }


	 function all_shift_limit1(){//Active status
         $this->db1->order_by('shift_id','desc');
         $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
         $this->db1->where('status','1');
         $query=$this->db1->get(TABLE_PRE.'shift');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }

     function  insert_cash_drawer($data){
			$data1=array(
			//'closing_datetime' => CURRENT_TIMESTAMP,
			'is_ended'=>'1',
			);
			  /*$id=$this->session->userdata('user_hotel');
              $this->db1->where('hotel_id', $id);
              $this->db1->where('is_ended','0');
              $query1=$this->db1->update(TABLE_PRE.'cash_drawer',$data1);*/


         $query=$this->db1->insert(TABLE_PRE.'cash_drawer',$data);
         if($query){
             return $this->db1->insert_id();

         }else{
             return 'Some Database Error Occurred';
         }



     }

     function  update_drawer($id){
        date_default_timezone_set('Asia/Kolkata');
         $this->db1->where('drawer_id',$id);
         $data=array(
             'closing_datetime' => date("Y-m-d H:i:s"),
             'is_ended' => 1
         );
		 $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
         $query=$this->db1->update('hotel_cash_drawer',$data);
         if($query){
             return true;
         }else{
             return false;
         }
     }

     function all_cash_drawer_limit(){
        $sql="SELECT
				  cd.`drawer_id`, cd.`cashDrawerName`, cd.`hotel_id`, cd.`profitCenter`, se.`status`, se.`hls_id`, se.`user_id`
				FROM
				  `hotel_cash_drawer` as cd
				  LEFT OUTER JOIN `hotel_login_session` as se 
				  ON se.`cashdrawer` = cd.`drawer_id`  and se.`status` = 'Success'
				WHERE
				  cd.`is_ended` = 0 AND cd.`hotel_id` = ".$this->session->userdata('user_hotel')."
				GROUP BY 
				  cd.`drawer_id`
				";
        //$sql="select * from hotel_cash_drawer where is_ended='0' ";
         $query=$this->db1->query($sql);
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }

     function all_logs(){

         $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
         $query=$this->db1->get('shift_log');
         //$this->db1->order_by('shift_log.log_id','desc');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }



    /* Number of All Transactions */
    function all_guest_row(){
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'guest');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }

    /* All Hotels With Transactions */
    function all_guest_limit()
    {
        //$this->db1->order_by('g_id','desc');
        //$this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db1->limit($limit,$start);
		$today = date('Y-m-d');
		$this->db1->where('cust_from_date',$today);
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->select_sum('no_of_guest');
		$query = $this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

        /*$this->db1->select_sum('age');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $this->db1->where($date,'');*/
    }

	/* 18.11.2015 */

	function all_guest_limit_view()
    {
        $this->db1->order_by('g_name','asc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
       // $this->db1->where('user_id=',$this->session->userdata('user_id'));
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'guest');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

        /*$this->db1->select_sum('age');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $this->db1->where($date,'');*/
    }

	function get_guest_details($g_id_edit)
	{
		//echo "In Model";
		//echo  $g_id_edit;
		//exit();
		$this->db1->select('*');
        $this->db1->where('g_id=',$g_id_edit);
       // $this->db1->where('hotel_id=',$hid);
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'guest');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}

    function get_room_details($room_id)
    {
        //echo "In Model";
        //echo  $g_id_edit;
        //exit();
        $this->db1->select('*');
        $this->db1->where('room_id=',$room_id);
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }


    function get_hotel_details($hotel_id)
    {
//echo "ooo".$hotel_id;
        $this->db1->select('*');
        $this->db1->from('hotel_hotel_contact_details');
        $this->db1->join('hotel_hotel_details', 'hotel_hotel_contact_details.hotel_id = hotel_hotel_details.hotel_id', 'inner');
         $this->db1->join('hotel_hotel_tax_details' , 'hotel_hotel_tax_details.hotel_id = hotel_hotel_details.hotel_id', 'inner');
        $this->db1->join('hotel_hotel_billing_setting' , 'hotel_hotel_billing_setting.hotel_id = hotel_hotel_details.hotel_id', 'inner');
        $this->db1->join('hotel_hotel_general_preference' , 'hotel_hotel_general_preference.hotel_id = hotel_hotel_details.hotel_id', 'inner');
		$this->db1->where('hotel_hotel_details.hotel_id=',$hotel_id);
        $this->db1->group_by('hotel_hotel_details.hotel_id');
        $query=$this->db1->get();
//print_r($query);exit;
        /*
        $this->db1->select('*');
        $this->db1->where('hotel_id=',$hotel_id);
        $query=$this->db1->get(TABLE_PRE.'hotel_details');*/
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
    function get_hotel_details_row($hotel_id)
    {

        $this->db1->select('*');
        $this->db1->from('hotel_hotel_contact_details');
        $this->db1->join('hotel_hotel_details', 'hotel_hotel_contact_details.hotel_id = hotel_hotel_details.hotel_id', 'inner');
        $this->db1->join('hotel_hotel_tax_details' , 'hotel_hotel_tax_details.hotel_id = hotel_hotel_details.hotel_id', 'inner');
        $this->db1->join('hotel_hotel_billing_setting' , 'hotel_hotel_billing_setting.hotel_id = hotel_hotel_details.hotel_id', 'inner');
        $this->db1->join('hotel_hotel_general_preference' , 'hotel_hotel_general_preference.hotel_id = hotel_hotel_details.hotel_id', 'inner');

        $this->db1->where('hotel_hotel_details.hotel_id=',$hotel_id);
        $this->db1->group_by('hotel_hotel_details.hotel_id');
        $query=$this->db1->get();

        /*
        $this->db1->select('*');
        $this->db1->where('hotel_id=',$hotel_id);
        $query=$this->db1->get(TABLE_PRE.'hotel_details');*/
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
    }
    function get_hotel_details2($hotel_id)
    {

        $this->db->select('*');
        $this->db->from('hotel_details');

        $this->db->where('hotel_id',$hotel_id);
        $query=$this->db->get();

        if($query->num_rows()==1){
            return $query->result();
        }else{
            return false;
        }
    }

    function edit_hotel_details($hotel_edit,$h_id)
    {
        $this->db1->where('hotel_id', $h_id);
        $query_details = $this->db1->update(TABLE_PRE.'hotel_details', $hotel_edit);
        if($query_details){
            return true;
        }
        else{
            return false;
        }
    }

    function edit_hotel_contact($hotel_contact_details_edit,$h_id)
    {
        $this->db1->where('hotel_id', $h_id);
        $query_contact = $this->db1->update(TABLE_PRE.'hotel_contact_details', $hotel_contact_details_edit);
        if ($query_contact) {
            return true;
        }
        else{
            return false;
        }
    }

    function edit_hotel_tax($hotel_tax_details_edit,$h_id)
    {
        $this->db1->where('hotel_id', $h_id);
        $query_tax = $this->db1->update(TABLE_PRE.'hotel_tax_details', $hotel_tax_details_edit);
        if ($query_tax) {
            return true;
        }
        else{
            return false;
        }
    }

    function edit_hotel_billing($hotel_billing_settings_edit,$h_id)
    {
        $this->db1->where('hotel_id', $h_id);
        $query_billing = $this->db1->update(TABLE_PRE.'hotel_billing_setting', $hotel_billing_settings_edit);
        if ($query_billing) {
            return true;
        }
        else{
            return false;
        }
    }


    function edit_hotel_preferences($hotel_general_preference_edit,$h_id)
    {
        $this->db1->where('hotel_id', $h_id);
        $query_preference = $this->db1->update(TABLE_PRE.'hotel_general_preference', $hotel_general_preference_edit);
        if ($query_preference){
            return true;
        }
        else{
            return false;
        }
    }


	function get_broker_details($b_id_edit)
	{
		//echo "In Model";
		//echo  $b_id_edit;
		//exit();
		$this->db1->select('*');
        $this->db1->where('b_id=',$b_id_edit);
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'broker');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
 /*   function get_channel_details($c_id_edit)
    {
        //echo "In Model";
        //echo  $b_id_edit;
        //exit();
        $this->db1->select('*');
        $this->db1->where('channel_id=',$c_id_edit);
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'channel');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }*/

	function get_c_details($c_id_edit)
	{
		//echo "In Model";
		//echo  $b_id_edit;
		//exit();
		$this->db1->select('*');
        $this->db1->where('c_id=',$c_id_edit);
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'compliance');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}


	function edit_guest($guest_edit,$g_id)
	{
		$this->db1->where('g_id', $g_id);
		$query = $this->db1->update(TABLE_PRE.'guest', $guest_edit);
		//echo  $this->db1->last_query();
		if($query){
			return true;
		}else{
			return false;
		}

	}

    function edit_room($room_edit,$room_id)
    {  
        $this->db1->where('room_id', $room_id);
        $query = $this->db1->update(TABLE_PRE.'room', $room_edit['room']);
		$this->db1->where_in('room_id', $room_id);
        $query=$this->db1->delete(TABLE_PRE.'room_room_features');
		foreach($room_edit['feature'] as $key => $value)
		{
		  $roomFeature['room_feature_id'] = $key;
		   $roomFeature['hotel_id'] = $this->session->userdata('user_hotel');
		   $roomFeature['room_feature_type'] = $value[0];
		  $roomFeature['room_feature_charge'] = $value[1];
		  $roomFeature['room_id'] = $room_id;
		  $qry=$this->db1->insert(TABLE_PRE.'room_room_features',$roomFeature);
		}
		//print_r($roomFeature); exit;
        //echo  $this->db1->last_query();
        if($query){
            return true;
        }else{
            return false;
        }

    }

    function delete_room($room_id){
        $this->db1->where_in('room_id',$room_id);
        $query=$this->db1->delete(TABLE_PRE.'room');
        if($query){
            return true;
        }else{
            return false;
        }
    }

     function delete_servic($room_id){
         $this->db1->where_in('room_id',$room_id);
         $query=$this->db1->delete(TABLE_PRE.'room');
         if($query){
             return true;
         }else{
             return false;
         }
     }

    function delete_broker($b_id){
        $this->db1->where_in('b_id',$b_id);
        $query=$this->db1->delete(TABLE_PRE.'broker');
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function delete_service($booking_id,$service_id){
       $this->db1->where('booking_id', $booking_id);
       $query = $this->db1->update(TABLE_PRE.'bookings', $service_id);
	  
        if($query){
            return true;
        }else{
            return false;
        }


    }
	
	function delete_service_mapping($booking_id,$s_id){
		$this->db1->where('booking_id', $booking_id);
		$this->db1->where('service_id', $s_id);
       $query = $this->db1->delete(TABLE_PRE.'service_mapping');
	   if($query){
		   return true;
	   }else{
		   return false;
	   }
	}

	function get_charge_tag_by_id($tag_id){		

		$this->db1->select('*');
        $this->db1->where('extra_charge_tag_id=',$tag_id);
        
        $query=$this->db1->get(TABLE_PRE.'extra_charge_tag');
		if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	} // _sb dt20_10_16
	
	function get_all_charge_tag(){				
		$this->db1->select('*');        
        $query=$this->db1->get(TABLE_PRE.'extra_charge_tag');
		if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	} // _sb dt20_10_16
	
    function search_charge($word){
		if(isset($word) && $word)
			{
			$sql="select * from  hotel_extra_charges where charge_name LIKE '%".$word."%' GROUP BY charge_name";
			$result=$this->db1->query($sql);
				if($result->num_rows()>0)
				  {
						return $result->row();
				  }
				else
				  {
						return false;
				  }
		    }
		else
			{
			return false;
			}
    }
	function search_charge_auto(){
		
		$this->db1->select('*');
		$this->db1->from('hotel_extra_charges');
		$this->db1->join('hotel_extra_charge_tag', 'hotel_extra_charges.ec_tag = hotel_extra_charge_tag.extra_charge_tag_id','left outer');
		$query = $this->db1->get();
        return $query->result_array();
	}
	function search_service_auto(){
		
		$query = $this->db1->get('hotel_service');
        return $query->result_array();
	}
	function search_service($keyword){
		if(isset($keyword) && $keyword){
			$sql="select * from hotel_service where s_name LIKE '%".$keyword."%' GROUP BY s_name";
			$result=$this->db1->query($sql);
				if($result->num_rows()>0)
				  {
						return $result->result();
				  }
				else
				  {
						return false;
				  }
		}else
			{
			return false;
			}

	}

    function delete_charge($booking_id,$service_id){

       // $sql="update hotel_bookings set service_id='".$service_id."' where booking_id='"+$booking_id+"'  ";
        // print_r($service_id);
        // exit();

       $this->db1->where('booking_id', $booking_id);
        $query = $this->db1->update(TABLE_PRE.'bookings', $service_id);

        //echo $this->db1->last_query();
        //exit();
        //echo  $this->db1->last_query();
        if($query){
            return true;
        }else{
            return false;
        }


    }

    function delete_event($e_id){
        $this->db1->where_in('e_id',$e_id);
        $query=$this->db1->delete(TABLE_PRE.'events');
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function delete_guest($g_id){
        $this->db1->where_in('g_id',$g_id);
        $query=$this->db1->delete(TABLE_PRE.'guest');
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function delete_compliance($c_id){
        $this->db1->where_in('c_id',$c_id);
        $query=$this->db1->delete(TABLE_PRE.'compliance');
        if($query){
            return true;
        }else{
            return false;
        }
    }

	function edit_broker($broker_edit,$b_id)
	{
		//echo "In model".$b_id;
		//print_r($broker_edit);
		//exit();
		$this->db1->where('b_id', $b_id);
		$query = $this->db1->update(TABLE_PRE.'broker', $broker_edit);
		//echo  $this->db1->last_query();
		if($query){
			return true;
		}else{
			return false;
		}
	}

     function  edit_booking($b_id,$g_id){

         //exit();
        // $this->db1->where('booking_id', $b_id);
        // $query = $this->db1->update(TABLE_PRE.'bookings', $g_id);
         //echo  $this->db1->last_query();

         $sql="update hotel_bookings  set guest_id_others = CONCAT(guest_id_others,',','".$g_id."') where booking_id= '".$b_id."' ";

         $query= $this->db1->query($sql);

         if($query){
             return true;
         }else{
             return false;
         }
     }

    function edit_compliance($compliance_edit,$c_id)
    {
        $this->db1->where('c_id', $c_id);
        $this->db1->where('hotel_id', $this->session->userdata('user_hotel'));
        $query = $this->db1->update(TABLE_PRE.'compliance', $compliance_edit);
        //echo  $this->db1->last_query();
        if($query){
            return true;
        }else{
            return false;
        }

    }

	function all_booking_id()
	{
		$this->db1->select('booking_id');
		//$this->db1->from(TABLE_PRE.'bookings');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query = $this->db1->get(TABLE_PRE.'bookings');
		if($query->num_rows() > 0){
   			foreach($query->result_array() as $row){
    		$data[] = $row;
   			}
   			return $data;
  		}
		else
		{
			return false;
		}
	}

	/* 18.11.2015 */
	/* 19.11.2015*/
	function all_t_amount($val)
	{
		/*$agr = array(
			'hotel_id' => $this->session->userdata('user_hotel'),
			'booking_id'=> $val);
		$this->db1->select('*');
		$this->db1->where($agr);
		$query = $this->db1->get(TABLE_PRE.'bookings');
		if($query->num_rows() > 0){
   			foreach($query->result_array() as $row){
    		$data[] = $row;
   			}
   			return $data;
  		}
		else
		{
			return false;
		}*/

		$this->db1->select('base_room_rent,rent_mode_type,mod_room_rent');
		$this->db1->from('hotel_bookings');
		$this->db1->where('booking_id',$val);

		$query = $this->db1->get();
		if($query->num_rows() > 0){
   			/*foreach($query->result_array() as $row1){
    		//$data1[] = $row1;
   			}
   			return $data1;*/
			return $query->result();
  		}
		else
		{
			return false;
		}
	}
	/*19.11.2015*/

	function  all_bookings_unique($hotel_id){


        $this->db1->select('*');
        $this->db1->from('hotel_transactions');
       // $this->db1->join('hotel_bookings', 'hotel_transactions.t_booking_id = hotel_bookings.booking_id', 'inner');
        date_default_timezone_set('Asia/Kolkata');
        //$date = date("Y/m/d");
        $this->db1->where('transaction_releted_t_id=',1);
        $this->db1->where('hotel_id=',$hotel_id);
		$this->db1->select_sum('t_amount');
		 $this->db1->where("date_format(t_date,'%Y/%m/%d')", date("Y/m/d"));
       // $this->db1->like('hotel_transactions.t_date', $date);
        $query = $this->db1->get();
        
		if($query->num_rows()>0)
        {
			return $query->result();
        }
		else
        {
            return false;
		}
    }
    
    
    function  all_bookings_unique_Today($hotel_id){


       $sql="SELECT 
( CASE  WHEN `t_payment_mode`='cash' THEN sum(`t_amount`)
END ) as CashP,

( CASE  WHEN `t_payment_mode`='cards' THEN sum(`t_amount`)
END ) as CardP,
( CASE  WHEN `t_payment_mode`='fund' THEN sum(`t_amount`)
END ) as FundP,
( CASE  WHEN `t_payment_mode`='cheque' THEN sum(`t_amount`)
END ) as chqP
,
( CASE  WHEN `t_payment_mode`='ewallet' THEN sum(`t_amount`)
END ) as ewaP,
( CASE  WHEN `t_payment_mode`='draft' THEN sum(`t_amount`)
END ) as draP,
sum(t_amount) as Totsum
FROM `hotel_transactions` 
WHERE date(`t_date`)=CURRENT_DATE() AND transaction_releted_t_id=1 AND hotel_id='".$hotel_id."'
group by `t_payment_mode`";
     
	   $query = $this->db1->query($sql);
        
		if($query->num_rows()>0)
        {
			return $query->result();
        }
		else
        {
            return false;
		}
    }
    

     function  revenue_by_date($date){


         $this->db1->select('*');
         $this->db1->from('hotel_transactions');
         $this->db1->join('hotel_bookings', 'hotel_transactions.t_booking_id = hotel_bookings.booking_id', 'inner');

         $this->db1->where('hotel_transactions.transaction_releted_t_id=',1);
         //$this->db1->or_where('hotel_transactions.transaction_releted_t_id=', 6);

         //$this->db1->where('hotel_transactions.transaction_releted_t_id=',6);
         $this->db1->where('hotel_bookings.hotel_id=',$this->session->userdata('user_hotel'));
         $this->db1->select_sum('hotel_transactions.t_amount');
         //$this->db1->from(TABLE_PRE.'transactions');
         //$query = $this->db1->get_where(TABLE_PRE.'transactions',array('t_date' => $date));
         //$this->db1->like('hotel_transactions.t_date', $date, 'after');
		 // $this->db1->where("date_format(cust_end_date,'%Y-%m-%d') >=",$date_2);
         $this->db1->where("date_format(hotel_transactions.t_date,'%Y/%m/%d')", $date,'after');
         $this->db1->where("hotel_transactions.t_status",'Done');
         $query = $this->db1->get();
        // $str = $this->db1->last_query();
       //  print_r($str);
         //exit();
         if($query->num_rows()>0)
         {
             return $query->result();
         }
         else
         {
             return false;
         }
     }



function report_search($visit,$date_1,$date_2){

			$this->db1->select('*');

			if($date_1!="" && $date_2!="" && $visit!="")
			{

					$this->db1->where('cust_from_date>=', $date_1);
					$this->db1->where('cust_from_date <=', $date_2);
					$this->db1->where('nature_visit',$visit);

			}
			else if($date_1!="" && $date_2!="" && $visit=="")
			{
				$this->db1->where('cust_from_date>=', $date_1);
				$this->db1->where('cust_from_date <=', $date_2);
			}
			else if($date_1=="" && $date_2=="" && $visit!="")
			{
				$this->db1->where('nature_visit',$visit);
			}


					$query=$this->db1->get(TABLE_PRE.'bookings');
					if($query->num_rows()>0){
							return $query->result();
						}
						else{
							return false;
						}


    }


    function fetch_all_address($pincode)
    {
            $this->db1->distinct();
            $this->db1->select('area_0,area_1,area_4');
            $this->db1->from('hotel_area_code');
		//	$this->db1->where('user_id',$this->session->userdata('user_id'));
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
            $this->db1->where('pincode',$pincode);
            $query=$this->db1->get();

            if($query->num_rows() >0)
            {
                return $query->result();
            }
            else
            {
                return false;
            }
    }
	
	function fetch_all_address_static($pincode)
    {
            $this->db->distinct();
            $this->db->select('area_0,area_1,area_4');
            $this->db->from('hotel_area_code');
            $this->db->where('pincode',$pincode);
            $query=$this->db->get();

            if($query->num_rows() >0)
            {
                return $query->result();
            }
            else
            {
                return false;
            }
    }
		function fetch_all_address_static2($pincode)
		{
            $this->db->distinct();
            $this->db->select('area_0,area_1');
          //  $this->db->from('hotel_area_code');
            $this->db->where('pincode',$pincode);
            $query=$this->db->get('hotel_area_code');

            if($query->num_rows() >0)
            {
                return $query->row();
            }
            else
            {
                return false;
            }
    }									   

    function all_f_reports(){

        $this->db1->where('t_amount !=',0);
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->order_by('t_booking_id','desc');
       // $this->db1->group_by('t_booking_id');
		//$this->db1->like('t_date',date("Y/m/d"));
		 $this->db1->where("date_format(t_date,'%Y/%m/%d')", date("Y/m/d"));
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function all_f_reports1(){

        $this->db1->where('t_amount !=',0);
        $this->db1->where('transaction_type','Booking Payment');
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->order_by('t_booking_id','desc');
        $this->db1->group_by('t_booking_id');
		$this->db1->like('t_date',date("Y/m/d"));
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	
	function cust_name($bi){

		$this->db1->where('booking_id',$bi);
		$query=$this->db1->get(TABLE_PRE.'bookings');

        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	function transaction_type(){
		$this->db1->select('*');
		$query=$this->db1->get(TABLE_PRE.'transaction_type');
		if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	
	function transaction_type_name($typeid){
		$this->db1->select('*');
		$this->db1->where('hotel_transaction_type_id',$typeid);
		$query=$this->db1->get(TABLE_PRE.'transaction_type');
		if($query){
            return $query->row()->hotel_transaction_type_name;
        }else{
            return false;
        }
	}

	function all_f_types(){
        //$this->db1->limit($limit,$start);
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
      //  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'transaction_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	function all_f_entity(){
        //$this->db1->limit($limit,$start);
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
    //   $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'entity_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }


		function add_booking_transaction($t_transaction){
				//echo "In Model";
				//print_r($t_transaction);
				//exit();
				$query=$this->db1->insert(TABLE_PRE.'transactions',$t_transaction);
				$sql = $this->db1->last_query();
				//echo $sql;
				//exit();
        		if($query){
            			return true;
        		}else{
            			return false;
        		}
		}

		function get_data_for_excel()
		{
			$this->db1->select('booking_id,cust_name,room_id,cust_from_date,cust_end_date,cust_contact_no,mod_room_rent,nature_visit');
        	$this->db1->from(TABLE_PRE.'bookings');
        	$query=$this->db1->get();

        	if($query->num_rows() >0){
            	return $query->result();
        	}
        	else{
            	return false;
        	}
		}

        function update_checkout_status_grp($grp_id,$date_checkout,$dateCheckout)
        {
			 date_default_timezone_set('Asia/Kolkata');
            $times=$this->dashboard_model->checkout_time();
            foreach($times as $time) {
                if($time->hotel_check_out_time_fr=='PM') {
					if($time->hotel_check_out_time_hr == 12) {
                      $modifier = ($time->hotel_check_out_time_hr);
					} else {
					  $modifier = ($time->hotel_check_out_time_hr + 12);
					}
                }
                else{
                    $modifier = ($time->hotel_check_out_time_hr);
                }
				$hotel_check_out_time_mm = $time->hotel_check_out_time_mm;
            }

			$current_time = date("H:i:s");
			$date = date('Y-m-d').' '.$modifier.':'.$hotel_check_out_time_mm.':00';
			$time_def = $modifier+($hotel_check_out_time_mm/60);
			$time_sys = date("H");
			$time_sys = $time_sys + (date("i")/60);
			$time_diff = $time_sys-$time_def;
			$buffer_hr = 2;

            $time_date_data = date('Y-m-d H:i:s');

			$date1=date_create($dateCheckout);
			$date2=date_create(date('Y-m-d'));
			$dDiff = $date1->diff($date2);
			$diff= $dDiff->format('%R').$dDiff->days;
			$defchkout = $modifier.':'.$hotel_check_out_time_mm.':00';
			$chkout_time=date("H:i:s");

			 if(($time_diff >= 0))
			 {
				 if($date1 == date('Y-m-d'))
				 {
					$current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - ((60 * 60 * $modifier)+$hotel_check_out_time_mm*60));
				 }
				 else if($diff == -1)
				 {
					$current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - ((60 * 60 * $modifier)+$hotel_check_out_time_mm*60));
				 }
				 else if($dateCheckout > date('Y-m-d'))
				 {
					$current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - ((60 * 60 * $modifier)+$hotel_check_out_time_mm*60));$chkout_time = date('H:i:s', strtotime($defchkout));
				 }
				 else
				 {
					$current_date = $dateCheckout;
					$chkout_time = date('H:i:s', strtotime($defchkout));
				 }
			 }
			 else if($diff <= 0)
				 {
					$current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - ((60 * 60 * $modifier)+$hotel_check_out_time_mm*60));
				 }
			 else
			 {
				 $current_date = $dateCheckout;
				 $chkout_time = date('H:i:s', strtotime($defchkout));
				 // Use this if the checkout date time is supposed to be extended to the system date time _sb
				 //date('Y-m-d H:i:s', strtotime($time_date_data) - ((60 * 60 * $modifier)+$hotel_check_out_time_mm*60));
			 }

            //checkout time logic
           /* $times=$this->dashboard_model->checkout_time();
            foreach($times as $time) {
                if($time->hotel_check_out_time_fr=='PM') {
					if($time->hotel_check_out_time_hr == 12) {
                      $modifier = ($time->hotel_check_out_time_hr);
					} else {
					  $modifier = ($time->hotel_check_out_time_hr + 12);
					}
                }
                else{
                    $modifier = ($time->hotel_check_out_time_hr);
                }
            }
			$current_time = date("H:i:s");
			$date = date('Y-m-d').' '.$modifier.':'.$time->hotel_check_out_time_mm.':00';
			$datetime1 = new DateTime();
			$datetime2 = new DateTime($date);
			$interval = $datetime1->diff($datetime2);
			$h = $interval->format('%h');
			$m = $interval->format('%i');
			$time_diff = round($h + $m/60,2);

			if($time_diff > 0 && $time_diff < $modifier){
				//$current_date = date('Y-m-d H:i:s', strtotime($date) - 60 * 60 * $modifier);
				$time_date_data=date('Y-m-d H:i:s');
				$current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - 60 * 60 * $modifier);
			} else {
				$time_date_data=date('Y-m-d H:i:s');
				$current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - 60 * 60 * $modifier);
				$date_checkout_modified = date('Y-m-d H:i:s', strtotime($date_checkout) +60 * 60 * $modifier);
			}

            /*echo $time_date_data."".$date_checkout_modified;
            exit();*/

            /*if($time_date_data>$date_checkout){

                $current_date=$date_checkout;
            }*/
			 $dataG = array(
               'status' => '6'
               );
             $this->db1->where('id', $grp_id);
             $queryG=$this->db1->update(TABLE_PRE.'group',$dataG);
			 
			$data = array(

               'booking_status_id' => '6',
                'cust_end_date' => $current_date,
                'checkout_time' => date("H:i:s"),
				'checkout_date' => date('Y-m-d'),
			   'booking_status_id_secondary' => ''
               );
            /*$data = array(

               'booking_status_id' => '6',
               'cust_end_date' => $current_date,
			   //'cust_end_date_actual' => date('Y-m-d'),
               'checkout_time' => date("H:i:s"),
               'booking_status_id_secondary' => ''
               );*/
             $this->db1->where('group_id', $grp_id);
             $query=$this->db1->update(TABLE_PRE.'bookings',$data);
			 //echo $this->db1->last_query();
			 //exit;
			 
			 $this->dashboard_model->update_checkout_status_stay_guest_grp($grp_id,'checkout');
			 
             if($query)
             {
                 return true;
             }
             else{
                 return false;
             }
        }
		
		
		// Single Booking Checkout
		function update_checkout_status($booking_id,$date_checkout,$dateCheckout,$dateCheckin){
            //checkout time logic
            $times=$this->dashboard_model->checkin_time();
            foreach($times as $time) {
                if($time->hotel_check_in_time_fr=='PM') {
					if($time->hotel_check_in_time_hr == 12) {
                      $modifier = ($time->hotel_check_in_time_hr);
					} else {
					  $modifier = ($time->hotel_check_in_time_hr + 12);
					}
                }
                else{
                    $modifier = ($time->hotel_check_in_time_hr);
                }
				$hotel_check_out_time_mm = $time->hotel_check_in_time_mm;
            }

            date_default_timezone_set('Asia/Kolkata');

			$current_time = date("H:i:s");
			$date = date('Y-m-d').' '.$modifier.':'.$hotel_check_out_time_mm.':00';
			$time_def = $modifier+($hotel_check_out_time_mm/60);
			$time_sys = date("H");
			$time_sys = $time_sys + (date("i")/60);
			$time_diff = $time_sys-$time_def;
			//$buffer_hr = 2;

            $time_date_data = date('Y-m-d H:i:s');
			// Main Logic for Daypilot Checkout Datetime - Single Booking _sb
			 //if(($time_diff >= 0) || ($dateCheckout <= date('Y-m-d')))

			//date differance in days..
			$date1=date_create($dateCheckout);
			$date2=date_create(date('Y-m-d'));
			$dDiff = $date1->diff($date2);
			$diff= $dDiff->format('%R').$dDiff->days;
			$defchkout = $modifier.':'.$hotel_check_out_time_mm.':00';
			$chkout_time=date("H:i:s");
			//logic for showing in Day Pilot _sb
			if(($time_diff >= 0))
			 {
				 if($date1 == date('Y-m-d'))
				 {
					$current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - ((60 * 60 * $modifier)+$hotel_check_out_time_mm*60));
				 }
				 else if($diff == -1)
				 {
					$current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - ((60 * 60 * $modifier)+$hotel_check_out_time_mm*60));
				 }
				 else if($dateCheckout > date('Y-m-d'))
				 {
					$current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - ((60 * 60 * $modifier)+$hotel_check_out_time_mm*60));$chkout_time = date('H:i:s', strtotime($defchkout));
				 }
				 else
				 {
					$current_date = $dateCheckout;
					$chkout_time = date('H:i:s', strtotime($defchkout));
				 }
			 }
			 else if($diff <= 0)
				 {
					$current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - ((60 * 60 * $modifier)+$hotel_check_out_time_mm*60));
				 }
			 else
			 {
				 $current_date = $dateCheckout;
				 $chkout_time = date('H:i:s', strtotime($defchkout));
				 // Use this if the checkout date time is supposed to be extended to the system date time _sb
				 // date('Y-m-d H:i:s', strtotime($time_date_data) - ((60 * 60 * $modifier)+$hotel_check_out_time_mm*60));
			 }


            //$current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - 60 * 60 * $modifier);
           // $date_checkout_modified = date('Y-m-d H:i:s', strtotime($date_checkout) +60 * 60 * $modifier);

            /*echo $time_date_data."".$date_checkout_modified;
            exit();*/

            /*if($time_date_data>$date_checkout_modified){
                $current_date=$date_checkout;
                $current_time = date("H:i:s",strtotime($current_date));
            }
            else{
                $current_date = $time_date_data;
                $current_time = date("H:i:s");
            }*/

			if($dateCheckin <= $dateCheckout){
				
					$current_date_actual = date('Y-m-d', strtotime($current_date));
					$data = array(
					   'booking_status_id' => '6',
						'cust_end_date' => $current_date,
						'checkout_time' => date('H:i:s'),
						'checkout_date' => date('Y-m-d'),
					   'booking_status_id_secondary' => ''
					   );
					   
					  
					 $this->db1->where('booking_id', $booking_id);
					 $query=$this->db1->update(TABLE_PRE.'bookings',$data);
					 
					// $a=$this->dashboard_model->update_checkout_status_stay_guest($booking_id,'checkout');
					 if($query)
					 {
						 return true;
					 }
					 else{
						 return false;
					 }
			}
			else{
				
				return false;
				
			}
		}
		function update_cancel_status($booking_id,$reason,$type,$cancel_date)
		{
			
			$this->db1->select('*');
			$this->db1->where('booking_id',$booking_id);
			$newquery = $this->db1->get(TABLE_PRE.'bookings');
			if($newquery){
           $booking_stat = $newquery->row()->booking_status_id;
        }
			
			$data = array(
               'booking_status_id' => '7',
			   'booking_status_id_secondary' => '',
			   'cancellation_reason' => $reason,
			   'reason_note' => $type,
			   'booking_status_id_previous'  => $booking_stat,
			   'cancel_date' =>$cancel_date
               );
			   
			   $data1 = array(
			   
			   'isCancelled' => '1'
			   );
			   
			/*   echo "<pre>";
			   print_r($data);
			   exit;
			   
			 */  
			 $this->db1->where('booking_id', $booking_id);
			 $query=$this->db1->update(TABLE_PRE.'bookings',$data);
			 if($query)
			 {
				// return print_r($data);
				$this->db1->where('booking_id', $booking_id);
				$this->db1->update('booking_charge_line_item',$data1);
				
				 return true;
			 }
			 else{
				 return false;
			 }
		}
		
		
		
		function GBupdate_cancel_status($group_id,$reason,$type)
		{    
			$data = array(
               'status' => '7',
			  'cancellation_reason' => $reason,
			   'reason_note' => $type,
               );
			
			 $this->db1->where('id', $group_id);
			 $query=$this->db1->update(TABLE_PRE.'group',$data);
			 if($query)
			 {
				 return true;
			 }
			 else{
				 return false;
			 }
		}

        function get_booking_id($room_id,$date){
            $sql="select booking_id from hotel_bookings where room_id='".$room_id."' and cust_from_date_actual<='".$date."' and cust_end_date_actual >='".$date."'";
            $query=$this->db1->query($sql);
            return $query->result();
        }
		
		function dpOffset($booking_id){
			//checkin time logic
            $times = $this->dashboard_model->checkin_time();
            foreach($times as $time) {
				$hotel_check_in_time_mm = $time->hotel_check_in_time_mm;
				if(isset($time->hotel_buffer_hour))
					$buffer = $time->hotel_buffer_hour;
				else
					$buffer = 0;
				
                if($time->hotel_check_in_time_fr=='PM') {
                    if($time->hotel_check_in_time_hr == 12) {
                      $modifier = ($time->hotel_check_in_time_hr);
					} 
					else {
					  $modifier = ($time->hotel_check_in_time_hr + 12);
					}
                }
                else{
                    $modifier = ($time->hotel_check_in_time_hr);
                }
            }

            $data = $this->dashboard_model->get_booking_details2($booking_id);

            foreach ($data as $key ) {
                
                 $actual_checkin = $key->cust_from_date;
				 //$date2 = $key->cust_from_date_actual;
				 $date2 = date("Y-m-d", strtotime($actual_checkin));
            }

			date_default_timezone_set('Asia/Kolkata');
			$current_time = date("H:i:s");
			$date = date('Y-m-d');
			$time_def = $modifier + ($hotel_check_in_time_mm/60);  // Offset Hrs, say 12
			$time_sys = date("H");
			$time_sys = $time_sys + (date("i")/60);
			$time_diff = $time_sys - $time_def;
			

            $time_date_data = date('Y-m-d H:i:s');

			$current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - ((60 * 60 * ($modifier))+$hotel_check_in_time_mm*60));  // According to hotel Checkin time, daypilot Offset (default checkin time)

            if($current_date < $actual_checkin){
                $current_date = $actual_checkin;
            }
			else if(($date == $date2) && ($time_diff <= 0))
			{
				$current_date =	$date2." 00:00:00";
			}
			else
				$current_date = $current_date;  //date('Y-m-d H:i:s', strtotime($time_date_data) - ((60 * 60 * $modifier)+$hotel_check_in_time_mm*60));
			
			return $current_date;
		} // End function dpOffset


		function update_checkin_status($booking_id)
		{
            $current_date = $this->dashboard_model->dpOffset($booking_id);
			date_default_timezone_set('Asia/Kolkata');
			
			if(isset($current_date)){
			 $data = array(
               'booking_status_id' => '5',
               'cust_from_date'=> $current_date,
			   'checkin_time' => date("H:i:s"),
			   'checkin_date' => date("Y-m-d"),
			   'booking_status_id_secondary' => ''
             );
			 $this->db1->where('booking_id', $booking_id);
			 $query=$this->db1->update(TABLE_PRE.'bookings',$data);
			 if($query) {
				 return true;
			 }
			 else {
				 return false;	
			 }
			} // end outer if
			else
				return false; 
			 
		}//End function update_checkin_status _sb

	function update_confirmcheckin_status($booking_id,$checkin_time,$group_id=NULL)
	{
        date_default_timezone_set('Asia/Kolkata');
		$c = 0;
		$i = 0;
		$k = 0;
		//    $current_date = $this->dashboard_model->dpOffset($booking_id);
		if($group_id == NULL){
						
						if(isset($booking_id)){
						 $data = array(
						   'booking_status_id' => '5',
						   //'cust_from_date'=> $current_date,
						   'checkin_time' => $checkin_time,
						   'checkin_date' => date("Y-m-d"),
						   'booking_status_id_secondary' => ''
						 );
						 
						 $this->db1->where('booking_id', $booking_id);
						 $query=$this->db1->update(TABLE_PRE.'bookings',$data);
						 if($query) {
							 return true;
						 }
						 else {
							 return false;	
						 }
						} // end outer if
						else
							return false; 
						 
		}
		else {
						
						//group  booking
					
			$gdtls = $this->dashboard_model->get_booking_details_grp_new($booking_id);  
			$ho = $this->session->userdata('user_hotel');
			
			/*echo '<pre>';
			print_r($gdtls);
			echo '</pre>';*/
			
			if(isset($gdtls)){
						
					$new_book = array(	
						'booking_id'=>$booking_id,
						'group_id'=>$group_id,
						'hotel_id' => $ho,	
						'user_id' => $this->session->userdata('user_id'),
						'cust_from_date' =>$gdtls->cust_from_date, 
						'cust_end_date' => $gdtls->cust_end_date,
						'cust_from_date_actual' => $gdtls->cust_from_date_actual,
						'cust_end_date_actual' => $gdtls->cust_end_date_actual,	
						'checkout_time' => $gdtls->checkout_time,	
						'booking_status_id' => $gdtls->booking_status_id,
					);	
					
			$this->dashboard_model->add_checkin_outData($new_book);	// for checkin checkout backup table
			
			$current_date = $this->dashboard_model->dpOffset($booking_id); // Calling function to get the dp offset checkin date time			  
			$chkinDte = date("Y-m-d", strtotime($current_date)); 
			$sysD = date('Y-m-d H:i:s');
			$sysD = date('Y-m-d H:i:s', strtotime($sysD) - ((60 * 60 * (6))));	// Setting checkin available 6 hrs after 12:00 AM
			$sysD = date("Y-m-d", strtotime($sysD)); 

					if($chkinDte <= $sysD && $gdtls->booking_status_id != 5 && $gdtls->booking_status_id != 7){
						echo 'Inside If';
						$c++; 
						$data = array(
							'booking_status_id' => '5',
						//	'cust_from_date' => $current_date,
							'checkin_time' => $checkin_time,
							'checkin_date' => date("Y-m-d"),
							'booking_status_id_secondary' => ''
						);
						
						//print_r($data);
						
						//$where = 'CAST(`cust_from_date` AS DATE) = '.'"'.date("Y-m-d", strtotime($current_date)).'"'.' AND `group_id` = '.$group_id.' AND `booking_id` = '.$gdtls->booking_id;  // _sb changed to below line 16Jul17 3:47 AM
						$where = 'group_id` = '.$group_id.' AND `booking_id` = '.$gdtls->booking_id; 
						//echo '</br	>'.$where;
						
						$this->db1->where($where);
						$query=$this->db1->update(TABLE_PRE.'bookings',$data);  // Updating Bookings tbl, single booking table
						
					}					 
					else if($gdtls->booking_status_id == 5){
						$k++;
					}
				$this->dashboard_model->update_checkin_status_stay_guest_grp($gdtls->booking_id,'checkin');
				  $i++;
				
				if(isset($current_date) && ($c > 0)){
					$dataG = array(
						'status' => (($i == $c || ($i == ($c + $k))) ? '5' : '12')	 // Partially or full check in
					);
				   
					$this->db1->where('id', $group_id);
					$queryG=$this->db1->update(TABLE_PRE.'group',$dataG); // Updating Group tbl, group master table

					 if($queryG)
					 {
						 return true;
					 }
					 else{
						 return false;
					 }
				}
			} else {
				return false;
			}
		}// end of else 
	} // End
		
	function update_customcheckin_status($booking_id,$checkin_time,$cnfrm_chk_date,$group_id=NULL)
		{
					if($group_id==NULL){
					$current_date = $this->dashboard_model->dpOffset($booking_id);
					date_default_timezone_set('Asia/Kolkata');
					
					if(isset($current_date)){
					 $data = array(
					   'booking_status_id' => '5',
					   'cust_from_date'=> $current_date,
					   'checkin_time' => $checkin_time,
					   'checkin_date' => date('Y-m-d',strtotime($cnfrm_chk_date)),
					   'booking_status_id_secondary' => ''
					 );
					 
					 
					 $this->db1->where('booking_id', $booking_id);
					 $query=$this->db1->update(TABLE_PRE.'bookings',$data);
					 if($query) {
						 return true;
					 }
					 else {
						 return false;	
					 }
					} // end outer if
					else
						return false; 
					 
				}
				else{
					
					// group booking
					
						$gdtls = $this->dashboard_model->get_booking_details_grp_new($booking_id);  
						$ho = $this->session->userdata('user_hotel');
					
			
			
			
					if(isset($gdtls)){
			
			
					$new_book = array(	
					'booking_id'=>$booking_id,
					'group_id'=>$group_id,
					'hotel_id' => $ho,	
					'user_id' => $this->session->userdata('user_id'),
					'cust_from_date' =>$gdtls->cust_from_date, 
					'cust_end_date' => $gdtls->cust_end_date,
					'cust_from_date_actual' => $gdtls->cust_from_date_actual,
					'cust_end_date_actual' => $gdtls->cust_end_date_actual,	
					'checkout_time' => $gdtls->checkout_time,	
					'booking_status_id' => $gdtls->booking_status_id,
					);				
					
					$this->dashboard_model->add_checkin_outData($new_book);	
			
			
					
				  $current_date = $this->dashboard_model->dpOffset($gdtls->booking_id);		// Calling function to get the dp offset checkin date time			  
				  $chkinDte = date("Y-m-d", strtotime($current_date)); 
				  $sysD = date('Y-m-d H:i:s');
				  $sysD = date('Y-m-d H:i:s', strtotime($sysD) - ((60 * 60 * (6))));	// Setting checkin available 6 hrs after 12:00 AM
				  $sysD = date("Y-m-d", strtotime($sysD)); 
				  //$arr[$i] = ($chkinDte.' - '.$sysD);
					if($chkinDte == $sysD && $gdtls->booking_status_id != 5 && $gdtls->booking_status_id != 7){
						$c++; 
						$data = array(
							'booking_status_id' => '5',
							'cust_from_date' => $current_date,
							//'checkin_time' => date("H:i:s"),
							//'checkin_date' => date("Y-m-d"),
							'checkin_time' => $checkin_time,
							'checkin_date' => date('Y-m-d',strtotime($cnfrm_chk_date)),
							'booking_status_id_secondary' => ''
						);
						
						$where = 'CAST(`cust_from_date` AS DATE) = '.'"'.date("Y-m-d", strtotime($current_date)).'"'.' AND `group_id` = '.$group_id.' AND `booking_id` = '.$gdtls->booking_id; 
						$this->db1->where($where);
						$query=$this->db1->update(TABLE_PRE.'bookings',$data);  // Updating Bookings tbl, single booking table
						
					}					 
					else if($gdtls->booking_status_id == 5){
						$k++;
					}
					$this->dashboard_model->update_checkin_status_stay_guest_grp($gdtls->booking_id,'checkin');
				  $i++;
				//} // End foreach
				
				//return 'i = '.$i.' c = '.$c.' k = '.$k.''.$arr;
				//return $arr;
				
				if(isset($current_date) && ($c > 0)){
					$dataG = array(
						'status' => (($i == $c || ($i == ($c + $k))) ? '5' : '12')	 // Partially or full check in
					);
				   
					$this->db1->where('id', $group_id);
					$queryG=$this->db1->update(TABLE_PRE.'group',$dataG); // Updating Group tbl, group master table

					 if($queryG)
					 {
						 return true;
					 }
					 else{
						 return false;
					 }
				}
			}
			else{
				return false;
			}
				}
				
				
		}//end of function
		
		function get_allot_kid($booking_id,$type,$groupID=''){
			
			
			
			
				if($groupID == ''){
				$this->db1->select('*');
				$this->db1->where('booking_id',$booking_id);
				$this->db1->where('booking_type',$type);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$query = $this->db1->get('kit_inventory_log');
				if(isset($query->row()->warehouse_id) && $query->row()->warehouse_id!=''){
					
					$warehouseid = $query->row()->warehouse_id;
					$kit =  $query->row()->kit_id;
					$this->session->set_userdata('warehouse', $warehouseid);
					$this->session->set_userdata('kitid', $kit);
				}
				else{
					
					$this->db1->select('*');
					$this->db1->where('default_warehouse','1');
					$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					$query1 = $this->db1->get('hotel_warehouse');
					if($query1->num_rows()>0){
							
							$warehouse = $query1->row()->id;
							$this->session->set_userdata('warehouse', $warehouse);
							$this->session->set_userdata('kitid', 'N/A');
					}
				}
				
				
				return $query->result();
				}
				else{
					
					$this->db1->select('*');
					//$this->db1->where('booking_id',$booking_id);
					$this->db1->where('group_id',$groupID);
					$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					$query = $this->db1->get('kit_inventory_log');
					if(isset($query->row()->warehouse_id) && $query->row()->warehouse_id!=''){
					
					$warehouseid = $query->row()->warehouse_id;
					$kit =  $query->row()->kit_id;
					$this->session->set_userdata('warehouse', $warehouseid);
					$this->session->set_userdata('kitid', $kit);
				}
				else{
					
					$this->db1->select('*');
					$this->db1->where('default_warehouse','1');
					$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					$query1 = $this->db1->get('hotel_warehouse');
					if($query1->num_rows()>0){
							
							$warehouse = $query1->row()->id;
							$this->session->set_userdata('warehouse', $warehouse);
							$this->session->set_userdata('kitid', 'N/A');
					}
				}
				
				
					return $query->result();
				}
				
		}
		
		
		function get_default_kit_name($kit_id_name){
			
				if($kit_id_name == 'N/A' || $kit_id_name == '' || $kit_id_name == null || $kit_id_name == 0 || $kit_id_name == '0'){
					
					return 'N/A';
				} else {
					$this->db1->select('*');
					$this->db1->where('hotel_define_kit_id',$kit_id_name);
					//$this->db1->where('booking_type',$type);
					//$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					$query = $this->db1->get('hotel_define_kit');
					
					if($query->num_rows()>=0){
						return $query->row()->kit_name;
					} else {
						return 0;
					}
				}
		}
		
		function get_default_kit_type($kit_id_name){
			
				if($kit_id_name == 'N/A' || $kit_id_name == '' || $kit_id_name == null || $kit_id_name == 0){
					
					return 'N/A';
				}
				else{
					$this->db1->select('*');
					$this->db1->where('hotel_define_kit_id',$kit_id_name);
					//$this->db1->where('booking_type',$type);
					//$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					$query = $this->db1->get('hotel_define_kit');
					if($query->num_rows()>=0){
						
						return "Complementary";
					}
				}
				
			
		}
		
		function add_kit_to_booking($data){
			
			//echo "<pre>";
			//print_r($data);exit;
			
		$query = $this->db1->insert('kit_inventory_log',$data);
		if($query){
            			return true;
        		}else{
            			return false;
        		}
	//	$query1 = $this->db1->update('hotel')	
			
		}
		
		function get_kit_name($item){
			
			
				$this->db1->select('*');
				$this->db1->where('hotel_stock_inventory_id',$item);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$query = $this->db1->get('hotel_stock_inventory');
				if($query->num_rows()>=0){
					
					return $query->row()->a_name;
					
				}
			
		}
		
		function insert_sub_stock_by_item($sub_stock_update_array){
			
			
							$query = $this->db1->insert('hotel_add_sub_stock_inventory',$sub_stock_update_array);
							if($query){
								
								return true;
							}
							else{
								return false;
								
							}
			
		}
		
	
		
		function update_stock_by_item($stock_update_array,$kit_name,$warehouse){
			
			
							$this->db1->where('hotel_stock_inventory_id',$kit_name);
							$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
							$this->db1->where('warehouse',$warehouse);
							$query = $this->db1->update('hotel_stock_inventory',$stock_update_array);
							if($query){
								
								return true;
							}
							else{
								return false;
								
							}
			
			
		}
		
		function find_kit_by_hotel($hotel_id){
			
				$this->db1->select('*');
				$this->db1->where('hotel_id',$hotel_id);
				$query = $this->db1->get('hotel_define_kit');
				if($query->num_rows() > 0){
					
					
					return $query->result();
				}
			
		}
		
		function get_item_by_kit($kitid){
			
				$hotel_id = $this->session->userdata('user_hotel');
				$this->db1->select('*');
				$this->db1->where('hotel_id',$hotel_id);
				$this->db1->where('kit_id',$kitid);
				$query = $this->db1->get('hotel_kit_mapping');
				if($query->num_rows() > 0){
					
					return $query->result();
				}
			
		}
		
		
		
		function item_checking_warhouse($booking_id,$hotel_id,$type){
			
			$warehouse=0;
			$itemno ="";
			if($booking_id){
				
				$this->db1->select('*');
				$this->db1->where('booking_id',$booking_id);
				$this->db1->where('hotel_id',$hotel_id);
				$query = $this->db1->get('hotel_bookings');
				if($query->num_rows()>=0){
					
					$roomid = $query->row()->room_id;
					//echo "roomid".$roomid;exit;
					if($roomid){
						
						$this->db1->select('*');
						$this->db1->where('room_id',$roomid);
						$this->db1->where('hotel_id',$hotel_id);
						$query = $this->db1->get('hotel_room');
						if($query->num_rows()>=0){
							
							$unit_id = $query->row()->unit_id;
								
							}
				}
			}
			
			}
			
			
			$this->db1->select('*');
			$this->db1->where('hotel_id',$hotel_id);
			$this->db1->where('id',$unit_id);
			$query = $this->db1->get('hotel_unit_type');
			
				
			if($query->num_rows()>=0){
				
				$allot_kit_id = $query->row()->kit;
				
				
			
				$this->db1->select('*');
				$this->db1->where('default_warehouse','1');
				$this->db1->where('hotel_id',$hotel_id);
				$query = $this->db1->get('hotel_warehouse');
				if($query->num_rows()>0){
					
					$warehouse = $query->row()->id;
				}
				
				
				$this->db1->select('*');
				$this->db1->where('kit_id',$allot_kit_id);
				$this->db1->where('hotel_id',$hotel_id);
				$query = $this->db1->get('hotel_kit_mapping');
				
				$item_array = array();
					$i=0;
				if($query->num_rows()>=0){
							
					
					$item_array = $query->result();
					foreach($item_array as $row1){
						
						$this->db1->select('*');
						$this->db1->where('item_id',$row1->item_name);
						$this->db1->where('hotel_id',$hotel_id);
						$this->db1->where('warehouse_id',$warehouse);
						$stock_query = $this->db1->get('hotel_stock_qty_mapping');
						if($stock_query->num_rows()> 0){
							
							$itemno = "success";
						}
						else{
							
							$itemno = "failed";
							//return $itemno;
							break;
						}
						
						
					}
					
					
				}
		}
		return $itemno;
		
			}
		
		function update_stock_qty_mapping($kit_name,$warehouse,$kit_qty){
			
						$this->db1->select('*');
						$this->db1->where('item_id',$kit_name);
						$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
						$this->db1->where('warehouse_id',$warehouse);
						$stock_qty_query = $this->db1->get('hotel_stock_qty_mapping');
						if($stock_qty_query->num_rows() > 0){
							
							$stock_qty_array['qty'] = $stock_qty_query->row()->qty - $kit_qty;
							
							$stock_qty_array['user_id'] = $this->session->userdata('user_id');
							$stock_qty_array['note']  =  "item  update successfully on".date('Y-m-d')."by".$this->session->userdata('user_id');
							
							$this->db1->where('item_id',$kit_name);
							$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
							$this->db1->where('warehouse_id',$warehouse);
							$this->db1->update('hotel_stock_qty_mapping',$stock_qty_array);
						}
							
			
			
		}
		
		function update_del_stock_inventory($item_name,$qty){
			
			
						$this->db1->select('*');
						$this->db1->where('hotel_stock_inventory_id',$item_name);
						$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
						//$this->db1->where('warehouse',$warehouse);
						$stock_query = $this->db1->get('hotel_stock_inventory');
						if($stock_query->num_rows()>=0){
							
							$stock_update_array['qty'] = $stock_query->row()->qty + $qty;
							$stock_update_array['closing_qty'] = $stock_query->row()->closing_qty + $qty;
							$stock_update_array['user_id'] = $this->session->userdata('user_id');
							$stock_update_array['note']  =  "stock update successfully on".date('Y-m-d')."by".$this->session->userdata('user_id');
							$stock_update_array['date']  =  date('Y-m-d');
							
							//update stock inventory..
							
							$this->db1->where('hotel_stock_inventory_id',$item_name);
							$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
							$this->db1->update('hotel_stock_inventory',$stock_update_array);
						}
			
			
			
		}
		
		
		function update_stock_qty_mapping_del($kit_name,$warehouse,$kit_qty){
			
						$this->db1->select('*');
						$this->db1->where('item_id',$kit_name);
						$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
						$this->db1->where('warehouse_id',$warehouse);
						$stock_qty_query = $this->db1->get('hotel_stock_qty_mapping');
						if($stock_qty_query->num_rows() > 0){
							
							$stock_qty_array['qty'] = $stock_qty_query->row()->qty + $kit_qty;
							
							$stock_qty_array['user_id'] = $this->session->userdata('user_id');
							$stock_qty_array['note']  =  "item  update successfully on".date('Y-m-d')."by".$this->session->userdata('user_id');
							
							$this->db1->where('item_id',$kit_name);
							$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
							$this->db1->where('warehouse_id',$warehouse);
							$this->db1->update('hotel_stock_qty_mapping',$stock_qty_array);
						}
							
			
			
		}
		
		
	function allot_kit_booking($booking_id,$hotel_id,$type,$groupId=''){
			
			
			$warehouse=0;
			$itemresult = $this->item_checking_warhouse($booking_id,$hotel_id,$type);
			if($itemresult == 'success'){
			if($booking_id){
				
				$this->db1->select('*');
				$this->db1->where('booking_id',$booking_id);
				$this->db1->where('hotel_id',$hotel_id);
				$query = $this->db1->get('hotel_bookings');
				if($query->num_rows()>=0){
					
					$roomid = $query->row()->room_id;
					//echo "roomid".$roomid;exit;
					if($roomid){
						
						$this->db1->select('*');
						$this->db1->where('room_id',$roomid);
						$this->db1->where('hotel_id',$hotel_id);
						$query = $this->db1->get('hotel_room');
						if($query->num_rows()>=0){
							
							$unit_id = $query->row()->unit_id;
								
							}
				}
			}
			
			}
			
			
			$this->db1->select('*');
			$this->db1->where('hotel_id',$hotel_id);
			$this->db1->where('id',$unit_id);
			$query = $this->db1->get('hotel_unit_type');
			
				
			if($query->num_rows()>=0){
				
				$allot_kit_id = $query->row()->kit;
				
				
			///////////////////	
				$this->db1->select('*');
				$this->db1->where('default_warehouse','1');
				$this->db1->where('hotel_id',$hotel_id);
				$query = $this->db1->get('hotel_warehouse');
				if($query->num_rows()>0){
					
					$warehouse = $query->row()->id;
				}
				
				
				$this->db1->select('*');
				$this->db1->where('kit_id',$allot_kit_id);
				$this->db1->where('hotel_id',$hotel_id);
				$query = $this->db1->get('hotel_kit_mapping');
				
				
				
				$update_kit_array = array();
				$stock_update_array = array();
				$sub_stock_update_array = array();
					$i=0;
				if($query->num_rows()>=0){
							
					
					$kit_array = $query->result();
					
					
					foreach($kit_array as $row){
						
						
						$update_kit_array['kit_id'] = $allot_kit_id;
						$update_kit_array['item_id'] = $row->item_name;
						$update_kit_array['qty'] = $row->qty;
						$update_kit_array['price'] = $row->unit_price;
						$update_kit_array['warehouse_id'] = $warehouse;
						$update_kit_array['hotel_id'] = $hotel_id;
						$update_kit_array['user_id'] = $this->session->userdata('user_id');
						$update_kit_array['booking_id'] = $booking_id;
						$update_kit_array['booking_type'] = $type;
						$update_kit_array['group_id'] = $groupId;
						//insert into kit_inventory_log...
						
						$results = $this->db1->get('kit_inventory_log');	
						$this->db1->insert('kit_inventory_log',$update_kit_array);
						
						
						$stock_qty=array(
							'hotel_id'=>$this->session->userdata('user_hotel'),
							'user_id'=>$this->session->userdata('user_id'),
							'warehouse_id'=>$warehouse,
							'qty'=>$row->qty,
							'item_id'=>$row->item_name,
							'note'=>'consumption',
					);
						
						
						$res = $this->update_consumption_warehouse_stock($stock_qty);
						
						// update stock inventory ..
						
						
						$this->db1->select('*');
						$this->db1->where('hotel_stock_inventory_id',$row->item_name);
						$this->db1->where('hotel_id',$hotel_id);
						//$this->db1->where('warehouse',$warehouse);
						$stock_query = $this->db1->get('hotel_stock_inventory');
						if($stock_query->num_rows()>=0){
							
							$stock_update_array['qty'] = $stock_query->row()->qty - $row->qty;
							$stock_update_array['closing_qty'] = $stock_query->row()->closing_qty - $row->qty;
							$stock_update_array['user_id'] = $this->session->userdata('user_id');
							$stock_update_array['note']  =  "stock update successfully on".date('Y-m-d')."by".$this->session->userdata('user_id');
							$stock_update_array['date']  =  date('Y-m-d');
							
							//update stock inventory..
							
							$this->db1->where('hotel_stock_inventory_id',$row->item_name);
							$this->db1->where('hotel_id',$hotel_id);
							$this->db1->update('hotel_stock_inventory',$stock_update_array);
							
							//stock sub inventory...
							
							//$sub_stock_update_array['hotel_transfer_asset_id'] = $booking_id;
							$sub_stock_update_array['hotel_stock_inventory_id'] = $row->item_name;
							//$sub_stock_update_array['date']  = date('Y-m-d H:i:s');
							$sub_stock_update_array['a_type'] = $stock_query->row()->item_type;
							$sub_stock_update_array['a_category']  =  $stock_query->row()->a_category;
							$sub_stock_update_array['item']  =  $row->item_name;
							$sub_stock_update_array['vendor'] = '';
							$sub_stock_update_array['warehouse'] = $warehouse;
							$sub_stock_update_array['link_purchase']  =  'n/a';
							
							$sub_stock_update_array['qty']  =  $row->qty;
							
							$sub_stock_update_array['opening_qty']  =  $stock_query->row()->opening_qty;
							$sub_stock_update_array['closing_qty']  = $stock_query->row()->closing_qty - $row->qty;
							
							$sub_stock_update_array['price']  =  $row->unit_price;
							$sub_stock_update_array['operation']  = "consumption";
							$sub_stock_update_array['note']  =  "consumption by Booking (".$type.') '.$booking_id;
							$sub_stock_update_array['user_id']  =  $this->session->userdata('user_id');
							$sub_stock_update_array['hotel_id']  =  $hotel_id;
						
							//print_r($sub_stock_update_array);
							
							$this->db1->insert('hotel_add_sub_stock_inventory',$sub_stock_update_array);
							
							
						}
						
						
						
						// end of updation 
						
					}//end of for
				
				//	
				
					
				}//end of if
				
					
			
				
				
			}
			else{
				
				echo "Kit";
				exit;
			}
		
		
	}
		}
	
		
		// Check In function from POP Edit Booking for Gro Booking || MC Booking
        function update_checkin_status_grp($grp_id,$booking_id1=NULL, $bk_ids = NULL)
        {
            // Pass $bk_ids as an array of the booking ids (single) while calling
            date_default_timezone_set('Asia/Kolkata');
            if($bk_ids == NULL){
                $gdtls = $this->dashboard_model->get_booking_details_grp($grp_id);   // All single booking details under the grp id
			//	print_r($gdtls);exit;
            }
            else if($bk_ids != NULL){
                $gdtls = $bk_ids;	// Array of Booking ids passed as arguments, optional (Used when we specify which rooms to checkin)
            }
            else{
                return false;
            }
			
			foreach($gdtls as $grpsinglekit){
				
				
				$booking_id =  $grpsinglekit->booking_id;
				$ho = $this->session->userdata('user_hotel');
			$this->dashboard_model->allot_kit_booking($booking_id,$ho,"sb",$grp_id);
			
			$new_book = array(	
			'booking_id'=>$grpsinglekit->booking_id,
			'group_id'=>$grpsinglekit->group_id,
			'hotel_id' => $ho,	
			'user_id' => $this->session->userdata('user_id'),
			'cust_from_date' =>$grpsinglekit->cust_from_date, 
			'cust_end_date' => $grpsinglekit->cust_end_date,
			'cust_from_date_actual' => $grpsinglekit->cust_from_date_actual,
			'cust_end_date_actual' => $grpsinglekit->cust_end_date_actual,	
			'checkout_time' => $grpsinglekit->checkout_time,	
			'booking_status_id' => $grpsinglekit->booking_status_id,
			);				
			
			$this->dashboard_model->add_checkin_outData($new_book);	
			
			}
			//exit;
			
			$i = 0; // Foreach Counter
			$c = 0; // count if check in date is sys date & not checked in
			$k = 0; // Checked In booking count
			
			if(isset($gdtls)){
				foreach($gdtls as $y){
					
				  $current_date = $this->dashboard_model->dpOffset($y->booking_id);		// Calling function to get the dp offset checkin date time			  
				  $chkinDte = date("Y-m-d", strtotime($current_date)); 
				  $sysD = date('Y-m-d H:i:s');
				  $sysD = date('Y-m-d H:i:s', strtotime($sysD) - ((60 * 60 * (6))));	// Setting checkin available 6 hrs after 12:00 AM
				  $sysD = date("Y-m-d", strtotime($sysD)); 
				  //$arr[$i] = ($chkinDte.' - '.$sysD);
					if($chkinDte == $sysD && $y->booking_status_id != 5 && $y->booking_status_id != 7){
						$c++; 
						$data = array(
							'booking_status_id' => '5',
							'cust_from_date' => $current_date,
							'checkin_time' => date("H:i:s"),
							'checkin_date' => date("Y-m-d"),
							'booking_status_id_secondary' => ''
						);
						
						$where = 'CAST(`cust_from_date` AS DATE) = '.'"'.date("Y-m-d", strtotime($current_date)).'"'.' AND `group_id` = '.$grp_id.' AND `booking_id` = '.$y->booking_id; 
						$this->db1->where($where);
						$query=$this->db1->update(TABLE_PRE.'bookings',$data);  // Updating Bookings tbl, single booking table
						
					}					 
					else if($y->booking_status_id == 5){
						$k++;
					}
					$this->dashboard_model->update_checkin_status_stay_guest_grp($y->booking_id,'checkin');
				  $i++;
				} // End foreach
				
				//return 'i = '.$i.' c = '.$c.' k = '.$k.''.$arr;
				//return $arr;
				
				if(isset($current_date) && ($c > 0)){
					$dataG = array(
						'status' => (($i == $c || ($i == ($c + $k))) ? '5' : '12')	 // Partially or full check in
					);
				   
					$this->db1->where('id', $grp_id);
					$queryG=$this->db1->update(TABLE_PRE.'group',$dataG); // Updating Group tbl, group master table

					 if($queryG)
					 {
						 return true;
					 }
					 else{
						 return false;
					 }
				}
			}
			else{
				return false;
			}			
			 
        } // End function update_checkin_status_grp _sb

		
		
		
		function update_checkin_status_grp_new($grp_id,$booking_id)
        {
            // Pass $bk_ids as an array of the booking ids (single) while calling
            date_default_timezone_set('Asia/Kolkata');
            
			
			
				
				
			//	$booking_id =  $grpsinglekit->booking_id;
				//$ho = $this->session->userdata('user_hotel');
			//$this->dashboard_model->allot_kit_booking($booking_id,$ho,"sb",$grp_id);
			
		
			//exit;
			
			$i = 0; // Foreach Counter
			$c = 0; // count if check in date is sys date & not checked in
			$k = 0; // Checked In booking count
			$gdtls = $this->dashboard_model->get_booking_details_grp_new($booking_id);  
			if(isset($gdtls)){
				
					
			$ho = $this->session->userdata('user_hotel');
			
					
						
						
					$new_book = array(	
					'booking_id'=>$booking_id,
					'group_id'=>$group_id,
					'hotel_id' => $ho,	
					'user_id' => $this->session->userdata('user_id'),
					'cust_from_date' =>$gdtls->cust_from_date, 
					'cust_end_date' => $gdtls->cust_end_date,
					'cust_from_date_actual' => $gdtls->cust_from_date_actual,
					'cust_end_date_actual' => $gdtls->cust_end_date_actual,	
					'checkout_time' => $gdtls->checkout_time,	
					'booking_status_id' => $gdtls->booking_status_id,
					);				
					
					$this->dashboard_model->add_checkin_outData($new_book);	
					
					
					
				  $current_date = $this->dashboard_model->dpOffset($gdtls->booking_id);		// Calling function to get the dp offset checkin date time			  
				  $chkinDte = date("Y-m-d", strtotime($current_date)); 
				  $sysD = date('Y-m-d H:i:s');
				  $sysD = date('Y-m-d H:i:s', strtotime($sysD) - ((60 * 60 * (6))));	// Setting checkin available 6 hrs after 12:00 AM
				  $sysD = date("Y-m-d", strtotime($sysD)); 
				  //$arr[$i] = ($chkinDte.' - '.$sysD);
					if($chkinDte == $sysD && $gdtls->booking_status_id != 5 && $gdtls->booking_status_id != 7){
						$c++; 
						$data = array(
							'booking_status_id' => '5',
							'cust_from_date' => $current_date,
							'checkin_time' => date("H:i:s"),
							'checkin_date' => date("Y-m-d"),
							'booking_status_id_secondary' => ''
						);
						
						$where = 'CAST(`cust_from_date` AS DATE) = '.'"'.date("Y-m-d", strtotime($current_date)).'"'.' AND `group_id` = '.$grp_id.' AND `booking_id` = '.$booking_id; 
						$this->db1->where($where);
						$query=$this->db1->update(TABLE_PRE.'bookings',$data);  // Updating Bookings tbl, single booking table
						
					}					 
					else if($booking_status_id == 5){
						$k++;
					}
					$this->dashboard_model->update_checkin_status_stay_guest_grp($booking_id,'checkin');
				  $i++;
			//	} // End foreach
				
				//return 'i = '.$i.' c = '.$c.' k = '.$k.''.$arr;
				//return $arr;
				
				if(isset($current_date) && ($c > 0)){
					$dataG = array(
						'status' => (($i == $c || ($i == ($c + $k))) ? '5' : '12')	 // Partially or full check in
					);
				   
					$this->db1->where('id', $grp_id);
					$queryG=$this->db1->update(TABLE_PRE.'group',$dataG); // Updating Group tbl, group master table

					 if($queryG)
					 {
						 return true;
					 }
					 else{
						 return false;
					 }
				}
			}
			else{
				return false;
			}			
			 
        } // End function update_checkin_status_grp _sb
		
		function guest_check($id){

			$query=$this->db1->where(TABLE_PRE.'guest',$id);
        if($query->num_rows()==1){
            return true;
        }else{
            return false;
        }
		}

     function room_last_checkin_date($id){
        /* $this->db1->select('cust_from_date');
         $this->db1->where('room_id',$id);
         $this->db1->where('booking_status_id',5);
         $this->db1->or_where('booking_status_id',6);*/

         $sql="select cust_from_date from hotel_bookings where room_id=".$id." and (booking_status_id=5 or booking_status_id=6) ";



         $query=$this->db1->query($sql);
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }

     }

    //Get Hotel default Checkin Time
    public function checkin_time(){
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'hotel_general_preference');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	//Get Hotel default Checkout Time
    public function checkout_time(){
         $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
         $query=$this->db1->get(TABLE_PRE.'hotel_general_preference');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
    }

    function update_broker_commission($id,$commission){


        $query="UPDATE `".TABLE_PRE."broker` SET `broker_commission_total` = `broker_commission_total` + '".$commission."' WHERE `b_id`='".$id."' ";
        $result=$this->db1->query($query);

        if($result)
        {
            return true;
        }
        else{
            return false;
        }

    }
   function update_channel_commission2($id,$commission){


        $query="UPDATE `".TABLE_PRE."channel` SET `channel_commission_total` = `channel_commission_total` + '".$commission."' WHERE `channel_id`='".$id."' ";
        $result=$this->db1->query($query);

        if($result)
        {
            return true;
        }
        else{
            return false;
        }

    }

    function update_broker_booking($data){
        $this->db1->where('booking_id',$data['booking_id']);
        $query=$this->db1->update(TABLE_PRE.'bookings',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }


    function pay_broker($id,$commission){
        $query="UPDATE `".TABLE_PRE."broker` SET `broker_commission_payed` = `broker_commission_payed` + '".$commission."' WHERE `b_id`='".$id."' ";
        $result=$this->db1->query($query);
        if($result)
        {
            return true;
        }
        else{
            return false;
        }

    }


	function get_guest_name($val){
		$this->db1->select('g_name');
		$this->db1->like('g_name', $val);
        $query=$this->db1->get(TABLE_PRE.'guest');
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return false;
        }

	}

     function get_guest_row($id){
         $this->db1->select('*');
         $this->db1->like('g_id', $id);
         $query=$this->db1->get(TABLE_PRE.'guest');
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }

     }




	function  remove_online($id){
        $this->db1->where('u_id', $id);
        $query=$this->db1->delete(TABLE_PRE.'online');
        if($query){
            return true;
        }else{
            return false;
        }
    }

     function  remove_transaction($id){


         $this->db1->where('t_booking_id', $id);
         $query=$this->db1->delete(TABLE_PRE.'transactions');
         if($query){
             return true;
         }else{
             return false;
         }
     }
	 
	  function  remove_transaction_grp($grpid){


         $this->db1->where('t_group_id', $grpid);
         $query=$this->db1->delete(TABLE_PRE.'transactions');
         if($query){
             return true;
         }else{
             return false;
         }
     }
	 

     function  remove_log($id){


         $this->db1->where('log_id', $id);
         date_default_timezone_set('Asia/Kolkata');
         $data=array(

             'end_time' => date("H:i"),
             'logged_out' =>1
         );
         $query=$this->db1->update('shift_log',$data);
         if($query){
             return true;
         }else{
             return false;
         }
     }

     function  remove_maid($id){


         $this->db1->where('room_id', $id);
         $query=$this->db1->delete(TABLE_PRE.'housekeeping_matrix');
         if($query){
             return true;
         }else{
             return false;
         }
     }

    function  all_onlines(){
        date_default_timezone_set('Asia/Kolkata');

        $this->db1->group_by('u_id');
        $this->db1->order_by('online_id', "desc");
        $this->db1->where('online_from >', date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s"))-60*60*8));
        $this->db1->where('u_id!=', $this->session->userdata('user_id'));
        $query=$this->db1->get(TABLE_PRE.'online');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

    }

    function get_user_details($id){

        $this->db->where('admin_id=',$id);
        $query=$this->db->get(TABLE_PRE.'admin_details');
        if($query->num_rows()==1){
            return $query->result();
        }else{
            return false;
        }
    }

     function get_shift_details($id){

         $this->db1->where('shift_id=',$id);
         $query=$this->db1->get(TABLE_PRE.'shift');
         if($query->num_rows()==1){
             return $query->result();
         }else{
             return false;
         }
     }

	function  add_feedback($data){
        $query=$this->db1->insert(TABLE_PRE.'feedback',$data);
        if($query){
            return true;
        }else{
            return false;
        }

    }
	function fetch_fdback_id($id){
		$this->db1->where('id',$id);
         $query=$this->db1->get(TABLE_PRE.'feedback');
         if($query->num_rows()>0){

             return $query->result();
         }

	}

		function update_feedback($feedback){
			  $this->db1->where('id',$feedback['id']);
			$query=$this->db1->update(TABLE_PRE.'feedback',$feedback);
        if($query){
            return true;
        }else{
            return false;
        }

		}
	function all_feedback(){
	$this->db1->select('*,(sleep_quality + room_quality + env_quality+ food_quality+extra+package+service+ambience+cleanliness+staff+reception+ease_booking) as tot');
        $this->db1->order_by('id','desc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $this->db1->where('user_id=',$this->session->userdata('user_id'));
        $query=$this->db1->get(TABLE_PRE.'feedback');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	function delete_feedback($fid){
        $this->db1->where_in('id',$fid);
        $query=$this->db1->delete(TABLE_PRE.'feedback');
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function send_message($data){
    $query=$this->db1->insert(TABLE_PRE.'message',$data);
    if($query){
        return true;
    }else{
        return false;
    }
}

    function get_message($from,$to){
        $this->db1->where('u_from_id=',$from);
        $this->db1->where('u_to_id=',$to);
        $this->db1->where('m_seen!=',1);
        $this->db1->group_by('m_id');
        $query=$this->db1->get(TABLE_PRE.'message');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function update_message($from,$to,$id){
        $query="UPDATE `".TABLE_PRE."message` SET `m_seen` = 1  WHERE `u_from_id`='".$from."' AND `u_to_id`='".$to."'  AND `m_id`='".$id."'";
        $result=$this->db1->query($query);
        if($result)
        {
            return true;
        }
        else{
            return false;
        }

    }

    function all_messages($to){
        $this->db1->where('u_to_id=',$to);
        //$this->db1->where('u_to_id=',$to);
        $this->db1->where('m_seen!=',1);
        $this->db1->group_by('m_id');
        $query=$this->db1->get(TABLE_PRE.'message');
        if($query->num_rows()>=0){
            return $query->num_rows();
        }else{
            return false;
        }
    }
    function all_messages_unseen($to){
        $this->db1->where('u_to_id=',$to);
        //$this->db1->where('u_to_id=',$to);
        $this->db1->where('m_seen!=',1);
        $this->db1->group_by('m_id');
        $query=$this->db1->get(TABLE_PRE.'message');
        if($query->num_rows()>=1){
            return $query->result();
        }else{
            return false;
        }
    }

    function  all_pending(){
        $this->db1->where('booking_status_id_secondary=',3);

        $this->db1->or_where('booking_status_id_secondary=',9);
       // $this->db1->group_by('booking_id');
        $query=$this->db1->get(TABLE_PRE.'bookings');

        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

    }
	
	
	 function  all_pending_by_date($noti_date){
		 
		 if($noti_date == 'today'){
			
			 $today = date('Y-m-d');
			  $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
			$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d'));
			 $this->db1->where('booking_status_id_secondary=',3);

			// $this->db1->or_where('booking_status_id_secondary=',9);
		   
			
			 $query=$this->db1->get(TABLE_PRE.'bookings');
			 
		 }
       

        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

    }
	

	function add_unit_type($data,$kit_id){
        $query=$this->db1->insert(TABLE_PRE.'unit_type',$data);

		$s11=$this->db1->insert_id();
	//	echo "S!!".$s11;
		if($s11>=0){

			$data = array(
					'primary_kit_id' => $kit_id,
					'unit_type_id' => $s11,
					'secondary_kit_id'=>'0'
					);

			$query=$this->db1->insert(TABLE_PRE.'unit_kit_mapping',$data);
			$s=$this->db1->insert_id();
        }

        if($query){
            return $s11;
        }else{
            return false;
        }

    }
	
	
	
	function get_rate_plan_value($hotel_id){
				
				$this->db1->select('populate_status');
				$this->db1->where('hotel_id',$hotel_id);
				$query = $this->db1->get('hotel_rateplan_index');
				if($query->num_rows()>= 0){
					
					return $query->row()->populate_status;
				}
				else{
						return false;
				}
			}

	
	
	function get_room_by_unittype($unitId,$hotel_id){
		
		
		$this->db1->select('*');
		$this->db1->where('hotel_id',$hotel_id);
		$this->db1->where('unit_id',$unitId);
		$query = $this->db1->get('hotel_room');
		if($query->num_rows()>=0){
			
			return $query->row();
			
		}
		else{
			
			return false;
		}
	}
	
	function all_unit_type(){
		
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->order_by('id','desc');
        $query=$this->db1->get(TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	function getUnitTypeName(){
        //$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->order_by('name','asc');
        $query=$this->db1->get(TABLE_PRE.'unit_type_name');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }






	function delete_unit_type($fid){
		
		
		
		$plan_rate_type = $this->rate_plan_model->all_rate_plan_type();
		$booking_source = $this->rate_plan_model->all_booking_source_type();
		$all_unit_type = $this->rate_plan_model->all_units1();
		$all_meal_plan = $this->rate_plan_model->all_meal_plan();
		$occupancy = $this->rate_plan_model->all_occupancy();
		
		
		
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
						  $this->db1->where('unit_type_id',$fid);
			              $this->db1->delete('rate_plan');
		
		
	//	....................
        $this->db1->where('id=',$fid);
        $query=$this->db1->delete(TABLE_PRE.'unit_type');
        if($query){
            return true;
        }else{
            return false;
        }
    }

	function get_unit_name($unt){
		
		$this->db1->select('id,unit_name');
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('unit_type=',$unt);
        $query=$this->db1->get(TABLE_PRE.'unit_type');
		
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return 0;
     
        }
    }
	
	//previous version ...of get_unit_name
	function get_unit_name_oldversion($unt){
		$this->db1->select('id,unit_name');
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('unit_type=',$unt);
        $query=$this->db1->get(TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return 0;
        }
    }

     function get_unit_exact($id){
         /*$this->db1->select('*');
         $this->db1->where('unit_type=',$id);
         $query=$this->db1->get(TABLE_PRE.'unit_type');
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }*/

         $this->db1->where('id=',$id);
         $query=$this->db1->get(TABLE_PRE.'unit_type');
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }

	}

	function get_unit_exact_by_unitno($no){ // Get Details of Units based on the Unit No _sb dt1_10_16
        //SELECT * FROM `hotel_room` WHERE `room_no` = 505
        $this->db1->select('unit_id');
        $this->db1->where('room_no =',$no);
        $query=$this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
		}
	}

	function get_unit_class($untID){
		$this->db1->select('*');
        $this->db1->where('id',$untID);
        $query=$this->db1->get(TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->row_array();
        }else{
            return false;
        }
    }

	function get_all_room_feature(){
		
		$this->db1->select('room_feature_id,room_feature_name');
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
        //$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'room_features');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	function get_unit_details($untID){
		$this->db1->select('*');
        $this->db1->where('id=',$untID);
        $query=$this->db1->get(TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->row_array();
        }else{
            return false;
        }
    }

	function get_all_room_feature_id($untID){
		$this->db1->select('*');
        $this->db1->where('room_id=',$untID);
        $query=$this->db1->get(TABLE_PRE.'room_room_features');
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return false;
        }
    }


	function get_booking_details($bookingID){
		$this->db1->select('hotel_bookings.*,hotel_guest.*');
        $this->db1->from('hotel_bookings');
		$this->db1->where('hotel_bookings.booking_id=',$bookingID);
		$this->db1->where('hotel_bookings.hotel_id=',$this->session->userdata('user_hotel'));
		$this->db1->join('hotel_guest', 'hotel_bookings.guest_id = hotel_guest.g_id');
        $query=$this->db1->get();
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows() ==1 )
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

	function get_booking_details_r($bookingID){
		$this->db1->select('hotel_bookings.*,hotel_guest.*');
        $this->db1->from('hotel_bookings');
		$this->db1->where('hotel_bookings.booking_id=',$bookingID);
		$this->db1->join('hotel_guest', 'hotel_bookings.guest_id = hotel_guest.g_id');
        $query=$this->db1->get();
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows() ==1 )
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
	
	function get_booking_details_arr($bookingID){
		$this->db1->select('hotel_bookings.*,hotel_guest.*');
        $this->db1->from('hotel_bookings');
		$this->db1->where('hotel_bookings.booking_id=',$bookingID);
		$this->db1->join('hotel_guest', 'hotel_bookings.guest_id = hotel_guest.g_id');
        $query=$this->db1->get();
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows() ==1 )
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
	
    function get_booking_details_by_date($date,$id=NULL){
		//echo $this->db->database;
		// echo '<br>'.$this->db1->database;exit;
        $this->db1->select('hotel_bookings.*,hotel_guest.*');
        $this->db1->from('hotel_bookings');
        //$this->db1->where('hotel_bookings.booking_id=',$bookingID);
        $this->db1->where('hotel_bookings.cust_from_date_actual <=',$date);
         $this->db1->where('hotel_bookings.cust_end_date_actual >=',$date);
         $this->db1->where('hotel_bookings.booking_status_id =',5);
         $this->db1->where('hotel_bookings.hotel_id',$id);

        $this->db1->join('hotel_guest', 'hotel_bookings.guest_id = hotel_guest.g_id');
        $this->db1->join('hotel_room', 'hotel_bookings.room_id = hotel_room.room_id');
        $query=$this->db1->get();
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows() > 0 )
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }


	function get_booking_details_unique($bookingID){
         $this->db1->select('*');
         $this->db1->from('hotel_bookings');
         $this->db1->where('hotel_bookings.booking_id=',$bookingID);
         $this->db1->join('hotel_guest', 'hotel_bookings.guest_id = hotel_guest.g_id');
         $query=$this->db1->group_by('booking_id');
         $query=$this->db1->get();
         //echo "<pre>";
         //print_r($query->result());
         //exit;
         if($query->num_rows() ==1 )
         {
             return $query->row();
         }
         else
         {
             return false;
         }
     }

     function get_booking_details2($bookingID){
         $this->db1->select('*');
         $this->db1->from('hotel_bookings');
         $this->db1->where('hotel_bookings.booking_id=',$bookingID);
        // $this->db1->join('hotel_guest', 'hotel_bookings.guest_id = hotel_guest.g_id');
         $query=$this->db1->get();
         //echo "<pre>";
         //print_r($query->result());
         //exit;
         if($query->num_rows() > 0)
         {
             return $query->result();
         }
         else
         {
             return false;
         }
     }
     function  insert_shadow_booking($data){

         $query=$this->db1->insert(TABLE_PRE.'bookings_shadow',$data);
         if($query){
             return true;
         }else{
             return false;
         }

     }


     function all_units(){
         //$this->db1->where('id',$unit_id);
         //$this->db1->order_by('unit_id', 'ASC');
		// $this->db1->where('user_id',$this->session->userdata('user_id'));
         $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
         $query = $this->db1->get(TABLE_PRE.'unit_type');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }
	 
	// Returns a particular service details with totals for a particuler Booking (please put pass the booking type as well for grp bookings)
    function get_service_details($id,$booking_id){
        /*$this->db1->select('*');
        $this->db1->where('service_id=',$id);
        $this->db1->where('booking_id=',$booking_id);
		$query=$this->db1->get(TABLE_PRE.'service_mapping');*/
		
		//old sql query...previously written ....
		/*$sql = "SELECT `service_id`, SUM(`qty`) AS qty, `s_price` AS s_price ,SUM(`tax`) AS tax, SUM(`total`) AS total FROM `hotel_service_mapping`
		WHERE (`booking_id`='".$booking_id."') AND (`service_id`='".$id."') 
		GROUP BY `service_id`";
		*/
		//Uncomment to get the old code back (New code is appropiate for showing the services in booking edit pages)
		
		// change sql query on 19/12/16...NB  remove this line (`booking_id`='".$booking_id."') AND
		$sql = "SELECT `service_id`, SUM(`qty`) AS qty, `s_price` AS s_price ,SUM(`tax`) AS tax, SUM(`total`) AS total FROM `hotel_service_mapping`
		WHERE (`booking_id`='".$booking_id."') AND (`service_id`='".$id."') AND (`hotel_id`='".$this->session->userdata('user_hotel')."') 
		GROUP BY `service_id`";

        $query=$this->db1->query($sql); // Running the query
		
		if($query->num_rows() > 0){
            return $query->row(); // Returning the rows if there are (+)ive rows
        }
		else {
            return false;
        }
		
    } // End Function _sb dt18_10_16
	
	function get_services($id,$booking_id){
			 
		$this->db1->distinct('service_id');
	    $this->db1->select_sum('s_price');
		$this->db1->where('service_id=',$id);
        $this->db1->where('booking_id=',$booking_id);
		$query=$this->db1->get(TABLE_PRE.'service_mapping');
		if($query->num_rows() > 0){
            return $query->row();
        } else {
            return false;
        }
		
    }
	
	

	 function get_service_details1($id){
        $this->db1->select('*');
       $this->db1->where('s_id=',$id);
        $query=$this->db1->get(TABLE_PRE.'service');
        if($query->num_rows() > 0){
            return $query->row();
        } else {
            return false;
        }
    }

  function get_service_name($id){
	  $this->db1->select('s_name');
	  $this->db1->where('s_id',$id);
	  $query=$this->db1->get(TABLE_PRE.'service');
	if($query){
		return $query->row();
	}else{
		return false;
	}
  }
    function get_charge_details($id){
        $this->db1->select('*');
        $this->db1->where('crg_id=',$id);
        $query=$this->db1->get('hotel_extra_charges_mapping');
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }
	
	function get_charge_tax($id){
        $this->db1->select('crg_tax');
        $this->db1->where('crg_id=',$id);
        $query=$this->db1->get(TABLE_PRE.'charges');
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }
	
	 function get_charge_details2($id,$booking_id){
        /*$this->db1->select('*');
        $this->db1->where('service_id=',$id);
        $this->db1->where('booking_id=',$booking_id);
		$query=$this->db1->get(TABLE_PRE.'service_mapping');*/
		//Uncomment to get the old code back (New code is appropiate for showing the services in booking edit pages)
		$sql = "SELECT `service_id`, SUM(`qty`) AS qty, `s_price` AS s_price ,SUM(`tax`) AS tax, SUM(`total`) AS total FROM `hotel_service_mapping`
		WHERE (`booking_id`='".$booking_id."') AND (`service_id`='".$id."') 
		GROUP BY `service_id`";

        $query=$this->db1->query($sql); // Running the query
		
		if($query->num_rows() > 0){
            return $query->row(); // Returning the rows if there are (+)ive rows
        }
		else {
            return false;
        }
		
    }
	 function get_service_details2($booking_id){
        /*$this->db1->select('*');
        $this->db1->where('service_id=',$id);
        $this->db1->where('booking_id=',$booking_id);
		$query=$this->db1->get(TABLE_PRE.'service_mapping');*/
		//Uncomment to get the old code back (New code is appropiate for showing the services in booking edit pages)
		$sql = "SELECT `service_id`, SUM(`qty`) AS qty, `s_price` AS s_price ,SUM(`tax`) AS tax, SUM(`total`) AS total FROM `hotel_service_mapping`
		WHERE (`booking_id`='".$booking_id."') AND (`sm_booking_type`='sb')
		GROUP BY `service_id`";

        $query=$this->db1->query($sql); // Running the query
		
		if($query->num_rows() > 0){
            return $query->result(); // Returning the rows if there are (+)ive rows
        }
		else {
            return false;
        }
		
    }
	 function get_service_details3($booking_id){
        /*$this->db1->select('*');
        $this->db1->where('service_id=',$id);
        $this->db1->where('booking_id=',$booking_id);
		$query=$this->db1->get(TABLE_PRE.'service_mapping');*/
		//Uncomment to get the old code back (New code is appropiate for showing the services in booking edit pages)
		$sql = "SELECT `service_id`, SUM(`qty`) AS qty, `s_price` AS s_price ,SUM(`tax`) AS tax, SUM(`total`) AS total FROM `hotel_service_mapping`
		WHERE (`booking_id`='".$booking_id."') AND (`sm_booking_type`='gb') AND (`hotel_id`='".$this->session->userdata('user_hotel')."')
		GROUP BY `service_id`";

        $query=$this->db1->query($sql); // Running the query
		
		if($query->num_rows() > 0){
            return $query->row(); // Returning the rows if there are (+)ive rows
        }
		else {
            return false;
        }
		
    }
	


	function get_hotel_room($roomID){
		$this->db1->select('hotel_room.room_no,hotel_hotel_details.hotel_name');
        $this->db1->from('hotel_room');
		$this->db1->where('hotel_room.room_id=',$roomID);
		$this->db1->join('hotel_hotel_details', 'hotel_room.hotel_id = hotel_hotel_details.hotel_id');
        $query=$this->db1->get();
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

	function get_brokerNameByID($brokerID){
		$this->db1->select('*');
        $this->db1->where('b_id=',$brokerID);
        $query=$this->db1->get(TABLE_PRE.'broker');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }
    function get_channelNameByID($channelID){
        $this->db1->select('*');
        $this->db1->where('channel_id=',$channelID);
        $query=$this->db1->get(TABLE_PRE.'channel');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }
    function get_brokerCommissionByID($brokerID){
        $this->db1->select('broker_commission');
        $this->db1->where('b_id=',$brokerID);
        $query=$this->db1->get(TABLE_PRE.'broker');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }
    function get_channelCommissionByID($brokerID){
        $this->db1->select('channel_commission');
        $this->db1->where('channel_id=',$brokerID);
        $query=$this->db1->get(TABLE_PRE.'channel');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

	function get_brokerName(){
		$this->db1->select('b_id,b_name');
        $query=$this->db1->get(TABLE_PRE.'broker');
        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }
	function get_amountPaidByBookingID($amountPaid){
		$sql = "SELECT SUM(t_amount) as tm FROM `hotel_transactions` WHERE t_booking_id ='$amountPaid' AND t_status = 'Done' GROUP BY t_booking_id";
		$query = $this->db1->query($sql);
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }
    function get_amountPaidByGroupID($gid){
        $sql = "SELECT SUM(t_amount) as tm FROM `hotel_transactions` WHERE t_group_id ='$gid' AND t_status = 'Done' GROUP BY t_group_id";
        $query = $this->db1->query($sql);
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

	function getAmountByGroupID($amountPaid){
        $sql = "SELECT SUM(price) as tm FROM `hotel_groups_bookings_details` WHERE group_booking_id ='$amountPaid' GROUP BY group_booking_id";
        $query = $this->db1->query($sql);
        if($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
	function getAD($amountPaid){
        $sql = "SELECT additional_discount as ad FROM `hotel_group` WHERE id ='$amountPaid' ";
        $query = $this->db1->query($sql);
        if($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }





	// Updates the stay details on booking edit page
    function add_stay_details($stay,$booking_id){
        $this->db1->where('booking_id', $booking_id);
        $query = $this->db1->update(TABLE_PRE.'bookings', $stay);
        //echo  $this->db1->last_query();
		//exit;
        if($query){
            return true;
        }else{
            return false;
        }

    }
	
	

    function add_pay_info($payInfo,$booking_id){
        $this->db1->where('booking_id', $booking_id);
        $query = $this->db1->update(TABLE_PRE.'bookings', $payInfo);
        //echo  $this->db1->last_query();
		//exit;
        if($query){
            return true;
        }else{
            return false;
        }

    }


	function get_booking_status($sID){
		$this->db1->select('*');
        $this->db1->where('status_id=',$sID);
        $query=$this->db1->get('booking_status_type');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

    function add_guest_info($guest,$guest_id,$preference,$booking_id,$guest2,$extra_guest){
		$this->db1->where('booking_id', $booking_id);
        $qry = $this->db1->update(TABLE_PRE.'bookings', $preference);
        $query1=count($this->dashboard_model->get_guest_row($guest_id));

		if($query1==1){

		 $query=$this->db1->insert(TABLE_PRE.'guest',$guest2);
          if($query){
              $g_id=$this->db1->insert_id();
			   $this->dashboard_model->edit_booking($booking_id,$g_id);
         }else{
             return false;
         }



		}



        $query2=$this->db1->insert(TABLE_PRE.'extra_guest',$extra_guest);

        if($query && $query2){
            return true;
        }else{
            return false;
        }

    }


	function all_feedbackByID($booking_id){
		$this->db1->select('*,(sleep_quality + room_quality + env_quality+ food_quality+extra+package+service+ambience+cleanliness+staff+reception+ease_booking) as tot');
        $this->db1->where('booking_id', $booking_id);
        $this->db1->where('hotel_id', $this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'feedback');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }


    function  add_channel($data){
        $query=$this->db1->insert(TABLE_PRE.'channel',$data);
        if($query){
            return true;
        }else{
            return false;
        }



    }

	function all_channels(){
        $this->db1->order_by('channel_id','desc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'channel');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }


	function get_channel_details($b_id_edit){
		$this->db1->select('*');
        $this->db1->where('channel_id=',$b_id_edit);
        $query=$this->db1->get(TABLE_PRE.'channel');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
	}


	function edit_channel($channel,$channel_id){
		$this->db1->where('channel_id', $channel_id);
		$query = $this->db1->update(TABLE_PRE.'channel', $channel);
		if($query){
			return true;
		}else{
			return false;
		}
	}


    function delete_channel($channel_id){
        $this->db1->where_in('channel_id',$channel_id);
        $query=$this->db1->delete(TABLE_PRE.'channel');
        if($query){
            return true;
        }else{
            return false;
        }
    }
     function delete_transaction($id){
         $this->db1->where_in('t_booking_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'transactions');
         if($query){
             return true;
         }else{
             return false;
         }
     }
     function delete_booking($id){
         $this->db1->where_in('booking_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'bookings');
		   $this->db1->where('booking_id',$id);
         $query1=$this->db1->delete(TABLE_PRE.'bookingbk');
         if($query && $query1 ){
             return true;
         }else{
             return false;
         }
     }
	 
	  function delete_stay_guest($id){
         $this->db1->where('booking_id',$id);
         $query=$this->db1->delete('hotel_stay_guest');
         if($query){
             return true;
         }else{
             return false;
         }
     }
	 
	 
     function delete_group($id){
         $this->db1->where_in('id',$id);
         $query=$this->db1->delete(TABLE_PRE.'group');
         if($query){
             return true;
         }else{
             return false;
         }
     }

     function delete_group_details($id){
         $this->db1->where_in('group_booking_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'groups_bookings_details');
         if($query){
             return true;
         }else{
             return false;
         }
     }

     function all_channel_limit(){
        $this->db1->order_by('channel_id','desc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $this->db1->where('user_id=',$this->session->userdata('user_id'));
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'channel');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function update_channel_commission($channel_id,$commission)
    {
        $query="UPDATE `".TABLE_PRE."channel` SET `channel_commission_total` = `channel_commission_total` + '".$commission."' WHERE `channel_id`='".$channel_id."' ";
        $result=$this->db1->query($query);

        if($result)
        {
            return true;
        }
        else{
            return false;
        }

    }

    function update_channel_booking($data){
        $this->db1->where('booking_id',$data['booking_id']);
        $query=$this->db1->update(TABLE_PRE.'bookings',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function pay_channel($channel_id,$commission){
        $query="UPDATE `".TABLE_PRE."channel` SET `channel_commission_payed` = `channel_commission_payed` + '".$commission."' WHERE `channel_id`='".$channel_id."' ";
        $result=$this->db1->query($query);
        if($result)
        {
            return true;
        }
        else{
            return false;
        }

    }

    function  rule_create($data){

        $query=$this->db1->insert(TABLE_PRE.'service_rule',$data);
        if($query){
            return $this->db1->insert_id();

        }else{
            return 'Some Database Error Occurred';
        }

       // return 'ok';

    }

    function  insert_service($data){

        $query=$this->db1->insert(TABLE_PRE.'service',$data);
        if($query){
            return $this->db1->insert_id();

        }else{
            return 'Some Database Error Occurred';
        }

        // return 'ok';

    }

    function add_asset($asset){
        $query=$this->db1->insert(TABLE_PRE.'assets',$asset);
        if($query){
            return true;
        }else{
            return false;
        }
    }
	function update_asset($asset){

			$this->db1->where('a_id',$asset['a_id']);
			$query=$this->db1->update(TABLE_PRE.'assets',$asset);
		if($query){
			return true;
		}
}

	function update_asset_type($asset_type){
		$id=$asset_type['asset_type_id'];
		$this->db1->where('asset_type_id',$id);
		$query=$this->db1->update(TABLE_PRE.'asset_type',$asset_type);
		if($query){
			 return true;
		}
	}
	function view_assets($id){
		 $this->db1->where('a_id',$id);
        $query = $this->db1->get(TABLE_PRE.'asset');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}


    function all_assets_limit(){
        $this->db1->order_by('a_id','desc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
       // $this->db1->where('user_id=',$this->session->userdata('user_id'));
        $query=$this->db1->get(TABLE_PRE.'assets');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	function fetch_asset_by_id($id){
		$this->db1->where('hotel_stock_inventory_id',$id);
		$query=$this->db1->get(TABLE_PRE.'stock_inventory');
		if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	function fetch_assets_by_id($id){
		$this->db1->where('a_id',$id);
		$query=$this->db1->get(TABLE_PRE.'assets');
		if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }

	}
	function update_stock_invent($asset){
		$this->db1->where('hotel_stock_inventory_id',$asset['hotel_stock_inventory_id']);
		$query=$this->db1->update(TABLE_PRE.'stock_inventory',$asset);
		if($query){
             return true;
         }else{
             return false;
         }
	}

	function delete_assets($id){
		$this->db1->where('a_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'assets');
         if($query){
             return true;
         }else{
             return false;
         }

	}
    function all_services(){
       // $this->db1->order_by('a_id','desc');
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'service');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
    function  service_booking_match($booking_id,$service_rule){

       if ($service_rule == '' || $service_rule == NULL) {
            $sql = "SELECT * FROM `hotel_bookings` LEFT OUTER JOIN `hotel_guest` ON `hotel_bookings`.`guest_id`=`hotel_guest`.`g_id`  LEFT OUTER JOIN `hotel_room` ON `hotel_bookings`.`room_id`= `hotel_room`.`room_id`
            WHERE booking_id=".$booking_id." and hotel_room.hotel_id=".$this->session->userdata('user_hotel')."";
        }
        else{
             $sql = "SELECT * FROM `hotel_bookings` LEFT OUTER JOIN `hotel_guest` ON `hotel_bookings`.`guest_id`=`hotel_guest`.`g_id`  LEFT OUTER JOIN `hotel_room` ON `hotel_bookings`.`room_id`= `hotel_room`.`room_id`
            WHERE booking_id=".$booking_id." AND ".$service_rule." and hotel_id=".$this->session->userdata('user_hotel')."";
        }

        $query=$this->db1->query($sql);
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

     function  room_feature_all($room_id){

         $sql = "SELECT * FROM `hotel_room_room_features` LEFT OUTER JOIN `hotel_room_features` ON `hotel_room_room_features`.`room_feature_id`=`hotel_room_features`.`room_feature_id` WHERE room_id=".$room_id."  ";

         $query=$this->db1->query($sql);
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }

    function  add_service_to_booking_db($data){

        $sql1="UPDATE `hotel_bookings` SET service_price = service_price +'".$data['service_price']."'    WHERE booking_id ='".$data['booking_id']."' ";
        $sql2="UPDATE `hotel_bookings` SET  service_id=concat(service_id,'".$data['service_id']."')  WHERE booking_id ='".$data['booking_id']."' ";


        /* $this->db1->where('booking_id',$data['booking_id']);*/
        $query1=$this->db1->query($sql1);
        $query2=$this->db1->query($sql2);
        if($query1 && $query2){
            return 'Service Added Successfully';
        }else{
            return 'DB Error';
        }

    }

	function add_service_to_mapping($services){
		$query=$this->db1->insert(TABLE_PRE.'service_mapping',$services);
		if($query){
			return true;
		}
		else{
			return false;
		}
	}
    function  add_charge_to_booking_db($data){

        $sql1="UPDATE `hotel_bookings` SET booking_extra_charge_amount = booking_extra_charge_amount +'".$data['booking_extra_charge_amount']."'    WHERE booking_id ='".$data['booking_id']."' ";
        $sql2="UPDATE `hotel_bookings` SET  booking_extra_charge_id=concat(booking_extra_charge_id,'".$data['booking_extra_charge_id']."')  WHERE booking_id ='".$data['booking_id']."' ";


        /* $this->db1->where('booking_id',$data['booking_id']);*/
        $query1=$this->db1->query($sql1);
        $query2=$this->db1->query($sql2);
        if($query1 && $query2){
            return 'Charge Added Successfully';
        }else{
            return 'DB Error';
        }

    }

    function  add_charge_to_booking_db_group($data){

        $sql1="UPDATE `hotel_group` SET charges_cost = charges_cost +'".$data['booking_extra_charge_amount']."'    WHERE id ='".$data['booking_id']."' ";
        $sql2="UPDATE `hotel_group` SET  charges_id=concat(charges_id,'".$data['booking_extra_charge_id']."')  WHERE id ='".$data['booking_id']."' ";


        /* $this->db1->where('booking_id',$data['booking_id']);*/
        $query1=$this->db1->query($sql1);
        $query2=$this->db1->query($sql2);
        if($query1 && $query2){
            return 'Charge Added Successfully';
        }else{
            return 'DB Error';
        }

    }

    function add_asset_type($asset_type){
        $query=$this->db1->insert(TABLE_PRE.'asset_type',$asset_type);
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function all_assets_tasks(){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
    //    $this->db1->where('user_id',$this->session->userdata('user_id'));  
        $this->db1->order_by('asset_type_id','desc');
        $query=$this->db1->get(TABLE_PRE.'asset_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function all_assets_types(){
        $this->db1->order_by('asset_type_id','desc');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
      //  $this->db1->where('user_id',$this->session->userdata('user_id'));  
        $query = $this->db1->get(TABLE_PRE.'asset_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	function all_assets_category(){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
       // $this->db1->where('user_id',$this->session->userdata('user_id'));  
		 $this->db1->order_by('asset_category_id','desc');
        $query = $this->db1->get(TABLE_PRE.'asset_category');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	function fetch_type_by_id($id){
		$this->db1->where('asset_type_id',$id);
        $query = $this->db1->get(TABLE_PRE.'asset_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}

	function delete_a_type($id){
		 $this->db1->where('asset_type_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'asset_type');
         if($query){
             return true;
         }else{
             return false;
         }
	}

	function all_maid(){
		$this->db1->order_by('maid_id','desc');
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));  
       // $this->db1->where('staff_availability','Available');
        $query = $this->db1->get(TABLE_PRE.'housekeeping_maid');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

	}

    function  add_maid($data){
        $query=$this->db1->insert(TABLE_PRE.'housekeeping_maid',$data);
        if($query){
            return true;
        }else{
            return false;
        }



    }

    function  assign_maid($data){
        $query=$this->db1->insert(TABLE_PRE.'housekeeping_matrix',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function assigned_maid_update($rum_id,$data){
        $this->db1->where('room_id=',$rum_id);
        $query=$this->db1->update(TABLE_PRE.'housekeeping_matrix',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function check_if_room_ok($rum_id){
        $this->db1->where('room_id',$rum_id);
        $query = $this->db1->get(TABLE_PRE.'housekeeping_matrix');
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    function all_maids(){
       // $this->db1->order_by('asset_type_id','desc');
		$this->db1->where('type','Maid');
		//$this->db1->where('maid_status','0');
		$this->db1->where('staff_availability','Available');
        $query = $this->db1->get(TABLE_PRE.'housekeeping_maid');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	function all_auditor(){
		$this->db1->where('type','Auditor');
        $query = $this->db1->get(TABLE_PRE.'housekeeping_maid');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
     function all_maids_shuffle(){
         $this->db1->order_by('maid_id', 'RANDOM');
         // $this->db1->order_by('asset_type_id','desc');
         $query = $this->db1->get(TABLE_PRE.'housekeeping_maid');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }
    function all_maids_limit(){
        $this->db1->where('maid_status =',0);


        $query = $this->db1->get(TABLE_PRE.'housekeeping_maid');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
    function update_maid($maid){

        $this->db1->where('maid_id=',$maid['maid_id']);
        $query=$this->db1->update(TABLE_PRE.'housekeeping_maid',$maid);
        if($query){
            return true;
        }else{
            return false;
        }
    }

     function get_hotel_contact_details($hotel_id){
         $this->db1->where('hotel_id',$hotel_id);
         $query = $this->db1->get(TABLE_PRE.'hotel_contact_details');
         if($query->num_rows()>0){
             return $query->row_array();
         }
         else{
             return false;
         }

     }
	 function insert_shadow_transaction($data1){
		$query=$this->db1->insert(TABLE_PRE.'transactions_shadow',$data1);
        if($query){
            return true;
        }else{
            return false;
        }

	 }

     function get_transaction_details($book_id)
     {
		 $this->db1->select('*');
         $this->db1->where('t_booking_id',$book_id);
         $query = $this->db1->get(TABLE_PRE.'transactions');

         if($query->num_rows()>0){
             return $query->row();
         }
         else{
             return false;
         }
     }
	 
	 
	 
	  function get_transaction_cancel($book_id)
     {
		 $this->db1->select('*');
         $this->db1->where('t_booking_id',$book_id);
		 $this->db1->where('transaction_type_id',6);
         $query = $this->db1->get(TABLE_PRE.'transactions');

         if($query->num_rows()>0){
             return $query->row();
         }
         else{
             return false;
         }
     }
	 
	  function get_transaction_cancel_by_grp($book_id)
     {
		 $this->db1->select('*');
         $this->db1->where('t_group_id',$book_id);
		 $this->db1->where('transaction_type_id',6);
         $query = $this->db1->get(TABLE_PRE.'transactions');

         if($query->num_rows()>0){
             return $query->row();
         }
         else{
             return false;
         }
     }
	  function get_transaction_refund_single($book_id)
     {
		 $this->db1->select('*');
         $this->db1->where('t_booking_id',$book_id);
		 $this->db1->where('transaction_type_id',2);
         $query = $this->db1->get(TABLE_PRE.'transactions');

         if($query->num_rows()>0){
             return $query->row();
         }
         else{
             return false;
         }
     }
	 
	  function get_transaction_refund_by_grp($book_id)
     {
		 $this->db1->select('*');
         $this->db1->where('t_group_id',$book_id);
		 $this->db1->where('transaction_type_id',2);
         $query = $this->db1->get(TABLE_PRE.'transactions');

         if($query->num_rows()>0){
             return $query->row();
         }
         else{
             return false;
         }
     }
	 
     function get_payment_details($book_id)
     {
         $this->db1->select('*');
         $this->db1->from(TABLE_PRE.'room');
         $this->db1->join(TABLE_PRE.'bookings', 'hotel_room.room_id = hotel_bookings.room_id');
         $this->db1->where('booking_id',$book_id);
         $query = $this->db1->get();

         if($query->num_rows()>0){
             return $query->result();
         }
         else{
             return false;
         }
     }

     function get_total_payment($booking_id)
     {

         $this->db1->where('t_booking_id',$booking_id);
         $this->db1->select_sum('t_amount');
         $query = $this->db1->get(TABLE_PRE.'transactions');

         if($query->num_rows()>0){
             return $query->result();
         }
         else{
             return false;
         }
     }
	 function get_total_payment_1($booking_id)
     {
		 $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
         $this->db1->where('t_booking_id',$booking_id);
         $this->db1->select_sum('t_amount');
		
         $query = $this->db1->get(TABLE_PRE.'transactions');

         if($query->num_rows()>0){
             return $query->row();
         }
         else{
             return false;
         }
     }

     function  room_maid_match($room_id){

         $sql = "SELECT * FROM `hotel_housekeeping_matrix` LEFT OUTER JOIN `hotel_housekeeping_maid` ON `hotel_housekeeping_matrix`.`maid_id`=`hotel_housekeeping_maid`.`maid_id`  WHERE room_id=".$room_id." AND `status`=0 GROUP BY `room_id`, `hotel_housekeeping_matrix`.`maid_id` ";

         $query=$this->db1->query($sql);
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }

     function  room_maid_match_row($room_id){

         $sql = "SELECT * FROM `hotel_housekeeping_matrix` LEFT OUTER JOIN `hotel_housekeeping_maid` ON `hotel_housekeeping_matrix`.`maid_id`=`hotel_housekeeping_maid`.`maid_id`  WHERE room_id=".$room_id." AND `status`=0 GROUP BY `room_id`, `hotel_housekeeping_matrix`.`maid_id` ";

         $query=$this->db1->query($sql);
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }
     }

    function add_gi_details($stay,$booking_id){
        $this->db1->where('booking_id', $booking_id);
        $query = $this->db1->update(TABLE_PRE.'bookings', $stay);
        //echo  $this->db1->last_query();
        //exit;
        if($query){
            return true;
        }else{
            return false;
        }

    }

    function all_service_result(){
        $this->db1->order_by('s_id','desc');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
     //   $this->db1->where('user_id',$this->session->userdata('user_id'));  
        $query=$this->db1->get(TABLE_PRE.'service');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function all_laundry_service(){
        //$this->db1->order_by('laundry_id','desc');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
        $this->db1->order_by('laundry_id','desc');  
        $query=$this->db1->get(TABLE_PRE.'laundry_master');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

     function get_bookings_on_date($date){
         $sql="select * from hotel_bookings where cust_from_date <= '".$date."' and cust_end_date >= '".$date."' ";
         //$this->db1->select('booking_status_id','booking_id');
         //$this->db1->where('cust_from_date <=', $date);
         //$this->db1->where('cust_end_date >=', $date);
         //$query=$this->db1->get(TABLE_PRE.'bookings');
         $query=$this->db1->query($sql);
         if($query->num_rows()>0){
             return $query->result();
         }else{

             return $query->result();

         }
     }


     function bookings_by_date($date){

$a=$this->session->userdata('user_hotel');
         $sql="select * from hotel_bookings where booking_taking_date= '".$date."' and booking_status_id !='7' and hotel_id= '".$a."'";
	// $sql="select * from hotel_bookings where booking_taking_date= '".$date."' and booking_status_id !='7'  ";
	 
         #and booking_status_id !='7'and booking_status_id !='6' and booking_status_id_secondary !='3' and booking_status_id !='8'

         $query= $this->db1->query($sql);

         return $query->num_rows();
     }

     function  get_hotel_pincode(){

         $sql="select hotel_pincode from hotel_hotel_contact_details where hotel_id='".$this->session->userdata('user_hotel')."'";

         $query= $this->db1->query($sql);

         return $query->result();
     }


     function add_purchase($array,$add_pay,$item){
        $query=$this->db1->insert(TABLE_PRE.'purchase',$array);
        $this->db1->insert(TABLE_PRE.'payments',$add_pay);
		$payments_id=$this->db1->insert_id();
		$item['payment_id']=$payments_id;
		$item['hotel_id']=$this->session->userdata('user_hotel');
		$item['user_id']=$this->session->userdata('user_id');
	$query1=$this->db1->insert(TABLE_PRE.'line_item_details',$item);
		
		
		/*return $id= $this->db1->insert_id();
        $arrVal['purchase_id'] = $id;*/
       // $query1=$this->db1->insert(TABLE_PRE.'transactions',$arrVal);
        if($query1){
            return true;
        }else{
            return false;
        }
    }

	function get_last_pur_id()
	{
	$last_row=$this->db1->select('id')->order_by('id',"desc")->limit(1)->get(TABLE_PRE.'purchase')->row();
		if($last_row){
		return $last_row->id;
		}
		else{
			return false;
		}
	}


    function add_purchase_pos($array){
        $query=$this->db1->insert(TABLE_PRE.'purchase',$array);

        if($query){
            return true;
        }else{
            return false;
        }
    }

     function  get_shift_log($date,$time_start,$time_end){
         $sql="SELECT * FROM `hotel_bookings` WHERE booking_taking_date= '".date("Y-m-d",strtotime($date))."' and booking_taking_time >= '".$time_start."' and booking_taking_time <= '".$time_end."'";
         $query=$this->db1->query($sql);
         if($query){
             return $query->result();
         }else{
             return false;
         }
     }

     function  get_shift_log_endless($date,$time_start){
         $sql="SELECT * FROM `hotel_bookings` WHERE booking_taking_date= '".date("Y-m-d",strtotime($date))."' and booking_taking_time >= '".$time_start."' ";
         $query=$this->db1->query($sql);
         if($query){
             return $query->result();
         }else{
             return false;
         }
     }

     function  previous_shift_id($user_id){

         $sql="select log_id from shift_log where logged_out='0' and user_id='".$user_id."'  ";

         $query=$this->db1->query($sql);
         if($query){
             return $query->result();
         }else{
             return false;
         }
     }

     function get_floor(){


         $sql="select hotel_floor from hotel_hotel_details where hotel_id='".$this->session->userdata('user_hotel')."'";
         $query=$this->db1->query($sql);
         if($query){
             return $query->result();
         }else{
             return false;
         }
     }
     function  add_dgset($data){
        $query=$this->db1->insert(TABLE_PRE.'dgset',$data);
        if($query){
            return true;
        }else{
            return false;
        }




    }
     function all_dgset(){
		// $this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->order_by('dgset_id','asc');
        //$this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'dgset');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }


     function delete_dgset($id){
         $this->db1->where('dgset_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'dgset');
         if($query){
             return true;
         }else{
             return false;
         }
     }

function all_dgset_limit_view()
    {
		$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->order_by('dgset_id','desc');
        //$this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'dgset');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }


 }
 function fetch_dgset($id){
     $this->db1->where('dgset_id',$id);
     $this->db1->select('*');
     $query=$this->db1->get(TABLE_PRE.'dgset');

         if($query){
             return $query->row();
         }else{
             return false;
         }
 }
 function update_dgset($dgset)
 {
        $dgset_id=$dgset['dgset_id'];
        $this->db1->where_in('dgset_id', $dgset_id);
        $query_details = $this->db1->update(TABLE_PRE.'dgset',$dgset);
        if($query_details){
            return true;
        }
        else{
            return false;
        }
    }
    function fetch_gen($id)
    {
	//PASS BLANK '' TO GET ALL THE DG SET AS RESULT
        $this->db1->select('*');
		if($id=='active')
		{
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get_where(TABLE_PRE.'dgset',"status='".$id."'");
		}
		else
		{
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		 $query=$this->db1->get(TABLE_PRE.'dgset');
		}
        if($query->num_rows()>0)
        {
            return $query->result();
        }else
        {
            return false;
        }
    }
    function fetch_user()
    {   $id=0;
        $this->db1->where_in('admin_booking_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
//$this->db1->where('user_id',$this->session->userdata('user_id'));  
        $this->db1->select('*');
        $query=$this->db1->get(TABLE_PRE.'admin_details');
        if($query->num_rows()>0)
        {
            return $query->result();
        }else
        {
            return false;
        }
    }
    function  add_usage_log($data)
    {
        $query=$this->db1->insert(TABLE_PRE.'usage_log',$data);
   
		$usage_log_id=$this->db1->insert_id();
        if($query){
            return $usage_log_id;
        }else{
            return false;
        }

    }
    function all_usage_log()
    {
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
         $this->db1->order_by('usage_id','desc');
        $query=$this->db1->get(TABLE_PRE.'usage_log');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
    function delete_usage_log($id)
    {
         $this->db1->where('usage_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'usage_log');
         if($query){
             return true;
         }else{
             return false;
         }
    }
    function fetch_purchase_id(){

         $this->db1->select('*');
         $query=$this->db1->get(TABLE_PRE.'purchase');
         if($query->num_rows()>0){

             return $query->result();
         }else {
			 return false;
		 }

 }
    function  add_sub_fuel($add_sub,$add_pay,$item,$in_out_type){
        if($in_out_type!='sub')
		{
		$this->db1->insert(TABLE_PRE.'payments',$add_pay);
		$payments_id=$this->db1->insert_id();
		$item['payment_id']=$payments_id;
		$item['hotel_id']=$this->session->userdata('user_hotel');
		$item['user_id']=$this->session->userdata('user_id');
		$this->db1->insert(TABLE_PRE.'line_item_details',$item);
		}
        $query=$this->db1->insert(TABLE_PRE.'add_sub_fuel_log',$add_sub);
		$l_id=$this->db1->insert_id();
        if($query){
		    return $l_id;
        }else{
            return false;
        }
    }


	function add_payments($add_pay,$arrVal,$line_itmes,$pay_details){
		
		$query=$this->db1->insert(TABLE_PRE.'payments',$add_pay);
        $id= $this->db1->insert_id();
        $arrVal['payment_id'] = $id;
		$arrVal['details_id'] = $id;
		$pay_details['hotel_payment_id']= $id;
		
		if($arrVal['t_payment_mode'] != ''){
	    $query1=$this->db1->insert(TABLE_PRE.'transactions',$arrVal);
		}
		if($pay_details['amount']>0){
		$pay=$this->db1->insert(TABLE_PRE.'payments_details',$pay_details);
		}
		foreach($line_itmes['product'] as $key => $val){

			$product=$line_itmes['product'][$key];
			$qty=$line_itmes['qty'][$key];
			$price=$line_itmes['price'][$key];
			$tax=$line_itmes['tax'][$key];
			$disc=$line_itmes['disc'][$key];
			$total=$line_itmes['total'][$key];
			$cls=$line_itmes['cls'][$key];
			$note=$line_itmes['note'][$key];



			$abc=array(
				'product'=>$product,
				'qty'=>$qty,
				'price'=>$price,
				'tax'=>$tax,
				'discount'=>$disc,
				'total'=>$total,
				'class'=>$cls,
				'note'=>$note,
				'hotel_id'=>$this->session->userdata('user_hotel'),
				'user_id'=>$this->session->userdata('user_id'),
				'payment_id'=>$id
								);

    	 $query2=$this->db1->insert(TABLE_PRE.'line_item_details',$abc);
		}

		if($query){
		    return true;
        }else{
            return false;
        }
	}
	
	function update_payments($add_pay,$pay_id,$arrVal,$line_itmes,$pay_details){
			 $arrVal['payment_id'] = $pay_id;
		     $arrVal['details_id'] = $pay_id;
			 //echo "id ".$pay_id; exit;
		 $this->db1->where('hotel_payment_id',$pay_id);
         $query=$this->db1->update(TABLE_PRE.'payments',$add_pay);
         $query1=$this->db1->update(TABLE_PRE.'payments_details',$pay_details);
         foreach($line_itmes['product'] as $key => $val){

			$product_id=$line_itmes['id'][$key];
			//$payment_id=$line_itmes['payment_id'][$key];
			$product=$line_itmes['product'][$key];
			$qty=$line_itmes['qty'][$key];
			$price=$line_itmes['price'][$key];
			$tax=$line_itmes['tax'][$key];
			$disc=$line_itmes['disc'][$key];
			$total=$line_itmes['total'][$key];
			$cls=$line_itmes['cls'][$key];
			$note=$line_itmes['note'][$key];
			
			
			if($product_id!=''){	
			$abc=array(
				'product'=>$product,
				'qty'=>$qty,
				'price'=>$price,
				'tax'=>$tax,
				'discount'=>$disc,
				'total'=>$total,
				'class'=>$cls,
				'note'=>$note,
				'hotel_id'=>$this->session->userdata('user_hotel'),
				'user_id'=>$this->session->userdata('user_id'),
				'payment_id'=>$pay_id
								);
			//print_r($abc); echo "yes"; exit;
    	
		 $this->db1->where('line_items_id',$product_id);
		$query3=$this->db1->update(TABLE_PRE.'line_item_details',$abc);
		if($arrVal['t_amount'] > 0 && $arrVal['t_amount'] != ''){
		$query4=$this->db1->insert(TABLE_PRE.'transactions',$arrVal);
		}
			}
		
		 else{ 
				//echo "hi"; 
			 $abc=array(
				'product'=>$product,
				'qty'=>$qty,
				'price'=>$price,
				'tax'=>$tax,
				'discount'=>$disc,
				'total'=>$total,
				'class'=>$cls,
				'note'=>$note,
				'hotel_id'=>$this->session->userdata('user_hotel'),
				'user_id'=>$this->session->userdata('user_id'),
				'payment_id'=>$pay_id
				);
				//print_r($abc); exit;
				 $query2=$this->db1->insert(TABLE_PRE.'line_item_details',$abc);
				//print_r($abc); echo "no".$product_id; exit;
				$this->db1->where('line_items_id',$product_id);
			$query3=$this->db1->update(TABLE_PRE.'line_item_details',$abc);	
		 }
		}
		//print_r($abc); echo "no".$product_id; exit;
		 if($query){
             return true;
         }else{
             return false;
         }
	}

	



    function all_payments(){
        // $top="Salary/WagePayments";
         $this->db1->select('*');
         //$this->db1->where('type_of_payment',$top);
         $query=$this->db1->get(TABLE_PRE.'payments');//print_r($query);  die;
         if($query->num_rows()>0){
             return $query->result();
         } else {
             return false;
         }
    }


	function all_salpayments(){
		$top="Salary/Wage Payments";
		 $this->db1->select('*');
		 $this->db1->where('type_of_payment',$top);
         $query=$this->db1->get(TABLE_PRE.'payments');//print_r($query);  die;
         if($query->num_rows()>0){
             return $query->result();
         }else {
			 return false;
		 }
	}

    function delete_salpayments($id){
        $this->db1->where('hotel_payment_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'payments');
         if($query){
             return true;
         }else{
             return false;
         }
    }


	function all_billpayments()
	{
		//$top="Bill payments";
		 $this->db1->select('*');
		 $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		 $this->db1->order_by('date','desc');
		  
         $query=$this->db1->get(TABLE_PRE.'payments');
         if($query->num_rows()>0){

             return $query->result();
         }else {
			 return false;
		 }
	}

	function fetch_bill_payments($id){
			 $this->db1->where('hotel_payment_id',$id);
         $query=$this->db1->get(TABLE_PRE.'payments');
         if($query->num_rows()>0){
               return $query->result();
         }else{
             return false;
         }
	}

    function delete_billpayments($id){
        $this->db1->where('hotel_payment_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'payments');
         if($query){
             return true;
         }else{
             return false;
         }

 }
	function all_mispayments()
	{
		$top="Miscellaneous Payments";
		 $this->db1->select('*');
		 $this->db1->where('type_of_payment',$top);
         $query=$this->db1->get(TABLE_PRE.'payments');//print_r($query);  die;
         if($query->num_rows()>0){

             return $query->result();
         }else {
			 return false;
		 }
	}

    function delete_mispayments($id){
        $this->db1->where('hotel_payment_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'payments');
         if($query){
             return true;
         }else{
             return false;
         }

 }
 
 function get_p_mode_by_name($name){
         $this->db1->select('*');
         $this->db1->where('p_mode_name',$name);
         $query=$this->db1->get(TABLE_PRE.'payment_mode');//print_r($query);  die;
         if($query->num_rows()>0){

             return $query->row();
         }else {
             return false;
         }
    }
 
    function all_taxpayments()
    {
        $top="Tax/Compliance Payment";
         $this->db1->select('*');
         $this->db1->where('type_of_payment',$top);
         $query=$this->db1->get(TABLE_PRE.'payments');//print_r($query);  die;
         if($query->num_rows()>0){

             return $query->result();
         }else {
             return false;
         }
    }

    function all_expense_report()
    {

         $this->db1->select('*');
         $this->db1->where('transaction_releted_t_id','4');
         $this->db1->or_where('transaction_releted_t_id','7');
         $this->db1->or_where('transaction_releted_t_id','5');
         $this->db1->or_where('transaction_releted_t_id','12');
         $this->db1->or_where('transaction_from_id','4');
         $this->db1->or_where('transaction_to_id','5');
         $this->db1->or_where('transaction_to_id','7');
          $this->db1->or_where('transaction_to_id','6');
           $this->db1->or_where('transaction_to_id','11');
         $query=$this->db1->get(TABLE_PRE.'transactions');//print_r($query);  die;
         if($query->num_rows()>0){

             return $query->result();
         }else {
             return false;
         }
    }


    function get_payment_id()
    {
            $sql=$this->db1->insert_id('payments');
            //print_r($sql);  die;
            $this->db1->where('hotel_payment_id',$sql);
            $this->db1->select('hotel_payment_id');

         $query=$this->db1->get(TABLE_PRE.'payments');//print_r($query);  die;
         if($query->num_rows()>0){

             return $query->result();
         }else {
             return false;
         }
    }
    function filter_expense_report($top,$fDate,$tDate)
    {

    //echo $top;
    //exit;
        if($top=="Bill Payments"){
             $this->db1->select('*');
             $this->db1->where('transaction_releted_t_id','5');
             $this->db1->where('transaction_from_id','4');
             $this->db1->where('transaction_to_id','6');
             $query=$this->db1->get(TABLE_PRE.'transactions');
             //print_r($query);  die;
             if($query->num_rows()>0){
                 return $query->result();
             }else {
                 return false;
             }
        } else if($top=="Salary/Wage Payments"){
         $this->db1->select('*');
         $this->db1->where('transaction_releted_t_id','7');
         $this->db1->where('transaction_from_id','4');
         $this->db1->where('transaction_to_id','7');

         $query=$this->db1->get(TABLE_PRE.'transactions');//print_r($query);  die;
         if($query->num_rows()>0){

             return $query->result();
         }else {
             return false;
         }
    }else if($top=="Miscellaneous Payments"){
         $this->db1->select('*');
         $this->db1->where('transaction_releted_t_id','12');
         $this->db1->where('transaction_from_id','4');
         $this->db1->where('transaction_to_id','11');

         $query=$this->db1->get(TABLE_PRE.'transactions');//print_r($query);  die;
         if($query->num_rows()>0){

             return $query->result();
         }else {
             return false;
         }
    }
    else if($top=="Tax/Compliance Payment"){
         $this->db1->select('*');
         $this->db1->where('transaction_releted_t_id','4');
         $this->db1->where('transaction_from_id','4');
         $this->db1->where('transaction_to_id','5');

         $query=$this->db1->get(TABLE_PRE.'transactions');//print_r($query);  die;
         if($query->num_rows()>0){

             return $query->result();
         }else {
             return false;
         }
        }else if($top=="Purchase"){
             $this->db1->select('*');
             $this->db1->where('transaction_releted_t_id','5');
             $this->db1->where('transaction_from_id','4');
             $this->db1->where('transaction_to_id','5');

         $query=$this->db1->get(TABLE_PRE.'transactions');//print_r($query);  die;
         if($query->num_rows()>0){
             return $query->result();
         }else {
             return false;
         }
    } else{
      $this->db1->select('*');
         $this->db1->where('transaction_releted_t_id','4');
         $this->db1->or_where('transaction_releted_t_id','7');
         $this->db1->or_where('transaction_releted_t_id','5');
         $this->db1->or_where('transaction_releted_t_id','12');
         $this->db1->or_where('transaction_from_id','4');
         $this->db1->or_where('transaction_to_id','5');
         $this->db1->or_where('transaction_to_id','7');
          $this->db1->or_where('transaction_to_id','6');
           $this->db1->or_where('transaction_to_id','11');
         $query=$this->db1->get(TABLE_PRE.'transactions');//print_r($query);  die;
         if($query->num_rows()>0){

             return $query->result();
         }else {
             return false;
         }

}
}

function roomcount($start_date,$end_date){
		
				  $sql="SELECT COUNT(`booking_id`) as id FROM `booking_charge_line_item` WHERE `date` BETWEEN '".$start_date."' AND '".$end_date."' AND `hotel_id` = '".$this->session->userdata('user_hotel')."' AND `isCancelled` <> '1'";		  
				  
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->row();
		}else{
			return 0;
		}

 }
 function roomcountnew($start_date){
		//echo $dt." " ;
      /*  $this->db1->select('*');
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where("booking_status_id!= ",7);
        $this->db1->where('cust_from_date',$start_date);
      
         $query=$this->db1->get(TABLE_PRE.'bookings');*/
		 
				  $sql="SELECT COUNT(`booking_id`) as id FROM `booking_charge_line_item` WHERE `date` BETWEEN '".$start_date."' AND '".$start_date."' AND `hotel_id` = '".$this->session->userdata('user_hotel')."' AND `isCancelled` <> '1'";		  
				  
		$qry = $this->db1->query($sql);
		 
         if($qry->num_rows()>0){
             return $qry->num_rows();
         }else{
            return 0;
         }

 }

 function totalrooms(){

         $this->db1->select('room_no');
		 //$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
         $query=$this->db1->get(TABLE_PRE.'room');
         // print_r($query);
         // die;
         if($query->num_rows()>0){
             return $query->result();
         }else{
            return false;
         }
		
 }
 
  function roomCount1(){

         $this->db1->select('count(room_no) as count');
		 //$this->db1->where('user_id',$this->session->userdata('user_id'));
         $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		
         $query=$this->db1->get(TABLE_PRE.'room');
         // print_r($query);
         // die;
         if($query->num_rows()>0){
             return $query->row();
         }else{
            return false;
         }
		
 }


 function avgdailyrent($start_date,$end_date){
	$this->db1->select('booking_id,rm_total,rr_tot_tax,mp_tot,mp_tax,exrr,exrr_tax,exmp,exmp_tax,extra_service_total_amount,service_price');
		//$this->db1->select_sum('rm_total');
         $this->db1->order_by('booking_id','desc');
		$this->db1->where('booking_status_id !=',7);
        $this->db1->where('hotel_id', $this->session->userdata('user_hotel'));
       // $this->db1->where('cust_from_date_actual>=', $dt);
       // $this->db1->where('cust_from_date_actual>=', $dt); 
	    $this->db1->where("date_format(cust_from_date,'%Y-%m-%d')>=",$start_date);
        $this->db1->where("date_format(cust_from_date,'%Y-%m-%d')<=", $end_date);
       // $this->db1->limit(100);
		$query=$this->db1->get('booking_view2');
		//echo $this->db1->last_query();
		//	exit;
	  if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

 }
 
 function avgdailyrentnew($start_date){
	$this->db1->select('booking_id,rm_total,rr_tot_tax,mp_tot,mp_tax,exrr,exrr_tax,exmp,exmp_tax,extra_service_total_amount,service_price');
		//$this->db1->select_sum('rm_total');
         $this->db1->order_by('booking_id','desc');
		$this->db1->where('booking_status_id !=',7);
        $this->db1->where('hotel_id', $this->session->userdata('user_hotel'));
       // $this->db1->where('cust_from_date_actual>=', $dt);
       // $this->db1->where('cust_from_date_actual>=', $dt); 
	    $this->db1->where('cust_from_date',$start_date);
      //  $this->db1->where('cust_from_date_actual <=', $end_date);
        $this->db1->limit(100);
		$query=$this->db1->get('booking_view2');
		//echo $this->db1->last_query();
		//	exit;
	  if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

 }



function add_lost_item($lost_item){
        $query=$this->db1->insert(TABLE_PRE.'lost_item',$lost_item);
        if($query){
            return true;
        }

     }

 function fetch_lost_item(){
    $this->db1->order_by('l_id','asc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db1->where('user_id=',$this->session->userdata('user_id'));
        $query=$this->db1->get(TABLE_PRE.'lost_item');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
 }
 function fetch_l_id($id){

         $this->db1->where('l_id',$id);
         $query=$this->db1->get(TABLE_PRE.'lost_item');
         if($query->num_rows()>0){

             return $query->result();
         }

 }
 function update_lost_item($lost_item){
    $this->db1->where('l_id',$lost_item['l_id']);
        $query=$this->db1->update(TABLE_PRE.'lost_item',$lost_item);
        if($query){
            return true;
        }else{
            return false;
        }

 }
 function delete_l_item($id){
        $this->db1->where('l_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'lost_item');
         if($query){
             return true;
         }else{
             return false;
         }

 }

function add_found_item($found_item){

    $query=$this->db1->insert(TABLE_PRE.'found_item',$found_item);
        if($query){
            return true;
        }
}

    function fetch_admin(){
        $this->db1->order_by('admin_id','asc');
        //$this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'admin_details');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

function fetch_found_item(){

		$this->db1->order_by('f_id','asc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db1->where('user_id=',$this->session->userdata('user_id'));
        $query=$this->db1->get(TABLE_PRE.'found_item');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
}

 function fetch_f_item($item){
	 $array = array('type' => $item,'status' => 'found');
	$this->db1->where($array);
         $query=$this->db1->get(TABLE_PRE.'found_item');
         if($query->num_rows()>0){

             return $query->result_array();
         }

 }

function fetch_lost_items($item){
	$array = array('type' => $item,'status' => 'lost');
	$this->db1->where($array);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));       
         $query=$this->db1->get(TABLE_PRE.'lost_item');
         if($query->num_rows()>0){

             return $query->result_array();
         }

}


function fetch_f_id($f_id){

         $this->db1->where('f_id',$f_id);
         $query=$this->db1->get(TABLE_PRE.'found_item');
         if($query->num_rows()>0){

             return $query->result();
         }

}

function update_release_item($r_item){
	$this->db1->where('hotel_release_item_id',$r_item['hotel_release_item_id']);
        $query=$this->db1->update(TABLE_PRE.'release_item',$r_item);
        if($query){
            return true;
        }else{
            return false;
        }

}
 function delete_item($id){
    $this->db1->where('f_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'found_item');
         if($query){
             return true;
         }else{
             return false;
         }
     }
 function delete_f_item($item){
	  $this->db1->where('item_title',$item);
         $query=$this->db1->delete(TABLE_PRE.'found_item');
         if($query){
             return true;
         }else{
             return false;
         }

 }



function update_found_item($found_item){

        $this->db1->where('f_id',$found_item['f_id']);
        $query=$this->db1->update(TABLE_PRE.'found_item',$found_item);
        if($query){
            return true;
        }else{
            return false;
        }

}
function release_item($r_item){
		//$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->insert(TABLE_PRE.'release_item',$r_item);
		if($query){
			return true;
		}
		else{
			return false;
		}
}

function fetch_release_item(){

	$this->db1->order_by('hotel_release_item_id','desc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'release_item');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }


}

function fetch_r_id($id){
	 $this->db1->where('hotel_release_item_id',$id);
         $query=$this->db1->get(TABLE_PRE.'release_item');
         if($query->num_rows()>0){

             return $query->result();
         }

}
 function delete_r_item($id){
	 $this->db1->where('hotel_release_item_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'release_item');
         if($query){
             return true;
         }else{
             return false;
         }

 }
function fetch_corporate(){
		$this->db1->order_by('hotel_corporate_id','asc');
        $this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
      //  $this->db1->where('user_id=',$this->session->userdata('user_id'));
        $query=$this->db1->get(TABLE_PRE.'corporate');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

}
function update_corporate($corporate){
	   $this->db1->where('hotel_corporate_id',$corporate['hotel_corporate_id']);
        $query=$this->db1->update(TABLE_PRE.'corporate',$corporate);
        if($query){
            return true;
        }else{
            return false;
        }

}
function fetch_c_id($id){
      $this->db1->where('hotel_corporate_id',$id);
         $query=$this->db1->get(TABLE_PRE.'corporate');
         if($query->num_rows()>0){

             return $query->result();
         }
}

function fetch_c_id1($id){
      $this->db1->where('hotel_corporate_id',$id);
         $query=$this->db1->get(TABLE_PRE.'corporate');
         if($query->num_rows()>0){

             return $query->row();
         }
}

function add_corporate($corporate,$corporate_discount){
	
	//echo "<pre>";
	//print_r($corporate);exit;
	
     $query=$this->db1->insert(TABLE_PRE.'corporate',$corporate);

	 $corporate_id=$this->db1->insert_id();

		//$corporate_discount['corporate_id']=$corporate_id;

		foreach($corporate_discount['contract_start_date'] as $key => $val){

				$start_date=$corporate_discount['contract_end_date'][$key];
				$end_date=$corporate_discount['contract_end_date'][$key];
				$start_discount=$corporate_discount['contract_discount_start'][$key];
				$default_apply=$corporate_discount['default_apply'][$key];

				 $data=array(
                                'contract_start_date'=>$start_date,
                                'contract_end_date'=>$end_date,
                                'cp_discount_rule_id'=>$start_discount,
                                'default_apply'=>$default_apply,
								'corporate_id'=>$corporate_id
								);
				$query1=$this->db1->insert('cp_discount_map',$data);

							}



		if($query && $query1){
            return true;
        }

}
function delete_corporate($id){
	$this->db1->where('hotel_corporate_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'corporate');
         if($query){
             return true;
         }else{
             return false;
         }

}
function fetch_e_id($id){
	$this->db1->where('e_id',$id);
         $query=$this->db1->get(TABLE_PRE.'events');
         if($query->num_rows()>0){

             return $query->row();
         }

}
    function update_event($event){

	  $this->db1->where('e_id',$event['e_id']);
	  $query=$this->db1->update(TABLE_PRE.'events',$event);

	  if($query){
             return true;
         }else{
             return false;
         }
    }

/* Add Charge */
    function insert_charge($data){
        $query=$this->db1->insert('hotel_extra_charges_mapping',$data);
        if($query){
            return $this->db1->insert_id();
        }else{
            return false;
        }
    }
//1.04.2016
// New function 0n 0905..
	function insert_extra_charge($data1){
		
		$query=$this->db1->insert('hotel_extra_charges',$data1);
        if($query){
            return $this->db1->insert_id();
        }else{
            return false;
        }
		
		
	}
	
	
function ch_extra_charge($name){
	    
	    $this->db1->where('charge_name',$name);
	    $query=$this->db1->get('hotel_extra_charges');
	 if($query->num_rows()>0){
	    return 1;
	}else{
	    return 0;
	}
	    
	}   
	
	
	
	
	
    function booking_details(){
		$this->db1->order_by('b.booking_id','asc');
		$this->db1->select('b.*');
		//$this->db1->select('g.*');
		$this->db1->from('hotel_bookings b');
		//$this->db1->join('hotel_guest g','b.guest_id=g.g_id');
		$query = $this->db1->get();//echo $this->db1->last_query();die;

         if($query->num_rows()>0){

             return $query->result();
         }
		 else{
			 return false;
		 }

    }
    function booking_details_forguest($cond){
		if(!empty($cond)){
			$this->db1->where($cond);
			$this->db1->select('b.*');
			$this->db1->from('hotel_bookings b');
			$query = $this->db1->get();
			 if($query->num_rows()>0){
				 return $query->result();
			 }
			 else{
			 return false;
			 }
		}

    }

    function update_service($data){
        $this->db1->where('s_id',$data['s_id']);
        $query=$this->db1->update(TABLE_PRE.'service',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }

	function add_misc_income($m_income,$arrVal){
		$query=$this->db1->insert(TABLE_PRE.'misc_income',$m_income);
        $id = $this->db1->insert_id();
        $arrVal['details_id'] = $id;
        $query1 = $this->db1->insert(TABLE_PRE.'transactions',$arrVal);
		if($query1){
		    return true;
        }
		else{
            return false;
        }

	}
	function fetch_m_income(){
		$this->db1->order_by('hotel_misc_income_id','desc');
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'misc_income');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

	}

	function fetch_m_income_id($id){
		$this->db1->where('hotel_misc_income_id',$id);
        //$this->db1->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'misc_income');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

	}

	function update_misc_income($m_income){

		$this->db1->where('hotel_misc_income_id',$m_income['hotel_misc_income_id']);
        $query=$this->db1->update(TABLE_PRE.'misc_income',$m_income);
        if($query){
            return true;
        }else{
            return false;
        }

	}

	function delete_m_income($id){

		$this->db1->where('hotel_misc_income_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'misc_income');
		 
		 $this->db1->where('details_id',$id);
         $query1=$this->db1->delete(TABLE_PRE.'transactions');
		 
         if($query && $query1){
             return true;
         }else{
             return false;
         }


	}
	function delete_e_report($id){
		$this->db1->where('t_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'transactions');
         if($query){
             return true;
         }else{
             return false;
         }
	}

	function get_all_room_name(){
		$hotel_id = $this->session->userdata('user_hotel');
		$this->db1->select('room_no');
		$this->db1->where('hotel_id',$hotel_id);
		$this->db1->group_by('room_no');
        $query=$this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }


     /* Add Unit Block */
    function add_unit_block($data){
		$query=$this->db1->insert(TABLE_PRE.'unit_block',$data);
        if($query){
            return true;
        }else{
            return false;
        }
	}

	public  function all_unit_block(){
		$this->db1->where('room_status!=','D');
        $this->db1->where('hotel_room.hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->join('hotel_unit_type', 'hotel_room.unit_id = hotel_unit_type.id');
        $this->db1->group_by('room_no');
        $this->db1->order_by('room_no','asc');
		//$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function  insert_pos($data){
        $query=$this->db1->insert(TABLE_PRE.'pos',$data);
        if($query){
            return $this->db1->insert_id();;
        }else{
            return false;
        }

    }


    function  insert_pos_item($data){
        $query=$this->db1->insert(TABLE_PRE.'pos_items',$data);
        if($query){
            return $this->db1->insert_id();;
        }else{
            return false;
        }

    }

    function all_pos($booking_id){
        $sql="select * from hotel_pos where booking_id='".$booking_id."' and booking_type='sb'";
        $query=$this->db1->query($sql);
        if($query){
            return $query->result();
        }else{
            return false;
        }

    }

    function all_group_pos($booking_id){
        $sql="select * from hotel_pos where booking_id='".$booking_id."' and booking_type='gb'";
        $query=$this->db1->query($sql);
        if($query){
            return $query->result();
        }else{
            return false;
        }

    }



     function all_pos_items($pos_id){
        $sql="select * from hotel_pos_items where pos_id='".$pos_id."'";
        $query=$this->db1->query($sql);
        if($query){
            return $query->result();
        }else{
            return false;
        }

    }

	function fetch_r_summary(){
		$this->db1->order_by('cust_from_date','asc');
		$this->db1->where('hotel_guest.hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->join('hotel_guest','hotel_guest.g_id = hotel_bookings.guest_id');
		 $this->db1->join('hotel_room', 'hotel_room.room_id=hotel_bookings.room_id');
		 $this->db1->join('hotel_unit_type', 'hotel_room.unit_id=hotel_unit_type.id');
		 $this->db1->join('booking_status_type','booking_status_type.status_id=hotel_bookings.booking_status_id');
		 //$this->db1->join('hotel_transactions', 'hotel_transactions.t_booking_id=hotel_bookings._id');
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'bookings',TABLE_PRE.'guest',TABLE_PRE.'room',TABLE_PRE.'unit_type',TABLE_PRE.'booking_status_type');

	   if($query->num_rows()>0){
            return $query->result();

        }else{
            return false;
        }


	}
	function all_r_by_date($start_date,$end_date){
	    $this->db1->order_by('booking_id','asc');
        $this->db1->where('cust_from_date_actual >=', $start_date);
        $this->db1->where('cust_from_date_actual <=', $end_date);        
        $this->db1->join('hotel_guest','hotel_guest.g_id = hotel_bookings.guest_id');
		$this->db1->join('hotel_room', 'hotel_room.room_id=hotel_bookings.room_id');
		$this->db1->join('hotel_unit_type', 'hotel_room.unit_id=hotel_unit_type.id');
		$this->db1->join('booking_status_type','booking_status_type.status_id=hotel_bookings.booking_status_id');
		 //$this->db1->join('hotel_transactions', 'hotel_transactions.t_booking_id=hotel_bookings._id');
        //$this->db1->limit($limit,$start);
		$this->db1->where('hotel_room.hotel_id', $this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'bookings',TABLE_PRE.'guest',TABLE_PRE.'room',TABLE_PRE.'unit_type',TABLE_PRE.'booking_status_type');

	   if($query->num_rows()>0){
            return $query->result();

        }else{
            return false;
        }

	}



	 function  get_room_by_id($room_id){
		 $this->db1->where('room_id',$room_id);
		 $this->db1->join('hotel_unit_type', 'hotel_room.unit_id=hotel_unit_type.id');
		 $query=$this->db1->get(TABLE_PRE.'room',TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

	 }

    function all_trasac($booking_id){



    }

    function get_available_room($date1 ,$date2 ){
       $sql="select bk.room_id,
					rm.room_no,
					booking_id
			   from hotel_bookings as bk 
			   LEFT JOIN hotel_room as rm on bk.room_id = rm.room_id
			   where 
					(((date(cust_from_date) between '".$date1."' and '".$date2."' ) or  (date(cust_end_date) between '".$date1."' and '".$date2."' ) or (date(cust_from_date) <'".$date1."' and date(cust_end_date) >'".$date2."') or (date(cust_from_date) >'".$date1."' and date(cust_end_date) <'".$date2."') or  (date(cust_from_date) = '".$date2."' and date(cust_end_date) >'".$date2."')) 
			   and booking_status_id !='7'  and  date(cust_from_date) != '".$date2."') 
			   and bk.hotel_id = ".$this->session->userdata('user_hotel')."
			   ORDER BY 
					cast(rm.room_no as unsigned)";

       $query=$this->db1->query($sql);
        if($query->num_rows() > 0 ){
            return $query->result();
        } else {
            return $query->result();
        }
    }
	
	function get_available_room_test($date1 ,$date2 ){
       $sql="select bk.room_id,booking_id, rm.room_no
	   from hotel_bookings as bk 
	   left join hotel_room as rm on bk.room_id = rm.room_id
	   where (((date(cust_from_date) between '".$date1."' and '".$date2."' ) or  (date(cust_end_date) between '".$date1."' and '".$date2."' ) or (date(cust_from_date) <'".$date1."' and date(cust_end_date) >'".$date2."') or (date(cust_from_date) >'".$date1."' and date(cust_end_date) <'".$date2."') or  (date(cust_from_date) = '".$date2."' and date(cust_end_date) >'".$date2."')) or date(cust_from_date) = '".$date1."')
	   and booking_status_id !='7' and booking_status_id !='8' and booking_status_id !='6' and  date(cust_from_date) != '".$date2."'
	   and bk.hotel_id = ".$this->session->userdata('user_hotel')."
	   group by rm.room_no";

       $query=$this->db1->query($sql);
        if($query->num_rows() > 0 ){
            return $query->result();
        } else {
            return $query->result();
        }
    }
	
	function get_avail_single_room($date1 ,$date2,$roomid,$sngleid){
		
       $sql="select room_id,booking_id from hotel_bookings where ((date(cust_from_date) between '".$date1."' and '".$date2."' ) or  (date(cust_end_date) between '".$date1."' and '".$date2."' ) or (date(cust_from_date) <'".$date1."' and date(cust_end_date) >'".$date2."') or (date(cust_from_date) >'".$date1."' and date(cust_end_date) <'".$date2."') or  (date(cust_from_date) = '".$date2."' and date(cust_end_date) >'".$date2."')) and booking_status_id !='7' and booking_status_id !='6' and  date(cust_end_date) != '".$date1."' and  date(cust_from_date) != '".$date2."' and room_id = '".$roomid."' and `booking_id` != '".$sngleid."'";
	
	 // echo $sql ;exit;
       $query=$this->db1->query($sql);
        if($query->num_rows() > 0 ){
            return $query->row();
        } 
    }
	
	function get_avail_single_room_final($date1 ,$date2,$roomid){
		$hotel_id = $this->session->userdata("user_hotel");
		
       $sql="select room_id,booking_id from hotel_bookings where ((date(cust_from_date) between '".$date1."' and '".$date2."' ) or  (date(cust_end_date) between '".$date1."' and '".$date2."' ) or (date(cust_from_date) <'".$date1."' and date(cust_end_date) >'".$date2."') or (date(cust_from_date) >'".$date1."' and date(cust_end_date) <'".$date2."') or  (date(cust_from_date) = '".$date2."' and date(cust_end_date) >'".$date2."')) and booking_status_id !='7' and booking_status_id !='6' and  date(cust_end_date) != '".$date1."' and  date(cust_from_date) != '".$date2."' and room_id = '".$roomid."' and hotel_id = ".$hotel_id."";
	
	 // echo $sql ;exit;
       $query=$this->db1->query($sql);
        if($query->num_rows() > 0 ){
            return $query->row();
        } 
    }
	
	function get_avail_single_room_final_grp($date1 ,$date2,$roomid){
		
		$hotel_id = $this->session->userdata("user_hotel");
		
       $sql="select * from unitavaillog where ((date(checkin_date) between '".$date1."' and '".$date2."' ) or  (date(checkout_date) between '".$date1."' and '".$date2."' ) or (date(checkin_date) <'".$date1."' and date(checkout_date) >'".$date2."') or (date(checkin_date) >'".$date1."' and date(checkout_date) <'".$date2."') or date(checkin_date) = '".$date1."' or (date(checkin_date) = '".$date2."' and date(checkout_date) >'".$date2."')) and  date(checkout_date) != '".$date1."' and  date(checkin_date) != '".$date2."' and unit_id = '".$roomid."' and hotel_id = ".$hotel_id."";
	
	 // echo $sql ;exit;
       $query=$this->db1->query($sql);
        if($query->num_rows() > 0 ){
            return $query->row();
        } 
    }

	
	function get_avail_single_room_final_grp2($date1 ,$date2,$roomid){
		
	   $hotel_id = $this->session->userdata("user_hotel");
       $sql="select * from unitavaillog where ((date(checkin_date) between '".$date1."' and '".$date2."' ) or (date(checkout_date) between '".$date1."' and '".$date2."' ) or (date(checkin_date) <'".$date1."' and date(checkout_date) >'".$date2."') or (date(checkin_date) >'".$date1."' and date(checkout_date) <'".$date2."') or date(checkin_date) = '".$date1."' or (date(checkin_date) = '".$date2."' and date(checkout_date) >'".$date2."')) and  date(checkout_date) != '".$date1."' and  date(checkin_date) != '".$date2."' and unit_id = '".$roomid."' and hotel_id = ".$hotel_id."";
	
	 // echo $sql ;exit;
       $query=$this->db1->query($sql);
        if($query->num_rows() > 0 ){
            return $query->row();
        } 
    }

	
	

    function remove_amount_pos($pos_id,$amount){
        $sql="update hotel_pos set total_paid='total_paid+".$amount."'  where pos_id=".$pos_id."";
         $sql1="update hotel_pos set total_due='total_due-".$amount."'  where pos_id=".$pos_id."";

        $query=$this->db1->query($sql);
         $query1=$this->db1->query($sql1);
        if($query){
            return true;
        }else{
            return false;
        }

    }

    function all_pos_booking($booking_id){
        $sql="select * from hotel_pos where booking_id='".$booking_id."' and booking_type='sb'";
         $query=$this->db1->query($sql);
          return $query->result();
    }

    function all_pos_booking_group($booking_id){
        $sql="select * from hotel_pos where booking_id='".$booking_id."' and booking_type='gb'";
         $query=$this->db1->query($sql);
          return $query->result();


    }
    function get_pos_items($pos_id){
		
		$this->db1->select('*');
		$this->db1->where('pos_id',$pos_id);
		$query = $this->db1->get('hotel_pos_items');
		if($query->num_rows()>0){
			
		return $query->result();	
		}else{
			
		return false;	
		}
	}
    
    
    function GPcost($booking_id){
        $sql="select * from hotel_group where id='".$booking_id."'";
         $query=$this->db1->query($sql);
          return $query->row();
    }
    function SBcost($booking_id){
        $sql="select * from hotel_bookings where booking_id='".$booking_id."'";
         $query=$this->db1->query($sql);
          return $query->row();
    }




    function  add_group_booking($data,$data1,$data2,$taxDB,$booking_source,$pCenter,$comment,$preference,$nature_visit,$marketing_personel,$travel_agent_type,$travel_agent_id,$travel_agent_commission,$commission_applicable,$dataNB,$noofdays,$tax){
			
		
	/*	echo "<pre>";
		print_r($data);
		echo "</pre>";
		exit;
		
	*/	
		if($data1['tax_applicable']=='YES'){
			$tax_apply='1';
		}else{
			$tax_apply='0';
		}
        if($data2['guestID'] ==NULL){
		  //$data2['g_id'] = $data2['guestID'];
		  unset($data2['guestID']);
		  unset($data2['g_id']);
		 // print_r($data2); exit;
          $query=$this->db1->insert(TABLE_PRE.'guest',$data2);
          $insert_id_guest = $this->db1->insert_id();
		}else{
			//echo "gggg"; exit;
			$insert_id_guest=$data2['guestID'];
		}
		$data1['guestID'] = $insert_id_guest;
		$data1['status'] = 2;
		foreach($data['mealPlan'] as $key => $val){
			$data1['meal_plan'] = $data['mealPlan'][$key];
			
		}
		//echo "<pre>";
		//print_r($data1);exit;
		
		$query=$this->db1->insert(TABLE_PRE.'group',$data1);
		$insert_id = $this->db1->insert_id();
		$grpidNB  = $insert_id;
		//print_r($data);
		//exit();
		$trrG = 0;
		$tfiG = 0;
		
		$l_count = 0;
		
		// end
		
		date_default_timezone_set("Asia/Kolkata");
		
        $times=$this->dashboard_model->checkin_time();
		
        foreach($times as $time) {
            if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
                $modifier = ($time->hotel_check_in_time_hr + 12) + $time->hotel_check_in_time_mm/60;
            }
            else{
                $modifier = ($time->hotel_check_in_time_hr) + $time->hotel_check_in_time_mm/60;
            }
        }

        $times2=$this->dashboard_model->checkout_time();
		
        foreach($times2 as $time2) {
            if($time2->hotel_check_out_time_fr=='PM' && $time2->hotel_check_out_time_hr !="12") {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12) + $time->hotel_check_out_time_mm/60;
            }
            else{
                $modifier2 = ($time2->hotel_check_out_time_hr) + $time->hotel_check_out_time_mm/60;
            }
        }

		
		
		
		
		foreach($data['roomID'] as $key => $val){

			if($data['extra_person_types'][$key] == 'adult')
			{
	
					$arr = array(
						'roomID' => $data['roomID'][$key],
						'type' => $data['type'][$key],
						'roomNO' => $data['roomNO'][$key],
						'gestName' => $data['gestName'][$key],
						'startDate' => $data['startDate'][$key],
						'endDate' => $data['endDate'][$key],
						'days' => $data['days'][$key],
						'mealPlan' => $data['mealPlan'][$key],
						'adultRoomRent' => $data['adultRoomRent'][$key],
						'afi' => $data['afi'][$key],
						'childRoomRent' => $data['childRoomRent'][$key],
						'cfi' => $data['cfi'][$key],
						'adultNO' => $data['adultNO'][$key],
						'childNO' => $data['childNO'][$key],
						'tfi' => $data['tfi'][$key],
						'room_rent_tax'  => $data['tax'][$key],
						'rr_tax_response' => $data['txrrspns'][$key],
						'mp_tax_response' => $data['txmrspns'][$key],
						'food_tax'  => $data['vat'][$key],
						'extra_person_value'  => $data['exra'][$key],
						'extra_person_type' => $data['extra_person_types'][$key] ,
						'extra_person_no'  => $data['exp'][$key] ,
						'trr' => $data['trr'][$key],
						'pdr' => $data['pdr'][$key],
						'food_vat' => $data['vat'][$key],
						'onlyFoodVat' => $data['vat'][$key],
						'room_vat' => $data['tax'][$key],
						'room_st' => $data['tax'][$key],
						'room_sc' => $data['tax'][$key],
						'price' => $data['price'][$key],
						'service' => 0.0,
						'group_booking_id' => $insert_id,
						'guestID' => $insert_id_guest,
						'tax_apply' =>$tax_apply,
					);

			} else if($data['extra_person_types'][$key] == 'child')	{
				
				
				$arr = array(
					'roomID' => $data['roomID'][$key],
					'type' => $data['type'][$key],
					'roomNO' => $data['roomNO'][$key],
					'gestName' => $data['gestName'][$key],
					'startDate' => $data['startDate'][$key],
					'endDate' => $data['endDate'][$key],
					'days' => $data['days'][$key],
					'mealPlan' => $data['mealPlan'][$key],
					'adultRoomRent' => $data['adultRoomRent'][$key],
					'afi' => $data['afi'][$key],
					'childRoomRent' => $data['childRoomRent'][$key],
					'cfi' => $data['cfi'][$key],
					'adultNO' => $data['adultNO'][$key],
					'childNO' => $data['childNO'][$key],
					'tfi' => $data['tfi'][$key],
					'room_rent_tax'  => $data['tax'][$key],
					'rr_tax_response' => $data['txrrspns'][$key],
					'mp_tax_response' => $data['txmrspns'][$key],
					'food_tax'  => $data['vat'][$key],
					'extra_person_value'  => $data['exrc'][$key],
					'extra_person_type' => $data['extra_person_types'][$key] ,
					'extra_person_no'  => $data['exp'][$key] ,
					'trr' => $data['trr'][$key],
					'pdr' => $data['pdr'][$key],
					'food_vat' => $data['vat'][$key],
					'onlyFoodVat' => $data['vat'][$key],
					'room_vat' => $data['tax'][$key],
					'room_st' => $data['tax'][$key],
					'room_sc' => $data['tax'][$key],
					'price' => $data['price'][$key],
					//'room_rent_tax_details'=>$tax_details,
					///'service' => $data['service'][$key],
					'service' => 0.0,
					'group_booking_id' => $insert_id,
					'guestID' => $insert_id_guest,
					'tax_apply' =>$tax_apply,
					);
				
				
				
				
				
			}
			
			else{
				
				$arr = array(
					'roomID' => $data['roomID'][$key],
					'type' => $data['type'][$key],
					'roomNO' => $data['roomNO'][$key],
					'gestName' => $data['gestName'][$key],
					'startDate' => $data['startDate'][$key],
					'endDate' => $data['endDate'][$key],
					'days' => $data['days'][$key],
					'mealPlan' => $data['mealPlan'][$key],
					'adultRoomRent' => $data['adultRoomRent'][$key],
					'afi' => $data['afi'][$key],
					'childRoomRent' => $data['childRoomRent'][$key],
					'cfi' => $data['cfi'][$key],
					'adultNO' => $data['adultNO'][$key],
					'childNO' => $data['childNO'][$key],
					'tfi' => $data['tfi'][$key],
					'room_rent_tax'  => $data['tax'][$key],
					'rr_tax_response' => $data['txrrspns'][$key],
					'mp_tax_response' => $data['txmrspns'][$key],
					'food_tax'  => $data['vat'][$key],
					'extra_person_value'  => '',
					'extra_person_type' => $data['extra_person_types'][$key] ,
					'extra_person_no'  => $data['exp'][$key] ,
					'trr' => $data['trr'][$key],
					'pdr' => $data['pdr'][$key],
					'food_vat' => $data['vat'][$key],
					'onlyFoodVat' => $data['vat'][$key],
					'room_vat' => $data['tax'][$key],
					'room_st' => $data['tax'][$key],
					'room_sc' => $data['tax'][$key],
					'price' => $data['price'][$key],
					//'room_rent_tax_details'=>$tax_details,
					///'service' => $data['service'][$key],
					'service' => 0.0,
					'group_booking_id' => $insert_id,
					'guestID' => $insert_id_guest,
					'tax_apply' =>$tax_apply,
					);
					
				
			}
			
			$query=$this->db1->insert(TABLE_PRE.'groups_bookings_details',$arr);
			
			$dataNB[$key]['group_id'] = $insert_id; // NB
			
			$trrG = $trrG + ( $data['days'][$key] * $data['trr'][$key]);
			$tfiG = $tfiG + ( $data['days'][$key] * $data['tfi'][$key]);

			
		} // End foreach
		
         //checkin time logic
     
		
		
		$bookingIDNB = array();
		//$stay_guest = array();
		$l_count =0;
        foreach($data['roomID'] as $key => $val){
			
			
			// check in & check out time 
		
		
		$start_time = $data['checkintime'][$key]; //'14:15:19';
        $end_time = $data['checkouttime'][$key];
		
		
		
		$start_hour = round($start_time);
        $end_hour = round($end_time);
        
        $final_start_hour = $start_hour - $modifier;
        $final_end_hour = $end_hour - $modifier;
        
        $final_start_time = $final_start_hour.":00:00";
        $final_end_time = $final_end_hour.":00:00";
		
		
        $checkin_time_grp = date("H:i:s", strtotime($start_time));
        $checkout_time_grp = date("H:i:s", strtotime($end_time));
		
		
		
		
		$start_dt = date('Y-m-d', strtotime($data['startDate'][$key])); //'2017-05-26';
        $end_dt = date('Y-m-d', strtotime($data['endDate'][$key])); //'2017-05-27';
		
        $cust_from_date = date('Y-m-d',strtotime($start_dt))."T".date("H:i:s", strtotime($final_start_time));
        $cust_end_date = date('Y-m-d',strtotime($end_dt))."T".date("H:i:s", strtotime($final_end_time));
        
        $from_date = date('Y-m-d',strtotime($start_dt))."T".date("H:i:s", strtotime($start_time));
        $from_date_final=date('Y-m-d H:i:s', strtotime($from_date) - 60 * 60 * $modifier);

        $end_date = date('Y-m-d',strtotime($end_dt))."T".date("H:i:s", strtotime($end_time));
        $end_date_final=date('Y-m-d H:i:s', strtotime($end_date) - 60 * 60 * $modifier);
		// end of check in & check out time logic
		
            //tax calculation
            if($data['trr'][$key] < 3000){
                $rvat = ($data['trr'][$key]* 5)/100;
            } else {
                $rvat = ($data['trr'][$key]* 10)/100;
            }
            $st = ($data['trr'][$key]* 8.7)/100;
            $sc = ($data['trr'][$key]* 3)/100;

            $tax=$rvat+$st+$sc;

			 $chk_last_bk=$this->dashboard_model->chk_last_bk();
           $chk_last_bk=$chk_last_bk+1;
			
			$booking_source_info=$this->bookings_model->get_source_type_id_by_name_source($booking_source);
			
			$btypeinfo=$this->bookings_model->get_source_type_name($booking_source_info->bk_source_type_id);
		//	echo $booking_source;
		//	echo "<pre>";
		//	print_r($btypeinfo);
			//echo $data['broker_id'];exit;
			if($btypeinfo->bst_id==5){
			$agentTy='Broker';
			$nname=$this->bookings_model->get_broker_details($data['broker_id']);
			if(isset($nname) && $nname){
				foreach($nname as $nn){
			$n=$nn->b_name;
				}
			}
			}
			if($btypeinfo->bst_id==4){
			$agentTy='Channel';
			$nname=$this->bookings_model->get_channel_details($data['broker_id']);
			if(isset($nname) && $nname){
				foreach($nname as $nn){
			$n=$nn->channel_name;
				}
			}
			}
			if($btypeinfo->bst_id==4 || $btypeinfo->bst_id==5){
				$n=$agentTy.' - '.$n;
			}else{
				$n=$btypeinfo->bst_name;
			}
			if(isset($data['tax'][$key]) && $data['tax'][$key]>0){
				$tax_details='Tax';
			}else{
				$tax_details='';
			}
			
			//if(isset($data['tax'][$key]) && $data['tax'][$key]>0){
			
            $arr1 = array(
                 'booking_id_actual' => "BK0".$this->session->userdata('user_hotel')."-".$this->session->userdata('user_id')."/".date("d").date("m").date("y")."/".$chk_last_bk,
                'user_id' => $this->session->userdata("user_id"),
                'hotel_id' => $this->session->userdata("user_hotel"),
            'room_id' => $data['roomID'][$key],
            'guest_id' => $insert_id_guest,
			'room_rent_tax_details'=>$tax_details,
             'no_of_adult' => $data['adultNO'][$key],
            'no_of_child' => $data['childNO'][$key],
            'no_of_guest' => $data['adultNO'][$key]+$data['childNO'][$key],
            //'booking_source' =>$booking_source,
			//'booking_source_name' => $booking_source_name->bst_name,
			 
			 'booking_source' =>$btypeinfo->bst_id,
			 'booking_source_name' =>$n,
			'comment' =>$comment,
			'preference' =>$preference,
			'p_center' =>$pCenter,
			'nature_visit' => $nature_visit,
            'cust_name' =>$data['gestName'][$key],

            'cust_from_date' =>$from_date_final,
            'cust_end_date' => $end_date_final,
            'cust_from_date_actual' => date('Y-m-d',strtotime($data['startDate'][$key])),
            'cust_end_date_actual' =>date('Y-m-d',strtotime($data['endDate'][$key])),
          /*  'checkin_time' => $modifier,
            'confirmed_checkin_time' => $modifier,
            'checkin_time_actual' => $modifier,
            'checkout_time' => $modifier2,
            'confirmed_checkout_time' => $modifier2,*/
			'checkin_time' => $checkin_time_grp,
            'confirmed_checkin_time' => $checkin_time_grp,
            'checkin_time_actual' => $checkin_time_grp,
            'checkout_time' => $checkout_time_grp,
            'confirmed_checkout_time' => $checkout_time_grp,
            'base_room_rent' => $data['pdr'][$key],

            'booking_status_id' => 2,
            'booking_status_id_previous' => 2,

             'room_rent_total_amount' => $data['price'][$key],
             'room_rent_tax_amount' => 0,
             'room_rent_sum_total' => $data['price'][$key],
            // 'service_price' => $data['service'][$key],
				 'service_price' => 0.0,
             'stay_days' =>$data['days'][$key],
             'stay_days_updated' =>$data['days'][$key],
             'booking_taking_time' => date("H:i:s"),
             'booking_taking_date' =>date("Y-m-d"),
			 'marketing_personnel'=>$marketing_personel,
              'group_id' => $insert_id
				
            );
			//echo "<pre>";
			//print_r($arr1);
           // exit;
            $query=$this->db1->insert(TABLE_PRE.'bookings',$arr1);
			$insert_id1 = $this->db1->insert_id();
			
			
			array_push($bookingIDNB,$this->db1->insert_id());
			
			
			
			
			$dataNB[$key]['booking_id'] = $this->db1->insert_id();
			
			// populate table hoj_travelagentcommission
	if($data['set_agent_ota']=='5' || $data['set_agent_ota']=='4')
	{if($data['set_agent_ota']=='5')
		{
			$typ='agent';
		}else if($data['set_agent_ota']=='4'){
			$typ='ota';
		}
	 
	$commission_applicable=$data['commission_applicable'];
	$applyedAmount=$data['applyedAmount'];
		if($commission_applicable=='1'){$capp='rr';}
		else if($commission_applicable=='2'){$capp='rrf';}
		else if($commission_applicable=='3'){$capp='tb';}
		//echo "<pre>";
		//print_r($arr11);exit;
		
				$arr11=array(
		'taType'=>$typ,
		'taID'=>$data['broker_id'],
		//'isCancelled'=>'N/A',
		'masterID'=>'N/A',
		'groupID'=>$insert_id,
		'bookingID'=>$insert_id1,
		'commAppOn'=>$capp,
		'applyedAmount'=>$applyedAmount,
		'bkStatus'=>2,
		'amount'=>$data['totcom'],
		'tax'=>0,
		'isPaid'=>'N/A',
		'note'=>'N/A',
		'paymentID'=>'N/A',
		'addedDate'=>date('Y-m-d H:i:s'),
		'hotel_id'=>$this->session->userdata('user_hotel'),
		'user_id'=>$this->session->userdata('user_id')
		
		);
		//echo "<pre>";
		///print_r($arr11);
		
		//exit;
		$query11=$this->db1->insert('hoj_travelagentcommission',$arr11);
	}
	$doc = $this->guest_verified($insert_id_guest);
	
			$stay_guest = array(
			'booking_id' =>$insert_id1,
			'group_id' =>$insert_id,
			'g_id' => $insert_id_guest,
			'addEvent' =>'pending',
		//	'stayPurpose' => $nature_visit,
			'docVerif'=> $doc,
			'charge_applicable' => '0',
			'chargeAmt' => 0.00,
			'check_in_date' =>date('Y-m-d H:i:s', strtotime($data['startDate'][$key])),
			//'check_in_date' =>date('Y-m-d H:i:s', strtotime($data['startDate'][$key])),
			'check_out_date' => date('Y-m-d H:i:s', strtotime($data['endDate'][$key])),
			//'check_out_date' => date('Y-m-d H:i:s', strtotime($data['endDate'][$key])),
			'user_id' => $this->session->userdata('user_id'),
            'hotel_id' => $this->session->userdata('user_hotel')
			);
			
			//$this->into_stay_guest_grp($stay_guest);
			if($l_count == 0){
				
				$query_guest = $this->db1->insert('hotel_stay_guest',$stay_guest);
				$l_count++;
			}
			
			
			
			
			
			
        }// end of for
		//exit;
		
		$as =array();
		$ctot=0;
		$amtG=0;
		if(isset($data['cname'])){
		foreach($data['cname'] as $key => $val){
			$arr3 = array(
			'crg_description' => $data['cname'][$key],
			'crg_quantity' => $data['qty'][$key],
			'crg_unit_price' => $data['up'][$key],
			'crg_tax' => $data['ctax'][$key],
			'crg_total' => $data['total'][$key]
			);
			//exit;
			$query=$this->db1->insert(TABLE_PRE.'charges',$arr3);
			array_push($as,$this->db1->insert_id());
			$ctot = $ctot + $data['total'][$key];
		}
		}
		$this->lineitemGroup($dataNB,$noofdays);
		//print_r($as);
			if($commission_applicable =='1'){ 
					$grandAgent= number_format($trrG,2,'.','');
					$com = ($grandAgent*$travel_agent_commission)/100;
					$amtG= number_format($com,2,'.','');
			}
			if($commission_applicable =='2'){ 
					$grandAgent= number_format(($trrG + $tfiG),2,'.','');
					$com = ($grandAgent*$travel_agent_commission)/100;
					$amtG= number_format($com,2,'.','');
			}
			if($commission_applicable =='3'){ 
					$grandAgent= number_format(($trrG + $tfiG + $ctot),2,'.','');
					$com = ($grandAgent*$travel_agent_commission)/100;
					$amtG= number_format($com,2,'.','');
			}	
		if($travel_agent_type == 'B'){
			$sqlG = "UPDATE hotel_broker SET broker_commission_total = broker_commission_total + '".$amtG."' WHERE b_id = '".$travel_agent_id."'";
			$this->db1->query($sqlG);			
		} else {
			$sqlG = "UPDATE hotel_channel SET 	channel_commission_total = 	channel_commission_total + '".$amtG."' WHERE channel_id = '".$travel_agent_id."'";
			$this->db1->query($sqlG);			
		}
		
		
		//echo $ctot;
		$cid = implode(",",$as);
		$arr2 = array(
			'charges_id' => $cid,
			'charges_cost' => $ctot
			);
        $this->db1->where('id',$insert_id);
        $query=$this->db1->update(TABLE_PRE.'group',$arr2);
		//exit;
        if($query){
            return $insert_id;
        }else{
            return false;
        }
		
		

    }

function lineitemGroup($dataNB,$noofdays){
	
			/*echo "</br>lineitemGroup</br><pre>";
			print_r($noofdays);
			echo "<pre>";
			print_r($dataNB);
			echo "</pre>";*/
			
			//$query=$this->db1->insert('booking_charge_line_item',$dataNB);
			for($q=0;$q<count($dataNB);$q++){
				//echo $q;
				for($u=0;$u<$noofdays[$q]['dayspace'];$u++){
					
				
					if($u!=0){
						$date=date_create($dataNB[$q]['date']);
						date_add($date,date_interval_create_from_date_string("1 days"));
						$dataNB[$q]['date'] = date_format($date,"Y-m-d");
					}
					$query=$this->db1->insert('booking_charge_line_item',$dataNB[$q]);
				}
			}
			//exit();
}

function delete_purchase($id){
		$this->db1->where('id',$id);
         $query=$this->db1->delete(TABLE_PRE.'purchase');
         if($query){
             return true;
         }else{
             return false;
         }


}

     function  getPurchaseById($id){
         $this->db1->where('id',$id);
         $query=$this->db1->get(TABLE_PRE.'purchase');
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }
     }

		function into_stay_guest($data,$bktype){
			
			if($bktype == 'gb'){
				
				$this->db1->insert('hotel_stay_guest',$data);
			}
			else{
				
				$this->db1->insert('hotel_stay_guest',$data);
			}
			
		}

		function into_stay_guest_grp($data1){
			
			$query = $this->db1->insert('hotel_stay_guest',$data1);
			if($query){
			return true;
		}
		else{
			return false;
		}
			
		}
		
		function guest_verified($gid){
			
			
				$this->db1->select('*');
				$this->db1->where('g_id',$gid);
				$this->db1->where('g_id_proof!=',' ');
				$query=$this->db1->get('hotel_guest');
				if($query->num_rows()>0){
					
					return '1';
				}
				else{
					return '0';
				}
			
		}

    function get_group_details($grp){
        $sql="select * from hotel_groups_bookings_details where group_booking_id='".$grp."'";
        $query=$this->db1->query($sql);
        return $query->result();
    }
	
	

    function get_group($grp){
        $sql="select * from hotel_group where id='".$grp."'";
        $query=$this->db1->query($sql);
        return $query->result();
    }
	function get_group1($grp){
        $sql="select * from hotel_group where id='".$grp."'";
        $query=$this->db1->query($sql);
        return $query->row();
    }

	function move_group_details_shadow($detail){
		$query=$this->db1->insert(TABLE_PRE.'groups_bookings_details_shadow',$detail);
		if($query){
			return true;
		}
		else{
			return false;
		}
	}

	function get_group_transaction_details($id){
		$this->db1->select('*');
        $this->db1->where('t_group_id',$id);
      	$query=$this->db1->get(TABLE_PRE.'transactions');
        return $query->result();
	}

	function move_group_transaction_shadow($detail){
		$query=$this->db1->insert(TABLE_PRE.'transactions_shadow',$detail);
		if($query){
			return true;
		}
		else{
			return false;
		}
	}

	function delete_group_transaction($id){
		$this->db1->where('t_group_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'transactions');
         if($query){
             return true;
         }else{
             return false;
         }
	}


	function move_group_shadow($data){
		$query=$this->db1->insert(TABLE_PRE.'group_shadow',$data);
		if($query){
			return true;
		}
		else{
			return false;
		}
	}

	 function move_extra_charge_shadow($data){
	   $query=$this->db1->insert(TABLE_PRE.'charges',$data);
		if($query){
			return true;
		}
		else{
			return false;
		}
   }

	function get_pos($booking_id){

        $this->db1->select('*');
        $this->db1->where('booking_id',$booking_id);
        //$this->db1->where('booking_type','gb');
		$query=$this->db1->get(TABLE_PRE.'pos');
        return $query->result();
	}

	function get_pos_details($id){
		 $this->db1->select('*');
        $this->db1->where('booking_id',$id);
        $query=$this->db1->get(TABLE_PRE.'pos');
		if($query->num_rows()>0)
		{
        return $query->result();
		}
		else
		{
			return false;
		}
	}


	function pos_count($id){
		 $this->db1->select('*');
        $this->db1->where('booking_id',$id);
        $query=$this->db1->get(TABLE_PRE.'pos');
        return $query->num_rows();
	}

	function delete_pos($id){
		$this->db1->where_in('booking_id',$id);
        $query=$this->db1->delete(TABLE_PRE.'pos');
        if($query){
            return true;
        }else{
            return false;
        }
	}

	function delete_extra_charge_by_id($id){
		$this->db1->where_in('crg_id',$id);
        $query=$this->db1->delete(TABLE_PRE.'charges');
        if($query){
            return true;
        }else{
            return false;
        }
	}



	function move_pos_shadow($data){
		$query=$this->db1->insert(TABLE_PRE.'pos_shadow',$data);
		if($query){
			return true;
		}
		else{
			return false;
		}
	}
    function get_booking_details_grp($grp){
          $sql="select * from hotel_bookings left join hotel_room on hotel_bookings.room_id=hotel_room.room_id where group_id='".$grp."'";
        $query=$this->db1->query($sql);
		//print_r($query->result());
		//exit;
        return $query->result();
    }
	
	function get_booking_details_grp_new($single_bkid){
		
        $sql="select * 
			  from hotel_bookings 
			  left join hotel_room on hotel_bookings.room_id=hotel_room.room_id 
			  where booking_id='".$single_bkid."'";

        $query=$this->db1->query($sql);
		//print_r($query->result());
		//exit;
        return $query->row();
    }
	
	
	
	
	function get_booking_lineitem_details_grp($grp){
		
		$this->db1->select('*');
		$this->db1->where('group_id',$grp);
		$query = $this->db1->get('booking_charge_line_item');
		if($query->num_rows()>0)
		{
        return $query->result();
		}
		else
		{
			return false;
		}
		
	}

        function all_transaction_group($id){
        $this->db1->order_by('t_id','desc');
        $this->db1->where('t_group_id=',$id);
        //$this->db1->limit($limit,$start);
        $query=$this->db1->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

     function all_group(){
        //$sql="select * from hotel_group ";
        //$query=$this->db1->query($sql);
        $this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
	//	$this->db1->where('user_id',$this->session->userdata('user_id'));  
       $query= $this->db1->get(TABLE_PRE.'group');
		if($query){
		return $query->result();
		}else{
			return false;
		}
    }


	function update_purchase($a,$b,$id){
		//print_r($a);
        //exit;
		$this->db1->where('id',$id);
        $query=$this->db1->update(TABLE_PRE.'purchase',$a);
        if($query){
            return true;
        }else{
            return false;
        }

	}


	function all_guest_by_date($start_date,$end_date){
		$this->db1->distinct();
		$this->db1->select('group_id');
		$this->db1->order_by('booking_id','asc');
        $this->db1->where('cust_from_date>=', $start_date);
        $this->db1->where('cust_from_date <=', $end_date);
		$query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

	}

	function get_grp_details($grp_id){
		//$this->db1->order_by('group_id',$grp_id);
        $query=$this->db1->get(TABLE_PRE.'group');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}

	function get_grp_details_tax($grp_id){
		$this->db1->select('tax_applicable');
		$this->db1->where('id',$grp_id);
		$query=$this->db1->get(TABLE_PRE.'group');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
	}

	function get_group_detalis($group_id){
		 $sql="select * from hotel_groups_bookings_details where group_booking_id='".$group_id."'";
        $query=$this->db1->query($sql);
		if($query->num_rows()>0)
		{
        return $query->result();
		}

	}


	function get_room_feature_charge($rid,$bid){
		$this->db1->select('room_feature_charge');
        $this->db1->where('room_id=',$bid);
		$this->db1->where('room_feature_id=',$rid);
        $query=$this->db1->get(TABLE_PRE.'room_room_features');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }

        function get_group_id($rid){
        $this->db1->select('group_id');
        $this->db1->where('booking_id',$rid);

        $query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }

	function getAdditionalDiscount($a,$b){
		$arr = array(
			'additional_discount' => $a
			);
		$this->db1->where('id',$b);
        $query=$this->db1->update(TABLE_PRE.'group',$arr);
        if($query){
            return true;
        }else{
            return false;
        }
	}

    function getAdditionalDiscountSingle($a,$b){
        $arr = array(
            'discount' => $a,

            );
        $this->db1->where('booking_id',$b);
        $query=$this->db1->update(TABLE_PRE.'bookings',$arr);
        if($query){
            return true;
        }else{
            return false;
        }
    }



		function getUnitDetail($id){
		$wh=array('id'=>$id );
		$this->db1->select('*')->from(TABLE_PRE.'unit_type')->where($wh);
        $query=$this->db1->get();
        if($query->num_rows()>0){
            return $query->row();
}else{
            return false;
        }
    }

    function get_profit_center(){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query = $this->db1->get(TABLE_PRE.'profit_center');
        if($query->num_rows()>0){
            return $query->result();

        }else{
            return false;
        }
    }


        function updateUnit($unit){
			  $this->db1->where('id',$unit['id']);
			$query=$this->db1->update(TABLE_PRE.'unit_type',$unit);
        if($query){
            return true;
        }else{
            return false;
        }

		}


    function insert_finance($setup){
        $query = $this->db1->insert(TABLE_PRE.'finance',$setup);
        if($query){
            return $this->db1->insert_id();
        }else{
            return false;
        }
    }


    function get_setting($hotel_id){
        $this->db1->where('fin_hotel_id',$hotel_id);
        $query = $this->db1->get(TABLE_PRE.'finance');
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    function update_finance($setup){
        $this->db1->where('fin_hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->update(TABLE_PRE.'finance',$setup);
        if($query){
            return true;
        }else{
            return false;
        }
    }




	   function fetch_gen_by_id($id)
    {
			$wh=array('dgset_id'=>$id);
		$this->db1->select('fuel_consumption,fuel')->from(TABLE_PRE.'dgset')->where($wh);
        $query=$this->db1->get();
        if($query->num_rows()>0)
        {
            return $query->row();
        }else
        {
            return false;
        }
    }


	function add_stock_invent($asset){
		$query = $this->db1->insert(TABLE_PRE.'stock_inventory',$asset);
        if($query){
            return true;
        }else{
            return false;
        }

	}
	function get_stock($id){
		$sql="select * from hotel_stock_qty_mapping where item_id='".$id."'";
        $query=$this->db1->query($sql);
        return $query->result();
	}


	function new_stock($item,$stock_qty,$add_pay,$l_item){
		//print_r($item);
		//exit;
		
		$this->db1->insert(TABLE_PRE.'payments',$add_pay);
		$payment_id=$this->db1->insert_id();
		$l_item['payment_id']=$payment_id;
		$l_item['hotel_id']=$this->session->userdata('user_hotel');
		$l_item['user_id']=$this->session->userdata('user_id');
		$this->db1->insert(TABLE_PRE.'line_item_details',$l_item);
		$query = $this->db1->insert(TABLE_PRE.'add_sub_stock_inventory',$item);
			$query1="UPDATE `hotel_stock_inventory` SET `qty` =qty + '".$item["qty"]."',`closing_qty` ='".$item["closing_qty"]."',`opening_qty` ='".$item["opening_qty"]."'  WHERE `hotel_stock_inventory_id` ='".$item["hotel_stock_inventory_id"]."' ";


			/*$query1="UPDATE `hotel_stock_inventory` SET `qty` =qty + '".$item["qty"]."',`closing_qty` =opening_qty + '".$item["qty"]."'  WHERE `a_category` ='".$item["a_category"]."' AND `item_type` ='".$item["a_type"]."' ";*/
		//	$id= $this->db1->insert_id();
		//	$arrVal['details_id'] = $id;
          //  $query2=$this->db1->insert(TABLE_PRE.'transactions',$arrVal);
            
		$this->db1->query($query1);
  
        if($query1){
            return true;
        }else{
            return false;
        }
	}

function update_stock($item,$stock_qty){
	$query = $this->db1->insert(TABLE_PRE.'add_sub_stock_inventory',$item);
	$query1="UPDATE `hotel_stock_inventory` SET `qty` =qty - '".$item["qty"]."',`closing_qty` ='".$item["closing_qty"]."'  WHERE `hotel_stock_inventory_id` ='".$item["hotel_stock_inventory_id"]."'";
	 $query2="UPDATE `hotel_stock_qty_mapping` SET `qty` =qty -'".$stock_qty["qty"]."' where item_id='".$stock_qty["item_id"]."' and warehouse_id='".$stock_qty["warehouse_id"]."'";
	//print_r( $query2);
	//exit;
	$q=$this->db1->query($query1);
    $q2=$this->db1->query($query2);
	if($q && $q2){
		return true;
	}else{
		return false;
	}

	}


	function insert_warehouse_stock($stock_qty){
		$query=$this->db1->insert(TABLE_PRE.'stock_qty_mapping',$stock_qty);

		if($query){
			return true;
		}else{
			return false;
		}
	}

	 function update_warehouse_stock($stock_qty){
		// print_r($stock_qty);exit;
		 $query="UPDATE `hotel_stock_qty_mapping` SET `qty` =qty +'".$stock_qty["qty"]."' where item_id='".$stock_qty["item_id"]."' and warehouse_id='".$stock_qty["warehouse_id"]."'";
		$query1=$this->db1->query($query);
		if($query1){
			return true;
		}else{
			return false;
		}
	 }
	 
	 
	 
	 function update_consumption_warehouse_stock($stock_qty){
		// print_r($stock_qty);exit;
		 $query="UPDATE `hotel_stock_qty_mapping` SET `qty` =qty -'".$stock_qty["qty"]."' where item_id='".$stock_qty["item_id"]."' and warehouse_id='".$stock_qty["warehouse_id"]."'";
		$query1=$this->db1->query($query);
		if($query1){
			return true;
		}else{
			return false;
		}
	 }


	function get_qty($w_id,$item_id){
		$this->db1->select('qty');
		$this->db1->where('warehouse_id',$w_id);
		$this->db1->where('item_id',$item_id);
		$query=$this->db1->get(TABLE_PRE.'stock_qty_mapping');

		if($query){
			return $query->row();
		}
	}
	function chk_warehouse($wh){
		$this->db1->select('*');
		$this->db1->where('warehouse_id',$wh['to_warehouse_id']);
		$this->db1->where('item_id',$wh['item_id']);
		$query = $this->db1->get(TABLE_PRE.'stock_qty_mapping');
        if($query){
            return $query->num_rows();
        }else{
            return false;
        }
	}

	function transfer_asset($transfer_asset){
	//	print_r($transfer_asset) ;//exit;
		$chk_wh=$this->dashboard_model->chk_warehouse($transfer_asset);
		//print_r($chk_wh); exit;
		if($chk_wh>0){
			$query=$this->db1->insert(TABLE_PRE.'transfer_asset',$transfer_asset);

			$id=$this->db1->insert_id();


			$transfer_log=array('hotel_transfer_asset_id'=>$id);
					$item_id=$transfer_asset['item_id'];
				   echo $frmwh_id=$transfer_asset['from_warehouse_id'];// exit;
					$towh_id=$transfer_asset['to_warehouse_id'];
					$get_item=$this->dashboard_model->get_item($item_id);
					$item_name=$this->dashboard_model->get_stock_name($item_id);
					$from_wh_name=$this->dashboard_model->get_warehouse($frmwh_id);
					$to_wh_name=$this->dashboard_model->get_warehouse($towh_id);

				$transfer_log=array(
						'hotel_transfer_asset_id'=>$id,
						'item'=>$item_id,
						'a_type'=>$get_item->a_type,
						'a_category'=>$get_item->a_category,
						'qty'=>$this->input->post('qty'),
						'opening_qty'=>$get_item->opening_qty,
						'closing_qty'=>$get_item->closing_qty,
						'warehouse'=>$towh_id,
						'operation'=>"Transfer",
						'note'=>"Transfer ".$item_name->a_name." from ".$from_wh_name->name." to ".$to_wh_name->name
					);


			//print_r($transfer_log);
			//exit;
			$query3 = $this->db1->insert(TABLE_PRE.'add_sub_stock_inventory',$transfer_log);

		$query1="UPDATE `hotel_stock_qty_mapping` SET `qty` =qty +'".$transfer_asset["qty"]."' where item_id='".$transfer_asset["item_id"]."' and warehouse_id='".$transfer_asset["to_warehouse_id"]."'";
		$query2="UPDATE `hotel_stock_qty_mapping` SET `qty` =qty -'".$transfer_asset["qty"]."' where item_id='".$transfer_asset["item_id"]."' and warehouse_id='".$transfer_asset["from_warehouse_id"]."'";
		$qr1=$this->db1->query($query1);
	    $qr2=$this->db1->query($query2);
		if($qr1 && $qr2 && $query3){
			return true;
		}else{
			return false;
		}
		}else{     // echo "not"; exit;
				    $t_a['hotel_id']=$transfer_asset['hotel_id'];
					$t_a['item_id']=$transfer_asset['item_id'];
					$t_a['warehouse_id']=$transfer_asset['to_warehouse_id'];
					$t_a['warehouse_id']=$transfer_asset['to_warehouse_id'];
					$t_a['qty']=$transfer_asset['qty'];



			$qr1=$this->db1->insert(TABLE_PRE.'stock_qty_mapping',$t_a);
			$query2="UPDATE `hotel_stock_qty_mapping` SET `qty` =qty -'".$transfer_asset["qty"]."' where item_id='".$transfer_asset["item_id"]."' and warehouse_id='".$transfer_asset["from_warehouse_id"]."'";

			$query=$this->db1->insert(TABLE_PRE.'transfer_asset',$transfer_asset);
			$id=$this->db1->insert_id();



				 $item_id=$transfer_asset['item_id'];
				 $frmwh_id=$transfer_asset['from_warehouse_id'];
					$towh_id=$transfer_asset['to_warehouse_id'];
					$get_item=$this->dashboard_model->get_item($item_id);
					$item_name=$this->dashboard_model->get_stock_name($item_id);
					$from_wh_name=$this->dashboard_model->get_warehouse($frmwh_id);
					$to_wh_name=$this->dashboard_model->get_warehouse($towh_id);

				$transfer_log=array(
						'hotel_transfer_asset_id'=>$id,
						'item'=>$item_id,
						'a_type'=>$get_item->a_type,
						'a_category'=>$get_item->a_category,
						'qty'=>$this->input->post('qty'),
						'opening_qty'=>$get_item->opening_qty,
						'closing_qty'=>$get_item->closing_qty,
						'warehouse'=>$towh_id,
						'operation'=>"Transfer",
						'note'=>"Transfer ".$item_name->a_name." from ".$from_wh_name->name." to ".$to_wh_name->name
					);
				//print_r($transfer_log); exit;
			$query3 = $this->db1->insert(TABLE_PRE.'add_sub_stock_inventory',$transfer_log);
			$qr2=$this->db1->query($query2);

		if( $query && $qr1 && $qr2 && $query3){
			return true;
		}else{
			return false;
		}
		}



	}

function get_item($item_id){
		$this->db1->select('*');
		$this->db1->order_by('hotel_add_sub_stock_inventory_id','desc');
		$this->db1->limit(1,0);

		$this->db1->where('item',$item_id);
		$query = $this->db1->get(TABLE_PRE.'add_sub_stock_inventory');
		if($query){
            return $query->row();
        }else{
            return false;
        }
	}
	function all_transfer_asset(){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		$this->db1->where('user_id',$this->session->userdata('user_id'));  
		$query=$this->db1->get(TABLE_PRE.'transfer_asset');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

	}

	function get_stock_category($id){
		$this->db1->select('name');
		$this->db1->where('id',$id);
		$query = $this->db1->get(TABLE_PRE.'stock_inventory_category');
        if($query){
            return $query->row();
        }else{
            return false;
        }
	}

	function get_stock_item($id){
		$this->db1->select('a_name');
		$this->db1->where('hotel_stock_inventory_id',$id);
		$query = $this->db1->get(TABLE_PRE.'stock_inventory');
        if($query){
            return $query->row();
        }else{
            return false;
        }
	}

	function all_transfer_asset_by_date($start_date,$end_date){
		$this->db1->select('*');

        $this->db1->where("date_format(date,'%Y-%m-%d')>=", $start_date);
        $this->db1->where("date_format(date,'%Y-%m-%d')<=", $end_date);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		$this->db1->where('user_id',$this->session->userdata('user_id'));  
		$query=$this->db1->get(TABLE_PRE.'transfer_asset');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}

	function fetch_ta($id){
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$query = $this->db1->get(TABLE_PRE.'transfer_asset');

        if($query){
            return $query->row();
        }else{
            return false;
        }
	}

	function delete_transfer_asset($id){
		//$id=14;
		$fetch_ta=$this->dashboard_model->fetch_ta($id);
		//print_r($fetch_ta); exit;
		$query1="UPDATE `hotel_stock_qty_mapping` SET `qty` =qty +'".$fetch_ta->qty."' where item_id='".$fetch_ta->item_id."' and warehouse_id='".$fetch_ta->from_warehouse_id."'";
		$query2="UPDATE `hotel_stock_qty_mapping` SET `qty` =qty -'".$fetch_ta->qty."' where item_id='".$fetch_ta->item_id."' and warehouse_id='".$fetch_ta->to_warehouse_id."'";
		$qr1=$this->db1->query($query1);
	    $qr2=$this->db1->query($query2);
		//print_r($query1);
		//print_r($query2);
		//exit;
		$this->db1->where_in('id',$id);
        $query=$this->db1->delete(TABLE_PRE.'transfer_asset');
		$this->db1->where_in('hotel_transfer_asset_id',$id);
		$query1=$this->db1->delete(TABLE_PRE.'add_sub_stock_inventory');

        if($query && $query1 && $qr1 && $qr2){
            return true;
        }else{
            return false;
        }
	}


	function get_item_name($cat_name,$cat_type){
		$this->db1->select('a_name,hotel_stock_inventory_id');
		$this->db1->where('a_category',$cat_name);
	//	$this->db1->where('item_type',$cat_type);
		$query = $this->db1->get(TABLE_PRE.'stock_inventory');
        if($query){
            return $query->result();
        }else{
            return false;
        }
	}

	function get_item_name1($item_id){
		$this->db1->select('a_name');
		$this->db1->where('hotel_stock_inventory_id',$item_id);
		$query = $this->db1->get(TABLE_PRE.'stock_inventory');
        if($query){
            return $query->row();
        }else{
            return false;
        }
	}

	function get_item_name_by_wh_id($wh_id){

		$this->db1->select('item_id');
		$this->db1->where('warehouse_id',$wh_id);
		$query = $this->db1->get(TABLE_PRE.'stock_qty_mapping');
        if($query){
            return $query->result();
        }else{
            return false;
        }


	}

	function get_warehouse($id){

		$this->db1->select('name');
		$this->db1->where('id',$id);
		$query = $this->db1->get(TABLE_PRE.'warehouse');
        if($query){
            return $query->row();
        }else{
            return false;
        }
	}

	
	function get_warehouse_kit($id){

		$this->db1->select('name');
		$this->db1->where('id',$id);
		$query = $this->db1->get(TABLE_PRE.'warehouse');
        if($query){
            return $query->row()->name;
        }else{
            return false;
        }
	}
	
	
	
	function get_wh_id_from_mpping($id){
		$this->db1->select('warehouse_id');
		$this->db1->where('item_id',$id);
		$query = $this->db1->get(TABLE_PRE.'stock_qty_mapping');
        if($query){
            return $query->result();
        }else{
            return false;
        }
	}

	function get_warehouse_details(){
		$this->db1->select('*');

		$query = $this->db1->get(TABLE_PRE.'stock_qty_mapping');
        if($query){
            return $query->result();
        }else{
            return false;
        }
	}

	function get_stock_id($name){
		$this->db1->select('hotel_stock_inventory_id');
		$this->db1->where('a_name',$name);
		//$this->db1->where('hotel_stock_inventory_id',$id);

		$query = $this->db1->get(TABLE_PRE.'stock_inventory');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
	}


	function get_item_price($id){
		$this->db1->select('u_price,hotel_stock_inventory_id,qty,opening_qty,closing_qty,warehouse,l_stock');
		$this->db1->where('hotel_stock_inventory_id',$id);
		$query = $this->db1->get(TABLE_PRE.'stock_inventory');
        if($query){
            return $query->row();
        }else{
            return false;
        }
	}

	function all_stock_invent(){
		 /*$sql="select * from hotel_stock_inventory ";
        $query=$this->db1->query($sql);
        return $query->result();*/
		
		 $this->db1->select('*');
		 $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		 //$this->db1->where('user_id',$this->session->userdata('user_id'));  
		$query = $this->db1->get(TABLE_PRE.'stock_inventory');
        if($query){
            return $query->result();
        }else{
            return false;
        }
		
		
		
	}

	function get_stock_name($id){
		 $this->db1->select('a_name');
		 $this->db1->where('hotel_stock_inventory_id',$id);
        $query = $this->db1->get(TABLE_PRE.'stock_inventory');
        if($query){
            return $query->row();
        }else{
            return false;
        }
	}

	function chk_duplicate_name($name){
		 $this->db1->where('a_name',$name);
        $query = $this->db1->get(TABLE_PRE.'stock_inventory');
        if($query){
            return $query->num_rows();
        }else{
            return false;
        }
	}

	function all_stock_invent_by_date($start_date,$end_date){
		$this->db1->select('*');
        $this->db1->where('date>=', $start_date);
        $this->db1->where('date <=', $end_date);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
       $this->db1->where('user_id',$this->session->userdata('user_id'));  
		$query=$this->db1->get(TABLE_PRE.'stock_inventory');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	
	function search_kit_autocomplete(){
		$this->db1->select('*');
       
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
       $this->db1->where('user_id',$this->session->userdata('user_id'));  
		$query=$this->db1->get(TABLE_PRE.'stock_inventory');
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return false;
        }
	}
	
	
	function all_new_stock_invent_by_date($start_date,$end_date){
		$this->db1->select('*');
		$this->db1->order_by('hotel_add_sub_stock_inventory_id','desc');
		//$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') >=",$date_1);
        $this->db1->where('date>=',$start_date);
        $this->db1->where('date <=',$end_date);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		$this->db1->where('user_id',$this->session->userdata('user_id'));  
		$query=$this->db1->get(TABLE_PRE.'add_sub_stock_inventory');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }




	}
	function all_new_stock_invent(){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		//$this->db1->where('user_id',$this->session->userdata('user_id'));  
		$query=$this->db1->get(TABLE_PRE.'add_sub_stock_inventory');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}



	function delete_stock_invent($id){
		$this->db1->where('hotel_stock_inventory_id',$id);
		$query=$this->db1->delete(TABLE_PRE.'stock_inventory');
		$this->db1->where('hotel_stock_inventory_id',$id);
		$query1=$this->db1->delete(TABLE_PRE.'add_sub_stock_inventory');
		$this->db1->where('item_id',$id);
		$query2=$this->db1->delete(TABLE_PRE.'stock_qty_mapping');
		$this->db1->where('item_id',$id);
		$query3=$this->db1->delete(TABLE_PRE.'transfer_asset');
		if($query && $query1 && $query2 && $query3){
			return true;
		}
		else{
			return false;
		}
	}
	function get_auditor_name($audi_id){
        $this->db1->where('maid_id',$audi_id);
        $query = $this->db1->get(TABLE_PRE.'housekeeping_maid');
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }


    function get_all_tax(){
        $query=$this->db1->get(TABLE_PRE.'tax');
        if($query){
            return $query->result();
        }else{
            return false;
        }
    }

    function maid_is_available($md_id){
        $this->db1->where('maid_id',$md_id);
        $query = $this->db1->get(TABLE_PRE.'housekeeping_maid');
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    function get_maid_row($room_id){
        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'housekeeping_matrix');
        $this->db1->join(TABLE_PRE.'housekeeping_maid', 'hotel_housekeeping_matrix.maid_id = hotel_housekeeping_maid.maid_id');
        $this->db1->where('room_id',$room_id);
        $query = $this->db1->get();
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    function generate_housekeeping_log_entry($data4){
        $query = $this->db1->insert(TABLE_PRE.'housekeeping_logs',$data4);
        if($query){
            return $this->db1->insert_id();
        }else{
            return false;
        }
    }

    function generate_housekeeping_log_update($cleared_time,$room_id,$maid_id){
        $this->db1->set('room_cleared_on',$cleared_time);
        $this->db1->where('room_id',$room_id);
        $this->db1->where('maid_id',$maid_id);
        $query=$this->db1->update(TABLE_PRE.'housekeeping_logs');
        if($query){
            return $this->db1->insert_id();
        }else{
            return false;
        }
    }

    function get_housekeeping_logs(){
        $this->db1->where('hotel_housekeeping_hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->order_by("log_id", "desc");
        $query = $this->db1->get(TABLE_PRE.'housekeeping_log_detail');
		
		if($query){
            return $query->result();
        }else{
            return false;
        }
    }

    function get_maid_by_id($id_val){
        $this->db1->where('maid_id',$id_val);
        $query = $this->db1->get(TABLE_PRE.'housekeeping_maid');
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    function get_room_by_id_2($id_val){
        $this->db1->where('room_id',$id_val);
        $query = $this->db1->get(TABLE_PRE.'room');
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    function delete_maid($maid_id){
        $this->db1->where_in('maid_id',$maid_id);
        $query=$this->db1->delete(TABLE_PRE.'housekeeping_maid');
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function update_maid_2($data,$id_val){
        $this->db1->where('maid_id',$id_val);
        $query = $this->db1->update('hotel_housekeeping_maid',$data);
        if($query){
            return $this->db1->insert_id();
        }
        else{
            return false;
        }
    }



	function view_all_fuel_log()
	{
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
		$this->db1->order_by('id','desc');
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query = $this->db1->get(TABLE_PRE.'add_sub_fuel_log');
        if($query){
            return $query->result();
        }else{
            return false;
        }

	}
	function delete_fuel_item($a)
	{
		$data =
		 array(
			 'id'=>$a,
			  );

		 $this->db1->delete(TABLE_PRE.'add_sub_fuel_log',$data);
	}

    function add_tax($data){
        $query = $this->db1->insert(TABLE_PRE.'tax',$data);
        if($query){
            return $this->db1->insert_id();
        }
        else{
            return false;
        }
    }


    function get_booking_extra_charge_adult($id)
    {
        $this->db1->where('room_id',$id);
        $this->db1->select('adult_rate');
        $query = $this->db1->get(TABLE_PRE.'room');
        if($query){
            return $query->row();
        }else{
            return false;
        }

    }


    function get_booking_extra_charge_child($id)
    {
        $this->db1->where('room_id',$id);
        $this->db1->select('kid_rate');
        $query = $this->db1->get(TABLE_PRE.'room');
        if($query){
            return $query->row();
        }else{
            return false;
        }

    }
	 function chk_duplicate_fuel_type($type){
		 $this->db1->where('fuel_name',$type);
		 $query=$this->db1->get(TABLE_PRE.'master_fuel_stock');
		 if($query){
			 return $query->num_rows();
		 }
	 }

	function fetch_fuel_name($id){
		$this->db1->select('fuel_name');
		$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('id',$id);
		 $query=$this->db1->get(TABLE_PRE.'master_fuel_stock');
		 if($query){
			 return $query->row();
		 }
	}
	function fetch_quantity($f_id){
		$this->db1->select('reorder_level,cur_stock');
		$this->db1->where('id',$f_id);
		 $query=$this->db1->get(TABLE_PRE.'master_fuel_stock');
		 if($query){
			 return $query->row();
		 }
	}

	public function get_master_fuel_stock()
	{
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	  // $this->db1->select('*')->from(TABLE_PRE.'master_fuel_stock');
	   $query = $this->db1->get(TABLE_PRE.'master_fuel_stock');
	   if($query){
            return $query->result();
        }else{
            return false;
        }

	}



	 function update_master_fuel($up_array)
 {
        $fuel_name=$up_array['fuel_name'];
		$quantity=$up_array['quantity'];
        $this->db1->where_in('id', $fuel_name);
		if($up_array['in_out_type']=='add')
		{
		$this->db1->set('cur_stock', 'cur_stock+'.$quantity, FALSE);
		}
		else
		{
		$this->db1->set('cur_stock', 'cur_stock-'.$quantity, FALSE);

		}
        $query_details = $this->db1->update(TABLE_PRE.'master_fuel_stock');
        if($query_details){
            return true;
        }
        else{
            return false;
        }
    }
	function all_note_preference(){
		$this->db1->order_by('booking_id','asc');
        $this->db1->where('cust_from_date>=', $start_date);
        $this->db1->where('cust_from_date <=', $end_date);
		$query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	function get_status_by_id($status_id){
			//$this->db1->select('booking_status');
			$this->db1->where('status_id',$status_id);
        $query = $this->db1->get('booking_status_type');

		if($query){
            return $query->result();
        }else{
            return false;
        }
	}


	function add_fuel($data)
	{
    $query=$this->db1->insert(TABLE_PRE.'master_fuel_stock',$data);
	if($query)
		return  true;
	else
		return ;
	}




	function delete_fuel_log_by_usage_id($id)
	{
		$del_fuel=array ('usage_log_id'=>$id);
		$this->db1->delete(TABLE_PRE.'add_sub_fuel_log',$del_fuel);
	}

		function get_dg_rating()
		{
			//$this->db1->where('user_id',$this->session->userdata('user_id'));
       // $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$query = $this->db1->get(TABLE_PRE.'dg_rating');
		return $query->result();
		}

	function get_fuel_qty_type($id)
	{
		$this->db1->select('*');
	    $this->db1->where('usage_log_id',$id);
		//$del_fuel=array ('usage_log_id'=>$id);
		$query=$this->db1->get(TABLE_PRE.'add_sub_fuel_log');
		 $ret=$query->row();
		if($ret)
		{
			return $ret;
		}
		else
		{
			$this->db1->select('*');
	    $this->db1->where('id',$id);
		//$del_fuel=array ('usage_log_id'=>$id);
		$query=$this->db1->get(TABLE_PRE.'add_sub_fuel_log');
		return $query->row();

		}

		 }

	function delete_transaction_by_details_id($id)
	{
		$del_fuel=array ('details_id'=>$id);
		$this->db1->delete(TABLE_PRE.'transactions',$del_fuel);

	}


	function getNatureVisit(){
        $this->db1->order_by('booking_nature_visit_name','asc');
        $query=$this->db1->get(TABLE_PRE.'booking_nature_visit');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	//pop new booking (except default one)
	function getNatureVisit1(){
        
		//$this->db1->where('nature_visit_default','0');
		$this->db1->where('status','1');
		$this->db1->order_by('booking_nature_visit_name','asc');
        $query=$this->db1->get(TABLE_PRE.'booking_nature_visit');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	function default_nature_visit(){
        $this->db1->select('booking_nature_visit_id,booking_nature_visit_name');
		$this->db1->where('nature_visit_default','1');
        $query=$this->db1->get(TABLE_PRE.'booking_nature_visit');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return $query->row();
        }
    }

	function getNatureVisitById($id){
        $this->db1->where('booking_nature_visit_id',$id);
        $query=$this->db1->get(TABLE_PRE.'booking_nature_visit');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }

	function getsourceId($id){
        $this->db1->where('booking_source_id',$id);
        $query=$this->db1->get(TABLE_PRE.'booking_source');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }
	
	function get_booking_source_type(){
        $query=$this->db1->get('booking_source_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function get_booking_source_type_by_id($id){
        $this->db1->where('bst_id',$id);
        $query=$this->db1->get('booking_source_type');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }


	function getmarketingId($id){
        $this->db1->where('id',$id);
        $query=$this->db1->get(TABLE_PRE.'marketing_personnel');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }

     function change_booking_status($id,$b_id){
        $sql="update hotel_bookings set booking_status_id='".$id."' where booking_id='".$b_id."'   ";
        $query=$this->db1->query($sql);
        return $query;
    }


	function update_f_item($f_id)
	{
		$f_up=array('status'=>'returned');
		$this->db1->where('f_id',$f_id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->update(TABLE_PRE.'found_item',$f_up);
	}
	function update_l_item($l_id)
	{
		$l_up=array('status'=>'recovered');
		$this->db1->where('l_id',$l_id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->update(TABLE_PRE.'lost_item',$l_up);

	}


   function  add_food_plan($data1,$data2){
		$query=$this->db1->insert(TABLE_PRE.'food_plan',$data1);
		$insert_id = $this->db1->insert_id();

		foreach($data2['s_name'] as $key => $val){
			$arr = array(
			's_name' => $data2['s_name'][$key],
			'qty' => $data2['qty'][$key],
			's_unit_cost' => $data2['s_unit_cost'][$key],
			'food_plan_id' => $insert_id
			);
			//exit;
			$query=$this->db1->insert(TABLE_PRE.'food_plan_mapping',$arr);
		}
		//exit;
        if($query){
            return true;
        }else{
            return false;
        }

    }


	function all_food_plans(){
        $this->db1->where('hotel_id',$this->session->userdata('user_id'));
        $this->db1->order_by('id','desc');
        $query=$this->db1->get(TABLE_PRE.'food_plan');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	function add_vendor($data)
	{
		$query=$this->db1->insert(TABLE_PRE.'vendor',$data);
		return $query;
	}





			function get_parent_vendor()
			{
				$this->db1->where('hotel_vendor_heirc',"n");
				$query=$this->db1->get(TABLE_PRE.'vendor');
				return $query->result();

			}

			function all_vendor()
			{
				$this->db1->select('*');
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
				//$this->db1->where('user_id',$this->session->userdata('user_id'));  
				$this->db1->where('status','1');
				$query=$this->db1->get(TABLE_PRE.'vendor');
				return $query->result_array();

			}
			
				function all_vendors()
			{
				$this->db1->select('*');
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
			//	$this->db1->where('user_id',$this->session->userdata('user_id'));  
				//$this->db1->where('status','1');
				$query=$this->db1->get(TABLE_PRE.'vendor');
				return $query->result();

			}

			function delete_vendor($id){
		 $this->db1->where('hotel_vendor_id',$id);
         $query=$this->db1->delete(TABLE_PRE.'vendor');
         if($query){
             return true;
         }else{
             return false;
         }
	}


			function get_unit_type_by_id($id)
			{	$this->db1->select('unit_name');
				$this->db1->where('id',$id);
				$query=$this->db1->get(TABLE_PRE.'unit_type');
				if($query){
					return $query->row();
				}
			//$res= $query->row();
				//return $res->unit_name;
			}
			
			function get_unit_type_by_id1($id)
			{	$this->db1->select('*');
				$this->db1->where('id',$id);
				$query=$this->db1->get(TABLE_PRE.'unit_type');
				if($query){
					return $query->row();
				}
			//$res= $query->row();
				//return $res->unit_name;
			}

			/*function search_booking_by_room_id($id)
			{
				$this->db1->select('*');
				$this->db1->where('room_id='.$id.'cust_from_date_actual_1 >');
				$query=$this->db1->get(TABLE_PRE.'booking');
				$res= $query->num_rows();
				return $res;
			}*/


	 function add_kit($data1){
		 $query=$this->db1->insert(TABLE_PRE.'define_kit',$data1);
		 $insert_id = $this->db1->insert_id();
        if(isset($query)){
            return $insert_id;
        }else{
            return false;
        }


	 }

	 function add_kit_mapping($data)
	 {
		 $query=$this->db1->insert(TABLE_PRE.'kit_mapping',$data);
	 }


     function serviceCostById($id){
		$this->db1->select('s_price');
	    $this->db1->where('s_id',$id);
		$query=$this->db1->get(TABLE_PRE.'service');
		return $query->row();
     }



 function get_all_staff_type(){
        $query = $this->db1->get(TABLE_PRE.'housekeeping_staff_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
			function get_staff_by_type($a)
			{
			$this->db1->where('type',$a);
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));      
			$this->db1->where("hotel_housekeeping_maid.alo_unit < hotel_housekeeping_maid.max_alo_unit");
			$query = $this->db1->get(TABLE_PRE.'housekeeping_maid');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
			}

//}
		function set_housekeeping_log_entry($a)
		{
			$query=$this->db1->insert(TABLE_PRE.'housekeeping_log_detail',$a);
			return $query;
		}

		function get_unit_class_list()
		{
			//echo $this->session->userdata('user_hotel');
			//exit;
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			//$this->db1->where('user_id',$this->session->userdata('user_id'));
			//$this->db1->get(TABLE_PRE.'unit_class');
			$query=$this->db1->get(TABLE_PRE.'unit_class');
			return $query->result();
		}

		function get_all_kit()
		{
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$query=$this->db1->get(TABLE_PRE.'define_kit');
			return $query->result();
		}


		function get_housekeeping_pending_asso_staff($room_id,$staff_type,$prop_type) {
			
			$this->db1->where('hotel_housekeeping_hotel_id',$this->session->userdata('user_hotel'));
			$this->db1->where('room_id',$room_id);
			$this->db1->where('staff_type',$staff_type);
			//$this->db1->where('hotel_property_type',$prop_type);
			$this->db1->where('status','pending');
			$query=$this->db1->get(TABLE_PRE.'housekeeping_log_detail');
			return $query->result();
		}
		
		function get_housekeeping_pending_asso_staff1($a,$b,$c) {
			
				if(($c=='room')||($c=='section')) {
					$this->db1->where("room_id=".$a." and staff_type='".$b."' and status ='pending' and hotel_property_type='".$c."'");
				}
				
				if($c=='all') {
					$this->db1->where("room_id=".$a." and staff_type='".$b."' and status ='pending'");
				}

			$query=$this->db1->get(TABLE_PRE.'housekeeping_log_detail');
			return $query->result();
		}


        function update_indi($due,$groupID) {

                   $sql="update `hotel_group` set indi_due='".$due."' where id='".$groupID."' ";
                   $this->db1->query($sql);

        }

		function get_fd_tax_by_id($booking_id){
			$this->db1->select('fvat,lvat');
			$this->db1->where('booking_id',$booking_id);
			$query = $this->db1->get(TABLE_PRE.'pos');

			if($query){
				return $query->row();
			}
			else{
				return false;
			}

		}

		function update_housekeeping_log_status_by_staff_type($room_id,$staff_type,$prop_type)
		{
			date_default_timezone_set('Asia/Kolkata');
			$up_array=array(
			'status'=>'Done',
			'compleated_date'=>date('Y-m-d H:i:s'),
			);
			$this->db1->where("room_id=".$room_id." and staff_type='".$staff_type."' and status='pending'");
			$this->db1->update(TABLE_PRE.'housekeeping_log_detail',$up_array);
		}

	function delete_fp($id){
		 $this->db1->where('id',$id);
         $query=$this->db1->delete(TABLE_PRE.'food_plan');
         if($query){
             return true;
         }else{
             return false;
         }
	}
	
	function delete_tax_type($tax_name){
		$this->db1->where('tax_id',$tax_name);
         $query=$this->db1->delete('hotel_tax_type');
         if($query){
             return true;
         }else{
             return false;
         }
		
		
	}

	function delete_tax_categ($tax_name){
		$this->db1->where('tc_id',$tax_name);
         $query=$this->db1->delete('hotel_tax_category');
         if($query){
             return true;
         }else{
             return false;
         }
		
		
	}

		/*function get_staff_by_id($a)
			{	//$this->db1->select('maid_name');
				$this->db1->where('maid_id',$a);
			$query = $this->db1->get(TABLE_PRE.'housekeeping_maid');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
			}*/

		function get_all_housekeeping_status()
			{$this->db1->where('type','Housekeeping');
			$query = $this->db1->get(TABLE_PRE.'housekeeping_status');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
			}

		function get_housekkeping_color_by_cleane_id()
		{
			$this->db1->where('maid_id',$a);
			$query = $this->db1->get(TABLE_PRE.'housekeeping_maid');
			if($query->num_rows()>0)
				{
				return $query->row();
				}
			else{
            return false;
				}
		}

	function allFoodPlansById($id){
        $this->db1->where('id',$id);
        $query=$this->db1->get(TABLE_PRE.'food_plan');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }




		function get_color_by_houskeeping_status_id($a)
		{
			$this->db1->where('status_id',$a);
			$query = $this->db1->get(TABLE_PRE.'housekeeping_status');
			if($query->num_rows()>0)
				{
				return $query->row();
				}
			else
				{
				return false;
				}

		}

	function allServices($id){
        $this->db1->where('food_plan_id',$id);
        $query=$this->db1->get(TABLE_PRE.'food_plan_mapping');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }


	function get_work_nature_id_by_staff_type($id)
	{	$this->db1->select('housekeeping_nature_work_id');
		 $this->db1->where('related_staff_type_name',$id);
        $query=$this->db1->get(TABLE_PRE.'housekeeping_nature_work');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
	}


	function get_room_no($id)
	{
		$this->db1->select('room_no');
		 $this->db1->where("room_id=".$id." and hotel_id=".$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
	}

	function get_assignment_type_by_id($id)
	{
		$this->db1->select('housekeeping_nature_work_name');
		 $this->db1->where("housekeeping_nature_work_id=".$id." and housekeeping_nature_work_hotel_id=".$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'housekeeping_nature_work');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }

	}



	function get_room_housekeeping(){
		 $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query = $this->db1->get(TABLE_PRE.'room');
		if($query){
            return $query->result();
        }else{
            return false;
        }
    }

    function  edit_food_plan($data1,$data2,$id){
		$this->db1->where('id',$id);
		$this->db1->update(TABLE_PRE.'food_plan',$data1);

        $this->db1->where('food_plan_id',$id);
        $query_delete=$this->db1->delete(TABLE_PRE.'food_plan_mapping');

		foreach($data2['s_name'] as $key => $val){
			$arr = array(
			's_name' => $data2['s_name'][$key],
			'qty' => $data2['qty'][$key],
			's_unit_cost' => $data2['s_unit_cost'][$key],
			'food_plan_id' => $id
			);
			//exit;
			$query=$this->db1->insert(TABLE_PRE.'food_plan_mapping',$arr);
		}
		//exit;
        if($query){
            return true;
        }else{
            return false;
        }

    }

	function get_all_housekeeping_status_by_id($id)
		{
				//$this->db1->select('status_name');
				$this->db1->where('status_id',$id);
				$query = $this->db1->get(TABLE_PRE.'housekeeping_status');
				if($query->num_rows()>0)
					{
					return $query->row();
					}
				else
					{
					return false;
					}
		}

	function get_section()
	{
		$query=$this->db1->get(TABLE_PRE.'section');
		return $query->result();
	}

	function get_tax_details($hotel_id){
	$this->db1->select('*');
	$this->db1->where('hotel_id',$hotel_id);
	$query=$this->db1->get(TABLE_PRE.'hotel_billing_setting');
	 if($query->num_rows()>0){
            return $query->result();
        }
        else{
            return false;
        }

	}

	function update_auditing_status($room,$prop_type)
	{
	if($prop_type=='room')
		{
        $this->db1->where("room_id=".$room['room_id']." and hotel_id=".$this->session->userdata('user_hotel')."");
        $query=$this->db1->update(TABLE_PRE.'room',$room);
		}
		if($prop_type=='section')
		{
        $this->db1->where("sec_id=".$room['sec_id']." and sec_hotel_id=".$this->session->userdata('user_hotel')."");
        $query=$this->db1->update(TABLE_PRE.'section',$room);
		}

        if($query){
            return true;
        }else{
            return false;
        }
	}

		function update_maid_alo_unit($a,$b)
		{
			if($b=="+1")
			{
				$this->db1->where("maid_id=".$a);
				$this->db1->set('alo_unit','alo_unit+1', FALSE);
				$qry=$this->db1->update(TABLE_PRE.'housekeeping_maid');
				if($qry)
				{
				return true;
				}
			}
			else
			{
				$this->db1->where("maid_id=".$a);
				$this->db1->set('alo_unit','alo_unit-1', FALSE);
				$qry=$this->db1->update(TABLE_PRE.'housekeeping_maid');
				if($qry)
				{
				return true;
				}
			}
		}

		  function get_guest_detail($guest_id){
		$this->db1->select('*');
		$this->db1->where('g_id',$guest_id);
		$query=$this->db1->get(TABLE_PRE.'guest');
		if($query->num_rows()>0){
            return $query->result();
        }
        else{
            return false;
        }

	}
	
	  function get_guest_detail_row($guest_id){
		$this->db1->select('*');
		$this->db1->where('g_id',$guest_id);
		$query=$this->db1->get(TABLE_PRE.'guest');
		if($query->num_rows()>0){
            return $query->row();
        }
        else{
            return false;
        }

	}

	function get_company_name($corporate_id){
		$this->db1->select('name');
		$this->db1->where('hotel_corporate_id',$corporate_id);
		$query=$this->db1->get(TABLE_PRE.'corporate');
		if($query->num_rows()>0){
            return $query->row();
        }
        else{
            return false;
        }
	}

	function get_invoice($bd_id){
		$this->db1->select('*');
		$this->db1->where('related_id',$bd_id);
		$this->db1->where('invoice_type','booking');
		 $query=$this->db1->get('invoice_master');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }

	}
	
	function get_invoice_index($bd_id){
		$this->db1->select('*');
		$this->db1->where('related_id',$bd_id);
		$this->db1->where('invoice_type','booking');
		 $query=$this->db1->get('invoice_master');
        if($query->num_rows() > 0)
        {
            return "exists";
        }
        else
        {
            return "not exists";
        }

	}
	
	function get_invoice_grp($bd_id){
		
		/*$this->db1->select('*');
		$this->db1->where('related_id',$bd_id);
		$this->db1->where('invoice_type','grp_booking');
		$query=$this->db1->get('invoice_master');*/
		
		$sql = "SELECT *
		FROM
			`invoice_master`
		WHERE
			`invoice_type` = 'grp_booking' AND `related_id` = ".$bd_id." AND `hotel_id` = ".$this->session->userdata('user_hotel');		
		
		$qry = $this->db1->query($sql);
		//echo $qry->row();
		//return $qry->row();
		//exit;
		if($qry->num_rows()>0){
			return $qry->row();
		}else{
			return false;
		}
	}

	function get_bookings($group_id){
		$this->db1->select('booking_status_id');
		$this->db1->where('group_id',$group_id);
		 $query=$this->db1->get('hotel_bookings');
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
	}

	function get_bookings_status($status_id){
		$this->db1->select('*');
		$this->db1->where('status_id',$status_id);
		 $query=$this->db1->get('booking_status_type');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
	}


function delete_extra_charges($ch_id){
	$this->db1->where_in('crg_id',$ch_id);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->delete(TABLE_PRE.'extra_charges_mapping');
        if($query){
            return true;
        }else{
            return false;
        }
}

function get_booking_by_b_id($b_id){
		$this->db1->select('*');
		$this->db1->where('broker_id',$b_id);
		 $query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else
        {
            return false;
        }
}
function get_monthly_booking($val){
		$this->db1->select('*');
		$this->db1->like('cust_end_date_actual',$val);

		 $query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else
        {
            return false;
        }
}


function get_monthly_pending($val){
	    $this->db1->select('*');

		$this->db1->like('cust_end_date_actual',$val);
	    $query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else
        {
            return false;
        }
}


			function get_group_booking_details($bookingID){
         $this->db1->select('*');
         $this->db1->from(TABLE_PRE.'group');
         $this->db1->where('id=',$bookingID);
         $query=$this->db1->get();
         if($query->num_rows() > 0)
         {
             return $query->row();
         }
         else
         {
             return false;
         }
     }

			 function delete_group_charge($booking_id,$service_id)
			 {
       $this->db1->where('id', $booking_id);
        $query = $this->db1->update(TABLE_PRE.'group', $service_id);
        if($query){
            return true;
        }else{
            return false;
        }


    }


	function delete_charge_extra($ch_id){
	$this->db1->where_in('crg_id',$ch_id);
        $query=$this->db1->delete(TABLE_PRE.'charges');
        if($query){
            return true;
        }else{
            return false;
        }
}


function all_warehouse(){
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
	    $query=$this->db1->get(TABLE_PRE.'warehouse');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
}

function all_stock_invent_cat(){
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
       // $this->db1->where('user_id',$this->session->userdata('user_id')); 
	    $query=$this->db1->get(TABLE_PRE.'stock_inventory_category');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }


}



         function get_group_food_plan_by_grp_id($grp_id)
		 {
			 $this->db1->select('mealPlan');
			 $this->db1->distinct();
			 $this->db1->where('group_booking_id',$grp_id);
			 $query=$this->db1->get(TABLE_PRE.'groups_bookings_details');
			 if($query->num_rows()>0)
			 {
				 return $query->row();
			 }
		 }


		 //fetch data from hotel_transaction
   function data_transactions($id){

        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'transactions');
        $this->db1->where('t_id',$id);
        $query=$this->db1->get();
        if($query){

            return $query->row();
        }
        else{

            return false;
        }

	}


	//update transactions
	function update_transactions($data){

              $id=$data['t_id'];
              $this->db1->where('t_id',$id);
              $query=$this->db1->update(TABLE_PRE.'transactions',$data);

              if($query){

                return true;
              }
   }

   function delete_hotel_transaction($id){
	    $this->db1->where('t_id',$id);
        $query=$this->db1->delete(TABLE_PRE.'transactions');
        if($query){
            return true;
        }else{
            return false;
        }
   }



   function get_extra_charge_by_id($extra_ch_id){
		$this->db1->where('crg_id',$extra_ch_id);
        $query=$this->db1->get('hotel_extra_charges_mapping');
         if($query){

            return $query->result();
        }
        else{

            return false;
        }
		}

		function all_pc(){
			$this->db1->select('*');
			$this->db1->where('status','1');
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$query=$this->db1->get(TABLE_PRE.'profit_center');
			if($query){

            return $query->result();
        }
        else{

            return false;
			}
		}

		function all_pc1(){
			$this->db1->select('*');
			$this->db1->where('profit_center_default','0');
			$this->db1->where('status','1');
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$query=$this->db1->get(TABLE_PRE.'profit_center');
			if($query){

            return $query->result();
        }
        else{

            return false;
			}
		}



		function all_pc_active(){
			$this->db1->select('*');
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$this->db1->where('status','1');
			$query=$this->db1->get(TABLE_PRE.'profit_center');
			if($query){

            return $query->result();
        }
        else{

            return false;
			}
		}


		function all_pc_inactive(){
			$this->db1->select('*');
			$this->db1->where('status','0');
			$query=$this->db1->get(TABLE_PRE.'profit_center');
			if($query){

            return $query->result();
        }
        else{

            return false;
			}
		}

		function all_booking_nature_visit(){
       $this->db1->select('*');
			$query=$this->db1->get(TABLE_PRE.'booking_nature_visit');
			if($query){

            return $query->result();
        }
        else{

            return false;
			}
		}


   /*

   $id=$data['t_id'];
              $this->db1->where('t_id',$id);
              $query=$this->db1->update(TABLE_PRE.'transactions',$data);


   */
   function add_pc($data){
	   $query=$this->db1->insert(TABLE_PRE.'profit_center',$data);

	$last_id=$this->db1->insert_id();
	//$this->db1->where('id',!$last_id);
	//$query=$this->db1->update(TABLE_PRE.'profit_center',$data);





		if($query){
		return $last_id;
		}

}

function update_default_pc($a){
	$b=$a;
	$b--;
	while($b) {
    $sql = "UPDATE ".TABLE_PRE."profit_center SET profit_center_default ='0' WHERE id =".$b;
    $this->db1->query($sql);

	/* $this->db1->where('profit_center_default','0');
              $query=$this->db1->update(TABLE_PRE.'booking_source',$data);
	*/
	$b--;
	}

}
function edit_update_default_pc($a){
	$b=$a;
	//$b--;
	//while($b) {
    $sql = "UPDATE ".TABLE_PRE."profit_center SET profit_center_default ='0' WHERE profit_center_default='1' and id!=".$a." and hotel_id=".$this->session->userdata('user_hotel');
    $this->db1->query($sql);

	/* $this->db1->where('profit_center_default','0');
              $query=$this->db1->update(TABLE_PRE.'booking_source',$data);
	*/
	//$b--;
	//}

}
	function delete_pc($id){
	$this->db1->where_in('id',$id);
        $query=$this->db1->delete(TABLE_PRE.'profit_center');
        if($query){
            return true;
        }else{
            return false;
        }
}

//fetch dat edit_profit_center
   function edit_profit_center($id){

        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'profit_center');
        $this->db1->where('id',$id);
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }

	//update profit_center

	function update_profit_center($data){

              $id=$data['id'];
              $this->db1->where('id',$id);
              $query=$this->db1->update(TABLE_PRE.'profit_center',$data);

              if($query){

                return $id;
              }

   }



   // check_profit_center_default
   function check_profit_center_default($a){

        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'profit_center');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('profit_center_default',$a);
        $query=$this->db1->get();
        if($query->num_rows()>0){

            return true;
        }
        else{

            return false;
        }
    }


	function get_group_booking_guest_details($bookingID){
		$this->db1->select('hotel_bookings.*,hotel_guest.*');
        $this->db1->from('hotel_bookings');
		$this->db1->where('hotel_bookings.booking_id=',$bookingID);
		$this->db1->join('hotel_guest', 'hotel_bookings.guest_id = hotel_guest.g_id');
        $query=$this->db1->get();
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows() ==1 )
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

	function get_sbID($gID){
       $this->db1->select('booking_id');
        $this->db1->from(TABLE_PRE.'bookings');
        $this->db1->where('group_id',$gID);
		$this->db1->limit(1);
        $query=$this->db1->get();
		if($query){
            return $query->row();
        } else {
            return false;
		}
	}

    public function deletePosSale($id,$hid)
    {
		$this->db1->where('hotel_id',$hid);
        $query = $this->db1->delete('hotel_pos', array('pos_id' => $id));
        if($query){
            return true;
        }else{
            return false;
        }
    }
    public function deletePosSaleItem($id,$hid)
    {
		$this->db1->where('hotel_id', $hid);
        $query = $this->db1->delete('hotel_pos_items', array('pos_id' => $id));
        if($query){
            return true;
        }else{
            return false;
        }
    }
	public function getPosID($id,$hid)
    {
		$this->db1->where('hotel_id',$hid);
        $q = $this->db1->get_where('hotel_pos', array('invoice_number' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

	function updatePosPaidAmount($val1,$val2,$hid){
		$sql = "UPDATE hotel_pos SET total_paid = total_paid + '".$val2."',total_paid_pos = total_paid_pos + '".$val2."',total_due= total_due - '".$val2."' WHERE invoice_number = '".$val1."' and hotel_id = '".$hid."'";
		$query=$this->db1->query($sql);
        if($query){
            return true;
        }else{
            return false;
        }
	}

	function get_fuel_usage_count($nm)
	{
		$this->db1->where('fuel_name',$nm);
		$qry=$this->db1->get(TABLE_PRE.'add_sub_fuel_log');
		return $qry->num_rows();
	}


	function delete_fuel($nm)
	{   $this->db1->where('fuel_name',$nm);
		$qry=$this->db1->delete(TABLE_PRE."master_fuel_stock");
		if($qry)
		{
			return true;
		}
	}

	function fetch_fuel($id){
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$qry=$this->db1->get(TABLE_PRE.'master_fuel_stock');
		if($qry){
			return $qry->row();
		}
	}

	function update_fuel($id,$data){
		$this->db1->where('id',$id);
		$query=$this->db1->update(TABLE_PRE.'master_fuel_stock',$data);
		if($query){
			return true;
		}
	}


	function get_sbID_detail($gID){
       $this->db1->select('booking_status_id');
        $this->db1->from(TABLE_PRE.'bookings');
        $this->db1->where('group_id',$gID);
		$this->db1->limit(1);
        $query=$this->db1->get();
		if($query){
            return $query->row();
        } else {
            return false;
		}
	}

function data_vendor($id){

        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'vendor');
        $this->db1->where('hotel_vendor_id',$id);
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
function data_vendor_details($id){
	$this->db1->select('*');
        $this->db1->from(TABLE_PRE.'vendor');
        $this->db1->where('hotel_vendor_id',$id);
        $query=$this->db1->get();
        if($query){

            return $query->row();
        }
        else{

            return false;
        }
}	
	

/* Update vendor */
	function update_vendor($hotel){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		//$this->db1->where('user_id',$hotel['user_id']);
        $this->db1->where('hotel_vendor_id',$hotel['hotel_vendor_id']);
        $query=$this->db1->update(TABLE_PRE.'vendor',$hotel);
		//$query=$this->db1->get();
        if($query){
            return true;
        }else{
            return false;
        }
    }

function edit_shift($id){

        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'shift');
        $this->db1->where('shift_id',$id);
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }

/* Update shift */
	function update_shift($hotel){

        $this->db1->where('shift_id',$hotel['shift_id']);
        $query=$this->db1->update(TABLE_PRE.'shift',$hotel);
		//$query=$this->db1->get();
        if($query){
            return true;
        }else{
            return false;
        }
    }

function delete_shift($nm)
	{   $this->db1->where('shift_id',$nm);
		$qry=$this->db1->delete(TABLE_PRE."shift");
		if($qry)
		{
			return true;
		}
	}
	function checkout_submission_on_credit($data)
	{
	$this->db1->insert('checked_out_on_credit',$data);
	$l_id=$this->db1->insert_id();
	return $l_id;
	}

function ceck_shift_time($start,$end){

	//echo 'hello'.$start;exit;
        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'shift');
        $this->db1->where('max(shift_opening_time)<'.$start);

        $query=$this->db1->get();
		
        if($query->num_rows()>0){//->num_rows()>0
			echo "value is  present";
            return 1;
        }
        else{
			echo "value is  not present";
            return 0;
        }



}

function get_checkout_on_credit($booking_id,$booking_type)
     {
		$this->db1->select('*');
         $this->db1->where('booking_id',$booking_id);
		 $this->db1->where('booking_type',$booking_type);

         $query = $this->db1->get('checked_out_on_credit');

         if($query->num_rows()>0){
             return $query->result();
         }
         else{
             return false;
         }
     }

      function invoice_sttings(){
			$this->db1->select('*');
			$query=$this->db1->get(TABLE_PRE.'settings');
			if($query){

            return $query->result();
        }
        else{

            return false;
			}
}


		function get_invoice_settings($id){
			$this->db1->where('hotel_id',$id);
			$query = $this->db1->get(TABLE_PRE.'settings');
				 if($query){
					 return $query->result();
				 }
				 else{
					 return false;
				 }
			}

		function update_invoice_settings($data){
			//$id=$data['id'];
			$this->db1->where('hotel_id',$data['hotel_id']);
			$query = $this->db1->update(TABLE_PRE.'settings',$data);
			if($query){
				return true;
			}
			else{
				return false;
			}
}
function ft_invoice_settings($data){
			//$id=$data['id'];
			$this->db1->where('hotel_id',$data['hotel_id']);
			$query = $this->db1->update(TABLE_PRE.'settings',$data);
			if($query){
				return true;
			}
			else{
				return false;
			}
}






	 function update_credit_security_details($booking_id,$booking_type,$data)
	 {

		$this->db1->where('booking_id',$booking_id);
		$this->db1->where('booking_type',$booking_type);
		$query = $this->db1->update('checked_out_on_credit',$data);

		if($query){
             return true;
         }
         else{
             return false;
         }
	 }

	 function profit_center_status(){

		 $this->db1->select('status');
		  $this->db1->where('id',$id);
		  $query = $this->db1->get(TABLE_PRE.'profit_center',$data);



		  $this->db1->where('id',$id);
         $query = $this->db1->update(TABLE_PRE.'profit_center',$data);
         if($query){
             return true;
         }
         else{
             return false;
         }
	 }

	 function get_group_pending_payments()
	 {	$this->db1->where('booking_type','gb');
		 $query=$this->db1->get('checked_out_on_credit');
		 if($query->num_rows()>0)
		 {
			 return $query->result();
		 }
		 else
		 {
			 return false;
		 }
	 }



	 function update_bookings_guest_id($id,$booikng_id){
		 $data=array('guest_id'=>$id);
		 $this->db1->where('booking_id',$booikng_id);
		 $query=$this->db1->update(TABLE_PRE.'bookings',$data);
		 if($query){
			 return true;
		 }
	 }


	function get_all_sb_by_group_id($grp_id)
	{
		$this->db1->where('group_id',$grp_id);
		$qry=$this->db1->get(TABLE_PRE.'bookings');
		if($qry->num_rows()>0)
		{
			return $qry->result();
		}
		else
		{
			return false;
		}
	}

	function get_group_booking_detail_by_grpid_and_room_id($grp_id,$room_id)
	{
		$this->db1->where('roomID',$room_id);
		$this->db1->where('group_booking_id',$grp_id);
		$query=$this->db1->get(TABLE_PRE."groups_bookings_details");
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}


	 function get_total_from_group_booking_total($g_id)
	 {
		$sql="SELECT sum(pdr) as pdr, sum(room_st) as room_st , sum(room_sc) as room_sc , sum(room_vat) as room_vat , sum(food_vat) as food_vat , sum(service) as service FROM `hotel_groups_bookings_details` WHERE `group_booking_id`=".$g_id;
		$qry=$this->db1->query($sql);
		if($qry->num_rows() > 0)
		{
			return $qry->row();
		}
		else
		{
			return false;
		}
	 }

	 function all_cirtificate(){
		$this->db1->select('*');
        $this->db1->from(TABLE_PRE.'cirtificate_type');
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }

	}


function search_item($word){
		if(isset($word) && $word)
			{
			$sql="select * from hotel_stock_inventory where a_name LIKE '%".$word."%'";
			$result=$this->db1->query($sql);
				if($result->num_rows()>0)
				  {
						return $result->result();
				  }
				else
				  {
						return false;
				  }
		    }
		else
			{
			return false;
			}
    }
	
	
	function get_booking_report(){
		
		/*$sql = "select booking.*, hot.hotel_name as hotel_name, CONCAT(adm.`admin_first_name`,' ',adm.`admin_last_name`) as 'admin_name',
		drl.`rule_name` as rule_name
		from((SELECT disc.*, 
			(sin.room_rent_total_amount+room_rent_tax_amount) as totalAmt,
			gst.g_name
			FROM `hotel_discount_mapping` as disc
			LEFT JOIN `hotel_bookings` as sin
			ON disc.related_id = sin.booking_id
			LEFT JOIN `hotel_guest` as gst
			ON sin.guest_id = gst.g_id
			WHERE disc.related_type = 'single_booking'
			and disc.hotel_id =".$this->session->userdata('user_hotel')."
			GROUP BY disc.id
			ORDER BY disc.`add_date` DESC)
			UNION
			(SELECT disc.*, 
			SUM(sin.room_rent_sum_total) as totalAmt,
			gst.g_name
			FROM `hotel_discount_mapping` as disc
			LEFT JOIN `hotel_bookings` as sin
			ON disc.related_id = sin.group_id
			LEFT JOIN `hotel_group` as grp
			ON disc.related_id = grp.id
			LEFT JOIN `hotel_guest` as gst
			ON grp.guestID = gst.g_id
			WHERE disc.related_type = 'group_booking'
			GROUP BY disc.id
			ORDER BY disc.`add_date` DESC)) as booking 
		LEFT JOIN `hotel_hotel_details` as hot
		ON booking.hotel_id = hot.hotel_id
		LEFT JOIN `hotel_admin_details` as adm
		ON booking.user_id = adm.admin_id
		LEFT JOIN `hotel_discount_ruls` as drl
		ON booking.discount_plan_id = drl.id
		ORDER BY booking.add_date DESC";*/
		$sql="select booking.*, hot.hotel_name as hotel_name, CONCAT(adm.`admin_first_name`,' ',adm.`admin_last_name`) as 'admin_name',
		drl.`rule_name` as rule_name
		from((SELECT disc.*, 
			(sin.room_rent_total_amount+room_rent_tax_amount) as totalAmt,
			gst.g_name
			FROM `hotel_discount_mapping` as disc
			LEFT JOIN `hotel_bookings` as sin
			ON disc.related_id = sin.booking_id
			LEFT JOIN `hotel_guest` as gst
			ON sin.guest_id = gst.g_id
			WHERE disc.related_type = 'single_booking'
			and disc.hotel_id ='".$this->session->userdata('user_hotel')."'
			GROUP BY disc.id
			ORDER BY disc.`add_date` DESC)
			UNION
			(SELECT disc.*, 
			SUM(sin.room_rent_sum_total) as totalAmt,
			gst.g_name
			FROM `hotel_discount_mapping` as disc
			LEFT JOIN `hotel_bookings` as sin
			ON disc.related_id = sin.group_id
			LEFT JOIN `hotel_group` as grp
			ON disc.related_id = grp.id
			LEFT JOIN `hotel_guest` as gst
			ON grp.guestID = gst.g_id
			WHERE disc.related_type = 'group_booking'
			GROUP BY disc.id
			ORDER BY disc.`add_date` DESC)) as booking 
		LEFT JOIN `hotel_hotel_details` as hot
		ON booking.hotel_id = hot.hotel_id
		LEFT JOIN `hotel_admin_details` as adm
		ON booking.user_id = adm.admin_id
		LEFT JOIN `hotel_discount_ruls` as drl
		ON booking.discount_plan_id = drl.id
        where  booking.hotel_id ='".$this->session->userdata('user_hotel')."'
		ORDER BY booking.add_date DESC";

$result=$this->db1->query($sql);
if($result->num_rows()>0){
	
	return $result->result();
}
else
{
	return false;
}	

		
}
function get_booking_report_by_condition($start,$end){
	
	$sql = "select booking.*, hot.hotel_name as hotel_name, CONCAT(adm.`admin_first_name`,' ',adm.`admin_last_name`) as 'admin_name',
		drl.`rule_name` as rule_name
		from((SELECT disc.*, 
			(sin.room_rent_total_amount+room_rent_tax_amount) as totalAmt,
			gst.g_name
			FROM `hotel_discount_mapping` as disc
			LEFT JOIN `hotel_bookings` as sin
			ON disc.related_id = sin.booking_id
			LEFT JOIN `hotel_guest` as gst
			ON sin.guest_id = gst.g_id
			WHERE disc.related_type = 'single_booking' and
			 booking.hotel_id =".$this->session->userdata('user_hotel')."			
			GROUP BY disc.id
			ORDER BY disc.`add_date` DESC)
			UNION
			(SELECT disc.*, 
			SUM(sin.room_rent_sum_total) as totalAmt,
			gst.g_name
			FROM `hotel_discount_mapping` as disc
			LEFT JOIN `hotel_bookings` as sin
			ON disc.related_id = sin.group_id
			LEFT JOIN `hotel_group` as grp
			ON disc.related_id = grp.id
			LEFT JOIN `hotel_guest` as gst
			ON grp.guestID = gst.g_id
			WHERE disc.related_type = 'group_booking'
			GROUP BY disc.id
			ORDER BY disc.`add_date` DESC)) as booking 
		LEFT JOIN `hotel_hotel_details` as hot
		ON booking.hotel_id = hot.hotel_id
		LEFT JOIN `hotel_admin_details` as adm
		ON booking.user_id = adm.admin_id
		LEFT JOIN `hotel_discount_ruls` as drl
		ON booking.discount_plan_id = drl.id
		where  booking.hotel_id =".$this->session->userdata('user_hotel')."
	and  (date(booking.add_date) BETWEEN '$start' and '$end')	
	ORDER BY booking.add_date DESC";

$result=$this->db1->query($sql);
if($result->num_rows()>0){
	
	return $result->result();
}
else
{
	return false;
}		
	
}

function tax_rule_ajax_specific($t_cate,$start,$end,$start_price,$end_price){
	
	$sql = "SELECT * 
FROM `hotel_tax_rule` 
WHERE `r_cat` = '$t_cate' AND
('$end_price' >= `r_start_price`) AND (`r_end_price` >= '$start_price') AND
('$end' >= `r_start_date`) AND (`r_end_date` >= '$start')";
	$res = $this->db1->query($sql);
	if($res->num_rows() > 0)
	{
	   return "YES";
	}
	else
	{
	   return "nope";
	}
}

       function get_kit_details()
	   {
		   
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		   $res=$this->db1->get(TABLE_PRE.'define_kit');
		   if($res->num_rows() > 0)
		   {
			   return $res->result();
		   }
		   else
		   {
			   return false;
		   }

	   }

		function get_kit_mapping_items($id)
		{
			$this->db1->where('kit_id',$id);
			$res=$this->db1->get(TABLE_PRE.'kit_mapping');
			if($res->num_rows() > 0)
		   {
			   return $res->result();
		   }
		   else
		   {
			   return false;
		   }

		}

		function get_kit_items_details($id)
		{
			$this->db1->where('hotel_stock_inventory_id',$id);
			$res=$this->db1->get(TABLE_PRE.'stock_inventory');
			if($res->num_rows() > 0)
		   {
			   return $res->row();
		   }
		   else
		   {
			   return false;
		   }

		}


		function delete_kit_items($id)
		{
			$this->db1->where('kit_id',$id);
			$res=$this->db1->delete(TABLE_PRE.'kit_mapping');
			if($res)
		   {
			   return true;
		   }
		   else
		   {
			   return false;
		   }

		}

		function delete_individual_mapping_item($id,$kit)
		{
			$this->db1->where('item_name',$id);
			$this->db1->where('kit_id',$kit);
			$res=$this->db1->delete(TABLE_PRE.'kit_mapping');
			return $this->db1->affected_rows();
		}

		function delete_kit($kit_id)
		{
			$this->db1->where('hotel_define_kit_id',$kit_id);
			$res=$this->db1->delete(TABLE_PRE.'define_kit');
			if($res)
		   {
			   return true;
		   }
		   else
		   {
			   return false;
		   }
		}



		function get_kit_details_by_id($id)
	   {
	       $this->db1->where('hotel_define_kit_id',$id);
		   $res=$this->db1->get(TABLE_PRE.'define_kit');
		   if($res->num_rows() > 0)
		   {
			   return $res->row();
		   }
		   else
		   {
			   return false;
		   }

	   }


	   function edit_kit($data1,$kit_id){
		   $this->db1->where('hotel_define_kit_id',$kit_id);
		 $query=$this->db1->update(TABLE_PRE.'define_kit',$data1);
        if(isset($query)){
            return true;
        }else{
            return false;
        }


	 }

	 function get_distribution_warehouse()
	 {
		$this->db1->where('kit','1');
		$query = $this->db1->get(TABLE_PRE."warehouse");

		if($query->num_rows()>0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	 }

	 function get_unit_kit_mapping($unit)
	 {
		 $this->db1->where('unit_type_id',$unit);
		 $qry = $this->db1->get(TABLE_PRE.'unit_kit_mapping');
		 if($qry->num_rows() > 0)
		 {
			 return $qry->row();
		 }
		 else
		 {
			 return false;
		 }

	 }

	 function kit_allotment($item_allot)
	 {
		 $this->db1->insert(TABLE_PRE."kit_allotment_log",$item_allot);
		 return $this->db1->insert_id();
	 }
	 function check_kit_distribution_status($booking_id)
	 {
		$this->db1->where('allot_booking_id',$booking_id);
		$result = $this->db1->get(TABLE_PRE."kit_allotment_log");
		if($result->num_rows() > 0)
		{
			return $result->row();
		}
		else
		{
			return false;
		}
	 }

	function get_payment_mode_list()
	{
		$query = $this->db1->get(TABLE_PRE."payment_mode");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	function get_payment_mode_by_id($id)
	{
		$this->db1->where("p_mode_id", $id);
		$query = $this->db1->get(TABLE_PRE."payment_mode");
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	function posFinalPayment($bookingID){
		$sql_gp = "UPDATE ".TABLE_PRE."pos SET is_pay = '1' , total_paid = total_due where booking_id='".$bookingID."'";
        //echo $sql_gp;
        //exit;
		$this->db1->query($sql_gp);
	}


	function get_all_tax_type()
	{	
		$this->db1->where("hotel_id", $this->session->userdata('user_hotel'));
		$qry = $this->db1->get(TABLE_PRE.'tax_type');
		if($qry->num_rows()>0)
		{
			return $qry->result();
		}
		else
		{
			return false;
		}
	}
	
	
	function get_distinct_tax_type()
	{
		$this->db1->select('*');
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
         $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$qry = $this->db1->get(TABLE_PRE.'tax_type');
		//$sql="select * from hotel_tax_type";
		//$qry = $this->db1->query($sql);
		if($qry->num_rows()>0)
		{
			return $qry->result();
		}
		else
		{
			return false;
		}
	}
	

	function get_tax_type_by_id($id)
	{
		 $this->db1->where('tax_id',$id);
		$qry = $this->db1->get(TABLE_PRE.'tax_type');
		if($qry->num_rows()>0)
		{
			return $qry->row();
		}
		else
		{
			return false;
		}
	}

	function add_tax_rule($tax_data)
	{
		if($this->db1->insert(TABLE_PRE."tax_rule",$tax_data))
		{
			return $this->db1->insert_id();
		}
		else
		{
			return false;
		}

	}

	function add_tax_rule_component($component_array)
	{
		if($this->db1->insert(TABLE_PRE."tax_rule_components",$component_array))
		{
			return $this->db1->insert_id();
		}
		else
		{
			return false;
		}

	}

	function check_tax_slab($tax_slab)
	{
		//print_r($tax_slab);exit();
		$this->db1->where('r_cat' , $tax_slab['tax_cat']);
		$this->db1->where('r_end_price >= ' , $tax_slab['r_start_price']);
		$this->db1->where('r_end_date >= ' , $tax_slab['r_start_date']);

		$query = $this->db1->get(TABLE_PRE.'tax_rule');
		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
		    return false;
		}
}

function add_extra_charges($charges){
	
	$query=$this->db1->insert(TABLE_PRE.'extra_charges',$charges);
	
	if($query) {
		return true;
	} else {
		return false;
	}
}

function get_charge_name($name){
	$this->db1->where("charge_name",$name);
		$query = $this->db1->get(TABLE_PRE."extra_charges");
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		else
		{
			return false;
		}
}

function get_charges(){
	$this->db1->select('*');
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	$query=$this->db1->get(TABLE_PRE.'extra_charges');
	if($query){
		return $query->result();
	}
}

//new method created for extra charges @ 11-11-16...

function get_extra_charges(){
	
	$this->db1->select('*');
	$query = $this->db1->get(TABLE_PRE.'extra_charges_tag');
	if($query){
		return $query->result();
	}
	
	
}

// end....


function get_charge_by_id($id){
	$this->db1->where('hotel_extra_charges_id',$id);
	$query=$this->db1->get(TABLE_PRE.'extra_charges');
	if($query->num_rows()>0){
		return $query->row();
	}

}
function update_charge($id,$data){
	$this->db1->where('hotel_extra_charges_id',$id);
	$query=$this->db1->update(TABLE_PRE.'extra_charges',$data);
	if($query){
		return true;
	}else{
		return false;
	}
}

function update_tax_rule($id,$data){
	
	$this->db1->where('r_id',$id);
	$query=$this->db1->update('hotel_tax_rule',$data);
	if($query){
		return true;
	}else{
		return false;
	}
	
	
}

function update_tax_rule_component($id,$data){
	
	$this->db1->where('id',$id);
	$query=$this->db1->update('hotel_tax_rule_components',$data);
	if($query){
		return true;
		
	}else{
		
		return false;
	}
	
}


function update_tax($id,$data){
	
	$this->db1->where('tax_id',$id);
	$query=$this->db1->update('hotel_tax_type',$data);
	if($query){
		return true;
	}else{
		return false;
	}
	
	
}

function update_tax_categorys($id,$data){
	
	$this->db1->where('tc_id',$id);
	$query=$this->db1->update('hotel_tax_category',$data);
	if($query){
		return true;
	}else{
		return false;
	}
	
	
}


function delete_extra_charge($id){
	$this->db1->where('hotel_extra_charges_id',$id);
	$query=$this->db1->delete(TABLE_PRE.'extra_charges');
	if($query){
		return true;
	}else{
		return false;
	}
}


  function get_all_tax_rule()
		  {
			$this->db1->select('*');
			//$this->db1->where('user_id',$this->session->userdata('user_id'));
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$query = $this->db1->get(TABLE_PRE.'tax_rule');
			if($query->num_rows()>0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}

		}
		
	 function get_specific_tax_rule($tc_name)
		  {
			  $this->db1->select('*');
			  //$this->db1->where('user_id',$this->session->userdata('user_id'));
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			  $this->db1->where('r_cat',$tc_name);
			$query = $this->db1->get('hotel_tax_rule');
			if($query->num_rows()>0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}

		}
			
	function cattype_namelist($tax_cate){
		
		$this->select('*');
		$this->db1->where('tc',$id);
		$query=$this->db1->get('hotel_tax_category');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}
		
	}		
			
	function get_all_tax_category(){
		
			$this->db1->select('*');
			//$this->db1->where('user_id',$this->session->userdata('user_id'));
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$query = $this->db1->get(TABLE_PRE.'tax_category');
			if($query->num_rows()>0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}

		
		
	}	
	function get_distinct_tax_category(){
		
			$this->db1->select('*');
			//$this->db1->distinct();
		//	$this->db1->where('user_id',$this->session->userdata('user_id'));
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$query = $this->db1->get(TABLE_PRE.'tax_category');
			if($query->num_rows()>0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}

		
		
	}
	
	function get_specific_tax_category($tc_id){
		
			$this->db1->select('*');
			//$this->db1->distinct();
			$this->db1->where('tc_id',$tc_id);
			$query = $this->db1->get(TABLE_PRE.'tax_category');
			if($query->num_rows()>0)
			{
				return $query->row();
			}
			else
			{
				return false;
			}

		
		
	}	
	
	
	
	function add_tax_category($tax_cate){
		
	$query=$this->db1->insert(TABLE_PRE.'tax_category',$tax_cate);
	if($query){
		return true;
	}else{
		return false;
	}

	}
			
			
			
	function get_tax_rule($id){
		$this->db1->where('r_id',$id);
		$query=$this->db1->get(TABLE_PRE.'tax_rule');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}
	}

	function delete_tax_rule($id){
		$this->db1->where('r_id',$id);
		$query=$this->db1->delete(TABLE_PRE.'tax_rule');
		$this->db1->where('p_r_id',$id);
		$query1=$this->db1->delete(TABLE_PRE.'tax_rule_components`');
		if($query && $query1){
			return true;
		}else{
			return false;
		}
	}
	function delete_tax_rule_only($id){
		$this->db1->where('id',$id);
		$query1=$this->db1->delete(TABLE_PRE.'tax_rule_components`');
		if($query1){
			return true;
		}else{
			return false;
		}
	}


	function get_all_tax_rule_component_by_rule_id($rule_id)
	{
		$this->db1->where('p_r_id',$rule_id);
		$query = $this->db1->get(TABLE_PRE.'tax_rule_components');
			if($query->num_rows()>0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
	}

	function tax_slab($date,$type,$price)
	{
			$this->db1->select('r_id');
			$this->db1->where('r_cat', $type);

			$this->db1->where('r_start_date <=', $date);
			$this->db1->where('r_end_date >=', $date);

			$this->db1->where('r_start_price <=', $price);
			$this->db1->where('r_end_price >=', $price);

			$query = $this->db1->get(TABLE_PRE."tax_rule");
			if($query->num_rows()>0)
			{
				$query_tax = $query->row();
				$this->db1->where('p_r_id',$query_tax->r_id);
				$t_res = $this->db1->get(TABLE_PRE.'tax_rule_components');
				if($t_res->num_rows()>0)
				{
					return $t_res->result();
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
	}

    function all_admin_roles(){
		$this->db1->order_by('user_permission_id','desc');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'user_permission');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }


 	function inlineEditRole($col,$editval,$id){
		$data = array(
            $col => $editval
        );
		$this->db1->where('user_permission_id',$id);
	    $query=$this->db1->update(TABLE_PRE.'user_permission',$data);
        if($query){
            return true;
        }else{
            return false;
        }
	}

	function get_no_tax_booking(){
		$this->db1->select('*');
		$this->db1->where('service_tax',0);
		$this->db1->where('luxury_tax',0);
		$query=$this->db1->get(TABLE_PRE.'bookings');
	//	echo $this->db1->last_query();exit;
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}

	function get_upt($id){
		$this->db1->select('*');
		$this->db1->where('user_permission_id',$id);
		$query=$this->db1->get(TABLE_PRE.'user_permission');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
	}

	/*update User Permission */
	function update_user_permission($data,$id){
		$this->db1->where('user_permission_id',$id);
	    $query=$this->db1->update(TABLE_PRE.'user_permission',$data);
        if($query){
            return true;
        }else{
            return false;
        }
	}

 	function inlineEditPermission($col,$editval,$id){
		$upt=$this->dashboard_model->get_upt($id);
		$getVal = explode(',',$upt->user_permission_type);
        if($editval == '1')	{
			array_push($getVal, $col);
		} else {
		    if (($key = array_search($col, $getVal)) !== false) {
				unset($getVal[$key]);
			}
		}
        $getVal = implode(',',$getVal);
		$data = array(
            'user_permission_type' => $getVal
        );
		$this->db1->where('user_permission_id',$id);
	    $query=$this->db1->update(TABLE_PRE.'user_permission',$data);
        if($query){
            return true;
        }else{
            return false;
        }
	}

function check_booking($start_dt,$end_dt,$chkin_time){
	//$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') >=",$date_1);

	$this->db1->where('booking_status_id',5);
	$this->db1->where("date_format(cust_from_date,'%d-%m-%Y') >=",$start_dt);
	$this->db1->where("date_format(cust_end_date,'%d-%m-%Y') <=",$end_dt);
	//$this->db1->where('checkin_time >=',$chkin_time);
	//$this->db1->where('checkout_time >=',$chkin_time);
	$qry=$this->db1->get(TABLE_PRE.'bookings');
	if($qry){
		return $qry->num_rows();
	}
}


     function get_room_number_exact_unit($room_id,$unit_id){

         $this->db1->where('room_id=',$room_id);
		 $this->db1->where('unit_id=',$unit_id);
         $query=$this->db1->get(TABLE_PRE.'room');
         if($query->num_rows()>0){
            return $query->row();
         }else{
            return false;
         }
     }

   function search_vendor($keyword){
		if(isset($keyword) && $keyword){
			$sql="select * from hotel_vendor where hotel_vendor_name LIKE '%".$keyword."%' GROUP BY hotel_vendor_name";
			$result=$this->db1->query($sql);
				if($result->num_rows()>0)
				  {
						return $result->result();
				  }
				else
				  {
						return false;
				  }
		}else
			{
			return false;
			}

	}


	function get_vendor_name($v_id){

		 $this->db1->where('hotel_vendor_id=',$v_id);
         $query=$this->db1->get(TABLE_PRE.'vendor');
         if($query->num_rows()>0){
            return $query->row();
         }else{
            return false;
         }
	}

    function duplicate_product($p_name){
		 $this->db1->where('product=',$p_name);
         $query=$this->db1->get(TABLE_PRE.'line_item_details');
         if($query->num_rows()>0){
            return $query->num_rows();
         }else{
            return false;
         }
	}

    function get_item_details($id){
		$this->db1->where('payment_id=',$id);
         $query=$this->db1->get(TABLE_PRE.'line_item_details');
         if($query->num_rows()>0){
            return $query->result();
         }else{
            return false;
         }
		
	}

  function bill_to_com($id){
	     $this->db1->select('bill_to_com');
         $this->db1->where('booking_id=',$id);
         $query=$this->db1->get(TABLE_PRE.'bookings');
        
		 
        if($query->num_rows()>0){

           $query2=$query->row();
		  
        }
        
		  if($query2->bill_to_com=='1'){
		  $data = array(
            'bill_to_com' =>'0',

        );
		  }
		  else{
			   $data = array(
            'bill_to_com' =>'1',

        );
			  
		  }
			
		 $this->db1->where('booking_id',$id);
		 $query3=$this->db1->update(TABLE_PRE.'bookings',$data);
		
		
		 if($query3){

                return $query2->bill_to_com;
              }
			  else{
				  return false;
			  }
		 
		 
  }
   function bill_to_com_grp($id){
	     $this->db1->select('bill_to_com');
         $this->db1->where('id=',$id);
         $query=$this->db1->get(TABLE_PRE.'group');
        
		 
        if($query->num_rows()>0){

           $query2=$query->row();
		  
        }
        
		  if($query2->bill_to_com=='1'){
		  $data = array(
            'bill_to_com' =>'0',

        );
		  }
		  else{
			   $data = array(
            'bill_to_com' =>'1',

        );
			  
		  }
			
		 $this->db1->where('id',$id);
		 $query3=$this->db1->update(TABLE_PRE.'group',$data);
		
		
		 if($query3){

                return $query2->bill_to_com;
              }
			  else{
				  return false;
			  }
		 
		 
  }
  
  function get_pay_details($id){
	  	$this->db1->where('hotel_payment_id=',$id);
         $query=$this->db1->get(TABLE_PRE.'payments_details');
         if($query->num_rows()>0){
            return $query->row();
         }else{
            return false;
         }
  }
  
  function delete_itm($id,$i_id){
	 $this->db1->where('payment_id',$id);
	 $this->db1->where('line_items_id',$i_id);
     $query=$this->db1->delete(TABLE_PRE.'line_item_details');
        if($query){
            return true;
        }else{
            return false;
        }
  }
 
function update_itmAmt($id, $due_amt){
	$data=array(
	'payments_due'=>$due_amt
	);
	$this->db1->where('hotel_payment_id=',$id);
         $query=$this->db1->update(TABLE_PRE.'payments',$data);
         if($query){
            return true;
         }else{
            return false;
         }
}
 
     public function getGuest(){
        $myQuery = "select g_id,g_name from hotel_guest GROUP BY hotel_guest.g_name ";		
         $query = $this->db1->query($myQuery);
         if($query->num_rows()>0){
            return $query->result();
         }else{
            return false;
         }
     } 
  
  
   function update_g_info($g_id,$data,$data1){
	  $this->db1->where('g_id',$g_id);
	  $query=$this->db1->update('hotel_guest',$data);
	  $this->db1->where('guest_id',$g_id);
	$query1=$this->db1->update('hotel_bookings',$data1);
	  if($query ){
		  return true;
	  }
  }
  
  // Function to return tax rules for a given price for a day
  function getTaxElements($price,$date,$type,$args,$state=NULL){
	  
	  //$stateH = $this->unit_class_model->get_state_hotel();
	  
	  $stateH = 'West Bengal';
	  $stateH = strtoupper($stateH);
	  
	  //echo '-'.$stateH.'- -'.$state.'- '.strcmp((string)$state,(string)$stateH).($state === $stateH);
	  
	  $same_state = "";
	  
	  if($state != NULL && isset($state)){
		  $state = strtoupper($state);
		  if($stateH && isset($stateH)){
			if(trim($state) == trim($stateH)){
				//echo 'SAME';
				$same_state = " AND same_state = '1'";		
			}
		    else {
			  $same_state = " AND same_state = '0'";
			}
		 }
	  }
		
		 // echo $same_state;
		
	  $sql =    "SELECT * from taxplan_view
				WHERE `hotel_id` =".$this->session->userdata('user_hotel')." AND
				`tc_type` = '$type' AND
				'$price' BETWEEN `r_start_price` AND `r_end_price` AND
				'$date' BETWEEN `r_start_date` AND `r_end_date` 
				".$same_state."";
				
			//	echo $sql;
			//	exit;

	$result=$this->db1->query($sql);
	
	if($result->num_rows()>0){		
		return $result->result();
		//return $tax;
	}
	else
	{
		return 0;
	}		
  } // End function getTaxElements _sb
  
  function current_allocation($id){
	  
	  
		$this->db1->where('staff_id',$id);
		$this->db1->where('status','pending');
         $query=$this->db1->get(TABLE_PRE.'housekeeping_log_detail');
         if($query->num_rows()>0){
            return $query->num_rows();
         }else{
            return false;
         }
	  
  }
  
  
  function get_bookingLineItem($ID){
		$this->db1->select('*');
        $this->db1->where('booking_id',$ID);
        $query=$this->db1->get('booking_charge_line_item_view');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }
  function get_bk_actual_id($bookings_id){
	  $this->db1->select('booking_id_actual');
        $this->db1->where('booking_id',$bookings_id);
        $query=$this->db1->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
  }
  
  
  function get_all_bookingLineItem(){
		/*
		order by bcli_id desc
		$this->db1->select('*');
		$this->db1->limit(10);
		$this->db1->order_by('booking_id','desc');
        $query=$this->db1->get('booking_charge_line_item');*/
		$sql="select * from booking_charge_line_item  where hotel_id=".$this->session->userdata('user_hotel')." limit 100";
		$query = $this->db1->query($sql);
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
  
   function all_transactions_limit_10()
    {
        
		$d=date('Y-m-d');
		$hotel=$this->session->userdata('user_hotel');
		$user=$this->session->userdata('user_id');
        //$sql="SELECT * FROM `hotel_transactions` WHERE `t_date` > $d - interval '10' day";
	  $sql="SELECT * FROM `hotel_transactions` WHERE  t_status = 'Done' and hotel_id='$hotel' and user_id='$user' order by t_id desc limit 10 ";
		$query = $this->db1->query($sql);
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function all_transactions_limit_10_group_by_date()
    {
        
		$d=date('Y/m/d');
		$hotel=$this->session->userdata('user_hotel');
		$user=$this->session->userdata('user_id');
        //$sql="SELECT * FROM `hotel_transactions` WHERE `t_date` > $d - interval '10' day";
		$sql="SELECT t_id,DATE(t_date) as t_date,t_status,hotel_id,transaction_type,SUM(t_amount) as t_amount
		FROM `hotel_transactions` 
		WHERE DATE(`t_date`) > ( DATE(NOW()) - INTERVAL 10 DAY) and t_status = 'Done' and hotel_id='$hotel' and user_id='$user' 
		group by DATE(t_date) 
		ORDER BY DATE(t_date) DESC 
		limit 10 ";
		$query = $this->db1->query($sql);
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	
	function total_transaction()
	 {
	   $hotel=$this->session->userdata('user_hotel');
	   $user=$this->session->userdata('user_id');
	   $query_date=date('Y-m-d');


	$sd= date('Y/m/01', strtotime($query_date));
	$nd=date('Y/m/t', strtotime($query_date));
   	  //$sql="SELECT sum(t_amount) as total FROM `hotel_transactions` where user_id=23 group by MONTH($d)";
	  $sql="SELECT sum(t_amount) as total FROM `hotel_transactions` where user_id='". $user."' and (t_date between '".$sd."' and '".$nd."')";
		$query = $this->db1->query($sql);
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }
	
	
  function get_bookingLineItem_group_id($ID){
		$this->db1->select('*');
        $this->db1->where('group_id',$ID);
        $query=$this->db1->get('booking_charge_line_item');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function get_line_items_details($ID){
		$this->db1->select('*');
        $this->db1->where('booking_id',$ID);
        $query=$this->db1->get('booking_charge_line_item');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	function insert_shadow_line_items($data){
		$query=$this->db1->insert('booking_charge_line_item_shadow',$data);
		if($query){
			return true;
		}
	}
	function delete_line_items($id){
		$this->db1->where('booking_id',$id);
		$query=$this->db1->delete('booking_charge_line_item');
		if($query){
			return true;
		}
	} 
	
	function update_booking_info($id,$data){
		 $this->db1->where('booking_id',$id);
	$query=$this->db1->update('hotel_bookings',$data);
	  if($query){
		  return true;
	  }
	}
  
   function get_meal_plan_by_id($id){
        $this->db1->where('hotel_meal_plan_cat_id',$id);
        $query=$this->db1->get('hotel_meal_plan_cat');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }
	
	function single_booking_count()
    {
       $hotel=$this->session->userdata('user_hotel');
	   $user=$this->session->userdata('user_id');
	   $query_date=date('Y-m-d');


	$sd= date('Y-m-01', strtotime($query_date));
	$nd=date('Y-m-t', strtotime($query_date));
 $sql1="SELECT * FROM `hotel_bookings` where group_id='0' and user_id='". $user."' and (booking_taking_date between '".$sd."' and '".$nd."')";
		$query1 = $this->db1->query($sql1);
        if($query1->num_rows()>0){
            return $query1->num_rows();
        }else{
            return false;
        }
    }
	
	function group_booking_count()
    {
       
	   $hotel=$this->session->userdata('user_hotel');
	   $user=$this->session->userdata('user_id');
	   $query_date=date('Y-m-d');


	$sd= date('Y-m-01', strtotime($query_date));
	$nd=date('Y-m-t', strtotime($query_date));
 $sql1="SELECT * FROM `hotel_bookings` where group_id>'0' and user_id='". $user."' and (booking_taking_date between '".$sd."' and '".$nd."')";
		$query1 = $this->db1->query($sql1);
        if($query1->num_rows()>0){
            return $query1->num_rows();
        }else{
            return false;
        }
    }
	
	function get_last_10_booking(){ $user=$this->session->userdata('user_id');
		 $sql="(SELECT booking_taking_date as addON,booking_id as bid, cust_name as customerName FROM `hotel_bookings` where user_id=$user ORDER BY `booking_id` desc LIMIT 5)
UNION
(SELECT booking_date as addON, id as bid, name as customerName FROM `hotel_group` where user_id=$user ORDER BY `id` desc LIMIT 5)";
        $query = $this->db1->query($sql);
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function total_login()
    {
     
		  //$sql="SELECT count(user_id) as total FROM `hotel_login_session` where user_id= $user";
		  
	   $hotel=$this->session->userdata('user_hotel');
	   $user=$this->session->userdata('user_id');
	   $query_date=date('Y-m-d');


			$sd= date('Y-m-01', strtotime($query_date));
			$nd=date('Y-m-t', strtotime($query_date));
		 $sql1="SELECT count(user_id) as total FROM `hotel_login_session` where user_id='". $user."' and status='Success'  and (login_dateTime between '".$sd."' and '".$nd."')";
		$query1 = $this->db1->query($sql1);
        if($query1->num_rows()>0){
            return $query1->row();
        }else{
            return false;
        }
		
    }
	
	 function get_customerName_booking($id){

		 $this->db1->where('booking_id',$id);
         $query=$this->db1->get(TABLE_PRE.'bookings');
         if($query->num_rows()>0){
            return $query->row();
         }else{
            return false;
         }
     }
	
	function get_avRm($start,$end,$room_id){
		//$this->db1->where()
		/*$this->db1->select('*');
		$this->db1->where('room_id',$rm_id);
		$this->db1->where("date_format(cust_end_date,'%d-%m-%Y') >=",$start);
		$this->db1->where("date_format(cust_end_date,'%d-%m-%Y') <=",$end);
		$qry=$this->db1->get(TABLE_PRE.'bookings');*/
		$sql="select room_id,booking_id from hotel_bookings where `room_id`=$room_id and ((date(cust_from_date) between '$start' and '$end' ) or (date(cust_end_date) between '$start' and '$end' ) or (date(cust_from_date) <'$end' and date(cust_end_date) >'$start') or (date(cust_from_date) >'$start' and date(cust_end_date) <'$end') or (date(cust_from_date) = '$start' and date(cust_end_date) >'$end')) and booking_status_id !='7' and booking_status_id !='6' AND `booking_status_id` != 1 and date(cust_end_date) != '$start' and date(cust_from_date) != '$end'";
		$qry=$this->db1->query($sql);
		if($qry->num_rows()>1){
			return 1;
		}else{
			return 0;
		}
	}
	
	function getBookingSource_byDate($date){
		
		$sql="SELECT `booking_taking_date`,COUNT(*) as count,`booking_source`,`booking_source_name` FROM `hotel_bookings` 
		WHERE `hotel_id` = ".$this->session->userdata('user_hotel')." AND `booking_status_id` != 7 AND `booking_status_id` != 8 AND `booking_taking_date` = '".$date."'
		GROUP BY `booking_taking_date`, `booking_source`  
		ORDER BY `booking_taking_date`, count  DESC";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
	}
	
	function getBookingById($id){
		
		$sql="SELECT
					`room_id`,`cust_from_date_actual`,`cust_end_date_actual`
				FROM
					`hotel_bookings`
				WHERE
					`hotel_id` = ".$this->session->userdata('user_hotel')." AND `booking_status_id` = '6' AND `booking_id` = ".$id;
					
		$qry = $this->db1->query($sql);

		if($qry->num_rows()>0){
			return $qry->row();
		}else{
			return 0;
		}
	}
	
	function getAllBookingCntByDate($id){
		
		$sql="SELECT
				COUNT(*) as cnt
			  FROM
				`hotel_bookings`
			  WHERE
				`hotel_id` = ".$this->session->userdata('user_hotel')." AND  `room_id` = ".$id." AND DATE(NOW()) BETWEEN DATE(`cust_from_date`) AND DATE(`cust_end_date`) 
				AND `booking_status_id` NOT IN (6,7,8)";
				
		//echo $sql;
					
		$qry = $this->db1->query($sql);

		return $qry->row();
		
	}
	
	function getBookingStatus_byDate($date){
		
		$sql="SELECT
			  bk.`booking_taking_date` as date,
			  ty.`booking_status` as status,
			  COUNT(bk.`booking_id`) as count,
			  ty.bar_color_code as color
			FROM
			  `hotel_bookings` AS bk
			  LEFT JOIN booking_status_type as ty
			  ON bk.`booking_status_id` = ty.`status_id`
			WHERE
			  bk.`hotel_id` = ".$this->session->userdata('user_hotel')." AND bk.`booking_taking_date` = '".$date."'
			GROUP BY
			  bk.`booking_taking_date`,
			  bk.`booking_status_id`
			ORDER BY
			  bk.`booking_taking_date` DESC,
			  COUNT(bk.`booking_id`)";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
	}
	
	function getBookingGrp_byDate($date){
		
		$sql="SELECT
				  `booking_taking_date`,
				  COUNT(*) AS grp
				FROM
				  `hotel_bookings`
				WHERE
				  `hotel_id` = '".$this->session->userdata('user_hotel')."' AND `booking_status_id` != 7 AND `booking_status_id` != 1 AND `booking_taking_date` = '".$date."' AND `group_id` <> 0
				GROUP BY
				  `booking_taking_date`
				ORDER BY
				  `booking_taking_date`";
	
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
	}
	
	function getLineItem_byTakingDate($date){
		
		$sql="SELECT
				  SUM(`room_rent`) + SUM(`rr_total_tax`) + SUM(`meal_plan_chrg`) + SUM(`mp_total_tax`) as amt
				FROM
				  `booking_charge_line_item`
				WHERE
				  `booking_id` IN (SELECT bk.`booking_id` FROM hotel_bookings as bk WHERE bk.`booking_taking_date` = '".$date."') AND `hotel_id` = ".$this->session->userdata('user_hotel')." AND `isCancelled` <> '1'";
	
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
	}
	
	
	
	function getBookingGrpWithin_byDate($date1){
		
		$sql = "select 
					count(*) as count,
					SUM(if(`group_id`<> 0, 1, 0)) as grp
				from hotel_bookings 
				where 
					((((date(cust_from_date) between '".$date1."' and DATE_ADD('".$date1."',INTERVAL 1 DAY)) or  
					(date(cust_end_date) between '".$date1."' and DATE_ADD('".$date1."',INTERVAL 1 DAY)) or 
					(date(cust_from_date) <'".$date1."' and date(cust_end_date) >DATE_ADD('".$date1."',INTERVAL 1 DAY)) or 
					(date(cust_from_date) >'".$date1."' and date(cust_end_date) < DATE_ADD('".$date1."',INTERVAL 1 DAY)) or  
					(date(cust_from_date) = DATE_ADD('".$date1."',INTERVAL 1 DAY)))
					and date(cust_end_date) > DATE_ADD('".$date1."',INTERVAL 1 DAY)) and booking_status_id !='7' 
					and  date(cust_from_date) != DATE_ADD('".$date1."',INTERVAL 1 DAY)) OR 
					(date(cust_from_date) = '".$date1."')";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
	}
	
	
	function getBookingAmt_byDate($date){
		
		$sql = "SELECT
				  `date`,
				  SUM(`room_rent`) as 'room_rent',
				  SUM(`rr_total_tax`) as 'room_tax',
				  SUM(`meal_plan_chrg`) as 'meal',
				  SUM(`mp_total_tax`) as 'meal_tax'
				FROM
				  `booking_charge_line_item`
				WHERE
				  `date` = '".$date."' AND `isCancelled` <> '1' AND
				  `hotel_id` = ".$this->session->userdata('user_hotel');
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
	}
	
	
	function getBookingNature_byDate($date){
		
		$sql="SELECT
		  `booking_taking_date`,
		  COUNT(*) AS count,
		  `nature_visit`
		FROM
		  `hotel_bookings`
		WHERE
		  `hotel_id` = ".$this->session->userdata('user_hotel')." AND `booking_status_id` != 7 AND `booking_status_id` != 1 AND `booking_taking_date` = '".$date."'
		GROUP BY
		  `nature_visit`
		ORDER BY
		  `booking_taking_date`, count DESC";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
	}
	
		
	
	
	function getBookingUnitType_byDate($date){
		
		$sql="SELECT
		  bk.`booking_taking_date`,
		  count(*) as count,
		  ty.`unit_name`
		FROM
		  `hotel_bookings` as bk
		  LEFT JOIN `hotel_room` as rm
		  ON bk.room_id = rm.room_id
		  LEFT JOIN hotel_unit_type as ty
		  ON rm.`unit_id` = ty.`id`
		WHERE
		  bk.`hotel_id` = '".$this->session->userdata('user_hotel')."' AND bk.`booking_status_id` != 7 AND bk.`booking_status_id` != 1 AND bk.`booking_taking_date` = '".$date."'
		GROUP BY
		  bk.`booking_taking_date`,
		  rm.`unit_id`
		ORDER BY
		  bk.`booking_taking_date`,
		  count DESC";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
	}
	
	
	function getBooking_byDate(){
		
		$sql="SELECT `booking_taking_date`,`booking_id` ,COUNT(*) as count, SUM(`stay_days`) as days FROM `hotel_bookings` 
		WHERE `hotel_id` = ".$this->session->userdata('user_hotel')." AND `booking_status_id` IN (1,2,4,5,6) AND `booking_taking_date` > 0
		GROUP BY `booking_taking_date`  
		ORDER BY `booking_taking_date` DESC";
		
		/*$sql="SELECT `booking_taking_date`,`booking_id` FROM `hotel_bookings` 
		WHERE `hotel_id` = ".$this->session->userdata('user_hotel')." AND `booking_status_id` != 7 AND `booking_status_id` != 1
		  
		ORDER BY `booking_taking_date` DESC";*/
		/*	$sql="SELECT
  `bk`.`booking_id` AS `booking_id`,
    COUNT(*) AS grp,
  `bk`.`cust_from_date` AS `cust_from_date`,
  `bk`.`cust_end_date` AS `cust_end_date`,
  `bk`.`booking_taking_date` AS `booking_taking_date`,
  `bk`.`booking_status_id` AS `booking_status_id`,
  `bk`.`extra_service_total_amount` AS `extra_service_total_amount`,
  `bk`.`booking_extra_charge_amount` AS `booking_extra_charge_amount`,
  `bk`.`booking_extra_charge_id` AS `booking_extra_charge_id`,
  `bk`.`service_price` AS `service_price`,
  `bk`.`room_id` AS `room_id`,
  `bk`.`group_id` AS `group_id`,
  `bk`.`hotel_id` AS `hotel_id`,
  `ch_line_item`.`rr_tot` AS `rm_total`,
  `ch_line_item`.`rr_tot_tax` AS `rr_tot_tax`,
  `ch_line_item`.`exrr` AS `exrr`,
  `ch_line_item`.`exrr_tax` AS `exrr_tax`,
  `ch_line_item`.`mp_tot` AS `mp_tot`,
  `ch_line_item`.`mp_tax` AS `mp_tax`,
  `ch_line_item`.`exmp` AS `exmp`,
  `ch_line_item`.`exmp_tax` AS `exmp_tax`
FROM
  
    `hotel_bookings` `bk`
  LEFT JOIN
    `booking_charge_line_item_view` `ch_line_item` ON(
      
        `bk`.`booking_id` = `ch_line_item`.`booking_id`
  
WHERE
  `bk`.`booking_status_id` <> 7 AND `bk`.`booking_status_id` != 1
GROUP BY
  `bk`.`booking_id`
ORDER BY
  `bk`.`booking_id` DESC;";*/
		
		
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
	}
	
	function getBookingCustFrom_byDate(){
		
		$sql="SELECT date(`cust_from_date`) as date, COUNT(*) as count FROM `hotel_bookings` 
		WHERE `hotel_id` = ".$this->session->userdata('user_hotel')." AND `booking_status_id` != 7 AND `booking_status_id` != 8
		GROUP BY date(`cust_from_date`) 
		ORDER BY date(`cust_from_date`) DESC";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
	}
	
	function getBookingLineItem_byDate(){
		
		$sql = "SELECT
				  `date`,
				  SUM(`room_rent`) as roomRent,
				  SUM(`rr_total_tax`) as rrTax,
				  SUM(`meal_plan_chrg`) as mealPlan,
				  SUM(`mp_total_tax`) as mpTax
				FROM
				  `booking_charge_line_item`
				WHERE
				  `isCancelled` <> '1' AND `hotel_id` = ".$this->session->userdata('user_hotel')."
				GROUP BY 
				  `date`
				ORDER BY
				  `date` desc";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getBookingLineItem_byDate1($start_date,$end_date){
		
		$sql="SELECT
				  `date`,
				 
				  SUM(`room_rent`) as roomRent,
				  SUM(`rr_total_tax`) as rrTax,
				  SUM(`meal_plan_chrg`) as mealPlan,
				  SUM(`mp_total_tax`) as mpTax
				FROM
				  `booking_charge_line_item`
				WHERE
				  `isCancelled` <> '1' AND `hotel_id` = ".$this->session->userdata('user_hotel')."
				  AND  `date` BETWEEN '".$start_date."' AND '".$end_date."'
				GROUP BY 
				  `date`
				ORDER BY
				  `date` asc";		  
				  
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getPosAmt($start_date,$end_date){
		$sql="SELECT `booking_id`,`booking_type`, sum(`total_amount`) as tot_Amt FROM `hotel_pos` WHERE DATE(`date`) BETWEEN '".$start_date."' AND '".$end_date."'";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->row();
		}else{
			return 0;
		}
	
	}
	
	
	function getExtraCharge_byDate($date){
		
		$sql = "SELECT
				  COUNT(`crg_id`) as count, 
				  SUM(`crg_quantity`) as itemCount,
				  SUM(`crg_total`) as total
				FROM
				  `hotel_extra_charges_mapping`
				WHERE
				  DATE(`aded_on`) = '".$date."' AND `hotel_id` = ".$this->session->userdata('user_hotel')."
				GROUP BY 
				  DATE(`aded_on`)";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getExtraCharge_byBk($id, $type){
		
		$sql = "SELECT
				  COUNT(`crg_id`) as count, 
				  SUM(`crg_quantity`) as itemCount,
				  SUM(`crg_total`) as total
				FROM
				  `hotel_extra_charges_mapping`
				WHERE
				  crg_booking_id = ".$id." AND booking_type = '".$type."' AND `hotel_id` = ".$this->session->userdata('user_hotel')."
				GROUP BY
				  crg_booking_id";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	
	function getExtraChargeAmt($start_date,$end_date){
		
		$sql = "SELECT
				  COUNT(`crg_id`) as count, 
				  SUM(`crg_quantity`) as itemCount,
				  SUM(`crg_total`) as total
				FROM
				  `hotel_extra_charges_mapping`
				WHERE
				  DATE(`aded_on`) BETWEEN '".$start_date."' AND '".$end_date."' AND `hotel_id` = ".$this->session->userdata('user_hotel')."
				GROUP BY 
				  DATE(`aded_on`)";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->row();
		}else{
			return 0;
		}
		
	}
	
	function getPOS_byDate($date){
		
		$sql = "SELECT
				  COUNT(`pos_id`) as count,
				  SUM(`bill_amount`) as total
				FROM
				  `hotel_pos`
				WHERE
				  DATE(`date`) = '".$date."'";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getLaundry_byDate($date){
		
		$sql = "SELECT
				  COUNT(`laundry_id`) as count,
				  SUM(`grand_total`) as total
				FROM
				  `hotel_laundry_master`
				WHERE
				  `hotel_id` = ".$this->session->userdata('user_hotel')." AND DATE(`dateAdded`) = '".$date."' AND `laundry_booking_type` IN ('sb','gb')";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
		function getDisc_byDate($date){
		
		$sql = "SELECT
				  COUNT(`id`)as count,
				  SUM(`discount_amount`) as total
				FROM
				  `hotel_discount_mapping`
				WHERE
				  `hotel_id` = ".$this->session->userdata('user_hotel')." AND DATE(`add_date`) = '".$date."' AND (`booking_id` <> 0 OR `group_id` <> 0)";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
		}
		
		
		function getDiscAmt($start,$end){
		
		$sql = "SELECT
				  COUNT(`id`)as count,
				  SUM(`discount_amount`) as total
				FROM
				  `hotel_discount_mapping`
				WHERE
				  `hotel_id` = ".$this->session->userdata('user_hotel')." AND DATE(`add_date`) BETWEEN '".$start."' AND '".$end."' AND (`booking_id` <> 0 OR `group_id` <> 0)";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->row();
		}else{
			return 0;
		}
		
		}
	function getLaundry_byDate1($start_date,$end_date){
		
		$sql = "SELECT
				  COUNT(`laundry_id`) as count,
				  SUM(`grand_total`) as total
				FROM
				  `hotel_laundry_master`
				WHERE
				  `hotel_id` = ".$this->session->userdata('user_hotel')." AND DATE(`dateAdded`) BETWEEN '".$start_date."' AND '".$end_date."' AND `laundry_booking_type` IN ('sb','gb')";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->row();
		}else{
			return 0;
		}
		
	}
	
	function getAdj_byDate($date){
		
		$sql = "SELECT
				  COUNT(`id`) as count,
				  SUM(`amount`) as total
				FROM
				  `hotel_adjustments`
				WHERE
				  `hotel_id` = ".$this->session->userdata('user_hotel')." AND DATE(`addedOn`) = '".$date."' AND (`group_id` <> 0 OR `booking_id` <> 0)";
						$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getadjAmt($start_date,$end_date){
		
		$sql = "SELECT
				 SUM(`amount`) as total
				FROM
				  `hotel_adjustments`
				WHERE
				  `hotel_id` = ".$this->session->userdata('user_hotel')." AND DATE(`addedOn`) BETWEEN '".$start_date."' AND '".$end_date."'   AND (`group_id` <> 0 OR `booking_id` <> 0)";
						$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->row();
		}else{
			return 0;
		}
		
	}
	
	
	
	
	function get_unitID_room($id){
		$this->db1->where('room_id',$id);
        $query=$this->db1->get('hotel_room');
        //if($query->num_rows()>0){
            $idA=$query->row();
            $idB=$idA->unit_id;
		$this->db1->where('id',$idB);
        $query1=$this->db1->get('hotel_unit_type');
        if($query1->num_rows()>0){
             return $query1->row();
        }
    }
	
	function get_guestInfo(){
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	//	$this->db1->order_by('g_id','desc');
		$query1=$this->db1->get('hotel_guest');
        if($query1->num_rows()>0){
             return $query1->result_array();
        }
	}
	
	function get_payments_items($id){
		$this->db1->select('*');
		$this->db1->where('payment_id',$id);
		$query1=$this->db1->get('hotel_line_item_details');
        if($query1->num_rows()>0){
             return $query1->result();
        }
	}
	
	function all_hotel($id,$user_id){
	  // echo "==".$id;
	  // exit;
		$this->db1->select('*');
		//$this->db->where('chain_id',$id);
		$this->db1->where('admin_id',$user_id);
		$this->db1->from('hotel_admin_details');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
		
        if($query){

            return $query->result();
        }
        else{

            return "wrong";
        }
		
	}
	
	function hotel_name($id){
		$this->db1->select('*');
		$this->db1->where('hotel_id',$id);
		$this->db1->from(TABLE_PRE.'hotel_details');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->row();
        }
        else{

            return "wrong";
        }
		
	}
	
	function get_hotel_name1($hot_id,$chain_id){
		
		$this->db1->select('*');
		//$this->db1->where('chain_id',$chain_id);
		$this->db1->where('hotel_id',$hot_id);
		$this->db1->from('hotel_details_testing');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
		 //print_r($query->result());exit;
        if($query){

            return $query->row();
        }
        else{

            return "wrong";
        }
		
		
	}
	function getUnitType(){
			$this->db1->select('*');
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$query=$this->db1->get('hotel_unit_type');
			if($query->num_rows()>0){
				return $query->result();
			}
		}

	function GetHd($id){
		$this->db1->select('name');
		$this->db1->where('id',$id);
			$query=$this->db1->get('hotel_module_heading');
			if($query->num_rows()>0){
				return $query->row();
			}
	}
function get_booking_details3($bookingID){
		$hotel_id=$this->session->userdata('user_hotel');
        $this->db1->order_by('cust_from_date','desc');
		$this->db1->where('booking_status_id !=',7);
		$this->db1->where('booking_id',$bookingID);
		$this->db1->where('hotel_id',$hotel_id);
		//$this->db1->where('user_id',$this->session->userdata('user_id'));

        //$this->db1->limit(10);
        $query=$this->db1->get('booking_view');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }	
	function get_All_booking_details(){
		$hotel_id = $this->session->userdata('user_hotel');
        $this->db1->order_by('booking_id','desc');
		$this->db1->where('booking_status_id !=',7);
		$this->db1->where('booking_status_id !=',8);
		$this->db1->where('hotel_id',$hotel_id);
        $query=$this->db1->get('booking_view');
		
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	
	
	
	
	
	function get_tranc_details($booking_id){
		$hotel_id=$this->session->userdata('user_hotel');
       
		$this->db1->where('t_booking_id',$booking_id);
		$this->db1->where('hotel_id',$hotel_id);
        $query=$this->db1->get('hotel_transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	function getUnit_details($uid){
		$this->db1->where('id',$uid);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get('hotel_unit_type');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
	}
	
	function set_default($warehouseId,$hotel_id){
					$data=array(
                                'default_warehouse'=>'0',
                               
								);
		// $this->db1->where('id',$warehouseId);
		 $this->db1->where('hotel_id',$hotel_id);
	$query=$this->db1->update('hotel_warehouse',$data);
	
					$data=array(
                                'default_warehouse'=>'1',
                               
								);
		 $this->db1->where('id',$warehouseId);
		 $this->db1->where('hotel_id',$hotel_id);
	$query=$this->db1->update('hotel_warehouse',$data);
	  if($query){
		  return true;
	  }
	}
	
	function set_default_kit($kitId,$hotel_id){
					$data=array(
                                'default_kit'=>'0',
                               
								);
		// $this->db1->where('id',$warehouseId);
		 $this->db1->where('hotel_id',$hotel_id);
	$query=$this->db1->update('hotel_define_kit',$data);
	
					$data=array(
                                'default_kit'=>'1',
                               
								);
		 $this->db1->where('hotel_define_kit_id',$kitId);
		 $this->db1->where('hotel_id',$hotel_id);
	$query=$this->db1->update('hotel_define_kit',$data);
	  if($query){
		  return true;
	  }
	}
	
function get_default_kit($kitId,$hotel_id){
					
		// $this->db1->where('hotel_define_kit_id',$kitId);
		 $this->db1->where('hotel_id',$hotel_id);
		 $this->db1->where('default_kit','1');
	$query=$this->db1->get('hotel_define_kit');
	   if($query->num_rows()>0){
            return 1;
        }else{
            return 0;
        }
	}	
	
	function delete_alot_kit($id){
		$this->db1->where_in('id',$id);
		$query=$this->db1->delete('kit_inventory_log');
		if($query){
			return true;
		}
	}
	function pay_transactions($id,$type){
		$hotel_id=$this->session->userdata('user_hotel');
       
		$this->db1->where('payment_id',$id);
		$this->db1->where('transaction_type',$type);
		$this->db1->where('hotel_id',$hotel_id);
        $query=$this->db1->get('hotel_transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}

//edit it data format earlier back up
function all_bookings_daily_by_date($start_date,$end_date){
	//	echo"$start_date";echo"hi<br/>";	echo"$end_date";exit;	
		$hotel_id=$this->session->userdata('user_hotel');
		$sql="(SELECT * FROM ((SELECT
  `sbk`.`booking_id` AS `booking_id`,
   0 AS `shadow_id`,
  `sbk`.`group_id` AS `type`,
  `sbk`.`guest_id` AS `guest_id`,
  `sbk`.`booking_source` AS `booking_source`,
  `sbk`.`room_id` AS `room_id`,
  `sbk`.`bill_to_com` AS `btc`,
  `sbk`.`cust_from_date_actual` AS `cust_from_date_actual`,
  `sbk`.`cust_end_date_actual` AS `cust_end_date_actual`,
  `sbk`.`booking_status_id` AS `booking_status_id`,
   `stp`.booking_status as `booking_status`,
  `sbk`.`extra_service_total_amount` AS `extra_service_total_amount`,
  `sbk`.`booking_extra_charge_amount` AS `booking_extra_charge_amount`,
  `sbk`.`booking_extra_charge_id` AS `booking_extra_charge_id`,
  `sbk`.`service_price` AS `service_price`,
  `sbk`.`hotel_id` AS `hotel_id`,
  `ch_line_item`.`rr_tot` AS `rm_total`,
  `ch_line_item`.`rr_tot_tax` AS `rr_tot_tax`,
  `ch_line_item`.`exrr` AS `exrr`,
  `ch_line_item`.`exrr_tax` AS `exrr_tax`,
  `ch_line_item`.`mp_tot` AS `mp_tot`,
  `ch_line_item`.`mp_tax` AS `mp_tax`,
  `ch_line_item`.`exmp` AS `exmp`,
  `ch_line_item`.`exmp_tax` AS `exmp_tax`
FROM
 
                    `hotel_bookings` `sbk`
                  LEFT JOIN
                    `hotel_transactions` `tran` ON
                    
                          `sbk`.`booking_id` = `tran`.`t_booking_id`
                         AND `tran`.`t_status` = 'Done'
                   
                LEFT JOIN
                  `booking_charge_line_item_view` `ch_line_item` ON
                    
                      `sbk`.`booking_id` = `ch_line_item`.`booking_id` 
               
               LEFT JOIN
            `booking_status_type` `stp` ON
              
                `sbk`.`booking_status_id` = `stp`.`status_id`
              
          
        LEFT JOIN
          `booking_status_type` `sts` ON
            
              `sbk`.`booking_status_id_secondary` = `sts`.`status_id`
         
      
   
  WHERE `sbk`.`group_id`=0 AND  `sbk`.`cust_from_date_actual`>='".$start_date."' AND  `sbk`.`cust_from_date_actual`<='".$end_date."'  AND `sbk`.`hotel_id`='".$hotel_id."'
  GROUP BY
  `sbk`.`booking_id`
)
  UNION
  (
   SELECT
  `sbk1`.`booking_id` AS `booking_id`,
   `sbk1`.`bookings_shadow_id` AS `shadow_id`,
  `sbk1`.`group_id` AS `type`,
  `sbk1`.`guest_id` AS `guest_id`,
  `sbk1`.`booking_source` AS `booking_source`,
  `sbk1`.`room_id` AS `room_id`,
  `sbk1`.`bill_to_com` AS `btc`,
  `sbk1`.`cust_from_date_actual` AS `cust_from_date_actual`,
  `sbk1`.`cust_end_date_actual` AS `cust_end_date_actual`,
  `sbk1`.`booking_status_id` AS `booking_status_id`,
   `stp`.booking_status as `booking_status`,
  `sbk1`.`extra_service_total_amount` AS `extra_service_total_amount`,
  `sbk1`.`booking_extra_charge_amount` AS `booking_extra_charge_amount`,
  `sbk1`.`booking_extra_charge_id` AS `booking_extra_charge_id`,
  `sbk1`.`service_price` AS `service_price`,
  `sbk1`.`hotel_id` AS `hotel_id`,
  `ch_line_item`.`rr_tot` AS `rm_total`,
  `ch_line_item`.`rr_tot_tax` AS `rr_tot_tax`,
  `ch_line_item`.`exrr` AS `exrr`,
  `ch_line_item`.`exrr_tax` AS `exrr_tax`,
  `ch_line_item`.`mp_tot` AS `mp_tot`,
  `ch_line_item`.`mp_tax` AS `mp_tax`,
  `ch_line_item`.`exmp` AS `exmp`,
  `ch_line_item`.`exmp_tax` AS `exmp_tax`
FROM
 
                    `hotel_bookings_shadow` `sbk1`
                  LEFT JOIN
                    `hotel_transactions_shadow` `tran` ON
                    
                          `sbk1`.`booking_id` = `tran`.`t_booking_id`
                         AND `tran`.`t_status` = 'Done'
                   
                LEFT JOIN
                  `booking_charge_line_item_view` `ch_line_item` ON
                    
                      `sbk1`.`booking_id` = `ch_line_item`.`booking_id`
               
              LEFT JOIN
                `hotel_guest` `gst` ON `sbk1`.`guest_id` = `gst`.`g_id`
           
            
          LEFT JOIN
            `booking_status_type` `stp` ON
              
                `sbk1`.`booking_status_id` = `stp`.`status_id`
              
          
        LEFT JOIN
          `booking_status_type` `sts` ON
            
              `sbk1`.`booking_status_id_secondary` = `sts`.`status_id`
      
   
WHERE `sbk1`.`group_id`=0 AND  `sbk1`.`cust_from_date_actual`>='".$start_date."' AND  `sbk1`.`cust_from_date_actual`<='".$end_date."'  AND `sbk1`.`hotel_id`='".$hotel_id."'
 GROUP BY
  `sbk1`.`booking_id` )
  
UNION

(SELECT
 `bk`.`id` AS `booking_id`,
 	0 AS `shadow_id`,
  `bk`.`id` AS `type`,
  `bk`.`guestID` AS `guest_id`,
  `bk`.`booking_source` AS `booking_source`,
  `bk`.`number_room` AS `room_id`,
  `bk`.`bill_to_com` AS `bill_to_com`,
  `bk`.`start_date` AS `cust_from_date_actual`,
 `bk`.`end_date` AS `cust_end_date_actual`,
  `bk`.`status` AS `booking_status_id`,
  `stp`.booking_status as `booking_status`,
  `bk`.`charges_cost` AS `extra_service_total_amount`,
  `bk`.`charges_cost` AS `booking_extra_charge_amount`,
  `bk`.`charges_id` AS `booking_extra_charge_id`,
  `bk`.`charges_cost` AS `service_price`,
  `bk`.`hotel_id` AS `hotel_id`,
  
 SUM( `ch_line_item`.`rr_tot`) AS `rm_total`,
 SUM( `ch_line_item`.`rr_tot_tax`) AS `rr_tot_tax`,
  SUM(`ch_line_item`.`exrr`) AS `exrr`,
  SUM(`ch_line_item`.`exrr_tax`) AS `exrr_tax`,
  SUM(`ch_line_item`.`mp_tot`) AS `mp_tot`,
  SUM(`ch_line_item`.`mp_tax`) AS `mp_tax`,
 SUM( `ch_line_item`.`exmp`) AS `exmp`,
 SUM( `ch_line_item`.`exmp_tax`) AS `exmp_tax`
FROM
 `hotel_group` `bk`
     LEFT JOIN
     `booking_charge_line_item_view` `ch_line_item` ON
                   
                      `bk`.`id` = `ch_line_item`.`group_id`
                       LEFT JOIN
            `booking_status_type` `stp` ON
              
                `bk`.`status` = `stp`.`status_id`

 WHERE `bk`.`start_date`>='".date('m/d/Y',strtotime($start_date))."' AND `bk`.`start_date`<='".date('m/d/Y',strtotime($end_date))."' AND `bk`.`hotel_id`='".$hotel_id."'
GROUP BY
  `bk`.`id`
)
  UNION
 ( SELECT
 `bk`.`id` AS `booking_id`,
  `bk`.group_shadow_id as `shadow_id`,
  `bk`.`id` AS `type`,
  `bk`.`guestID` AS `guest_id`,
  `bk`.`booking_source` AS `booking_source`,
  `bk`.`number_room` AS `room_id`,
  `bk`.`bill_to_com` AS `bill_to_com`,
  `bk`.`start_date` AS `cust_from_date_actual`,
 `bk`.`end_date` AS `cust_end_date_actual`,
  `bk`.`status` AS `booking_status_id`,
  `stp`.booking_status as `booking_status`,
  `bk`.`charges_cost` AS `extra_service_total_amount`,
  `bk`.`charges_cost` AS `booking_extra_charge_amount`,
  `bk`.`charges_id` AS `booking_extra_charge_id`,
  `bk`.`charges_cost` AS `service_price`,
  `bk`.`hotel_id` AS `hotel_id`,
  
 SUM( `ch_line_item`.`rr_tot`) AS `rm_total`,
 SUM( `ch_line_item`.`rr_tot_tax`) AS `rr_tot_tax`,
  SUM(`ch_line_item`.`exrr`) AS `exrr`,
  SUM(`ch_line_item`.`exrr_tax`) AS `exrr_tax`,
  SUM(`ch_line_item`.`mp_tot`) AS `mp_tot`,
  SUM(`ch_line_item`.`mp_tax`) AS `mp_tax`,
 SUM( `ch_line_item`.`exmp`) AS `exmp`,
 SUM( `ch_line_item`.`exmp_tax`) AS `exmp_tax`
FROM
 `hotel_group_shadow` `bk`
     LEFT JOIN
     `booking_charge_line_item_view` `ch_line_item` ON
                   
                      `bk`.`id` = `ch_line_item`.`group_id`
   LEFT JOIN
            `booking_status_type` `stp` ON
              
                `bk`.`status` = `stp`.`status_id`
WHERE `bk`.`start_date`>='".date('m/d/Y',strtotime($start_date))."' AND `bk`.`start_date`<='".date('m/d/Y',strtotime($end_date))."' AND `bk`.`hotel_id`='".$hotel_id."'
GROUP BY
  `bk`.`id`)) as A ORDER BY A.booking_id)";
   //echo "$sql"; exit;
  $qry = $this->db1->query($sql); 
        if($qry->num_rows()>0){
            return $qry->result();
        }else{
            return false;
        }
		
		
		
		
	}
	//back up before earlier before editing date format 10oct1992
	

	
	//function start
	function all_bookings_daily_grp(){
		$date=date("Y-m-d");
		$date1=date("m/d/Y");
		$hotel_id=$this->session->userdata('user_hotel');
		$sql="(SELECT * FROM ((SELECT
  `sbk`.`booking_id` AS `booking_id`,
   0 AS `shadow_id`,
  `sbk`.`group_id` AS `type`,
  `sbk`.`guest_id` AS `guest_id`,
  `sbk`.`booking_source` AS `booking_source`,
  `sbk`.`room_id` AS `room_id`,
  `sbk`.`bill_to_com` AS `btc`,
  `sbk`.`cust_from_date_actual` AS `cust_from_date_actual`,
  `sbk`.`cust_end_date_actual` AS `cust_end_date_actual`,
  `sbk`.`booking_status_id` AS `booking_status_id`,
   `stp`.booking_status as `booking_status`,
  `sbk`.`extra_service_total_amount` AS `extra_service_total_amount`,
  `sbk`.`booking_extra_charge_amount` AS `booking_extra_charge_amount`,
  `sbk`.`booking_extra_charge_id` AS `booking_extra_charge_id`,
  `sbk`.`service_price` AS `service_price`,
  `sbk`.`hotel_id` AS `hotel_id`,
  `ch_line_item`.`rr_tot` AS `rm_total`,
  `ch_line_item`.`rr_tot_tax` AS `rr_tot_tax`,
  `ch_line_item`.`exrr` AS `exrr`,
  `ch_line_item`.`exrr_tax` AS `exrr_tax`,
  `ch_line_item`.`mp_tot` AS `mp_tot`,
  `ch_line_item`.`mp_tax` AS `mp_tax`,
  `ch_line_item`.`exmp` AS `exmp`,
  `ch_line_item`.`exmp_tax` AS `exmp_tax`
FROM
 
                    `hotel_bookings` `sbk`
                  LEFT JOIN
                    `hotel_transactions` `tran` ON
                    
                          `sbk`.`booking_id` = `tran`.`t_booking_id`
                         AND `tran`.`t_status` = 'Done'
                   
                LEFT JOIN
                  `booking_charge_line_item_view` `ch_line_item` ON
                    
                      `sbk`.`booking_id` = `ch_line_item`.`booking_id` 
               
               LEFT JOIN
            `booking_status_type` `stp` ON
              
                `sbk`.`booking_status_id` = `stp`.`status_id`
              
          
        LEFT JOIN
          `booking_status_type` `sts` ON
            
              `sbk`.`booking_status_id_secondary` = `sts`.`status_id`
         
      
   
 WHERE  (`sbk`.`cust_from_date_actual` BETWEEN  '".$date."' and '".$date."') or  (`sbk`.`cust_end_date_actual` between '".$date."' and '".$date."' )  AND  `sbk`.`hotel_id`='".$hotel_id."' AND `sbk`.`group_id`=0
)
  UNION
  (
   SELECT
  `sbk1`.`booking_id` AS `booking_id`,
   `sbk1`.`bookings_shadow_id` AS `shadow_id`,
  `sbk1`.`group_id` AS `type`,
  `sbk1`.`guest_id` AS `guest_id`,
  `sbk1`.`booking_source` AS `booking_source`,
  `sbk1`.`room_id` AS `room_id`,
  `sbk1`.`bill_to_com` AS `btc`,
  `sbk1`.`cust_from_date_actual` AS `cust_from_date_actual`,
  `sbk1`.`cust_end_date_actual` AS `cust_end_date_actual`,
  `sbk1`.`booking_status_id` AS `booking_status_id`,
   `stp`.booking_status as `booking_status`,
  `sbk1`.`extra_service_total_amount` AS `extra_service_total_amount`,
  `sbk1`.`booking_extra_charge_amount` AS `booking_extra_charge_amount`,
  `sbk1`.`booking_extra_charge_id` AS `booking_extra_charge_id`,
  `sbk1`.`service_price` AS `service_price`,
  `sbk1`.`hotel_id` AS `hotel_id`,
  `ch_line_item`.`rr_tot` AS `rm_total`,
  `ch_line_item`.`rr_tot_tax` AS `rr_tot_tax`,
  `ch_line_item`.`exrr` AS `exrr`,
  `ch_line_item`.`exrr_tax` AS `exrr_tax`,
  `ch_line_item`.`mp_tot` AS `mp_tot`,
  `ch_line_item`.`mp_tax` AS `mp_tax`,
  `ch_line_item`.`exmp` AS `exmp`,
  `ch_line_item`.`exmp_tax` AS `exmp_tax`
FROM
 
                    `hotel_bookings_shadow` `sbk1`
                  LEFT JOIN
                    `hotel_transactions_shadow` `tran` ON
                    
                          `sbk1`.`booking_id` = `tran`.`t_booking_id`
                         AND `tran`.`t_status` = 'Done'
                   
                LEFT JOIN
                  `booking_charge_line_item_view` `ch_line_item` ON
                    
                      `sbk1`.`booking_id` = `ch_line_item`.`booking_id`
               
              LEFT JOIN
                `hotel_guest` `gst` ON `sbk1`.`guest_id` = `gst`.`g_id`
           
            
          LEFT JOIN
            `booking_status_type` `stp` ON
              
                `sbk1`.`booking_status_id` = `stp`.`status_id`
              
          
        LEFT JOIN
          `booking_status_type` `sts` ON
            
              `sbk1`.`booking_status_id_secondary` = `sts`.`status_id`
      
   
WHERE  (`sbk1`.`cust_from_date_actual` BETWEEN  '".$date."' and '".$date."') or  (`sbk1`.`cust_end_date_actual` between '".$date."' and '".$date."' ) AND  `sbk1`.`hotel_id`='".$hotel_id."' AND  `sbk1`.`group_id`='0'
 GROUP BY
  `sbk1`.`booking_id` )
  
UNION

(SELECT
 `bk`.`id` AS `booking_id`,
 	0 AS `shadow_id`,
  `bk`.`id` AS `type`,
  `bk`.`guestID` AS `guest_id`,
  `bk`.`booking_source` AS `booking_source`,
  `bk`.`number_room` AS `room_id`,
  `bk`.`bill_to_com` AS `bill_to_com`,
  `bk`.`start_date` AS `cust_from_date_actual`,
 `bk`.`end_date` AS `cust_end_date_actual`,
  `bk`.`status` AS `booking_status_id`,
  `stp`.booking_status as `booking_status`,
  `bk`.`charges_cost` AS `extra_service_total_amount`,
  `bk`.`charges_cost` AS `booking_extra_charge_amount`,
  `bk`.`charges_id` AS `booking_extra_charge_id`,
  `bk`.`charges_cost` AS `service_price`,
  `bk`.`hotel_id` AS `hotel_id`,
  
 SUM( `ch_line_item`.`rr_tot`) AS `rm_total`,
 SUM( `ch_line_item`.`rr_tot_tax`) AS `rr_tot_tax`,
  SUM(`ch_line_item`.`exrr`) AS `exrr`,
  SUM(`ch_line_item`.`exrr_tax`) AS `exrr_tax`,
  SUM(`ch_line_item`.`mp_tot`) AS `mp_tot`,
  SUM(`ch_line_item`.`mp_tax`) AS `mp_tax`,
 SUM( `ch_line_item`.`exmp`) AS `exmp`,
 SUM( `ch_line_item`.`exmp_tax`) AS `exmp_tax`
FROM
 `hotel_group` `bk`
     LEFT JOIN
     `booking_charge_line_item_view` `ch_line_item` ON
                   
                      `bk`.`id` = `ch_line_item`.`group_id`
                       LEFT JOIN
            `booking_status_type` `stp` ON
              
                `bk`.`status` = `stp`.`status_id`

WHERE (`bk`.`start_date` BETWEEN  '".$date1."' and '".$date1."') or  (`bk`.`end_date` between '".$date1."' and '".$date1."' ) or ( `bk`.`end_date` <'".$date1."' and `bk`.`end_date` >'".$date1."')  AND `bk`.`hotel_id`='".$hotel_id."'
GROUP BY
  `bk`.`id`
)
  UNION
 ( SELECT
 `bk`.`id` AS `booking_id`,
  `bk`.group_shadow_id as `shadow_id`,
  `bk`.`id` AS `type`,
  `bk`.`guestID` AS `guest_id`,
  `bk`.`booking_source` AS `booking_source`,
  `bk`.`number_room` AS `room_id`,
  `bk`.`bill_to_com` AS `bill_to_com`,
  `bk`.`start_date` AS `cust_from_date_actual`,
 `bk`.`end_date` AS `cust_end_date_actual`,
  `bk`.`status` AS `booking_status_id`,
  `stp`.booking_status as `booking_status`,
  `bk`.`charges_cost` AS `extra_service_total_amount`,
  `bk`.`charges_cost` AS `booking_extra_charge_amount`,
  `bk`.`charges_id` AS `booking_extra_charge_id`,
  `bk`.`charges_cost` AS `service_price`,
  `bk`.`hotel_id` AS `hotel_id`,
  
 SUM( `ch_line_item`.`rr_tot`) AS `rm_total`,
 SUM( `ch_line_item`.`rr_tot_tax`) AS `rr_tot_tax`,
  SUM(`ch_line_item`.`exrr`) AS `exrr`,
  SUM(`ch_line_item`.`exrr_tax`) AS `exrr_tax`,
  SUM(`ch_line_item`.`mp_tot`) AS `mp_tot`,
  SUM(`ch_line_item`.`mp_tax`) AS `mp_tax`,
 SUM( `ch_line_item`.`exmp`) AS `exmp`,
 SUM( `ch_line_item`.`exmp_tax`) AS `exmp_tax`
FROM
 `hotel_group_shadow` `bk`
     LEFT JOIN
     `booking_charge_line_item_view` `ch_line_item` ON
                   
                      `bk`.`id` = `ch_line_item`.`group_id`
   LEFT JOIN
            `booking_status_type` `stp` ON
              
                `bk`.`status` = `stp`.`status_id`
WHERE (`bk`.`start_date` BETWEEN  '".$date1."' and '".$date1."') or  (`bk`.`end_date` between '".$date1."' and '".$date1."' ) or ( `bk`.`end_date` <'".$date1."' and `bk`.`end_date` >'".$date1."') AND `bk`.`hotel_id`='".$hotel_id."'
GROUP BY
  `bk`.`id`)) as A ORDER BY A.booking_id)";
 // echo $sql;exit;
  $qry = $this->db1->query($sql); 
        if($qry->num_rows()>0){
            return $qry->result();
        }else{
            return false;
        }
	}
	
function all_bookings_daily_chkout(){
		$date=date("Y-m-d");
		$date1=date("m/d/Y");
		$hotel_id=$this->session->userdata('user_hotel');
		$sql="(SELECT
  `bk`.`booking_id` AS `booking_id`,
  `bk`.`group_id` AS `type`,
 `bk`.`guest_id` AS `guest_id`,
  `bk`.`booking_source` AS `booking_source`,
  `bk`.`room_id` AS `room_id`,
  `bk`.`cust_from_date_actual` AS `cust_from_date_actual`,
  `bk`.`cust_end_date_actual` AS `cust_end_date_actual`,
  `bk`.`booking_status_id` AS `booking_status_id`,
  `bk`.`extra_service_total_amount` AS `extra_service_total_amount`,
  `bk`.`booking_extra_charge_amount` AS `booking_extra_charge_amount`,
  `bk`.`booking_extra_charge_id` AS `booking_extra_charge_id`,
  `bk`.`service_price` AS `service_price`,
  `bk`.`hotel_id` AS `hotel_id`,
   `bk`.`bill_to_com` AS `bill_to_com`,
  `ch_line_item`.`rr_tot` AS `rm_total`,
  `ch_line_item`.`rr_tot_tax` AS `rr_tot_tax`,
  `ch_line_item`.`exrr` AS `exrr`,
  `ch_line_item`.`exrr_tax` AS `exrr_tax`,
  `ch_line_item`.`mp_tot` AS `mp_tot`,
  `ch_line_item`.`mp_tax` AS `mp_tax`,
  `ch_line_item`.`exmp` AS `exmp`,
  `ch_line_item`.`exmp_tax` AS `exmp_tax`
FROM
  (
    (
      (
        (
          (
            (
              (
                (
                  (
                    `hotel_bookings` `bk`
                  LEFT JOIN
                    `hotel_transactions` `tran` ON(
                      (
                        (
                          `bk`.`booking_id` = `tran`.`t_booking_id`
                        ) AND(`tran`.`t_status` = 'Done')
                      )
                    )
                  )
                LEFT JOIN
                  `booking_charge_line_item_view` `ch_line_item` ON(
                    (
                      `bk`.`booking_id` = `ch_line_item`.`booking_id`
                    )
                  )
                )
              LEFT JOIN
                `hotel_guest` `gst` ON((`bk`.`guest_id` = `gst`.`g_id`))
              )
            LEFT JOIN
              `hotel_admin_details` `adm` ON((`bk`.`user_id` = `adm`.`admin_id`))
            )
          LEFT JOIN
            `booking_status_type` `stp` ON(
              (
                `bk`.`booking_status_id` = `stp`.`status_id`
              )
            )
          )
        LEFT JOIN
          `booking_status_type` `sts` ON(
            (
              `bk`.`booking_status_id_secondary` = `sts`.`status_id`
            )
          )
        )
      LEFT JOIN
        `hotel_broker` `ag` ON(
          (
            (`bk`.`broker_id` = `ag`.`b_id`) AND(`ag`.`hotel_id` = `bk`.`hotel_id`)
          )
        )
      )
    LEFT JOIN
      `hotel_channel` `ch` ON(
        (
          (`bk`.`channel_id` = `ch`.`channel_id`) AND(`ch`.`hotel_id` = `bk`.`hotel_id`)
        )
      )
    )
  LEFT JOIN
    `hotel_room` `rm` ON(
      (
        (`bk`.`room_id` = `rm`.`room_id`) AND(`bk`.`hotel_id` = `rm`.`hotel_id`)
      )
    )
  )
 
 WHERE `bk`.`group_id`=0 AND  `bk`.`cust_end_date_actual`='".$date."' AND  `bk`.`booking_status_id`=6 AND  `bk`.`hotel_id`='".$hotel_id."'
GROUP BY
  `bk`.`booking_id`
ORDER BY
  `bk`.`booking_id` DESC) 
 
 UNION
  
  (SELECT
  `bk`.`id` AS `booking_id`,
  `bk`.`id` AS `type`,
  `bk`.`guestID` AS `guest_id`,
  `bk`.`booking_source` AS `booking_source`,
  `bk`.`number_room` AS `room_id`,
  `bk`.`start_date` AS `cust_from_date_actual`,
  
  `bk`.`end_date` AS `cust_end_date_actual`,
  `bk`.`status` AS `booking_status_id`,
  `bk`.`charges_cost` AS `extra_service_total_amount`,
  `bk`.`charges_cost` AS `booking_extra_charge_amount`,
  `bk`.`charges_id` AS `booking_extra_charge_id`,
  `bk`.`charges_cost` AS `service_price`,
  `bk`.`hotel_id` AS `hotel_id`,
  `bk`.`bill_to_com` AS `bill_to_com`,
 SUM( `ch_line_item`.`rr_tot`) AS `rm_total`,
 SUM( `ch_line_item`.`rr_tot_tax`) AS `rr_tot_tax`,
  SUM(`ch_line_item`.`exrr`) AS `exrr`,
  SUM(`ch_line_item`.`exrr_tax`) AS `exrr_tax`,
  SUM(`ch_line_item`.`mp_tot`) AS `mp_tot`,
  SUM(`ch_line_item`.`mp_tax`) AS `mp_tax`,
 SUM( `ch_line_item`.`exmp`) AS `exmp`,
 SUM( `ch_line_item`.`exmp_tax`) AS `exmp_tax`
FROM
 `hotel_group` `bk`
     LEFT JOIN
     `booking_charge_line_item_view` `ch_line_item` ON
                   
                      `bk`.`id` = `ch_line_item`.`group_id`

 WHERE `bk`.`end_date`='".$date1."' AND `bk`.`status`=6 AND `bk`.`hotel_id`='".$hotel_id."'
GROUP BY
  `bk`.`id`
ORDER BY
  `bk`.`id` DESC)";
   
  $qry = $this->db1->query($sql); 
        if($qry->num_rows()>0){
            return $qry->result();
        }else{
            return false;
        }
	}	
	
function all_bookings_chkout_by_date($start_date,$end_date){
			
		$hotel_id=$this->session->userdata('user_hotel');
		$sql="(SELECT
  `bk`.`booking_id` AS `booking_id`,
  `bk`.`group_id` AS `type`,
  `bk`.`guest_id` AS `guest_id`,
  `bk`.`booking_source` AS `booking_source`,
  `bk`.`room_id` AS `room_id`,
  `bk`.`cust_from_date_actual` AS `cust_from_date_actual`,
  `bk`.`cust_end_date_actual` AS `cust_end_date_actual`,
  `bk`.`booking_status_id` AS `booking_status_id`,
  `bk`.`extra_service_total_amount` AS `extra_service_total_amount`,
  `bk`.`booking_extra_charge_amount` AS `booking_extra_charge_amount`,
  `bk`.`booking_extra_charge_id` AS `booking_extra_charge_id`,
  `bk`.`service_price` AS `service_price`,
  `bk`.`hotel_id` AS `hotel_id`,
  `bk`.`bill_to_com` AS `bill_to_com`,
  `ch_line_item`.`rr_tot` AS `rm_total`,
  `ch_line_item`.`rr_tot_tax` AS `rr_tot_tax`,
  `ch_line_item`.`exrr` AS `exrr`,
  `ch_line_item`.`exrr_tax` AS `exrr_tax`,
  `ch_line_item`.`mp_tot` AS `mp_tot`,
  `ch_line_item`.`mp_tax` AS `mp_tax`,
  `ch_line_item`.`exmp` AS `exmp`,
  `ch_line_item`.`exmp_tax` AS `exmp_tax`
FROM
  (
    (
      (
        (
          (
            (
              (
                (
                  (
                    `hotel_bookings` `bk`
                  LEFT JOIN
                    `hotel_transactions` `tran` ON(
                      (
                        (
                          `bk`.`booking_id` = `tran`.`t_booking_id`
                        ) AND(`tran`.`t_status` = 'Done')
                      )
                    )
                  )
                LEFT JOIN
                  `booking_charge_line_item_view` `ch_line_item` ON(
                    (
                      `bk`.`booking_id` = `ch_line_item`.`booking_id`
                    )
                  )
                )
              LEFT JOIN
                `hotel_guest` `gst` ON((`bk`.`guest_id` = `gst`.`g_id`))
              )
            LEFT JOIN
              `hotel_admin_details` `adm` ON((`bk`.`user_id` = `adm`.`admin_id`))
            )
          LEFT JOIN
            `booking_status_type` `stp` ON(
              (
                `bk`.`booking_status_id` = `stp`.`status_id`
              )
            )
          )
        LEFT JOIN
          `booking_status_type` `sts` ON(
            (
              `bk`.`booking_status_id_secondary` = `sts`.`status_id`
            )
          )
        )
      LEFT JOIN
        `hotel_broker` `ag` ON(
          (
            (`bk`.`broker_id` = `ag`.`b_id`) AND(`ag`.`hotel_id` = `bk`.`hotel_id`)
          )
        )
      )
    LEFT JOIN
      `hotel_channel` `ch` ON(
        (
          (`bk`.`channel_id` = `ch`.`channel_id`) AND(`ch`.`hotel_id` = `bk`.`hotel_id`)
        )
      )
    )
  LEFT JOIN
    `hotel_room` `rm` ON(
      (
        (`bk`.`room_id` = `rm`.`room_id`) AND(`bk`.`hotel_id` = `rm`.`hotel_id`)
      )
    )
  )
 
 WHERE `bk`.`group_id`=0 AND  `bk`.`cust_end_date_actual`>='".$start_date."' AND  `bk`.`cust_end_date_actual`<='".$end_date."' AND  `bk`.`booking_status_id`=6 AND `bk`.`hotel_id`='".$hotel_id."'
GROUP BY
  `bk`.`booking_id`
ORDER BY
  `bk`.`booking_id` DESC) 
 
 UNION
  
  (SELECT
  `bk`.`id` AS `booking_id`,
  `bk`.`id` AS `type`,
  `bk`.`guestID` AS `guest_id`,
  `bk`.`booking_source` AS `booking_source`,
  `bk`.`number_room` AS `room_id`,
  `bk`.`start_date` AS `cust_from_date_actual`,
  
  `bk`.`end_date` AS `cust_end_date_actual`,
  `bk`.`status` AS `booking_status_id`,
  `bk`.`charges_cost` AS `extra_service_total_amount`,
  `bk`.`charges_cost` AS `booking_extra_charge_amount`,
  `bk`.`charges_id` AS `booking_extra_charge_id`,
  `bk`.`charges_cost` AS `service_price`,
  `bk`.`hotel_id` AS `hotel_id`,
  `bk`.`bill_to_com` AS `bill_to_com`,
 SUM( `ch_line_item`.`rr_tot`) AS `rm_total`,
 SUM( `ch_line_item`.`rr_tot_tax`) AS `rr_tot_tax`,
  SUM(`ch_line_item`.`exrr`) AS `exrr`,
  SUM(`ch_line_item`.`exrr_tax`) AS `exrr_tax`,
  SUM(`ch_line_item`.`mp_tot`) AS `mp_tot`,
  SUM(`ch_line_item`.`mp_tax`) AS `mp_tax`,
 SUM( `ch_line_item`.`exmp`) AS `exmp`,
 SUM( `ch_line_item`.`exmp_tax`) AS `exmp_tax`
FROM
 `hotel_group` `bk`
     LEFT JOIN
     `booking_charge_line_item_view` `ch_line_item` ON
                   
                      `bk`.`id` = `ch_line_item`.`group_id`

 WHERE `bk`.`end_date`>='".date('m/d/Y',strtotime($start_date))."' AND `bk`.`end_date`<='".date('m/d/Y',strtotime($end_date))."' AND `bk`.`status`=6 AND `bk`.`hotel_id`='".$hotel_id."'
GROUP BY
  `bk`.`id`
ORDER BY
  `bk`.`id` DESC)";
   
  $qry = $this->db1->query($sql); 
        if($qry->num_rows()>0){
            return $qry->result();
        }else{
            return false;
        }
		
		
		
		
	}	
	
	
	
	function all_bookings_daily(){
		$hotel_id=$this->session->userdata('user_hotel');
        $this->db1->order_by('cust_from_date','desc');
		
		$this->db1->where('booking_status_id !=',7);
		$this->db1->where('cust_from_date_actual',date("Y-m-d"));
		$this->db1->where('hotel_id',$hotel_id);
	  //  $this->db1->where('group_id',0);
		//$this->db1->where('user_id',$this->session->userdata('user_id'));

        //$this->db1->limit(10);
        $query=$this->db1->get('booking_view');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	   function chk_gb($booking_id){
   		$this->db1->select('group_id');
		$this->db1->where('booking_id',$booking_id);
		$query=$this->db1->get('hotel_bookings');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }

    }
	function chk_last_bk(){
		$hotel_id=$this->session->userdata('user_hotel');
		$this->db1->select('count(*)');
		$this->db1->where('hotel_id',$hotel_id);
		$this->db1->where('booking_status_id!=',7);
		$query=$this->db1->get('hotel_bookings');
        
		 $cnt = $query->row_array();
		return $cnt['count(*)'];
		
		/*$this->db1->select('count(*)');
		$this->db1->where('hotel_id',$hotel_id);
		$query = $this->db1->get('hotel_bookings');
		$cnt = $query->row_array();
		return $cnt['count(*)'];*/
		
		
        }
	function chk_last_gb(){
		$hotel_id=$this->session->userdata('user_hotel');
		$this->db1->select('count(*)');
		$this->db1->where('hotel_id',$hotel_id);
		$this->db1->where('status!=',7);
		$query=$this->db1->get('hotel_group');
        
		 $cnt = $query->row_array();
		return $cnt['count(*)'];
	}
	
	function chk_last_sb(){
		$hotel_id=$this->session->userdata('user_hotel');
		$this->db1->select('count(*)');
		$this->db1->where('hotel_id',$hotel_id);
		$this->db1->where('status!=',7);
		$query=$this->db1->get('hotel_bookings');
        
		 $cnt = $query->row_array();
		return $cnt['count(*)'];
	}
	
	function chk_lasltransaction(){
		$hotel_id=$this->session->userdata('user_hotel');
		$this->db1->select('count(*)');
		$this->db1->where('hotel_id',$hotel_id);
		$query=$this->db1->get('hotel_transactions');
        
		 $cnt = $query->row_array();
		return $cnt['count(*)'];
	}
	function all_bookings_single_group(){
	  

	 $this->db1->select("id AS bid,name AS cname,booking_date AS bdate,booking_source AS bsource,start_date AS fdate,end_date AS tdate,id AS type");
    $this->db1->distinct();
    $this->db1->from("hotel_group");
	$this->db1->where("status_id!= ",7);
   $this->db1->where("date_format(booking_date,'%Y-%m-%d') =", date("Y-m-d"));
  
    $query1 = $this->db1->get_compiled_select(); // It resets the query just like a get()

    $this->db1->select("booking_id AS bid,cust_name AS cname,booking_taking_date AS bdate,booking_source AS bsource,cust_from_date_actual AS fdate,cust_end_date_actual AS tdate,group_id AS type");
    $this->db1->distinct();
    $this->db1->from("hotel_bookings");
    $this->db1->where("group_id",0);
	$this->db1->where("booking_status_id!= ",7);
	$this->db1->where("date_format(booking_taking_date,'%Y-%m-%d') =", date("Y-m-d"));
    $query2 = $this->db1->get_compiled_select(); 

    $query = $this->db1->query($query1." UNION ".$query2);

    return $query->result();	 
		 
		 
		 
  }

function all_login_reports(){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->order_by('hls_id','desc');
        $query=$this->db1->get('hotel_login_session');
		return $query->result();
}

function add_adjustment($data){
	$query=$this->db1->insert("hotel_adjustments",$data);
	if($query){
		return true;
	}
}

function all_adjst($bookingID){
	$this->db1->where('booking_id',$bookingID);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	$query=$this->db1->get('hotel_adjustments');
	if($query->num_rows()>0){
		return $query->result();
	}
}

function all_adjst_gp($id){
	$this->db1->where('group_id',$id);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	$query=$this->db1->get('hotel_adjustments');
	if($query->num_rows()>0){
		return $query->result();
	}
}

function get_adjuset_amt($id){
	$this->db1->where('booking_id',$id);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	$this->db1->select_sum('amount');
	$query=$this->db1->get('hotel_adjustments');
	if($query->num_rows()>0){
		return $query->row();
	}
}
function get_adjuset_amtAll($id){
	$this->db1->where('booking_id',$id);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	$this->db1->select('amount');
	$query=$this->db1->get('hotel_adjustments');
	if($query->num_rows()>0){
		return $query->result();
	}
}

function get_adjuset_amt_grp($id){
	$this->db1->where('group_id',$id);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	$this->db1->select_sum('amount');
	$query=$this->db1->get('hotel_adjustments');
	if($query->num_rows()>0){
		return $query->row();
	}
}

function get_adjuset_amt_grpAll($id){
	$this->db1->where('group_id',$id);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	$this->db1->select('amount');
	$query=$this->db1->get('hotel_adjustments');
	if($query->num_rows()>0){
		return $query->result();
	}
}

function delete_Adjst($id){
	
	$this->db1->where('id',$id);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	$query=$this->db1->delete('hotel_adjustments');
	if($query){
		return true;
	}
}

function transaction_report($start,$end){
	$sql="SELECT 
  DATE(tr.`t_date`) as date,
  COUNT(tr.`t_id`) as count,
  tr.`t_status`,  
  tr.`p_center`,
  COUNT(tr.`p_center`) AS p_c,
  ty.`hotel_transaction_type_name`,
  ty.`hotel_transaction_type_cat`,
  `t_payment_mode`,
  SUM(`t_amount`) as total
FROM
  `hotel_transactions` as tr
  LEFT JOIN `hotel_transaction_type` as ty ON
  tr.`transaction_type_id` = ty.`hotel_transaction_type_id`
  WHERE   DATE(tr.`t_date`) BETWEEN '".$start."' and '".$end."' AND tr.`t_status`='Done'AND tr.`hotel_id`='".$this->session->userdata('user_hotel')."'
GROUP BY 
  DATE(`t_date`)
ORDER BY
  `t_date` asc";
	$query=$this->db1->query($sql);
	if($query->num_rows()>0){
		return $query->result();
	}
}

function transaction_report1($start,$end){
	$sql="SELECT 
  DATE(tr.`t_date`) as date,
  COUNT(tr.`t_id`) as count,
  tr.`t_status`,  
  tr.`p_center`,
  COUNT(tr.`p_center`) AS p_c,
   tr.`transaction_type_id`,
  ty.`hotel_transaction_type_name`,
  ty.`hotel_transaction_type_cat`,
  `t_payment_mode`,
  SUM(`t_amount`) as total
FROM
  `hotel_transactions` as tr
  LEFT JOIN `hotel_transaction_type` as ty ON
  tr.`transaction_type_id` = ty.`hotel_transaction_type_id`
  WHERE   DATE(tr.`t_date`) BETWEEN '".$start."' and '".$end."' AND tr.`t_status`='Done' AND tr.`hotel_id`='".$this->session->userdata('user_hotel')."'
GROUP BY 
   `t_date`
ORDER BY
  `t_date` asc";
	$query=$this->db1->query($sql);
	if($query->num_rows()>0){
		return $query->result();
	}
}

function mode_count($date,$status){
	$sql="SELECT COUNT(*) AS md FROM `hotel_transactions` WHERE DATE(`t_date`)='".$date."' AND `hotel_id`='".$this->session->userdata('user_hotel')."' AND `t_payment_mode`='".$status."' AND `t_status`='Done'";
	$query=$this->db1->query($sql);
	if($query->num_rows()>0){
		return $query->row();
	}
	}
	
	function get_date_booking($id){
	$this->db1->where('booking_id',$id);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));	
	$query=$this->db1->get('hotel_bookings');
	if($query->num_rows()>0){
		return $query->row();
	}
}
	
	
	function update_checkin_status_stay_guest($booking_id,$stat)
		{
           // $current_date = $this->dashboard_model->get_date_booking($booking_id);
			date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
			
			 $data = array(
               'check_in_date'=> $d,
               'status'=> $stat
             );
			 $this->db1->where('booking_id', $booking_id);
			 $query=$this->db1->update(TABLE_PRE.'stay_guest',$data);
			 if($query) {
				 return true;
			 }
			 else {
				 return false;	
			 }
			
			 
		}
		function update_checkin_status_stay_guest_grp($group_id,$stat)
		{
           // $current_date = $this->dashboard_model->get_date_booking($booking_id);
			date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
			
			 $data = array(
               'check_in_date'=> $d,
               'status'=> $stat
             );
			 $this->db1->where('booking_id', $group_id);
			 $query=$this->db1->update(TABLE_PRE.'stay_guest',$data);
			 if($query) {
				 return true;
			 }
			 else {
				 return false;	
			 }
			
			
		}
		
		function update_checkout_status_stay_guest_grp($group_id,$stat)
		{
           // $current_date = $this->dashboard_model->get_date_booking($booking_id);
			date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
			
			 $data = array(
               'check_out_date'=> $d,
               'status'=> $stat
             );
			 $this->db1->where('group_id', $group_id);
			 $query=$this->db1->update(TABLE_PRE.'stay_guest',$data);
			 if($query) {
				 return true;
			 }
			 else {
				 return false;	
			 }
			
			
		}
		
		function update_checkout_status_stay_guest($booking_id,$stat)
		{
           // $current_date = $this->dashboard_model->get_date_booking($booking_id);
			date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
			
			 $data = array(
               'check_out_date'=> $d,
               'status'=> $stat
             );
			 $this->db1->where('booking_id', $booking_id);
			 $query=$this->db1->update(TABLE_PRE.'stay_guest',$data);
			 if($query) {
				 return true;
			 }
			 else {
				 return false;	
			 }
			
		}
		
		function update_cancel_status_stay_guest($booking_id,$stat)
		{
           // $current_date = $this->dashboard_model->get_date_booking($booking_id);
			date_default_timezone_set('Asia/Kolkata');
			$d=date('Y-m-d H:i:s');
		
			 $data = array(
               'isCancelled'=> 1,
               'status'=> $stat
             );
			 $this->db1->where('booking_id', $booking_id);
			 $query=$this->db1->update(TABLE_PRE.'stay_guest',$data);
			 if($query) {
				 return true;
			 }
			 else {
				 return false;	
			 }
			
		}
	
	function getActiceLogins(){
		
		$sql="	SELECT
				  MAX(hls_id) as max, COUNT(*) as counter
				FROM
				  `hotel_login_session`
				WHERE
				  `user_id` = ".$this->session->userdata('user_id')." AND `hotel_id` = ".$this->session->userdata('user_hotel')." AND `status` = 'Success'
				ORDER BY
				  `hls_id` desc";
		$query=$this->db1->query($sql);
		
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
	}
	
	function get_lastPay(){
		$this->db1->select('	hotel_payment_id','desc');
		//$this->db1->limit(1);
			 $query=$this->db1->get(TABLE_PRE.'payments');
			 if($query) {
				 return $query->num_rows();
			 }
			 else {
				 return false;	
			 }
	}	

	
	function all_fuel_stock_itm(){

		$sql='SELECT *
			FROM ((SELECT DISTINCT(`product`) as item ,line_items_id AS id,price AS u_price
			FROM `hotel_line_item_details`
			WHERE hotel_line_item_details.hotel_id="'.$this->session->userdata('user_hotel').'" ORDER BY product)
			UNION
			(SELECT DISTINCT(`a_name`) as item , hotel_stock_inventory_id AS id,u_price AS u_price
			FROM `hotel_stock_inventory`
			WHERE  hotel_stock_inventory.hotel_id="'.$this->session->userdata('user_hotel').'" ORDER BY a_name)
			UNION
			(SELECT DISTINCT(`fuel_name`) as item,id AS id,fuel_name AS u_price
			FROM `hotel_master_fuel_stock`
			WHERE  hotel_master_fuel_stock.hotel_id="'.$this->session->userdata('user_hotel').'" ORDER BY `fuel_name`)) as data
			GROUP BY data.item';
		
		$query=$this->db1->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();	 
		}
	}
	
function getDetailsBk($id){
	$this->db1->where('booking_id',$id);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));	
	$query=$this->db1->get('hotel_bookingbk');
	if($query->num_rows()>0){
		return $query->row();
}	
}	

function changebkdetalis($id,$data){
	$this->db1->where('booking_id',$id);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));	
	$query=$this->db1->update('hotel_bookings',$data);
	if($query){
		return true;
	}
}

function add_checkin_outData($data){	
	$this->db1->insert('hotel_bookingbk',$data);
} 



function getDetailsGB($id){
	$this->db1->where('group_id',$id);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));	
	$query=$this->db1->get('hotel_bookingbk');
	if($query->num_rows()>0){
		return $query->result();
}	
}

function change_GBstatus($id,$data){
	$data1=array(
	'status'=>$data['booking_status_id']
	);
	
	$this->db1->where('id',$id);
	$query=$this->db1->update('hotel_group',$data1);
	if($query){
		return true;
	}
}
function total_hotels_hotel_id()
    {   
        $this->db1->where('admin_id',$this->session->userdata('user_id'));
        $query=$this->db1->get(TABLE_PRE.'admin_details');
        //echo $this->db1->last_query();exit;
        if($query->num_rows()>0){
			return $query->result();
        }
        else
        {
            return false;
        }
    }
	function hotel_information($id){
		$this->db1->where('hotel_id',$id);
		$query = $this->db1->get(TABLE_PRE.'hotel_details');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}
	}
	

function get_printer($hotel_id){
		$this->db1->select('printer_settings');
		$this->db1->where('hotel_id',$hotel_id);
		$query = $this->db1->get(TABLE_PRE.'settings');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}
	}
	function get_template_settings($id){
			$this->db1->where('hotel_id',$id);
			$query = $this->db1->get('mail_settings');
				 if($query){
					 return $query->row();
				 }
				 else{
					 return false;
				 }
			}
			
			
	function tax_rule_extraCharge($hotel_id){
	    	$this->db1->where('hotel_id',$hotel_id);
	    	$this->db1->where('tc_type','Extra Charge');
			$query=$this->db1->get('hotel_tax_category');
		if($query->num_rows()>0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	
	function get_Tsettings($hotel_id){
	    	$this->db1->where('hotel_id',$hotel_id);
		$query = $this->db1->get('general_setting');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}
	}
	
	function getIndianCurrency(float $number)
	{
		$decimal = round($number - ($no = floor($number)), 2) * 100;
		$hundred = null;
		$digits_length = strlen($no);
		$i = 0;
		$str = array();
		$words = array(0 => '', 1 => 'one', 2 => 'two',
			3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
			7 => 'seven', 8 => 'eight', 9 => 'nine',
			10 => 'ten', 11 => 'eleven', 12 => 'twelve',
			13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
			16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
			19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
			40 => 'forty', 50 => 'fifty', 60 => 'sixty',
			70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
		$digits = array('', 'hundred','thousand','lakh', 'crore');
		while( $i < $digits_length ) {
			$divider = ($i == 2) ? 10 : 100;
			$number = floor($no % $divider);
			$no = floor($no / $divider);
			$i += $divider == 10 ? 1 : 2;
			if ($number) {
				$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
				$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
				$str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
			} else $str[] = null;
		}
		$Rupees = implode('', array_reverse($str));
		$paise = ($decimal) ? " & " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
		return ($Rupees ? 'Rupees '.$Rupees  : '') . $paise;
	}
	
	function get_adhoc_bill_master($id){
		
		if($id == '' || $id== null){
			$sql = "SELECT
						*
					FROM
						`adhoc_bill_master`
					WHERE
						`hotel_id` = ".$this->session->userdata('user_hotel')."
					ORDER BY
						`addedOn` desc";
			
			$query=$this->db1->query($sql);
			if($query){
				return $query->result();
			}
			else{
				return false;
			}
		}
		else{
			
				$sql = "SELECT
						*
					FROM
						`adhoc_bill_master`
					WHERE
						`hotel_id` = ".$this->session->userdata('user_hotel')."
						and `ahb_id` =".$id."
					ORDER BY
						`addedOn` desc";
			
			$query=$this->db1->query($sql);
			if($query){
				return $query->result();
			}
			else{
				return false;
			}
			
		}
	}
	
	
	
	function get_adhoc_bill_line(){
		
		$sql = "SELECT
					*
				FROM
					`adhoc_bill_master`
				WHERE
					`hotel_id` = ".$this->session->userdata('user_hotel')."
				ORDER BY
					`addedOn` desc";
		
		$query=$this->db1->query($sql);
        if($query){
            return $query->result();
        }
        else{
            return false;
        }
	}
	
	function get_adhoc_bill_master_byID($id){
		
		$sql = "SELECT
					*
				FROM
					`adhoc_bill_master`
				WHERE
					`hotel_id` = ".$this->session->userdata('user_hotel')." AND
					`ahb_id` = ".$id."
				ORDER BY
					`addedOn` desc";
		
		$query=$this->db1->query($sql);
        if($query){
            return $query->result();
        }
        else{
            return false;
        }
	}
	
	function get_adhoc_bill_line_byID($id){
		
		$sql = "SELECT
					*
				FROM
					`adhoc_bill_line`
				WHERE
					`hotel_id` = ".$this->session->userdata('user_hotel')." AND
					`ahb_id` = ".$id."
				ORDER BY
					`added_on` asc";
		
		$query=$this->db1->query($sql);
        if($query){
            return $query->result();
        }
        else{
            return false;
        }
	}
	
	function add_adhoc_bill($dataAhb,$dataAhbLine) {
		
		/*	echo "<pre>";
			print_r($dataAhb);
			echo "</pre>";
			exit;
		*/

		if(isset($dataAhb)){
			$queryM = $this->db1->insert('adhoc_bill_master',$dataAhb);
			$insert_idM = $this->db1->insert_id();
		}
		
		//echo $insert_idM;
		//exit;

		/*echo 'bonjour mui model da la add_adhoc_bill';
		print_r($dataAhb);
		print_r($dataAhbLine);*/

		foreach($dataAhbLine as $index => $val){

					$arrAbhLine = array(
						'ahb_id' => $insert_idM,
						'hotel_id' => $dataAhbLine[$index]['hotel_id'],
						'name' => $dataAhbLine[$index]['name'],
						'hsn_sac' => $dataAhbLine[$index]['hsn_sac'],
						'qty' => $dataAhbLine[$index]['qty'],
						'rate' => $dataAhbLine[$index]['rate'],
						'sgst' => $dataAhbLine[$index]['sgst'],
						'cgst' => $dataAhbLine[$index]['cgst'],
						'igst' => $dataAhbLine[$index]['igst'],
						'utgst' => $dataAhbLine[$index]['utgst'],
						'total_tax' => $dataAhbLine[$index]['total_tax'],
						'disc' => $dataAhbLine[$index]['disc'],
						'adjustment' => $dataAhbLine[$index]['adjustment'],
						'total' => $dataAhbLine[$index]['total'],
						'note' => $dataAhbLine[$index]['note'],
						'added_on' => $dataAhbLine[$index]['added_on'],
					);					
					
					$query=$this->db1->insert('adhoc_bill_line',$arrAbhLine);
			
		} //End foreach

		//print_r($arrAbhLine);
		//exit;

        if($query){
            return $insert_idM;
        }else{
            return false;
        }
    }
    
    
    
    
    	function edit_adhoc_bill($dataAhb,$dataAhbLine,$adhoc_payment,$ahb_id) {
		
		/*	echo "<pre>";
			print_r($dataAhb);
			echo "</pre>";
			echo "******";
			echo "<pre>";
			print_r($dataAhbLine);
			echo "</pre>";
			exit;
		*/
        $insert_idM = '';

		if(isset($dataAhb)){
		 //   $insert_idM = $dataAhb['ahb_id'];
		    $this->db1->where('ahb_id',$ahb_id);
			$queryM = $this->db1->update('adhoc_bill_master',$dataAhb);
			if($queryM){
			    
			    
			    	foreach($dataAhbLine as $index => $val){

					$arrAbhLine = array(
						'ahb_id' => $dataAhbLine[$index]['ahb_id'],
						'hotel_id' => $dataAhbLine[$index]['hotel_id'],
						'name' => $dataAhbLine[$index]['name'],
						'hsn_sac' => $dataAhbLine[$index]['hsn_sac'],
						'qty' => $dataAhbLine[$index]['qty'],
						'rate' => $dataAhbLine[$index]['rate'],
						'sgst' => $dataAhbLine[$index]['sgst'],
						'cgst' => $dataAhbLine[$index]['cgst'],
						'igst' => $dataAhbLine[$index]['igst'],
						'utgst' => $dataAhbLine[$index]['utgst'],
						'total_tax' => $dataAhbLine[$index]['total_tax'],
						'disc' => $dataAhbLine[$index]['disc'],
						'adjustment' => $dataAhbLine[$index]['adjustment'],
						'total' => $dataAhbLine[$index]['total'],
						'note' => $dataAhbLine[$index]['note'],
						'added_on' => $dataAhbLine[$index]['added_on'],
					);					
					
					$query=$this->db1->insert('adhoc_bill_line',$arrAbhLine);
			
		} //End foreach

			}//inner if
		}
		
	

        if($queryM){
           // return $insert_idM;
           if($adhoc_payment['t_payment_mode']!='' && $adhoc_payment['t_amount']!=''){
               
               
               $adhoc_payment['details_id'] = $ahb_id;
               $this->dashboard_model->add_adhoc_bill_tran($adhoc_payment);
                return $ahb_id;
           }
           
            return $ahb_id;
        }else{
            return false;
        }
    }
	
	function add_adhoc_bill_tran($adhoc_payment) {
		
		if(isset($adhoc_payment)){
			$queryT = $this->db1->insert('hotel_transactions',$adhoc_payment);
			$insert_idT = $this->db1->insert_id();
		}
	}
	
	function get_booking_number($id,$type){
	      if($type=='sb'){
	          $this->db1->where('booking_id',$id);
	      }elseif($type=='gb'){
	          $this->db1->where('group_id',$id);
	   }
	   	
	   	$query=$this->db1->get('booking_number');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}
	}
	function get_invoice_number($id,$type){
	      if($type=='sb'){
	          $this->db1->where('booking_id',$id);
	      }elseif($type=='gb'){
	          $this->db1->where('group_id',$id);
	   }
	   	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	   	$query=$this->db1->get('invoice_master_number');
	   	//echo $this->db1->last_query();exit;
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}
	}
	function paybillBycmp($b_type,$b_id){
	    
	    $this->db1->where('booking_type',$b_type);
	    $this->db1->where('booking_id',$b_id);
	    $query = $this->db1->get('checked_out_on_credit');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}
	}
	
	function get_single_under_group($grp_id){
	    
	   $this->db1->select('*');
	   $this->db1->where('group_id',$grp_id);
	   $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	   $query = $this->db1->get('hotel_bookings');
	   if($query->num_rows()>0){
	       
	       return $query->result();
	   }
	    
	}
	
	function allAdPayments(){
	    $this->db1->select('*');
	 $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	   $query = $this->db1->get('hotel_advance_bk_payment');
	   if($query->num_rows()>0){
	       
	       return $query->result();
	   }
	}

} // End Dashboard Model (c) TheIT

?>