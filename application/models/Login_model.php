<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {
    
	/* Login Authorization */
	
	/*function __construct(){
		
		parent::__construct();
		$CI = &get_instance();
		$this->db1 = $CI->load->database('superadmin', TRUE);
	}*/
	
    function login($data){
        $query=$this->db1->get_where(TABLE_PRE.'login',$data);
        if($query->num_rows()==1){
            return $query->row();
            
        }else{ 
            return false;
        }
    }
	
	/* User Type Fetch */
	function login_user_type($data){
		$this->db1->where('user_type_id',$data);
		$query=$this->db1->get(TABLE_PRE.'user_type');
        if($query->num_rows()==1){
            return $query->row();
        }else{ 
            return false;
        }
	}
	
	/* Fetch User Info */
    function get_user_info($id){
		$this->db1->get_where('login_id',$id);
        $query=$this->db1->get(TABLE_PRE.'login');
        if($query->num_rows()==1){
            return $query->row();
        }else{ 
            return false;
        }
    }
	
	/* Admin Activate in Login Table */
	function activate_admin($status){
		$this->db1->where('user_id',$status['login_id']);
        $query=$this->db1->update(TABLE_PRE.'login',$status);
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
	/* Admin Activate in Admin Table */
	function activate_admin_status($admin){
		$this->db1->where('admin_id',$admin['admin_id']);
        $query=$this->db->update(TABLE_PRE.'admin_details',$admin);
        if($query){
            return true;
        }else{
            return false;
        }
	}

    /*function get_permission($id){
        $this->db->where('user_id',$id);
        $query=$this->db->get(TABLE_PRE.'user_permission');
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
    }*/

     function get_permission($id){
      //  $this->db->where('user_id',$id);
      /*  $qry=$this->db->get(TABLE_PRE.'login');
		$bt = $qry->row();
        $admin_role_id = $bt->admin_role_id; */
   //    echo $this->db->database;
	//	echo $admin_role_id;
		
        $this->db1->where('user_permission_id',$id);
        $query=$this->db1->get(TABLE_PRE.'user_permission');
        // echo $this->db1->last_query();
         //exit;
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
    }




	
	
    function online_insert($data){
        $query=$this->db1->insert(TABLE_PRE.'online',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
    function  check_shift($user_id,$date){

        $sql="select * from shift_log LEFT JOIN hotel_shift on shift_log.shift_id=hotel_shift.shift_id where shift_log.user_id='".$user_id."' and shift_log.date='".$date."' and shift_log.logged_out !='1'";
        $query=$this->db1->query($sql);

        if($query->num_rows()==1){
            return $query->result();
        }else{
            return false;
        }

    }
	
	//INSERT login_session TABLE 
	function login_session($data){
        $query=$this->db1->insert(TABLE_PRE.'login_session',$data);
        if($query){
            return $this->db1->insert_id();
        }else{
            return false;
        }
    }
	
	//UPDATE login_session TABLE 
	function update_login_session($data,$id){
		$this->db1->where('user_id',$data['user_id']);
		$this->db1->where('hls_id',$id);
        $query=$this->db1->update(TABLE_PRE.'login_session',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
	 function userInfo($data){
        
		
		$this->db->where('user_name',$data);
        $query=$this->db->get(TABLE_PRE.'login');
        if($query->num_rows()==1){
            return $query->row();            
        }else{ 
            return false;
        }
    }
	
	
	function update_login_session1($data,$id,$hotel_id){
		$this->db1->where('hls_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->update(TABLE_PRE.'login_session',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
	
	
}

?>