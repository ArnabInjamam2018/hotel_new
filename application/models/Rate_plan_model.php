<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Rate_plan_model extends CI_Model {
  
 /* function __construct(){
		
		parent::__construct();
		$CI = &get_instance();
		$this->db1 = $CI->load->database('superadmin', TRUE);
	}*/
 	function all_rate_plan(){
	 $this->db1->select('*');
	// $this->db1->where('user_id',$this->session->userdata('user_id'));
     $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	 $this->db1->limit(1000);
     $query=$this->db1->get('rate_plan');
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
	
	}
	
	function rate_plan_populate_tracking($hotel_id){
		
		$this->db1->select('*');
		$this->db1->where('hotel_id',$hotel_id);
		$this->db1->from('hotel_rateplan_index');
		$query = $this->db1->get();
		if($query){

            return $query->row();
        }
        else{

            return false;
        }
	
	}
	
	function add_rate_plan()
	{
		
		
		$plan_rate_type = $this->rate_plan_model->all_rate_plan_type();
		$booking_source = $this->rate_plan_model->all_booking_source_type();
		$all_unit_type = $this->rate_plan_model->all_units1();
		$all_meal_plan = $this->rate_plan_model->all_meal_plan();
		$occupancy = $this->rate_plan_model->all_occupancy();
		
		/*echo "<pre>";
		print_r($plan_rate_type);
		echo "<pre>";
		print_r($booking_source);
		echo "<pre>";
		print_r($all_unit_type);
		echo "<pre>";
		print_r($all_meal_plan);
		echo "<pre>";
		print_r($occupancy);*/
		
		foreach($plan_rate_type as $rate_type){
			foreach($booking_source as $bs){
				foreach($all_unit_type as $u_type){
					foreach($all_meal_plan as $m_c){
						foreach($occupancy as $oc){
							
							$data=array(
							'rate_plan_type_id'=>$rate_type->rate_plan_type_id,	
							'rate_plan_type_name'=>$rate_type->name,
							'source_type_id'=>$bs->bst_id,	
							'source_type_name'=>$bs->bst_name,								
							'unit_type_id'=>$u_type->id,	
							'unit_type_name'=>$u_type->unit_name,						
							'meal_plan_id'=>$m_c->hotel_meal_plan_cat_id,
							'meal_plan'=>$m_c->name,
							'occupancy_type'=>$oc->name,
							'occupancy_id'=>$oc->hotel_occupancy_id,
							//'user_id'=>$this->session->userdata('user_id'),
							'hotel_id'=>$this->session->userdata('user_hotel')
							);
							
							
			              $this->db1->insert('rate_plan',$data);
						}
							/*echo "<pre>";
							print_r($data); 
							exit;*/
					}
				}
			}
		}	
		
		$data=array(
		'hotel_id'=>$this->session->userdata('user_hotel'),
		'user_id'=>$this->session->userdata('user_id'),
		'populate_status'=>'1',
		'chain_id'=>'3'
		);
		$hotel_id = $this->session->userdata('user_hotel');
		$this->db1->where('hotel_id',$hotel_id);		
		$query1=$this->db1->update('hotel_rateplan_index',$data);
		if($query1){
		return true;
		}
		else{
			return false;
	}
			
			
			
	}
	function edit_rate_plan()
	{
		/*$hotel_id = $this->session->userdata('user_hotel');
		$this->db1->where('hotel_id',$hotel_id);
		$this->db1->truncate('rate_plan');*/
		$plan_rate_type = $this->rate_plan_model->all_rate_plan_type();
		$booking_source = $this->rate_plan_model->all_booking_source_type();
		$all_unit_type = $this->rate_plan_model->all_units1();
		$all_meal_plan = $this->rate_plan_model->all_meal_plan();
		$occupancy = $this->rate_plan_model->all_occupancy();
		
		/*echo "<pre>";
		print_r($plan_rate_type);
		echo "<pre>";
		print_r($booking_source);
		echo "<pre>";
		print_r($all_unit_type);
		echo "<pre>";
		print_r($all_meal_plan);
		echo "<pre>";
		print_r($occupancy);*/
		
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));	
		$this->db1->delete('rate_plan');
		
		foreach($plan_rate_type as $rate_type){
			foreach($booking_source as $bs){
				foreach($all_unit_type as $u_type){
					foreach($all_meal_plan as $m_c){
						foreach($occupancy as $oc){
							
							$data=array(
							'rate_plan_type_id'=>$rate_type->rate_plan_type_id,	
							'rate_plan_type_name'=>$rate_type->name,
							'source_type_id'=>$bs->bst_id,	
							'source_type_name'=>$bs->bst_name,								
							'unit_type_id'=>$u_type->id,	
							'unit_type_name'=>$u_type->unit_name,						
							'meal_plan_id'=>$m_c->hotel_meal_plan_cat_id,
							'meal_plan'=>$m_c->name,
							'occupancy_type'=>$oc->name,
							'occupancy_id'=>$oc->hotel_occupancy_id,
							//'user_id'=>$this->session->userdata('user_id'),
							'hotel_id'=>$this->session->userdata('user_hotel')
							);
							
							$this->db1->insert('rate_plan',$data);
						}
							/*echo "<pre>";
							print_r($data); 
							exit;*/
					}
				}
			}
		}	

							//echo "<pre>";
							//print_r($data); 
							//exit;
			
	}
	
	function all_rate_plan_type(){
		$this->db1->select('*');
      $this->db1->from(TABLE_PRE.'rate_plan_type');
	//  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
       $query=$this->db1->get();//echo "<pre>";//print_r($query);exit;
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	
	function all_booking_source(){
		$this->db1->select('*');
       $query=$this->db1->get(TABLE_PRE.'booking_source');
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	function all_booking_source_type(){
		$this->db1->select('*');
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
      //  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get('booking_source_type');
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	function all_meal_plan(){
		$this->db1->select('*');
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
     //  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
       $query=$this->db1->get(TABLE_PRE.'meal_plan_cat');
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	function all_units1(){
         //$this->db1->where('id',$unit_id);
         //$this->db1->order_by('unit_id', 'ASC');
		// $this->db1->where('user_id',$this->session->userdata('user_id'));
         $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
         $query = $this->db1->get(TABLE_PRE.'unit_type');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }
	 
	 function all_units2($id){
         $this->db1->where('id',$id);         
         $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
         $query = $this->db1->get(TABLE_PRE.'unit_type');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }
	function all_occupancy(){
		$this->db1->select('*');
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
       // $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->get(TABLE_PRE.'occupancy');
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
 function get_rate_plan($p_id,$s_id){
	 //$p_id = 1;
	 //$s_id = 1;
	 
	 $sql="
		SELECT ty.unit_name,rp.*
		FROM `rate_plan` as rp
        LEFT JOIN hotel_unit_type as ty ON ty.id = rp.`unit_type_id`
		WHERE rp.`hotel_id` = ".$this->session->userdata('user_hotel')." AND rp.`rate_plan_type_id` = ".$p_id." AND rp.`source_type_id` = ".$s_id."
		";
			$query=$this->db1->query($sql);
		 //$query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
		}	 
 }	
	
function update_rate_plan($r_id,$data){
	
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));	
		$this->db1->where('rate_plan_id=',$r_id);
	    $query=$this->db1->update('rate_plan',$data);
		if($query){
            return true;
        }else{
            return false;
        }
}

function update_r_plan($u_id,$m_id,$o_id,$data){
		$this->db1->where('unit_type_id=',$u_id);
		$this->db1->where('meal_plan_id=',$m_id);
		$this->db1->where('occupancy_id=',$o_id);
		//$this->db1->where('rate_plan_type_id=',$se_id);
		//$this->db1->where('source_type_id=',$so_id);
	    $query=$this->db1->update('rate_plan',$data);
		if($query){
            return true;
        }else{
            return false;
        }
}

function rate_plan_setting_view($data,$maping){

    	$query=$this->db1->insert('rate_plan_setting',$data);

    	foreach($maping['season_name'] as $key => $val){

				$name=$maping['season_name'][$key];
				$start=$maping['start'][$key];
				$end=$maping['end'][$key];

					$start_date= date("y-m-d",strtotime($start));
                	$end_date= date("y-m-d",strtotime($end));

			 $data=array(
                                'season_name'=>$name,
                                'start_date'=>$start_date,
                                'end_date'=>$end_date,
                                'hotel_id'=>$this->session->userdata('user_hotel'),
								'user_id'=>$this->session->userdata('user_id')
										
                         );
				
				$query1=$this->db1->insert('season_mapping',$data);

			}

        if($query){

            return true;
        }
        else{

          return false;
        }
    	     
    }



function get_season_by_id($id){
 
        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'rate_plan_type');
        $this->db1->where('rate_plan_type_id',$id);
        $query=$this->db1->get();
        if($query){

            return $query->row();
        }
        else{

            return false;
        }


    }

    	function all_season_mapping(){
		$this->db1->select('*');
		$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
       $query=$this->db1->get('season_mapping');
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	function delete_season_mapping_by_id($id){
		
		$this->db1->where('season_mapping_id',$id);
              $query=$this->db1->delete('season_mapping');
             
	}

	function add_season_mapping($data){

    	$query=$this->db1->insert('season_mapping',$data);

        if($query){

            return true;
        }
        else{

          return false;
        }
    	     
    }
	
	
	function update_room_rent_tax($data){

           $h_id=$this->session->userdata('user_hotel');
              $this->db1->where('hotel_id',$h_id);
              $query=$this->db1->update('rate_plan_setting',$data);

              if($query){

                return true;
              }
   }

   function update_meal_plan_tax($data){

           $h_id=$this->session->userdata('user_hotel');
              $this->db1->where('hotel_id',$h_id);
              $query=$this->db1->update('rate_plan_setting',$data);

              if($query){

                return true;
              }
   }

  function get_plan_setting_by_h_id(){
	  $h_id=$this->session->userdata('user_hotel');
	 $this->db1->where('hotel_id',$h_id);
      $query=$this->db1->get('rate_plan_setting');

              if($query){

                return $query->result();
              }
  }	  

  
   function  get_all_rate_plan(){
	 $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
//	 $this->db1->order_by('unit_type_id', 'ASC');
     $this->db1->order_by('rate_plan_id', 'ASC');
      $query=$this->db1->get('rate_plan_summary');
        if($query){
            return $query->result();
        }
   }
  
   function default_season($data){
	  $h_id=$this->session->userdata('user_hotel');
	 $this->db1->where('hotel_id',$h_id);
      $query=$this->db1->update('rate_plan_setting',$data);

              if($query){

                return $query->result();
              }
  }
  
   function currentDate(){
	$h_id=$this->session->userdata('user_hotel');
	 $this->db1->where('hotel_id',$h_id);
	 $query=$this->db1->get('season_mapping');
	 if($query){
		 return $query->result();
	 }
	 
 }


function toggle_applied_tax($data){
	
	  $h_id=$this->session->userdata('user_hotel');
	 $this->db1->where('hotel_id',$h_id);
      $query=$this->db1->update('rate_plan_setting',$data);

              if($query){

                return true;
              }
  }

function get_applied_tax(){
	
	
     $h_id=$this->session->userdata('user_hotel');
	 $this->db1->where('hotel_id',$h_id);
	 $query=$this->db1->get('rate_plan_setting');
		if($query){

                return $query->row();
              }
}  
  
  function get_season($strt_date){
	
		//$sql='SELECT * FROM `season_mapping` WHERE '.$strt_date.' BETWEEN start_date and end_date ORDER by season_mapping_id limit 0,1';
		//$query1=$this->db1->query($sql);
		// return $query1->row();$u->count();
		$this->db1->select('*');
		$this->db1->where('start_date <=',$strt_date);
		$this->db1->where('end_date >=',$strt_date);
		$this->db1->limit(1);
		$query1=$this->db1->get('season_mapping');
		
		if($query1->num_rows()>0){
			return $query1->row();
		}
		else{
			
			return false;
		}
		
	  
  }
  
  function get_season_id($data){
	   $this->db1->select('rate_plan_type_id');
	   $this->db1->where('name',$data);
	 $query=$this->db1->get('hotel_rate_plan_type');
		if($query){

                return $query->row();
              }
  }
  
  
  function get_unit_type_id($room_id){
	  $this->db1->select('*');
	  $this->db1->where('room_id',$room_id);
	    $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	   $query=$this->db1->get('hotel_room');
		if($query){

                return $query->row();
              }
  }
   function get_unit_type_id1($room_id){
	  $this->db1->select('*');
	  $this->db1->where('room_no',$room_id);
	   $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	   $query=$this->db1->get('hotel_room');
		if($query){
				//print_r($query->row());
				//exit();
                return $query->row();
              }
		else{
			
			return "NO ID";
		}
  }    function get_unit_type_id_rm_id($room_id){
	  $this->db1->select('*');
	  $this->db1->where('room_id',$room_id);
	   $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	   $query=$this->db1->get('hotel_room');
		if($query){
				//print_r($query->row());
				//exit();
                return $query->row();
              }
		else{
			
			return "NO ID";
		}
  }
  
  
  function get_meal_plan($bc,$s_id,$o_id,$u_id){
	  $this->db1->where('rate_plan_type_id',$s_id);
	  $this->db1->where('source_type_id',$bc);
	  $this->db1->where('unit_type_id',$u_id);
	  $this->db1->where('occupancy_id',$o_id);
	   $query=$this->db1->get('rate_plan');
		if($query){

                return $query->result();
              } 
  }
  
  function get_rmCh($plan,$source,$season,$unitType,$pax){
	  $this->db1->where('meal_plan_id',$plan);
	  $this->db1->where('source_type_id',$source);
	  $this->db1->where('rate_plan_type_id',$season);
	  $this->db1->where('unit_type_id',$unitType);
	  $this->db1->where('occupancy_id',$pax);
	  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	  $query=$this->db1->get('rate_plan');
		if($query){
			//print_r($query);
			//exit;
			return $query->row();
		} 
		else{
			
			return "wrong";
		}
  }// End get_rmCh
  
  function get_exACh($plan,$source,$season,$unitType){
	  $this->db1->where('meal_plan_id',$plan);
	  $this->db1->where('source_type_id',$source);
	  $this->db1->where('rate_plan_type_id',$season);
	  $this->db1->where('unit_type_id',$unitType);
	  $this->db1->where('occupancy_id',5);
	  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	  $query=$this->db1->get('rate_plan');
		if($query){
			return $query->row();
		} 
  }// End get_exCh _sb dt17_11_16
  
  function get_exCCh($plan,$source,$season,$unitType){
	  $this->db1->where('meal_plan_id',$plan);
	  $this->db1->where('source_type_id',$source);
	  $this->db1->where('rate_plan_type_id',$season);
	  $this->db1->where('unit_type_id',$unitType);
	  $this->db1->where('occupancy_id',6);
	  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	  $query=$this->db1->get('rate_plan');
		if($query){
			return $query->row();
		} 
  }// End get_exCh _sb dt17_11_16
  
  
  function fetch_food_plan_name($food_plans){
	  $this->db1->select('name');
	  $this->db1->where('hotel_meal_plan_cat_id',$food_plans);
	  $query=$this->db1->get('hotel_meal_plan_cat');
		if($query){
			return $query->row();
		}  
  }
  
  
  function check_season_by_condition($season,$start){
	  
	 // $sql ="select * from `season_mapping` where  $start between `start_date` and `end_date`";
	  $this->db1->select('*');
	//$this->db1->where('season_name',$season);
	 $this->db1->where('start_date <=',$start);
	$this->db1->where('end_date >=',$start);
	$this->db1->where('user_id',$this->session->userdata('user_id'));
    $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	  $query=$this->db1->get('season_mapping');
	// $query=$this->db1->query($sql);
		if($query->num_rows()>0){
			return "true";
		}
else{		
	  //return $sql;
  }
  
  }
  
  function update_laundry_rate($data,$id){
		$this->db1->where('lr_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));		
	    $query=$this->db1->update('hotel_laundry_rates',$data);
		if($query){
            return true;
        }else{
            return false;
        }
}

function get_laundry_rates($cloth,$fabric,$service){
		$this->db1->select($service);
		$this->db1->where('lr_cloth_type_id',$cloth);
		$this->db1->where('lr_fabric_type_id',$fabric);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));		
	    $query=$this->db1->get('hotel_laundry_rates');
		if($query){
            return $query->row();
        }else{
            return false;
        }
}
  
  }// End Model Class	
	

?>