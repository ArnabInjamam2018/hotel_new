<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Reports_model extends CI_Model {
  
 	function booking_view(){
		$this->db1->select('*');
		$query=$this->db1->get('booking_view');
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}
	
	function admin_transaction_summary(){
		
		$sql = "SELECT 
					admin_name, date(t_date) AS t_date, hotel_id, hotel_transaction_type_name, hotel_transaction_type_cat, p_mode_des,SUM(t_amount) AS amt
				FROM `transactionview_2` where hotel_id=".$this->session->userdata('user_hotel')."
				GROUP BY 
					admin_name, date(t_date), hotel_id, hotel_transaction_type_cat, hotel_transaction_type_name, p_mode_des 
				ORDER BY 
					date(t_date) DESC, hotel_transaction_type_cat, SUM(t_amount) DESC";
		
		$query=$this->db1->query($sql);
        if($query){
            return $query->result();
        }
        else{
            return false;
        }
	}
	
	function getCheckoutSummary(){
		
		$sql = "SELECT
				  `checkout_date` as date,
				  COUNT(`booking_id`) as roomsCheckout,
				  SUM(`no_of_guest`) as guestCheckout,
				  SUM(case when `group_id` <> 0 then 1 else 0 end) as groupsCheckout
				FROM
				  `hotel_bookings`
				WHERE
				  `checkout_date` IS NOT NULL AND hotel_id = ".$this->session->userdata('user_hotel')."
				GROUP BY `checkout_date`
				ORDER BY `checkout_date` DESC";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getCheckoutExp($date){
		
		$sql = "SELECT 
				  COUNT(*) AS count,
				  SUM(
					CASE WHEN `booking_status_id` <> 6 AND `booking_status_id` <> 5 THEN 1 ELSE 0 END
				  ) AS countPending,
				SUM(
					CASE WHEN `booking_status_id` = 5 THEN 1 ELSE 0 END
				  ) AS countCheckin
				FROM
				  `hotel_bookings`
				WHERE
				  hotel_id = ".$this->session->userdata('user_hotel')." AND DATE(cust_end_date) = '".$date."' AND DATE(cust_from_date) <> '".$date."' AND booking_status_id <> 7 AND booking_status_id <> 8";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}

	function getCheckinSummary(){
		
		$sql = "SELECT
				  `checkin_date` AS date,
				  COUNT(`booking_id`) AS roomsCheckin,
				  SUM(`no_of_guest`) AS guestCheckin,
				  SUM(
					CASE WHEN `group_id` <> 0 THEN 1 ELSE 0 END
				  ) AS groupsCheckin
				FROM
				  `hotel_bookings`
				WHERE
				  `checkin_date` IS NOT NULL AND hotel_id = ".$this->session->userdata('user_hotel')." 
				GROUP BY
				  `checkin_date`
				ORDER BY
				  `checkin_date` DESC";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getCheckinExp($date){
		
		$sql = "SELECT 
				  COUNT(*) AS count,
				  SUM(
					CASE WHEN `booking_status_id` <> 5 AND `booking_status_id` <> 6 THEN 1 ELSE 0 END
				  ) AS countNonCheckin,
				  SUM(
					CASE WHEN `booking_status_id` = 5 THEN 1 ELSE 0 END
				  ) AS countCheckin,
				  SUM(
					CASE WHEN `booking_status_id` = 6 THEN 1 ELSE 0 END
				  ) AS countCheckout
				FROM
				  `hotel_bookings`
				WHERE
				  hotel_id = ".$this->session->userdata('user_hotel')." AND DATE(cust_from_date) = '".$date."' AND booking_status_id <> 7";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getTransacDate(){
		
		$sql = "SELECT
				  DATE(ta.`t_date`) as date,
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') 
					  THEN ta.`t_amount` ELSE 0 END) AS income, 
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN ta.`t_amount` ELSE 0 END) AS expense,
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN 1 ELSE 0 END) AS cinc, 
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN 1 ELSE 0 END) AS cexp
				FROM
				  `hotel_transactions` as ta JOIN  hotel_transaction_type as ty on 
				  ta.`transaction_type_id` = ty.`hotel_transaction_type_id`
				WHERE
				  ta.`hotel_id` = ".$this->session->userdata('user_hotel')." AND (ta.`t_status` = 'Done' OR ta.`t_status` = 'done')
				GROUP BY
				  DATE(ta.`t_date`) ASC";
				//LIMIT 50";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}
		else{
			return 0;
		}
		
	}
	
	
	function getBookingDetails(){
		
		$today_date = date('Y-m-d');
			
				$sql = "SELECT * FROM `hotel_bookings` WHERE '".$today_date."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and `booking_source` BETWEEN '1' AND '5' and `booking_status_id` != 11";
				//echo "sql".$sql;
				//exit;
				$qry = $this->db1->query($sql);
		
		
		if($qry->num_rows()>0){
			return $qry->result();
		}
		else{
			return 0;
		}
		
	}
	
	function getTransacMonth(){
		
		$sql = "SELECT
				  MONTHNAME(ta.`t_date`) as month, YEAR(ta.`t_date`) as year,
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') 
					  THEN ta.`t_amount` ELSE 0 END) AS income, 
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN ta.`t_amount` ELSE 0 END) AS expense,
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN 1 ELSE 0 END) AS cinc, 
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN 1 ELSE 0 END) AS cexp
				FROM
				  `hotel_transactions` as ta JOIN  hotel_transaction_type as ty on 
				  ta.`transaction_type_id` = ty.`hotel_transaction_type_id`
				WHERE
				  ta.`hotel_id` = ".$this->session->userdata('user_hotel')." AND (ta.`t_status` = 'Done' OR ta.`t_status` = 'done')
				GROUP BY
				  YEAR(ta.`t_date`), MONTHNAME(ta.`t_date`)
                ORDER BY
                  YEAR(ta.`t_date`) DESC, MONTH(ta.`t_date`) DESC";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getTransacByDateGrpType($date){
		
		$sql = "SELECT
					`transaction_type` as type, COUNT(*) as cnt,
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN ta.`t_amount` ELSE 0 END) AS income, 
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN ta.`t_amount` ELSE 0 END) AS expense,
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN 1 ELSE 0 END) AS cinc, 
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN 1 ELSE 0 END) AS cexp
				FROM hotel_transactions as ta	
				LEFT JOIN hotel_transaction_type as ty ON
					ta.`transaction_type_id` = ty.hotel_transaction_type_id
				WHERE 
				  ty.`hotel_id` = ".$this->session->userdata('user_hotel')." AND  
				  DATE(`t_date`) = '".$date."' AND (`t_status` = 'Done' OR `t_status` = 'done')
				GROUP BY 
				  DATE(`t_date`) DESC,`transaction_type`";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getTransacByDateGrpTypeMon($month, $year){
		
		$sql = "SELECT
					`transaction_type` as type, COUNT(*) as cnt,
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN ta.`t_amount` ELSE 0 END) AS income, 
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN ta.`t_amount` ELSE 0 END) AS expense,
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN 1 ELSE 0 END) AS cinc, 
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN 1 ELSE 0 END) AS cexp
				FROM hotel_transactions as ta	
				LEFT JOIN hotel_transaction_type as ty ON
					ta.`transaction_type_id` = ty.hotel_transaction_type_id
				WHERE 
				  ty.`hotel_id` = ".$this->session->userdata('user_hotel')." AND  
				  MONTHNAME(`t_date`) = '".$month."' AND YEAR(`t_date`) = '".$year."' AND (`t_status` = 'Done' OR `t_status` = 'done')
				GROUP BY 
				  `transaction_type`";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getTransacByDateGrpPC($date){
		
		$sql = "SELECT
				  `p_center` AS pc,
				  COUNT(*) AS cnt,
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN ta.`t_amount` ELSE 0 END) AS income, 
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN ta.`t_amount` ELSE 0 END) AS expense,
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN 1 ELSE 0 END) AS cinc, 
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN 1 ELSE 0 END) AS cexp
				FROM hotel_transactions as ta	
				LEFT JOIN hotel_transaction_type as ty ON
					ta.`transaction_type_id` = ty.hotel_transaction_type_id
				WHERE
				  ta.`hotel_id` = ".$this->session->userdata('user_hotel')." AND  
				  DATE(`t_date`) = '".$date."' AND (`t_status` = 'Done' OR `t_status` = 'done')
				GROUP BY DATE
				  (`t_date`) DESC,
				  `p_center`";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getTransacByDateGrpPCMon($month, $year){
		
		$sql = "SELECT
				  `p_center` AS pc,
				  COUNT(*) AS cnt,
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN ta.`t_amount` ELSE 0 END) AS income, 
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN ta.`t_amount` ELSE 0 END) AS expense,
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN 1 ELSE 0 END) AS cinc, 
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN 1 ELSE 0 END) AS cexp
				FROM hotel_transactions as ta	
				LEFT JOIN hotel_transaction_type as ty ON
					ta.`transaction_type_id` = ty.hotel_transaction_type_id
				WHERE
				  ta.`hotel_id` = ".$this->session->userdata('user_hotel')." AND  
				  MONTHNAME(`t_date`) = '".$month."' AND YEAR(`t_date`) = '".$year."' AND (`t_status` = 'Done' OR `t_status` = 'done')
				GROUP BY `p_center`";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getTransacByDateGrpMode($date){
		
		$sql = "SELECT
				  `t_payment_mode` AS mode,
				  COUNT(*) AS cnt,
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN ta.`t_amount` ELSE 0 END) AS income, 
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN ta.`t_amount` ELSE 0 END) AS expense,
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN 1 ELSE 0 END) AS cinc, 
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN 1 ELSE 0 END) AS cexp
				FROM hotel_transactions as ta	
				LEFT JOIN hotel_transaction_type as ty ON
					ta.`transaction_type_id` = ty.hotel_transaction_type_id
				WHERE
				  ta.`hotel_id` = ".$this->session->userdata('user_hotel')." AND DATE(`t_date`) = '".$date."' AND(
					`t_status` = 'Done' OR `t_status` = 'done')
				GROUP BY DATE
				  (`t_date`) DESC,
				  `t_payment_mode`";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getTransacByDateGrpModeMon($month, $year){
		
		$sql = "SELECT
				  `t_payment_mode` AS mode,
				  COUNT(*) AS cnt,
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN ta.`t_amount` ELSE 0 END) AS income, 
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN ta.`t_amount` ELSE 0 END) AS expense,
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN 1 ELSE 0 END) AS cinc, 
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN 1 ELSE 0 END) AS cexp
				FROM hotel_transactions as ta	
				LEFT JOIN hotel_transaction_type as ty ON
				  ta.`transaction_type_id` = ty.hotel_transaction_type_id
				WHERE
				  ta.`hotel_id` = ".$this->session->userdata('user_hotel')." AND MONTHNAME(`t_date`) = '".$month."' AND YEAR(`t_date`) = '".$year."' AND(`t_status` = 'Done' OR `t_status` = 'done')
				GROUP BY 
				  `t_payment_mode`";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getTransacByDateGrpSts($date){
		
		$sql = "SELECT
				  `t_status` AS sts,
				  COUNT(*) AS cnt,
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN ta.`t_amount` ELSE 0 END) AS income, 
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN ta.`t_amount` ELSE 0 END) AS expense,
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN 1 ELSE 0 END) AS cinc, 
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN 1 ELSE 0 END) AS cexp
				FROM hotel_transactions as ta	
				LEFT JOIN hotel_transaction_type as ty ON
					ta.`transaction_type_id` = ty.hotel_transaction_type_id
				WHERE
				  ta.`hotel_id` = ".$this->session->userdata('user_hotel')." AND DATE(`t_date`) = '".$date."'
				GROUP BY DATE
				  (`t_date`) DESC,
				  `t_status`";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getTransacByDateGrpStsMon($month, $year){
		
		$sql = "SELECT
				  `t_status` AS sts,
				  COUNT(*) AS cnt,
				  SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN ta.`t_amount` ELSE 0 END) AS income, 
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN ta.`t_amount` ELSE 0 END) AS expense,
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Income') THEN 1 ELSE 0 END) AS cinc, 
					SUM(CASE WHEN (ty.`hotel_transaction_type_cat` = 'Expense') THEN 1 ELSE 0 END) AS cexp
				FROM hotel_transactions as ta	
				LEFT JOIN hotel_transaction_type as ty ON
					ta.`transaction_type_id` = ty.hotel_transaction_type_id
				WHERE
				  ta.`hotel_id` = ".$this->session->userdata('user_hotel')." AND MONTHNAME(`t_date`) = '".$month."' AND YEAR(`t_date`) = '".$year."'
				GROUP BY `t_status`";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function reservationList(){
		
		$sql = "SELECT
				  DISTINCT (CASE WHEN (bk.`group_id` = 0) THEN bk.`booking_id` ELSE bk.`group_id` END) as bkid,
				  count(*) as cou,
				  (CASE WHEN (bk.`group_id` = 0) THEN 'single' ELSE 'group' END) as type,
				  (CASE WHEN (bk.`group_id` = 0) THEN gt.g_name ELSE '' END) as g_name,
				  (CASE WHEN (bk.`group_id` = 0) THEN st.booking_status ELSE '' END) as booking_status,
                  (CASE WHEN (bk.`group_id` = 0) THEN rm.`room_no` ELSE '' END) as room_no,
                  (CASE WHEN (bk.`group_id` = 0) THEN rm.`room_name` ELSE '' END) as room_name,
                  (CASE WHEN (bk.`group_id` = 0) THEN bk.`booking_source_name` ELSE '' END) as source,
                  (CASE WHEN (bk.`group_id` = 0) THEN bk.`nature_visit` ELSE '' END) as nature,
                  (CASE WHEN (bk.`group_id` = 0) THEN bk.`no_of_guest` ELSE '' END) as pax,
                  (CASE WHEN (bk.`group_id` = 0) THEN bk.`cust_from_date_actual` ELSE '' END) as start,
                  (CASE WHEN (bk.`group_id` = 0) THEN bk.`cust_end_date_actual` ELSE '' END) as end,
				  gt.g_contact_no, gt.g_address, gt.g_city, gt.g_state, gt.g_pincode, gt.g_country,
				  gt.g_email, gt.g_gender, gt.g_married, gt.g_occupation,
				  st.bar_color_code, st.body_color_code,
				  ln.rr_tot as room_rent,
				  ln.rr_tot_tax,
				  ln.exrr,
				  ln.exrr_tax,
				  ln.mp_tot,
				  ln.mp_tax,
				  ln.exmp,
				  ln.exmp_tax,
				  SUM(lng.rr_tot) as room_rent_g,
				  SUM(lng.rr_tot_tax) as rr_tot_tax_g,
				  SUM(lng.exrr) as exrr_g,
				  SUM(lng.exrr_tax) as exrr_tax_g,
				  SUM(lng.mp_tot) as mp_tot_g,
				  SUM(lng.mp_tax) as mp_tax_g,
				  SUM(lng.exmp) as exmp_g,
				  SUM(lng.exmp_tax) as exmp_tax_g
				FROM
				  `hotel_bookings` as bk
				  LEFT JOIN `hotel_group` AS gbk ON gbk.id = bk.`group_id`
                  LEFT JOIN `hotel_guest` AS gt ON gt.g_id = bk.`guest_id` AND bk.`group_id` = 0	  
				  LEFT JOIN `booking_status_type` AS st ON st.status_id = bk.booking_status_id
                  LEFT JOIN `hotel_room` AS rm ON rm.`room_id` = bk.`room_id` AND bk.`group_id` = 0
				  LEFT JOIN `booking_charge_line_item_view` as ln ON bk.booking_id = ln.booking_id AND bk.`group_id` = 0
				  LEFT JOIN `booking_charge_line_item_view` as lng ON bk.group_id = lng.group_id AND bk.`group_id` <> 0
				WHERE
				  bk.`booking_status_id` NOT IN (7,8) AND bk.`hotel_id` = ".$this->session->userdata('user_hotel')."
				GROUP BY 
				  bk.`booking_id`
				ORDER BY
				  bk.`booking_id` DESC";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
	}
	
	function singleBk_undGrp($grpID){
		
		$sql = "SELECT
				  bk.`booking_id`,
				  bk.`booking_source_name` as source,
				  bk.`nature_visit` as nature,
				  bk.`no_of_guest` as pax,
				  bk.cust_from_date_actual as start,
				  bk.cust_end_date_actual as end,
				  gt.g_name, gt.g_contact_no, gt.g_address, gt.g_city, gt.g_state, gt.g_pincode, gt.g_country,
				  gt.g_email, gt.g_gender, gt.g_married, gt.g_occupation,
				  st.booking_status, st.bar_color_code,
				  st.body_color_code,
				  rm.`room_no`,
				  rm.`room_name`,
				  ln.rr_tot as room_rent,
				  ln.rr_tot_tax,
				  ln.exrr,
				  ln.exrr_tax,
				  ln.mp_tot,
				  ln.mp_tax,
				  ln.exmp,
				  ln.exmp_tax,
				  (CASE WHEN (SUM(pos.total_amount) <> 0) THEN SUM(pos.total_amount) ELSE 0 END) as posAmt,
				  (CASE WHEN (SUM(exc.`crg_total`) <> 0) THEN SUM(exc.`crg_total`) ELSE 0 END) as crg_total,
				  (CASE WHEN (SUM(disc.`discount_amount`) <> 0) THEN SUM(disc.`discount_amount`) ELSE 0 END) as discAmt
				FROM
				  `hotel_bookings` as bk
                  LEFT JOIN hotel_guest as gt ON gt.g_id = bk.`guest_id`				  
				  LEFT JOIN booking_status_type as st ON st.status_id = bk.booking_status_id
				  LEFT JOIN `hotel_room` AS rm ON rm.`room_id` = bk.`room_id`
				  LEFT JOIN `booking_charge_line_item_view` as ln ON bk.booking_id = ln.booking_id
				  LEFT JOIN `hotel_pos` as pos ON bk.booking_id = pos.booking_id
				  LEFT JOIN `hotel_extra_charges_mapping` as exc ON exc.crg_booking_id = bk.booking_id AND exc.booking_type = 'sb'
				  LEFT JOIN `hotel_discount_mapping` as disc ON disc.booking_id = bk.booking_id
				WHERE
				  bk.`booking_status_id` NOT IN (7,8) AND bk.`group_id` = ".$grpID." AND bk.`hotel_id` = ".$this->session->userdata('user_hotel')."
				GROUP BY 
				  bk.`booking_id`
				ORDER BY
				  bk.`booking_id` DESC";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}
		else{
			return 0;
		}
	}
        
        function chartList(){
		
		$sql = "SELECT 
				CONCAT(   `admin_first_name`,' ',`admin_last_name`) AS USER,
				  COUNT(*) AS COUNT
				FROM
				  `hotel_login_session`,
				  `hotel_admin_details`
				WHERE
				  hotel_admin_details.admin_id = hotel_login_session.user_id AND hotel_admin_details.hotel_id = 6 AND `admin_first_name` <> NULL AND `hotel_id` = ".$this->session->userdata('user_hotel')."
				GROUP BY
				  hotel_login_session.user_id
				ORDER BY
				  `hotel_login_session`.`hls_id` DESC";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result_array();
		}else{
			return 0;
		}
		
	}
        
	function chartList4(){
		
		$sql = "SELECT
				  `nature_visit` as nature_visit, SUM(`no_of_guest`) as No_Of_Guest_Sum
				FROM
				  `hotel_bookings`
				WHERE
				  `booking_status_id` NOT IN (7,8)
				GROUP BY
				  `booking_source_name`";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result_array();
		}else{
			return 0;
		}
		
	}
	
	function chartdata(){
		
		$sql = "SELECT
				  `transaction_type` as type, SUM(`t_amount`) as amt
				FROM
				  `hotel_transactions`
				WHERE
				  `transaction_type` <> '' AND `t_status` = 'Done'
				GROUP BY
				  `transaction_type`
				ORDER BY 
				  SUM(`t_amount`) DESC";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function guest_stay(){
		
		$sql = "SELECT
				  gt.*, sgt.*, bk.group_id, bk.booking_source, bk.nature_visit, bk.booking_status_id, st.booking_status, st.bar_color_code, st.body_color_code
				FROM
				  `hotel_stay_guest` sgt
				  RIGHT JOIN hotel_bookings as bk ON bk.booking_id = sgt.booking_id AND bk.booking_status_id IN (1,2,4,5,6)
				  LEFT JOIN hotel_guest as gt ON gt.g_id = sgt.g_id				  
				  LEFT JOIN booking_status_type as st ON st.status_id = bk.booking_status_id
				WHERE
				  sgt.`isCancelled` <> '1' AND sgt.`hotel_id` = ".$this->session->userdata('user_hotel')."
				ORDER BY
				  sgt.booking_id DESC,
				  sgt.`addOn` DESC";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function bkCntTakenbyDateOnly(){
		
		$sql ="SELECT
				  `booking_taking_date` as date, COUNT(*) as value
				FROM
				  `hotel_bookings`
				WHERE
				  `hotel_id` = ".$this->session->userdata('user_hotel')." AND booking_taking_date > '2000-01-01' AND `booking_status_id` IN (1,2,4,5,6)
				GROUP BY
				  `booking_taking_date`
				ORDER BY
				  `booking_taking_date` ASC";
		
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function adjReport(){
		
		$sql ="SELECT
				  adj.*, DATE(adj.addedOn) as addOn, gst.g_name, bk.booking_source_name, sty.booking_status, sty.bar_color_code, sty.body_color_code, CONCAT(ad.admin_first_name,' ',ad.admin_last_name) as admin_name, SUM(lnc.rr_tot+lnc.rr_tot_tax+lnc.mp_tot+lnc.mp_tax) as bookingTotal
				FROM
				  `hotel_adjustments` as adj
				  LEFT JOIN `hotel_bookings` as bk ON bk.`booking_id` = adj.`booking_id` AND `booking_status_id` IN (1,2,4,5,6)
				  LEFT JOIN `hotel_group` as gbk ON gbk.id = adj.`group_id`
				  LEFT JOIN booking_status_type as sty ON sty.status_id = bk.`booking_status_id` OR sty.status_id = gbk.`status`
				  LEFT JOIN `hotel_guest` as gst ON gst.g_id = bk.guest_id OR gbk.guestID = gst.g_id
				  LEFT JOIN hotel_admin_details as ad ON ad.admin_id = adj.`user_id`
				  LEFT JOIN booking_charge_line_item_view as lnc on lnc.booking_id = adj.booking_id OR lnc.group_id = adj.group_id
				WHERE
				  adj.`hotel_id` = ".$this->session->userdata('user_hotel')."
				GROUP BY 
				  adj.id
				  ";
				  
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function resTakenChart(){
		
		$sql ="SELECT
				  `booking_taking_date` as date,
				  COUNT(*) AS count,
				  SUM(`stay_days`) AS days,
				  SUM(lnc.rr_tot+lnc.rr_tot_tax+lnc.mp_tot+lnc.mp_tax) AS bookingAmt,
				  SUM(lnc.rr_tot+lnc.rr_tot_tax+lnc.mp_tot+lnc.mp_tax)/SUM(`stay_days`) AS avgAmt
				FROM
				  `hotel_bookings` as bk
				  LEFT JOIN booking_charge_line_item_view as lnc ON bk.booking_id = lnc.booking_id
				WHERE
				  bk.`hotel_id` = ".$this->session->userdata('user_hotel')." AND `booking_status_id` IN (1,2,4,5,6) AND `booking_taking_date` > 0
				GROUP BY
				  `booking_taking_date`
				ORDER BY
				  `booking_taking_date` ASC";
				  
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getDisc_byBk($id,$type){
		
		if($type == 'sb')
			$booking_type = 'booking_id';
		else if($type == 'gb')
			$booking_type = 'group_id';
		
		$sql = "SELECT
				  COUNT(`id`)as count,
				  SUM(`discount_amount`) as total
				FROM
				  `hotel_discount_mapping`
				WHERE
				  `hotel_id` = ".$this->session->userdata('user_hotel')." AND `".$booking_type."` = ".$id."
				GROUP BY
				  `booking_id`";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
		
	}
	
	function getPOS_byBk($id,$type){		
		if($type == 'sb')
			$booking_type = 'booking_id';
		else if($type == 'gb')
			$booking_type = 'booking_id';
		
		$sql = "SELECT
				  COUNT(`pos_id`) as count,
				  SUM(`total_amount`) as total
				FROM
				  `hotel_pos`
				WHERE
				  `hotel_id` = ".$this->session->userdata('user_hotel')." AND `booking_id` = ".$id." AND `booking_type` = '".$type."'
				GROUP BY
				  `booking_id`";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
	}
	
	function getLaundry_byBk($id,$type){
		
		if($type == 'sb')
			$booking_type = 'booking_id';
		else if($type == 'gb')
			$booking_type = 'group_id';
		
		$sql = "SELECT
				  COUNT(`laundry_id`) as count,
				  SUM(`grand_total`) as total
				FROM
				  `hotel_laundry_master`
				WHERE
				  `hotel_id` = ".$this->session->userdata('user_hotel')." AND `".$booking_type."` = ".$id."
				GROUP BY
				  `booking_id`, `group_id`";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0){
			return $qry->result();
		}else{
			return 0;
		}
	}
	
	function getAdj_byBk($id,$type) {
		
		if($type == 'sb')
			$booking_type = 'booking_id';
		else if($type == 'gb')
			$booking_type = 'group_id';
		
		$sql = "SELECT
				  COUNT(`id`) as count,
				  SUM(`amount`) as total
				FROM
				  `hotel_adjustments`
				WHERE
				  `hotel_id` = ".$this->session->userdata('user_hotel')." AND `".$booking_type."` = ".$id."
				GROUP BY
				  `booking_id`, `group_id`";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0) {
			return $qry->result();
		} else {
			return 0;
		}
		
	}
	
	function getPaid_byBk($id,$type) {
		
		if($type == 'sb')
			$booking_type = 't_booking_id';
		else if($type == 'gb')
			$booking_type = 't_group_id';
		
		$sql = "SELECT
					COUNT(`t_id`) AS COUNT,
					SUM(`t_amount`) AS total
				FROM
					`hotel_transactions`
				WHERE
					`hotel_id` = ".$this->session->userdata('user_hotel')." AND `".$booking_type."` = ".$id." AND (t_status = 'Done' OR t_status = 'done')
				GROUP BY
					`t_booking_id`,
					`t_group_id`";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0) {
			return $qry->result();
		} else {
			return 0;
		}
		
	}
	
	function getBrokerComms($id,$type) {
		
		$sql = "SELECT
					`b_agency`,
					`b_name`,
					`b_photo_thumb`,
					SUM(tac.amount) + SUM(tac.tax) as amt
				FROM
					`hotel_broker` as bk
					LEFT JOIN hoj_travelagentcommission as tac on (bk.`b_id` = tac.taID) AND tac.taType = '".$type."'
				WHERE
					bk.`hotel_id` = ".$this->session->userdata('user_hotel')." AND `b_id` = ".$id."
				GROUP BY
					tac.taID";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0) {
			return $qry->row();
		} else {
			return 0;
		}
		
	}
	
	
	function revPar() {
		
		$sql = "SELECT
					MONTHNAME(`cust_from_date`) as month, Year(`cust_from_date`) as year, 
					DAY(LAST_DAY(`cust_from_date`)) as days, 
					(select count(*) from hotel_room where `hotel_id` = 6)*DAY(LAST_DAY(`cust_from_date`)) as rmcnt,
					count(bk.`booking_id`) as bkcnt, 
					count(bk.`booking_id`)/((select count(*) from hotel_room where `hotel_id` = 6)*DAY(LAST_DAY(`cust_from_date`)))*100 as occu,
					SUM(ln.rr_tot + ln.rr_tot_tax + ln.mp_tot + ln.mp_tax) as bkrev,
					SUM(ln.rr_tot + ln.mp_tot) as totRR,
					SUM(ln.rr_tot_tax + ln.mp_tax) as totTx,
                    SUM(exc.crg_quantity*exc.crg_unit_price) as excAmt,
                    SUM(exc.crg_tax) as excTax,
                    SUM(pos.total_amount) as posTot,
                    SUM(pos.food_sgst + pos.food_cgst + pos.food_igst + pos.food_utgst + pos.liquor_sgst + pos.liquor_cgst + pos.liquor_igst + pos.liquor_utgst) as posTax
				FROM
					`hotel_bookings` as bk 
					LEFT JOIN `booking_charge_line_item_view` as ln ON ln.booking_id = bk.`booking_id` AND `booking_status_id` NOT IN (7,8)
                    LEFT JOIN `hotel_extra_charges_mapping` as exc ON (exc.booking_type = 'sb' AND exc.crg_booking_id = bk.booking_id) OR (exc.booking_type = 'gb' AND exc.crg_booking_id = bk.group_id)
                    LEFT JOIN `hotel_pos` as pos ON (pos.booking_type = 'sb' AND pos.booking_id = bk.booking_id) OR (pos.booking_type = 'gb' AND pos.booking_id = bk.group_id)
				WHERE
					Year(`cust_from_date`) > 2016 AND `booking_status_id` NOT IN (7,8) AND bk.`hotel_id` = 6
				GROUP BY
					MONTH(`cust_from_date`)";
		$qry = $this->db1->query($sql);
		
		if($qry->num_rows()>0) {
			return $qry->result();
		} else {
			return 0;
		}
		
	}
	function occupied_room_report(){
		$date=date("Y-m-d");
		//$sql = "select * from hotel_bookings inner join hotel_room on hotel_bookings.room_id = hotel_room.room_id where booking_status_id not in('1','8','7','4') and ((date(cust_from_date) between '".$date."' and '".$date."' ) or  (date(cust_end_date) between '".$date."' and '".$date."' ) or (date(cust_from_date) <'".$date."' and date(cust_end_date) >'".$date."') or (date(cust_from_date) >'".$date."' and date(cust_end_date) <'".$date."') or  (date(cust_from_date) = '".$date."' and date(cust_end_date) >'".$date."'))";
		$sql="SELECT
                bk.*,
                (CASE WHEN `food_plans` <> 0 THEN mp.name ELSE gp.meal_plan END) as meal_plan_name,
                hotel_room.room_no
            FROM
                hotel_bookings as bk
            INNER JOIN
                hotel_room ON bk.room_id = hotel_room.room_id
            LEFT JOIN
            	hotel_group as gp ON bk.group_id = gp.id
            LEFT JOIN 
            	hotel_meal_plan_cat as mp ON bk.`food_plans` = 	mp.hotel_meal_plan_cat_id
            WHERE
                booking_status_id IN('5') AND (
                    (
                        DATE(cust_from_date) BETWEEN '".$date."' AND '".$date."'
                    ) OR(
                        DATE(cust_end_date) BETWEEN '".$date."' AND '".$date."'
                    ) OR(
                        DATE(cust_from_date) < '".$date."' AND DATE(cust_end_date) > '".$date."'
                    ) OR(
                        DATE(cust_from_date) > '".$date."' AND DATE(cust_end_date) < '".$date."'
                    ) OR(
                        DATE(cust_from_date) = '".$date."' AND DATE(cust_end_date) > '".$date."')) AND (CASE WHEN DATE(cust_end_date) = '".$date."' THEN TIME(`cust_end_date`) > 0 ELSE TRUE END)";
		$query = $this->db1->query($sql);
		if($query->num_rows()>0){
			
			return $query->result();
			
		}
		else{
			
			return 0;
		}
		
	}
	function occupied_room_report_by_date($date1,$date2){
		
		//$sql = "select * from hotel_bookings inner join hotel_room on hotel_bookings.room_id = hotel_room.room_id where booking_status_id not in('1','8','7','4') and ((date(cust_from_date) between '".$date1."' and '".$date2."' ) or  (date(cust_end_date) between '".$date1."' and '".$date2."' ) or (date(cust_from_date) <'".$date1."' and date(cust_end_date) >'".$date2."') or (date(cust_from_date) >'".$date1."' and date(cust_end_date) <'".$date2."') or  (date(cust_from_date) = '".$date2."' and date(cust_end_date) >'".$date2."'))";
		$sql="SELECT
                bk.*,
                (CASE WHEN `food_plans` <> 0 THEN mp.name ELSE gp.meal_plan END) as meal_plan_name,
                hotel_room.room_no
            FROM
                hotel_bookings as bk
            INNER JOIN
                hotel_room ON bk.room_id = hotel_room.room_id
            LEFT JOIN
            	hotel_group as gp ON bk.group_id = gp.id
            LEFT JOIN 
            	hotel_meal_plan_cat as mp ON bk.`food_plans` = 	mp.hotel_meal_plan_cat_id
            WHERE
                booking_status_id IN('5') AND (
                    (
                        DATE(cust_from_date) BETWEEN '".$date1."' AND '".$date2."'
                    ) OR(
                        DATE(cust_end_date) BETWEEN '".$date1."' AND '".$date2."'
                    ) OR(
                        DATE(cust_from_date) < '".$date1."' AND DATE(cust_end_date) > '".$date2."'
                    ) OR(
                        DATE(cust_from_date) > '".$date1."' AND DATE(cust_end_date) < '".$date2."'
                    ) OR(
                        DATE(cust_from_date) = '".$date1."' AND DATE(cust_end_date) > '".$date2."')) AND (CASE WHEN DATE(cust_end_date) = '".$date1."' THEN TIME(`cust_end_date`) > 0 ELSE TRUE END) AND (CASE WHEN DATE(cust_end_date) = '".$date2."' THEN TIME(`cust_end_date`) > 0 ELSE TRUE END)";
		$query = $this->db1->query($sql);
		if($query->num_rows()>0){
			
			return $query->result();
			
		}
		else{
			
			return 0;
		}
		
	}
	function invoice_report(){
		
		$sql = "select * from invoice_master ORDER BY `id` DESC";
		$query = $this->db1->query($sql);
		if($query->num_rows()>0){
			
			return $query->result();
			
		}
		else{
			
			return 0;
		}
		
	}
	function invoice_report_by_date($start_date,$end_date){
		
		$sql = "select * from invoice_master where date(`date_generated`) BETWEEN '".$start_date."' and  '".$end_date."' ORDER BY `id` DESC";
		$query = $this->db1->query($sql);
		if($query->num_rows()>0){
			
			return $query->result();
			
		}
		else{
			
			return 0;
		}
		
	}
	function get_guest($gid){
			
			
				$this->db1->select('g_name');
				$this->db1->where('g_id',$gid);
				$query=$this->db1->get('hotel_guest');
				if($query->num_rows()>0){
					
					return $query->result();
				}
				else{
					return 0;
				}
			
		}
	function get_bk_date_single($bid){
			
			
				$this->db1->select('*');
				$this->db1->where('booking_id',$bid);
				$query=$this->db1->get('hotel_bookings');
				if($query->num_rows()>0){
					
					return $query->result();
				}
				else{
					return 0;
				}
			
		}
	function get_bk_date_grp($bid){
			
			
				$this->db1->select('*');
				$this->db1->where('id',$bid);
				$query=$this->db1->get('hotel_group');
				if($query->num_rows()>0){
					
					return $query->result();
				}
				else{
					return 0;
				}
			
		}
	
	function addAD_payments($data){
	    $query=$this->db1->insert('hotel_advance_bk_payment',$data);
	
	   if($query){
	       return true;
	   }else{
	       return false;
	   }
	
	    
	}
	
} // End Reports Model
?>