<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Setting_model extends CI_Model {
  
  
  /*function __construct(){
		
		parent::__construct();
		$CI = &get_instance();
		$this->db1 = $CI->load->database('superadmin', TRUE);
	}*/
 	function inlineEdit($col,$editval,$id){
		$data = array(
            $col => $editval
        );
		$this->db1->where('id',$id);
	    $query=$this->db1->update(TABLE_PRE.'settings',$data); 
        if($query){
            return true;
        }else{
            return false;
        }
	}

     function get_available_room($date1,$date2){
		 
        $date1=date("Y-m-d",strtotime($date1));
        $date2=date("Y-m-d",strtotime($date2));
        $rooms=$this->dashboard_model->all_rooms();
        $array1=array();
        $array2=array();
        $bookings_between= $this->dashboard_model->get_available_room($date1,$date2);
        $i=0; $j=0;
        foreach ($rooms as $room ) {
            $array1[$i]=$room->room_id;
            $i++;
        }
        foreach ($bookings_between as $key ) {
            $array2[$j]=$key->room_id;
            $j++;
        }
		$result = array_diff($array1, $array2);
		return $result;
    }
    
    function get_admin_details(){
		
		
		$this->db1->select('*');
		$this->db1->where('admin_id',$this->session->userdata('user_id'));
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$result = $this->db1->get(TABLE_PRE.'admin_details');
		if($result->num_rows() > 0){
			
				return $result->row();
		}
		else{
			
		}
		
	}
    
    
    	function get_status_unitavail_log(){
	
		$user_id = $this->session->userdata('user_id');
		
		$sql = "select unitavaillog.user_id,hotel_admin_details.admin_first_name,hotel_admin_details. admin_last_name,hotel_unit_type.unit_name,unitavaillog.hotel_id,unitavaillog.unit_id,hotel_room.room_no, unitavaillog.checkin_date, unitavaillog.checkout_date from unitavaillog INNER JOIN hotel_admin_details ON hotel_admin_details.admin_id = unitavaillog.user_id INNER JOIN hotel_room ON unitavaillog.unit_id = hotel_room.room_id INNER JOIN hotel_unit_type ON hotel_room.unit_id = hotel_unit_type.id WHERE  unitavaillog.hotel_id = ".$this->session->userdata('user_hotel');
		
	//	echo $sql;exit;
		
		$query = $this->db1->query($sql);
	//	print_r($query);
	//	exit;
		if($query){
			
			return $query->result();
		}
		else{
		    
		    $data=array('status' =>'no details');
		    return $data;
		    
		}
	}
	
	function del_from_unitavaillog($unit_id,$rowno){
		
		
    	$this->db1->where('unit_id',$unit_id);
		$this->db1->where('user_id',$this->session->userdata('user_id'));
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query = $this->db1->delete('unitavaillog');
		if($this->db1->affected_rows() > 0){
			
			
			return $rowno;
		}
		else{
			
			return 'failed';
		}
	}
	
		function my_yummpos_setting(){
		
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$pos_result = $this->db1->get('pos_settings');
		if($pos_result->num_rows() > 0){
			
				return $pos_result->row();
		}
		else{
			
		}
		
	}
	
	function hotel_to_yummpos_setting($data){
		
		$val = $this->db1->insert('pos_settings',$data);
			if($val){
				
				return "success";
			}
			else{
				return "failed";
			}
		
	}
	
	function update_hotel_to_yummpos_setting($data_pos,$hotel_id_pos){
	
		$this->db1->where('hotel_id',$hotel_id_pos);
		$query = $this->db1->update('pos_settings',$data_pos);
		if($query){
			
			return "updated";

		}else{
			
			return "not updated";

		}
	}
	
	function check_avail_room($date1,$date2,$rmid,$sngleid){
		
			$date1=date("Y-m-d",strtotime($date1));
			$date2=date("Y-m-d",strtotime($date2));
			//$array1=array();
			//$array2=array();
			$bookings_between= $this->dashboard_model->get_avail_single_room($date1,$date2,$rmid,$sngleid);
			return $bookings_between;
		
	}
	
	function check_avail_room_final($date1,$date2,$rmid){
		
			$date1=date("Y-m-d",strtotime($date1));
			$date2=date("Y-m-d",strtotime($date2));
			
			$bookings_between= $this->dashboard_model->get_avail_single_room_final($date1,$date2,$rmid);
			return $bookings_between;
		
	}
	
	function check_avail_room_final_grp($date1,$date2,$rmid){
		
			//$date1=date("Y-m-d",strtotime($date1));
			//$date2=date("Y-m-d",strtotime($date2));
			
			$bookings_between= $this->dashboard_model->get_avail_single_room_final_grp($date1,$date2,$rmid);
			//print_r($bookings_between);exit;
			return $bookings_between;
		
	}
	
	
	function check_avail_room_final_grp2($date1,$date2,$rmid){
		
			$date1=date("Y-m-d",strtotime($date1));
			$date2=date("Y-m-d",strtotime($date2));
			
			$bookings_between= $this->dashboard_model->get_avail_single_room_final_grp2($date1,$date2,$rmid);
			//print_r($bookings_between);exit;
			return $bookings_between;
		
	}
	
	
	function insert_avail_room_final_grp($date1,$date2,$rmid){
		
			$unit_avail_log = array(
			
			"unit_id" =>$rmid,
			"type" => 'gb',
			"note" => 'group booking',
			"checkin_date" =>$date1,
			"checkout_date" =>$date2,
			"hotel_id" => $this->session->userdata('user_hotel'),
			"user_id" => $this->session->userdata('user_id')
			);
			
			//print_r($unit_avail_log);exit;
			
			$val = $this->db1->insert('unitavaillog',$unit_avail_log);
			if($val){
				
				return "success";
			}
			else{
				return "failed";
			}
		
	}
	
	
	function check_avail_room_delete_grp($date1,$date2,$rmid){
		
		$this->db1->where('checkin_date',$date1);
		$this->db1->where('checkout_date',$date2);
		$this->db1->where('unit_id',$rmid);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query = $this->db1->delete('unitavaillog');
			if($query){
				
				return "success";
			}
			else{
				return "failed";
			}
		
		
	}
	
	function update_guest($guestid,$guestdetails,$bookingid,$guest_details_bookings){
		
		$this->db1->where('g_id',$guestid);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query = $this->db1->update('hotel_guest',$guestdetails);
		if($query){
			
				$this->db1->where('booking_id',$bookingid);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$query1 = $this->db1->update('hotel_bookings',$guest_details_bookings);
				if($query1){
					
					return "success";
				}
				else{
					
						return "failed";
				}
		}
		
	}


    function edit_group_booking($data,$data2,$taxDB,$gbID,$guestID,$tax,$hid_group_status){
		
		/*echo "<pre>";
		print_r($data);
		echo "</pre>";
		echo "*************************";
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		echo "######################";
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		exit;*/

		$hst = $taxDB->hotel_service_tax;
		$hsc = $taxDB->hotel_service_charge;
		$lt1 = $taxDB->luxury_tax_slab1;
		$lt2 = $taxDB->luxury_tax_slab2;
		$slab1 = $taxDB->luxury_tax_slab1_range_to;
		$slab2 = $taxDB->luxury_tax_slab1_range_from;
		$slab = $taxDB->luxury_tax_slab2_range_to;
		$slab3 = $taxDB->luxury_tax_slab2_range_from;
		
		usort($data['startDate'], function($a, $b) {
			$dateTimestamp1 = strtotime($a);
			$dateTimestamp2 = strtotime($b);

			return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
		});
		usort($data['endDate'], function($a, $b) {
			$dateTimestamp1 = strtotime($a);
			$dateTimestamp2 = strtotime($b);

			return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
		});
	
		$sDate = $data['startDate'][0];
		$eDate = $data['endDate'][count($data['endDate']) - 1];
		$gpDate = array(
			'start_date' => $sDate,
			'end_date' => $eDate
		);
		$this->db1->where('id',$gbID);
		$this->db1->update(TABLE_PRE.'group',$gpDate);	


		// add new code for check in and check out time....

		 $times=$this->dashboard_model->checkin_time();
		
        foreach($times as $time) {
            if($time->hotel_check_in_time_fr=='PM') {
                $modifier = ($time->hotel_check_in_time_hr + 12);
            } else {
                $modifier = ($time->hotel_check_in_time_hr);
            }
        }
		
        $times2=$this->dashboard_model->checkout_time();
        foreach($times2 as $time2) {
            if($time->hotel_check_in_time_fr=='PM') {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12);
            } else {
                $modifier2 = ($time->hotel_check_out_time_hr);
            }
        }
		
		$totalGuest = 0;
		$totalRoom = 0;

// .....end ....
		
		
		// 
		foreach($data['sbID'] as $key => $val){
			
			if(($data['trr'][$key] >= $slab1) && ($data['trr'][$key] <= $slab2))
			{
				$rvat = ($data['trr'][$key]* $lt1)/100;
			} 
			else if(($data['trr'][$key] >= $slab) /*&& ($data['trr'][$key] <= $slab3)*/)
			{
				$rvat = ($data['trr'][$key]* $lt2)/100;
			} else {
				$rvat = 0 ;
			}
			
			$st = ($data['trr'][$key]* $hst)/100;
			$sc = ($data['trr'][$key]* $hsc)/100;
			
			// code for inserting reservation data into hotel_bookings table...
		
			$totalGuest = $totalGuest + $data['adultNO'][$key]+$data['childNO'][$key];
			$totalRoom++;
			$sname=$this->unit_class_model->source_id_by_name($data2['booking_source']);
			if(isset($sname))
				$snamef = $sname->bk_source_type_id;
			else $snamef = '';
			
			$sInfo=$this->unit_class_model->source_by_bookingid_grp($data['booking_ID2']);
			
            $arr_new_booking = array(
            'user_id' => $this->session->userdata("user_id"),
            'hotel_id' => $this->session->userdata("user_hotel"),
            'room_id' => $data['roomID'][$key],
            'guest_id' => $guestID,
            'no_of_adult' => $data['adultNO'][$key],
            'no_of_child' => $data['childNO'][$key],
            'no_of_guest' => $data['adultNO'][$key]+$data['childNO'][$key],
            //'booking_source_name' =>$data2['booking_source'],
            'booking_source_name' => $sInfo->booking_source_name,
            'booking_source' =>$snamef,
			'comment' =>$data2['comment'],
			'preference' =>$data2['preference'],
			'p_center' =>$data2['p_center'],
			'nature_visit' => $data2['nature_visit'],
            'cust_name' =>$data['gestName'][$key],
            'cust_from_date' =>date('Y-m-d H:i:s', strtotime($data['startDate'][$key])), 
            'cust_end_date' => date('Y-m-d H:i:s', strtotime($data['endDate'][$key])), 
            'cust_from_date_actual' => date('Y-m-d',strtotime($data['startDate'][$key])),
            'cust_end_date_actual' =>date('Y-m-d',strtotime($data['endDate'][$key])),
            'checkin_time' => $modifier,
            'confirmed_checkin_time' => $modifier,
            'checkin_time_actual' => $modifier,
            'checkout_time' => $modifier2,
            'confirmed_checkout_time' => $modifier2,
            'base_room_rent' => $data['pdr'][$key],
            'booking_status_id' => $hid_group_status,
            'booking_status_id_previous' => 4,
            'room_rent_total_amount' => $data['price'][$key],
            'room_rent_tax_amount' => 0,
            'room_rent_sum_total' => $data['price'][$key],
			//'service_price' => $data['service'][$key],
			'service_price' => 0,
            'stay_days' =>$data['days'][$key],
            'stay_days_updated' =>$data['days'][$key],
            'booking_taking_time' => date("H:i:s"),
            'booking_taking_date' => date("Y-m-d"),
            'group_id' => $gbID
            );  
            //exit;
			
			
		/*	echo '<pre>';
			print_r($arr_new_booking);
			exit;*/
		
			//Start New Code
			
		// end of hotel bookings table
			if($data['sbID'][$key] == '0'){
				
				//	echo "*****insert all tables *****";
			 
			
			if( $data['extra_person_types'][$key] == 'adult'){   // make a condition for extra person type
			$arr = array(
			'roomID' => $data['roomID'][$key],
			'type' => $data['type'][$key],
			'roomNO' => $data['roomNO'][$key],
			'gestName' => $data['gestName'][$key],
			'startDate' => $data['startDate'][$key],
			'endDate' => $data['endDate'][$key],
			'days' => $data['days'][$key],
			'mealPlan' => $data['mealPlan'][$key],
			//'adultRoomRent' => $data['adultRoomRent'][$key],
			//'afi' => $data['afi'][$key],
			//'childRoomRent' => $data['childRoomRent'][$key],
			//'cfi' => $data['cfi'][$key],
			'adultNO' => $data['adultNO'][$key],
			'childNO' => $data['childNO'][$key],
			'tfi' => $data['tfi'][$key],
			'trr' => $data['trr'][$key],
			'pdr' => $data['pdr'][$key],
			'room_rent_tax'  => $data['tax'][$key],
			'food_tax'  => $data['vat'][$key],
			'rr_tax_response' => $data['txrrspns'][$key],
			'mp_tax_response'  => $data['txmrspns'][$key],
			'extra_person_value'  => $data['exra'][$key],
			'extra_person_type'  =>  $data['extra_person_types'][$key],
			'extra_person_no'  =>  $data['exp'][$key],
			'food_vat' => $data['vat'][$key],
			'room_vat' => $data['tax'][$key],
			'room_st' => $data['tax'][$key],
			'room_sc' => $data['tax'][$key],
			'price' => $data['price'][$key],
		//	'service' => $data['service'][$key],
			'group_booking_id' => $gbID
			);
			
			}
			else if( $data['extra_person_types'][$key] == 'child'){   // make a condition for extra person type
			$arr = array(
			'roomID' => $data['roomID'][$key],
			'type' => $data['type'][$key],
			'roomNO' => $data['roomNO'][$key],
			'gestName' => $data['gestName'][$key],
			'startDate' => $data['startDate'][$key],
			'endDate' => $data['endDate'][$key],
			'days' => $data['days'][$key],
			'mealPlan' => $data['mealPlan'][$key],
			//'adultRoomRent' => $data['adultRoomRent'][$key],
			//'afi' => $data['afi'][$key],
			//'childRoomRent' => $data['childRoomRent'][$key],
			//'cfi' => $data['cfi'][$key],
			'adultNO' => $data['adultNO'][$key],
			'childNO' => $data['childNO'][$key],
			'tfi' => $data['tfi'][$key],
			'trr' => $data['trr'][$key],
			'pdr' => $data['pdr'][$key],
			'room_rent_tax'  => $data['tax'][$key],
			'food_tax'  => $data['vat'][$key],
			'rr_tax_response' => $data['txrrspns'][$key],
			'mp_tax_response'  => $data['txmrspns'][$key],
			'extra_person_value'  => $data['exra'][$key],
			'extra_person_type'  =>  $data['extra_person_types'][$key],
			'extra_person_no'  =>  $data['exp'][$key],
			'food_vat' => $data['vat'][$key],
			'room_vat' => $data['tax'][$key],
			'room_st' => $data['tax'][$key],
			'room_sc' => $data['tax'][$key],
			'price' => $data['price'][$key],
		//	'service' => $data['service'][$key],
			'group_booking_id' => $gbID
			);
			
			}
			else{
			$arr = array(	
			'roomID' => $data['roomID'][$key],
			'type' => $data['type'][$key],
			'roomNO' => $data['roomNO'][$key],
			'gestName' => $data['gestName'][$key],
			'startDate' => $data['startDate'][$key],
			'endDate' => $data['endDate'][$key],
			'days' => $data['days'][$key],
			'mealPlan' => $data['mealPlan'][$key],
			//'adultRoomRent' => $data['adultRoomRent'][$key],
			//'afi' => $data['afi'][$key],
			//'childRoomRent' => $data['childRoomRent'][$key],
			//'cfi' => $data['cfi'][$key],
			'adultNO' => $data['adultNO'][$key],
			'childNO' => $data['childNO'][$key],
			'tfi' => $data['tfi'][$key],
			'trr' => $data['trr'][$key],
			'pdr' => $data['pdr'][$key],
			'room_rent_tax'  => $data['tax'][$key],
			'food_tax'  => $data['vat'][$key],
			'rr_tax_response' => $data['txrrspns'][$key],
			'mp_tax_response'  => $data['txmrspns'][$key],
			'extra_person_value'  => $data['exra'][$key],
			'extra_person_type'  =>  $data['extra_person_types'][$key],
			'extra_person_no'  =>  $data['exp'][$key],
			'food_vat' => $data['vat'][$key],
			'room_vat' => $data['tax'][$key],
			'room_st' => $data['tax'][$key],
			'room_sc' => $data['tax'][$key],
			'price' => $data['price'][$key],
		//	'service' => $data['service'][$key],
			'group_booking_id' => $gbID
			);	
					
				
			}
		
			$query=$this->db1->insert(TABLE_PRE.'groups_bookings_details',$arr);

				/*echo "insert groups_bookings_details";
				echo "<pre>";
				print_r($arr);
				echo "</pre>";*/
				
				$query=$this->db1->insert(TABLE_PRE.'bookings',$arr_new_booking);
				/*echo "insert hotels_bookings";
				echo "<pre>";
				print_r($arr_new_booking);
				echo "</pre>";*/
				
				
				$dataNB = array(
			
				'booking_id' => $this->db1->insert_id(),
                'date' => date('Y-m-d', strtotime($data['startDate'][$key])),
				'hotel_id' => $this->session->userdata("user_hotel"),
				'group_id'  => $gbID,
                'meal_plan_chrg' => $data['tfi'][$key],
				'room_rent' => $data['trr'][$key],
				'rr_total_tax' => $data['tax'][$key],
				'mp_total_tax' => $data['vat'][$key],
				
				'rr_tax_response' => $data['txrrspns'][$key],
				'mp_tax_response' => $data['txmrspns'][$key],
                'added_on' => date('Y-m-d H:i:s')
			
			
			);
				$noofdays = array(
				
				 'dayspace'  =>  $data['days'][$key],
				
				);
				
				
				//echo "==========================";
				
				
				
				for($u=0;$u<$noofdays['dayspace'];$u++){
					
				//echo "NB".$u;
					if($u!=0){
						//echo "NBasu".$u;
						$date=date_create($dataNB['date']);
						date_add($date,date_interval_create_from_date_string("1 days"));
						$dataNB['date'] = date_format($date,"Y-m-d");
					}
					
					/*echo "##########################line item insert ";
					echo "<pre>";
					print_r($dataNB);
					echo "</pre>";*/
					
					$query=$this->db1->insert('booking_charge_line_item',$dataNB);
				}
		
		 
			}
			// end of if...
			else {
				
				//echo "*****update all tables *****";
				
		// hotel booking table update ...15-05-17 ....
				$bookingDate = array(
					'cust_from_date' =>date('Y-m-d H:i:s', strtotime($data['startDate'][$key])), 
					'cust_end_date' => date('Y-m-d H:i:s', strtotime($data['endDate'][$key])), 
					'cust_from_date_actual' => date('Y-m-d',strtotime($data['startDate'][$key])),
					'cust_end_date_actual' =>date('Y-m-d',strtotime($data['endDate'][$key])),
					
					
					'user_id' => $this->session->userdata("user_id"),
					'hotel_id' => $this->session->userdata("user_hotel"),
					'room_id' => $data['roomID'][$key],
					'guest_id' => $guestID,
					'no_of_adult' => $data['adultNO'][$key],
					'no_of_child' => $data['childNO'][$key],
					'no_of_guest' => $data['adultNO'][$key]+$data['childNO'][$key],
					'booking_source' =>$data2['booking_source'],
					'comment' =>$data2['comment'],
					'preference' =>$data2['preference'],
					'p_center' =>$data2['p_center'],
					'nature_visit' => $data2['nature_visit'],
					'cust_name' =>$data['gestName'][$key],            
					'checkin_time' => $modifier,
					'confirmed_checkin_time' => $modifier,
					'checkin_time_actual' => $modifier,
					'checkout_time' => $modifier2,
					'confirmed_checkout_time' => $modifier2,
					'base_room_rent' => $data['pdr'][$key],
					//'booking_status_id' => 4,
					//'booking_status_id_previous' => 4,
					'room_rent_total_amount' => $data['price'][$key],
					'room_rent_tax_amount' => 0,
					'room_rent_sum_total' => $data['price'][$key],
				//	'service_price' => $data['service'][$key],
					'stay_days' =>$data['days'][$key],
					'stay_days_updated' =>$data['days'][$key],
					//'booking_taking_time' => date("H:i:s"), // Not required during update
					//'booking_taking_date' =>date("Y-m-d"), // Not required during update
					'group_id' => $gbID,
					'service_tax'=>$tax['room_st'][$key],
					'luxury_tax'=>$tax['room_vat'][$key],
					'service_charge'=>$tax['room_sc'][$key],
					//''=>$tax['vat'],
					
					
				);
				
			/*	echo "*****Booking table *****";
				echo "<pre>";
				print_r($bookingDate);
				echo "</pre>";*/
				
				$this->db1->where('group_id',$gbID);
				$this->db1->where('room_id',$data['roomID'][$key]);
				$this->db1->update(TABLE_PRE.'bookings',$bookingDate);
				// end of hotel booking table update.
				
				
				// new coding....update part ....update of line item table...and groups_bookings_details table
				
				
			if( $data['extra_person_types'][$key] == 'adult'){   // make a condition for extra person type
			$arr1 = array(
			'roomID' => $data['roomID'][$key],
			'type' => $data['type'][$key],
			'roomNO' => $data['roomNO'][$key],
			'gestName' => $data['gestName'][$key],
			'startDate' => $data['startDate'][$key],
			'endDate' => $data['endDate'][$key],
			'days' => $data['days'][$key],
			'mealPlan' => $data['mealPlan'][$key],
			//'adultRoomRent' => $data['adultRoomRent'][$key],
			//'afi' => $data['afi'][$key],
			//'childRoomRent' => $data['childRoomRent'][$key],
			//'cfi' => $data['cfi'][$key],
			'adultNO' => $data['adultNO'][$key],
			'childNO' => $data['childNO'][$key],
			'tfi' => $data['tfi'][$key],
			'trr' => $data['trr'][$key],
			'pdr' => $data['pdr'][$key],
			'room_rent_tax'  => $data['tax'][$key],
			'food_tax'  => $data['vat'][$key],
			'rr_tax_response' => $data['txrrspns'][$key],
			'mp_tax_response'  => $data['txmrspns'][$key],
			'extra_person_value'  => $data['exra'][$key],
			'extra_person_type'  =>  $data['extra_person_types'][$key],
			'extra_person_no'  =>  $data['exp'][$key],
			'food_vat' => $data['vat'][$key],
			'room_vat' => $data['tax'][$key],
			'room_st' => $data['tax'][$key],
			'room_sc' => $data['tax'][$key],
			'price' => $data['price'][$key],
		//	'service' => $data['service'][$key],
			'group_booking_id' => $gbID
			);
			
		/*	echo "adult*****";
			print_r($arr1);
			*/
			
			
			
			$dataNB = array(
			
				
            //    'date' => date('Y-m-d', strtotime($data['startDate'][$key])),
				//'hotel_id' => $this->session->userdata("user_hotel"),
				'group_id'  => $gbID,
                'meal_plan_chrg' => $data['tfi'][$key],
				'room_rent' => $data['trr'][$key],
				'rr_total_tax' => $data['tax'][$key],
				'mp_total_tax' => $data['vat'][$key],
				
				'rr_tax_response' => $data['txrrspns'][$key],
				'mp_tax_response' => $data['txmrspns'][$key],
                'added_on' => date('Y-m-d H:i:s')
			
			
			);
		/*	echo "<pre>";
			echo "adult*****";
			print_r($dataNB);*/
			
			
			}
			else if( $data['extra_person_types'][$key] == 'child'){   // make a condition for extra person type
			$arr1 = array(
			'roomID' => $data['roomID'][$key],
			'type' => $data['type'][$key],
			'roomNO' => $data['roomNO'][$key],
			'gestName' => $data['gestName'][$key],
			'startDate' => $data['startDate'][$key],
			'endDate' => $data['endDate'][$key],
			'days' => $data['days'][$key],
			'mealPlan' => $data['mealPlan'][$key],
			//'adultRoomRent' => $data['adultRoomRent'][$key],
			//'afi' => $data['afi'][$key],
			//'childRoomRent' => $data['childRoomRent'][$key],
			//'cfi' => $data['cfi'][$key],
			'adultNO' => $data['adultNO'][$key],
			'childNO' => $data['childNO'][$key],
			'tfi' => $data['tfi'][$key],
			'trr' => $data['trr'][$key],
			'pdr' => $data['pdr'][$key],
			'room_rent_tax'  => $data['tax'][$key],
			'food_tax'  => $data['vat'][$key],
			'rr_tax_response' => $data['txrrspns'][$key],
			'mp_tax_response'  => $data['txmrspns'][$key],
			'extra_person_value'  => $data['exra'][$key],
			'extra_person_type'  =>  $data['extra_person_types'][$key],
			'extra_person_no'  =>  $data['exp'][$key],
			'food_vat' => $data['vat'][$key],
			'room_vat' => $data['tax'][$key],
			'room_st' => $data['tax'][$key],
			'room_sc' => $data['tax'][$key],
			'price' => $data['price'][$key],
		//	'service' => $data['service'][$key],
			'group_booking_id' => $gbID
			);
			
		/*	echo "child*****";
			print_r($arr1);*/
			
			
			$dataNB = array(
			
				
              //  'date' => date('Y-m-d', strtotime($data['startDate'][$key])),
				//'hotel_id' => $this->session->userdata("user_hotel"),
				'group_id'  => $gbID,
                'meal_plan_chrg' => $data['tfi'][$key],
				'room_rent' => $data['trr'][$key],
				'rr_total_tax' => $data['tax'][$key],
				'mp_total_tax' => $data['vat'][$key],
				
				'rr_tax_response' => $data['txrrspns'][$key],
				'mp_tax_response' => $data['txmrspns'][$key],
                'added_on' => date('Y-m-d H:i:s')
			
			
			);
			/*	echo "<pre>";
				echo "child*****";
				print_r($dataNB);
				*/
			}
			else{
			$arr1 = array(	
			'roomID' => $data['roomID'][$key],
			'type' => $data['type'][$key],
			'roomNO' => $data['roomNO'][$key],
			'gestName' => $data['gestName'][$key],
			'startDate' => $data['startDate'][$key],
			'endDate' => $data['endDate'][$key],
			'days' => $data['days'][$key],
			'mealPlan' => $data['mealPlan'][$key],
			//'adultRoomRent' => $data['adultRoomRent'][$key],
			//'afi' => $data['afi'][$key],
			//'childRoomRent' => $data['childRoomRent'][$key],
			//'cfi' => $data['cfi'][$key],
			'adultNO' => $data['adultNO'][$key],
			'childNO' => $data['childNO'][$key],
			'tfi' => $data['tfi'][$key],
			'trr' => $data['trr'][$key],
			'pdr' => $data['pdr'][$key],
			'room_rent_tax'  => $data['tax'][$key],
			'food_tax'  => $data['vat'][$key],
			'rr_tax_response' => $data['txrrspns'][$key],
			'mp_tax_response'  => $data['txmrspns'][$key],
			'extra_person_value'  => $data['exra'][$key],
			'extra_person_type'  =>  $data['extra_person_types'][$key],
			'extra_person_no'  =>  $data['exp'][$key],
			'food_vat' => $data['vat'][$key],
			'room_vat' => $data['tax'][$key],
			'room_st' => $data['tax'][$key],
			'room_sc' => $data['tax'][$key],
			'price' => $data['price'][$key],
		//	'service' => $data['service'][$key],
			'group_booking_id' => $gbID
			);	
		
			
			$dataNB = array(

            //  'date' => date('Y-m-d', strtotime($data['startDate'][$key])),
			//	'hotel_id' => $this->session->userdata("user_hotel"),
				'group_id'  => $gbID,
                'meal_plan_chrg' => $data['tfi'][$key],
				'room_rent' => $data['trr'][$key],
				'rr_total_tax' => $data['tax'][$key],
				'mp_total_tax' => $data['vat'][$key],				
				'rr_tax_response' => $data['txrrspns'][$key],
				'mp_tax_response' => $data['txmrspns'][$key],
                'added_on' => date('Y-m-d H:i:s')

			);
				/*echo "<pre>";
				echo "nope*****";
				print_r($dataNB);	*/	
				
			
			
			}
			
			
				/*echo "<pre>";
				echo "groups_bookings_details*****";
				print_r($arr1);*/

			// new update part
				$this->db1->where('id',$data['sbID'][$key]);
				$this->db1->update(TABLE_PRE.'groups_bookings_details',$arr1);
				
			
				
				// line item update...
				if($data['dateitemchange'][$key] == 'yes' && $data['lineitemchange'][$key] == 'yes'){
				
				$this->db1->where('hotel_id',$this->session->userdata("user_hotel"));
					$this->db1->where('booking_id',$data['singleBKID'][$key]);
					$this->db1->delete('booking_charge_line_item');
					
					$dataNB = array(
			
				'booking_id' => $data['singleBKID'][$key],
                'date' => date('Y-m-d', strtotime($data['startDate'][$key])),
				'hotel_id' => $this->session->userdata("user_hotel"),
				'group_id'  => $gbID,
                'meal_plan_chrg' => $data['tfi'][$key],
				'room_rent' => $data['trr'][$key],
				'rr_total_tax' => $data['tax'][$key],
				'mp_total_tax' => $data['vat'][$key],
				
				'rr_tax_response' => $data['txrrspns'][$key],
				'mp_tax_response' => $data['txmrspns'][$key],
                'added_on' => date('Y-m-d H:i:s')
			
			
			);
				$noofdays = array(
				
				 'dayspace'  =>  $data['days'][$key],
				
				);
				
				
				//echo "==========================";
				
				
				
				for($u=0;$u<$noofdays['dayspace'];$u++){
					
				//echo "NB".$u;
					if($u!=0){
						//echo "NBasu".$u;
						$date=date_create($dataNB['date']);
						date_add($date,date_interval_create_from_date_string("1 days"));
						$dataNB['date'] = date_format($date,"Y-m-d");
					}
				/*	
					echo "##########################line item update_insert ";
					echo "<pre>";
					print_r($dataNB);
					echo "</pre>";
					*/
					$query=$this->db1->insert('booking_charge_line_item',$dataNB);
				}
					
				/*	echo "<pre>";
					echo "dataNB*****line item";
					print_r($dataNB);*/
					
				}
				else if($data['lineitemchange'][$key] == 'yes'){
				
					$this->db1->where('hotel_id',$this->session->userdata("user_hotel"));
					$this->db1->where('booking_id',$data['singleBKID'][$key]);
					$this->db1->update('booking_charge_line_item',$dataNB);
					
				/*	echo "<pre>";
					echo "dataNB*****line item";
					print_r($dataNB);*/
					
				}
				else if($data['dateitemchange'][$key] == 'yes'){
					
					$this->db1->where('hotel_id',$this->session->userdata("user_hotel"));
					$this->db1->where('booking_id',$data['singleBKID'][$key]);
					$this->db1->delete('booking_charge_line_item');
					
					$dataNB = array(
			
				'booking_id' => $data['singleBKID'][$key],
                'date' => date('Y-m-d', strtotime($data['startDate'][$key])),
				'hotel_id' => $this->session->userdata("user_hotel"),
				'group_id'  => $gbID,
                'meal_plan_chrg' => $data['tfi'][$key],
				'room_rent' => $data['trr'][$key],
				'rr_total_tax' => $data['tax'][$key],
				'mp_total_tax' => $data['vat'][$key],
				
				'rr_tax_response' => $data['txrrspns'][$key],
				'mp_tax_response' => $data['txmrspns'][$key],
                'added_on' => date('Y-m-d H:i:s')
			
			
			);
				$noofdays = array(
				
				 'dayspace'  =>  $data['days'][$key],
				
				);
				
				
				//echo "==========================";
				
				
				
				for($u=0;$u<$noofdays['dayspace'];$u++){
					
				//echo "NB".$u;
					if($u!=0){
						//echo "NBasu".$u;
						$date=date_create($dataNB['date']);
						date_add($date,date_interval_create_from_date_string("1 days"));
						$dataNB['date'] = date_format($date,"Y-m-d");
					}
					
					/*echo "##########################line item update_insert ";
					echo "<pre>";
					print_r($dataNB);
					echo "</pre>";*/
					
					$query=$this->db1->insert('booking_charge_line_item',$dataNB);
				}
					
				}
				// end of line item update..
				
			}// end of main else..
			
			//End New Code
		}// end of for loop
		
		$dataGroup = array(
            'number_guest' => $totalGuest,
			'number_room' => $totalRoom
        );
		
		
		$this->db1->where('id',$gbID);
	    $grpquery = $this->db1->update(TABLE_PRE.'group',$dataGroup); 			
        if($grpquery){
            return true;
        }else{
            return false;
        }
		
	

    } // function edit_group_booking

	function get_guest($bID){
		$this->db1->select('guest_id');
		$this->db1->where('booking_id',$bID);
		$query=$this->db1->get(TABLE_PRE.'bookings');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}	
	}
	
	
	function cancel_bookings(){
		$hotel_id=$this->session->userdata('user_hotel');
       
		/*$sql="SELECT * FROM `hotel_bookings` WHERE `booking_status_id` = 7 and `group_id` = 0 and `hotel_id` = ".$hotel_id." UNION SELECT * FROM `hotel_bookings` WHERE `booking_status_id` = 7 and `group_id` > 0 and `hotel_id` = ".$hotel_id;*/
		
	//	$sql = "SELECT * FROM `hotel_bookings` WHERE `booking_status_id` = 7 ORDER BY `cancel_date` desc";
	
	
	$this->db1->select("id AS bid,name AS cname,booking_date AS bdate,booking_source AS bsource,start_date AS fdate,end_date AS tdate,id AS type,cancel_date as cnd,cancellation_reason as c_reason,booking_notes as bid_previous,status as booking_status,number_guest as nog, 	nature_visit as nature,guestID as gid");
    $this->db1->distinct();
    $this->db1->from("hotel_group");
//	$this->db1->where("date_format(booking_date,'%Y-%m-%d') =", date("Y-m-d"));
	$this->db1->where("status", 7);
  
    $query1 = $this->db1->get_compiled_select(); // It resets the query just like a get()

    $this->db1->select("booking_id AS bid,cust_name AS cname,booking_taking_date AS bdate,booking_source AS bsource,cust_from_date_actual AS fdate,cust_end_date_actual AS tdate,group_id AS type,cancel_date as cnd,cancellation_reason as c_reason,booking_status_id_previous as bid_previous,booking_status_id as booking_status,no_of_guest as nog,nature_visit as nature,guest_id as gid");
    $this->db1->distinct();
    $this->db1->from("hotel_bookings");
    $this->db1->where("group_id",0);
	$this->db1->where("booking_status_id",7);
	//$this->db1->where("date_format(booking_taking_date,'%Y-%m-%d') =", date("Y-m-d"));
    $query2 = $this->db1->get_compiled_select(); 

    $query = $this->db1->query($query1." UNION ".$query2);

    return $query->result();	 
		 
    }
	
	
	function cancel_bookings_by_date($cust_from_date_actual,$cust_end_date_actual){
		$hotel_id=$this->session->userdata('user_hotel');
	
	
	$this->db1->select("id AS bid,name AS cname,booking_date AS bdate,booking_source AS bsource,start_date AS fdate,end_date AS tdate,id AS type,cancel_date as cnd,cancellation_reason as c_reason,booking_notes as bid_previous,status as booking_status,number_guest as nog, 	nature_visit as nature,guestID as gid");
    $this->db1->distinct();
    $this->db1->from("hotel_group");
	$this->db1->where("cancel_date >=",$cust_from_date_actual);

    $this->db1->where("cancel_date <=", $cust_end_date_actual);
	$this->db1->where("status", 7);
  
    $query1 = $this->db1->get_compiled_select(); // It resets the query just like a get()

    $this->db1->select("booking_id AS bid,cust_name AS cname,booking_taking_date AS bdate,booking_source AS bsource,cust_from_date_actual AS fdate,cust_end_date_actual AS tdate,group_id AS type,cancel_date as cnd,cancellation_reason as c_reason,booking_status_id_previous as bid_previous,booking_status_id as booking_status,no_of_guest as nog,nature_visit as nature,guest_id as gid");
    $this->db1->distinct();
    $this->db1->from("hotel_bookings");
    $this->db1->where("group_id",0);
	$this->db1->where("booking_status_id",7);
	$this->db1->where("cancel_date >=",$cust_from_date_actual);

    $this->db1->where("cancel_date <=", $cust_end_date_actual);
    $query2 = $this->db1->get_compiled_select(); 

    $query = $this->db1->query($query1." UNION ".$query2);

    return $query->result();	 

		
		
    }


	function get_gbDetails($bID){
		$this->db1->select('*');
		$this->db1->where('id',$bID);
		$query=$this->db1->get(TABLE_PRE.'group');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}	
	}
	


 	function checkUser($val){
		$this->db1->select('*');
		$this->db1->where('user_name',$val);
		$query=$this->db1->get(TABLE_PRE.'login');
		if($query->num_rows()>0){
			return 1;
		}else{
			return 0;
		}	
	}


		function allProfitCenter(){
			$this->db1->select('*');
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$this->db1->where('status','1');
			$query=$this->db1->get(TABLE_PRE.'profit_center');
			if($query){
           		 return $query->result();
	        } else {
	            return false;
			}
		}


		function allAdminRole(){
			$this->db1->select('*');
			$query=$this->db1->get(TABLE_PRE.'user_permission');
			if($query){
           		 return $query->result();
	        } else {
	            return false;
			}
		}

		
		
		
			
	function add_admin_user($admin_details,$login,$hotel_id){
		$this->db1->insert(TABLE_PRE.'admin_details',$admin_details);
		$this->db->insert(TABLE_PRE.'admin_details',$admin_details);
		$insert_id = $this->db1->insert_id();
		$login['user_id'] = $insert_id;	
		$rateplan_index_array = array(
		
		"hotel_id" => $hotel_id,
		"user_id" =>  $insert_id,
		"populate_status" => "0",
		"chain_id" => "1"
		);
		//echo "<pre>";print_r($rateplan_index_array);exit;
		$this->db1->insert('hotel_rateplan_index',$rateplan_index_array);
		
		
		$user_id=$insert_id;
		
		$sql="INSERT INTO `booking_status_type`( `user_id`, `hotel_id`, `booking_status_type`, `booking_status`, `bar_color_code`, `body_color_code`, `text_color_code`, `booking_status_slug`) VALUES
(".$user_id." , ".$hotel_id." , 'primary', 'Temporary hold', '#4b0585', '#c8c1e8', '', ''),
(".$user_id." , ".$hotel_id." , 'primary', 'Advance', '#f0463e', '#f8f1f3', '', ''),
(".$user_id." , ".$hotel_id." ,  'secondary', 'Pending', '#a872c9', '#e9dfea', '', 'P'),
(".$user_id." , ".$hotel_id." , 'primary', 'Confirmed', '#438368', '#fefc96', '', ''),
(".$user_id." , ".$hotel_id." ,  'primary', 'Checked in', '#38ba93', '#bff9d8', '', ''),
(".$user_id." , ".$hotel_id." ,  'primary', 'Checked out', '#4a638a', '#bbbbbb', '', ''),
(".$user_id." , ".$hotel_id." ,  'primary', 'Cancelled', '#4f346b', '#eeeeee', '', ''),
(".$user_id." , ".$hotel_id." , 'primary', 'Incomplete', '#3e2d39', '#e3e3e3', '#111111', ''),
(".$user_id." , ".$hotel_id." ,  'secondary', 'Due', '#ffb62d', '#ffa500', '', 'D'),
(".$user_id." , ".$hotel_id." ,  'secondary', 'Partial', '', '', '', 'H'),
(".$user_id." , ".$hotel_id." , 'secondary', 'Block', '', '', '', ''),
(".$user_id." , ".$hotel_id." , 'primary', 'Partially Checked In', '#136F35', '#C9FF00', '', ''),
(".$user_id." , ".$hotel_id." ,  'primary', 'Partially Checked out', '#a72bc1', '', '', '');";

		$query=$this->db1->query($sql);
	//LAUNDRY CREATION AT THE TIME OF HOTEL CREATION
	
	$cloth_type=$this->unit_class_model->all_laundry_cloth_type();
		$fabric_type=$this->unit_class_model->all_laundry_fabric_type();
		
		foreach ($cloth_type as $ct){
			//echo $ct->laundry_ct_name.'<br>';			
			foreach ($fabric_type as $ft){
				//echo $ft->fabric_type.'<br>';
				$a=$ct->laundry_ct_id;
				$b=$ft->fabric_id;
				$sql2="INSERT INTO hotel_laundry_rates(lr_cloth_type_id,lr_fabric_type_id,user_id,hotel_id) VALUES($a,$b,$user_id,$hotel_id)";
				$query2=$this->db1->query($sql2);
			}
		}
		//END LAUNDRY 
		$query=$this->db1->insert(TABLE_PRE.'login',$login);	
		$query1=$this->db->insert(TABLE_PRE.'login_master',$login);
		$query2=$this->db->select('user_id')->order_by('login_id','desc')->limit(1)->get('hotel_login_master')->row('user_id');
		$data=array(
		'user_id'=>$query2,
		'hotel_id'=>$hotel_id
		
		);
		$this->db1->where('admin_id',$login['user_id']);
		$this->db1->update(TABLE_PRE.'admin_details',$data);
		
		//static table admin_details update..need to be change 
		
		$this->db->where('admin_id',$login['user_id']);
		$this->db->update(TABLE_PRE.'admin_details',$data);
		if($query){
			return true;
		}else{
			return false;
		}
	}

function add_admin_user1($admin_details,$login){
		$this->db->insert(TABLE_PRE.'admin_details',$admin_details);
		$this->db1->insert(TABLE_PRE.'admin_details',$admin_details);
		$insert_id = $this->db1->insert_id();
		$login['user_id'] = $insert_id;	
		$hotel_id=$this->session->userdata('user_hotel');
	
/*	$rateplan_index_array = array(
		
		"hotel_id" => $hotel_id,
		"user_id" =>  $insert_id,
		"populate_status" => "0",
		"chain_id" => "1"
		);
		//echo "<pre>";print_r($rateplan_index_array);exit;
		$this->db1->insert('hotel_rateplan_index',$rateplan_index_array);
		*/
		
		$user_id=$insert_id;
		
		/*$sql="INSERT INTO `booking_status_type`( `user_id`, `hotel_id`, `booking_status_type`, `booking_status`, `bar_color_code`, `body_color_code`, `text_color_code`, `booking_status_slug`) VALUES
(".$user_id." , ".$hotel_id." , 'primary', 'Temporary hold', '#4b0585', '#c8c1e8', '', ''),
(".$user_id." , ".$hotel_id." , 'primary', 'Advance', '#f0463e', '#f8f1f3', '', ''),
(".$user_id." , ".$hotel_id." ,  'secondary', 'Pending', '#a872c9', '#e9dfea', '', 'P'),
(".$user_id." , ".$hotel_id." , 'primary', 'Confirmed', '#438368', '#fefc96', '', ''),
(".$user_id." , ".$hotel_id." ,  'primary', 'Checked in', '#38ba93', '#bff9d8', '', ''),
(".$user_id." , ".$hotel_id." ,  'primary', 'Checked out', '#4a638a', '#bbbbbb', '', ''),
(".$user_id." , ".$hotel_id." ,  'primary', 'Cancelled', '#4f346b', '#eeeeee', '', ''),
(".$user_id." , ".$hotel_id." , 'primary', 'Incomplete', '#3e2d39', '#e3e3e3', '#111111', ''),
(".$user_id." , ".$hotel_id." ,  'secondary', 'Due', '#ffb62d', '#ffa500', '', 'D'),
(".$user_id." , ".$hotel_id." ,  'secondary', 'Partial', '', '', '', 'H'),
(".$user_id." , ".$hotel_id." , 'secondary', 'Block', '', '', '', ''),
(".$user_id." , ".$hotel_id." , 'primary', 'Partially Checked In', '#136F35', '#C9FF00', '', ''),
(".$user_id." , ".$hotel_id." ,  'primary', 'Partially Checked out', '#a72bc1', '', '', '');";
*/
		//$query=$this->db1->query($sql);
	//LAUNDRY CREATION AT THE TIME OF HOTEL CREATION
	
	$cloth_type=$this->unit_class_model->all_laundry_cloth_type();
		$fabric_type=$this->unit_class_model->all_laundry_fabric_type();
		//echo "hello";exit;
		/*foreach ($cloth_type as $ct){
			//echo $ct->laundry_ct_name.'<br>';			
			foreach ($fabric_type as $ft){
				//echo $ft->fabric_type.'<br>';
				$a=$ct->laundry_ct_id;
				$b=$ft->fabric_id;
				$sql2="INSERT INTO hotel_laundry_rates(lr_cloth_type_id,lr_fabric_type_id,user_id,hotel_id) VALUES($a,$b,$user_id,$hotel_id)";
				$query2=$this->db1->query($sql2);
			}
		}*/
		//END LAUNDRY 
		$query=$this->db1->insert(TABLE_PRE.'login',$login);	
		
		$query1=$this->db->insert(TABLE_PRE.'login_master',$login);
		$query2=$this->db->select('user_id')->order_by('login_id','desc')->limit(1)->get('hotel_login_master')->row('user_id');
		
		$data=array('user_id'=>$query2);
		$this->db1->where('admin_id',$login['user_id']);
		$this->db1->update(TABLE_PRE.'admin_details',$data);
		if($query){
			return $query1;
		}else{
			return false;
		}
	}
    function all_admin_user(){		
		$this->db1->select('*');
        $this->db1->from('hotel_admin_details');
        //$this->db1->where('hotel_admin_details.hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->join('hotel_login', 'hotel_admin_details.admin_id = hotel_login.user_id');
        $query=$this->db1->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }


	function getUserType($bID){
		$this->db1->select('user_type_name');
		$this->db1->where('user_type_id',$bID);
		$query=$this->db1->get(TABLE_PRE.'user_type');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}	
	}

	function getAdminRole($bID){
		$this->db1->select('admin_roll_name');
		$this->db1->where('user_permission_id',$bID);
		$query=$this->db1->get(TABLE_PRE.'user_permission');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}	
	}	

    function getAdminUserById($id){		
		$this->db1->select('*');
		$this->db1->where('admin_id',$id);
        $this->db1->from('hotel_admin_details');
		$this->db1->join('hotel_login', 'hotel_admin_details.admin_id = hotel_login.user_id');
        $query=$this->db1->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

 		function edit_admin_user($admin_details,$login,$admin_id,$login_id){
		$this->db1->where('admin_id',$admin_id);
	    $this->db1->update(TABLE_PRE.'admin_details',$admin_details); 
		$this->db1->where('user_id',$admin_id);
	    $query=$this->db1->update(TABLE_PRE.'login',$login); 
	    $this->db->where('user_id',$admin_id);
	    $query1=$this->db->update(TABLE_PRE.'login_master',$login); 
        if($query && $query1){
            return true;
        }else{
            return false;
        }
	}
	
    function delete_admin_user($admin_id,$login_id){
        $this->db1->where('admin_id',$admin_id);
        $query=$this->db1->delete(TABLE_PRE.'admin_details');
		$this->db1->where('login_id',$login_id);
		$query=$this->db1->delete(TABLE_PRE.'login');
        if($query){
            return true;
        }else{
            return false;
        }
    }	
	
    function get_admin($admin_id)
    {
        $this->db1->where('admin_user_type',$admin_id);
        $query=$this->db1->get(TABLE_PRE.'admin_details');
        if($query->num_rows()==1){

            return $query->row();
        }else{
            return false;
        }



    }	
    function send_sms($message,$contact){
		$details=$this->get_admin('1');
		$receiver= $details->admin_phone1;
		$username = "samrat360@gmail.com";
		$hash = "2c806042ceba707a9ba592295dd8a6aceda01083";
		$test = "0";
		$numbers =$receiver.",".$contact;
		$sender = "TXTLCL"; 
		$message = urlencode($message);
		$data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test;
		$ch = curl_init('http://api.textlocal.in/send/?');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch); 
		curl_close($ch);
    }	
	
	function showAgentDetails($agent_type){
		if ($agent_type == 'B'){
		    $tab = 'hotel_broker';
		} else {
			$tab = 'hotel_channel';
		}
		
		$this->db1->select('*');
        $query=$this->db1->get($tab);
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return 0;
        }
    }	
	
	function showAgentCommission($agent_type,$id){
		if ($agent_type == 'B'){
		    $tab = 'hotel_broker';
			$this->db1->select('*');
			$this->db1->where('b_id',$id);
			$query=$this->db1->get($tab);			
		} else {
			$tab = 'hotel_channel';
			$this->db1->select('*');
			$this->db1->where('channel_id',$id);
			$query=$this->db1->get($tab);			
		}
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return 0;
        }
    }	
	
	
 	function updateAgentCommission($cr,$ca,$gid){
		$dataGroup = array(
            'travel_agent_commission' => $cr,
			'commission_applicable' => $ca
        );
		$this->db1->where('id',$gid);
	    $query=$this->db1->update(TABLE_PRE.'group',$dataGroup); 		
        if($query){
            return 1;
        }else{
            return 0;
        }
	}	
	
	function get_guest_details($guestID){
		
		$this->db1->select('*');
		$this->db1->where('g_id',$guestID);
		$query = $this->db1->get('hotel_guest');
		if($query){
			
			return $query->row();
		}
	}
	
	
	
	
	function get_transaction_type_cat($type_cat_id){
		
		$this->db1->select('*');
		$this->db1->where('hotel_transaction_type_id',$type_cat_id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from('hotel_transaction_type');
		$query=$this->db1->get();
		 if($query){
            return $query->row();
		 }
	}
	
	
	function get_transactionType_name()
	{
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query = $this->db1->get(TABLE_PRE.'transaction_type');

		if($query->num_rows()>0){
			return $query->result();	 
		}
		else{
			return false;
		}
	}
	
	
	function get_arrival($b_source,$b_stat,$status_text,$date_status){
	
	//echo "today=>".$date_status;
	//exit;
	if($date_status == 'Today'){
		
		if($b_source!=100){
			
				if($status_text == 'arrival'){
					$b_status_array = array(5,6,7);
					$this->db1->select('*');
					$this->db1->where('booking_source',$b_source);
					
				   $this->db1->where_not_in('booking_status_id',$b_status_array);
					$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d'));
					$query = $this->db1->get('hotel_bookings');
					if($query){
						
						return $query->result();
					}
					
				}
				else if($status_text == 'Departures'){
					
					$this->db1->select('*');
					$this->db1->where('booking_source',$b_source);
					$this->db1->where('booking_status_id=',$b_stat);
					//$this->db1->where('booking_status_id!=',5);
					//$this->db1->where('booking_status_id!=',7);
					$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					$this->db1->where("date_format(cust_end_date,'%Y-%m-%d') =",date('Y-m-d'));
					$query = $this->db1->get('hotel_bookings');
					if($query){
						
						return $query->result();
					}
					
					
				}
				else if($status_text == 'checkout'){
					
					$this->db1->select('*');
					$this->db1->where('booking_source',$b_source);
					$this->db1->where('booking_status_id=',$b_stat);
					$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					$this->db1->where('cust_end_date_actual',date('Y-m-d'));
					$query = $this->db1->get('hotel_bookings');
					if($query){
						
						return $query->result();
					}
					
					
				}
				else if($status_text == 'all booking'){
					
					$this->db1->select('*');
					$this->db1->where('booking_source',$b_source);
					$this->db1->where('booking_status_id!=',$b_stat);
					$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d'));
					$query = $this->db1->get('hotel_bookings');
					if($query){
						
						return $query->result();
					}
					
					
				}
				else if($status_text == 'No Show Bookings'){
				
				$status_array = array(2,3,4);
				$this->db1->select('*');
				$this->db1->where('booking_source!=',$b_source);
				$this->db1->where_in('booking_status_id',$status_array);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d'));
				$query = $this->db1->get('hotel_bookings');
				if($query){
					
					return $query->result();
				}
				
				
			}
		}
		else{
			
			
					if($status_text == 'arrival'){
						$b_status_array = array(5,6,7);
					$this->db1->select('*');
					$this->db1->where('booking_source!=',$b_source);
					
				$this->db1->where_not_in('booking_status_id',$b_status_array);
					$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d'));
					$query = $this->db1->get('hotel_bookings');
					if($query){
						
						return $query->result();
					}
					
				}
				else if($status_text == 'Departures'){
					
					$this->db1->select('*');
					$this->db1->where('booking_source!=',$b_source);
					$this->db1->where('booking_status_id=',$b_stat);
					//$this->db1->where('booking_status_id!=',5);
					//$this->db1->where('booking_status_id!=',7);
					$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					$this->db1->where("date_format(cust_end_date,'%Y-%m-%d') =",date('Y-m-d'));
					$query = $this->db1->get('hotel_bookings');
					if($query){
						
						return $query->result();
					}
					
					
				}
				else if($status_text == 'checkout'){
					
					$this->db1->select('*');
					$this->db1->where('booking_source!=',$b_source);
					$this->db1->where('booking_status_id=',$b_stat);
					$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
					$this->db1->where('cust_end_date_actual',date('Y-m-d'));
					$query = $this->db1->get('hotel_bookings');
					if($query){
						
						return $query->result();
					}
					
					
				}
				else if($status_text == 'all booking'){
					
				$today_date = date('Y-m-d');
			
				$sql = "SELECT * FROM `hotel_bookings` WHERE '".$today_date."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and `booking_source` BETWEEN '1' AND '5' and `booking_status_id` != 11";
				//echo "sql".$sql;
				//exit;
				$query = $this->db1->query($sql);
				if($query){
					
					return $query->result();
				}
					
					
				}
				
				else if($status_text == 'No Show Bookings'){
				
				$status_array = array(2,3,4);
				
				$this->db1->select('*');
				$this->db1->where('booking_source!=',$b_source);
				//$this->db1->where('booking_status_id=',$b_stat);
				//$this->db1->where('booking_status_id=',2);
				//$this->db1->where('booking_status_id=',4);
				$this->db1->where_in('booking_status_id',$status_array);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d'));
				$query = $this->db1->get('hotel_bookings');
				if($query){
					
					return $query->result();
				}
				
				
			}
			
			
		}
	
	}
	else if($date_status == 'Yesterday'){
		
		if($b_source!=100){
			if($status_text == 'arrival'){
				$b_status_array = array(5,6,7);
				$this->db1->select('*');
				$this->db1->where('booking_source',$b_source);
				
				$this->db1->where_not_in('booking_status_id',$b_status_array);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				
				$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
				$query = $this->db1->get('hotel_bookings');
				if($query){
					
					return $query->result();
				}
				
			}
			else if($status_text == 'Departures'){
				
				$this->db1->select('*');
				$this->db1->where('booking_source',$b_source);
				$this->db1->where('booking_status_id=',$b_stat);
				//$this->db1->where('booking_status_id!=',5);
				//$this->db1->where('booking_status_id!=',7);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$this->db1->where("date_format(cust_end_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
				$query = $this->db1->get('hotel_bookings');
				if($query){
					
					return $query->result();
				}
				
				
			}
			else if($status_text == 'checkout'){
				
				$this->db1->select('*');
				$this->db1->where('booking_source',$b_source);
				$this->db1->where('booking_status_id=',$b_stat);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$this->db1->where('cust_end_date_actual',date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
				$query = $this->db1->get('hotel_bookings');
				if($query){
					
					return $query->result();
				}
				
				
			}
			else if($status_text == 'all booking'){
				
				$this->db1->select('*');
				$this->db1->where('booking_source',$b_source);
				$this->db1->where('booking_status_id!=',$b_stat);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
				$query = $this->db1->get('hotel_bookings');
				
				if($query){
					
					return $query->result();
				}
				
				
			}
			else if($status_text == 'No Show Bookings'){
				
				$status_array = array(2,3,4);
				$this->db1->select('*');
				$this->db1->where('booking_source!=',$b_source);
				$this->db1->where_in('booking_status_id',$status_array);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
				$query = $this->db1->get('hotel_bookings');
				if($query){
					
					return $query->result();
				}
				
				
			}
			
	}
	else{
		
			if($status_text == 'arrival'){
				$this->db1->select('*');
				$this->db1->where('booking_source!=',$b_source);
				$this->db1->where('booking_status_id!=',$b_stat);
				$this->db1->where('booking_status_id!=',6);
				$this->db1->where('booking_status_id!=',7);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
				$query = $this->db1->get('hotel_bookings');
				if($query){
					
					return $query->result();
				}
				
			}
			else if($status_text == 'Departures'){
				
				$this->db1->select('*');
				$this->db1->where('booking_source!=',$b_source);
				$this->db1->where('booking_status_id=',$b_stat);
				//$this->db1->where('booking_status_id!=',5);
				//$this->db1->where('booking_status_id!=',7);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$this->db1->where("date_format(cust_end_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
				$query = $this->db1->get('hotel_bookings');
				if($query){
					
					return $query->result();
				}
				
				
			}
			else if($status_text == 'checkout'){
				
				$this->db1->select('*');
				$this->db1->where('booking_source!=',$b_source);
				$this->db1->where('booking_status_id=',$b_stat);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$this->db1->where('cust_end_date_actual',date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
				$query = $this->db1->get('hotel_bookings');
				if($query){
					
					return $query->result();
				}
				
				
			}
			else if($status_text == 'all booking'){
				
				$yesterday_date = date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))));
			/*	echo "b_source".$b_source."booking_status_id".$b_stat;
				exit;
				$this->db1->select('*');
				$this->db1->where('booking_source!=',$b_source);
				$this->db1->where('booking_status_id!=',$b_stat);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
				$query = $this->db1->get('hotel_bookings');*/
				$sql = "SELECT * FROM `hotel_bookings` WHERE '".$yesterday_date."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and `booking_source` BETWEEN '1' AND '5' and `booking_status_id` != 11";
				//echo "sql".$sql;
				//exit;
				$query = $this->db1->query($sql);
				if($query){
					
					return $query->result();
				}
				
				
			}
			
		else if($status_text == 'No Show Bookings'){
				
				$status_array = array(2,3,4);
				
				$this->db1->select('*');
				$this->db1->where('booking_source!=',$b_source);
				$this->db1->where_in('booking_status_id',$status_array);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
				$query = $this->db1->get('hotel_bookings');
				if($query){
					
					return $query->result();
				}
				
				
			}
		
	}
	
	}
	
	
	
	else if($date_status == 'Tommorow'){
		
		if($b_source!=100){
		
		if($status_text == 'arrival'){
			$b_status_array = array(5,6,7);
			$this->db1->select('*');
			$this->db1->where('booking_source',$b_source);
			
			$this->db1->where_not_in('booking_status_id',$b_status_array);
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d')))));
			$query = $this->db1->get('hotel_bookings');
			if($query){
				
				return $query->result();
			}
			
		}
		else if($status_text == 'Departures'){
			
			$this->db1->select('*');
			$this->db1->where('booking_source',$b_source);
			$this->db1->where('booking_status_id=',$b_stat);
			//$this->db1->where('booking_status_id!=',5);
			//$this->db1->where('booking_status_id!=',7);
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$this->db1->where("date_format(cust_end_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d')))));
			$query = $this->db1->get('hotel_bookings');
			if($query){
				
				return $query->result();
			}
			
			
		}
		else if($status_text == 'checkout'){
			
			$this->db1->select('*');
			$this->db1->where('booking_source',$b_source);
			$this->db1->where('booking_status_id=',$b_stat);
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$this->db1->where('cust_end_date_actual',date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d')))));
			$query = $this->db1->get('hotel_bookings');
			if($query){
				
				return $query->result();
			}
			
			
		}
		else if($status_text == 'all booking'){
			
			$this->db1->select('*');
			$this->db1->where('booking_source',$b_source);
			$this->db1->where('booking_status_id!=',$b_stat);
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d')))));
			$query = $this->db1->get('hotel_bookings');
			if($query){
				
				return $query->result();
			}
			
			
		}
		else if($status_text == 'No Show Bookings'){
				
				$status_array = array(2,3,4);
				$this->db1->select('*');
				$this->db1->where('booking_source!=',$b_source);
				$this->db1->where_in('booking_status_id',$status_array);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d')))));
				$query = $this->db1->get('hotel_bookings');
				if($query){
					
					return $query->result();
				}
				
				
			}
		
	}
	else{
		
		
		if($status_text == 'arrival'){
			$b_status_array = array(5,6,7);
			
			$this->db1->select('*');
			$this->db1->where('booking_source!=',$b_source);
			$this->db1->where_not_in('booking_status_id',$b_status_array);
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d')))));
			$query = $this->db1->get('hotel_bookings');
			if($query){
				
				return $query->result();
			}
			
		}
		else if($status_text == 'Departures'){
			
			$this->db1->select('*');
			$this->db1->where('booking_source!=',$b_source);
			$this->db1->where('booking_status_id=',$b_stat);
			//$this->db1->where('booking_status_id!=',5);
			//$this->db1->where('booking_status_id!=',7);
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$this->db1->where("date_format(cust_end_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d')))));
			$query = $this->db1->get('hotel_bookings');
			if($query){
				
				return $query->result();
			}
			
			
		}
		else if($status_text == 'checkout'){
			
			$this->db1->select('*');
			$this->db1->where('booking_source!=',$b_source);
			$this->db1->where('booking_status_id=',$b_stat);
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$this->db1->where('cust_end_date_actual',date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d')))));
			$query = $this->db1->get('hotel_bookings');
			if($query){
				
				return $query->result();
			}
			
			
		}
		else if($status_text == 'all booking'){
			
			$tommorow_date = date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d'))));
			
				$sql = "SELECT * FROM `hotel_bookings` WHERE '".$tommorow_date."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and `booking_source` BETWEEN '1' AND '5' and `booking_status_id` != 11";
				//echo "sql".$sql;
				//exit;
				$query = $this->db1->query($sql);
				if($query){
					
					return $query->result();
				}
				
			
			
		}
		else if($status_text == 'No Show Bookings'){
				
				$status_array = array(2,3,4);
				$this->db1->select('*');
				$this->db1->where('booking_source!=',$b_source);
				$this->db1->where_in('booking_status_id',$status_array);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$this->db1->where("date_format(cust_from_date,'%Y-%m-%d') =",date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d')))));
				$query = $this->db1->get('hotel_bookings');
				if($query){
					
					return $query->result();
				}
				
				
			}
		
		
		
		
	}
	}// end of if..
	
}// end of function
	
	function current_day_revenue_bookings($t_date){
		
		if($t_date == 'Yesterday'){
			
		$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))."' and `hotel_id`= '".$this->session->userdata('user_hotel')."'";
		$query = $this->db1->query($sql);
		if($query){
			
			return  $query->result();
		}
			
			
			
		}
		else if($t_date == 'today'){
			
			$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d')."' and `hotel_id`= '".$this->session->userdata('user_hotel')."'";
		$query = $this->db1->query($sql);
		if($query){
			
			return  $query->result();
		}
		}
		else{
			
				$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d', strtotime('-7 day', strtotime(date('Y-m-d'))))."' and `hotel_id`= '".$this->session->userdata('user_hotel')."'";
		$query = $this->db1->query($sql);
		if($query){
			
			return  $query->result();
		}
			
		}
		
		
		
	}

function get_day_revenue_bookings($t_date,$t_stat,$t_modes){
		
		if($t_date == 'Yesterday'){
			if($t_modes != 'all_modes'){
				if($t_stat == 'all'){
					
						$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))."' and `hotel_id`= '".$this->session->userdata('user_hotel')."' and `t_payment_mode`= '".$t_modes."'";
					$query = $this->db1->query($sql);
					if($query){
						
						return  $query->result();
					}
				}
				else{
					
						$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))."' and `hotel_id`= '".$this->session->userdata('user_hotel')."' and `t_payment_mode`= '".$t_modes."' and `transaction_type_id` = '".$t_stat."'";
					$query = $this->db1->query($sql);
					if($query){
						
						return  $query->result();
					}
				}
				
		}	
		else{
			
					if($t_stat == 'all'){
					
						$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))."' and `hotel_id`= '".$this->session->userdata('user_hotel')."' and `t_payment_mode`!= '".$t_modes."'";
					$query = $this->db1->query($sql);
					if($query){
						
						return  $query->result();
					}
				}
				else{
					
						$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))."' and `hotel_id`= '".$this->session->userdata('user_hotel')."' and `t_payment_mode`!= '".$t_modes."' and `transaction_type_id` = '".$t_stat."'";
					$query = $this->db1->query($sql);
					if($query){
						
						return  $query->result();
					}
				}
			
			
			
		}		
			
			
		}
		
		else if($t_date == 'today'){
			
			if($t_modes!= 'all_modes'){
				
							if($t_stat == 'all'){
								
							$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d')."' and `hotel_id`= '".$this->session->userdata('user_hotel')."' and `t_payment_mode` = '".$t_modes."'";
						$query = $this->db1->query($sql);
							if($query){
								
								return  $query->result();
							}
						}
				
				
							else{
									
										$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d')."' and `hotel_id`= '".$this->session->userdata('user_hotel')."' and `t_payment_mode`= '".$t_modes."' and  `transaction_type_id` = '".$t_stat."'";
									$query = $this->db1->query($sql);
								
									if($query){
										
										return  $query->result();
									}
								}
		}	
				
		else{
			
					if($t_stat == 'all'){
					
						$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d')."' and `hotel_id`= '".$this->session->userdata('user_hotel')."' and `t_payment_mode`!= '".$t_modes."'";
					$query = $this->db1->query($sql);
					
					if($query){
						
						return  $query->result();
					}
				}
				else{
					
						$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d')."' and `hotel_id`= '".$this->session->userdata('user_hotel')."' and `t_payment_mode`!= '".$t_modes."' and `transaction_type_id` = '".$t_stat."'";
					$query = $this->db1->query($sql);
					if($query){
						
						return  $query->result();
					}
				}
			
			
			
		}		
			
	}
	// last seven days ....
		else if($t_date == 'seven_days'){
			//echo $t_date;
		//	exit;
			
				if($t_modes != 'all_modes'){
				if($t_stat == 'all'){
					
						
						$sql = "SELECT
								  *
								FROM
								  `hotel_transactions`
								WHERE DATE
								  (`t_date`) > DATE_SUB(NOW(), INTERVAL 7 DAY) AND `hotel_id` = ".$this->session->userdata('user_hotel').""."  and `t_payment_mode`= '".$t_modes."'
								ORDER BY `t_date`  ASC";
								
						$query = $this->db1->query($sql);					
						
						if($query){
							
							return  $query->result();
						}
				}
				else{
					$sql = "SELECT
								  *
							FROM
								  `hotel_transactions`
							WHERE DATE
								  (`t_date`) > DATE_SUB(NOW(), INTERVAL 7 DAY) AND `hotel_id` = ".$this->session->userdata('user_hotel').""."  and `t_payment_mode`= '".$t_modes."' and `transaction_type_id` = '".$t_stat."'
							ORDER BY `t_date`  ASC";
							
					$query = $this->db1->query($sql);
					if($query){
						
						return  $query->result();
					}
				}
				
		}	
		else{
			
					if($t_stat == 'all'){
					
						
								$sql = "SELECT
								  *
								FROM
								  `hotel_transactions`
								WHERE DATE
								  (`t_date`) > DATE_SUB(NOW(), INTERVAL 7 DAY) AND `hotel_id` = ".$this->session->userdata('user_hotel').""."  and `t_payment_mode`!= '".$t_modes."'
								ORDER BY `t_date`  ASC";
								
								
						$query = $this->db1->query($sql);
						if($query){
							
							return  $query->result();
						}
				}
				else{
						
						$sql = "SELECT
								  *
								FROM
								  `hotel_transactions`
								WHERE DATE
								  (`t_date`) > DATE_SUB(NOW(), INTERVAL 7 DAY) AND `hotel_id` = ".$this->session->userdata('user_hotel').""."  and `t_payment_mode`!= '".$t_modes."' and `transaction_type_id` = '".$t_stat."'
								ORDER BY `t_date`  ASC";
								
								
					$query = $this->db1->query($sql);
					if($query){
						
						return  $query->result();
					}
				}
			
			
			
		}
				
		}
		
	}
	
	// start of guest in house function
	
	function guest_in_house(){
		
		//if($g_date == 'today'){
			
			
			$today_date = date('Y-m-d');
			
			//	$sql = "SELECT * FROM `hotel_stay_guest` WHERE booking_id = (select booking_id from `hotel_bookings` where '".$today_date."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and `booking_source` BETWEEN '1' AND '5' and `booking_status_id` in (2,4,5))";
				
				
					$sql = "SELECT
					  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`,bok.`room_id`,bok.`cust_name`,bok.`no_of_guest`,bok.`group_id`,bok.`booking_status_id`,hr.`room_no`,bok.`cust_from_date`,bok.`cust_end_date`,gst.`g_state`,gst.`g_city`,gst.`g_contact_no`,gst.`g_email`,sty.`docVerif`,gst.`g_photo_thumb`,bok.`room_id`,gst.`g_type`,sty.`check_in_date`,sty.`check_out_date`,sty.`status`, bok.`hotel_id` as hotel_id, bok.`user_id`,
					  (CASE WHEN (bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) as status1
					FROM
					  `hotel_stay_guest` as sty
					  LEFT JOIN hotel_guest as gst ON sty.`g_id` = gst.`g_id`
					  LEFT JOIN hotel_bookings as bok ON sty.`booking_id` = bok.`booking_id` 
					  LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
					WHERE
					  bok.booking_status_id IN (5,6) and '".$today_date."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and `booking_source` BETWEEN '1' AND '5'";
				$query = $this->db1->query($sql);
				if($query){
					
					return $query->result();
				}
			
		//}
		
		
		
	}
	// end of function...
	function guest_in_house_ajax($guest_date,$g_type,$g_source){
		
		if($guest_date == 'today'){
	
		$today_date = date('Y-m-d');
			
		if($g_type == 'all_types'){
			if($g_source == 'all_source'){
				
				$sql = "SELECT
					  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`,bok.`room_id`,bok.`cust_name`,bok.`no_of_guest`,bok.`group_id`,bok.`booking_status_id`,bok.`cust_from_date`,bok.`cust_end_date`,gst.`g_state`,gst.`g_city`,gst.`g_contact_no`,gst.`g_email`,sty.`docVerif`,gst.`g_photo_thumb`,bok.`room_id`,gst.`g_type`,sty.`status`,bok.`stay_days`, sty.`stayPurpose`, bok.`hotel_id` as hotel_id, bok.`user_id`,sty.`check_in_date`,sty.`check_out_date`,hr.`room_no`,
					  (CASE WHEN (bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) as status1
					FROM
					  `hotel_stay_guest` as sty
					  LEFT JOIN hotel_guest as gst ON sty.`g_id` = gst.`g_id`
					  LEFT JOIN hotel_bookings as bok ON sty.`booking_id` = bok.booking_id
					  LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
					WHERE
					  bok.booking_status_id IN (5,6) and '".$today_date."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and bok.`booking_source` BETWEEN '1' AND '5' and bok.`hotel_id` = ".$this->session->userdata('user_hotel')."";
			}
			else{
				
				
				
				$sql = "SELECT
					  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`,bok.`room_id`,bok.`cust_name`,bok.`no_of_guest`,bok.`group_id`,bok.`booking_status_id`,bok.`cust_from_date`,bok.`cust_end_date`,gst.`g_state`,gst.`g_city`,gst.`g_contact_no`,gst.`g_email`,sty.`docVerif`,gst.`g_photo_thumb`,bok.`room_id`,gst.`g_type`,sty.`status`, sty.`stayPurpose`, bok.`hotel_id` as hotel_id, bok.`user_id`,sty.`check_in_date`,sty.`check_out_date`,hr.`room_no`,
					  (CASE WHEN (bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) as status1
					FROM
					  `hotel_stay_guest` as sty
					  LEFT JOIN hotel_guest as gst ON sty.`g_id` = gst.`g_id`
					  LEFT JOIN hotel_bookings as bok ON sty.`booking_id` = bok.booking_id
					   LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
					WHERE
					  bok.booking_status_id IN (5,6) and '".$today_date."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and bok.`booking_source` = ".$g_source." and bok.`hotel_id` = ".$this->session->userdata('user_hotel')."";
			}
		}
		// individual guest type
		else{
			
			if($g_source == 'all_source'){
			$sql = "SELECT
					  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`,bok.`room_id`,bok.`cust_name`,bok.`no_of_guest`,bok.`group_id`,bok.`booking_status_id`,bok.`cust_from_date`,bok.`cust_end_date`,gst.`g_state`,gst.`g_city`,gst.`g_contact_no`,gst.`g_email`,sty.`docVerif`,gst.`g_photo_thumb`,bok.`room_id`,gst.`g_type`,sty.`status`, sty.`stayPurpose`, bok.`hotel_id` as hotel_id, bok.`user_id`,sty.`check_in_date`,sty.`check_out_date`,hr.`room_no`,
					  (CASE WHEN (bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) as status1
					FROM
					  `hotel_stay_guest` as sty
					  LEFT JOIN hotel_guest as gst ON sty.`g_id` = gst.`g_id`
					  LEFT JOIN hotel_bookings as bok ON sty.`booking_id` = bok.booking_id
					   LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
					WHERE
					  bok.booking_status_id IN (5,6) and '".$today_date."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and bok.`booking_source` BETWEEN '1' AND '5' and gst.`g_type`='".$g_type."' and bok.`hotel_id` = ".$this->session->userdata('user_hotel')."" ;
			}
			else{
				
				
				
				$sql = "SELECT
					  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`,bok.`room_id`,bok.`cust_name`,bok.`no_of_guest`,bok.`group_id`,bok.`booking_status_id`,bok.`cust_from_date`,bok.`cust_end_date`,gst.`g_state`,gst.`g_city`,gst.`g_contact_no`,gst.`g_email`,sty.`docVerif`,gst.`g_photo_thumb`,bok.`room_id`,gst.`g_type`,sty.`status`, sty.`stayPurpose`, bok.`hotel_id` as hotel_id, bok.`user_id`,sty.`check_in_date`,sty.`check_out_date`,hr.`room_no`,
					  (CASE WHEN (bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) as status1
					FROM
					  `hotel_stay_guest` as sty
					  LEFT JOIN hotel_guest as gst ON sty.`g_id` = gst.`g_id`
					  LEFT JOIN hotel_bookings as bok ON sty.`booking_id` = bok.booking_id
					   LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
					WHERE
					  bok.booking_status_id IN (5,6) and '".$today_date."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and bok.`booking_source` = ".$g_source." and gst.`g_type` = '".$g_type."' and bok.`hotel_id` = ".$this->session->userdata('user_hotel')."";
			}

}
				//echo $sql;exit;
					
				$query = $this->db1->query($sql);
				if($query){
					
					return $query->result();
				}
			
		}
		else if($guest_date == 'yesterday'){
			
			if($g_type == 'all_types'){
				if($g_source == 'all_source'){
					
						$sql = "SELECT
						  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`,bok.`room_id`,bok.`cust_name`,bok.`no_of_guest`,bok.`group_id`,bok.`booking_status_id`,bok.`cust_from_date`,bok.`cust_end_date`,gst.`g_state`,gst.`g_city`,gst.`g_contact_no`,gst.`g_email`,sty.`docVerif`,gst.`g_photo_thumb`,bok.`room_id`,gst.`g_type`,sty.`status`, sty.`stayPurpose`, bok.`hotel_id` as hotel_id, bok.`user_id` as user_id,sty.`check_in_date`,sty.`check_out_date`,hr.`room_no`,
						  (CASE WHEN (bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) as status
						FROM
						  `hotel_stay_guest` as sty
						  LEFT JOIN hotel_guest as gst ON sty.`g_id` = gst.`g_id`
						  LEFT JOIN hotel_bookings as bok ON sty.`booking_id` = bok.booking_id
						  LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
						WHERE
						  bok.booking_status_id IN (5,6) and '".date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and bok.`booking_source` BETWEEN '1' AND '5' and bok.`hotel_id` = ".$this->session->userdata('user_hotel')."";
				}
				else{
					
					$sql = "SELECT
							  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`,bok.`room_id`,bok.`cust_name`,bok.`no_of_guest`,bok.`group_id`,bok.`booking_status_id`,bok.`cust_from_date`,bok.`cust_end_date`,gst.`g_state`,gst.`g_city`,gst.`g_contact_no`,gst.`g_email`,sty.`docVerif`,gst.`g_photo_thumb`,bok.`room_id`,gst.`g_type`,sty.`status`,hr.`room_no`, sty.`stayPurpose`, bok.`hotel_id` as hotel_id, bok.`user_id` as user_id,sty.`check_in_date`,sty.`check_out_date`,
							  (CASE WHEN (bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) as status
							FROM
							  `hotel_stay_guest` as sty
							  LEFT JOIN hotel_guest as gst ON sty.`g_id` = gst.`g_id`
							  LEFT JOIN hotel_bookings as bok ON sty.`booking_id` = bok.booking_id
							  LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
							WHERE
							  bok.booking_status_id IN (5,6) and '".date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and bok.`booking_source` = ".$g_source." and bok.`hotel_id` = ".$this->session->userdata('user_hotel')."";
					
				}
			}
			// specific guest type...
			else{
				
						if($g_source == 'all_source'){
					
						$sql = "SELECT
						  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`,bok.`room_id`,bok.`cust_name`,bok.`no_of_guest`,bok.`group_id`,bok.`booking_status_id`,bok.`cust_from_date`,bok.`cust_end_date`,gst.`g_state`,gst.`g_city`,gst.`g_contact_no`,gst.`g_email`,sty.`docVerif`,gst.`g_photo_thumb`,bok.`room_id`,gst.`g_type`,sty.`status`, sty.`stayPurpose`, bok.`hotel_id` as hotel_id, bok.`user_id` as user_id,sty.`check_in_date`,sty.`check_out_date`,hr.`room_no`,
						  (CASE WHEN (bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) as status
						FROM
						  `hotel_stay_guest` as sty
						  LEFT JOIN hotel_guest as gst ON sty.`g_id` = gst.`g_id`
						  LEFT JOIN hotel_bookings as bok ON sty.`booking_id` = bok.booking_id
						  LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
						WHERE
						  bok.booking_status_id IN (5,6) and '".date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and bok.`booking_source` BETWEEN '1' AND '5' and gst.`g_type`= '".$g_type."' and bok.`hotel_id` = ".$this->session->userdata('user_hotel')."";
				}
				else{
					
					$sql = "SELECT
							  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`,bok.`room_id`,bok.`cust_name`,bok.`no_of_guest`,bok.`group_id`,bok.`booking_status_id`,bok.`cust_from_date`,bok.`cust_end_date`,gst.`g_state`,gst.`g_city`,gst.`g_contact_no`,gst.`g_email`,sty.`docVerif`,gst.`g_photo_thumb`,bok.`room_id`,gst.`g_type`,sty.`status`, sty.`stayPurpose`, bok.`hotel_id` as hotel_id, bok.`user_id` as user_id,sty.`check_in_date`,sty.`check_out_date`,hr.`room_no`,
							  (CASE WHEN (bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) as status
							FROM
							  `hotel_stay_guest` as sty
							  LEFT JOIN hotel_guest as gst ON sty.`g_id` = gst.`g_id`
							  LEFT JOIN hotel_bookings as bok ON sty.`booking_id` = bok.booking_id
							  LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
							WHERE
							  bok.booking_status_id IN (5,6) and '".date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))."' BETWEEN date(`cust_from_date`) AND date(`cust_end_date`) and bok.`booking_source` = ".$g_source." and gst.`g_type`= '".$g_type."' and bok.`hotel_id` = ".$this->session->userdata('user_hotel')."";
					
				}
				
			}			
				$query = $this->db1->query($sql);
				if($query){
					
					return $query->result();
				}
			
		}
		else if($guest_date == 'seven_days'){
			
			if($g_type == 'all_types'){
			if($g_source == 'all_source'){
					$sql = "SELECT
							  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`, bok.`room_id`, bok.`cust_name`, bok.`no_of_guest`,
							  bok.`stay_days`,  bok.`group_id`,  bok.`booking_status_id`,  sty.`check_in_date`,
							  sty.`check_out_date`,  bok.`cust_from_date`, bok.`cust_end_date`, gst.`g_state`, gst.`g_city`, gst.`g_contact_no`, gst.`g_email`, sty.`docVerif`, gst.`g_photo_thumb`, bok.`room_id`,sty.`check_in_date`,sty.`check_out_date`, gst.`g_type`, hr.`room_no`, (CASE WHEN(bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) AS status1, sty.status, sty.stayPurpose, bok.`hotel_id` as hotel_id, bok.`user_id` as user_id
							FROM
							  `hotel_stay_guest` AS sty
							LEFT JOIN
							  hotel_guest AS gst ON sty.`g_id` = gst.`g_id`
							LEFT JOIN
							  hotel_bookings AS bok ON sty.`booking_id` = bok.booking_id
							  LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
							WHERE
							  bok.booking_status_id IN(5,
							  6) AND (DATE(`cust_from_date`) BETWEEN '".date('Y-m-d', strtotime('-7 day', strtotime(date('Y-m-d'))))."' AND '".date('Y-m-d')."'
							  OR DATE(`cust_end_date`) BETWEEN '".date('Y-m-d', strtotime('-7 day', strtotime(date('Y-m-d'))))."' AND '".date('Y-m-d')."')
							  AND bok.`booking_source` BETWEEN '1' AND '5' AND bok.`hotel_id` = '".$this->session->userdata("user_hotel")."'";
							  
			}
			else{
				
						$sql = "SELECT
							  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`, bok.`room_id`, bok.`cust_name`, bok.`no_of_guest`,
							  bok.`stay_days`,  bok.`group_id`,  bok.`booking_status_id`,  sty.`check_in_date`,
							  sty.`check_out_date`,  bok.`cust_from_date`, bok.`cust_end_date`, gst.`g_state`, gst.`g_city`, gst.`g_contact_no`, gst.`g_email`, sty.`docVerif`, gst.`g_photo_thumb`, bok.`room_id`, sty.`check_in_date`,sty.`check_out_date`, gst.`g_type`, hr.`room_no`, (CASE WHEN(bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) AS status1, sty.status, sty.stayPurpose, bok.`hotel_id` as hotel_id, bok.`user_id` as user_id
							FROM
							  `hotel_stay_guest` AS sty
							LEFT JOIN
							  hotel_guest AS gst ON sty.`g_id` = gst.`g_id`
							LEFT JOIN
							  hotel_bookings AS bok ON sty.`booking_id` = bok.booking_id
							   LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
							WHERE
							  bok.booking_status_id IN(5,
							  6) AND (DATE(`cust_from_date`) BETWEEN '".date('Y-m-d', strtotime('-7 day', strtotime(date('Y-m-d'))))."' AND '".date('Y-m-d')."'
							  OR DATE(`cust_end_date`) BETWEEN '".date('Y-m-d', strtotime('-7 day', strtotime(date('Y-m-d'))))."' AND '".date('Y-m-d')."')
							  AND bok.`booking_source` = ".$g_source." and bok.`hotel_id` = '".$this->session->userdata("user_hotel")."'";
			}
			}
			// specific guest type..
			else{
				
				
					if($g_source == 'all_source'){
					$sql = "SELECT
							  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`, bok.`room_id`, bok.`cust_name`, bok.`no_of_guest`,
							  bok.`stay_days`,  bok.`group_id`,  bok.`booking_status_id`,  sty.`check_in_date`,
							  sty.`check_out_date`,  bok.`cust_from_date`, bok.`cust_end_date`, gst.`g_state`, gst.`g_city`, gst.`g_contact_no`, gst.`g_email`, sty.`docVerif`, gst.`g_photo_thumb`, bok.`room_id`,hr.`room_no`, gst.`g_type`,sty.`check_in_date`,sty.`check_out_date`, (CASE WHEN(bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) AS status1, sty.status, sty.stayPurpose, bok.`hotel_id` as hotel_id, bok.`user_id` as user_id
							FROM
							  `hotel_stay_guest` AS sty
							LEFT JOIN
							  hotel_guest AS gst ON sty.`g_id` = gst.`g_id`
							LEFT JOIN
							  hotel_bookings AS bok ON sty.`booking_id` = bok.booking_id
							   LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
							WHERE
							  bok.booking_status_id IN(5,
							  6) AND (DATE(`cust_from_date`) BETWEEN '".date('Y-m-d', strtotime('-7 day', strtotime(date('Y-m-d'))))."' AND '".date('Y-m-d')."'
							  OR DATE(`cust_end_date`) BETWEEN '".date('Y-m-d', strtotime('-7 day', strtotime(date('Y-m-d'))))."' AND '".date('Y-m-d')."')
							  AND bok.`booking_source` BETWEEN '1' AND '5' and bok.`hotel_id` = '".$this->session->userdata("user_hotel")."' and gst.`g_type`= '".$g_type."'";
							  
			}
			else{
				
						$sql = "SELECT
							  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`, bok.`room_id`, bok.`cust_name`, bok.`no_of_guest`,
							  bok.`stay_days`,  bok.`group_id`,  bok.`booking_status_id`,  sty.`check_in_date`,
							  sty.`check_out_date`,  bok.`cust_from_date`, bok.`cust_end_date`, gst.`g_state`, gst.`g_city`, gst.`g_contact_no`, gst.`g_email`, sty.`docVerif`, gst.`g_photo_thumb`, bok.`room_id`, gst.`g_type`,sty.`check_in_date`,sty.`check_out_date`,hr.`room_no`, (CASE WHEN(bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) AS status1, sty.status, sty.stayPurpose, bok.`hotel_id` as hotel_id, bok.`user_id` as user_id
							FROM
							  `hotel_stay_guest` AS sty
							LEFT JOIN
							  hotel_guest AS gst ON sty.`g_id` = gst.`g_id`
							LEFT JOIN
							  hotel_bookings AS bok ON sty.`booking_id` = bok.booking_id
							   LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
							WHERE
							  bok.booking_status_id IN(5,
							  6) AND (DATE(`cust_from_date`) BETWEEN '".date('Y-m-d', strtotime('-7 day', strtotime(date('Y-m-d'))))."' AND '".date('Y-m-d')."'
							  OR DATE(`cust_end_date`) BETWEEN '".date('Y-m-d', strtotime('-7 day', strtotime(date('Y-m-d'))))."' AND '".date('Y-m-d')."')
							  AND bok.`booking_source` = ".$g_source." and gst.`g_type`= '".$g_type."' and bok.`hotel_id` = '".$this->session->userdata("user_hotel")."'";
			}
						
				
				
				
				
			}

				$query = $this->db1->query($sql);
				
				if($query){
					return $query->result();
				}
		}
		
	}
	
	
	
	
	
	
	function guest_specific_date_ajax($start_date_guest,$end_date_guest,$g_type,$g_source){
		
			
		if($g_type == 'all_types'){
			if($g_source == 'all_source'){
				
				$sql = "SELECT
					  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`,bok.`room_id`,bok.`cust_name`,bok.`no_of_guest`,bok.`group_id`,bok.`booking_status_id`,bok.`cust_from_date`,bok.`cust_end_date`,gst.`g_state`,gst.`g_city`,gst.`g_contact_no`,gst.`g_email`,sty.`docVerif`,gst.`g_photo_thumb`,bok.`room_id`,gst.`g_type`,sty.`status`,bok.`stay_days`, sty.`stayPurpose`, bok.`hotel_id` as hotel_id, bok.`user_id`,sty.`check_in_date`,sty.`check_out_date`,hr.`room_no`,
					  (CASE WHEN (bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) as status1
					FROM
					  `hotel_stay_guest` as sty
					  LEFT JOIN hotel_guest as gst ON sty.`g_id` = gst.`g_id`
					  LEFT JOIN hotel_bookings as bok ON sty.`booking_id` = bok.booking_id
					  LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
					WHERE
					  bok.booking_status_id IN (5,6) and date(`cust_from_date`) BETWEEN '".$start_date_guest ."' and '".$end_date_guest ."' and bok.`booking_source` BETWEEN '1' AND '5' and bok.`hotel_id` = ".$this->session->userdata('user_hotel')."";
			}
			else{
				
				
				
				$sql = "SELECT
					  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`,bok.`room_id`,bok.`cust_name`,bok.`no_of_guest`,bok.`group_id`,bok.`booking_status_id`,bok.`cust_from_date`,bok.`cust_end_date`,gst.`g_state`,gst.`g_city`,gst.`g_contact_no`,gst.`g_email`,sty.`docVerif`,gst.`g_photo_thumb`,bok.`room_id`,gst.`g_type`,sty.`status`, sty.`stayPurpose`, bok.`hotel_id` as hotel_id, bok.`user_id`,sty.`check_in_date`,sty.`check_out_date`,hr.`room_no`,
					  (CASE WHEN (bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) as status1
					FROM
					  `hotel_stay_guest` as sty
					  LEFT JOIN hotel_guest as gst ON sty.`g_id` = gst.`g_id`
					  LEFT JOIN hotel_bookings as bok ON sty.`booking_id` = bok.booking_id
					   LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
					WHERE
					  bok.booking_status_id IN (5,6) and date(`cust_from_date`) BETWEEN '".$start_date_guest ."' and '".$end_date_guest ."' and bok.`booking_source` = ".$g_source." and bok.`hotel_id` = ".$this->session->userdata('user_hotel')."";
			}
		}
		// individual guest type
		else{
			
			if($g_source == 'all_source'){
			$sql = "SELECT
					  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`,bok.`room_id`,bok.`cust_name`,bok.`no_of_guest`,bok.`group_id`,bok.`booking_status_id`,bok.`cust_from_date`,bok.`cust_end_date`,gst.`g_state`,gst.`g_city`,gst.`g_contact_no`,gst.`g_email`,sty.`docVerif`,gst.`g_photo_thumb`,bok.`room_id`,gst.`g_type`,sty.`status`, sty.`stayPurpose`, bok.`hotel_id` as hotel_id, bok.`user_id`,sty.`check_in_date`,sty.`check_out_date`,hr.`room_no`,
					  (CASE WHEN (bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) as status1
					FROM
					  `hotel_stay_guest` as sty
					  LEFT JOIN hotel_guest as gst ON sty.`g_id` = gst.`g_id`
					  LEFT JOIN hotel_bookings as bok ON sty.`booking_id` = bok.booking_id
					   LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
					WHERE
					  bok.booking_status_id IN (5,6) and date(`cust_from_date`) BETWEEN '".$start_date_guest ."' and '".$end_date_guest ."' and bok.`booking_source` BETWEEN '1' AND '5' and gst.`g_type`='".$g_type."' and bok.`hotel_id` = ".$this->session->userdata('user_hotel')."" ;
			}
			else{
				
				
				
				$sql = "SELECT
					  sty.`g_id`, gst.g_name, bok.booking_id, sty.`addEvent`,bok.`room_id`,bok.`cust_name`,bok.`no_of_guest`,bok.`group_id`,bok.`booking_status_id`,bok.`cust_from_date`,bok.`cust_end_date`,gst.`g_state`,gst.`g_city`,gst.`g_contact_no`,gst.`g_email`,sty.`docVerif`,gst.`g_photo_thumb`,bok.`room_id`,gst.`g_type`,sty.`status`, sty.`stayPurpose`, bok.`hotel_id` as hotel_id, bok.`user_id`,sty.`check_in_date`,sty.`check_out_date`,hr.`room_no`,
					  (CASE WHEN (bok.booking_status_id = 6) THEN 'checkout' ELSE 'checkin' END) as status1
					FROM
					  `hotel_stay_guest` as sty
					  LEFT JOIN hotel_guest as gst ON sty.`g_id` = gst.`g_id`
					  LEFT JOIN hotel_bookings as bok ON sty.`booking_id` = bok.booking_id
					   LEFT JOIN hotel_room as hr ON bok.`room_id` = hr.`room_id`
					WHERE
					  bok.booking_status_id IN (5,6) and date(`cust_from_date`) BETWEEN '".$start_date_guest ."' and '".$end_date_guest ."' and bok.`booking_source` = ".$g_source." and gst.`g_type` = '".$g_type."' and bok.`hotel_id` = ".$this->session->userdata('user_hotel')."";
			}

}
				//echo $sql;exit;
					
				$query = $this->db1->query($sql);
				if($query){
					
					return $query->result();
				}
		
		
	}
	//end of function
	
	
	// only the name changed need be change the whole...
	function get_revenue_specific_date($t_date,$t_stat,$t_modes){
		
		if($t_date == 'Yesterday'){
			if($t_modes != 'all_modes'){
				if($t_stat == 'all'){
					
						$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))."' and `hotel_id`= '".$this->session->userdata('user_hotel')."' and `t_payment_mode`= '".$t_modes."'";
					$query = $this->db1->query($sql);
					if($query){
						
						return  $query->result();
					}
				}
				else{
					
						$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))."' and `hotel_id`= '".$this->session->userdata('user_hotel')."' and `t_payment_mode`= '".$t_modes."' and `transaction_type_id` = '".$t_stat."'";
					$query = $this->db1->query($sql);
					if($query){
						
						return  $query->result();
					}
				}
				
		}	
		else{
			
					if($t_stat == 'all'){
					
						$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))."' and `hotel_id`= '".$this->session->userdata('user_hotel')."' and `t_payment_mode`!= '".$t_modes."'";
					$query = $this->db1->query($sql);
					if($query){
						
						return  $query->result();
					}
				}
				else{
					
						$sql = "SELECT * FROM `hotel_transactions` WHERE date(`t_date`) = '".date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))."' and `hotel_id`= '".$this->session->userdata('user_hotel')."' and `t_payment_mode`!= '".$t_modes."' and `transaction_type_id` = '".$t_stat."'";
					$query = $this->db1->query($sql);
					if($query){
						
						return  $query->result();
					}
				}
			
			
			
		}		
			
			
		}
	
	
	}//end of function
	
	
	function getAllCountry(){

		$sql ="SELECT * FROM `area_country`";

		

		$qry = $this->db->query($sql);

		

		if($qry->num_rows()>0){

			return $qry->result();

		}else{

			return 0;

		}

		

	}
	
	function getAllState(){

		$sql ="SELECT `state_id`,`country_id`,`Name`,`Type` FROM `area_state`";

		$qry = $this->db->query($sql);

		if($qry->num_rows()>0){

			return $qry->result();

		}else{

			return 0;

		}
	}
	
	
	function getPosSettings(){

		$sql ="SELECT * FROM `pos_settings` WHERE `hotel_id` = ".$this->session->userdata('user_hotel')."";

		$qry = $this->db1->query($sql);

		if($qry->num_rows()>0){

			return $qry->row();

		}else{

			return 0;

		}
	}
	
	
	function auto_generate_inv_id(){
	    
	    
	   $sql ="SELECT * FROM 
(SELECT * FROM 
(SELECT
    `id` as id, 'grp_booking' as type, `status` as status, STR_TO_DATE(start_date, '%m/%d/%Y') as start_date, STR_TO_DATE(end_date, '%m/%d/%Y') as end_date,`name` as g_name
FROM
    `hotel_group`
    where `hotel_id` =". $this->session->userdata('user_hotel')."
ORDER BY 
 `end_date` ASC, `id` ASC) as grp
UNION ALL
SELECT * FROM 
(SELECT
    `booking_id` as id, 'booking' as type, `booking_status_id` as status, DATE(`cust_from_date`) as start_date, DATE(`cust_end_date`) as end_date,`cust_name` as g_name
FROM
    `hotel_bookings`
      where `hotel_id` =". $this->session->userdata('user_hotel')." AND `group_id` = 0
ORDER BY
 DATE(`cust_end_date`) ASC, `booking_id` ASC) as sin) as a
WHERE start_date > 0 AND end_date > 0 AND status IN (6)
ORDER BY end_date, id";

$getresult = $this->db1->query($sql);
if($getresult->num_rows()>0){
    
    return $getresult->result();
}
	    
	}
	
	function insert_auto_inv_id($data,$cnt){
	    
	    if($cnt == 1){
	        $sql = "TRUNCATE invoice_master";
		    $trun_result = $this->db1->query($sql);
	    }
		
		

		$val = $this->db1->insert('invoice_master',$data);
			if($val){
				
				return "success";
			}
			else{
				return "failed";
			}
		}
		
		
		
	
}// end of class
?>