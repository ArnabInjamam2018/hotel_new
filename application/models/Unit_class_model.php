<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class unit_class_model extends CI_Model {
    
	//Get all the value
	function all_unit_clsss(){
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		//$this->db1->where('user_id',$this->session->userdata('user_id'));  
		$this->db1->from(TABLE_PRE.'unit_class');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	  function all_unit_class_model_add($data){

    	$query=$this->db1->insert(TABLE_PRE.'unit_type_name',$data);

        if($query){

            return true;
        }
        else{

          return false;
        }
    	     
    }
	
	function all_unit_class_add($data){

    	$query=$this->db1->insert(TABLE_PRE.'unit_class',$data);

        if($query){

            return true;
        }
        else{

          return false;
        }
    	     
    }
	
	function set_upload_optionsGst() {
        $config = array(
            'upload_path' => './upload/guest/originals/photo/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'overwrite' => FALSE
        );
		
        return $config;
    }
	function add_quick_room($data){

		foreach($data['room_no'] as $key => $val){
			
			/*
			 $this->upload->initialize($this->set_upload_optionsGst());
                if ($this->upload->do_upload('image'))   {
                    /*  Image Upload 18.11.2015 */
					
                  /*  $room_image = $this->upload->data('file_name');
                    $filename = $room_image;
                    $source_path = './upload/unit/original/image1/' . $filename;
                    $target_path = './upload/unit/thumb/image1/';

                    $config_manip = array(
                        'image_library' => 'gd2',
                        'source_image' => $source_path,
                        'new_image' => $target_path,
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'width' => 250,
                        'height' => 250
                    );
                   // $this->load->library('image_lib', $config_manip);
                         $this->image_lib->initialize($config_manip);
                    if (!$this->image_lib->resize()) {
                        $this->session->set_flashdata('err_msg', $this->image_lib->display_errors());
                        redirect('dashboard/add_guest');
                    }
                    // clear //
                    $thumb_name = explode(".", $filename);
                    $room_thumb = $thumb_name[0] . $config_manip['thumb_marker'] . "." . $thumb_name[1];
                    $this->image_lib->clear();
					
					
                } else {
                    $room_image = "";
                    $room_thumb = "";
                } 
			*/
			
			
			$room_no=$data['room_no'][$key];
			$room_name=$data['room_name'][$key];
			$unit_id=$data['unit_id'][$key];
			$room_bed=$data['room_bed'][$key];
//			$max_occupancy=$data['max_occupancy'][$key];
			$floor_id=$data['floor_no'][$key];
			$hotel_id=$this->session->userdata('user_hotel');
			//$floor_id='8';
			
			$abc=array(
								//'room_image'=>$room_image,
								//'room_image_thumb'=>$room_thumb,
								'room_no'=>$room_no,
                                'room_name'=>$room_name,
                                'unit_id'=>$unit_id,
                                'room_bed'=>$room_bed,
								//'max_occupancy'=>$max_occupancy,
								'hotel_id'=>$hotel_id,
								'floor_no'=>$floor_id,
								'hotel_id'=>$this->session->userdata('user_hotel'),
								'user_id'=>$this->session->userdata('user_id')
								);
								//echo "<pre>";
								//print_r($abc);
								//exit;
								
    	$query=$this->db1->insert(TABLE_PRE.'room',$abc);
		}
        if($query){

            return true;
        }
        else{

          return false;
        }
    	     
    }
	
	
	
	
	
	
	function delete_class_type_model($id){
		
		$this->db1->where('hotel_unit_class_id',$id);
              $query=$this->db1->delete('hotel_unit_class');
             
	}
	function fetch_id($id){
 
        $this->db1->select('*');
        $this->db1->from('hotel_unit_class');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('hotel_unit_class_id',$id);
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	
	function update_data_model($data){

            $id=$data['hotel_unit_class_id'];
              $this->db1->where('hotel_unit_class_id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'unit_class_name',$data);

              if($query){

                return true;
              }
   }
   
   function update_class($data){

            $id=$data['hotel_unit_class_id'];
              $this->db1->where('hotel_unit_class_id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'unit_class',$data);

              if($query){

                return true;
              }
   }
   
   
   //all unit type category
   
   function all_unit_type_category_data(){
 
        $this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		//$this->db1->where('user_id',$this->session->userdata('user_id'));  
        $this->db1->from('hotel_unit_type_name');
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	function unit_type_fetch_id($id){
 
        $this->db1->select('*');
        $this->db1->from('hotel_unit_type_name');
        $this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	function unit_type_fetch_id1($id){
		
		$sql="select * from hotel_unit_type_name left join hotel_unit_type on hotel_unit_type.unit_cate_id=hotel_unit_type_name.id where hotel_unit_type.id=$id";
		$query=$this->db1->query($sql);
        return $query->row();
		
    }
	
	//update all unit type category
	
	function update1($data){

            $id=$data['id'];
              $this->db1->where('id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update('hotel_unit_type_name',$data);

              if($query){

                return true;
              }
   }
   
   //delete
   function delete_unit_class_type($id){
		
		$this->db1->where('id',$id);
              $query=$this->db1->delete('hotel_unit_type_name');
             
	}
	
	//LAUNDRY SECTION 
	
	function hotel_laundry_type(){
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'laundry_type');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	
	 function model_add_hotel_laundry_type($data){

    	$query=$this->db1->insert('hotel_laundry_type',$data);

        if($query){

            return true;
        }
        else{

          return false;
        }
    	     
    }
	
	function delete_laundry_type($id){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('laundry_type_id',$id);
              $query=$this->db1->delete('hotel_laundry_type');
             
	}
	
	//fetch id from table hotel_laundry_type
   function laundry_fetch_id($id){
 
        $this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->from('hotel_laundry_type');
        $this->db1->where('laundry_type_id',$id);
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	
	//update all laundry type
	
	function update_laundry_type($data){

            $id=$data['laundry_type_id'];
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $this->db1->where('laundry_type_id',$id);
              $query=$this->db1->update('hotel_laundry_type',$data);

              if($query){

                return true;
              }
   }
   
   
   //laundry fabric section
   
   
   //LAUNDRY SECTION 
	
	function hotel_laundry_fabric_type(){
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'laundry_fabric_type');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	//add fabric
	function model_add_hotel_laundry_fabric_type($data){

    	$query=$this->db1->insert('hotel_laundry_fabric_type',$data);

        if($query){

            return true;
        }
        else{

          return false;
        }
    	     
    }
	
	//delete fabric
	function delete_laundry_fabric_type($id){
		
		$this->db1->where('fabric_id',$id);
              $query=$this->db1->delete('hotel_Laundry_fabric_type');
             
	}
	
	//fetch id from table hotel_laundry_fabric_type
   function laundry_fetch_fabric_id($id){
 
        $this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->from('hotel_Laundry_fabric_type');
        $this->db1->where('fabric_id',$id);
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	
	//update all laundry fabric type
	
	function update_laundry_fabric_type($data){

            $id=$data['fabric_id'];
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $this->db1->where('fabric_id',$id);
              $query=$this->db1->update('hotel_Laundry_fabric_type',$data);

              if($query){

                return true;
              }
   }
   
   //----------------------------------------------------------------------------
   //fetch id from table hotel_laundry_line_item
   
	
	function hotel_laundry_line_item(){
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'laundry_line_item');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}



  
	
	//
	//delete fabric
	function delete_hotel_laundry_line_item($id){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('laundry_id',$id);
              $query=$this->db1->delete(TABLE_PRE.'laundry_line_item');
             
	}

	
	function model_add_hotel_laundry_line_item($data){

      $query=$this->db1->insert(TABLE_PRE.'laundry_line_item',$data);

        if($query){

            return true;
        }
        else{

          return false;
        }
           
    }


//insert amenities
     function add_amenities($data){

      $query=$this->db1->insert('hotel_aminity',$data);

        if($query){

            return true;
        }
        else{

          return false;
        }
           
    }
	
	//show all amenities
	function all_Amenities()
    {
        $this->db1->select('*');
		$this->db1->from(TABLE_PRE.'aminity');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
    }
	
	
	//delete AMENITIES
	//delete fabric
	function delete_amenities($id){
		
		$this->db1->where('id',$id);
              $query=$this->db1->delete(TABLE_PRE.'aminity');
             
	}
	
	
	//fetch id from table: hotel_aminity
   function fetch_amenities($id){
 
        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'aminity');
        $this->db1->where('id',$id);
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	
	//update Hotel AMENITIES
	
	function update_amenities($data){

            $id=$data['id'];
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $this->db1->where('id',$id);
              $query=$this->db1->update('hotel_Laundry_fabric_type',$data);

              if($query){

                return true;
              }
   }
   
	//show all admin from admin details
	function all_admin()
    {
        $this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'admin_details');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
    }
	
	//add warehouse
	function add_warehouse($data){
    	$query=$this->db1->insert(TABLE_PRE.'warehouse',$data);
        if($query){
            return true;
        }
        else{
          return false;
        }     
    }
	
	
	//show all warehouse
	function all_warehouse()
    {
        $this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
     //   $this->db1->where('user_id',$this->session->userdata('user_id'));  
		$this->db1->from(TABLE_PRE.'warehouse');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
    }
	
	//delete fabric
	function delete_warehouse($id){
		
		$this->db1->where('id',$id);
              $query=$this->db1->delete(TABLE_PRE.'warehouse');
             
	}
	
	//fetch dat from hotel_warehouse
   function data_warehouse($id){
 
        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'warehouse');
        $this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	//update all warehouse
	
	function update_warehouse($data){

              $id=$data['id'];
              $this->db1->where('id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'warehouse',$data);

              if($query){

                return true;
              }
   }
   
   //add add_Stock_inventory_category
	function add_Stock_inventory_category($data){

    	$query=$this->db1->insert(TABLE_PRE.'stock_inventory_category',$data);

        if($query){

            return true;
        }
        else{

          return false;
        }
    }
	
	
	//show all stock_inventory_category
	function all_stock_inventory_category()
    {
        $this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
      //  $this->db1->where('user_id',$this->session->userdata('user_id'));  
		$this->db1->from(TABLE_PRE.'stock_inventory_category');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
    }
	
	
	//delete delete_stock_inventory_category
	function delete_stock_inventory_category($id){
		
		$this->db1->where('id',$id);
              $query=$this->db1->delete(TABLE_PRE.'stock_inventory_category');
             
	}
	
	
	//fetch data from hotel_stock_inventory_category
   function data_hotel_stock_inventory_category($id){
 
        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'stock_inventory_category');
        $this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	
	//update stock inventory category
	function update_stock_inventory_category($data){

              $id=$data['id'];
              $this->db1->where('id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'stock_inventory_category',$data);

              if($query){

                return true;
              }
   }
   
   //all section
   function all_section(){
		$this->db1->select('*');
		//$this->db1->where('user_id',$this->session->userdata('user_id'));
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'section');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	//insert amenities
     function add_section($data){

      $query=$this->db1->insert(TABLE_PRE.'section',$data);

        if($query){

            return true;
        }
        else{

          return false;
        }
           
    }
	
	
	//delete hotel section
	function delete_section($id){
		
		$this->db1->where('sec_id',$id);
              $query=$this->db1->delete(TABLE_PRE.'section');
             
	}
	
	//fetch data from hotel_section
   function edit_section($id){
	 $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'section');
        $this->db1->where('sec_id',$id);
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }



    }
	
	
	
	//update update_hotel_section
	function update_hotel_section($data){

              $id=$data['sec_id'];
              $this->db1->where('sec_id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'section',$data);

              if($query){

                return true;
              }
   }
	
	//LAUNDRY SECTION 
	
	function booking_nature_visit(){
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'booking_nature_visit');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	function active_booking_nature_visit($hotel_id){
		$this->db1->select('*');
		$this->db1->where('status','1');
		$this->db1->where('hotel_id',$hotel_id);
		$this->db1->from(TABLE_PRE.'booking_nature_visit');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	function all_marketing_personnel(){
		$this->db1->select('*');
		//$this->db1->where('status','1');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'marketing_personnel');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	function inactive_booking_nature_visit(){
		$this->db1->select('*');
		$this->db1->where('status','0');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'booking_nature_visit');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	//delete booking nature visit
	function delete_booking_nature_visit($id){
		
		$this->db1->where('booking_nature_visit_id',$id);
              $query=$this->db1->delete(TABLE_PRE.'booking_nature_visit');
             
	}
	
	//fetch data from booking nature visit
   function get_booking_nature_visit($id){
	 $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'booking_nature_visit');
        $this->db1->where('sec_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
   }
   
   
   //fetch dat from booking nature visit
   function data_booking_nature_visit($id){
 //echo $id;exit();
        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'booking_nature_visit');
        $this->db1->where('booking_nature_visit_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get();
        if($query){

            return $query->row();
        }
        else{

            return false;
        }


    }

//update all warehouse
	
	function update_booking_naure_visit($data){

              $id=$data['booking_nature_visit_id'];
              $this->db1->where('booking_nature_visit_id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'booking_nature_visit',$data);

              if($query){

                return $id;
              }
   }
   
   function add_booking_nature_visit($data){

    	$query=$this->db1->insert(TABLE_PRE.'booking_nature_visit',$data);
		$last_id=$this->db1->insert_id();
        if($query){

            return $last_id;
        }
        else{

          return false;
        }
    	     
    }
	
   
   function get_booking_source(){
		$this->db1->select('*');
		$this->db1->where('status','1');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'booking_source');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	function hotel_area_code(){
		//$this->db1->select('*');
		$this->db1->where('area_1','goa');
		//$this->db1->from(TABLE_PRE.'area_code');//TABLE_PRE="hotel_"
		//$query=$this->db1->get();
		//$this->db1->limit(5000);
		$query = $this->db1->get(TABLE_PRE.'area_code'); 
        if($query){
            return $query->result();
        } else {
            return false;
        }
	}
	
	function hotel_area_code_filter($state){
		$this->db1->select('*');
		$this->db1->where('area_1',$state);
		$this->db1->from(TABLE_PRE.'area_code');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
		//$query = $this->db1->get(TABLE_PRE.'area_code'); 
        if($query){
            return $query->result();
        } else {
            return false;
        }
	}
	
	 function get_booking_source1(){
		$this->db1->select('*');
		//$this->db1->where('booking_default','0');
		$this->db1->where('status','1');
		$this->db1->from(TABLE_PRE.'booking_source');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	function default_booking_source(){
		$this->db1->select('*');
		//$this->db1->where('booking_default','1');
		//$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'booking_source');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->row();
        }
        else{

           return $query->row();
        }
		
	}
	
	 
   function get_booking_source_inactive(){
		$this->db1->select('*');
		$this->db1->where('status','0');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'booking_source');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	
	//delete booking nature visit
	function delete_booking_source($id){
		
		$this->db1->where('booking_source_id',$id);
              $query=$this->db1->delete(TABLE_PRE.'booking_source');
             
	}
	
	 //fetch dat from booking nature visit
   function data_booking_source($id){
 
        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'booking_source');
        $this->db1->where('booking_source_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	
	//update all warehouse
	
	function update_booking_source($data,$id){

             // $id=$data['booking_source_id'];
              $this->db1->where('booking_source_id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'booking_source',$data);

              if($query){

                return $id;
              }
			  else{
				  return false;
			  }
   }
   
   
   

    	function add_booking_source($data){
	   $query=$this->db1->insert(TABLE_PRE.'booking_source',$data);
	   
	$last_id=$this->db1->insert_id();
	//$this->db1->where('id',!$last_id);
	//$query=$this->db1->update(TABLE_PRE.'profit_center',$data);
	
	
	
	
	
		if($query){
		return $last_id;
		}
		

    	     
    }
	
	
	function update_booking_source_default($a){
	$b=$a;
	$b--;
	while($b) {
    $sql = "UPDATE ".TABLE_PRE."booking_source SET booking_default ='0' WHERE booking_source_id =".$b." hotel_id=".$this->session->userdata('user_hotel');
    $this->db1->query($sql);
	
	/* $this->db1->where('profit_center_default','0');
              $query=$this->db1->update(TABLE_PRE.'booking_source',$data);
	*/
	$b--;
	}
	
}
	
	
    function change_profit_center_status($id){
		
		
		$this->db1->select('status');
        $this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->get(TABLE_PRE.'profit_center');
	 //  $query=$this->db1->get();
        if($query->num_rows()>0){

           $query2=$query->row();
		  
        }
        
		  if($query2->status=='1'){
		  $data = array(
            'status' =>'0',

        );
		  }
		  else{
			   $data = array(
            'status' =>'1',

        );
			  
		  }
			
		$this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		 $query3=$this->db1->update(TABLE_PRE.'profit_center',$data);
		
		
		 if( $query3){

                return $query2->status;
              }
			  else{
				  return false;
			  }
		//return $a->status;
			/*$this->db1->where('id',$id);
			 $query=$this->db1->update(TABLE_PRE.'profit_center',$data);
		
		
		 if($query){

                return true;
              }
			  else{
				  return false;
			  }*/
		
	}
	
	
	function check_booking_source_default($a){
 
        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'booking_source');
        $this->db1->where('booking_default',$a);
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get();
        if($query->num_rows()>0){

            return true;
        }
        else{

            return false;
        }
    }
	
	
	
	
	
		function booking_source_active(){
			$this->db1->select('*');
			$this->db1->where('status','1');
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$query=$this->db1->get(TABLE_PRE.'booking_source');
			if($query){

            return $query->result();
        }
        else{

            return false;
			}
		}
	
function change_booking_source_status($id){
		
		$this->db1->select('status');
        $this->db1->where('booking_source_id',$id);
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->get(TABLE_PRE.'booking_source');
	 //  $query=$this->db1->get();
        if($query->num_rows()>0){

           $query2=$query->row();
		  
        }
        
		  if($query2->status=='1'){
		  $data = array(
            'status' =>'0',

        );
		  }
		  else{
			   $data = array(
            'status' =>'1',

        );
			  
		  }
			
		$this->db1->where('booking_source_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		 $query3=$this->db1->update(TABLE_PRE.'booking_source',$data);
		
		
		 if( $query3){

                return $query2->status;
              }
			  else{
				  return false;
			  }
		//return $a->status;
	/*	if($status=='1'){
			$this->db1->where('booking_source_id',$id);
			 $query=$this->db1->update(TABLE_PRE.'booking_source',$data);
		}
		else{
			$this->db1->where('booking_source_id',$id);
			$query=$this->db1->update(TABLE_PRE.'booking_source',$data1);
		}
		
		 if($query){

                return true;
              }
			  else{
				  return false;
			  }*/
		
	}
	
	function A1ctive_booking_nature_visit(){
		$this->db1->select('*');
		$this->db1->where('status','1');
		$this->db1->from(TABLE_PRE.'booking_nature_visit');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	 function check_nature_visit_default($a){
 
        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'booking_nature_visit');
        $this->db1->where('nature_visit_default',$a);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get();
        if($query->num_rows()>0){

            return true;
        }
        else{

            return false;
        }
    }
	
	function update_booking_nature_visit_default($a){
	$b=$a;
	$b--;
	while($b) {
    $sql = "UPDATE ".TABLE_PRE."booking_nature_visit SET nature_visit_default ='0' WHERE booking_nature_visit_id =".$b." and hotel_id=".$this->session->userdata('user_hotel') ;
    $this->db1->query($sql);
	
	/* $this->db1->where('profit_center_default','0');
              $query=$this->db1->update(TABLE_PRE.'booking_source',$data);
	*/
	$b--;
	}
	
}

function booking_nature_visit_status($id){
		$this->db1->select('status');
        $this->db1->where('booking_nature_visit_id',$id);
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->get(TABLE_PRE.'booking_nature_visit');
	 //  $query=$this->db1->get();
        if($query->num_rows()>0){

           $query2=$query->row();
		  
        }
        
		  if($query2->status=='1'){
		  $data = array(
            'status' =>'0',

        );
		  }
		  else{
			   $data = array(
            'status' =>'1',

        );
			  
		  }
			
		$this->db1->where('booking_nature_visit_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		 $query3=$this->db1->update(TABLE_PRE.'booking_nature_visit',$data);
		
		
		 if( $query3){

                return $query2->status;
              }
			  else{
				  return false;
			  }
		
		
		
	}
	
	
	
	function profit_center_default(){
		$this->db1->select('*');
        $this->db1->from(TABLE_PRE.'profit_center');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('profit_center_default','1');
        $query=$this->db1->get();
        if($query->num_rows()>0){
           return $query->row();
        }
        else{
           return $query->row();
        }
	}
	
	function state(){
		$this->db1->distinct();
		$this->db1->select('area_1');
		$query=$this->db1->get(TABLE_PRE.'area_code');
		return $query->result();
		
	}
	
	
	function delete_hotel_area_code($id){
	$this->db1->where('id',$id);
        $query=$this->db1->delete(TABLE_PRE.'area_code');
        if($query){
            return true;
        }else{
            return false;
        }
}

function add_hotel_area_code($data){

    	$query=$this->db1->insert(TABLE_PRE.'area_code',$data);

        if($query){

            return true;
        }
        else{

          return false;
        }
    	     
    }
	
	function edit_update_booking_source($a){
	$b=$a;
	//$b--;
	//while($b) {
    $sql = "UPDATE ".TABLE_PRE."booking_source SET booking_default ='0' WHERE 	booking_default='1' and booking_source_id!=".$a;
    $this->db1->query($sql);
	
	
}

function edit_update_nature_visit($a){
	
    $sql = "UPDATE ".TABLE_PRE."booking_nature_visit SET 	nature_visit_default ='0' WHERE nature_visit_default='1' and booking_nature_visit_id!=".$a;
    $this->db1->query($sql);
	
	
	
}

 function fetch_area_code($id){
		$this->db1->select('*');
        $this->db1->from(TABLE_PRE.'area_code');
        $this->db1->where('id',$id);
        $query=$this->db1->get();
        if($query->num_rows()>0){

           return $query->row();
        }
        else{

           return false;
        }
		
	}
	
	//update all laundry type
	
	function update_area_code($data){

            $id=$data['id'];
              $this->db1->where('id',$id);
              $query=$this->db1->update(TABLE_PRE.'area_code',$data);

              if($query){

                return true;
              }
   }
   
   
    function change_all_vendor_status($id){
		
			$this->db1->select('status');
        $this->db1->where('hotel_vendor_id',$id);
		$query=$this->db1->get(TABLE_PRE.'vendor');
	 //  $query=$this->db1->get();
        if($query->num_rows()>0){

           $query2=$query->row();
		  
        }
        
		  if($query2->status=='1'){
		  $data = array(
            'status' =>'0',

        );
		  }
		  else{
			   $data = array(
            'status' =>'1',

        );
			  
		  }
			
		$this->db1->where('hotel_vendor_id',$id);
		 $query3=$this->db1->update(TABLE_PRE.'vendor',$data);
		
		
		 if( $query3){

                return $query2->status;
              }
			  else{
				  return false;
			  }
		//return $a->status;
	}
	
	function vendor_filter($status){
		
		if($status=="Active Vendor"){
			$a='0';
		}
		else{
			$a='1';
			
		}
		$this->db1->select('*');
		$this->db1->where('status',$a);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'vendor');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
		//$query = $this->db1->get(TABLE_PRE.'area_code'); 
        if($query){
            return $query->result();
        } else {
            return false;
        }
	}
	
	function profit_center_filter($status){
		
		if($status=="Active Profit Center"){
			$a='0';
		}
		else{
			$a='1';
			
		}
		$this->db1->select('*');
		$this->db1->where('status',$a);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'profit_center');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
		//$query = $this->db1->get(TABLE_PRE.'area_code'); 
        if($query){
            return $query->result();
        } else {
            return false;
        }
	}
	function booking_source_filter($status){
		
		if($status=="Active Booking Source"){
			$a='0';
		}
		else{
			$a='1';
			
		}
		$this->db1->select('*');
		$this->db1->where('status',$a);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'booking_source');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
		//$query = $this->db1->get(TABLE_PRE.'area_code'); 
        if($query){
            return $query->result();
        } else {
            return false;
        }
	}
	
	function booking_nature_visit_filter($status){
		
		if($status=="Active Booking Nature Visit"){
			$a='0';
		}
		else{
			$a='1';
			
		}
		$this->db1->select('*');
		$this->db1->where('status',$a);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'booking_nature_visit');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
		//$query = $this->db1->get(TABLE_PRE.'area_code'); 
        if($query){
            return $query->result();
        } else {
            return false;
        }
	}
	
	
	function booking_status_type(){
			$this->db1->select('*');
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$query=$this->db1->get('booking_status_type');
			if($query){

            return $query->result();
        }
        else{

            return false;
			}
		}
		
		/*function booking_status_type1(){
			$this->db1->select('*');
			$query=$this->db1->get('booking_status_type');
			if($query){

            return $query->result();
        }
        else{

            return false;
			}
		}*/
		
		function housekeeping_status(){
			$this->db1->select('*');
			$query=$this->db1->get(TABLE_PRE.'housekeeping_status');
			if($query){

            return $query->result();
        }
        else{

            return false;
			}
		}
		
		function booking_status_bar_color($data,$hid){

              $id=$data['status_id'];
              $this->db1->where('status_id',$id);
			  $this->db1->where('hotel_id',$hid);
              $query=$this->db1->update('booking_status_type',$data);

              
   }
   
   function change_Primary_color($data,$hid){

              $id=$data['status_id'];
              $this->db1->where('status_id',$id);
				$this->db1->where('hotel_id',$hid);
              $query=$this->db1->update(TABLE_PRE.'housekeeping_status',$data);

              
   }
   
   
   function fetch_category($id){

			  
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'unit_type_name');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
    function edit_unit_type($id){

			  
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'unit_type');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   
    function fetch_section($id){

			  
		$this->db1->select('*');
		$this->db1->where('sec_id',$id);
$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'section');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   function fetch_unit_class($id){

			  
		$this->db1->select('*');
		$this->db1->where('hotel_unit_class_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'unit_class');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   function get_warehouse_stock($id){
		$this->db1->select('*');
		$this->db1->where('warehouse_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'stock_qty_mapping');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   function get_stock_inventory($id){

			  
		$this->db1->select('*');
		$this->db1->where('hotel_stock_inventory_id',$id);
	$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'stock_inventory');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   function check_welcome_kit($data){
				
				$data1 = array(            
			'kit'=>'0'
			);
			  
              //$this->db1->where('id',$id);
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query1=$this->db1->update(TABLE_PRE.'warehouse',$data1);//update by new
			  
			  $id=$data['id'];
              $this->db1->where('id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'warehouse',$data);//update by new 
			
			
			$this->db1->select('*');
			$this->db1->where('id',$id);
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$this->db1->from(TABLE_PRE.'warehouse');//TABLE_PRE="hotel_"
			$query1=$this->db1->get();
			return $query1->row();
			//return $query->row();  
        
    }
	function check_welcome_kit1(){
	  
			$this->db1->select('*');
			$this->db1->where('kit','1');
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$this->db1->from(TABLE_PRE.'warehouse');//TABLE_PRE="hotel_"
			$query=$this->db1->get();
			return $query->row();
    }
	
	function edit_dg_set($id){

			  
		$this->db1->select('*');
		$this->db1->where('dgset_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'dgset');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   function edit_corporate($id){

			  
		$this->db1->select('*');
		$this->db1->where('hotel_corporate_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'corporate');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   function update_dgset($data){

              $id=$data['dgset_id'];
              $this->db1->where('dgset_id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'dgset',$data);

              return true;
   }
   
   
   function update_admin_details($data){

              $id=$data['user_id'];
              $this->db1->where('user_id',$id);
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'admin_details',$data);

              return true;
   }
   
   function update_corporate($data){

              $id=$data['hotel_corporate_id'];
              $this->db1->where('hotel_corporate_id',$id);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'corporate',$data);

              return true;
   }
   
   function edit_event($id){

			  
		$this->db1->select('*');
		$this->db1->where('e_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'events');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   
    function edit_assets_type($id){

			  
		$this->db1->select('*');
		$this->db1->where('asset_type_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'asset_type');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   
   function get_data($data){

            $id=$data['hotel_unit_class_id'];
              $this->db1->where('hotel_unit_class_id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'unit_class_name',$data);

              if($query){

                return true;
              }
   }
   
    function edit_stock($id){

			  
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'stock_inventory_category');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
	
	function edit_warehouse_aj($id){

			  
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'warehouse');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
	
	
	function fetch_booking_nature_visit($id){

			  
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('booking_nature_visit_id',$id);
		$this->db1->from(TABLE_PRE.'booking_nature_visit');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
	
	function get_edit_booking_source($id){

			  
		$this->db1->select('*');
		$this->db1->where('booking_source_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'booking_source');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   
   function get_edit_profit_center($id){

			  
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'profit_center');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   function transaction_from_entity($id){
 
        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'entity_type');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('hotel_entity_type_id',$id);
        $query=$this->db1->get();
        if($query){

            return $query->row();
        }
        else{

            return false;
        }


    }
	
	
	 function add_marketing_personel($data){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
    	$query=$this->db1->insert(TABLE_PRE.'marketing_personnel',$data);
		//$last_id=$this->db1->insert_id();
        if($query){

            return true;
        }
        else{

          return false;
        }
    	     
    }
	
	function delete_marketing_personnel($id){
		
		$this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->delete(TABLE_PRE.'marketing_personnel');
             
	}
	
	
	function edit_marketing_personel($id){

			  
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'marketing_personnel');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   function update_marketing_personel($data){

              $id=$data['id'];
              $this->db1->where('id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'marketing_personnel',$data);

              if($query){

                return true;
              }
   }
   
   
   	function all_marketing_personel(){
			$this->db1->select('*');
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$query=$this->db1->get(TABLE_PRE.'marketing_personnel');
			if($query){

            return $query->result();
        }
        else{

            return false;
			}
		}
	
	
	function check_room_no($a){ 
        $this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('room_no',$a);
		 $this->db1->from(TABLE_PRE.'room');
        $query=$this->db1->get();
        if($query->num_rows()>0){

            return '1';
        }
        else{

            return '0';
        }
    }
	
	
	function enquery_management(){
        $this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		//$this->db1->where('user_id',$this->session->userdata('user_id'));  
		$this->db1->from(TABLE_PRE.'guest');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
    }
	
	function all_enquery_list(){
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		//$this->db1->where('status','1');
		$this->db1->from(TABLE_PRE.'enquery_list');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	
	function add_enquery_management($data){

    	$query=$this->db1->insert(TABLE_PRE.'enquery_list',$data);

        if($query){

            return true;
        }
        else{

          return false;
        }
    	     
    }
	
	
	//delete_enquery_management
	function delete_enquery_management($id){
		
		$this->db1->where('id',$id);
              $query=$this->db1->delete(TABLE_PRE.'enquery_list');
             
	}
	
	
	
	function edit_enquery_management($id){
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'marketing_personnel');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   
   function get_enquery_management($id){

			  
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'enquery_list');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   
   function update_enquery_management($data){

            $id=$data['id'];
              $this->db1->where('id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'enquery_list',$data);

              if($query){

                return true;
              }
   }
   
   function all_shift_filter($status){
		
		if($status=="Active Shift"){
			$a='0';
		}
		else{
			$a='1';
			
		}
		$this->db1->select('*');
		$this->db1->where('status',$a);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'shift');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
		//$query = $this->db1->get(TABLE_PRE.'area_code'); 
        if($query){
            return $query->result();
        } else {
            return false;
        }
	}
	
	
	
function shift_status($id){
		$this->db1->select('status');
        $this->db1->where('shift_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$query=$this->db1->get(TABLE_PRE.'shift');
	 //  $query=$this->db1->get();
        if($query->num_rows()>0){

           $query2=$query->row();
		  
        }
        
		  if($query2->status=='1'){
		  $data = array(
            'status' =>'0',

        );
		  }
		  else{
			   $data = array(
            'status' =>'1',

        );
			  
		  }
			
		$this->db1->where('shift_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		 $query3=$this->db1->update(TABLE_PRE.'shift',$data);
		
		
		 if( $query3){

                return $query2->status;
              }
			  else{
				  return false;
			  }
		
		
		
	}
	
	
	
	function enquery_list_filter($status){
		
		if($status=="Active Enquery"){
			$a='0';
		}
		else{
			$a='1';
			
		}
		$this->db1->select('*');
		$this->db1->where('status',$a);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'enquery_list');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
		//$query = $this->db1->get(TABLE_PRE.'area_code'); 
        if($query){
            return $query->result();
        } else {
            return false;
        }
	}
	
	function check_shift_time($start,$end){
				
		$this->db1->select('*');		
		$this->db1->where("(shift_opening_time>='$start' OR shift_closing_time<='$end')");
        $query=$this->db1->get(TABLE_PRE.'shift');
        if($query->num_rows()>0){
            return 1;
        }
        else{
            return 0;
	}
	}
	
	
		function add_cash($data){
		 /*$this->db1->where('is_ended','0');
		 $query=$this->db1->update(TABLE_PRE.'cash_drawer',$data);*/
		  $query=$this->db1->insert(TABLE_PRE.'cash_drawer',$data);
		 
		if($query){
		return $this->db1->insert_id();
		}
		else{
			return false;
		}  
    }
	function edit_cash($data,$id){
		 $this->db1->where('drawer_id',$id);
		  $query=$this->db1->update(TABLE_PRE.'cash_drawer',$data);
		 
		if($query){
		//return $this->db1->insert_id();
		return true;
		}
		else{
			return false;
		}  
    }
	
	
	
	function get_closing_stock($id=NULL){
		if(isset($id)){
			$sql="select * from hotel_cash_drawer where drawer_id=".$id;
		}
		else{
			$sql="select * from hotel_cash_drawer where drawer_id=(select max(drawer_id) from hotel_cash_drawer)";
		}
        $query=$this->db1->query($sql);
		
        if($query){
             return $query->row();
        }
        else{
            return 0;
		}
    }
	
	function get_mode_cash($mode,$id){
		if(isset($id)){
		$sql="select $mode from hotel_cash_drawer where drawer_id=".$id;
		}else{
			$sql="select $mode from hotel_cash_drawer where drawer_id=(select max(drawer_id) from hotel_cash_drawer)";
		}
        $query=$this->db1->query($sql);
        if($query){
             return $query->row();
			// return $sql;
			
        }
        else{
            return 0;
	}
    }
	
	
	
	function set_opening_cash($a){
		//$a--;
		$this->db1->where('drawer_id',$a);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'cash_drawer');
		return $query->row();
	}
	function update_opening_balance($cash,$id,$deposit,$withdraw,$clossing){
		//$clossing=(($cash-$withdraw)+$deposit);
		$data=array(
		'opening_cash'=>$cash,
		'closing_cash'=>$clossing,
		'hotel_id'=>$this->session->userdata('user_hotel'),
		'user_id'=>$this->session->userdata('user_id'),
		);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('drawer_id',$id);
		 $query=$this->db1->update(TABLE_PRE.'cash_drawer',$data);
		if($query){
		return true;
		}
		else{
			return false;
	}
	
	
	}
	
	function get_clossing_cash_drawer_info($a){
 
        $this->db1->select('*');
        $this->db1->from(TABLE_PRE.'cash_drawer');
        $this->db1->where('drawer_id',$a);
        $query=$this->db1->get();
        if($query){

            return $query->row();
        }
        else{

            return false;
        }
    }
	
	function update_cash_collection($cash,$c,$cash_collection){
		$c+=$cash;
		$cash_collection+=$cash;
		$data=array(
		'cash_collection'=>$cash_collection,	
		'closing_cash'=>$c
		);
		$this->db1->where('is_ended','0');
		$this->db1->where('drawer_id',$this->session->userdata('cd_key'));
		 $query=$this->db1->update(TABLE_PRE.'cash_drawer',$data);
		if($query){
		return true;
		}
		else{
			return false;
	}
	
	
	}
	
	function update_cash_drawer_mode($amount,$mode){
		
		
		$data=array(
		$mode=>$amount,		
		);
		$this->db1->where('is_ended','0');
		$this->db1->where('drawer_id',$this->session->userdata('cd_key'));
		 $query=$this->db1->update(TABLE_PRE.'cash_drawer',$data);
		if($query){
		return true;
		}
		else{
			return false;
	}
	
	}
	function status_color($id){ 
        $this->db1->select('*');
		//$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('status_id',$id);
        $query=$this->db1->get('booking_status_type');
        if($query){

            return $query->row();
        }
        else{

            return false;
        }


    }
	
	function add_discount_rules($data){

    	$query=$this->db1->insert(TABLE_PRE.'discount_ruls',$data);

        if($query){

            return true;
        }
        else{

          return false;
        }
    	     
    }
	
	
	function discount_rules(){ 
	
        $this->db1->select('*');
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
		//$this->db1->where('user_id',$this->session->userdata('user_id'));  
        $query=$this->db1->get(TABLE_PRE.'discount_ruls');
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	
	function delete_discount_rule($id){
		
		$this->db1->where('id',$id);
              $query=$this->db1->delete(TABLE_PRE.'discount_ruls');
             
	}
	
	
	 function get_discount_plan_by_id($id){
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'discount_ruls');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   
   function update_discount_rule($data){

              $id=$data['id'];
              $this->db1->where('id',$id);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'discount_ruls',$data);

              return true;
   }
   
   function get_rule($id,$c){
			$this->db1->select('*');
			$this->db1->where('id',$id);
			$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
			$query=$this->db1->get(TABLE_PRE.'discount_ruls');
			if($query){

            return $query->row();
        }
        else{

            return false;
			}
		}
		
		
		function getProfitCenterById($id){
        $this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get(TABLE_PRE.'profit_center');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }
	
	function all_industry_type(){
		
        $query=$this->db1->get(TABLE_PRE.'industry_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	
	function all_laundry_service_type(){ 
        $this->db1->select('*');
        $query=$this->db1->get(TABLE_PRE.'laundry_service_type');
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	
	function all_laundry_cloth_type(){ 
        $this->db1->select('*');
        $query=$this->db1->get(TABLE_PRE.'laundry_cloth_type');
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	function all_laundry_color(){ 
        $this->db1->select('*');
        $query=$this->db1->get('color');
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	function all_laundry_fabric_type(){ 
        $this->db1->select('*');
       $query=$this->db1->get(TABLE_PRE.'laundry_fabric_type');
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	function reset_laundry_rates(){
		$hotel_id=$this->session->userdata('user_hotel');
		$user_id=$this->session->userdata('user_id');
		
		//$this->db1->query("TRUNCATE hotel_laundry_rates");
		
		$cloth_type=$this->unit_class_model->all_laundry_cloth_type();
		$fabric_type=$this->unit_class_model->all_laundry_fabric_type();
		
		foreach ($cloth_type as $ct){
			foreach ($fabric_type as $ft){
				//echo $ft->fabric_type.'<br>';
				$a=$ct->laundry_ct_id;
				$b=$ft->fabric_id;
				$sql2="UPDATE `hotel_laundry_rates` SET `lr_laundry_rate`=0,`lr_dry_cleaning_rate`=0,`lr_ironing_rate`=0,`lr_default_type`=0  where `hotel_id`=$hotel_id and `lr_cloth_type_id`=$a and `lr_fabric_type_id`=$b";
				$query2=$this->db1->query($sql2);
			}
		}
		/*foreach ($cloth_type as $ct){
			//echo $ct->laundry_ct_name.'<br>';			
			foreach ($fabric_type as $ft){
				//echo $ft->fabric_type.'<br>';
				$a=$ct->laundry_ct_id;
				$b=$ft->fabric_id;
				$sql2="INSERT INTO hotel_laundry_rates(lr_cloth_type_id,lr_fabric_type_id,user_id,hotel_id) VALUES($a,$b,$user_id,$hotel_id)";
				$query2=$this->db1->query($sql2);
			}
		}*/
		
	}
	
	function all_laundry_rates(){ 
        $this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));        
        //$this->db1->where('user_id',$this->session->userdata('user_id'));  
       $query=$this->db1->get(TABLE_PRE.'laundry_rates');
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	
	function get_cloth_type_by_id($id){
		$this->db1->select('*');
		$this->db1->where('laundry_ct_id',$id);
		$this->db1->from(TABLE_PRE.'laundry_cloth_type');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   function get_fabric_type_by_id($id){
		$this->db1->select('*');
		$this->db1->where('fabric_id',$id);
		$this->db1->from(TABLE_PRE.'laundry_fabric_type');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
	
	function get_price_by_clothID_fabricId($cloth,$fab,$service){
		$this->db1->select('*');
		$this->db1->where('lr_cloth_type_id',$cloth);
		$this->db1->where('lr_fabric_type_id',$fab);
		$this->db1->from(TABLE_PRE.'laundry_rates');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   
   function get_service_type_by_id($id){
		$this->db1->select('*');
		$this->db1->where('laundry_type_id',$id);
		$this->db1->from(TABLE_PRE.'laundry_service_type');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
	

 function get_color_by_id($id){
		$this->db1->select('*');
		$this->db1->where('color_id',$id);
		$this->db1->from('color');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
	
	function get_single_booking(){
		$this->db1->select('*');
		$this->db1->where('group_id','0');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('booking_status_id','5');
		$this->db1->from(TABLE_PRE.'bookings');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->result();
              }
   }
	
	function get_group_booking(){
		$this->db1->select('*');
		//$this->db1->where('group_id!=','0');
		$this->db1->where('status','5');
		//$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'group');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->result();
              }
   }
	
	function add_laundry_service($service,$corporate_discount,$asd){
     $query=$this->db1->insert(TABLE_PRE.'laundry_master',$service);
	 $master_id=$this->db1->insert_id();
	 
	if($asd>0){
	 $asd['details_id']=$master_id;
     $query3=$this->db1->insert(TABLE_PRE.'transactions',$asd);
	}

	 

		//$corporate_discount['corporate_id']=$corporate_id;
/*						'cloth_type_id'=>$cloth_type,
						'fabric_type_id'=>$fabric,
						//''=>$size,
						'item_condition'=>$condition,						
						'laundry_service_type_id'=>$service,
						'laundry_item_qty'=>$qty,
						'laundry_item_color'=>$color,
						'laundry_item_brand'=>$brand,
						
						//''=>$price,
						'item_note'=>$note,
						*/
		foreach($corporate_discount['cloth_type_id'] as $key => $val){

				$cloth_type_id=$corporate_discount['cloth_type_id'][$key];
				$fabric_type_id=$corporate_discount['fabric_type_id'][$key];
				$item_condition=$corporate_discount['item_condition'][$key];
				
				$item_size=$corporate_discount['item_size'][$key];
				$item_price=$corporate_discount['item_price'][$key];
				
				$laundry_service_type_id=$corporate_discount['laundry_service_type_id'][$key];
				$laundry_item_qty=$corporate_discount['laundry_item_qty'][$key];
				$laundry_item_color=$corporate_discount['laundry_item_color'][$key];
				$laundry_item_brand=$corporate_discount['laundry_item_brand'][$key];
				$item_note=$corporate_discount['item_note'][$key];
				//$item_tax=$corporate_discount['item_tax'][$key];

				 $data=array(
                                'cloth_type_id'=>$cloth_type_id,
                                'fabric_type_id'=>$fabric_type_id,
                                'item_condition'=>$item_condition,
								
                                'item_size'=>$item_size,
                                'item_price'=>$item_price,
								
                                'laundry_service_type_id'=>$laundry_service_type_id,
								'laundry_item_qty'=>$laundry_item_qty,
								'laundry_item_color'=>$laundry_item_color,
								'laundry_item_brand'=>$laundry_item_brand,
								'item_note'=>$item_note,
								'laundry_master_id'=>$master_id,
								);
				$query1=$this->db1->insert(TABLE_PRE.'laundry_line_item',$data);

							}



		//if($query && $query1 &&  $query3){
		if($query){
            return true;
        }

}

function get_guest_by_booking_id($id){
		$this->db1->select('*');
		$this->db1->where('booking_id',$id);
		$this->db1->from(TABLE_PRE.'bookings');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   function get_group_guest_by_booking_id($id){
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->from(TABLE_PRE.'group');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   function get_guest_by_guest_id($id){
		$this->db1->select('*');
		$this->db1->where('g_id',$id);
		$this->db1->from(TABLE_PRE.'guest');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
	
	function get_group_guest_by_guest_id($id){
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->from(TABLE_PRE.'group');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   //delete delete_laundry_service
	function delete_laundry_service($id){
		
		$this->db1->where('laundry_id',$id);
              $query=$this->db1->delete(TABLE_PRE.'laundry_master');
             
	}
	
	
	function laundry_service_by_id($id){
		$this->db1->select('*');
		$this->db1->where('laundry_id',$id);
		$this->db1->from(TABLE_PRE.'laundry_master');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   function get_line_itm($id){
		$this->db1->select('*');
		$this->db1->where('laundry_master_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'laundry_line_item');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->result();
              }
   }
   function laundry_lineitem($id){
		$this->db1->select('*');
		$this->db1->where('laundry_master_id',$id);
		$this->db1->from(TABLE_PRE.'laundry_line_item');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->result();
              }
   }
   function update_laundry_service($service,$corporate_discount,$laundry_payment,$id,$pay_amount){
     //$query=$this->db1->insert(TABLE_PRE.'laundry_master',$service);
	if($pay_amount>0){
	 //$laundry_payment['details_id']=$master_id;
     $query3=$this->db1->insert(TABLE_PRE.'transactions',$laundry_payment);
	}
   	$this->db1->where('laundry_id',$id);
	 $query=$this->db1->update(TABLE_PRE.'laundry_master',$service);
	 
	// echo $this->db1->last_query();exit;
	 $this->db1->where('laundry_id',$id);
	 $qu=$this->db1->get(TABLE_PRE.'laundry_master');
		$qu=$qu->row();
		
		
		/*$pay_amount+=$qu->total_amount;
		$due=$qu->grand_total-$pay_amount;
		$dt=array(
		'total_amount'=>$pay_amount,
		'payment_due'=>$due
		);
			$this->db1->where('laundry_id',$id);
	 $query=$this->db1->update(TABLE_PRE.'laundry_master',$dt);*/
						
		foreach($corporate_discount['cloth_type_id'] as $key => $val){

				$laundry_id=$corporate_discount['laundry_id'][$key];
				$cloth_type_id=$corporate_discount['cloth_type_id'][$key];
				$fabric_type_id=$corporate_discount['fabric_type_id'][$key];
				$item_condition=$corporate_discount['item_condition'][$key];
				
				$item_size=$corporate_discount['item_size'][$key];
				$item_price=$corporate_discount['item_price'][$key];
				
				$laundry_service_type_id=$corporate_discount['laundry_service_type_id'][$key];
				$laundry_item_qty=$corporate_discount['laundry_item_qty'][$key];
				$laundry_item_color=$corporate_discount['laundry_item_color'][$key];
				if($laundry_item_color){
					$laundry_item_color='';
				}
				$laundry_item_brand=$corporate_discount['laundry_item_brand'][$key];
				$item_note=$corporate_discount['item_note'][$key];
				$laundry_master_id=$corporate_discount['laundry_master_id'];
				//$item_tax=$corporate_discount['item_tax'][$key];

				 $data=array(
                                'laundry_id'=>$laundry_id,
                                'cloth_type_id'=>$cloth_type_id,
                                'fabric_type_id'=>$fabric_type_id,
                                'item_condition'=>$item_condition,								
                                'item_size'=>$item_size,
                                'item_price'=>$item_price,								
                                'laundry_service_type_id'=>$laundry_service_type_id,
								'laundry_item_qty'=>$laundry_item_qty,
								'laundry_item_color'=>$laundry_item_color,
								'laundry_item_brand'=>$laundry_item_brand,
								'item_note'=>$item_note,
								'laundry_master_id'=>$laundry_master_id,
								);
								//$this->db1->where('laundry_id',$laundry_id);
								
				$query1=$this->db1->insert(TABLE_PRE.'laundry_line_item',$data);
				//echo "<pre>";
				//print_r($data);
				
				/*$this->db1->where('laundry_master_id',$id);
				$this->db1->where('laundry_id',$laundry_id);
				$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
				$query1=$this->db1->update(TABLE_PRE.'laundry_line_item',$data);	 
				if($query1){
					
				}else{
					$query1=$this->db1->insert(TABLE_PRE.'laundry_line_item',$data);
				}*/

							}



		//if($query && $query1 &&  $query3){
		if($query){
            return true;
        }

}

 function transaction_laundry(){
		$this->db1->select('*');
		$this->db1->where('details_type','Laundry');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'transactions');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->result();
              }
   }
   
   function booking_by_booking_id($booking_id){
       $this->db1->select('*');
		$this->db1->where('booking_id',$booking_id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'bookings');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }

    }


function add_laundry_extraservice($data){
              $query=$this->db1->insert(TABLE_PRE.'service_mapping',$data);
			
        if($query){

            return true;
        }
        else{

          return false;
        }
             
	}
	function add_discount_mapping($data){
              $query=$this->db1->insert(TABLE_PRE.'discount_mapping',$data);
			
        if($query){

            return true;
        }
        else{

          return false;
        }
             
	}


 function all_discount_details($id){
		$this->db1->select('*');
		$this->db1->where('booking_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'discount_mapping');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->result();
              }
   }
   function all_discount_group_details($id){
		$this->db1->select('*');
		$this->db1->where('group_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'discount_mapping');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->result();
              }
   }
   
   
   //delete delete_discount_mapping
	function delete_discount_mapping($id){
		
		$this->db1->where('id',$id);
              $query=$this->db1->delete(TABLE_PRE.'discount_mapping');
			  
             if($query){
				 return true;
			 }
	}
	
	 function discount_rule_name_by_id($id){
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'discount_ruls');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   function all_groupBooing_discount_mapping($id){
		$this->db1->select('*');
		$this->db1->where('group_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'discount_mapping');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->result();
              }
   }
	
	function add_groupBooing_discount_mapping($data){
              $query=$this->db1->insert(TABLE_PRE.'discount_mapping',$data);
			
        if($query){

            return true;
        }
        else{

          return false;
        }
             
	}
	
	function getLastDiscount(){
		$this->db1->select('MAX(id) as last');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'discount_mapping');
		$query=$this->db1->get();
		
		if($query){
            return $query->row();
        }
	}
	
	function get_discount_details_single($id){	
		$this->db1->select('*');
		$this->db1->where('booking_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'discount_mapping');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->result();
              }
	}
	
	function get_discount_details_group($id){	
		$this->db1->select('*');
		$this->db1->where('group_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'discount_mapping');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->result();
              }
	}
	
	function get_downloadStatus_single($id){
		$this->db1->select('*');
		$this->db1->where('related_id',$id);
		$this->db1->where('invoice_type','booking');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from('invoice_master');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
             if($query->num_rows()>0){

                return 1;
              }else{
				  return 0;
			  }
		
	}
	
	
	function profile_pic_change($data,$ab){

            
              $this->db1->where('admin_id',$ab);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'admin_details',$data);

              if($query){

                return true;
              }
   }
   function guest_doc_change($data,$ab){
              $this->db1->where('g_id',$ab);
			  //$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'guest',$data);

              if($query){

                return true;
              }
   }
   
   
   
   
   
   function update_admin_profile($data,$id){

            
              $this->db1->where('admin_id',$id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'admin_details',$data);

              if($query){

                return true;
              }
   }
   
   function country_by_id($id){
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->from(TABLE_PRE.'countries');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
        return $query?$query->row():false;   
		
	}
	
	function all_meal_plan_cat(){	
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'meal_plan_cat');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->result();
              }
	}
	
	function fetch_country(){
		$this->db1->select('*');
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'countries');//TABLE_PRE="hotel_"
		 $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	function check_discount_name($id,$name,$dis){	
		$this->db1->select('*');
		$this->db1->where('booking_id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('discount_for',$name);
		//$this->db1->where('discount_plan_id',$dis);
		$this->db1->from(TABLE_PRE.'discount_mapping');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query->num_rows()>0){

                return 1;
              }
			  else{
				  return 0;
			  }
	}
	
	function check_discount_plan($discount_id,$booking_id){	
		$this->db1->select('*');
		$this->db1->where('booking_id',$booking_id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('discount_plan_id',$discount_id);
		$this->db1->from(TABLE_PRE.'discount_mapping');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query->num_rows()>0){

                return 1;
              }
			  else{
				  return 0;
			  }
	}

function get_adhoc_details($id){
		$this->db1->select('*');
		$this->db1->where('laundry_id',$id);
		$this->db1->from(TABLE_PRE.'laundry_master');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }


function get_transaction_type($id){
		$this->db1->select('*');
		$this->db1->where('hotel_transaction_type_id',$id);
		$this->db1->from(TABLE_PRE.'transaction_type');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   function close_cashdrawer($id,$valu,$closetime){
	   $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	   $this->db1->where('drawer_id',$id);
       $query1=$this->db1->get(TABLE_PRE.'cash_drawer');
	   $query1=$query1->row();
	   
	   
	   $clossing=$query1->cash_collection+$query1->deposit- $valu;
		
	   date_default_timezone_set('Asia/Kolkata');
	   $data=array(
	   'is_ended'=>1,
	   'withdraw'=>$valu,
	   'shift_log_id'=>$this->session->userdata('shift_id'),
	   'closing_cash'=>$clossing,
	   'closing_datetime'=>$closetime
	   );
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('drawer_id',$id);
        $query=$this->db1->update(TABLE_PRE.'cash_drawer',$data);
			  
			  
			  
			  if($query){
				  return 1;
			  }
             return 0;
	}
	
	function close_cashdrawer_newopen($id,$closetime){
	   $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
	   $this->db1->where('drawer_id',$id);
       $query1=$this->db1->get(TABLE_PRE.'cash_drawer');
	   $query1=$query1->row();
	   
	   
	   $clossing=$query1->closing_cash;
		
	   date_default_timezone_set('Asia/Kolkata');
	   $data=array(
	   'is_ended'=>1,
	   'closing_cash'=>$clossing,
	   'closing_datetime'=>$closetime
	   );
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('drawer_id',$id);
        $query=$this->db1->update(TABLE_PRE.'cash_drawer',$data);
			  
			  
			  
			  if($query){
				  return 1;
			  }
             return 0;
	}
	
	function get_present_stock($id){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('drawer_id',$id);
		$this->db1->from(TABLE_PRE.'cash_drawer');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   function save_cash_drawer_log($data){
              $query=$this->db1->insert('log_cash_drawer',$data);
			$insert_id = $this->db1->insert_id();
        if($query){

            return  $insert_id;
        }
        else{

          return false;
        }
             
	}
	 function update_cash_drawer_log($data,$Gid){
		
		//$this->db1->where('id',$Gid);
       // $query=$this->db1->update('log_cash_drawer',$data);
		
		$sql="UPDATE `log_cash_drawer` SET `leaveDateTime`='".$data['leaveDateTime']."' where `id`=".$Gid."";
		//echo $sql;exit;
		$query=$this->db1->query($sql);
        if($query){

            return true;
        }
        else{

          return false;
        }
             
	}
	
	
	function update_cash_drawer($id,$data){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('drawer_id',$id);
        $query=$this->db1->update(TABLE_PRE.'cash_drawer',$data);
			
        if($query){

            return true;
        }
        else{

          return false;
        }
             
	}
	
	function all_cashdrawer(){		
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->order_by('drawer_id','desc');
        $query=$this->db1->get(TABLE_PRE.'cash_drawer');
		return $query->result();
	}
	function all_cashdrawer_log(){		
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->order_by('id','desc');
        $query=$this->db1->get('log_cash_drawer');
		return $query->result();
	}
   function bookings_info_presentDay_single(){
		
	 $this->db1->select("id AS bid,name AS cname,booking_date AS bdate,booking_source AS bsource,start_date AS fdate,end_date AS tdate,id AS type,number_room AS nRoom,number_guest AS tGuest");
    $this->db1->distinct();
    $this->db1->from("hotel_group");
   $this->db1->where("date_format(booking_date,'%Y-%m-%d') =", date("Y-m-d"));
  
    $query1 = $this->db1->get_compiled_select(); // It resets the query just like a get()

    $this->db1->select("booking_id AS bid,cust_name AS cname,booking_taking_date AS bdate,booking_source AS bsource,cust_from_date_actual AS fdate,cust_end_date_actual AS tdate,group_id AS type,cust_name AS nRoom,no_of_guest AS tGuest");
    $this->db1->distinct();
    $this->db1->from("hotel_bookings");
    $this->db1->where("group_id",0);
	$this->db1->where("booking_status_id!= ",7);
	$this->db1->where("date_format(booking_taking_date,'%Y-%m-%d') =", date("Y-m-d"));
    $query2 = $this->db1->get_compiled_select(); 

    $query = $this->db1->query($query1." UNION ".$query2);

    return $query->result();
    }
	
	function total_singleBooking_today($d){
		
		$sql="SELECT count(*) as singletotal FROM `hotel_bookings` WHERE date_format(booking_taking_date,'%Y-%m-%d') ='".$d."' and group_id=0";
		$query=$this->db1->query($sql);
        return $query->row();
	}
	function total_singleBooking_checkin($d){
		
		$sql="SELECT count(*) as checkinsingle FROM `hotel_bookings` WHERE date_format(booking_taking_date,'%Y-%m-%d') ='".$d."' and group_id=0 and booking_status_id=5";
		$query=$this->db1->query($sql);
        return $query->row();
	}
	function total_singleBooking_checkout($d){
		
		$sql="SELECT count(*) as checkoutsingle FROM `hotel_bookings` WHERE date_format(booking_taking_date,'%Y-%m-%d') ='".$d."' and group_id=0 and booking_status_id=6";
		$query=$this->db1->query($sql);
        return $query->row();
	}
   function total_groupBooking_today($d){
		
		$sql="SELECT count(*) as grouptotal FROM `hotel_group` WHERE date_format(booking_date,'%Y-%m-%d') ='".$d."'";
		$query=$this->db1->query($sql);
        return $query->row();
	}
	
	function generate_nightAudit($data){
              $query=$this->db1->insert('hotel_night_audit_master',$data);
			$insert_id = $this->db1->insert_id();
        if($query){

            return  $insert_id;
        }
        else{

          return false;
        }
             
	}
	
	function total_groupBooking_checkin($d){
		
		$sql="SELECT count(*) as checkingroup FROM `hotel_group` WHERE date_format(	booking_date,'%Y-%m-%d') ='".$d."'  and status=5";
		$query=$this->db1->query($sql);
        return $query->row();
	}
	function total_groupBooking_checkout($d){
		
		$sql="SELECT count(*) as checkoutgroup FROM `hotel_group` WHERE date_format(booking_date,'%Y-%m-%d') ='".$d."'  and status=6";
		$query=$this->db1->query($sql);
        return $query->row();
	}
	
	function total_singleBooking_advance($d){
		
		$sql="SELECT count(*) as singleAdvance FROM `hotel_bookings` WHERE date_format(booking_taking_date,'%Y-%m-%d') ='".$d."' and group_id=0 and booking_status_id=2";
		$query=$this->db1->query($sql);
        return $query->row();
	}
	function total_groupBooking_advance($d){
		
		$sql="SELECT count(*) as groupAdvance FROM `hotel_group` WHERE date_format(booking_date,'%Y-%m-%d') ='".$d."'  and status=2";
		$query=$this->db1->query($sql);
        return $query->row();
	}
	function total_singleBooking_confirm($d){
		
		$sql="SELECT count(*) as singleConfirm FROM `hotel_bookings` WHERE date_format(booking_taking_date,'%Y-%m-%d') ='".$d."' and group_id=0 and booking_status_id=4";
		$query=$this->db1->query($sql);
        return $query->row();
	}
	function total_groupBooking_confirm($d){
		
		$sql="SELECT count(*) as groupConfirm FROM `hotel_group` WHERE date_format(booking_date,'%Y-%m-%d') ='".$d."'  and status=4";
		$query=$this->db1->query($sql);
        return $query->row();
	}
	function total_singleBooking_cancel($d){
		
		$sql="SELECT count(*) as singleCancel FROM `hotel_bookings` WHERE date_format(booking_taking_date,'%Y-%m-%d') ='".$d."' and group_id=0 and booking_status_id=7";
		$query=$this->db1->query($sql);
        return $query->row();
	}
	function total_groupBooking_cancel($d){
		
		$sql="SELECT count(*) as groupCancel FROM `hotel_group` WHERE date_format(booking_date,'%Y-%m-%d') ='".$d."'  and status=7";
		$query=$this->db1->query($sql);
        return $query->row();
	}
	
	function total_login($user,$hotel,$d){
		//user_id='".$user."' and
		$sql="SELECT count(*) as login FROM `hotel_login_session` WHERE date_format(login_dateTime,'%Y-%m-%d') ='".$d."'  and hotel_id='".$hotel."'and 	status='Success'";
		$query=$this->db1->query($sql);
        return $query->row();
	}
	
	function total_stayDays($d){
		
		$sql="SELECT sum(stay_days) as stayDays FROM `hotel_bookings` WHERE date_format(booking_taking_date,'%Y-%m-%d') ='".$d."'";
		$query=$this->db1->query($sql);
        return $query->row();
	}

	function all_night_audit(){		
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->order_by('nadt_m_id','desc');
        $query=$this->db1->get('hotel_night_audit_master');
		return $query->result();
	}
	function all_night_audit_settings(){		
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->order_by('id','desc');
        $query=$this->db1->get('hotel_night_audit_settings');
		return $query->result();
	}
function get_audit_settings(){
$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get('hotel_night_audit_settings');
		return $query->result();
}
	function update_night_audit_set($data){
		
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->get('hotel_night_audit_settings');
		$flg=$query->num_rows();
		
		if($flg){
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db1->update(TABLE_PRE.'night_audit_settings',$data);
		}else{
		$query=$this->db1->insert(TABLE_PRE.'night_audit_settings',$data);
		}	
        if($query){

            return true;
        }
        else{

          return false;
        }
             
	}


function userName_details($d){
		
		//$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('admin_id',$d);
        $query=$this->db1->get('hotel_admin_details');
		return $query->row();
        
	}
	
	function getCashdrawerName($id){
		
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->where('drawer_id',$id);
        $query=$this->db1->get('hotel_cash_drawer');
		return $query->row();
        
	}

	function all_travelAgentCommision(){ 
	
        //$this->db1->select('*');
		$this->db1->order_by('hoj_travelagentcommission.tacID','desc');
		$this->db1->from('hoj_travelagentcommission');
        $this->db1->where('hotel_bookings.hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->join('hotel_bookings','hotel_bookings.booking_id=hoj_travelagentcommission.bookingID');
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
		function all_travelAgentCommision_by_date1($start,$end){ 
	
        $this->db1->select('*');
		$this->db1->order_by('tacID','desc');
		$query=$this->db1->from('hoj_travelagentcommission');
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('addedDate>=',$start);
        $this->db1->where('addedDate<=',$end);
		
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	function all_travelAgentCommision_grp_single($type){ 
	
        $this->db1->select('*');
		$this->db1->order_by('tacID','desc');
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		if($type=='1'){
			$this->db1->where('groupID >',0);
		}else if($type=='0'){
			$this->db1->where('groupID',0);
		}
        $query=$this->db1->get('hoj_travelagentcommission');
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	function all_laudry_master_line($id){	
		/*$sql="SELECT  ma.*,  LN.*
FROM
  `hotel_laundry_master` AS ma
LEFT JOIN
  hotel_laundry_line_item AS LN ON ma.`laundry_id` = LN.laundry_master_id
  
WHERE
  ma.`booking_id` =".$id."";*/
  
  $pre="Add to Booking Bill";
  $sql="SELECT  ma.*,  LN.*,CO.color_name,CT.laundry_ct_name,FB.fabric_type,SV.laundry_type_name
FROM
  `hotel_laundry_master` AS ma
LEFT JOIN
  hotel_laundry_line_item AS LN ON ma.`laundry_id` = LN.laundry_master_id
  LEFT JOIN
  color AS CO ON LN.`laundry_item_color` = CO.`color_id`
  LEFT JOIN
  hotel_laundry_cloth_type AS CT ON LN.`cloth_type_id` = CT.`laundry_ct_id`
  LEFT JOIN
  hotel_laundry_fabric_type AS FB ON LN.`fabric_type_id` = FB.`fabric_id`
   LEFT JOIN
  hotel_laundry_service_type AS SV ON LN.`laundry_service_type_id` = SV.`laundry_type_id`
WHERE
  ma.`booking_id` =".$id."  and ma.`hotel_id` =".$this->session->userdata('user_hotel')." and ma.billing_preference='" .$pre."'";
		$query=$this->db1->query($sql);
        return $query->result();
			  
   }
   
   function update_laundry_master($lid,$quantity){

            $data=array(
			'total_item'=>$quantity
			);
              $this->db1->where('laundry_id',$lid);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'laundry_master',$data);

              if($query){

                return true;
              }
   }
   function source_id_by_name($name){ 
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('booking_source_name',$name);
        $query=$this->db1->get('hotel_booking_source');
        if($query){

            return $query->row();
        }
        else{

            return false;
        }


    }
	
	function source_by_bookingid($id){ 
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('booking_id',$id);
        $query=$this->db1->get('hotel_bookings');
        if($query){

            return $query->row();
        }
        else{

            return false;
        }
    }
	
	function source_by_bookingid_grp($id){ 
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('group_id',$id);
        $query=$this->db1->get('hotel_bookings');
        if($query){

            return $query->row();
        }
        else{

            return false;
        }
    }
	
	function add_profitcenter($data1){
  $query=$this->db1->insert(TABLE_PRE.'profit_center',$data1);
  $insert_id = $this->db1->insert_id();
  if($query){
	  

   return  $insert_id;
  }else{
	   return 0;
  }
	}
	
	
	function cd_withdraw($data){
		 
		  $query=$this->db1->insert('cd_withdraw',$data);
		 
		if($query){
		return $this->db1->insert_id();
		}
		else{
			return false;
		}  
    }
	function add_hotel_stay_guest($data){
		 
		  $query=$this->db1->insert('hotel_stay_guest',$data);
		 
		if($query){
		return $this->db1->insert_id();
		}
		else{
			return false;
		}  
    }
	
	function get_hotel_stay_guest($bookingID){
		
		
		$sql="SELECT
  s.*,
  g.*
FROM
  `hotel_stay_guest` AS s
LEFT JOIN
  hotel_guest AS g ON g.`g_id` = s.g_id
WHERE
  s.`booking_id` =".$bookingID."";
		
		 $query=$this->db1->query($sql);
        
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	function get_hotel_stay_guest_group($group_id){
		
		
		$sql="SELECT
  s.*,
  g.*,
  r.*
FROM
  `hotel_stay_guest` AS s
LEFT JOIN hotel_guest AS g ON g.`g_id` = s.g_id

LEFT JOIN hotel_bookings AS b ON b.`booking_id` = s.booking_id

LEFT JOIN hotel_room AS r ON r.`room_id` = b.room_id
WHERE
  s.`group_id` =".$group_id."";
		
		 $query=$this->db1->query($sql);
        
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	function add_guest_det($data){
		 
		  $query=$this->db1->insert('hotel_guest',$data);
		 
		if($query){
		return $this->db1->insert_id();
		}
		else{
			return false;
		}  
    }
	
	function update_checkout_status_stay_guest($data,$stayid){
              $this->db1->where('id',$stayid);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'stay_guest',$data);

              if($query){

                return 1;
              }else{
				  return 0;
			  }
   }
   
   function check_no_guest($id){ 
        $sql="SELECT count(id) as total  FROM
  `hotel_stay_guest` WHERE `booking_id` =".$id."";
		
		 $query=$this->db1->query($sql);
        
        if($query){

            return $query->row();
        }
        else{

            return false;
        }


    }
	
	function delete_stay_guest($booking_id,$stay_id){
		
		$this->db1->where('booking_id',$booking_id);
		$this->db1->where('id',$stay_id);
              $query=$this->db1->delete(TABLE_PRE.'stay_guest');
			  
             if($query){
				 return 1;
			 }else{
				  return 0;
			 }
	}
	function delete_stay_guest1($group_id,$stay_id){
		
		$this->db1->where('group_id',$group_id);
		$this->db1->where('id',$stay_id);
              $query=$this->db1->delete(TABLE_PRE.'stay_guest');
			  
             if($query){
				 return 1;
			 }else{
				  return 0;
			 }
	}
	
	function update_group_master($data,$group_id){
              $this->db1->where('id',$group_id);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'group',$data);

              if($query){

                return 1;
              }else{
				  return 0;
			  }
   }
   
   function room_view_data($hotel_id){
		
		
		$sql="SELECT
  ty.`unit_name`,ty.`unit_type`,ty.`unit_class`, `room_no`,`unit_id`, rm.`room_id`,bo.`booking_id_actual`
FROM
  `hotel_room` as rm RIGHT JOIN hotel_unit_type as ty ON  rm.`unit_id` = ty.`id` 
RIGHT JOIN `hotel_bookings` as Bo ON Bo.`room_id`= rm.`room_id`
WHERE
  rm.`hotel_id` =".$hotel_id."
ORDER BY
  `unit_id`";
		
		 $query=$this->db1->query($sql);
        
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	function check_dublicate_guest($booking_id,$guestid){ 
        $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('booking_id',$booking_id);
        $this->db1->where('g_id',$guestid);
        $query=$this->db1->get('hotel_stay_guest');
        if($query){

            return $query->num_rows();
        }
        else{

            return false;
        }


    }
	
	function get_info_laundryMaster($id){
		$this->db1->select('total_amount');
		$this->db1->where('laundry_id',$id);
		$this->db1->from(TABLE_PRE.'laundry_master');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   function get_color_code($id){ 
        $this->db1->where('status_id',$id);
        $query=$this->db1->get('booking_status_type');
        if($query){

            return $query->row();
        }
        else{

            return false;
        }
    }
	function get_booking_info($id,$hotel_id){ 
        $sql="SELECT BO.*,RO.*,GROUP_CONCAT(RO.room_no) as no
  
FROM
  hotel_bookings as BO
  
  LEFT JOIN hotel_room AS RO ON RO.`room_id` = BO.`room_id`
  where BO.group_id=".$id." and BO.hotel_id=".$hotel_id."";
   $query=$this->db1->query($sql);
        if($query){

            return $query->row();
        }
        else{

            return false;
        }
    }
	
	
	
	function get_guest_details($id,$hotel_id){ 
        $sql="SELECT GS.*,SGS.*
  
FROM
  hotel_guest as GS
  
  LEFT JOIN hotel_stay_guest AS SGS ON GS.`g_id` = SGS.`g_id`
  where GS.g_id=".$id." and GS.hotel_id=".$hotel_id."";
   $query=$this->db1->query($sql);
        if($query){

            return $query->row();
        }
        else{

            return false;
        }
    }
	
	
	function guest_pic_change($data,$ab){
              $this->db1->where('g_id',$ab);
			  $this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
              $query=$this->db1->update(TABLE_PRE.'guest',$data);

              if($query){

                return true;
              }
   }
   
   function stay_guest_by_guestID($id){ 
        $this->db1->where('hotel_guest.hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('hotel_guest.g_id',$id);
		 $this->db1->from('hotel_stay_guest');
        $this->db1->join('hotel_guest','hotel_guest.g_id=hotel_stay_guest.g_id');
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }


    }
	
	function booking_guestID($id){ 
        $this->db1->where('hotel_bookings.hotel_id',$this->session->userdata('user_hotel'));
        $this->db1->where('hotel_bookings.guest_id',$id);
		 $this->db1->from('hotel_bookings');
        $this->db1->join('hotel_room','hotel_room.room_id=hotel_bookings.room_id');
        $this->db1->join('booking_charge_line_item','booking_charge_line_item.booking_id=hotel_bookings.booking_id');
        $this->db1->join('hotel_guest','hotel_guest.g_id=hotel_bookings.guest_id');
        $this->db1->join('hotel_transactions','hotel_transactions.t_booking_id=hotel_bookings.booking_id');
        $this->db1->join('hotel_lost_item','hotel_lost_item.booking_id=hotel_bookings.booking_id');
        $this->db1->join('hotel_release_item','hotel_release_item.booking_id=hotel_bookings.booking_id');
        $query=$this->db1->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
    }
	
	function get_state_hotel(){
		$hotelID=$this->session->userdata('user_hotel');
		
        $this->db1->select('hotel_state');
        $this->db1->where('hotel_id',$hotelID);
		 $this->db1->from('hotel_hotel_contact_details');       
        $query=$this->db1->get();
        if($query){

            return $query->row();
        }
        else{

            return false;
        }
		
		}
		
		 function all_unit_type1($id){

			  
		$this->db1->select('*');
		$this->db1->where('id',$id);
		$this->db1->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db1->from(TABLE_PRE.'unit_type');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   function laundry_booking_id($id,$co){
		$this->db1->select('sum(grand_total) as gtotal');
		if($co=='0'){
		$this->db1->where('booking_id',$id);
		}else{
			$this->db1->where('group_id',$id);
		}
		$this->db1->from(TABLE_PRE.'laundry_master');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->row();
              }
   }
   
   function all_laudry_master_line1($id){	
		/*$sql="SELECT  ma.*,  LN.*
FROM
  `hotel_laundry_master` AS ma
LEFT JOIN
  hotel_laundry_line_item AS LN ON ma.`laundry_id` = LN.laundry_master_id
  
WHERE
  ma.`booking_id` =".$id."";*/
  $sql="SELECT  ma.*,  LN.*,CO.color_name,CT.laundry_ct_name,FB.fabric_type,SV.laundry_type_name
FROM
  `hotel_laundry_master` AS ma
LEFT JOIN
  hotel_laundry_line_item AS LN ON ma.`laundry_id` = LN.laundry_master_id
  LEFT JOIN
  color AS CO ON LN.`laundry_item_color` = CO.`color_id`
  LEFT JOIN
  hotel_laundry_cloth_type AS CT ON LN.`cloth_type_id` = CT.`laundry_ct_id`
  LEFT JOIN
  hotel_laundry_fabric_type AS FB ON LN.`fabric_type_id` = FB.`fabric_id`
   LEFT JOIN
  hotel_laundry_service_type AS SV ON LN.`laundry_service_type_id` = SV.`laundry_type_id`
WHERE
  ma.`booking_id` =".$id." and ma.`hotel_id` =".$this->session->userdata('user_hotel')."";
		$query=$this->db1->query($sql);
        return $query->result();
			  
   }
   
   function all_laudry_master_line_group($id){	
		/*$sql="SELECT  ma.*,  LN.*
FROM
  `hotel_laundry_master` AS ma
LEFT JOIN
  hotel_laundry_line_item AS LN ON ma.`laundry_id` = LN.laundry_master_id
  
WHERE
  ma.`booking_id` =".$id."";*/
  $sql="SELECT  ma.*,  LN.*,CO.color_name,CT.laundry_ct_name,FB.fabric_type,SV.laundry_type_name,ma.discount
FROM
  `hotel_laundry_master` AS ma
LEFT JOIN
  hotel_laundry_line_item AS LN ON ma.`laundry_id` = LN.laundry_master_id
  LEFT JOIN
  color AS CO ON LN.`laundry_item_color` = CO.`color_id`
  LEFT JOIN
  hotel_laundry_cloth_type AS CT ON LN.`cloth_type_id` = CT.`laundry_ct_id`
  LEFT JOIN
  hotel_laundry_fabric_type AS FB ON LN.`fabric_type_id` = FB.`fabric_id`
   LEFT JOIN
  hotel_laundry_service_type AS SV ON LN.`laundry_service_type_id` = SV.`laundry_type_id`
WHERE
  ma.`group_id` =".$id." and ma.`hotel_id` =".$this->session->userdata('user_hotel')." and ma.billing_preference='" .$pre."'";
		$query=$this->db1->query($sql);
        return $query->result();
			  
   }
   
   function get_laundry_master($id,$co){
		$this->db1->select('*');
		if($co=='0'){
		$this->db1->where('booking_id',$id);
		$this->db1->where('billing_preference','Add to Booking Bill');
		}else{
			$this->db1->where('group_id',$id);
			$this->db1->where('billing_preference','Add to Booking Bill');
		}
		$this->db1->from(TABLE_PRE.'laundry_master');//TABLE_PRE="hotel_"
		$query=$this->db1->get();
              if($query){

                return $query->result();
              }
   }
   
   function get_line_item_info($id){ 
        /*$sql="select group_concat(LA.item_price) as LA.itemPrice, group_concat(LA.laundry_item_qty) as LA.qty  from hotel_laundry_line_item as LA 
		left join hotel_laundry_cloth_type as CL on CL.laundry_ct_id=LA.cloth_type_id 
		where LA.laundry_master_id=".$id." and hotel_id=".$this->session->userdata('user_hotel')."";*/
		
		
		$sql="select group_concat(' ',CL.laundry_ct_name,' : ',laundry_item_qty,' X ',item_price,'') as qty  from hotel_laundry_line_item as LA 
		left join hotel_laundry_cloth_type as CL on CL.laundry_ct_id=LA.cloth_type_id 
		where LA.laundry_master_id=".$id." and LA.hotel_id=".$this->session->userdata('user_hotel')."";
   $query=$this->db1->query($sql);
        if($query){

            return $query->row();
        }
        else{

            return false;
        }
    }
    
    
    function delete_adhoc_line_master($adh_id){
        
       $this->db1->where('ahl_id',$adh_id);
        
       $query =  $this->db1->delete('adhoc_bill_line');
       if($query){
           
           return "success";
       }
        
    }
    

}

?>