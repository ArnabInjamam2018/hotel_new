<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_model extends CI_Model {
	
	 function  add_chain_master($data){
        $query=$this->db->insert('chain_master',$data);
        if($query){
            return true;
        }else{
            return false;
        }



    }
	
	
	
	 function  add_child_master($data){
        $query=$this->db->insert('child_account',$data);
        if($query){
            return true;
        }else{
            return false;
        }



    }
	
	
	
	
	
	function all_chain_master(){
		$this->db->select('*');
		$this->db->from('chain_master');//TABLE_PRE="hotel_"
		 $query=$this->db->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	function all_child_account(){
		$this->db->select('*');
		
		$this->db->from('child_account');//TABLE_PRE="hotel_"
		$this->db->order_by('id','desc');
		 $query=$this->db->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	function all_child_account1(){
		$this->db->select('*');
		$this->db->from('hotel_details');//TABLE_PRE="hotel_"
		 $query=$this->db->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	function delete_chain_master($id){
		
		$this->db->where('id',$id);
              $query=$this->db->delete('chain_master');
             
	}
	function delete_child_account($id){
		
		$this->db->where('master_id',$id);
              $query=$this->db->delete('chain_master');
             
	}
	
	function get_chain_master1($id){
		
		$this->db->where('id',$id);
      $query=$this->db->get('chain_master');
	  if($query->num_rows()>0){
		  return $query->row();
	  }
	  else{
		  return $query->row();
	  }
	}
	
	function get_chain_master($id){
		
		$this->db->where('id',$id);
      $query=$this->db->get('child_account');
	  if($query->num_rows()>0){
		  return $query->row();
	  }
	  else{
		  return $query->row();
	  }
	}
	
	function get_child($id){
		
		$this->db->where('id',$id);
      $query=$this->db->get('child_account');
	  if($query->num_rows()>0){
		  return $query->row();
	  }
	  else{
		  return false;
	  }
	}
	
	
	
	function update_master_account($data){

            $id=$data['id'];
              $this->db->where('id',$id);
              $query=$this->db->update('chain_master',$data);

              if($query){

                return true;
              }
   }
   
   function update_child_account($data){

            $id=$data['id'];
              $this->db->where('id',$id);
              $query=$this->db->update('child_account',$data);

              if($query){

                return true;
              }
   }
   
   function  add_transaction_account($data){
        $query=$this->db->insert('transaction_account',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
	function  add_bill_account($data){
        $query=$this->db->insert('bill_account',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
	function all_transaction_account(){
		$this->db->select('*');
		$this->db->from('transaction_account');//TABLE_PRE="hotel_"
		 $query=$this->db->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	function all_bill_account(){
		$this->db->select('*');
		$this->db->from('bill_account');//TABLE_PRE="hotel_"
		 $query=$this->db->get();
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	function get_transaction($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		$this->db->from('transaction_account');//TABLE_PRE="hotel_"
		 $query=$this->db->get();
        if($query){

            return $query->row();
        }
        else{

            return false;
        }
		
	}
	
	function update_transaction($data,$id){

            //$id=$data['id'];
              $this->db->where('id',$id);
              $query=$this->db->update('transaction_account',$data);

              if($query){

                return true;
              }
   }
   function all_hotel($id){
	  // echo "==".$id;
	  // exit;
		$this->db->select('*');
		$this->db->where('chain_id',$id);
		$this->db->from('hotel_details');//TABLE_PRE="hotel_"
		 $query=$this->db->get();
		 //print_r($query->result());exit;
        if($query){

            return $query->result();
        }
        else{

            return false;
        }
		
	}
	
	
	
}

?>