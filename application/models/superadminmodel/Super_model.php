<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Super_model extends CI_Model {
    
	/* Login Authorization */
	
	function __construct(){
		
		parent::__construct();
		/*$CI = &get_instance();
		$databsae = 'superadmin';
		$this->db = $CI->load->database($databsae, TRUE);*/
	}
	
    function login($user_name,$user_password){
		
	
		//echo "wwwww";
		return $this->mylogin($user_name,$user_password);
       
    }
	
	function mylogin($user_name,$user_password){
		
		 echo "helloqqq";
		// echo $this->db->database; 
		$this->db->where('user_name',$user_name);
		$this->db->where('user_password',$user_password);
		$query=$this->db->get('login');
        if($query->num_rows()==1){
          //print_r($query->row());exit;
		    return $query->row();
        }else{ 
            return false;
        }
	
	}	
	/* User Type Fetch */
	function login_user_type($data){
		$this->db->where('user_type_id',$data);
		$query=$this->db->get(TABLE_PRE.'user_type');
        if($query->num_rows()==1){
            return $query->row();
        }else{ 
            return false;
        }
	}
	
	
	function count_chain(){
		
		//$this->db->count('*');
		$query = $this->db->get('chain_master');
		return $query->num_rows();
		
		
	}
	
	function count_child_account(){
		
		//$this->db->count('*');
		$query = $this->db->get('child_account');
		return $query->num_rows();
			
	}
	/* Fetch User Info */
    function get_user_info($id){
		$this->db->get_where('login_id',$id);
        $query=$this->db->get(TABLE_PRE.'login');
        if($query->num_rows()==1){
            return $query->row();
        }else{ 
            return false;
        }
    }
	
	/* Admin Activate in Login Table */
	function activate_admin($status){
		$this->db->where('user_id',$status['login_id']);
        $query=$this->db->update(TABLE_PRE.'login',$status);
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
	/* Admin Activate in Admin Table */
	function activate_admin_status($admin){
		$this->db->where('admin_id',$admin['admin_id']);
        $query=$this->db->update(TABLE_PRE.'admin_details',$admin);
        if($query){
            return true;
        }else{
            return false;
        }
	}

    /*function get_permission($id){
        $this->db->where('user_id',$id);
        $query=$this->db->get(TABLE_PRE.'user_permission');
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
    }*/

    function get_permission($id){
        $this->db->where('user_id',$id);
        $qry=$this->db->get(TABLE_PRE.'login');
		$bt = $qry->row();
        $admin_role_id = $bt->admin_role_id; 
        //exit;		
        $this->db->where('user_permission_id',$admin_role_id);
        $query=$this->db->get(TABLE_PRE.'user_permission');
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
    }

	function get_db1_details($hotelId){
		
		$this->db->select('*');
		$this->db->where('hotel_id',$hotelId);
		$query = $this->db->get('chain_master');
		if($query){
			
			return $query->row();
		}
		else{
			
			return false;
		}
		
	}

	
	
    function online_insert($data){
        $query=$this->db->insert(TABLE_PRE.'online',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }
    function  check_shift($user_id,$date){

        $sql="select * from shift_log LEFT JOIN hotel_shift on shift_log.shift_id=hotel_shift.shift_id where shift_log.user_id='".$user_id."' and shift_log.date='".$date."' and shift_log.logged_out !='1'";
        $query=$this->db->query($sql);

        if($query->num_rows()==1){
            return $query->result();
        }else{
            return false;
        }

    }
	
	//INSERT login_session TABLE 
	function login_session($data){
        $query=$this->db->insert(TABLE_PRE.'login_session',$data);
        if($query){
            return $this->db->insert_id();
        }else{
			
            return false;
        }
    }
	
	//UPDATE login_session TABLE 
	function update_login_session($data,$id){
		$this->db->where('user_id',$data['user_id']);
		$this->db->where('hls_id',$id);
        $query=$this->db->update(TABLE_PRE.'login_session',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
	 function userInfo($data){
        
		
		$this->db->where('user_name',$data);
        $query=$this->db->get(TABLE_PRE.'login');
        if($query->num_rows()==1){
            return $query->row();            
        }else{ 
            return false;
        }
    }
	
	function login_details($system_info,$user_name){
		$this->db->where('user_name',$user_name);
        $query=$this->db->insert('login_details',$system_info);
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
	function logout_details($userid,$system_info){
		
		
		$this->db->where('id',$userid);
        $query=$this->db->update('login_details',$system_info);
        if($query){
            return true;
        }else{
            return false;
        }
			
		
	}
	
	function hotel_login($username,$pwd){
		//echo $this->db->database;
		//print_r($data);
		//exit;
		//echo $username.' pwd: '.$pwd; die();
		$this->db->select('*');
		$this->db->where('user_name',$username);
		$this->db->where('user_password',$pwd);
        $query=$this->db->get('hotel_login_master');
	   // echo $this->db->last_query();
	  //  exit;
        if($query->num_rows()==1){
        		
            return $query->row();
            
        }else{ 
            return false;
        }
    }
	
	  function add_hotel($hotel,$hotel_contact_details,$hotel_tax_details,$hotel_billing_settings,$hotel_general_preference,$admin_details,$login,$child_account)
	  {
		  
		  /*echo "*************************";
		echo $this->db1->database;
		echo "*************************";
		echo $this->db->database;
		//exit;
	  echo "<pre>";
		  print_r($hotel);
		  echo "</pre>";
		  echo "=======";
			  echo "<pre>";
		  print_r($hotel_contact_details);
		  echo "</pre>";
		  echo "=======";
		  echo "<pre>";
		  print_r($hotel_tax_details);
		  echo "</pre>";
		  echo "=======";
		  echo "<pre>";
		  print_r($hotel_billing_settings);
		  echo "</pre>";
		  echo "=======";
		  echo "<pre>";
		  print_r($hotel_general_preference);
		  echo "</pre>";
		  exit;
		  */
		  $query1=$this->db->insert('child_account',$child_account);//admin database........
		$query1=$this->db1->insert('child_account',$child_account);//dynamic database........
		$hotel_id = $this->db->insert_id();
		$data = array(
			'hotel_id' => $hotel_id,
			//'user_id'=>$this->session->userdata('user_id')
		);
		  $hotel=array_merge($hotel, $data);
		$query=$this->db1->insert('hotel_details',$hotel);//admin database........
		$query=$this->db->insert('hotel_details',$hotel);//admin database........
		$query=$this->db1->insert('hotel_hotel_details',$hotel);//dynamic database........
		
		
		$admin_details['admin_hotel'] = $hotel_id;
		$login['hotel_id'] = $hotel_id;
		$this->load->model('setting_model', 'user');
		$result = $this->user->add_admin_user($admin_details,$login,$hotel_id);
		
		$new_hotel_contact_details = array_merge($hotel_contact_details, $data);
		/*echo "<pre>";
		print_r($hotel_tax_details);
		print_r($new_hotel_contact_details);
		echo "</pre>";
		echo "*************************";
		echo $this->db1->database;
		echo "*************************";
		echo $this->db->database;
		exit;*/
		
		$new_hotel_tax_details = array_merge($hotel_tax_details , $data);
		$new_hotel_billing_settings = array_merge($hotel_billing_settings,$data);
		$new_hotel_general_preference = array_merge($hotel_general_preference,$data);
		$query_contact = $this->db1->insert(TABLE_PRE.'hotel_contact_details',$new_hotel_contact_details);
		$query_contact1 = $this->db->insert('hotel_contact_details',$new_hotel_contact_details);
		//$query_tax = $this->db->insert(TABLE_PRE.'hotel_tax_details',$new_hotel_tax_details);
		//$query_tax = $this->db1->insert(TABLE_PRE.'hotel_tax_details',$new_hotel_tax_details);
		$query_tax = 1;
		$query_billing = $this->db1->insert(TABLE_PRE.'hotel_billing_setting',$new_hotel_billing_settings);
		$query_billing = $this->db->insert(TABLE_PRE.'hotel_billing_setting',$new_hotel_billing_settings);
		$query_general = $this->db1->insert(TABLE_PRE.'hotel_general_preference',$new_hotel_general_preference);
		
		// table: hotel_settings create
		$settings_table = array(		
			'hotel_id' => $hotel_id,
			'gb_prefix' => 1,
			'sb_prefix' => 1,
			'booking_source_inv_applicable' => 1,
			'booking_note_inv_applicable' => 1,
			'company_details_inv_applicable' => 1,
			'service_tax_applicable' => 1,
			'service_charg_applicable' => 1,
			'luxury_tax_applicable' => 1,
			'invoice_setting_food_paln' =>1,
			'invoice_setting_service' => 1,
			'invoice_setting_extra_charge' => 1,
			'invoice_setting_laundry' => 1,
			'authorized_signatory_applicable' =>1,
			'invoice_pref' =>1,
			'invoice_suf' =>1,
			'test' =>1,
			'color' => 1,
			'total_tax' => 1,
			'unit_cat' => 1,
			'unit_no' =>1,
			
			
		);
		$this->db1->insert('hotel_settings',$settings_table);
		
		
		
		//BOOKING STATUS TYPE TABLE INSERT
	
		
		
        if($query){
				if($query_contact){
					if($query_tax){
						if($query_billing){
							if($query_general){
								return true;
							}else{
								return false;
							}
						}
						else{
							return false;
						}
					}
					else{
						return false;
					}
				}
				else{
					return false;
				}

        }else{
            return false;
        }
	}
	
	function get_hotel($hotel_id)
    {
        $this->db->where('hotel_id',$hotel_id);
        $query=$this->db->get('hotel_details');
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }



    }
}

?>