<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Hotel Objects</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/assets/dashboard/assets/admin/pages/css/login-soft.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo base_url();?>/assets/dashboard/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/assets/dashboard/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/assets/dashboard/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="<?php echo base_url();?>/assets/dashboard/assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/assets/dashboard/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="<?php echo base_url();?>assets/favicon.ico"/>
<style>
body {
	background: #364150;
}
.center {
	position: absolute;
	top: 50%;
	left: 50%;
	width: 50%;
	margin: -105px 0 0 -25%;
	text-align: center;
	height: 210px;
}
.center p {
	font-size: 22px;
	color: #FFF;
	font-family: "Open Sans", sans-serif;
}
.opt {
	border-radius: 8px;
}
.btn:hover {
	color: #333333;
	background: #099;
	border: 1px solid black;
}
.opt:hover {
	background: yellow !important;
}
.btn:hover {
	color: #333333;
	background: #099;
	border: 1px solid #5BC8B8;
}
@media screen and (max-width:768px){
	p a, p span{
		display:block;
	}
	p span{
		margin:10px 0 !important;
	}
	.center p{
		font-size:14px;
	}
}
@media screen and (max-width:480px){
	.center{
		width:100%;
		left:0;
		margin-left:0;
	}
}
</style>
</head>
<body>
<?php

$form = array(
    'class' 			=> 'form-body',
    'id'				=> 'form',
    'method'			=> 'post'
);

echo form_open_multipart('superadmin/redirect',$form);

if(isset($hotels)&& $hotels):
?>
<div class="center">
  <h1> <img src="<?php echo base_url();?>/assets/dashboard/assets/admin/layout/img/hotel-object1.png" style="width:50%;"></h1>
  <?php
    date_default_timezone_set("Asia/Kolkata");

    $shifts=$this->dashboard_model->all_shift_limit();

    foreach($shifts as $shift){
        $time =date("H:i");
        if($time < $shift->shift_closing_time && $time > $shift->shift_opening_time){

            $shift_name=$shift->shift_name;

            $shift_id=$shift->shift_id;

        }
    }
    $drawers=$this->dashboard_model->all_cash_drawer_limit();
    if($drawers && isset($drawers)){
        foreach($drawers as $drawer) {
            ?>
  <div class="content">
    <p>Current Cash Drawer Session Id: <?php echo $drawer->drawer_id; ?></p>
    <p><a class="btn green" style="color: #ffffff;" href="<?php echo base_url() ?>cashdrawer/old_session?id=<?php echo $drawer->drawer_id; ?>">Enter</a> <span style="margin-left:20px;margin-right:20px;margin-bottom:20px;">OR</span> <a class="btn blue" style="color: #ffffff;" href="<?php echo base_url() ?>cashdrawer/new_session?id=<?php echo $drawer->drawer_id; ?>">Create A New Cash Drawer Session </a></p>
    <?php
        }}else{

        ?>
    <p><?php echo "No Cash Drawer Session" ; ?></p>
    <a style="color: #ffffff" href="<?php echo base_url() ?>cashdrawer/new_session">Create A New Cash Drawer Session </a>
    <?php


    }

    ?>
    <p>Current Shift: <?php echo $shift_name; ?></p>
    <!-- <select  name="user_hotel" onchange="xyz()" >
        <option class="opt" value="">Select Hotel</option>
        <?php foreach($hotels as $hot):
            ?>
            <option  value="<?php echo $hot->hotel_id; ?>"><?php echo $hot->hotel_name; ?></option>
        <?php endforeach; ?>
    </select> --> 
    
    <!--<button class="btn" type="submit" id="go">Go to preffered hotel</button>-->
    
    <?php endif; ?>
  </div>
</div>
<?php form_close(); ?>
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN PAGE LEVEL PLUGINS --> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/select2/select2.min.js"></script> 
<!-- END PAGE LEVEL PLUGINS --> 
<!-- BEGIN PAGE LEVEL SCRIPTS --> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/scripts/metronic.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/admin/layout/scripts/layout.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/admin/layout/scripts/demo.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/admin/pages/scripts/login-soft.js" type="text/javascript"></script> 
<!-- END PAGE LEVEL SCRIPTS --> 
<script src="<?php echo base_url();?>assets/common/js/jquery.validate.min.js"></script> 
<script src="<?php echo base_url();?>assets/login/js/index.js"></script> 
<script>
jQuery(document).ready(function() {     
  Metronic.init(); // init metronic core components
Layout.init(); // init current layout
  Login.init();
  Demo.init();
       // init background slide images
       $.backstretch([
        "<?php echo base_url();?>/assets/dashboard/images/1.jpg",
        "<?php echo base_url();?>/assets/dashboard/images/2.jpg",
        "<?php echo base_url();?>/assets/dashboard/images/3.jpg",
        "<?php echo base_url();?>/assets/dashboard/images/4.jpg"
        ], {
          fade: 1500,
          duration: 5000
    }
    );
});
function xyz(){
	document.getElementById('form').submit();
}
</script>
</body>
</html>
