<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->

<!--[if !IE]><!-->

<html lang="en" class="no-js">

<!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

<meta charset="utf-8"/>

<title>RoyalPOS | <?php if(isset($heading) && $heading) echo $heading; else echo "Dashboard";?></title>

<meta content="width=device-width, initial-scale=1" name="viewport"/>

<!--<link rel="manifest" href="<?php echo base_url();?>assets/manifest.json" />-->

<link rel="shortcut icon" href="<?php echo base_url();?>assets/favicon.ico" type="image/x-icon" />

<!--<link href="https://fonts.googleapis.com/css?family=Exo+2:400,700|Monda:400,700|Open+Sans:400,700|Roboto:400,700" rel="stylesheet">-->

<link href="<?php echo base_url();?>assets/layouts/layout/css/font.css" rel="stylesheet" type="text/css" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo base_url();?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url();?>assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="<?php echo base_url();?>assets/favicon.ico" />
<!-- END HEAD -->
<style>
@media (max-width: 440px){
	.login .content .btn{
		font-size:10px;
	}
}
</style>
</head>


<body class="page-md login">
  <div class="logo"> 
 <!-- <img src="<?php //echo base_url();?>assets/layouts/layout/img/hotel-object1.png" style="width:23%;"/> -->
  </div>
  <div class="content portlet bordered">
    <?php
	$cash=0;
$form = array(
    'class' 			=> 'login-form',
    'id'				=> 'form',
    'method'			=> 'post'
);

//echo form_open_multipart('superadmin/redirect',$form);

if(isset($hotels)&& $hotels):
?>
    <?php 
    date_default_timezone_set("Asia/Kolkata");

    $shifts = $this->dashboard_model->all_shift_limit();
	$shift_name = 'n/a';
	
	
	//echo '<br>'.is_array($shifts).'<br>';
	
    if(isset($shifts) && $shifts){
		foreach($shifts as $shift){
        $time =date("H:i:s");
			if($time < $shift->shift_closing_time && $time > $shift->shift_opening_time){

				$shift_name=$shift->shift_name;

				$shift_id=$shift->shift_id;

			}
		}
	}
	else{
		//echo 'n/a';
	}
	$i = 0;
	
    $drawers=$this->dashboard_model->all_cash_drawer_limit();
	//echo "=========================<pre>";
	//print_r($drawers);
	//exit;
    if($drawers && isset($drawers)){
		echo '
		
		
		';
		//echo "<pre>";
		//print_r($this->session->userdata());exit; 
    ?>
	 
	<div class="portlet-title">
        <div class="uppercase text-center">
            Select an open cash drawer session
			<br/><small>or</small><br/>
			create one
        </div>                           
    </div>

	<div class="portlet-body">
		<div class="mt-element-list">
		<div class="mt-list-head list-simple ext-1 font-white bg-green-sharp text-center" style="padding: 8px;">
                                                Click to select Cash Darwers
                                        </div>
		<div class="mt-list-container list-simple ext-1">
		
	<ul class="cash-draw">
	<?php
	
	if(isset($drawers) && $drawers){
	
	foreach($drawers as $drawer) {
             
	$cash= $this->unit_class_model->get_closing_stock();
	$i = $cash->drawer_id;
	$cls1 = '';
	$stl = '';
	if($drawer->status != 'Success' || $drawer->user_id == $this->session->userdata('user_id')){
		$cls1 = 'done';
		
		if($drawer->user_id == $this->session->userdata('user_id'))
			$stl = 'style="color:#EF4B51; font-weight:800;"';
	}
	

	?>
    
	<li class="mt-list-item <?php echo $cls1;?>" style="padding: 8px;">
                                                    <div class="list-icon-container aller">
                                                        <?php echo $drawer->drawer_id; ?>
                                                    </div>
           <div class="list-datetime aller uppercase"> <?php
			if($drawer->profitCenter>0){
			$n=$this->unit_class_model->getProfitCenterById($drawer->profitCenter);
			echo $n->profit_center_location;
			//echo $drawer->profitCenter;
			
			}else{
				echo $n='N/A';
			}
			?> 
		   </div>
                                                    <div class="list-item-content text-center uppercase">
                                                        
														<?php
															if($cls1 == 'done'){
														?>
                                                            <a <?php echo $stl; ?> href="<?php echo base_url() ?>cashdrawer/old_session?id=<?php echo $drawer->drawer_id;?>" class="Monda"><?php echo $drawer->cashDrawerName; ?></a>
														<?php
															}
															else{
														?>
															<span <?php echo $stl; ?> href="<?php echo base_url() ?>cashdrawer/old_session?id=<?php echo $drawer->drawer_id;?>" class="Monda"><?php echo $drawer->cashDrawerName; ?></span>
															<?php }?>
                                                        
                                                    </div>
    </li>
	
	
	<?php
        }
	}?>
		
		 </div>
         </div>
		</ul>
		
		
		<span style="margin:7px 0; display:block;" class="text-center font-grey-cascade">- OR -</span>
		
	
		 <button type="button" class="btn Red Sunglo btn-block" data-toggle="collapse" data-target="#demo">Create A New Cash Drawer Session </button>
		
		</p>
	<?php }else{

        ?>
    <h3 class="form-title uppercase" style="font-size: 17px;"><?php echo "No Cash Drawer Session" ; ?></h3>
	
  
    <button type="button" class="btn green btn-block" data-toggle="collapse" data-target="#demo">Create A New Cash Drawer Session </button>
    <?php


    }
		date_default_timezone_set("Asia/Kolkata");
		$gg = $this->unit_class_model->get_closing_stock();
		
			if(isset($cash->closing_cash)){$c =$cash->closing_cash; }else{$c ='';}
		if(isset($cash->drawer_id)){$i =$cash->drawer_id; }else{$i ='';}
			
    ?>
	<div id="demo" class="collapse">
   
   
    <div class="form-group">
        <div class="input-icon"> 
        	<input type="text" class="form-control"  id="name" name="name"  placeholder="Name">
        </div>
    </div>
	
    
                        <div class="form-group">
                          <?php $pc1=$this->dashboard_model->all_pc(); 
						  if(isset($pc1) && $pc1){
						  ?>
                          <select name="p_center" id="profitcenter" class="form-control" id="" required>
                           
                            <?php $pc=$this->dashboard_model->all_pc();
                                foreach($pc as $prfit_center){
                                ?>
                            <option value="<?php echo $prfit_center->id;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                            <?php }?>
                          </select>
						  <?php }else {?>
						  
						  <input type="text" class="form-control" name="p_center1" id="p_center1" placeholder="No Cash drawer Available Enter A Name" required >
						  <?php }?>
                        </div>
						
						     
                      
   
	
	 <input type="hidden" name="opening" id="opening" value="<?php echo $c ?>">
 	<div class="form-group">
        <div class="input-icon"> <i class="fa fa-inr"></i>
        	<input type="text" class="form-control" id="deposit" name="deposit"  placeholder="Deposit">
        </div>
    </div>
  
   
   <button  onclick="cashdrawer()" class="btn btn-primary btn-block">Submit</button> 
   
   
   
   
   
   
   
   
   
  </div>
    <div class="create-account" style="margin-top:25px;">
			<p class="uppercase" style="color:#ffffff">
				Running Shift: <?php echo $shift_name; ?>
			</p>
		</div>        
    <?php endif; ?>
    <?php form_close(); ?>
  </div>
</div>
<div class="copyright" style="color:#fff;"> 

	<?php echo date('Y'); ?> &copy; <a href="http://BookMania.com" title="BookMania" target="_blank" style="color:#fff;">All Rights Reserved @ BookMania</a> 

</div>


<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url();?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/login.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS --> 
<script>
jQuery(document).ready(function() { 
       // init background slide images
       $.backstretch([
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/1.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/2i.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/3.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/4.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/5.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/6.jpg",		
        ], {
          fade: 1500,
          duration: 5000
    });
});
</script> 
<!-- END JAVASCRIPTS --> 
<script>
function xyz(){
	document.getElementById('form').submit();
}

function clossing_cash_drawer_alert(id){
	
			alert('CASH DRAWER ID CLOSE:  '+id);
       /*   $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>cashdrawer/get_clossing_cash_drawer_info",
                data:{id:id},
                 success:function(data)
                {
					 alert(data.id);
				/*swal({   title: "Are you sure?",
				text: data+"!",  
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Fix as Default!",   cancelButtonText: "No, cancel plz!", 
				closeOnConfirm: true,  
				closeOnCancel: true },
				function(isConfirm){   
				 });
                }
            });*/
			swal({   title: "Are you sure?",
				text: 'hhh',  
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Fix as Default!",   cancelButtonText: "No, cancel plz!", 
				closeOnConfirm: true,  
				closeOnCancel: true },
				function(isConfirm){   
				 });
		  
	}

function new_cash_drawer(){
	alert('hello world');
}
</script>
<script>
function cashdrawer(){
	//alert(<?php echo $this->session->userdata('user_hotel')?>);
	
	var name=$('#name').val();
	var deposit=$('#deposit').val();
	//var withdraw=$('#withdraw').val();
	var profitcenter=$('#profitcenter').val();
	var p_center1=$('#p_center1').val();
	var opening=$('#opening').val();
	//alert(deposit+profitcenter+name+p_center1);
	if(profitcenter==''){
	if(p_center1!=''){
		profitcenter=1;
	}
	}
	jQuery.ajax(
                        {
                            type: "POST",
                            url: "<?php echo base_url(); ?>cashdrawer/add_cash_index2",
                           // dataType: 'json',
                            data: {opening:opening,name:name,deposit:deposit,profitcenter:name,p_center1:p_center1},
                            success: function(data){
								//alert(data);
							if(data==1){
								$('#responsive2').modal('hide');
								//swal("Success!", name+" is set as new cashdrawer : ", "success");
								window.location.replace("<?php echo base_url().'dashboard';?>");
							}
                            }
                        });
						//redirect('dashboard');
	
}


</script>
			
</body>
</html>
