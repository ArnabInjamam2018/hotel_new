<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Hotel Objects</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link href='https://fonts.googleapis.com/css?family=Exo+2:400,600,700,800,500' rel='stylesheet' type='text/css'>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>/assets/dashboard/assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo base_url();?>assets/dashboard/assets/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/global/css/plugins-md.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/admin/layout/css/themes/light2.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="<?php echo base_url();?>assets/favicon.ico"/>
<style>
@media (max-width: 440px){
	.login .content .btn{
		font-size:10px;
	}
}
</style>
</head>
<body class="page-md login">
  <!--<div class="logo"> <img src="<?php //echo base_url();?>/assets/dashboard/assets/admin/layout/img/hotel-object1.png" style="width:23%;"/> --></div>
  <div class="content">
    <?php

$form = array(
    'class' 			=> 'login-form',
    'id'				=> 'form',
    'method'			=> 'post',
);

echo form_open_multipart('Cashdrawer/add_cash',$form);

date_default_timezone_set("Asia/Kolkata");
$cash= $this->unit_class_model->get_closing_stock();
$c=$cash->closing_cash;	

$i=$cash->drawer_id;

?>
    
<h3 class="form-title uppercase" style="font-size: 17px;">Cash In Hand</h3>	

<div class="form-group"> 
  <input type="hidden" name="hid" value="<?php if(isset ($data1) ){echo $data1['id'];};?>">
  <div class="input-icon"> <i class="fa fa-inr"></i>
  <input type="text" class="form-control" name="amount" id="amount" placeholder="<?php echo $c; ?>" disabled>
  </div>
</div>
 
<div class="row">
 <div class="col-sm-6">
 <div class="form-group">
        <div class="input-icon"> <i class="fa fa-inr"></i>
        	<input type="text" class="form-control" id="withdraw"  onkeypress="return onlyNos(event, this);" min="1" max="<?php echo $c; ?>" name="name" onfocus="abc()" placeholder="Name">
        </div>
    </div>
	<div class="form-group">
    
                        <div class="form-group">
                          <label>Profit Center <span class="required"> * </span> </label>
                          <select name="p_center" class="form-control input-sm bs-select" id="" required>
                            <?php $pc=$this->dashboard_model->all_pc();?>
                            <?php
                              $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
                              ?>
                            <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
                            <?php $pc=$this->dashboard_model->all_pc1();
                                foreach($pc as $prfit_center){
                                ?>
                            <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                            <?php }?>
                          </select>
                        </div>
                      
    </div>
 	<div class="form-group">
        <div class="input-icon"> <i class="fa fa-inr"></i>
        	<input type="text" class="form-control" id="withdraw"  onkeypress="return onlyNos(event, this);" min="1" max="<?php echo $c; ?>" name="withdraw" onfocus="abc()" placeholder="Withdraw">
        </div>
    </div>
	<input type="hidden" name="hid1" value="<?php echo $c; ?>">
 </div>
 <div class="col-sm-6">
 	<div class="form-group">
        <div class="input-icon"> <i class="fa fa-inr"></i>
        	<input type="text" class="form-control" id="deposit" name="deposit" onfocus="abc1()" placeholder="Deposit">
        </div>
    </div>
  </div>
</div>

<button type="submit"  class="btn btn-primary btn-block">Submit</button>        
  
    <?php form_close(); ?>
</div>
<div class="copyright" style="color:#fff;"> 2016 &copy; <a href="http://www.theinfinitytree.com/" title="The Infinity Tree" style="color:#fff;">The Infinity Tree</a> </div>
<!--[if lt IE 9]>
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/excanvas.min.js"></script> 
<![endif]--> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN PAGE LEVEL PLUGINS --> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/assets/dashboard/assets/global/plugins/select2/select2.min.js"></script> 
<!-- END PAGE LEVEL PLUGINS --> 
<!-- BEGIN PAGE LEVEL SCRIPTS --> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/global/scripts/metronic.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/admin/layout/scripts/layout.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/admin/layout/scripts/demo.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>/assets/dashboard/assets/admin/pages/scripts/login-soft.js" type="text/javascript"></script> 
<!-- END PAGE LEVEL SCRIPTS --> 
<script src='<?php echo base_url();?>assets/common/js/jquery.validate.min.js'></script> 
<script src="<?php echo base_url();?>assets/login/js/index.js"></script> 
<script>
jQuery(document).ready(function() {     
  Metronic.init(); // init metronic core components
Layout.init(); // init current layout
  Login.init();
  Demo.init();
       // init background slide images
       $.backstretch([
        "<?php echo base_url();?>/assets/dashboard/images/1.jpg",
        "<?php echo base_url();?>/assets/dashboard/images/2i.jpg",
        "<?php echo base_url();?>/assets/dashboard/images/3.jpg",
        "<?php echo base_url();?>/assets/dashboard/images/4.jpg",
        "<?php echo base_url();?>/assets/dashboard/images/5.jpg",
        "<?php echo base_url();?>/assets/dashboard/images/6.jpg",		
        ], {
          fade: 1500,
          duration: 5000
    }
    );
});
function xyz(){
	document.getElementById('form').submit();
}

function abc(){
	//alert('fuck');
	$('#deposit').val('');
}
function abc1(){
	//alert('fuck1');
	$('#withdraw').val('');
}
</script>
</body>
</html>
