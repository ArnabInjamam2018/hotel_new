<?php 

$onlines=$this->dashboard_model->all_onlines();

$admin_data=$this->dashboard_model->get_user_details($this->session->userdata('user_id'));
$num_messages=0;
$num_messages=$this->dashboard_model->all_messages($this->session->userdata('user_id'));
$id = 1;

if(isset($admin_data) && $admin_data){
	foreach($admin_data as $ad){

		$name=$ad->admin_first_name;
		$image=$ad->admin_image;
		$image_url=base_url("upload/admin/".$ad->admin_image);
	}
}

?>

<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-toastr.min.js" type="text/javascript"></script>
<?php
$tasks = $this->dashboard_model->tasks_pending_unique();
$j=0;
if(isset($tasks)&&$tasks){
    $j=0;
    foreach($tasks as $task) {

        $t_details=$task->task;

        ?>
<?php $j++;}} ?>


<script type="text/javascript">
$(document).ready(function() {	

	
	// start New Code 	
	$('.single_2').on('mouseenter',mouseEnter);
	$('.single_2').on('mouseleave', mouseLeave);
	function mouseEnter() {
	  clearTimeout($(this).data('h_hover'));
	  var that = this;
	  var h_hover = setTimeout(function(){
		$(that).click();
	  }, 1500);
	  $(this).data('h_hover',h_hover)
	}

	function mouseLeave() {
	  clearTimeout($(this).data('h_hover'));
	  $.fancybox.close();
	}	
	// End New Code	

	$(".single_2").fancybox({
		openEffect	: 'fade',
		closeEffect	: 'fade',

	});
	
});


</script>

<!-- BEGIN BODY -->

<body class="page-header-fixed page-content-white page-sidebar-closed page-md">
<div class="page-wrapper">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top"> 
  <!-- BEGIN HEADER INNER -->
  <div class="page-header-inner"> 
    <!-- BEGIN LOGO -->
    <div class="page-logo"> <a href="<?php echo base_url();?>"> <!--<img src="<?php //echo base_url();?>assets/pages/img/logo-hotel.png" alt="logo" class="logo-default"/>-->
	</a>
      <div class="menu-toggler sidebar-toggler hide"> </div>
    </div>
    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"><span></span></a>
    <div class="top-menu">
      <ul class="nav navbar-nav pull-right">
		<li class="dropdown dropdown-extended" id="header_notification_bar"> <a href="<?php echo base_url();?>dashboard#quickInfo" class="dropdown-toggle"> <i class="fa fa-bell-o"></i> <span class="badge badge-default">
          <?php
                    echo '..'; ?>
          </span> </a>
         
        </li>
		
        <!-- END NOTIFICATION DROPDOWN    <span class="badge badge-default"> <?php //echo $i; ?></span> --> 
        <!-- BEGIN INBOX DROPDOWN --> 
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        
        <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <i class="fa fa-envelope-open-o"></i> <span class="badge badge-default">
          <?php
                    $num_messages=0;
                    $num_messages=$this->dashboard_model->all_messages($this->session->userdata('user_id'));
                    $all_messages=$this->dashboard_model->all_messages_unseen($this->session->userdata('user_id'));
                    echo $num_messages; ?>
          </span> </a>
          <ul class="dropdown-menu">
            <li class="external">
              <h3>You have <span class="bold"><?php echo $num_messages; ?> New</span> Messages</h3>
              <a href="#">view all</a>
            </li>
            <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
              <?php
                            if(isset($all_messages)&&$all_messages):
                                foreach($all_messages as $msg):


                                        ?>
              <li class=" dropdown dropdown-quick-sidebar-toggler"> <a href="javascript:;" class="dropdown-toggle"> 
                <!--<span class="time"></span>--> 
                <span class="details"> <span class="label label-sm label-icon label-warning"> <i class="fa fa-envelope"> </i>
                <?php
                                        $admin_details=$this->dashboard_model->get_admin($msg->u_from_id);
                                        foreach($admin_details as $det) {
                                            echo $det->admin_first_name;
                                        }
                                        ?>
                </span> <?php echo "     ".$msg->m_body ?></span> </a> </li>
              <?php endforeach; ?>
              <?php endif; ?>
            </ul>
          </ul>
        </li>
        <li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <i class="fa fa-calendar"></i> <span class="badge badge-default"> <?php echo $j ?> </span> </a>
          <ul class="dropdown-menu extended tasks">
            <li class="external">
              <h3>You have <span class="bold"><?php echo $j ?> pending</span> tasks</h3>
              <a href="#">view all</a> </li>
            <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
              <?php
                        $tasks = $this->dashboard_model->tasks_pending_unique();

                        if(isset($tasks)&&$tasks){
                        $j=0;
                        foreach($tasks as $task) {

                        $t_details=$task->task;

                        ?>
              <li> <a href="<?php base_url() ?>dashboard/task_details?task_id=<?php echo $task->id ?>&task_asignee=<?php echo $task->from_id ?>"> 
                <!--<span class="time"></span>--> 
                <span class="details"> <span class="label label-sm label-icon label-warning"> <i class="fa fa-exclamation"></i> </span> <?php echo $t_details ?>.</span> </a> </li>
              <?php }} ?>
            </ul>
          </ul>
        </li>
        <li class="dropdown dropdown-user"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
         
          <?php 
					$id=$this->session->userdata('user_id');
					
					
					$admin_info=$this->dashboard_model->admin_status2($id);
					/*echo "<pre>";			print_r($admin_info);*/
					$type=$this->dashboard_model->get_user_type1($admin_info->admin_user_type);
					if($type->user_type_slug=='SUPA'){
						$typ="SA";
					}else if($type->user_type_slug=='AD'){
						$typ="A";
					}else if($type->user_type_slug=='SUBA'){
						$typ="SbA";
					}else {
						$typ="G";
					}
					
					if($admin_info->admin_image){
						$im=$admin_info->admin_image;
					}
					else{
						$im='';
					}
					
				if($admin_info->admin_first_name){
				
				$info=$admin_info->admin_first_name.' ('.$typ.') ';
				}else{
					$info='';
				}
					//if($this->session->userdata('user_type_slug')=="AD" && isset($admin_info) && $admin_info):?>
          <img alt="" class="img-circle" src="<?php echo base_url();?>upload/admin/<?php echo $im;?>"/> <span class="username username-hide-on-mobile"> <?php echo $info;?> </span>
          <?php //endif;?>
          <i class="fa fa-angle-down"></i> </a>
          <ul class="dropdown-menu dropdown-menu-default">
			<?php
  $id= 1;
   $this->load->model('superadminmodel/super_model');
   ?> 
		  
		  
             <li> 
               <a href="<?php echo base_url(); ?>dashboard/profile"> <i class="icon-user"></i> My Profile </a> 
	</li>
           
            <li> <a href="<?php echo base_url(); ?>dashboard/add_booking_calendar"> <i class="icon-calendar"></i> My Calendar </a> </li>
            <!--<li> <a href="#"> <i class="icon-envelope-open"></i> My Inbox <span class="badge badge-danger"> <?php echo $num_messages; ?></span> </a> </li>-->
            <li> <a href="<?php echo base_url(); ?>dashboard/all_tasks"> <i class="icon-rocket"></i> My Tasks <span class="badge badge-success"> <?php echo $j ?> </span> </a> </li>
            <li class="divider"> </li>
            <li> <a href="<?php echo base_url();?>dashboard/lockscreen"> <i class="icon-lock"></i> Lock Screen </a> </li>
            <li> <a href="<?php echo base_url();?>dashboard/logout"> <i class="icon-key"></i> Log Out </a> </li>
          </ul>
        </li>
        <!-- END USER LOGIN DROPDOWN --> 
        <!-- BEGIN QUICK SIDEBAR TOGGLER --> 
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        
        <li class="dropdown dropdown-quick-sidebar-toggler"> <a href="javascript:;" class="dropdown-toggle"> <i class="fa fa-angle-double-left"></i> </a> </li>
        
        <!-- END QUICK SIDEBAR TOGGLER -->
      </ul>
    </div>
    <!-- END TOP NAVIGATION MENU -->
    <div class="clint_logo" id="cl_logo" >
      
       <select id="payments" name="payments" style="width:auto; display:none;" onchange="hotel(this.value)">
	   
	   <?php 
	  //$id11=1 ;
	  $user_id= $this->session->userdata('user_id');
	  
	  $hotel=$this->dashboard_model->all_hotel($id11,$user_id);
	  

	foreach($hotel as $ht){
	
		$admin_hotel=explode(',',$ht->admin_hotel);
	
		for($i=0;$i<count($admin_hotel);$i++){
		$hotel=$this->dashboard_model->get_hotel_details2($admin_hotel[$i]);
		
		foreach($hotel as $hot){
			echo "HID".$hot->hotel_id;
			 $h_name=$this->dashboard_model->hotel_name($hot->hotel_id);
			 $h_img=$this->dashboard_model->hotel_information($hot->hotel_id);
			?>

			<option value="<?php echo $hot->hotel_id  ?>" 
			<?php if($this->session->userdata('user_hotel')==$hot->hotel_id) echo " selected "; ?>
			data-image="<?php echo base_url();?>upload/hotel/<?php echo $h_img->hotel_logo_images_thumb; ?>" >
			<?php
			echo $h_name->hotel_name;
			?></option>
			
		<?php
		}
		
		}
		}
	
		?>
      </select>
      
    </div>
  </div>
  <!-- END HEADER INNER --> 
</div>
<!-- END HEADER -->

<script>

function hotel(id){
	$.ajax({
					type:"POST",
					url: "<?php echo base_url()?>dashboard/hotel_change",
					data:{a:id},

					success:function(data)
					{
					   swal(' '+data.msg,'Welcome to Hotel '+data.hotel, 'success');
					   //$data=$this->load->view('backend/dashboard/index',$data,TRUE);
					   //$this->set_output($data); 
						
						window.location.replace("<?php echo base_url();?>Cashdrawer/index2");
						//redirect("<?php echo base_url();?>Cashdrawer/index2");
						
					},  

	});

}
</script>
<script>
  
$('#header_notification_bar').on('click',function(){
	$('#dash_bo a').click();
});

</script>