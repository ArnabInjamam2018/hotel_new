<!DOCTYPE html>
<html >
<head>
<meta charset="UTF-8">
<title>HotelObjects</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<link href='https://fonts.googleapis.com/css?family=Exo+2:400,600,700,800,500' rel='stylesheet' type='text/css'>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />-->
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />-->
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<!--<link href="<?php echo base_url();?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url();?>assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />-->
<link href="<?php echo base_url();?>assets/global/plugins/layout.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/pages/css/lock-2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="<?php echo base_url();?>assets/favicon.ico" />
<!-- END HEAD -->
</head>
<body>
<div class="page-lock">
	<div class="page-logo">
		<a class="brand" href="index.html">
		<img src="<?php echo base_url();?>assets/layouts/layout/img/hotel-object1.png" style="width:23%;">
		</a>
	</div>
	<div class="page-body">
	<?php if(isset($admin_name)): ?>
        <?php foreach($admin_name as $admin): ?>
	<?php ?>
		<?php if($this->session->userdata('user_type_slug')=="SUPA"):?>
          <img alt="" class="page-lock-img" src="<?php echo base_url();?>upload/admin/user.png"/>
          <?php endif;?>
          <?php 
			$id=$this->session->userdata('user_id');
			$admin_info=$this->dashboard_model->admin_status($id);
			if($this->session->userdata('user_type_slug')=="AD" && isset($admin_info) && $admin_info):?>
			<img alt="" class="page-lock-img" src="<?php echo base_url();?>upload/admin/<?php echo $admin_info->admin_image;?>"/> 
          <?php endif;?>        
		<div class="page-lock-info">
		
		
		
			<h1>Locked</h1>
            <span class="locked"><?php echo $admin->admin_first_name ?> <?php echo $admin->admin_last_name ?></span>
			<span class="email"><?php echo $admin->admin_email ?> </span>			
			<form method="post" class="form-inline" action="<?php echo base_url() ?>dashboard/unlock">
            	<div class="form-group form-md-line-input">
                    <input type="password" name="p_lock" class="form-control" placeholder="Password">
                    <label></label>
                </div>
                <button type="submit" class="btn blue icn-only"><i class="m-icon-swapright m-icon-white"></i></button>
				<!-- /input-group -->
				<div class="relogin">
					<a href="<?php echo base_url() ?>/dashboard/logout">
					Not <?php echo $admin->admin_first_name ?> ?</a>
				</div>
			</form>

            <?php endforeach; ?>
			
		<?php endif; ?>
		</div>
	</div>
	<div class="page-footer-custom text-center">
		 <?php echo date('Y'); ?> &copy; <a href="http://www.infitree.co/" title="The Infinity Tree" style="color:#fff;">Infitree Elucidation</a>
	</div>
</div>
<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>-->
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript">
</script>
<script src="<?php echo base_url();?>assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url();?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/login.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS --> 
<script>
jQuery(document).ready(function() { 
       // init background slide images
       $.backstretch([
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/1.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/2i.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/3.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/4.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/5.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/6.jpg",		
        ], {
          fade: 1500,
          duration: 5000
    });
});
</script> 
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>