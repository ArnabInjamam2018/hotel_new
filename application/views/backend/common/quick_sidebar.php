</div>
</div>
</div>
</div>
<a href="javascript:;" class="page-quick-sidebar-toggler">
    <i class="fa fa-angle-double-right"></i>
</a>
<div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
  <div class="page-quick-sidebar">
    <div class="nav-justified">
      <ul class="nav nav-tabs nav-justified">
        <li class="active"> <a href="#quick_sidebar_tab_1" data-toggle="tab"> Users
          <?php
                            $i=0;
                            if(isset($onlines) && $onlines): ?>
          <?php foreach($onlines as $online):
                                   date_default_timezone_set('Asia/Kolkata');
                                   /* echo $online->online_from;
                                    echo  date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s"))-60*5);*/

                            $details=$this->dashboard_model->get_user_details($online->u_id);
                            ?>
          <?php if(isset($details) && $details): ?>
          <?php foreach($details as $d):

                                        $i++;

                            ?>
          <?php
                            endforeach;
                            endif;
                                endforeach;
                            endif;
                            ?>
          <span class="badge badge-danger"><?php echo $i; ?></span> </a> </li>
        <li> <a href="#quick_sidebar_tab_2" data-toggle="tab"> Alerts <span class="badge badge-success">0</span> </a> </li>
        <li class="dropdown"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> More<i class="fa fa-angle-down"></i> </a>
          <ul class="dropdown-menu pull-right" role="menu">
            <li> <a href="#quick_sidebar_tab_3" data-toggle="tab"> <i class="icon-bell"></i> Alerts </a> </li>
            <li> <a href="#quick_sidebar_tab_3" data-toggle="tab"> <i class="icon-info"></i> Notifications </a> </li>
            <li> <a href="#quick_sidebar_tab_3" data-toggle="tab"> <i class="icon-speech"></i> Activities </a> </li>
            <li class="divider"> </li>
            <li> <a href="#quick_sidebar_tab_3" data-toggle="tab"> <i class="icon-settings"></i> Settings </a> </li>
          </ul>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active page-quick-sidebar-chat" id="quick_sidebar_tab_1">
          <div class="page-quick-sidebar-chat-users" data-rail-color="#ddd" data-wrapper-class="page-quick-sidebar-list">
            <h3 class="list-heading">Staff</h3>
            <ul class="media-list list-items">
              <?php if(isset($onlines) && $onlines): ?>
              <?php foreach($onlines as $online):

                                        $details=$this->dashboard_model->get_user_details($online->u_id);
                                        ?>
              <?php if(isset($details) && $details): ?>
              <?php foreach($details as $d):

                                            ?>
              <li class="media" onClick="return chat_fetch(<?php echo $d->admin_id; ?>,'<?php echo $d->admin_first_name; ?>','<?php echo base_url("upload/".$d->admin_image); ?>')">
                <div class="media-status"> <span class="badge badge-success"><?php echo "1"; ?></span> </div>
                <img class="media-object" src="<?php echo base_url("upload/".$d->admin_image); ?>" alt="...">
                <div class="media-body">
                  <h4 class="media-heading"><?php echo $d->admin_first_name." ".$d->admin_last_name ?></h4>
                  <div class="media-heading-sub">
                    <?php
                                             if($d->admin_user_type==1){
                                                 echo "Super Admin";
                                             }
                                             else if($d->admin_user_type==2){
                                                 echo "Admin";
                                             }
                                             else if ($d->admin_user_type==3){
                                                 echo "Sub Admin";
                                             }
                                             else{
                                                 echo "Sub Admin";
                                             }
                                             $hot=$this->dashboard_model->get_hotel($d->admin_hotel);

                                             if($d->admin_user_type!=1){

                                             echo ": ".$hot->hotel_name;
                                           }

                                             ?>
                  </div>
                </div>
              </li>
              <?php
                                endforeach;
                                endif;
                                    endforeach;
                                endif;
                                        ?>
            </ul>
          </div>
          <div class="page-quick-sidebar-item">
            <div class="page-quick-sidebar-chat-user">
              <div class="page-quick-sidebar-nav"> <a href="javascript:;" class="page-quick-sidebar-back-to-list"><i class="icon-arrow-left"></i>Back</a> </div>
              <div class="page-quick-sidebar-chat-user-messages"> </div>
              <div class="page-quick-sidebar-chat-user-form">
                <div class="input-group">
                  <input type="hidden" name="to_id" id="u_id" value="">
                  <input type="hidden" name="to_uname" id="u_name" value="">
                  <input type="hidden" name="to_image" id="u_image" value="">
                  <input type="text" id="msg_box" class="form-control" placeholder="Type a message here..." onKeyPress="return send_message(event)">
                  <div class="input-group-btn">
                    <button type="button" class="btn blue"><i class="icon-paper-clip"></i></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">

function chat_fetch(id,name,image){
	//alert(name);
	document.getElementById("u_id").value=id;
	document.getElementById("u_name").value=name;
	document.getElementById("u_image").value=image;
}

function send_message(e) {
	if (e.keyCode == 13) {
	   //alert("dada");

		var to= document.getElementById("u_id").value;
		var msg= document.getElementById("msg_box").value;

		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			//alert(xhttp.readyState);
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				//alert(xhttp.response);
			}
		};
		xhttp.open("GET", "dashboard/send_message?from=<?php echo $this->session->userdata('user_id'); ?>&to="+to+"&message="+msg+"", true);
		xhttp.send();


	}
}
var QuickSidebar = function () {

        // Handles quick sidebar toggler
        var handleQuickSidebarToggler = function () {
            // quick sidebar toggler
            $('.top-menu .dropdown-quick-sidebar-toggler a, .page-quick-sidebar-toggler').click(function (e) {
                $('body').toggleClass('page-quick-sidebar-open');
            });
        };

        // Handles quick sidebar chats
        var handleQuickSidebarChat = function () {
            var wrapper = $('.page-quick-sidebar-wrapper');
            var wrapperChat = wrapper.find('.page-quick-sidebar-chat');

            var initChatSlimScroll = function () {
                var chatUsers = wrapper.find('.page-quick-sidebar-chat-users');
                var chatUsersHeight;

                chatUsersHeight = wrapper.height() - wrapper.find('.nav-justified > .nav-tabs').outerHeight();
				
				
				

                // chat user list
                //Metronic.destroySlimScroll(chatUsers);
				$('.destroy').click(function(){
					$('.scrollable').slimScroll({
						destroy:true
					});
				});
				//today 
				
				
				
                chatUsers.attr("data-height", chatUsersHeight);
                Metronic.initSlimScroll(chatUsers);

                var chatMessages = wrapperChat.find('.page-quick-sidebar-chat-user-messages');
                var chatMessagesHeight = chatUsersHeight - wrapperChat.find('.page-quick-sidebar-chat-user-form').outerHeight() - wrapperChat.find('.page-quick-sidebar-nav').outerHeight();

                // user chat messages
                //Metronic.destroySlimScroll(chatMessages);
				$('.destroy').click(function(){
					$('.scrollable').slimScroll({
						destroy:true
					});
				})
				
				//Today
				
				
				
                chatMessages.attr("data-height", chatMessagesHeight);
                Metronic.initSlimScroll(chatMessages);
            };

            initChatSlimScroll();
            Metronic.addResizeHandler(initChatSlimScroll); // reinitialize on window resize

            wrapper.find('.page-quick-sidebar-chat-users .media-list > .media').click(function () {
                wrapperChat.addClass("page-quick-sidebar-content-item-shown");
            });

            wrapper.find('.page-quick-sidebar-chat-user .page-quick-sidebar-back-to-list').click(function () {
                wrapperChat.removeClass("page-quick-sidebar-content-item-shown");
            });

            var handleChatMessagePost = function (e) {
                e.preventDefault();

                var chatContainer = wrapperChat.find(".page-quick-sidebar-chat-user-messages");
                var input = wrapperChat.find('.page-quick-sidebar-chat-user-form .form-control');

                var text = input.val();
                if (text.length === 0) {
                    return;
                }

                var preparePost = function(dir, time, name, avatar, message) {
                    var tpl = '';
                    tpl += '<div class="post '+ dir +'">';
                    tpl += '<img class="avatar" alt="" src="' + avatar +'"/>';
                    tpl += '<div class="message">';
                    tpl += '<span class="arrow"></span>';
                    tpl += '<a href="#" class="name">'+name+'</a>&nbsp;';
                    tpl += '<span class="datetime">' + time + '</span>';
                    tpl += '<span class="body">';
                    tpl += message;
                    tpl += '</span>';
                    tpl += '</div>';
                    tpl += '</div>';

                    return tpl;
                };

                // handle post
                var n=document.getElementById("u_name").value;
                var av=document.getElementById("u_image").value;
                var time = new Date();
				//var img = '<?php //echo $image_url?>';
                var message = preparePost('out', (time.getHours() + ':' + time.getMinutes()), 
				'<?php echo $this->session->userdata('user_name') ?>', '', text);
                message = $(message);
				//console.log(img);
                chatContainer.append(message);

                var getLastPostPos = function() {
                    var height = 0;
                    chatContainer.find(".post").each(function() {
                        height = height + $(this).outerHeight();
                    });

                    return height;
                };

                chatContainer.slimScroll({
                    //scrollTo: getLastPostPos()
                });

                input.val("");
                
            };

            wrapperChat.find('.page-quick-sidebar-chat-user-form .btn').click(handleChatMessagePost);
            wrapperChat.find('.page-quick-sidebar-chat-user-form .form-control').keypress(function (e) {
                if (e.which == 13) {
                    handleChatMessagePost(e);
                    return false;
                }
            });
        };

        // Handles quick sidebar tasks
        var handleQuickSidebarAlerts = function () {
            var wrapper = $('.page-quick-sidebar-wrapper');
            var wrapperAlerts = wrapper.find('.page-quick-sidebar-alerts');

            var initAlertsSlimScroll = function () {
                var alertList = wrapper.find('.page-quick-sidebar-alerts-list');
                var alertListHeight;

                alertListHeight = wrapper.height() - wrapper.find('.nav-justified > .nav-tabs').outerHeight();

                // alerts list
                //Metronic.destroySlimScroll(alertList);
				
				$('.destroy').click(function(){
					$('.scrollable').slimScroll({
						destroy:true
					});
				});
								
				//Today
				
                alertList.attr("data-height", alertListHeight);
                Metronic.initSlimScroll(alertList);
            };

            initAlertsSlimScroll();
            Metronic.addResizeHandler(initAlertsSlimScroll); // reinitialize on window resize
        };

        // Handles quick sidebar settings
        var handleQuickSidebarSettings = function () {
            var wrapper = $('.page-quick-sidebar-wrapper');
            var wrapperAlerts = wrapper.find('.page-quick-sidebar-settings');

            var initSettingsSlimScroll = function () {
                var settingsList = wrapper.find('.page-quick-sidebar-settings-list');
                var settingsListHeight;

                settingsListHeight = wrapper.height() - wrapper.find('.nav-justified > .nav-tabs').outerHeight();

                // alerts list
               // Metronic.destroySlimScroll(settingsList);
				$('.destroy').click(function(){
					$('.scrollable').slimScroll({
						destroy:true
					});
				});
				
				//Today
								
                settingsList.attr("data-height", settingsListHeight);
                Metronic.initSlimScroll(settingsList);
            };

            initSettingsSlimScroll();
            Metronic.addResizeHandler(initSettingsSlimScroll); // reinitialize on window resize
        };

        return {

            init: function () {
                //layout handlers
                handleQuickSidebarToggler(); // handles quick sidebar's toggler
                handleQuickSidebarChat(); // handles quick sidebar's chats
                handleQuickSidebarAlerts(); // handles quick sidebar's alerts
                handleQuickSidebarSettings(); // handles quick sidebar's setting
            }
        };

    }; 

function offscrene(){
	document.getElementById('in_cl_logo').style.display= 'block';
	document.getElementById('cl_logo').style.display= 'none';
}

</script>