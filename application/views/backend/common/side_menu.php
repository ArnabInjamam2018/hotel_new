<?php //$this->output->cache(20);
$array = explode(',',$this->session->userdata('user_permission'));
			 //$found = in_array("1",$array);
		//if($found)
?>

<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu page-header-fixed page-sidebar-menu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <li class="nav-item <?php if(isset($active) && $active=='dashboard') echo 'active';?>">
                <a href="<?php echo base_url();?>dashboard">
                    <i class="fa fa-dashboard"></i> <span class="title">Dashboard </span>
                </a>
            </li>
          	<?php if($this->session->userdata('user_type_slug')=="SUPA" || $this->session->userdata('user_type_slug')=="AD"):  ?>
          	<?php if($this->session->userdata('admin') =="1"):  ?>
          	<li class="nav-item <?php if(isset($heading) && $heading=='Admin') echo'active open' ;?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-user-o"></i> 
                    <span class="title">Admin</span>                
                    <span class="arrow <?php if(isset($active) && ($active=='add_admin' || $active=='add_admin_user' || $active=='all_admin_user' || $active=='add_admin_roles' || $active=='add_master_account' || $active=='account_settings' || $active=='broker_payments')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">
					<?php if($this->session->userdata('user_type_slug')=="SUPA") :  ?><?php else:  ?>
                  
                    <li class="nav-item <?php if(isset($active) && $active=='all_admin_user') echo 'active open';?>"> 
                    	<a href="<?php echo base_url();?>setting/all_admin_user" class="nav-link"> 
                        	<i class="fa fa-group"></i> 
                        	<span class="title">All Admin Users</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='all_admin_roles') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_admin_roles" class="nav-link"> 
                            <i class="fa fa-key"></i> 
                            <span class="title">All Admin Roles</span>
                        </a> 
                    </li>
					
                    
                    <?php endif; ?>
                </ul>
          	</li>
          	<?php endif; ?>
          	<?php endif; ?>
			<?php $found = in_array("10",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Room')echo'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-building" aria-hidden="true"></i>
                    <span class="title">Inventory</span>            
                    <span class="arrow <?php if(isset($active) && ($active=='add_room' || $active=='quick_room_add' || $active=='all_rooms' || $active=='unit_type' || $active=='all_unit_class' || $active=='all_unit_type_category' || $active=='all_unit_block' || $active=='add_kit' || $active=='hotel_section' || $active=='hotel_section')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">                    
                    <li class="nav-item <?php if(isset($active) && $active=='quick_room_add') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>unit_class_controller/quick_room_add" class="nav-link"> 
                            <i class="fa fa-bolt" aria-hidden="true"></i> 
                            <span class="title">Quick Room Add</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='all_rooms') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_rooms" class="nav-link"> 
                            <i class="fa fa-bed" aria-hidden="true"></i> 
                            <span class="title">Units</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='unit_type') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/unit_type" class="nav-link"> 
                            <i class="fa fa-flag" aria-hidden="true"></i> 
                            <span class="title">Unit Type</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='all_unit_class') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>unit_class_controller/all_unit_class" class="nav-link"> 
                            <i class="fa fa-flag" aria-hidden="true"></i> 
                            <span class="title">Unit class</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='all_unit_type_category') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>unit_class_controller/all_unit_type_category" class="nav-link"> 
                            <i class="fa fa-flag" aria-hidden="true"></i> 
                            <span class="title">Unit Category</span>
                        </a> 
                    </li>                    
                    <li class="nav-item <?php if(isset($active) && $active=='all_unit_block') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_unit_block"> 
                            <i class="fa fa-th" aria-hidden="true"></i> 
                            <span class="title">Unit Blocks</span>
                        </a> 
                    </li>                    
                    <li class="nav-item <?php if(isset($active) && $active=='kit_card') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/kit_card" class="nav-link"> 
                            <i class="fa fa-shopping-basket" aria-hidden="true"></i> 
                            <span class="title">kits</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='hotel_section') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>unit_class_controller/hotel_section" class="nav-link"> 
                            <i class="fa fa-puzzle-piece"></i>
                            <span class="title">Housekeeping Section</span>
                        </a> 
                    </li>
                </ul>
            </li>
            <?php endif; ?>
			<?php $found = in_array("25",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Booking')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-braille" aria-hidden="true"></i> 
                    <span class="title">Bookings</span>
                    <span class="arrow <?php if(isset($active) && ($active=='add_booking_calendar' || $active=='add_group_booking' || $active=='all_group_booking' || $active=='all_bookings' ||  $active=='pending_payments' ||  $active=='pending_checkin_checkout')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php if(isset($active) && $active=='add_booking_calendar') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/add_booking_calendar" class="nav-link"> 
                            <i class="fa fa-table" aria-hidden="true"></i>
                            <span class="title">Frontdesk</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='add_group_booking') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/add_group_booking" class="nav-link"> 
                            <i class="fa fa-group" aria-hidden="true"></i> 
                            <span class="title">Add Group Booking</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='all_group_booking') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_group_booking" class="nav-link"> 
                            <i class="fa fa-group" aria-hidden="true"></i> 
                            <span class="title">All Group Booking</span>
                        </a>
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='all_bookings') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_bookings" class="nav-link"> 
                            <i class="fa fa-th-list" aria-hidden="true"></i> 
                            <span class="title">All Bookings</span>
                        </a> 
                    </li>                
                    <li class="nav-item <?php if(isset($active) && $active=='pending_payments') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/pending_payments" class="nav-link"> 
                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
                            <span class="title">Pending Payments</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='pending_checkin_checkout') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/pending_checkin_checkout" class="nav-link"> 
                            <i class="fa fa-hourglass-half" aria-hidden="true"></i>
                            <span class="title">Pending Bookings</span>
                        </a> 
                    </li>
                </ul>
            </li>
            <?php endif; ?>
			<?php $found = in_array("13",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($active) && $active=='enquiry_management' || $active=='enquiry') echo 'active open';?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-globe" aria-hidden="true"></i> 
                    <span class="title">Enquiry & Sales</span> 
                    <span class="arrow <?php if(isset($active) && ($active=='enquiry_management' || $active=='enquiry')) echo 'open';?>"></span>
                </a>
                <ul class="sub-menu">
                   <!-- <li class="nav-item <?php if(isset($active) && $active=='enquiry_management') echo 'active open';?>"> 
                        <a href="<?php //echo base_url();?>unit_class_controller/enquiry_management" class="nav-link"> 
                            <i class="fa fa-plus-square"></i>
                            <span class="title">Enquiry Management</span>
                        </a> 
                    </li>-->
					<li class="nav-item <?php if(isset($active) && $active=='enquiry') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>enquiry" class="nav-link"> 
                            <i class="fa fa-plus-square"></i>
                            <span class="title">Booking Enquiry</span>
                        </a> 
                    </li>
                </ul>
            </li>
            <?php endif; ?>
			<?php $found = in_array("13",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Compliance')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-certificate" aria-hidden="true"></i> 
                    <span class="title">Compliance</span>
                    <span class="arrow <?php if(isset($active) && ($active=='all_compliance')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">                    
                    <li class="nav-item <?php if(isset($active) && $active=='all_compliance') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_compliance" class="nav-link"> 
                            <i class="fa fa-th-list" aria-hidden="true"></i> 
                            <span class="title">Certificates</span>
                        </a> 
                    </li>
                </ul>
            </li>
            <?php endif; ?>        
			<?php $found = in_array("7",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Guests')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-address-book-o" aria-hidden="true"></i> 
                    <span class="title">Guest</span>
                    <span class="arrow <?php if(isset($active) && ($active=='all_guest' || $active=='all_corporate' || $active=='discount_rules')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">                
                    <li class="nav-item <?php if(isset($active) && $active=='all_guest') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_guest" class="nav-link">  
                            <i class="fa fa-street-view" aria-hidden="true"></i> 
                            <span class="title">Guests</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='all_corporate') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_corporate" class="nav-link">
                            <i class="fa fa-industry" aria-hidden="true"></i> 
                            <span class="title">Corporates</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='discount_rules') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>unit_class_controller/discount_rules" class="nav-link">
                            <i class="fa fa-industry" aria-hidden="true"></i> 
                            <span class="title">Discount Rules</span>
                        </a> 
                    </li>
                </ul>
            </li>
            <?php endif; ?>
			<?php $foundb = in_array("19",$array); $foundc = in_array("51",$array);  if($foundb || $foundc):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Broker & Channel')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-black-tie" aria-hidden="true"></i>
                    <span class="title">Travel Agents</span>
                    <span class="arrow <?php if(isset($active) && ($active=='all_broker' || $active=='broker_payments' || $active=='all_channel' || $active=='channel_payments')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">
					<?php if($foundb):  ?>
                    <li class="nav-item <?php if(isset($sub_heading) && $sub_heading=='Broker')echo 'active open';?>"> 
                        <a href="javascript:;" class="nav-link nav-toggle"> 
                        <i class="fa fa-bullhorn" aria-hidden="true"></i> 
                        <span class="title">Broker</span>
                        <span class="arrow <?php if(isset($active) && ($active=='all_broker' || $active=='broker_payments')) echo 'open';?>"></span> 
                        </a>
                        <ul class="sub-menu">                            
                            <li class="nav-item <?php if(isset($active) && $active=='all_broker') echo 'active open';?>"> 
                                <a href="<?php echo base_url();?>dashboard/all_broker" class="nav-link"> 
                                    <i class="fa fa-th-list" aria-hidden="true"></i> 
                                    <span class="title">View Broker</span>
                                </a> 
                            </li>
                            <li class="nav-item <?php if(isset($active) && $active=='broker_payments') echo 'active open';?>"> 
                                <a href="<?php echo base_url();?>dashboard/broker_payments" class="nav-link"> 
                                    <i class="fa fa-th-list" aria-hidden="true"></i> 
                                    <span class="title">Broker Payments</span>
                                </a> 
                            </li>
                        </ul>
                    </li>
                    <?php endif; ?>
					<?php if($foundc):  ?>
                    <li class="nav-item <?php if(isset($sub_heading) && $sub_heading=='Channel')echo 'active open';?>"> 
                        <a href="javascript:;" class="nav-link nav-toggle"> 
                            <i class="fa fa-tripadvisor" aria-hidden="true"></i> 
                            <span class="title">Channel/ OTA</span>
                            <span class="arrow <?php if(isset($active) && ($active=='all_channel' || $active=='channel_payments')) echo 'open';?>"></span> 
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item <?php if(isset($active) && $active=='all_channel') echo 'active open';?>"> 
                                <a href="<?php echo base_url();?>dashboard/all_channel" class="nav-link"> 
                                    <i class="fa fa-th-list" aria-hidden="true"></i> 
                                    <span class="title">View Channel/ OTA</span>
                                </a> 
                            </li>
                            <li class="nav-item <?php if(isset($active) && $active=='channel_payments') echo 'active open';?>">
                                <a href="<?php echo base_url();?>dashboard/channel_payments" class="nav-link"> 
                                    <i class="fa fa-th-list" aria-hidden="true"></i> 
                                    <span class="title">Channel Payments</span>
                                </a> 
                            </li>
                        </ul>
                    </li>
                    <?php endif; ?>
                </ul>
			</li>
          	<?php endif; ?>
			<?php $found = in_array("44",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Events')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-calendar-times-o" aria-hidden="true"></i> 
                    <span class="title">Events</span>
                    <span class="arrow <?php if(isset($active) && ($active=='add_event' || $active=='all_events')) echo 'open';?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php if(isset($active) && $active=='all_events') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_events" class="nav-link"> 
                            <i class="fa fa-calendar-o"></i> 
                            <span class="title">All Events</span>
                        </a> 
                    </li>
                </ul>
            </li>
            <?php endif; ?>        
			<?php $found = in_array("54",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Service')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-thumbs-up"></i> 
                    <span class="title">Extra Services</span>
                    <span class="arrow <?php if(isset($active) && ($active=='all_service' || $active=='all_laundry_service' || $active=='all_laundry_rates')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">
                   
                    <li class="nav-item <?php if(isset($sub_heading) && $sub_heading=='Laundry Services') echo 'active open';?>"> 
                        <a href="javascript:;" class="nav-link nav-toggle"> 
                            <i class="fa fa-shirtsinbulk" aria-hidden="true"></i> 
                            <span class="title">Laundry Services</span>
                            <span class="arrow <?php if(isset($active) && ($active=='all_laundry_service' || $active=='all_laundry_rates')) echo 'open';?>"></span>
                        </a>            
                        <ul class="sub-menu" id="ajaxLoad">
                            <li class="nav-item <?php if(isset($active) && $active=='all_laundry_service') echo 'active open';?>">
                                <a href="<?php echo base_url();?>dashboard/all_laundry_service" class="nav-link"> 
                                    <i class="fa fa-shirtsinbulk" aria-hidden="true"></i>
                                    <span class="title">All Laundry Services</span>
                                </a> 
                            </li>
                            <li class="nav-item <?php if(isset($active) && $active=='all_laundry_rates') echo 'active open';?>"> 
                                <a href="<?php echo base_url();?>unit_class_controller/all_laundry_rates" class="nav-link"> 
                                    <i class="fa fa-shirtsinbulk" aria-hidden="true"></i> 
                                    <span class="title">All Laundry Rates</span>
                                </a> 
                            </li>
                        </ul>
                    </li>
					
					<li class="nav-item <?php if(isset($sub_heading) && $sub_heading=='AdHoc Bills') echo 'active open';?>"> 
                        <a href="javascript:;" class="nav-link nav-toggle"> 
                            <i class="fa fa-shirtsinbulk" aria-hidden="true"></i> 
                            <span class="title">AdHoc Bills</span>
                            <span class="arrow <?php if(isset($active) && ($active=='adhoc_bills' || $active=='adhoc_bills')) echo 'open';?>"></span>
                        </a>            
                        <ul class="sub-menu">
                            <li class="nav-item <?php if(isset($active) && $active=='adhoc_bills') echo 'active open';?>">
                                <a href="<?php echo base_url();?>dashboard/adhoc_bills" class="nav-link"> 
                                    <i class="fa fa-shirtsinbulk" aria-hidden="true"></i>
                                    <span class="title">All AdHoc Bills</span>
                                </a> 
                            </li>
							<li class="nav-item <?php if(isset($active) && $active=='adhoc_bills') echo 'active open';?>">
                                <a href="<?php echo base_url();?>dashboard/add_adhoc_bill" class="nav-link"> 
                                    <i class="fa fa-shirtsinbulk" aria-hidden="true"></i>
                                    <span class="title">Add AdHoc Bill</span>
                                </a> 
                            </li>
                        </ul>
                    </li>
					
					<li class="nav-item <?php if(isset($active) && $active=='all_service') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_service" class="nav-link"> 
                            <i class="fa fa-th-list" aria-hidden="true"></i> 
                            <span class="title">Other Services</span>
                        </a> 
                    </li>
                </ul>
            </li>
            <?php endif; ?>
			<?php $found = in_array("10",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Food Plan') echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-cutlery"></i> 
                    <span class="title">Food Plan</span>
                    <span class="arrow <?php if(isset($active) && ($active=='all_food_plans')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu" id="ajaxLoad">
                    <li class="nav-item <?php if(isset($active) && $active=='all_food_plans') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_food_plans" class="nav-link"> 
                            <i class="fa fa-th-list" aria-hidden="true"></i> 
                            <span class="title">All Food Plans</span>
                        </a> 
                    </li>
                </ul>
            </li>
            <?php endif?>
			<?php $found = in_array("57",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Assets')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-archive" aria-hidden="true"></i> 
                    <span class="title">Assets</span>
                    <span class="arrow <?php if(isset($active) && ($active=='all_assets'|| $active=='asset_type')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php if(isset($active) && $active=='all_assets') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_assets" class="nav-link"> 
                            <i class="fa fa-th-list" aria-hidden="true"></i> 
                            <span class="title">Assets</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='asset_type') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/asset_type" class="nav-link"> 
                            <i class="fa fa-th-list" aria-hidden="true"></i> 
                            <span class="title">Asset Type</span>
                        </a> 
                    </li>
                </ul>
            </li>
            <?php endif ?>
			<?php $found = in_array("60",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Stock Inventory')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-cubes" aria-hidden="true"></i>
                    <span class="title">Stock / Inventory</span>
                    <span class="arrow <?php if(isset($active) && ($active=='all_stock_invent' || $active=='stock_invent_log' || $active=='stock_inventory_category' || $active=='warehouse' || $active=='transfer_asset')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php if(isset($active) && $active=='all_stock_invent') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_stock_invent" class="nav-link"> 
                            <i class="fa fa-cube" aria-hidden="true"></i> 
                            <span class="title">All Stock & Inventory</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='stock_invent_log') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/stock_invent_log" class="nav-link"> 
                            <i class="fa fa-cube" aria-hidden="true"></i> 
                            <span class="title">Stock & Inventory Log</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='stock_inventory_category') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>unit_class_controller/stock_inventory_category" class="nav-link"> 
                            <i class="fa fa-cube" aria-hidden="true"></i> 
                            <span class="title">S/I Category</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='warehouse') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>unit_class_controller/warehouse" class="nav-link"> 
                            <i class="fa fa-cube" aria-hidden="true"></i> 
                            <span class="title">S/I Warehouse</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='transfer_asset') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/transfer_asset" class="nav-link"> 
                            <i class="fa fa-exchange" aria-hidden="true"></i> 
                            <span class="title">Transfer Asset</span> 
                        </a> 
                    </li>
                </ul>
            </li>
            <?php endif ?>
			<?php $found = in_array("48",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Feedback')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                    <span class="title"> Feedback</span>
                    <span class="arrow <?php if(isset($active) && ($active=='add_feedback' || $active=='all_feedback')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">
                    
                    <li class="nav-item <?php if(isset($active) && $active=='all_feedback') echo 'active open';?>"> 
                    <a href="<?php echo base_url();?>dashboard/all_feedback" class="nav-link"> 
                        <i class="fa fa-th-list" aria-hidden="true"></i> 
                        <span class="title">All Feedback</span>
                    </a> 
                    </li>
                </ul>
            </li>
            <?php endif; ?>
			<?php $found = in_array("10",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Housekeeping')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-h-square"></i> 
                    <span class="title">Facility Management</span>
                    <span class="arrow <?php if(isset($active) && ($active=='housekeeping' || $active=='housekeeping_staff' || $active=='housekeeping_log' || $active=='room_housekeeping' || $active=='maid_assignment_list')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php if(isset($active) && $active=='housekeeping') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/housekeeping" class="nav-link"> 
                            <i class="fa fa-trash"></i> 
                            <span class="title">Housekeeping</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='housekeeping_staff') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/housekeeping_staff" class="nav-link"> 
                            <i class="fa fa-male"></i> 
                            <span class="title">Housekeeping Staff</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='housekeeping_log') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/housekeeping_log" class="nav-link"> 
                            <i class="fa fa-book"></i> 
                            <span class="title">Housekeeping Log</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='room_housekeeping') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/room_housekeeping" class="nav-link"> 
                            <i class="fa fa-book"></i> 
                            <span class="title">Room Housekeeping Status</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='maid_assignment_list') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/maid_assignment_list" class="nav-link"> 
                            <i class="fa fa-list" aria-hidden="true"></i> 
                            <span class="title">Maid Assignment List</span>
                        </a> 
                    </li>
                </ul>
            </li>
            <?php endif ?>        
			<?php $found = in_array("10",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='View All Tasks')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-tasks" aria-hidden="true"></i>
                    <span class="title">Tasks</span>
                    <span class="arrow <?php if(isset($active) && ($active=='all_tasks')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">
                	<li class="nav-item <?php if(isset($active) && $active=='all_tasks') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_tasks" class="nav-link"> 
                            <i class="fa fa-th-list" aria-hidden="true"></i> 
                            <span class="title">All Tasks</span> 
                        </a> 
                    </li>
                </ul>
            </li>
            <?php endif; ?>
			<?php $found = in_array("7",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='DG set')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-flash"></i> 
                    <span class="title">DG set & Fuel</span>
                    <span class="arrow <?php if(isset($active) && ($active=='add_dgset' || $active=='all_dguest' || $active=='usage_log' || $active=='stopwatch_usage_log' || $active=='all_usage_log' || $active=='add_sub_fuel_log' || $active=='view_fuel_log' || $active=='add_fuel')) echo 'open';?>"></span>
                </a>
                <ul class="sub-menu">                    
                    <li class="nav-item <?php if(isset($active) && $active=='all_dgset') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_dgset" class="nav-link"> 
                        <i class="fa fa-eye"></i> 
                        <span class="title">View DG set</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='all_usage_log') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_usage_log" class="nav-link"> 
                        <i class="fa fa-plug" aria-hidden="true"></i> 
                        <span class="title">Usage Log</span>
                        </a> 
                    </li>                    
                    <li class="nav-item <?php if(isset($active) && $active=='view_fuel_log') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/view_fuel_log" class="nav-link"> 
                        <i class="fa fa-list-ul" aria-hidden="true"></i> 
                        <span class="title">View Fuel Log</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='add_fuel') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/add_fuel" class="nav-link"> 
                        <i class="fa fa-tint"></i> 
                        <span class="title">Add New Fuel</span>
                        </a> 
                    </li>
                </ul>
            </li>
            <?php endif; ?>
			<?php $found = in_array("76",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Lost Item' || $heading=='Release Item' )echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-paw"></i> 
                    <span class="title">Lost and Found</span>
                    <span class="arrow <?php if(isset($active) && ($active=='all_lost_item' || $active=='all_found_items' || $active=='all_release_item'|| $active=='release_item')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php if(isset($active) && $active=='all_lost_item') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_lost_item" class="nav-link"> 
                        <i class="fa fa-low-vision" aria-hidden="true"></i> 
                        <span class="title">Lost Items</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='all_found_items') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_found_items" class="nav-link"> 
                        <i class="fa fa-binoculars" aria-hidden="true"></i> 
                        <span class="title">Found Items</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='all_release_item') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_release_item" class="nav-link"> 
                        <i class="fa fa-smile-o" aria-hidden="true"></i> 
                        L<span class="title">ost & Found items</span>
                        </a> 
                    </li>					
                </ul>
            </li>
            <?php endif; ?>
			<?php if($this->session->userdata('user_type_slug')=="SUPA" ):  ?> <?php endif; ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Shift')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-history" aria-hidden="true"></i>
                    <span class="title">Shifts</span>
                    <span class="arrow <?php if(isset($active) && ($active=='add_shift' || $active=='all_shifts' || $active=='log_shift')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">
                    
                    <li class="nav-item <?php if(isset($active) && $active=='all_shifts') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_shifts" class="nav-link"> 
                        <i class="fa fa-history" aria-hidden="true"></i>
                        <span class="title">Shifts</span>
                        </a> 
                    </li>
                    <li class="nav-item <?php if(isset($active) && $active=='log_shift') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/log_shift" class="nav-link"> 
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <span class="title">Shift Log</span>
                        </a> 
                    </li>
                </ul>
            </li>
           
			<?php $found = in_array("28",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Finance')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-money" aria-hidden="true"></i>
                    <span class="title">Finance</span>
                    <span class="arrow <?php if(isset($active) && ($active=='all_transactions' || $active=='finance_setup' || $active=='add_purchase' || $active=='all_purchase' || $active=='all_billpayments' || $active=='misc_income' || $active=='all_vendor')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">
					<?php if($this->session->userdata('transaction') =='1'):  ?>                    
                    <li class="nav-item <?php if(isset($active) && $active=='all_transactions') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_transactions" class="nav-link">
                            <i class="fa fa-pencil"></i> 
                            <span class="title">All Transanction</span>
                        </a> 
                    </li>
                    <?php endif; ?>                    
                    <?php if($this->session->userdata('transaction') =='1'):  ?>
                    <!--<li class="nav-item <?php if(isset($sub_heading) && $sub_heading=='Finance Setup')echo 'active open';?>"> 
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-cogs"></i> 
                            <span class="title">Finance Setup</span>                    
                            <span class="arrow <?php if(isset($active) && ($active=='finance_setup')) echo 'open';?>"></span> 
                        </a>
                        <ul class="sub-menu">
                        	<li class="nav-item <?php if(isset($active) && $active=='finance_setup') echo 'active open';?>"> 
                                <a href="<?php echo base_url();?>dashboard/finance_setup" class="nav-link"> 
                                    <i class="fa fa-cog"></i> 
                                    <span class="title">Master Setting</span>
                                </a> 
                            </li>
                        </ul>
                    </li>-->
                    <?php endif; ?>
                    <?php  $found = in_array("82",$array);  if($found){  ?>
                    <li class="nav-item <?php if(isset($active) && $active=='all_purchase') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/all_purchase" class="nav-link"> 
                            <i class="fa fa-th-list" aria-hidden="true"></i> 
                            <span class="title">Purchase</span>
                        </a> 
                    </li>
                    
                    <?php } ?>
                    <?php   $found = in_array("28",$array);  if($found):   ?>
                    <li class="nav-item <?php if(isset($active) && $active=='all_billpayments') echo 'active open';?>">
                        <a href="<?php echo base_url();?>dashboard/all_billpayments"  class="nav-link">
                            <i class="fa fa-money"></i>
                            <span class="title">Payments</span>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if($this->session->userdata('channel') =='1'):  ?>
                    <li class="nav-item <?php if(isset($active) && $active=='misc_income') echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>dashboard/misc_income" class="nav-link"> 
                            <i class="fa fa-money"></i> 
                            <span class="title">Miscellenious Income</span> 
                        </a> 
                    </li>
                    <?php endif; ?>
                    <?php if($this->session->userdata('channel') =='1'):  ?>
                    <li class="nav-item <?php if(isset($active) && $active=='all_vendor')echo 'active open';?>">
                        <a href="<?php echo base_url();?>dashboard/all_vendor" class="nav-link">
                            <i class="fa fa-money"></i> 
                            <span class="title">Vendor</span>
                        </a>
                    </li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>
			<?php  $found = in_array("38",$array);  if($found):  ?>
            <li class="nav-item <?php if(isset($active) && $active=='hotel_reports') echo 'active';?>">
                <a href="<?php echo base_url();?>reports">
                    <i class="fa fa-area-chart"></i> <span class="title">Reports & Charts </span>
                </a>
            </li>
            
            <?php endif; ?>
			<?php if($this->session->userdata('room') =='1'):  ?>
            <li class="nav-item <?php if(isset($active) && $active=='setting') echo 'active';?>">
                <a href="<?php echo base_url();?>setting">
                    <i class="fa fa-wrench" aria-hidden="true"></i> <span class="title">Settings </span>
                </a>
            </li>
           
            <?php endif;?>
			<?php 
					$posSettings = $this->setting_model->getPosSettings();
					if(isset($posSettings) && $posSettings-> yumm_status == 1){
			?>
					<li class="nav-item "> 
                        <a href="<?php echo 'http://'.$posSettings->yumm_link; ?>" class="nav-link" target="_blank">  
                            <i style="color:#FFAE04; font-size:20px" class="fa fa-cutlery"" aria-hidden="true"></i> 
                            <span class="title"><?php echo 'POS'; ?></span>
                        </a> 
                    </li>
			<?php  } ?>
        </ul>
    	<?php if($page=="backend/dashboard/index"){ ?>
        <div class="panel-group accordion" id="accordion3" style="margin-top:60px; padding:0 10px;">
          <div class="panel panel-default" style="background-color:lightgrey">
            <div class="panel-heading">
              <h4 class="panel-title" style="font-size:14px;"> <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" style="text-decoration:none; font-weight:bold;"> Booking Status</a> </h4>
            </div>
            <div id="collapse_3_1" class="panel-collapse collapse">
              <div class="panel-body">
                <ul class="list-unstyled brdr-list">
        
                <?php
                $color=$this->unit_class_model->booking_status_type();
                if(isset($color) && $color){
                        foreach($color as $col){
                            $name=$col->booking_status;
                            $co=$col->bar_color_code;
                        ?>
                  <li class="nav-item"><span class="brdr" style="background:<?php echo $co;?>;"></span><?php echo $name; ?></li>
        
                <?php }} ?>
                </ul>
              </div>
            </div>
          </div>
          <div class="panel panel-default" style="background-color:lightgrey">
            <div class="panel-heading">
              <h4 class="panel-title" style="font-size:14px;"> <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" style="text-decoration:none; font-weight:bold;"> Housekeeping Status </a> </h4>
            </div>
            <div id="collapse_3_2" class="panel-collapse collapse">
              <div class="panel-body">
                <ul class="list-unstyled brdr-list">
                <?php
                $color=$this->unit_class_model->housekeeping_status();
                if(isset($color) && $color){
                        foreach($color as $col){
                            $name=$col->status_name;
                            $co=$col->color_primary;
                        ?>
                  <li class="nav-item"><span class="brdr" style="background:<?php echo $co;?>;"></span><?php echo $name; ?></li>
                 <?php }} ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
    	<?php } ?>
    </div>
</div>
<?php //$this->cache->clean();?>
<script>
  (function() {

    "use strict";

    var toggles = document.querySelectorAll(".c-hamburger");

    for (var i = toggles.length - 1; i >= 0; i--) {
      var toggle = toggles[i];
      toggleHandler(toggle);
    };

    function toggleHandler(toggle) {
      toggle.addEventListener( "click", function(e) {
        e.preventDefault();
        (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
      });
    }

  })();
  
  
  function openNav() {
		document.getElementById("mySidenav").style.width = "250px";
		document.getElementById("main").style.marginLeft = "250px";
	}


</script>
<!-- END SIDEBAR -->
