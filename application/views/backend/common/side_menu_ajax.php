<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
  <div class="page-sidebar navbar-collapse">
    <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
      <li class="sidebar-toggler-wrapper"> 
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="sidebar-toggler"></div>
        <!-- END SIDEBAR TOGGLER BUTTON --> 
      </li>
      <li <?php if(isset($heading) && $heading=='Dashboard'):?> class="start active open" <?php endif;?>> <a href="<?php echo base_url();?>dashboard"> <i class="fa fa-dashboard"></i> <span class="title">Dashboard</span>
        <?php if(isset($active) && $active=='dashboard'):?>
        <span class="selected"></span>
        <?php endif;?>
        </a> </li>
      <?php if($this->session->userdata('user_type_slug')=="SUPA" || $this->session->userdata('user_type_slug')=="AD"):  ?>
      <?php if($this->session->userdata('admin') =="1"):  ?>
      <li <?php if(isset($heading) && $heading=='Admin'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-user"></i> <span class="title">Admin</span>
        <?php if(isset($active) && ($active=='add_admin' || $active=='all_admin')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_admin' || $active=='all_admin')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <?php if($this->session->userdata('user_type_slug')=="SUPA") :  ?>
          <li <?php if(isset($active) && $active=='add_admin') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_admin"> <i class="fa fa-user-plus"></i> Add Admin</a> </li>
          <?php else:  ?>
          <li <?php if(isset($active) && $active=='add_admin') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_admin"> <i class="fa fa-user-plus"></i> Add Subadmin</a> </li>
          <?php endif; ?>
        </ul>
      </li>
      <?php endif; ?>
      <?php endif; ?>
      <?php if($this->session->userdata('room') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='Room'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-object-ungroup"></i> <span class="title">Unit</span>
        <?php if(isset($active) && ($active=='add_room' || $active=='all_rooms' || $active=='unit_type' || $active=='add_unit_block' || $active=='all_unit_block')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_room' || $active=='all_rooms' || $active=='unit_type' || $active=='add_unit_block' || $active=='all_unit_block')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <li <?php if(isset($active) && $active=='add_room') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_room"> <i class="fa fa-plus-square"></i> Add Unit </a> </li>
          <li <?php if(isset($active) && $active=='all_rooms') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_rooms"> <i class="fa fa-puzzle-piece"></i> All Units</a> </li>
          <li <?php if(isset($active) && $active=='unit_type') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/unit_type"> <i class="fa fa-puzzle-piece"></i> Unit Type</a> </li>
		  <li <?php if(isset($active) && $active=='all_unit_class') echo "class='active'";?>> <a href="<?php echo base_url();?>unit_class_controller/all_unit_class"> <i class="fa fa-puzzle-piece"></i> Unit class</a> </li>
		  		  <li <?php if(isset($active) && $active=='all_unit_type_category') echo "class='active'";?>> <a href="<?php echo base_url();?>unit_class_controller/all_unit_type_category"> <i class="fa fa-puzzle-piece"></i> Unit type Category</a> </li>
          <!--<li <?php if(isset($active) && $active=='add_unit_block') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_unit_block"> <i class="glyphicon glyphicon-bed"></i> Add Unit Block</a> </li>-->
          <li <?php if(isset($active) && $active=='all_unit_block') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_unit_block"> <i class="fa fa-puzzle-piece"></i> Unit Blocks</a> </li>
          <li <?php if(isset($active) && $active=='all_unit_block') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_kit"> <i class="fa fa-puzzle-piece"></i> Wellcome Kit</a> </li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('booking') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='Booking'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-bed"></i> <span class="title">Booking</span>
        <?php if(isset($active) && ($active=='add_booking' || $active=='add_booking_calendar' || $active=='all_bookings' )):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_booking' || $active=='add_booking_calendar' || $active=='all_bookings')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
           <li <?php if(isset($active) && $active=='add_booking') echo "class='active'";?>>
                                <a href="<?php echo base_url();?>dashboard/add_booking">
                                    <i class="fa fa-archive"></i>
                                    Add Booking</a>
                            </li> 
          <li <?php if(isset($active) && $active=='add_booking_calendar') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_booking_calendar"> <i class="fa fa-archive"></i> Frontdesk</a> </li>
          <li <?php if(isset($active) && $active=='add_booking_calendar') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_group_booking"> <i class="fa fa-users"></i> Add Group Booking</a> </li>
          <li <?php if(isset($active) && $active=='add_booking_calendar') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_group_booking"> <i class="fa fa-users"></i> All Group Booking</a> </li>
          <li <?php if(isset($active) && $active=='all_bookings') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_bookings"> <i class="fa fa-flag"></i> All Bookings</a> </li>
          <li <?php if(isset($active) && $active=='booking_edit') echo "class='active'";?>>
                                <a href="<?php echo base_url();?>dashboard/booking_edit">
                                    <i class="fa fa-file-archive-o"></i>
                                    Booking Edit</a>
                            </li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('compliance') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='Compliance'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-check-square-o"></i> <span class="title">Compliance System</span>
        <?php if(isset($active) && ($active=='add_compliance' || $active=='all_compliance')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_compliance' || $active=='all_compliance')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <li <?php if(isset($active) && $active=='add_compliance') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_compliance"> <i class="fa fa-plus-square"></i> Add Certificate</a> </li>
          <li <?php if(isset($active) && $active=='all_compliance') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_compliance"> <i class="fa fa-file-archive-o"></i> Show all Certificates</a> </li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('hotel_m') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='Hotel Management'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-university"></i> <span class="title">Hotel Management</span>
        <?php if(isset($active) && ($active=='add_hotel_m' || $active=='all_hotel_m')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_hotel_m' || $active=='all_hotel_m')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <?php if($this->session->userdata('user_type_slug')=="SUPA"):  ?>
          <li <?php if(isset($active) && $active=='add_hotel_m') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_hotel_m"> <i class="fa fa-plus-square"></i> Add Hotels</a> </li>
          <?php endif; ?>
          <li <?php if(isset($active) && $active=='all_hotel_m') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_hotel_m"> <i class="fa fa-eye"></i> View Hotels</a> </li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('guest') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='Guests'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-user-plus"></i> <span class="title">Guest</span>
        <?php if(isset($active) && ($active=='add_guest' || $active=='all_guest' || $active=='all_corporate')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_guest' || $active=='all_guest' || $active=='all_corporate')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <li <?php if(isset($active) && $active=='add_guest') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_guest"> <i class="fa fa-plus-square"></i> Add Guest</a> </li>
          <li <?php if(isset($active) && $active=='all_guest') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_guest"> <i class="fa fa-eye"></i> View Guests</a> </li>
          <li <?php if(isset($active) && $active=='all_corporate') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_corporate"> <i class="fa fa-user-secret"></i> Corporate</a> </li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('broker') =='1' || $this->session->userdata('channel') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='Broker & Channel'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-cubes"></i> <span class="title">Broker & Channel</span>
        <?php if(isset($active) && ($active=='add_broker' || $active=='all_broker' || $active=='broker_payments' || $active=='add_channel' || $active=='all_channel' || $active=='channel_payments')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow "></span> </a>
        <ul class="sub-menu">
          <?php if($this->session->userdata('broker') =='1'):  ?>
          <li <?php if(isset($sub_heading) && $sub_heading=='Broker'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-user-plus"></i> <span class="title">Broker</span>
            <?php if(isset($active) && ($active=='add_broker' || $active=='all_broker' || $active=='broker_payments')):?>
            <span class="selected"></span>
            <?php endif;?>
            <span class="arrow <?php if(isset($active) && ($active=='add_broker' || $active=='all_broker' || $active=='broker_payments')) echo 'open';?>"></span> </a>
            <ul class="sub-menu">
              <li <?php if(isset($active) && $active=='add_broker') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_broker"> <i class="fa fa-pencil"></i> Add Broker</a> </li>
              <li <?php if(isset($active) && $active=='all_broker') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_broker"> <i class="fa fa-file-archive-o"></i> View Broker</a> </li>
              <li <?php if(isset($active) && $active=='broker_payments') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/broker_payments"> <i class="fa fa-file-archive-o"></i> Broker Payments</a> </li>
            </ul>
          </li>
          <?php endif; ?>
          <?php if($this->session->userdata('channel') =='1'):  ?>
          <li <?php if(isset($sub_heading) && $sub_heading=='Channel'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-user-plus"></i> <span class="title">Channel</span>
            <?php if(isset($active) && ($active=='add_channel' || $active=='all_channel' || $active=='channel_payments')):?>
            <span class="selected"></span>
            <?php endif;?>
            <span class="arrow <?php if(isset($active) && ($active=='add_channel' || $active=='all_channel' || $active=='channel_payments')) echo 'open';?>"></span> </a>
            <ul class="sub-menu">
              <li <?php if(isset($active) && $active=='add_channel') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_channel"> <i class="fa fa-pencil"></i> Add Channel</a> </li>
              <li <?php if(isset($active) && $active=='all_channel') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_channel"> <i class="fa fa-file-archive-o"></i> View Channel</a> </li>
              <li <?php if(isset($active) && $active=='channel_payments') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/channel_payments"> <i class="fa fa-file-archive-o"></i> Channel Payments</a> </li>
            </ul>
          </li>
          <?php endif; ?>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('event') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='Events'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-anchor"></i> <span class="title">Events</span>
        <?php if(isset($active) && ($active=='add_event' || $active=='all_events')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_event' || $active=='all_events')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <li <?php if(isset($active) && $active=='add_event') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_event"> <i class="fa fa-calendar-plus-o"></i> Add an Event</a> </li>
          <li <?php if(isset($active) && $active=='all_events') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_events"> <i class="fa fa-calendar-o"></i> All Events</a> </li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('service') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='Service'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-thumbs-up"></i> <span class="title">Service</span>
        <?php if(isset($active) && ($active=='add_service' || $active=='all_service')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_service' || $active=='all_service')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <li <?php if(isset($active) && $active=='add_service') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_service"> <i class="fa fa-plus-square"></i> Add Service</a> </li>
          <li <?php if(isset($active) && $active=='all_service') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_service"> <i class="fa fa-file-archive-o"></i> All Services</a> </li>
        </ul>
      </li>
      <?php endif; ?>
      <li <?php if(isset($heading) && $heading=='Food Plan'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> 
      <i class="fa fa-cutlery"></i> <span class="title">Food Plan</span>
        <?php if(isset($active) && ($active=='add_food_plan' || $active=='all_food_plans')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_food_plan' || $active=='all_food_plans')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <li <?php if(isset($active) && $active=='add_food_plan') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_food_plan"> <i class="fa fa-plus-square"></i> Add Food Plan</a> </li>
          <li <?php if(isset($active) && $active=='all_food_plans') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_food_plans"> <i class="fa fa-file-archive-o"></i> All Food Plans</a> </li>
        </ul>
      </li>
      <li <?php if(isset($heading) && $heading=='Assets'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-briefcase"></i> <span class="title">Assets</span>
        <?php if(isset($active) && ($active=='add_asset' || $active=='all_assets' || $active=='asset_type')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_asset' || $active=='all_assets'|| $active=='asset_type')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <li <?php if(isset($active) && $active=='add_asset') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_asset"> <i class="fa fa-plus-square"></i> Add Asset</a> </li>
          <li <?php if(isset($active) && $active=='all_assets') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_assets"> <i class="fa fa-file-archive-o"></i> All Assets</a> </li>
          <li <?php if(isset($active) && $active=='asset_type') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/asset_type"> <i class="fa fa-file-archive-o"></i> Asset Type</a> </li>
          <li  <?php if(isset($active) && $active=='asset_type') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/asset_type"> <i class="fa fa-file-archive-o"></i> Stock/ Inventory</a>
            <ul class="sub-menu">
              <li <?php if(isset($active) && $active=='asset_type') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_stock_invent"><i class="fa fa-pencil"></i>All Stock & Inventory </a> </li>
              <li <?php if(isset($active) && $active=='asset_type') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/stock_invent_log"><i class="fa fa-pencil"></i>Stock & Inventory Log</a> </li>
            </ul>
          </li>
        </ul>
      </li>
      <?php if($this->session->userdata('feedback') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='Feedback'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-pencil"></i> <span class="title">Feedback</span>
        <?php if(isset($active) && ($active=='add_feedback' || $active=='all_feedback')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_feedback' || $active=='all_feedback')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <li <?php if(isset($active) && $active=='add_feedback') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_feedback"> <i class="fa fa-plus-square"></i> Add Feedback</a> </li>
          <li <?php if(isset($active) && $active=='all_feedback') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_feedback"> <i class="fa fa-file-archive-o"></i> All Feedback</a> </li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('user_type_slug') =='G'):  ?>
      <li <?php if(isset($heading) && $heading=='Feedback'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-building-o"></i> <span class="title">Housekeeeping</span>
        <?php if(isset($active) && ($active=='add_feedback' || $active=='all_feedback')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_feedback' || $active=='all_feedback')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <li <?php if(isset($active) && $active=='add_feedback') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_housekeeping"> <i class="fa fa-file-archive-o"></i> Add Housekeeping</a> </li>
          <li <?php if(isset($active) && $active=='all_feedback') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_housekeepings"> <i class="fa fa-file-archive-o"></i> All Housekeepings</a> </li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('feedback') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='Housekeeping'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-h-square"></i> <span class="title">Housekeeping</span>
        <?php if(isset($active) && ($active=='housekeeping' || $active=='assingment')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='housekeeping' || $active=='assingment')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <li <?php if(isset($active) && $active=='housekeeping') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/housekeeping"> <i class="fa fa-trash"></i> Housekeeping</a> </li>
          <li <?php if(isset($active) && $active=='assingment') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/assingment"> <i class="fa fa-list"></i> Assingment</a> </li>
          <li <?php if(isset($active) && $active=='assingment') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/housekeeping_staff"> <i class="fa fa-male"></i> Housekeeping Staff</a> </li>
          <li <?php if(isset($active) && $active=='assingment') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/housekeeping_log"> <i class="fa fa-book"></i> Housekeeping Log</a> </li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('feedback') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='View All Tasks'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-calendar"></i> <span class="title">Tasks</span>
        <?php if(isset($active) && ($active=='all_tasks')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='all_tasks')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <li <?php if(isset($active) && $active=='all_tasks') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_tasks"> <i class="fa fa-file-archive-o"></i> All Tasks </a> </li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('guest') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='DG set'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="glyphicon glyphicon-flash"></i> <span class="title">DG set & Fuel</span>
        <?php if(isset($active) && ($active=='add_dgset' || $active=='all_guest' || $active=='usage_log' || $active=='stopwatch_usage_log' || $active=='all_usage_log' || $active=='add_sub_fuel_log')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_dgset' || $active=='all_dguest' || $active=='usage_log' || $active=='stopwatch_usage_log' || $active=='all_usage_log' || $active=='add_sub_fuel_log')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <li <?php if(isset($active) && $active=='add_dgset') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_dgset"> <i class="fa fa-plus-square"></i> Add DG set</a> </li>
          <li <?php if(isset($active) && $active=='all_dgset') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_dgset"> <i class="glyphicon glyphicon-eye-open"></i> View DG set</a> </li>
          <li <?php if(isset($active) && $active=='usage_log') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/usage_log"> <i class="glyphicon glyphicon-pencil"></i> Log Usage</a> </li>
          <li <?php if(isset($active) && $active=='all_usage_log') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_usage_log"> <i class="glyphicon glyphicon-eye-open"></i> View Usage Log</a> </li>
          <li <?php if(isset($active) && $active=='add_sub_fuel_log') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_sub_fuel_log"> <i class="glyphicon glyphicon-tint"></i> Add/Substract Fuel</a> </li>
          <li <?php if(isset($active) && $active=='view_fuel_log') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/view_fuel_log"> <i class="glyphicon glyphicon-eye-open"></i> View Fuel Log</a> </li>
          <li <?php if(isset($active) && $active=='add_fuel') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_fuel"> <i class="glyphicon glyphicon-pencil"></i> Add Fuel</a> </li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('guest') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='Lost Item'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-paw"></i> <span class="title">Lost and Found</span>
        <?php if(isset($active) && ($active=='all_lost_item' || $active=='all_found_items' || $active=='release_item')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='all_lost_item' || $active=='all_found_items' || $active=='release_item')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
            <li <?php if(isset($active) && $active=='add_lost_item') echo "class='active'";?>>
                                <a href="<?php echo base_url();?>dashboard/add_lost_item">
                                    <i class="fa fa-pencil"></i>
                                    Add Lost Item</a>
                            </li> 
          
          <li <?php if(isset($active) && $active=='all_lost_item') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_lost_item"> <i class="fa fa-pencil"></i> Lost Items</a> </li>
          <li <?php if(isset($active) && $active=='all_found_items') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_found_items"> <i class="fa fa-file-archive-o"></i> Found Items</a> </li>
          <li <?php if(isset($active) && $active=='release_item') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_release_item"> <i class="fa fa-file-archive-o"></i> Lost & Found item</a> </li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('user_type_slug')=="SUPA" ):  ?>
      <li <?php if(isset($heading) && $heading=='Admin'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="icon-user"></i> <span class="title">Shift</span>
        <?php if(isset($active) && ($active=='add_admin' || $active=='all_admin')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='add_admin' || $active=='all_admin')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <li <?php if(isset($active) && $active=='add_admin') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_shift"> <i class="icon-user-follow"></i> Add Shfit</a> </li>
          <li <?php if(isset($active) && $active=='add_admin') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_shifts"> <i class="icon-user-follow"></i> All Shifts</a> </li>
          <li <?php if(isset($active) && $active=='add_admin') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/log_shift"> <i class="icon-user-follow"></i> Shift Log</a> </li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if($this->session->userdata('transaction') =='1' || $this->session->userdata('channel') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='Finance'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-inr"></i> <span class="title">Finance</span>
        <?php if(isset($active) && ($active=='all_transactions' || $active=='add_purchase' || $active=='all_purchase' || $active=='all_salpayments' || $active=='misc_income' || $active=='income_report' || $active=='all_expense_report' || $active=='channel_payments' || $active=='channel_payments' || $active=='channel_payments')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow <?php if(isset($active) && ($active=='all_transactions' || $active=='add_purchase' || $active=='all_purchase' || $active=='all_salpayments' || $active=='misc_income' || $active=='income_report' || $active=='all_expense_report' || $active=='channel_payments' || $active=='channel_payments' || $active=='channel_payments')) echo 'open';?>"></span> </a>
        <ul class="sub-menu">
          <?php if($this->session->userdata('transaction') =='1'):  ?>
          <li <?php if(isset($sub_heading) && $sub_heading=='Transactions'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-money"></i> <span class="title">Transanctions</span>
            <?php if(isset($active) && ($active=='all_transactions')):?>
            <span class="selected"></span>
            <?php endif;?>
            <span class="arrow <?php if(isset($active) && ($active=='all_transactions')) echo 'open';?>"></span> </a>
            <ul class="sub-menu">
              <li <?php if(isset($active) && $active=='all_transactions') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_transactions"><i class="fa fa-pencil"></i> All Transanction</a> </li>
            </ul>
          </li>
          <?php endif; ?>
          <?php if($this->session->userdata('transaction') =='1'):  ?>
          <li  <?php if(isset($active) && $active=='misc_income') echo "class='active'";?>> <a href="javascript:;"><i class="fa fa-cogs"></i> <span class="title">Finance Setup</span>
            <?php if(isset($active) && ($active=='all_transactions')):?>
            <span class="selected"></span>
            <?php endif;?>
            <span class="arrow <?php if(isset($active) && ($active=='all_transactions')) echo 'open';?>"></span> </a>
            <ul class="sub-menu">
              <li <?php if(isset($active) && $active=='finance_setup') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/finance_setup"> <i class="fa fa-cog"></i> Master Setting</a> </li>
              <li <?php if(isset($active) && $active=='broker_payments') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/tax_setting"> <i class="fa fa-cog"></i> Tax Settings</a> </li>
              <li <?php if(isset($active) && $active=='broker_payments') echo "class='active'";?>> <a href="#"> <i class="fa fa-cog"></i> Business Hierarchy</a> </li>
            </ul>
          </li>
          <?php endif; ?>
          <?php if($this->session->userdata('broker') =='1'):  ?>
          <li <?php if(isset($sub_heading) && $sub_heading=='Purchase'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-money"></i> <span class="title">Purchase</span>
            <?php if(isset($active) && ($active=='add_purchase' || $active=='all_purchase')):?>
            <span class="selected"></span>
            <?php endif;?>
            <span class="arrow <?php if(isset($active) && ($active=='add_purchase' || $active=='all_purchase')) echo 'open';?>"></span> </a>
            <ul class="sub-menu">
              <li <?php if(isset($active) && $active=='add_purchase') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_purchase"> <i class="fa fa-pencil"></i> Add Purchase</a> </li>
              <li <?php if(isset($active) && $active=='all_purchase') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_purchase"> <i class="fa fa-file-archive-o"></i> View Purchase</a> </li>
            	<li <?php if(isset($active) && $active=='channel_payments') echo "class='active'";?>><a href="<?php echo base_url();?>dashboard/channel_payments"><i class="fa fa-file-archive-o"></i>Channel Payments</a>
                </li>
             </ul>
          </li>
          <?php endif; ?>
          <?php if($this->session->userdata('channel') =='1'):  ?>
          <li <?php if(isset($active) && $active=='all_salpayments') echo "class='active'";?>><a href="<?php echo base_url();?>dashboard/all_salpayments"><i class="fa fa-money"></i>Payments</a></li>
          <?php endif; ?>
          <?php if($this->session->userdata('channel') =='1'):  ?>
          <li  <?php if(isset($active) && $active=='misc_income') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/misc_income"> <i class="fa fa-money"></i> <span class="title">Miscellenious Income</span> </a> </li>
          <?php endif; ?>
        </ul>
      </li>
      <?php endif; ?>

      <?php if($this->session->userdata('room') =='1' || $this->session->userdata('channel') =='1'):  ?>
      <li <?php if(isset($heading) && $heading=='Hotel Reports'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-area-chart"></i> <span class="title">Hotel Reports</span>
        <?php if(isset($active) && ($active=='Hotel Reports' || $active=='Hotel Reports' || $active=='broker_payments' || $active=='Hotel Reports' || $active=='Hotel Reports' || $active=='Hotel Reports')):?>
        <span class="selected"></span>
        <?php endif;?>
        <span class="arrow "></span> </a>
        <ul class="sub-menu">
          <?php if($this->session->userdata('room') =='1'):  ?>
          <li <?php if(isset($sub_heading) && $sub_heading=='room'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-rupee"></i> <span class="title">Financial Reports</span>
            <?php if(isset($active) && ($active=='Hotel Reports' || $active=='Hotel Reports' || $active=='Hotel Reports')):?>
            <span class="selected"></span>
            <?php endif;?>
            <span class="arrow <?php if(isset($active) && ($active=='Hotel Reports' || $active=='Hotel Reports' || $active=='Hotel Reports')) echo 'open';?>"></span> </a>
            <ul class="sub-menu">
              <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/monthly_summary_report"> <i class="glyphicon glyphicon-list-alt"></i> Monthly Summary Reports</a> </li>
              <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/revpar_room_report"> <i class="glyphicon glyphicon-bitcoin"></i> RevPar Room Report</a> </li>
              <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/financial_report"> <i class="glyphicon glyphicon-list-alt"></i> Financial Report</a> </li>
              <li <?php if(isset($active) && $active=='all_f_reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_f_reports"> <i class="fa fa-file-archive-o"></i> Daily Financial Report</a> </li>
              <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/deposit_report"> <i class="glyphicon glyphicon-inbox"></i> Deposit Report</a> </li>
              <li <?php if(isset($active) && $active=='income_report') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/income_report"> <i class="glyphicon glyphicon-briefcase"></i> Income Report</a> </li>
              <li <?php if(isset($active) && $active=='channel_payments') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/channel_payments"> <i class="fa glyphicon glyphicon-usd"></i> Cash Flow Report</a> </li>
              <li <?php if(isset($active) && $active=='channel_payments') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/channel_payments"> <i class="glyphicon glyphicon-piggy-bank"></i> Monthly Profit & Loss Report</a> </li>
              <li <?php if(isset($active) && $active=='channel_payments') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/channel_payments"> <i class="fa fa-file-archive-o"></i> Tax Collection Report</a> </li>
            </ul>
          </li>
          <?php endif; ?>
          <?php if($this->session->userdata('room') =='1'):  ?>
          <li <?php if(isset($sub_heading) && $sub_heading=='Hotel Reports'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-wpforms"></i> <span class="title">Reservation Reports</span>
            <?php if(isset($active) && ($active=='Hotel Reports' || $active=='Hotel Reports' || $active=='Hotel Reports')):?>
            <span class="selected"></span>
            <?php endif;?>
            <span class="arrow <?php if(isset($active) && ($active=='Hotel Reports' || $active=='Hotel Reports' || $active=='Hotel Reports')) echo 'open';?>"></span> </a>
            <ul class="sub-menu">
              <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/add_channel"> <i class="fa fa-pencil"></i> No Show Report</a> </li>
              <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_channel"> <i class="fa fa-file-archive-o"></i> Group Stay Report</a> </li>
              <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/channel_payments"> <i class="fa fa-file-archive-o"></i> Channel Payments</a> </li>
              <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/reservation_summary"> <i class="fa fa-file-archive-o"></i>Reservation Summary</a> </li>
              <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/group_summary"> <i class="fa fa-file-archive-o"></i>Group Stay Summary</a> </li>
              <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/reservation_report"> <i class="fa fa-file-archive-o"></i>Reservation(ADR) Report</a> </li>
              <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/cancelled_reservation"> <i class="fa fa-file-archive-o"></i>Cancelled Reservations </a> </li>
              <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/note_preference_report"> <i class="fa fa-file-archive-o"></i>Note & Preference Report </a> </li>
            </ul>
          </li>
		  
		   <?php if($this->session->userdata('room') =='1'):  ?>
          <li <?php if(isset($sub_heading) && $sub_heading=='Hotel Reports'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-wpforms"></i> <span class="title">Tax Reports</span>
            <?php if(isset($active) && ($active=='Hotel Reports' || $active=='Hotel Reports' || $active=='Hotel Reports')):?>
            <span class="selected"></span>
            <?php endif;?>
            <span class="arrow <?php if(isset($active) && ($active=='Hotel Reports' || $active=='Hotel Reports' || $active=='Hotel Reports')) echo 'open';?>"></span> </a>
            <ul class="sub-menu">
              <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/tax_report"> <i class="fa fa-pencil"></i>Tax Report</a> </li>
             <li <?php if(isset($active) && $active=='Hotel Reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/lodging_tax_report"> <i class="fa fa-pencil"></i>Lodging Tax Report</a> </li>
            </ul>
          </li>
		  <?php endif;?>
		  
		  
          <li <?php if(isset($sub_heading) && $sub_heading=='Hotel Reports'):?> class="start active open" <?php endif;?>> <a href="javascript:;"> <i class="fa fa-wpforms"></i> <span class="title">Guest Report</span>
            <?php if(isset($active) && ($active=='Hotel Reports' || $active=='Hotel Reports' || $active=='Hotel Reports')):?>
            <span class="selected"></span>
            <?php endif;?>
            <span class="arrow <?php if(isset($active) && ($active=='add_channel' || $active=='all_channel' || $active=='channel_payments')) echo 'open';?>"></span> </a>
            <ul class="sub-menu">
              <li <?php if(isset($active) && $active=='all_reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/all_reports"> <i class="fa fa-file-archive-o"></i> Customer Report</a></li>
              <li <?php if(isset($active) && $active=='cust_police_reports') echo "class='active'";?>> <a href="<?php echo base_url();?>dashboard/cust_police_reports"> <i class="fa fa-file-archive-o"></i> Customer Police Report</a> </li>
            </ul>
          </li>
          <?php endif; ?>
        </ul>
      </li>
      <?php endif; ?>
      <li <?php if(isset($heading) && $heading=='Dashboard'):?> class="start active open" <?php endif;?>> <a href="javascript:void(0)" onclick="jTest()"> <i class="fa fa-dashboard"></i> <span class="title">test</span>
        <?php if(isset($active) && $active=='dashboard'):?>
        <span class="selected"></span>
        <?php endif;?>
        </a> </li>
    </ul>
    <?php if($page=="backend/dashboard/index"){ ?>
    <div class="panel-group accordion" id="accordion3" style="margin-top:60px; padding:0 10px;">
      <div class="panel panel-default" style="background-color:lightgrey">
        <div class="panel-heading">
          <h4 class="panel-title" style="font-size:14px;"> <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" style="text-decoration:none; font-weight:bold;"> Booking Status</a> </h4>
        </div>
        <div id="collapse_3_1" class="panel-collapse collapse">
          <div class="panel-body">
            <ul class="list-unstyled brdr-list">
              <li><span class="brdr" style="background:#75509B;"></span>Temp Hold</li>
              <li><span class="brdr" style="background:#C04A67;"></span>Advance</li>
              <li><span class="brdr" style="background:#0FD906;"></span>Confirmed</li>
              <li><span class="brdr" style="background:#39B9A1;"></span>Checked-in</li>
              <li><span class="brdr" style="background:#297cac;"></span>Checked-out</li>
              <li><span class="brdr" style="background:#f52929;"></span>Pending</li>
              <li><span class="brdr" style="background:#FFA500;"></span>Due</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel panel-default" style="background-color:lightgrey">
        <div class="panel-heading">
          <h4 class="panel-title" style="font-size:14px;"> <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" style="text-decoration:none; font-weight:bold;"> Housekeeping Status </a> </h4>
        </div>
        <div id="collapse_3_2" class="panel-collapse collapse">
          <div class="panel-body">
            <ul class="list-unstyled brdr-list">
              <li>
                <div class="brdr" style="background:#33CFE0;"></div>
                Inspected</li>
              <li>
                <div class="brdr" style="background:#1AAE9A;"></div>
                Clean</li>
              <li>
                <div class="brdr" style="background:#FEBF15;"></div>
                Dirty</li>
              <li>
                <div class="brdr" style="background:#323E4F;"></div>
                Maintaince</li>
              <li>
                <div class="brdr" style="background:#5E738B;"></div>
                Repair</li>
              <li>
                <div class="brdr" style="background:#E74443;"></div>
                Undefine</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
</div>
<script>
  (function() {

    "use strict";

    var toggles = document.querySelectorAll(".c-hamburger");

    for (var i = toggles.length - 1; i >= 0; i--) {
      var toggle = toggles[i];
      toggleHandler(toggle);
    };

    function toggleHandler(toggle) {
      toggle.addEventListener( "click", function(e) {
        e.preventDefault();
        (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
      });
    }

  })();
</script> 
<!-- END SIDEBAR --> 
