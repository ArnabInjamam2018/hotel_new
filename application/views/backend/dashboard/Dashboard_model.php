<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



 class Dashboard_model extends CI_Model {
    /* Add Hotel */
    function add_hotel($hotel,$hotel_contact_details,$hotel_tax_details,$hotel_billing_settings,$hotel_general_preference){
		$query=$this->db->insert(TABLE_PRE.'hotel_details',$hotel);
		$hotel_id = $this->db->insert_id();
		$data = array(
			'hotel_id' => $hotel_id
		);\
\

		$new_hotel_contact_details = array_merge($hotel_contact_details, $data);
		$new_hotel_tax_details = array_merge($hotel_tax_details , $data);
		$new_hotel_billing_settings = array_merge($hotel_billing_settings,$data);
		$new_hotel_general_preference = array_merge($hotel_general_preference,$data);
		$query_contact = $this->db->insert(TABLE_PRE.'hotel_contact_details',$new_hotel_contact_details);
		$query_tax = $this->db->insert(TABLE_PRE.'hotel_tax_details',$new_hotel_tax_details);
		$query_billing = $this->db->insert(TABLE_PRE.'hotel_billing_setting',$new_hotel_billing_settings);
		$query_general = $this->db->insert(TABLE_PRE.'hotel_general_preference',$new_hotel_general_preference);

        if($query){
				if($query_contact){
					if($query_tax){
						if($query_billing){
							if($query_general){
								return true;
							}else{
								return false;
							}
						}
						else{
							return false;
						}
					}
					else{
						return false;
					}
				}
				else{
					return false;
				}
				
        }else{
            return false;
        }
	}
	
	/* Ajax Hotel Email Check */
	function hotel_email_check($email){
		$query=$this->db->get_where(TABLE_PRE.'hotel_details',$email);
		
        if($query->num_rows()==1){
			
			$a = $this->db->insert_id();
			echo $a;
			exit();
            return $query->row();
        }else{ 
            return false;
        }
		
	}

    // Task Assigner Return

    function task_assiger(){
        $this->db->select('*');
        //$this->db->group_by(array("admin_first_name", "admin_middle_name","admin_last_name"));
        $this->db->from('hotel_admin_details');
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    function task_add($task_entry){
        $query=$this->db->insert(TABLE_PRE.'tasks',$task_entry);
        if($query){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function tasks_pending(){
        $this->db->select('*');
        $this->db->where('status=','0');
        $this->db->order_by('priority','asc');
        $this->db->from(TABLE_PRE.'tasks');
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        else
        {
            return false;
        }
    }

     function tasks_pending_unique(){
         $this->db->select('*');
         $this->db->where('status=','0');
         $this->db->where('to_id=',$this->session->userdata('user_id'));
         $this->db->order_by('priority','asc');
         $this->db->from(TABLE_PRE.'tasks');
         $query=$this->db->get();
         if($query->num_rows()>0){
             return $query->result();
         }
         else
         {
             return false;
         }
     }

    function tasks_all(){
        $this->db->select('*');
        $this->db->order_by('priority','asc');
        $this->db->from(TABLE_PRE.'tasks');
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    function get_task_user($from){
        $this->db->select('*');
        $this->db->where('admin_id=',$from);
        $this->db->from(TABLE_PRE.'admin_details');
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    function task_complete($task_id){     
        $this->db->set('status','1');     
        $this->db->where('id', $task_id);  
        $query_details = $this->db->update(TABLE_PRE.'tasks'); 
        if($query_details){
            return true;
        }
        else{
            return false;
        }
    }

    function get_task_row($task_id){
        $this->db->select('*');
        $this->db->where('id=',$task_id);
        $this->db->from(TABLE_PRE.'tasks');
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->row();
        }
        else
        {
            return false;
        }
    }

    function get_assignee_task($assignee_id){
        $this->db->select('*');
        $this->db->where('to_id=',$assignee_id);
        $this->db->from(TABLE_PRE.'tasks');
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    //End Assigner
	
	/* Ajax Hotel Phone Check */
	function hotel_phone_check($phone){
		$query=$this->db->get_where(TABLE_PRE.'hotel_details',$phone);
        if($query->num_rows()==1){
            return $query->row();
        }else{ 
            return false;
        }
	}
	
	/* Number of All Hotels */
	function all_hotels_row(){
		//$this->db->where('hotel_status!=','D');
        $query=$this->db->get(TABLE_PRE.'hotel_details');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }
    
	/* All Hotels With Pagination */
    function all_hotels(){
		/*$this->db->where('hotel_status!=','D');
        $this->db->order_by('hotel_id','desc');
		//$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'hotel_details');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }*/

        /*$this->db->select('*');
        $this->db->from('hotel_hotel_billing_setting');
        $this->db->join('hotel_hotel_contact_details', 'hotel_hotel_billing_setting.hotel_id = hotel_hotel_contact_details.hotel_id');*/
		
		
		$this->db->select('*');
        $this->db->from('hotel_hotel_contact_details');
		$this->db->join('hotel_hotel_details', 'hotel_hotel_contact_details.hotel_id = hotel_hotel_details.hotel_id');
        $query=$this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    /* All Hotels */
    function total_hotels()
    {
        $this->db->where('hotel_status!=','D');
        $this->db->order_by('hotel_id','desc');
        $query=$this->db->get(TABLE_PRE.'hotel_details');
        if($query->num_rows()>0){
            return $query->result();
        }
        else
        {
            return false;
        }
    }
	
	/* Get Particular Hotel Information */
	function get_hotel($hotel_id)
    {
        $this->db->where('hotel_id',$hotel_id);
        $query=$this->db->get(TABLE_PRE.'hotel_details');
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }

        

    }
	
	function get_admin($admin_id)
    {
        $this->db->where('admin_id',$admin_id);
        $query=$this->db->get(TABLE_PRE.'admin_details');
        if($query->num_rows()==1){
		
            return $query->result();
        }else{
            return false;
        }

        

    }
	
	function check_unlock($pw)
    {
        $this->db->where('user_id',$this->session->userdata('user_id'));
		$this->db->where('user_password',sha1(md5($pw)));
		
        $query=$this->db->get(TABLE_PRE.'login');
        if($query->num_rows()==1){
		
            return true;
        }else{
            return false;
        }

        

    }
	
	
	
	/* Update Hotel */
	function update_hotel($hotel){
        $this->db->where('hotel_id',$hotel['hotel_id']);
        $query=$this->db->update(TABLE_PRE.'hotel_details',$hotel);
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
	/* Delete Hotel (Only Status Changed) */
	function delete_hotel($hotel_id,$hotel){
        $this->db->where_in('hotel_id',$hotel_id);
        $query=$this->db->update(TABLE_PRE.'hotel_details',$hotel);
        if($query){
            return true;
        }else{
            return false;
        }
    }


	
	/* Ajax Hotel Status Update */
	function hotel_status_update($status){
		$this->db->where('hotel_id',$status['hotel_id']);
        $query=$this->db->update(TABLE_PRE.'hotel_details',$status);
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
	/* All Hotel */
	function all_hotel_list(){
		$this->db->where('hotel_status!=','D');
        $this->db->order_by('hotel_name','asc');
        $query=$this->db->get(TABLE_PRE.'hotel_details');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	/* Get All User Types */
	function get_user_type(){
		$this->db->where('user_type_slug!=','SUPA');
        $query=$this->db->get(TABLE_PRE.'user_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	
	/* Add Permission */
	function add_permission($permission){
		$query=$this->db->insert_batch(TABLE_PRE.'permission_types',$permission);
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
	/* Number of All Permissions */
	function all_permissions_row(){
		$this->db->group_by('permission_label');
		$this->db->where('permission_status!=','D');
        $query=$this->db->get(TABLE_PRE.'permission_types');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }
    
	/* All Permissions */
    function all_permissions(){
		$this->db->group_by('permission_label');
		$this->db->where('permission_status!=','D');
        $this->db->order_by('permission_label','asc');
		//$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'permission_types');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	/* Particular User Type Information */
	function get_user_type_by_id($user){
		$this->db->where('user_type_id',$user);
		 $query=$this->db->get(TABLE_PRE.'user_type');
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
	}
	
	/* Particular Permission */
	function get_permission($permission_id){
        $this->db->where('permission_id',$permission_id);
        $query=$this->db->get(TABLE_PRE.'permission_types');
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
    }
	
	/* Get All Permission By Label */
	function get_permission_info($label){
		$this->db->where('permission_label',$label);
        $query=$this->db->get(TABLE_PRE.'permission_types');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	
	/* Update Permission */
	function update_permission($permission){
       	$query=$this->db->update_batch(TABLE_PRE.'permission_types', $permission, 'permission_id');
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
	/* Add Admin */
    function add_admin($admin){
		$query=$this->db->insert(TABLE_PRE.'admin_details',$admin);
        if($query){
            return $this->db->insert_id();
        }else{
            return false;
        }
	}
	
	/* Add User For Login */
    function add_user($user){
		$query=$this->db->insert(TABLE_PRE.'login',$user);
        if($query){
            return $this->db->insert_id();
        }else{
            return false;
        }
	}
	
	/* Add User For Login */
    function get_country(){
		$this->db->order_by('country_name','asc');
		$query=$this->db->get(TABLE_PRE.'countries');
        if($query){
            return $query->result();
        }else{
            return false;
        }
	}
	
	/* Add User For Login */
    function get_star_rating(){
		$this->db->order_by('star_rating_value','asc');
		$query=$this->db->get(TABLE_PRE.'star_rating');
        if($query){
            return $query->result();
        }else{
            return false;
        }
	}
	
	/* Ajax Username Check */
	function username_check($username){
		$query=$this->db->get_where(TABLE_PRE.'login',$username);
        if($query->num_rows()==1){
            return $query->row();
        }else{ 
            return false;
        }
	}
	
	/* Ajax Email Check */
	function email_check($email){
		$query=$this->db->get_where(TABLE_PRE.'login',$email);
        if($query->num_rows()==1){
            return $query->row();
        }else{ 
            return false;
        }
	}
	
	/* All Permission Label Applicable*/
	function admin_permission_label($permission_users){
		$this->db->like('permission_users',$permission_users,'both');
		$this->db->group_by('permission_label');
		$query=$this->db->get(TABLE_PRE.'permission_types');
        if($query->num_rows()>0){
            return $query->result();
        }else{ 
            return false;
        }
	}
	
	/* All Permissions Depending on Label for Admin */
	function get_permission_by_label($permission_label){
		$this->db->where('permission_label',$permission_label);
		$query=$this->db->get(TABLE_PRE.'permission_types');
        if($query->num_rows()>0){
            return $query->result();
        }else{ 
            return false;
        }
	}
	
	/*Add User Permission */
	function add_user_permission($permission){
		$query=$this->db->insert(TABLE_PRE.'user_permission',$permission);
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
	/* Check Admin Status */
	function admin_status($id){
		$this->db->where('admin_id',$id);
		$query = $this->db->get(TABLE_PRE.'admin_details');
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}
	}
	
	/* Admin Registration */
	function admin_registration($admin){
        $this->db->where('admin_id',$admin['admin_id']);
        $query=$this->db->update(TABLE_PRE.'admin_details',$admin);
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
	/* Update Login Information */
	function update_login($data){
        $this->db->where('login_id',$data['login_id']);
        $query=$this->db->update(TABLE_PRE.'login',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }

    /*Add Booking */
    function  add_booking($data){
        $query=$this->db->insert(TABLE_PRE.'bookings',$data);
        if($query){
            return true;
        }else{
            return false;
        }



    }

     /* */

    /* All Bookings Except cancelled */

    function  all_bookings(){

        $this->db->order_by('cust_from_date','asc');
		//$this->db->where('booking_status_id !=',7);
		
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
    /* Return a particular booking under a booking id */
    function  particular_booking($booking_id){

        $this->db->where('booking_id=',$booking_id);
        $this->db->where('booking_status_id !=',7);

        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'bookings');
        if($query->num_rows()>=0){
            return $query->result();
        }else{
            return false;
        }


    }
	function  all_recent_bookings(){
		$today = new DateTime('now-1 day');
		$today_str = $today->format('Y-m-d H:i:s');
        $this->db->order_by('cust_from_date','asc');
		$this->db->where('cust_from_date>=',$today_str);
		$this->db->where('booking_status_id !=',8);
		$this->db->where('booking_status_id !=',7);
		
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	function  get_status($id){
		
		$this->db->where('status_id',$id);
		
		
        //$this->db->limit($limit,$start);
        $query=$this->db->get('booking_status_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function  all_bookings_report(){

        $this->db->order_by('cust_from_date','asc');
		$this->db->join('hotel_guest', 'hotel_guest.g_id = hotel_bookings.guest_id');
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }



    /* Get Room Id*/
    function get_room_id($hotel_id, $room_no){

        $this->db->where('hotel_id=',$hotel_id);
        $this->db->where('room_no=',$room_no);
        $query=$this->db->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    /* Get Room Number */

    function get_room_number($room_id){

        $this->db->where('room_id=',$room_id);
        $query=$this->db->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
     function get_room_number_exact($room_id){

         $this->db->where('room_id=',$room_id);
         $query=$this->db->get(TABLE_PRE.'room');
         if($query->num_rows()>0){
            return $query->row();
         }else{
            return false;
         }
     }

     function room_details($room_id){
        $sql="select * from hotel_room left join hotel_unit_type on hotel_room.unit_id=hotel_unit_type.id where hotel_room.room_id='".$room_id."'";
        $query=$this->db->query($sql);
        return $query->result();
     }

    /* Add Room */
    function add_room($data){
		//echo '<pre>';
		//print_r($data['feature']);
		//exit;
		$query=$this->db->insert(TABLE_PRE.'room',$data['room']);
		$insert_id = $this->db->insert_id();
		foreach($data['feature'] as $key => $value)
		{
		  $roomFeature['room_feature_id'] = $key;
		  $roomFeature['room_feature_type'] = $value[0];
		  $roomFeature['room_feature_charge'] = $value[1];
		  $roomFeature['room_id'] = $insert_id;
		  $qry=$this->db->insert(TABLE_PRE.'room_room_features',$roomFeature);
		}		
        if($query){
            return true;
        }else{
            return false;
        }
	}
	
    function get_room_feature_type($room_feature_id,$room_id){
		$this->db->select('room_feature_type');
        $this->db->where('room_feature_id=',$room_feature_id);
		$this->db->where('room_id=',$room_id);
        $query=$this->db->get(TABLE_PRE.'room_room_features');
        if($query->num_rows()> '0'){
            return $query->row();
        }else{
            return false;
        }
    }
	/* Number of All Rooms */
	function all_rooms_row(){
		$this->db->where('room_status!=','D');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }
    
	/* All Hotels With Pagination */
    public  function all_rooms(){
		$this->db->where('room_status!=','D');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $this->db->join('hotel_unit_type', 'hotel_room.unit_id = hotel_unit_type.id');
        $this->db->group_by('room_no');
        $this->db->order_by('room_no','asc');
		//$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    /* Get Particular Hotel Information */
    function get_room($room_id){
        $this->db->where('room_id=',$room_id);
        $query=$this->db->get(TABLE_PRE.'room');
        if($query->num_rows()==1){

            return $query->row();
        }else{
            return false;
        }
    }

    /* Update Hotel */
    function update_room($room){

        $this->db->where('room_id=',$room['room_id']);
        $query=$this->db->update(TABLE_PRE.'room',$room);
        if($query){
            return true;
        }else{
            return false;
        }
    }
     function make_room_dirty($id){
         $sql="update hotel_room set clean_id='2' where room_id='".$id."' ";
         $query=$this->db->query($sql);
         if($query){
             return true;
         }else{
             return false;
         }
     }
     function check_duplicate_room($id,$no){
         $sql="select * from hotel_room where hotel_id='".$id ."' and room_no='".$no ."' ";
         $query=$this->db->query($sql);
         if($query){
             return $query->num_rows();
         }else{
             return false;
         }
     }

    function  add_transaction($data){
        $query=$this->db->insert(TABLE_PRE.'transactions',$data);
        if($query){
            return true;
        }else{
            return false;
        }
     }

    

    
   

    function all_transactions(){
        $this->db->order_by('t_id','desc');
        $query=$this->db->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

     function  all_purchase_limit(){
         //$this->db->order_by('t_id','desc');
         //$array = array('transaction_from_id' => "6", 'transaction_releted_t_id' => "5", 'status' => $status);
         //$this->db->where_in('');
         $query=$this->db->get(TABLE_PRE.'purchase');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }
     function  all_purchase_row(){
         $this->db->order_by('p_id','desc');
         $query=$this->db->get(TABLE_PRE.'purchase');
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }
     }

    function all_transactions_by_date($start_date, $end_date){
        $this->db->order_by('t_id','asc');
        $this->db->where('t_date >=', $start_date);
        $this->db->where('t_date <=', $end_date);
        $query=$this->db->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function all_tax_report_by_date($start_date, $end_date){
        $this->db->order_by('booking_id','asc');
        $this->db->where('cust_from_date_actual >=', $start_date);
        $this->db->where('cust_from_date_actual <=', $end_date);
        
		$query=$this->db->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }	
    }
	
	

     function all_transactions_by_date_endless($start_date, $end_date){
         $this->db->order_by('t_id','asc');
         $this->db->where('t_date >=', $start_date);
        // $this->db->where('t_date <=', $end_date);
         $query=$this->db->get(TABLE_PRE.'transactions');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }

     function opening_balance_by_date($date){
         $this->db->order_by('t_id','asc');
         $this->db->where('t_date <=', $date);
         // $this->db->where('t_date <=', $end_date);
         $query=$this->db->get(TABLE_PRE.'transactions');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }


     function all_bookings_by_date($start_date,$end_date){
        $this->db->order_by('cust_from_date','asc');
        $this->db->where('booking_status_id !=',7);
        $this->db->where('cust_from_date >=', $start_date);
        $this->db->where('cust_from_date <=', $end_date);
        $query=$this->db->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	/*function all_transactions_report(){
        $this->db->order_by('t_id','asc');
		join(TABLE_PRE.'guest', 'hotel_guest.g_id = credentials.cid');
        $query=$this->db->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }*/

    /* Number of All Transactions */
    function all_transactions_row(){
        $query=$this->db->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }

    /* All Hotels With Transactions */
    /*
    function all_transactions_limit(){
        $this->db->order_by('t_id','desc');
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
    */

    function all_transactions_limit()
    {
        $this->db->select('*');
        $this->db->order_by('t_id','desc');
        $this->db->from(TABLE_PRE.'transactions');
        $this->db->join('hotel_transaction_type', 'hotel_transactions.transaction_releted_t_id = hotel_transaction_type.hotel_transaction_type_id');        
        $query=$this->db->get();

        /*
        $this->db->select('*');
        $this->db->where('hotel_id=',$hotel_id);
        $query=$this->db->get(TABLE_PRE.'hotel_details');*/
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function find_name($get_id_name){
        $this->db->where('hotel_entity_type_id=',$get_id_name);
        $query=$this->db->get(TABLE_PRE.'entity_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function  add_compliance($data){
        $query=$this->db->insert(TABLE_PRE.'compliance',$data);
        if($query){
			
            return true;
        }else{
			
            return false;
        }



    }

    function all_compliance(){
        $this->db->order_by('c_id','asc');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'compliance');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    /* Number of All Transactions */
    function all_compliance_row(){
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'compliance');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }

    /* All Hotels With Transactions */
    function all_compliance_limit(){
        $this->db->order_by('c_id','desc');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'compliance');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function  add_guest($data){
        $query=$this->db->insert(TABLE_PRE.'guest',$data);
        if($query){
            return true;
        }else{
            return false;
        }



    }

     function  add_guest2($data){
         $query=$this->db->insert(TABLE_PRE.'guest',$data);
         if($query){
             return $this->db->insert_id();
         }else{
             return false;
         }



     }

    function  add_broker($data){
        $query=$this->db->insert(TABLE_PRE.'broker',$data);
        if($query){
            return true;
        }else{
            return false;
        }



    }


    function all_broker(){
        $this->db->order_by('b_id','asc');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'broker');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    /* Number of All Transactions */
    function all_broker_row(){
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'broker');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }

    /* All Hotels With Transactions */
    function all_broker_limit(){
        $this->db->order_by('b_id','desc');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'broker');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function all_guest(){
        $this->db->order_by('g_id','asc');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'guest');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function  add_event($data){
        $query=$this->db->insert(TABLE_PRE.'events',$data);
        if($query){
            return true;
        }else{
            return false;
        }

    }


    function all_events(){
        $this->db->order_by('e_id','desc');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db->where('e_notify=',1);
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'events');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function all_events_limit(){
        $this->db->order_by('e_id','desc');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db->where('e_notify=',1);
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'events');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

     function  add_shift($data){
         $query=$this->db->insert(TABLE_PRE.'shift',$data);
         if($query){
             return true;
         }else{
             return false;
         }

     }

     function  add_shift_log($data){
         $query=$this->db->insert('shift_log',$data);
         if($query){
             return $this->db->insert_id();
         }else{
             return false;
         }

     }



     function all_shift_limit(){
         $this->db->order_by('shift_id','desc');
         $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
         $query=$this->db->get(TABLE_PRE.'shift');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }

     function  insert_cash_drawer($data){

         $query=$this->db->insert(TABLE_PRE.'cash_drawer',$data);
         if($query){
             return $this->db->insert_id();

         }else{
             return 'Some Database Error Occurred';
         }



     }

     function  update_drawer($id){
        date_default_timezone_set('Asia/Kolkata');
         $this->db->where('drawer_id',$id);
         $data=array(
             'closing_datetime' => date("Y-m-d H:i:s"),
             'is_ended' => 1
         );
         $query=$this->db->update('hotel_cash_drawer',$data);
         if($query){
             return true;
         }else{
             return false;
         }
     }

     function all_cash_drawer_limit(){
        $sql="select * from hotel_cash_drawer where is_ended='0' and hotel_id='".$this->session->userdata('user_hotel')."' ";
         $query=$this->db->query($sql);
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }

     function all_logs(){

         $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
         $query=$this->db->get('shift_log');
         //$this->db->order_by('shift_log.log_id','desc');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }



    /* Number of All Transactions */
    function all_guest_row(){
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'guest');
        if($query->num_rows()>0){
            return $query->num_rows();
        }else{
            return false;
        }
    }

    /* All Hotels With Transactions */
    function all_guest_limit()
    {
        //$this->db->order_by('g_id','desc');
        //$this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db->limit($limit,$start);
		$today = date('Y-m-d');
		$this->db->where('cust_from_date',$today);
        $this->db->where('hotel_id',$this->session->userdata('user_hotel'));
		$this->db->select_sum('no_of_guest');
		$query = $this->db->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

        /*$this->db->select_sum('age');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $this->db->where($date,'');*/
    }
	
	/* 18.11.2015 */
	
	function all_guest_limit_view()
    {
        $this->db->order_by('g_id','desc');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'guest');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

        /*$this->db->select_sum('age');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $this->db->where($date,'');*/
    }
	
	function get_guest_details($g_id_edit)
	{
		//echo "In Model";
		//echo  $g_id_edit;
		//exit();
		$this->db->select('*');
        $this->db->where('g_id=',$g_id_edit);
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'guest');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}

    function get_room_details($room_id)
    {
        //echo "In Model";
        //echo  $g_id_edit;
        //exit();
        $this->db->select('*');
        $this->db->where('room_id=',$room_id);
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }


    function get_hotel_details($hotel_id)
    {

        $this->db->select('*');
        $this->db->from('hotel_hotel_contact_details');
        $this->db->join('hotel_hotel_details', 'hotel_hotel_contact_details.hotel_id = hotel_hotel_details.hotel_id', 'inner');
        $this->db->join('hotel_hotel_tax_details' , 'hotel_hotel_tax_details.hotel_id = hotel_hotel_details.hotel_id', 'inner');
        $this->db->join('hotel_hotel_billing_setting' , 'hotel_hotel_billing_setting.hotel_id = hotel_hotel_details.hotel_id', 'inner');
        $this->db->join('hotel_hotel_general_preference' , 'hotel_hotel_general_preference.hotel_id = hotel_hotel_details.hotel_id', 'inner');       

        $this->db->where('hotel_hotel_details.hotel_id=',$hotel_id);
        $this->db->group_by('hotel_hotel_details.hotel_id');
        $query=$this->db->get();

        /*
        $this->db->select('*');
        $this->db->where('hotel_id=',$hotel_id);
        $query=$this->db->get(TABLE_PRE.'hotel_details');*/
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
    function get_hotel_details_row($hotel_id)
    {

        $this->db->select('*');
        $this->db->from('hotel_hotel_contact_details');
        $this->db->join('hotel_hotel_details', 'hotel_hotel_contact_details.hotel_id = hotel_hotel_details.hotel_id', 'inner');
        $this->db->join('hotel_hotel_tax_details' , 'hotel_hotel_tax_details.hotel_id = hotel_hotel_details.hotel_id', 'inner');
        $this->db->join('hotel_hotel_billing_setting' , 'hotel_hotel_billing_setting.hotel_id = hotel_hotel_details.hotel_id', 'inner');
        $this->db->join('hotel_hotel_general_preference' , 'hotel_hotel_general_preference.hotel_id = hotel_hotel_details.hotel_id', 'inner');       

        $this->db->where('hotel_hotel_details.hotel_id=',$hotel_id);
        $this->db->group_by('hotel_hotel_details.hotel_id');
        $query=$this->db->get();

        /*
        $this->db->select('*');
        $this->db->where('hotel_id=',$hotel_id);
        $query=$this->db->get(TABLE_PRE.'hotel_details');*/
        if($query->num_rows()==1){
            return $query->row();
        }else{
            return false;
        }
    }
    function get_hotel_details2($hotel_id)
    {

        $this->db->select('*');
        $this->db->from('hotel_hotel_details');

        $this->db->where('hotel_id=',$hotel_id);
        $query=$this->db->get();

        if($query->num_rows()==1){
            return $query->result();
        }else{
            return false;
        }
    }

    function edit_hotel_details($hotel_edit,$h_id)
    {
        $this->db->where('hotel_id', $h_id);        
        $query_details = $this->db->update(TABLE_PRE.'hotel_details', $hotel_edit); 
        if($query_details){
            return true;
        }
        else{
            return false;
        }
    }

    function edit_hotel_contact($hotel_contact_details_edit,$h_id)
    {
        $this->db->where('hotel_id', $h_id);       
        $query_contact = $this->db->update(TABLE_PRE.'hotel_contact_details', $hotel_contact_details_edit);
        if ($query_contact) {
            return true;
        }
        else{
            return false;
        }
    }

    function edit_hotel_tax($hotel_tax_details_edit,$h_id)
    {
        $this->db->where('hotel_id', $h_id);
        $query_tax = $this->db->update(TABLE_PRE.'hotel_tax_details', $hotel_tax_details_edit);
        if ($query_tax) {
            return true;
        }
        else{
            return false;
        }
    }

    function edit_hotel_billing($hotel_billing_settings_edit,$h_id)
    {
        $this->db->where('hotel_id', $h_id);
        $query_billing = $this->db->update(TABLE_PRE.'hotel_billing_setting', $hotel_billing_settings_edit);
        if ($query_billing) {
            return true;
        }
        else{
            return false;
        }
    }


    function edit_hotel_preferences($hotel_general_preference_edit,$h_id)
    {
        $this->db->where('hotel_id', $h_id);
        $query_preference = $this->db->update(TABLE_PRE.'hotel_general_preference', $hotel_general_preference_edit);           
        if ($query_preference) {
            return true;
        }
        else{
            return false;
        }
    }

	
	function get_broker_details($b_id_edit)
	{
		//echo "In Model";
		//echo  $b_id_edit;
		//exit();
		$this->db->select('*');
        $this->db->where('b_id=',$b_id_edit);
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'broker');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
 /*   function get_channel_details($c_id_edit)
    {
        //echo "In Model";
        //echo  $b_id_edit;
        //exit();
        $this->db->select('*');
        $this->db->where('channel_id=',$c_id_edit);
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'channel');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }*/
	
	function get_c_details($c_id_edit)
	{
		//echo "In Model";
		//echo  $b_id_edit;
		//exit();
		$this->db->select('*');
        $this->db->where('c_id=',$c_id_edit);
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'compliance');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	
	
	function edit_guest($guest_edit,$g_id)
	{
		$this->db->where('g_id', $g_id);
		$query = $this->db->update(TABLE_PRE.'guest', $guest_edit);
		//echo  $this->db->last_query();
		if($query){
			return true;
		}else{
			return false;
		}
		
	}

    function edit_room($room_edit,$room_id)
    {
        $this->db->where('room_id', $room_id);
        $query = $this->db->update(TABLE_PRE.'room', $room_edit['room']);
		$this->db->where_in('room_id', $room_id);
        $query=$this->db->delete(TABLE_PRE.'room_room_features');
		foreach($room_edit['feature'] as $key => $value)
		{
		  $roomFeature['room_feature_id'] = $key;
		  $roomFeature['room_feature_type'] = $value;
		  $roomFeature['room_id'] = $room_id;
		  $qry=$this->db->insert(TABLE_PRE.'room_room_features',$roomFeature);
		}
        //echo  $this->db->last_query();
        if($query){
            return true;
        }else{
            return false;
        }
        
    }

    function delete_room($room_id){
        $this->db->where_in('room_id',$room_id);
        $query=$this->db->delete(TABLE_PRE.'room');
        if($query){
            return true;
        }else{
            return false;
        }
    }

     function delete_servic($room_id){
         $this->db->where_in('room_id',$room_id);
         $query=$this->db->delete(TABLE_PRE.'room');
         if($query){
             return true;
         }else{
             return false;
         }
     }

    function delete_broker($b_id){
        $this->db->where_in('b_id',$b_id);
        $query=$this->db->delete(TABLE_PRE.'broker');
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function delete_service($booking_id,$service_id){

       // $sql="update hotel_bookings set service_id='".$service_id."' where booking_id='"+$booking_id+"'  ";
        // print_r($service_id);
        // exit();

       $this->db->where('booking_id', $booking_id);
        $query = $this->db->update(TABLE_PRE.'bookings', $service_id);

        //echo $this->db->last_query();
        //exit();
        //echo  $this->db->last_query();
        if($query){
            return true;
        }else{
            return false;
        }


    }

    function search_charge($word){
        $sql="select crg_description from hotel_charges where crg_description LIKE '%".$word."%'";
        $result=$this->db->query($sql);
        return $result->result();
    }

    function delete_charge($booking_id,$service_id){

       // $sql="update hotel_bookings set service_id='".$service_id."' where booking_id='"+$booking_id+"'  ";
        // print_r($service_id);
        // exit();

       $this->db->where('booking_id', $booking_id);
        $query = $this->db->update(TABLE_PRE.'bookings', $service_id);

        //echo $this->db->last_query();
        //exit();
        //echo  $this->db->last_query();
        if($query){
            return true;
        }else{
            return false;
        }


    }

    function delete_event($e_id){
        $this->db->where_in('e_id',$e_id);
        $query=$this->db->delete(TABLE_PRE.'events');
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function delete_guest($g_id){
        $this->db->where_in('g_id',$g_id);
        $query=$this->db->delete(TABLE_PRE.'guest');
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function delete_compliance($c_id){
        $this->db->where_in('c_id',$c_id);
        $query=$this->db->delete(TABLE_PRE.'compliance');
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
	function edit_broker($broker_edit,$b_id)
	{
		//echo "In model".$b_id;
		//print_r($broker_edit);
		//exit();
		$this->db->where('b_id', $b_id);
		$query = $this->db->update(TABLE_PRE.'broker', $broker_edit);
		//echo  $this->db->last_query();
		if($query){
			return true;
		}else{
			return false;
		}
	}

     function  edit_booking($b_id,$g_id){

         //exit();
        // $this->db->where('booking_id', $b_id);
        // $query = $this->db->update(TABLE_PRE.'bookings', $g_id);
         //echo  $this->db->last_query();

         $sql="update hotel_bookings  set guest_id_others = CONCAT(guest_id_others,',','".$g_id."') where booking_id= '".$b_id."' ";

         $query= $this->db->query($sql);

         if($query){
             return true;
         }else{
             return false;
         }
     }

    function edit_compliance($compliance_edit,$c_id)
    {
        $this->db->where('c_id', $c_id);
        $query = $this->db->update(TABLE_PRE.'compliance', $compliance_edit);
        //echo  $this->db->last_query();
        if($query){
            return true;
        }else{
            return false;
        }
        
    }
	
	function all_booking_id()
	{
		$this->db->select('booking_id');
		//$this->db->from(TABLE_PRE.'bookings');
		$this->db->where('hotel_id',$this->session->userdata('user_hotel'));
		$query = $this->db->get(TABLE_PRE.'bookings');
		if($query->num_rows() > 0){
   			foreach($query->result_array() as $row){
    		$data[] = $row;
   			}
   			return $data;
  		}
		else
		{
			return false;
		}
	}
	
	/* 18.11.2015 */
	/* 19.11.2015*/
	function all_t_amount($val)
	{
		/*$agr = array(
			'hotel_id' => $this->session->userdata('user_hotel'),
			'booking_id'=> $val);
		$this->db->select('*');
		$this->db->where($agr);
		$query = $this->db->get(TABLE_PRE.'bookings');
		if($query->num_rows() > 0){
   			foreach($query->result_array() as $row){
    		$data[] = $row;
   			}
   			return $data;
  		}
		else
		{
			return false;
		}*/
		
		$this->db->select('base_room_rent,rent_mode_type,mod_room_rent');
		$this->db->from('hotel_bookings');
		$this->db->where('booking_id',$val);
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
   			/*foreach($query->result_array() as $row1){
    		//$data1[] = $row1;
   			}
   			return $data1;*/
			return $query->result();
  		}
		else
		{
			return false;
		}
	}
	/*19.11.2015*/
	
	function  all_bookings_unique(){


        $this->db->select('*');
        $this->db->from('hotel_transactions');
        $this->db->join('hotel_bookings', 'hotel_transactions.t_booking_id = hotel_bookings.booking_id', 'inner');
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d");
        $this->db->where('hotel_transactions.transaction_releted_t_id=',1);
        //$this->db->or_where('hotel_transactions.transaction_releted_t_id=', 6);

        //$this->db->where('hotel_transactions.transaction_releted_t_id=',6);
        $this->db->where('hotel_bookings.hotel_id=',$this->session->userdata('user_hotel'));
		$this->db->select_sum('hotel_transactions.t_amount');
        //$this->db->from(TABLE_PRE.'transactions');
		//$query = $this->db->get_where(TABLE_PRE.'transactions',array('t_date' => $date));
        $this->db->like('hotel_transactions.t_date', $date, 'after');
        $query = $this->db->get();
        //$str = $this->db->last_query();
        //echo $str;
        //exit();
		if($query->num_rows()>0)
        {
			return $query->result();	 
        }
		else
        {
            return false;
		}
    }

     function  revenue_by_date($date){


         $this->db->select('*');
         $this->db->from('hotel_transactions');
         $this->db->join('hotel_bookings', 'hotel_transactions.t_booking_id = hotel_bookings.booking_id', 'inner');

         $this->db->where('hotel_transactions.transaction_releted_t_id=',1);
         //$this->db->or_where('hotel_transactions.transaction_releted_t_id=', 6);

         //$this->db->where('hotel_transactions.transaction_releted_t_id=',6);
         $this->db->where('hotel_bookings.hotel_id=',$this->session->userdata('user_hotel'));
         $this->db->select_sum('hotel_transactions.t_amount');
         //$this->db->from(TABLE_PRE.'transactions');
         //$query = $this->db->get_where(TABLE_PRE.'transactions',array('t_date' => $date));
         $this->db->like('hotel_transactions.t_date', $date, 'after');
         $query = $this->db->get();
         //$str = $this->db->last_query();
         //echo $str;
         //exit();
         if($query->num_rows()>0)
         {
             return $query->result();
         }
         else
         {
             return false;
         }
     }



     function report_search($visit,$room_id, $date_1,$date_2,$status,$r_hotel){


        $this->db->like('nature_visit',$visit);
        $this->db->like('room_id',$room_id);

//     $sql = "SELECT * FROM ".TABLE_PRE."bookings WHERE  ( ((cust_end_date <= '".$date_2."') AND (cust_from_date >= '".$date_1."')) OR (nature_visit LIKE '%".$visit."%') OR  (room_id LIKE '".$room_id."') ) ";
if($status=="5"){
	$this->db->where('booking_status_id',5);
}

if($r_hotel!=""){
	$this->db->where('hotel_id',$r_hotel);
}


if($date_1  && $date_2)

{
    if($date_2=="1970-01-01")
    {
        $date_2="3000-01-01";


    }

    $this->db->where('cust_from_date >=', $date_1);
    $this->db->where('cust_end_date <=', $date_2);

}
       // $this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'bookings');




        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }



    }


    function fetch_all_address($pincode)
    {
            $this->db->distinct();
            $this->db->select('area_0,area_1,area_4');
            $this->db->from('hotel_area_code');
            $this->db->where('pincode',$pincode);
            $query=$this->db->get();

            if($query->num_rows() >0)
            {
                return $query->result();
            }
            else
            {
                return false;
            }
    }

    function all_f_reports(){
		
        $this->db->where('t_amount !=',0);
        $this->db->order_by('t_booking_id','desc');
		
        $this->db->like('t_date',date("Y-m-d"));
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	function cust_name($bi){
		
		$this->db->where('booking_id',$bi);
		$query=$this->db->get(TABLE_PRE.'bookings');
		 
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	function transaction_type(){
		$this->db->select('*');
		$query=$this->db->get(TABLE_PRE.'transaction_type');
		if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	
	function all_f_types(){
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'transaction_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function all_f_entity(){
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'entity_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	
		function add_booking_transaction($t_transaction){
				//echo "In Model";
				//print_r($t_transaction);
				//exit();
				$query=$this->db->insert(TABLE_PRE.'transactions',$t_transaction);
				$sql = $this->db->last_query();
				//echo $sql;
				//exit();
        		if($query){
            			return true;
        		}else{
            			return false;
        		}
		}
		
		function get_data_for_excel()
		{
			$this->db->select('booking_id,cust_name,room_id,cust_from_date,cust_end_date,cust_contact_no,mod_room_rent,nature_visit');
        	$this->db->from(TABLE_PRE.'bookings');
        	$query=$this->db->get();

        	if($query->num_rows() >0){
            	return $query->result();
        	}
        	else{
            	return false;
        	}
		}

        function update_checkout_status_grp($grp_id,$date_checkout)
        {
            //checkout time logic
            $times=$this->dashboard_model->checkout_time();
            foreach($times as $time) {
                if($time->hotel_check_out_time_fr=='PM') {
                    $modifier = ($time->hotel_check_out_time_hr + 12);
                }
                else{
                    $modifier = ($time->hotel_check_out_time_hr);
                }
            }

            date_default_timezone_set('Asia/Kolkata');
            $time_date_data=date('Y-m-d H:i:s');
            $current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - 60 * 60 * $modifier);
            $date_checkout_modified = date('Y-m-d H:i:s', strtotime($date_checkout) +60 * 60 * $modifier);

            /*echo $time_date_data."".$date_checkout_modified;
            exit();*/

            if($time_date_data>$date_checkout_modified){

                $current_date=$date_checkout;
            }


            $data = array(

               'booking_status_id' => '6',
                'cust_end_date' => $current_date,
                'checkout_time' => date("H:i:s"),
               'booking_status_id_secondary' => ''
               );
             $this->db->where('group_id', $grp_id);
             $query=$this->db->update(TABLE_PRE.'bookings',$data);
             if($query)
             {
                 return true;
             }
             else{
                 return false;
             }
        }
		
		function update_checkout_status($booking_id,$date_checkout)
		{
            //checkout time logic
            $times=$this->dashboard_model->checkout_time();
            foreach($times as $time) {
                if($time->hotel_check_out_time_fr=='PM') {
                    $modifier = ($time->hotel_check_out_time_hr + 12);
                }
                else{
                    $modifier = ($time->hotel_check_out_time_hr);
                }
            }

            date_default_timezone_set('Asia/Kolkata');
            $time_date_data=date('Y-m-d H:i:s');
            $current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - 60 * 60 * $modifier);
            $date_checkout_modified = date('Y-m-d H:i:s', strtotime($date_checkout) +60 * 60 * $modifier);

            /*echo $time_date_data."".$date_checkout_modified;
            exit();*/

            if($time_date_data>$date_checkout_modified){

                $current_date=$date_checkout;
            }


			$data = array(

               'booking_status_id' => '6',
                'cust_end_date' => $current_date,
                'checkout_time' => date("H:i:s"),
			   'booking_status_id_secondary' => ''
               );
			 $this->db->where('booking_id', $booking_id);
			 $query=$this->db->update(TABLE_PRE.'bookings',$data);
			 if($query)
			 {
				 return true;
			 }
			 else{
				 return false;
			 }
		}
		function update_cancel_status($booking_id)
		{
			$data = array(
               'booking_status_id' => '7',
			   'booking_status_id_secondary' => ''
               );
			 $this->db->where('booking_id', $booking_id);
			 $query=$this->db->update(TABLE_PRE.'bookings',$data);
			 if($query)
			 {
				 return true;
			 }
			 else{
				 return false;
			 }
		}

        function get_booking_id($room_id,$date){
            $sql="select booking_id from hotel_bookings where room_id='".$room_id."' and cust_from_date_actual<='".$date."' and cust_end_date_actual >='".$date."'";
            $query=$this->db->query($sql);
            return $query->result();
        }


		
		function update_checkin_status($booking_id)
		{
            //checkin time logic
            $times=$this->dashboard_model->checkin_time();
            foreach($times as $time) {
                if($time->hotel_check_in_time_fr=='PM') {
                    $modifier = ($time->hotel_check_in_time_hr + 12);
                }
                else{
                    $modifier = ($time->hotel_check_in_time_hr);
                }
            }

            $data=$this->dashboard_model->get_booking_details2($booking_id);

            foreach ($data as $key ) {
                # code...
                $actual_checkin=$key->cust_from_date;
            }

            date_default_timezone_set('Asia/Kolkata');
            $time_date_data=date('Y-m-d H:i:s');
             $current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - 60 * 60 * $modifier);

            if($current_date< $actual_checkin){
                $current_date=$actual_checkin;
            }
			$data = array(
               'booking_status_id' => '5',
                'cust_from_date'=> $current_date,
			   'checkin_time' => date("H:i:sa"),
			   'booking_status_id_secondary' => ''
               );
			 $this->db->where('booking_id', $booking_id);
			 $query=$this->db->update(TABLE_PRE.'bookings',$data);
			 if($query)
			 {
				 return true;
			 }
			 else{
				 return false;
			 }
		}

        function update_checkin_status_grp($grp_id)
        {
            //checkin time logic
            $times=$this->dashboard_model->checkin_time();
            foreach($times as $time) {
                if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
                    $modifier = ($time->hotel_check_in_time_hr + 12);
                }
                else{
                    $modifier = ($time->hotel_check_in_time_hr);
                }
            }

            $data=$this->dashboard_model->get_booking_details2($booking_id);

            foreach ($data as $key ) {
                # code...
                $actual_checkin=$key->cust_from_date;
            }

            date_default_timezone_set('Asia/Kolkata');
            $time_date_data=date('Y-m-d H:i:s');
             $current_date = date('Y-m-d H:i:s', strtotime($time_date_data) - 60 * 60 * $modifier);

            if($current_date< $actual_checkin){
                $current_date=$actual_checkin;
            }
            $data = array(
               'booking_status_id' => '5',
                'cust_from_date'=> $current_date,
               'checkin_time' => date("H:i:sa"),
               'booking_status_id_secondary' => ''
               );
             $this->db->where('group_id', $grp_id);
             $query=$this->db->update(TABLE_PRE.'bookings',$data);
             if($query)
             {
                 return true;
             }
             else{
                 return false;
             }
        }
		
		function guest_check($id){
			
			$query=$this->db->where(TABLE_PRE.'guest',$id);
        if($query->num_rows()==1){
            return true;
        }else{ 
            return false;
        }
		}

     function room_last_checkin_date($id){
        /* $this->db->select('cust_from_date');
         $this->db->where('room_id',$id);
         $this->db->where('booking_status_id',5);
         $this->db->or_where('booking_status_id',6);*/

         $sql="select cust_from_date from hotel_bookings where room_id=".$id." and (booking_status_id=5 or booking_status_id=6) ";



         $query=$this->db->query($sql);
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }

     }

    //Get Hotel Checkin Time
    public function checkin_time(){
        $this->db->where('hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'hotel_general_preference');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
     public function checkout_time(){
         $this->db->where('hotel_id',$this->session->userdata('user_hotel'));
         $query=$this->db->get(TABLE_PRE.'hotel_general_preference');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }

    function update_broker_commission($id,$commission){


        $query="UPDATE `".TABLE_PRE."broker` SET `broker_commission_total` = `broker_commission_total` + '".$commission."' WHERE `b_id`='".$id."' ";
        $result=$this->db->query($query);

        if($result)
        {
            return true;
        }
        else{
            return false;
        }

    }
   function update_channel_commission2($id,$commission){


        $query="UPDATE `".TABLE_PRE."channel` SET `channel_commission_total` = `channel_commission_total` + '".$commission."' WHERE `channel_id`='".$id."' ";
        $result=$this->db->query($query);

        if($result)
        {
            return true;
        }
        else{
            return false;
        }

    }

    function update_broker_booking($data){
        $this->db->where('booking_id',$data['booking_id']);
        $query=$this->db->update(TABLE_PRE.'bookings',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }


    function pay_broker($id,$commission){
        $query="UPDATE `".TABLE_PRE."broker` SET `broker_commission_payed` = `broker_commission_payed` + '".$commission."' WHERE `b_id`='".$id."' ";
        $result=$this->db->query($query);
        if($result)
        {
            return true;
        }
        else{
            return false;
        }

    }
	
	
	function get_guest_name($val){
		$this->db->select('g_name');
		$this->db->like('g_name', $val);
        $query=$this->db->get(TABLE_PRE.'guest');
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return false;
        }
		
	}

     function get_guest_row($id){
         $this->db->select('*');
         $this->db->like('g_id', $id);
         $query=$this->db->get(TABLE_PRE.'guest');
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }

     }
	
	
	
	
	function  remove_online($id){


        $this->db->where('u_id', $id);
        $query=$this->db->delete(TABLE_PRE.'online');
        if($query){
            return true;
        }else{
            return false;
        }
    }

     function  remove_transaction($id){


         $this->db->where('t_booking_id', $id);
         $query=$this->db->delete(TABLE_PRE.'transactions');
         if($query){
             return true;
         }else{
             return false;
         }
     }

     function  remove_log($id){


         $this->db->where('log_id', $id);
         date_default_timezone_set('Asia/Kolkata');
         $data=array(

             'end_time' => date("H:i"),
             'logged_out' =>1
         );
         $query=$this->db->update('shift_log',$data);
         if($query){
             return true;
         }else{
             return false;
         }
     }

     function  remove_maid($id){


         $this->db->where('room_id', $id);
         $query=$this->db->delete(TABLE_PRE.'housekeeping_matrix');
         if($query){
             return true;
         }else{
             return false;
         }
     }

    function  all_onlines(){
        date_default_timezone_set('Asia/Kolkata');

        $this->db->group_by('u_id');
        $this->db->order_by('online_id', "desc");
        $this->db->where('online_from >', date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s"))-60*60*8));
        $this->db->where('u_id!=', $this->session->userdata('user_id'));
        $query=$this->db->get(TABLE_PRE.'online');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

    }

    function get_user_details($id){

        $this->db->where('admin_id=',$id);
        $query=$this->db->get(TABLE_PRE.'admin_details');
        if($query->num_rows()==1){
            return $query->result();
        }else{
            return false;
        }
    }

     function get_shift_details($id){

         $this->db->where('shift_id=',$id);
         $query=$this->db->get(TABLE_PRE.'shift');
         if($query->num_rows()==1){
             return $query->result();
         }else{
             return false;
         }
     }
	
	function  add_feedback($data){
        $query=$this->db->insert(TABLE_PRE.'feedback',$data);
        if($query){
            return true;
        }else{
            return false;
        }

    }
	function fetch_fdback_id($id){
		$this->db->where('id',$id);
         $query=$this->db->get(TABLE_PRE.'feedback');
         if($query->num_rows()>0){
             
             return $query->result();
         }
		
	}
	
		function update_feedback($feedback){
			  $this->db->where('id',$feedback['id']);
			$query=$this->db->update(TABLE_PRE.'feedback',$feedback);
        if($query){
            return true;
        }else{
            return false;
        }
			
		}
	function all_feedback(){
	$this->db->select('*,(sleep_quality + room_quality + env_quality+ food_quality+extra+package+service+ambience+cleanliness+staff+reception+ease_booking) as tot');		
        $this->db->order_by('id','desc');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'feedback');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

	function delete_feedback($fid){
        $this->db->where_in('id',$fid);
        $query=$this->db->delete(TABLE_PRE.'feedback');
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
    function send_message($data){
    $query=$this->db->insert(TABLE_PRE.'message',$data);
    if($query){
        return true;
    }else{
        return false;
    }
}

    function get_message($from,$to){
        $this->db->where('u_from_id=',$from);
        $this->db->where('u_to_id=',$to);
        $this->db->where('m_seen!=',1);
        $this->db->group_by('m_id');
        $query=$this->db->get(TABLE_PRE.'message');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function update_message($from,$to,$id){
        $query="UPDATE `".TABLE_PRE."message` SET `m_seen` = 1  WHERE `u_from_id`='".$from."' AND `u_to_id`='".$to."'  AND `m_id`='".$id."'";
        $result=$this->db->query($query);
        if($result)
        {
            return true;
        }
        else{
            return false;
        }

    }

    function all_messages($to){
        $this->db->where('u_to_id=',$to);
        //$this->db->where('u_to_id=',$to);
        $this->db->where('m_seen!=',1);
        $this->db->group_by('m_id');
        $query=$this->db->get(TABLE_PRE.'message');
        if($query->num_rows()>=0){
            return $query->num_rows();
        }else{
            return false;
        }
    }
    function all_messages_unseen($to){
        $this->db->where('u_to_id=',$to);
        //$this->db->where('u_to_id=',$to);
        $this->db->where('m_seen!=',1);
        $this->db->group_by('m_id');
        $query=$this->db->get(TABLE_PRE.'message');
        if($query->num_rows()>=1){
            return $query->result();
        }else{
            return false;
        }
    }

    function  all_pending(){
        $this->db->where('booking_status_id_secondary=',3);

        $this->db->or_where('booking_status_id_secondary=',9);
       // $this->db->group_by('booking_id');
        $query=$this->db->get(TABLE_PRE.'bookings');

        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }

    }

	function  add_unit_type($data){
        $query=$this->db->insert(TABLE_PRE.'unit_type',$data);
        if($query){
            return true;
        }else{
            return false;
        }

    }
	function all_unit_type(){
        $this->db->order_by('id','desc');
        $query=$this->db->get(TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function getUnitTypeName(){
        $this->db->order_by('name','asc');
        $query=$this->db->get(TABLE_PRE.'unit_type_name');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }	
	
	
	
	
	
	
	function delete_unit_type($fid){
        $this->db->where('id=',$fid);
        $query=$this->db->delete(TABLE_PRE.'unit_type');
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
	function get_unit_name($unt){
		$this->db->select('id,unit_name');
        $this->db->where('unit_type=',$unt);
        $query=$this->db->get(TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return false;
        }
    }


     function get_unit_exact($id){
         /*$this->db->select('*');
         $this->db->where('unit_type=',$id);
         $query=$this->db->get(TABLE_PRE.'unit_type');
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }*/

         $this->db->where('id=',$id);
         $query=$this->db->get(TABLE_PRE.'unit_type');
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }

 }

	function get_unit_class($untID){
		$this->db->select('unit_class');
        $this->db->where('id=',$untID);
        $query=$this->db->get(TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->row_array();
        }else{
            return false;
        }
    }	
	
	function get_all_room_feature(){
		$this->db->select('room_feature_id,room_feature_name');
        $query=$this->db->get(TABLE_PRE.'room_features');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }	

	function get_unit_details($untID){
		$this->db->select('*');
        $this->db->where('id=',$untID);
        $query=$this->db->get(TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->row_array();
        }else{
            return false;
        }
    }
	
	function get_all_room_feature_id($untID){
		$this->db->select('*');
        $this->db->where('room_id=',$untID);
        $query=$this->db->get(TABLE_PRE.'room_room_features');
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return false;
        }
    }

	
	function get_booking_details($bookingID){
		$this->db->select('hotel_bookings.*,hotel_guest.*');
        $this->db->from('hotel_bookings');
		$this->db->where('hotel_bookings.booking_id=',$bookingID);
		$this->db->join('hotel_guest', 'hotel_bookings.guest_id = hotel_guest.g_id');
        $query=$this->db->get();
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows() ==1 )
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

    function get_booking_details_by_date($date){
        $this->db->select('hotel_bookings.*,hotel_guest.*');
        $this->db->from('hotel_bookings');
        //$this->db->where('hotel_bookings.booking_id=',$bookingID);
        $this->db->where('hotel_bookings.cust_from_date_actual <=',$date);
         $this->db->where('hotel_bookings.cust_end_date_actual >=',$date);
         $this->db->where('booking_status_id ',5);

        $this->db->join('hotel_guest', 'hotel_bookings.guest_id = hotel_guest.g_id');
        $this->db->join('hotel_room', 'hotel_bookings.room_id = hotel_room.room_id');
        $query=$this->db->get();
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows() > 0 )
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }



     function get_booking_details_unique($bookingID){
         $this->db->select('*');
         $this->db->from('hotel_bookings');
         $this->db->where('hotel_bookings.booking_id=',$bookingID);
         $this->db->join('hotel_guest', 'hotel_bookings.guest_id = hotel_guest.g_id');
         $query=$this->db->group_by('booking_id');
         $query=$this->db->get();
         //echo "<pre>";
         //print_r($query->result());
         //exit;
         if($query->num_rows() ==1 )
         {
             return $query->row();
         }
         else
         {
             return false;
         }
     }

     function get_booking_details2($bookingID){
         $this->db->select('*');
         $this->db->from('hotel_bookings');
         $this->db->where('hotel_bookings.booking_id=',$bookingID);
        // $this->db->join('hotel_guest', 'hotel_bookings.guest_id = hotel_guest.g_id');
         $query=$this->db->get();
         //echo "<pre>";
         //print_r($query->result());
         //exit;
         if($query->num_rows() > 0)
         {
             return $query->result();
         }
         else
         {
             return false;
         }
     }
     function  insert_shadow_booking($data){

         $query=$this->db->insert(TABLE_PRE.'bookings_shadow',$data);
         if($query){
             return true;
         }else{
             return false;
         }

     }


     function all_units(){
         //$this->db->where('id',$unit_id);
         //$this->db->order_by('unit_id', 'ASC');
         $query = $this->db->get(TABLE_PRE.'unit_type');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }
    function get_service_details($id){
        $this->db->select('*');
        $this->db->where('s_id=',$id);
        $query=$this->db->get(TABLE_PRE.'service');
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }
    function get_charge_details($id){
        $this->db->select('*');
        $this->db->where('crg_id=',$id);
        $query=$this->db->get(TABLE_PRE.'charges');
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

	
	function get_hotel_room($roomID){
		$this->db->select('hotel_room.room_no,hotel_hotel_details.hotel_name');
        $this->db->from('hotel_room');
		$this->db->where('hotel_room.room_id=',$roomID);
		$this->db->join('hotel_hotel_details', 'hotel_room.hotel_id = hotel_hotel_details.hotel_id');
        $query=$this->db->get();
        //echo "<pre>";
        //print_r($query->result());
        //exit;
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }	
	
	function get_brokerNameByID($brokerID){
		$this->db->select('*');
        $this->db->where('b_id=',$brokerID);
        $query=$this->db->get(TABLE_PRE.'broker');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }
    function get_channelNameByID($channelID){
        $this->db->select('*');
        $this->db->where('channel_id=',$channelID);
        $query=$this->db->get(TABLE_PRE.'channel');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }
    function get_brokerCommissionByID($brokerID){
        $this->db->select('broker_commission');
        $this->db->where('b_id=',$brokerID);
        $query=$this->db->get(TABLE_PRE.'broker');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }
    function get_channelCommissionByID($brokerID){
        $this->db->select('channel_commission');
        $this->db->where('channel_id=',$brokerID);
        $query=$this->db->get(TABLE_PRE.'channel');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

	function get_brokerName(){
		$this->db->select('b_id,b_name');
        $query=$this->db->get(TABLE_PRE.'broker');
        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }
	function get_amountPaidByBookingID($amountPaid){
		$sql = "SELECT SUM(t_amount) as tm FROM `hotel_transactions` WHERE t_booking_id ='$amountPaid' GROUP BY t_booking_id";
		$query = $this->db->query($sql);
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }
    function get_amountPaidByGroupID($amountPaid){
        $sql = "SELECT SUM(t_amount) as tm FROM `hotel_transactions` WHERE t_group_id ='$amountPaid' GROUP BY t_group_id";
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }


    function add_stay_details($stay,$booking_id){
        $this->db->where('booking_id', $booking_id);
        $query = $this->db->update(TABLE_PRE.'bookings', $stay);
        //echo  $this->db->last_query();
		//exit;
        if($query){
            return true;
        }else{
            return false;
        }
        
    }

    function add_pay_info($payInfo,$booking_id){
        $this->db->where('booking_id', $booking_id);
        $query = $this->db->update(TABLE_PRE.'bookings', $payInfo);
        //echo  $this->db->last_query();
		//exit;
        if($query){
            return true;
        }else{
            return false;
        }
        
    }
	

	function get_booking_status($sID){
		$this->db->select('*');
        $this->db->where('status_id=',$sID);
        $query=$this->db->get('booking_status_type');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

    function add_guest_info($guest,$guest_id,$preference,$booking_id){
		$this->db->where('booking_id', $booking_id);
        $qry = $this->db->update(TABLE_PRE.'bookings', $preference);
        $this->db->where('g_id', $guest_id);
        $query = $this->db->update(TABLE_PRE.'guest', $guest);
        //echo  $this->db->last_query();
		//exit;
        if($query){
            return true;
        }else{
            return false;
        }
        
    }	
	
	
	function all_feedbackByID($booking_id){
		$this->db->select('*,(sleep_quality + room_quality + env_quality+ food_quality+extra+package+service+ambience+cleanliness+staff+reception+ease_booking) as tot');
        $this->db->where('booking_id', $booking_id);
        $query=$this->db->get(TABLE_PRE.'feedback');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	
    function  add_channel($data){
        $query=$this->db->insert(TABLE_PRE.'channel',$data);
        if($query){
            return true;
        }else{
            return false;
        }



    }	
	
	function all_channels(){
        $this->db->order_by('channel_id','desc');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'channel');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	
	function get_channel_details($b_id_edit){
		$this->db->select('*');
        $this->db->where('channel_id=',$b_id_edit);
        $query=$this->db->get(TABLE_PRE.'channel');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
	}	
	
	
	function edit_channel($channel,$channel_id){
		$this->db->where('channel_id', $channel_id);
		$query = $this->db->update(TABLE_PRE.'channel', $channel);
		if($query){
			return true;
		}else{
			return false;
		}
	}	
	
	
    function delete_channel($channel_id){
        $this->db->where_in('channel_id',$channel_id);
        $query=$this->db->delete(TABLE_PRE.'channel');
        if($query){
            return true;
        }else{
            return false;
        }
    }
     function delete_transaction($id){
         $this->db->where_in('t_booking_id',$id);
         $query=$this->db->delete(TABLE_PRE.'transactions');
         if($query){
             return true;
         }else{
             return false;
         }
     }
     function delete_booking($id){
         $this->db->where_in('booking_id',$id);
         $query=$this->db->delete(TABLE_PRE.'bookings');
         if($query){
             return true;
         }else{
             return false;
         }
     }
     function delete_group($id){
         $this->db->where_in('id',$id);
         $query=$this->db->delete(TABLE_PRE.'group');
         if($query){
             return true;
         }else{
             return false;
         }
     }

     function delete_group_details($id){
         $this->db->where_in('group_booking_id',$id);
         $query=$this->db->delete(TABLE_PRE.'groups_bookings_details');
         if($query){
             return true;
         }else{
             return false;
         }
     }

     function all_channel_limit(){
        $this->db->order_by('channel_id','desc');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'channel');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function update_channel_commission($channel_id,$commission)
    {
        $query="UPDATE `".TABLE_PRE."channel` SET `channel_commission_total` = `channel_commission_total` + '".$commission."' WHERE `channel_id`='".$channel_id."' ";
        $result=$this->db->query($query);

        if($result)
        {
            return true;
        }
        else{
            return false;
        }

    }

    function update_channel_booking($data){
        $this->db->where('booking_id',$data['booking_id']);
        $query=$this->db->update(TABLE_PRE.'bookings',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
    function pay_channel($channel_id,$commission){
        $query="UPDATE `".TABLE_PRE."channel` SET `channel_commission_payed` = `channel_commission_payed` + '".$commission."' WHERE `channel_id`='".$channel_id."' ";
        $result=$this->db->query($query);
        if($result)
        {
            return true;
        }
        else{
            return false;
        }

    }

    function  rule_create($data){

        $query=$this->db->insert(TABLE_PRE.'service_rule',$data);
        if($query){
            return $this->db->insert_id();

        }else{
            return 'Some Database Error Occurred';
        }

       // return 'ok';

    }

    function  insert_service($data){

        $query=$this->db->insert(TABLE_PRE.'service',$data);
        if($query){
            return $this->db->insert_id();

        }else{
            return 'Some Database Error Occurred';
        }

        // return 'ok';

    }

    function add_asset($asset){
        $query=$this->db->insert(TABLE_PRE.'assets',$asset);
        if($query){
            return true;
        }else{
            return false;
        }
    }
	
	function update_asset_type($asset_type){
		$id=$asset_type['asset_type_id'];
		$this->db->where('asset_type_id',$id);
		$query=$this->db->update(TABLE_PRE.'asset_type',$asset_type);
		if($query){
			 return true;
		}
	}
	function view_assets($id){
		 $this->db->where('a_id',$id);
        $query = $this->db->get(TABLE_PRE.'asset');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	
	
    function all_assets_limit(){
        $this->db->order_by('a_id','desc');
        $this->db->where('a_hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'assets');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	function fetch_asset_by_id($id){
		$this->db->where('hotel_stock_inventory_id',$id);
		$query=$this->db->get(TABLE_PRE.'stock_inventory');
		if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}	
	function fetch_assets_by_id($id){
		
			
	}
	function update_stock_invent($asset){
		$this->db->where('hotel_stock_inventory_id',$asset['hotel_stock_inventory_id']);
		$query=$this->db->update(TABLE_PRE.'stock_inventory',$asset);
		if($query){
             return true;
         }else{
             return false;
         }
	}
	
	function delete_assets($id){
		$this->db->where('a_id',$id);
         $query=$this->db->delete(TABLE_PRE.'assets');
         if($query){
             return true;
         }else{
             return false;
         }
		
	}
    function all_services(){
       // $this->db->order_by('a_id','desc');
       // $this->db->where('a_hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'service');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
    function  service_booking_match($booking_id,$service_rule){

        if ($service_rule == '' || $service_rule == NULL) {
            $sql = "SELECT * FROM `hotel_bookings` LEFT OUTER JOIN `hotel_guest` ON `hotel_bookings`.`guest_id`=`hotel_guest`.`g_id`  LEFT OUTER JOIN `hotel_room` ON `hotel_bookings`.`room_id`= `hotel_room`.`room_id`
            WHERE booking_id=".$booking_id." ";
        }
        else{
             $sql = "SELECT * FROM `hotel_bookings` LEFT OUTER JOIN `hotel_guest` ON `hotel_bookings`.`guest_id`=`hotel_guest`.`g_id`  LEFT OUTER JOIN `hotel_room` ON `hotel_bookings`.`room_id`= `hotel_room`.`room_id`
            WHERE booking_id=".$booking_id." AND ".$service_rule." ";
        }

        $query=$this->db->query($sql);
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

     function  room_feature_all($room_id){

         $sql = "SELECT * FROM `hotel_room_room_features` LEFT OUTER JOIN `hotel_room_features` ON `hotel_room_room_features`.`room_feature_id`=`hotel_room_features`.`room_feature_id` WHERE room_id=".$room_id."  ";

         $query=$this->db->query($sql);
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }

    function  add_service_to_booking_db($data){

        $sql1="UPDATE `hotel_bookings` SET service_price = service_price +'".$data['service_price']."'    WHERE booking_id ='".$data['booking_id']."' ";
        $sql2="UPDATE `hotel_bookings` SET  service_id=concat(service_id,'".$data['service_id']."')  WHERE booking_id ='".$data['booking_id']."' ";


        /* $this->db->where('booking_id',$data['booking_id']);*/
        $query1=$this->db->query($sql1);
        $query2=$this->db->query($sql2);
        if($query1 && $query2){
            return 'Service Added Successfully';
        }else{
            return 'DB Error';
        }

    }
    function  add_charge_to_booking_db($data){

        $sql1="UPDATE `hotel_bookings` SET booking_extra_charge_amount = booking_extra_charge_amount +'".$data['booking_extra_charge_amount']."'    WHERE booking_id ='".$data['booking_id']."' ";
        $sql2="UPDATE `hotel_bookings` SET  booking_extra_charge_id=concat(booking_extra_charge_id,'".$data['booking_extra_charge_id']."')  WHERE booking_id ='".$data['booking_id']."' ";


        /* $this->db->where('booking_id',$data['booking_id']);*/
        $query1=$this->db->query($sql1);
        $query2=$this->db->query($sql2);
        if($query1 && $query2){
            return 'Charge Added Successfully';
        }else{
            return 'DB Error';
        }

    }

    function  add_charge_to_booking_db_group($data){

        $sql1="UPDATE `hotel_group` SET charges_cost = charges_cost +'".$data['booking_extra_charge_amount']."'    WHERE id ='".$data['booking_id']."' ";
        $sql2="UPDATE `hotel_group` SET  charges_id=concat(charges_id,'".$data['booking_extra_charge_id']."')  WHERE id ='".$data['booking_id']."' ";


        /* $this->db->where('booking_id',$data['booking_id']);*/
        $query1=$this->db->query($sql1);
        $query2=$this->db->query($sql2);
        if($query1 && $query2){
            return 'Charge Added Successfully';
        }else{
            return 'DB Error';
        }

    }

    function add_asset_type($asset_type){
        $query=$this->db->insert(TABLE_PRE.'asset_type',$asset_type);
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function all_assets_tasks(){
        $this->db->order_by('asset_type_id','desc');
        $query=$this->db->get(TABLE_PRE.'asset_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function all_assets_types(){
        $this->db->order_by('asset_type_id','desc');
        $query = $this->db->get(TABLE_PRE.'asset_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	function all_assets_category(){
		 $this->db->order_by('asset_category_id','desc');
        $query = $this->db->get(TABLE_PRE.'asset_category');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	function fetch_type_by_id($id){
		$this->db->where('asset_type_id',$id);
        $query = $this->db->get(TABLE_PRE.'asset_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	
	function delete_a_type($id){
		 $this->db->where('asset_type_id',$id);
         $query=$this->db->delete(TABLE_PRE.'asset_type');
         if($query){
             return true;
         }else{
             return false;
         }
	} 
	
	function all_maid(){
		$this->db->order_by('maid_id','desc');
        //$this->db->where('type','Maid');
        $this->db->where('staff_availability','Available');
        $query = $this->db->get(TABLE_PRE.'housekeeping_maid');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
		
	}

    function  add_maid($data){
        $query=$this->db->insert(TABLE_PRE.'housekeeping_maid',$data);
        if($query){
            return true;
        }else{
            return false;
        }



    }

    function  assign_maid($data){
        $query=$this->db->insert(TABLE_PRE.'housekeeping_matrix',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function assigned_maid_update($rum_id,$data){
        $this->db->where('room_id=',$rum_id);
        $query=$this->db->update(TABLE_PRE.'housekeeping_matrix',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function check_if_room_ok($rum_id){
        $this->db->where('room_id',$rum_id);
        $query = $this->db->get(TABLE_PRE.'housekeeping_matrix');
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    function all_maids(){
       // $this->db->order_by('asset_type_id','desc');
		$this->db->where('type','Maid');
		//$this->db->where('maid_status','0');
		$this->db->where('staff_availability','Available');
        $query = $this->db->get(TABLE_PRE.'housekeeping_maid');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	function all_auditor(){
		$this->db->where('type','Auditor');
        $query = $this->db->get(TABLE_PRE.'housekeeping_maid');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
     function all_maids_shuffle(){
         $this->db->order_by('maid_id', 'RANDOM');
         // $this->db->order_by('asset_type_id','desc');
         $query = $this->db->get(TABLE_PRE.'housekeeping_maid');
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }
    function all_maids_limit(){
        $this->db->where('maid_status =',0);


        $query = $this->db->get(TABLE_PRE.'housekeeping_maid');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
    function update_maid($maid){

        $this->db->where('maid_id=',$maid['maid_id']);
        $query=$this->db->update(TABLE_PRE.'housekeeping_maid',$maid);
        if($query){
            return true;
        }else{
            return false;
        }
    }

     function get_hotel_contact_details($hotel_id){
         $this->db->where('hotel_id',$hotel_id);
         $query = $this->db->get(TABLE_PRE.'hotel_contact_details');
         if($query->num_rows()>0){
             return $query->row_array();
         }
         else{
             return false;
         }

     }
     function get_transaction_details($book_id)
     {
         $this->db->where('t_booking_id',$book_id);
         $query = $this->db->get(TABLE_PRE.'transactions');

         if($query->num_rows()>0){
             return $query->result();
         }
         else{
             return false;
         }
     }
     function get_payment_details($book_id)
     {
         $this->db->select('*');
         $this->db->from(TABLE_PRE.'room');
         $this->db->join(TABLE_PRE.'bookings', 'hotel_room.room_id = hotel_bookings.room_id');
         $this->db->where('booking_id',$book_id);
         $query = $this->db->get();

         if($query->num_rows()>0){
             return $query->result();
         }
         else{
             return false;
         }
     }

     function get_total_payment($booking_id)
     {

         $this->db->where('t_booking_id',$booking_id);
         $this->db->select_sum('t_amount');
         $query = $this->db->get(TABLE_PRE.'transactions');

         if($query->num_rows()>0){
             return $query->result();
         }
         else{
             return false;
         }
     }

     function  room_maid_match($room_id){

         $sql = "SELECT * FROM `hotel_housekeeping_matrix` LEFT OUTER JOIN `hotel_housekeeping_maid` ON `hotel_housekeeping_matrix`.`maid_id`=`hotel_housekeeping_maid`.`maid_id`  WHERE room_id=".$room_id." AND `status`=0 GROUP BY `room_id`, `hotel_housekeeping_matrix`.`maid_id` ";

         $query=$this->db->query($sql);
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return false;
         }
     }

     function  room_maid_match_row($room_id){

         $sql = "SELECT * FROM `hotel_housekeeping_matrix` LEFT OUTER JOIN `hotel_housekeeping_maid` ON `hotel_housekeeping_matrix`.`maid_id`=`hotel_housekeeping_maid`.`maid_id`  WHERE room_id=".$room_id." AND `status`=0 GROUP BY `room_id`, `hotel_housekeeping_matrix`.`maid_id` ";

         $query=$this->db->query($sql);
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }
     }

    function add_gi_details($stay,$booking_id){
        $this->db->where('booking_id', $booking_id);
        $query = $this->db->update(TABLE_PRE.'bookings', $stay);
        //echo  $this->db->last_query();
        //exit;
        if($query){
            return true;
        }else{
            return false;
        }
        
    }

    function all_service_result(){
        $this->db->order_by('s_id','desc');
        $query=$this->db->get(TABLE_PRE.'service');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

     function get_bookings_on_date($date){
         $sql="select * from hotel_bookings where cust_from_date <= '".$date."' and cust_end_date >= '".$date."' ";
         //$this->db->select('booking_status_id','booking_id');
         //$this->db->where('cust_from_date <=', $date);
         //$this->db->where('cust_end_date >=', $date);
         //$query=$this->db->get(TABLE_PRE.'bookings');
         $query=$this->db->query($sql);
         if($query->num_rows()>0){
             return $query->result();
         }else{

             return $query->result();

         }
     }


     function bookings_by_date($date){


         $sql="select * from hotel_bookings where booking_taking_date= '".$date."' and booking_status_id !='7'  ";

         #and booking_status_id !='7'and booking_status_id !='6' and booking_status_id_secondary !='3' and booking_status_id !='8' 
         
         $query= $this->db->query($sql);

         return $query->num_rows();
     }

     function  get_hotel_pincode(){

         $sql="select hotel_pincode from hotel_hotel_contact_details where hotel_id='".$this->session->userdata('user_hotel')."'";

         $query= $this->db->query($sql);

         return $query->result();
     }


     function add_purchase($array,$arrVal){
        $query=$this->db->insert(TABLE_PRE.'purchase',$array);
        /*return $id= $this->db->insert_id();
        $arrVal['purchase_id'] = $id;*/
        $query1=$this->db->insert(TABLE_PRE.'transactions',$arrVal);
        if($query1){
            return true;
        }else{
            return false;
        }
    }

	function get_last_pur_id()
	{
	$last_row=$this->db->select('id')->order_by('id',"desc")->limit(1)->get(TABLE_PRE.'purchase')->row();
		return $last_row->id;
	}
	
	
    function add_purchase_pos($array){
        $query=$this->db->insert(TABLE_PRE.'purchase',$array);
    
        if($query){
            return true;
        }else{
            return false;
        }
    }

     function  get_shift_log($date,$time_start,$time_end){
         $sql="SELECT * FROM `hotel_bookings` WHERE booking_taking_date= '".date("Y-m-d",strtotime($date))."' and booking_taking_time >= '".$time_start."' and booking_taking_time <= '".$time_end."'";
         $query=$this->db->query($sql);
         if($query){
             return $query->result();
         }else{
             return false;
         }
     }

     function  get_shift_log_endless($date,$time_start){
         $sql="SELECT * FROM `hotel_bookings` WHERE booking_taking_date= '".date("Y-m-d",strtotime($date))."' and booking_taking_time >= '".$time_start."' ";
         $query=$this->db->query($sql);
         if($query){
             return $query->result();
         }else{
             return false;
         }
     }

     function  previous_shift_id($user_id){

         $sql="select log_id from shift_log where logged_out='0' and user_id='".$user_id."'  ";

         $query=$this->db->query($sql);
         if($query){
             return $query->result();
         }else{
             return false;
         }
     }

     function get_floor(){


         $sql="select hotel_floor from hotel_hotel_details where hotel_id='".$this->session->userdata('user_hotel')."'";
         $query=$this->db->query($sql);
         if($query){
             return $query->result();
         }else{
             return false;
         }
     }
     function  add_dgset($data){
        $query=$this->db->insert(TABLE_PRE.'dgset',$data);
        if($query){
            return true;
        }else{
            return false;
        }




    }
     function all_dgset(){
        $this->db->order_by('dgset_id','asc');
        //$this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'dgset');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
	
	
     function delete_dgset($id){
         $this->db->where('dgset_id',$id);
         $query=$this->db->delete(TABLE_PRE.'dgset');
         if($query){
             return true;
         }else{
             return false;
         }
     }

function all_dgset_limit_view()
    {
        $this->db->order_by('dgset_id','desc');
        //$this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'dgset');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }


 }
 function fetch_dgset($id){
     $this->db->where('dgset_id',$id);
     $this->db->select('*');
     $query=$this->db->get(TABLE_PRE.'dgset');
     
         if($query){
             return $query->row();
         }else{
             return false;
         }
 }
 function update_dgset($dgset)
 {
        $dgset_id=$dgset['dgset_id'];
        $this->db->where_in('dgset_id', $dgset_id);        
        $query_details = $this->db->update(TABLE_PRE.'dgset',$dgset); 
        if($query_details){
            return true;
        }
        else{
            return false;
        }
    }
    function fetch_gen($id)
    {
	//PASS BLANK '' TO GET ALL THE DG SET AS RESULT	
        $this->db->select('*');
		if($id=='active')
		{
        $query=$this->db->get_where(TABLE_PRE.'dgset',"status='".$id."'");
		}
		else
		{
		 $query=$this->db->get(TABLE_PRE.'dgset');	
		}
        if($query->num_rows()>0)
        {
            return $query->result();
        }else
        {
            return false;
        }
    }
    function fetch_user()
    {   $id=0;
        $this->db->where_in('admin_booking_id',$id);
        $this->db->select('*');
        $query=$this->db->get(TABLE_PRE.'admin_details');
        if($query->num_rows()>0)
        {
            return $query->result();
        }else
        {
            return false;
        }
    }
    function  add_usage_log($data)
    {
        $query=$this->db->insert(TABLE_PRE.'usage_log',$data);
		$usage_log_id=$this->db->insert_id();
        if($query){
            return $usage_log_id;
        }else{
            return false;
        }

    }
    function all_usage_log()
    {
         $this->db->order_by('usage_id','asc');
        //$this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'usage_log');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
    function delete_usage_log($id)
    {
         $this->db->where('usage_id',$id);
         $query=$this->db->delete(TABLE_PRE.'usage_log');
         if($query){
             return true;
         }else{
             return false;
         } 
    }
    function fetch_purchase_id(){

         $this->db->select('*');
         $query=$this->db->get(TABLE_PRE.'purchase');
         if($query->num_rows()>0){
             
             return $query->result();
         }else {
			 return false;
		 }

 }
    function  add_sub_fuel($add_sub){
        $query=$this->db->insert(TABLE_PRE.'add_sub_fuel_log',$add_sub);
		$l_id=$this->db->insert_id();
        if($query){
		    return $l_id;
        }else{
            return false;
        }
    }


	function add_payments($add_pay,$arrVal){
		$query=$this->db->insert(TABLE_PRE.'payments',$add_pay);
        $id= $this->db->insert_id();
        $arrVal['payment_id'] = $id;
		$arrVal['details_id'] = $id;
        $query1=$this->db->insert(TABLE_PRE.'transactions',$arrVal);
		if($query1){
		    return true;
        }else{
            return false;
        }
	}





    function all_payments(){
        // $top="Salary/WagePayments";
         $this->db->select('*');
         //$this->db->where('type_of_payment',$top);
         $query=$this->db->get(TABLE_PRE.'payments');//print_r($query);  die;
         if($query->num_rows()>0){
             return $query->result();
         } else {
             return false;
         }
    }


	function all_salpayments(){	
		$top="Salary/Wage Payments";
		 $this->db->select('*');
		 $this->db->where('type_of_payment',$top);
         $query=$this->db->get(TABLE_PRE.'payments');//print_r($query);  die;
         if($query->num_rows()>0){
             return $query->result();
         }else {
			 return false;
		 }
	}

    function delete_salpayments($id){ 
        $this->db->where('hotel_payment_id',$id);
         $query=$this->db->delete(TABLE_PRE.'payments');
         if($query){
             return true;
         }else{
             return false;
         }
    }


	function all_billpayments()
	{	
		$top="Bill payments";
		 $this->db->select('*');
		 $this->db->where('type_of_payment',$top);
         $query=$this->db->get(TABLE_PRE.'payments');//print_r($query);  die;
         if($query->num_rows()>0){
             
             return $query->result();
         }else {
			 return false;
		 }
	}

    function delete_billpayments($id){ 
        $this->db->where('hotel_payment_id',$id);
         $query=$this->db->delete(TABLE_PRE.'payments');
         if($query){
             return true;
         }else{
             return false;
         }

 }
	function all_mispayments()
	{	
		$top="Miscellaneous Payments";
		 $this->db->select('*');
		 $this->db->where('type_of_payment',$top);
         $query=$this->db->get(TABLE_PRE.'payments');//print_r($query);  die;
         if($query->num_rows()>0){
             
             return $query->result();
         }else {
			 return false;
		 }
	}

    function delete_mispayments($id){ 
        $this->db->where('hotel_payment_id',$id);
         $query=$this->db->delete(TABLE_PRE.'payments');
         if($query){
             return true;
         }else{
             return false;
         }

 }
    function all_taxpayments()
    {   
        $top="Tax/Compliance Payment";
         $this->db->select('*');
         $this->db->where('type_of_payment',$top);
         $query=$this->db->get(TABLE_PRE.'payments');//print_r($query);  die;
         if($query->num_rows()>0){
             
             return $query->result();
         }else {
             return false;
         }
    }

    function all_expense_report()
    {   
        
         $this->db->select('*');
         $this->db->where('transaction_releted_t_id','4');
         $this->db->or_where('transaction_releted_t_id','7');
         $this->db->or_where('transaction_releted_t_id','5');
         $this->db->or_where('transaction_releted_t_id','12');
         $this->db->or_where('transaction_from_id','4');
         $this->db->or_where('transaction_to_id','5');
         $this->db->or_where('transaction_to_id','7');
          $this->db->or_where('transaction_to_id','6');
           $this->db->or_where('transaction_to_id','11');
         $query=$this->db->get(TABLE_PRE.'transactions');//print_r($query);  die;
         if($query->num_rows()>0){
             
             return $query->result();
         }else {
             return false;
         }
    }


    function get_payment_id()
    {   
            $sql=$this->db->insert_id('payments');
            //print_r($sql);  die;
            $this->db->where('hotel_payment_id',$sql);
            $this->db->select('hotel_payment_id');
         
         $query=$this->db->get(TABLE_PRE.'payments');//print_r($query);  die;
         if($query->num_rows()>0){
             
             return $query->result();
         }else {
             return false;
         }
    }
    function filter_expense_report($top,$fDate,$tDate)
    {  

    //echo $top;
    //exit; 
        if($top=="Bill Payments"){
             $this->db->select('*');
             $this->db->where('transaction_releted_t_id','5');         
             $this->db->where('transaction_from_id','4');
             $this->db->where('transaction_to_id','6');
             $query=$this->db->get(TABLE_PRE.'transactions');
             //print_r($query);  die;
             if($query->num_rows()>0){        
                 return $query->result();
             }else {
                 return false;
             }
        } else if($top=="Salary/Wage Payments"){
         $this->db->select('*');
         $this->db->where('transaction_releted_t_id','7');         
         $this->db->where('transaction_from_id','4');
         $this->db->where('transaction_to_id','7');
         
         $query=$this->db->get(TABLE_PRE.'transactions');//print_r($query);  die;
         if($query->num_rows()>0){
             
             return $query->result();
         }else {
             return false;
         }
    }else if($top=="Miscellaneous Payments"){
         $this->db->select('*');
         $this->db->where('transaction_releted_t_id','12');         
         $this->db->where('transaction_from_id','4');
         $this->db->where('transaction_to_id','11');
         
         $query=$this->db->get(TABLE_PRE.'transactions');//print_r($query);  die;
         if($query->num_rows()>0){
             
             return $query->result();
         }else {
             return false;
         }
    }
    else if($top=="Tax/Compliance Payment"){
         $this->db->select('*');
         $this->db->where('transaction_releted_t_id','4');         
         $this->db->where('transaction_from_id','4');
         $this->db->where('transaction_to_id','5');
         
         $query=$this->db->get(TABLE_PRE.'transactions');//print_r($query);  die;
         if($query->num_rows()>0){
             
             return $query->result();
         }else {
             return false;
         }
        }else if($top=="Purchase"){
             $this->db->select('*');
             $this->db->where('transaction_releted_t_id','5');         
             $this->db->where('transaction_from_id','4');
             $this->db->where('transaction_to_id','5');
         
         $query=$this->db->get(TABLE_PRE.'transactions');//print_r($query);  die;
         if($query->num_rows()>0){             
             return $query->result();
         }else {
             return false;
         }
    } else{
      $this->db->select('*');
         $this->db->where('transaction_releted_t_id','4');
         $this->db->or_where('transaction_releted_t_id','7');
         $this->db->or_where('transaction_releted_t_id','5');
         $this->db->or_where('transaction_releted_t_id','12');
         $this->db->or_where('transaction_from_id','4');
         $this->db->or_where('transaction_to_id','5');
         $this->db->or_where('transaction_to_id','7');
          $this->db->or_where('transaction_to_id','6');
           $this->db->or_where('transaction_to_id','11');
         $query=$this->db->get(TABLE_PRE.'transactions');//print_r($query);  die;
         if($query->num_rows()>0){
             
             return $query->result();
         }else {
             return false;
         }
    
}
}
function roomcount($dt){

        $this->db->select('*');
        $this->db->like('cust_from_date',$dt);

         $query=$this->db->get(TABLE_PRE.'bookings');
         // print_r($query);
         // die;
         if($query->num_rows()>0){           
             return $query->num_rows();
         }else{
            return 0;
         }

 }
 
 function totalrooms(){

         $this->db->select('room_no');
         $query=$this->db->get(TABLE_PRE.'room');
         // print_r($query);
         // die;
         if($query->num_rows()>0){           
             return $query->result();
         }else{
            return false;
         }

 }


 function avgdailyrent($dt){

         $this->db->select('*');
         $this->db->like('cust_from_date',$dt);
         $query=$this->db->get(TABLE_PRE.'bookings');
         // print_r($query);
         // die;
         if($query->num_rows()>0){           
             return $query->result();
         }else{
            return false;
         }

 }


    
function add_lost_item($lost_item){
        $query=$this->db->insert(TABLE_PRE.'lost_item',$lost_item);
        if($query){
            return true;
        }
        
     }

 function fetch_lost_item(){
    $this->db->order_by('l_id','asc');
        //$this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'lost_item');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
 }
 function fetch_l_id($id){

         $this->db->where('l_id',$id);
         $query=$this->db->get(TABLE_PRE.'lost_item');
         if($query->num_rows()>0){
             
             return $query->result();
         }

 }
 function update_lost_item($lost_item){
    $this->db->where('l_id',$lost_item['l_id']);
        $query=$this->db->update(TABLE_PRE.'lost_item',$lost_item);
        if($query){
            return true;
        }else{
            return false;
        }

 }
 function delete_l_item($id){ 
        $this->db->where('l_id',$id);
         $query=$this->db->delete(TABLE_PRE.'lost_item');
         if($query){
             return true;
         }else{
             return false;
         }

 }
     
function add_found_item($found_item){

    $query=$this->db->insert(TABLE_PRE.'found_item',$found_item);
        if($query){
            return true;
        }
}

    function fetch_admin(){
        $this->db->order_by('admin_id','asc');
        //$this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'admin_details');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

function fetch_found_item(){

    $this->db->order_by('f_id','asc');
        //$this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'found_item');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
}

 function fetch_f_item($item){
	 $array = array('type' => $item,'status' => 'found');
	$this->db->where($array);
         $query=$this->db->get(TABLE_PRE.'found_item');
         if($query->num_rows()>0){
             
             return $query->result_array();
         }
	 
 }
 
function fetch_lost_items($item){
	$array = array('type' => $item,'status' => 'lost');
	$this->db->where($array);
         $query=$this->db->get(TABLE_PRE.'lost_item');
         if($query->num_rows()>0){
             
             return $query->result_array();
         }
	
}


function fetch_f_id($f_id){

         $this->db->where('f_id',$f_id);
         $query=$this->db->get(TABLE_PRE.'found_item');
         if($query->num_rows()>0){
             
             return $query->result();
         }
    
}

function update_release_item($r_item){
	$this->db->where('hotel_release_item_id',$r_item['hotel_release_item_id']);
        $query=$this->db->update(TABLE_PRE.'release_item',$r_item);
        if($query){
            return true;
        }else{
            return false;
        }
	
}
 function delete_item($id){
    $this->db->where('f_id',$id);
         $query=$this->db->delete(TABLE_PRE.'found_item');
         if($query){
             return true;
         }else{
             return false;
         }
     }
 function delete_f_item($item){
	  $this->db->where('item_title',$item);
         $query=$this->db->delete(TABLE_PRE.'found_item');
         if($query){
             return true;
         }else{
             return false;
         }
	 
 }
 


function update_found_item($found_item){

        $this->db->where('f_id',$found_item['f_id']);
        $query=$this->db->update(TABLE_PRE.'found_item',$found_item);
        if($query){
            return true;
        }else{
            return false;
        }

}
function release_item($r_item){
	
		$query=$this->db->insert(TABLE_PRE.'release_item',$r_item);
		if($query){
			return true;
		}
		else{
			return false;
		}
}

function fetch_release_item(){
	
	$this->db->order_by('hotel_release_item_id','asc');
        //$this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'release_item');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	
	
}

function fetch_r_id($id){
	 $this->db->where('hotel_release_item_id',$id);
         $query=$this->db->get(TABLE_PRE.'release_item');
         if($query->num_rows()>0){
             
             return $query->result();
         }
	
}
 function delete_r_item($id){
	 $this->db->where('hotel_release_item_id',$id);
         $query=$this->db->delete(TABLE_PRE.'release_item');
         if($query){
             return true;
         }else{
             return false;
         }
	 
 }
function fetch_corporate(){
		$this->db->order_by('hotel_corporate_id','asc');
        //$this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'corporate');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	
}
function update_corporate($corporate){
	   $this->db->where('hotel_corporate_id',$corporate['hotel_corporate_id']);
        $query=$this->db->update(TABLE_PRE.'corporate',$corporate);
        if($query){
            return true;
        }else{
            return false;
        }
	
}
function fetch_c_id($id){
      $this->db->where('hotel_corporate_id',$id);
         $query=$this->db->get(TABLE_PRE.'corporate');
         if($query->num_rows()>0){
             
             return $query->result();
         }
}
function add_corporate($corporate){
     $query=$this->db->insert(TABLE_PRE.'corporate',$corporate);
        if($query){
            return true;
        }
    
} 
function delete_corporate($id){
	$this->db->where('hotel_corporate_id',$id);
         $query=$this->db->delete(TABLE_PRE.'corporate');
         if($query){
             return true;
         }else{
             return false;
         }
	
}
function fetch_e_id($id){
	$this->db->where('e_id',$id);
         $query=$this->db->get(TABLE_PRE.'events');
         if($query->num_rows()>0){
             
             return $query->result();
         }
	
}    
    function update_event($event){

	  $this->db->where('e_id',$event['e_id']);
	  $query=$this->db->update(TABLE_PRE.'events',$event); 

	  if($query){
             return true;
         }else{
             return false;
         }
    }

/* Add Charge */
    function insert_charge($data){
        $query=$this->db->insert(TABLE_PRE.'charges',$data);
        if($query){
            return $this->db->insert_id();
        }else{
            return false;
        }
    } 
//1.04.2016
    function booking_details(){
		$this->db->order_by('b.booking_id','asc');
		$this->db->select('b.*');
		//$this->db->select('g.*');
		$this->db->from('hotel_bookings b');
		//$this->db->join('hotel_guest g','b.guest_id=g.g_id');
		$query = $this->db->get();//echo $this->db->last_query();die;

         if($query->num_rows()>0){
             
             return $query->result();
         }
		 else{
			 return false;
		 }
	
    }
    function booking_details_forguest($cond){
		if(!empty($cond)){
			$this->db->where($cond);
			$this->db->select('b.*');
			$this->db->from('hotel_bookings b');
			$query = $this->db->get();
			 if($query->num_rows()>0){
				 return $query->result();
			 }
			 else{
			 return false;
			 }
		} 
	
    }

    function update_service($data){
        $this->db->where('s_id',$data['s_id']);
        $query=$this->db->update(TABLE_PRE.'service',$data);
        if($query){
            return true;
        }else{
            return false;
        }
    }

	function add_misc_income($m_income,$arrVal){
		 $query=$this->db->insert(TABLE_PRE.'misc_income',$m_income);
        $id= $this->db->insert_id();
        $arrVal['details_id'] = $id;
        $query1=$this->db->insert(TABLE_PRE.'transactions',$arrVal);
		if($query1){
		    return true;
        }else{
            return false;
        }
	
	}
	function fetch_m_income(){
		$this->db->order_by('hotel_misc_income_id','asc');
        //$this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'misc_income');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
		
	}
	
	function fetch_m_income_id($id){
		$this->db->where('hotel_misc_income_id',$id);
        //$this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'misc_income');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
		
	}
	
	function update_misc_income($m_income){
		
		$this->db->where('hotel_misc_income_id',$m_income['hotel_misc_income_id']);
        $query=$this->db->update(TABLE_PRE.'misc_income',$m_income);
        if($query){
            return true;
        }else{
            return false;
        }
		
	}
 
	function delete_m_income($id){
	
		$this->db->where('hotel_misc_income_id',$id);
         $query=$this->db->delete(TABLE_PRE.'misc_income');
         if($query){
             return true;
         }else{
             return false;
         }
	
	
	}
	function delete_e_report($id){
		$this->db->where('t_id',$id);
         $query=$this->db->delete(TABLE_PRE.'transactions');
         if($query){
             return true;
         }else{
             return false;
         }
	}
	
	function get_all_room_name(){
		$hotel_id = $this->session->userdata('user_hotel');
		$this->db->select('room_no');
		$this->db->where('hotel_id',$hotel_id);
		$this->db->group_by('room_no');
        $query=$this->db->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }	
	
	
     /* Add Unit Block */
    function add_unit_block($data){
		$query=$this->db->insert(TABLE_PRE.'unit_block',$data);		
        if($query){
            return true;
        }else{
            return false;
        }
	}	
	
	public  function all_unit_block(){
		$this->db->where('room_status!=','D');
        $this->db->where('hotel_id=',$this->session->userdata('user_hotel'));
        $this->db->join('hotel_unit_type', 'hotel_room.unit_id = hotel_unit_type.id');
        $this->db->group_by('room_no');
        $this->db->order_by('room_no','asc');
		//$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

    function  insert_pos($data){
        $query=$this->db->insert(TABLE_PRE.'pos',$data);
        if($query){
            return $this->db->insert_id();;
        }else{
            return false;
        }

    }
	

    function  insert_pos_item($data){
        $query=$this->db->insert(TABLE_PRE.'pos_items',$data);
        if($query){
            return $this->db->insert_id();;
        }else{
            return false;
        }

    }

    function all_pos($booking_id){
        $sql="select * from hotel_pos where booking_id='".$booking_id."'";
        $query=$this->db->query($sql);
        if($query){
            return $query->result();
        }else{
            return false;
        }

    }

     function all_pos_items($pos_id){
        $sql="select * from hotel_pos_items where pos_id='".$pos_id."'";
        $query=$this->db->query($sql);
        if($query){
            return $query->result();
        }else{
            return false;
        }

    }

	function fetch_r_summary(){
		$this->db->order_by('cust_from_date','asc');
		$this->db->join('hotel_guest','hotel_guest.g_id = hotel_bookings.guest_id');
		 $this->db->join('hotel_room', 'hotel_room.room_id=hotel_bookings.room_id');
		 $this->db->join('hotel_unit_type', 'hotel_room.unit_id=hotel_unit_type.id');
		 $this->db->join('booking_status_type','booking_status_type.status_id=hotel_bookings.booking_status_id');
		 //$this->db->join('hotel_transactions', 'hotel_transactions.t_booking_id=hotel_bookings._id');
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'bookings',TABLE_PRE.'guest',TABLE_PRE.'room',TABLE_PRE.'unit_type',TABLE_PRE.'booking_status_type');
       
	   if($query->num_rows()>0){
            return $query->result();
			
        }else{
            return false;
        }
	
		
	}
	function all_r_by_date($start_date,$end_date){
	    $this->db->order_by('booking_id','asc');
        $this->db->where('cust_from_date_actual >=', $start_date);
        $this->db->where('cust_from_date_actual <=', $end_date);
        
		$query=$this->db->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }	
	}
	
	
	
	 function  get_room_by_id($room_id){
		 $this->db->where('room_id',$room_id);
		 $this->db->join('hotel_unit_type', 'hotel_room.unit_id=hotel_unit_type.id');
		 $query=$this->db->get(TABLE_PRE.'room',TABLE_PRE.'unit_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
		 
	 }

    function all_trasac($booking_id){
        
        
        
    }   

     function get_available_room($date1 ,$date2 ){
       

       $sql="select room_id from hotel_bookings where (cust_from_date_actual between '".$date1."' and '".$date2."' ) or  (cust_end_date_actual between '".$date1."' and '".$date2."' ) or (cust_from_date <='".$date1."' and cust_end_date>='".$date2."') or (cust_from_date >='".$date1."' and cust_end_date <='".$date2."') ";
       $query=$this->db->query($sql);
        if($query->num_rows() > 0 )
        {
            return $query->result();
        }
        else
        {
            return $query->result();
        }
    }

    function remove_amount_pos($pos_id,$amount){
        $sql="update hotel_pos set total_paid=total_paid+'".$amount."'  where pos_id='".$pos_id."'";
         $sql1="update hotel_pos set total_due=total_due-'".$amount."'  where pos_id='".$pos_id."'";
        $query=$this->db->query($sql);
         $query1=$this->db->query($sql1);
        if($query){
            return true;
        }else{
            return false;
        }

    }

    function all_pos_booking($booking_id){
        $sql="select * from hotel_pos where booking_id='".$booking_id."'";
         $query=$this->db->query($sql);
          return $query->result();
        

    }

    function  add_group_booking($data,$data1,$data2){
		$query=$this->db->insert(TABLE_PRE.'group',$data1);
		$insert_id = $this->db->insert_id();

        if($data2['guestID'] =="0"){
        $query=$this->db->insert(TABLE_PRE.'guest',$data2);
          $insert_id_guest = $this->db->insert_id();
      }else{
        $insert_id_guest=$data2['guestID'];
      }
		//print_r($data);
		//exit();
		
		foreach($data['roomID'] as $key => $val){
			if($data['trr'][$key] < 3000){
				$rvat = ($data['trr'][$key]* 5)/100;
			} else {
				$rvat = ($data['trr'][$key]* 10)/100;
			}
			$st = ($data['trr'][$key]* 8.7)/100;
			$sc = ($data['trr'][$key]* 3)/100;
			$arr = array(
			'roomID' => $data['roomID'][$key],
			'type' => $data['type'][$key],
			'roomNO' => $data['roomNO'][$key],
			'gestName' => $data['gestName'][$key],
			'startDate' => $data['startDate'][$key],
			'endDate' => $data['endDate'][$key],
			'days' => $data['days'][$key],
			'mealPlan' => $data['mealPlan'][$key],
			'adultRoomRent' => $data['adultRoomRent'][$key],
			'afi' => $data['afi'][$key],
			'childRoomRent' => $data['childRoomRent'][$key],
			'cfi' => $data['cfi'][$key],
			'adultNO' => $data['adultNO'][$key],
			'childNO' => $data['childNO'][$key],
			'tfi' => $data['tfi'][$key],
			'trr' => $data['trr'][$key],
			'pdr' => $data['pdr'][$key],
			'food_vat' => $data['vat'][$key],
			'room_vat' => $rvat,
			'room_st' => $st,
			'room_sc' => $sc,
			'price' => $data['price'][$key],
			'service' => $data['service'][$key],
			'group_booking_id' => $insert_id
			);	
			//exit;
			$query=$this->db->insert(TABLE_PRE.'groups_bookings_details',$arr);
		}
		//exit;
         //checkin time logic
        $times=$this->dashboard_model->checkin_time();
        foreach($times as $time) {
            if($time->hotel_check_in_time_fr=='PM') {
                $modifier = ($time->hotel_check_in_time_hr + 12);
            }
            else{
                $modifier = ($time->hotel_check_in_time_hr);
            }
        }

        $times2=$this->dashboard_model->checkout_time();
        foreach($times2 as $time2) {
            if($time->hotel_check_in_time_fr=='PM') {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12);
            }
            else{
                $modifier2 = ($time->hotel_check_out_time_hr);
            }
        }


        foreach($data['roomID'] as $key => $val){

            //tax calculation
            if($data['trr'][$key] < 3000){
                $rvat = ($data['trr'][$key]* 5)/100;
            } else {
                $rvat = ($data['trr'][$key]* 10)/100;
            }
            $st = ($data['trr'][$key]* 8.7)/100;
            $sc = ($data['trr'][$key]* 3)/100;

            $tax=$rvat+$st+$sc;


            $arr1 = array(
                'user_id' => $this->session->userdata("user_id"),
                'hotel_id' => $this->session->userdata("user_hotel"),
            'room_id' => $data['roomID'][$key],
            'guest_id' => $insert_id_guest,
             'no_of_adult' => $data['adultNO'][$key],
            'no_of_child' => $data['childNO'][$key],
            'no_of_guest' => $data['adultNO'][$key]+$data['childNO'][$key],
            'booking_source' =>"frontdesk",
            'cust_name' =>$data['gestName'][$key],

             'cust_from_date' =>date('Y-m-d H:i:s', strtotime($data['startDate'][$key])), 
            'cust_end_date' => date('Y-m-d H:i:s', strtotime($data['endDate'][$key])), 
            'cust_from_date_actual' => date('Y-m-d',strtotime($data['startDate'][$key])),
            'cust_end_date_actual' =>date('Y-m-d',strtotime($data['endDate'][$key])),
            'checkin_time' => $modifier,
            'confirmed_checkin_time' => $modifier,
            'checkin_time_actual' => $modifier,
            'checkout_time' => $modifier2,
            'confirmed_checkout_time' => $modifier2,
            'base_room_rent' => $data['pdr'][$key],

            'booking_status_id' => 4,
             'booking_status_id_previous' => 4,

             'room_rent_total_amount' => $data['price'][$key],
             'room_rent_tax_amount' => 0,
             'room_rent_sum_total' => $data['price'][$key],
             'service_price' => $data['service'][$key],

             'stay_days' =>$data['days'][$key],
             'stay_days_updated' =>$data['days'][$key],
             'booking_taking_time' => date("H:i:s"),
             'booking_taking_date' =>date("Y-m-d"),

              'group_id' => $insert_id



            
            //'mealPlan' => $data['mealPlan'][$key],
            //'adultRoomRent' => $data['adultRoomRent'][$key],
            //'afi' => $data['afi'][$key],
            //'childRoomRent' => $data['childRoomRent'][$key],
            //'cfi' => $data['cfi'][$key],
           
            //'pdr' => $data['pdr'][$key],
            //'price' => $data['price'][$key],
            
           
            );  
            //exit;
            $query=$this->db->insert(TABLE_PRE.'bookings',$arr1);
        }
		$as =array();
		$ctot=0;
		foreach($data['cname'] as $key => $val){
			$arr3 = array(
			'crg_description' => $data['cname'][$key],
			'crg_quantity' => $data['qty'][$key],
			'crg_unit_price' => $data['up'][$key],
			//'crg_tax' => $data['ctax'][$key],
			'crg_total' => $data['total'][$key]
			);	
			//exit;
			$query=$this->db->insert(TABLE_PRE.'charges',$arr3);
			array_push($as,$this->db->insert_id());
			$ctot = $ctot + $data['total'][$key];
		}
		
		//print_r($as);
		//echo $ctot;
		$cid = implode(",",$as);
		$arr2 = array(
			'charges_id' => $cid,
			'charges_cost' => $ctot
			);
        $this->db->where('id',$insert_id);
        $query=$this->db->update(TABLE_PRE.'group',$arr2);			
		//exit;
        if($query){
            return $insert_id;
        }else{
            return false;
        }

    }



function delete_purchase($id){
	$this->db->where('id',$id);
         $query=$this->db->delete(TABLE_PRE.'purchase');
         if($query){
             return true;
         }else{
             return false;
         }
	
	
}

     function  getPurchaseById($id){
         $this->db->where('id',$id);
         $query=$this->db->get(TABLE_PRE.'purchase');
         if($query->num_rows()>0){
             return $query->row();
         }else{
             return false;
         }
     }




    function get_group_details($grp){
        $sql="select * from hotel_groups_bookings_details where group_booking_id='".$grp."'";
        $query=$this->db->query($sql);
        return $query->result();
    }

       function get_group($grp){
        $sql="select * from hotel_group where id='".$grp."'";
        $query=$this->db->query($sql);
        return $query->result();
    }

    function get_booking_details_grp($grp){
          $sql="select * from hotel_bookings left join hotel_room on hotel_bookings.room_id=hotel_room.room_id where group_id='".$grp."'";
        $query=$this->db->query($sql);
        return $query->result();
    }

        function all_transaction_group($id){
        $this->db->order_by('t_id','desc');
        $this->db->where('t_group_id=',$id);
        //$this->db->limit($limit,$start);
        $query=$this->db->get(TABLE_PRE.'transactions');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

     function all_group(){
        $sql="select * from hotel_group ";
        $query=$this->db->query($sql);
        return $query->result();
    }


	function update_purchase($a,$b,$id){
		//print_r($a);
        //exit;
		$this->db->where('id',$id);
        $query=$this->db->update(TABLE_PRE.'purchase',$a);
        if($query){
            return true;
        }else{
            return false;
        }
		
	}

	
	function all_guest_by_date($start_date,$end_date){
		$this->db->distinct();
		$this->db->select('group_id');
		$this->db->order_by('booking_id','asc');
        $this->db->where('cust_from_date>=', $start_date);
        $this->db->where('cust_from_date <=', $end_date);
		$query=$this->db->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
		
	}
	
	function get_grp_details($grp_id){
		//$this->db->order_by('group_id',$grp_id);
        $query=$this->db->get(TABLE_PRE.'group');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	
	function all_groups(){
		$this->db->select('*');
		 $query=$this->db->get(TABLE_PRE.'groups_bookings_details');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
		
	}
	function get_group_detalis($group_id){
		 $sql="select * from hotel_groups_bookings_details where id='".$group_id."'";
        $query=$this->db->query($sql);
        return $query->result();
		
	}


	function get_room_feature_charge($rid,$bid){
		$this->db->select('room_feature_charge');
        $this->db->where('room_id=',$bid);
		$this->db->where('room_feature_id=',$rid);
        $query=$this->db->get(TABLE_PRE.'room_room_features');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }

        function get_group_id($rid){
        $this->db->select('group_id');
        $this->db->where('booking_id',$rid);
       
        $query=$this->db->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }

	function getAdditionalDiscount($a,$b){
		$arr = array(
			'additional_discount' => $a
			);
		$this->db->where('id',$b);
        $query=$this->db->update(TABLE_PRE.'group',$arr);
        if($query){
            return true;
        }else{
            return false;
        }
	}


		
		function getUnitDetail($id){
		$wh=array('id'=>$id );
		$this->db->select('*')->from(TABLE_PRE.'unit_type')->where($wh);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->row();
}else{
            return false;
        }
    }

    function get_profit_center(){
        $query = $this->db->get(TABLE_PRE.'profit_center');
        if($query->num_rows()>0){
            return $query->result();

        }else{
            return false;
        }
    }

	
        function updateUnit($unit){
			  $this->db->where('id',$unit['id']);
			$query=$this->db->update(TABLE_PRE.'unit_type',$unit);
        if($query){
            return true;
        }else{
            return false;
        }
			
		}


    function insert_finance($setup){
        $query = $this->db->insert(TABLE_PRE.'finance',$setup);
        if($query){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

	
    function get_setting($hotel_id){
        $this->db->where('fin_hotel_id',$hotel_id);
        $query = $this->db->get(TABLE_PRE.'finance');
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    function update_finance($setup){
        $this->db->where('fin_hotel_id',$this->session->userdata('user_hotel'));
        $query=$this->db->update(TABLE_PRE.'finance',$setup);
        if($query){
            return true;
        }else{
            return false;
        }
    }


	
	
	   function fetch_gen_by_id($id)
    {	
			$wh=array('dgset_id'=>$id);
		$this->db->select('fuel_consumption,fuel')->from(TABLE_PRE.'dgset')->where($wh);
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->row();
        }else
        {
            return false;
        }
    }
	
	
	function add_stock_invent($asset){
		$query = $this->db->insert(TABLE_PRE.'stock_inventory',$asset);
        if($query){
            return true;
        }else{
            return false;
        }
		
	}
	function new_stock($item,$arrVal){
	//	print_r($item);
		//exit;
		$query = $this->db->insert(TABLE_PRE.'add_sub_stock_inventory',$item);
		if($item['operation']=='add'){
			$query1="UPDATE `hotel_stock_inventory` SET `qty` =qty + '".$item["qty"]."',`closing_qty` =opening_qty + '".$item["qty"]."'  WHERE `a_category` ='".$item["a_category"]."' AND `item_type` ='".$item["a_type"]."' AND `a_name` = '".$item["item"]."'";
			$id= $this->db->insert_id();
			$arrVal['details_id'] = $id;
            $query2=$this->db->insert(TABLE_PRE.'transactions',$arrVal);
		} else {
			$query1="UPDATE `hotel_stock_inventory` SET `qty` =qty - '".$item["qty"]."',`closing_qty` =opening_qty - '".$item["qty"]."'  WHERE `a_category` ='".$item["a_category"]."' AND `item_type` ='".$item["a_type"]."' AND `a_name` ='".$item["item"]."'";
		}
       $this->db->query($query1);
	   //echo $query1;
	   //exit;
        if( $query1){
            return true;
        }else{
            return false;
        }
	}
	function get_item_name($cat_name){
		$this->db->select('a_name');
		$this->db->where('a_category',$cat_name);
		$query = $this->db->get(TABLE_PRE.'stock_inventory');
        if($query){
            return $query->result();
        }else{
            return false;
        }
	}
	
	function get_item_price($itm){
		$this->db->select('u_price');
		$this->db->where('a_name',$itm);
		$query = $this->db->get(TABLE_PRE.'stock_inventory');
        if($query){
            return $query->result();
        }else{
            return false;
        }
	}
	
	function all_stock_invent(){
		 $sql="select * from hotel_stock_inventory ";
        $query=$this->db->query($sql);
        return $query->result();
	}
	function all_new_stock_invent(){
		 $sql="select * from hotel_add_sub_stock_inventory ";
        $query=$this->db->query($sql);
        return $query->result();
	}
	
	function delete_stock_invent($id){
		$this->db->where('hotel_stock_inventory_id',$id);
		$query=$this->db->delete(TABLE_PRE.'stock_inventory');
		if($query){
			return true;
		}
		else{
			return false;
		}
	}
	function get_auditor_name($audi_id){
        $this->db->where('maid_id',$audi_id);
        $query = $this->db->get(TABLE_PRE.'housekeeping_maid');
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }
	
	
    function get_all_tax(){
        $query=$this->db->get(TABLE_PRE.'tax');
        if($query){
            return $query->result();
        }else{
            return false;
        }
    }

    function maid_is_available($md_id){
        $this->db->where('maid_id',$md_id);
        $query = $this->db->get(TABLE_PRE.'housekeeping_maid');
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    function get_maid_row($room_id){
        $this->db->select('*');
        $this->db->from(TABLE_PRE.'housekeeping_matrix');
        $this->db->join(TABLE_PRE.'housekeeping_maid', 'hotel_housekeeping_matrix.maid_id = hotel_housekeeping_maid.maid_id');
        $this->db->where('room_id',$room_id);
        $query = $this->db->get();
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    function generate_housekeeping_log_entry($data4){
        $query = $this->db->insert(TABLE_PRE.'housekeeping_logs',$data4);
        if($query){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function generate_housekeeping_log_update($cleared_time,$room_id,$maid_id){
        $this->db->set('room_cleared_on',$cleared_time);
        $this->db->where('room_id',$room_id);
        $this->db->where('maid_id',$maid_id);
        $query=$this->db->update(TABLE_PRE.'housekeeping_logs');
        if($query){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function get_housekeeping_logs(){
        //$query = $this->db->get(TABLE_PRE.'housekeeping_logs');
        $query = $this->db->get(TABLE_PRE.'housekeeping_log_detail');
		if($query){
            return $query->result();
        }else{
            return false;
        }
    }

    function get_maid_by_id($id_val){
        $this->db->where('maid_id',$id_val);
        $query = $this->db->get(TABLE_PRE.'housekeeping_maid');
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    function get_room_by_id_2($id_val){
        $this->db->where('room_id',$id_val);
        $query = $this->db->get(TABLE_PRE.'room');
        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    function delete_maid($maid_id){
        $this->db->where_in('maid_id',$maid_id);
        $query=$this->db->delete(TABLE_PRE.'housekeeping_maid');
        if($query){
            return true;
        }else{
            return false;
        }
    }

    function update_maid_2($data,$id_val){
        $this->db->where('maid_id',$id_val);
        $query = $this->db->update('hotel_housekeeping_maid',$data);
        if($query){
            return $this->db->insert_id();
        }
        else{
            return false;
        }
    }
	
	
	
	function view_all_fuel_log()
	{ 
        $query = $this->db->get(TABLE_PRE.'add_sub_fuel_log');
        if($query){
            return $query->result();
        }else{
            return false;
        }
		
	}
	function delete_fuel_item($a)
	{
		$data = 
		 array(
			 'id'=>$a,
			  );
			  
		 $this->db->delete(TABLE_PRE.'add_sub_fuel_log',$data);
	}

    function add_tax($data){
        $query = $this->db->insert(TABLE_PRE.'tax',$data);
        if($query){
            return $this->db->insert_id();
        }
        else{
            return false;
        }
    }


    function get_booking_extra_charge_adult($id)
    { 
        $this->db->where('room_id',$id);
        $this->db->select('adult_rate');
        $query = $this->db->get(TABLE_PRE.'room');
        if($query){
            return $query->row();
        }else{
            return false;
        }
        
    }


    function get_booking_extra_charge_child($id)
    { 
        $this->db->where('room_id',$id);
        $this->db->select('kid_rate');
        $query = $this->db->get(TABLE_PRE.'room');
        if($query){
            return $query->row();
        }else{
            return false;
        }
        
    }
	
	public function get_master_fuel_stock()
	{
	   $this->db->select('fuel_name,cur_stock')->from(TABLE_PRE.'master_fuel_stock');
	   $query = $this->db->get();
	   if($query){
            return $query->result();
        }else{
            return false;
        }
	   
	}
	
	 function update_master_fuel($up_array)
 {
        $fuel_name=$up_array['fuel_name'];
		$quantity=$up_array['quantity'];
        $this->db->where_in('fuel_name', $fuel_name);    
		if($up_array['in_out_type']=='add')
		{
		$this->db->set('cur_stock', 'cur_stock+'.$quantity, FALSE);
		}
		else
		{
		$this->db->set('cur_stock', 'cur_stock-'.$quantity, FALSE);
	
		}
        $query_details = $this->db->update(TABLE_PRE.'master_fuel_stock'); 
        if($query_details){
            return true;
        }
        else{
            return false;
        }
    }
	function all_note_preference(){
		$this->db->order_by('booking_id','asc');
        $this->db->where('cust_from_date>=', $start_date);
        $this->db->where('cust_from_date <=', $end_date);
		$query=$this->db->get(TABLE_PRE.'bookings');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
	}
	function get_status_by_id($status_id){
			//$this->db->select('booking_status');
			$this->db->where('status_id',$status_id);
        $query = $this->db->get('booking_status_type');
       
		if($query){
            return $query->result();
        }else{
            return false;
        }
	}
	
	
	function add_fuel($data)
	{
    $query=$this->db->insert(TABLE_PRE.'master_fuel_stock',$data);
	if($query)
		return  true;
	else
		return ;
	}
	
	
	
	
	function delete_fuel_log_by_usage_id($id)
	{
		$del_fuel=array ('usage_log_id'=>$id);
		$this->db->delete(TABLE_PRE.'add_sub_fuel_log',$del_fuel);
	}
	
		function get_dg_rating()
		{
			$query = $this->db->get(TABLE_PRE.'dg_rating');
		return $query->result();
		}
	
	function get_fuel_qty_type($id)
	{
		$this->db->select('*');
	    $this->db->where('usage_log_id',$id);
		//$del_fuel=array ('usage_log_id'=>$id);
		$query=$this->db->get(TABLE_PRE.'add_sub_fuel_log');
		 $ret=$query->row();
		if($ret)
		{
			return $ret;
		}	
		else
		{
			$this->db->select('*');
	    $this->db->where('id',$id);
		//$del_fuel=array ('usage_log_id'=>$id);
		$query=$this->db->get(TABLE_PRE.'add_sub_fuel_log');
		return $query->row();
		 
		}
		
		 }
	
	function delete_transaction_by_details_id($id)
	{
		$del_fuel=array ('details_id'=>$id);
		$this->db->delete(TABLE_PRE.'transactions',$del_fuel);
		
	}
	
	
	function getNatureVisit(){
        $this->db->order_by('booking_nature_visit_name','asc');
        $query=$this->db->get(TABLE_PRE.'booking_nature_visit');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }

     function change_booking_status($id,$b_id){
        $sql="update hotel_bookings set booking_status_id='".$id."' where booking_id='".$b_id."'   ";
        $query=$this->db->query($sql);
        return $query;
    }
	
	
	function update_f_item($f_id)
	{
		$f_up=array('status'=>'returned');
		$this->db->where('f_id',$f_id);
		$this->db->update(TABLE_PRE.'found_item',$f_up);
	}
	function update_l_item($l_id)
	{
		$l_up=array('status'=>'recovered');
		$this->db->where('l_id',$l_id);
		$this->db->update(TABLE_PRE.'lost_item',$l_up);
		
	}
	
	
   function  add_food_plan($data1,$data2){
		$query=$this->db->insert(TABLE_PRE.'food_plan',$data1);
		$insert_id = $this->db->insert_id();

		foreach($data2['s_name'] as $key => $val){
			$arr = array(
			's_name' => $data2['s_name'][$key],
			'qty' => $data2['qty'][$key],
			's_unit_cost' => $data2['s_unit_cost'][$key],
			'food_plan_id' => $insert_id
			);	
			//exit;
			$query=$this->db->insert(TABLE_PRE.'food_plan_mapping',$arr);
		}		
		//exit;
        if($query){
            return true;
        }else{
            return false;
        }

    }	
	
	
	function all_food_plans(){
        $this->db->order_by('id','desc');
        $query=$this->db->get(TABLE_PRE.'food_plan');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }	
	
	function add_vendor($data)
	{
		$query=$this->db->insert(TABLE_PRE.'vendor',$data);
		return $query;
	}
	
	function get_parent_vendor()
	{	
		$this->db->where('hotel_vendor_heirc',"n");
		$query=$this->db->get(TABLE_PRE.'vendor');
		return $query->result();
		
	}
	
			function all_vendor()
			{
		$query=$this->db->get(TABLE_PRE.'vendor');
		return $query->result();
			}
			
			function get_unit_type_by_id($id)
			{
				$this->db->select('unit_name');
				$this->db->where('id',$id);
				$query=$this->db->get(TABLE_PRE.'unit_type');
				$res= $query->row();
				return $res->unit_name;
			}
			
			/*function search_booking_by_room_id($id)
			{
				$this->db->select('*');
				$this->db->where('room_id='.$id.'cust_from_date_actual_1 >');
				$query=$this->db->get(TABLE_PRE.'booking');
				$res= $query->num_rows();
				return $res;
			}*/
			
			
	 function add_kit($data1,$data2){
		 $query=$this->db->insert(TABLE_PRE.'define_kit',$data1);
		 $insert_id = $this->db->insert_id();

		foreach($data2['i_name'] as $key => $val){
			$arr = array(
			'item_name' => $data2['i_name'][$key],
			'qty' => $data2['qty'][$key],
			'unit_price' => $data2['unit_price'][$key],
			'qty_for_apply' => $data2['qty_for'][$key],
			'kit_id' => $insert_id
			);	
			//exit;
			$query=$this->db->insert(TABLE_PRE.'kit_mapping',$arr);
			$insert_id1=$this->db->insert_id();
		}		
		//exit;
        if($query){
            return $insert_id1;
        }else{
            return false;
        }
			
		 
	 }


     function serviceCostById($id){
		$this->db->select('s_price');
	    $this->db->where('s_id',$id);
		$query=$this->db->get(TABLE_PRE.'service');
		return $query->row();
     }	



 function get_all_staff_type(){
        $query = $this->db->get(TABLE_PRE.'housekeeping_staff_type');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }
			function get_staff_by_type($a)
			{
				$this->db->where('type',$a);
			$query = $this->db->get(TABLE_PRE.'housekeeping_maid');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }	
			}
	 
//}
		function set_housekeeping_log_entry($a)
		{
			$query=$this->db->insert(TABLE_PRE.'housekeeping_log_detail',$a);
			return $query;
		}
	
		function get_unit_class_list()
		{
			$query=$this->db->get(TABLE_PRE.'unit_class');
			return $query->result();
		}
		
			function get_housekeeping_pending_asso_staff($a,$b)
			{
			$this->db->where("room_id=".$a ." and staff_type='".$b."' and status ='pending'");
			$query=$this->db->get(TABLE_PRE.'housekeeping_log_detail');
			return $query->result();
			}	


            function update_indi($due,$groupID){
                   $sql="update `hotel_group` set indi_due='".$due."' where id='".$groupID."' ";
                   $this->db->query($sql);
                   
            }
			
		function get_fd_tax_by_id($booking_id){
			$this->db->select('fvat,lvat');
			$this->db->where('booking_id',$booking_id);
			$query = $this->db->get(TABLE_PRE.'pos');
       
			if($query){
				return $query->row();
			}
			else{
				return false;
			}
			
		}	
		
		function update_housekeeping_log_status_by_staff_type($room_id,$staff_type)
		{
			$up_array=array(
			'status'=>'Done',
			'compleated_date'=>date('Y-m-d'),
			);
			$this->db->where("room_id=".$room_id." and staff_type='".$staff_type."' and status='pending'");
			$this->db->update(TABLE_PRE.'housekeeping_log_detail',$up_array);
		}
		
	function delete_fp($id){
		 $this->db->where('id',$id);
         $query=$this->db->delete(TABLE_PRE.'food_plan');
         if($query){
             return true;
         }else{
             return false;
         }
	}	
		
		
		/*function get_staff_by_id($a)
			{	//$this->db->select('maid_name');
				$this->db->where('maid_id',$a);
			$query = $this->db->get(TABLE_PRE.'housekeeping_maid');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }	
			}*/
			
		function get_all_housekeeping_status()
			{$this->db->where('type','Housekeeping');
			$query = $this->db->get(TABLE_PRE.'housekeeping_status');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }	
			}
	
		function get_housekkeping_color_by_cleane_id()
		{
			$this->db->where('maid_id',$a);
			$query = $this->db->get(TABLE_PRE.'housekeeping_maid');
			if($query->num_rows()>0)
				{
				return $query->row();
				}
			else{
            return false;
				}	
		}
	
	function allFoodPlansById($id){
        $this->db->where('id',$id);
        $query=$this->db->get(TABLE_PRE.'food_plan');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
    }	
	
	
	
	
		function get_color_by_houskeeping_status_id($a)
		{
			$this->db->where('status_id',$a);
			$query = $this->db->get(TABLE_PRE.'housekeeping_status');
			if($query->num_rows()>0)
				{
				return $query->row();
				}
			else
				{
				return false;
				}	
			
		}
	
	function allServices($id){
        $this->db->where('food_plan_id',$id);
        $query=$this->db->get(TABLE_PRE.'food_plan_mapping');
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return false;
        }
    }	
	
	
	function get_work_nature_id_by_staff_type($id)
	{	$this->db->select('housekeeping_nature_work_id');
		 $this->db->where('related_staff_type_name',$id);
        $query=$this->db->get(TABLE_PRE.'housekeeping_nature_work');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
	}
	
	
	function get_room_no($id)
	{
		$this->db->select('room_no');
		 $this->db->where("room_id=".$id." and hotel_id=".$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'room');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
	}
	
	function get_assignment_type_by_id($id)
	{
		$this->db->select('housekeeping_nature_work_name');
		 $this->db->where("housekeeping_nature_work_id=".$id." and housekeeping_nature_work_hotel_id=".$this->session->userdata('user_hotel'));
        $query=$this->db->get(TABLE_PRE.'housekeeping_nature_work');
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return false;
        }
		
	}
	
	
	
	function get_room_housekeeping(){
        $query = $this->db->get(TABLE_PRE.'room');
		if($query){
            return $query->result();
        }else{
            return false;
        }
    }
	
    function  edit_food_plan($data1,$data2,$id){
		$this->db->where('id',$id);
		$this->db->update(TABLE_PRE.'food_plan',$data1);
		
        $this->db->where('food_plan_id',$id);
        $query_delete=$this->db->delete(TABLE_PRE.'food_plan_mapping');
		
		foreach($data2['s_name'] as $key => $val){
			$arr = array(
			's_name' => $data2['s_name'][$key],
			'qty' => $data2['qty'][$key],
			's_unit_cost' => $data2['s_unit_cost'][$key],
			'food_plan_id' => $id
			);	
			//exit;
			$query=$this->db->insert(TABLE_PRE.'food_plan_mapping',$arr);
		}		
		//exit;
        if($query){
            return true;
        }else{
            return false;
        }

    }	
	
	function get_all_housekeeping_status_by_id($id)
		{
				//$this->db->select('status_name');
				$this->db->where('status_id',$id);
				$query = $this->db->get(TABLE_PRE.'housekeeping_status');
				if($query->num_rows()>0)
					{
					return $query->row();
					}
				else
					{
					return false;
					}	
		}
	
		
			
}

?>