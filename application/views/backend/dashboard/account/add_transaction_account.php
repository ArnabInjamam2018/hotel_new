<!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Transaction Account</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
		$form = array(
			'class' 			=> '',
			'id'				=> 'form',
			'method'			=> 'post',								
		);
		echo form_open_multipart('account/add_transaction_account',$form);
	?>
	
	
    <div class="form-body">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="t_id" placeholder="Transaction ID *" required="required">
            <label></label>
            <span class="help-block">Transaction ID *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="c_id" placeholder="Child Account ID *" required="required">
            <label></label>
            <span class="help-block">Child Account ID *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control"  onkeypress=" return onlyNos(event, this);"  name="amount" placeholder="Amount Paid">
            <label></label>
            <span class="help-block">Amount Paid</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="time_startup" class="form-control timepicker timepicker-default timepicker-24" placeholder="Time Stamp">
            <label></label>
            <span class="help-block">Time Stamp</span> </div>
        </div>
		<div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select  name="payment">
            	<option value=""  selected="selected">Mode of Payment</option>
                <option value="Cash">Cash</option>
                <option value="Card">Card</option>
                <option value="Check">Check</option>
            </select>
            <label></label>
            <span class="help-block">Mode of Payment</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="card_no" class="form-control" placeholder="Card No">
            <label></label>
            <span class="help-block">Card No</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="cvv_no" class="form-control" placeholder="CVV No">
            <label></label>
            <span class="help-block">CVV No</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="exp_date" class="form-control date-picker" placeholder="Exp Date">
            <label></label>
            <span class="help-block">Exp Date</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="bank_name" class="form-control" placeholder="Bank Name">
            <label></label>
            <span class="help-block">Bank Name</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="check_no" placeholder="Check No">
            <label></label>
            <span class="help-block">Check No</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="bill_id" placeholder="Bill ID">
            <label></label>
            <span class="help-block">Bill ID</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="amount_pending" class="form-control" placeholder="Amount Pending">
            <label></label>
            <span class="help-block">Amount Pending</span> </div>
        </div>     
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="discount" class="form-control" placeholder="Discount">
            <label></label>
            <span class="help-block">Discount</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="tax" class="form-control" placeholder="Tax Amount">
            <label></label>
            <span class="help-block">Tax Amount</span> </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" id="btnSubmit" class="btn blue" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
  </div>
  <?php echo form_close(); ?> 
  <!-- END CONTENT --> 
</div>
