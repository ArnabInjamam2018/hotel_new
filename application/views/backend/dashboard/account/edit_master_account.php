<!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Edit Master Account</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
		$form = array(
			'class' 			=> '',
			'id'				=> 'form',
			'method'			=> 'post',								
		);
		echo form_open_multipart('account/edit_chain_master',$form);
	?>
	<?php if(isset($master)&&($master)){
////echo '<pre>';		
//print_r($master);
		?>
		
	
    <div class="form-body">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
			
            <input type="text" class="form-control" name="master_id" placeholder="Master ID *" value="<?php echo $master->master_id; ?>" required="required">
            <label></label>
            <span class="help-block">Master ID *</span> </div>
        </div>
<input type="hidden" id="hid" name="hid" value="<?php echo $master->id; ?>">
         <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="hotel_group_name" value="<?php echo $master->hotel_group_name; ?>" placeholder="Hotel Group Name *" required="required">
            <label></label>
            <span class="help-block">Hotel Group Name *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="address" value="<?php echo $master->Address; ?>" placeholder="Address">
            <label></label>
            <span class="help-block">Address</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="pincode" class="form-control"  onkeypress=" return onlyNos(event, this);"  value="<?php echo $master->master_id; ?>" placeholder="Pincode">
            <label></label>
            <span class="help-block">Pincode</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="city" class="form-control" value="<?php echo $master->city; ?>" placeholder="City">
            <label></label>
            <span class="help-block">City</span> </div>
        </div>        
		<div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="state" class="form-control" value="<?php echo $master->state; ?>" placeholder="State">
            <label></label>
            <span class="help-block">State</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="country" class="form-control" value="<?php echo $master->country; ?>" placeholder="Country">
            <label></label>
            <span class="help-block">Country</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="contact_no" class="form-control"  onkeypress=" return onlyNos(event, this);"  value="<?php echo $master->contact_no; ?>"  placeholder="Contact No">
            <label></label>
            <span class="help-block">Contact No</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="email_id" class="form-control" value="<?php echo $master->email; ?>" placeholder="Emai Id">
            <label></label>
            <span class="help-block">Emai Id</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="password" class="form-control" value="<?php echo $master->password; ?>" placeholder="Password">
            <label></label>
            <span class="help-block">Password</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="contact_person"value="<?php echo $master->contact_prerson_name; ?>" placeholder="Contact Person Name">
            <label></label>
            <span class="help-block">Contact Person Name</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="account_no"  onkeypress=" return onlyNos(event, this);"    value="<?php echo $master->no_of_accounts; ?>" placeholder="No of Account">
            <label></label>
            <span class="help-block">No of Account</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="plan_id" class="form-control"value="<?php echo $master->plan_id; ?>" placeholder="Plan Id">
            <label></label>
            <span class="help-block">Plan Id</span> </div>
        </div>        
		
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="amount_due" class="form-control"   onkeypress=" return onlyNos(event, this);"     value="<?php echo $master->toal_amount_paid; ?>" placeholder="Amount Due">
            <label></label>
            <span class="help-block">Total Amount Paid</span> </div>
        </div>
		<div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="amount_due" class="form-control"  onkeypress=" return onlyNos(event, this);"    value="<?php echo $master->amount_due; ?>" placeholder="Amount Due">
            <label></label>
            <span class="help-block">Amount Due</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="bill_cycle"value="<?php echo $master->bill_cycle; ?>" class="form-control date-picker" placeholder="Bill Cycle">
            <label></label>
            <span class="help-block">Bill Cycle</span> </div>
        </div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
            <textarea class="form-control" name="bill_cycle_details" required="required" placeholder="Bill Cycle Details *"><?php echo $master->bill_cycle_details; ?></textarea>
            <label></label>
            <span class="help-block">Bill Cycle Details *</span> </div>
        </div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
            <textarea class="form-control" name="comment"  placeholder="Comments"><?php echo $master->comments; ?></textarea>
            <label></label>
            <span class="help-block">Comments *</span> </div>
        </div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
            <textarea class="form-control" name="email_details"  placeholder="Email Counfiger Details"><?php echo $master->email_config_details; ?></textarea>
            <label></label>
            <span class="help-block">Email Counfiger Details</span> </div>
        </div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
            <textarea class="form-control" name="sms_details" placeholder="SMS Counfiger Details"><?php echo $master->sms_config_details; ?></textarea>
            <label></label>
            <span class="help-block">SMS Counfiger Details</span> </div>
        </div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
            <textarea class="form-control" name="api_details" placeholder="Misc API Counfiger Details"><?php echo $master->Mise_API_config_details; ?></textarea>
            <label></label>
            <span class="help-block">Misc API Counfiger Details</span> </div>
        </div>
       <!-- <div class="col-md-4">
          <div class="form-group form-md-line-input uploadss">
          <label>Upload Photo</label>
          <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/dashboard/assets/global/img/no-img.png" alt=""/> </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
            <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('image_photo');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
          </div>
          </div>
        </div>-->
		<div class="col-md-4">
                        <div class="form-group form-md-line-input uploadss">
                          <label>Upload Asset Image</label>
                          <div class="fileinput fileinput-new" data-provides="fileinput" readonly>
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="<?php echo base_url();?>upload/account/<?php echo $master->image;?>" alt=""/>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                            </div>
                            <div>
                                <span class="btn default btn-file">
                                <span class="fileinput-new">
                                Select image </span>
                                <span class="fileinput-exists">
                                Change </span>
                                <?php echo form_upload('image_photo');?>
                                </span>
                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                Remove </a>
                            </div>
                          </div>
                          <?php //echo form_upload('a_asset_image');?> 
                          <!--<input type="file"  name="a_asset_image" />--> 
                        </div>
                    </div>
		
      </div>
    </div><?php }?>
    <div class="form-actions right">
      <button type="submit" id="btnSubmit" class="btn blue" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
  </div>
	<?php echo form_close(); ?> 
  <!-- END CONTENT --> 
</div>
