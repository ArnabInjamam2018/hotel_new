<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Bill Account </div>
    <div class="tools"> 
      <a href="javascript:;" class="collapse">
        </a>
        <a href="#portlet-config" data-toggle="modal" class="config">
        </a>
        <a href="javascript:;" class="reload">
        </a>
        <a href="javascript:;" class="remove">
        </a> 
    </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-6">
          <div class="btn-group"> <a href="<?php echo base_url();?>account/add_bill_account">
            <button  class="btn green"> Add New <i class="fa fa-plus"></i> </button>
            </a> </div>
        </div>
        <div class="col-md-6">
          <div class="btn-group pull-right" style="margin-left:10px;">
            <button class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-filter"></i> </button>
            <ul class="dropdown-menu pull-right checkbox-list">
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="0" type="checkbox">
                  Id</label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="1" type="checkbox">
                  Image</label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="2" type="checkbox">
                  Hotel Name</label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column"  checked data-columnindex="3" type="checkbox">
                  No of Room </label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="4" type="checkbox">
                  plan </label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="5" type="checkbox">
                  Billing Cycle </label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="6" type="checkbox">
                 Billing Cycle Details</label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="7" type="checkbox">
                  Renewalcharge</label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="8" type="checkbox">
                  Total Amount Paid</label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="9" type="checkbox">
                  Contact No</label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="10" type="checkbox">
                  Address</label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="11" type="checkbox">
                  City</label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="12" type="checkbox">
                  Pincode</label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="13" type="checkbox">
                  State</label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="14" type="checkbox">
                  Axis room config details</label>
              </li>
               <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                  Special Notes </label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                 API details</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                  Country</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                  Contact Person</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                  email</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                password</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                password</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                Secondary_email</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                Pending Amount</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                db Config Details</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                Pdate Added</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                Master Account Id</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                Status</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                Pos Config Details</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                HRM Config Details</label>
              </li>
			  <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="15" type="checkbox">
                Account config Details</label>
              </li>
              <li>
                <label>
                  <input class="show_hide_column" checked data-columnindex="16" type="checkbox">
                  Actions</label>
              </li>
            </ul>
          </div>
          <div class="btn-group pull-right">
            <button class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-download"></i> </button>
            <ul class="dropdown-menu pull-right">
              <li> <a href="javascript:;"> Print </a> </li>
              <li> <a href="javascript:;"> Save as PDF </a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <table class="table table-striped table-hover table-bordered table-scrollable" id="sample_1">
      <thead>
        <tr> 
          <!--<th scope="col">
                Select
            </th>-->
          <th scope="col"> Id </th>
          <th scope="col"> 		billing_cycle </th>
          <th scope="col"> 	billing_cycle_details </th>
          <th scope="col"> 	payable_amount </th>
          <th scope="col"> 	previous_pending_amount	 </th>
		  <th scope="col"> service_tax_amount</th>
		  <th scope="col"> 	other_tax_1_amount </th>
		  <th scope="col"> other_tax_2_amount </th>
		  <th class="none"> 	mise_charges </th>
		  <th class="none"> mise_charges_details</th>
		  <th class="none"> 	status</th>                       
		  <th class="none"> 	child_account_id</th>                       
		  <th class="none"> 	bill_generated_date</th>                       
		  <th class="none"> 	minimum_amount_due</th>                       
          <th scope="col"> Actions </th>
        </tr>
      </thead>
      <tbody>
        <?php
            $x = 1;
            if(isset($bill_account) && $bill_account){
            foreach($bill_account as $account){
                ?>
        <tr id="row_<?php echo $account->id;?>">
          <td><?php echo $account->id; ?></td> 
		  
          <td><?php echo $account->billing_cycle; ?></td>
          <td><?php echo $account->billing_cycle_details; ?></td>
          <td><?php echo $account->payable_amount; ?></td>
          <td><?php echo $account->previous_pending_amount	; ?></td>
          <td><?php echo $account->service_tax_amount	; ?></td>
          <td><?php echo $account->other_tax_1_amount	; ?></td>
          <td><?php echo $account->other_tax_2_amount	; ?></td>
          <td><?php echo $account->	mise_charges	; ?></td>
          <td><?php echo $account->	mise_charges_details	; ?></td>
          <td><?php echo $account->	status	; ?></td>
          <td><?php echo $account->	child_account_id	; ?></td>
          <td><?php echo $account->	bill_generated_date	; ?></td>
          <td><?php echo $account->	minimum_amount_due	; ?></td>
          
          <td class="ba">
            <div class="btn-group">
              <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo  $account->id;?>')" data-toggle="modal" class="btn red btn-sm"><i class="fa fa-trash"></i></a></li>
                <li><a  data-toggle="modal" href="#responsive" onclick="edit('<?php echo  $account->id;?>')"  class="btn green btn-sm"><i class="fa fa-edit"></i> </a></li>
              </ul>
            </div>
           </td>
        </tr>
        <?php $x++ ; 
            }} ?>
       
      </tbody>
    </table>
  </div>
</div>

<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Transaction Account</h4>
      </div>
      <?php
/*
	  $form = array(
		  'class' 			=> 'form-body',
		  'id'				=> 'form',
		  'method'			=> 'post'
	  );
	
	  echo form_open_multipart('dashboard/add_profit_center',$form);*/
	
	  ?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
		  <input type="hidden" id="hid">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="pc_location" id="child_account" required placeholder="Child Account..">
              <label></label>
              <span class="help-block">Child Account..</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="amount_paid" name="balance" placeholder="Amount Paid  ..."  maxlength="10" onkeypress=" return onlyNos(event, this);" >
              <label></label>
              <span class="help-block">Amount Paid</span> </div>
          </div>
          
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
               <input autocomplete="off" type="text" class="form-control" id="timestamp" name="balance" placeholder="Add on"  maxlength="10"  >
              <label></label>
              <span class="help-block">Add on...</span> </div>
          </div>
		  <div class="col-md-4">
            <div class="form-group form-md-line-input">
             <input autocomplete="off" type="text" class="form-control" id="mode_of_payment" name="balance" placeholder="Payment Mode"  maxlength="10"  >
              <label></label>
              <span class="help-block">Payment Mode</span> </div>
          </div>
		   <div class="col-md-4">
            <div class="form-group form-md-line-input">
             <input autocomplete="off" type="text" class="form-control" id="card_no" name="balance" placeholder=" Card No"  maxlength="10"  >
              <label></label>
              <span class="help-block"> Card No</span> </div>
          </div>
		   <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="cvv_no" name="balance" placeholder="CVV No"  maxlength="10"  >
              <label></label>
              <span class="help-block">CVV No</span> </div>
          </div>
		  <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="exp_date" name="balance" placeholder="Exp Date"  maxlength="10"  >
              <label></label>
              <span class="help-block">Exp Date</span> </div>
          </div> 
		  <div class="col-md-4">           
		   <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="bank_name" name="balance" placeholder="Bank Name"  maxlength="10"  >
              <label></label>
              <span class="help-block">Bank Name</span>
			  </div>
		 </div>
			    <div class="col-md-4">  
			  <div class="form-group form-md-line-input">
             <input autocomplete="off" type="text" class="form-control" id="check_no" name="balance" placeholder="Check No"  maxlength="10"  >
              <label></label>
              <span class="help-block">Check No</span> 
			  </div></div>
			  <div class="col-md-4">  
			  <div class="form-group form-md-line-input">
             <input autocomplete="off" type="text" class="form-control" id="bill_id" name="balance" placeholder="Bill Id"  maxlength="10"  >
              <label></label>
              <span class="help-block">Bill Id</span> 
			  </div>
			  </div>
			  
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" onclick="update()" class="btn green">Save</button>
      </div>
      <?php //echo form_close(); ?> </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo base_url();?>assets/common/js/table_sort_style.js"></script> 
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          

            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>account/delete_child_account",
                data:{id:id},
                success:function(data)
                {
                    
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                           $('#row_'+id).remove();
						   

                        });
                }
            });



        });
    }
	
</script>
<script>
function edit(v){
		//alert(v);
		 $('#hid').val(v);    
		$.ajax({
                type:"POST",
                url: "<?php echo base_url();?>account/get_transaction",
                data:{id:v},
                success:function(data)
                {
                   // alert(data.child_account);
					//alert(data.amount_paid);
                                 
                   $('#child_account').val(data.child_account);                   
                   $('#amount_paid').val(data.amount_paid);                   
                   $('#timestamp').val(data.timestamp);                   
                   $('#mode_of_payment').val(data.mode_of_payment);                   
                   $('#card_no').val(data.card_no);                   
                   $('#cvv_no').val(data.cvv_no);                   
                   $('#exp_date').val(data.exp_date);                   
                   $('#bank_name').val(data.bank_name);                   
                   $('#check_no').val(data.check_no);                   
                   $('#bill_id').val(data.bill_id);                   
														  
                   
					
                }
            });
	}
	
	function update(){
		
					var hid=$('#hid').val();                   
					var child_account=$('#child_account').val();                   
                   var amount_paid=$('#amount_paid').val();                   
                   //var timestamp=$('#timestamp').val();                   
                   var mode_of_payment=$('#mode_of_payment').val();                   
                   var card_no=$('#card_no').val();                   
                   var cvv_no=$('#cvv_no').val();                   
                   var exp_date=$('#exp_date').val();                   
                   var bank_name=$('#bank_name').val();                   
                   var check_no=$('#check_no').val();                   
                   var bill_id=$('#bill_id').val();   
				   //alert(hid);
				   //alert(hid);
											
		$.ajax({
                type:"POST",
                url: "<?php echo base_url();?>account/update_transaction",
                data:{
					hid:hid,
					child_account:child_account,
					amount_paid:amount_paid,
					mode_of_payment:mode_of_payment,
					card_no:card_no,
					cvv_no:cvv_no,
					exp_date:exp_date,
					bank_name:bank_name,
					check_no:check_no,
					bill_id:bill_id
					},
                success:function(data)
                {
                     $('#responsive').modal('toggle');
						location.reload();	
														  
                   
					
                }
            });
	}
</script>