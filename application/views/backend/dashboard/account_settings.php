<!-- BEGIN PAGE CONTENT-->

<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"><i class="glyphicon glyphicon-bed"></i> <span class="caption-subject bold uppercase">Account Setting</span> </div>
  </div>
  <div class="portlet-body form">
    <div class="tabbable-line tabbable-full-width">
      <ul class="nav nav-tabs">
        <li class="active"> <a href="#tab_1_1" data-toggle="tab"> General </a> </li>
        <li> <a href="#tab_1_2" data-toggle="tab"> Email </a> </li>
        <li> <a href="#tab_1_3" data-toggle="tab"> API </a> </li>
        <li> <a href="#tab_1_4" data-toggle="tab"> Plan </a> </li>
        <li> <a href="#tab_1_5" data-toggle="tab"> Payment </a> </li>
        <li> <a href="#tab_1_6" data-toggle="tab"> Users </a> </li>
        <li> <a href="#tab_1_7" data-toggle="tab"> Sub Accounts </a> </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1_1">
          <div class="form-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="Organization Name *">
                  <label></label>
                  <span class="help-block">Organization Name *</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="Location *">
                  <label></label>
                  <span class="help-block">Location *</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="Currency *">
                  <label></label>
                  <span class="help-block">Currency *</span> </div>
              </div>
              <div class="col-md-12">
                <div class="form-group form-md-line-input uploadss">
                  <label>Upload Photo</label>
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                    <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
                      <input type="file"  name="image" />
                      </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-actions right">
            <button type="submit" class="btn blue">Submit</button>
          </div>
        </div>
        <div class="tab-pane" id="tab_1_2">
          <div class="form-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="Organization Name *">
                  <label></label>
                  <span class="help-block">Organization Name *</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="Location *">
                  <label></label>
                  <span class="help-block">Location *</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="Currency *">
                  <label></label>
                  <span class="help-block">Currency *</span> </div>
              </div>
              <div class="col-md-12">
                <div class="form-group form-md-line-input uploadss">
                  <label>Upload Photo</label>
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                    <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
                      <input type="file"  name="image" />
                      </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-actions right">
            <button type="submit" class="btn blue">Submit</button>
          </div>
        </div>
        <div class="tab-pane" id="tab_1_3">
          <div class="form-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="Wheather *">
                  <label></label>
                  <span class="help-block">Wheather *</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="SMS *">
                  <label></label>
                  <span class="help-block">SMS *</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="POS *">
                  <label></label>
                  <span class="help-block">POS *</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="Finance *">
                  <label></label>
                  <span class="help-block">Finance *</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="Channel Manafer *">
                  <label></label>
                  <span class="help-block">Channel Manafer *</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="Website *">
                  <label></label>
                  <span class="help-block">Website *</span> </div>
              </div>
            </div>
          </div>
          <div class="form-actions right">
            <button type="submit" class="btn blue">Submit</button>
          </div>
        </div>
        <div class="tab-pane" id="tab_1_4">
          <div class="form-body">
            <div class="row">
              <div class="col-md-4">
                <a href="#" style="font-size: 25px;">Current Plan</a>
              </div>
              <div class="col-md-4">
                <a href="#" style="font-size: 25px;">Other Plans</a>
              </div>
              <div class="col-md-4">
                <button type="submit" class="btn blue">Upgrade</button>
              </div>
            </div>
          </div>
          <div class="form-actions right">
            <button type="submit" class="btn blue">Submit</button>
          </div>
        </div>
        <div class="tab-pane" id="tab_1_5">
          <div class="form-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="Total Recieved *" disabled="disabled">
                  <label></label>
                  <span class="help-block">Total Recieved *</span> </div>
              </div>
              <div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input type="text" autocomplete="off" class="form-control" id="" name="" required="required" placeholder="Pending Payment *" disabled="disabled">
                  <label></label>
                  <span class="help-block">Pending Payment *</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                	<button type="submit" class="btn blue">Make Payment</button>
                    <a href="#" class="btn blue">List Payments</a>
                </div>
              </div>
            </div>
          </div>
          <div class="form-actions right">
            <button type="submit" class="btn blue">Submit</button>
          </div>
        </div>
        <div class="tab-pane" id="tab_1_6">
        	<div class="form-body">        	
            <table class="table table-striped table-hover table-bordered">
              <thead>
                <tr>
                  <th width="5%" align="center" valign="middle" scope="col"> Sl. No. </th>
                  <th width="15%" align="center" valign="middle" scope="col"> Name </th>
                  <th width="10%" align="center" valign="middle" scope="col"> Username </th>
                  <th width="10%" align="center" valign="middle" scope="col"> Profit Center </th>          
                  <th width="10%" align="center" valign="middle" scope="col"> User Type </th>
                  <th width="10%" align="center" valign="middle" scope="col"> Admin Role </th>
                  <th width="10%" align="center" valign="middle" scope="col"> Status </th>
                  <th width="10%" align="center" valign="middle" scope="col"> Action </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td align="left" valign="middle">sdvds</td>
                  <td align="left" valign="middle">fsdsdfgvfsd</td>
                  <td align="left" valign="middle">sdfgv</td>
                  <td align="left" valign="middle">sdfv</td>
                  <td align="left" valign="middle">asdfg</td>
                  <td align="left" valign="middle">sdg</td>
                  <td align="left" valign="middle">sdgsdgds</td>
                  <td align="center" valign="middle" class="ba">
                    <div class="btn-group">
                      <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                      <ul class="dropdown-menu pull-right" role="menu">
                        <li> <a href="#" data-toggle="modal" class="btn red btn-sm"> <i class="fa fa-trash"></i></a> </li>
                        <li> <a href="#" data-toggle="modal" class="btn green btn-sm"><i class="fa fa-edit"></i></a> </li>
                      </ul>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            </div>
            <div class="form-actions right">
            	<a href="#" class="btn green"> Add New <i class="fa fa-plus"></i></a>
            </div>
        </div>
        <div class="tab-pane" id="tab_1_7">
        	<div class="form-body">        	
            <table class="table table-striped table-hover table-bordered">
              <thead>
                <tr>
                  <th width="5%" align="center" valign="middle" scope="col"> Sl. No. </th>
                  <th width="15%" align="center" valign="middle" scope="col"> Name </th>
                  <th width="10%" align="center" valign="middle" scope="col"> Username </th>
                  <th width="10%" align="center" valign="middle" scope="col"> Profit Center </th>          
                  <th width="10%" align="center" valign="middle" scope="col"> User Type </th>
                  <th width="10%" align="center" valign="middle" scope="col"> Admin Role </th>
                  <th width="10%" align="center" valign="middle" scope="col"> Status </th>
                  <th width="10%" align="center" valign="middle" scope="col"> Action </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td align="left" valign="middle">sdvds</td>
                  <td align="left" valign="middle">fsdsdfgvfsd</td>
                  <td align="left" valign="middle">sdfgv</td>
                  <td align="left" valign="middle">sdfv</td>
                  <td align="left" valign="middle">asdfg</td>
                  <td align="left" valign="middle">sdg</td>
                  <td align="left" valign="middle">sdgsdgds</td>
                  <td align="center" valign="middle" class="ba">
                    <div class="btn-group">
                      <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                      <ul class="dropdown-menu pull-right" role="menu">
                        <li> <a href="#" data-toggle="modal" class="btn red btn-sm"> <i class="fa fa-trash"></i></a> </li>
                        <li> <a href="#" data-toggle="modal" class="btn green btn-sm"><i class="fa fa-edit"></i></a> </li>
                      </ul>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            </div>
            <div class="form-actions right">
            	<a href="#" class="btn green"> Add New <i class="fa fa-plus"></i></a>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- END PAGE CONTENT--> 