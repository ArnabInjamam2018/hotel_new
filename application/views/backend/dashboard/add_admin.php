<!-- BEGIN PAGE CONTENT-->
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$(document).ready(function(){
    $("form").submit(function(){
		
			$.password = $('#admin_password').val();
			$.password_again =$('#admin_retype_password').val();
			if($.password != $.password_again)
			{
				alert('Password Does Not Matched');
				return false;
			}
			else
			{
				return true;
			}
		
		});
	});
</script>
<div class="portlet box blue">
<div class="portlet-title">
  <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Admin</span> </div>
</div>
<div class="portlet-body form">
  <?php

                        $form = array(
                            'id'				=> 'form',
                            'method'			=> 'post'
                        );

                        if(isset($permission) && $permission){
                            foreach($permission as $per){
                                $admin_permission=$this->dashboard_model->get_permission_by_label($per->permission_label);
                                if(isset($admin_permission) && $admin_permission){
                                    $i=0;
                                    foreach($admin_permission as $admin_perm){
                                        $permission_types[$per->permission_label][$i]=array(
                                            'value'=>$admin_perm->permission_value,
                                            'id'=>$admin_perm->permission_id
                                        );
                                        $i++;
                                    }
                                }
                            }
                        }
                        $key=array_keys($permission_types);

                        echo form_open_multipart('dashboard/add_admin',$form);

                        ?>
  <div class="form-body">
    <div class="row">
      <?php if($this->session->userdata('user_type_slug')=="SUPA"):  ?>
      <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select name="admin_hotel" id="admin_hotel" class="form-control bs-select">
              <option value="" disabled="disabled" selected="selected">Hotel Name</option>
              <?php $hotels=$this->dashboard_model->all_hotels();
                                        if(isset($hotels) && $hotels){
                                            foreach($hotels as $hotel){

                                            ?>
              <option value="<?php echo $hotel->hotel_id ?>"><?php echo $hotel->hotel_name ?></option>
              <?php }} ?>
            </select>
            <label></label><span class="help-block">Hotel Name *</span>
          </div>
      </div>
      <?php endif; ?>
      <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input name="admin_first_name" type="text" class="form-control" id="form_control_1" required="required" onkeypress=" return onlyLtrs(event,this);" placeholder="First Name *">                
            <label></label><span class="help-block">First Name *</span>
          </div>
      </div>
      <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input name="admin_middle_name" type="text" class="form-control" id="form_control_1" onkeypress="return onlyLtrs(event, this);" placeholder="Middle Name">
            <label></label><span class="help-block">Middle Name</span>
          </div>
      </div>
      <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input name="admin_last_name" type="text" class="form-control" id="form_control_1" required="required" onkeypress="return onlyLtrs(event, this);" placeholder="Last Name *">
            <label></label><span class="help-block">Last Name *</span> 
          </div>
      </div>
      <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input name="admin_email" type="email" class="form-control" id="form_control_1" required="required" placeholder="Email *">
            <label></label><span class="help-block">Email *</span> 
          </div>
      </div>
	  
	  
	  
	  
	  
	  
      <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input name="admin_username" type="email" class="form-control" id="form_control_1" required="required" placeholder="Uesr Name *">
            <label></label><span class="help-block">Uesr Name *</span> 
          </div>
      </div>
      <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input name="admin_password" type="password" class="form-control" id="admin_password"  required="required" placeholder="Password *">
            <label></label><span class="help-block">Password *</span> 
          </div>
      </div>
      <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  name="admin_retype_password" type="password" class="form-control" id="admin_retype_password" required="required" placeholder="Retype Password *">
            <span class="help-block">Retype Password *</span> 
          </div>
      </div>
      <div class="col-md-12" style="padding-top: 20px;">
          <div class="row">
              <?php foreach($key as $keys):?>
                  <div class="col-md-6 form-horizontal">
                      <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label" for="form_control_1"><span class="label" style="color:#048B9A; background-color:#F0F0F0; font-size:13px; font-weight:bold;"><?php echo $keys;?> <i class="fa fa-angle-double-right" aria-hidden="true" style="font-size: 12px;"></i></span></label>
                        <div class="col-md-8">
                          <div class="md-checkbox-inline" >
                            <?php $i=1; ?>
                            <?php foreach($permission_types[$keys] as $per=>$value):?>
                            <div class="md-checkbox" >
                              <input class="md-check" id="checkbox<?php echo $value['id'];?>" type="checkbox" name="permission[]" value="<?php echo $value['id'];?>" checked="checked"/>
                              <label for="checkbox<?php echo $value['id'];?>"> <span></span> <span class="check"></span> <span class="box"></span> <?php echo $value['value'];?></label>
                            </div>
                            <?php $i++; ?>
                            <?php endforeach;?>
                          </div>
                        </div>
                      </div>
                  </div>
              <?php endforeach;?>
          </div>
      </div>
      <div class="form-group col-md-12 uploadss">
        <label>Upload Photo</label>
        <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="width: 200px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
          <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
          <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('image_photo');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> <a href="#basic" class="btn blue fileinput-exists fancybox">Edit</a> </div>
        </div>
      </div>
    </div>
  </div>
    <div class="form-actions right">
        <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn submit">Submit</button>
              <button type="reset"   class="btn default">Reset</button>
            </div>
        </div>
    </div>
  
  <?php echo form_close(); ?> 
</div>
</div>
<div id="basic" style="text-align:center; display:none;">    
     <img src="http://122.163.127.3/hotelobjects_dev/upload/thumb/Loews-Hotel-Room_thumb.jpg" id="demo8" alt="Jcrop Example"/>
    <form action="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jcrop/crop-demo.php" method="post" id="demo8_form" style="padding:5px 0;">
        <input type="hidden" id="crop_x" name="x"/>
        <input type="hidden" id="crop_y" name="y"/>
        <input type="hidden" id="crop_w" name="w"/>
        <input type="hidden" id="crop_h" name="h"/>
        <input type="submit" value="Crop Image" class="btn btn-large green"/>
    </form>
</div>