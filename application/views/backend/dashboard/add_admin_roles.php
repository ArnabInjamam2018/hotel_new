<!-- BEGIN PAGE CONTENT-->

<script type="text/javascript">
	$( document ).ready( function () {
		$( "form" ).submit( function () {
			$.password = $( '#admin_password' ).val();
			$.password_again = $( '#admin_retype_password' ).val();
			if ( $.password != $.password_again ) {
				swal( 'Password Does Not Match' );
				return false;
			} else {
				return true;
			}
		} );
	} );
</script>
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Admin Roles</span> </div>
	</div>
	<div class="portlet-body form">

		<?php
		
		$form = array(
			'id' => 'form',
			'method' => 'post'
		);

		if ( isset( $permission ) && $permission ) {

			foreach ( $permission as $per ) {
				$admin_permission = $this->dashboard_model->get_permission_by_label( $per->permission_label );

				if ( isset( $admin_permission ) && $admin_permission ) {

					$i = 0;
					foreach ( $admin_permission as $admin_perm ) {

						$permission_types[ $per->permission_label ][ $i ] = array(
							'value' => $admin_perm->permission_value,
							'id' => $admin_perm->permission_id,
						);
						$i++;
					}
				}
			}
		}

		$key = array_keys( $permission_types );



		echo form_open_multipart( 'dashboard/add_admin_roles', $form );
		?>
		<div class="form-body">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<input name="admin_roll_name" type="text" class="form-control" id="form_control_1" required="required" onkeypress=" return onlyLtrs(event,this);" placeholder="Admin Role Name *">
						<label></label><span class="help-block">Admin Role Name *</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">

						<select name="admin_type" class="form-control bs-select">
							<option value="">Admin Type</option>
							<option value="SUPA">Super Admin</option>
							<option value="AD">Admin</option>
							<option value="SUBA">Sub Admin</option>
						</select>
						<label></label><span class="help-block">Admin Type *</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<input name="max_discount" type="text" class="form-control" id="max_discount" required="required" placeholder="Max Discount Percentage *">
						<label></label><span class="help-block">Max Discount Percentage</span>
					</div>
				</div>
				<div class="col-md-12">
					<h3 class="subhead">Modules Privilages</h3>
				</div>

				<?php

				$lastHd = '';
				$lastLb = '';
				$c = 0;
				
	$len = sizeof($per_all);
				foreach ( $per_all as $key2 ) {
	
					$c++;

					if ( $lastLb != $key2->permission_label && $c > 1 ) {
						echo '</div></div></div></div>';
					}

					if ($lastHd != $key2->name) {
							echo '</div>';
						?>
					<div id="arole-block">
						<div class="col-md-12">
							<span class="Monda">
								<?php echo $key2->name; ?>
							</span>
						</div>
					<?php 
				
					}	
		if($lastLb != $key2->permission_label){

		?>

				<div class="col-md-6 form-horizontal">
					<div class="form-group form-md-line-input">
						<label class="col-md-5 control-label" style="color:#048B9A; font-size: 12px; padding-top: 8px;"><?php echo $key2->permission_label; ?> &nbsp;<i class="fa fa-angle-double-right" aria-hidden="true" style="font-size: 12px;"></i></label>
						<div class="col-md-7">
							<div class="md-checkbox-inline">
								<?php 
								$i=1; 
							?>

								<div class="md-checkbox">
									<input class="md-check" id="checkbox<?php echo $key2->permission_id;?>" type="checkbox" name="permission[]" value="<?php echo $key2->permission_id;?>" checked="checked"/>
									<label for="checkbox<?php echo $key2->permission_id;?>"> <span></span> <span class="check"></span> <span class="box"></span> <?php echo $key2->permission_value; ?></label>
								</div>
								
								<?php 
									$i++;
									
									$lastHd = $key2->name;
									$lastLb = $key2->permission_label;
								?>



								<?php
								} else {
									?>
								<div class="md-checkbox">
									<input class="md-check" id="checkbox<?php echo $key2->permission_id;?>" type="checkbox" name="permission[]" value="<?php echo $key2->permission_id;?>" checked="checked"/>
									<label for="checkbox<?php echo $key2->permission_id;?>"> <span></span> <span class="check"></span> <span class="box"></span> <?php echo $key2->permission_value; ?></label>
								</div>								
								<?php
								}
								
								} // end foreach 

								?>
								</div></div></div></div></div>
								
								<?php 
									
									if($len == $c)
										echo '';
								?>
								<div class="row">
								<div class="col-md-12">
									<h3 class="subhead">Other Privilages</h3>
								</div>
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-6 form-horizontal">
											<div class="form-group form-md-line-input">
												<label class="col-md-6 control-label" for="form_control_1">Can take bookings (Single & Group)</label>
												<div class="col-md-6">
													<div class="md-radio-inline">
														<div class="switch">

															<input type="checkbox" id="chk1" class="md-check cmn-toggle cmn-toggle-round checkbox" name="take_booking">

															<label for="chk1"></label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6 form-horizontal">
											<div class="form-group form-md-line-input">
												<label class="col-md-6 control-label" for="form_control_1">Can Edit & Delete Bookings</label>
												<div class="col-md-6">
													<div class="md-radio-inline">
														<div class="switch">

															<input type="checkbox" id="chk2" class="md-check cmn-toggle cmn-toggle-round checkbox" name="edit_booking">

															<label for="chk2"></label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6 form-horizontal">
											<div class="form-group form-md-line-input">
												<label class="col-md-6 control-label" for="form_control_1">Can interact with booking Calander</label>
												<div class="col-md-6">
													<div class="md-radio-inline">
														<div class="switch">

															<input type="checkbox" id="chk3" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_booking_engine">
															
															<label for="chk3"></label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6 form-horizontal">
											<div class="form-group form-md-line-input">
												<label class="col-md-6 control-label" for="form_control_1">Can access settings</label>
												<div class="col-md-6">
													<div class="md-radio-inline">
														<div class="switch">

															<input type="checkbox" id="chk4" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_setting">
															
															<label for="chk4"></label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6 form-horizontal">
											<div class="form-group form-md-line-input">
												<label class="col-md-6 control-label" for="form_control_1">Can add Admin</label>
												<div class="col-md-6">
													<div class="md-radio-inline">
														<div class="switch">

															<input type="checkbox" id="chk5" class="md-check cmn-toggle cmn-toggle-round checkbox" name="add_admin">

															<label for="chk5"></label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6 form-horizontal">
											<div class="form-group form-md-line-input">
												<label class="col-md-6 control-label" for="form_control_1">Can access reports</label>
												<div class="col-md-6">
													<div class="md-radio-inline">
														<div class="switch">

															<input type="checkbox" id="chk6" class="md-check cmn-toggle cmn-toggle-round " name="access_report">
															
															<label for="chk6"></label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								</div>
							
							
						</div>
						<div class="form-actions right">
									<button type="submit" class="btn submit">Submit</button>
									<a href="<?php echo base_url();?>dashboard/all_admin_roles" class="btn red">Cancel</a>
						</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>