<!-- BEGIN PAGE CONTENT-->
<script>

 

</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/dashboard/Colorpicker/farbtastic.css">
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/Colorpicker/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/Colorpicker/farbtastic.js" type="text/javascript"></script>

<div class="row ds">
  <div class="col-md-12">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green"> <i class="icon-pin font-green"></i> <span class="caption-subject bold uppercase"> Add Area Code</span> </div>
      </div>
      <div class="portlet-body form">
        <?php

                            $form = array(
                                'class' 			=> 'form-body',
                                'id'				=> 'form',
                                'method'			=> 'post',								
                            );
							
							

                            echo form_open_multipart('unit_class_controller/add_hotel_area_code',$form);

                            ?>
        <div class="form-body"> 
          <!-- 17.11.2015-->
          <?php if($this->session->flashdata('err_msg')):?>
          <div class="form-group">
            <div class="col-md-12 control-label">
              <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
            </div>
          </div>
          <?php endif;?>
          <?php if($this->session->flashdata('succ_msg')):?>
          <div class="form-group">
            <div class="col-md-12 control-label">
              <div class="alert alert-success alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
            </div>
          </div>
          <?php endif;?>
          <!-- 17.11.2015-->
          
          <div class="row">
         <div class="form-group form-md-line-input form-md-floating-label col-md-4 ">
              <input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" name="country" onkeypress=" return onlyLtrs(event, this);" required="required" >
              <label>Country  <span class="required">*</span></label>
              <span class="help-block">Country name...</span> </div>
				
          <div class="form-group form-md-line-input form-md-floating-label col-md-4 ">
              <input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" name="state" onkeypress=" return onlyLtrs(event, this);" required="required" >
              <label>State  <span class="required">*</span></label>
              <span class="help-block">State name...</span> </div>
            
			 <div class="form-group form-md-line-input form-md-floating-label col-md-4 ">
              <input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" name="city" onkeypress=" return onlyLtrs(event, this);" required="required" >
              <label>City  <span class="required">*</span></label>
              <span class="help-block">City name...</span> 
			  </div>
			  <div class="form-group form-md-line-input form-md-floating-label col-md-4 ">
              <input autocomplete="off" type="text"  class="form-control" id="form_control_1" name="pincode"  required="required" >
              <label>Pincode  <span class="required">*</span></label>
              <span class="help-block">Pincode</span> 
			  </div>
			  
           <div class="form-group form-md-line-input form-md-floating-label col-md-4 ">
              <input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" name="locality1" onkeypress=" return onlyLtrs(event, this);" required="required" >
              <label>Locality 1  <span class="required">*</span></label>
              <span class="help-block">Locality name...</span> 
		   </div>
		    <div class="form-group form-md-line-input form-md-floating-label col-md-4 ">
              <input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" name="locality2" onkeypress=" return onlyLtrs(event, this);" required="required" >
              <label>Locality 2  <span class="required">*</span></label>
              <span class="help-block">Locality name...</span> 
           
          </div>
          </div>
          <div class="form-actions right">
            <button type="submit" class="btn blue" >Submit</button>
            <!-- 18.11.2015  -- onclick="return check_mobile();"
            <button  type="reset" class="btn default">Reset</button> -->
          </div>
          <?php form_close(); ?>
          <!-- END CONTENT --> 
        </div>
      </div>
    </div>
  </div>
<script language="JavaScript">

   function check(value){/*
		//alert(value);
		
			if(value==1){	
          $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/check_nature_visit_default",
                data:{a:value},
                 success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                 

				swal({   title: "Are you sure?",
				text: data+"!",  
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Fix as Default!",   cancelButtonText: "No, cancel plz!", 
				closeOnConfirm: true,  
				closeOnCancel: true },
				function(isConfirm){   
				if (isConfirm) {     
				$("#default").val('1');
				} else {   
				$("#default").val('0');
				} });



				 
                }
            });
			}
		  
		  
		  //});
		
	}*/
	

		
 $(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});   
</script> 

