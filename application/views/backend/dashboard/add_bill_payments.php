<script src="<?php echo base_url();?>assets/global/plugins/typeahead1/jquery.typeahead.js"></script>

<style>
	.red {
		background-color: red;
	}
</style>
<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
<?php endif;?>
<!-- 17.11.2015-->
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Bill Payments</span> </div>
	</div>
	<div class="portlet-body form">
		<?php
		$form = array(
			'class' => '',
			'id' => 'form',
			'method' => 'post',

		);
		echo form_open_multipart( 'dashboard/add_bill_payments', $form );
		//print_r($bil_no);
		?>

		<div class="form-body">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control" id="date" name="date" value="<?php echo date("d/m/Y");?>" readonly placeholder="Date *">
						<label></label>
						<span class="help-block">Date *</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" id="top" name="top" required onchange="typesofpayments();">
							<option style="color:#AAAAAA;" value="" selected="selected" disabled="disabled">Select Payment Type</option>
							<option style="color:#C1704B;" value="Bill payments">Bill Payments</option>
							<option style="color:#12135A;" value="Salary/Wage Payments">Salary/Wage Payments</option>
							<option style="color:#144F2A;" value="Miscellaneous Payments">Miscellaneous Payments</option>
							<option style="color:#523742;" value="Tax/Compliance Payment">Tax/Compliance Payment</option>
						</select>
						<label></label>
						<span class="help-block">Select Payment Type *</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control" readonly id="hotel_name" name="hotel_name" placeholder="Hotel Name" value="<?php echo $hotel_name->hotel_name;?>">
						<label></label>
						<span class="help-block">Hotel Name</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" id="profit_center" name="profit_center" required>
							<option style="color:#AAAAAA;" value="" selected="selected" disabled="disabled">Profit Center *</option>
							<?php $pc=$this->dashboard_model->all_pc();?>
							<?php
							$defProfit = $this->unit_class_model->profit_center_default();
							if ( isset( $defProfit ) && $defProfit ) {
								$defPro = $defProfit->profit_center_location;
							} else {
								$defPro = "Select";
							}
							?>
							<option value="<?php echo $defPro;  ?>" selected>
								<?php echo $defPro; ?>
							</option>
							<?php $pc=$this->dashboard_model->all_pc1();
                        foreach($pc as $prfit_center){
                        ?>
							<option value="<?php echo $prfit_center->profit_center_location;?>">
								<?php echo  $prfit_center->profit_center_location;?>
							</option>
							<?php }?>
						</select>
						<label></label>
						<span class="help-block">Profit Center *</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control date-picker" id="bill_date" onchange="check_date()" name="bill_date" placeholder="Bill Date *">
						<label></label>
						<span class="help-block">Bill Date*</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" id="paymentt" name="payment_term" required onchange="paymentterm(this.value);">
							<option value="" selected="selected" disabled="disabled">Select Payment Term</option>
							<option value="immediate">Immediate</option>
							<option value="pia">PIA (Paid in advance)</option>
							<option value="net7">Net 7 (7 day credit)</option>
							<option value="net14">Net 14 (14 day credit)</option>
							<option value="net30">Net 30 (30 day credit)</option>
							<option value="net60">Net 60 (60 day credit)</option>
							<option value="net90">Net 90 (90 day credit)</option>
							<option value="cod">COD</option>
							<option value="billofex">Bill of exchange</option>
							<option value="contra">Contra</option>
							<option value="other">Specify Date</option>
						</select>
						<label></label>
						<span class="help-block">Payment Term</span> </div>
				</div>
				
				
			<div id="bill_other_dt" class="col-md-3" style="display:none;">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control date-picker" id="specific_date" name="specific_date" placeholder="Specific Payment Date *">
						<label></label>
						<span class="help-block">Specific Payment Date *</span> </div>
				</div>	
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control" name="ref_no" placeholder="Referance No">
						<label></label>
						<span class="help-block">Referance No</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control" name="bill_no" placeholder="Bill No" value="<?php echo 1+$bil_no."-".$this->session->userdata('user_id')."-HM0".$this->session->userdata('user_hotel');?>">
						<label></label>
						<span class="help-block">Bill No</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control date-picker" id="bill_due_date" onchange="check_date()" name="bill_due_date" placeholder="Due Date *">
						<label></label>
						<span class="help-block">Due Date*</span> </div>
				</div>
				<div id="billpayments" style='display:none;'>
					<h3 class="form-heading col-md-12">Bill Details</h3>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<div class="typeahead__container">
								<div class="typeahead__field"> <span class="typeahead__query">
                <input class="js-typeahead-user_v1 form-control" name="vendor[query]" id="vendor" value="" type="search" placeholder="Search" autocomplete="off">
                </span>
									</div>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="address" name="address" placeholder="Address*">
							<label></label>
							<span class="help-block">Address*</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="ph_no" name="ph_no" placeholder="Phone No *">
							<label></label>
							<span class="help-block">Phone No*</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="email" name="email" placeholder="Email *">
							<label></label>
							<span class="help-block">Email Id*</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" id="payforbill" name="billpayfor" onchange="paymentforbill(this.value);">
								<option value="" selected="selected" disabled="disabled">Payment For</option>
								<option value="month">Select Month</option>
								<option value="other">Others</option>
							</select>
							<label></label>
							<span class="help-block">Payment For *</span> </div>
					</div>
					<div id="bill_month" class="col-md-4" style="display:none; ">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" name="bill_month">
              <option value="<?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php echo date('F-Y');?>" selected="selected"><?php echo date('F-Y');?></option>
              <option value="<?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>
              </option>
            </select>
						
							<label></label>
							<span class="help-block">Select Month *</span> </div>
					</div>
					<div id="bill_other" class="col-md-4" style="display:none;">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" name="billother" placeholder="Other">
							<label></label>
							<span class="help-block">Other</span> </div>
					</div>
					<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control date-picker" id="delivery_date" onblur="change_date()"  name="delivery_date" placeholder="Delivery Date *" value="<?php echo date("m/d/Y");?>">
						<label></label>
						<span class="help-block">Delivery Date*</span> </div>
				</div>	
				</div>
				<div id="salarypayments" style='display:none;'>
					<h3 class="form-heading col-md-12">Salary Details</h3>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" name="person_name" placeholder="Person Namey">
							<label></label>
							<span class="help-block">Person Name</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" name="designation" placeholder="Designation/Role *">
							<label></label>
							<span class="help-block">Designation/Role *</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" id="payforsal" name="salpayfor" onchange="paymentforsal();">
								<option value="">Payment For</option>
								<option value="month">Select Month</option>
								<option value="other">Others</option>
							</select>
							<label></label>
							<span class="help-block">Payment For *</span> </div>
					</div>
					<div id="sal_month" class="col-md-4" style="display:none">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" name="sal_month">
              <option value="<?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php echo date('F-Y');?>" selected><?php echo date('F-Y');?></option>
              <option value="<?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>
              </option>
            </select>
						
							<label></label>
							<span class="help-block">Select Month *</span> </div>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label col-md-3" id="sal_other" style="display:none;">
						<input type="text" class="form-control" id="form_control_1" name="sal_other" placeholder="Other">
						<label></label>
						<span class="help-block">Other</span> </div>
				</div>
				<!--end div for salary payments-->
				<!--Start of misc payments div-->
				<div id="miscpayments" style='display:none;'>
					<h3 class="form-heading col-md-12">Misc Payment Details</h3>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" name="payment_to1" placeholder="Payment to *">
							<label></label>
							<span class="help-block">Payment to *</span> </div>
					</div>
					<!--<div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="note_misc" >
                <label></label>
                <span class="help-block">Note *</span>
              </div>
          </div>-->
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" id="payfor" name="miscpayfor" onchange="paymentfor();">
								<option value="" selected="selected" disabled="disabled">Payment For</option>
								<option value="month">Select Month</option>
								<option value="other">Others</option>
							</select>
							<label></label>
							<span class="help-block">Payment For *</span> </div>
					</div>
					<div id="misc_month" class="col-md-4" style="display:none;">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" name="misc_month">
              <!--  <option value="<?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>
                  </option> -->
              <option value="<?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php echo date('F-Y');?>" selected><?php echo date('F-Y');?></option>
              <option value="<?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>
              </option>
            </select>
						
							<label></label>
							<span class="help-block">Select Month *</span> </div>
					</div>
					<div class="col-md-4" id="misc_other" style="display:none">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" name="miscother" placeholder="Other">
							<label></label>
							<span class="help-block">Other</span> </div>
					</div>
				</div>
				<!--End of Misc Payment div-->
				<!--Start of Tax/complience div-->
				<div id="taxpayments" style='display:none;'>
					<h3 class="form-heading col-md-12">Tax/Compliance Details</h3>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" name="tax_type" maxlength="3" onkeypress=" return onlyNos(event, this);">
								<option value="" selected="selected" disabled="disabled">Payment For *</option>
								<option value="Tax">Tax</option>
								<option value="Complience">Complience</option>
							</select>
							<label></label>
							<span class="help-block">Payment For *</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" name="payment_to" placeholder="Payment to *">
							<label></label>
							<span class="help-block">Payment to</span> </div>
					</div>
					<!-- <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="tax_payment_to" placeholder="Note">
                <label></label>
                <span class="help-block">Note</span>
              </div>
          </div>-->
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" id="payfortax" name="payfortax" onchange="paymentfortax();">
								<option value="" selected="selected" disabled="disabled">Payment For</option>
								<option value="month">Select Month</option>
								<option value="other">Others</option>
							</select>
							<label></label>
							<span class="help-block">Payment For</span> </div>
					</div>
					<div id="tax_month" class="col-md-4" style="display:none;">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" name="tax_month">
              <!--  <option value="<?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>
              </option> -->
              <option value="<?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php echo date('F-Y');?>" selected><?php echo date('F-Y');?></option>
              <option value="<?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>
              </option>
              <option value="<?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>
              </option>
            </select>
						
							<label></label>
							<span class="help-block">Select Month</span> </div>
					</div>
					<div class="col-md-4" id="tax_other" style="display:none">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" name="tax_other" placeholder="Other">
							<label></label>
							<span class="help-block">Other</span> </div>
					</div>
				</div>
				<!--End of Tax/complience Div-->
				<!--Start of common div-->
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-md-line-input">
								<textarea class="form-control" id="form_control_1" name="description" placeholder="Description"></textarea>
								<label></label>
								<span class="help-block">Description</span> </div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-md-line-input" id="unitClass">
								<textarea autocomplete="off" class="form-control" id="memo" name="memo" placeholder="Memo"></textarea>
								<label></label>
								<span class="help-block">Memo</span> </div>
						</div>
					</div>
				</div>
				 <div class="col-md-12">
			<h3 class="form-heading">Upload Section</h3>
		</div>
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-4"> <!-- Image Upload -->
              <div class="form-group form-md-line-input uploadss">
              <label>Upload purchase order</label>
              <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                <div> 
					<span class="btn default btn-file"> 
						<span class="fileinput-new"> Select image </span> 
						<span class="fileinput-exists"> Change </span> 
						<?php echo form_upload('purchase_doc');?> 
					</span> 
					<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> 
				</div>
              </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input uploadss">
              <label>Upload Other document</label>
              <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('other_doc');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
              </div>
              </div>
            </div>
          </div>
        </div>
				<div class="col-md-12">
					<h4 style="margin:45px 0 10px; color:#3EB9C6; font-size:22px"> Items Details </h4>
					<table class="table table-striped table-hover table-bordered mass-book1" id="items">
						<thead>
							<tr>
								<!--<th width="10%">Image</th>-->
								<th width="25%">Product/ Service Name</th>
								<th width="10%">QTY </th>
								<th width="10%">Unit Price</th>
								<th width="10%">Tax %</th>
								<th width="10%">Discount %</th>
								<th width="10%">Total</th>
								<th width="10%">Class</th>
								<th width="10%">Note</th>
								<th align="center" width="5%">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="form-group">
								<!--<input id="product" name="product[]" onchange="check_duplicate(this.value)" type="text" value="" placeholder="Product/ Service Name *" class="form-control input-sm">-->
								  <div class="typeahead__container">
								<div class="typeahead__field"> <span class="typeahead__query">
               <input onchange="check_duplicate(this.value)"  class="js-typeahead-user_v2 form-control" name="product[query]" id="product"  type="search"  placeholder="Product/ Service Name *"  autocomplete="off">
			  </span></div>
							</div>
							
								</td>
								<td class="form-group"><input id="qty" name="qty[]" type="text" value="" class="form-control input-sm" onkeypress=" return onlyNos(event, this);" placeholder="QTY *">
								</td>
								<td class="form-group"><input id="price" name="price[]" type="text" value="" onkeypress=" return onlyNos(event, this);" class="form-control input-sm" onblur="calculation(this.value)" placeholder="Price *">
								</td>
								<td class="form-group"><input id="tax" name="tax[]" type="text" value="" onkeypress=" return onlyNos(event, this);" onblur="calculation(this.value)" class="form-control input-sm" placeholder="Tax">
								</td>
								<td class="form-group"><input type="text" autocomplete="off" class="form-control input-sm" id="disc" name="disc[]" onblur="calculation(this.value)" placeholder="Discount" onkeypress=" return onlyNos(event, this);">
								</td>
								<td class="form-group"><input type="text" autocomplete="off" class="form-control input-sm" id="total" name="total[]" placeholder="Total *" onkeypress=" return onlyNos(event, this);">
								</td>
								<td class="form-group"><input id="cls" name="cls[]" type="text" value="" placeholder="Class" class="form-control input-sm">
								</td>
								<td class="form-group"><input id="note1" name="note1[]" type="text" value="" placeholder="Note" class="form-control input-sm">
								</td>
								<td><a class="btn blue btn-xs" id="additems"><i class="fa fa-plus" aria-hidden="true"></i></a>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="text-right" style="font-size:20px; color:#666666; border-bottom:1px solid #999999; padding:0 20px 10px 0;"> Total Amount : <i class="fa fa-inr" style="font-size: 80%; color:#CD4B4B;"></i> <span style="color:#CD4B4B;" id="sp">0</span> </div>
				</div>
				<div class="col-md-12">
					<h4 style="margin:45px 0 10px; color:#3EB9C6; font-size:22px"> Account Details </h4>
					<table class="table table-striped table-hover table-bordered table-scrollable mass-book1" id="accounts">
						<thead>
							<tr>
								<!--<th scope="col">
						Select
					</th>-->
								<th scope="col"> Account </th>
								<th scope="col"> Description </th>
								<th scope="col"> Amount </th>
								<th scope="col"> Class </th>
								<th scope="col"> Vendor/Reciver </th>
								<th align="center" scope="col" width="5%"> Action </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="form-group"><input id="account" name="account[]" type="text" value="" placeholder="Account" class="form-control input-sm">
								</td>
								<td class="form-group"><input id="des" name="des[]" type="text" value="" class="form-control input-sm" placeholder="Description *">
								</td>
								</td>
								<td class="form-group"><input id="amount" name="amount1[]" type="text" value="" class="form-control input-sm" placeholder="Amount *">
								</td>
								<td class="form-group"><input type="text" autocomplete="off" class="form-control input-sm" id="cls1" name="cls1[]" placeholder="Class *">
								</td>
								<td class="form-group"><input type="text" autocomplete="off" class="form-control input-sm" id="vendor1" name="vendor1[]" placeholder="Vendor *">
								</td>
								<td><a class="btn blue btn-xs" id="addaccounts"><i class="fa fa-plus" aria-hidden="true"></i></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-12" style="text-align:center; margin-top: 20px;">
					<div class="btn-group">
						<label for="autocellwidth" class="auto_cl btn grey-cascade" id="auto_cl_id" style="background-color:#39B9A1;">
            <input type="checkbox" id="autocellwidth" class="toggle" style="display:none;">
            <span id="disp">Pay Now</span></label>
					
					</div>
				</div>
				<div id="pay" style="display:none;">
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" id="profit_center1" name="profit_center1">
								<option value="" selected="selected" disabled="disabled">Profit Center *</option>
								<?php $pc=$this->dashboard_model->all_pc();?>
								<?php
								$defProfit = $this->unit_class_model->profit_center_default();
								if ( isset( $defProfit ) && $defProfit ) {
									$defPro = $defProfit->profit_center_location;
								} else {
									$defPro = "Select";
								}
								?>
								<option value="<?php echo $defPro;  ?>" selected>
									<?php echo $defPro; ?>
								</option>
								<?php $pc=$this->dashboard_model->all_pc1();
                        foreach($pc as $prfit_center){
                        ?>
								<option value="<?php echo $prfit_center->profit_center_location;?>">
									<?php echo  $prfit_center->profit_center_location;?>
								</option>
								<?php }?>
							</select>
							<label></label>
							<span class="help-block">Profit Center *</span> </div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="pay_amount" name="pay_amount" placeholder="Amount" onchange="chnge_amt(this.value)">
							<label></label>
							<span class="help-block">Amount</span> </div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" id="mop" name="mop" onChange="paym(this.value);">
								<option value="" disabled selected>Mode Of Payment</option>
								<?php $mop = $this->dashboard_model->get_payment_mode_list();
			  if($mop != false)
			  {
				  foreach($mop as $mp)
				  {
				?>
								<option value="<?php echo $mp->p_mode_name; ?>">
									<?php echo $mp->p_mode_des; ?>
								</option>
								<?php }
			  } ?>
							</select>
							<label></label>
							<span class="help-block">Modes of Payments *</span> </div>
					</div>
					
					<div id="cards" style="display:none;">
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<select name="card_type" class="form-control bs-select" placeholder="Card type" id="card_type">
									<option value="" disabled selected>Select Card Type</option>
									<option value="Cr">Credit Card</option>
									<option value="Dr">Debit Card</option>
									<option value="gift">Gift Card</option>
								</select>
								<label></label>
								<span class="help-block">Card type *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" name="card_bank_name" class="form-control input-sm" placeholder="Bank name" id="bankname">
								<label></label>
								<span class="help-block">Bank Name *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" name="card_no" class="form-control input-sm" placeholder="Card no" id="card_no">
								<label></label>
								<span class="help-block">Card NO *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control input-sm" placeholder="Name on Card" name="card_name" id="card_name">
								<label></label>
								<span class="help-block">Name on Card *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<select name="card_expm" class="form-control bs-select" placeholder="Month" id="card_expm">
											<option value="" disabled selected>Select Month</option>
											<?php 
                            $MonthArray = array(
                            "1" => "January", "2" => "February", "3" => "March", "4" => "April",
                            "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                            "9" => "September", "10" => "October", "11" => "November", "12" => "December",);
                            
                                for($i=1;$i<13;$i++)
                                {
                            ?>
											<option value="<?php echo $MonthArray[$i]; ?>">
												<?php echo $MonthArray[$i]; ?>
											</option>
											<?php }  ?>
										</select>
										<label></label>
										<span class="help-block">Exp Month *</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<select name="card_expy" class="form-control bs-select" placeholder="Year" id="card_expy">
											<option value="" disabled selected>Select year</option>
											<?php 
                                for($i=2016;$i<2075;$i++)
                                {
                            ?>
											<option value="<?php echo $i; ?>">
												<?php echo $i; ?>
											</option>
											<?php }  ?>
										</select>
										<label></label>
										<span class="help-block">Exp Year *</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control input-sm" placeholder="CVV" name="card_cvv" id="card_cvv">
										<label></label>
										<span class="help-block">CVV *</span>
									</div>
								</div>
							</div>
						</div>
						

						<!--<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="payment_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_ca">
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Transaction Declined</option>
					 </select>                  
                  </div>
                </div>-->

					</div>
					<div id="fundss" style="display:none;">
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Bank name" name="ft_bank_name" id="bankname_f">
								<label></label>
								<span class="help-block">Bank Name *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control input-sm" placeholder="Acc no" name="ft_account_no" id="ac_no">
								<label></label>
								<span class="help-block">A/C No *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control input-sm" placeholder="IFSC code" name="ft_ifsc_code" id="ifsc">
								<label></label>
								<span class="help-block">IFSC Code *</span>
							</div>
						</div>

						<!--<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="payment_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_f">
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Declined</option>
					 </select>                  
                  </div>
                </div>-->

					</div>
					<div id="cheque" style="display:none;">
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Bank name" name="chk_bank_name" id="bankname_c">
								<label></label>
								<span class="help-block">Bank Name *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control input-sm" placeholder="Cheque no" name="checkno" id="chq_no">
								<label></label>
								<span class="help-block">Cheque No *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control input-sm" placeholder="Drawer Name" name="chk_drawer_name" id="drw_name_c">
								<label></label>
								<span class="help-block">Drawer Name *</span>
							</div>
						</div>

						<!--<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="payment_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_c" >
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Bounced</option>
					 </select>                  
                  </div>
                </div>-->

					</div>
					<div id="draft" style="display:none;">
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Bank name" name="draft_bank_name" id="bankname_d">
								<label></label>
								<span class="help-block">Bank Name *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control input-sm" placeholder="Draft no" name="draft_no" id="drf_no">
								<label></label>
								<span class="help-block">Draft No *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control input-sm" placeholder="Drawer Name" name="draft_drawer_name" id="drw_name_d">
								<label></label>
								<span class="help-block">Drawer Name *</span>
							</div>
						</div>

						<!--<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="payment_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_d" >
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Bounced</option>
					 </select>                  
                  </div>
                </div>-->

					</div>
					<div id="ewallet" style="display:none;">
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Wallet Name" name="wallet_name" id="w_name">
								<label></label>
								<span class="help-block">Wallet Name *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control input-sm" placeholder="Transaction ID" name="wallet_tr_id" id="tran_id">
								<label></label>
								<span class="help-block">Transaction ID *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control input-sm" placeholder="Recieving Acc" name="wallet_rec_acc" id="recv_acc">
								<label></label>
								<span class="help-block">Recieving Acc *</span>
							</div>
						</div>

					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<select name="p_status_w" class="form-control bs-select" placeholder="Payment Status" id="p_status_w">
								<option value="Done" selected>Payment Recieved</option>
								<option value="Pending">Payment Processing</option>
								<option value="Cancel">Transaction Declined</option>
							</select>
							<label></label>
							<span class="help-block">Payment Status *</span>
						</div>
					</div>
					<!--End of fundtranfer under common div-->
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" name="approved_by" placeholder="Approved By *">
							<label></label>
							<span class="help-block">Approved By *</span> </div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" name="recievers_name" placeholder="Reciver Name  *">
							<label></label>
							<span class="help-block">reciver Name *</span> </div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" name="paid_by" placeholder="Reciever Name">
							<label></label>
							<span class="help-block">Paid By</span> </div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" name="recievers_desig" placeholder="Reciever's Designation *">
							<label></label>
							<span class="help-block">Reciever's Designation *</span> </div>
					</div>
				</div>
				<div id="pay1">
					<!-- <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" id="profit_center1" name="profit_center1">
              <option value="" selected="selected" disabled="disabled">Profit Center *</option>
              <?php $pc=$this->dashboard_model->all_pc();?>
              <?php
                      $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
                      ?>
              <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
              <?php $pc=$this->dashboard_model->all_pc1();
                        foreach($pc as $prfit_center){
                        ?>
              <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
              <?php }?>
            </select>
            <label></label>
            <span class="help-block">Profit Center *</span> </div>
        </div>-->
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control date-picker" id="payments_due_date" name="payments_due_date" placeholder="payments Due Date" required="required">
							<label></label>
							<span class="help-block">Payments Due Date</span> </div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" readonly id="payments_due" onmouseover="check_due(this.value)" name="payments_due" placeholder="Payments Due">
							<input autocomplete="off" type="hidden" class="form-control" readonly id="payments_due1" name="payments_due1" placeholder="Payments Due">
							<label></label>
							<span class="help-block">Payments Due</span> </div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-actions right">
			<input type="submit" class="btn submit" value="Submit">
			<!-- 18.11.2015  -- onclick="return check_mobile();" -->
			<button type="reset" class="btn default">Reset</button>
		</div>
		<?php form_close(); ?>
	</div>

	<input autocomplete="off" type="hidden" class="form-control" id="vendor_id" name="vendor_id">
	<input autocomplete="off" type="hidden" class="form-control" id="chng_pay_amt" name="chng_pay_amt">
	<script>
	function change_date(date){
	var pay_trm=$('#paymentt').val();
	var d_date=$('#delivery_date').val();
	if(pay_trm=='cod'){
		$("#bill_due_date").val(d_date);
		$("#payments_due_date").val(d_date);
	}
	}
		function paym( val ) {
			if ( val == 'cards' ) {
				document.getElementById( 'cards' ).style.display = 'block';
			} else {
				document.getElementById( 'cards' ).style.display = 'none';
			}
			if ( val == 'fund' ) {
				document.getElementById( 'fundss' ).style.display = 'block';
			} else {
				document.getElementById( 'fundss' ).style.display = 'none';
			}
			if ( val == 'cheque' ) {
				document.getElementById( 'cheque' ).style.display = 'block';
			} else {
				document.getElementById( 'cheque' ).style.display = 'none';
			}
			if ( val == 'draft' ) {
				document.getElementById( 'draft' ).style.display = 'block';
			} else {
				document.getElementById( 'draft' ).style.display = 'none';
			}
			if ( val == 'ewallet' ) {
				document.getElementById( 'ewallet' ).style.display = 'block';
			} else {
				document.getElementById( 'ewallet' ).style.display = 'none';
			}
			if ( val == 'cash' ) {
				document.getElementById( 'cardss' ).style.display = 'none';
				document.getElementById( 'fundss' ).style.display = 'none';
				document.getElementById( 'ewallet' ).style.display = 'none';
				document.getElementById( 'cheque' ).style.display = 'none';
				document.getElementById( 'draft' ).style.display = 'none';
			}
		}

		function check_due( val ) {
			//alert(val);
			var due = $( "#payments_due" ).val( val ).val();
			var pay_amt = $( '#pay_amount' ).val();
			var ch_pay_amt = $( '#chng_pay_amt' ).val();

			//alert(ch_pay_amt);
			if ( val == due ) {
				$( "#paments_status" ).attr( 'value', '0' ).change();
			}
		}
		$( document ).ready( function () {
			cellWidth = 100;
			cellWidthSpec = $( this ).is( ":checked" ) ? "Auto" : "Fixed";

		} );

		$( "#autocellwidth" ).click( function () {
			cellWidth = 100; // reset for "Fixed" mode
			cellWidthSpec = $( this ).is( ":checked" ) ? "Auto" : "Fixed";
			
			document.getElementById( 'auto_cl_id' ).style.backgroundColor = $( this ).is( ":checked" ) ? "#297CAC" : "#39B9A1";
			var a = $( this ).is( ":checked" ) ? "Pay Later" : "Pay Now";
			if ( a == 'Pay Later' ) {
				document.getElementById( 'pay' ).style.display = 'block';
				document.getElementById( 'pay1' ).style.display = 'block';
			} else {

				document.getElementById( 'pay' ).style.display = 'none';
				document.getElementById( 'pay1' ).style.display = 'block';
			}

			$( '#disp' ).text( a );

		} );



		function typesofpayments() {
			var x = document.getElementById( "top" ).value;

			if ( x == "Bill payments" ) {
				document.getElementById( "billpayments" ).style.display = "block";
			} else {
				document.getElementById( "billpayments" ).style.display = "none";
			}
			if ( x == "Salary/Wage Payments" ) {
				document.getElementById( "salarypayments" ).style.display = "block";
			} else {
				document.getElementById( "salarypayments" ).style.display = "none";
			}
			if ( x == "Miscellaneous Payments" ) {
				document.getElementById( "miscpayments" ).style.display = "block";
			} else {
				document.getElementById( "miscpayments" ).style.display = "none";
			}
			if ( x == "Tax/Compliance Payment" ) {
				document.getElementById( "taxpayments" ).style.display = "block";
			} else {
				document.getElementById( "taxpayments" ).style.display = "none";
			}
		}

		function modesofpayments() {
			var y = document.getElementById( "mop" ).value;
			//alert(y);

			if ( y == "check" ) {
				document.getElementById( "check" ).style.display = "block";
			} else {
				document.getElementById( "check" ).style.display = "none";
			}
			if ( y == "draft" ) {
				document.getElementById( "draft" ).style.display = "block";
			} else {
				document.getElementById( "draft" ).style.display = "none";
			}
			if ( y == "fund" ) {
				document.getElementById( "fundtransfer" ).style.display = "block";
			} else {
				document.getElementById( "fundtransfer" ).style.display = "none";
			}
		}

		function paymentfor() {
			var y = document.getElementById( "payfor" ).value;
			//alert(y);
			if ( y == "month" ) {
				document.getElementById( "misc_month" ).style.display = "block";
				document.getElementById( "misc_other" ).style.display = "none";
			}
			if ( y == "other" ) {
				document.getElementById( "misc_other" ).style.display = "block";
				document.getElementById( "misc_month" ).style.display = "none";
			}
		}

		function paymentforsal() {
			var y = document.getElementById( "payforsal" ).value;

			if ( y == "month" ) {
				document.getElementById( "sal_month" ).style.display = "block";
				document.getElementById( "sal_other" ).style.display = "none";
			}
			if ( y == "other" ) {
				document.getElementById( "sal_other" ).style.display = "block";
				document.getElementById( "sal_month" ).style.display = "none";
			}
		}

		function paymentforbill() {
			var y = document.getElementById( "payforbill" ).value;
			//alert(y);
			if ( y == "month" ) {
				document.getElementById( "bill_month" ).style.display = "block";
				document.getElementById( "bill_other" ).style.display = "none";
			}
			if ( y == "other" ) {
				document.getElementById( "bill_other" ).style.display = "block";
				document.getElementById( "bill_month" ).style.display = "none";
			}
		}




		function paymentterm() {
			var y = document.getElementById( "paymentt" ).value;
			var bill_date = $( "#bill_date" ).val();
			if(bill_date==''){
				swal("Please Select Bill Date !", "", "warning")
			}else{
		

			if ( y == "other" ) {
				document.getElementById( "bill_other_dt" ).style.display = "block";
			} else if ( y == "immediate" ) {
				// alert(bill_date);
				var someDate = new Date();
				someDate.setDate( someDate.getDate()  );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;

				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				//alert(someDate);
				document.getElementById( "bill_other_dt" ).style.display = "none";
			}
			else if ( y == "pia" ) {
				// alert(bill_date);
				var someDate = new Date();
				someDate.setDate( someDate.getDate());
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;

				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				//alert(someDate);
				document.getElementById( "bill_other_dt" ).style.display = "none";
			}
			
			else if ( y == "net7" ) {
				// alert(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 7 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;

				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				//alert(someDate);
				document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "net14" ) {
				// alert(bill_date);
				///var someDate = new Date(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 14 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;
				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				// alert(joindate);
				document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "net30" ) {
				// alert(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 30 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;
				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				// alert(joindate);
				document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "net60" ) {
				// alert(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 60 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;
				$( '#bill_due_date' ).val( fromdate1 );
				// alert(joindate);
				document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "net90" ) {
				// alert(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 90 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;
				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				// alert(joindate);
				document.getElementById( "bill_other_dt" ).style.display = "none";
			}
		} //sj
		}
		function paymentfortax() {
			var y = document.getElementById( "payfortax" ).value;
			// alert(y);
			if ( y == "month" ) {
				document.getElementById( "tax_month" ).style.display = "block";
				document.getElementById( "tax_other" ).style.display = "none";
			}
			if ( y == "other" ) {
				document.getElementById( "tax_other" ).style.display = "block";
				document.getElementById( "tax_month" ).style.display = "none";
			}
		}

		function removeRow( id, total ) {

			swal( {
				title: "Are you sure?",
				text: "Do you want to remove the row!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			}, function () {
				swal( "Deleted!", "Yes delete the row.", "success" );
				$( '#row_' + id ).remove();
			} );

			var total1 = parseFloat( $( '#sp' ).text() );
			//alert(total);
			//alert(total1);
			//alert(new_sum);
			new_sum = parseFloat( total1 ) - parseFloat( total );

			$( '#sp' ).text( new_sum );
			$( '#payments_due' ).val( new_sum );

		}

		var sum = 0;
		var flag = 0;
		var flag1 = 0;

		$( "#additems" ).click( function () {

			var x = 0;
			var product = $( '#product' ).val();
			var qty = $( '#qty' ).val();
			var price = $( '#price' ).val();
			var tax = $( '#tax' ).val();
			var total = $( '#total' ).val();
			var disc = $( '#disc' ).val();
			var cls = $( '#cls' ).val();
			var note = $( '#note1' ).val();
			if ( tax == '' ) {
				tax = 0;
			}
			if ( disc == '' ) {
				disc = 0;
			}
			if ( cls == '' ) {
				cls = 0;
			}
			if ( product != '' && qty != '' && price != '' ) {

				//$('#ids').val( it+','+ i_name) ;
				$( '#items tbody tr:first' ).before( '<tr id="row_' + flag + '" class="ss">' +
					'<td><input name="product1[]" id="product1' + flag + '"  type="text" value="' + product + '" class="form-control input-sm" ></td>' +
					'<td><input name="qty1[]" id="qty1' + flag + '" type="text" value="' + qty + '" onblur="calculation1(' + flag + ');recalculationNB()" class="form-control input-sm" ></td>' +
					'<td><input name="price1[]" id="price1' + flag + '" type="text" value="' + price + '" onblur="calculation1(' + flag + ');recalculationNB()" class="form-control input-sm" ></td>' +
					'<td><input name="tax1[]" id="tax1' + flag + '" type="text" value="' + tax + '" onblur="calculation1(' + flag + ')" class="form-control input-sm" ></td>' +

					'<td><input name="disc1[]" id="disc1' + flag + '" type="text" value="' + disc + '" onblur="calculation1(' + flag + ')" class="form-control input-sm" ></td>' +
					'<td><input name="total1[]" id="total1' + flag + '"  type="text" value="' + total + '" class="form-control input-sm" ></td>' +

					'<td><input name="cls1[]" id="cls1' + flag + '" type="text" value="' + cls + '" class="form-control input-sm" ></td>' +
					'<td><input name="note11[]" id="note11' + flag + '" type="text" value="' + note + '" class="form-control input-sm" ></td>' +
					'<td><a href="javascript:void(0);" class="btn red btn-xs" onclick="removeRow(' + flag + ',' + total + ')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>' );

				x = x + 1;

				sum = sum + parseFloat( $( '#total1' + flag ).val() );
				$( '#sp' ).text( sum );
				flag++;

				var product = $( '#product' ).val( '' );
				$( '#qty' ).val( '' );
				$( '#price' ).val( '' );
				$( '#tax' ).val( '' );
				$( '#total' ).val( '' );
				$( '#disc' ).val( '' );
				$( '#cls' ).val( '' );
				$( '#note1' ).val( '' );
				$( "#payments_due" ).val( sum );
				$( "#payments_due1" ).val( sum );
				toastr.success( 'Added Successfully! -', 'success!' );

			} else {
				swal(
					'',
					' Please Enter a value !',
					'warning'
				)
			}
		} );


		$( "#addaccounts" ).click( function () {

			var x1 = 0;
			var account = $( '#account' ).val();
			var des = $( '#des' ).val();
			var amount = $( '#amount' ).val();
			var cls1 = $( '#cls1' ).val();
			var vendor1 = $( '#vendor1' ).val();



			if ( account != '' && des != '' && amount != '' && cls1 != '' && vendor != '' ) {
				$( '#accounts tbody tr:first' ).before( '<tr id="row_' + flag1 + '" class="ss">' +
					'<td><input name="account[]" id="contract_start_date' + flag1 + '" type="text" value="' + account + '" class="form-control input-sm" readonly></td>' +
					'<td><input name="des[]" id="contract_start_date' + flag1 + '" type="text" value="' + des + '" class="form-control input-sm" readonly></td>' +
					'<td><input name="amount[]" id="contract_start_date' + flag1 + '" type="text" value="' + amount + '" class="form-control input-sm" readonly></td>' +
					'<td><input name="cls1[]" id="contract_start_date' + flag1 + '" type="text" value="' + cls1 + '" class="form-control input-sm" readonly></td>' +

					'<td><input name="vendor1[]" id="contract_start_date' + flag1 + '" type="text" value="' + vendor1 + '" class="form-control input-sm" readonly></td>' +

					'<td><a href="javascript:void(0);" class="btn red btn-xs" onclick="removeRow(' + flag1 + ')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>' );
				x1 = x1 + 1;
				flag1++;
				var product = $( '#product' ).val( '' );
				$( '#account' ).val( '' );
				$( '#des' ).val( '' );
				$( '#tax' ).val( '' );
				$( '#amount' ).val( '' );
				$( '#cls1' ).val( '' );
				$( '#vendor1' ).val( '' );

			} else {
				alert( 'enter value' );
			}
		} );


		function calculation( val ) {
			var qty = $( '#qty' ).val();
			var price = $( '#price' ).val();
			var tax = $( '#tax' ).val();
			var disc = $( '#disc' ).val();

			var amount = qty * parseFloat( price ) + ( qty * parseFloat( price ) * tax / 100 );
			var t_amount = amount - ( qty * parseFloat( price ) * disc / 100 );
			if ( val == 0 ) {
				$( "#total" ).val( amount );
			} else {
				$( "#total" ).val( t_amount );
			}
		}


		function calculation1( id ) {
			//alert(id);
			var qty = $( '#qty1' + id ).val();
			var price = $( '#price1' + id ).val();
			var tax = $( '#tax1' + id ).val();
			var disc = $( '#disc1' + id ).val();

			var amount = qty * parseFloat( price ) + ( qty * parseFloat( price ) * tax / 100 );
			var t_amount = amount - ( qty * parseFloat( price ) * disc / 100 );
			$( "#total1" + id ).val( t_amount );
		}

		function recalculationNB() {

			var i = 0;
			var total = 0;
			var count = $( '#items tbody tr' ).length;
			//alert(count);
			for ( i = 0; i < count - 1; i++ ) {

				if ( parseInt( $( '#total1' + i ).val() ) > 0 ) {


					total = parseInt( total ) + parseInt( $( '#total1' + i ).val() );

				} else {

					break;
				}


			}


			//	alert('Total ' +total);
			$( "#sp" ).text( total );
			$( "#payments_due" ).text( total );
			$( "#payments_due1" ).text( total );

		}


		function show_modal() {
			$( '#basic1' ).modal( 'show' );
		}


		function set_vendor( a, b, c, d ) {
			//alert(d)
			$( "#select_l_itm" ).val( a );
			$( "#address" ).val( b );
			$( "#ph_no" ).val( c );
			$( "#vendor_id" ).val( d );

			$( '#basic1' ).modal( 'toggle' );

		}
	</script>
	<script>
		function search_vendor() {

			var vendor_name = $( "#check_vendor" ).val();



			$.ajax( {
				type: "POST",
				url: "<?php echo base_url(); ?>dashboard/search_vendor",
				data: {
					data: vendor_name
				},
				success: function ( data ) {
					if ( data != "" ) {
						//alert(data);
						return false;
						$( "#services" ).show();
						$( '#services' ).html( data );
						//alert($('#service').html());
					}
				}
			} );
		}

		function chnge_amt( val ) {
			if ( val == '' ) {
				val = 0;
			}

			var due_pay = $( '#payments_due1' ).val();
			var pay_amt = $( '#pay_amount' ).val();

			var due_amt = parseFloat( due_pay ) - parseFloat( val );
			if ( parseFloat( val ) <= due_pay ) {
				var pay_due = $( "#payments_due" ).val( due_amt );
				var pay_amt1 = $( '#payments_due' ).val();
				//alert(pay_amt1);
			} else {

				swal( "Amount must be lesser then Due amount!", "", "error" )
				$( '#pay_amount' ).val( '' );
				//pay_amt=$('#pay_amount').val(due_pay);

			}

			if ( due_amt == 0 ) {
				$( '#payments_due_date' ).css( 'display', 'none' );
			} else {
				$( '#payments_due_date' ).css( 'display', 'block' );
			}
		}

		function check_date() {

			var a = $( '#bill_date' ).val();
			
			a = new Date( a );
			var b = $( '#bill_due_date' ).val();
			b = new Date( b );
			if ( a >= b && b != '' ) {
				// alert('End Date sould getter tan Start Date ');
				swal( "Due Date sould getter than Bill Date!", "", "error" )
				$( '#bill_due_date' ).val( '' );
			}


		}

		function check_duplicate( val ) {
			$.ajax( {
				type: "POST",
				url: "<?php echo base_url(); ?>dashboard/duplicate_product",
				data: {
					data: val
				},
				success: function ( data ) {

					if ( data.data == "y" ) {
						swal( "Duplicate product name!", "", "error" )
						$( '#product' ).val( '' );
					}
				}
			} );
		}
		
	$.typeahead( {
			input: '.js-typeahead-user_v2',
			minLength: 1,
			order: "asc",
			dynamic: true,
			delay: 200,
			backdrop: {
				"background-color": "#fff"
			},

			source: {
				project: {
					display: "name",
					ajax: [ {
						type: "GET",
						url: "<?php echo base_url();?>dashboard/all_fuel_stock_itm",
						data: {
							q: "{{query}}"
						}
					}, "data.name" ],
					template: '<div class="clearfix">' +
						
						'<div class="project-information">' +
						'<span class="pro-name" style="font-size:15px; "> {{name}}</span>' +
						
						'</div>' +
						'</div>'
				}
			},
			callback: {
				onClick: function ( node, a, item, event ) {
				//	alert( JSON.stringify( item ) );
					if(isNaN(item.uprice)){
						$("#price").val('');
					}else{		
					$("#price").val(item.uprice);
					}

				},
				onSendRequest: function ( node, query ) {
					console.log( 'request is sent' )

				},
				onReceiveRequest: function ( node, query ) {
					//console.log('request is received')
					
				}
			},
			
			debug: true
		} );		
	</script>
	<script>
		$.typeahead( {
			input: '.js-typeahead-user_v1',
			minLength: 1,
			order: "asc",
			dynamic: true,
			delay: 200,
			backdrop: {
				"background-color": "#fff"
			},

			source: {
				project: {
					display: "name",
					ajax: [ {
						type: "GET",
						url: "<?php echo base_url();?>dashboard/search_vendor",
						data: {
							q: "{{query}}"
						}
					}, "data.name" ],
					template: '<div class="clearfix">' +
						'<span class="project-img">' +
						'<img src="{{image}}">' +
						'</span>' +
						'<div class="project-information">' +
						'<span class="pro-name" style="font-size:15px; "> {{name}}</span>' +
						'<span class="pro-name" style="font-size:12px; color:#000"><i class="fa fa-map-marker" aria-hidden="true"></i> {{city}}</span>' +
						'<span class="pro-name" style="font-size:12px; color:#666"><i class="fa fa-briefcase" aria-hidden="true"></i> {{industry}} - {{field}} </span>' +
						//'<span class="pro-name" style="font-size:15px;"> {{contracted}}</span>'+
						//'<span class="pro-name" style="font-size:15px;"> {{parent_vendor}}</span>'+
						//'<span class="pro-name" style="font-size:15px;"> {{icon}}</span>'+
						'</div>' +
						'</div>'
				}
			},
			callback: {
				onClick: function ( node, a, item, event ) {
				
					$( "#vendor_id" ).val( item.id );
					$( "#address" ).val( item.address );
					$( "#ph_no" ).val( item.ph_no );
					$( "#email" ).val( item.email );
				},
				onSendRequest: function ( node, query ) {
					console.log( 'request is sent' )

				},
				onReceiveRequest: function ( node, query ) {
					$( "#vendor_id" ).val( '' );
					$( "#address" ).val( '' );
					$( "#ph_no" ).val( '' );
					$( "#email" ).val( '' );

				}
			},
			debug: true
		} );
		
		
	
		

		$( document ).ready( function () {
			toastr.optionsOverride = 'positionclass = "toast-bottom-right"';
			toastr.options.positionClass = 'toast-bottom-right';

		} )
	</script>