<!-- BEGIN PAGE CONTENT-->
<style>
.ds .required {
	color: #e02222;
	font-size: 12px;
	padding-left: 2px;
}
.ds .form-group.form-md-line-input .form-control ~ label {
	width: 94%;
	left: 19px;
}
.ds .form-group.form-md-line-input {
	margin-bottom: 15px;
}
.ds .lt {
	line-height: 38px;
}
.ds .tld {
	margin-bottom: 10px !important;
	padding-top: 10px;
}
.tld_in {
	background-color: #f8f8f8;
	width: 100%;
	float: left;
	padding-top: 7px;
}
.tld_in:hover {
	background-color: #f1f1f1;
}
.ds .dropzone .dz-default.dz-message {
	width: 100%;
	height: 50px;
	margin-left: 0px;
	margin-top: 0px;
	top: 0;
	left: 0;
	font-size: 50%;
}
.ds .dropzone {
	min-height: 130px;
}
.form-group.form-md-line-input.form-md-floating-label.col-md-6.full {
	width: 100%;
}
.form-group.form-md-line-input.form-md-floating-label.col-md-6.add {
	width: 100%;
}
.form-group.form-md-line-input.form-md-floating-label.col-md-6.rght {
	width: 50%;
	margin-top: -85px;/* float: right; */
}
.form-group.form-md-line-input.form-md-floating-label.col-md-6.dwn {
	width: 51%;
	float: right;
}
</style>
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
<div class="row ds">
  <div class="col-md-12">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green"> <i class="icon-pin font-green"></i> <span class="caption-subject bold uppercase"> Bill Payments</span> </div>
      </div>
      <div class="portlet-body form">
        <?php

                            $form = array(
                                'class' 			=> 'form-body',
                                'id'				=> 'form',
                                'method'			=> 'post',								
                            );
							
							

                            echo form_open_multipart('dashboard/add_bill_payments',$form);

                            ?>
        <div class="form-body"> 
          <!-- 17.11.2015-->
          <?php if($this->session->flashdata('err_msg')):?>
          <div class="form-group">
            <div class="col-md-12 control-label">
              <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
            </div>
          </div>
          <?php endif;?>
          <?php if($this->session->flashdata('succ_msg')):?>
          <div class="form-group">
            <div class="col-md-12 control-label">
              <div class="alert alert-success alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
            </div>
          </div>
          <?php endif;?>
          <!-- 17.11.2015-->
          <div class="row">
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <input autocomplete="off" type="text" class="form-control date-picker" id="date" name="date" required="required">
              <label>Date<span class="required">*</span></label>
            </div>
            <div class="form-group form-md-line-input col-md-6">
              <select class="form-control"  id="top" name="top"  required="required" onchange="typesofpayments();">
                <option value="">---Types Of Payment---</option>
                <option value="Billpayments">Bill Payments</option>
                <option value="Salary/WagePayments">Salary/Wage Payments</option>
                <option value="MiscellaneousPayments">Miscellaneous Payments</option>
              </select>
              <label>Types Of Payments<span class="required">*</span></label>
            </div>
            <div id="billpayments" style='display:none; background:#EDEDED ; float:left; width:100%; padding-bottom:10px;'>
              <div class="col-md-12">
                <h3>Bill Payments</h3>
                <h4>Payment For</h4>
              </div>
              <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="vendor_name" >
                <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                <label> Authority/Vendor Name<span class="required">*</span></label>
                <span class="help-block">Enter Authority/Vendor name...</span> </div>
              <div id="c_choice" class="form-group form-md-line-input col-md-4">
                <select class="form-control" name="bill_month">
                  <option value="<?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>
                  </option>
                  
                  <option value="<?php echo date('F-Y');?>" selected="selected"><?php echo date('F-Y');?></option>
                  <option value="<?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>
                  </option>
                </select>
                <label>Select Month <span class="required">*</span></label>
              </div>
              <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="billother" >
                
                <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                <label>other</label>
                <span class="help-block"></span> </div>
            </div>
            <!--div for salary payments-->
            <div id="salarypayments"  style='display:none; background:#EDEDED ; float:left; width:100%; padding-bottom:10px;'>
              <div class="col-md-12">
                <h3>Salary Payments</h3>
                <h4>Payment For</h4>
              </div>
              <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="person_name">
                <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                <label> Person Name<span class="required">*</span></label>
                <span class="help-block">Enter the person name to whom salary/wage to be paid...</span> </div>
              <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="designation">
                
                <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                <label>Designation/Role<span class="required">*</span></label>
                <span class="help-block">Enter the designation or role of the person...</span> </div>
              <div id="salary_month" class="form-group form-md-line-input col-md-3">
                <select class="form-control" name="sal_month">
                  <option value="<?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php echo date('F-Y');?>" selected><?php echo date('F-Y');?></option>
                  <option value="<?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>
                  </option>
                </select>
                <label>Select Month <span class="required">*</span></label>
              </div>
              <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <input type="text" class="form-control" id="form_control_1" name="sal_other" >
                
                <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                <label>other</label>
                <span class="help-block"></span> </div>
            </div>
            <!--end div for salary payments--> 
            
            <!--Start of misc payments div-->
            
            <div id="miscpayments" style='display:none; background:#EDEDED ; float:left; width:100%; padding-bottom:10px;'>
              <div class="col-md-12">
                <h3>Miscellaneous Payments</h3>
              </div>
              <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="payment_to" >
                
                <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                <label>Payment to:<span class="required">*</span></label>
                <span class="help-block">Enter the Name of the person to whom the payment will be done...</span> </div>
              <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="payment_for" >
                
                <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                <label>Payment for:<span class="required">*</span></label>
                <span class="help-block">Enter the pupose of the payment...</span> </div>
              <div id="m_misc" class="form-group form-md-line-input col-md-3">
                <select class="form-control" name="misc_month">
                  <!--  <option value="<?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>
                  </option> -->
                  <option value="<?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php echo date('F-Y');?>" selected><?php echo date('F-Y');?></option>
                  <option value="<?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>
                  </option>
                  <option value="<?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>
                  </option>
                </select>
                <label>Select Month <span class="required">*</span></label>
              </div>
              <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="miscother">
                
                <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                <label>other</label>
                <span class="help-block"></span> </div>
            </div>
            <!--End of Misc Payment div--> 
            
            <!--Start of common div-->
            <div id="common">
              <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="description" >
                
                <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                <label> Description</label>
                <span class="help-block">Enter Description of transanction...</span> </div>
              <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="amount" required="required">
                
                <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                <label>Amount<span class="required">*</span></label>
                <span class="help-block">Enter amount of the transanction...</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <select class="form-control"  id="mop" name="mop" required="required" onchange="modesofpayments();">
                  <option value="">---Mode Of Payment---</option>
                  <option value="cash">Cash</option>
                  <option value="check">Check</option>
                  <option value="draft">Draft</option>
                  <option value="fundtransfer">Fund Tranfer</option>
                </select>
                <label>Modes of Payments<span class="required">*</span></label>
              </div>
              <!--Payment mode by check-->
              <div id="check" style='display:none'>
                <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="checkno">
                  
                  <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                  <label>Check no.<span class="required">*</span></label>
                  <span class="help-block">Enter check no...</span> </div>
                <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="check_bank_name">
                  
                  <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                  <label>Bank Name<span class="required">*</span></label>
                  <span class="help-block">Enter Bank Name...</span> </div>
              </div>
              <!--End div of Payment mode by check--> 
              <!--Start of payment mode by draft-->
              <div id="draft" style='display:none'>
                <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="draft_no">
                  
                  <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                  <label>Draft no.<span class="required">*</span></label>
                  <span class="help-block">Enter draft no...</span> </div>
                <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="draft_bank_name" >
                  
                  <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                  <label>Bank Name<span class="required">*</span></label>
                  <span class="help-block">Enter Bank Name...</span> </div>
              </div>
              <!--End of payment mode by draft--> 
              <!--Start of fundtranfer under common div-->
              <div id="fundtransfer" style='display:none'>
                <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_bank_name">
                  
                  <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                  <label>Bank Name.<span class="required">*</span></label>
                  <span class="help-block">Enter Bank Name...</span> </div>
                <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_account_no" >
                  
                  <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                  <label>Account No<span class="required">*</span></label>
                  <span class="help-block">Enter Account no...</span> </div>
                <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_ifsc_code" >
                  
                  <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                  <label>IFSC Code<span class="required">*</span></label>
                  <span class="help-block">Enter IFSC code...</span> </div>
              </div>
              <!--End of fundtranfer under common div-->
              <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="approved_by" required="required">
                
                <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                <label>Approved By<span class="required">*</span></label>
                <span class="help-block">Enter Approval...</span> </div>
              <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="recievers_name" required="required">
                
                <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                <label>Reciever Name:<span class="required">*</span></label>
                <span class="help-block">Enter Recievers Name...</span> </div>
              <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="recievers_desig" required="required">
                
                <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
                <label>Reciever's Designation:<span class="required">*</span></label>
                <span class="help-block">Enter Recievers Designation...</span> </div>
            </div>
            <!--End of common div--> 
            
            <!--<div class="form-group form-md-line-input form-md-floating-label col-md-4">
									
										<input type="text" class="form-control" id="form_control_1" >
										<label id="form_control_1">Others <span class="required">*</span></label>
										<span class="help-block">Enter Others Option...</span>
									</div>--> 
            <!-- <div class="form-group form-md-line-input form-md-floating-label col-md-6 dwn">
									<p><strong>Upload Photo</strong><span class="required">*</span></p>
			<!--<div action="http://localhost/hotel//assets/dashboard/assets/global/plugins/dropzone/upload.php" class="dropzone" id="my-dropzone">
					</div>--> <!-- 17.11.2015 --> 
            <!--<?php echo form_upload('image_photo');?>--> 
            <!-- 17.11.2015 --> 
          </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-6 rght"> 
            <!-- <p><strong>Upload ID Proof</strong><span class="required">*</span></p>
                                      <?php echo form_upload('image_idpf');?> --> 
            <!--<div action="http://localhost/hotel//assets/dashboard/assets/global/plugins/dropzone/upload.php" class="dropzone" id="my-dropzone">
					</div>--> 
            <!-- 17.11.2015 --> 
            
            <!-- 17.11.2015 --> 
          </div>
        </div>
        <div class="form-actions noborder">
          <button type="submit" class="btn blue" >Submit</button>
          <!-- 18.11.2015  -- onclick="return check_mobile();" -->
          <button  type="reset" class="btn default">Reset</button>
        </div>
      </div>
      <?php form_close(); ?>
      
      <!-- END CONTENT --> 
    </div>
  </div>
</div>
</div>
</div>
<script>
   function check_Cylinders(value){
   		
       if(value =="Cylinders"){

           document.getElementById("c_choice").style.display="block";
       }else{
           document.getElementById("c_choice").style.display="none";
       }
      	if(value =="Rotation"){

           document.getElementById("r_choice").style.display="block";
       }else{
           document.getElementById("r_choice").style.display="none";
       }
       if(value =="Design"){

           document.getElementById("d_choice").style.display="block";
       }else{
           document.getElementById("d_choice").style.display="none";
       }
   }
   function typesofpayments()
   {
	  var x= document.getElementById("top").value;
	   
	   if(x=="Billpayments"){
		   document.getElementById("billpayments").style.display="block";
	   }else{
		   document.getElementById("billpayments").style.display="none";
	   }
	   if(x=="Salary/WagePayments"){
		   document.getElementById("salarypayments").style.display="block";
	   }else{
		   document.getElementById("salarypayments").style.display="none";
	   }
	   if(x=="MiscellaneousPayments"){
		   document.getElementById("miscpayments").style.display="block";
	   }else{
		   document.getElementById("miscpayments").style.display="none";
	   }
   }
   function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	   //alert(y);
	    
	   if(y=="check"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fundtransfer"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }
   
</script> 
