
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Booking Nature Visit</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('unit_class_controller/add_booking_nature_visit',$form);

                        ?>
    <div class="form-body"> 
      <!-- 17.11.2015-->
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <!-- 17.11.2015-->
      
      <div class="row">
      	<div class="col-md-4">
     	<div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" name="name" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Nature visit name *">
          <label></label>
          <span class="help-block">Nature visit name *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="form_control_1"  name="desc" placeholder="Description">
          <label></label>
          <span class="help-block">Description</span> </div>
       </div>
       <div class="col-md-4">
       <div class="form-group form-md-line-input">
        <select onchange="check(this.value)" class="form-control"  id="default" name="default" required="required" >
        	<option value="" selected="selected" disabled="disabled">Default nature visit</option>
          <option value="0">No</option>
          <option value="1">Yes</option>
        </select>
        <label></label>
        <span class="help-block">Default nature visit *</span>
      </div>
       </div>
      </div>
      </div>
      <div class="form-actions right">
        <button type="submit" class="btn blue" >Submit</button>
        <!-- 18.11.2015  -- onclick="return check_mobile();" -->
        <button  type="reset" class="btn default">Reset</button>
      </div>
      <?php form_close(); ?>
      <!-- END CONTENT --> 
    </div>
  </div>
<script language="JavaScript">

   function check(value){
		//alert(value);
		
			if(value==1){	
          $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/check_nature_visit_default",
                data:{a:value},
                 success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                 

				swal({   title: "Are you sure?",
				text: data+"!",  
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Fix as Default!",   cancelButtonText: "No, cancel plz!", 
				closeOnConfirm: true,  
				closeOnCancel: true },
				function(isConfirm){   
				if (isConfirm) {     
				$("#default").val('1');
				} else {   
				$("#default").val('0');
				} });



				 
                }
            });
			}
		  
		  
		  //});
		
	}
	

		
 $(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});   
</script> 

