
<script type="text/javascript">
	
	$(document).ready(function(){
    	 $("#chk").hide();
		 $("#b_agency_name").hide()
		 //$("#ag_name").attr("required","false");
		 $("#ct_name").attr("required","true");	
    });
	
	function hideShow(t)
	{
		if($('#agency_yn').val() == '5')
		{
			$("#chk").show();
			$("#b_agency_name").show();
			$("#b_contact_name").hide();
			$("#ct_name").removeAttr("required");
			$("#ag_name").attr("required","true");
		 	//$("#ct_name").attr("required","false");
		}
		else
		{
			$("#chk").hide();
			$("#b_agency_name").show();
			$("#b_contact_name").show();
			//$("#ag_name").attr("required","false");
		 	$("#ct_name").attr("required","true")
			$("#ag_name").removeAttr("required");
			$("#ag_name").val("");
		}
	}
	
</script>
<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Broker</span> </div>
  </div>
  <div class="portlet-body form">
  <?php

                    $form = array(
                        'class' 			=> '',
                        'id'				=> 'form',
                        'method'			=> 'post'
                    );

                    echo form_open_multipart('dashboard/add_broker',$form);

                    ?>
  
    <div class="form-body">      
      <div class="row">
      	<div class="col-md-4">
        <div class="form-group form-md-line-input">
          <select class="form-control bs-select" id="agency_yn" required name="b_agency" onchange="hideShow();" >
          	<option value="" disabled="disabled" selected="selected">Agency? *</option>
            <option value="6">No</option>
            <option value="5">Yes</option>
          </select>
          <label></label>
          <span class="help-block">Agency? *</span>
        </div>
       	</div>
        <div class="col-md-4" id="chk" style="display:none;">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="ag_name" name="b_agency_name" onkeypress="return onlyLtrs(event, this);" placeholder="Agency Name *">
          <label></label>
          <span class="help-block">Agency Name *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="ct_name" name="b_name"  onkeypress="return onlyLtrs(event, this);" placeholder="Contact Name *">
          <label></label>
          <span class="help-block">Contact Name *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="b_address" required="required" placeholder="Address *">
          <label></label>
          <span class="help-block">Address *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="b_contact" required="required" onkeypress="return onlyNos(event, this);" maxlength="10" placeholder="Mobile No *">
          <label></label>
          <span class="help-block">Mobile No *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  type="email" autocomplete="off" class="form-control" id="form_control_1" name="b_email" placeholder="Email *">
          <label></label>
          <span class="help-block">Email *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="form_control_1" name="b_website" placeholder="Website *">
          <label></label>
          <span class="help-block">Website *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="form_control_1" name="b_pan" placeholder="Pan Card No.">
          <label></label>
          <span class="help-block">Pan Card No.</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="form_control_1" name="b_bank_acc_no"  onkeypress="return onlyNos(event, this);" placeholder="Bank Account no.">
          <label></label>
          <span class="help-block">Bank Account no.</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="b_bank_ifsc" placeholder="IFSC Code *">
          <label></label>
          <span class="help-block">IFSC Code *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="b_commission" placeholder="Commission %">
          <label></label>
          <span class="help-block">Commission %</span> </div>
        </div>
        <div class="col-md-12">
			<div class="row">
				<div class="col-md-4">
				<div class="form-group form-md-line-input uploadss">
				  <label>Upload Photo</label>
				  <div class="fileinput fileinput-new" data-provides="fileinput">
					<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
					<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
					<div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('image_photo');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
				  </div>
				</div>
				</div>
				<div class="col-md-4">
				<div class="form-group form-md-line-input uploadss">
				  <label>Upload Document or photo</label>
				  <div class="fileinput fileinput-new" data-provides="fileinput">
					<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
					<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
					<div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('doc_photo');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
				  </div>
				</div>
				</div>
			</div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn submit">Submit</button>
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>