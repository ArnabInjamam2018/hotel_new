<!-- BEGIN PAGE CONTENT-->
<!--<script src="../../../../assets/common/js/jquery.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$(document).ready(function(){
    	 $("#chk").hide();
		 $("#b_agency_name").hide()
		 //$("#ag_name").attr("required","false");
		 $("#ct_name").attr("required","true");	
    });
	
	function hideShow(t)
	{
		if($('#agency_yn').val() == '5')
		{
			$("#chk").show();
			$("#b_agency_name").show();
			$("#b_contact_name").hide();
			$("#ct_name").removeAttr("required");
			$("#ag_name").attr("required","true");
		 	//$("#ct_name").attr("required","false");
		}
		else
		{
			$("#chk").hide();
			$("#b_agency_name").show();
			$("#b_contact_name").show();
			//$("#ag_name").attr("required","false");
		 	$("#ct_name").attr("required","true")
			$("#ag_name").removeAttr("required");
			$("#ag_name").val("");
		}
	}
	
</script>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Channel</span> </div>
  </div>
  <div class="portlet-body form">
  <?php

                $form = array(
                    'class' 			=> '',
                    'id'				=> 'form',
                    'method'			=> 'post'
                );

                echo form_open_multipart('dashboard/add_channel',$form);

                ?>
  
    <div class="form-body">
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <div class="row">
      	<div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="ct_name" name="channel_name"  onkeypress="return onlyLtrs(event, this);" placeholder="Channel Name *">
          <label></label>
          <span class="help-block">Channel Name *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="ct_name" name="channel_contact_name"  onkeypress="return onlyLtrs(event, this);" placeholder="Contact Name *">
          <label></label>
          <span class="help-block">Contact Name *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="channel_address" required="required"  placeholder="Address *">
          <label></label>
          <span class="help-block">Address *</span> </div>
       	</div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="channel_contact" required="required" onkeypress="return onlyNos(event, this);" maxlength="10" placeholder="Mobile No *">
          <label></label>
          <span class="help-block">Mobile No *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  type="email" autocomplete="off" class="form-control" id="form_control_1" name="channel_email"  placeholder="Email">
          <label></label>
          <span class="help-block">Email</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="form_control_1" name="channel_website" placeholder="Website">
          <label></label>
          <span class="help-block">Website</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="form_control_1" name="channel_pan" placeholder="Pan Card No.">
          <label></label>
          <span class="help-block">Pan Card No.</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="form_control_1" name="channel_bank_acc_no"  onkeypress="return onlyNos(event, this);" placeholder="Bank Account No.">
          <label></label>
          <span class="help-block">Bank Account No.</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="channel_bank_ifsc" placeholder="IFSC Code *">
          <label></label>
          <span class="help-block">IFSC Code</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="channel_commission" placeholder="Commission %">
          <label></label>
          <span class="help-block">Commission %</span> </div>
        </div>
        <div class="col-md-3">
        <div class="form-group form-md-line-input uploadss">
          <label>Upload Photo</label>
          <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/>
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
            </div>
            <div>
                <span class="btn default btn-file">
                <span class="fileinput-new">
                Select image </span>
                <span class="fileinput-exists">
                Change </span>
                <?php echo form_upload('image_photo');?> 
                </span>
                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                Remove </a>
            </div>
        </div>
        </div>
        </div>
		<div class="col-md-3">
        <div class="form-group form-md-line-input uploadss">
          <label>Upload Photo or Document</label>
          <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/>
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
            </div>
            <div>
                <span class="btn default btn-file">
                <span class="fileinput-new">
                Select image </span>
                <span class="fileinput-exists">
                Change </span>
                <?php echo form_upload('doc_photo');?> 
                </span>
                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                Remove </a>
            </div>
        </div>
        </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn submit">Submit</button>
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>
