<!-- BEGIN PAGE CONTENT-->
<script type="text/javascript">
	
	$(document).ready(function(){
    $("form").submit(function(){
        
		$.valid_from = Date.parse($('#c_valid_from').val());
		$.valid_upto = Date.parse($('#c_valid_upto').val());
		$.renewal = Date.parse($('#c_renewal').val());
		$.value = $.now();
		
		if($.valid_from > $.value)
		{
			alert('Valid Date Should Be Lesser Than The System Date');
			return false;
		}else if($.valid_upto < $.value)
		{
			alert('Valid Upto Date Should Be Greater Than The Todays Date');
			return false;
		}
		else if ($.renewal < $.value)
		{
			alert('Renewal Date Should Be Greater Than The Todays Date');
			return false;
		}
		else
		{
			return true;
		}
		
    	});
        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);		
		$('.date-picker').datepicker({
                orientation: "left",
				startDate: today,
                autoclose: true,
				todayHighlight: true
		});
	
});

</script>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Certificate</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                $form = array(
                'class' 			=> '',
                'id'				=> 'form',
                'method'			=> 'post'
                );

                echo form_open_multipart('dashboard/add_compliance',$form);

                ?>
    <div class="form-body">
      <div class="row">
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select required="required" class="form-control bs-select" id="form_control_2" name="c_valid_for" >
            	<option disabled="disabled" selected="selected">Valid for</option>
              <option value="Self">Self </option>
              <option value="Hotel">Hotel</option>
            </select>
            <label></label>
            <span class="help-block">Valid for *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" id="form_control_2">
              <?php $hotels=$this->dashboard_model->all_hotels();
                            if(isset($hotels) && $hotels){
                                foreach($hotels as $hotel){

                                    ?>
              <option value="<?php echo $hotel->hotel_id ?>"><?php echo $hotel->hotel_name ?></option>
              <?php }} ?>
            </select>
            <label></label>
            <span class="help-block">Hotel Name *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" name="c_name" required="required" placeholder="Certificate Name *">
            <label></label>
            <span class="help-block">Certificate Name *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" name="c_authority" required="required" placeholder="Certificate Authority *">
            <label></label>
            <span class="help-block">Certificate Authority *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" name="c_owner" required="required" placeholder="Certificate Owner *">
            <label></label>
            <span class="help-block">Certificate Owner *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" onchange="other(this.value)" id="form_control_2" name="c_type1">
            <?php if(isset($cirtificate) && ($cirtificate)){
               // print_r($cirtificate);
                foreach($cirtificate as $cir){?>
              <option value="<?php echo $cir->type; ?>"><?php echo $cir->type;?></option>  
            <?php }}?>		

                <option value="others">Others</option> 
            </select>
            <label></label>
            <span class="help-block">Certificate Type *</span> </div>
        </div>
        <div class="col-md-3" id="Certificate" style="display:none;">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" name="c_type" placeholder="Others Certificate Type">
            <label></label>
            <span class="help-block">Others Certificate Type</span> </div>
        </div>
        
        
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" id="form_control_2" name="c_importance">
              <option value="y">Low</option>
              <option value="o">Medium</option>
              <option value="r">High</option>
            </select>
            <label></label>
            <span class="help-block">Importance *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" required="required" name="c_valid_from" class="form-control date-picker "  id="c_valid_from" placeholder="Valid From *">
            <label></label>
            <span class="help-block">Valid From *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" required="required" name="c_valid_upto" class="form-control date-picker" id="c_valid_upto" placeholder="Valid Up to *">
            <label></label>
            <span class="help-block">Valid Up to *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" name="c_renewal" required="required" class="form-control date-picker" id="c_renewal" placeholder="Renewal Date *">
            <label></label>
            <span class="help-block">Renewal Date *</span> </div>
        </div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
            <textarea class="form-control" name="c_description" required="required" placeholder="Description *"></textarea>
            <label></label>
            <span class="help-block">Description *</span> </div>
        </div>
		<div class="col-md-12">
		<div class="col-md-3">
              <div class="form-group form-md-line-input uploadss">
                <label>Primary Notification period (Days) <span class="required">*</span></label>
                <input name="c_primary" class="knob" data-width="40%" value="50" data-rotation="clockwise">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group form-md-line-input uploadss">
                <label>Secondary  Notification period (Hours) <span class="required">*</span></label>
                <input name="c_secondary" class="knob" data-width="40%" data-fgColor="#66EE66" data-rotation="clockwise" value="35">
              </div>
            </div>
		
        
          
            <div class="col-md-3">
              <div class="form-group form-md-line-input uploadss">
                <label>Upload Photo</label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                  			<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                  <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
                    <input type="file" name="image_certificate" />
                    </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                </div>
                <!--<input type="file" name="image_certificate" />--> 
                <!-- <div action="<?php //echo base_url();?>/assets/dashboard/assets/global/plugins/dropzone/upload.php" class="dropzone" id="my-dropzone">--> 
              </div>
            </div>
            
          
		   
            <div class="col-md-3">
              <div class="form-group form-md-line-input uploadss">
                <label>Upload Photo 2</label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                  			<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                  <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
                    <input type="file" name="image_certificate2" />
                    </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                </div>
                <!--<input type="file" name="image_certificate" />--> 
                <!-- <div action="<?php //echo base_url();?>/assets/dashboard/assets/global/plugins/dropzone/upload.php" class="dropzone" id="my-dropzone">--> 
              </div>
            </div>
            
          
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn submit" >Submit</button>
      <button type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
  </div>
</div>
<script>
function other(id){
	if(id=='others'){
		$('#Certificate').show();
	}
	else{
		$('#Certificate').hide();
	}
}
</script>