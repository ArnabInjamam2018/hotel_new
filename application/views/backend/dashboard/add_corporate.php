<!-- 17.11.2015-->
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <!-- 19.07.2016-->
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-industry"></i> <span class="caption-subject bold uppercase"> Add Corporate</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/add_corporate',$form);

                        ?>
    <div class="form-body"> 
      <div class="row">
		<div class="col-md-12">
			<h3 class="form-heading">Company Details</h3>
			
			<!--<hr style="height:0.5px; border:none; color:#AAAAAA; background-color:#AAAAAA;">-->
		</div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input ">
              <input autocomplete="off" type="text" class="form-control" id="company_name" name="company_name" onkeypress=" return onlyLtrs(event, this);"  placeholder="Company Name">
              <label></label>
              <span class="help-block">Company Name</span>
            </div>
        </div >
		<div class="col-md-4">
			<div class="form-group form-md-line-input">
              <select class="form-control bs-select" name="reg_type" id="reg_type" required >
                <option value="" disabled="disabled" selected="selected">Select incorporation types</option>
                <option value="propitor">propitor</option>
				<option value="Partnership">Partnership</option>
				<option value="LLC">LLC</option>
				<option value="Priivately Limited">Priivately Limited</option>
				<option value="Limited">Limited</option>
              </select>
              <label></label>
              <span class="help-block">Types of Reg</span>
			</div>
		</div>
		
		<?php 
		$industry=$this->unit_class_model->all_industry_type();
		?>
		<div class="col-md-4">
			<div class="form-group form-md-line-input">
              <select class="form-control bs-select" name="industry_type" id="industry_type" required >
			  <option value="0" >Select industry type</option>
			  <?php

			  if(isset($industry) && $industry){
				  foreach($industry as $ind){			  
			  
			  ?>
                <option value="<?php echo $ind->it_name ?>" ><?php echo $ind->it_name ?></option>
			  <?php }} ?>
              </select>
              <label></label>
              <span class="help-block">Industry type</span>
			</div>
		</div>	  
		<div class="col-md-4"> 
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="field" name="field" required="off" placeholder="Field of work">
              <label></label>
              <span class="help-block">Field of work</span>
            </div> 
        </div>
		
		<div class="col-md-4">
			<div class="form-group form-md-line-input">
              <select class="form-control bs-select" name="rating" id="rating" >
			  <option value="0" >Select Star Rating</option>			  
                <option value="1" >1 Star</option>
                <option value="2" >2 Star</option>
                <option value="3" >3 Star</option>
                <option value="4" >4 Star</option>
                <option value="5" >5 Star</option>			  
              </select>
              <label></label>
              <span class="help-block">Star Rating</span>
			</div>
		</div>	

		<div class="col-md-4"> 
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="no_branch" name="no_branch" onkeypress=" return onlyNos(event, this);"  placeholder="No of Branch">
              <label></label>
              <span class="help-block">No of Branch</span> 
            </div> 
        </div>
		
		<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control" id="employees_no" name="employees_no" onkeypress=" return onlyNos(event, this);"  placeholder="No of Employee">
              <label></label>
              <span class="help-block">No of Employee</span>
            </div>
        </div>
			
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control" id="address" name="address" placeholder="Address*">
              <label></label>
              <span class="help-block">Address</span> 
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control" id="pin_code" onblur="fetch_all_address()" name="pin_code" placeholder="pin code">
              <label></label>
              <span class="help-block">pin code</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control" id="city" name="city" placeholder="City">
              <label></label>
              <span class="help-block">City</span>
            </div>
        </div>
		
		<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control  " id="state" name="state" placeholder="state">
              <label></label>
              <span class="help-block">state</span>
            </div>
        </div>
        <div class="col-md-4"> 
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="country" name="country" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Country">
              <label></label>
              <span class="help-block">Country</span> 
            </div> 
        </div>				<div class="col-md-4"> 
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="gstin" name="gstin"  placeholder="GSTIN No">
              <label></label>
              <span class="help-block">GSTIN No</span> 
            </div> 
        </div>
		</br>		</br>
		<div class="col-md-12"></div>
		<div class="col-md-12">
			<h3 class="form-heading">Contact Details</h3>
			<!-- <hr style="height:0.5px; border:none; color:#AAAAAA; background-color:#AAAAAA;"> -->
		</div>
		
		<div class="col-md-4"> 
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="contatc_person" name="contatc_person" onkeypress=" return onlyLtrs(event, this);"  placeholder="Contact Person">
              <label></label>
              <span class="help-block">Contact Person</span> 
            </div> 
        </div>
		
		<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control  " id="contact_no" name="contact_no" placeholder="Contract No">
              <label></label>
              <span class="help-block">Contract No</span>
            </div>
        </div>
		
        <div class="col-md-4"> 
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="corp_email" name="corp_email"  placeholder="Contact Email">
              <label></label>
              <span class="help-block">Contact Email</span> 
            </div> 
        </div>
		
		<div class="col-md-4"> 
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="manager_name" name="manager_name"  placeholder="Manager Name">
              <label></label>
              <span class="help-block">Manager Name</span> 
            </div> 
        </div>
		
		<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control  " id="manager_no" name="manager_no" placeholder="Manager Contact no">
              <label></label>
              <span class="help-block">Manager Contact no</span>
            </div>
        </div>
        
		
		<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control  " id="corp_manager_email" name="corp_manager_email" placeholder="Manager Email">
              <label></label>
              <span class="help-block">Manager Email</span>
            </div>
        </div>
		
		<div class="col-md-4"> 
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="director_name" name="director_name"  placeholder="Director Name">
              <label></label>
              <span class="help-block">Director Name</span> 
            </div> 
        </div>
		
		<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control  " id="director_no" name="director_no" placeholder="Director Contact no">
              <label></label>
              <span class="help-block">Director Contact no</span>
            </div>
        </div>
		
		<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control  " id="corp_director_email" name="corp_director_email" placeholder="Director Email">
              <label></label>
              <span class="help-block">Director Email</span>
            </div>
        </div>
        
		<div class="col-md-12"></div>
		<div class="col-md-12" style="padding-top:25px;">
		
			<h3 class="form-heading">Define Discount Rules</h3>
			
		</div>
		
     <div class="col-md-12" style="padding-top:5px;">
          <div class="">
            <table class="table table-striped table-bordered table-hover table-scrollable" id="items">
              <thead>
                <tr>
                  <th width="20%">Contract Start Date</th>
                  <th width="20%">Contract End Date </th>
                  <th width="20%">Discount Rule</th>
                   <th width="10%">Default Apply?</th>
				  <th align="center" width="5%">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="form-group"><input id="i_name" type="hidden"  class="form-control input-sm " >
                    <input id="contract_start_date" name="contract_start_date[]" type="text" onchange="check_date()"  class="form-control input-sm date-picker" placeholder="Enter Start Date" tabindex="1" autocomplete="off">
                    <div id="charges"></div></td>
                  <td class="form-group"><input id="contract_end_date" name="contract_end_date[]" onchange="check_date()" type="text" value="" placeholder="Enter End Date" class="form-control input-sm date-picker"></td>
                  <td class="hidden-480 form-group">
				  <?php 
					$discount_rule=$this->unit_class_model->discount_rules();					
				  ?>
				  <select class="form-control"   name="contract_discount_start[]" id="contract_discount_start" placeholder="Discount Rule" >
				  <option value="0"  selected="">Select Discount Rule</option>
				  <?php foreach($discount_rule as $rule){ ?>
					<option value="<?php echo $rule->id;?>"><?php echo $rule->rule_name?></option>
				 
                  <?php } ?>
                </select>
				</td>
				
				<td class="hidden-480 form-group">
					<select class="form-control"   name="default_rule[]" id="default_rule" placeholder="Discount Rule" >
					  <option value="0" selected="">No</option>				 
					  <option value="1">Apply</option>
					  
					</select>
				</td>
				
				
                  <td align="center"><a class="btn green btn-sm"  id="additems"><i class="fa fa-plus" aria-hidden="true"></i></a></td>
                </tr>
               
              </tbody>
            </table>
          </div>
        </div>
		
		<div class="col-md-12">
			<h3 class="form-heading">Upload Section</h3>
		</div>
		<div class="col-md-4">
              <div class="form-group form-md-line-input uploadss">
                <label>Upload Image</label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                  <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select Vendor image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('vendor_image');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                </div>
              </div>
          </div>
			
            <div class="col-md-4">
              <div class="form-group uploadss" style="padding-top: 20px; margin:0;">
                <label>Upload File</label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                	<div class="input-group input-large">
                      <!--<input type="file" name="pdf" />
                      <span class="fileinput-new"> Select Vendor file </span> --> 
                        <div class="form-control uneditable-input input-fixed" data-trigger="fileinput">
                            <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename">
                            </span>
                        </div>
                        <span class="input-group-addon btn default btn-file">
                        <span class="fileinput-new">
                        Select file </span>
                        <span class="fileinput-exists">
                        Change </span>
                        <input type="file" name="pdf" />
                        </span>
                        <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">
                        Remove </a> 
                	</div>                
                </div>
              </div>
            </div>
		
		
      </div>
      </div>
      <div class="form-actions right">
        <button type="submit" class="btn submit" >Submit</button>
        <!-- 18.11.2015  -- onclick="return check_mobile();" -->
        <button  type="reset" class="btn default">Reset</button>
      </div>
	  
	  <input type="hidden" name="hid">
      <?php form_close(); ?>
      <!-- END CONTENT --> 
    
  </div>
</div>
<script>
var flag=0,flag1=0;
function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});

function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	   //alert(y);
	    
	   if(y=="check"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fundtransfer"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }
   var ff=flag;
  var check=$('#contract_start_date'+ff).val();
  var a=$('#contract_end_date').val();
  //if(check<=a){
  
  
   $("#additems").click(function(){
	   $a=$('#contract_start_date').val();
	   $b=$('#contract_end_date').val();
	   $c=$('#contract_discount_start').val();
	   $d=$('#contract_discount_end').val();
	  $e=$('#default_rule').val();
	  //alert($e);
	  
	   if($a!='' && $b!='' && $c!='' && $d!='' && $e!=''){
	var x=0;
	var contract_start_date = $('#contract_start_date').val();
	var contract_end_date =$('#contract_end_date').val();
	var contract_discount_start = $('#contract_discount_start').val();
	var default_rule =$('#default_rule').val();
	
	//var contract_discount_end = $('#contract_discount_end').val();
	
	
	//$('#ids').val( it+','+ i_name) ;
	$('#items tr:first').after('<tr id="row_'+flag+'"><td>'+
	'<input name="contract_start_date[]" id="contract_start_date'+flag+'" type="text" value="'+contract_start_date+'" class="form-control input-sm" readonly></td><td><input name="contract_end_date[]" id="contract_end_date'+flag+'" type="text" value="'+contract_end_date+'" class="form-control input-sm" readonly></td><td class="hidden-480"><input name="contract_discount_start[]" id="contract_discount_start'+flag+'" type="text" value="'+contract_discount_start+'" class="form-control input-sm" readonly></td><td class="hidden-480"><input name="default_rule[]" id="contract_discount_start'+flag+'" type="text" value="'+default_rule+'" class="form-control input-sm" readonly></td><td><a  class="btn red btn-sm" onclick="removeRow('+flag+')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>');	
	
	
	
	x=x+1;
	flag1=flag;
	//alert(flag);
	flag++;
	   $('#contract_start_date').val('');
	   $('#contract_end_date').val('');
	   $('#contract_discount_start').val('');
	   //$('#contract_discount_end').val('');
	   }
	   else{
		   alert("Enter data");
	   }
		});
		
function check_date(){
	
	var a=$('#contract_start_date').val();
	 a=new Date(a);
	   var b=$('#contract_end_date').val();
	   b=new Date(b);
	   if(a>=b && b!='' ){
		   alert('End Date sould getter tan Start Date ');
		   $('#contract_end_date').val('');
	   }
	  
	  var c=$('#contract_end_date'+(flag-1)).val();
	 // alert(c);
	    c=new Date(c);
		if(c>a){
			
			alert('Please enter valid date');
			$('#contract_start_date').val('');
			$('#contract_end_date').val('');
		}
		
	  
	   
}

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pin_code').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("country").focus();
			$('#country').val(data.country);
			document.getElementById("state").focus();
			$('#state').val(data.state);
			document.getElementById("city").focus();
			$('#city').val(data.city);
		}

	});
}
	   
	   

</script> 
<script>function removeRow(a){
		$('#row_'+a).remove();
		
	}</script>