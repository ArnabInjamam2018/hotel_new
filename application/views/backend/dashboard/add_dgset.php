<!-- BEGIN PAGE CONTENT-->
<script>
function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add DG set</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/add_dgset',$form);

                        ?>
    <div class="form-body"> 
      <!-- 17.11.2015-->
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <!-- 17.11.2015-->
      <div class="row">
      	<div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="hidden" name="dgset_id" >
          <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="gen_name"  required="required" placeholder="Generator Name *">
          <label></label>
          <span class="help-block">Generator Name *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="asset_code" required="required" placeholder="Asset Code *">
          <label></label>
          <span class="help-block">Asset Code *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control" id="make" name="make"  required="required" onblur="fetch_all_address()" onkeypress=" return onlyLtrs(event, this);" placeholder="Make *">
              <label></label>
              <span class="help-block">Make *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="model" name="model" value="" required= "required" placeholder="Model *"/>
              <label></label>
              <span class="help-block">Model *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <select class="form-control bs-select"  name="ratings" required>
            <option value="" selected="selected" disabled="disabled">Select Ratings</option>
            <?php foreach($rating as $rat) 
            {
                $value=$rat->Rating." ".$rat->Unit;?>
            <option value="<?php echo $value;?>"><?php echo $value; ?></option>
            <?php } ?>
          </select>
          <label></label>
          <span class="help-block">Select Ratings *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <select class="form-control bs-select"  name="fuel" required >
            <option value="" selected>Select Fuel</option>
            <?php foreach($master_log as $mas) 
          {
    echo "<option value='".$mas->fuel_name."'>".$mas->fuel_name."</option>";
          }
    ?>
          </select>
          <label></label>
          <span class="help-block">Fuel *</span> </div>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-12">
          <h4>Engine Details</h4>
        </div>
        <div class="col-md-4">
            <div id="c_choice" class="form-group form-md-line-input">
              <select class="form-control bs-select" name="cylinder_choice">
              	<option value="" selected="selected" disabled="disabled">Select Number of Cylinders</option>
                <option value="4">4</option>
                <option value="6">6</option>
                <option value="8">8</option>
                <option value="12">12</option>
              </select>
              <label></label>
              <span class="help-block">Select Number of Cylinders *</span>
            </div>
        </div>
        <div class="col-md-4">
            <div id="r_choice" class="form-group form-md-line-input">
              <select class="form-control bs-select" name="Rotation_choice">
              	<option value="" selected="selected" disabled="disabled">Select Rotation</option>
                <option value="CW">CW</option>
                <option value="CWW">CWW</option>
              </select>
              <label></label>
              <span class="help-block">Select Rotation *</span>
            </div>
        </div>
        <div class="col-md-4">
            <div id="d_choice" class="form-group form-md-line-input">
              <select class="form-control bs-select" name="Design_choice">
              	<option value="" selected="selected" disabled="disabled">Design *</option>
                <option value="Water Cooled"> Water Cooled</option>
                <option value="Direct Inject"> Direct Inject</option>
              </select>
              <label></label>
              <span class="help-block">Design *</span>
            </div> 
        </div>  
        <div class="col-md-4"> 
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="alternator_make" required="required" placeholder="Alternator Make *">
          <label></label>
          <span class="help-block">Alternator Make *</span> </div>
        </div>
        <div class="col-md-4"> 
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="fuel_consumption" name="fuel_consumption" onkeyup="chk_if_num('fuel_consumption');"  required="required" placeholder="Fuel Consumption Per Litre *">
              <label></label>
              <span class="help-block">Fuel Consumption Per Litre *</span> </div>
        </div>
        <div class="col-md-4"> 
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select" id="form_control_2" name="status" required>
              	<option value="" disabled="disabled" selected="selected">Select Status *</option>
                <option value="Active">Active</option>
                <option value="Faulty">Faulty</option>
                <option value="Under Maintanance">Under Maintanance</option>
              </select>
              <label></label>
              <span class="help-block">Select Status *</span>
            </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control date-picker" id="amc-d" name="amc" placeholder="Next AMC Date">
          <label></label>
          <span class="help-block">Next AMC Date *</span> </div>
        </div>
        <div class="col-md-12">
            <div class="form-group form-md-line-input uploadss">
                <label>Upload Image</label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                  <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('dg_image');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn blue" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
  </div>
  <!-- END CONTENT --> 
</div>
<script>
function chk_if_num( a )
		{
			//alert($('#'+a+'').val());
				if(isNaN($('#'+a+'').val())==true)
				{
					//alert ("please enter a valid "+a);
					$('#'+a+'').val("");
					
				}			
		}
		
   function check_Cylinders(value){
   		
       if(value =="Cylinders"){

           document.getElementById("c_choice").style.display="block";
       }else{
           document.getElementById("c_choice").style.display="none";
       }
      	if(value =="Rotation"){

           document.getElementById("r_choice").style.display="block";
       }else{
           document.getElementById("r_choice").style.display="none";
       }
       if(value =="Design"){

           document.getElementById("d_choice").style.display="block";
       }else{
           document.getElementById("d_choice").style.display="none";
       }
   }
   


</script> 
