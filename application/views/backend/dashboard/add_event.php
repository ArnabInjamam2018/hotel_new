<!-- BEGIN PAGE CONTENT-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/dashboard/Colorpicker/farbtastic.css">
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/Colorpicker/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/Colorpicker/farbtastic.js" type="text/javascript"></script>

<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Event</span> </div>
  </div>
  <div class="portlet-body form"> 
    <?php if($this->session->flashdata('err_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-danger alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
    <?php if($this->session->flashdata('succ_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-success alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
    <?php

            $form = array(
                'class' 			=> '',
                'id'				=> 'form',
                'method'			=> 'post'
            );

            echo form_open_multipart('dashboard/add_event',$form);

            ?>
    <div class="form-body">
      <div class="row">
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" name="e_name" required="required" placeholder="Event Name *">
          <label></label>
          <span class="help-block">Event Name *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input" onclick="getfocus()">
          <input type="text" autocomplete="off" required="required" name="e_from" class="form-control date-picker"  id="c_valid_from" placeholder="Event From *">
          <label></label>
          <span class="help-block">Event From *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" required="required" name="e_upto" class="form-control date-picker" id="c_valid_upto" placeholder="Event Up to *">
          <label></label>
          <span class="help-block">Event Up to *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-checkboxes form-md-line-input">
            <div class="md-checkbox">
                <input type="checkbox" id="checkbox1" class="md-check" name="e_notify" value="1">
                <label for="checkbox1">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>
                Notify While Taking Booking? </label>
            </div>
        </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input last">
          <input type="text" name="e_event_color" required="required" placeholder="Mention the Color You want to Apply To Your Event *" id="pickcolor" class="form-control call-picker">
          <div class="color-holder call-picker"></div>
          <div class="color-picker" id="color-picker" style="display: none"></div>
          <label></label>
          <span class="help-block">Mention the Color You want to Apply To Your Event *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input last">
          <input type="text" name="e_event_text_color" required="required" placeholder="Mention the Color You want to Apply To Your Text *" id="pickcolor2" class="form-control call-picker2">
          <div class="color-holder2 call-picker2"></div>
          <div class="color-picker" id="color-picker2" style="display: none"></div>
          <label></label>
          <span class="help-block">Mention the Color You want to Apply To Your Text *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-checkboxes form-md-line-input">
            <div class="md-checkbox">
                <input type="checkbox" id="checkbox2" class="md-check" name="e_seasonal" value="1">
                <label for="checkbox2">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>
                Is Seasonal Event? </label>
            </div>
        </div>
      </div>
    </div>
  	</div>
  <div class="form-actions right">
    <button type="submit" class="btn blue" >Submit</button>
    <button type="submit" class="btn default">Reset</button>
  </div>
  <?php form_close(); ?>
  </div>
</div>
<div class="clearfix">
<?php  $this->session->flashdata('succ_msg') ?>
</div>
<!-- END PAGE CONTENT--> 
<!-- END CONTENT -->
<script>
    var colorList = ['000000', '993300', '333300', '003300', '003366', '000066', '333399', '333333',
        '660000', 'FF6633', '666633', '336633', '336666', '0066FF', '666699', '666666', 'CC3333', 'FF9933', '99CC33', '669966', '66CCCC', '3366FF', '663366', '999999', 'CC66FF', 'FFCC33', 'FFFF66', '99FF66', '99CCCC', '66CCFF', '993366', 'CCCCCC', 'FF99CC', 'FFCC99', 'FFFF99', 'CCffCC', 'CCFFff', '99CCFF', 'CC99FF', 'FFFFFF'
    ];
    var picker = $('#color-picker');

    for (var i = 0; i < colorList.length; i++) {
        picker.append('<li class="color-item" data-hex="' + '#' + colorList[i] + '" style="background-color:' + '#' + colorList[i] + ';"></li>');
    }

    $('body').click(function() {
        picker.fadeOut();
    });

    $('.call-picker').click(function(event) {
        event.stopPropagation();
        picker.fadeIn();
        picker.children('li').hover(function() {
            var codeHex = $(this).data('hex');

            $('.color-holder').css('background-color', codeHex);
            $('#pickcolor').val(codeHex);
        });
    });

    var picker2 = $('#color-picker2');

    for (var i2 = 0; i2 < colorList.length; i2++) {
        picker2.append('<li class="color-item" data-hex="' + '#' + colorList[i2] + '" style="background-color:' + '#' + colorList[i2] + ';"></li>');
    }

    $('body').click(function() {
        picker2.fadeOut();
    });

    $('.call-picker2').click(function(event) {
        event.stopPropagation();
        picker2.fadeIn();
        picker2.children('li').hover(function() {
            var codeHex = $(this).data('hex');

            $('.color-holder2').css('background-color', codeHex);
            $('#pickcolor2').val(codeHex);
        });
    });
</script> 
<script>
 function getfocus() {
	//alert("ulala")
    document.getElementById("c_valid_from").focus();
	//e.prevent();
}

</script>