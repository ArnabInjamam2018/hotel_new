<!-- BEGIN PAGE CONTENT-->

<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Charges</span> </div>
  </div>
  <div class="portlet-body form"> 
    <?php if($this->session->flashdata('err_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-danger alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
    <?php if($this->session->flashdata('succ_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-success alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
    <?php

            $form = array(
                'class' 			=> '',
                'id'				=> 'form',
                'method'			=> 'post'
            );

            echo form_open_multipart('dashboard/add_charges',$form);

            ?>
    <div class="form-body">
      <div class="row">
	  
        <div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="name" name="name" required="required" placeholder="Name *" onblur="get_chrage_name(this.value)">
          <label></label>
          <span class="help-block">Name *</span> </div>
        </div>
        
		<div class="col-md-3">
        <div class="form-group form-md-line-input">
					<select class="form-control bs-select"  name="tag" >
					 
					  <?php 
						$tag = $this->dashboard_model->get_all_charge_tag();
						if(isset($tag)){
							foreach($tag as $tag){
								echo '<option value="'.$tag->extra_charge_tag_id.'">'.$tag->extra_charge_tag_name.'</option>';
							}	
						}
					  ?>
					 
					</select>
          <label></label>
          <span class="help-block">Category Tag</span> </div>
        </div>
		
		<div class="col-md-3">
        <div class="form-group form-md-line-input" onclick="getfocus()">
          <input type="text" autocomplete="off" required="required" name="charge_description" class="form-control"  id="charge_description" placeholder="Charge Description *">
          <label></label>
          <span class="help-block">Charge Description *</span> </div>
        </div>
        
		<div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input type="number" autocomplete="off" required="required" name="u_price" class="form-control" id="u_price" placeholder="Unit Price *" >
          <label></label>
          <span class="help-block">Unit Price *</span> </div>
        </div>
		
		<div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input type="number" autocomplete="off" required="required" name="c_sgst" class="form-control" id="c_sgst" placeholder="Charges SGST % *">
          <label></label>
          <span class="help-block"> SGST *</span> </div>
        </div>
		<div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input type="number" autocomplete="off" required="required" name="c_cgst" class="form-control" id="c_cgst" placeholder="Charges CGST % *">
          <label></label>
          <span class="help-block">CGST *</span> </div>
        </div>
		<div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input type="number" autocomplete="off" required="required" name="c_igst" class="form-control" id="c_igst" placeholder="Charges IGST % *">
          <label></label>
          <span class="help-block">IGST *</span> </div>
        </div>
		<div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input type="number" autocomplete="off" name="c_utgst" class="form-control" id="c_utgst" placeholder="Charges UTGST %">
          <label></label>
          <span class="help-block">UTGST</span> </div>
        </div>
		<div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" required="required" name="c_tax" class="form-control" id="c_tax" placeholder="Charges Total Tax *">
          <label></label>
          <span class="help-block"> Total Tax *</span> </div>
        </div>
        
        	<div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" required="required" name="sac_code" class="form-control" id="sac_code" placeholder="HSN/ SAC CODE">
          <label></label>
          <span class="help-block"> HSN/ SAC CODE</span> </div>
        </div>
		
		<div class="col-md-3">
				  <div class="form-group form-md-line-input">
					<select class="form-control bs-select"  name="c_tax_type" id="c_tax_type" >
					<option value="">Select </option>
					 <?php  if(isset($tax_rule_extra) && $tax_rule_extra){
						foreach($tax_rule_extra as $ext){

					 ?>
					  <option value="<?php echo $ext->tc_id; ?>"><?php echo $ext->tc_name ?></option>
					 
					 <?php }
					 } ?>
					</select>
					<label></label>
					<span class="help-block">Tax type*</span> 
				  </div>
				</div>
		<div class="col-md-3">
				  <div class="form-group form-md-line-input">
					<select class="form-control bs-select"  name="u_p_editable" >
					 
					  <option value="1">Yes</option>
					  <option value="0">No</option>
					 
					</select>
					<label></label>
					<span class="help-block">Unit Price Editable? *</span> </div>
				</div>
		<div class="col-md-3">
				  <div class="form-group form-md-line-input">
					<select class="form-control bs-select"  name="tax_editable" >
					 
					  <option value="1">Yes</option>
					  <option value="0">No</option>
					 
					</select>
					<label></label>
					<span class="help-block">Tax Editable? *</span> </div>
				</div>
				
				<!-- add new drop box -->
				
				
			<!-- end of  add new drop box -->	
	</div>
  	</div>
  <div class="form-actions right">
    <button type="submit" class="btn submit" >Submit</button>
    <button type="submit" class="btn default">Reset</button>
  </div>
  <?php form_close(); ?>
  </div>
</div>
<div class="clearfix">
<?php  $this->session->flashdata('succ_msg') ?>
</div>
<!-- END PAGE CONTENT--> 
<!-- END CONTENT -->
<script>
	function get_chrage_name(val){
	//alert(val);
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/get_chrage_name",
	   data: "val="+val,
	   success: function(data){
		   if(data.data=="1") {
                           
							swal({
                            title: "Duplicate Charge Name",
                            text: "Please enter a unique name for the new charge",
                            type: "warning"
                        },
                        function(){
							 document.getElementById("name").value = "";
                           // location.reload();

                        });
							
                           // document.getElementById("room_name").value = "";
                        }
	   }
	});
}
</script>