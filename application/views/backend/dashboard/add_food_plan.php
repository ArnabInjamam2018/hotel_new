

<script type="text/javascript">
$(function (){
	$(".js-example-basic-multiple").select2();	
	$(".js-example-basic-multiple").on("change", function () { 
		//console.log('autosubmitting'); 
		var tVal = $("#getOpt").val();
		var arr = tVal.split("~");
		var name = arr[0];
		var id = arr[1];
		$("#c_name").val(name);
		$.ajax({
   		url: '<?php echo base_url(); ?>dashboard/serviceCostById',
		type: "GET",
		dataType: "json",
		data: {id:id},
		success: function(data){
			   //console.log(data);
			   var price = data.s_price;	
			   $("#c_total").val(price);
		   }
		});		
	});	
	//FormValidationWizard.init();
});					
</script>
<div class="row">
  <?php if($this->session->flashdata('err_msg')):?>
  <div class="form-group">
    <div class="col-md-12 control-label">
      <div class="alert alert-danger alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
    </div>
  </div>
  <?php endif;?>
  <?php if($this->session->flashdata('succ_msg')):?>
  <div class="form-group">
    <div class="col-md-12 control-label">
      <div class="alert alert-success alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
    </div>
  </div>
  <?php endif;?>
  <div class="col-md-12">
    <div class="portlet box blue" id="form_wizard_1">
      <div class="portlet-title">
        <div class="caption"> <i class="icon-pin font-green"></i> ADD Food Plan - <span class="step-title"> Step 1 of 3 </span> </div>
        <div class="tools hidden-xs"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body form">
	  
        <form action="<?php echo base_url();?>dashboard/add_food_plan" class=""  enctype="multipart/form-data" id="submit_form" method="POST">
          <div class="form-wizard">
            <div class="form-body">
              <ul class="nav nav-pills nav-justified steps">
                <li> <a href="#tab1" data-toggle="tab" class="step"> <span class="number"> 1 </span> <span class="desc"> <i class="fa fa-check"></i> General Information </span> </a> </li>
                <li> <a href="#tab2" data-toggle="tab" class="step"> <span class="number"> 2 </span> <span class="desc"> <i class="fa fa-check"></i> Service Configuration </span> </a> </li>
                <li> <a href="#tab3" data-toggle="tab" class="step active"> <span class="number"> 3 </span> <span class="desc"> <i class="fa fa-check"></i> Price Information</span> </a> </li>
              </ul>
              <div id="bar" class="progress progress-striped" role="progressbar">
                <div class="progress-bar progress-bar-success"> </div>
              </div>
              <div class="tab-content">
                <div class="alert alert-danger display-none" id="chkDiv">
                  <button class="close" data-dismiss="alert"></button>
                  You have some form errors. Please check below. </div>
                <div class="alert alert-success display-none">
                  <button class="close" data-dismiss="alert"></button>
                  Your form validation is successful! </div>
                <div class="tab-pane active" id="tab1">
                	<div class="row">
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="fp_name" id="fp_name" placeholder="Plan Name *" />
                          <label></label>
                          <span class="help-block">Plan Name *</span> 
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <select class="form-control bs-select"  name="fp_category" >
                            <option value="">Select Plan Category</option>
                            <option value="AP">AP</option>
                            <option value="AMP">AMP</option>
                            <option value="EP">EP</option>
                            <option value="CP">CP</option>
                          </select>
                          <label></label>
                          <span class="help-block">Plan Category *</span> 
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <select class="form-control bs-select"  name="fp_class" >
                            <option value="">Select Plan Class </option>
                            <option value="Premium">Premium</option>
                            <option value="Deluxe">Deluxe</option>
                            <option value="Standard">Standard</option>
                          </select>
                          <label></label>
                          <span class="help-block">Plan Class *</span> 
                        </div>
                      </div>	
                      <div class="col-md-3">	
                        <div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="fp_description" placeholder="Plan Description *"/>
                          <label></label>
                          <span class="help-block">Plan Description *</span> 
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input col-md-4">
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                            <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
                              
                              <?php echo form_upload('fp_image');?>
                              </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                          </div>
                        </div>
                      </div>
                  	</div>
                </div>
                <div class="tab-pane" id="tab2">
                  
				   <div class="portlet-body form">
					<div class="form-body form-horizontal form-row-sepe">
					  <div class="form-body">
						<table class="table table-striped table-hover" id="extra_service">
						  
							<tr>
							  <th width="45%"> Service Name<span class="required">* </span> </th>
							  <th width="30%"> Service Periodicity<span class="required">* </span> </th>
							  <th width="25%"> Individual Service Cost<span class="required">* </span></th>
							  <th width="5%"> Action </th>
							</tr>               
						  
						  
						</table>
						<h4 style="text-align:center; border-top:1px solid #eee; padding:10px 0 25px;"><strong>Add Multiple Services :</strong></h4>
						<table class="table table-striped table-hover" >
						  <thead>
							<tr>
							  <th width="45%"> Service Name<span class="required">* </span> </th>
							  <th width="25%"> Service Periodicity<span class="required">* </span> </th>
							  <th width="30%"> Individual Service Cost<span class="required">* </span></th>
							  <th width="5%"> Action </th>
							</tr>
						  </thead>
						
						  <tbody>
							<tr>
							  <td class="form-group">
            <div class="form-group form-md-line-input">
             
				  <select class="form-control bs-select"   name="discount_rule[]" id="service_name" required="required" multiple>
				
				<?php if(isset($service_list)){ foreach($service_list as $service){?>
                  <option value="<?php echo $service->s_name.'~'.$service->s_id;?>"><?php echo $service->s_name;?></option>
								<?php } }?>
                </select>
              <span class="help-block">Service name</span> </div>
 
							  
							  
							  </td>
							  <td class="form-group">
							  <select class="form-control" id="c_quantity" >
								<option value="">Select An Option</option>
								<option value="1">Once per day</option>
								<option value="2">Twice per day</option>
								<option value="3">Thrice per day</option>
							  </select>	
							  </td>
							  <td class="hidden-480 form-group">
							  <input id="c_total" type="text" value="" class="form-control input-sm">
						  
							  </td>
							  <td><button class="btn green" type="button" id="addCharge"><i class="fa fa-plus" aria-hidden="true"></i></button>
								<br></td>
							</tr>                
						  </tbody>
						</table>
					  </div>
					</div>
				  </div>
                 
                </div>
                <div class="tab-pane clearfix" id="tab3">
                	<div class="col-md-12">
                  <div class="row">
                    <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                      <input  type="type" autocomplete="off" class="form-control" id="form_control_1" name="fp_price_adult" required="required" onkeypress="return onlyNos(event, this);" >
                      <label>Price Per Adult <span class="required">*</span> </label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                      <input  type="type" autocomplete="off" class="form-control" id="form_control_1" name="fp_price_children" required="required" onkeypress="return onlyNos(event, this);" >
                      <label>Price Per Children<span class="required">*</span> </label>
                      </div>
                    <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                      <div class="row">
                        <label class="control-label col-md-4">Tax Applied?<span class="required">*</span></label>
                        <div class="col-md-8">
                          <select id="tax" class="form-control" name="fp_tax_applied" onchange="showTax(this.value)">
                            <option value="">Select An Option</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                      <div class="row">
                        <label class="control-label col-md-6">Discount Applied?<span class="required">*</span></label>
                        <div class="col-md-6">
                          <select class="form-control" id="discount" name="fp_discount_applied">
                            <option value="">Select An Option</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                          </select>
                        </div>
                      </div>
                    </div>
					
					<div class="form-group form-md-line-input form-md-floating-label col-md-6" id="showTax" style="display:none;">
                      <input  type="type" autocomplete="off" class="form-control" id="form_control_1" name="fp_tax_percentage" required="required" onkeypress="return onlyNos(event, this);" >
                      <label>Tax percentage(%) <span class="required">*</span> </label>
                    </div>
					
                  </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-actions right"> <a href="javascript:;" class="btn default button-previous"> <i class="m-icon-swapleft"></i> Back </a> <a href="javascript:;" id="btn1" class="btn blue button-next"> Continue <i class="m-icon-swapright m-icon-white"></i> </a> <a href="javascript:;" class="btn submit button-submit" > Submit <i class="m-icon-swapright m-icon-white"></i> </a> </div>
          </div>
        </form>
 
      </div>
    </div>
  </div>
</div>
<style>
.select2-container{
	width:300px !important;
}
</style>
<script type="text/javascript">
var x=0;			
$("#addCharge").click(function(){
	var c_name = $("#c_name").val();
	var service_name = $("#service_name").val();
	var c_quantity = $("#c_quantity").val();
	var c_total = $("#c_total").val();
	if(c_name == ""){
		alert("Please select service name.");
		return false;
	}
	if(c_quantity == ""){
		alert("Please select service periodicity.");
		return false;
	}	
	x=x+1;
	$('#extra_service tr:last').after('<tr id="row_'+x+'"><td><label><input name="s_name[]" id="c_name5" type="text" value="'+service_name+'" class="form-control input-sm"  readonly></label></td><td><label><input name="qty[]" id="c_name1" type="text" value="'+c_quantity+'" class="form-control input-sm" readonly></label></td><td class="hidden-480"><label><input name="s_unit_cost[]" id="c_name4" type="text" value="'+c_total+'" class="form-control input-sm" readonly></label></td><td><a href="javascript:void(0);" class="btn red btn-sm" onclick="removeRow('+x+')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>');	
	$("#c_name").val("");
	$("#c_quantity").val("");
	$("#c_unit_price").val("");
	$("#c_tax").val("");
	$("#c_total").val("");	
    $("#getOpt").select2("val", "");
	$("#eVal").val(x);
});	
function removeRow(val) {
   $('#row_'+val).remove();
}
function showTax(val) {
	//alert("HERE");
	if(val == 'Yes'){
		$('#form_validation_wizard').find('#showTax').show();
	} else {
		$('#form_validation_wizard').find('#showTax').hide();	
	}
}					
</script>
<!-- END PAGE CONTENT-->