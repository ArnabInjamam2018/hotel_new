<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}


function chk_if_num( a )
		{
				if(isNaN($('#'+a+'').val())==true)
				{
					//alert ("please enter a valid "+a);
					$('#'+a+'').val("");
					
				}			
		}


</script>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add new Fuel</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/add_fuel',$form);

                        ?>
    <div class="form-body">
      <div class="row">
      	<div class="col-md-4">
        <div class="form-group form-md-line-input"> 
        <input autocomplete="off" type="text" class="form-control" id="fuel_name" name="fuel_name" onblur="chk_type(value)" required placeholder="Fuel Type *">
          <label></label>
          <span class="help-block">Fuel Type *</span></div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="fuel_desc" name="fuel_desc"  required placeholder="Fuel Desscription *">
              <label></label>
              <span class="help-block">Fuel Desscription *</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="unit" name="unit"  required placeholder="Fuel Unit *">
              <label></label>
              <span class="help-block">Fuel Unit *</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="reorder_level" name="reorder_level"  required placeholder="Minimum Stock to be Maintained *">
              <label></label>
              <span class="help-block">Minimum Stock to be Maintained *</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input" id="qty" >
              <input autocomplete="off" type="text" id="quantity" class="form-control" name="quantity" onKeyup="chk_if_num('quantity')" required="required" placeholder="Fuel Quantity *">
              <label></label>
              <span class="help-block">Fuel Quantity *</span>         
            </div>
        </div>
      </div> 
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn submit" onClick="check_date()">Submit</button>
      <button  type="reset" class="btn default">Reset</button>
    </div>  			
  <?php form_close(); ?>
  </div>
  <!-- END CONTENT --> 
</div>
<script>
function chk_type(value){
	var type=value;
	//alert(type);
	
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/chk_duplicate_fuel_type",
	   data: {data:type},
	   success: function(msg){
			
		   if(msg.data=="1"){
			   swal({
                            title: "Duplicate Fuel Name",
                            text: "",
                            type: "warning"
                        },
                        function(){
							document.getElementById("fuel_name").value = "";
                           // location.reload();

                        });
		   }
		   // $("#hd").html(msg);
	   }
	});
	
	}





   function check_Cylinders(value){
   		
       if(value =="Cylinders"){

           document.getElementById("c_choice").style.display="block";
       }else{
           document.getElementById("c_choice").style.display="none";
       }
      	if(value =="Rotation"){

           document.getElementById("r_choice").style.display="block";
       }else{
           document.getElementById("r_choice").style.display="none";
       }
       if(value =="Design"){

           document.getElementById("d_choice").style.display="block";
       }else{
           document.getElementById("d_choice").style.display="none";
       }
   }
   function ShowHideDiv() {
	   //alert("here");
        var add = document.getElementById("add");
        var purchaseid = document.getElementById("purchaseid");
        var sub = document.getElementById("sub");
		var qty = document.getElementById("qty");
        purchaseid.style.display = add.checked ? "block" : "none";
        qty.style.display = sub.checked ? "block" : "none";
		document.getElementById("sqty").disabled = false;
    }
	/*function soft_delete(){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){


		var p_id=document.getElementById('p_id').value;
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/select_quantity?p_id="+p_id,
                data:{p_id:p_id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }*/
	</script>
	