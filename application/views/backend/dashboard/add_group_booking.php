<script src="<?php echo base_url();?>assets/global/plugins/typeahead1/jquery.typeahead.js"></script>

<!--<h1 style="color:red;">Under maintaince Please revisit in 30 minutes, do not take bookigs now</h1>-->

<div class="row">
  <div class="col-md-12"> 
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
	<?php
	$attrib = array('role' => 'form', 'id' => 'myForm');
	echo form_open_multipart("dashboard/add_group_booking", $attrib)
	?>
    <div style="border: 1px solid #FF5722; border-top: 0;" class="portlet box blue">
		<div id="slideout">
     		<div class="slideout_outer">
				<a class="btn red btn-xs" onClick="get_status_unit_avail_log();">UAL</a>
				<div id="slideout_inner">
					<div class="scroller" style="height:180px;" data-always-visible="1" data-rail-visible1="1">
					<table class="table table-bordered table-hover" style="margin:0;"></table>
					</div>
				</div>
			</div>
		</div>					   
      <div class="portlet-title" style="background:#F8681A;">
        <div class="caption"> <i style="font-size:18px" class="fa fa-calendar-check-o" aria-hidden="true"></i> <strong> ADD GROUP BOOKING </strong> </div>
        <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
          <div class="row" style="margin-bottom:45px;">
		  
			<div class="col-md-12">
				<h3 class="form-heading" style="color:#F8681A;">Guest Details</h3>
			    
			</div>
		  
			<div class="col-md-3">			
                <div class="form-group form-md-line-input">
                    
                            <div class="typeahead__container ">
                                <div class="typeahead__field">            
                                    <span class="typeahead__query">
                                        <input class="js-typeahead-user_v1 form-control" name="g_name[query]" id="g_name" type="search" placeholder="search guest here..." autocomplete="off" onblur="get_guest_name(this.value)">
                                    </span>
									
                                 
                                </div>
                            </div>
                     
					<input class="form-control" type="hidden" placeholder="Guest Name" id="gn" name="name">
					
					
                </div>
            </div>
			
			 <div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input class="form-control" type="text" id="g_phn" name="g_phn" placeholder="Guest Contact No">
                  <label></label>
                  <span class="help-block">Guest Contact No</span> 
                </div>
            </div>	
			
			<div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input class="form-control" type="text" id="g_email" name="g_email" placeholder="Email Id">
                  <label></label>
                  <span class="help-block">Guest Email Id</span> 
                </div>
            </div>
			
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input class="form-control" type="text" id="gp" name="pincode"  onblur="fetch_all_address_hotel(this.value,'#g_country','#g_state')"  placeholder="Guest Pincode">
                  <label></label>
                  <span class="help-block">Guest Pincode</span> 
                </div>
            </div>	
			
			
			<div class="col-md-3">       
			<div class="form-group form-md-line-input">     
			<!--<input class="form-control" type="text" id="g_country" name="g_country" placeholder="Country" required="required">    -->
			<select class="form-control bs-select" id="g_country" name="g_country" data-live-search="true" data-size="8" data-show-subtext="true">
			<option value="" disabled="disabled">Select Country Name </option>
			<?php $allCountry = $this->setting_model->getAllCountry();
			
					if(isset($allCountry) && $allCountry){
						$i = 0;
						$prevReg = '';
						$defC = 'INDIA';
						$selectC = '';
						$defC = strtoupper($defC);
						
							foreach($allCountry as $country){
							
								if($country->Region != $prevReg){
									echo '<optgroup label="'.$country->Region.'">';
								}
							
								if(trim($defC) == trim(strtoupper($country->Country))){
									$selectC = 'selected="selected"';
								} else {
									$selectC = '';
								}
							
								if(isset($country->Country)){
							?>
									<option value="<?php echo trim(strtoupper($country->Country)); ?>" <?php echo $selectC; ?>>
										<?php echo $country->Country; ?>
									</option>
							<?php
								}

								$prevReg = $country->Region;
							}
							}
							
							?>
			</select>
			<label></label> 
			<span class="help-block">Country *</span>   
			</div>        
			</div>	
			
			<div class="col-md-3" id="g_state_div1" >         
			<div class="form-group form-md-line-input" > 
			<!--<input class="form-control" type="text" id="g_state" name="g_state" placeholder="State" required="required">	-->
			<input  type="hidden" id="g_state_code" name="g_state_code" placeholder="State" style="display:none;" >   
			<select class="form-control bs-select" id="g_state" name="g_state" data-live-search="true" data-size="8" data-show-subtext="true">
							<option value=""  disabled="disabled">Select State Name </option>
							<?php
							
							$allState = $this->setting_model->getAllState();
				
				//print_r($allState);
				//exit;
					
					if(isset($allState) && $allState){
						$i = 0;
						$prevTyp = '';
						$defS = 'WEST BENGAL';
						$selectS = '';
						$defS = strtoupper($defS);
						
						foreach($allState as $state){
							
							if($state->Type != $prevTyp)
								echo '<optgroup label="'.$state->Type.'">';
							
							if(trim($defS) == trim(strtoupper($state->Name))){
								$selectS = 'selected="selected"';
							} else {
								$selectS = '';
							}
							
							if(isset($state->Name)){
								?>
							<option value="<?php echo trim(strtoupper($state->Name)); ?>" <?php echo $selectS; ?>>
								<?php echo $state->Name; ?>
							</option>
							<?php
							}

							$prevTyp = $state->Type;
							}
							}
							?>
						<!--	<option value=""> Other Country State </option>-->
							</select>
							
			<label></label>  
			<span class="help-block">State *</span>      
			</div>          
			</div>	
			
			<div class="col-md-3" id="other_st" style="display:none;">
                <div class="form-group form-md-line-input" >
				
			<input type="text" class="form-control" place="put your country state" id="g_state1" name="g_state1">
				</div>
				</div>
			
			<div class="col-md-12"></br></div>
			<div class="col-md-12">
				<h3 class="form-heading" style="color:#F8681A;">Booking Details</h3>
			    <!-- <hr style="height:0.5px; border:none; color:#AAAAAA; background-color:#AAAAAA;"> -->
			</div>
		  
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <select name="p_center" class="form-control bs-select" id="pc">                        
                          <?php $pc=$this->dashboard_model->all_pc();?>
                          <?php
                          $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
                          ?>
                          <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
                           <?php $pc=$this->dashboard_model->all_pc1();
                            foreach($pc as $prfit_center){
                            ?>
                          
                          <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                            <?php }?>
                    </select>
                <?php if(isset($taxDB) && $taxDB):?>
                    <input type="hidden" id="dbst" value="<?php echo $taxDB->hotel_service_tax; ?>" />
                    <input type="hidden" id="dbsc" value="<?php echo $taxDB->hotel_service_charge; ?>"/>
                    <input type="hidden" id="dblt1" value="<?php echo $taxDB->luxury_tax_slab1; ?>"/>
                    <input type="hidden" id="dblt2" value="<?php echo $taxDB->luxury_tax_slab2; ?>"/>
					
					<input type="hidden" id="dbSlab1" value="<?php echo $taxDB->luxury_tax_slab1_range_to; ?>"/>
						<input type="hidden" id="dbSlab2" value="<?php echo $taxDB->luxury_tax_slab1_range_from; ?>"/>
						
					
                    <input type="hidden" id="dbSlab" value="<?php echo $taxDB->luxury_tax_slab2_range_to; ?>"/>
						<input type="hidden" id="dbSlab3" value="<?php echo $taxDB->luxury_tax_slab2_range_from; ?>"/>
						
                <?php endif; ?>		
                    <label></label>
                    <span class="help-block">Profit Center</span> 
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input class="form-control" type="hidden" id="guestID" name="guestID" value="0">
                    <input class="form-control" type="hidden" id="hd" value="0">				
                    <input class="form-control" type="number" id="ng" name="number_guest" placeholder="PAX Count">
                    <label></label>
                    <span class="help-block">PAX Count</span> 
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input class="form-control" type="number" id="nr" name="number_room" placeholder="No of Units" onblur="myFunction()">
                    <label></label>
                    <span class="help-block">No of Units</span> 
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <select name="room_rent_type" class="form-control bs-select" id="room_rent_type" required >
                      <option value="" disabled="disabled" selected="selected">Select room rent type</option>
                      <option value="Basic" selected="selected">Basic</option>
                      <option value="Seasonal">Seasonal</option>
                      <option value="Weekend">Weekend</option>
                      <option value="Weekend">Special</option>
                  </select>
                  <label></label>	
                  <span class="help-block">Room rent type</span> 			  
                </div>
            </div>
            <div class="col-md-3">	
                <div class="form-group form-md-line-input">
                  <input class="form-control" type="text" id="arrival_mode" name="arrival_mode" placeholder="Arrival Mode">
                  <label></label>
                  <span class="help-block">Arrival Mode</span> 	
                </div>
            </div>
			<div class="col-md-3">
                           <div class="form-group form-md-line-input">
                            <select class="form-control bs-select" name="meal_plan_type" id="meal_plan_type">
                              <option  value="1" selected="selected">No Plan</option>
                              <option value="2">EP</option>
							  <option value="3">CP</option>
							  <option value="4">MAP</option>
							  <option value="5">AP</option>
                             
                           </select>
                  <label></label>	
                  <span class="help-block">Meal plan type</span> 		
                          </div>
						  </div>
            
            <div class="col-md-3">				
                <div class="form-group form-md-line-input">
                <?php $visit=$this->dashboard_model->getNatureVisit1(); ?>
                   <select name="nature_visit" class="form-control bs-select" id="nature_visit">
                   <?php $defNature=$this->dashboard_model->default_nature_visit(); if(isset($defNature)&& $defNature){$defname=$defNature->booking_nature_visit_name;}else{$defname="Select";}  ?>
                             <option value="<?php echo $defname; ?>"selected><?php echo $defname;?></option>
                         <?php foreach ($visit as $key ) {
                                    # code...
                                  ?>
                          <option value="<?php echo $key->booking_nature_visit_name; ?>"><?php echo $key->booking_nature_visit_name; ?></option>
                          <?php } ?>
    </select>
                    <label></label>
                    <span class="help-block">Select Nature of visit</span> 
                </div>
            </div>		
			
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <!--<select name="booking_source" class="form-control" id="booking_source" onchange="insert_commision(this.value)">-->
                  <select name="booking_source" class="form-control bs-select" id="booking_source" onchange="agentOta(this.value)" >
                        <?php $defBookingSource=$this->unit_class_model->default_booking_source(); 
                            
                            if(isset($defBookingSource) && $defBookingSource){$defname=$defBookingSource->booking_source_name ;}else{$defname="Select";}
                            ?>

                         <option value="<?php echo $defname;?>" selected><?php echo $defname;?></option>
                          <?php 
                          $dta=$this->unit_class_model->get_booking_source1();
                          if($dta)
                          {
                              foreach($dta as $d) 
                          {
                              ?>
                              <option value="<?php echo $d->booking_source_name;?>"><?php echo $d->booking_source_name;?></option>
                         
                          <?php } }?>
                  </select>
				  <input type="hidden" name="set_agent_ota" id="set_agent_ota">
                  <label></label>
                  <span class="help-block">Booking Source</span> 
                </div>
            </div>
			<div class="col-md-3" style="display:none;" id="Agent_ota">
			<div class="form-group form-md-line-input">
                  <select name="broker_id" id="broker_id" class="form-control" onchange="get_commision(this.value)" >
                  </select>
				  <label></label>
                  <span class="help-block">Agent/OTA</span>
             </div>
             </div>
			<div class="col-md-3" style="display:none;" id="Agent_ota1">
			<div class="form-group form-md-line-input" id="broker_commision">
                          
                          <input value="" type="text" class="form-control" name="broker_commission" id="broker_commission" placeholder="Commision" />
						  <span class="help-block">Commision</span>
                        </div>
                        </div>
			<div id="showDetails" style="display:none;">
           <!-- <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <select name="travel_agent_id" id="travel_agent_id" class="form-control bs-select" onchange="showAgentCommission(this.value)">
                    </select>                	
                    <label></label>
                    <span class="help-block">Select Travel Agent</span> 
                </div>
            </div>-->
            <!-- <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input class="form-control" type="" id="travel_agent_commission" name="travel_agent_commission" placeholder="Commission Percentage">
                    <label></label>
                    <span class="help-block">Commission Percentage </span> 
                </div>
            </div>-->
			<div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <select name="commission_applicable" id="commission_applicable" class="form-control bs-select" >                        
                         <option value="0" selected="selected">Commision applicable for</option>
                         <option value="1">Room Rent</option>
                         <option value="2">Room Rent + Food</option>
                         <option value="3">Total Bill </option>
                    </select>                	
                    <label></label>
                    <span class="help-block">Commision applicable for</span> 
                </div>
            </div>
			
			</div>
            <div style="display:none;">
                <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                  <select name="" class="form-control bs-select">
                        <option value=""></option>
                      <option value="Frontdesk">Frontdesk</option>
                      <option value="Online">Online</option>
                      <option value="Telephonic">Telephonic</option>
                      <option value="Broker">Broker</option>
                      <option value="Channel">Channel</option>
                  </select>
                  <label>Booking Sources</label>
                </div>
                <div class="form-group col-md-3">
                  <input class="form-control" type="text" name="">
                  <label>Broker Percentage</label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input class="form-control" type="text" id="booking_preference" name="booking_preference" placeholder="Booking Preference">
                  <label></label>
                  <span class="help-block">Booking Preference</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input class="form-control" type="text" id="booking_notes" name="booking_notes" placeholder="Booking Notes">
                  <label></label>
                  <span class="help-block">Booking Notes</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <select name="tax_applicable" class="form-control bs-select" id="tax_applicable">
                        <!--<option value="" disabled="disabled" selected="selected">Select Tax applicable or not</option>-->
                      <option value="YES">YES</option>
                      <option value="NO">NO</option>
                    </select>
                    <label></label>
                    <span class="help-block">Select Tax applicable or not</span>
                </div>	
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input type="text" class="form-control date-picker" name="start_date" required="required" autocomplete="off" id="sd" placeholder="Start Date *">
                  <label></label>
                  <span class="help-block">Start Date *</span>
                </div>
            </div>
			<?php 
					$times=$this->dashboard_model->checkin_time();
					foreach($times as $time) {
                        if ($time->hotel_check_out_time_fr == 'PM') {
                            $checkout_time = ($time->hotel_check_out_time_hr + 12) . ":" . $time->hotel_check_out_time_mm;
                        } else {
                            $checkout_time = ($time->hotel_check_out_time_hr) . ":" . $time->hotel_check_out_time_mm;
                        }
						if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
							$checkin_time = ($time->hotel_check_in_time_hr + 12) . ":" . $time->hotel_check_in_time_mm;
						}
						else{
							$checkin_time = ($time->hotel_check_in_time_hr) . ":" . $time->hotel_check_in_time_mm;
						}
                    }
			?>
			<div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input type="text" class="form-control timepicker timepicker-24" name="checkin_time" required="required" autocomplete="off" id="checkin_time" value="<?php echo $checkin_time;?>" placeholder="Check-in Time *">
                  <label></label>
                  <span class="help-block">Check in Time *</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input type="text" class="form-control date-picker" name="end_date" required="required" autocomplete="off" id="ed" placeholder="End Date *">
                  <label></label>
                  <span class="help-block">End Date *</span>
                </div>
            </div>
			
			<div class="col-md-3">
                <div class="form-group form-md-line-input">
				<input type="hidden" name="default_checkin_time" id="def_checkin_time" value="<?php echo $checkin_time;?>" >
				<input type="hidden" name="def_checkout_time" id="def_checkout_time" value="<?php echo $checkout_time;?>" >
                  <input type="text" class="form-control timepicker timepicker-24" name="checkout_time" required="required" autocomplete="off" id="checkout_time" value="<?php echo $checkout_time;?>" placeholder="Check-out Time *">
                  <label></label>
                  <span class="help-block">Check Out Time *</span>
                </div>
            </div>
			<div class="col-md-3">
				  <div class="form-group form-md-line-input">
					<select class="form-control bs-select" id="marketing_personel" name="marketing_personel" required >
						
						<?php $marketing=$this->unit_class_model->all_marketing_personel(); 
						//print_r($pc);
						if(isset($marketing) && $marketing){
							foreach($marketing as $salse){	
						?>
						<option value="<?php echo $salse->name; ?>"> <?php echo $salse->name; ?></option>
						<?php }} ?>
					</select>
					<label></label>
					<span class="help-block">Marketing Personel</span>
				</div>
				</div>
			
            <div class="col-md-12">
                <div class="form-group form-md-line-input">
                <button type="button" class="btn submit pull-right" onclick="getVal()">Submit</button>
                </div>
            </div>
          </div>
		  <div id="loading-image" class="pop-lo">
		  <img src="<?php echo base_url()  ?>assets/ajax-loader.gif" >
		  
		  </div>
          <div id="rmid" style="display:none; margin-bottom: 30px;">
            <div class="row">
                <div class="col-md-6 bl-allroom" id="rmt">
					<div class="all-re">
						<div class="all-retitle">
							<strong style="color:#33D0E1; font-weight:800;">Available Room</strong>
						</div>
						<div class="portlet-body form">
						</div>
					</div>
				</div>
				<div class="col-md-6 bl-allholy" id="evt">
					<div class="all-re">
						<div class="all-retitle">
                        	<strong>All Events</strong>
						</div>
						<div class="portlet-body form">
						</div>
					</div>
				</div>
            </div>
          </div>
          <div id="bcid" style="display:none; padding-bottom:20px;">
          	<ul class="nav nav-tabs">
				<li class="active">
                    <a onclick="indrOff(1)" href="#default" data-toggle="tab">Default Rates</a>
                </li>
                <li class="popovers" data-container="body" data-trigger="hover" data-placement="bottom" data-content="Select to define room & meal rates per room" data-original-title="Define rates per Room">
                    <a onclick="indrOff(2)" href="#room" data-toggle="tab">Per Room Rate</a>
                </li>
                <li class="popovers" data-container="body" data-trigger="hover" data-placement="bottom" data-content="Select to define room & meal rates per adult & child" data-original-title="Define rates per Individual">
                    <a onclick="indrOn()" href="#individual" data-toggle="tab">Per Individual Rate</a>
                </li>								
            </ul>
            <div class="tab-content">
				<div class="row tab-pane fade active in" id="default">
                    <div class="col-md-12"> <span style="padding: 8px 0; display: block;"><i class="fa fa-info" aria-hidden="true" style="color:#6623BD;" ></i> This is the default rates, to define rates per room or individual, please select appropiate options.
                      </span> </div>
                </div>
            	<div class="row tab-pane fade" id="room">
                  <div class="col-md-12">
                    <div role="form" class="form-inline" style="overflow:hidden;">
                      <div class="pull-left">
                        <label class="pad-s">Default Room Rent</label>
                        <div class="form-group">
                          <input class="form-control input-small" type="text" id="trrID" value="" placeholder="Room Charge">
                        </div>
                        <!--<div class="form-group">
                          <select class="form-control" onchange="getChange1(this.value)">
                                <option  value="" selected="selected">Plan</option>
                                <option value="AP">AP</option>
                                <option value="MAP">MAP</option>
                                <option value="EP">EP</option>
                                <option value="CP">CP</option>
                            </select>
                        </div>-->
                        <button class="btn greenLight" type="button" id="btn1">Add</button>
                      </div>
                      <div class="pull-right">
                        <label class="pad-s">Default Food Inclusion</label>
                        <div class="form-group">
                          <input class="form-control input-small" type="text" id="tfiID" value="" placeholder="Meal Plan Charge">
                        </div>
                        <div class="form-group">
                          <input class="form-control input-small" type="text" placeholder="Meal Plan" id="cmp1" style="display:none;">
						    <select class="form-control" onchange="getChange1(this.value)">
                                <option value="No Plan" selected="selected">No Plan</option>
								<option value="EP">EP</option>
								<option value="CP">CP</option>
                                <option value="MAP">MAP</option>
								<option value="AP">AP</option>                              
                            </select>
                        </div>
                        <button class="btn greenLight" type="button" id="btn2">Add</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row tab-pane fade" id="individual">
                  <div class="col-md-12">
                    <div role="form" class="form-inline" style="overflow:hidden;">
                      <div class="pull-left">
                        <label class="pad-s">Adult</label>
                        <div class="form-group">
                          <input class="form-control input-small" type="text" placeholder="Room Charge" id="arc" onblur="getARC(this.value)">
                        </div>
                        <div class="form-group">
                          <input class="form-control input-small" type="text" placeholder="Meal Plan Charge" id="afi" onblur="getAFI(this.value)">
                        </div>
                        <div class="form-group">
                          <select class="form-control" onchange="getChange(this.value)">
                                <option value="No Plan" selected="selected">No Plan</option>
								<option value="EP">EP</option>
								<option value="CP">CP</option>
                                <option value="MAP">MAP</option>
								<option value="AP">AP</option> 
                            </select>
                        </div>
                      </div>
                      <div class="pull-right">
                        <label class="pad-s">Kids</label>
                        <div class="form-group">
                          <input class="form-control input-small" type="text" placeholder="Room Charge" id="crc" onblur="getCRC(this.value)">
                        </div>
                        <div class="form-group">
                          <input class="form-control input-small" type="text" placeholder="Meal Plan Charge" id="cfi" onblur="getCFI(this.value)">
                        </div>
                        <div class="form-group">
                          <input class="form-control input-small" type="text" placeholder="Meal Plan" id="cmp">
                        </div>
						<button class="btn greenLight" type="button" id="btnpir">Add</button>
                      </div>                  
                    </div>
                  </div>
                </div>
            </div>
          </div>
		  
       <div id="tid" style="display:none;position:relative;">
	   
	    <div id="loading-image5" class="pop-lo">
		  <img src="<?php echo base_url()  ?>assets/ajax-loader.gif" >
		  
		  </div>
          <div class="table-scrollable" >

            <table class="table table-striped table-hover table-bordered mass-book" id="myTable">
              <tbody>
              <tr>
                  <th> # </th>
                  <th> Unit Type </th>
                  <th> Unit ID </th>
                  <th> Guest Name </th>
                  <th> CI Date </th>
                <!--  <th> CI Date1 </th>-->
				  <th> CI Time</th>
                  <th> CO Date </th>
                 <!-- <th> CO Date1 </th>-->
				  <th> CO Time</th>
                  <th> Days </th>
                  <th> Status </th>
                  <th> F_Plan </th>
                  <th> A_R_R </th>
                  <th> A_F_I </th>
                  <th> C_R_R </th>
                  <th> C_F_I </th>
                  <th> Adlt </th>
                  <th> Kid </th>
				  <th> Extra</th>
				  <th> TAX? </th>
				  <th> T_R_R </th>				  
				  <th> RR Tax </th>
                  <th> T_F_I </th> 
				  <th> MP Tax </th>	
                  <th> P_D_R </th>
                  <th width="8%"> Total Amt </th>
                </tr>
              </tbody>
                <tfoot>
                	<tr>
                		<td colspan="16" style="font-weight:700; color:#666666;"> TOTAL </td>
						<td></td>
                        <td style="color:#2FC3A1;"><input id="bkrr_total" class="s-inp" style="font-weight:700; width:100%" value="0" /></td>
                        <td style="color:#2FC3A1;"><input id="bkrt_total" class="s-inp" style="font-weight:700; width:100%" value="0"  /></td>
                        <td style="color:#2FC3A1;"><input id="bkmp_total" class="s-inp" style="font-weight:700; width:100%" value="0"  /></td>
                        <td style="color:#2FC3A1;"><input id="bkft_total" class="s-inp" style="font-weight:700; width:100%" value="0" /></td>
                        <td style="color:#2FC3A1;"></td>
                        <td style="color:#FF5722;"><input id="bk_total" class="s-inp" style="font-weight:700; width:100%" value="0" /></td>
                    </tr>
                </tfoot>              
            </table>
          </div>
          <div class="note" style="font-size:12px; font-style:italic; padding:10px; background:#e5e5e5; color:#1BBC9B;">
          	* Adult Room Rent – A_R_R | Adult Food Inclusion – A_F_I | Child Room Rent – C_R_R | Per Day Rate - P_D_R | Total Room Rent - T_R_R | Total Food Inclusion - T_F_I
          </div>
		  
        </div>
        <div id="ex-ch-add">
		    <div id="btnAC" style="display:none; padding:0 10px;" class="text-center clearfix"> 
        	<input type='hidden' id='hideshow' class='btn greenLight' value='Add Extra Charges' style="margin:15px 0;">
            <div id="spn" class="pull-right"style="display:none;"> 
            <button type="button" id='newBtn' class="btn btn-primary" style="margin:15px 0; padding: 5px;"><i class="fa fa-refresh fa-spin" style="font-size:22px; line-height:22px;"></i></button>
            </div>
            </div>		    			
			<!--<input type='button' id='rc' class='btn btn-primary' value='ReCalculate' style="margin:15px 0;">-->
			<div class="all-re" style="display:none;" id="ex-charge">
              <div class="all-retitle">
                Extra Charge Added:
              </div>
              <div class="portlet-body form">
                  <div class="form-body">
                    
                    <table class="table table-striped table-bordered table-hover" id="extra_service">
                      <thead>
                        <tr>
                          <th width="25%"> Description </th>
                          <th width="15%"> Qty </th>
                          <th width="20%"> Unit Price </th>
                          <th width="15%"> Tax % </th>
                          <th width="20%"> Total </th>
                          <th width="5%"> Action </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="form-group"><input id="c_name" type="text" value="" class="form-control input-sm"></td>
                          <td class="form-group"><input id="c_quantity" type="number" value="" class="form-control input-sm"></td>
                          <td class="hidden-480 form-group"><input onblur="calculation(this.value)" id="c_unit_price" type="text" value="" class="form-control input-sm"></td>
                          <td class="hidden-480 form-group"><input id="c_tax" onblur="calculation2(this.value)" type="text" value="" class="form-control input-sm"></td>
                          <td class="hidden-480 form-group"><input id="c_total" type="text" value="" class="form-control input-sm" readonly></td>
                          <td><button class="btn green btn-xs" type="button" id="addCharge"><i class="fa fa-plus" aria-hidden="true"></i></button>

                            <br></td>
                        </tr>                
                      </tbody>
                    </table>
                  </div>
              </div>
        	</div>
    	</div>
    	<div id="bid" style="display:none;">
          <div class="clearfix" style="padding:10px; border-top: 1px solid #ddd;">
            <input class="btn blue pull-right" type="button" id="next" value="Next">            
          </div>
	 	</div> 
        <div id="totalsss" style="display:none; padding:10px; border-left:1px solid #e1e1e1; border-right:1px solid #e1e1e1;">
            <ul>
                <li>Total Room Rent: <i class="fa fa-inr"></i><span id="tp5"></span></li>
                <li>Total Room Rent Tax: <i class="fa fa-inr"></i><span id="tp4"></span></li>
                <li>Total Food Inclution: <i class="fa fa-inr"></i><span id="tp3"></span></li>
                <li>Total Food Vat: <i class="fa fa-inr"></i><span id="tp2"></span></li>
                <li>Total Price: <i class="fa fa-inr"></i><span id="tp1"></span></li>
				<li>Total Additional Charges: <i class="fa fa-inr"></i><span id="tp6"></span></li>
				<li>Total Tax on Additional Charges: <i class="fa fa-inr"></i><span id="tp7"></span></li>
				<li>Total Commision: <i class="fa fa-inr"></i><span id="tp8"></span></li>
            </ul>
        </div>
		<input type="hidden" name="totprice" id="totprice">
		<input type="hidden" name="totcom" id="totcom">
		<input type="hidden" name="applyedAmount" id="applyedAmount">
    	<div id="lastid" style="display:none;">
          <div class="clearfix" style="padding:10px; border-top: 1px solid #ddd;">
            <input class="btn blue pull-right" type="button" value="Generate Group Booking" id="btnSubmit" onclick="disbtn()">
			<input class="btn blue" type="button" value="Re-Calculate" id="reCal" onclick="disbtn()">
          </div>
	 	</div> 		
      </div>	  
    </div>
	</form>
    <!-- END EXAMPLE TABLE PORTLET--> 
  </div>
</div>
<script src="<?php echo base_url();?>build/js/app/js/ratePlan.js" type="text/javascript"></script>
<script>
function disbtn(){
	$("#btnSubmit").attr('disabled',true);
	$("#reCal").attr('disabled',true);
}
var dbTax1 = parseFloat($("#dbst").val()) + parseFloat($("#dbsc").val()) + parseFloat($("#dblt1").val());
var dbTax2 = parseFloat($("#dbst").val()) + parseFloat($("#dbsc").val()) + parseFloat($("#dblt2").val());
var dbTax3 = parseFloat($("#dbst").val()) + parseFloat($("#dbsc").val());
var slab1 = parseFloat($("#dbSlab1").val());
var slab2 = parseFloat($("#dbSlab2").val());

var slab = parseFloat($("#dbSlab").val());
var slab3 = parseFloat($("#dbSlab3").val());

function agentOta(source) {

    jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>bookings/get_source_type_id_by_name_source",
        data: {
            source: source,


        },
        success: function(data) {

            if (data.id == 5) {
                document.getElementById('Agent_ota').style.display = 'block';
                document.getElementById('Agent_ota1').style.display = 'block';
                document.getElementById('showDetails').style.display = 'block';

                $('#set_agent_ota').val('5');
                return_broker();
            } else if (data.id == 4) {
                document.getElementById('Agent_ota').style.display = 'block';
                document.getElementById('Agent_ota1').style.display = 'block';
                document.getElementById('showDetails').style.display = 'block';
                $('#set_agent_ota').val('4');
                return_channel();

            } else {
                document.getElementById('Agent_ota').style.display = 'none';
                document.getElementById('Agent_ota1').style.display = 'none';
                document.getElementById('showDetails').style.display = 'none';
                $('#set_agent_ota').val('');
            }

        } // end ajax success
    });

}

function get_commision(flag1) {
    var flag = $('#set_agent_ota').val();
    //alert(flag);
    var b_id = $('#broker_id').val();
    // alert(b_id);
    var booking_id = '1';

    if (flag == '5') {
        var url = 'get_commision_grp';
    } else if (flag == '4') {
        var url = 'get_commision2_grp';
    }
    jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>bookings/" + url + "",
        datatype: 'json',
        data: {
            b_id: b_id,
            booking_id: booking_id
        },
        success: function(data) {
            //alert(data);
            var room_rent = parseInt(data.base_room_rent);
            var commision = parseInt(data.broker_commission);
            commision = commision + '%';
            //var tot_commision = (room_rent * (commision/100));
            $('#broker_commission').val(commision);
        }
    });


    return false;
}

function parseDate(str) {
    //alert('str' +str);
    //console.log(str);
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[0] - 1, mdy[1]);
}

function myFunction() {
    var noOfRoom = $("#nr").val();
    if (noOfRoom < 2) {
        swal({
                title: "Group booking must have minimum 2 Units.",
                text: "",
                type: "error",
                confirmButtonColor: "#F73F54"
            },
            function() {});
        //alert('Group booking must have minimum 2 rooms.');
        $("#nr").val("");
        $("#nr").focus();
        return false;
    }
}


var mealname = '';
var totval = 0;
var totfoodtax = 0;
var totmealchrg = 0;

function getMealplan(mealid) {

    $.ajax({
        url: '<?php echo base_url(); ?>rate_plan/getRateName',
        type: "POST",
        data: {
            mealid: mealid
        },
        success: function(data) {
            //alert(data);

            mealname = data;
            console.log('mealname in ajx==' + mealname);
        },
    });

    //return mealname;

}

var x = 0;
var days = 0;
var newstartDate = '';
var roomRentType = '';
var val = 0;
var tax = '';
var meal_charge = 0;
var tVal = 0;
window.unitavaillofflag = 0;
var myArray = new Array();

function indrOff(n) {
    window.bcid = n;
    var count = window.countRow;
    for (i = 1; i <= count; i++) {
        if ($("#startDate" + i).val()) {
            $('#arc1_' + i).addClass("s-inp");
            $('#afi1_' + i).addClass("s-inp");
            $('#crc1_' + i).addClass("s-inp");
            $('#cfi1_' + i).addClass("s-inp");
            $('#trr_' + i).addClass("s-inp");
            $('#tfi_' + i).addClass("s-inp");
            $('#tax_' + i).addClass("s-inp");
            $('#vat_' + i).addClass("s-inp");
            $('#arc1_' + i).val('0');
            $('#afi1_' + i).val('0');
            $('#crc1_' + i).val('0');
            $('#cfi1_' + i).val('0');
            $('#trrID').val('');
            $('#tfiID').val('');
        }
    }

    //totalAmt();
} // End function indrOff

function indrOn() {
    var count = window.countRow;
    window.bcid = 3;
    //$('.s-inp').removeClass("s-inp");
    for (i = 1; i <= count; i++) {
        if ($("#startDate" + i).val()) {
            $('#arc1_' + i).removeClass("s-inp");
            $('#afi1_' + i).removeClass("s-inp");
            $('#crc1_' + i).removeClass("s-inp");
            $('#cfi1_' + i).removeClass("s-inp");
            $('#trr_' + i).removeClass("s-inp");
            $('#tfi_' + i).removeClass("s-inp");
            $('#tax_' + i).removeClass("s-inp");
            $("#arc").val('');
            $("#afi").val('');
            $("#crc").val('');
            $("#cfi").val('');
        }
    }
} // End function indrOn


function pdrUpdate(row) {

    let rrTot = parseFloat($("#trr_" + row).val());
    let mpTot = parseFloat($("#tfi_" + row).val());
    let rrTax = parseFloat($("#tax_" + row).val());
    let vt = parseFloat($('#vat_' + row).val());
    let days = parseInt($('#days_' + row).val());
    let pdr = rrTot + mpTot + rrTax + vt;
    //console.log('pdr = '+pdr);
    $('#pdr_' + row).val(pdr);
    $('#price_' + row).val(pdr * days);
    totalAmt();

} // End function pdrUpdate


function addRow1(roomVal, roomNo) {
	
	console.log('Humba');

	console.log('Test Yo Lk1 RoomNo '+roomNo);
	console.log('Test Yo Lk1 RoomVal '+roomVal);
    std_clk = $("#sd").val();
    end_clk = $("#ed").val();
    //alert('std_clk' +std_clk);
    //alert('end_clk' +end_clk);
    //alert('roomVal' +roomVal);
	
    setTimeout(function() {
		console.log('Test Yo Lk1');
        addRow(roomVal, roomNo);

    }, 500);
}


function addRow(roomVal, roomNo) {
	
	console.log(typeof(roomNo));
	
    let ta = 1;
    let cls = 'btn pink';
    let meal_charge = 0;
    let days = 0;
    let days1 = 0;
    var z = 0;
    var mydate = '';
    var roomId = '';
    if ($("#ab_" + roomVal).prop("checked") == true) {

        roomRentType = $("#room_rent_type").val();
        var noOfRoom = $("#nr").val();
        var startDate = parseDate($("#sd").val());
        newstartDate = $("#sd").val();
        var endDate = parseDate($("#ed").val());
        var guestName = $("#gn").val();
        var selected1 = $("#sd").val();
		var checkin_time = $("#checkin_time").val();
        var selected2 = $("#ed").val();
		var checkout_time = $("#checkout_time").val();
        var tax = 0;
        $("#tid").css("display", "block");
        $("#bid").css("display", "block");
        $("#btnAC").css("display", "block");
        getMealplan($('#meal_plan_type').val());
		
		days1 = (datesGrp('globl',4,1) - datesGrp('globl',3,1)) / (1000 * 60 * 60 * 24);
		days = (endDate - startDate) / (1000 * 60 * 60 * 24);
		days = days1;
		
		console.log('Dates from addRow: End: datesGrp(4)='+datesGrp('globl',4,1)+' , endDate='+endDate+' | Start: datesGrp(3)='+datesGrp('globl',3,1)+' , startDate='+startDate+' | '+days+' '+days1);
		
		if(days == 0) days = 1;
		console.log('Test Yo Lk2');
        if (x >= noOfRoom) {
		
            //	x=x-1;
            console.log(x + ' ' + noOfRoom);
           
            swal({
                    title: "Unit Count Exceded!",
                    text: "Unit count can not be greater than " + noOfRoom,
                    type: "error"
                },
                function() {});

            $("#ab_" + roomVal).prop("checked", false);
            return false;
        } else if (x <= noOfRoom && window.countRow <= noOfRoom) {
			
			//console.log('inside addRow -> else if (x <= noOfRoom && window.countRow <= noOfRoom)');

            $('#loading-image').show();

            $.ajax({

                type: "POST",
                url: "<?php echo base_url(); ?>setting/check_avail_room_final_grp",
                datatype: 'json',
                async: false,
                data: {
                    std: std_clk,
                    end: end_clk,
                    rmid: roomVal
                },
                success: function(data) {
					
					//console.log('check_avail_room_final_grp data: ' + data);
					
                    if (data) {
						
                        //swal('Room No  ' + '' + roomNo + '  already exists Between this daterange ' + ' ' + std_clk + ' to ' + ' ' + end_clk);
						swal({
							title: "Unit Blocked!",
							text: 'Unit '+ roomNo +' is blocked between daterange' + std_clk + 'to ' + end_clk + ' please remove the unit from UAL',
							type: "warning",
							confirmButtonColor: "#F27474"
						}); 
                        $("input.noClick").attr("disabled", true);
                        $("#abc_" + roomVal).find('input.noClick').prop("disabled", true);
                        $("#ab_" + roomVal).prop("checked", false);
                        $("#ab_" + roomVal).css("background-color", "red");
                        return false;

                    } else {

                        x = x + 1;
                        window.countRow = window.countRow + 1;
						window.unitavaillofflag = window.unitavaillofflag + 1;

                        $("input.noClick").attr("disabled", true);
                        $("#abc_" + roomVal).removeClass('grey-cascade');
                        $("#abc_" + roomVal).addClass('btn-success');
                        myArray.push(roomVal);
					
                        $.ajax({
                            url: '<?php echo base_url(); ?>dashboard/get_room_details',
                            type: "GET",
                            dataType: "json",
                            data: {
                                id: roomVal
                            },
                            success: function(data) {
                              
                                var taxApplicable = $("#tax_applicable").val();
                                if (taxApplicable == 'YES') {

                                    var tax1 = 0;

                                } else {
                                    var tax1 = 0;
                                }

                                tax = 0;
                                val = 0;
                                meal_charge = 0;
                                var xVal = parseFloat(tax) + parseFloat(val) + parseFloat(meal_charge);
                                var tVal = days * parseFloat(xVal);
                                var type1 = 1;
                                var type2 = 2;

                                val = 0;
                                console.log(x);
                                console.log(data[0].unit_id);

                                $('#myTable tbody tr:last').after('<tr id="row_' + roomVal + '"><td><span id="span_' + roomVal + '">' + x + '</span><input id="mo_' + x + '" class="form-control input-sm" type="hidden" value="' + data[0].max_occupancy + '"><input id="rmo_' + x + '" name="roomID[]" class="form-control input-sm" type="hidden" value="' + roomVal + '"><input type="hidden" id="un_id_'+ x +'" name="un_id[]" value="' + data[0].unit_id + '"></td><td><input class="form-control input-sm" type="text" value="' + data[0].unit_name + '" name="type[]" readonly></td><td><input class="form-control input-sm" type="text" value="' + roomNo + '" name="roomNO[]" readonly></td><td><input class="form-control input-sm s-inp" type="text" value="' + guestName + '" name="gestName[]" readonly></td><td><input class="form-control date-picker input-sm" type="text" value="' + selected1 + '" data-provide="datepicker" onchange="dayCal(' + x + ')" autocomplete="off" id="startDate' + x + '" name="startDate[]"><input class="form-control date-picker input-sm s-inp" type="hidden" value="' + datesGrp('globl',1,x) + '" onchange="dayCal(' + x + ')" autocomplete="off" id="startDateA' + x + '" name="startDateA[]" ></td><td><input class="form-control timepicker timepicker-24 input-sm" type="text" value="' + checkin_time + '" data-provide="timepicker" onchange="chkintime(' + x + '), dayCal(' + x + ')"   autocomplete="off" id="checkintime' + x + '" name="checkintime[]"></td><td><input onchange="dayCal(' + x + ')" class="form-control date-picker input-sm" type="text" value="' + selected2 + '" data-provide="datepicker" id="endDate' + x + '" autocomplete="off" name="endDate[]"><input onchange="dayCal(' + x + ')" class="form-control date-picker input-sm s-inp" type="hidden" value="' + datesGrp('globl',2,x) + '" id="endDateA' + x + '" autocomplete="off" name="endDateA[]" ></td><td><input class="form-control timepicker timepicker-24 input-sm" type="text" value="' + checkout_time + '" data-provide="timepicker" onchange="chkouttime(' + x + '), dayCal(' + x + ')"  autocomplete="off" id="checkouttime' + x + '" name="checkouttime[]"></td><td><input id="days_' + x + '" class="form-control input-sm s-inp" type="text" value="' + days + '" name="days[]"></td><td><input class="form-control input-sm" type="text" value="Confirmed"></td><td><input class="form-control input-sm mp" type="text" value="' + mealname + '" name="mealPlan[]"></td><td><input id="arc1_' + x + '" class="form-control input-sm arc s-inp" type="text" value="0" name="adultRoomRent[]"></td><td><input id="afi1_' + x + '" class="form-control input-sm afi s-inp" type="text" value="0" name="afi[]"></td><td><input id="crc1_' + x + '" class="form-control input-sm crc s-inp" type="text" value="0" name="childRoomRent[]"></td><td><input id="cfi1_' + x + '" class="form-control input-sm cfi s-inp" type="text" value="0" name="cfi[]"></td><td><input class="form-control input-sm" type="text" value="" placeholder="Enter" id="ao_' + x + '" onchange="checkOccupancyNB(' + roomVal + ',' + x + ' )" name="adultNO[]"><input class="form-control input-sm s-inp" type="hidden" name="exra[]" id="exra' + x + '"><input class="form-control input-sm s-inp" type="hidden" name="exma[]" id="exma' + x + '"></td><td><input class="form-control input-sm" type="text" value="0" placeholder="Enter" id="co_' + x + '" onchange="checkOccupancyNB(' + roomVal + ',' + x + ')" name="childNO[]"><input class="form-control input-sm s-inp" type="hidden" name="exrc[]" id="exrc' + x + '"><input class="form-control input-sm s-inp" type="hidden" name="exmc[]" id="exmc' + x + '"></td><td><input class="form-control input-sm s-inp" type="text"  name="exp[]" id="exp_' + x + '"><span id="extra_' + x + '" class="hideANDseek extra_per"><label class="radio-inline add1"><input class="addition3" type="radio" name="charge_mode_type[]" id="charge_mode_type' + x + '" value="adult" onclick="set_extra_charge(this.value,' + x + '); exCls(this.value,' + x + ');" style="display:none;"><i id="exadf_' + x + '" class="fa fa-male"></i></label><label class="radio-inline sub1"><input class="addition4" type="radio" name="charge_mode_type[]" id="charge_mode_type' + x + '" value="child" onclick="set_extra_charge(this.value,' + x + '); exCls(this.value,' + x + ');" style="display:none;"><i id="exchf_' + x + '" class="fa fa-child"></i> </label></span><input type="hidden" name= "extra_person_types[]" value="" id="charge_mode_type1' + x + '"></td><td><button onclick="taxApply(' + x + ');" id="taxApply_' + x + '" type="button" class="' + cls + '" style="font-size:10px" value="' + ta + '"> Tax</button></td><td><input onchange="getRtax(' + x + '); pdrUpdate(' + x + ')" onblur="reCalculate(' + x + ')" id="trr_' + x + '" class="form-control input-sm trr" type="text" value="' + val + '" name="trr[]"><input type="hidden" name="trrc[]" id="trrc' + x + '"></td><td><input id="tax_' + x + '" class="form-control input-sm tax" type="text" value="' + tax1 + '" name="tax[]"></td><td><input onchange="getVat(' + x + '); pdrUpdate(' + x + ');" id="tfi_' + x + '" class="form-control input-sm tfi" type="text" value="' + meal_charge + '" name="tfi[]"><input type="hidden" name="tfic[]" id="tfic' + x + '"></td><td><input id="vat_' + x + '" class="form-control input-sm vat" type="text" value="0" name="vat[]"></td><td><input id="pdr_' + x + '" class="form-control input-sm pdr s-inp" type="text" value="' + xVal + '" name="pdr[]" ></td><td><input onchange="totalAmt()" id="price_' + x + '" class="form-control input-sm price s-inp" type="text" value="' + tVal + '" name="price[]" ><input type="hidden" name="txrrspns[]" id="txrrspns_' + x + '"  class ="" ><input type="hidden" name="txmrspns[]" id="txmrspns_' + x + '"><input type="hidden" name="o_id[]" id="o_id' + x + '"></td></tr>');
								
								
								 $(".timepicker-24").timepicker({
									autoclose: !0,
									minuteStep: 1,
									showSeconds: !1,
									showMeridian: !1
								});
                            },

                            complete: function() {
                                $("input.noClick").removeAttr("disabled");
                                //$('#loading-image').hide();
                            },

                        }); // end of room details ajax..

                    } // end of inner else /../.

                }, // end chk grp success function

                complete: function() {

					setTimeout(function(){
						$("input.noClick").removeAttr("disabled");
						$('#loading-image').hide();
					},3000);
                },


            }); // end of chk grp ajax


        } // end of else if 
    } else {

        //alert('last else ' + x);
		$('#loading-image').show();
        $.ajax({

            type: "POST",
            url: "<?php echo base_url(); ?>setting/check_avail_room_delete_grp",
            datatype: 'json',
            async: false,
            data: {
                std: std_clk,
                end: end_clk,
                rmid: roomVal
            },
            success: function(data) {

                //console.log(data);

                if (data.success == "success") {

                    myArray.removeByValue(roomVal);
                    $('#row_' + roomVal).remove();
                    $("#abc_" + roomVal).removeClass('btn-success');
                    $("#abc_" + roomVal).addClass('grey-cascade');
                    x = x - 1;
                    window.countRow = window.countRow - 1;
					 window.unitavaillofflag = window.unitavaillofflag - 1;
                } else {

                    swal('Kindly press cntrl + r to reload the page');
                }

            },
			complete:function(){
				
				setTimeout(function(){
					
					$('#loading-image').hide();
				},1000);
				
			},
        }); // End Ajax

    }
    for (var m = 0; m < myArray.length; m++) {
        z = z + 1;
        $("#span_" + myArray[m]).text(z);
    }

    setTimeout(totalAmt(), 2500);

} // End function addRow

	function warning(unitavaillofflag)
    {
		alert('unitavaillofflag'  +unitavaillofflag);
      // alert('Detecting the page refresh using Java Script'); //you can put your code here.
	  if(parseInt(unitavaillofflag) >= 1 ){
		  
		  alert("unitavaillofflag ");
	  }
	   else{
		   
		    alert(" unitavaillofflag");
	   }
	   
    }
    
	//window.onbeforeunload = warning(window.unitavaillofflag);

function taxApply(t) {

    let tfi = parseFloat($('#tfi_' + t).val());
    let trr = parseFloat($('#trr_' + t).val());
    let sd = $('#startDate' + t).val();
    let tax = $("#taxApply_" + t).val();

    if (tax === '1') {
        $("#taxApply_" + t).attr('class', 'btn default');
        $("#taxApply_" + t).val('0');
        $('#tax_' + t).val(0);
        $('#vat_' + t).val(0);
        $('#txmrspns_' + t).val('{"total":0}');
        $('#txrrspns_' + t).val('{"total":0}');
        pdrUpdate(t);
        //reCalculate(t);
    } else if (tax === '0') {
        $("#taxApply_" + t).attr('class', 'btn pink');
        $("#taxApply_" + t).val('1');
        getTax(trr, sd, t, 'Booking');
        getTax(tfi, sd, t, 'Meal Plan');
        pdrUpdate(t);
    }

} // End function taxApply


function indiRecalNBCheck(x, roomID) {
    //alert(x);
    var ad = $("#ao_" + x).val();
    var kd = $("#co_" + x).val();
    //alert(ad);
    //alert(kd);
    if (ad == "" || kd == "") {

    } else {
        indiRecalNB(x, roomID);
    }

}


function indiRecalNB(x, roomID) {

    //alert("room X" +x);

    var adult = parseInt($("#ao_" + x).val());
    var child = parseInt($("#co_" + x).val());

    if (!isNaN(adult) && !isNaN(child)) {

        //var occupancy = adult + child;
		//alert('roomID' +roomID);
		
        ratePlaningNB(x, newstartDate, $("#booking_source").val(), roomRentType, roomID, '<?php echo base_url() ?>', $('#meal_plan_type').val());

    }
    /*	else{
    	alert('Enter no of adult && kid please');
    	$("#ao_"+x).val('');
    	$("#co_"+x).val('');
    }
    */
} // End function indiRecalNB

function exCls(typ, row) {
    //console.log('exCls - '+typ+' '+row);
    if (typ === 'child') {
        $('#exchf_' + row).css('color', '#32C3D4');
        $('#exadf_' + row).css('color', 'black');
        $('#charge_mode_type1' + row).val('child');
    } else if (typ === 'adult') {
        $('#exadf_' + row).css('color', '#32C3D4');
        $('#exchf_' + row).css('color', 'black');
        $('#charge_mode_type1' + row).val('adult');
    }

} // End function exCls

var unitType = '';

function check_occupancy(row, maxo, defo) {

    let adult = parseInt($("#ao_" + row).val());
    let child = parseInt($("#co_" + row).val());
    let pax = adult + child;
    let extra = parseInt($("#exp_" + row).val());
    console.log('Max Occupancy - ' + maxo);
    console.log('Def Occupancy - ' + defo);
    console.log('PAX - ' + pax);

    if (pax > 0) {

        if (parseInt(maxo) < parseInt(pax)) {

            $("#o_id" + row).val();

        } else if (parseInt(defo) < (parseInt(adult) + parseInt(child))) {

            $("#o_id" + row).val(defo);
        } else {
            if (defo >= pax && pax === 1) {
                $("#o_id" + row).val(1);
            } else if (defo >= pax && pax === 2) {
                $("#o_id" + row).val(2);
            } else if (defo >= pax && pax === 3) {
                $("#o_id" + row).val(3);
            } else if (defo >= pax && pax > 3) {
                $("#o_id" + row).val(4);
                swal('Default occupancy applied!');
            } else if (defo < pax && pax <= 3) {
                $("#o_id" + row).val(defo);
            } else {
                $("#o_id" + row).val(4);
                swal('Default occupancy applied!');
            }
        }

    } // End outer if

} // End function check_occupancy

function ratePlaningNB(row_no, sta_date, booking_source, room_rent_type, room_id, url, mealplan) {

//	var g_state = $('#g_state').val()
//	alert($('#g_state').val());
    if (window.bcid === 1) {
        let pax = $("#o_id" + row_no).val();
        var a;
        jQuery.ajax({
            type: "POST",
            url: url + "rate_plan/get_season",
            data: {
                start_dt: sta_date
            },
            success: function(data) {

                //alert("SEASON ID RESPONSE:" +data);
                //alert(data);
                myseason = data;
                //alert(myseason);

            },
            async: false,

        });


        if (room_rent_type == 'Basic') {
            room_rent_type = 'Base';
        } else if (room_rent_type == 'Seasonal') {

            room_rent_type = 's_room_charge';
        } else if (room_rent_type == 'Weekend') {

            room_rent_type = 'w_room_charge';
        } else {
            room_rent_type = 'Base';
        }



        if (mealplan == 'N/A' || mealplan == "no plan" || mealplan == '') {
            mealplan = 1;
        } else if (mealplan == 'EP') {
            mealplan = 2;
        } else if (mealplan == 'CP') {
            mealplan = 3;
        } else if (mealplan == 'MAP') {
            mealplan = 4;
        } else if (mealplan == 'AP') {
            mealplan = 5;
        }
	//alert('room_id' +room_id);
        $.ajax({
            url: url + "rate_plan/get_unit_type_id",
            type: "POST",
            data: {
                room_id: room_id
            },
            success: function(data) {
                //alert(data);//unit id
                unitType = data;
                //alert(unitType);


            },
            async: false,
        });

        if (booking_source == 'Frontdesk') {
            booking_source = 1;
        }
        booking_source = 1;

        $.ajax({
            url: url + "rate_plan/get_rmCh",
            type: "POST",
            data: {
                chType: room_rent_type,
                mplan: mealplan,
                source: booking_source,
                season: myseason,
                unitType: unitType,
                pax: pax
            },
            success: function(data) {
                //console.log(data);
                //	alert(data.meal_charge);
                meal_charge = data.meal_charge;
                room_charge_ea = data.room_charge_ea;
                room_charge_ec = data.room_charge_ec;
                meal_charge_ea = data.meal_charge_ea;
                meal_charge_ec = data.meal_charge_ec;

                $('#trr_' + row_no).val(data.room_charge).change();
                $('#trrc' + row_no).val(data.room_charge);

                val = data.room_charge;
                //alert(val);
                //console.log('RR' + val);

                var taxApplicable = $("#tax_applicable").val();
                if (taxApplicable == 'YES') {

                    //alert("Inside ajax" +val);
                    getTax(val, newstartDate, row_no, 'Booking');


                } else {

                    tax = 0;
                    swal("Else part for No tax Selection" + tax);
                }

            },
            async: false,
        });

        if (isNaN(tax) == false) {

            tax = 0;
        }

        totval = parseFloat(totval) + parseFloat(val);
        totfoodtax = parseFloat(totfoodtax) + parseFloat(tax);
        //console.log(totfoodtax);
        totmealchrg = parseFloat(totmealchrg) + parseFloat(meal_charge);
        var totprice = parseFloat(val) + parseFloat(meal_charge) + parseFloat(tax);

        $('#exra' + row_no).val(room_charge_ea);
        $('#exrc' + row_no).val(room_charge_ec);
        $('#exma' + row_no).val(meal_charge_ea);
        $('#exmc' + row_no).val(meal_charge_ec);
        $('#tfi_' + row_no).val(meal_charge).change();
        $('#tfic' + row_no).val(meal_charge).change();
        //$('#pdr_' +row_no).val(totprice);
        //$('#price_' +row_no).val(totprice * days).change();
        //console.log('Total - '+totprice * days);
    }
} // End function ratePlaningNB

function totalAmt() {
    var startDate = '';
    var endDate = '';
    var days = 0;
    var count = window.countRow;
    var total = 0;
    var tmp = 0;
    var trr = 0;
    var trt = 0;
    var tat = 0;
    var p = 0;
    var st = 0;
    var sc = 0;
    var lt = 0;
    var vt = 0;

    for (i = 1; i <= count; i++) {
        if ($("#startDate" + i).val()) {
            startDate = parseDate($("#startDate" + i).val());
            endDate = parseDate($("#endDate" + i).val());
            days = (endDate - startDate) / (1000 * 60 * 60 * 24);
            p = parseFloat($("#price_" + i).val());
            rrTot = parseFloat($("#trr_" + i).val());
            mpTot = parseFloat($("#tfi_" + i).val());
            rrTax = parseFloat($("#tax_" + i).val());
            vt = parseFloat($('#vat_' + i).val());
            total = total + p;
            trr = trr + (rrTot) * days;
            tmp = tmp + (mpTot) * days;
            trt = trt + (rrTax) * days;
            tat = tat + vt * days;
            //console.log('Ita - '+i+' | Days - '+days+' Total for Row = '+total);
        }
    }

    total = +total.toFixed(2);
    trt = +trt.toFixed(2);
    trr = +trr.toFixed(2);
    tmp = +tmp.toFixed(2);
    tat = +tat.toFixed(2);
    $("#bk_total").val(total);
    $("#bkrt_total").val(trt);
    $("#bkrr_total").val(trr);
    $("#bkmp_total").val(tmp);
    $("#bkft_total").val(tat);
} // Emd function totalAmount

function getVat(x) {
    let tfi = parseFloat($('#tfi_' + x).val());
    let sd = $('#startDate' + x).val();
    //console.log('getVat - '+getTax(1500,sd,x,'Meal Plan'));
    getTax(tfi, sd, x, 'Meal Plan');
}

function getRtax(x) {

    let trr = parseFloat($('#trr_' + x).val());
    let sd = $('#startDate' + x).val();
  // alert('trr' +trr);
    //console.log('getVat - '+getTax(1500,sd,x,'Meal Plan'));
    getTax(trr, sd, x, 'Booking');
}

function getTax(val, newstartDate, row_no, rType) {

	var g_state = $('#g_state').val();
	if(g_state == null || g_state ==''){
		
		g_state = $('#g_state1').val();
	}
	//alert('g_state' +g_state);			   
//	alert('g_state_gettax' +$('#g_state').val());
 console.log('g_state = ' + g_state);
    console.log('rType = ' + rType);

    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>dashboard/showtaxplan1",
        data: {
            price: val,
            args: 'a',
            type1: rType,
            dates: newstartDate,
			g_state:g_state
        },
        async: false,
        success: function(data) {

          //  alert(data);
            //console.log(data);
            let taxR = JSON.stringify(data);
            //$('#txrspns


            console.log('Tax Response - '+taxR);
            tax = data.total;
            roomId = data.room_id;
            if (rType === 'Booking') {
                $('#tax_' + row_no).val(tax);
                console.log('Success! getTax - Booking');
                $('#txrrspns_' + row_no).val(taxR);
            } else if (rType === 'Meal Plan') {
                $('#vat_' + row_no).val(tax);
                console.log('Success! getTax - Meal Plan');
                $('#txmrspns_' + row_no).val(taxR);
            }
            //console.log('Tax ajax: '+tax);

        },
    });
    //alert("HERE");

} // End function getTax


function formatDate(date) {
    var d = new Date(startDate),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function reCalculate(x) {
    var trr = $('#trr_' + x).val();
    //alert(trr);
    //alert(newstartDate);
    //alert(x);
    //alert(trr);
    var taxApplicable = $("#tax_applicable").val();
    if (taxApplicable == 'YES') {
        getTax(trr, newstartDate, x, 'Booking');
    } else {
        var tax = 0;
    }
    //	$('#tax_'+x).val(tax);
    var tfi = $("#tfi_" + x).val();
    var vat = $("#vat_" + x).val();
    var pdr = parseFloat(trr) + parseFloat(tax) + parseFloat(tfi) + parseFloat(vat);
    //$('#pdr_'+x).val(pdr);
    var startDate = parseDate($("#sd").val());
    var endDate = parseDate($("#ed").val());
    var days = (endDate - startDate) / (1000 * 60 * 60 * 24);
    var price = days * pdr;
    //$('#price_'+x).val(price);
}
Array.prototype.removeByValue = function(val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == val) {
            this.splice(i, 1);
            break;
        }
    }
} // End function reCalculate(x)

$("#btnpir").click(function() {

    clearRates(); // Resetting the rates & taxes to 0
    let arc = parseFloat($("#arc").val());
    let afi = parseFloat($("#afi").val());
    let crc = parseFloat($("#crc").val());
    let cfi = parseFloat($("#cfi").val());
    let adult = 0;
    let kid = 0;
    let pax = 0,
        exp = 0,
        ext = 0,
        xpa = 0,
        xpc = 0,
        trr = 0;
    let row = window.countRow;

    if (!isNaN(arc) && !isNaN(afi) && !isNaN(crc) && !isNaN(cfi)) {

        for (let i = 1; i <= row; i++) {

            adult = parseInt($("#ao_" + i).val());
            kid = parseInt($("#co_" + i).val());
            exp = parseInt($("#exp_" + i).val());
            trr = parseInt($("#trr_" + i).val());
            ext = $('#charge_mode_type1' + i).val();

            if (!isNaN(exp)) {
                if (ext === 'adult') {
                    xpa = exp;
                    xpc = 0;
                } else if (ext === 'child') {
                    xpa = 0;
                    xpc = exp;
                }
            } else {
                xpa = 0;
                xpc = 0;
            }

            pax = adult + kid;
            adult = adult + xpa;
            kid = kid + xpc;

            if (!isNaN(trr)) {
                $("#trr_" + i).val(arc * adult + crc * kid).change();
                $("#tfi_" + i).val(afi * adult + cfi * kid).change();
                console.log('Loop[' + i + '] - ' + (arc * adult + crc * kid));
                console.log('ARC - ' + arc);
                console.log('PAX - ' + pax);
            }

        } // Emd For Loop

    } else {

        swal('Please enter both the adult & kid individual rates properly!');
    }

}); // End #btnpir click function


function clearRates() {

    $(".trr").val(0).change();
    $(".tfi").val(0);
    $(".tax").val(0);
    $(".vat").val(0);

}


function getChange(amp) {
    $("#cmp").val(amp);
    $(".mp").val(amp);
}

function getChange1(amp) {
    $("#cmp1").val(amp);
    $(".mp").val(amp);
}

function getARC(amp) {
    $(".arc").val(amp);
}

function getAFI(amp) {
    $(".afi").val(amp);
}

function getCRC(amp) {
    $(".crc").val(amp);
}

function getCFI(amp) {
    $(".cfi").val(amp);
}

var occuM = 0;
var extraperson = 0;
var rc = 0;

function checkOccupancyNB(rid, x) {

	//console.log('checkOccupancyNB('+rid+', '+', '+x); 
    var u_id = $("#un_id_" +x).val();
    var adlt = parseInt($('#ao_' + x).val());
    var kd = parseInt($('#co_' + x).val());
    var max = parseInt($('#ng').val());
    let pax = adlt + kd;
    let a = 0,
        k = 0,
        t = 0;
    var count = window.countRow;

    for (i = 1; i <= count; i++) {

        a = parseInt($('#ao_' + i).val());
        k = parseInt($('#co_' + i).val());
        t = a + k + t;
    }

    if (t > max) {
        swal('The total no of adult & kid should be less than' + max + '!');
        $('#ao_' + x).val('');
        $('#co_' + x).val('')
    } else if (!isNaN(adlt) && !isNaN(kd)) {
		$('#loading-image5').show();
		
        $.ajax({
            type: "POST",
            // url: "<?php echo base_url(); ?>dashboard/roomOccu",
            url: "<?php echo base_url(); ?>dashboard/edit_unit_type_new",
            data: {
                u_id: u_id
            },
            async: false,
			//beforeSend: function () { $('#loading-image5').show(); },
			
            success: function(data) {

                let occuM = data.max_occupancy;
                let occuD = data.default_occupancy;

                if (occuM < pax) {

                    swal({
                            title: "Total No of Guest can not be greater than Max Occupancy (" + occuM + " Guest)",
                            text: "",
                            type: "error",
                            confirmButtonColor: "#f73f54"
                        },
                        function() {});

                    $('#ao_' + x).val('');
                    $('#co_' + x).val('');
                    $('#trr_' + x).val(0).change();
                    $('#tax_' + x).val(0);
                    $('#tfi_' + x).val(0);
                    $('#vat_' + x).val(0);
                    $('#exp_' + x).val(0);

                    pdrUpdate(x);
                    $('#myTable tbody tr td').find('span[id="extra_' + x + '"]').addClass('hideANDseek');
                } else if (pax <= occuD) {

                    $('#myTable tbody tr td').find('span[id="extra_' + x + '"]').addClass('hideANDseek');
                    $('#exp_' + x).val(0);
                    check_occupancy(x, occuM, occuD);
                } else if (pax <= occuM) {

                    var i = 0;
                    extraperson = pax - occuD;
                    $('#myTable tbody tr td').find('span[id="extra_' + x + '"]').removeClass('hideANDseek');
                    $('#exp_' + x).val(extraperson);
                    $('#exchf_' + x).css('color', '#000000');
                    $('#exadf_' + x).css('color', '#000000');
                    check_occupancy(x, occuM, occuD);
                    //console.log('Extra Person = '+extraperson);
                }

                indiRecalNB(x, rid);
            },
			
			complete:function(){
				setTimeout(function(){
					
					$('#loading-image5').hide();
					
				},500);
				
				 
			},
				
			
			
        }); // End AJAX
    } // End outer if


} // End function checkOccupancyNB


var click = 0;
var rcs = '';

function set_extra_charge(extraname, row) {
    if (window.bcid === 1) {
        let extraperson = parseInt($('#exp_' + row).val());
        let exra = parseFloat($('#exra' + row).val());
        let exrc = parseFloat($('#exrc' + row).val());
        let exma = parseFloat($('#exma' + row).val());
        let exmc = parseFloat($('#exmc' + row).val());
        let trrc = parseFloat($('#trrc' + row).val());
        let tfic = parseFloat($('#tfic' + row).val());
        let exrm = 0;
        let exmp = 0;



        console.log('eP' + $('#exp_' + row).val());
        //alert('Extra person' +$('#exp_'+row).val());
        //alert("exra" +exra +''+exma);
        if (extraname === 'adult') {

            exrm = extraperson * exra;
            exmp = extraperson * exma;
            $('#trr_' + row).val(trrc + exrm).change();
            $('#tfi_' + row).val(tfic + exmp).change();
            console.log('Ex Adlt - ' + exrm);
        } else if (extraname === 'child') {

            exrm = extraperson * exrc;
            exmp = extraperson * exmc;
            $('#trr_' + row).val(trrc + exrm).change();
            $('#tfi_' + row).val(tfic + exmp).change();
            console.log('Ex Chld - ' + exrm);
        }
    }

} // End function set_extra_charge

function Odd_set_extra_charge() {
    var oid = '';

    if (window.bcid === 1) {
        var mp = $('#meal_plan_type').val();
        var source = $('#booking_source').val();
        rcs = $('#trr_' + row).val();

        if (click == 0) {
            click = 1;
            rc = rcs;
        }

        //alert('Exp - '+extraperson);
        if (extraname == 'adult') {

            oid = 5;
        } else {

            oid = 6;
        }

        //alert(oid);

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>dashboard/extrachargeNB",
            data: {
                oid: oid,
                myseason: myseason,
                unitType: unitType,
                meal: mp,
                booking: source
            },
            async: false,
            success: function(data) {

                //alert(data);

                //console.log(data);

                var totrc = parseFloat(rc) + (parseFloat(data.extra_room_charge) * extraperson);
                var fi = $('#tfi_' + row).val();
                var totfi = parseFloat(fi) + parseFloat(data.meal_charge);
                $('#trr_' + row).val(totrc).change();
                $('#tfi_' + row).val(totfi).change();

                //console.log(totrc);
                //console.log(totfi);
            },
        });

        //alert();
    }
}



$("#btnSubmit").click(function() {
    var values1 = $("input[name='adultNO[]']")
        .map(function() {
            return $(this).val();
        }).get();
    var total1 = 0;
    for (var i = 0; i < values1.length; i++) {
        total1 += parseInt(values1[i]);
    }
    var values2 = $("input[name='childNO[]']")
        .map(function() {
            return $(this).val();
        }).get();
    var total2 = 0;
    for (var i = 0; i < values2.length; i++) {
        total2 += parseInt(values2[i]);
    }
    var total = total1 + total2;
    var noOfGuest = $("#ng").val();
    var drr = $("#trrID").val();
    if (drr > 0) {
        $('form#myForm').submit();
    } else {
        if (total == noOfGuest) {
            $('form#myForm').submit();
        } else {
				swal({
                    title: "PAX Mismatch!",
                    text: "Number of guest and total number of Adult and child should be equal",
                    type: "error"
                },
                function() {
					
				});
            return false;
        }
    }
});




$("#btn2").click(function() {

    let rr = $("#tfiID").val();
    let row = window.countRow;

    for (let i = 1; i <= row; i++) {

        $("#tfi_" + i).val(rr).change();

    }

});


$("#btnx").click(function() { // Big Function by Ashes Da
    var val1 = $("#trrID").val();
    if (val1 > 0) {
        var val2 = $("#tfiID").val();
        $(".trr").val(val1);

        var startDate = parseDate($("#sd").val());
        var endDate = parseDate($("#ed").val());
        var days = (endDate - startDate) / (1000 * 60 * 60 * 24);

        var taxApplicable = $("#tax_applicable").val();
        if (taxApplicable == 'YES') {
            if ((val1 >= slab1) && (val1 <= slab2)) {
                var tax = (parseFloat(val1) * parseFloat(dbTax1)) / 100;
            } else if ((val1 >= slab) /*&& (val1 <= slab3)*/ ) {
                var tax = (parseFloat(val1) * parseFloat(dbTax2)) / 100;
            } else {
                var tax = (parseFloat(val1) * parseFloat(dbTax3)) / 100;
            }
        } else {
            var tax = 0;
        }
        if (val2 > 0) {
            var val3 = parseFloat(val1) + parseFloat(val2) + parseFloat(tax);
        } else {
            var val3 = parseFloat(val1) + parseFloat(tax);
        }
        var val4 = days * val3;
        $(".tax").val(tax);
        $(".pdr").val(val3);
        $(".price").val(val4);
    } else {
        //alert("Please enter value.");
        swal({
                title: "Please enter value",
                text: "",
                type: "error"
            },
            function() {});
        return false;
    }

    setTimeout(totalAmt(), 2500);

});

$("#btn1").click(function() {

    let rr = $("#trrID").val();
    let row = window.countRow;

    for (let i = 1; i <= row; i++) {

        $("#trr_" + i).val(rr).change();

    }

});

$("#btny").click(function() { // Big Function by Ashes Da
    var val1 = $("#trrID").val();
    var val2 = $("#tfiID").val();
    if (val2 > 0) {
        if (val1 > 0) {
            var tax = $(".tax").val();
            var trr = $(".trr").val();
            if (trr > 0) {
                var x = parseFloat(tax) + parseFloat(trr) + parseFloat(val2);
            } else {
                var x = parseFloat(val2);
            }
            var vat = (parseFloat(val2) * 1000) / 1145;
            $(".pdr").val(x);
            var startDate = parseDate($("#sd").val());
            var endDate = parseDate($("#ed").val());
            var days = (endDate - startDate) / (1000 * 60 * 60 * 24);
            var val4 = days * parseFloat(x);
            var m = parseFloat(val2) - parseFloat(vat);
            $(".price").val(val4);
            $(".vat").val(m);
            $(".tfi").val(vat);
        } else {
            var startDate = parseDate($("#sd").val());
            var endDate = parseDate($("#ed").val());
            var days = (endDate - startDate) / (1000 * 60 * 60 * 24);
            var vat = (parseFloat(val2) * 1000) / 1145;
            var m = parseFloat(val2) - parseFloat(vat);
            var values1 = $("input[name='pdr[]']")
                .map(function() {
                    return $(this).val();
                }).get();
            //alert(values1);
            $(".vat").val(m);
            $(".tfi").val(vat);
            var total = 0;
            for (var i = 1; i <= values1.length; i++) {
                var xVal = parseFloat($("#pdr_" + i).val()) + parseFloat(val2);
                var tVal = days * parseFloat(xVal);
                //$("#pdr_"+i).val(xVal);
                //$("#pdr_"+i).val(xVal);
                //	$("#price_"+i).val(tVal);
            }
        }
    } else {
        //alert("Please enter value.");
        swal({
                title: "Please enter value",
                text: "",
                type: "error"
            },
            function() {});
        return false;
    }

    setTimeout(totalAmt(), 2500);
});

/*jQuery(document).ready(function(){
    jQuery('#hideshow').live('click', function(event) {        
         jQuery('#ex-charge').toggle('show');
    });
});*/

var checkSubmit = () => {
	
    var count = window.countRow;
    var flag;
    let adult;
    let child;
    var startDateO;
    var endDateO;
    var startDate;
    var endDate;
    var days;
	var chkin_time
	var chkout_time;
	var def_chkout_time;

    var arr = [];
    flag = 1;

    for (i = 1; i <= count; i++) {
		
        startDate = parseDate($("#startDate" + i).val());
        chkin_time = $("#checkintime"+i).val();
        endDate = parseDate($("#endDate" + i).val());
		chkIt = chkin_time.split(':');
		chkout_time = $("#checkouttime"+i).val();
		chkOt = chkout_time.split(':');
	    def_chkout_time = $("#checkout_time").val();
		
		startDate = new Date(startDate.getTime() + (parseInt(chkIt[0])*60 + parseInt(chkIt[1]))*60000);
		endDate = new Date(endDate.getTime() + (parseInt(chkOt[0])*60 + parseInt(chkOt[1]))*60000);
		
        endTime = parseDate($("#endDate" + i).val());
        days = parseFloat((endDate - startDate) / (1000 * 60 * 60 * 24));
        std = $("#startDate" + i).val();
        end = $("#endDate" + i).val();
        rmid = $("#rmo_" + i).val();
		
		/*console.log(days <= 0);
		console.log( $("#endDate" + i).val() == '');
		console.log($("#endDate" + i).val() == null);*/
		console.log('Dates: '+startDate+' '+' '+endDate + ' | Days: '+days+' | '+(days <= 0 || $("#endDate" + i).val() == '' || $("#endDate" + i).val() == null));
		
        if (days <= 0 || $("#endDate" + i).val() == '' || $("#endDate" + i).val() == null) {
            flag = 0;
            $("#endDate" + i).css('color', 'red');
            $("#endDate" + i).css('border', '1px solid red');
        } else {
            $("#endDate" + i).css('color', 'green');
            $("#endDate" + i).css('border', 'none');
        }
		
		console.log('Room No: '+rmid);
		
        if (rmid > 0) {

            $.ajax({

                type: "POST",
                url: "<?php echo base_url(); ?>setting/check_avail_room_final1",
                datatype: 'json',
                async: false,
                data: {
                    std: std,
                    end: end,
                    rmid: rmid
                },
                success: function(data) {

                    //console.log('check_avail_room_final1 => '+data);

                    if (data) {

                        flag = 4;
                        $("#endDate" + i).css('color', 'red');
                        $("#endDate" + i).css('border', '1px solid red');
                        //swal('Unit '+ rmid +' is blocked between daterange' + std + 'to ' + end);
						swal({
							title: "Unit Blocked!",
							text: 'Unit '+ rmid +' is blocked between daterange' + std + 'to ' + end + ' please remove the unit from UAL',
							type: "error",
							confirmButtonColor: "#F27474"
						});
                        return flag;
						
                    } else {
						
                    }
                },
            });
        }

        if (i > 1) {
            for (j = 1; j <= (arr.length - 1); j++) {
				console.log(startDate+' > '+arr[j]+' | ');
				console.log(startDate >= arr[j] || $("#startDate" + j).val() == '' || $("#startDate" + j).val() == null);
                if (startDate > arr[j] || $("#startDate" + j).val() == '' || $("#startDate" + j).val() == null) {
                    flag = 0; 
                    $("#startDate" + i).css('color', 'red');
                    $("#startDate" + j).css('border', '1px solid red');
                    $("#endDate" + j).css('color', 'red');
                } else {
                    $("#startDate" + i).css('color', 'green');
                    $("#startDate" + j).css('border', 'none');
                    $("#endDate" + j).css('color', 'green');
                }
            }
        }

        startDateO = parseDate($("#startDate" + i).val());
        endDateO = parseDate($("#endDate" + i).val());
        arr[i] = endDate;
    }

    return flag;

} // End function checkSubmit


$("#hideshow").click(function() {
    $('#ex-charge').toggle('show');
});

//$("#next").click(function(){
$("#next").on('click', function() {

    //alert('commission_applicable');
    var commission_applicable11 = $('#commission_applicable').val();
    //alert(commission_applicable11);
    var flag = checkSubmit();

    if (flag === 0) {
        swal({
            title: "Date Conflict!",
            text: "Plase check the date range",
            type: "error",
            confirmButtonColor: "#F27474"
        });
    } else if (flag === 0) {
        swal({
            title: "Date Conflict!",
            text: "Plase check the date range",
            type: "error",
            confirmButtonColor: "#F27474"
        });
    } else if (flag === 4) {

        swal({
            title: "Date and Booking Conflict!",
            text: "Plase check the date range and room available",
            type: "error",
            confirmButtonColor: "#F27474"
        });


    } else {

        //console.log('next');
        //alert($("#next").val());
        var values11 = $("input[name='adultNO[]']")
            .map(function() {
                return $(this).val();
            }).get();
        //console.log(values11);
        var total11 = 0;
        $.each(values11, function() {
            total11 += parseInt(this);
        });
        //console.log(total11);
        var values111 = $("input[name='childNO[]']")
            .map(function() {
                return $(this).val();
            }).get();
        //console.log(values111);
        var total111 = 0;
        $.each(values111, function() {
            total111 += parseInt(this);
        });
        //console.log(total111);
        var totalGst = total11 + total111;
        console.log('totalgst' + totalGst);
        var noOfGuest = $("#ng").val();
        if (noOfGuest != totalGst) {
            swal("Total number of adult & child must be equal to Number of Guest");
            return false;
        } else {
            $('#totalsss').toggle('show');
            var values1 = $("input[name='price[]']")
                .map(function() {
                    return $(this).val();
                }).get();
            //console.log(values1);
            var total1 = 0;
            $.each(values1, function() {
                total1 += parseFloat(this);
            });
            var values2 = $("input[name='vat[]']")
                .map(function() {
                    return $(this).val();
                }).get();
            //alert(values1);
            var total2 = 0;
            $.each(values2, function() {
                total2 += parseFloat(this);
            });
            var values3 = $("input[name='tfi[]']")
                .map(function() {
                    return $(this).val();
                }).get();
            //alert(values1);
            var total3 = 0;
            $.each(values3, function() {
                total3 += parseFloat(this);
            });
            var values4 = $("input[name='tax[]']")
                .map(function() {
                    return $(this).val();
                }).get();
            //alert(values1);
            var total4 = 0;
            $.each(values4, function() {
                total4 += parseFloat(this);
            });
            var values5 = $("input[name='trr[]']")
                .map(function() {
                    return $(this).val();
                }).get();
            //alert(values1);
            var total5 = 0;
            $.each(values5, function() {
                total5 += parseFloat(this);
            });
            var values6 = $("input[name='total[]']")
                .map(function() {
                    return $(this).val();
                }).get();
            //alert(values1);
            var total6 = 0;
            $.each(values6, function() {
                total6 += parseFloat(this);
            });
            var values7 = $("input[name='totalTax[]']")
                .map(function() {
                    return $(this).val();
                }).get();
            //alert(values1);
            var total7 = 0;
            $.each(values7, function() {
                total7 += parseFloat(this);
            });
            total1 = Math.round(total1 * 100) / 100;
            total2 = Math.round(total2 * 100) / 100;
            total3 = Math.round(total3 * 100) / 100;
            total4 = Math.round(total4 * 100) / 100;
            total5 = Math.round(total5 * 100) / 100;
            total6 = Math.round(total6 * 100) / 100;
            total7 = Math.round(total7 * 100) / 100;
            //alert(total);	
            $("#bid").css("display", "none");
            $("#lastid").css("display", "block");
            $("#tp1").text(total1);
            $("#totprice").val(total1);
            $("#tp2").text(total2);
            $("#tp3").text(total3);
            $("#tp4").text(total4);
            $("#tp5").text(total5);
            $("#tp6").text(total6);
            $("#tp7").text(total7);
            var broker_commission = parseFloat($('#broker_commission').val());
            var r1 = parseFloat(total5);
            var mi1 = parseFloat(total3);
            //r1=r1+mi1;
            //var commission_applicable1=parseFloat($('#commission_applicable')val());
            //alert(commission_applicable11);
            if (commission_applicable11 == '1') {
                r1 = r1;
            } else if (commission_applicable11 == '2') {
                r1 = mi1 + r1;
            } else if (commission_applicable11 == '3') {
                r1 = total1;
            } else {
                r1 = 0;
            }
            var applyedAmount = r1;
            //alert(r1+': this is ');
            $('#applyedAmount').val(applyedAmount);
            if (isNaN(broker_commission)) {
                broker_commission = 0;

            }
            r1 = r1 * (broker_commission / 100);
            $("#tp8").text(r1);
            $("#totcom").val(r1);
        }

    } // end of else

});

$("#reCal").click(function() {
    var values11 = $("input[name='adultNO[]']")
        .map(function() {
            return $(this).val();
        }).get();
    //alert(values1);
    var total11 = 0;
    $.each(values11, function() {
        total11 += parseInt(this);
    });
    var values111 = $("input[name='childNO[]']")
        .map(function() {
            return $(this).val();
        }).get();
    //alert(values1);
    var total111 = 0;
    $.each(values111, function() {
        total111 += parseInt(this);
    });
    var totalGst = total11 + total111;
    var noOfGuest = $("#ng").val();
    if (noOfGuest != totalGst) {
        //alert("Total numbar of adult & child must be equal to Number of Guest");
        swal({
                title: "Total number of adult & child=" + totalGst + "must be equal to Number of Guest=" + noOfGuest,
                text: "",
                type: "error"
            },
            function() {});
        return false;
    } else {
        //$('#totalsss').toggle('show');
        var values1 = $("input[name='price[]']")
            .map(function() {
                return $(this).val();
            }).get();
        //alert(values1);
        var total1 = 0;
        $.each(values1, function() {
            total1 += parseFloat(this);
        });
        var values2 = $("input[name='vat[]']")
            .map(function() {
                return $(this).val();
            }).get();
        //alert(values1);
        var total2 = 0;
        $.each(values2, function() {
            total2 += parseFloat(this);
        });
        var values3 = $("input[name='tfi[]']")
            .map(function() {
                return $(this).val();
            }).get();
        //alert(values1);
        var total3 = 0;
        $.each(values3, function() {
            total3 += parseFloat(this);
        });
        var values4 = $("input[name='tax[]']")
            .map(function() {
                return $(this).val();
            }).get();
        //alert(values1);
        var total4 = 0;
        $.each(values4, function() {
            total4 += parseFloat(this);
        });
        var values5 = $("input[name='trr[]']")
            .map(function() {
                return $(this).val();
            }).get();
        //alert(values1);
        var total5 = 0;
        $.each(values5, function() {
            total5 += parseFloat(this);
        });
        var values6 = $("input[name='total[]']")
            .map(function() {
                return $(this).val();
            }).get();
        //alert(values1);
        var total6 = 0;
        $.each(values6, function() {
            total6 += parseFloat(this);
        });
        var values7 = $("input[name='totalTax[]']")
            .map(function() {
                return $(this).val();
            }).get();
        //alert(values1);
        var total7 = 0;
        $.each(values7, function() {
            total7 += parseFloat(this);
        });
        total1 = Math.round(total1 * 100) / 100;
        total2 = Math.round(total2 * 100) / 100;
        total3 = Math.round(total3 * 100) / 100;
        total4 = Math.round(total4 * 100) / 100;
        total5 = Math.round(total5 * 100) / 100;
        total6 = Math.round(total6 * 100) / 100;
        total7 = Math.round(total7 * 100) / 100;
        //alert(total);	
        //$("#bid").css("display", "none");
        $("#lastid").css("display", "block");
        $("#tp1").text(total1);
        $("#tp2").text(total2);
        $("#tp3").text(total3);
        $("#tp4").text(total4);
        $("#tp5").text(total5);
        $("#tp6").text(total6);
        $("#tp7").text(total7);
    }
});

/*function calculation(value) {
	var a=document.getElementById("c_quantity").value;
	var total=parseInt(a)*parseInt(value);
	document.getElementById("c_total").value=total;
	
}
function calculation2(value) {
	if(value > '0'){
		var a=document.getElementById("c_total").value;
		var total=parseInt(a)+(parseInt(a)*(parseInt(value)/100));
		document.getElementById("c_total").value=total;
    }
}*/

function calculation(value) {
    if (value > '0') {
        var a = document.getElementById("c_quantity").value;
        var total = parseFloat(a) * parseFloat(value);
        document.getElementById("c_total").value = total;
    } else {
        document.getElementById("c_total").value = '0'
        document.getElementById("c_tax").value = '0'
    }

}

function calculation2(value) {
    if (value > '0') {
        var a = document.getElementById("c_total").value;
        var total = parseFloat(a) + (parseInt(a) * (parseFloat(value) / 100));
        document.getElementById("c_total").value = total;
    } else {
        var x = document.getElementById("c_unit_price").value;
        var y = document.getElementById("c_quantity").value;
        document.getElementById("c_total").value = parseInt(x) * parseInt(y);
    }

}


$("#addCharge").click(function() {
    var c_name = $("#c_name").val();
    var c_quantity = $("#c_quantity").val();
    var c_unit_price = $("#c_unit_price").val();
    var c_tax = $("#c_tax").val();
    var c_total = $("#c_total").val();
    var totalTax = (c_quantity * c_unit_price * c_tax) / 100;
    totalTax = Math.round(totalTax * 100) / 100;
    x = x + 1;
    if (c_name == "") {
        //alert("Please enter name.");
        swal({
                title: "Please enter name",
                text: "",
                type: "error"
            },
            function() {});
        return false;
    }
    if (c_quantity == "") {
        //alert("Please enter quantity.");
        swal({
                title: "Please enter quantity",
                text: "",
                type: "error"
            },
            function() {});
        return false;
    }
    if (c_unit_price == "") {
        //alert("Please enter price.");
        swal({
                title: "Please enter price",
                text: "",
                type: "error"
            },
            function() {});
        return false;
    }
    $('#extra_service tr:last').before('<tr id="row_' + x + '"><td><input name="totalTax[]" id="c_name55" type="hidden" value="' + totalTax + '" class="form-control input-sm"><label><input name="cname[]" id="c_name5" type="text" value="' + c_name + '" class="form-control input-sm" readonly></label></td><td><label><input name="qty[]" id="c_name1" type="text" value="' + c_quantity + '" class="form-control input-sm" readonly></label></td><td class="hidden-480"><label><input name="up[]" id="c_name2" type="text" value="' + c_unit_price + '" class="form-control input-sm" readonly></label></td><td class="hidden-480"><label><input name="ctax[]" id="c_name3" type="text" value="' + c_tax + '" class="form-control input-sm" readonly></label></td><td class="hidden-480"><label><input name="total[]" id="c_name4" type="text" value="' + c_total + '" class="form-control input-sm" readonly></label></td><td><a href="javascript:void(0);" class="btn red btn-xs" onclick="removeRow(' + x + ')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>');
    $("#c_name").val("");
    $("#c_quantity").val("");
    $("#c_unit_price").val("");
    $("#c_tax").val("");
    $("#c_total").val("");
});

function removeRow(val) {
    $('#row_' + val).remove();
}

var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

$(document).ready(function() {

    window.countRow = 0;
    window.bcid = 1;
    $('#loading-image').hide();
    $('#loading-image1').hide();
	$('#loading-image5').hide();

	$('#loading-image7').hide();

    $('.date-picker').datepicker({
            orientation: "left",
            startDate: '-30d',
            autoclose: true,
            todayHighlight: true
    })
		
    .on('changeDate', function(ev) {
            var selected1 = $("#sd").val();
            var selected2 = $("#ed").val();
            //alert("HERE");
            $.ajax({
                url: '<?php echo base_url(); ?>dashboard/get_available_room2',
                type: "POST",
                data: {
                    date1: selected1,
                    date2: selected2
                },
                success: function(data) {
                    $("#hd").val(data);
					console.log('no of available room: '+data);
                },
            });
    });


});

function dayCal(x) {

    //console.log('I am inside dayCal(' + x + ')');
    var pdr = parseFloat($("#pdr_" + x).val());
    var startDate = parseDate($("#startDate" + x).val());
    var endDate = parseDate($("#endDate" + x).val());
    //var days = (endDate - startDate) / (1000 * 60 * 60 * 24);
	let days = (datesGrp('line',4,x) - datesGrp('line',3,x)) / (1000 * 60 * 60 * 24);
    var tot = +(pdr * days).toFixed(2);
    $("#startDateA" + x).val(datesGrp('line',1,x));
    $("#endDateA" + x).val(datesGrp('line',2,x));
    $("#days_" + x).val(days);
    $("#price_" + x).val(pdr * days).change();
}

function chkintime(x){
	//console.log('I am inside chkintime(' + x + ')');
	var chkin_time = $("#checkintime"+x).val();
	var def_chkin_time = $("#def_checkin_time").val();
	
	var ctime=chkin_time.split(/:/);
	var chk_time=ctime[0]*3600+ctime[1]*60;
	
	var dctime=def_chkin_time.split(/:/);
	var chkd_time=dctime[0]*3600+dctime[1]*60;
	
	
	//alert(chk_time);
	//alert(chkin_time);
	//alert(def_chkin_time);
}
function chkouttime(x){
	//console.log('I am inside chkouttime(' + x + ')');
	var chkout_time = $("#checkouttime"+x).val();
	var def_chkout_time = $("#checkout_time").val();
	
	var ctime=chkout_time.split(/:/);
	var chk_time=ctime[0]*3600+ctime[1]*60;
	
	var dctime=def_chkout_time.split(/:/);
	var chkd_time=dctime[0]*3600+dctime[1]*60;
	
}
$("#addGuest").click(function() {
    $('#myModalNorm1').modal('show');
    $('#return_guest').html("");
    $("#return_guest").css("display", "none");
});

function return_guest_search() {

    var guest_name = $('#guest_search').val();
    //alert(guest_name);

    jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>bookings/return_guest_search",
        datatype: 'json',
        data: {
            guest: guest_name
        },
        success: function(data) {
            var resultHtml = '';
            resultHtml += '<table class="table table-bordered" cellpadding="0" cellspacing="0" ><thead><tr><th>Name</th><th>Address</th><th width="100">Mobile No</th></tr></thead><tbody>';
            $.each(data, function(key, value) {
                resultHtml += '<tr  class="crsr collapsed" data-toggle="collapse" data-target="#toggleDemo' + value.g_id + '">';
                resultHtml += '<td>' + value.g_name + '</td>';
                resultHtml += '<td>' + value.g_address + '</td>';
                resultHtml += '<td>' + value.g_contact_no + '</td>';
                resultHtml += '</tr><tr id="toggleDemo' + value.g_id + '"  class="mrgn">';
                resultHtml += '<td class="no-marin"  colspan="3"  cellpadding="0" cellspacing="0">';
                resultHtml += '<div class="side clearfix" style="width:100%;"><div class="col-xs-7 ex"><p><label style="font-size:13px;">' + value.g_name + '</label><br><label>';

                resultHtml += '<input type="hidden" id="g_id_hide"  value="' + value.g_id + '" ></input>';
                resultHtml += '<label style="font-size:13px;">Room No: ' + value.room_no + '</label><br><label style="font-size:13px;">Last Check In Date: ' + value.cust_from_date + '</label><br> <button class="act active btn blue btn-sm" name="g_id" id="g_id"" value="' + value.g_id + '" onclick="get_guest(' + value.g_id + ')" >Continue</button> </p></div>';
                if (value.g_photo_thumb != '') {
                    resultHtml += '<div class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                        '<img src="<?php echo base_url() ?>upload/guest/' + value.g_photo_thumb + '" width="100px" height="92px"  > ' +
                        '</div>';
                } else {
                    resultHtml += '<div  class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                        '<img src="<?php echo base_url() ?>upload/guest/no_images.png" width="100px" height="92px" > ' +
                        '</div>';
                }
                //resultHtml+='<td><input type="radio" name="g_id" id="'+value.g_id+'" value="'+value.g_id+'" onclick="get_guest(this.value)" /></td>';
                resultHtml += '</div></td></tr>';
            });
            resultHtml += '</tbody></table>';
            $('#return_guest').html(resultHtml);
            //alert(data);
            // console.log(data);
            //$('#dtls').html(data);
        }
    });
    $("#return_guest").css("display", "block");
    return false;


}

function get_guest(id) {
    var g_id = id;
    jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>bookings/return_guest_get",
        datatype: 'json',
        data: {
            guest_id: g_id
        },
        success: function(data) {
            //alert(JSON.stringify(data));
            $("#myModalNorm1").modal("hide")
            $('#gn').val(data.g_name);
            $('#guestID').val(data.g_id);
            $('#gp').val(data.g_pincode);
            $('#g_phn').val(data.g_contact_no);
            $('#g_email').val(data.g_email);
            $('#gp').addClass('focus');
            $('#guest_search').val("");
        }
    });
    return false;
}

function insert_commision(val) {
    //alert(val);
}
$(document).on('blur', '#sd', function() {
    $('#sd').addClass('focus');
});
$(document).on('blur', '#ed', function() {
    $('#ed').addClass('focus');
});
$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
    var target = $(e.target).attr("href") // activated tab
    //alert(target);
    if (target = '#individual') {
        $("#trrID").val("");
        $("#tfiID").val("");
    }
    if (target = '#default') {
        $("#arc").val("");
        $("#afi").val("");
        $("#crc").val("");
        $("#cfi").val("");
    }
});

$("#newBtn").click(function() {
    var noRoom = $("#nr").val();
    //alert(noRoom);
    return false
    var x1 = $("#trrID").val();
    var x2 = $("#arc").val();
    var x3 = $("#crc").val();
    var x4 = $("#tfiID").val();
    var ng = parseInt($("#ng").val());

    for (x = 1; x <= noRoom; x++) {
        var days = parseInt($("#days_" + x).val());
        if ($("#arc1_" + x).val() != "") {
            var arc1 = parseInt($("#arc1_" + x).val());
        } else {
            var arc1 = '0';
        }

        if ($("#afi1_" + x).val() != "") {
            var afi1 = parseInt($("#afi1_" + x).val());
        } else {
            var afi1 = '0';
        }

        if ($("#crc1_" + x).val() != "") {
            var crc1 = parseInt($("#crc1_" + x).val());
        } else {
            var crc1 = '0';
        }

        if ($("#cfi1_" + x).val() != "") {
            var cfi1 = parseFloat($("#cfi1_" + x).val());
        } else {
            var cfi1 = '0';
        }


        if ($("#ao_" + x).val() != "") {
            var ao = parseInt($("#ao_" + x).val());
        } else {
            var ao = '0';
        }
        if ($("#co_" + x).val() != "") {
            var co = parseInt($("#co_" + x).val());
        } else {
            var co = '0';
        }

        var to = parseInt($("#ao_" + x).val()) + parseInt($("#co_" + x).val());
        var mo = $("#mo_" + x).val();
        if (x4 == "") {
            if (x1 == "" && x2 == "" && x3 == "") {
                //alert("HERE1");
                var fi = (ao * afi1) + (co * cfi1);
                var rr = $("#trr_" + x).val();
                var tx = $("#tax_" + x).val();
                var tv = parseFloat(fi) + parseFloat(rr) + parseFloat(tx);
                var ttv = days * parseFloat(tv);
                //$("#pdr_"+x).val(tv);
                //$("#price_"+x).val(ttv);
                var vat = (parseFloat(fi) * 1000) / 1145;
                var m = parseFloat(fi) - parseFloat(vat);
                //$("#tfi_"+x).val(vat);
                //$("#vat_"+x).val(m);				
            } else {
                //alert("HERE");
                var val1 = (ao * arc1) + (ao * afi1);
                var val2 = (co * crc1) + (co * cfi1);
                if (x1 == "") {
                    var val11 = (ao * arc1) + (co * crc1);
                } else {
                    var val11 = x1;
                }
                var val22 = (ao * afi1) + (co * cfi1);

                /*alert(val1);
                //alert(val2);
                //alert(val11);
                //alert(val22);*/

                $("#trr_" + x).val(val11);
                var taxApplicable = $("#tax_applicable").val();
                if (taxApplicable == 'YES') {
                    /*if(val11 < slab){
                    	var tax = (parseFloat(val11) * parseFloat(dbTax1))/100;
                    } else {
                    	var tax = (parseFloat(val11) * parseFloat(dbTax2))/100;
                    }*/

                    if ((val11 >= slab1) && (val11 <= slab2)) {
                        var tax = (parseFloat(val11) * parseFloat(dbTax1)) / 100;
                    } else if ((val11 >= slab) /*&& (val11 <= slab3)*/ ) {
                        var tax = (parseFloat(val11) * parseFloat(dbTax2)) / 100;
                    } else {
                        var tax = (parseFloat(val11) * parseFloat(dbTax3)) / 100;
                    }
                } else {
                    var tax = 0;
                }
                if (x1 == "") {
                    var val3 = parseFloat(val1) + parseFloat(val2) + parseFloat(tax);
                } else {
                    var val3 = parseFloat(x1) + parseFloat(val22) + parseFloat(tax);
                }
                var val4 = days * parseFloat(val3);
                $("#tax_" + x).val(tax);

                //$("#pdr_"+x).val(val3);
                //			$("#price_"+x).val(val4);
                var vat = (parseFloat(val22) * 1000) / 1145;
                var m = parseFloat(val22) - parseFloat(vat);
                //$("#tfi_"+x).val(vat);
                //$("#vat_"+x).val(m);

            }
        }

    }
});

</script>
<script>

let datesGrp = (mode,typ,x) => {
	
	if(mode == 'globl'){
		console.log('globl');
		
		var selected1 = $("#sd").val();
		var selected2 = $("#ed").val();
		selected1 = new Date(selected1);
		selected2 = new Date(selected2);
		var chkin_time = $("#checkin_time").val();	
		var chkout_time = $("#checkout_time").val();
		var chkIt = chkin_time.split(':');
		var chkOt = chkout_time.split(':');
		
	} else if(mode == 'line') {
		
		console.log('line');
		
		var selected1 = $("#startDate"+x).val();
		var selected2 = $("#endDate"+x).val();
		selected1 = new Date(selected1);
		selected2 = new Date(selected2);
		var chkin_time = $("#checkintime"+x).val();	
		var chkout_time = $("#checkouttime"+x).val();
		var chkIt = chkin_time.split(':');
		var chkOt = chkout_time.split(':');

	}
				let def_chkin_time = $("#def_checkin_time").val();
				let def_chkout_time = $("#def_checkout_time").val();
				
				let chkItD = def_chkin_time.split(':');
				let chkOtD = def_chkout_time.split(':');
				
				//console.log(chkIt[0] +' < '+ chkItD[0]);
				//console.log(parseInt(chkIt[0]) < parseInt(chkItD[0]));
				
				if((parseInt(chkIt[0]) < parseInt(chkItD[0])) || (parseInt(chkIt[0]) == parseInt(chkItD[0]) && (parseInt(chkIt[1]) < parseInt(chkItD[1])))){
					selected1.setDate( selected1.getDate() - 1 );
					console.log('Yarly Date checkin '+selected1);
				}
				
				if((parseInt(chkOt[0]) > parseInt(chkOtD[0])) || (parseInt(chkOt[0]) == parseInt(chkOtD[0]) && (parseInt(chkOt[1]) > parseInt(chkOtD[1])))){
					selected2.setDate( selected2.getDate() + 1 );
					console.log('Late Date checkout '+selected2);
				}
				
				if(typ == 3)
					return selected1;
				else if(typ == 4)
					return selected2;
				
				let selected1s = selected1.getFullYear() + "-" + (selected1.getMonth()+1) + "-" + selected1.getDate();
				let selected2s = selected2.getFullYear() + "-" + (selected2.getMonth()+1) + "-" + selected2.getDate();
				
				if(typ == 1){
					return selected1s;
				}
				else if(typ == 2){
					return selected2s;
				}
}

function getVal(){
	
	//console.log('datesGrp():');
	//console.log('datesGrp(): '+datesGrp(1)+'  '+datesGrp(2));
	
	var set_agent_ota = $("#set_agent_ota").val();
	var commission_applicable=$("#commission_applicable").val();
	var selected1;
	//alert(commission_applicable);
	//alert(set_agent_ota);
	var selected2;
	var guest_state = $('#g_state').val();
	var guest_country = $('#g_country').val();
	var noOfGuest = $("#ng").val();
	var noOfRoom  = $("#nr").val();
	var guestName = $("#gn").val();
	var guestPin = $("#gp").val();
	var profitCenter = $("#pc").val();
	var travel_agent_type = $("#travel_agent_type").val();
	
	//	alert('noOfGuest' +noOfGuest);
	//	alert('noOfRoom' +noOfRoom);
	var room_rent_type = $("#room_rent_type").val();
	var booking_source = $("#booking_source").val();
	var tax_applicable = $("#tax_applicable").val();
	var meal_plan_id = $('#meal_plan_type').val();
	var startDate = parseDate($("#sd").val());
	var endDate = parseDate($("#ed").val());
	var days = (endDate-startDate)/(1000*60*60*24);
	if(profitCenter == '0'){
		//alert('Please select profit center');
		 swal({
				title: "Please select profit center",
				text: "",
				type: "error",
				confirmButtonColor: "#F73F54"
			},
			function(){                      
			});
		return false;
	}
	
	if(guest_state == ''){
		
		swal({
				title: "Please Select Guest's state",
				text: "",
				type: "error",
				confirmButtonColor: "#F73F54"
			
			
		},
		function(){	
		});
		return false;
		
	}
	
	if(guest_country == ''){
		
		swal({
				title: "Please Select Guest's Country",
				text: "",
				type: "error",
				confirmButtonColor: "#F73F54"
			
			
		},
		function(){	
		});
		return false;
		
	}
	
	
	if(noOfGuest == ""){
		//alert('Please enter number of guest.');
		swal({
				title: "Please enter the number of guest",
				text: "",
				type: "error",
				confirmButtonColor: "#F73F54"
			},
			function(){                      
			});
		return false;
	}
	if(noOfRoom == ''){
		//alert('Please enter number of room.');
		swal({
				title: "Please enter number of Units",
				text: "",
				type: "error",
				confirmButtonColor: "#F73F54"
			},
			function(){                      
			});		
		return false;
	}
	if(noOfRoom < 2){
		//alert('Group booking must have minimum 2 rooms.');
		swal({
				title: "Group booking must have minimum 2 Units",
				text: "",
				type: "error",
				confirmButtonColor: "#F73F54"
			},
			function(){                      
			});
		return false;
	}	
	if(room_rent_type == '0'){
		//alert('Please select room rent type.');
		swal({
				title: "Please select room rent type",
				text: "",
				type: "error"
			},
			function(){                      
			});
		return false;
	}	
	 
	if(parseInt(noOfGuest) < parseInt(noOfRoom)){
		
		swal({
				title: "Check PAX & Unit Count",
				text: "No of guest must be greater than no of room",
				type: "warning"
			},
			function(){                      
			});
		return false;
		
		
	}
	if(guestName == ''){
		//alert('Please enter guest name.');
		swal({
				title: "Insufficent Data",
				text: "Please enter guest name",
				type: "warning"
			},
			function(){                      
			});
		return false;
	}
	if(guestPin == ''){
		//alert('Please enter pin code.');
		swal({
				title: "Please enter pin code",
				text: "",
				type: "warning"
			},
			function(){                      
			});
		return false;
	}
	

	if(booking_source == '0'){
		//alert('Please select booking source.');
		swal({
				title: "Please select booking source",
				text: "",
				type: "warning"
			},
			function(){                      
			});
		return false;
	}
	if(tax_applicable == '0'){
		//alert('Please select tax applicable or not.');
		swal({
				title: "Please select booking source",
				text: "",
				type: "warning"
			},
			function(){                      
			});
		return false;
	}
	
		 var travel_agent_commission = $("#travel_agent_commission").val();
		 //var commission_applicable = $("#commission_applicable").val();
		 var travel_agent_id = $("#travel_agent_id").val();		 
	     if(travel_agent_type != 'T'){
				if(travel_agent_id == '0'){
					swal({
							title: "Please select Travel Agent Name",
							text: "",
							type: "warning"
						},
						function(){                      
						});
					return false;
				}			 
				if(travel_agent_commission == ''){
					swal({
							title: "Please enter travel agent commission",
							text: "",
							type: "warning"
						},
						function(){                      
						});
					return false;
				}
				if((commission_applicable == '0') &&( set_agent_ota == '5' || set_agent_ota== '4')){
					swal({
							title: "Please select commission applicable for.",
							text: "",
							type: "warning"
						},
						function(){                      
						});
					return false;
				}
		 }	

		if(days >= 0){			
			var dt = $("#hd").val();
			dt = parseInt(dt);
			noOfRoom = parseInt(noOfRoom);

			if( noOfRoom > dt){
				
				swal({
					title: "Insufficent Inventory",
					text: "Selected number of rooms is not available within the selected date range, please check carefully!",
					type: "warning"
				},
				function(){
					$("#rmt").html('');
					$("#evt").html('');
					$("#rmid").css("display", "none");
					$("#bcid").css("display", "none");
				});
				
				return false;				   
			} else {

				
				selected1 = datesGrp('globl',1,1);
				selected2 = datesGrp('globl',2,1);

				//console.log(selected1);
				//console.log(selected2);
					
				$("#rmid").css("display", "block");
				$("#bcid").css("display", "block");
				$('#loading-image').show();
				$.ajax({				
					url: '<?php echo base_url(); ?>dashboard/getAvailableRoomAndEvent',
					type: "POST",
					data: {date1:selected1 , date2:selected2},
					success: function(data){
					
					   var res = data.split("~"); 	
					   $("#rmt").html(res[0]);
					   $("#evt").html(res[1]);
					},
					complete:function(){
						
						//$('#myTable tbody tr').empty();
						$('#myTable tbody tr:gt(0)').remove();
						$('#myTable tfoot').remove();
						x=0;
						//	$("#rmt").remove();
						window.countRow = 0;
						$('#loading-image').hide();
					},
				});
			} 
		} else {
			//alert('End date must be greater than start date');
			swal({
				title: "Date Error",
				text: "End date must be greater than start date",
				type: "warning"
			},
			function(){                      
			});
			return false;
		}
	
	
} // End function getVal()
</script>
<?php 
$val = $this->uri->segment(3);
if(isset($val) && $val > 0) { ?>
<script>
$(window).load(function(){
        $('#myModalNorm').modal('show');
    });	
</script>
<?php } ?>
<div id="loader" style="display:none;">
 
  <div class='loader'>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>	
    <div class='door'></div>
    <div class='hotel-sign'> <span>H</span> <span>O</span> <span>T</span> <span>E</span> <span>L</span> </div>
  </div>
</div>
 <div id="loading-image1">
		  <img src="<?php echo base_url();  ?>assets/ajax-loader.gif" width="94" height="94">
		  
		  </div>
<!-- Modal -->
<div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Group Booking successfully added</label>
                  </div>
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <a href="<?php echo base_url()?>dashboard/add_group_booking"><button type="button" class="btn btn-danger">
                            Close
                </button></a>
                <a href="<?php echo base_url()?>dashboard/booking_edit_group?group_id=<?php echo $this->uri->segment(3);?>"><button type="button" class="btn btn-primary">
                    View Details
                </button></a>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModalNorm1" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header" style="background:#95A5A6; color:#ffffff;">
            	Search Guest
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
                
                  <div class="form-group">
                    	<div class="input-group">
                      <input type="email" class="form-control" id="guest_search" name="guest_search" placeholder="Search Guest"/>
                      <span class="input-group-btn">
					  <button type="button" class="btn green" id="btnModal" onclick="return_guest_search()" >Search</button>
                      </span>
                      </div>
                  </div>
				  
				  
               <div id="return_guest" style="overflow-y:scroll; height:300px; display:none;">
			   
			   
			   </div>
                
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn red"
                        data-dismiss="modal">
                            Close
                </button>
            </div>
        </div>
    </div>
</div>
<script>

$(document).unbind("keyup").keyup(function(e){ 
    
	var code = e.which; 
    if(code == 13){
       //console.log("HERE");
	   $("#btnModal").click();
    }
});

function showAgentDetails(val){

	$('#travel_agent_commission').val("");
	if(val == 'T'){
		$("#showDetails").css("display", "none");
	} else {
		$("#showDetails").css("display", "block");
		$.ajax({
		   type: "POST",
		   url: "<?php echo base_url();?>setting/showAgentDetails",
		   data: "val="+val,
		   success: function(msg){
			   if(msg==0){
				swal({
					title: "Select another category!",
					text: "This category does not have any unit type defined under it.",
					type: "error",
					confirmButtonColor: "#F27474"
				})
			   }
				$("#travel_agent_id").html(msg);
		   }
		});
	}
}

function get_status_unit_avail_log(){
    
   // alert('hello');
	 var htmlText='';
   // $('div#slideout a.show_me').show();
  // $('div#my_slideout').RemoveClass('classic');
  $('div#slideout').toggleClass('classic');
   
        
   $.ajax({
        
        type:"POST",
        url:"<?php echo base_url(); ?>setting/modify_unit_avail_log",
      datatype:'json',
        data:{clickme:'click'},
        success:function(msg){
			
			if(msg.length > 0){
           // alert(msg);
           console.log(msg);
          var data =msg;
            console.log(data);
			
           for(var obcount =0; obcount<=data.length-1; obcount++){
			  if(data[obcount] == "undefined"){
			   
				break;
			  }
			  else{
				  
								
				htmlText =htmlText +"<tr class = span_"+obcount +"><td>" +data[obcount].admin_first_name +' ' + data[obcount].admin_last_name + ' </td><td>  ' + data[obcount].unit_name+'</td><td> <span class="label label-sm label-success"> '+data[obcount].room_no +" </span></td><td> "+ data[obcount].checkin_date +" TO "+ data[obcount].checkout_date +"</td><td style='text-align:right'><a onClick=unit_delete(" + obcount +"," + data[obcount].unit_id +"); class='unitclass_" +obcount+" btn red btn-xs'>"+ "  <i class='fa fa-trash'></i>" +"</a></td></tr>";
					//alert(htmlText);
			  }
		
			}
		  // alert(htmlText);
		   console.log(htmlText);
            $('div#slideout_inner table').html(htmlText);
           //alert(data);
			}else{
				
				htmlText = htmlText + "No Booking exists";
				  $('div#slideout_inner table').html(htmlText);
			}
            
        },
        
    });
    
    
    
}									 
function showAgentCommission(id){
	    var agent_type = document.getElementById("travel_agent_type").value;
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>setting/showAgentCommission",
                datatype:'json',
                data:{agent_type:agent_type,id:id},
                success:function(data)
                {
                    $('#travel_agent_commission').val(data.commission);
                }
            });


        return false;
} 

 $('body').on('focus',".date-picker", function(){
    $(this).datepicker({
		orientation: "left",
		startDate: today,
		autoclose: true,
		todayHighlight: true
    });
});


$.typeahead({
    input: '.js-typeahead-user_v1',
    minLength: 1,
    order: "asc",
    dynamic: true,
    delay: 200,
    backdrop: {
        "background-color": "#fff"
    },
   
    source: {
       project: {
            display: "name",
            ajax: [{
                type: "GET",
                url: "<?php echo base_url();?>dashboard/test_typehead",
                data: {
                    q: "{{query}}"
                }
            }, "data.name"],
            template: '<div class="clearfix">' +
                '<div class="project-img">' +
                    '<img class="img-responsive" src="{{image}}">' +
                '</div>' +
                '<div class="project-information">' +
                    '<span class="pro-name" style="font-size:15px;"> {{name}}</span><span><i class="fa fa-phone"></i> <strong>Contact no:</strong> {{ph_no}} </span><span><i class="fa fa-envelope"></i> <strong>Email:</strong> {{email}}</span></span>' +
                '</span>' +
				'</div>' +
            '</div>'
        }
    },
    callback: {
        onClick: function (node, a, item, event) {
         // alert(JSON.stringify(item));
		 
			$("#gp").val(item.pincode);
		//	$("#g_address_child").val(item.address);
			$("#g_phn").val(item.ph_no);
			$("#g_email").val(item.email);
			$("#gn").val(item.name);
			$('#guestID').val(item.id);
			var st = item.state;
			var state_g = st.toUpperCase();
			$('#g_state').val(state_g).change();
			var cnt = item.country;
			var country_g = cnt.toUpperCase();
			$('#g_country').val(country_g).change();
			//$('#g_state_code').val(item.state_code);
		 
        },
        onSendRequest: function (node, query) {
            console.log('request is sent')
			
        },
        onReceiveRequest: function (node, query) {
            //console.log('request is received')
			//alert("defesddsfdsfs");
			if(query!=''){
				$("#gp").val('');
		//	$("#g_address_child").val(item.address);
			$("#g_phn").val('');
			$("#g_email").val('');
			$("#gn").val('');
			$('#guestID').val('');
			$('#g_state').val('');
			$('#g_country').val('');
			$('#g_state_code').val('');
			}
        },
         onCancel: function (node, query) {
            //console.log('request is received')
			//alert("defesddsfdsfs");
			if(query!=''){
				$("#gp").val(item.pincode);
		//	$("#g_address_child").val(item.address);
			$("#g_phn").val(item.ph_no);
			$("#g_email").val(item.email);
			$("#gn").val(item.name);
			$('#guestID').val(item.id);
			var st = item.state;
			var state_g = st.toUpperCase();
			$('#g_state').val(state_g).change();
			var cnt = item.country;
			var country_g = cnt.toUpperCase();
			$('#g_country').val(country_g);
		//	$('#g_state_code').val(item.state_code);
			
			
			}
        }		
    },
    debug: false
});


function get_guest_name(name){
	//alert(name);
		$("#gn").val(name);
}

function fetch_all_address_hotel(pincode,g_country,g_state)
{
	//var pin_code = document.getElementById(pincode).value;
	//alert(pincode+g_country+g_state);
	

	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address2",
		dataType: 'json',
		data: {pincode: pincode},
		success: function(data){
			//alert(data);
			console.log('fetch_address2' +data);
		if(data.state!=''){
			var st = data.state;
			var state_g = st.toUpperCase();
			//alert('state_g' +state_g);
				console.log('state_g' +state_g);
			//$('#g_state').val(state_g).change();
			var cnt = data.country;
			var country_g = cnt.toUpperCase();
		//	$('#g_country').val(country_g);
		//	alert('country_g' +country_g);
			
			$(g_country).val(country_g).change();
			$(g_state).val(state_g).change();
			
		}
		else{
			
		}
			//var full_address = (data.city+","+data.state+","+data.country);
			//$('#cust_full_address').val(full_address);
		}

	});
}


$('#g_country').on('change',function(){
	
	
	var contyr = $(this).val();
	//alert('contyr' +contyr);
	if(contyr == "INDIA"){
		
	//	alert('ind');
		$('#g_state_div1').show();
		$('#other_st').hide();
		$('#g_state1').val('');
	}
	else{
		
	//	alert('hind');
		$('#other_st').show();
		$('#g_state_div1').hide();
		$('#g_state').val('');
		//<input type="text" class="form-control" place="put your country state" id="g_state" name="g_state">
	//	$('#g_state_div').pre('<input type="text" class="form-control"place="put your country state" id="g_state" name="g_state">');
	}
	
});
function return_broker()
    {
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_broker",
                datatype:'json',
                success:function(data)
                {
                    var resultHtml = '';
                    resultHtml+='<option value="">Select Broker</option>'
                    $.each(data, function(key,value){
						//alert(value.b_id+','+value.b_name);
                        resultHtml+='<option value="'+ value.b_id +'">'+ value.b_name +'</option>';
						
                    });
					//alert(resultHtml);
                    $('#broker_id').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
        return false;


    }
	
	 function return_channel()
    {
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_channel",
                datatype:'json',
                success:function(data)
                {
                    var resultHtml = '';
                    resultHtml+='<option value="">Select Channel</option>'
                    $.each(data, function(key,value){

                        resultHtml+='<option value="'+ value.channel_id +'">'+ value.channel_name +'</option>';

                    });
                    $('#broker_id').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
        return false;


    }

</script>
<script>

  function unit_delete(obcount,unit_id){
	
	
	
///	alert('del'+obcount +'unit_id' +unit_id);
	
	$.ajax({
		
		type:"POST",
		url:"<?php echo base_url();?>setting/del_from_unitavaillog",
		data:{unit_id:unit_id,obcount:obcount},
		success:function(msg){
			
		//	alert('msg' +msg);
			if(msg!= "failed"){
			//	$('div#my_slideout').addClass('soclassic');
				$('div#slideout_inner').find('.span_'+msg).remove();
				
			}
			else{
				
				swal('you cannot delete this room');
			}
			
		}
	})
	
	
}
	
	
</script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/typeahead1/jquery.typeahead.css">