<div class="portlet box blue" id="form_wizard_1">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin font-green"></i> ADD HOTELS - <span class="step-title"> Step 1 of 4 </span> </div>
    <div class="tools hidden-xs"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body form">
    <form class="" action="#" id="submit_form" method="POST">
      <div class="form-wizard">
        <div class="form-body">
          <ul class="nav nav-pills nav-justified steps">
            <li> <a href="#tab1" data-toggle="tab" class="step"> <span class="number"> 1 </span> <span class="desc"> <i class="fa fa-check"></i> Account Setup </span> </a> </li>
            <li> <a href="#tab2" data-toggle="tab" class="step"> <span class="number"> 2 </span> <span class="desc"> <i class="fa fa-check"></i> Profile Setup </span> </a> </li>
            <li> <a href="#tab3" data-toggle="tab" class="step active"> <span class="number"> 3 </span> <span class="desc"> <i class="fa fa-check"></i> Billing Setup </span> </a> </li>
            <li> <a href="#tab4" data-toggle="tab" class="step"> <span class="number"> 4 </span> <span class="desc"> <i class="fa fa-check"></i> Preferences </span> </a> </li>
          </ul>
          <div id="bar" class="progress progress-striped" role="progressbar">
            <div class="progress-bar progress-bar-success"> </div>
          </div>
          <div class="tab-content">
            <div class="alert alert-danger display-none">
              <button class="close" data-dismiss="alert"></button>
              You have some form errors. Please check below. </div>
            <div class="alert alert-success display-none">
              <button class="close" data-dismiss="alert"></button>
              Your form validation is successful! </div>
            <div class="tab-pane tab-block active" id="tab1">
              <h3 class="block">Personal</h3>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="First Name *"/>
                    <label></label>
                    <span class="help-block">First Name *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Last Name *"/>
                    <label></label>
                    <span class="help-block">Last Name *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control date-picker" name="" required placeholder="DOB *"/>
                    <label></label>
                    <span class="help-block">DOB *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Sex *"/>
                    <label></label>
                    <span class="help-block">Sex *</span> </div>
                </div>
              </div>
              <div class="row">
                <label class="col-md-12">Present Address <span class="required" aria-required="true">* </span></label>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Address Line *"/>
                    <label></label>
                    <span class="help-block">Address Line *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Pin code *"/>
                    <label></label>
                    <span class="help-block">Pin code *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="City *"/>
                    <label></label>
                    <span class="help-block">City *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="State *"/>
                    <label></label>
                    <span class="help-block">State *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Country *"/>
                    <label></label>
                    <span class="help-block">Country *</span> </div>
                </div>
              </div>
              <div class="row">
                <label class="col-md-12">Permanent Address <span class="required" aria-required="true">* </span></label>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Address Line *"/>
                    <label></label>
                    <span class="help-block">Address Line *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Pin code *"/>
                    <label></label>
                    <span class="help-block">Pin code *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="City *"/>
                    <label></label>
                    <span class="help-block">City *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="State *"/>
                    <label></label>
                    <span class="help-block">State *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Country *"/>
                    <label></label>
                    <span class="help-block">Country *</span> </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Phone No *"/>
                    <label></label>
                    <span class="help-block">Phone No *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Mobile No *"/>
                    <label></label>
                    <span class="help-block">Mobile No *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Personal Email *"/>
                    <label></label>
                    <span class="help-block">Personal Email *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Professional Email"/>
                    <label></label>
                    <span class="help-block">Professional Email</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Father’s Name"/>
                    <label></label>
                    <span class="help-block">Father’s Name</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Mother’s Name"/>
                    <label></label>
                    <span class="help-block">Mother’s Name</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <select class="form-control bs-select"  name="" required>
                      <option value="" disabled="disabled" selected="selected">Select Marital Status</option>
                      <option value="1">Single</option>
                      <option value="2">Married</option>
                      <option value="3">Divorced</option>
                      <option value="4">Widowed</option>
                    </select>
                    <label></label>
                    <span class="help-block">Marital Status *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Anniversary Date"/>
                    <label></label>
                    <span class="help-block">Anniversary Date</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Spouse Name"/>
                    <label></label>
                    <span class="help-block">Spouse Name</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Spouse Phone No"/>
                    <label></label>
                    <span class="help-block">Spouse Phone No</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="No of Dependents"/>
                    <label></label>
                    <span class="help-block">No of Dependents</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="No of Children"/>
                    <label></label>
                    <span class="help-block">No of Children</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <select class="form-control bs-select"  name="">
                      <option value="" disabled="disabled" selected="selected">Select Religion</option>
                      <option value="1">Hindu</option>
                      <option value="2">Muslim</option>
                      <option value="3">Christian</option>
                      <option value="4">Buddhism</option>
                      <option value="4">Jain</option>
                      <option value="4">Atheist</option>
                      <option value="4">Others</option>
                    </select>
                    <label></label>
                    <span class="help-block">Religion</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Other religious believe"/>
                    <label></label>
                    <span class="help-block">Other religious believe</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Caste"/>
                    <label></label>
                    <span class="help-block">Caste</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Handicapped?"/>
                    <label></label>
                    <span class="help-block">Handicapped?</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required="required" placeholder="Blood Group *"/>
                    <label></label>
                    <span class="help-block">Blood Group *</span> </div>
                </div>
              </div>
              <div class="row">  
                <label class="col-md-12">Emergency Contact details </label>
                <div class="col-md-12">
                    <table class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>Contact Name</th>
                          <th>Relation</th>
                          <th>Phone/ Mobile No</th>
                          <th width="5%"><a href="javascript:;" class="btn green btn-xs" style="margin-right:10px;"> <i class="fa fa-plus"></i></a></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><input type="text" class="form-control" name="" placeholder="Contact Name"/></td>
                          <td><input type="text" class="form-control" name="" placeholder="Relation"/></td>
                          <td><input type="text" class="form-control" name="" required="required" placeholder="Phone/ Mobile No"/></td>
                          <td><a href="javascript:;" class="btn red btn-xs"> <i class="fa fa-minus"></i></a></td>
                        </tr>
                      </tbody>
                    </table>
                </div>                
              </div>
              <div class="row">
                <label class="col-md-12">Medical Condition</label>
                <div class="col-md-12">
                    <table class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>Condition Name</th>
                          <th>Relation</th>
                          <th width="5%"><a href="javascript:;" class="btn green btn-xs"> <i class="fa fa-plus"></i></a></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><input type="text" class="form-control" name="" placeholder="Condition Name"/></td>
                          <td><input type="text" class="form-control" name="" placeholder="Relation"/></td>
                          <td><a href="javascript:;" class="btn red btn-xs"> <i class="fa fa-minus"></i></a></td>
                        </tr>
                      </tbody>
                    </table>
                </div>                 
              </div>
              <div class="row">
                <label class="col-md-12">Extra-Curricular activities </label>
                <div class="col-md-12">
                	<table class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>Condition Name</th>
                          <th>Level</th>
                          <th>Activity Description</th>
                          <th width="5%"><a href="javascript:;" class="btn green btn-xs"> <i class="fa fa-plus"></i></a></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><input type="text" class="form-control" name="" placeholder="Condition Name"/></td>
                          <td>
                          	<select class="form-control bs-select"  name="">
                              <option value="" disabled="disabled" selected="selected">Select Level</option>
                              <option value="1">Novice</option>
                              <option value="2">Beginner</option>
                              <option value="3">Intermediate</option>
                              <option value="4">Pro</option>
                            </select>
                          </td>
                          <td><input type="text" class="form-control" name="" placeholder="Activity Description"/></td>
                          <td><a href="javascript:;" class="btn red btn-xs"> <i class="fa fa-minus"></i></a></td>
                        </tr>
                      </tbody>
                    </table>                  
                </div>
              </div>
            </div>
            <div class="tab-pane tab-block" id="tab2">
              <h3 class="block">Professional</h3>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <select class="form-control bs-select"  name="">
                      <option value="" disabled="disabled" selected="selected">Select Hotel / Company</option>
                      <option value="1">Hotel</option>
                      <option value="2">Company</option>
                    </select>
                    <label></label>
                    <span class="help-block">Hotel / Company</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Organization"/>
                    <label></label>
                    <span class="help-block">Organization</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Branch"/>
                    <label></label>
                    <span class="help-block">Branch</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Department *"/>
                    <label></label>
                    <span class="help-block">Department *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Workgroup"/>
                    <label></label>
                    <span class="help-block">Workgroup</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Location *"/>
                    <label></label>
                    <span class="help-block">Location *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Profit Center *"/>
                    <label></label>
                    <span class="help-block">Profit Center *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" required placeholder="Designation *"/>
                    <label></label>
                    <span class="help-block">Designation *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Role"/>
                    <label></label>
                    <span class="help-block">Role</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <select class="form-control bs-select"  name="">
                      <option value="" disabled="disabled" selected="selected">Select Level</option>
                      <option value="1">Intern</option>
                      <option value="2">Fresher</option>
                      <option value="3">Junior</option>
                      <option value="4">Executive</option>
                      <option value="5">Lower Management</option>
                      <option value="6">Medium Management</option>
                      <option value="7">Higher Management</option>
                      <option value="8">Stakeholder</option>
                    </select>
                    <label></label>
                    <span class="help-block">Level</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Reporting to"/>
                    <label></label>
                    <span class="help-block">Reporting to</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control date-picker" name="" placeholder="Joining Date"/>
                    <label></label>
                    <span class="help-block">Joining Date</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <select class="form-control bs-select"  name="">
                      <option value="" disabled="disabled" selected="selected">Select Mode of Engagement</option>
                      <option value="1">Full Time</option>
                      <option value="2">Part Time</option>
                    </select>
                    <label></label>
                    <span class="help-block">Mode of Engagement</span> </div>
                </div>
              </div>
              <div class="row">
                <label class="col-md-12">Salary Details</label>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="CTC – Yearly"/>
                    <label></label>
                    <span class="help-block">CTC – Yearly</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Basic"/>
                    <label></label>
                    <span class="help-block">Basic</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="DA"/>
                    <label></label>
                    <span class="help-block">DA</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="HRA"/>
                    <label></label>
                    <span class="help-block">HRA</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Allowance"/>
                    <label></label>
                    <span class="help-block">Allowance</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Conveyance"/>
                    <label></label>
                    <span class="help-block">Conveyance</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Medical"/>
                    <label></label>
                    <span class="help-block">Medical</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="LTA"/>
                    <label></label>
                    <span class="help-block">LTA</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Mobile & Misc"/>
                    <label></label>
                    <span class="help-block">Mobile & Misc</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="PF – Employee"/>
                    <label></label>
                    <span class="help-block">PF – Employee</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="PF - Employer"/>
                    <label></label>
                    <span class="help-block">PF - Employer</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Cash in Hand PM"/>
                    <label></label>
                    <span class="help-block">Cash in Hand PM</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-horizontal">
                    <div class="form-group form-md-line-input">
                      <label class="col-md-3 control-label" for="form_control_1">Taxable?</label>
                      <div class="col-md-9">
                        <div class="md-radio-inline">
                          <div class="md-radio">
                            <input type="radio" id="radio1" name="radio2" class="md-radiobtn">
                            <label for="radio1"> <span></span> <span class="check"></span> <span class="box"></span> YES </label>
                          </div>
                          <div class="md-radio">
                            <input type="radio" id="radio2" name="radio2" class="md-radiobtn">
                            <label for="radio2"> <span></span> <span class="check"></span> <span class="box"></span> NO </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <label class="col-md-12">Total Leave</label>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Casual Paid"/>
                    <label></label>
                    <span class="help-block">Casual Paid </span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="" placeholder="Privilege Paid"/>
                    <label></label>
                    <span class="help-block">Privilege Paid</span> </div>
                </div>
              </div>
              <div class="row">
                <label class="col-md-12">Previous Employment detail </label>
                <div class="col-md-12">
                	<table class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>Company Name</th>
                          <th>Industry Type</th>
                          <th>Location</th>
                          <th>Designation</th>
                          <th>CTC</th>
                          <th>Job Role</th>
                          <th>Reason for leaving</th>
                          <th width="5%"><a href="javascript:;" class="btn green btn-xs"> <i class="fa fa-plus"></i></a></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><input type="text" class="form-control" name="" placeholder="Company Name"/></td>
                          <td><input type="text" class="form-control" name="" placeholder="Industry Type"/></td>
                          <td><input type="text" class="form-control" name="" placeholder="Location"/></td>
                          <td><input type="text" class="form-control" name="" placeholder="Designation"/></td>
                          <td><input type="text" class="form-control" name="" placeholder="CTC"/></td>
                          <td><input type="text" class="form-control" name="" placeholder="Job Role"/></td>
                          <td><input type="text" class="form-control" name="" placeholder="Reason for leaving"/></td>
                          <td><a href="javascript:;" class="btn red btn-xs"> <i class="fa fa-minus"></i></a></td>
                        </tr>
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab3">
              <h3 class="block">Upload Section</h3>
              <div class="row">
              	<div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label">Pan Card</label>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                          <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                          <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="..." >
                          </span> <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label">Voter ID Card</label>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                          <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                          <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="..." >
                          </span> <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label">Adhar Card</label>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                          <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                          <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="..." >
                          </span> <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label">Class 10 Mark Sheet</label>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                          <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                          <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="..." >
                          </span> <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label">Class 12 Mark Sheet</label>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                          <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                          <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="..." >
                          </span> <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label">Graduation Mark Sheet</label>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                          <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                          <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="..." >
                          </span> <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label">Post-Graduation Mark Sheet</label>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                          <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                          <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="..." >
                          </span> <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label">Previous Company Release Letter</label>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                          <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                          <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="..." >
                          </span> <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label">Last 3 Salary Slip</label>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                          <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                          <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="..." >
                          </span> <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label">CV</label>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                          <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                          <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="..." >
                          </span> <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label">Offer Letter</label>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                          <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                          <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="..." >
                          </span> <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label">Pan Card</label>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                          <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                          <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="..." >
                          </span> <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab4">
              <h3 class="block">Billing Settings</h3>
              
            </div>            
          </div>
        </div>
        <div class="form-actions right">
          <div class="row">
            <div class="col-md-offset-3 col-md-9"> <a href="javascript:;" class="btn default button-previous"> <i class="m-icon-swapleft"></i> Back </a> <a href="javascript:;" class="btn blue button-next" onclick="check();"> Continue <i class="m-icon-swapright m-icon-white"></i> </a> <a href="javascript:;" class="btn green button-submit" onclick="submit_form()"> Submit <i class="m-icon-swapright m-icon-white"></i> </a> 
              <!--<input type="submit" value="Submit" onclick="submit_form()" class="btn green button-submit" />--> 
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
