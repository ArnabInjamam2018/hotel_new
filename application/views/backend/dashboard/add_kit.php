<?php if($this->session->flashdata('err_msg')):?>
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
      <?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Define Kits</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                            $form = array(
                                'class' 			=> '',
                                'id'				=> 'form',
                                'method'			=> 'post',								
                            );
							
							

                            echo form_open_multipart('dashboard/add_kit',$form);

                            ?>
    <div class="form-body">     
      <div class="row">
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="kit_name" placeholder="Kit Name *">
            <label></label>
            <span class="help-block">Kit Name *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="kit_des" placeholder="Kit Description *">
            <label></label>
            <span class="help-block">Kit Description *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select"  name="kit_type" required >
              <option value="" selected="selected" disabled="disabled">Select Type</option>
              <option value="Standard">Standard</option>
              <option value="Custom">Custom</option>
              <option value="Premium">Premium</option>
              <option value="Special">Special</option>
            </select>
            <label></label>
            <span class="help-block">Select Type *</span> </div>
        </div>
		
		
        <div class="col-md-12" style="padding-top:30px;">
          <div class="">
            <table class="table table-striped table-bordered table-hover table-scrollable" id="items">
              <thead>
                <tr>
                  <th width="20%"> Item Name</th>
                  <th width="20%"> Qty </th>
                  <th width="20%"> Unit Price </th>
                  <th width="20%">Qty Apply For</th>
                  <th width="15%"> Total </th>
                  <th width="5%"> Action </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="form-group"><input id="i_name" type="hidden"  class="form-control input-sm" >
                    <input id="i_name_name" onkeyup="search_item()" type="text"  class="form-control input-sm" tabindex="1" autocomplete="off">
                    <div id="charges"></div></td>
                  <td class="form-group"><input id="i_quantity" type="text" value="" class="form-control input-sm" onkeyup="chk_if_num('i_quantity'),calculation();"></td>
                  <td class="hidden-480 form-group"><input onblur="calculation()" id="i_unit_price" type="text" value="" class="form-control input-sm" onkeyup="chk_if_num('i_unit_price');"></td>
                  <td><select class="form-control input-sm bs-select"  id="qty_apply_for" required >
				   <option value="Fixed" selected>Fixed</option>
                      <option value="The No of Guests">The No of Guests</option>
                     
                    </select></td>
                  <td class="hidden-480 form-group"><input id="i_total" type="text" value="" class="form-control input-sm" readonly></td>
                  <td><a class="btn green btn-xs"  id="additems"><i class="fa fa-plus" aria-hidden="true"></i></a></td>
                </tr>
                <tr> </tr>
              </tbody>
            </table>
          </div>
        </div>
        <input type="text" Style="display:none;" id="count" name="count" value="0"  />
        <input type="hidden"  id="ids" name="ids" value=""  />
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="kit_extra_cost" name="kit_extra_cost" placeholder="Kit Extra Cost" onkeyup="chk_if_num('kit_extra_cost');">
            <label></label>
            <span class="help-block">Kit Extra Cost</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" id="discount" name="is_sellable" required onchange="show_sp();">
              <option value="" disabled="disabled" selected="selected">Is Sellable?</option>
              <option value="y">Yes</option>
              <option value="n">No</option>
            </select>
            <span class="help-block">Is Sellable?</span> </div>
        </div>
        <div class="col-md-4"  id="kit_sel_div" style="display:none;">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="kit_sel_price" name="kit_sel_price" value= "0" placeholder="Kit Selling price" onkeyup="chk_if_num('kit_sel_price');" required="required">
            <label></label>
            <span class="help-block">Kit Selling price</span> </div>
        </div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input uploadss">
            <label>Upload Photo</label>
            <div class="fileinput fileinput-new" data-provides="fileinput">
              <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
              <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('image');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn submit" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>
<script>
$( document ).ready(function() {
    $('#i_unit_price').val('0');
});


var x=0;
   function check_corporate(value){

       if(value =="corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
       }else{
           document.getElementById("c_name").style.display="none";
           document.getElementById("c_des").style.display="none";
       }
   }
    function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});

function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	   //alert(y);
	    
	   if(y=="check"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fundtransfer"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }
   
   $("#additems").click(function(){

	var i_name = $("#i_name").val();
	var i_name_name =$("#i_name_name").val();
	var i_quantity = $("#i_quantity").val();
	var i_unit_price = $("#i_unit_price").val();
	var i_total = $("#i_total").val();
	var qty_for=$("#qty_apply_for").val();
	/*alert(i_name);
	alert(i_quantity);
	alert(i_unit_price);
	alert(i_total);*/
	//alert(qty_for);
	//var totalTax = ( i_quantity * i_unit_price );
	//totalTax = Math.round(totalTax*100)/100;
	//$('#i_total').val(totalTax);
	
	
	
	var x= parseInt($('#count').val());
	if(
	//$("#i_name").val() != "" &&
	$("#i_quantity").val() != "" &&
	$("#i_unit_price").val() != "" &&
	$("#i_total").val("") != ""
	)
	
	{	var flag = 0;
		var it=$('#ids').val();
		var res = it.split(',');
		
		var l = 0;
    for(l = 0; l < res.length ; l++)
		{
			//alert(res[l]);
			if (res[l] == i_name)
			{
				
				flag++;
			}
		}
		
		alert(flag);
		if (1)
		{
	$('#ids').val( it+','+ i_name) ;
	$('#items tr:first').after('<tr id="row_'+i_name+'"><td>'+
	'<input  id="i_name5" type="text" value="'+i_name_name+'" class="form-control input-sm" readonly><input name="i_name_'+x+'" id="i_name5" type="hidden" value="'+i_name+'" class="form-control input-sm" readonly></td><td><input name="qty_'+x+'" id="c_name1" type="text" value="'+i_quantity+'" class="form-control input-sm" readonly></td><td class="hidden-480"><input name="unit_cost_'+x+'" id="c_name4" type="text" value="'+i_unit_price+'" class="form-control input-sm" readonly></td><td class="hidden-480"><input name="qty_for_'+x+'" id="c_name4" type="text" value="'+qty_for+'" class="form-control input-sm" readonly><td class="hidden-480"><input name="total_'+x+'" id="c_name4" type="text" value="'+i_total+'" class="form-control input-sm" readonly></td></td><td><a href="javascript:void(0);" class="btn red btn-xs" onclick="removeRow('+i_name+')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>');	
	x=x+1;
	
	$('#count').val(x);
	
	$("#i_name").val("");
	$("#i_name_name").val("");
	$("#i_quantity").val("");
	$("#i_unit_price").val("");
	//$("#c_tax").val("");
	$("#i_total").val("");		
		}
		/*else
		{
			alert("This item is already added");
		}*/
	}
	else
	{
	alert("Please Fill the item details properly");	
	$("#i_name").focus();
	}
});	


function calculation() {
	//i_quantity,i_unit_price
	var a=document.getElementById("i_quantity").value;
	var value=document.getElementById("i_unit_price").value;
//alert(a);
//alert(value);
	
	var total=parseFloat(a)*parseFloat(value);
	//alert(value);
	if( isNaN(total)== false)
	{
	document.getElementById("i_total").value=total;
	}
	else
	{
		alert("Please fill up Qty. and Unit Price Properly !!");
		$('#i_quantity').focus();
		//$('#i_unit_price').focus();
	}
	
	
}
   
</script> 
<script>
function chk_if_num( a )
		{
	
				if(isNaN($('#'+a+'').val())==true)
				{
					
					$('#'+a+'').val("");
					
				}			
		}
		
		
		function removeRow(a)
		{
			
			 text = $('#ids').val();
			 text = text.replace(","+a, "");
			 $('#ids').val(text);
			 $('#row_'+a).remove();   
		}
		
	function show_sp()
	{
		//alert($('#discount').val());
		if($('#discount').val() == 'y')
		{
			
		$('#kit_sel_div').show();
		}
		else
		{
		$('#kit_sel_price').val(0);
		$('#kit_sel_div').hide();
		}
	}
</script> 
<script>
          
function search_item(){

  var value=document.getElementById("i_name_name").value;

  //alert(value);

    $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>dashboard/search_item",
    data:{keyword:value},
    success: function(data){
		if(data != "")
		{
	  $("#charges").show();
      $('#charges').html(data);
		}
    }
    });
 }
 
 </script> 
<script>
 function selectitem(event,name,unit_price,id) 
{
	
	if(event.keyCode == 13)
	{
$("#i_name").val(id);
$("#i_name_name").val(name);
$("#i_unit_price").val(unit_price);
$("#charges").hide();
	}
	if(event == 0)
	{
		$("#i_name").val(id);
$("#i_name_name").val(name);
$("#i_unit_price").val(unit_price);
$("#charges").hide();
	}
}

 </script>
