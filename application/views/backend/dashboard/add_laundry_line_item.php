<div class="portlet-body form"> 
        
        <!--guest details edit field-->
        
<div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Extra Charge Added:</span> 
          <!--<i class="fa fa-pencil-square-o pull-right" style="color:gray;margin-top:-22px;font-size:20px;cursor:pointer;" onClick='extra_info()'></i>--> 
        </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body form-horizontal form-row-sepe">
          <div class="form-body">
           <?php
                /*$form1= array(
                    'class'=> 'form-body form-horizontal form-row-sepe',
                    'id'=> 'form1',
                    'method'=> 'post',
                    'name'=> 'from1'
                );
                echo form_open('unit_class_controller/edit_hotel_laundry_line_item',$form1);*/
            ?>
            <table class="table table-striped table-bordered table-hover" id="sample_1">
              <thead>
                <tr>
                  <th width="10%"> Laundry Id </th>
                  <!-- <th width="10%"> Guest Id </th> -->
                  <th width="10%"> Name </th>
                  <th width="15%"> Description</th>
                  <th width="5%"> Color</th>
                  <th width="5%"> Fabric</th>
                  <th width="5%"> Condition</th>
                  <th width="25%"> Delivery Date</th>
                  <th width="25%"> Laundry Type</th>
                  <th width="25%"> Action</th>
                </tr>
              </thead>
             <tbody>
                <?php
                        if(isset($unit_class) && $unit_class):

                          foreach($unit_class as $gst):
               
 ?>
                <tr>                  
                  <td class="hidden-480"><?php echo $gst->laundry_id ?></td>
                  <!-- <td class="hidden-480"><label><?php //echo $gst->guest_id ?></label></td> -->
                  <td class="hidden-480"><?php echo $gst->item_name ?></td>
                  <td class="hidden-480"><?php echo $gst->item_desc ?></td>
                  <td class="hidden-480"><?php echo $gst->color ?></td>
                  <td class="hidden-480"><?php echo $gst->fabric_type ?></td>
                  <td class="hidden-480"><?php echo $gst->item_condition ?></td>
                  <td class="hidden-480"><?php echo $gst->delivery_date ?></td>
                  <td class="hidden-480"><?php echo $gst->laundry_type ?></td>
                  <td ><a onclick="soft_delete('<?php echo $gst->laundry_id;?>')" data-toggle="modal"  class="btn red" ><i class="fa fa-trash"></i></a><br>
                   <a class="btn green edit"   href="javascript:;"><i class="fa fa-edit" aria-hidden="true" class="btn blue"></i></button></td>
                </tr>
                <?php
                        endforeach;
                        endif;
                  ?>
              </tbody>
              
            </table>
            <?php form_close(); ?>
              
            
            <h4 style="text-align:center; border-top:1px solid #eee; padding:10px 0 25px;"><strong>Add Charges:</strong></h4>
            <?php
                $form= array(
                    'class'=> 'form-body form-horizontal form-row-sepe',
                    'id'=> 'form',
                    'method'=> 'post',
                    'name'=> 'from'
                );
                echo form_open('unit_class_controller/add_hotel_laundry_line_item',$form);
            ?>
            <table class="table table-striped table-hover" id="extra_service">
              <thead>
                <tr>
                  <!-- <th width="10%"> Guest Id </th> -->
                  <th width="18%">Name </th>
                  <th width="10%">Description</th>
                  <th width="10%">Color</th>
                  <th width="15%">Fabric</th>
                  <th width="15%">Condition</th>
                  <th width="10%">Delivery Date</th>
                  <th width="20%">Laundry Type</th>
                  <th width="15%">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <!-- <td class="form-group"><input onkeyup="search_charge()" id="c_name" type="text" value="" class="form-control input-sm">
                    <div id="suggesstion-box"></div></td> -->
                  <!-- <td class="form-group"><input id="c_quantity" type="text" name="guest_id" value="" class="form-control input-sm" required="required"></td> -->
                  <td class="form-group"><input id="c_quantity" type="text" name="name" value="" class="form-control input-sm" required="required"></td>
                  <td class="form-group"><input id="c_quantity" type="text" name="description" value="" class="form-control input-sm" required="required"></td>
                  <td class="form-group"><input id="c_quantity" type="text" name="color" value="" class="form-control input-sm" required="required"></td>

                  <!-- <td class="form-group"><input id="c_quantity" type="text" name="fabric" value="" class="form-control input-sm"></td> -->

                  <td><select onchange="change_status(this.value)" name="fabric" id="fabric" class="form-control input-sm" required="required">
                                         <option disabled="disabled" selected="selected">--Select--</option>
              <?php
                        if(isset($fabric) && $fabric):

                          foreach($fabric as $gst):
                    
                                               ?>
                                        <option value="<?php echo  $gst->fabric_type; ?>"><?php echo  $gst->fabric_type; ?> </option>


                               <?php
                        endforeach;
                        endif;
                  ?>
                  </select></td>



                  <!-- <td class="form-group"><input onblur="calculation(this.value)" id="c_unit_price" type="text" value="" class="form-control input-sm"></td>
                  <td class="form-group"><input id="c_tax" onblur="calculation2(this.value)" type="text" value="" class="form-control input-sm"></td> -->
                  <td class="form-group"><input id="c_total" type="text" name="condition" value="" class="form-control input-sm" required="required"></td>
                  <td class="form-group"><input id='datetimepicker1' type="text" name="delivery_date" value="" class="form-control input-sm" required="required"></td>
                   <td><select onchange="change_status(this.value)" name="laundry_type" id="laundry" class="form-control input-sm" required="required">
                                         <option disabled="disabled" selected="selected">--Select--</option>
              <?php
                        if(isset($laundry) && $laundry):

                          foreach($laundry as $gst):
                    
                                               ?>
                                        <option value="<?php echo  $gst->laundry_type_name; ?>"><?php echo  $gst->laundry_type_name; ?> </option>


                               <?php
                        endforeach;
                        endif;
                  ?>
                  </select></td>
                  <td class="form-group"><button class="btn green" onclick="add_charge()"><i class="fa fa-plus" aria-hidden="true"></i></button>

                    <br></td>
                </tr> <?php form_close(); ?>
                <script>
          
function search_charge(){

  var value=document.getElementById("c_name").value;

  //alert(value);

    $.ajax({
    type: "POST",
    url: "http://122.163.127.3/hotelobjects/dashboard/search_charge",
    data:'keyword='+value,
    success: function(data){
      $("#suggesstion-box").show();
      $("#suggesstion-box").html(data);
     // $("#search-box").css("background","#FFF");
    }
    });
 }
//To select charge name
// function selectCountry(val) {
// $("#c_name").val(val);
// $("#suggesstion-box").hide();
// }



//                             function calculation(value) {
//                                 var a=document.getElementById("c_quantity").value;
//                                 var total=parseInt(a)*parseInt(value);
//                                 document.getElementById("c_total").value=total;
                                
//                             }
//                             function calculation2(value) {
//                                 var a=document.getElementById("c_total").value;
//                                 var total=parseInt(a)+(parseInt(a)*(parseInt(value)/100));
//                                 document.getElementById("c_total").value=total;
            
//                             }

    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/delete_hotel_laundry_line_item?f_id="+id,
                data:{f_id:id},
                success:function(data)
                {
          //alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }


                        </script>
                        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
        </script>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
   </div>