<!-- 17.11.2015-->
<script src="<?php echo base_url();?>assets/global/plugins/typeahead1/jquery.typeahead.js"></script>
<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
<?php endif;?>
<!-- 19.07.2016-->
<div class="portlet box blue">
	<div class="portlet-title" style="background-color:#95C900;">
		<div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Laundry Service</span> </div>
	</div>
	<div class="portlet-body form">
		<?php

		$form = array(
			'class' => '',
			'id' => 'form',
			'method' => 'post',
		);



		echo form_open_multipart( 'unit_class_controller/add_laundry_service', $form );

		?>
		<div class="form-body">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" id="booking_type" name="booking_type" onchange="booking_type1(this.value)" required placeholder="Guest Type">
							<option value="0">Ad Hoc</option>
							<option value="1">Single Booking</option>
							<option value="2">Group Booking</option>
						</select>
						<label></label>
						<span class="help-block">Guest Type *</span> </div>
				</div>
				<input type="hidden" name="guest_booking_id" id="guest_booking_id">
				<input type="hidden" name="quantity" id="quantity">
				<input type="hidden" name="hidTax" id="hidTax">
				<input type="hidden" name="taxresponse" id="taxresponse">
				<?php $single_booking=$this->unit_class_model->get_single_booking(); ?>
				<div class="col-md-3" id="single_booking1" hidden>
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" id="single_booking" onchange="get_guest_by_booking_id(this.value)" name="single_booking" required="required" placeholder="booking">
							<option value="0"> Select</option>
							<?php
							if ( isset( $single_booking ) && $single_booking ) {

								foreach ( $single_booking as $single ) {
									?>
							<option value="<?php echo $single->booking_id ?>">
								<?php echo $single->booking_id.' - '.$single->cust_name ?>
							</option>
							<?php }}  ?>
						</select>
						<label></label>
						<span class="help-block">Single Booking</span> </div>
				</div>
				<?php $group_booking=$this->unit_class_model->get_group_booking();
		if(isset($group_booking) && $group_booking){
		
		?>
				<div class="col-md-3" id="group_booking1" hidden>
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" id="group_booking" onchange="get_guest_by_booking_id(this.value)" name="group_booking" required="required" placeholder="booking">
							<option value="0"> Select</option>
							<?php
							foreach ( $group_booking as $group ) {
								?>
							<option value="<?php echo $group->id ?>">
								<?php echo $group->id.' - '.$group->name ?>
							</option>
							<?php }  ?>
						</select>
						<label></label>
						<span class="help-block">Group Booking</span> </div>
				</div>
				<?php }  ?>
				<input type="hidden" name="tax_response" id="tax_response">
				<input type="hidden" name="booking_id_hid" id="booking_id_hid">
				<input type="hidden" id="hidden_id" name="hidden_id">
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<div class="typeahead__container">
							<div class="typeahead__field">
								<span class="typeahead__query">										
                                        <input class="js-typeahead-user_v1 form-control" name="g_name[query]" id="Guest_name1"  type="search" placeholder="search guest here..." autocomplete="off">
                                        <label></label>
										<span class="help-block">Search Guest Here</span>
							
								</span>
							</div>
						</div>
						<input type="hidden" name="Guest_name" id="Guest_name">
						<input type="hidden" name="Guest_name_new" id="Guest_name_new">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control  date-picker" id="received_date" name="received_date" placeholder="Recieved Date">
						<label></label>
						<span class="help-block">Recieved Date</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control  date-picker" id="delivery_date" name="delivery_date" placeholder="Delevery Date">
						<label></label>
						<span class="help-block">Delevery Date</span> </div>
				</div>
				<div class="col-md-3" id="bil_pref" style="display:none;">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" onchange="billing(this.value)" id="billing_pref" name="billing_pref">
							<option value="" disabled="disabled" selected="selected">Billing Preference</option>
							<option value="Bill Seperately">Bill Seperately</option>
							<option value="Add to Booking Bill">Add to Booking Bill</option>
						</select>
						<label></label>
						<span class="help-block">Billing Preference</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control" id="special_request" name="special_request" placeholder="Special Request">
						<label></label>
						<span class="help-block">Special Request</span> </div>
				</div>

				<div class="col-md-12">
					<h4 style="margin:45px 0 10px; color:#3EB9C6; font-size:20px"> <i class="fa fa-shirtsinbulk" aria-hidden="true"></i>&nbsp;Laundry Line Items </h4>
				</div>


				<div class="col-md-12" style="padding-top:0px;">
					<div class="">
						<table class="table table-striped table-hover table-bordered mass-book" id="items">
							<thead>
								<tr>
									<th width="10%">Cloth Type</th>
									<th width="10%">Fabric</th>
									<th width="7%">Size</th>
									<th width="10%">Condition</th>
									<th width="10%">Service</th>
									<th width="7%">Qty</th>
									<th width="10%">Color</th>
									<th width="7%">Brand</th>
									<th width="7%">Price</th>
									<th width="7%">Total</th>
									<!-- <th width="7%">Tax</th>
				  <th width="10%">Note</th>-->
									<th width="5%">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr id="abc1">
									<td class="hidden-480 form-group">
										<?php 
					$cloth_type=$this->unit_class_model->all_laundry_cloth_type();					
				  ?>
										<select class="form-control bs-select" name="cloth_type[]" id="cloth_type" onchange="get_laundry_rates();total_amt()" placeholder="Cloth Type">
											<option value="0">Select</option>
											<?php foreach($cloth_type as $cloth){ ?>
											<option value="<?php echo $cloth->laundry_ct_id;?>">
												<?php echo $cloth->laundry_ct_name?>
											</option>
											<?php } ?>
										</select>
									</td>
									<td class="hidden-480 form-group">
										<?php 
					$fabric_type=$this->unit_class_model->all_laundry_fabric_type();					
				  ?>
										<select class="form-control bs-select" onchange="get_laundry_rates();total_amt()" name="fabric[]" id="fabric" placeholder="Fabric Type">
											<option value="0">Select</option>
											<?php foreach($fabric_type as $fabric){ ?>
											<option value="<?php echo $fabric->fabric_id ?>">
												<?php echo $fabric->fabric_type ?>
											</option>
											<?php } ?>
										</select>
									</td>
									<td class="hidden-480 form-group">
										<?php 
					$discount_rule=$this->unit_class_model->discount_rules();					
				  ?>
										<select class="form-control bs-select" name="size[]" id="size" placeholder="Size">
											<option value="0" selected="">Select Size</option>
											<option value="kid">Kid</option>
											<option value="Teen">Teen</option>
											<option value="Adult">Adult</option>
											<option value="N/A">N/A</option>
										</select>
									</td>
									<td class="hidden-480 form-group">
										<select class="form-control bs-select" onchange="get_laundry_rates();total_amt()" name="condition[]" id="condition" placeholder="Condition">
											<option value="New">New</option>
											<option value="Used">Used</option>
											<option value="Old">Old</option>
										</select>
									</td>
									<td class="hidden-480 form-group">
										<?php 
					$service_type=$this->unit_class_model->all_laundry_service_type();					
				  ?>

										<!--onchange="get_price(this.value)"-->

										<select class="form-control bs-select" onchange="get_laundry_rates();total_amt()" name="service[]" id="service" placeholder="Laundry Service">
											<option value="0" selected="">Select Laundry Service</option>
											<?php foreach($service_type as $service){ ?>
											<option value="<?php echo $service->laundry_type_id ?>">
												<?php echo $service->laundry_type_name ?>
											</option>
											<?php } ?>
										</select>
									</td>
									<td class="hidden-480 form-group"><input autocomplete="off" type="text" class="form-control" name="qty[]" onblur="total_amt()" id="qty" placeholder="Qty" onkeypress='return onlyNos(event, this);'>
									</td>
									<td class="hidden-480 form-group">
										<?php 
					$laundry_color=$this->unit_class_model->all_laundry_color();					
				  ?>
										<select class="form-control bs-select" name="color[]" id="color" placeholder="Color">
											<option value="0">Select</option>
											<?php foreach($laundry_color as $color){ ?>
											<option value="<?php echo $color->color_id ?>">
												<?php echo $color->color_name ?>
											</option>
											<?php } ?>
										</select>
									</td>
									<td class="hidden-480 form-group"><input autocomplete="off" type="text" class="form-control" id="brand" name="brand[]" placeholder="Brand">
									</td>
									<td class="hidden-480 form-group"><input autocomplete="off" type="text" class="form-control" id="price" name="price[]" onblur="total_amt()" placeholder="Price">
									</td>
									<td class="hidden-480 form-group"><input autocomplete="off" type="text" class="form-control" id="total" name="total[]" placeholder="Total" readonly>
									</td>
									<!--<td class="hidden-480 form-group">
				<select class="form-control bs-select"   name="tax[]" id="tax"  placeholder="Tax" required>
				<option value="dis">select</option>
                  <option value="Yes">Yes</option>
                  <option value="NO">NO</option>
                  
                  
                  
                </select>
				</td>
				<td class="hidden-480 form-group">
              <input autocomplete="off" type="text" class="form-control" id="note" name="note[]"   placeholder="Note(Tax value)" readonly>
				</td>-->
									<td><a class="btn green btn-xs" id="additems"><i class="fa fa-plus" aria-hidden="true"></i></a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-md-12" align="right" style="font-size: 120%;"> Total Bill Amount : <i class="fa fa-inr" style="font-size: 85%;"><span id="total_amount"></span></i>
					<input type="hidden" name="hidden_total" id="hidden_total">
				</div>
				<input type="hidden" name="total_amount_hidden" id="total_amount_hidden">
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="tax[]" id="tax1" placeholder="Tax" required>
							<option value="dis">select</option>
							<option value="Yes">Yes</option>
							<option value="No">NO</option>
						</select>
						<label></label>
						<span class="help-block">Select Tax</span>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="decimal" class="form-control" id="discount" name="discount" onkeyup="g_total(this.value)" placeholder="Discount">
						<label></label>
						<span class="help-block">Discount</span>
					</div>
				</div>
				<div id="div_tax_type" class="col-md-3 form-horizontal"></div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control" id="grand_total" name="grand_total" placeholder="Grand Total" readonly>
						<label></label>
						<span class="help-block">Grand total</span>
					</div>
				</div>
				<div class="col-md-12" id="pay_id" style="text-align:center;margin-top: 20px; display:none">
					<div class="btn-group">
						<label for="autocellwidth" class="auto_cl btn grey-cascade" id="auto_cl_id" style="background-color:#39B9A1;">
              <input type="checkbox" id="autocellwidth" class="toggle" style="display:none;">
              <span id="disp">Pay Now</span></label>
					

					</div>
				</div>
				<div id="pay" style="display:none;">
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input type="text" class="form-control" id="pay_amount" name="card_number" value="" readonly/>
							<label></label>
							<span class="help-block">Total Amount Paybale *</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input type="number" class="form-control" id="due_amount" name="due_amount" value="" readonly>
							<label></label>
							<span class="help-block">Due Amount *</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input type="number" id="add_amount" class="form-control" name="pay_amount" placeholder="Enter Amount" onblur="check_amount(this.value);"/>
							<input type="hidden" id="booking_id" value="">
							<input type="hidden" id="booking_status_id" value="">
							<label></label>
							<span class="help-block">Amount *</span>
						</div>
						<script type="text/javascript">
							function check_amount( v ) {
								var f = 0;
								var amount = $( "#pay_amount" ).val();


								//alert(due_amount);
								if ( parseInt( amount ) < parseInt( v ) ) {
									//alert("The amount should be less than the due amount Rs: "+due_amount);
									swal( {
											title: "Amount Should Be Smaller",
											text: "(The amount should be less than the due amount Rs: +due_amount)",
											type: "warning"
										},
										function () {
											//location.reload();
										} );
									$( "#add_amount" ).val( "" );
									return false;
								} else {
									f = parseFloat( amount ) - parseFloat( v );
									f = f.toFixed( 2 )
									$( "#due_amount" ).val( f );
									return true;
								}
							}
						</script>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<select name="p_center" class="form-control bs-select" id="p_center">
								<?php $pc=$this->dashboard_model->all_pc();?>
								<?php
								$defProfit = $this->unit_class_model->profit_center_default();
								if ( isset( $defProfit ) && $defProfit ) {
									$defPro = $defProfit->profit_center_location;
								} else {
									$defPro = "Select";
								}
								?>
								<option value="<?php echo $defPro;  ?>" selected>
									<?php echo $defPro; ?>
								</option>
								<?php $pc=$this->dashboard_model->all_pc1();
						foreach($pc as $prfit_center){
						?>
								<option value="<?php echo $prfit_center->profit_center_location;?>">
									<?php echo  $prfit_center->profit_center_location;?>
								</option>
								<?php }?>
							</select>
							<label></label>
							<span class="help-block">Profit Center *</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" placeholder=" Booking Type" id="pay_mode" name="pay_mode" onChange="paym(this.value);">
								<option value="" disabled selected>Select Payment Mode</option>
								<?php 
				$mop = $this->dashboard_model->get_payment_mode_list();
				if($mop != false){
					foreach($mop as $mp){
			?>
								<option value="<?php echo $mp->p_mode_name; ?>">
									<?php echo $mp->p_mode_des; ?>
								</option>
								<?php } } ?>
							</select>
							<label></label>
							<span class="help-block">Payment Mode *</span>
						</div>
					</div>
					<div id="cards" style="display:none;">
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<select name="card_type" class="form-control bs-select" placeholder="Card type" id="card_type">
									<option value="" disabled selected>Select Card Type</option>
									<option value="Cr">Credit Card</option>
									<option value="Dr">Debit Card</option>
									<option value="gift">Gift Card</option>
								</select>
								<label></label>
								<span class="help-block">Card type *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" name="card_bank_name" class="form-control" placeholder="Bank name" id="bankname">
								<label></label>
								<span class="help-block">Bank Name *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<label>Card NO<span class="required"> * </span> </label>
								<input type="text" name="card_no" class="form-control" placeholder="Card no" id="card_no">
								<label></label>
								<span class="help-block">Card NO *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Name on Card" name="card_name" id="card_name">
								<label></label>
								<span class="help-block">Name on Card *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<select name="card_expm" class="form-control bs-select" placeholder="Month" id="card_expm">
											<option value="" disabled selected>Select Month</option>
											<?php 
					$MonthArray = array(
					"1" => "January", "2" => "February", "3" => "March", "4" => "April",
					"5" => "May", "6" => "June", "7" => "July", "8" => "August",
					"9" => "September", "10" => "October", "11" => "November", "12" => "December",);

						for($i=1;$i<13;$i++)
						{
					?>
											<option value="<?php echo $MonthArray[$i]; ?>">
												<?php echo $MonthArray[$i]; ?>
											</option>
											<?php }  ?>
										</select>
										<label></label>
										<span class="help-block">Select Month *</span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<select name="card_expy" class="form-control bs-select" placeholder="Year" id="card_expy">
											<option value="" disabled selected>Select year</option>
											<?php 
						for($i=2016;$i<2075;$i++)
						{
					?>
											<option value="<?php echo $i; ?>">
												<?php echo $i; ?>
											</option>
											<?php }  ?>
										</select>
										<label></label>
										<span class="help-block">Exp year *</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="CVV" name="card_cvv" id="card_cvv">
								<label></label>
								<span class="help-block">CVV *</span>
							</div>
						</div>
					</div>
					<div id="fundss" style="display:none;">
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Bank name" name="ft_bank_name" id="bankname_f">
								<label></label>
								<span class="help-block">Bank Name *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Acc no" name="ft_account_no" id="ac_no">
								<label></label>
								<span class="help-block">A/C No *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="IFSC code" name="ft_ifsc_code" id="ifsc">
								<label></label>
								<span class="help-block">IFSC Code *</span>
							</div>
						</div>
					</div>
					<div id="cheque" style="display:none;">
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Bank name" name="chk_bank_name" id="bankname_c">
								<label></label>
								<span class="help-block">Bank Name *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Cheque no" name="checkno" id="chq_no">
								<label></label>
								<span class="help-block">Cheque No *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Drawer Name" name="chk_drawer_name" id="drw_name_c">
								<label></label>
								<span class="help-block">Drawer Name *</span>
							</div>
						</div>
					</div>
					<div id="draft" style="display:none;">
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Bank name" name="draft_bank_name" id="bankname_d">
								<label></label>
								<span class="help-block">Bank Name *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Draft no" name="draft_no" id="drf_no">
								<label></label>
								<span class="help-block">Draft No *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Drawer Name" name="draft_drawer_name" id="drw_name_d">
								<label></label>
								<span class="help-block">Drawer Name *</span>
							</div>
						</div>
					</div>
					<div id="ewallet" style="display:none;">
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Wallet Name" name="wallet_name" id="w_name">
								<label></label>
								<span class="help-block">Wallet Name *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Transaction ID" name="wallet_tr_id" id="tran_id">
								<label></label>
								<span class="help-block">Transaction ID *</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" placeholder="Recieving Acc" name="wallet_rec_acc" id="recv_acc">
								<label></label>
								<span class="help-block">Recieving Acc *</span>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<select name="p_status" class="form-control bs-select" placeholder="Payment Status" id="p_status_w">
								<option value="Done">Payment Recieved</option>
								<option value="Pending" selected>Payment Processing</option>
								<option value="Cancel">Transaction Declined</option>
							</select>
							<label></label>
							<span class="help-block">Payment Status *</span>
						</div>
					</div>
				</div>
				<div id="pay1" class="col-md-3">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control date-picker" id="payments_due_date" name="payments_due_date" placeholder="payments Due Date">
						<label></label>
						<span class="help-block">Payments Due Date</span> </div>
				</div>
			</div>
		</div>
		<div class="form-actions right">
			<button type="submit" class="btn neongreen">Submit</button>
			<!-- 18.11.2015  -- onclick="return check_mobile();" -->
			<button type="reset" class="btn default">Reset</button>
		</div>
		<input type="hidden" name="hid">
		<?php form_close(); ?>
		<!-- END CONTENT -->

	</div>
</div>
<div class="modal fade" id="myModalNorm1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->

			<div class="modal-header" style="background:#95A5A6; color:#ffffff;"> Search Guest
				<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true">&times;</span> <span class="sr-only">Close</span> </button>
			</div>

			<!-- Modal Body -->
			<div class="modal-body">
				<div class="form-group">
					<div class="input-group">
						<input type="email" class="form-control" id="guest_search" placeholder="Search Guest"/>
						<span class="input-group-btn">
            <button type="button" class="btn green" onclick="return_guest_search()">Search</button>
            </span>
					
					</div>
				</div>
				<div id="return_guest" style="overflow-y:scroll; height:300px; display:none;"> </div>
			</div>

			<!-- Modal Footer -->
			<div class="modal-footer">
				<button type="button" class="btn red" data-dismiss="modal"> Close </button>
			</div>
		</div>
	</div>
</div>
<script>
	function paym( val ) {
		if ( val == 'cards' ) {
			document.getElementById( 'cards' ).style.display = 'block';
		} else {
			document.getElementById( 'cards' ).style.display = 'none';
		}
		if ( val == 'fund' ) {
			document.getElementById( 'fundss' ).style.display = 'block';
		} else {
			document.getElementById( 'fundss' ).style.display = 'none';
		}
		if ( val == 'cheque' ) {
			document.getElementById( 'cheque' ).style.display = 'block';
		} else {
			document.getElementById( 'cheque' ).style.display = 'none';
		}
		if ( val == 'draft' ) {
			document.getElementById( 'draft' ).style.display = 'block';
		} else {
			document.getElementById( 'draft' ).style.display = 'none';
		}
		if ( val == 'ewallet' ) {
			document.getElementById( 'ewallet' ).style.display = 'block';
		} else {
			document.getElementById( 'ewallet' ).style.display = 'none';
		}
		if ( val == 'cash' ) {
			document.getElementById( 'cardss' ).style.display = 'none';
			document.getElementById( 'fundss' ).style.display = 'none';
			document.getElementById( 'ewallet' ).style.display = 'none';
			document.getElementById( 'cheque' ).style.display = 'none';
			document.getElementById( 'draft' ).style.display = 'none';
		}
	}

	function billing( a ) {

		//alert(a);
		if ( a == "Bill Seperately" ) {
			$( '#pay_id' ).show();
			var dd = $( '#grand_total' ).val();
			$( '#pay_amount' ).val( dd );

		} else {
			$( '#pay' ).hide();
			$( '#pay_id' ).hide();
			//co();
			$( '#disp' ).text( "Pay Now" );
			$( '#auto_cl_id' ).style.backgroundColor = "#39B9A1";
		}

	}
	var quantity = 0;

	function cal_amount( a ) {
		//$('#pay_amount').val()
		if ( a == '' ) {
			a = 0;
		}
		var b = $( '#hidden_grand_total' ).val();
		a = parseFloat( a );
		b = parseFloat( b );
		a = b - a;
		$( '#payments_due' ).val( a );
	}

	function modesofpayments() {
		var y = document.getElementById( "mop" ).value;
		// alert(y);

		if ( y == "checks" ) {
			document.getElementById( "check" ).style.display = "block";
		} else {
			document.getElementById( "check" ).style.display = "none";
		}
		if ( y == "cards" ) {
			document.getElementById( "cards" ).style.display = "block";
		} else {
			document.getElementById( "cards" ).style.display = "none";
		}
		if ( y == "draft" ) {
			document.getElementById( "draft" ).style.display = "block";
		} else {
			document.getElementById( "draft" ).style.display = "none";
		}
		if ( y == "fund" ) {
			document.getElementById( "fundtransfer" ).style.display = "block";
		} else {
			document.getElementById( "fundtransfer" ).style.display = "none";
		}
	}
	$( document ).ready( function () {
		cellWidth = 100;
		cellWidthSpec = $( this ).is( ":checked" ) ? "Auto" : "Fixed";


		$( '#booking_type' ).on( 'change', function () {

			var type = $( '#booking_type' ).val();

			if ( type == '1' || type == '2' ) {

				//alert(type);
				$( 'select#billing_pref' ).append( '<option value="1">One</option>' );

			}
		} );

	} );

	$( "#autocellwidth" ).click( function () {


		cellWidth = 100; // reset for "Fixed" mode
		cellWidthSpec = $( this ).is( ":checked" ) ? "Auto" : "Fixed";

		document.getElementById( 'auto_cl_id' ).style.backgroundColor = $( this ).is( ":checked" ) ? "#297CAC" : "#39B9A1";
		var a = $( this ).is( ":checked" ) ? "Pay Later" : "Pay Now";
		if ( a == 'Pay Later' ) {
			document.getElementById( 'pay' ).style.display = 'block';
			document.getElementById( 'pay1' ).style.display = 'block';
		} else {

			document.getElementById( 'pay' ).style.display = 'none';
			document.getElementById( 'pay1' ).style.display = 'block';
		}

		$( '#disp' ).text( a );

	} );

	function co() {

		cellWidth = 100; // reset for "Fixed" mode
		cellWidthSpec = $( this ).is( ":checked" ) ? "Auto" : "Fixed";

		document.getElementById( 'auto_cl_id' ).style.backgroundColor = $( this ).is( ":checked" ) ? "#297CAC" : "#39B9A1";
		var a = $( this ).is( ":checked" ) ? "Pay Later" : "Pay Now";
		if ( a == 'Pay Later' ) {
			document.getElementById( 'pay' ).style.display = 'block';
			document.getElementById( 'pay1' ).style.display = 'block';
		} else {

			document.getElementById( 'pay' ).style.display = 'none';
			document.getElementById( 'pay1' ).style.display = 'block';
		}

		$( '#disp' ).text( a );
	}


	function g_total( discount ) {
		if ( window.grandtotal < discount ) {
			swal( "Denied!", "Discount Should not more than Total Bill!", "error" );
			$( '#discount' ).val( '' );
		}
		var ss = $( '#tax1' ).val();
		var hidden_grand_total = $( '#hidden_total' ).val();
		///alert(ss);
		if ( ss == 'Yes' ) {
			if ( discount != '' ) {
				discount = parseFloat( discount );
				var gtt1 = parseFloat( $( '#grand_total' ).val() );

				if ( discount > window.total ) {
					swal( "Denied!", "Discount Should not more than Total Bill!", "error" );
				} else {
					//discount=gtt1 - discount;
					//alert(window.grandtotal);
					window.grandtotal = window.grandtotal1 - discount;
					$( '#grand_total' ).val( window.grandtotal ); //alert();
					$( '#pay_amount' ).val( window.grandtotal );
					$( '#due_amount' ).val( window.grandtotal );

				}

			} else {
				window.grandtotal = window.total;
				window.grandtotal = window.grandtotal1;
				$( '#grand_total' ).val( window.grandtotal );
				$( '#pay_amount' ).val( window.grandtotal );
			}

		} else {

			var discount = parseFloat( $( '#discount' ).val() );

			var grand_total = $( '#grand_total' ).val();
			if ( discount >= 0 ) {
				var total = parseFloat( hidden_grand_total ) - parseFloat( discount );
				//alert('total :- ' +total);
				$( '#grand_total' ).val( total );
				$( '#due_amount' ).val( total );
			} else {
				$( '#grand_total' ).val( hidden_grand_total );
				$( '#due_amount' ).val( hidden_grand_total );
			}
		}

	}


	var flag = 0,
		flag1 = 0;
	sum = 0;

	function is_married( value ) {

		if ( value == "Yes" ) {

			document.getElementById( "g_anniv" ).style.display = "block";

		} else {
			document.getElementById( "g_anniv" ).style.display = "none";

		}
	}
	$( document ).on( 'blur', '#form_control_11', function () {
		$( '#form_control_11' ).addClass( 'focus' );
	} );
	$( document ).on( 'blur', '#form_control_12', function () {
		$( '#form_control_12' ).addClass( 'focus' );
	} );

	//.............tax rule addition dynamically.....


	$( 'select#tax1' ).on( 'change', function () {

		var tax_val = $( this ).val();
		var l_price = parseFloat( $( '#total_amount' ).text() );
		var args = 'a';
		var l_date = $( '#received_date' ).val();
		if ( l_date != '' ) {
			if ( tax_val == 'Yes' ) {
				$( '#discount' ).val( '' );
				$.ajax( {

					type: "POST",
					url: "<?php  echo base_url();?>dashboard/showtaxplan1",
					data: {
						"price": l_price,
						"type1": "Extra Service",
						"dates": l_date,
						"args": args
					},
					success: function ( data ) {

						//alert(data.total);
						var narr = data;
						var to = l_price + data.total;
						$( '#grand_total' ).val( to );
						window.grandtotal = to;
						window.grandtotal1 = to;
						$( '#pay_amount' ).val( to );
						$( '#due_amount' ).val( to );
						delete narr[ 'r_id' ];
						delete narr[ 'planName' ];
						delete narr[ 'total' ];
						$( '#div_tax_type' ).show();
						$.each( narr, function ( key, value ) { //key + ": " + value
							$( "#div_tax_type" ).append( '<div class="col-md-6"><div class="form-group form-md-line-input" style="padding-top: 20px;"><label class="control-label">' + key + ' = ' + value + '</label></div></div>' );
						} );
						$( '#hidTax' ).val( tax_val );
						var stringData = JSON.stringify( narr );
						$( '#taxresponse' ).val( stringData );


						console.log( data );
						//alert(data);

					},

				} );

			} else {
				var ab;
				$( '#discount' ).val( '' );
				$( '#taxresponse' ).val( '' );
				$( '#hidTax' ).val( tax_val );
				$( '#div_tax_type' ).hide();
				$( "#div_tax_type" ).html( '' );
				var gtt = parseFloat( $( '#grand_total' ).val() );
				//if(gtt>l_price){
				$( '#grand_total' ).val( l_price );
				window.grandtotal = l_price;
				window.grandtotal1 = l_price;
				$( '#pay_amount' ).val( l_price );
				$( '#due_amount' ).val( l_price );


				//}

				//alert('noooo');
			}
		} else {
			$( '#tax1' ).val( 'dis' ).change();
			swal( "Please Select Date!" );

		}

	} );


	// not working
	/*
	function tax1(){
		
		var tax_val = $('#tax1').val();
		//alert(tax_val);
		var l_price = $('#total_amount').text();
		var args = 'a';
		var l_date = $('#received_date').val();
		alert(l_date+'jjj');
		if(l_date!=''){
		if(tax_val == 'Yes'){
			
			
			//alert('hello' +l_price);
			$.ajax({
				
				type:"POST",
				url:"<?php  echo base_url();?>dashboard/showtaxplan1",
				data:{"price":l_price,"type1":"Extra Service","dates":l_date,"args":args},
				success:function(data){
					//alert(data.total);
					
					
					
				},
				
			});
			
		}
		else{
			
			//alert('noooo');
		}
		}else{
			swal("Please Select Date!");
		}
		
	}*/


	//..............

	/*function modesofpayments()
	   {
		   var y= document.getElementById("mop").value;
		   alert(y);
		    
		   if(y=="check"){
			   document.getElementById("check").style.display="block";
		   }else{
			   document.getElementById("check").style.display="none";
		   }
		   if(y=="draft"){
			   document.getElementById("draft").style.display="block";
		   }else{
			   document.getElementById("draft").style.display="none";
		   }
		   if(y=="fund"){
			   document.getElementById("fundtransfer").style.display="block";
		   }else{
			   document.getElementById("fundtransfer").style.display="none";
		   }
	   }*/
	var ff = flag;
	var check = $( '#cloth_type' + ff ).val();
	var a = $( '#cloth_type' ).val();
	//if(check<=a){
	//var color=0;

	$( "#additems" ).click( function () {
		var cloth_type = $( '#cloth_type' ).val();
		var size = $( '#size' ).val();
		var condition = $( '#condition' ).val();
		var service = $( '#service' ).val();
		var qty = $( '#qty' ).val();
		var color = $( '#color' ).val();
		var fabric = $( '#fabric' ).val();
		var brand = $( '#brand' ).val();
		var price = parseFloat( $( '#price' ).val() );
		var total = parseFloat( $( '#total' ).val() );


		//alert(total);
		if ( cloth_type > 0 && service > 0 && fabric > 0 && price > 0 && total > 0 ) {
			var x = 0;
			jQuery.ajax( {
				type: "POST",
				url: "<?php echo base_url(); ?>unit_class_controller/get_laundrydata_by_id",
				dataType: 'json',
				data: {
					cloth_type: cloth_type,
					fabric: fabric,
					service: service,
					color: color
				},
				success: function ( data ) {
					//alert(data.name);
					//$('#cloth_type'+flag).val(data.name);
					//$('#ids').val( it+','+ i_name) ;
					$( '#items tr:first' ).after( '<tr id="row_' + flag + '">' +
						'<td class="hidden-480"><input name="cloth_type[]" id="cloth_type' + flag + '" type="text" value="' + data.name + '" class="form-control input-sm" readonly></td>' +
						'<td class="hidden-480"><input name="fabric[]" id="fabric' + flag + '" type="text" value="' + data.fabric + '" class="form-control input-sm" readonly></td>' +
						'<td class="hidden-480"><input name="size[]" id="fabric' + flag + '" type="text" value="' + size + '" class="form-control input-sm" readonly></td>' +
						'<td class="hidden-480"><input name="condition[]" id="condition' + flag + '" type="text" value="' + condition + '" class="form-control input-sm" readonly ></td>' +
						'<td class="hidden-480"><input name="service[]" id="service' + flag + '" type="text" value="' + data.service + '" class="form-control input-sm" readonly></td>' +
						'<td class="hidden-480"><input name="qty[]" id="qty' + flag + '" type="text" value="' + qty + '"  class="form-control input-sm" readonly></td>' +
						'<td class="hidden-480"><input name="color[]" id="color' + flag + '" type="text" value="' + data.color + '" class="form-control input-sm" readonly></td>' +
						'<input type="hidden" name="h_color[]" value="' + color + '"><input type="hidden" name="h_cloth_type[]" value="' + cloth_type + '"><input type="hidden" name="h_fabric[]" value="' + fabric + '"><input type="hidden" name="h_service[]" value="' + service + '">' +
						'<td class="hidden-480"><input name="brand[]" id="brand' + flag + '" type="text" value="' + brand + '" class="form-control input-sm" readonly></td>' +
						'<td class="hidden-480"><input name="price[]" id="price' + flag + '" type="text" value="' + price + '"  class="form-control input-sm" readonly></td>' +
						'<td class="hidden-480"><input name="total[]" id="total' + flag + '" type="text" value="' + total + '"  class="form-control input-sm" readonly></td>' +
						'<td><table><tr><td><a  class="btn red btn-xs" onclick="removeRow(' + flag + ',' + total + ',' + qty + ')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>' );
					/*'<td class="hidden-480"><select class="form-control bs-select"   name="tax[]" id="tax1'+flag+'" onchange="tax1('+flag+')"  placeholder="Tax" required><option value="dis">select</option>'+
    '<option value="Yes">Yes</option><option value="NO">NO</option></select>'+
	'<td class="hidden-480"><input name="note[]" id="note'+flag+'" type="text" value="'+note+'" onblur="calculation('+flag+')" class="form-control input-sm"  readonly ></td>'+*/
					flag++;
					window.countRow++;
					window.totalquantity = parseFloat( window.totalquantity ) + parseFloat( qty );
					$( '#quantity' ).val( window.totalquantity );
					//alert(window.totalquantity);
					window.total = parseFloat( window.total ) + parseFloat( total );
					//alert(window.total);
					window.grandtotal = window.total;
					window.grandtotal1 = window.total;
					$( '#total_amount' ).text( window.total );
					window.grandtotal = window.grandtotal + window.total;
					//''alert(window.grandtotal+'here');
					$( '#grand_total' ).val( window.grandtotal );
					$( '#grand_total' ).val( window.total );
					$( '#pay_amount' ).val( window.total );
					//$('#pay_amount').val();
					$( '#hidden_total' ).val( window.total );
					$( '#total_amount_hidden' ).val( window.total );
					$( '#tax1' ).val( 'dis' ).change();
					$( '#discount' ).val( '' );
					$( "#div_tax_type" ).html( '' );
					$( '#add_amount' ).val( '' );
					$( '#due_amount' ).val( '' );
					var tm1 = 0;
					var tm = parseFloat( $( '#total_amount' ).text() );
					var gt1 = parseFloat( $( '#grand_total' ).val() );
					$( '#due_amount' ).val( gt1 );
					//$('#pay_amount').val("");
					//$('#grand_total').val("").change();


				}
			} );
		} else {
			/*   if(tax=='dis'){
				    alert("Select TAX ");
			   }*/
			swal( "warning!", "Please fillup all fields!", "error" );
		}
		//alert(note);
		if ( cloth_type > 0 && service > 0 && fabric > 0 && qty > 0 && price >= 0 && total >= 0 ) {


			$( '#cloth_type' ).val( '0' ).change();
			$( '#size' ).val( '0' ).change();
			$( '#condition' ).val( '0' );
			$( '#service' ).val( '0' ).change();
			$( '#qty' ).val( '' );
			$( '#color' ).val( '0' ).change();
			$( '#fabric' ).val( '0' ).change();
			$( '#brand' ).val( '' );
			$( '#price' ).val( '' );
			$( '#total' ).val( '' );
		}

	} );



	function check_date() {

		var a = $( '#contract_start_date' ).val();
		a = new Date( a );
		var b = $( '#contract_end_date' ).val();
		b = new Date( b );
		if ( a >= b && b != '' ) {
			alert( 'Date Should Getter Than Start Date ' );
			$( '#contract_end_date' ).val( '' );
		}

		var c = $( '#contract_end_date' + ( flag - 1 ) ).val();
		// alert(c);
		c = new Date( c );
		if ( c > a ) {

			alert( 'Please Enter Valid Date' );
			$( '#contract_start_date' ).val( '' );
			$( '#contract_end_date' ).val( '' );
		}



	}

	$( document ).ready( function () {
		$( '#total_amount' ).text( '0' );
		$( '#div_tax_type' ).hide();

		window.countRow = 0;
		window.total = 0;
		window.grandtotal = 0;
		window.grandtotal1 = 0;
		window.totalquantity = 0;
		$( '#pay_id' ).show();
		var dd = $( '#grand_total' ).val();
		$( '#pay_amount' ).val( dd );
		$( '#billing_pref' ).val( 'Bill Seperately' ).change();
	} );

	function fetch_all_address() {

		var pin_code = document.getElementById( 'pin_code' ).value;

		jQuery.ajax( {
			type: "POST",
			url: "<?php echo base_url(); ?>bookings/fetch_address",
			dataType: 'json',
			data: {
				pincode: pin_code
			},
			success: function ( data ) {
				//alert(data.country);
				document.getElementById( "country" ).focus();
				$( '#country' ).val( data.country );
				document.getElementById( "state" ).focus();
				$( '#state' ).val( data.state );
				document.getElementById( "city" ).focus();
				$( '#city' ).val( data.city );
			}

		} );




	}
	$( "#addGuest" ).click( function () {
		$( '#myModalNorm1' ).modal( 'show' );
		$( '#return_guest' ).html( "" );
		$( "#return_guest" ).css( "display", "none" );
	} );

	function return_guest_search() {
		var guest_name = $( '#guest_search' ).val();
		//alert(guest_name);
		jQuery.ajax( {
			type: "POST",
			url: "<?php echo base_url(); ?>bookings/return_guest_search",
			datatype: 'json',
			data: {
				guest: guest_name
			},
			success: function ( data ) {
				var resultHtml = '';
				resultHtml += '<table class="table table-bordered" cellpadding="0" cellspacing="0" ><thead><tr><th>Name</th><th>Address</th><th width="100">Mobile No</th></tr></thead><tbody>';
				$.each( data, function ( key, value ) {
					resultHtml += '<tr  class="crsr collapsed" data-toggle="collapse" data-target="#toggleDemo' + value.g_id + '">';
					resultHtml += '<td>' + value.g_name + '</td>';
					resultHtml += '<td>' + value.g_address + '</td>';
					resultHtml += '<td>' + value.g_contact_no + '</td>';
					resultHtml += '</tr><tr id="toggleDemo' + value.g_id + '"  class="mrgn">';
					resultHtml += '<td class="no-marin"  colspan="3"  cellpadding="0" cellspacing="0">';
					resultHtml += '<div class="side clearfix" style="width:100%;"><div class="col-xs-7 ex"><p><label style="font-size:13px;">' + value.g_name + '</label><br><label>';

					resultHtml += '<input type="hidden" id="g_id_hide"  value="' + value.g_id + '" ></input>';
					resultHtml += '<label style="font-size:13px;">Room No: ' + value.room_no + '</label><br><label style="font-size:13px;">Last Check In Date: ' + value.cust_from_date + '</label><br> <button class="act active btn blue btn-sm" name="g_id" id="g_id"" value="' + value.g_id + '" onclick="get_guest(' + value.g_id + ')" >Continue</button> </p></div>';
					if ( value.g_photo_thumb != '' ) {
						resultHtml += '<div class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
							'<img src="<?php echo base_url() ?>upload/guest/' + value.g_photo_thumb + '" width="100px" height="92px"  > ' +
							'</div>';
					} else {
						resultHtml += '<div  class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
							'<img src="<?php echo base_url() ?>upload/guest/no_images.png" width="100px" height="92px" > ' +
							'</div>';
					}
					//resultHtml+='<td><input type="radio" name="g_id" id="'+value.g_id+'" value="'+value.g_id+'" onclick="get_guest(this.value)" /></td>';
					resultHtml += '</div></td></tr>';
				} );
				resultHtml += '</tbody></table>';
				$( '#return_guest' ).html( resultHtml );
				//alert(data);
				// console.log(data);
				//$('#dtls').html(data);
			}
		} );
		$( "#return_guest" ).css( "display", "block" );
		return false;
	}
</script>
<script>
	function get_guest_by_id( id ) {
		//alert(id);


		jQuery.ajax( {
			type: "POST",
			url: "<?php echo base_url(); ?>bookings/return_guest_get",
			datatype: 'json',
			data: {
				guest_id: id
			},
			success: function ( data ) {
				if ( id ) {
					$( '#tab11' ).hide();
					$( '#tab12' ).show();
					$( '#id_guest2' ).val( id );
					//alert(data.booking_id);
					//alert(data.g_name);
					$( '#g_name_hid' ).val( data.g_name );
					$( '#Guest_name' ).val( data.g_name );
					$( '#guest_type1' ).val( data.g_type ).change();
					$( '#hidden_id' ).val( id );
				}


			}
		} );

	}


	function get_guest( id ) {
		//var g_id = $('#g_id_hide').val();
		var g_id = id;
		// alert(g_id);
		jQuery.ajax( {
			type: "POST",
			url: "<?php echo base_url(); ?>bookings/return_guest_get",
			datatype: 'json',
			data: {
				guest_id: g_id
			},
			success: function ( data ) {



				$( '#tab11' ).hide();
				$( '#tab12' ).show();
				$( '#id_guest2' ).val( id );

				$( '#Guest_name' ).val( data.g_name );
				$( '#guest_type1' ).val( data.g_type ).change();

				$( '#hidden_id' ).val( data.g_id );
				/* $('#cust_contact_no').val(data.g_contact_no);
					$('#cust_mail').val(data.g_email);
					*/
				$( '#myModalNorm1' ).modal( 'hide' );

			}
		} );
		return false;
	}


	function get_price( id ) {
		var cloth = $( '#cloth_type' ).val();
		//var service=$('#service').val();
		var fabric = $( '#fabric' ).val();

		jQuery.ajax( {
			type: "POST",
			url: "<?php echo base_url(); ?>unit_class_controller/get_price_by_clothID_fabricId",
			datatype: 'json',
			data: {
				cloth: cloth,
				fab: fabric,
				service: id
			},
			success: function ( data ) {
				//alert(data.dry);
				if ( service == '1' )
					$( '#price' ).val( data.dry );
				else if ( service == '2' )
					$( '#price' ).val( data.laundry );
				else
					$( '#price' ).val( data.iron );

				/*alert(data.id);
                  alert(data.laundry);
                  alert(data.dry);
                  alert(data.iron);*/
				//alert(data.id);

				//$('#tab11').hide();


			}
		} );



	}

	function booking_type1( id ) {
		//alert(id);
		if ( id == 1 ) {
			$( "#single_booking1" ).show();
			$( "#group_booking1" ).hide();
			$( "#Guest_name" ).prop( "disabled", true );
			$( '#bil_pref' ).show();
			$( '#billing_pref' ).val( 'Add to Booking Bill' ).change();
			$( '#pay' ).hide();
			$( '#pay_id' ).hide();
			//co();
			$( '#disp' ).text( "Pay Now" );
			$( '#auto_cl_id' ).style.backgroundColor = "#39B9A1";
		} else if ( id == 2 ) {
			$( "#group_booking1" ).show();
			$( "#single_booking1" ).hide();
			$( "#Guest_name" ).prop( "disabled", true );
			$( '#bil_pref' ).show();
			$( '#billing_pref' ).val( 'Add to Booking Bill' ).change();
			$( '#pay' ).hide();
			$( '#pay_id' ).hide();
			//co();
			$( '#disp' ).text( "Pay Now" );
			$( '#auto_cl_id' ).style.backgroundColor = "#39B9A1";
		} else {
			$( "#Guest_name" ).prop( "disabled", false );
			$( "#group_booking1" ).hide();
			$( "#single_booking1" ).hide();
			$( '#bil_pref' ).hide();

			$( '#pay_id' ).show();
			var dd = $( '#grand_total' ).val();
			$( '#pay_amount' ).val( dd );
			$( '#billing_pref' ).val( 'Bill Seperately' ).change();

			//$('#billing_pref').remove("Add to Booking Bill");
			//$('#billing_pref option[value="a5"]').remove();
			//$('#billing_pref').val("Add to Booking Bill").attr('disabled', true);
			//$('billing_pref option[value="'+val+'"]')
			//$("#billing_pref Add to Booking Bill:selected").attr('disabled','disabled') 
			/*
			$('#billing_pref').children('option[value=Add to Booking Bill]')
			                prop('disabled', true);*/
		}
	}


	function removeRow( a, b, q ) {
		var c = $( '#total_amount' ).text();
		window.total = window.total - b;
		window.totalquantity = window.totalquantity - q;
		$( '#quantity' ).val( window.totalquantity );
		b = parseFloat( c ) - b;
		$( '#total_amount' ).text( b );
		$( '#grand_total' ).val( b );

		$( '#row_' + a ).remove();

	}

	function editRow( a ) {
		//alert(a);
		$( '#row_' + a ).hide();
		//$('#line').show(); return "<html>" + $("html").html() + "</html>";
		$( '#row_' + a ).after( '<tr id="row_' + a + '">' +
			//'<td class="hidden-480"><input name="cloth_type[]" id="cloth_type'+a+'" type="text" value="'+a+'" class="form-control input-sm" ></td></tr></table>');
			//$("html").html() + "</html>");
			$( '#abc1' ).html() );

	}

	function get_guest_by_booking_id( id ) {
		//alert(id);
		var a = $( '#booking_type' ).val(); //single=2 group3
		//alert(a);
		jQuery.ajax( {
			type: "POST",
			url: "<?php echo base_url(); ?>unit_class_controller/get_guest_by_booking_id",
			datatype: 'json',
			data: {
				booking_id: id,
				booking_type: a
			},
			success: function ( data ) {
				//alert(data.id);
				//alert(data.name);					
				//alert(data.type);
				//$('#Guest_name').text(data.name);
				//alert(data.name);
				$( '#Guest_name_new' ).val( data.name );
				$( '#Guest_name' ).val( data.name );
				$( '#Guest_name1' ).val( data.name );
				$( '#guest_type1' ).val( data.type ).change();
				$( '#guest_booking_id' ).val( id );
				//alert(id);
				$( '#booking_id_hid' ).val( id );

			}
		} );

	}
</script>
<script>
	function calculation( id ) {

		var qty = $( '#qty' + id ).val();
		var price = $( '#price' + id ).val();
		var tax = $( '#tax1' ).val();
		var total = $( '#total' + id ).val();
		var note = $( '#note' + id ).val();
		//alert(qty+price+total+tax+note);
		var amount = parseFloat( qty ) * parseFloat( price ) + parseFloat( note );

		$( '#total' + id ).val( amount );
		$( '#tax' + id ).val( 'dis' ).change();
		$( '#note' + id ).val( '0' );



	}

	function total_amt() {
		var pr = parseFloat( $( '#price' ).val() );
		var qty = parseFloat( $( '#qty' ).val() );
		//alert(pr);
		//alert(qty);
		if ( qty > 0 && pr > 0 ) {
			pr = pr * qty;
			//alert('wrong');
			$( '#total' ).val( pr );
		} else {
			$( '#total' ).val( '' );
		}
	}

	function get_laundry_rates() {
		var cloth = $( '#cloth_type' ).val();
		var fabric = $( '#fabric' ).val();
		var service = $( '#service' ).val();
		var qt = parseFloat( $( '#qty' ).val() );
		if ( cloth != '' && service != '' && service > 0 ) {
			jQuery.ajax( {
				type: "POST",
				url: "<?php echo base_url(); ?>rate_plan/get_laundry_rates",
				data: {
					cloth: cloth,
					fabric: fabric,
					service: service,
				},
				success: function ( data ) {
						//alert(data.price);

						$( '#price' ).val( data.price );
						if ( qt > 0 ) {
							qt = qt * data.price;
							$( '#total' ).val( qt );
						}



					} // end ajax success
			} );

		}
		//alert('amt');
		total_amt();
	}

	$.typeahead( {
		input: '.js-typeahead-user_v1',
		minLength: 1,
		order: "asc",
		dynamic: true,
		delay: 200,
		backdrop: {
			"background-color": "#fff"
		},

		source: {
			project: {
				display: "name",
				ajax: [ {
					type: "GET",
					url: "<?php echo base_url();?>dashboard/test_typehead",
					data: {
						q: "{{query}}"
					}
				}, "data.name" ],
				template: '<div class="clearfix">' +
					'<div class="project-img">' +
					'<img class="img-responsive" src="{{image}}">' +
					'</div>' +
					'<div class="project-information">' +
					'<span class="pro-name" style="font-size:15px;"> {{name}}</span><span><i class="fa fa-phone"></i> <strong>Contact no:</strong> {{ph_no}} </span><span><i class="fa fa-envelope"></i> <strong>Email:</strong> {{email}}</span></span>' +
					'</span>' +
					'</div>' +
					'</div>'
			}
		},
		callback: {
			onClick: function ( node, a, item, event ) {
				// alert(JSON.stringify(item));

				//$("#cust_address").val(item.pincode);
				//$("#g_address_child").val(item.address);
				//$("#cust_contact_no").val(item.ph_no);
				//$("#cust_mail").val(item.email);
				$( "#Guest_name" ).val( item.name );
				$( "#Guest_name_new" ).val( item.name );
				//$('#id_guest2').val(item.id);

			},
			onSendRequest: function ( node, query ) {
				console.log( 'request is sent' )

			},
			onReceiveRequest: function ( node, query ) {
				//console.log('request is received')
				//alert("defesddsfdsfs");
				if ( query != '' ) {
					$( "#Guest_name" ).val( '' );

					/*$("#cust_address").val('');
			$("#g_address_child").val('');
			$("#cust_contact_no").val('');
			$("#cust_mail").val('');
			$('#id_guest2').val('');*/
				}
			},
			onCancel: function ( node, query ) {
				//console.log('request is received')
				alert( "defesddsfdsfs" );
				if ( query != '' ) {
					$( "#Guest_name" ).val( '' );
					/**	$("#cust_address").val('');
					$("#g_address_child").val('');
					$("#cust_contact_no").val('');
					$("#cust_mail").val('');
					$('#id_guest2').val('');*/
				}
			}
		},
		debug: true
	} );
</script>