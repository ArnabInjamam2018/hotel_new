<!-- 17.11.2015-->
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <!-- 19.07.2016-->
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Laundry Service</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

	$form = array(
		'class' 			=> '',
		'id'				=> 'form',
		'method'			=> 'post',								
	);
	
	

	echo form_open_multipart('unit_class_controller/add_laundry_service',$form);

	?>
    <div class="form-body"> 
      <div class="row">
		
		<div class="col-md-4">
            <div class="form-group form-md-line-input" ><!-- bs-select  -->
              <select  class="form-control bs-select" id="booking_type" name="booking_type" onchange="booking_type1(this.value)" required="required" placeholder="Guest Type">
                <option value="0" >Ad Hoc</option>
                <option value="1" >Single Booking</option>
                <option value="2" >Group Booking</option>
				
              </select>
              <label></label>
              <span class="help-block">Guest Type *</span> </div>
        </div>
	<input type="hidden" name="guest_booking_id" id="guest_booking_id"> 
	<input type="hidden" name="quantity" id="quantity"> 
		
<?php $single_booking=$this->unit_class_model->get_single_booking(); ?>

		<div class="col-md-4" id="single_booking1" hidden>
            <div class="form-group form-md-line-input" >
			
              <select  class="form-control bs-select" id="single_booking" onchange="get_guest_by_booking_id(this.value)"  name="single_booking"  required="required" placeholder="booking" >
                <option value="0" > Select</option>
				<?php
				if(isset($single_booking) && $single_booking){
					
					foreach($single_booking as $single){
				?>
				<option value="<?php echo $single->booking_id ?>" ><?php echo $single->booking_id.' - '.$single->cust_name ?></option>
				<?php }}  ?>
              </select>
              <label></label>
              <span class="help-block">Single Booking</span> </div>
        </div>
		
		<?php $group_booking=$this->unit_class_model->get_group_booking();//print_r($group_booking);
		if(isset($group_booking) && $group_booking){
		
		?>
		<div class="col-md-4" id="group_booking1" hidden>
            <div class="form-group form-md-line-input" >
			
              <select  class="form-control bs-select" id="group_booking" onchange="get_guest_by_booking_id(this.value)" name="group_booking"    required="required" placeholder="booking" >
				<option value="0" > Select</option>
			  <?php
				foreach($group_booking as $group){
				?>
				<option value="<?php echo $group->id ?>" > <?php echo $group->id.' - '.$group->name ?></option>
				<?php }}  ?>
              </select>
              <label></label>
              <span class="help-block">Group Booking</span> </div>
        </div>
	
	<input type="hidden" id="hidden_id" name="hidden_id" >
			 <div class="col-md-4">			
                <div class="form-group form-md-line-input">
                    <div class="input-group">
                      <input autocomplete="off" type="text" class="form-control" id="Guest_name" name="Guest_name" onblur="get_guest_by_id(this.value)"  required="required" placeholder="Guest Name">
               
                      <span class="input-group-btn">
                      <button class="btn green" type="button" id="addGuest"><i class="fa fa-eye" aria-hidden="true"></i></button>
                      </span>
                    </div>
                </div>
            </div>

       
		<div class="col-md-4"><!--onchange="check_corporate(this.value)"-->
            <div class="form-group form-md-line-input" >
              <select  class="form-control bs-select" id="guest_type1" name="guest_type" required="required">
                <option value="" disabled="disabled" selected="selected">Guest Type</option>
                <option value="Family" >Family</option>
                <option value="Bachelor">Bachelor</option>
                <option value="Tourist">Tourist</option>
                <!--<option value="Sr_citizen">Sr Citizen</option>-->
                <option value="Corporate">Corporate</option>
                <option value="VIP">VIP</option>
                <option value="Government_official">Government Official</option>
                <option value="Military">Military </option>
                <option value="internal_staff">Internal Staff</option>
              </select>
              <label></label>
              <span class="help-block">Guest Type *</span> </div>
        </div>
		
       
	    <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control  date-picker" id="received_date" name="received_date" placeholder="Recieved Date">
              <label></label>
              <span class="help-block">Recieved Date</span> </div>
        </div>
		
		 <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control  date-picker" id="delivery_date" name="delivery_date" placeholder="Delevery Date">
              <label></label>
              <span class="help-block">Delevery Date</span>
			  </div>
        </div>
		
			<div class="col-md-4">
				<div class="form-group form-md-line-input" >
				  <select  class="form-control bs-select" onchange="billing(this.value)" id="billing_pref" name="billing_pref" required="required">
					<option value="" disabled="disabled" selected="selected">Billing Preference</option>
					<option value="Bill Seperately" >Bill Seperately</option>
					<option value="Add to Booking Bill">Add to Booking Bill</option>
				  </select>
				  <label></label>
				  <span class="help-block">Billing Preference</span> </div>
			</div>
		
        <div class="col-md-4"> 
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="special_request" name="special_request"   placeholder="Special Request">
              <label></label>
              <span class="help-block">Special Request</span> 
            </div> 
        </div>
		
      
			
     <div class="col-md-12" style="padding-top:30px;">
          <div class="">
            <table class="table table-striped table-hover table-bordered mass-book" id="items">
              <thead>
                <tr>
                  <th width="10%">Cloth Type</th>
				   <th width="10%">Fabric</th>
                  <th width="7%">Size</th>
                  <th width="10%">Condition</th>
                  <th width="10%">Service</th>
				  <th width="7%">Qty</th>
				  <th width="10%">Color</th>				 
				  <th width="7%">Brand</th>
				  <th width="7%">Price</th>
				  <th width="7%">Total</th>
				  <th width="7%">Tax</th>
				  <th width="10%">Note</th>
				  <th width="5%">Action</th>
                </tr>
              </thead>
              <tbody>
               <tr id="abc1">
                   <td class="hidden-480 form-group">
				  <?php 
					$cloth_type=$this->unit_class_model->all_laundry_cloth_type();					
				  ?>
				  <select class="form-control bs-select"   name="cloth_type[]" id="cloth_type"  placeholder="Cloth Type" >
				  <option value="0">Select</option>
				  <?php foreach($cloth_type as $cloth){ ?>
                  <option value="<?php echo $cloth->laundry_ct_id;?>"><?php echo $cloth->laundry_ct_name?></option>
				 
                  <?php } ?>
                </select>
				</td>
                 <td class="hidden-480 form-group">
				  <?php 
					$fabric_type=$this->unit_class_model->all_laundry_fabric_type();					
				  ?>
				  <select class="form-control bs-select"   name="fabric[]" id="fabric"  placeholder="Fabric Type" > 
				   <?php foreach($fabric_type as $fabric){ ?>
                  <option value="<?php echo $fabric->fabric_id ?>"><?php echo $fabric->fabric_type ?></option>
                  <?php } ?>
                </select>
				</td>
                  <td class="hidden-480 form-group">
				  <?php 
					$discount_rule=$this->unit_class_model->discount_rules();					
				  ?>
				  <select class="form-control bs-select"   name="size[]" id="size"  placeholder="Size" >
				  <option value="0"  selected="">Select Size</option>
                  <option value="kid">Kid</option>
                  <option value="Teen">Teen</option>
                  <option value="Adult">Adult</option>
                  <option value="N/A">N/A</option>
                </select>
				</td>
				
				<td class="hidden-480 form-group">
				<select class="form-control bs-select"   name="condition[]" id="condition"  placeholder="Condition" >
                  <option value="New">New</option>
                  <option value="Used">Used</option>
                  <option value="Old">Old</option>
                  
                  
                </select>
				</td>
				<td class="hidden-480 form-group">
				  <?php 
					$service_type=$this->unit_class_model->all_laundry_service_type();					
				  ?>
				  <select class="form-control bs-select"   name="service[]" onchange="get_price(this.value)" id="service"  placeholder="Laundry Service" > 
				   <option value="0"  selected="">Select Laundry Service</option>
				   <?php foreach($service_type as $service){ ?>
                  <option value="<?php echo $service->laundry_type_id ?>"><?php echo $service->laundry_type_name ?></option>
                  <?php } ?>
                </select>
				</td>
				<td class="hidden-480 form-group">
				 
				   <input autocomplete="off" type="text" class="form-control" name="qty[]" onblur="total_amt()"  id="qty"  placeholder="Qty" >
				</td>
				
				<td class="hidden-480 form-group">
				  <?php 
					$laundry_color=$this->unit_class_model->all_laundry_color();					
				  ?>
				  <select class="form-control bs-select"   name="color[]" id="color"  placeholder="Color" > 
				    <option value=""></option>
				   <?php foreach($laundry_color as $color){ ?>
                  <option value="<?php echo $color->color_id ?>"><?php echo $color->color_name ?></option>
                  <?php } ?>
                </select>
				</td>
				
				
				
				<td class="hidden-480 form-group">
				<input autocomplete="off" type="text" class="form-control" id="brand" name="brand[]"  placeholder="Brand">
				</td>
				
				
				<td class="hidden-480 form-group">
              <input autocomplete="off" type="text" class="form-control" id="price" name="price[]"   placeholder="Price">
				</td>
			<td class="hidden-480 form-group">
              <input autocomplete="off" type="text" class="form-control"  id="total" name="total[]"   placeholder="Total" readonly>
				</td>
				<td class="hidden-480 form-group">
				<select class="form-control bs-select"   name="tax[]" id="tax"  placeholder="Tax" >
                  <option value="Yes">Yes</option>
                  <option value="NO">NO</option>
                  
                  
                  
                </select>
				</td>
				<td class="hidden-480 form-group">
              <input autocomplete="off" type="text" class="form-control" id="note" name="note[]"   placeholder="Note">
				</td>
                  <td><a class="btn green btn-sm"  id="additems"><i class="fa fa-plus" aria-hidden="true"></i></a></td>
                </tr>
               
              </tbody>
            </table>
          </div>
        </div>
	  <div class="col-md-2" align="right" style="font-size: 120%;">
	  Total Bill Amount : <i class="fa fa-inr" style="font-size: 85%;"><span id="total_amount"></span></i>
      </div>
	 
	  <div class="col-md-2" align="right" style="font-size: 120%;">
	  <input autocomplete="off" type="text" class="form-control" onblur="g_total()" id="vat" name="vat"  placeholder="Vat"></i>
      </div>
	   <div class="col-md-2" align="right" style="font-size: 120%;">
	  <input autocomplete="off" type="text" class="form-control" id="service_tax" name="service_tax" onblur="g_total()"  placeholder="Service Tax"></i>
      </div>
	  <div class="col-md-2" align="right" style="font-size: 120%;">
	  <input autocomplete="off" type="text" class="form-control" id="service_charge" name="service_charge" onblur="g_total()" placeholder="Service Charge"></i>
      </div>
	  <div class="col-md-2" align="right" style="font-size: 120%;">
	  <input autocomplete="off" type="text" class="form-control"  id="discount" name="discount" onblur="g_total()"  placeholder="Discount"></i>
      </div>
	  <div class="col-md-2" align="right" style="font-size: 120%;">
	  <input autocomplete="off" type="text" class="form-control" id="grand_total" name="grand_total" placeholder="Grand Total" readonly></i>
	  <input type="hidden" id="hidden_grand_total" name="hidden_grand_total"   ></i>
      </div>
	  
	  
	  <div class="col-md-12" id="pay_id" style="text-align:center;margin-top: 20px; display:none" >
             <div class="btn-group">
                          <label for="autocellwidth" class="auto_cl btn grey-cascade" id="auto_cl_id" style="background-color:#39B9A1;">
                            <input type="checkbox" id="autocellwidth" class="toggle" style="display:none;">
							<span id="disp">Pay Now</span></label>
                        </div>
          </div>
        
		  
			<div id="pay" style="display:none;">
			
			<!----------------------->
			
    
			<!----------------------->
			
			
			
			
			
			
			
			<div class="portlet light bordered" id="scrollDiv">
      <div class="portlet-title">
        <div id="payment" class="caption font-green-haze"> <span class="caption-subject bold uppercase">Take Payment</span> </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body form-row-sepe">
          <div class="form-body">
            <div class="row">
             
              <div class="col-md-3">
                <div class="form-group">
                  <label>Total Amount Paybale<span class="required">*</span></label>
                  <input type="text" required class="form-control input-sm" id="total_amount_payable" name="card_number" value="" disabled="disabled"/>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Due Amount<span class="required">*</span></label>
                    <?php 
						/*$amountPaid = $this->dashboard_model->get_amountPaidByGroupID('0');
						if(isset($amountPaid) && $amountPaid) {
							$paid=$amountPaid->tm;
						 } else {
						  $paid=0;
						}*/
				    ?>
                  <input type="text" class="form-control input-sm" id="due_amount"  name="card_number" value="" disabled="disabled"/>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Amount <span class="required"> * </span> </label>
                  <input type="number" id="add_amount" class="form-control input-sm" name="card_number" placeholder="Enter Amount" onblur="check_amount();"  required="required" />
                  <input type="hidden" id="booking_id" value="">
                  <input type="hidden" id="booking_status_id" value="">
                </div>
                <script type="text/javascript">
                    function check_amount(){
						var amount = $("#total_amount_payable").val();
						alert(amount);
						var due_amount  = $("#due_amount").val();
						if(!due_amount){
							due_amount=0;
						}
						alert(due_amount);
						if(parseInt(amount) > parseInt(due_amount)) {
						//alert("The amount should be less than the due amount Rs: "+due_amount);
							swal({
								title: "Amount Should Be Smaller",
								text: "(The amount should be less than the due amount Rs: +due_amount)",
								type: "warning"
							},
							function(){
								//location.reload();
							});
							$("#add_amount").val("");
							return false;
						} else {
							return true;
						}
                    }       
              </script> 
              </div>
              <div class="col-md-3">
                <label>Profit Center <span class="required"> * </span> </label>
                <select name="" class="form-control input-sm" id="p_center">
                   <?php $pc=$this->dashboard_model->all_pc();?>
						  <?php
						  $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
						  ?>
						  <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
                           <?php $pc=$this->dashboard_model->all_pc1();
							foreach($pc as $prfit_center){
							?>
						  
						  <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
							<?php }?>
                </select>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Payment Mode <span class="required"> * </span> </label>
                  <select name="country" class="form-control input-sm" placeholder=" Booking Type" id="pay_mode" onChange="paym(this.value);" required="required">
                    <option value="" disabled selected>Select Payment Mode</option>
                    <?php 
						$mop = $this->dashboard_model->get_payment_mode_list();
						if($mop != false){
							foreach($mop as $mp){
				    ?>
				     <option value="<?php echo $mp->p_mode_name; ?>"><?php echo $mp->p_mode_des; ?></option>
			        <?php } } ?>
                  </select>
				  
                </div>
              </div>
			  
              <div id="cards" style="display:none;">
				
				<div class="col-md-3">
                  <div class="form-group">
                    <label>Card type <span class="required"> * </span> </label>
					 <select name="card_type" class="form-control input-sm" placeholder="Card type" id="card_type" required="required">
						<option value="" disabled selected>Select Card Type</option>
						<option value="Cr">Credit Card</option>
						<option value="Dr">Debit Card</option>
						<option value="gift">Gift Card</option>
					 </select>                  
                  </div>
                </div>
				
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Bank Name <span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="bankname" >
                  </div>
                </div>
				
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Card NO<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Card no" id="card_no" >
                  </div>
                </div>
				
				<div class="col-md-3">
                  <div class="form-group">
                    <label>Name on Card<span class="required"> </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Name on Card" id="card_name" >
                  </div>
                </div>
				
				<div class="col-md-3">
                	<div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Exp Month<span class="required"> </span> </label>
                            <select name="card_expm" class="form-control input-sm" placeholder="Month" id="card_expm">
                            <option value="" disabled selected>Select Month</option>
                            <?php 
                            $MonthArray = array(
                            "1" => "January", "2" => "February", "3" => "March", "4" => "April",
                            "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                            "9" => "September", "10" => "October", "11" => "November", "12" => "December",);
                            
                                for($i=1;$i<13;$i++)
                                {
                            ?>
                            <option value="<?php echo $MonthArray[$i]; ?>"><?php echo $MonthArray[$i]; ?></option>
                            <?php }  ?>
                          </select>
                          </div>
                        </div>				
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Exp year<span class="required"> </span> </label>
                            
                            <select name="card_expy" class="form-control input-sm" placeholder="Year" id="card_expy">
                            <option value="" disabled selected>Select year</option>
                            <?php 
                                for($i=2016;$i<2075;$i++)
                                {
                            ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php }  ?>
                          </select>
                          </div>
                        </div>
                	</div>
				</div>
				
				<div class="col-md-3">
                  <div class="form-group">
                    <label>CVV<span class="required"> </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="CVV" id="card_cvv" >
                  </div>
                </div>
				
				<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="card_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_ca" required="required">
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Transaction Declined</option>
					 </select>                  
                  </div>
                </div>
				
              </div>
			  
              <div id="fundss" style="display:none;">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Bank Name <span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="bankname_f" >
                  </div>
                </div>
				
                <div class="col-md-3">
                  <div class="form-group">
                    <label>A/C No<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Acc no" id="ac_no" >
                  </div>
                </div>
				
                <div class="col-md-3">
                  <div class="form-group">
                    <label>IFSC Code<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="IFSC code" id="ifsc" >
                  </div>
                </div>
				
				<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="card_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_f" required="required">
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Declined</option>
					 </select>                  
                  </div>
                </div>
				
              </div>
			  <div id="cheque" style="display:none;">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Bank Name <span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="bankname_c" >
                  </div>
                </div>
				
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Cheque No<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Cheque no" id="chq_no" >
                  </div>
                </div>
				
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Drawer Name<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Drawer Name" id="drw_name_c" >
                  </div>
                </div>
				
				<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="card_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_c" required="required">
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Bounced</option>
					 </select>                  
                  </div>
                </div>
				
              </div>
			  
			  <div id="draft" style="display:none;">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Bank Name <span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="bankname_d" >
                  </div>
                </div>
				
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Draft No<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Draft no" id="drf_no" >
                  </div>
                </div>
				
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Drawer Name<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Drawer Name" id="drw_name_d" >
                  </div>
                </div>
				
				<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="card_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_d" required="required">
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Bounced</option>
					 </select>                  
                  </div>
                </div>
				
              </div>
			  
			  <div id="ewallet" style="display:none;">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Wallet Name <span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Wallet Name" id="w_name" >
                  </div>
                </div>
				
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Transaction ID<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Transaction ID" id="tran_id" >
                  </div>
                </div>
				
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Recieving Acc<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Recieving Acc" id="recv_acc" >
                  </div>
                </div>
				
				<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="p_status_w" class="form-control input-sm" placeholder="Payment Status" id="p_status_w" required="required">
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Transaction Declined</option>
					 </select>                  
                  </div>
                </div>
				
			</div>
			  
            </div>
          </div>
          <div class="form-actions right"> 
            
            <button onclick=" return ajax_hotel_submit_grp();" class="btn btn-primary" id="add_submit">Submit</button>
          </div>
        </div>
      </div>
    </div>
			  <!--<div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="pay_amount" name="pay_amount" onkeypress=" return onlyNos(event, this);" placeholder="Amount" onchange="cal_amount(this.value)">
                <label></label>
                <span class="help-block">Amountdfdfb</span> </div>
            </div>-->
			
          <!--<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select"  id="mop" name="mop" onchange="modesofpayments();">
                <option value="" disabled selected>Mode Of Payment</option>
                <?php $mop = $this->dashboard_model->get_payment_mode_list();//print_r($mop);exit;
			  if($mop != false)
			  {
				  foreach($mop as $mp)
				  {
				?>
                <option value="<?php echo $mp->p_mode_name; ?>"><?php echo $mp->p_mode_des; ?></option>
                <?php }
			  } ?>
              </select>
              <label></label>
              <span class="help-block">Modes of Payments *</span> </div>
          </div>-->
        <!--  <div id="check" style='display:none'>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="checkno" placeholder="Check no. *"  onkeypress=" return onlyNos(event, this);">
                <label></label>
                <span class="help-block">Check no. *</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="check_bank_name" placeholder="Bank Name *">
                <label></label>
                <span class="help-block">Bank Name *</span> </div>
            </div>
          </div>-->
		  
		  <!-- <div id="cards" style='display:none'>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="card_no" placeholder="Card no. *"  onkeypress=" return onlyNos(event, this);">
                <label></label>
                <span class="help-block">Card no. *</span> </div>
            </div>
			<div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="card_name" placeholder="Card Name *"  >
                <label></label>
                <span class="help-block">Card Name. *</span> </div>
            </div>
			<div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="date" class="form-control" id="form_control_1" name="card_exp_date" placeholder="Exp Date. *"  onkeypress=" return onlyNos(event, this);">
                <label></label>
                <span class="help-block">Exp date. *</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="card_bank_name" placeholder="Bank Name *">
                <label></label>
                <span class="help-block">Bank Name *</span> </div>
            </div>
          </div>
		  
		  
		  
          <!--End div of Payment mode by check--> 
          <!--Start of payment mode by draft-->
          <!--<div id="draft" style='display:none'>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="draft_no" placeholder="Draft no *"  onkeypress=" return onlyNos(event, this);">
                <label></label>
                <span class="help-block">Draft no</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="draft_bank_name" placeholder="Bank Name *">
                <label></label>
                <span class="help-block">Bank Name *</span> </div>
            </div>
          </div>
          <!--End of payment mode by draft--> 
          <!--Start of fundtranfer under common div-->
         <!-- <div id="fundtransfer" style='display:none'>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_bank_name" placeholder="Bank Name">
                <label></label>
                <span class="help-block">Bank Name</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_account_no" placeholder="Account No *" maxlength="15" onkeypress=" return onlyNos(event, this);">
                <label></label>
                <span class="help-block">Account No *</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_ifsc_code" placeholder="IFSC Code">
                <label></label>
                <span class="help-block">IFSC Code</span> </div>
            </div>
          </div>
          <!--End of fundtranfer under common div-->
         <!-- <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_approved_by"  placeholder="Approved By *">
              <label></label>
              <span class="help-block">Approved By *</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_recievers_name"  placeholder="reciver Name  *">
              <label></label>
              <span class="help-block">reciver Name *</span> </div>
          </div>
          
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_paid_by" placeholder="Reciever Name">
              <label></label>
              <span class="help-block">Paid By</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_recievers_desig" placeholder="Reciever's Designation *">
              <label></label>
              <span class="help-block">Reciever's Designation *</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select" id="profit_center1" name="profit_center1">
                <option value="" selected="selected" disabled="disabled">Profit Center *</option>
                <?php $pc=$this->dashboard_model->all_pc();?>
                <?php
                      $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
                      ?>
                <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
                <?php $pc=$this->dashboard_model->all_pc1();
                        foreach($pc as $prfit_center){
                        ?>
                <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                <?php }?>
              </select>
              <label></label>
              <span class="help-block">Profit Center *</span> </div>
          </div>
		</div>-->
		
		<div id="pay1" style="display:block">
			
			 <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control date-picker"   id="payments_due_date" name="payments_due_date" placeholder="payments Due Date">
              <label></label>
              <span class="help-block">Payments Due Date</span> </div>
          </div>
			
			<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" readonly id="payments_due" onmouseover="check_due(this.value)" name="payments_due" placeholder="Payments Due">
			   <input autocomplete="off" type="hidden" class="form-control" readonly id="payments_due1" name="payments_due1" placeholder="Payments Due">
              <label></label>
              <span class="help-block">Payments Due</span> </div>
          </div>
		
		
		</div>
		</div>
	  
	  </div>
	  
      <div class="form-actions right">
        <button type="submit" class="btn blue" >Submit</button>
        <!-- 18.11.2015  -- onclick="return check_mobile();" -->
        <button  type="reset" class="btn default">Reset</button>
      </div>
	  
	  <input type="hidden" name="hid">
      <?php form_close(); ?>
      <!-- END CONTENT --> 
    
  </div>
</div>
<div class="modal fade" id="myModalNorm1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content"> 
      
      <!-- Modal Header -->
      
      <div class="modal-header" style="background:#95A5A6; color:#ffffff;"> Search Guest
        <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true">&times;</span> <span class="sr-only">Close</span> </button>
      </div>
      
      <!-- Modal Body -->
      <div class="modal-body">
        <div class="form-group">
          <div class="input-group">
            <input type="email" class="form-control" id="guest_search" placeholder="Search Guest"/>
            <span class="input-group-btn">
            <button type="button" class="btn green" onclick="return_guest_search()">Search</button>
            </span> </div>
        </div>
        <div id="return_guest" style="overflow-y:scroll; height:300px; display:none;"> </div>
      </div>
      
      <!-- Modal Footer -->
      <div class="modal-footer">
        <button type="button" class="btn red" data-dismiss="modal"> Close </button>
      </div>
    </div>
  </div>
</div>

<script>
    function paym(val){
        if(val == 'cards'){ 
            document.getElementById('cards').style.display='block';         
        } else {
			document.getElementById('cards').style.display='none';
        }
        if(val=='fund') {
            document.getElementById('fundss').style.display='block';  
        } else {
			document.getElementById('fundss').style.display='none';
        }
        if(val=='cheque'){
            document.getElementById('cheque').style.display='block';  
        } else {
			document.getElementById('cheque').style.display='none';
        } 
		if(val=='draft'){
            document.getElementById('draft').style.display='block';  
        } else {
			document.getElementById('draft').style.display='none';
        }
		if(val=='ewallet'){
            document.getElementById('ewallet').style.display='block';  
        } else {
			document.getElementById('ewallet').style.display='none';
        }
		if(val=='cash'){
            document.getElementById('cardss').style.display='none';
            document.getElementById('fundss').style.display='none';
			document.getElementById('ewallet').style.display='none';
			document.getElementById('cheque').style.display='none';
			document.getElementById('draft').style.display='none';
        }		
    }

function billing(a){
	//alert(a);
	if(a=="Bill Seperately"){
		$('#pay_id').show();
	}
	else{
		$('#pay_id').hide();
	}
	
}
var quantity=0;
function cal_amount(a){
	//$('#pay_amount').val()
	if(a==''){
		a=0;
	}
	var b=$('#hidden_grand_total').val();
	a=parseFloat(a);
	b=parseFloat(b);
	a=b-a;
	$('#payments_due').val(a);
}
function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	  // alert(y);
	    
	   if(y=="checks"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="cards"){
		   document.getElementById("cards").style.display="block";
	   }else{
		   document.getElementById("cards").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fund"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }
 $(document).ready(function() {
                        cellWidth = 100;
                        cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                       
                    });

            $("#autocellwidth").click(function() {
						
						
                      cellWidth = 100;  // reset for "Fixed" mode
                        cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                      
					document.getElementById('auto_cl_id').style.backgroundColor = $(this).is(":checked") ? "#297CAC" : "#39B9A1";
                        var a = $(this).is(":checked") ? "Pay Later" : "Pay Now";
						if(a == 'Pay Later')
						{	
							document.getElementById('pay').style.display = 'block';
	                        document.getElementById('pay1').style.display = 'block';
						}
						else
						{	
							
						    document.getElementById('pay').style.display = 'none';
	                        document.getElementById('pay1').style.display = 'block';
						}
						
						$('#disp').text(a);

					});
					
					

function g_total(){
		
	var g=$('#hidden_grand_total').val();	
	g=parseFloat(g);
	//v=parseFloat(v);
	
	var discount=$('#discount').val();	
	var vat=$('#vat').val();	
	var service_tax=$('#service_tax').val();	
	var service_charge=$('#service_charge').val();
	
	/*vat=parseFloat(vat);
	service_tax=parseFloat(service_tax);
	service_charge=parseFloat(service_charge);
	*/
	if(vat == '' ){
		vat=0;
		
	}else{	
	vat=parseFloat(vat);
	vat =(parseFloat(g) * vat)/100;
	}
	
	if(service_tax == '' ){
		service_tax=0;
		
	}else{	
	service_tax=parseFloat(service_tax);
	service_tax =(parseFloat(g) * service_tax)/100;
	}
	
	if(service_charge == '' ){
		service_charge=0;
		
	}else{	
	service_charge=parseFloat(service_charge);
	service_charge =(parseFloat(g) * service_charge)/100;
	}
	//service_tax =(g * service_tax)/100;
	//service_charge =(g * service_charge)/100;
	
	grand_total=g+vat+service_tax+service_charge;
	
	
	if(discount == '' ){
		discount=0;
		
	}else{	
	discount=parseFloat(discount);
	grand_total =(grand_total- discount);
	}
	$('#grand_total').val(grand_total);
	$('#payments_due').val(grand_total);
}


var flag=0,flag1=0;sum=0;
function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});

/*function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	   alert(y);
	    
	   if(y=="check"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fund"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }*/
   var ff=flag;
  var check=$('#cloth_type'+ff).val();
  var a=$('#cloth_type').val();
  //if(check<=a){
  //var color=0;
  
   $("#additems").click(function(){
	   var cloth_type=$('#cloth_type').val();
	   var size=$('#size').val();
	   var condition=$('#condition').val();
	   var service=$('#service').val();
	   var qty=$('#qty').val();
	   var color=$('#color').val();
	   var fabric=$('#fabric').val();
	   var brand=$('#brand').val();
	   var price=$('#price').val();
	   var total=$('#total').val();
	   var tax=$('#tax').val();
	   var note=$('#note').val();
	  
	 //alert(total);
	   if(cloth_type>0 && service>0 && fabric>0 ){
	var x=0;
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>unit_class_controller/get_laundrydata_by_id",
		dataType: 'json',
		data: {cloth_type:cloth_type,fabric:fabric,service:service,color:color},
		success: function(data){
			//alert(data.name);
			//$('#cloth_type'+flag).val(data.name);
	//$('#ids').val( it+','+ i_name) ;
	$('#items tr:first').after('<tr id="row_'+flag+'">'+
	'<td class="hidden-480"><input name="cloth_type[]" id="cloth_type'+flag+'" type="text" value="'+data.name+'" class="form-control input-sm" ></td>'+
	'<td class="hidden-480"><input name="fabric[]" id="fabric'+flag+'" type="text" value="'+data.fabric+'" class="form-control input-sm" ></td>'+	
	'<td class="hidden-480"><input name="size[]" id="fabric'+flag+'" type="text" value="'+size+'" class="form-control input-sm" ></td>'+	
	'<td class="hidden-480"><input name="condition[]" id="condition'+flag+'" type="text" value="'+condition+'" class="form-control input-sm" ></td>'+
	'<td class="hidden-480"><input name="service[]" id="service'+flag+'" type="text" value="'+data.service+'" class="form-control input-sm" ></td>'+
	'<td class="hidden-480"><input name="qty[]" id="qty'+flag+'" type="text" value="'+qty+'" class="form-control input-sm" ></td>'+
	'<td class="hidden-480"><input name="color[]" id="color'+flag+'" type="text" value="'+data.color+'" class="form-control input-sm" ></td>'+
	'<input type="hidden" name="h_color[]" value="'+color+'"><input type="hidden" name="h_cloth_type[]" value="'+cloth_type+'"><input type="hidden" name="h_fabric[]" value="'+fabric+'"><input type="hidden" name="h_service[]" value="'+service+'">'+
	'<td class="hidden-480"><input name="brand[]" id="price'+flag+'" type="text" value="'+brand+'" class="form-control input-sm" ></td>'+
	'<td class="hidden-480"><input name="price[]" id="note'+flag+'" type="text" value="'+price+'" class="form-control input-sm" ></td>'+
	'<td class="hidden-480"><input name="total[]" id="note'+flag+'" type="text" value="'+total+'" class="form-control input-sm" ></td>'+
	'<td class="hidden-480"><input name="tax[]" id="note'+flag+'" type="text" value="'+tax+'" class="form-control input-sm" ></td>'+
	'<td class="hidden-480"><input name="note[]" id="note'+flag+'" type="text" value="'+note+'" class="form-control input-sm" ></td>'+
	'<td><table><tr><td><a  class="btn red btn-sm" onclick="removeRow('+flag+','+total+')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>'+
	'<td><a  class="btn red btn-sm" onclick="editRow('+flag+')" ><i class="fa fa-edit" aria-hidden="true"></i></a></td></tr></table></td></tr>');	
	
	flag++;
	quantity+=parseFloat(qty);
	   $('#quantity').val(quantity);
	}});   }
	   else{
		   alert("Enter data");
	   }
		if(cloth_type>0 && service>0 && fabric>0){
		var total=parseFloat(price)*parseFloat(qty);
	    sum+=parseFloat(total);
	//alert(sum);

	  $('#total_amount').text(sum);
	  $('#grand_total').val(sum);
	  $('#total_amount_payable').val(sum);
	  $('#hidden_grand_total').val(sum);
	  
	  var a=$('#hidden_grand_total').val();
						$('#payments_due').val(a);
	   
	//   alert('here');
	   //alert(sum);
	   
	   $('#cloth_type').val('');
	   $('#size').val('');
	   $('#condition').val('');
	   $('#service').val('');
	   $('#qty').val('');
	   $('#color').val('');
	   $('#fabric').val('');
	   $('#brand').val('');
	   $('#price').val('');
	   $('#note').val('');
	   $('#total').val('');
		}
	
		});
		
		
		
function check_date(){
	
	var a=$('#contract_start_date').val();
	 a=new Date(a);
	   var b=$('#contract_end_date').val();
	   b=new Date(b);
	   if(a>=b && b!='' ){
		   alert('Date Should Getter Than Start Date ');
		   $('#contract_end_date').val('');
	   }
	  
	  var c=$('#contract_end_date'+(flag-1)).val();
	 // alert(c);
	    c=new Date(c);
		if(c>a){
			
			alert('Please Enter Valid Date');
			$('#contract_start_date').val('');
			$('#contract_end_date').val('');
		}
		
	  
	   
}



function fetch_all_address()
{
	
	var pin_code = document.getElementById('pin_code').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("country").focus();
			$('#country').val(data.country);
			document.getElementById("state").focus();
			$('#state').val(data.state);
			document.getElementById("city").focus();
			$('#city').val(data.city);
		}

	});
	
	
	
	
}
	   $("#addGuest").click(function(){
		$('#myModalNorm1').modal('show');
		$('#return_guest').html("");
		$("#return_guest").css("display", "none");
	});
	   
 function return_guest_search(){		
        var guest_name = $('#guest_search').val();
        //alert(guest_name);
        jQuery.ajax({
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_search",
                datatype:'json',
                data:{guest:guest_name},
                success:function(data)
                {
                    var resultHtml = '';
                    resultHtml+='<table class="table table-bordered" cellpadding="0" cellspacing="0" ><thead><tr><th>Name</th><th>Address</th><th width="100">Mobile No</th></tr></thead><tbody>';
                    $.each(data, function(key,value){
                        resultHtml+='<tr  class="crsr collapsed" data-toggle="collapse" data-target="#toggleDemo'+value.g_id+'">';
                        resultHtml+='<td>'+ value.g_name +'</td>';
                        resultHtml+='<td>'+ value.g_address +'</td>';
                        resultHtml+='<td>'+ value.g_contact_no +'</td>';
                        resultHtml+='</tr><tr id="toggleDemo'+value.g_id+'"  class="mrgn">';
                        resultHtml+='<td class="no-marin"  colspan="3"  cellpadding="0" cellspacing="0">';
                        resultHtml += '<div class="side clearfix" style="width:100%;"><div class="col-xs-7 ex"><p><label style="font-size:13px;">' + value.g_name + '</label><br><label>';

                        resultHtml+='<input type="hidden" id="g_id_hide"  value="'+value.g_id+'" ></input>';
                        resultHtml+='<label style="font-size:13px;">Room No: '+value.room_no+'</label><br><label style="font-size:13px;">Last Check In Date: '+value.cust_from_date+'</label><br> <button class="act active btn blue btn-sm" name="g_id" id="g_id"" value="'+value.g_id+'" onclick="get_guest('+value.g_id+')" >Continue</button> </p></div>';
                        if(value.g_photo_thumb!='') {
                            resultHtml += '<div class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/' + value.g_photo_thumb + '" width="100px" height="92px"  > ' +
                            '</div>';
                        }
                        else{
                            resultHtml += '<div  class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/no_images.png" width="100px" height="92px" > ' +
                            '</div>';
                        }
                        //resultHtml+='<td><input type="radio" name="g_id" id="'+value.g_id+'" value="'+value.g_id+'" onclick="get_guest(this.value)" /></td>';
                        resultHtml+='</div></td></tr>';
                    });
                    resultHtml+='</tbody></table>';
                    $('#return_guest').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
			$("#return_guest").css("display", "block");
        return false;
    }
	
	
	
</script> 


<script>
function get_guest_by_id(id){
	//alert(id);
	
	
	 jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_get",
                datatype:'json',
                data:{guest_id:id},
                success:function(data)
                {
					if(id){
                    $('#tab11').hide();
                    $('#tab12').show();
                    $('#id_guest2').val(id);
					//alert(data.booking_id);
					//alert(data.g_name);
                    $('#Guest_name').val(data.g_name);
					$('#guest_type1').val(data.g_type).change();
					$('#hidden_id').val(id);
				}
                    
					
                }
            });
	
}


  function get_guest(id)
    {
        //var g_id = $('#g_id_hide').val();
        var g_id=id;
       // alert(g_id);
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_get",
                datatype:'json',
                data:{guest_id:g_id},
                success:function(data)
                { 
					
                  
				
                    $('#tab11').hide();
                    $('#tab12').show();
                    $('#id_guest2').val(id);
					
                    $('#Guest_name').val(data.g_name);
					$('#guest_type1').val(data.g_type).change();
					
                    $('#hidden_id').val(data.g_id);
                   /* $('#cust_contact_no').val(data.g_contact_no);
					$('#cust_mail').val(data.g_email);
					*/
					$('#myModalNorm1').modal('hide');
					
                }
            });
        return false;
    }


function get_price(id){
	var cloth=$('#cloth_type').val();
	//var service=$('#service').val();
	var fabric=$('#fabric').val();
	
	 jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>unit_class_controller/get_price_by_clothID_fabricId",
                datatype:'json',
                data:{cloth:cloth,fab:fabric,service:id},
                success:function(data)
                {  
				//alert(data.dry);
				if(service=='1')
					$('#price').val(data.dry);
				else if(service=='2')
					$('#price').val(data.laundry);
				else
					$('#price').val(data.iron);
					
				/*alert(data.id);
                  alert(data.laundry);
                  alert(data.dry);
                  alert(data.iron);*/
                  //alert(data.id);
				
                    //$('#tab11').hide();
                    
					
                }
            });
	
	
	
}

function booking_type1(id){
	//alert('1');
if(id==1){
$("#single_booking1").show();
$("#group_booking1").hide();
$("#Guest_name").prop( "disabled", true );

}
else if(id==2){
$("#group_booking1").show();
$("#single_booking1").hide();
$("#Guest_name").prop( "disabled", true );
}else{
$("#Guest_name").prop( "disabled", false );	
$("#group_booking1").hide();
$("#single_booking1").hide();
}
}


function removeRow(a,b){		
		var c=$('#total_amount').text();
		b=parseFloat(c)-b;
		$('#total_amount').text(b);
		$('#row_'+a).remove();
		
	}
	function editRow(a){		
		alert(a);
		$('#row_'+a).hide();
		//$('#line').show(); return "<html>" + $("html").html() + "</html>";
		$('#row_'+a).after('<tr id="row_'+a+'">'+
	    //'<td class="hidden-480"><input name="cloth_type[]" id="cloth_type'+a+'" type="text" value="'+a+'" class="form-control input-sm" ></td></tr></table>');
		//$("html").html() + "</html>");
		$('#abc1').html());
		
	}
		function get_guest_by_booking_id(id){
			//alert(id);
			var a=$('#booking_type').val();//single=2 group3
			//alert(a);
			jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>unit_class_controller/get_guest_by_booking_id",
                datatype:'json',
                data:{booking_id:id,booking_type:a},
                success:function(data)
                { 
					/*alert(data.id);
					alert(data.name);					
					alert(data.type);*/
					$('#Guest_name').val(data.name);
					$('#guest_type1').val(data.type).change();
					$('#guest_booking_id').val(id);
				
                }
            });
			
		}
		</script>
<script>
function total_amt(){
var pr=parseFloat($('#price').val());
	var qty=parseFloat($('#qty').val());
pr=pr*qty;

$('#total').val(pr);
}

</script>