<!-- BEGIN PAGE CONTENT-->
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
<!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>
<div class="form-group">
<div class="col-md-12 control-label">
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
</div>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="form-group">
<div class="col-md-12 control-label">
  <div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
</div>
</div>
<?php endif;?>
<!-- 17.11.2015-->
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Lost Item</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/add_lost_item',$form);

                        ?>
    <div class="form-body"> 
      <div class="row">
      	<div class="col-md-4">
        <div  class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control date-picker" id="form_control_11" name="reporting_date" required="required" placeholder="Reporting Date *">
          <label></label>
          <span class="help-block">Reporting Date *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="booking_id" placeholder="Booking Id">
          <label></label>
          <span class="help-block">Booking Id</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <select class="form-control bs-select"  name="type" required >
            <option value="">Select Type</option>
            <option value="Jewelery">Jewelery</option>
            <option value="Electronics">Electronics</option>
            <option value="Id Card">Id Card</option>
            <option value="Bag">Bag</option>
            <option value="Others">Others</option>
          </select>
          <label></label>
          <span class="help-block">Type *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="item_title" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Item Title">
          <label></label>
          <span class="help-block">Item Title</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="item_des" placeholder="Item Description">
          <label></label>
          <span class="help-block">Item Description</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <select class="form-control bs-select"  name="condition" required >
            <option value="">Select Condition</option>
            <option value="New">New</option>
            <option value="Used">Used</option>
            <option value="Id Card">Damaged</option>
            <option value="Bag">N/A</option>
          </select>
          <label></label>
          <span class="help-block">Condition *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="lost_in" placeholder="Lost In">
          <label></label>
          <span class="help-block">Lost In</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" type="text" class="form-control date-picker" id="form_control_12" name="lost_date" placeholder="Lost Date *">
          <label></label>
          <span class="help-block">Lost Date *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" type="text" class="form-control timepicker timepicker-default" id="form_control_1" name="lost_time" placeholder="Lost Time *">
          <label></label>
          <span class="help-block">Lost Time *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="lost_by" placeholder="lost By *">
          <label></label>
          <span class="help-block">lost By *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="mobile" name="g_contact_no"  required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" placeholder="Mobile No. *">
          <label></label>
          <span class="help-block">Mobile No. *</span> </div>
        </div>
       <!-- <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <select  class="form-control bs-select"  name="admin_name" required="required">
            <option value="" selected="selected" disabled="disabled">Select Admin</option>
            <?php ///if(isset($admin)&& $admin){

                                      //  foreach ($admin as $value) {
                                            # code...
                                    ?>
            <option value="<?php //echo $value->admin_first_name." ".$value->admin_middle_name." ".$value->admin_last_name ?>"><?php //echo $value->admin_first_name." ".$value->admin_middle_name." ".$value->admin_last_name?></option>
            <?php
                                 //   }}
                                ?>
          </select>
          <label></label>
          <span class="help-block">Admin Name *</span> </div>
        </div>-->
      </div>
      </div>
      <div class="form-actions right">
        <button type="submit" class="btn submit" >Submit</button>
        <!-- 18.11.2015  -- onclick="return check_mobile();" -->
        <button  type="reset" class="btn default">Reset</button>
      </div>
      <?php form_close(); ?>
      <!-- END CONTENT --> 
    </div>
  </div>
<script>
   function check_corporate(value){

       if(value =="corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
       }else{
           document.getElementById("c_name").style.display="none";
           document.getElementById("c_des").style.display="none";
       }
   }
    function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }

</script> 
