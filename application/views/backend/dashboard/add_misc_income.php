<script>
function calculate() {
	var amount = $('#amount').val();
	var tax = $('#tax').val();
	var tprice = parseFloat(amount) * (parseFloat(tax)/100);
	var price=parseFloat(amount)+tprice;
	$('#total').val(price);
	$('#total').addClass('edited');
}
function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
<!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>

<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<!-- 17.11.2015-->
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Miscellenious Income</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/add_misc_income',$form);

                        ?>
    <div class="form-body">
      <div class="row">
        <div class="col-md-3">
          <div  class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control date-picker" id="form_control_1" name="date" required="required" placeholder="Date *">
            <label></label>
            <span class="help-block">Date *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="recived_from" placeholder="Recived From *">
            <label></label>
            <span class="help-block">Recived From *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="item"  required="required" placeholder="Income For *">
            <label></label>
            <span class="help-block">Income For *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="note" placeholder="Note *">
            <label></label>
            <span class="help-block">Note *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="recived_by" placeholder="Recived By">
            <label></label>
            <span class="help-block">Recived By *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input onclick="total()" autocomplete="off" type="text" class="form-control" id="amount" name="amount" placeholder="Amount *" onkeypress=" return onlyNos(event, this);">
            <label></label>
            <span class="help-block">Amount *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control" id="tax" name="tax" onblur="calculate()" placeholder="Tax %" onkeypress=" return onlyNos(event, this);" required>
            <label></label>
            <span class="help-block">Tax %</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="total" name="total"  required="required"  onkeypress=" return onlyNos(event, this);" placeholder="Total *">
            <label></label>
            <span class="help-block">Total *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select  class="form-control bs-select"  name="admin_name" required="required">
              <option value="" selected="selected" disabled="disabled">Select Admin</option>
              <?php if(isset($admin)&& $admin){

                                        foreach ($admin as $value) {
                                            # code...
                                    ?>
              <option value="<?php echo $value->admin_first_name." ".$value->admin_middle_name." ".$value->admin_last_name ?>"><?php echo $value->admin_first_name." ".$value->admin_middle_name." ".$value->admin_last_name?></option>
              <?php
                                    }}
                                ?>
            </select>
            <label></label>
            <span class="help-block">Admin Name</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" id="profit_center" name="profit_center" required="required" >
              <option value="" selected="selected" disabled="disabled">Profit Center</option>
              <?php $pc=$this->dashboard_model->all_pc();?>
              <?php
                      $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
                      ?>
              <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
              <?php $pc=$this->dashboard_model->all_pc1();
                        foreach($pc as $prfit_center){
                        ?>
              <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
              <?php }?>
            </select>
            <label></label>
            <span class="help-block">Profit Center *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select  class="form-control bs-select" placeholder=" Booking Type" id="pay_mode" name="pay_mode" onChange="paym(this.value);" >
              <option value="" disabled selected>Select Payment Mode</option>
              <?php 
						$mop = $this->dashboard_model->get_payment_mode_list();
						if($mop != false){
							foreach($mop as $mp){
				    ?>
              <option value="<?php echo $mp->p_mode_name; ?>"><?php echo $mp->p_mode_des; ?></option>
              <?php } } ?>
            </select>
            <label></label>
            <span class="help-block">Payment Mode *</span> </div>
        </div>
        <div id="cards" style="display:none;">
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <select name="card_type" class="form-control bs-select" placeholder="Card type" id="card_type">
                <option value="" disabled selected>Select Card Type</option>
                <option value="Cr">Credit Card</option>
                <option value="Dr">Debit Card</option>
                <option value="gift">Gift Card</option>
              </select>
              <label></label>
              <span class="help-block">Card Type *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" name="card_bank_name" class="form-control" placeholder="Bank name" id="bankname" >
              <label></label>
              <span class="help-block">Bank Name *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" name="card_no" class="form-control" placeholder="Card no" id="card_no" >
              <label></label>
              <span class="help-block">Card NO *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="Name on Card" name="card_name" id="card_name" >
              <label></label>
              <span class="help-block">Name on Card *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group form-md-line-input">
                  <select name="card_expm" class="form-control bs-select" placeholder="Month" id="card_expm">
                    <option value="" disabled selected>Select Month</option>
                    <?php 
                            $MonthArray = array(
                            "1" => "January", "2" => "February", "3" => "March", "4" => "April",
                            "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                            "9" => "September", "10" => "October", "11" => "November", "12" => "December",);
                            
                                for($i=1;$i<13;$i++)
                                {
                            ?>
                    <option value="<?php echo $MonthArray[$i]; ?>"><?php echo $MonthArray[$i]; ?></option>
                    <?php }  ?>
                  </select>
                  <label></label>
                  <span class="help-block">Exp Month *</span> </div>
              </div>
              <div class="col-md-6">
                <div class="form-group form-md-line-input">
                  <select name="card_expy" class="form-control bs-select" placeholder="Year" id="card_expy">
                    <option value="" disabled selected>Select year</option>
                    <?php 
                                for($i=2016;$i<2075;$i++)
                                {
                            ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php }  ?>
                  </select>
                  <label></label>
                  <span class="help-block">Exp year *</span> </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="CVV" name="card_cvv" id="card_cvv" >
              <label></label>
              <span class="help-block">CVV *</span> </div>
          </div>
          
          <!--<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="payment_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_ca">
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Transaction Declined</option>
					 </select>                  
                  </div>
                </div>--> 
          
        </div>
        <div id="fundss" style="display:none;">
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="Bank name" name="ft_bank_name" id="bankname_f" >
              <label></label>
              <span class="help-block">Bank Name *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="Acc no" name="ft_account_no" id="ac_no" >
              <label></label>
              <span class="help-block">A/C No *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="IFSC code" name="ft_ifsc_code" id="ifsc" >
              <label></label>
              <span class="help-block">IFSC Code *</span> </div>
          </div>
          
          <!--<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="payment_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_f">
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Declined</option>
					 </select>                  
                  </div>
                </div>--> 
          
        </div>
        <div id="cheque" style="display:none;">
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="Bank name" name="chk_bank_name" id="bankname_c" >
              <label></label>
              <span class="help-block">Bank Name *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="Cheque no" name="checkno" id="chq_no" >
              <label></label>
              <span class="help-block">Cheque No *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="Drawer Name" name="chk_drawer_name" id="drw_name_c" >
              <label></label>
              <span class="help-block">Drawer Name *</span> </div>
          </div>
          
          <!--<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="payment_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_c" >
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Bounced</option>
					 </select>                  
                  </div>
                </div>--> 
          
        </div>
        <div id="draft" style="display:none;">
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="Bank name" name="draft_bank_name" id="bankname_d" >
              <label></label>
              <span class="help-block">Bank Name *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="Draft no" name="draft_no" id="drf_no" >
              <label></label>
              <span class="help-block">Draft No *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="Drawer Name" name="draft_drawer_name" id="drw_name_d" >
              <label></label>
              <span class="help-block">Drawer Name *</span> </div>
          </div>
          
          <!--<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="payment_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_d" >
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Bounced</option>
					 </select>                  
                  </div>
                </div>--> 
          
        </div>
        <div id="ewallet" style="display:none;">
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="Wallet Name" name="wallet_name" id="w_name" >
              <label></label>
              <span class="help-block">Wallet Name *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="Transaction ID" name="wallet_tr_id" id="tran_id" >
              <label></label>
              <span class="help-block">Transaction ID *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" placeholder="Recieving Acc" name="wallet_rec_acc" id="recv_acc" >
              <label></label>
              <span class="help-block">Recieving Acc *</span> </div>
          </div>
          <!--<div class="col-md-3">
            <div class="form-group form-md-line-input">
              <select name="p_status_w" class="form-control bs-select" placeholder="Payment Status" id="p_status_w" >
                <option value="Done" selected>Payment Recieved</option>
                <option value="Pending">Payment Processing</option>
                <option value="Cancel">Transaction Declined</option>
              </select>
              <label></label>
              <span class="help-block">Payment Status *</span> </div>
          </div>-->
        </div>
        <div class="col-md-3" id="p_status" style="display:block;">
          <div class="form-group form-md-line-input">
            <select name="p_status_w" class="form-control bs-select" placeholder="Payment Status" id="p_status_w" >
              <option value="Done" selected>Payment Recieved</option>
              <option value="Pending">Payment Processing</option>
              <option value="Cancel">Transaction Declined</option>
            </select>
            <label></label>
            <span class="help-block">Payment Status *</span> </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" onclick="total()" class="btn submit" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>
<script type="text/javascript">
	 
	
	function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	   //alert(y);
	    
	   if(y=="check"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fund"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }
	
	
	
 function paym(val){
	// alert(val);
        if(val == 'cards'){ 
            document.getElementById('cards').style.display='block';  
document.getElementById('p_status').style.display='block';			
        } else {
			document.getElementById('cards').style.display='none';
        }
        if(val=='fund') {
            document.getElementById('fundss').style.display='block'; 
document.getElementById('p_status').style.display='block';			
        } else {
			document.getElementById('fundss').style.display='none';
        }
        if(val=='cheque'){
            document.getElementById('cheque').style.display='block';
document.getElementById('p_status').style.display='block';			
        } else {
			document.getElementById('cheque').style.display='none';
        } 
		if(val=='draft'){
            document.getElementById('draft').style.display='block'; 
			document.getElementById('p_status').style.display='block';			
        } else {
			document.getElementById('draft').style.display='none';
        }
		if(val=='ewallet'){
            document.getElementById('ewallet').style.display='block';  
			document.getElementById('p_status').style.display='block';
        } else {
			document.getElementById('ewallet').style.display='none';
        }
		if(val=='cash'){
		//	alert("jygjyg");
				document.getElementById('p_status').style.display='none';
            document.getElementById('cardss').style.display='none';
            document.getElementById('fundss').style.display='none';
			document.getElementById('ewallet').style.display='none';
			document.getElementById('cheque').style.display='none';
			document.getElementById('draft').style.display='none';
		
        }		
    }
</script> 
