  
  <script src="<?php echo base_url();?>assets/global/plugins/typeahead1/jquery.typeahead.js"></script>
  <?php if($this->session->flashdata('err_msg')):?>
  <div class="form-group">
    <div class="col-md-12 control-label">
      <div class="alert alert-danger alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
    </div>
  </div>
  <?php endif;?>
  <?php if($this->session->flashdata('succ_msg')):?>
  <div class="form-group">
    <div class="col-md-12 control-label">
      <div class="alert alert-success alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
    </div>
  </div>
  <?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Purchase</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

            $form = array(
                'class' 			=> '',
                'id'				=> 'form',
                'method'			=> 'post'
            );

            echo form_open_multipart('dashboard/add_purchase',$form);

            ?>
    <div class="form-body">
      <div class="row">
    <?php
        ?>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" name="p_id" value="<?php echo $id+1;?>" required="required" readonly placeholder="Purchase Id">
          <label></label>
          <span class="help-block">Purchase Id *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">          
              <select name="a_id" id="a_id" class="form-control bs-select" tabindex="0">
                <option value="0" disabled selected>Not related to assests</option>
                <?php $assets=$this->dashboard_model->all_assets_limit();
                                if($assets && isset($assets)){
                                foreach($assets as $asset){
                                ?>
                <option value="<?php echo $asset->a_id ?>"> <?php echo $asset->a_name ?></option>
                <?php }} ?>
              </select>
              <label></label>
              <span class="help-block">Select an Assets</span>
            </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input" onclick="getfocus()">
          <input type="text" autocomplete="off" required="required" name="p_date" class="form-control date-picker "  id="c_valid_from" data-required="required" tabindex="1" placeholder="Purchase Date *">
          <label></label>
          <span class="help-block">Purchase Date *</span> </div>
        </div>
        <div class="col-md-4">
       <div class="form-group form-md-line-input">          
				  <div class="typeahead__container">
           <div class="typeahead__field">
			<span class="typeahead__query">
     <input class="js-typeahead-user_v1 form-control" name="p_supplier[query]" id="p_supplier" value="" type="search" placeholder="Search" autocomplete="off">
		<input autocomplete="off" type="hidden" id="vendor_id" name="vendor_id" class="form-control" name="note" placeholder="Note *">	
		   </span>
          
        </div>
    </div>
				</div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" name="p_i_name" required="required" tabindex="3" placeholder="Item Name *">
          <label></label>
          <span class="help-block">Item Name *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" name="p_i_description" tabindex="4" placeholder="Item Description">
          <label></label>
          <span class="help-block">Item Description</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="p_i_price" name="p_i_price" 
          required="required" tabindex="5" placeholder="Item Price *">
          <label></label>
          <span class="help-block">Item Price *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" onblur="calculate_price(this.value)" class="form-control" name="p_i_quantity" tabindex="6" required="required" placeholder="Item Quantity *">
          <label></label>
          <span class="help-block">Item Quantity *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <!--<input type="text" autocomplete="off" class="form-control" name="p_unit" required="required" tabindex="7" placeholder="Item Unit *">
          <label></label>-->
		  <select class="form-control bs-select" id="p_unit" name="p_unit" required="required" tabindex="7">
         	<option value="" selected="selected" disabled="disabled">Select an Unit</option>
         	<option value="Kg" >Kg</option>
         	<option value="Gm" >Gm</option>
         	<option value="Lt">Lt</option>
         	<option value="Pc">Pc</option>
         	<option value="Meter">Meter</option>
         	<option value="Foot">Foot</option>
         	<option value="Bag" >Bag</option>
         	<option value="Dozen" >Dozen</option>
         	<option value="Sq.M.M" >Sq.M.M</option>
            
          </select>
          <span class="help-block">Item Unit *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="p_price" name="p_price" required="required"  tabindex="8" placeholder="Item Price *">
          <label></label>
          <span class="help-block">Item Price *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input onblur="final_price_1(this.value)" type="text" autocomplete="off" class="form-control" name="p_tax" required="required" tabindex="9" placeholder="Tax(%) *">
          <label></label>
          <span class="help-block">Tax(%) *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input onblur="final_price_2(this.value)" type="text" autocomplete="off" class="form-control" name="p_discount" tabindex="10" placeholder="Discount(%) *">
          <label></label>
          <span class="help-block">Discount(%) *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="p_total_price" name="p_total_price" required="required" tabindex="11" placeholder="Item Total Price *">
          <input type="hidden" id="p_total_price2" value="">
          <label></label>
          <span class="help-block">Item Total Price *</span> </div>
        </div>
        <div class="col-md-4">
           <div class="form-group form-md-line-input">
         <select class="form-control bs-select" id="profit_center" name="profit_center" required="required" tabindex="12">
         	<option value="" selected="selected" disabled="disabled">Profit Center</option>
            <?php $pc=$this->dashboard_model->all_pc();?>
                      <?php
                      $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
                      ?>
                      <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
                       <?php $pc=$this->dashboard_model->all_pc1();
                        foreach($pc as $prfit_center){
                        ?>
                      
                      <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                        <?php }?>
          </select>
          <label></label>
          <span class="help-block">Profit Center</span> </div>
          </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn submit" >Submit</button>
      <button type="submit" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
  </div>
</div>
<div class="clearfix">
  <?php  $this->session->flashdata('succ_msg') ?>
</div>
 
<script>


    function getfocus() {
        //alert("ulala")
        //document.getElementById("c_valid_from").focus();
        //e.prevent();
    }

</script> 
<script>
    function calculate_price(id){

        var unit_price=document.getElementById("p_i_price").value;
       // alert(unit_price);
        document.getElementById("p_price").focus();
        document.getElementById('p_price').value =parseFloat(unit_price)*parseFloat(id);
    }

    function final_price_1(value){

        var item_price= document.getElementById('p_price').value;

        var item_price_int=parseFloat(item_price);
        var value_int=parseFloat(value);
        document.getElementById("p_total_price").focus();
        
		var total_price=item_price_int+(item_price_int*(value_int/100));

        document.getElementById('p_total_price').value=total_price.toFixed(2);

         document.getElementById('p_total_price2').value=document.getElementById('p_total_price').value;

    }

    function final_price_2(value){

        var item_price= document.getElementById('p_total_price2').value;

        var item_price_int=parseFloat(item_price);
        var value_int=parseFloat(value);
		var total_val=item_price_int-(item_price_int*(value_int/100));
         document.getElementById('p_total_price').value=total_val.toFixed(2);

    }
$(document).on('blur', '#c_valid_from', function () {
	$('#c_valid_from').addClass('focus');
});
</script>
<script>


function set_vendor(a){
	$("#p_supplier").val(a);
	$('#basic1').modal('toggle');
	
}
	function fetch_item(value){
		
		       // alert("ggg");
		 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/fetch_item",
                data:{item: value},
                success:function(data1)
                {	
						//alert(data1);
						
						$('#mdl1').html(data1);
					//alert(data);
                }
            });

	}

function fetch_item1(value){
		
		       // alert("ggg");
		 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/fetch_lost_items",
                data:{item: value},
                success:function(data1)
                {	
						//alert(data1);
						$('#mdl2').html(data1);
					//alert(data.itm);
                }
            });

	}
   
    
	function getVal(val){
		alert(val); return false;
		var val_arr=val.split("-");
		//alert(val_arr[0]);
		//alert(val_arr[1]);
		document.getElementById("select_f_itm").value=val_arr[0];
		document.getElementById("select_f_itm_id").value=val_arr[1];
		
		$('#basic').modal('hide');
	}
	function getValue(val){
		document.getElementById("id").value=val;
	}
	function getVal1(val){
		var val_arr=val.split("-");
		//alert(val_arr[0]);
		//alert(val_arr[1]);
		document.getElementById("select_l_itm").value=val_arr[0];
		document.getElementById("select_l_itm_id").value=val_arr[1];

		document.getElementById("id").value=val;
		$('#basic1').modal('hide');
	}
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});
</script> 

<script>

$.typeahead({
    input: '.js-typeahead-user_v1',
    minLength: 1,
    order: "asc",
    dynamic: true,
    delay: 200,
    backdrop: {
        "background-color": "#fff"
    },
   
    source: {
       project: {
            display: "name",
            ajax: [{
                type: "GET",
                url: "<?php echo base_url();?>dashboard/search_vendor",
                data: {
                    q: "{{query}}"
                }
            }, "data.name"],
            template: '<div class="clearfix">' +
						'<span class="project-img">' +
                    '<img src="{{image}}">' +
                '</span>' +
			          '<div class="project-information">' +
                    '<span class="pro-name" style="font-size:15px;"> {{name}}</span>' +
					 '<span class="pro-name" style="font-size:15px;"> {{icon}}</span>'+
                '</div>' +
            '</div>'
        }
    },
    callback: {
        onClick: function (node, a, item, event) {
         // alert(JSON.stringify(item));
		// $("#c_unit_price").val(item.unitprice);
		$("#vendor_id").val(item.id);
		//$("#vendor_id").val(item.id);
		
		
		 
        },
        onSendRequest: function (node, query) {
            console.log('request is sent')
			
        },
        onReceiveRequest: function (node, query) {
            //console.log('request is received')
	
			
        }
    },
    debug: true
});

</script>

