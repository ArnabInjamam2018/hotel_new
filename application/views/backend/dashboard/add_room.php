<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
<?php endif;?>
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"> <span class="caption-subject bold uppercase"><i class="glyphicon glyphicon-bed"></i>&nbsp; Add Room</span> </div>
	</div>
	<div class="portlet-body form">
		<form action="<?php echo base_url();?>dashboard/add_room" method="post" enctype="multipart/form-data" id="form">
			<div class="form-body">
				<div class="row">
					<!-- add unit type---->
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<select name="unit_type" id="unit_type" class="form-control bs-select" required onchange="get_unit_name(this.value)">

								<option value="" disabled="" selected="">Unit Category</option>
								<?php foreach($unitTypeName as $uName){ ?>
								<option value="<?php echo $uName->name;?>">
									<?php echo $uName->name;?>
								</option>
								<?php } ?>
							</select>
							<label></label>
							<span class="help-block">Unit Category *</span>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group form-md-line-input" id="unit_nty">
							<select class="form-control bs-select" id="unitName" name="unit_name" required onchange="get_unit_class(this.value)">
								<option value="" disabled="" selected="">Unit Type</option>
							</select>
							<label></label>
							<span class="help-block">Unit Type *</span>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group form-md-line-input" id="unitClass">
							<input type="text" autocomplete="off" class="form-control" readonly id="unit_class" name="unit_class" required="required" placeholder="Unit Class *">
							<label></label>
							<span class="help-block">Unit Class *</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input" id="unitClass">
							<input type="text" autocomplete="off" class="form-control" onblur="check_room_name(this.value)" id="room_name" name="room_name" placeholder="Room Name">
							<label></label>
							<span class="help-block">Room Name *</span>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input onblur="check_unit_no(this.value)" type="text" autocomplete="off" class="form-control" id="ct_name" name="room_no"  required="required" placeholder="Unit No *">
							<label></label>
							<span class="help-block">Unit No *</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<script>
								function check_unit_no( value ) {

									jQuery.ajax( {
										type: "POST",
										url: "<?php echo base_url(); ?>dashboard/check_duplicate_room?room_no" + value,
										datatype: 'json',
										data: {
											room_no: value
										},
										success: function ( data ) {
											if ( data.data == "1" ) {
												//alert("Duplicate Room number for Hotel");
												//document.getElementById("ct_name").value = "";
												swal( {
														title: "Duplicate Unit Number",
														text: "Two units can not have the same number!",
														confirmButtonColor: "#FFA500",
														type: "warning"
													},
													function () {
														document.getElementById( "ct_name" ).value = "";
														// location.reload();

													} );



											}
										}
									} );
								}

								function check_room_name( value ) {

									jQuery.ajax( {
										type: "POST",
										url: "<?php echo base_url(); ?>dashboard/check_duplicate_room_name?room_name" + value,
										datatype: 'json',
										data: {
											room_name: value
										},
										success: function ( data ) {
											//alert(data.data);
											if ( data.data == "1" ) {

												swal( {
														title: "Duplicate Room Name For Hotel",
														text: "",
														type: "warning"
													},
													function () {
														document.getElementById( "room_name" ).value = "";
														// location.reload();

													} );

												// document.getElementById("room_name").value = "";
											}
										}
									} );
								}
							</script>
							<?php $floors=$this->dashboard_model->get_floor();
                                             foreach($floors as $floor){
                                                 $floor_no=$floor->hotel_floor;
                                             }
                                             $i=0;
    
                                             //Number builder start
    
                                             //$locale = 'en_US';
                                            // $nf = new NumberFormatter($locale, NumberFormatter::ORDINAL);
                                             //number builder end
    
    
    
                                             ?>
							<select class="form-control bs-select" id="floor_no" name="floor_no" value="lala" required>
								<option disabled selected> Floor No</option>
								<!--<option value="select the floor" disabled="disabled">--Select The Floor--</option>-->
								<option value="<?php echo $i ?>">Ground</option>
								<?php $i=$i+1; while($i<$floor_no){ ?>
								<option value="<?php echo $i ?>">
									<?php  echo $i.substr(date('jS', mktime(0,0,0,1,($i%10==0?9:($i%100>20?$i%10:$i%100)),2000)),-2); ?>
								</option>
								<?php $i++; } ?>
							</select>
							<label></label>
							<span class="help-block">Floor No *</span>
						</div>
					</div>
					<!--   <div class="col-md-3">
              <div class="form-group form-md-line-input">
                <select class="form-control bs-select" id="room_bed" name="room_bed" required="required">
                  <option value="" disabled="disabled" selected="selected">Bed No</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="4">5</option>
                  <option value="4">6</option>
                  <option value="4">7</option>
                  <option value="4">8</option>
                  <option value="4">9</option>
                  <option value="4">10</option>
                </select>
                <label></label>
                <span class="help-block">Bed No *</span> 
              </div>
          </div>-->
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input type="text" autocomplete="off" class="form-control" id="form_control_1" name="room_rent" required="required" onkeypress="return onlyNos(event, this);" placeholder="Base Room Rent *">
							<label></label>
							<span class="help-block">Base Room Rent *</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input type="text" autocomplete="off" class="form-control" id="form_control_1" name="room_rent_weekend" required="required" onkeypress="return onlyNos(event, this);" placeholder="Weekend Room Rent *">
							<label></label>
							<span class="help-block">Weekend Room Rent *</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input type="text" autocomplete="off" class="form-control" id="form_control_1" name="room_rent_seasonal" required="required" onkeypress="return onlyNos(event, this);" placeholder="Seasonal Room Rent *">
							<label></label>
							<span class="help-block">Seasonal Room Rent *</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input type="text" autocomplete="off" class="form-control" id="form_control_1" name="max_discount" required="required" onkeypress="return onlyNos(event, this);" placeholder="Maximum Discount *">
							<label></label>
							<span class="help-block">Maximum Discount *</span>
						</div>
					</div>
					<!--<div class="col-md-3">
              <div class="form-group form-md-line-input">
                <input id="max_occupancy" class="form-control" type="text" name="max_occupancy" required="required" onkeypress="return onlyNos(event, this);" placeholder="Max Occupancy *">
                <label></label>
                <span class="help-block">Max Occupancy *</span> 
              </div>
          </div>-->
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<select id="rate_type" class="form-control bs-select" name="rate_type" onchange="get_additional_value(this.value)" required>
								<option value="" disabled="disabled" selected="selected">Rate type </option>
								<option value="0">Fixed</option>
								<option value="1">Depends on person</option>
							</select>
							<label></label>
							<span class="help-block">Rate type *</span>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group form-md-line-input" id="unitClass">
							<textarea autocomplete="off" class="form-control" id="note" name="note" placeholder="Note"></textarea>
							<label></label>
							<span class="help-block">Note</span>
						</div>
					</div>


					<div style="display:none;" id="smt" class="col-md-12">
						<div class="row">
							<label class="col-md-4" style="padding-top: 27px;">Additional Adult (Age > = 18) Rate</label>
							<div class="col-md-4" style="padding-top: 21px;">
								<div class="md-radio-inline">
									<div class="md-radio">
										<input type="radio" name="adult_radio" id="adult_radio1" class="md-radiobtn" value="1">														   
										<label for="adult_radio1">
											<span></span>
											<span class="check"></span>
											<span class="box"></span> Percentage(%) 
										</label>
									</div>
									<div class="md-radio">
										<input type="radio" name="adult_radio" id="adult_radio2" class="md-radiobtn" value="0">														   
										<label for="adult_radio2">
											<span></span>
											<span class="check"></span>
											<span class="box"></span> Fixed 
										</label>
									</div>
								</div>								
							</div>							
							<div class="col-md-4">
								<div class="form-group form-md-line-input">
									<input id="adult_rate" name="adult_rate" class="form-control" type="text" placeholder="Enter Rate *">
									<label></label>
									<span class="help-block">Enter Rate *</span>
								</div>
							</div>
							<label class="col-md-4" style="padding-top: 27px;">Additional Kid (age>6) Rate</label>
							<div class="col-md-4" style="padding-top: 21px;">
								<div class="md-radio-inline">
									<div class="md-radio">
										<input type="radio" name="kid_radio" id="kid_radio1" class="md-radiobtn" value="1">	
										<label for="kid_radio1">
											<span></span>
											<span class="check"></span>
											<span class="box"></span> Percentage(%) 
										</label>
									</div>
									<div class="md-radio">
										<input type="radio" name="kid_radio" id="kid_radio2" class="md-radiobtn" value="0" >
										<label for="kid_radio2">
											<span></span>
											<span class="check"></span>
											<span class="box"></span> Fixed 
										</label>
									</div>
								</div>	
							</div>
							<div class="col-md-4">
								<div class="form-group form-md-line-input">
									<input id="kid_rate" name="kid_rate" class="form-control" type="text" placeholder="Enter Rate *">
									<label></label>
									<span class="help-block">Enter Rate *</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<label style=" font-size:16px; padding: 30px 0 10px;">Available Features</label>
						<div class="add-roo">
							<div class="row">

								<?php
								if ( isset( $room_feature ) && $room_feature != '' ) {

									foreach ( $room_feature as $qry ) {
										?>
								<div class="col-md-3">
									<div class="form-group form-md-checkboxes" style="margin-bottom: 0;">
										<div class="md-checkbox">
											<input type="checkbox" id="chk_<?php echo $qry->room_feature_id; ?>" class="md-check" name="room_feature[]" value="<?php echo $qry->room_feature_id;?>" onclick="showType('<?php echo $qry->room_feature_id;?>')">
											<label for="chk_<?php echo $qry->room_feature_id; ?>"> <span></span> <span class="check"></span> <span class="box"></span> <?php echo $qry->room_feature_name; ?> </label>
										</div>
									</div>
									<div class="showhidetarget" id="featureType_<?php echo $qry->room_feature_id;?>" style="display:none;">
										<div class="form-group form-md-radios" style="margin-bottom: 0;">
											<div class="md-radio-inline">
												<div class="md-radio">
												   <input type="radio" name="t_<?php echo $qry->room_feature_id;?>" id="feature_type_<?php echo $qry->room_feature_id;?>_1" value="0" checked="checked" onclick="getVal('<?php echo $qry->room_feature_id;?>',this.value)" class="md-radiobtn">
													<label for="feature_type_<?php echo $qry->room_feature_id;?>_1">
														<span></span>
														<span class="check"></span>
														<span class="box"></span> Free </label>
												</div>
												<div class="md-radio">
												   <input type="radio" name="t_<?php echo $qry->room_feature_id;?>" id="feature_type_<?php echo $qry->room_feature_id;?>_2" value="1" onclick="getVal('<?php echo $qry->room_feature_id;?>',this.value)" class="md-radiobtn">
													<label for="feature_type_<?php echo $qry->room_feature_id;?>_2">
														<span></span>
														<span class="check"></span>
														<span class="box"></span> Chargeable </label>
												</div>
											</div>
										</div>
										<input type="hidden" name="uamt_<?php echo $qry->room_feature_id;?>" id="uamt_<?php echo $qry->room_feature_id;?>" value="0">
										<div id="samt_<?php echo $qry->room_feature_id;?>" style="display:none; margin: 0 0 10px 0; padding: 0;" class="form-group form-md-line-input">
											<input type="text" name="amt_<?php echo $qry->room_feature_id;?>" id="amt_<?php echo $qry->room_feature_id;?>" class="form-control" placeholder="Chargeable Amount" style="font-size:13px;">
											<label></label>
										</div>
									</div>
								</div>
								<?php }
				}
			  ?>
							</div>
						</div>
					</div>
					
					<div class="col-md-3">
						<div class="form-group form-md-line-input uploadss">
							<label>Upload Room Photo 1</label>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
								<div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
									<input type="file" name="image1"/>
									</span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
							</div>
						</div>
						
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input uploadss">
							<label>Upload Room Photo 2</label>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
								<div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
									<input type="file" name="image2"/>
									</span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
							</div>
						</div>
						
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input uploadss">
							<label>Upload Room Photo 3</label>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
								<div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
									<input type="file" name="image3"/>
									</span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
							</div>
						</div>
						
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input uploadss">
							<label>Upload Room Photo 4</label>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
								<div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
									<input type="file" name="image4"/>
									</span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
							</div>
						</div>
						
					</div>
					
				</div>
			</div>
			<div class="form-actions right">
				<button type="submit" class="btn submit">Submit</button>
				<button type="reset" onclick="reset()" class="btn default">Reset</button>
			</div>
		</form>
		<!-- END CONTENT -->

	</div>
	<!-- END PAGE CONTENT-->
</div>
<script>
	function reset() {
		$( '#unit_type' ).val( ' ' );
		$( '#unitName' ).val( ' ' );
		$( '#unit_class' ).val( ' ' );
		$( '#floor_no' ).val( ' ' );
		$( '#room_bed' ).val( ' ' );
		$( '#rate_type' ).val( ' ' );
	}


	function get_unit_name( val ) {
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>dashboard/get_unit_name",
			data: "val=" + val,
			success: function ( msg ) {
				if ( msg == 0 ) {
					swal( {
							title: "Select another category!",
							text: "This category does not have any unit type defined under it.",
							type: "error",
							confirmButtonColor: "#F27474"
						} )
						//swal("Select another category!", "This category does not have any unit type defined under it.", "error");
				}
				$( "#unit_nty" ).html( msg );
				
				$.getScript('<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js');
				$.getScript('<?php echo base_url();?>assets/pages/scripts/components-select2.min.js');
			}
		});
	}

	function get_unit_class( val ) {
		//alert(val);
		$.ajax( {
			type: "POST",
			url: "<?php echo base_url();?>dashboard/get_unit_class",
			data: "val=" + val,
			success: function ( msg ) {
				$( "#unitClass" ).html( msg );

			}
		} );


	}

	function showType( val ) {
		//alert(val);
		$( "#featureType_" + val ).toggle();
	}
	$( document ).ready( function () {
		$( '.showhidetarget' ).hide();
	} );

	function getVal( id, val ) {
		//alert(id);
		//alert(val);
		if ( val == '1' ) {
			document.getElementById( "samt_" + id ).style.display = "block";
			document.getElementById( "uamt_" + id ).value = "1";
		} else {
			document.getElementById( "uamt_" + id ).value = "0";
			document.getElementById( "amt_" + id ).value = "";
			document.getElementById( "samt_" + id ).style.display = "none";

		}
	}

	function get_additional_value( val ) {
		if ( val == '1' ) {
			document.getElementById( "smt" ).style.display = "block";
		} else {
			document.getElementById( "smt" ).style.display = "none";

		}
	}
</script>
<!-- END CONTENT -->