<!-- BEGIN PAGE CONTENT-->
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <span class="caption-subject bold uppercase"><i class="fa fa-plus-square"></i>&nbsp; Add Section</span> </div>
  </div>
  <div class="portlet-body form">
    
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
        <?php
                  $form = array(
                      'class'       => '',
                      'id'        => 'form',
                      'method'      => 'post',
      
                  ); 
                    echo form_open_multipart('unit_class_controller/add_hotel_section',$form);
                  ?>
        <div class="form-body">
        <div class="row">
        <div class="col-md-3">
        <div class="form-group form-md-line-input form-md-floating-label ">
          <input type="text" autocomplete="off" class="form-control" id="name" name="name" required="required" placeholder="Section Name *">
          <label></label>
          <span class="help-block">Section Name *</span> </div>
		</div>        
        <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" autocomplete="off" class="form-control" id="Housekeeping_status" name="Housekeeping_status" required="required" placeholder="Housekeeping Status *">
              <label></label>
              <span class="help-block">Housekeeping Status *</span> </div>
		  </div> 
		  <div class="col-md-3">
              <div class="form-group form-md-line-input">
              <input type="text" autocomplete="off" class="form-control" id="audit_status" name="audit_status" placeholder="Audit Status *" required="required">
              <label></label>
              <span class="help-block">Audit Status *</span> </div>
          </div>
        <div class="col-md-3">
		  <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="hotel_id" name="hotel_id" required="required" placeholder="Section Hotel Id *">
          <label></label>
          <span class="help-block">Section Hotel Id *</span> </div>
        </div>
        <div class="col-md-6">
          <div class="form-group form-md-line-input">
           <textarea class="form-control" row="3" name="note" id="note" placeholder="Note"></textarea>
          <label></label>
          <span class="help-block">Note</span> </div>
		</div>
        <div class="col-md-6">
            <div class="form-group form-md-line-input">
              <textarea class="form-control" row="3" name="desc" id="desc" placeholder="Description"></textarea>
              <label></label>
              <span class="help-block">Description</span> </div>
		</div>
        <div class="col-md-12">
            <div class="form-group form-md-line-input uploadss">
              <label>Upload Asset Image</label>
              <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/>
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                </div>
                <div>
                    <span class="btn default btn-file">
                    <span class="fileinput-new">
                    Select image </span>
                    <span class="fileinput-exists">
                    Change </span>
					<?php echo form_upload('image');?>
                    </span>
                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                    Remove </a>
                </div>
              </div>
            </div>
            </div>
        </div>
      </div>
        <div class="form-actions right">
          <button type="submit" class="btn blue">Submit</button>
          <button  type="reset" class="btn default">Reset</button>
        </div>
        <?php form_close(); ?>      
  </div>
</div>
 <script>
function check(){
  var a=$("#charge").val();
  var b=$("#tax").val();
  
  if(isNaN(a)){
	  alert("enter number");
   	$("#charge").val("");
  }
 if(isNaN(b)){
	  alert("enter number");
   	$("#tax").val("");
  }
}
</script>  
