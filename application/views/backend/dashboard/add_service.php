<script>
    function fetch_all_address_hotel(pincode,g_country,g_state,g_city)
    {
        var pin_code = document.getElementById(pincode).value;
        jQuery.ajax(
            {
                type: "POST",
                url: "<?php echo base_url(); ?>bookings/fetch_address",
                dataType: 'json',
                data: {pincode: pin_code},
                success: function(data){
                    //alert(data.country);
                    $(g_country).val(data.country);
                    $(g_state).val(data.state);
                    $(g_city).val(data.city);
                }

            });
    }
</script>
<script type="text/javascript">
    function submit_form()
    {
        document.getElementById('submit_form').submit();
    }
</script>
<!-- BEGIN PAGE CONTENT-->
<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue" id="form_wizard_1">
  <div class="portlet-title">
	<div class="caption"> <i class="icon-pin"></i> ADD SERVICE - <span class="step-title"> Step 1 of 4 </span> </div>
	<div class="tools hidden-xs"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body form">
	<form action="<?php echo base_url();?>dashboard/add_service" class=""  enctype="multipart/form-data" id="submit_form" method="POST">
	  <div class="form-wizard">
		<div class="form-body">
		  <ul class="nav nav-pills nav-justified steps">
			<li> <a href="#tab1" data-toggle="tab" class="step"> <span class="number"> 1 </span> <span class="desc"> <i class="fa fa-check"></i> Service Information </span> </a> </li>
			<li> <a href="#tab2" data-toggle="tab" class="step"> <span class="number"> 2 </span> <span class="desc"> <i class="fa fa-check"></i> Eligibility criteria 1 </span> </a> </li>
			<li> <a href="#tab3" data-toggle="tab" class="step active"> <span class="number"> 3 </span> <span class="desc"> <i class="fa fa-check"></i> Price and Tax </span> </a> </li>
		  </ul>
		  <div id="bar" class="progress progress-striped" role="progressbar">
			<div class="progress-bar progress-bar-success"> </div>
		  </div>
		  <div class="tab-content">
			<div class="alert alert-danger display-none">
			  <button class="close" data-dismiss="alert"></button>
			  You have some form errors. Please check below. </div>
			<div class="alert alert-success display-none">
			  <button class="close" data-dismiss="alert"></button>
			  Your form validation is successful! </div>
			<div class="tab-pane active" id="tab1">
			  <div class="row">
				<div class="col-md-4">
				  <div class="form-group form-md-line-input">
					<input type="text" class="form-control" name="s_name" placeholder="Service Name *" required/>
					<label></label>
					<span class="help-block">Service Name *</span> </div>
				</div>
				<div class="col-md-4">
				  <div class="form-group form-md-line-input">
					<select class="form-control bs-select"  name="s_category" required>
					  <option value="" selected="selected" disabled="disabled">Service Category *</option>
					  <option value="food & drink">Food and Drink</option>
					  <option value="travel & tour">Travel and Tour</option>
					  <option value="room service">Room Service</option>
					  <option value="transport">Transport</option>
					  <option value="honeymoon">Honeymoon</option>
					  <option value="mixed">Mixed</option>
					</select>
					<label></label>
					<span class="help-block">Service Category *</span> </div>
				</div>
				<div class="col-md-4">
				  <div class="form-group form-md-line-input">
					<input type="text" class="form-control" name="s_description" placeholder="Service Description *">
					<label></label>
					<span class="help-block">Service Description *</span> </div>
				</div>
				<div class="col-md-4">
				  <div class="form-group form-md-line-input">
					<select class="form-control bs-select"  name="s_no_member" required>
					  <option value="" selected="selected" disabled="disabled">Service Applied For *</option>
					  <option value="1">One Member</option>
					  <option value="2">All Member</option>
					  <option value="3">Custoem Number of Member</option>
					</select>
					<label></label>
					<span class="help-block">Service Applied For *</span> </div>
				</div>
				<div class="col-md-4">
				  <div class="form-group form-md-line-input">
					<input type="text" class="form-control" name="s_no_member_custom" placeholder="Custom Member"/>
					<label></label>
					<span class="help-block">Custom Member</span> </div>
				</div>
				<div class="col-md-4">
				  <div class="form-group form-md-line-input">
					<select class="form-control bs-select"  name="s_periodicity" required >
					  <option value="" selected="selected" disabled="disabled">Service Periodicity *</option>
					  <option value="1">Once per half day</option>
					  <option value="2">Once per day</option>
					  <option value="3">Once Per visit</option>
					  <option value="4">Once every month</option>
					  <option value="5">Once every year</option>
					</select>
					<label></label>
					<span class="help-block">Service Periodicity *</span> </div>
				</div>
				<div class="col-md-12">
				  <div class="row">
					<div class="col-md-4">
					  <div class="form-group form-md-line-input uploadss">
						<label>Service Image *</label>
						<div class="fileinput fileinput-new" data-provides="fileinput">
						  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/> </div>
						  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
						  <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
							<input type="file" name="s_image" />
							</span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
						</div>
					  </div>
					</div>
					<div class="col-md-4">
					  <div class="form-group form-md-line-input uploadss">
						<label>Service Icon *</label>
						<div class="fileinput fileinput-new" data-provides="fileinput">
						  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/> </div>
						  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
						  <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
							<input type="file" name="s_icon" />
							</span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
						</div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			</div>
			<div class="tab-pane" id="tab2">
			  <div class="row">
				<div class="col-md-12 form-horizontal">
					<div class="form-group form-md-line-input">
						<label class="col-md-4 control-label">Apply To All Guest</label>
						<div class="col-md-8">
						  <div class="md-radio-inline">
							<div class="md-radio">
							  <input type="radio" id="radio6" value="1" name="rent_mode_type" class="md-radiobtn" onclick="banner_noshow()">
							  <label for="radio6"> <span></span> <span class="check"></span> <span class="box"></span> Yes</label>
							</div>
							<div class="md-radio">
							  <input type="radio" id="radio7" value="0" name="rent_mode_type" class="md-radiobtn" checked onclick="banner_show()">
							  <label for="radio7"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
							</div>
						  </div>
						</div>
					</div>
				</div>
				<div class="col-md-12" id="ho_rules">
					<div class="form-group form-md-line-input">
					  <input class="form-control" readonly type="text" id="rule_list" name="rule_list_val" placeholder="Rules Entered *"/>
					  <label></label>
					  <span class="help-block">Rules Entered *</span>
					</div>
				</div>
				<div class="col-md-3" id="mc">
					<div class="form-group form-md-line-input">
					  <select id="Master_cat" class="form-control bs-select" data-hide-disabled="true" data-live-search="true" onchange="master_catagory(this.value)">
						<option  value="" disabled selected>Master Category</option>
						<option  value="Genaral Category" > Genaral Category</option>
						<option  value="Guest"> Guest</option>
						<option  value="Booking"> Booking </option>
						<option  value="Unit"> Unit </option>
						<option  value="Event"> Event</option>
					  </select>
					  <label></label>
					  <span class="help-block">Master Category</span>
					</div>
				</div>
				<div class="col-md-3" id="gn" style="display:none;">
					<div class="form-group form-md-line-input">
					  <select id="gen_cat"  class="form-control bs-select"  data-hide-disabled="true" data-live-search="true" onchange="open_oparetor()">
						<option  value="" disabled selected>Genaral Category</option>
						<option  value="Total Bill" > Total Bill</option>
						<option  value="date('Y-m-d')"> System Date</option>
					  </select>
					  <label></label>
					  <span class="help-block">Genaral Category</span>
					</div>
				</div>
				<div class="col-md-3" id="guest" style="display:none;">
				<div class="form-group form-md-line-input">
				  <select name="s_guest" id="guest_cat"  class="form-control bs-select"  data-hide-disabled="true" title="Guest" data-live-search="true" onchange="open_oparetor()">
					<option  value="" disabled selected>Guest</option>
					<option  value="g_name" > Guest name</option>
					<option  value="g_gender"> Guest gender</option>
					<option  value="g_age" > Guest age</option>
					<option  value="g_type"> Guest Type</option>
					<option  value="g_state" > Guest State</option>
					<option  value="g_vip=1"> VIP?</option>
					<option  value="g_preffered=1?" > Preferred?</option>
					<option  value="g_no_times"> No of times visited</option>
					<option  value="g_total_spending" > Total spending</option>
					<option  value="g_no_times=1"> New Guest?</option>
					<option  value="Global category"> Global category</option>
				  </select>
				  <label></label>
				  <span class="help-block">Guest</span>
				</div>
				</div>
				<div class="col-md-3" id="booking" style="display:none;">
				<div class="form-group form-md-line-input">
				  <select name="s_booking" id="booking_cat"  class="form-control bs-select"  data-hide-disabled="true" title="Booking" data-live-search="true" onchange="open_oparetor()">
					<option  value="" disabled selected>Booking</option>
					<option  value="cust_from_date" > Booking start date</option>
					<option  value="cust_end_date"> Booking end date</option>
					<option  value="no_of_guest" > No of Guest</option>
					<option  value="no_of_guest >1"> Family?</option>
					<option  value="no_of_guest =1" > Individual?</option>
					<option  value="nature_visit"> Nature of visit</option>
					<option  value="booking_source" > Booking source</option>
					<option  value="stay_days"> No of stay days</option>
					<option  value="room_rent_total_amount" > Total Rent</option>
					<option  value="Global category"> Global category</option>
					<option  value="Weekend"> Weekend</option>
				  </select>
				  <label></label>
				  <span class="help-block">Booking</span>
				</div>
				</div>
				<div class="col-md-3" id="unit" style="display:none;">
				<div class="form-group form-md-line-input">
				  <select name="s_unit" id="unit_cat"  class="form-control bs-select" data-hide-disabled="true" title="Unit" data-live-search="true" onchange="open_oparetor()">
					<option  value="" disabled selected>Unit</option>
					<option  value="room_no" > Room/ Unit no</option>
					<option  value="room_bed"> No of Bed</option>
					<option  value="floor_no" > Floor</option>
					<option  value="Room category"> Room category</option>
					<option  value="Global category" > Global category</option>
				  </select>
				  <label></label>
				  <span class="help-block">Unit</span>
				</div>
				</div>
				<div class="col-md-3" id="event" style="display:none;">
					<div class="form-group form-md-line-input">
					  <select name="s_event" id="event_cat"  class="form-control bs-select" data-hide-disabled="true" title="Event" data-live-search="true" onchange="open_oparetor()">
						<option  value="" disabled selected></option>
						<option  value="e_name" > Event Name</option>
						<option  value="e_from <=DATE() OR DATE() >=e_upto "> is Event?</option>
					  </select>
					  <label></label>
					  <span class="help-block">Event</span>
					</div>
				</div>
				<div class="col-md-3" id="oparetor" style="display:none;">
				<div class="form-group form-md-line-input" >
				  <select name="s_operator" id="operator_cat" class="form-control bs-select"  data-hide-disabled="true" title="Oparetor" data-live-search="true" onchange= "open_value()">
					<option  value="" disabled selected>Oparetor</option>
					<option  value="=" > =</option>
					<option  value=">" > > </option>
					<option  value="<" > < </option>
					<option  value="IN "> IN </option>
					<option  value="LIKE"> LIKE </option>
					<option  value="NOT EQUAL TO"> NOT EQUAL TO </option>
					<option  value="NOT IN"> NOT IN </option>
					<option  value="IS NULL"> IS NULL </option>
					<option  value="IS NOT NULL"> IS NOT NULL </option>
					<option  value="NOT LIKE"> NOT LIKE </option>
				  </select>
				  <label></label>
				  <span class="help-block">Oparetor</span>
				</div>
				</div>
				<div class="col-md-3" id="value_one" style="display:none;">
					<div class="form-group form-md-line-input" >
					  <input type="text" id="rule_value"  class="form-control" name="s_value" onkeypress="open_tgl_btn()" placeholder="value"/>
					  <label></label>
					  <span class="help-block">value</span>
					</div>
				</div>
				<div class="col-md-12">
				<div class="form-group form-md-line-input">
				  <button class="btn blue" id="btn_1" onclick="append_div();reset_div();" > Add Another Rule</button>
				  <button class="btn blue" id="btn_2" onclick="append_group()" > Start Grouping</button>
				  <div id="and-or" style="display:none; margin:0 10px;">
					<div class="form-group form-md-radios">
					  <div class="md-radio-inline">
						<div class="md-radio" style="margin-right: 12px;">
						  <input type="radio" id="radio8" name="rent_mode_type" class="md-radiobtn" value="AND" onclick="concat1(this.value)">
						  <label for="radio8"> <span></span> <span class="check"></span> <span class="box"></span> AND</label>
						</div>
						<div class="md-radio">
						  <input type="radio" id="radio9" name="rent_mode_type" class="md-radiobtn" value="OR" onclick="concat1(this.value)">
						  <label for="radio9"> <span></span> <span class="check"></span> <span class="box"></span> OR</label>
						</div>
					  </div>
					</div>
				  </div>
				  <button id="end_grouping" class="btn blue" onclick="append_group2()" style="display:none;"> End Grouping </button>
				</div>
				</div>
			  </div>
			</div>
			<script>
					var x = 1;

					setInterval(function(){
						var data= document.getElementById('rule_list').value;
						if(data.indexOf("(") > -1){

							document.getElementById('end_grouping').style.display="inline-block";

						}
						else{
							document.getElementById('end_grouping').style.display="none";
						}

					},1000);

					function concat1(value){
						document.getElementById('rule_list').value+= '  '+ value +' ';
						document.getElementById('and-or').style.display="none";

					}
				   function append_group(){
					   document.getElementById('rule_list').value+= ' ( ';
				   }
					function append_group2(){
						document.getElementById('rule_list').value+= ' ) ';
					}

					function append_div(){

						document.getElementById('').style.display="inline-block";
						var booking_cat = document.getElementById('booking_cat').value;
						var unit_cat = document.getElementById('unit_cat').value;
						var guest_cat = document.getElementById('guest_cat').value;
						var gen_cat = document.getElementById('gen_cat').value;
						var event_cat = document.getElementById('event_cat').value;
						var operator_cat = document.getElementById('operator_cat').value;
						var rule_value = document.getElementById('rule_value').value;
					  

						var total_rule=booking_cat+' '+unit_cat+' '+guest_cat+' '+gen_cat+' '+event_cat+' '+operator_cat+' '+"'"+rule_value+"'";
					  
						document.getElementById('rule_list').value+= ' '+ total_rule +' ';

						
						x = x+1;
					}
				
				</script>
			<div class="tab-pane" id="tab3">
			  <div class="row">
				<input type="hidden" name="s_rules" id="s_rules">
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
					  <input  type="type" autocomplete="off" class="form-control" id="form_control_1" name="s_price"  onkeypress="return onlyNos(event, this);" placeholder="Base Price *" required>
					  <label></label>
					  <span class="help-block">Base Price *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
					  <input  type="type" autocomplete="off" class="form-control" id="form_control_1" name="s_price_weekend" required="required" onkeypress="return onlyNos(event, this);" placeholder="Weekend Price *">
					  <label></label>
					  <span class="help-block">Weekend Price *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
					  <input  type="type" autocomplete="off" class="form-control" id="form_control_1" name="s_price_holiday" required="required" onkeypress="return onlyNos(event, this);" placeholder="Holiday Price *">
					  <label></label>
					  <span class="help-block">Holiday Price *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
					  <input  type="type" autocomplete="off" class="form-control" id="form_control_1" name="s_price_special" required="required" onkeypress="return onlyNos(event, this);" placeholder="Special Price *">
					  <label></label>
					  <span class="help-block">Special Price *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
					  <select id="tax" class="form-control bs-select" onchange="return hideShow();" name="s_tax_applied" >
						<option value="" disabled="disabled" selected="selected">Tax Applied? *</option>
						<option value="y">Yes</option>
						<option value="n">No</option>
					  </select>
					  <label></label>
					  <span class="help-block">Tax Applied? *</span> </div>
				</div>
				<div class="col-md-4" id="s_tax" style="display: none;">
				<div class="form-group form-md-line-input">
				  <input type="text" class="form-control" name="s_tax" placeholder=" Tax %" />
				  <label></label>
				  <span class="help-block"> Tax %</span> </div>
				</div>
				<div class="col-md-4">
				<div class="form-group form-md-line-input">
				  <select class="form-control bs-select" id="discount" onchange="return hideShow2();" name="s_discount_applied">
					<option value="" disabled="disabled" selected="selected">Discount Applied?</option>
					<option value="y">Yes</option>
					<option value="n">No</option>
				  </select>
				  <label></label>
				  <span class="help-block">Discount Applied?</span>
				</div>
				</div>
				<div class="col-md-4" id="service_discount" style="display: none;">
					<div class="form-group form-md-line-input">
					  <input type="text" class="form-control" name="s_discount" placeholder="Discount %"/>
					  <label></label>
					  <span class="help-block">Discount %</span>
					</div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
		<div class="form-actions right"> <a href="javascript:;" class="btn default button-previous"> <i class="m-icon-swapleft"></i> Back </a> <a href="javascript:;" class="btn blue button-next" onclick="check();"> Continue <i class="m-icon-swapright m-icon-white"></i> </a> <a href="javascript:;" class="btn submit button-submit" onclick="submit_form()"> Submit <i class="m-icon-swapright m-icon-white"></i> </a> </div>
	  </div>
	</form>
	<script type="text/javascript">
				function save_rule(){
					
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>dashboard/add_rule",
						data: $("#submit_form").serialize(),
						success: function(msg) {
							var s_rules=document.getElementById("s_rules").value;
							if(s_rules!='') {
								document.getElementById("s_rules").value = document.getElementById("s_rules").value + "," + msg;
							}
							else{
								document.getElementById("s_rules").value = msg;
							}
						   
						   swal({
								title: "Rule Saved Successfully",
								text: "Your Rule Saved Successfully ",
								type: "success"
							},
							function(){
								document.getElementById("save_rule_btn").disabled = true;
								document.getElementById("another_rule_btn").style.display = 'block';
							});
						  
						}
					});
				}
				function add_row()
				{
					document.getElementById("rule_entry").style.display = 'none';
					$('#tab2').load('<?php echo base_url() ?>assets/html_static/rule_entry.html');

				}
				function hideShow()
				{
					if($('#tax').val() == 'y')
					{
						$("#s_tax").show();

					}
					else
					{
						$("#s_tax").hide();

					}
				}
				function hideShow2()
				{
					if($('#discount').val() == 'y')
					{
						$("#service_discount").show();

					}
					else
					{
						$("#service_discount").hide();

					}
				}
				
				 function open_gn()
					{
						
						document.getElementById('gn').style.display= 'block';
						document.getElementById('guest').style.display= 'none';
						document.getElementById('booking').style.display= 'none';
						document.getElementById('unit').style.display= 'none';
						document.getElementById('event').style.display= 'none';
						
					}
					function open_guest()
					{
						
						document.getElementById('guest').style.display= 'block';
						document.getElementById('gn').style.display= 'none';
						document.getElementById('booking').style.display= 'none';
						document.getElementById('unit').style.display= 'none';
						document.getElementById('event').style.display= 'none';
						
					}
					function open_book()
					{
						
						document.getElementById('booking').style.display= 'block';
						document.getElementById('gn').style.display= 'none';
						document.getElementById('guest').style.display= 'none';
						document.getElementById('unit').style.display= 'none';
						document.getElementById('event').style.display= 'none';
						
					}
					function open_unit()
					{
						document.getElementById('unit').style.display= 'block';
						document.getElementById('booking').style.display= 'none';
						document.getElementById('gn').style.display= 'none';
						document.getElementById('guest').style.display= 'none';
						document.getElementById('event').style.display= 'none';
						
					}
					function open_event()
					{
						document.getElementById('event').style.display= 'block';
						document.getElementById('unit').style.display= 'none';
						document.getElementById('booking').style.display= 'none';
						document.getElementById('gn').style.display= 'none';
						document.getElementById('guest').style.display= 'none';
						
					}
					function open_oparetor()
					{
						document.getElementById('oparetor').style.display= 'block';
						
						
					}
					function open_value()
					{
						document.getElementById('value_one').style.display= 'block';
						
						
					}
					function open_tgl_btn()
					{
						document.getElementById('tgl_btn').style.display= 'block';
						
						
					}
					 
		function banner_noshow(){
		  document.getElementById('ho_rules').style.display = "none";
		  document.getElementById('mc').style.display = "none";
		  document.getElementById('btn_1').style.display = "none";
		  document.getElementById('btn_2').style.display = "none";
		  document.getElementById('btn_3').style.display = "none";
		  document.getElementById('and-or').style.display = "none";			  
		}

		function banner_show(){
		  document.getElementById('ho_rules').style.display = "block"; 
		  document.getElementById('mc').style.display = "block";
		  document.getElementById('btn_1').style.display = "inline-block";
		  document.getElementById('btn_2').style.display = "inline-block";
		  document.getElementById('btn_3').style.display = "inline-block";			  
		}

				function master_catagory(val_mc)
				{
					
					val_mc = val_mc.toString();
					
				
					
					if(val_mc == "Genaral Category"){
						document.getElementById('gn').style.display = "block";
						document.getElementById('guest').style.display = "none";
						document.getElementById('booking').style.display = "none";
						document.getElementById('unit').style.display = "none";
						document.getElementById('event').style.display = "none";
					}
					
					else if(val_mc == "Guest"){
						document.getElementById('guest').style.display = "block";
						document.getElementById('gn').style.display = "none";
						document.getElementById('booking').style.display = "none";
						document.getElementById('unit').style.display = "none";
						document.getElementById('event').style.display = "none";
					}
					
					else if(val_mc == "Booking"){
						document.getElementById('booking').style.display = "block";
						document.getElementById('gn').style.display = "none";
						document.getElementById('guest').style.display = "none";
						document.getElementById('unit').style.display = "none";
						document.getElementById('event').style.display = "none";
					}
					else if(val_mc == "Unit"){
						document.getElementById('unit').style.display = "block";
						document.getElementById('booking').style.display = "none";
						document.getElementById('gn').style.display = "none";
						document.getElementById('guest').style.display = "none";
						document.getElementById('event').style.display = "none";
					}
					else{
						document.getElementById('event').style.display = "block";
						document.getElementById('unit').style.display = "none";
						document.getElementById('booking').style.display = "none";
						document.getElementById('gn').style.display = "none";
						document.getElementById('guest').style.display = "none";
					}
					
										
				}

	   function reset_div(){
		   var x = document.getElementById("operator_cat").selectedIndex;
		   var y = document.getElementById("operator_cat").options;
		  //y[x]=0;

		   document.getElementById("operator_cat").selectedIndex = -1;
		   document.getElementById("unit_cat").selectedIndex = -1;
		   document.getElementById("booking_cat").selectedIndex = -1;
		   document.getElementById("guest_cat").selectedIndex = -1;
		   document.getElementById("gen_cat").selectedIndex = -1;
	   }  
				
				
			</script> 
  </div>
</div>
<!-- END PAGE CONTENT-->