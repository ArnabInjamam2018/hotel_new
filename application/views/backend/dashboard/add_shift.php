<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong><?php echo $this->session->flashdata('err_msg');?></strong>
	</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong><?php echo $this->session->flashdata('succ_msg');?></strong>
	</div>
<?php endif;?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-pin"></i>
            <span class="caption-subject bold uppercase"> Add Shift</span>
        </div>

    </div>
    <div class="portlet-body form">


        <?php

        $form = array(
            'class' 			=> '',
            'id'				=> 'form',
            'method'			=> 'post'
        );

        echo form_open_multipart('dashboard/add_shift',$form);

        ?>
        <div class="form-body">
        <div class="row">
			<div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" autocomplete="off" class="form-control" name="shift_name" required="required" placeholder="Shift Name *">
                    <label></label>
                    <span class="help-block">Shift Name *</span>
                </div>
			</div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input" >    
                    <input type="text" autocomplete="off" required="required" name="shift_opening_time"  onblur="check_time()" class="form-control timepicker timepicker-24"  id="shift_opening_time" placeholder="Shift From *">    
                    <label></label>
                    <span class="help-block">Shift From *</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
    
                    <input type="text" autocomplete="off" required="required" onblur="check_time()" name="shift_closing_time" class="form-control timepicker timepicker-24" id="shift_closing_time" placeholder="Shift Up to *">                    
                    <label></label>
                    <span class="help-block">Shift Up to *</span>
                </div>
			</div>
        </div>
        </div>
        <div class="form-actions right">
            <button type="submit" class="btn submit" >Submit</button>
            <button type="submit" class="btn default">Reset</button>
        </div>
<?php form_close(); ?>


    </div>        
</div>
<script>

$(document).ready(function(){
    $('#shift_opening_time').val('00:00:00');
	$('#shift_closing_time').val('00:00:00');
});

    function getfocus() {
        //alert("ulala")
        document.getElementById("c_valid_from").focus();
        //e.prevent();
    }
	
	
function check_time(){
	//alert(a);
	var start=$('#shift_opening_time').val();
	var end=$('#shift_closing_time').val();
	start=start.concat(':00');
	end=end.concat(':00');
	
	
	if(start>end &&  end!=''){
		swal("Invalid!", "You clicked the button!", "error");
							$('#shift_opening_time').val('');
							$('#shift_closing_time').val('');
	}
	
	 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/check_shift_time",
                data:{start:start,end:end},
                success:function(data)
                {	
						if(data.data=='1'){
							swal("Duplicate!", "You clicked the button!", "error");
							$('#shift_opening_time').val('');
							$('#shift_closing_time').val('');
						}
						
                }
            });
}
</script>