<!-- BEGIN PAGE CONTENT-->
<script src="<?php echo base_url();?>assets/global/plugins/typeahead1/jquery.typeahead.js"></script>

<script>
function fetch_all_address()
{
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}
</script>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Stock/ Inventory</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/update_stock_invent',$form);

                        ?>
    <div class="form-body"> 
      <!-- 17.11.2015-->
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <!-- 17.11.2015-->
      
      
         
		<div class="row">
		 <div class="col-md-4">
        <div class="form-group form-md-line-input">

         <input type="text" autocomplete="off" required="required" name="date" class="form-control"  value="<?php echo date("m/d/Y");?>" readOnly  placeholder="Date *">
          <label></label>
          <span class="help-block">Date *</span> </div>
        </div>
		<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control date-picker" id="bill_date"  name="bill_date" placeholder="Bill Date *">
						<label></label>
						<span class="help-block">Bill Date*</span> </div>
				</div>
				
		<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" id="paymentt" name="payment_term" required onchange="paymentterm(this.value);">
							<option value="" selected="selected" disabled="disabled">Select Payment Term</option>
							<option value="immediate">Immediate</option>
							<option value="pia">PIA (Paid in advance)</option>
							<option value="net7">Net 7 (7 day credit)</option>
							<option value="net14">Net 14 (14 day credit)</option>
							<option value="net30">Net 30 (30 day credit)</option>
							<option value="net60">Net 60 (60 day credit)</option>
							<option value="net90">Net 90 (90 day credit)</option>
							<option value="cod">COD</option>
							<option value="billofex">Bill of exchange</option>
							<option value="contra">Contra</option>
							<option value="other">Specify Date</option>
						</select>
						<label></label>
						<span class="help-block">Payment Term</span> </div>
				</div>			
		<!--<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" id="payment_status" name="payment_status" required >
							<option value="" selected="selected" disabled="disabled">Select Payment Status</option>
							<option value="Due">Due</option>
							<option value="Partial">Partial</option>
							<option value="Paid">Paid</option>
						</select>
						<label></label>
						<span class="help-block">Payment Status</span> </div>
				</div>	-->
				
			<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control date-picker" id="delivery_date" required name="delivery_date" placeholder="Delivery Date *">
						<label></label>
						<span class="help-block">Delivery Date*</span> </div>
				</div>	
				
			<div id="bill_other_dt" class="col-md-4" style="display:none;">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control date-picker" id="specific_date" name="specific_date" placeholder="Specific Payment Date *">
						<label></label>
						<span class="help-block">Specific Payment Date *</span> </div>
				</div>		
				
        <?php if(isset($assets) && $assets){
            //print_r($assets);
            foreach($assets as $asset){
            // echo $asset->a_name;
          }  ?>
        <input type="hidden"  name="stock_inventory_id" value="<?php echo $asset->hotel_stock_inventory_id ;?>">
        <input type="hidden" id="stock_inventory_id" name="stock_id" value="<?php echo $asset->hotel_stock_inventory_id ;?>">
        <input type="hidden" id="qty1" name="qty1" value="">
        <!--<input type="hidden"  name="u_price"  id="u_price" value="">-->
        <input type="hidden" id="opening_qty" name="opening_qty" value="<?php echo $asset->opening_qty ;?>">
        <input type="hidden"  name="unit" value="<?php echo $asset->unit ;?>">
        <input type="hidden" id="closing_qty" name="closing_qty" value="<?php echo $asset->closing_qty ;?>">
        <input type="hidden" id="bill_due_date" name="bill_due_date" value="">
		<?php }?>
        
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <select class="form-control bs-select"  id="a_category" name="a_category" required onchange="get_item_name(this.value)" >
            <option value="">Select Category</option>
            <?php 
                if(isset($category)){
                    //print_r($category);
                    //exit;
                foreach($category as $categorys){
                        
        ?>
            <option value="<?php echo $categorys->name; ?>"><?php echo $categorys->name; ?></option>
            
            <?php 
                
                }}		
                ?>
          </select>
          <label></label>
          <span class="help-block">Category *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <div id="hd">
            <select class="form-control bs-select" id="a_name" name="a_name" required >
              <option value="">Select Item</option>
              <?php if(isset($assets) && $assets){
			
                foreach($assets as $asset){
                
            ?>
              <option value="<?php echo $asset->a_name;?>">
             
              </option>
              <?php } }?>
            </select>
            <label></label>
            <span class="help-block">Select Item *</span> </div>
          </div>
        </div>
      <!--  <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="link_purchase" placeholder="Link Purchase">
          <label></label>
          <span class="help-block">Link Purchase</span> </div>
        </div>  -->
        <div class="col-md-4">
       <!-- <div class="form-group form-md-line-input">          
          <input autocomplete="off" type="text" class="form-control " id="select_l_itm" name="item"  data-toggle="modal" onfocus="show_modal()" readOnly placeholder="Vendor">
          <input autocomplete="off" type="hidden" class="form-control " id="select_l_itm_id" name="select_l_itm_id">
          <input autocomplete="off" type="hidden" class="form-control " id="v_id" name="v_id">
          <label></label>
          <span class="help-block">Vendor</span> </div>-->
		  <div class="form-group form-md-line-input">
            <div class="typeahead__container">
              <div class="typeahead__field"> <span class="typeahead__query">
                <input class="js-typeahead-user_v1 form-control" name="vendor[query]" id="vendor" value="" type="search" placeholder="Search Vendor" autocomplete="off">
                <label></label>
				  <span class="help-block">Search Vendor</span>
                </span> </div>
            </div>
          </div>
       	</div>
		
		
         <div class="col-md-4" id="wh"> 
            <div class="form-group form-md-line-input">
          <select class="form-control bs-select"  name="warehouse" required >
          	<option value="" disabled="disabled" selected="selected">Select Warehouse</option>
             <?php if(isset($warehouse)){
            //print_r($warehouse);
            foreach($warehouse as $wh){
          ?>
            <option value="<?php echo $wh->id;?>"><?php echo $wh->name;?></option>
             <?php }}?>
            
          </select>
          <label></label>
          <span class="help-block">Select Warehouse *</span> </div>
        </div>
        
        <div class="col-md-4"> 
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" name="qty" id="qty" required="required" onblur="calculate()" placeholder="QTY *">
          <label></label>
          <span class="help-block">QTY *</span> </div>
        </div>
		 <div class="col-md-4"> 
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" onblur="calculate()" id="unit_price" name="unit_price" placeholder="Unit Price *">
          <label></label>
          <span class="help-block"> Unit Price *</span> </div>
        </div>
		
        <div class="col-md-4"> 
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="price" onblur="back_calculate(this.value)" name="price" placeholder="Price *">
          <label></label>
          <span class="help-block"> Total Price *</span> </div>
        </div>
        <div class="col-md-12">
        <div class="form-group form-md-line-input">
          <textarea autocomplete="off" type="text" class="form-control"  name="note" placeholder="Note *"></textarea>
          <label></label>
          <span class="help-block">Note *</span> </div>
        </div>
      </div>
    </div>
	 <input autocomplete="off" type="hidden" class="form-control" id="vendor_id" name="vendor_id">
	 <input autocomplete="off" type="hidden" class="form-control" id="item_name" name="item_name">
    <div class="form-actions right">
      <button type="submit" class="btn submit" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
  
  </div>
</div>
<div class="modal fade" id="basic1" tabindex="-1" role="basic" aria-hidden="true" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Select Vendor</h4>
      </div>
      <div class="modal-body">
       

<?php if(isset($vendor) && $vendor):
				
				//print_r($vendor);exit;
                $i=1;
                foreach($vendor as $dgs):
                    $class = ($i%2==0) ? "active" : "success";	
                   
                    ?>

  
   
  <p><input type="button" class="btn default blue" id="vendor" value="<?php echo $dgs->hotel_vendor_name; ?>" onclick="set_vendor('<?php echo $dgs->hotel_vendor_name; ?>','<?php echo $dgs->hotel_vendor_id; ?>')" > </button></p>
  
  

<?php endforeach; ?>
<?php endif; ?>
                    
                        
                        
       
      <div id="mdl2"> </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button type="button" class="btn submit">Save changes</button>
      </div>
    </div>
    
  </div>
 
</div>
</div>
<script>



function show_modal(){
	$('#basic1').modal('show');
}	
  

function set_vendor(a,b){
	$("#select_l_itm").val(a);
	$("#v_id").val(b);
	$('#basic1').modal('toggle');
	
}

function calculate() {
	var uprice = $('#unit_price').val();
	var qty = $('#qty').val();
	if(uprice!=''){
		var tprice = parseFloat(uprice) * parseFloat(qty);
	
	$('#price').val(tprice);
	$('#price').addClass('edited');
	}else{
		$('#unit_price').val('0');
		$('#price').val(0);
	}
	
}

function back_calculate(total){
	var qty = $('#qty').val();
	var tot=parseFloat(total);
	var unit_price=parseFloat(tot/qty);
	//alert(unit_price);
	$('#unit_price').val(unit_price);
}

   function check_corporate(value){

       if(value =="corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
       }else{
           document.getElementById("c_name").style.display="none";
           document.getElementById("c_des").style.display="none";
       }
   }
    function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});

var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
$(document).ready(function() {
	$("#mydate").datepicker("setDate", new Date());

});
function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	   //alert(y);
	    
	   if(y=="check"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fundtransfer"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }
   
	   function get_item_name(val){
	//alert(val);
	//alert(val1);
	
	// var val= document.getElementById("a_type").value;
	// var val1= document.getElementById("a_category").value;
	//alert(val);
	//alert(val1);
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/get_item_name",
	   data: {data1:val},
	   success: function(msg){
		   //alert(msg);
		    $("#hd").html(msg);
	   }
	});
}
function get_item_price(val){
	
	var asset=val.split("~");
	var a_name=asset[1];
	$('#item_name').val(a_name);
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/get_item_price",
	   data: {data:val},
	   success: function(msg){
		  // alert(msg);
		  var value=msg.split("**");
		  var  value2=value[1];
		  var  value3=value[0];
		//  alert(value3);
           //return false;
		  
		  var val=value2.split("_");
         // alert(val[0]);
          //alert(val[1]);
         // alert(val[2]);
         // alert(val[3]);
         // alert(val[4]);
			 $("#u_price").val(val[0]);
		    $("#qty1").val(val[2]);
			 $("#opening_qty").val(val[3]);
		    $("#closing_qty").val(val[4]);
			
			//$("#wh").html("<select name='warehouse' id='warehouse' class='form-control focus'> <option value='"+ val[1]+"'>"+val[2]+"</option></select> <label> Select Warehouse <span class='required'>*</span></label><span class='help-block'>Condition...</span>");
	   }
	});
}


 function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_found_item?f_id="+id,
                data:{booking_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
   
</script> 
<script>
	function fetch_item(value){
		
		       // alert("ggg");
		 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/fetch_item",
                data:{item: value},
                success:function(data1)
                {	
						//alert(data1);
						
						$('#mdl1').html(data1);
					//alert(data);
                }
            });

	}

function fetch_item1(value){
		
		       // alert("ggg");
		 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/fetch_lost_items",
                data:{item: value},
                success:function(data1)
                {	
						//alert(data1);
						$('#mdl2').html(data1);
					//alert(data.itm);
                }
            });

	}
   
    
	function getVal(val){
		var val_arr=val.split("-");
		//alert(val_arr[0]);
		//alert(val_arr[1]);
		document.getElementById("select_f_itm").value=val_arr[0];
		document.getElementById("select_f_itm_id").value=val_arr[1];
		
		$('#basic').modal('hide');
	}
	function getValue(val){
		document.getElementById("id").value=val;
	}
	function getVal1(val){
		var val_arr=val.split("-");
		//alert(val_arr[0]);
		//alert(val_arr[1]);
		document.getElementById("select_l_itm").value=val_arr[0];
		document.getElementById("select_l_itm_id").value=val_arr[1];

		document.getElementById("id").value=val;
		$('#basic1').modal('hide');
	}
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});

function paymentterm() {
	
			var y = document.getElementById( "paymentt" ).value;
			var bill_date = $( "#bill_date" ).val();

			if ( y == "other" ) {
				document.getElementById( "bill_other_dt" ).style.display = "block";
			} else if ( y == "immediate" ) {
				// alert(bill_date);
				var someDate = new Date();
				someDate.setDate( someDate.getDate()  );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;

				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				//alert(someDate);
				document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "cod" ) {
				// alert(bill_date);
				var someDate = new Date();
				someDate.setDate( someDate.getDate()  );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;

				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				//alert(someDate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			}else if ( y == "billofex" ) {
				// alert(bill_date);
				var someDate = new Date();
				someDate.setDate( someDate.getDate()  );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;

				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				//alert(someDate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			}
			
			else if ( y == "pia" ) {
				// alert(bill_date);
				var someDate = new Date();
				someDate.setDate( someDate.getDate());
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;

				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				//alert(someDate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			}
			
			else if ( y == "net7" ) {
				// alert(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 7 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;

				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				//alert(someDate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "net14" ) {
				// alert(bill_date);
				///var someDate = new Date(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 14 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;
				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				// alert(joindate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "net30" ) {
				// alert(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 30 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;
				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				// alert(joindate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "net60" ) {
				// alert(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 60 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;
				$( '#bill_due_date' ).val( fromdate1 );
				// alert(joindate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "net90" ) {
				// alert(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 90 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;
				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				// alert(joindate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			}
		}


</script> 
<script>

$.typeahead({
    input: '.js-typeahead-user_v1',
    minLength: 1,
    order: "asc",
    dynamic: true,
    delay: 200,
    backdrop: {
        "background-color": "#fff"
    },
   
    source: {
       project: {
            display: "name",
            ajax: [{
                type: "GET",
                url: "<?php echo base_url();?>dashboard/search_vendor",
                data: {
                    q: "{{query}}"
                }
            }, "data.name"],
            template: '<div class="clearfix">' +
						'<span class="project-img">' +
                    '<img src="{{image}}">' +
                '</span>' +
			          '<div class="project-information">' +
                     '<span class="pro-name" style="font-size:15px; "> {{name}}</span>' +
					 '<span class="pro-name" style="font-size:12px; color:#000"><i class="fa fa-map-marker" aria-hidden="true"></i> {{city}}</span>'+
					 '<span class="pro-name" style="font-size:12px; color:#666"><i class="fa fa-briefcase" aria-hidden="true"></i> {{industry}} - {{field}} </span>'+
					 //'<span class="pro-name" style="font-size:15px;"> {{contracted}}</span>'+
					 //'<span class="pro-name" style="font-size:15px;"> {{parent_vendor}}</span>'+
					 //'<span class="pro-name" style="font-size:15px;"> {{icon}}</span>'+
                '</div>' +
            '</div>'
        }
    },
    callback: {
        onClick: function (node, a, item, event) {
         // alert(JSON.stringify(item));
		// $("#c_unit_price").val(item.unitprice);
		$("#vendor_id").val(item.id);
		//$("#vendor_id").val(item.id);
		
		
		 
        },
        onSendRequest: function (node, query) {
            console.log('request is sent')
			
        },
        onReceiveRequest: function (node, query) {
            //console.log('request is received')
	
			
        },
		onCancel: function (node, query) {
            
			if(query!=''){
					$("#vendor_id").val('');
			}
        }	
		
    },
    debug: true
});

$( document ).ready(function(){
	toastr.optionsOverride = 'positionclass = "toast-bottom-right"';
		toastr.options.positionClass = 'toast-bottom-right';
		 
	})
</script>