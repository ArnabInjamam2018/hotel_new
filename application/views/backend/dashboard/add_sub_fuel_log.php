<script src="<?php echo base_url();?>assets/global/plugins/typeahead1/jquery.typeahead.js"></script>

<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<script>
function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}




function show_add()
   {
	   
   $('#showbill').show();
   $('#purchaseid').show();
   $('#prc').show();
   $('#slr').show();
   $('#nt').show();
   $('#sub_ac').hide();
   }
   
   function show_sub()
   {
   var fuel_id= $('#fuel').val();
   var qty= $('#quantity').val();
   //alert(qty); 
   $('#purchaseid').hide();
     $('#showbill').hide();
   $('#prc').hide();
   $('#slr').hide();
   $('#nt').show();
   $('#sub_ac').hide();
	$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/fetch_quantity",
                data:{fuel_id:fuel_id},
                success:function(data)
                { //alert(data.cur_stock) ; 
				//alert(data.reorder_level) ; 
				if(qty<= data.cur_stock){
                    if( data.cur_stock == data.reorder_level){
                    swal({
                            title: data.cur_stock,
                            text: "Low Fuel Quantity!",
                            type: "warning"
                        },
                        function(){

                           $('#quantity').val('');

                        });
					}
                }
				}
            });
   }
/*$(document).on('blur', '#stopdatepicker', function () {
	$('#stopdatepicker').addClass('focus');
});*/
function chk_if_num( a )
		{
				if(isNaN($('#'+a+'').val())==true)
				{
					//alert ("please enter a valid "+a);
					$('#'+a+'').val("");
					
				}			
		}
$(document).on('blur', '#stopdatepicker', function () {
	$('#stopdatepicker').addClass('focus');
});
</script>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add/Substract Fuel Log</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/add_sub_fuel_log',$form);

                        ?>
						
						
    <div class="form-body"> 
      <div class="row">
      	<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select" id="fuel" name="fuel" required="required" >
                <option value="" selected="selected" disabled="disabled">Fuel</option>
                <?php foreach($master_log as $mas) 
              {
				  ?>
        <option value="<?php echo $mas->id.'~'.$mas->fuel_name;?>" <?php if(isset($f_id)){if($mas->id==$f_id){ echo "selected";}}?>><?php echo $mas->fuel_name; ?></option>";
			<?php	
		   }
        ?>
              </select>
              <label></label>
              <span class="help-block">Fuel *</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" readonly id="stopdatepicker" value="<?php echo date('d/m/Y');?>" name="o_date" required placeholder="Date *">
              <label></label>
              <span class="help-block">Date *</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input" >
              <input autocomplete="off" type="text" class="form-control timepicker timepicker-24" id="stime" name="o_time" required placeholder="Time *">
              <label></label>
              <span class="help-block">Time *</span>
            </div>
        </div>
		<div id="showbill" style="display:none;">
		<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control date-picker" id="bill_date"  name="bill_date" placeholder="Bill Date *">
						<label></label>
						<span class="help-block">Bill Date*</span> </div>
				</div>
		<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" id="paymentt" name="payment_term" required onchange="paymentterm(this.value);">
							<option value="" selected="selected" disabled="disabled">Select Payment Term</option>
							<option value="immediate">Immediate</option>
							<option value="pia">PIA (Paid in advance)</option>
							<option value="net7">Net 7 (7 day credit)</option>
							<option value="net14">Net 14 (14 day credit)</option>
							<option value="net30">Net 30 (30 day credit)</option>
							<option value="net60">Net 60 (60 day credit)</option>
							<option value="net90">Net 90 (90 day credit)</option>
							<option value="cod">COD</option>
							<option value="billofex">Bill of exchange</option>
							<option value="contra">Contra</option>
							<option value="other">Specify Date</option>
						</select>
						<label></label>
						<span class="help-block">Payment Term</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control date-picker" id="delivery_date" required name="delivery_date" placeholder="Delivery Date *">
						<label></label>
						<span class="help-block">Delivery Date*</span> </div>
				</div>
		<div id="bill_other_dt" class="col-md-3" style="display:none;">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control date-picker" id="specific_date" name="specific_date" placeholder="Specific Payment Date *">
						<label></label>
						<span class="help-block">Specific Payment Date *</span> </div>
				</div>		
				<input type="hidden" id="bill_due_date" name="bill_due_date" value="">
		</div>		
        <div class="col-md-4">
            <div class="form-group form-md-line-input" id="qty" >
              <input autocomplete="off" type="text" id="quantity" class="form-control" name="quantity" id="quantity" onKeyup="chk_if_num('quantity')" required="required" placeholder="Fuel Quantity *">
              <label></label>
              <span class="help-block">Fuel Quantity *</span>
            </div>
        </div>
        <div class="col-md-4 form-horizontal" style="padding-top:14px;">
            <div class="form-group form-md-line-input">
              <label class="col-md-6 control-label">Operation Type</label>
              <div class="col-md-6">
              <div class="md-radio-inline">
                <div class="md-radio">
                  <input type="radio" id="add" name="mode" value="add" onClick="show_add();" class="md-radiobtn" required="required">
                  <label for="add"> <span></span> <span class="check"></span> <span class="box"></span> Add </label>
                </div>
                <div class="md-radio">
                  <input type="radio" id="sub"name="mode" value="sub" onClick="show_sub();" class="md-radiobtn">
                  <label for="sub"> <span></span> <span class="check"></span> <span class="box"></span> Sub </label>
                </div>
              </div>
              </div>
            </div>
        </div>
        <div class="col-md-4" id="purchaseid" style="display:none;">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select"  name="purchaseid"    id="p_id">
                <option value="" selected="selected" disabled="disabled">Purchase ID</option>
                <?php if(isset($p_id) && $p_id){
                                                foreach($p_id as $pid){
                                                ?>
                <option value="<?php echo $pid -> p_id;?>"><?php echo $pid -> p_id;?></option>
                <?php }}?>
              </select>
              <label></label>
              <span class="help-block">Purchase ID *</span>
            </div>
        </div>
        <div class="col-md-4" id="prc" style="display:none;">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" id="price" class="form-control" onKeyup="chk_if_num('price')" name="price" placeholder="Fuel Price *">
              <label></label>
              <span class="help-block">Fuel Price *</span>
            </div>
        </div>
        <div class="col-md-4" id="slr" style="display:none;">
           <div class="form-group form-md-line-input">          
				  <div class="typeahead__container">
           <div class="typeahead__field">
			<span class="typeahead__query">
     <input class="js-typeahead-user_v1 form-control" name="vendor[query]" id="vendor" value="" type="search" placeholder="Search" autocomplete="off">
            </span>
          
        </div>
    </div>
				</div>
        </div>
        <div class="col-md-4" id="nt" style="display:none;">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" id="note" class="form-control" name="note" placeholder="Note *">
              <label></label>
              <span class="help-block">Note *</span>
            </div>
        </div>
        <div class="col-md-4" id="sub_ac">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" id="sub_account" class="form-control" name="sub_account" placeholder="Substraction on Account *">
              <label></label>
              <span class="help-block">Substraction on Account *</span>
            </div>
        </div>
      </div>
    </div>
	 <input autocomplete="off" type="hidden" id="vendor_id" name="vendor_id" class="form-control" name="note" placeholder="Note *">
    <div class="form-actions right">
      <button type="submit" class="btn submit" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
  </div>
  <!-- END CONTENT --> 
</div>






<script>
function get_id(val){
	
	var mode=$('#sub').val();
	alert(mode);
}
function set_vendor(a){
	$("#seller").val(a);
	$('#basic1').modal('toggle');
	
}

   function check_Cylinders(value){
   		
       if(value =="Cylinders"){

           document.getElementById("c_choice").style.display="block";
       }else{
           document.getElementById("c_choice").style.display="none";
       }
      	if(value =="Rotation"){

           document.getElementById("r_choice").style.display="block";
       }else{
           document.getElementById("r_choice").style.display="none";
       }
       if(value =="Design"){

           document.getElementById("d_choice").style.display="block";
       }else{
           document.getElementById("d_choice").style.display="none";
       }
   }
   function ShowHideDiv() {
	   //alert("here");
        var add = document.getElementById("add");
        var purchaseid = document.getElementById("purchaseid");
        var sub = document.getElementById("sub");
		var qty = document.getElementById("qty");
        purchaseid.style.display = add.checked ? "block" : "none";
        qty.style.display = sub.checked ? "block" : "none";
		document.getElementById("sqty").disabled = false;
    }
	function soft_delete(){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){


		var p_id=document.getElementById('p_id').value;
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/select_quantity?p_id="+p_id,
                data:{p_id:p_id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
	</script> 
<script type="text/javascript"> 
	function check_date()
			{
			//alert ($('#stopdatepicker').val());
	
            var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			//alert ("hello");
			 var curdate=new Date(mm+"/"+dd+"/"+yyyy);
			 var date2 = new Date($('#stopdatepicker').val());
			var dif= (curdate-date2);
			//alert (dif);
			 if(dif <=0)
				{
				alert ("Future date not allowed");
				$('#stopdatepicker').focus();
				$('#stopdatepicker').val("");
				}
	
	
			}
   
   
  
   
</script> 
<script>
	function fetch_item(value){
		
		       // alert("ggg");
		 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/fetch_item",
                data:{item: value},
                success:function(data1)
                {	
						//alert(data1);
						
						$('#mdl1').html(data1);
					//alert(data);
                }
            });

	}

function fetch_item1(value){
		
		       // alert("ggg");
		 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/fetch_lost_items",
                data:{item: value},
                success:function(data1)
                {	
						//alert(data1);
						$('#mdl2').html(data1);
					//alert(data.itm);
                }
            });

	}
   
    
	function getVal(val){
		var val_arr=val.split("-");
		//alert(val_arr[0]);
		//alert(val_arr[1]);
		document.getElementById("select_f_itm").value=val_arr[0];
		document.getElementById("select_f_itm_id").value=val_arr[1];
		
		$('#basic').modal('hide');
	}
	function getValue(val){
		document.getElementById("id").value=val;
	}
	function getVal1(val){
		var val_arr=val.split("-");
		//alert(val_arr[0]);
		//alert(val_arr[1]);
		document.getElementById("select_l_itm").value=val_arr[0];
		document.getElementById("select_l_itm_id").value=val_arr[1];

		document.getElementById("id").value=val;
		$('#basic1').modal('hide');
	}
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});


function paymentterm() {
	
			var y = document.getElementById( "paymentt" ).value;
			var bill_date = $( "#bill_date" ).val();

			if ( y == "other" ) {
				document.getElementById( "bill_other_dt" ).style.display = "block";
			} else if ( y == "immediate" ) {
				// alert(bill_date);
				var someDate = new Date();
				someDate.setDate( someDate.getDate()  );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;

				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				//alert(someDate);
				document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "cod" ) {
				// alert(bill_date);
				var someDate = new Date();
				someDate.setDate( someDate.getDate()  );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;

				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				//alert(someDate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			}else if ( y == "billofex" ) {
				// alert(bill_date);
				var someDate = new Date();
				someDate.setDate( someDate.getDate()  );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;

				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				//alert(someDate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			}
			
			else if ( y == "pia" ) {
				// alert(bill_date);
				var someDate = new Date();
				someDate.setDate( someDate.getDate());
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;

				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				//alert(someDate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			}
			
			else if ( y == "net7" ) {
				// alert(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 7 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;

				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				//alert(someDate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "net14" ) {
				// alert(bill_date);
				///var someDate = new Date(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 14 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;
				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				// alert(joindate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "net30" ) {
				// alert(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 30 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;
				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				// alert(joindate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "net60" ) {
				// alert(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 60 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;
				$( '#bill_due_date' ).val( fromdate1 );
				// alert(joindate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			} else if ( y == "net90" ) {
				// alert(bill_date);
				var someDate = new Date( bill_date );
				someDate.setDate( someDate.getDate() + 90 );
				var dd = someDate.getDate();
				var mm = someDate.getMonth() + 1; //January is 0!
				var yyyy = someDate.getFullYear();
				if ( dd < 10 ) {
					dd = '0' + dd;
				}
				if ( mm < 10 ) {
					mm = '0' + mm;
				}
				var fromdate1 = mm + '/' + dd + '/' + yyyy;
				$( '#bill_due_date' ).val( fromdate1 );
				$( '#payments_due_date' ).val( fromdate1 );
				// alert(joindate);
			document.getElementById( "bill_other_dt" ).style.display = "none";
			}
		}




</script> 
<script>

$.typeahead({
    input: '.js-typeahead-user_v1',
    minLength: 1,
    order: "asc",
    dynamic: true,
    delay: 200,
    backdrop: {
        "background-color": "#fff"
    },
   
    source: {
       project: {
            display: "name",
            ajax: [{
                type: "GET",
                url: "<?php echo base_url();?>dashboard/search_vendor",
                data: {
                    q: "{{query}}"
                }
            }, "data.name"],
            template: '<div class="clearfix">' +
						'<span class="project-img">' +
                    '<img src="{{image}}">' +
                '</span>' +
			          '<div class="project-information">' +
                    '<span class="pro-name" style="font-size:15px;"> {{name}}</span>' +
					 '<span class="pro-name" style="font-size:15px;"> {{city}}</span>'+
					 '<span class="pro-name" style="font-size:15px;"> {{industry}}</span>'+
					 '<span class="pro-name" style="font-size:15px;"> {{contracted}}</span>'+
					 '<span class="pro-name" style="font-size:15px;"> {{parent_vendor}}</span>'+
					 '<span class="pro-name" style="font-size:15px;"> {{icon}}</span>'+
                '</div>' +
            '</div>'
        }
    },
    callback: {
        onClick: function (node, a, item, event) {
         // alert(JSON.stringify(item));
		// $("#c_unit_price").val(item.unitprice);
		$("#vendor_id").val(item.id);
		//$("#vendor_id").val(item.id);
		
		
		 
        },
        onSendRequest: function (node, query) {
            console.log('request is sent')
			
        },
        onReceiveRequest: function (node, query) {
            //console.log('request is received')
	
			
        }
    },
    debug: true
});	

</script>