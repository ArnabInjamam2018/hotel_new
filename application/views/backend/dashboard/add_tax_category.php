<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>

<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Details of Tax Category</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/tax_type_category_add',$form);

                        ?>
						
						
    <div class="form-body"> 
      <div class="row">
      	<div class="col-md-4">
            <div class="form-group form-md-line-input">
        <input type="text" autocomplete="off" class="form-control" id="tax_cate_name" name="tax_cate_name" required="required" placeholder="Tax Name *" onblur="get_chrage_name(this.value)">

              <label></label>
              <span class="help-block">Tax Category Name *</span>
            </div>
        </div>
        <div class="col-md-4">
		 <div class="form-group form-md-line-input" >
		 <select class="form-control"  name="tax_cate_type" id="tax_cate_type">
					 
					  <option value="Booking">Booking</option>
					  <option value="Meal Plan">Meal Plan</option>
					  <option value="Extra Charge">Extra Charge</option>
					  <option value="Extra Service">Extra Service</option>
					  <option value="POS">POS</option>
					  <option value="Misc">Misc</option>
					 
					</select>
		<!--<input type="text" autocomplete="off" class="form-control" id="tax_cate_type" name="tax_cate_type" required="required" placeholder="Tax Category Type *" onblur="get_chrage_name(this.value)">-->
			  <label></label>
              <span class="help-block">Tax Category Type *</span>
            </div>
           
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
			 <input type="text" autocomplete="off" class="form-control" id="tax_cate_desc" name="tax_cate_desc" required="required" placeholder="Tax Category Description *" onblur="get_chrage_name(this.value)">
              <label></label>
              <span class="help-block">Tax Category Derscription *</span>
            </div>
        </div>
       
       
       
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn submit" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
  </div>
  <!-- END CONTENT --> 
</div>
<div class="modal fade" id="basic1" tabindex="-1" role="basic" aria-hidden="true" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Select Seller</h4>
      </div>
      <div class="modal-body">        

 <input type="button" class="btn default blue" id="vendor" value="<?php echo $dgs->hotel_vendor_name; ?>" onclick="set_vendor('<?php echo $dgs->hotel_vendor_name; ?>')"> </button>

      <div id="mdl2"> </div>
      
    </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button type="button" class="btn blue">Save changes</button>
      </div>
  </div>
  <!-- /.modal-dialog --> 
</div>
</div>

<script>
$(document).ready(function(){
	
	//alert('hello');
	$('#tax_category').on('change',function(){
		
		var category = $('#tax_category').val();
		//alert(category);
		$.ajax({
			
			type:"POST",
			url:"<?php echo base_url()?>dashboard/tax_ajax",
			data:{t_cate:category},
			success:function(data){
				
				//console.log(data);
				//alert(data);
				$('#tax_type1').html(data);
				
			},
			
		});
	});
	

});

</script>




