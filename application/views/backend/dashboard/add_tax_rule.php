<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Define Tax Rule</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                            $form = array(
                                'class' 			=> '',
                                'id'				=> 'form',
                                'method'			=> 'post',								
                            );	

                            echo form_open_multipart('dashboard/add_tax_rule',$form);

                            ?>
    <div class="form-body">      
      <div class="row"> 	   
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input name="r_name" id="r_name" type="text" class="form-control" placeholder="Rule name *"  required/>
            <label></label>
            <span class="help-block">Rule name *</span> </div>
      </div>      
	  <div class="col-md-3">
	  <div class="form-group form-md-line-input">
	  <select class="form-control input-sm" name="tax_cat" id="tax_cat" required>
			<option value="" disabled selected >Select Tax Category Name *</option>
			 <?php 
						$tag = $this->dashboard_model->get_distinct_tax_category();
						if(isset($tag)){
							foreach($tag as $tag){

								echo '<option value="'.$tag->tc_id.'">'.$tag->tc_name.''.' '.' ('.$tag->tc_type.')'.'</option>';
							}	
						}
					  ?>
			</select>
		</div>
		</div>	
        <div class="col-md-3">
          <div class="form-group form-md-line-input" id="defaultrange1">
            <input name="r_start_dates" id="r_start_dates" type="text" class="form-control" placeholder="Date Range *" required="required">
            <!--
			<span class="input-group-addon"> to </span>
            <input name="r_end_date" id="r_end_date" type="text" class="form-control" placeholder="Ending Date Range" required>
			
			-->
            <label></label>
            <span class="help-block">Date Range *</span>
          </div>
        </div> 	
		<div class="col-md-3">        
          <div class="form-group form-md-line-input">
            <input name="r_start_price" id="r_start_price" type="number" min="0" class="form-control" placeholder="Starting Price Range*" required />
            <label></label>
            <span class="help-block">Starting Price Range *</span> </div>
        </div>
		<div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input name="r_end_price" id="r_end_price" type="number" min="0" class="form-control" placeholder="Ending Price Range *"  required />
            <label></label>
            <span class="help-block">Ending Price Range *</span> </div>
			<input type="text" id="taxes" name="taxes" style="display:none;"/>
        </div>
		<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="gst" id="gst" required>
							<option value="" disabled selected>Select SGST/ IGST *</option>
							<option value="1">SGST </option>
							<option value="0">IGST </option>
						</select>
						<label></label>
						<span class="help-block">Select SGST/ IGST *</span>
					</div>
				</div>
		<div class="col-md-12">
          <div class="form-group form-md-line-input">
            <input name="r_desc" id="r_desc" type="text" class="form-control" placeholder="Rule Description" required/>
            <label></label>
            <span class="help-block">Rule Description</span> </div>
        </div>
		
		<div class="col-md-12">
		
      <!-- <div class="form-group form-md-line-input"> -->
	   <br></br>
	   <h4 style="color:#00564D;"><i class="fa fa-book" aria-hidden="true"></i> Tax Elements</h4>
	  <table class="table table-striped table-bordered table-hover table-scrollable" id="tax_items">
	  <thead>
	  <tr>
	  <th style="text-align:left;">Tax type</th>
	  <th style="text-align:left;">Tax Mode</th>
	  <th style="text-align:left;">Applied On</th>
	  <th style="text-align:left;">Calculated On</th>
	  <th style="text-align:left;">Value</th>
	  <th style="text-align:left;">Action</th>
	  </tr>
	  </thead>
	  <tbody>
	  <tr>
	  <td class="form-group">
        
            <select class="form-control input-sm" name="tax_type_5" id="tax_type" >
			<option value="" disabled selected>Select Tax Type</option>
			<?php $tax = $this->dashboard_model->get_distinct_tax_type();  // Getting the tax types  
				if($tax != false)
				{
					foreach($tax as $tx)
					{
					?>
					<option value="<?php echo $tx->tax_id; ?>"><?php echo $tx->tax_name; ?></option>
					<?php
					}			
				}?>
			</select>
            
		</td>
		<td class="form-group">
		
            <select class="form-control input-sm" name="tax_mode_5" id="tax_mode" >
			<option value="" disabled selected >Select mode</option>
			<option value="f" >Fixed</option>
			<option value="p" >Percentage (%)</option>
			</select>
            
		</td>
		
		<td class="form-group">
		
            <select class="form-control input-sm" name="applied_on_5" id="applied_on" >
			<option  value="" disabled selected>Applied on</option>
			<option value="b_a" >Booking Amount</option>
			<option value="p_a" >Pos Amount</option>
			<option value="f_p" disabled >Food Plan</option>         
			<option value="t_a" >Total Amount</option>
			</select>
           
		</td>
		<td class="form-group">
		
            <select class="form-control input-sm" name="calculated_on_5" id="calculated_on" >
			<option value="" disabled selected >Calculated on</option>
			<option value="n_r" >Net rate</option>
			<option value="n_p" >Net rate + Prev. tax</option>
			<option value="n_p_d" >Net rate + Prev. tax - Discount</option>
			</select>
           
		</td>
		<div id="value_change">
			<td class="form-group ">
			
			   <input type="number" min="0" name="tax_value_5" class="form-control input-sm" placeholder="Value" id="tax_value"/ >
			   
			</td>
		</div> 
		<td class="form-group">
		<a class="btn green btn-md" id="add_tax_rule" onclick="add_tax_item()"><i class="fa fa-plus"></i></a>
		</td>
        </tr>
		</tbody>
		</table>
		<div class="col-md-12">
       
      </div>
    <!--</div>-->
	</div>
    </div></div>
    <div class="form-actions right">
      <button type="submit" class="btn submit" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
   
  </div>
</div>


<!-- Modal addition ---------------->

<!-- End of modal add---------->

<script>
var x = 0;
function add_tax_item()
{
	var tax_type = $("#tax_type").val();
	var tax_type_text = $("#tax_type option:selected").text()
	
	var tax_mode =$("#tax_mode").val();
	var tax_mode_text = $("#tax_mode option:selected").text()
	
	var tax_value = $("#tax_value").val();
	
	var calculated_on = $("#calculated_on").val();
	var calculated_on_text = $("#calculated_on option:selected").text()
	
	var applied_on = $("#applied_on").val();
	var applied_on_text = $("#applied_on option:selected").text();
	
	//alert(tax_type+"_"+tax_mode+"_"+tax_value+"_"+calculated_on+"_"+applied_on);
	
	//return false;
	var flag = 0;
		var it=$('#taxes').val();
		var res = it.split(',');
		
		var l = 0;
    for(l = 0; l < res.length ; l++)
		{
			//alert(res[l]);
			if (res[l] == tax_type)
			{
				
				flag++;
			}
		}
	if( flag == 0)
	{
		if(
		$("#tax_type").val() != null &&
		$("#tax_mode").val() != null &&
		$("#tax_value").val() != "" &&
		$("#calculated_on").val("") != null &&
		$("#applied_on").val("") != null
		)
		{
			$('#tax_items tr:last').after('<tr id="row_'+x+'" >'+
			'<td><select name="tax_type[]" class="form-control input-sm" ><option value="'+tax_type+'" selected>'+tax_type_text+'</option></select></td>'+
			'<td><select name="tax_mode[]" class="form-control input-sm" ><option value="'+tax_mode+'" selected  >'+tax_mode_text+'</option></select></td>'+
			
			'<td ><select name="calculated_on[]" class="form-control input-sm"  ><option value="'+calculated_on+'" selected>'+calculated_on_text+'</option></select></td>'+
			'<td ><select name="applied_on[]" class="form-control input-sm"  ><option value="'+applied_on+'" selected>'+applied_on_text+'</option></select></td>'+
			'<td ><input name="tax_value[]" type="text" value="'+tax_value+'" class="form-control input-sm" readonly></td>'+
			'<td><a href="javascript:void(0);" class="btn red btn-md" onclick="removeRow('+x+','+tax_type+')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>'+
			
			'</tr>');	
			
			x=x+1;
			
			swal("Success", "Tax rule added", "success")
		
			$('#taxes').val($('#taxes').val()+','+tax_type);
			
			$('#tax_type').prop('selectedIndex',0);
			$('#tax_mode').prop('selectedIndex',0);
			$("#tax_value").val("");
			$('#calculated_on').prop('selectedIndex',0);
			$('#applied_on').prop('selectedIndex',0);
		}
		else
		{
			swal("Check Carefully", "Please Fill all the fields preoperly", "info") 
		}
		
	}
	else
	{
	swal("Check Carefully", "This Tax type is already added", "info")	
	}
}	
</script>
<script>
function removeRow(a,tax_type)
{
	swal({   title: "Are you sure?",   text: "You will not be able to recover this record!",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){ 
			 text = $('#taxes').val();
			 text = text.replace(","+tax_type,"");
			 $('#taxes').val(text); 
			 $('#row_'+a+'').remove();  
			 swal("Deleted!", "The tax Rule has been deleted", "success"); });
	
}

</script>

<script>

$(document).ready(function(){
	
	
	var cat='';
	var date='';
	
	
	 
	//alert(date);
	var	start_price='';
	$('#r_start_price').on('keyup',function(){
		
		cat = $('#tax_cat').val();
		date  = $('#r_start_dates').val();
		start_price = $('#r_start_price').val();
		//alert(price);
		
	});
	var	end_price='';
	$('#r_end_price').on('keyup',function(){
		
		end_price = $('#r_end_price').val();
		});
	$('#r_end_price').on('blur',function(){
		//alert(end_price);
		if(parseInt(start_price) >= parseInt(end_price)){
			
			$('#r_end_price').val("");
			swal("Alert!", "Starting Price cannot be greater or equal to ending price", "error");
		}
		else{
			//alert(date);
				$.ajax({
				
				type:"POST",
				url:"<?php echo base_url();?>dashboard/tax_rule_ajax",
				data:{category:cat,start_date:date,start_price:start_price,end_price:end_price},
				success:function(data){
					//alert(data);
					if(data=='EXSISTS'){
					swal("Alert!",  "Already rule is defined under this category", "error");
					$('#r_start_dates').val('');
					$('#r_start_price').val("");
					$('#r_end_price').val("");
					$('#r_start_dates').focus();
					}
					$('#responsive').modal('show');
					
				},
			});
			
		}
		
		
	});
		
	
	
	
});
</script>


