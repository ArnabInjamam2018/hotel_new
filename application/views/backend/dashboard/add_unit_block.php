<?php if($this->session->flashdata('err_msg')):?>
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <span class="caption-subject bold uppercase"><i class="glyphicon glyphicon-bed"></i>&nbsp; Add Unit Block</span> </div>
  </div>
  <div class="portlet-body form">    
    <form action="<?php echo base_url();?>dashboard/add_unit_block" method="post" enctype="multipart/form-data" id="form">
      <div class="form-body">
        <div class="row">
        	<div class="col-md-12">
              <div class="form-group form-md-line-input">
                <input type="text" id="form_control_1" class="form-control" name="name" placeholder="Regular input">
                <label></label>
                <span class="help-block">Regular input</span> 
              </div>
          	</div>
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <textarea rows="3" class="form-control" name="desc" placeholder="Textarea input"></textarea>
                <label></label>
                <span class="help-block">Textarea input</span>
              </div>
          	</div>
          <div class="col-md-12">
            <div class="form-group form-md-checkboxes form-md-line-input">
              <label style=" font-size:16px;">Available Rooms</label>
              <div class="md-checkbox-list">
                <?php 
				if(isset($room_feature) && $room_feature){
				foreach($room_feature as $qry){ ?>
                <div class="col-md-1">
                  <div class="md-checkbox">
                    <input type="checkbox" id="chk_<?php echo $qry->room_no; ?>" class="md-check" name="room_no[]" value="<?php echo $qry->room_no;?>" >
                    <label for="chk_<?php echo $qry->room_no; ?>"> <span></span> <span class="check"></span> <span class="box"></span> <?php echo $qry->room_no; ?> </label>
                  </div>
                </div>
                <?php } }?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="form-actions right">
        <button type="submit" class="btn submit">Submit</button>
        <button  type="reset" class="btn default">Reset</button>
      </div>
    </form>
    <!-- END CONTENT --> 
    
  </div>
  <!-- END PAGE CONTENT--> 
</div>
<script>
function get_unit_name(val){
	//alert(val);
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/get_unit_name",
	   data: "val="+val,
	   success: function(msg){
		    $("#unitName").html(msg);
	   }
	});
}
function get_unit_class(val){
	//alert(val);
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/get_unit_class",
	   data: "val="+val,
	   success: function(msg){
		    $("#unitClass").html(msg);
	   }
	});
}
function showType(val){
	//alert(val);
	$("#featureType_"+val).toggle();
}
$(document).ready(function () {
	$('.showhidetarget').hide();
});

function getVal(id,val){
	//alert(id);
	//alert(val);
	if(val == '1'){
	  document.getElementById("samt_"+id).style.display="block";
	  document.getElementById("uamt_"+id).value = "1";
	} else {
	  document.getElementById("uamt_"+id).value = "0";	
	  document.getElementById("amt_"+id).value = "";	
	  document.getElementById("samt_"+id).style.display="none";	
	 
	}
}

function get_additional_value(val){
	if(val == '1'){
	  document.getElementById("smt").style.display="block";
	} else {
	  document.getElementById("smt").style.display="none";	
	 
	}
}
</script> 
<!-- END CONTENT -->
<!--All Code done before-->
</div>
</div>
<!-- modal add under-->