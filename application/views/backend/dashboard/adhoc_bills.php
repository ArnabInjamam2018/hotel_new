<?php if($this->session->flashdata('err_msg')):?>
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>All Adhoc Billings</div>
    <div class="actions">
    	<a href="<?php  echo base_url()?>dashboard/add_adhoc_bill" class="btn btn-circle green btn-outline btn-sm" data-toggle="modal"><i class="fa fa-plus"></i>Add New </a>
    </div>
  </div> 
  <div class="portlet-body">
    
    <table class="table table-striped table-bordered table-hover" id="sample_3">
      <thead>
        <tr> 
         
          <th scope="col"> #</th>          
          <th scope="col"> Bill Date</th>          
		  <th scope="col"> Type</th>
		  <th scope="col"> Status</th>
          <th scope="col"> Guest Type </th>
          <th scope="col"> Guest Name </th>
          <th scope="col"> Item Count </th>
          <th scope="col"> Total</th>
          <th scope="col"> Disc</th>
          <th scope="col"> CGST</th>
          <th scope="col"> SGST </th>
          <th scope="col"> IGST </th>		            
		  <th scope="col"> UTGST </th>
		  <th scope="col"> Adj</th>
          <th scope="col"> Grand Total </th>  
		  <th scope="col"> Payment Status</th>
		  <th scope="col"> Total Amount Paid</th>		  
          <th scope="none">Bill Note</th>
          <th scope="col"> Action </th>
        </tr>
      </thead>
	  
      <tbody>	
	  
        <?php 
			if(isset($Ahb_data) && $Ahb_data){
				/*echo "<pre>";
				print_r($Ahb_data);
				exit;
				*/
                $i=1;						
				$cntr = 0;
                foreach($Ahb_data as $abh_key){	
				
					$cntr++;
                    
        ?>
		
        <tr id="row<?php /*echo $abh_key->laundry_id;*/ ?>">
		
			<td><?php echo $cntr ?></td>
			
			<td>
				<?php 
					echo $abh_key->bill_date;
				?>
			</td>
			
			<td>
				<?php 
					echo $abh_key->status;
				?>
			</td>
			
			<td>
				<?php 
					echo $abh_key->service_type;
				?>
			</td>
			
			<td><?php echo $abh_key->guest_type; ?></td>

			<td><?php echo $abh_key->guest_name; ?></td>

			<td>
				<?php 
					$lineData = $this->dashboard_model->get_adhoc_bill_line_byID($abh_key->ahb_id);
					
					if(isset($lineData)){
						
						$cntL = 0;
						$cgst = 0;
						$sgst = 0;
						$igst = 0;
						$utgst = 0;
						$totalL = 0;
						$discL = 0;
						$adjL = 0;
						$totL = 0;
						$paySt = '';
						
						foreach($lineData as $lineKey){
							$cntL++;
							$cgst = $cgst + $lineKey->cgst*$lineKey->qty*$lineKey->rate/100;
							$sgst = $sgst + $lineKey->sgst*$lineKey->qty*$lineKey->rate/100;
							$igst = $igst + $lineKey->igst*$lineKey->qty*$lineKey->rate/100;
							$utgst = $utgst + $lineKey->utgst*$lineKey->qty*$lineKey->rate/100;
							$totalL = $totalL + $lineKey->total;
							$discL = $discL + $lineKey->disc;
							$adjL = $adjL + $lineKey->adjustment;
							$totL = $totL + $lineKey->qty*$lineKey->rate;
							
						}
					}
					
					echo $cntL;
					
				?>
			</td>

			<td><?php echo $totL; ?></td>
			
			<td><?php echo $discL; ?></td>

			<td><?php echo $cgst; ?></td>

			<td><?php echo $sgst; ?></td>

			<td><?php echo $igst; ?></td>
			
			<td><?php echo $utgst; ?></td>
			
			<td><?php echo $adjL; ?></td>
			
			<td><?php echo $totalL; ?></td>		            
			
			<td><?php echo $abh_key->payment_status; ?></td>
			
			<td>
				<?php 
					
					$tranData = $this->dashboard_model->all_transaction_adhoc_byID($abh_key->ahb_id);
					$tranL = 0;
					
					if(isset($tranData)){
						
						foreach($tranData as $keyTr){
							
							if($keyTr->t_status == 'Done' || $keyTr->t_status == 'done')
								$tranL = $tranL + $keyTr->t_amount;
							
						}
					}
					
					echo $tranL; 
				?>
			</td>
			
			<td><?php echo $abh_key->bill_note; ?></td>
			
			<td class="ba">
				<div class="btn-group">
				  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
				  <ul class="dropdown-menu pull-right" role="menu">
					<li><a onclick="soft_delete('<?php echo $abh_key->id; ?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
					<li><a href="<?php echo base_url() ?>unit_class_controller/view_adhoc_bill?id=<?php echo $abh_key->ahb_id; ?>" data-toggle="modal" class="btn blue btn-xs"><i class="fa fa-edit"></i></a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/invoice_adhoc_bill?id=<?php echo $abh_key->ahb_id; ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-cloud-download"></i></a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/view_adhoc_bill?id=<?php echo $abh_key->ahb_id; ?>" data-toggle="modal" class="btn v-bill btn-xs"><i class="fa fa-eye"></i></a></li>
				  </ul>
				</div>
			</td>
			
        </tr>
		<?php 
		}
		}
		?>
       
      </tbody>
    </table>
  </div>
</div>

<script>

$(document).on('blur', '#ced', function () {
	$('#ced').addClass('focus');
});

function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/delete_laundry_service",
                data:{id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            $('#row'+id).remove();

                        });
                }
            });
        });
}
</script>