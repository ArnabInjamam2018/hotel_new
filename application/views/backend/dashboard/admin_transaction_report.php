<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light borderd">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-flag"></i> Admin Transaction Summary Report </div>
    <div class="tools"> <a href="javascript:;" class="reload"></a> </div>
  </div>
  <div class="portlet-body">
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr>
         <th width="2%" scope="col"> Id </th>
          <th scope="col"> Date </th>
          <th scope="col"> Admin Name </th>
          <th scope="col"> Hotel Name</th>
		  <th scope="col"> Category</th>
          <th scope="col"> Transaction type</th>
          <th scope="col"> Transaction mode</th>
          <th scope="col"> Amount</th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($transaction_report) && $transaction_report){
					  $d = '';
                      $i = 1;
                      foreach($transaction_report as $tr){
						  $i++;
                          //$class = ($i%2==0) ? "active" : "success";
                          //$g_id=$gst->hotel_unit_class_id;
                          ?>
        <tr>
         
          <td align="left"><?php echo $i ?></td>
          
          <td align="left">
			<?php 
				if($d != $tr->t_date)
					echo '<span style="color:#0277BD; font-weight:bold;">'.date("dS M, Y",strtotime($tr->t_date)).'</span>';
				else{
					echo '<span style="display:block;">'.date("dS M, Y",strtotime($tr->t_date)).'</span>';
					//echo '<i style="color:#E0F2F1;" class="fa fa-window-minimize" aria-hidden="true"></i>';
				}
				$d = $tr->t_date;
			?>
		  </td>
		  
		  <td align="left"><?php echo $tr->admin_name; ?></td>
          <td align="left"><?php echo $hotel_name->hotel_name; ?></td>
          
          <td align="left">
			<?php 
				echo $tr->hotel_transaction_type_cat; 
			?>
		  </td>
		  <td align="left"><?php echo $tr->hotel_transaction_type_name; ?></td>
          <td align="left"><?php echo $tr->p_mode_des; ?></td>
          <td align="left"><?php echo '<i class="fa fa-inr" aria-hidden="true"></i> '.$tr->amt; ?></td>

        </tr>
		<?php }} ?>
        
      </tbody>
    </table>
  </div>
</div>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class'=> 'form-body',
      'id'=> 'form',
      'method'=> 'post'
  );

  echo form_open_multipart('unit_class_controller/add_all_unit_type_category',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Unit Category</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="unit_type_category_name" placeholder="Unit Type Category" id="unit_class" required="required">
                <label></label>
                <span class="help-block">Unit Type Category</span> </div>
            </div>

            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <textarea class="form-control" row="3" name="unit_type_category_desc" id="desc" placeholder="Description"></textarea>
                <label></label>
                <span class="help-block">Description</span> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<div id="editmodal" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form1 = array(
      'class' 			=> 'form-body',
      'id'				=> 'form1',
      'method'			=> 'post'
  );

  echo form_open_multipart('unit_class_controller/all_unit_type_category_controller',$form1);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Unit Category</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
              <input type="hidden" id="hid1" name ="hid1" >
              <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="category1" placeholder="Unit Type Category" id="category1" required="required">
                <label></label>
                <span class="help-block">Unit Type Category</span> </div>
              </div>
              
              <div class="form-group form-md-line-input col-md-6">
                <textarea class="form-control" row="3" name="desc1" placeholder="Description" id="desc1"></textarea>
                <span class="help-block">Description</span> </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/delete_unit_class_type?f_id="+id,
                data:{f_id:id},
                success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
	
	function edit_category(id){
		//alert(id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/edit_category",
                data:{id:id},
                success:function(data)
                {
                    //alert(data.id);
					
                   $('#hid1').val(data.id);
					$('#category1').val(data.name);
				     $('#desc1').val(data.cat_desc);
				   $('#admin1').val(data.add_admin);
				   //$('#responsive').hide();
                    $('#editmodal').modal('toggle');
					
                }
            });
	}
</script> 
