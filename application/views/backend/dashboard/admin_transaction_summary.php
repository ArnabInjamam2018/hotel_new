<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-flag"></i>Admin Transaction Summary Report </div>
    <div class="tools"> <a href="javascript:;" class="reload"></a> </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-6">
          <div class="btn-group"> 
            <!--<button class="btn green"  data-toggle="modal" href="#responsive"> Add New <i class="fa fa-plus"></i> </button>-->
            <button class="btn green"  data-toggle="modal" href="#responsive"> Add New <i class="fa fa-plus"></i> </button>
          </div>
        </div>
      </div>
    </div>
	
	<?php print_r($admin_transaction_summary);
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr>
          <!--<th scope="col"> Id </th>-->
          <th scope="col"> Name </th>
          <th scope="col"> Description </th>
          <th scope="col"> Date added</th>
          <!--<th scope="col"> Add admin</th>-->
          <th width="5%" scope="col"> Action</th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($admin_transaction_summary) && $admin_transaction_summary){

                      $i=1;
                      foreach($admin_transaction_summary as $transaction){
                          //$class = ($i%2==0) ? "active" : "success";
                          //$g_id=$gst->hotel_unit_class_id;
                          ?>
        <tr>
         <!-- <td align="center"><?php echo $gst->id; ?></td> -->
          <td align="left"><?php echo $gst->name; ?></td>
          <td align="left">
          	<?php
				  
					if(($gst->cat_desc == NULL) || ($gst->cat_desc == '')){
						echo '<span style="color:#AAAAAA;">'."No Info".'</span>';
					}
					else{
						echo $gst->cat_desc;
					}
								
				?>
          </td>
          <td align="left"><?php echo date("g:ia \-\n l jS F Y",strtotime($gst->date_added));?></td>
          <!--<td align="left"><?php echo $gst->add_admin;?></td>-->
          <td align="center" class="ba">
          	<div class="btn-group">
                <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a onclick="soft_delete('<?php echo $gst->id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a>
                    </li>
                    <li>
                       <a onclick="edit_category('<?php echo $gst->id; ?>')" data-toggle="modal"  class="btn green btn-xs"> <i class="fa fa-edit"></i></a>
                    </li>
                </ul>
            </div>
            </td>
        </tr>
		<?php }} ?>
        
      </tbody>
    </table>
  </div>
</div>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class'=> 'form-body',
      'id'=> 'form',
      'method'=> 'post'
  );

  echo form_open_multipart('unit_class_controller/add_all_unit_type_category',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Unit Category</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="unit_type_category_name" placeholder="Unit Type Category" id="unit_class" required="required">
                <label></label>
                <span class="help-block">Unit Type Category</span> </div>
            </div>

            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <textarea class="form-control" row="3" name="unit_type_category_desc" id="desc" placeholder="Description"></textarea>
                <label></label>
                <span class="help-block">Description</span> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<div id="editmodal" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form1 = array(
      'class' 			=> 'form-body',
      'id'				=> 'form1',
      'method'			=> 'post'
  );

  echo form_open_multipart('unit_class_controller/all_unit_type_category_controller',$form1);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Unit Category</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
              <input type="hidden" id="hid1" name ="hid1" >
              <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="category1" placeholder="Unit Type Category" id="category1" required="required">
                <label></label>
                <span class="help-block">Unit Type Category</span> </div>
              </div>
              
              <div class="form-group form-md-line-input col-md-6">
                <textarea class="form-control" row="3" name="desc1" placeholder="Description" id="desc1"></textarea>
                <span class="help-block">Description</span> </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/delete_unit_class_type?f_id="+id,
                data:{f_id:id},
                success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
	
	function edit_category(id){
		//alert(id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/edit_category",
                data:{id:id},
                success:function(data)
                {
                    //alert(data.id);
					
                   $('#hid1').val(data.id);
					$('#category1').val(data.name);
				     $('#desc1').val(data.cat_desc);
				   $('#admin1').val(data.add_admin);
				   //$('#responsive').hide();
                    $('#editmodal').modal('toggle');
					
                }
            });
	}
</script> 
