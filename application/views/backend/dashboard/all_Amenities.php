<div class="row">
  <div class="form-body"> 
    <!-- 17.11.2015-->
    <?php if($this->session->flashdata('err_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-danger alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
    <?php if($this->session->flashdata('succ_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-success alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
  </div>
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-edit"></i>List of All Amenities </div>
        <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            <div class="col-md-6">
              <div class="btn-group"> <a href="<?php echo base_url();?>unit_class_controller/hotel_amenities">
                <button  class="btn green"> Add New <i class="fa fa-plus"></i> </button>
                </a> </div>
            </div>
            <div class="col-md-6">
              <div class="btn-group pull-right">
                <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu pull-right">
                  <li> <a href="javascript:;"> Print </a> </li>
                  <li> <a href="javascript:;"> Save as PDF </a> </li>
                  <!--<li> <a href="javascript:;"> Export to Excel </a> </li>-->
                </ul>
              </div>
            </div>
          </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="sample_1">
          <thead>
            <tr> 
              <th scope="col"> Photo </th>
              <th scope="col"> Name </th>
              <th scope="col"> Description </th>
              <th scope="col"> Applies to Hotel general </th>
              <th scope="col"> Applies to Units </th>
              <th scope="col"> Type </th>
              <th scope="col"> Charge </th>
              <th scope="col"> Tax </th>
			   <th scope="col"> Action </th>
            </tr>
          </thead>
          <tbody>
            <?php
				
			if(isset($amenities) && $amenities):
							
                            $i=1;
                            foreach($amenities as $gst):
                                $class = ($i%2==0) ? "active" : "success";
                                $g_id=$gst->id;
                                ?>
            <tr>               
              <td><img  width="60px" height="60px" src="<?php echo base_url();?>upload/amenities/<?php if( $gst->image== '') { echo "no_images.png"; } else { echo $gst->image; }?>" alt=""/>
                <?php
										/*echo $gst->g_photo ;
										echo $gst->g_photo ."_thumb"; */
									?></td>
              <td><?php echo $gst->name ?></td>
              <td><?php echo $gst->description ?> </td>
              <td><?php  if($gst->hotel_general=='on'){
				  echo 'yes';
			  } 
			  else{
				   echo 'No';
			  }?></td>
              
              <td><?php if($gst->units=='on'){
				  echo 'yes';
			  } 
			  else{
				   echo 'No';
			  } ?></td>
              <td><?php echo $gst->type ?></td>
			  <td><?php echo $gst->charge ?></td>
             
              <td><?php echo $gst->tax;?></td>
              
              <?php //echo $gst->g_photo ?>
              
              <!--<td>
                                        <img  width="100%" src="<?php //echo base_url();?>upload/<?php //if( $gst->g_id_proof== '') { echo "no_images.png"; } else { echo $gst->g_id_proof; }?>" alt=""/>
										<?php //echo $gst->g_id_proof ?>
                                    </td>-->
              
              <td><table width="100%">
                  <tr>
                    <td><a href="<?php echo base_url();?>unit_class_controller/hotel_amenities_edit/<?php echo $gst->id; ?>" class="btn blue" data-toggle="modal"><i class="fa fa-edit"></i></a></td>
                    <td><a onclick="soft_delete('<?php echo $gst->id;?>')">
                      <button data-toggle="modal"  class="btn red pull-right" ><i class="fa fa-trash"></i></button>
                      </a></td>
                  </tr>
                </table></td>
            </tr>
            <?php endforeach; ?>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    
    <!-- END SAMPLE TABLE PORTLET--> 
    
    <!-- BEGIN SAMPLE TABLE PORTLET--> 
    <!--
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-eye-slash"></i>All Rooms
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
                        	<?php
        $form = array(
            'class' 			=> 'form-horizontal',
            'method'			=> 'post'
        );

        $dropdown = array(
            '5'  				=> '5',
            '10'    			=> '10',
            '25'   				=> '25',
            '50'				=> '50',
            '100'				=> '100'
        );

        $js = 'class="form-control" onChange="this.form.submit();"';

        $pages = ($this->session->flashdata('pages')) ? $this->session->flashdata('pages') : 5;

        echo form_open('dashboard/all_rooms',$form);
        ?>
                        	<div class="form-group">
                                <label class="col-md-3 control-label">
                                    Rooms Per Page
                                </label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <?php echo form_dropdown('pages', $dropdown, $pages, $js);?>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close();?>
                            <?php if($this->session->flashdata('err_msg')):?>

                            <div class="alert alert-danger alert-dismissible text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong><?php echo $this->session->flashdata('err_msg');?></strong>
                            </div>

                            <?php endif;?>
                            <?php if($this->session->flashdata('succ_msg')):?>

                            <div class="alert alert-success alert-dismissible text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong><?php echo $this->session->flashdata('succ_msg');?></strong>
                            </div>

                            <?php endif;?>
                            <div id="status_msg">

                             </div>
                            <?php echo form_open('dashboard/room_delete');?>
							<div class="table-scrollable">
								<table class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>
										 Select
									</th>
									<th>
										 Room No
									</th>
									<th>
										 Number of Beds
									</th>
									<th>
										 Rent
									</th>
									<th>
										 Image
									</th>
                                    <th>
										 Action
									</th>
								</tr>
								</thead>
								<tbody>
								<?php if(isset($rooms) && $rooms):
            $i=1;
            foreach($rooms as $rom):
                if($rom->hotel_id==$this->session->userdata('user_hotel')):
                    $class = ($i%2==0) ? "active" : "success";

                    $room_id=$rom->room_id;
                    ?>

                                    <tr class="<?php echo $class;?>">
                                        <td><input type="checkbox" name="delete[]" value="<?php echo $rom->room_id;?>" class="form-control" onclick="this.form.submit();"/></td>
                                        <td><?php echo $rom->room_no;?></td>
                                        <td><?php echo $rom->room_bed;?></td>
                                        <td>₹ <?php echo $rom->room_rent;?></td>
                                        <td><img src="<?php echo base_url();?>upload/thumb/<?php echo $rom->room_image_thumb;?>" alt=""/></td>
                                        <td>

                                        	<a href="<?php echo base_url();?>dashboard/edit_room/<?php echo base64url_encode($room_id);?>" class="fa fa-edit"></a>
                                        <td>
                                    </tr>
                                <?php $i++;?>
                                <?php endif; ?>
                                <?php endforeach; ?>
                                <?php endif;?>
								</tbody>
								</table>
                                <p class="pull-right" style="padding-top:10px;">
                                    <input type="submit" name="submit" value="Delete" class="btn btn-danger" onclick="javascript:return confirm('Are you sure that you want to delete the selected records?')"/>
                            </p>

                            <?php echo form_close();?>
								<?php //if(isset($pagination) && $pagination)
            //echo $pagination; ?>
							</div>
						</div>
					</div>
					--> 
    <!-- END SAMPLE TABLE PORTLET--> 
  </div>
</div>
<!-- END PAGE CONTENT-->
</div>
</div>
<!-- END CONTENT --> 
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/delete_amenities?g_id="+id,
                data:{g_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });
        });
    }
</script> 
