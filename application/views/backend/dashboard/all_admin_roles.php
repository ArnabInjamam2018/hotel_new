<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Admin Roles </div>
    <div class="actions"> 
    	<a href="<?php echo base_url();?>dashboard/add_admin_roles" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i> Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    <!--<div class="table-toolbar">
      <div class="row">
        <div class="col-md-6">
          <div class="btn-group">
            
          </div>
        </div>        
      </div>
    </div>-->
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <!--<th scope="col">
						Select
					</th>-->
          <th> no </th>
          <th> Role Name </th>
          <th> Type </th>
          <th> Take Bookings </th>
          <th> Edit & Delete Bookings </th>
          <th> Interact with Booking Calander</th>
          <th> Access Settings</th>
          <th> Add Admin </th>
          <th> Access Reports </th>

          <th width="8%"> Action </th>
        </tr>
      </thead>
      <tbody>
            <?php 
			//echo "<pre>";
			//print_r($events);
			if(isset($events) && $events):
				
				$i=1;
				$j=1;
				foreach($events as $event):
					$class = ($i%2==0) ? "active" : "success";
					$e_id=$event->user_permission_id;
			?>
			<tr id="row_<?php echo $event->user_permission_id;?>">
			  <td align="left"><?php echo $j++; ?></td>
			  <td align="left"><?php echo $event->admin_roll_name; ?></td>
			  <td align="left"><?php echo $event->user_type; ?></td>

			  <td align="left">						
                    	<div class="md-radio-inline">
                            <div class="switch">
                                <?php if($event->access_report== '1') { ?>
                                     <input type="checkbox" onclick="handleClick(this.value,'take_booking','<?php echo $e_id;?>')" id="chk1<?php echo $event->user_permission_id;?>" class="md-check cmn-toggle cmn-toggle-round checkbox" name="take_booking" checked>
                                <?php } else { ?>
                                     <input type="checkbox" onclick="handleClick(this.value,'take_booking','<?php echo $e_id;?>')" id="chk1<?php echo $event->user_permission_id;?>" class="md-check cmn-toggle cmn-toggle-round checkbox" name="take_booking">
                                <?php }	?>
                                    <label for="chk1<?php echo $event->user_permission_id;?>"></label>
                            </div>
                        </div>
			  </td>
			  <td align="left">
				  
                        <div class="md-radio-inline">
                            <div class="switch">
                                <?php if($event->edit_booking == '1') { ?>
                                     <input type="checkbox" onclick="handleClick(this.value,'edit_booking','<?php echo "row_".$e_id;?>')" id="chk2<?php echo $event->user_permission_id;?>" class="md-check cmn-toggle cmn-toggle-round checkbox" name="edit_booking" checked>
                                <?php } else { ?>
                                     <input type="checkbox" onclick="handleClick(this.value,'edit_booking','<?php echo "row_".$e_id;?>')" id="chk2<?php echo $event->user_permission_id;?>" class="md-check cmn-toggle cmn-toggle-round checkbox" name="edit_booking">
                                <?php }	?>
                                    <label for="chk2<?php echo $event->user_permission_id;?>"></label>
                            </div>                           
                        </div>
			  </td>
			  <td align="left">
				
                        <div class="md-radio-inline">
                            <div class="switch">
                                <?php if($event->access_booking_engine == '1') { ?>
                                     <input type="checkbox" onclick="handleClick(this.value,'access_booking_engine','<?php echo $event->user_permission_id;?>')" id="chk3<?php echo $event->user_permission_id;?>" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_booking_engine" checked>
                                <?php } else { ?>
                                     <input type="checkbox" onclick="handleClick(this.value,'access_booking_engine','<?php echo $event->user_permission_id;?>')" id="chk3<?php echo $event->user_permission_id;?>" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_booking_engine">
                                <?php }	?>
                                    <label for="chk3<?php echo $event->user_permission_id;?>"></label>
                            </div>                       
                        </div>
</td>
			  <td align="left">
				 
                        <div class="md-radio-inline">
                            <div class="switch">
                                <?php if($event->access_setting == '1') { ?>
                                     <input type="checkbox" onclick="handleClick(this.value,'access_setting','<?php echo $event->user_permission_id;?>')" id="chk4<?php echo $event->user_permission_id;?>" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_setting" checked>
                                <?php } else { ?>
                                     <input type="checkbox" onclick="handleClick(this.value,'access_setting','<?php echo $event->user_permission_id;?>')" id="chk4<?php echo $event->user_permission_id;?>" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_setting">
                                <?php }	?>
                                    <label for="chk4<?php echo $event->user_permission_id;?>"></label>
                            </div>                            
                        </div>
</td>
			  <td align="left">
				 
                        <div class="md-radio-inline">
                            <div class="switch">
                                <?php if($event->add_admin == '1') { ?>
                                     <input type="checkbox" onclick="handleClick(this.value,'add_admin','<?php echo $event->user_permission_id;?>')" id="chk5<?php echo $event->user_permission_id;?>" class="md-check cmn-toggle cmn-toggle-round checkbox" name="add_admin" checked>
                                <?php } else { ?>
                                     <input type="checkbox" onclick="handleClick(this.value,'add_admin','<?php echo $event->user_permission_id;?>')" id="chk5<?php echo $event->user_permission_id;?>" class="md-check cmn-toggle cmn-toggle-round checkbox" name="add_admin">
                                <?php }	?>
                                    <label for="chk5<?php echo $event->user_permission_id;?>"></label>
                            </div>                   
                        </div>
</td>
			  <td align="left">
				            <div class="md-radio-inline">
                            <div class="switch">
                                <?php if($event->access_report == '1') { ?>
                                     <input type="checkbox" onclick="handleClick(this.value,'access_report','<?php echo $event->user_permission_id;?>')" id="chk6<?php echo $event->user_permission_id;?>" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_report" checked>
                                <?php } else { ?>
                                     <input type="checkbox" onclick="handleClick(this.value,'access_report','<?php echo $event->user_permission_id;?>')" id="chk6<?php echo $event->user_permission_id;?>" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_report">
                                <?php }	?>
                                    <label for="chk6<?php echo $event->user_permission_id;?>"></label>
                            </div>                        
                        </div>
</td>

			  <td align="center" class="ba">
				<div class="btn-group">
				  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
				  <ul class="dropdown-menu pull-right" role="menu">
					<li><a onclick="soft_delete('<?php echo $event->user_permission_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
					<li><a href="<?php echo base_url() ?>dashboard/edit_admin_role?id=<?php echo $event->user_permission_id;?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a> </li>
				  </ul>
				</div>
			  </td>
			</tr>
			<?php endforeach; ?>
			<?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
function handleClick(cb,col,a) {

    if(cb.checked){
		var editableObj = '1';
		
    } else {
		var editableObj = '0';
				
		}
		
	var id = a;
	//alert("edit"+editableObj);
	//alert("col"+col);
	//alert(id);
	$.ajax({
		url: "<?php echo base_url()?>dashboard/inlineEditRole",
		type: "POST",
		data:'column='+col+'&editval='+editableObj+'&id='+id,
		success: function(data){
			
		}        
    });
}
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_event?e_id="+id,
                data:{e_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            $('#row_'+id).remove();

                        });
                }
            });

        });
    }

 function getfocus() {
	//alert("ulala")
    document.getElementById("c_valid_from").focus();
	//e.prevent();
}
</script>