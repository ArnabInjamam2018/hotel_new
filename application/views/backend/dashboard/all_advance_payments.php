<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Advance Bookings Payments</div>
    <div class="actions">
    	<a class="btn btn-circle green btn-outline btn-sm" data-toggle="modal" href="#add_pay"> <i class="fa fa-plus"></i>Add New  </a>
    </div>
  </div>
  <div class="portlet-body">
    
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <!--<th scope="col">
						Select
					</th>-->
          <th scope="col"> Sl </th>
          <th scope="col"> Guest Name </th>
          <th scope="col"> Pincode </th>
          <th scope="col"> CheckIn Date </th>
          <th scope="col"> CheckOut Date </th>
          <th scope="col"> Room No </th>
          <th scope="col"> Amount </th>
          <th align="center" scope="col" width="5%"> Action </th>
        </tr>
      </thead>
      <tbody>
    <?php
    
   
    
            $sl=0;
        if(isset($ADpayments) && $ADpayments){
            foreach($ADpayments as $payments){
        
    
    ?>
        <tr>
		  <td><?php echo ++$sl;?></td>
          <td align="left"><?php echo $payments->g_name;?></td>
          <td align="left"> <?php echo $payments->pincode;?> </td>
          <td align="left"><?php 
			echo $payments->checkin_date;
			?>
		  </td>
          <td align="left"><?php echo $payments->checkout_date;?>
		  </td>
          <td align="left"><?php  	echo $payments->room_no;?></td>
          <td align="left"><?php  	echo $payments->amount;?></td>
         
          <td align="center" class="ba"><div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
                 <li><a onclick="edit_payment()" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a> </li>
                  <li><a href="#pay_recip"  class="btn blue btn-xs" data-toggle="modal" onclick="getRecptInfo('<?php echo $payments->g_name;?>',<?php echo $payments->checkin_date;?>,'<?php echo $payments->checkout_date;?>','<?php echo $payments->room_no;?>','<?php  echo $payments->amount;?>')" ><i class="fa fa-eye"></i></a></li>
              </ul>
            </td>
        </tr>
      <?php }} ?>
      </tbody>
    </table>
  </div>
</div>

<div id="add_pay" class="modal fade" tabindex="-1" aria-hidden="true" >
 
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Advance Booking</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" class="form-control" name="g_name" id="g_name" required="required" placeholder="Guest Name *">
                <label></label>
                <span class="help-block">Guest Name</span> </div>
             
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" required="required" name="pincode" class="form-control"  id="pincode" placeholder="Pincode *">
                <label></label>
                <span class="help-block">Pincode</span> </div>
             
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" required="required" name="checkin_date" class="form-control date-picker" id="checkin_date" placeholder="checkin Date *">
                <label></label>
                <span class="help-block">checkIn Date *</span> </div>
                <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" required="required" name="checkout_date" class="form-control date-picker" id="checkout_date" placeholder="checkout Date *">
                <label></label>
                <span class="help-block">checkOut Date *</span> </div>
               
                 <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" required="required" name="room_no" class="form-control" id="room_no" placeholder="Room No *">
                <label></label>
                <span class="help-block">Room No *</span> </div>
                
                 <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" required="required" name="amount" class="form-control" id="amount" placeholder="Amount *">
                <label></label>
                <span class="help-block">Amount*</span> </div>
         
            
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green" onclick="add_payments()">Save</button>
      </div>
    </div>
  </div>
  </div>
  



<script>

function add_payments(){



var g_name=$('#g_name').val();
var pincode=$('#pincode').val();
var checkin_date=$('#checkin_date').val();
var checkout_date=$('#checkout_date').val();
var room_no=$('#room_no').val();
var amount=$('#amount').val();
	 
	$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>reports/add_ADpayments",
                data:{g_name:g_name,
                      pincode:pincode,
                      checkin_date:checkin_date,
                      checkout_date:checkout_date,
                      room_no:room_no,
                      amount:amount
                },
                success:function(data)
                {   
                alert(data);
                }
            });
}

   
function edit_payment(){
    $('#edit_pay').modal('show');
}

    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

           $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_event?e_id="+id,
                data:{e_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            $('#row_'+id).remove();

                        });
                }
            });



        });
    }
    
    
    	let getRecptInfo = (g_name,checkin_date,checkout_date,room_no,amount) => {
		
		console.log('getRecptInfo');
		return false;
		$('#amt').text('INR '+amount);
	
	//	$('#profitC').text(profitC);
	//	$('#ttype').text(type);
	//	$('#tdate').text($('#tdate'+sl).text());
	//	$('#tstatus').text(status);
	//	$('#tid').text(id);
	//	$('#tmode').text($('#tpayMode'+sl).text());
	//	$('#tdesc').text(desc);
	//	$('#tfrom').text($('#tfroe'+sl).text());
	//	$('#tto').text($('#ttoe'+sl).text());
	//	$('#tincexp').text(tincexp);
	//	$('#tpaydetailsM').text($('#tpaydetails'+sl).text());
	//	$('#tnoteM').text($('#tnote'+sl).text());
	}
    
    
</script>  


<div class="modal fade" id="pay_recip" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<table width="100%">
					<tr>
						<td align="center">
							<strong style="font-size: 18px;">Payment Receipt </strong>
							<small id="tincexp" name="tincexp" style="float: right; padding-top: 6px; font-weight: bold; text-transform: uppercase; color: #4e9461;"> </small>
						</td>
					</tr>
					<tr>
						<td style="height: 10px;"></td>
					</tr>
					<tr>
						<td style="border-bottom: 1px solid #000000;"></td>
					</tr>
					<tr>
						<td style="height: 10px;"></td>
					</tr>
				</table>													
				<table width="100%">
					<tr>
						<td align="left" width="60%" style="font-size: 14px;">
							<strong><?php echo $hotel_name->hotel_name; ?></strong>
						</td>
						<td align="left" width="40%" style="font-size: 14px;">
							<strong> Name </strong>
						</td>
					</tr>
					<tr><td colspan="2" style="height: 4px"></td></tr>
					<tr>
						<td align="left">
							<?php echo  $hotel_contact['hotel_street1']; ?>
							<?php echo  $hotel_contact['hotel_street2'].', '; ?><br>
							<?php echo  $hotel_contact['hotel_district']; ?> -
							<?php echo  $hotel_contact['hotel_pincode']; ?>,<br>
							<?php echo  $hotel_contact['hotel_state']; ?> -
							<?php echo  $hotel_contact['hotel_country']; ?><br/>
							Phone:
							<?php echo  $hotel_contact['hotel_frontdesk_mobile']; ?><br/>
							Email:
							<?php echo  $hotel_contact['hotel_frontdesk_email']; ?><br/>
						</td>
						<td align="left">
							Address
						</td>
					</tr>
					<tr><td colspan="2" style="height: 4px"></td></tr>

					<tr>
						<td align="left">
							<label id="tdate" name="tdate"> </label>
						</td>
						<td align="left">
							<strong>
								<Strong><label id="tstatus" name="tstatus"> </label></strong>
							</strong>
						</td>
					</tr>
					<tr><td colspan="2" style="height: 4px"></td></tr>
					<tr>
						<td align="left">
							<label id="tid" name="tid"> </label>
						</td>
						<td align="left">&nbsp;
							
						</td>
					</tr>
					<tr>
						<td colspan="2" style="height: 10px;"></td>
					</tr>
					<tr>
						<td colspan="2" style="border-bottom: 1px solid #000000;"></td>
					</tr>
					<tr>
						<td colspan="2" style="height: 10px;"></td>
					</tr>
				</table>
				<table width="100%" class="td-pad">
					<tr>
						<td width="35%" align="left" valign="top">Amount:</td>
						<td align="left" valign="top">
							<label id="amt" name="amt"> </label> /-
						</td>
					</tr>
					<tr>
						<td align="left" valign="top">Mode:</td>
						<td align="left" valign="top">
							<label id="tmode" name="tmode"> </label>
						</td>
					</tr>
					
					<tr>
						<td align="left" valign="top">Payment Details:</td>
						<td align="left" valign="top">
							<label id="tpaydetailsM" name="tpaydetailsM"> </label>
						</td>
					</tr>
					
					<tr>
						<td align="left" valign="top">Transations Type:</td>
						<td align="left" valign="top">
							<label id="ttype" name="ttype"> </label>
						</td>
					</tr>		

					<tr>
						<td align="left" valign="top">Details:</td>
						<td align="left" valign="top">
							<label id="tdesc" name="tdesc"> </label>
						</td>
					</tr>
					
					<tr>
						<td align="left" valign="top">Admin:</td>
						<td align="left" valign="top">
							<label id="admin" name="admin"> </label>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top">From:</td>
						<td align="left" valign="top">
							<label id="tfrom" name="tfrom"> </label>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top">To:</td>
						<td align="left" valign="top">
							<label id="tto" name="tto"> </label>
						</td>
					</tr>
					
					<tr>
						<td align="left" valign="top">Note:</td>
						<td align="left" valign="top">
							<label id="tnoteM" name="tnoteM"> </label>
						</td>
					</tr>
					
					<tr>
						<td align="left" valign="top">Profit Center:</td>
						<td align="left" valign="top">
							<label id="profitC" name="profitC"> </label>
						</td>
					</tr>
					
					<tr>
						<td colspan="2" style="height: 10px;"></td>
					</tr>
					<tr>
						<td colspan="2" style="border-bottom: 1px solid #000000;"></td>
					</tr>
					<tr>
						<td colspan="2" style="height: 60px;"></td>
					</tr>
				</table>
				<table width="100%">
					<tr>								
						<td>--------------------------------</td>
						<td align="center">---------------------------------</td>
						<td align="right">-------------------------------</td>
					</tr>
					<tr>								
						<td>Paid By</td>
						<td align="center">Approved By</td>
						<td align="right">Recievd By</td>
					</tr>
				</table>		
			</div>
			<div class="modal-footer">
				<button type="button" class="btn dark btn-outline btn-xs" onclick="printDiv(pay_recip)">Print</button>
				<button type="button" class="btn green  btn-xs">Download</button>
			</div>
		</div>
	</div>
</div>





