<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Assets </div>
    <div class="actions">
    	<a href="<?php echo base_url();?>dashboard/add_asset" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    
    <table class="table table-striped table-bordered table-hover" id="sample_3">
      <thead>
        <tr> 
          <!--<th scope="col">
                    Select
                </th>-->
          <th scope="col"> Asset Image </th>
          <th scope="col"> Asset Type </th>
          <th scope="col"> Asset Category </th>
          <th scope="col"> Asset Name </th>
          <th scope="col"> Asset First Hand </th>
          <th scope="col"> Asset Bought On </th>
          <th scope="col"> Asset Description </th>
          <th scope="col"> Asset Registration Number </th>
          <th scope="col"> Asset Purchased From </th>
          <th scope="col"> Asset Seller Contact Number </th>
          <th scope="col"> Asset Service Contact Number </th>
          <th scope="col"> Asset Incharge </th>
          <th scope="col"> Asset Cost </th>
          <th scope="col"> Asset Annual Depreciation </th>
          <th scope="col"> Asset Decomission Date </th>
          <th scope="col"> Asset AMC </th>
          <th scope="col"> Asset AMC Agency Name </th>
          <th scope="col"> Asset AMC Contact Number </th>
          <th scope="col"> Asset AMC Renewal Date </th>
          <th scope="col"> Asset AMC Charge </th>
          <th scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php $x = 1; if(isset($assets) && $assets):
                foreach($assets as $assets):
                    //	echo "<pre>";
                        //print_r($assets);
                        //exit;
                    $a_id = $assets->a_id;
                    $a_type = $assets ->a_type;
                    $assets_id= $a_type.$a_id;
                    //echo $assets->a_asset_image;
                    ?>
        <tr>
          <td><a class="single_2" href="<?php echo base_url();?>upload/asset/originals/asset_image/<?php if( $assets->a_asset_image== '') { echo "no_images.png"; } else { echo $assets->a_asset_image; }?>"><img src="<?php echo base_url();?>upload/asset/originals/asset_image/<?php if( $assets->a_asset_image== '') { echo "no_images.png"; } else { echo $assets->a_asset_image; }?>" alt="" style="width:100%;"/></a></td>
          <td><?php echo $assets->a_type; ?></td>
          <td><?php echo $assets->a_category; ?></td>
          <td><?php echo $assets->a_name; ?></td>
          <td><?php echo $assets->a_first_hand; ?></td>
          <td><?php echo $assets->a_bought_date; ?></td>
          <td><?php echo $assets->a_description; ?></td>
          <td><?php echo $assets->a_reg_number; ?></td>
          <td><?php echo $assets->a_purchased_from; ?></td>
          <td><?php echo $assets->a_seller_contact_no; ?></td>
          <td><?php echo $assets->a_service_contact_no; ?></td>
          <td><?php echo $assets->a_incharge; ?></td>
          <td><?php echo $assets->a_cost; ?></td>
          <td><?php echo $assets->a_annual_depreciation; ?></td>
          <td><?php echo $assets->a_decomission_date; ?></td>
          <td><?php echo $assets->a_amc; ?></td>
          <td><?php echo $assets->a_amc_agency_name; ?></td>
          <td><?php echo $assets->a_amc_reg_contact_no; ?></td>
          <td><?php echo $assets->a_amc_renewal_date; ?></td>
          <td><?php echo $assets->a_amc_charge; ?></td>
          <td class="ba">
          	<div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $assets->a_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
                <li><a href="<?php echo base_url() ?>dashboard/Edit_asset?a_id=<?php echo $assets->a_id;?>" data-toggle="modal" onclick="st_alt()" class="btn green btn-xs"> <i class="fa fa-edit"></i></a></li>
                <li><a href="<?php echo base_url() ?>dashboard/view_asset?a_id=<?php echo $assets->a_id;?>" data-toggle="modal" class="btn blue btn-xs"><i class="fa fa-eye"></i></a></li>
              </ul>
            </div>
          </td>
        </tr>
        <?php $x++ ; endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>

<script>  
   function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_assets?a_id="+id,
                data:{a_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
</script> 
