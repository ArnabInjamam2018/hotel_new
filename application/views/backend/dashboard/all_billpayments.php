<!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Bill Payments </div>
    <div class="actions">
    	<a href="<?php echo base_url('dashboard/add_bill_payments');?>" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    <table class="table table-striped table-bordered table-hover" id="sample_3">
      <thead>
        <tr> 
          <!-- <th scope="col">
                                Select
                            </th>-->
          <th scope="col"> # </th>
          <th scope="col"> Payment Date </th>
          <th scope="col"> Payment Type </th>
        
          <th scope="col"> Paid to </th>
          <th scope="col" width="8%"> Line Item Details</th>
          <!-- <th scope="col"> Month </th>
          <th scope="col"> Others </th>-->
          
          <th scope="col"> Profit Center </th>
          
          <th scope="col"> Approved By </th>
          <th scope="col"> Paid By </th>
          <th scope="col"> Admin </th>
          <th scope="col" class="none"> Reciever's Name </th>
          <!--<th scope="col"> Reciever's Signature </th>-->
		  <th scope="col"> Payment Status </th>
          <th scope="col" class="none"> Due Amount </th>
		 
          <th scope="col"> Action </th>
         </tr>
      </thead>
      <tbody>
        <?php
	$sl_no=0;
		if(isset($salpay) && $salpay):
		//echo "<pre>";
		//print_r($salpay); exit;
			$i=1;//var_dump($salpay);die;
			foreach($salpay as $sp):
			$sl_no++;
				$class = ($i%2==0) ? "active" : "success";
				$sal_pay_id=$sp->hotel_payment_id;
				?>
        <tr> 
          <td><?php echo $sl_no; ?></td>
          <td><?php echo date('g:ia \ jS M, y',strtotime($sp->date)); ?></td>
          <td><?php echo $sp->type_of_payment;?></td>
          
          <td><?php
		    $v_id=$sp->vendor_id; 
		   $vendor_name=$this->dashboard_model->get_vendor_name($v_id);
		   if(isset($vendor_name) && $vendor_name){
			   echo $vendor_name->hotel_vendor_name;
		   }
		  ?></td>
          
         <td><?php $line_items=$this->dashboard_model->get_payments_items($sal_pay_id);
			//print_r($line_items);
			if(isset($line_items) && $line_items){			
			foreach($line_items as $li){
				echo '<span class="label-flat" style="background-color:#FFF9E2; color:#000000" >'.$li->product.'</span><span style="color:black; font-size:10px;">'.' '.$li->total.'</span>'."<br>";
				
			}
			}
			//print_r($line_items);
		 ?></td>
         <!-- <td><?php echo $sp->bill_month; ?></td>
          <td><?php echo $sp->bill_other; ?></td>-->
          
          <td><?php if(isset($sp->profit_center)){echo $sp->profit_center;} ?></td>
          <td><?php echo $sp->approved_by; ?></td>
          <td><?php echo $sp->paid_by; ?></td>
          <td><?php //echo $sp->admin; 
            $admin_name=$this->dashboard_model->get_admin1($sp->admin);
          echo $admin_name->admin_first_name." ".$admin_name->	admin_last_name;
          
          ?></td>
          <!--<td><?php echo $sp->recievers_desig; ?></td>-->
          <td><?php echo $sp->recievers_name.' ('.$sp->recievers_desig.')'; ?></td>
		    <td><?php 
					if(isset($sp->payments_status)){
						if($sp->payments_status==2){
							
							echo "<span class='label' style='background-color:#F39C12; color:#ffffff;'>Partial</span>";
						}elseif($sp->payments_status==1){
							echo "<span class='label' style='background-color:#008F2F; color:#ffffff'>Paid</span>";
						}elseif($sp->payments_status==0){
							echo "<span class='label' style='background-color:#E74C3C; color:#ffffff'>Unpaid</span>";
						}
					}
			  ?></td>			 
			 <td><?php echo $sp->payments_due;?></td>
			
             <td align="center" class="ba"><div class="btn-group">
            <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
            <ul class="dropdown-menu pull-right" role="menu">
              <li><a href="<?php echo base_url() ?>dashboard/delete_billpayments?billpay_id=<?php echo $sp->hotel_payment_id;?>" class="btn red btn-xs" data-toggle="modal"><i class="fa fa-trash"></i></a></li>
              <li><a href="<?php echo base_url();?>dashboard/edit_bill_payments?p_id=<?php echo $sp->hotel_payment_id; ?>" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li>
            </ul></td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>

<!-- END SAMPLE TABLE PORTLET-->

</div>
