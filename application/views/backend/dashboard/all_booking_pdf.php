<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td{
	font-size:9px;
}
</style>

<div style="padding:15px 35px;">
<table>
		<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));?>
    <tr>
      <td align="left" valign="middle"><img src="upload/hotel/<?php
			 
			if(isset($hotel_name->hotel_logo_images_thumb))echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/></td>
		<td align="right" valign="middle"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?></td>
        </tr>		
    
    <tr>
      <td width="100%" colspan="2"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td colspan="2"><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%" colspan="2">&nbsp;</td>
    </tr>
</table>
<table class="td-pad" width="100%">
  <thead>
    <tr style="background: #e5e5e5; color: #1b1b1b">
      <th> Booking Id </th>
      <th> Booking Status </th>
      <th> Customer Name </th>
      <th> Room Number </th>
      <th> Booking Source </th>
      <th> Nature Of Visit</th>
      <th> PAX</th>
      <th> From and Upto Date </th>
      <th> Customer Address </th>
      <th> Customer Contact Number </th>
      <th> Food Plan</th>
      <th> Amount to be paid </th>
      <th> Admin </th>
    </tr>
  </thead>
 <tbody style="background: #F2F2F2">
            <?php if(isset($bookings) && $bookings):
                        $i=1;
                        $deposit=0;
						
                        foreach($bookings as $booking):
							
			?>

          
          <?php
	    	
                            $rooms=$this->dashboard_model->get_room_number($booking->room_id);
                            if(isset($rooms) && $rooms):
                            foreach($rooms as $room):
							
							
                                if($room->hotel_id==$this->session->userdata('user_hotel')):


                                    foreach($transactions as $transaction):

                                        if($transaction->t_booking_id=='HM0'.$this->session->userdata('user_hotel').'00'.$booking->booking_id):

                                            $deposit=$deposit+$transaction->t_amount;
                                        endif;
                                    endforeach;



                                    /*Calculation start */
                                   
                                    $room_number=$room->room_no;
                                    $room_cost=$room->room_rent;
                                    /* Find Duration */
                                    $date1=date_create($booking->cust_from_date);
                                    $date2=date_create($booking->cust_end_date);
                                    $diff=date_diff($date1, $date2);
                                    $cust_duration= $diff->format("%a");

                                    /*Cost Calculations*/
                                    $total_cost=$cust_duration*$room_cost;
                                    $total_cost=$total_cost+$total_cost*0.15;
									$food_tax=0;
									if($booking->food_plans!="")
									{
									$data=$this->bookings_model->fetch_food_plan($booking->food_plans);
									if($data)
									$food_tax=($data->fp_tax_percentage/100)*$booking->food_plan_price;
									}
                  $due=($booking->room_rent_sum_total+$booking->service_price+$booking->food_plan_price+$food_tax+$booking->booking_extra_charge_amount)-$booking->cust_payment_initial-$deposit;

                                    $status_id=$booking->booking_status_id;

                                    $status=$this->dashboard_model->get_status_by_id($status_id);
									
									if(isset($status)){
										//print_r($status);
										foreach($status as $st){
										
                                    /*Calculation End */


                                    $class = ($i%2==0) ? "active" : "success";

                                    $booking_id='HM0'.$this->session->userdata('user_hotel').'00'.$booking->booking_id;
                                    ?>
             
		  <tr>
           <td><?php if($booking->group_id !="0"){
              
                } ?>
              

              <?php if($booking->group_id !="0"){
               // echo 'style="color:green; " ';
				}?>
            <?php echo $booking_id?></td>
            <td>
              <?php  echo $st->booking_status?>
              </td>
            <td><?php 

              $guests=$this->dashboard_model->get_guest_details($booking->guest_id);
				if(isset($guests) && $guests){
			     foreach ($guests as $key ) {
               if($key->g_name!=""){
                echo $key->g_name;
			   }
			   else{
				   echo "na";
			   }
				}}
            ?></td>
            <td><?php echo $room_number; ?></td>
            <td><?php echo $booking->booking_source;?></td>
            <td><?php echo $booking->nature_visit;?></td>
            <td><?php echo $booking->no_of_guest;?></td>
            <td><?php echo $booking->cust_from_date;?> -  <?php echo $booking->cust_end_date;?></td>
            <td><?php echo $booking->cust_address;?></td>
            <td><?php echo $booking->cust_contact_no;?></td>
            <td><?php $data=$this->bookings_model->fetch_food_plan($booking->food_plans);if(isset($data) && $data)echo $data->fp_name; ?></td>
            <td><?php echo max($due,0); ?></td>
            <td><?php 
				$admin=$this->dashboard_model->get_admin($booking->user_id);
			  
			  if(isset($admin) && $admin){
				  foreach($admin as $admin_name){
					  echo $admin_name->admin_first_name." ".$admin_name->admin_last_name;
				  }
			  }
			  
				?></td>
           
          </tr>
          <?php $i++;}}?>
          <?php endif; ?>
          <?php endforeach; ?>
          <?php endif; ?>
          <?php endforeach; ?>
          <?php endif;?>
   </tbody>
</table>
</div>
