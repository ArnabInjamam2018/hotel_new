<?php 
	//echo 'Test';
	//exit;
	ini_set("memory_limit", "-1");
set_time_limit(0);
?>

<div class="portlet bordered light">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-th-list"></i> All SINGLE BOOKINGS</div>
		<div class="actions">
			<a href="<?php echo base_url();?>dashboard/add_booking_calendar" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-table"></i>Booking Calender </a>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-toolbar">
			<div class="row">
				<div class="col-md-8">
					<?php

					$form = array(
						'class' => 'form form-inline ftop',
						'id' => 'form_date',
						'method' => 'post'
					);

					echo form_open_multipart( 'dashboard/get_booking_report_by_date', $form );

					?>
					<div class="form-group">
						<input type="text" autocomplete="off" value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
					</div>
					<div class="form-group">
						<input type="text" autocomplete="off" value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
					</div>
					<div class="form-group">

						<select id="select2_sample_modal_5" class="form-control js-example-basic-multiple" multiple="multiple" style="min-width:100px; max-width:265px;">
							<?php $admins = $this->dashboard_model->fetch_admin();
					foreach($admins as $adm){?>
							<option value="<?php echo $adm->admin_id; ?>">
								<?php echo $adm->admin_first_name." ".$adm->admin_last_name; ?>
							</option>
							<?php } ?>
						</select>
						<input name="admins" id="admin" type="text" style="display:none;"/>
					</div>
					<div class="form-group">
						<button onclick="get_val();" class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search" aria-hidden="true"></i></button>
					</div>


					<?php form_close(); ?>
				</div>
				<script type="text/javascript">
					function check_sub() {
						document.getElementById( 'form_date' ).submit();
					}
				</script>
				<div class="col-md-2 pull-right">
					<div class="form-group form-md-checkboxes">
						<div class="md-checkbox-inline text-right">
							<div class="md-checkbox">
								<input type="checkbox" id="chkbx_tdy" onclick="fetch_data()" class="md-check">
								<label for="chkbx_tdy">
									<span></span>
									<span class="check"></span>
									<span class="box"></span> No Tax </label>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="table1">
				<table class="table table-striped table-bordered table-hover" id="sample_3">
					<!--<table class="table table-striped">-->
					<thead>
						<tr>
							<!--<th> Select </th>-->
							<th width="8%"> Single Booking ID </th>
							<th> Status </th>
							<th> Guest Name </th>
							<th align="center"> Unit ID </th>
							<th> Source </th>
							<th> Nature</th>
							<th align="center"> PAX</th>
							<th width="10%"> Booking Dates </th>
							<th> Guest Address </th>
							<th class="none"> Guest Contact </th>
							<th class="none"> Food Plan</th>
							<th width="8%" align="center" class="none"> Room Rent </th>
							<th width="8%" align="center" class="none"> RR Tax </th>
							<th width="8%" align="center" class="none"> POS Amt </th>
							<th width="8%" align="center" class="none"> Ex Charge </th>
							<th width="8%" align="center" class="none"> Ex Serv </th>
							<th width="8%" align="center" class="none"> Disc </th>
							<th width="8%" align="center" class="none"> Amt Due </th>
							<th width="8%" align="center" class="none"> Paid </th>
							<th class="none"> Mktng Exe </th>
							<th class="none"> Admin </th>
							<th width="5%" scope="col" class="action"> Options </th>
						</tr>
					</thead>
					<tbody>
						<?php if(isset($bookings) && $bookings){
							//echo "<pre>";
							//print_r($bookings); exit;
						$i=1;
                        $deposit=0;
						$item_no=0;
						$msg="";
                        foreach($bookings as $booking){
						if($booking->service_tax=='0' && $booking->luxury_tax=='0'){
							if($booking->group_id=='0')
							{
								 $msg="<i class='fa fa-minus-circle' style='color:#E27979;'></i>";
							}
								 /*$msg="<i class='fa fa-minus-circle' style='float:right; color:#E27979;'></i>";*/
							}else{
								 $msg="";
							}
							$bills=$this->bookings_model->all_folio($booking->booking_id);
							if(isset($bills)){
						
						foreach ($bills as $item) {
						# code...

						  $item_no++;

							} }
					
		?>
						<?php
						$booking_id = 'HM0' . $this->session->userdata( 'user_hotel' ) . '00' . $booking->booking_id;
						$book_id = $booking->booking_id;
						?>

						<tr>


							<td>

								<a <?php
								// Booking ID
								//POS Flag logic start _sb $gb='gb' ; $sb='sb' ; $pos=$ this->bookings_model->pos_check($booking->group_id, $gb);
								$gb='gb' ; $sb='sb' ;
								$pos=$this->bookings_model->pos_check($booking->group_id, $gb);
				  $pos_s = $this->bookings_model->pos_check($booking->booking_id, $sb);
				  if(($pos_s == 1) && ($pos == 1)) //if POS is in Both Grp & Single _sb
					{
						$pos_flag = '<i class="fa fa-cutlery" style="float:right; color:#9C1276;"></i>';
					}
				  else if($pos_s == 1) //if POS is in Single under Grp
					{
						$pos_flag = '<i class="fa fa-cutlery" style="float:right; color:#FF7A24;"></i>';
					}
				  else if($pos == 1) //if POS is in grp
					{
						$pos_flag = '<i class="fa fa-cutlery" style="float:right; color:#E54683;"></i>';	
					} 
				  else 
					{
						$pos_flag = '';
					}
					// Pos Flag Logic end
				  if($booking->group_id !="0"){
					echo 'style="color:#F8681A;"';
				  }
				  else 
					echo 'style="color:#3D4B76;"';
				 
				 ?>
				  href="<?php echo base_url();?>dashboard/booking_edit?b_id=<?php echo $booking->booking_id ?>">
				  
				  
				<?php 
					if($booking->group_id !="0"){
						echo '<span class="fa fa-group" ><span style="font-family:Open Sans,sans-serif; font-size:11px;padding-left:6px; display: inline-block;">';
					} 
					else
					{
						echo '<span class="fa fa-user" ><span style="font-family:Open Sans,sans-serif; font-size:11px;padding-left:6px; display: inline-block;">';		
					}
					//echo $msg;
					?>
				 

				  <?php 
					echo $booking_id;
					echo ' '.$msg;
					echo '&nbsp'.$pos_flag;
				  ?></span></span></a>
							</td>
							<td>
								<span class="label" style="background-color:<?php echo $booking->status_pri_bar_col ?>; color:<?php echo $booking->status_pri_body_col ?>;">
									<?php  echo $booking->status_pri  ?>
								</span>

							</td>
							<td>
								<?php //Guest Name
					if($booking->g_name){
					echo $booking->g_name;}else{
						echo "n/a";
					}
				?>
							</td>
							<td align="center">
								<?php //Unit ID
				
					echo '<span class="label" style="background-color:#3D4B76; color:#ffffff;">'.$booking->room_no.'</span>'; 
				?>
							</td>
							<td>
								<?php 
					if($booking->booking_source == NULL){
						echo '<span style="color:#AAAAAA;">'."No Plan".'</span>';
					}
					else
						echo $booking->booking_source;
				?>
							</td>
							<td>
								<?php 
					if($booking->nature_visit == NULL){
						echo '<span style="color:#AAAAAA;">'."No Plan".'</span>';
					}
					else
						echo $booking->nature_visit;
				?>
							</td>
							<td align="center">
								<?php //PAX
					echo $booking->no_of_guest;
				?>
							</td>
							<td>
								<?php 
						$from=$booking->cust_from_date;
						$to=$booking->cust_end_date;
						$days = (strtotime($to)- strtotime($from))/24/3600;
				echo date("dS-M-Y",strtotime($booking->cust_from_date));?>
								<div style="color:#1F897F;">-to-</div>
								<?php 
				
				echo date("dS-M-Y",strtotime($booking->cust_end_date));?>
							</td>
							<td>
								<?php 
					$guest=$this->dashboard_model->get_guest_details($booking->guest_id);
					if(isset($guest) && $guest){
						foreach ($guest as $keys ) {
							$fg = 0;
							if(isset($keys->g_city) && $keys->g_city != '' && $keys->g_city != ' '){									
								echo $keys->g_city;
								
								$fg ++;
							}
							if(isset($keys->g_state) && $keys->g_state != '' && $keys->g_state != ' '){
								if($keys->g_city != ''){
									echo ',';
									echo '<br>';
								echo $keys->g_state;
								} else {
								echo $keys->g_state; 	
								}
								$fg ++;
							}
							if(isset($keys->g_pincode) && $keys->g_pincode!= ''){
								if($keys->g_state != ''){
								    echo ',PIN - '.$keys->g_pincode;
								} else {
									echo 'PIN - '.$keys->g_pincode;
								}
								$fg ++;
							}
							if(isset($keys->g_country) && $keys->g_country!=''){
								if($keys->g_pincode != ''){
								echo ','.$keys->g_country;
								} else {
								echo $keys->g_country; 	
								}
								$fg ++;
							}
					if($fg == 0){
						echo '<span style="color:#AAAAAA;">'."No Info".'</span>';
					}
						}
					}
				?>
							</td>
							<td>
								<?php


								if ( ( $booking->g_contact_no == NULL ) || ( $booking->g_contact_no == '' ) ) {
									echo '<span style="color:#AAAAAA;">' . "No Info" . '</span>';
								} else {
									echo $booking->g_contact_no;
								}



								?>
							</td>
							<td>
								<?php // Food Plan
					if(!empty($booking->mp_tot)){
					
					$food_tax=$booking->mp_tax;
					$food_amount = $booking->mp_tot+$food_tax;
					}
					else{
						$food_amount ='N/A';
					}
					$data=$this->bookings_model->fetch_food_plan($booking->food_plans);
					if(($data == NULL) || ($data == '') || ($data->fp_name == 'No Plan')){
						echo '<span style="color:#AAAAAA;">'."No Plan".'</span>';
					}
					else{
						$fp1 = $data->fp_name;
						$fp = (string)$fp1;
			echo '<span class="label" style="background-color:#dddddd; color:#333333;">'.$fp.'</span>'.'<br><br>'.$food_amount;
				
				}
				?>
							</td>

							<td align="center">

								<?php 
				

					$discount=$booking->discount;

					$poses=$this->dashboard_model->all_pos_booking($booking->booking_id);
                    $pos_amount=0;
                    if($poses){
                    foreach ($poses as $key ) {

                      # code...
                      $pos_amount=$pos_amount+$key->total_due;
                    }}
				$total_rr_cost = $booking->rm_total;
				$total_rr_tax = $booking->rr_tot_tax;
				$booking_sum_total=$total_rr_cost+$total_rr_tax;
				  $group_id = $booking->group_id;
				if($group_id =="0"){
				//$grand_total=$booking_sum_total;
				
					$grand_total=$total_rr_cost;
				
				}else{
					$grand_total=0;
				}
				if($booking->group_id =="0"){ 
				if($grand_total>0){
							echo '<span style="color:#4E85C5;">'.number_format($grand_total,2,'.','').'</span>';
						}
				else
					echo '<span style="color:#AAAAAA;">'.number_format($grand_total,2,'.','').'</span>';
				}
				else	
					echo '<span style="color:#AAAAAA;">n/a</span>';
				
				?>
							</td>

							<td align="center">
								<?php // RR Tax
				if($booking->group_id =="0"){ // If Single Booking
					if($total_rr_tax>0){
							echo '<span style="color:#4E85C5;">'.number_format($total_rr_tax,2,'.','').'</span>';
						}
					else
						echo '<span style="color:#AAAAAA;">'.number_format($total_rr_tax,2,'.','').'</span>';
				}
				else
					echo '<span style="color:#AAAAAA;">n/a</span>';
				?>
							</td>
							<td align="center">
								<?php // POS Amt
					$poses=$this->dashboard_model->all_pos_booking($booking->booking_id);
                   // echo "<pre>";
					//print_r($poses);
					$pos_amount=0;
                    if($poses){
                    foreach ($poses as $key ) {
						if(isset($key->total_due) && $key->total_due){

						$pos_amount=$pos_amount+$key->total_due;}
                    }}
					
					if($pos_amount>0){
							echo '<span style="color:#4E85C5;">'.number_format($pos_amount,2,'.','').'</span>';
						}
						else
							echo '<span style="color:#AAAAAA;">'.number_format($pos_amount,2,'.','').'</span>';
				?>
							</td>
							<td align="center">
								<?php // Extra Charge
					$ex_chrg_amt = $booking->booking_extra_charge_amount;
					if($booking->booking_extra_charge_amount>0){
							echo '<span style="color:#4E85C5;">'.number_format($booking->booking_extra_charge_amount,2,'.','').'</span>';
						}
						else
							echo '<span style="color:#AAAAAA;">'.number_format($booking->booking_extra_charge_amount,2,'.','').'</span>';
					//echo $booking->booking_extra_charge_amount;
				?>
							</td>
							<td>
								<?php // Extra Service
					$ex_ser = 0;
					if($ex_ser>0){
							echo '<span style="color:#4E85C5;">'.number_format($ex_ser,2,'.','').'</span>';
						}
						else
							echo '<span style="color:#AAAAAA;">'.number_format($ex_ser,2,'.','').'</span>';
				?>
							</td>
							<td>
								<?php
								if ( $booking->discount > 0 ) {
									echo '<span style="color:#4E85C5;">' . number_format( $booking->discount, 2, '.', '' ) . '</span>';
								} else
									echo '<span style="color:#AAAAAA;">' . number_format( $booking->discount, 2, '.', '' ) . '</span>';
								//echo $booking->discount;
								?>
							</td>
							<td align="center">
								<?php //Amount Due
					//$da = $due-$booking->discount;
					// $deposit is the paid amount
					
					$depo = $this->dashboard_model->get_amountPaidByBookingID($booking->booking_id);
					if(isset($depo->tm))
						$depos = $depo->tm;
					else
						$depos = 0;
					$pend_b = ($booking->rm_total + $total_rr_tax + $pos_amount + $ex_chrg_amt + $food_amount)-$booking->discount - $depos;
					
					if($pend_b<0){
					echo '<span style="color:#FF0074;">'.number_format(($pend_b),2,'.','').'</span>';}
					else if($pend_b>0){
					echo '<span class="blink-green" style="color:#03893B;">'.number_format(($pend_b),2,'.','').'</span>';}
					else if($pend_b == 0){
					echo '<span class="label" style="background-color:#0BF800; color:#333333;">Paid</span>';}
					/*if($due == 0){
						echo '<span class="label" style="background-color:#0BF800; color:black;"><i class="fa fa-check"></i> Paid</span>';
					}
					else
						echo number_format(max($due,0),2);*/
				?>
							</td>
							<td>
								<?php // Total Paid
					if($booking->discount>0){
							echo '<span style="color:#1B803D;">'.number_format($depos,2,'.','').'</span>';
						}
					else
						echo '<span style="color:#AAAAAA;">'.number_format($depos,2,'.','').'</span>';
				?>
							</td>
							<td>
								<?php // Marketing Personal
					if(isset($booking->booking_id_actual)&& $booking->booking_id_actual){
						if($booking->marketing_personnel == NULL){
							echo 'None';
						}
						else
							echo '<span class="label" style="background-color:#BCFFE5; color:#333333;">'.$booking->marketing_personnel.'</span>';
							//echo $booking->marketing_personnel;
					}
					else
						echo 'None';
				?>
							</td>
							<td>
								<?php 
					if(($booking->admin_name != Null) || ($booking->admin_name != ''))
					{
							echo $booking->admin_name;
					}
					else
						echo 'No Admin';
				?>
							</td>
							<td align="center" class="ba">
								<div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
									<ul class="dropdown-menu pull-right" role="menu">

										<?php if($booking->group_id=='0'){ ?>
										<li> <a onclick="soft_delete('<?php echo $booking->booking_id ?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a> </li>
										<?php }
						  else{ ?>
										<li> <a onclick="soft_delete('<?php echo $booking->booking_id ?>')" data-toggle="modal" class="btn red btn-xs disabled"><i class="fa fa-trash"></i></a> </li>
										<?php } ?>

										<li><a href="<?php echo base_url();?>dashboard/booking_edit?b_id=<?php echo $booking->booking_id ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a>
										</li>
										<li> <a href="<?php echo base_url();?>dashboard/<?php if($booking->group_id !=0){echo " booking_edit_group?group_id=".$booking->group_id." " ;}else{echo "booking_edit?b_id=".$booking->booking_id." &fl=1 " ;}?>" data-toggle="modal" class="btn blue btn-xs"><i class="fa fa-inr"></i></a>
										</li>
										<li><a id="dwn_link" onclick="download_pdf('<?php echo $book_id;?>');" class="btn purple btn-xs"><i class="fa fa-download"></i></a>
										</li>
									</ul>
								</div>
							</td>
						</tr>
						<input type="hidden" id="item_no" value="<?php echo $item_no;?>"> </input>

						<?php $i++; ?>

						<?php  
			}}
		  ?>

					</tbody>

				</table>

			</div>
		</div>
	</div>

	<script>
		/*function fetch_data( val ) {
			alert()
			$.ajax( {

				url: "<?php //echo base_url()?>dashboard/get_no_tax_booking",
				success: function ( data ) {
					$( '#table1' ).html( data );
					$( '#dataTable2' ).dataTable( {
						"pageLength": 10
					} );
				}
			} );
		}*/


		function soft_delete( id ) {
			swal( {
				title: "Are you sure?",
				text: "All the releted transactions and data will be deleted",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			}, function () {
				//alert(id);
				$.ajax( {

					url: "<?php echo base_url()?>dashboard/soft_delete_booking?id=" + id,
					type: "POST",
					data: {
						id: id
					},
					success: function ( data ) {
						//alert("Checked-In Successfully");
						//location.reload();
						if(data.data==0){
					    window.location.replace("<?php echo base_url()?>dashboard");
					}else{
					   	swal( {
								title: data.data,
								text: "",
								type: "success"
							},
							function () {

								location.reload();

							} ); 
					}
					
					    
					}
				} );

			} );
		}

		function download_pdf( booking_id ) {



			//alert(booking_id);
			$( "#dwn_link" ).attr( "href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id );
			var f = $( "#f" );
			$.post( f.attr( "action" ), f.serialize(), function ( result ) {

				close( eval( result ) );
			} );
			//return false;

		}
	</script>

	<script>
		$( document ).ready( function () {
			$( "#select2_sample_modal_5" ).select2( {

				/*	data: [
		 <?php $admins = $this->dashboard_model->fetch_admin();
					foreach($admins as $adm){?>
    {
      id: '<?php echo $adm->admin_id; ?>',
      text: '<?php echo $adm->admin_first_name." ".$adm->admin_last_name; ?>'
    },
	<?php } ?>
	]*/

				/* tags: [ <?php $admins = $this->dashboard_model->fetch_admin();
					foreach($admins as $adm)
					{echo '"'.$adm->admin_id.'",';}?>]*/
			} );
		} );

		function get_val() {
			$( "#admin" ).val( $( "#select2_sample_modal_5" ).val() );
			//alert($("#admin").val());
		}
	</script>
	<!-- END CONTENT -->
	