<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>    
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Broker's </div>
    <div class="actions">
    	<a href="<?php echo base_url();?>dashboard/add_broker" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
    </div>
  </div>
  <div class="portlet-body">    
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <!--<th scope="col">
                            Select
                        </th>-->
          <th scope="col"> Photo </th>
          <th scope="col"> Agency Type </th>
          <th scope="col"> Agency Name </th>
          <th scope="col"> Contact Name </th>
          <th scope="col"> Address </th>
          <th scope="col"> Contact No </th>
          <th scope="col"> Website </th>
          <th scope="col"> Broker Commision </th>
          <!--<th scope="col">
                            Broker Address
                        </th>
                        <th scope="col">
                            Broker Contact
                        </th>
                        <th scope="col">
                            Broker Email
                        </th>
                        <th scope="col">
                            Broker Website
                        </th>

                        <th scope="col">
                            Broker Pan Id
                        </th>
                        <th scope="col">
                            Broker Bank Account
                        </th>
                        <th scope="col">
                            Broker Bank IFSC Code
                        </th>
                        <th scope="col">
                            Broker Photo
                        </th>-->
          <th scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($broker) && $broker):
                        $i=1;
                        
                        foreach($broker as $brk):
                        
                            $class = ($i%2==0) ? "active" : "success";

                            $b_id=$brk->b_id;
                            ?>
        <tr> 
          <!-- <td width="50">
                                    <div class="md-checkbox pull-left">
                                        <input type="checkbox" id="checkbox1" class="md-check">
                                        <label for="checkbox1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                        </label>
                                    </div>
                                </td>-->
          <td width="5%">
              <a class="single_2" href="<?php echo base_url();?>upload/broker/image/<?php if( $brk->b_photo_thumb == '') { echo "Broker.jpg"; } else { echo $brk->b_photo_thumb; }?>">
              <img src="<?php echo base_url();?>upload/broker/image/<?php if( $brk->b_photo_thumb == '') { echo "Broker.jpg"; } else { echo $brk->b_photo_thumb; }?>" alt="" style="width:100%;"/>
              </a>
          </td>
          <td><?php if($brk->b_agency == 5)
                                    { echo "Agency";
                                    }else{
                                        echo "Self";
                                    }?></td>
          <td><?php echo $brk->b_agency_name ?></td>
          <td><?php echo $brk->b_name ?></td>
          <td><?php echo $brk->b_address ?></td>
          <td><?php echo '<span style"color:#AAAAAA;" class="fa fa-phone" aria-hidden="true"><span class="td-icon">'.$brk->b_contact.'</span></span>' ?></td>
          <!--<td><?php //echo $brk->b_email ?></td>-->
          <td><?php 
		  	if($brk->b_website == '' && $brk->b_website == NULL){
                            echo '<span style="color:#AAAAAA;">No info</span>';
                        }
                	else{
						echo '<span style"color:#AAAAAA;" class="fa fa-globe" aria-hidden="true"><span class="td-icon">'.$brk->b_website.'</span></span>';
					}
		   ?></td>
          <td><?php echo $brk->broker_commission ,"%"; ?></td>
          <!--<td>
			<?php //echo $brk->b_pan ?>
          </td>
          <td>
            <?php //echo $brk->b_bank_acc_no ?>
          </td>
          <td>
            <?php //echo $brk->b_bank_ifsc ?>
          </td>-->          
          <td class="ba">
          <div class="btn-group">
          <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
          <ul class="dropdown-menu pull-right" role="menu">
            <li><a onclick="soft_delete('<?php echo $brk->b_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
            <li><a href="<?php echo base_url();?>dashboard/edit_broker?b_id=<?php echo $brk->b_id; ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a></li>
          </ul>
        </div>
          </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_broker?b_id="+id,
                data:{booking_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
</script> 
