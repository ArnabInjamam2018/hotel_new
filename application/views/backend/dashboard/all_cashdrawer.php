

    <div class="portlet light borderd">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-th-list"></i> <strong> ALL CASHDRAWER</strong> </div>
        <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            
            <div class="col-md-8">
              <?php

	                            $form = array(
	                                'class'       => 'form-inline ftop',
	                                'id'        => 'form_date',
	                                'method'      => 'post'
	                            );

	                            echo form_open_multipart('#',$form);

	                            ?>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
              </div>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
              </div>
			  
			  <div class="form-group">
              <button onclick="get_val();" class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search" aria-hidden="true"></i></button>
			  </div>
			 
                  
              <?php form_close(); ?>
            </div>
            <script type="text/javascript">
				function check_sub(){
				   document.getElementById('form_date').submit();
				}
			</script>
            
          </div>
        </div>
		
			      
			<?php //echo "<pre>";print_r($this->session->userdata('pre_log_key'));  ?>
	<div id="table1">		
       <table class="table table-striped table-bordered table-hover" id="sample_1">
        <!--<table class="table table-striped">-->
          <thead>
            <tr>
              
			  <!--<th> Select </th>-->
              
              <th> # </th>
              <th> Cash Drawer Name </th>
              <th> Start Date </th>
              <th> End Date </th>
              <th> Starting Cash </th>
              <th> Closing Cash</th>
              <th> Withdraw </th>
              <th> Cash </th>
              <th> Card </th>
              <th> Fund </th>
              <th> Cheque </th>
              <th> Draft </th>
              <th> Ewallet </th>
             <th width="5%" scope="col" class="action"> Options </th>
			  
              
              
             <!-- <th width="5%" scope="col" class="action"> Options </th>-->
            </tr>
          </thead>
          <tbody>
            <?php $i=0;
			
			//echo "<pre>";
						//	print_r($drawers); exit;
			if(isset($drawers) && $drawers){
					
					$j=0;
						
                        foreach($drawers as $cashd){
						$j++;	
						
						if(	$cashd->is_ended){
							$cla='<span style="">'.$cashd->cashDrawerName.'</span>';
						}else{
							$cla='<span style="color:#008822; font-weight:bold;">'.$cashd->cashDrawerName.'</span>';
						}
						
		?>
         
									
          <tr>
            <td>  <?php 
					echo $j;
				  ?>
			 </td>
			 <td>  <?php 
					echo $cla;
				  ?>
			 </td>
			<td>
				<?php 
					echo date("g:i A \-\n l jS F Y",strtotime($cashd->opening_datetime));
				  ?>
			  
			</td>
			<td>
				<?php 
					if($cashd->closing_datetime > 0)
						echo date("g:i A \-\n l jS F Y",strtotime($cashd->closing_datetime));
				  ?>
			  
			</td>
            <!--<td>
				<?php 
					//echo $cashd->opening_cash;
				  ?>
			  
			</td>-->
            <td align="center">
				<?php 
					echo $cashd->opening_cash;
				  ?>
			  
			</td>
            	<td align="center">
				<?php 
					echo $cashd->closing_cash;
				  ?>
			  
			</td>
			<td align="center"><?php echo $cashd->withdraw; ?></td>
			
			<td align="center"><?php echo $cashd->cash_collection; ?></td>
			<td align="center"><?php echo $cashd->cards; ?></td>
			<td align="center"><?php echo $cashd->fund; ?></td>
			<td align="center"><?php echo $cashd->cheque; ?></td>
			<td align="center"><?php echo $cashd->draft; ?></td>
			<td align="center"><?php echo $cashd->ewallet; 
			
			//$Name= explode('-', $cashd->cashDrawerName,5);
					 //$Name=implode("-",$Name);
					
					//print_r($Name[4]);?></td>
			<td align="center" class="ba">
            	<div class="btn-group">
                  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    
					
					
                    <li><a onclick="edit_cash('<?php echo $cashd->drawer_id?>','<?php 
					
					$Name= explode("-", $cashd->cashDrawerName,5);
					 //$Name=implode("-",$Name);
					
					echo $Name[4];
					//echo $cashd->cashDrawerName?>','<?php echo $cashd->profitCenter?>','<?php echo $cashd->deposit?>')" data-toggle="modal" class="btn green btn-xs"><i title="Edit" class="fa fa-edit"></i></a>
					</li>
					
					<li><a onclick="cashdrawer_withdraw('<?php echo $cashd->drawer_id?>','<?php echo $cashd->cash_collection?>','<?php echo $cashd->deposit?>','<?php echo $cashd->withdraw?>')" data-toggle="modal" class="btn green btn-xs"><i title="withdraw" class="fa fa-window-maximize"></i></a>
					</li>
					
                    
					
                    
                  </ul>
                </div>
			  </td>
				
					
				 
			  
			
			
			
			
            <!--<td align="center" class="ba">
            	<div class="btn-group">
                  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    
					
					
                    <li><a href="<?php //echo base_url();?>dashboard/booking_edit?b_id=<?php 
					//echo $cashd->drawer_id ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a>
					</li>
					
                    
					
                    
                  </ul>
                </div>
			  </td>-->
          </tr>
		   <input type="hidden" id="item_no" value="<?php //echo $item_no;?>"> </input>
		   
			<?php $i++; ?>
          
          <?php  
			}}
		  ?>
         
            </tbody>
          
        </table>
      
    </div>
    </div>
    </div>
	<input type="hidden" name="hcid" id="hcid">
	<input type="hidden" name="hcashcollection" id="hcashcollection">
	<input type="hidden" name="hdiposit" id="hdiposit">
	<input type="hidden" name="hwith" id="hwith">
	
<div id="edit_cash" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" id="clickclick" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Cashdrawer</h4>
      </div>
      <div class="modal-body" style="height:225px;">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" id="name1"   min="1" max="<?php //echo $c; ?>" name="name1" onfocus="abc()" placeholder="Name">
              <span class="help-block">Name *</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select name="profitcenter1" class="form-control bs-select" id="profitcenter1" required>
                <?php $pc=$this->dashboard_model->all_pc();
                                foreach($pc as $prfit_center){
                                ?>
                <option value="<?php echo $prfit_center->id;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                <?php }?>
              </select>
              <span class="help-block">Profit center *</span> </div>
          </div>
          <input type="hidden" name="hcid" id="hcid">
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="number" class="form-control" id="deposit1" name="deposit1" onfocus="abc1()" placeholder="Deposit">
              <span class="help-block">Deposit </span> </div>
          </div>
          <!--  <div class="col-md-4">
		  	<div class="form-group form-md-line-input" id="hd">
			</div>
          </div>--> 
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" onclick="edit_cashdrawer();" class="btn submit" >Assign</button>
      </div>
    </div>
  </div>
</div>

<div id="cashdrawer_withdraw" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" id="clickclick" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Withdraw Cashdrawer</h4>
      </div>
      <div class="modal-body" style="height:225px;">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" id="withdraw1" onkeyup="check(this.value)"  min="1" max="<?php //echo $c; ?>" name="withdraw1" onfocus="abc()" placeholder="amount">
              <span class="help-block">withdraw *</span> </div>
          </div>
          
          
         
		 <div class="col-md-6">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" id="note1" name="note1"  placeholder="Reason">
              <span class="help-block">Note</span> </div>
          </div>
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" onclick="withdraw_cashdrawer();" class="btn submit" >withdraw</button>
      </div>
    </div>
  </div>
</div>
<script>

   function fetch_data(val){
	   alert()
	   $.ajax({
                
                url: "<?php echo base_url()?>dashboard/get_no_tax_booking",
				success:function(data)
                { 
                   $('#table1').html(data);
				   $('#dataTable2').dataTable( {
						"pageLength": 10 
					} );
                 }
            });
   }
   
					
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            //alert(id);
		$.ajax({
                
                url: "<?php echo base_url()?>dashboard/soft_delete_booking?id="+id,
				type:"POST",
                data:{id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });

        });
    }
	
function download_pdf(booking_id) {
	
	
	
	//alert(booking_id);
	$("#dwn_link").attr("href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id);
	var f = $("#f");
            $.post(f.attr("action"), f.serialize(), function (result) {
            
                close(eval(result));
            });
	 //return false;
	
}
	
	
</script> 

<script>

$( document ).ready(function() {
	$("#select2_sample_modal_5").select2({
		
	/*	data: [
		 <?php $admins = $this->dashboard_model->fetch_admin();
					foreach($admins as $adm){?>
    {
      id: '<?php echo $adm->admin_id; ?>',
      text: '<?php echo $adm->admin_first_name." ".$adm->admin_last_name; ?>'
    },
	<?php } ?>
	]*/
		 
          /* tags: [ <?php $admins = $this->dashboard_model->fetch_admin();
					foreach($admins as $adm)
					{echo '"'.$adm->admin_id.'",';}?>]*/
        });
});

function get_val()
{
	$("#admin").val($("#select2_sample_modal_5").val());
	//alert($("#admin").val());
}
function edit_cash(id,name,profit,diposit){
	/*alert(id);
	alert(name);
	alert(profit);
	alert(diposit);*/
	$('#name1').val(name);
	$('#profitcenter1').val(profit).change();
	$('#deposit1').val(diposit);
	$('#hcid').val(id);
	$('#edit_cash').modal('show');
	
}
function cashdrawer_withdraw(id,cashcollection,diposit,withd){
	$('#hcid').val(id);
	$('#hcashcollection').val(cashcollection);
	$('#hdiposit').val(diposit);
	$('#hwith').val(withd);
	
	$('#cashdrawer_withdraw').modal('show');
}
function edit_cashdrawer(){
	var name=$('#name1').val();
	var deposit=$('#deposit1').val();
	var profitcenter=$('#profitcenter1').val();
	var id=$('#hcid').val();
	jQuery.ajax(
                        {
                            type: "POST",
                            url: "<?php echo base_url(); ?>cashdrawer/edit_cash_drawer",
                           // dataType: 'json',
                            data: {name:name,deposit:deposit,profitcenter:profitcenter,id:id},
                            success: function(data){
								//alert(data);
							if(data==1){
								$('#responsive2_cash').modal('hide');
								swal("Success!", name+" is set as new cashdrawer : ", "success");
								location.reload();
							}
                            }
                        });
						redirect('dashboard');
	
}

function w_cashdrawer(){
	var name=$('#name1').val();
	var deposit=$('#deposit1').val();
	var profitcenter=$('#profitcenter1').val();
	var id=$('#hcid').val();
	jQuery.ajax(
                        {
                            type: "POST",
                            url: "<?php echo base_url(); ?>cashdrawer/edit_cash_drawer",
                           // dataType: 'json',
                            data: {name:name,deposit:deposit,profitcenter:profitcenter,id:id},
                            success: function(data){
								//alert(data);
							if(data==1){
								$('#responsive2_cash').modal('hide');
								swal("Success!", name+" is set as new cashdrawer : ", "success");
								location.reload();
							}
                            }
                        });
						redirect('dashboard');
	
}

function check(v){
	
	var id=$('#hcid').val();
	var cashcollection=parseFloat($('#hcashcollection').val());
	var diposit=parseFloat($('#hdiposit').val());
	var withdraw=parseFloat($('#hwith').val());
	//alert(id);
	var a=cashcollection+diposit-withdraw;
	//alert(a);
	if(v>a){
		$('#cashdrawer_withdraw').modal('hide');
		swal("warning!", "withdraw must be lower than cash available!", "warning");
		$('#withdraw1').val('');
		///$('#cashdrawer_withdraw').modal('show');
		
	}
	//alert(cashcollection);
	//alert(diposit);
}


function withdraw_cashdrawer(){
	var withdraw=$('#withdraw1').val();
	var note=$('#note1').val();	
	var id=$('#hcid').val();
	alert(withdraw);
	alert(note);
	alert(id);
	jQuery.ajax(
                        {
                            type: "POST",
                            url: "<?php echo base_url(); ?>cashdrawer/withdraw_cashdrawer",
                           // dataType: 'json',
                            data: {id:id,withdraw:withdraw,note:note},
                            success: function(data){
								//alert(data);
							if(data==1){
								$('#responsive2_cash').modal('hide');
								swal("Success!", name+" is set as new cashdrawer : ", "success");
								location.reload();
							}
                            }
                        });
						redirect('dashboard');
	
}
</script>
<!-- END CONTENT --> 
