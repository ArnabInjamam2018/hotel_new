<?php if($this->session->flashdata('err_msg')):?>
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet bordered light">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-certificate" aria-hidden="true"></i>List of All Compliance </div>
    <div class="actions">
    	<a href="<?php echo base_url();?>dashboard/add_compliance" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <!--<th scope="col">
                        Select
                    </th>-->
          
          <th scope="col"> Sl </th>
		  <th scope="col" width="10%"> Photo </th>
		  <th scope="col" width="10%"> Photo 2 </th>
          <th scope="col"> Name </th>
          <th scope="col"> Valid for </th>
          <th scope="col"> Authority </th>
          <th scope="col"> Owner </th>
          <th scope="col"> Type </th>
          <th scope="col"> Importance </th>
          <th scope="col"> Valid Form </th>
          <th scope="col"> Valid Up to </th>
          <th scope="col"> Renewal Date </th>
          <th scope="col"> Renewal Status </th>
          <th scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php 
			$color='';
			$days_e = 0; // Date Difference in Days
			$hours_e = 0;
			$min_e = 0;
			$disp = "";
			if(isset($compliance) && $compliance):
                    $i=1;
					$sl = 0;
                    foreach($compliance as $comp):   // Please stop using this style "_"
                        $class = ($i%2==0) ? "active" : "success";
						$sl++;

                        $c_id=$comp->c_id;
			/*$today=date("d-m-Y");
			$renewal=date("d-m-Y",strtotime($comp->c_renewal));
			//echo $renewal;  
			if($today > $renewal){
				 $color='#ffb9de';
				
				
			}*/ // What the hell is this, is this supposed to even work?! @Sanjib FYI @Ashes Da
			
			// Please see how to do this Properly!
			date_default_timezone_set('Asia/Kolkata'); //This is a must as server location can be global (Better set this in session)
			$exp = explode(' ',$comp->c_renewal);
			$renewal=new DateTime($exp[0]." "."00:00:00");
			$today = new DateTime();
			
			$dDiff = $renewal->diff($today);
            $days_e = $dDiff->format('%r%a'); // Date Difference in Days
			$hours_e = $dDiff->format('%r%h');
			$min_e = $dDiff->format('%r%i');
			
			// Logic for finding the date difference factorized
			// (-) Means remaining
			
			if(($days_e > 0))
			{
				$disp = ' Due';
				$color='#F9F9F9';				
			}
			else if($days_e < 0){
				$disp = ' Remaining';
			}				
			else if(($days_e == 0) && (($hours_e >= 0) |  ($min_e >= 0)))
			{
				$disp = ' Due';
				$color='#F9F9F9';
			}
			else if(($days_e == 0) && (($hours_e < 0) || ($min_e < 0)))
			{
				$disp = ' Remaining';
			}
			if($days_e > -5 && $days_e < 0)
				$color='#F9F9F9';
			
		?>
        
		
		<tr style="background-color:<?php /*echo $color;*/?>"> <!-- To make the rows colored -->
          <!-- <td width="50">
                                <div class="md-checkbox pull-left">
                                    <input type="checkbox" id="checkbox1" class="md-check">
                                    <label for="checkbox1">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span>
                                    </label>
                                </div>
                            </td>-->
          
          <td><?php echo $sl; ?></td>
		  <td><a class="single_2" href="<?php echo base_url();?>upload/certificate/thumb/cer_1/<?php if( $comp->c_file_thumb== '') { echo "no_images.png"; } else { echo $comp->c_file_thumb; }?>"><img src="<?php echo base_url();?>upload/certificate/original/cer_1/<?php if( $comp->c_file== '') { echo "no_images.png"; } else { echo $comp->c_file; }?>" alt="" style="width:100%;"/></a>
		  </td>
		  <td><a class="single_2" href="<?php echo base_url();?>upload/certificate/thumb/cer_2/<?php if( $comp->c_file_2_thumb== '') { echo "no_images.png"; } else { echo $comp->c_file_2_thumb; }?>"><img src="<?php echo base_url();?>upload/certificate/original/cer_2/<?php if( $comp->c_file_2== '') { echo "no_images.png"; } else { echo $comp->c_file_2; }?>" alt="" style="width:100%;"/></a>
		  </td>
		  <td>
			<?php // Certificate Name
				echo $comp->c_name;
				if($days_e > 0){
					echo '  <i style="color:#DD5063;"; class="fa fa-exclamation-circle" aria-hidden="true"></i>';
				}
				else if($days_e > -5 && $days_e < 0)
					echo '  <i style="color:#E0B566;"; class="fa fa-exclamation-circle" aria-hidden="true"></i>';
				else
					echo '  <i style="color:#6F9F67;"; class="fa fa-check" aria-hidden="true"></i>';
								
			?>
		  </td>
          <td>
			<?php // Valid For
				if($comp->c_valid_for == 'Self')
					echo '<Span class="label-" style="background-color:#F6EFE1; color:#000000;";>'.$comp->c_valid_for.'</span>'; 
				else
					echo '<Span class="label" style="background-color:#BEE0FF; color:#000000;";>'.$comp->c_valid_for.'</span>';
			?>
		  </td>
         
          <td>
			<?php // Certificate Authority
				echo $comp->c_authority; 
			?>
		  </td>
          <td>
			<?php // Certificate Owner
				echo $comp->c_owner; 
			?>
		  </td>
          <td>
			<?php // Certificate Type
				echo $comp->c_type; 
			?>
		  </td>
          <td> 
			<?php // Certificate Importance
				$color= $comp->c_importance;
                                if($color=="r"){

                                    echo '<span class="label" style="background-color:#3E3274; color:#ffffff;">High Importance</span>';
                                }
                                else if($color=="y"){

                                    echo '<span class="label" style="background-color:#52C6C9; color:#ffffff;">Low Importance</span>';
                                }
                                else if($color=="o"){
                                    echo '<span class="label" style="background-color:#F6990A; color:#ffffff;">Moderate Importance</span>';

                                }
								else
									echo '<span class="label" style="background-color:#D2BDB3; color:#ffffff;">No Importance</span>';

                                ?>
		  </td>
          <td>
			<?php // Valid From Date
				echo date("l jS F Y",strtotime($comp->c_valid_from)); 
			?>
		  </td>
          <td>
			<?php // Valid Upto Date
				echo date("l jS F Y",strtotime($comp->c_valid_upto)); 
			?>
		  </td>
          <td>
			<?php // Renewal Date
				echo date("l jS F Y",strtotime($comp->c_renewal)); 
			?>
		  </td>
		  <td>
			<?php // Renewal Status
				echo abs($days_e)." Day ".abs($hours_e)." Hr ".abs($min_e)." Min ";
				if($disp == ' Due'){
					echo '<span class="label glow" style="background-color:#E52828; color:#ffffff;";>'.$disp.'</span>';
				}
				else if($days_e > -5 && $days_e < 0)
					echo '<span class="label" style="background-color:#E0B566; color:#ffffff;";>Expiring Soon</span>';
				else
					echo '<span class="label" style="background-color:#6F9F67; color:#ffffff;";>'.$disp.'</span>';
			?>
		  </td>
          <td class="ba">   <!-- Action -->
          	<div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $comp->c_id; ?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
                <li><a href="<?php echo base_url() ?>dashboard/edit_compliance?c_id=<?php echo $c_id; ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a></li>
				<li><a href="<?php echo base_url();?>upload/certificate/original/cer_2/<?php if( $comp->c_file_2== '') { echo "no_images.png"; } else { echo $comp->c_file_2; }?>" download class="btn red btn-xs"><i class="fa fa-download"></i></a></li>
				<li><a href="<?php echo base_url();?>upload/certificate/original/cer_1/<?php if( $comp->c_file== '') { echo "no_images.png"; } else { echo $comp->c_file; }?>" download class="btn purple btn-xs"><i class="fa fa-download"></i></a></li>
			  </ul>
            </div>
          </td>
        </tr>
        <?php $i++;?>
			<?php  endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>

<script>

    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_compliance?c_id="+id,
                data:{c_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
</script> 
