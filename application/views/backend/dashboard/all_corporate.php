<?php if($this->session->flashdata('err_msg')):?>
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-industry"></i>All Associated Corporates </div>
    <div class="actions">
    	<a href="<?php  echo base_url()?>dashboard/add_corporate" class="btn btn-circle green btn-outline btn-sm" data-toggle="modal"><i class="fa fa-plus"></i>Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
		  
          <th width="15%" scope="col"> Corporate Name </th>
          <th scope="col"> Company Address </th>
          
          <th scope="col"> Industry </th>
		  
          <th scope="col" width="15%"> Employees </th>
          <!-- <th scope="col"> Rating </th> -->
          
          <th scope="col"> Contact Info </th>
          <th scope="col"> Manager Details </th>
          <th scope="col"> Director Details </th>
          <th scope="col"> No Of Booking </th>
          <th scope="col"> Total Transaction </th>
		  <th scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($corporate) && $corporate):
                        
                        $i=1;
                        foreach($corporate as $value):
                            $class = ($i%2==0) ? "active" : "success";
                            $c_id=$value->hotel_corporate_id;
                            ?>
        <tr id="row<?php echo $value->hotel_corporate_id; ?>">
          <td>
			<?php // Corporate Name
				echo '<strong>'.$value->name.'</strong>';
				$col = 'na';
				if($value->rating == 1)
					$col = '#F3565D';
				else if($value->rating == 2)
					$col = '#FF6E43';
				else if($value->rating == 3)
					$col = '#07608F';
				else if($value->rating == 4)
					$col = '#00BCD4';
				else if($value->rating == 5)
					$col = '#26A69A';
				
				if($col != 'na')
					echo '<br>'.$value->rating.' <i style="color:'.$col.';" class="fa fa-star" aria-hidden="true"></i>' ;
			?>
		  </td>
          
		  <td>
			<?php // Address
				//echo $value->address; 
				// Logic for showing Full Address properly _sb dt30_9_16
						
							$fg = 10;
						if(isset($value->address) && ($value->address != '' && $value->address != ' ')){
								echo ($value->address);
								$fg = 0	;
								
						}
						if(isset($value->city) && ($value->city != '' && $value->city != ' ')){
								if($fg == 0){
									echo ', ';
									
								}
									
								echo $value->city;
								echo '<br>';
								$fg = 1;
						}
						else if($fg == 0)
								$fg = 5;
						if(isset($value->state) && ($value->state != '' && $value->state != ' ')){
								if($fg == 5)
									echo '<br>';
								echo $value->state;
								$fg = 2;
								
							}
						if(isset($value->pincode) && $value->pincode!= NULL){
								if($fg == 2 || $fg == 5){
									echo ', ';
								}									
								echo 'Postal Code - '.$value->pincode;
								$fg = 3;
							}
						if(isset($value->country) && $value->country != '' && $value->country != ' '){
								if($fg == 3 || $fg == 2)
									echo ', ';
								echo $value->country;
								$fg = 4;
							}
						if($fg == 10){
								echo '<span style="color:#AAAAAA;">No info</span>';
							}
			?>
		  </td>
          
		  <td>
			<?php // Industry
				$f = 0;
				if($value->industry_type){
					echo $value->industry_type;
					$f = 1;
				}
				if($value->field != ''){
					echo '<br>'.$value->field;
					$f = 1;
				}
				if($f == 0)
					echo '<span style="color:#AAAAAA;">No info</span>';
			?>
		  </td>
         	  
		  <td>
			<?php  // Employee #
				$f = 0;
				if($value->no_of_emp){
					echo '<strong>Employee: </strong>'.$value->no_of_emp;
					$f = 1;
				}
				if($value->number_branch){
					echo '<br><strong>Branch: </strong>'.$value->number_branch;
					$f = 1;
				}
				
				if($f == 0)
					echo '<span style="color:#AAAAAA;">No info</span>';
            ?>
		  </td>
		  
		  <td>
			<?php // Contact Info
			
				$f = 0;
				if($value->contact_person){				
					echo '<strong>'.$value->contact_person.'</strong>';
					$f = 1;
				}
				if($value->g_contact_no){
					
					echo '<br><i style"color:#AAAAAA;" class="fa fa-phone" aria-hidden="true"></i>  &nbsp;'.$value->g_contact_no;
					$f = 1;
				}
				if($value->corp_email){
					echo '<br><span style"color:#AAAAAA;" class="fa fa-envelope" aria-hidden="true"><span class="td-icon">'.$value->corp_email.'</span></span>';
					$f = 1;
				}
				if($f == 0)
					echo '<span style="color:#AAAAAA;">No info</span>';
			?>
		  </td>
		  
		  <td>
			<?php // Manager Details
				//echo $value->g_contact_no;
				$f = 0;
				if($value->manager_name){
					
					echo '<strong>'.$value->manager_name.'</strong>';
					$f = 1;
				}
				if($value->manager_contact_no){
					
					echo '<br><span style"color:#AAAAAA;" class="fa fa-phone" aria-hidden="true"><span class="td-icon">'.$value->manager_contact_no.'</span></span>';
					$f = 1;
				}
				if($value->corp_manager_email){
					echo '<br><span style"color:#AAAAAA;" class="fa fa-envelope" aria-hidden="true"><span class="td-icon">'.$value->corp_manager_email.'</span></span>';
					$f = 1;
				}
				
				if($f == 0)
					echo '<span style="color:#AAAAAA;">No info</span>';
			?>
		  </td>
		  
		  <td>
			<?php // Director Details
				$f = 0;
				if($value->director_name){				
					echo '<strong>'.$value->director_name.'</strong>';
					$f = 1;
				}
				if($value->director_contact_no){					
					echo '<br><i style"color:#AAAAAA;" class="fa fa-phone" aria-hidden="true"></i>  &nbsp;'.$value->director_contact_no;
					$f = 1;
				}
				if($value->corp_director_email){
					echo '<br><span style"color:#AAAAAA;" class="fa fa-envelope" aria-hidden="true"><span class="td-icon">'.$value->corp_director_email.'</span></span>';
					$f = 1;
				}
				
				if($f == 0)
					echo '<span style="color:#AAAAAA;">No info</span>';
			?>
		  </td>
		  
          <td>
			<?php 
				echo $value->no_of_booking;
			?>
		  </td>
		  
          <td>
			<?php 
				echo $value->total_transaction;
			?>
		  </td>
          <td class="ba">
            <div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $value->hotel_corporate_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
                <li><a onclick="edit_corporate('<?php echo $value->hotel_corporate_id; ?>')" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a></li>
              </ul>
            </div>
          </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<script>

$(document).on('blur', '#ced', function () {
	$('#ced').addClass('focus');
});
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_corporate?c_id="+id,
                data:{booking_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            $('#row'+id).remove();

                        });
                }
            });
        });
    }
</script>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('dashboard/add_corporate',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Corporate</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group form-md-line-input ">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="name" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Name *">
                <label></label>
                <div class="highlight"></div>
                <span class="help-block">Name *</span> </div>
            </div >
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="mobile" name="g_contact_no"  required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" placeholder="Mobile No">
                <label></label>
                <span class="help-block">Mobile No</span>
                <div class="highlight"></div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="industry" placeholder="Industry*">
                <label></label>
                <span class="help-block">Industry</span>
                <div class="highlight"></div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="no_of_emp" placeholder="No of employees">
                <label></label>
                <span class="help-block">No of employees</span>
                <div class="highlight"></div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="contract_discount" placeholder="Contract discount">
                <label></label>
                <span class="help-block">Contract discount</span>
                <div class="highlight"></div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control date-picker" id="ced" name="contract_end_date" placeholder="Contract end date">
                <label></label>
                <span class="help-block">Contract end date</span> </div>
            </div>
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="address" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Address">
                <label></label>
                <span class="help-block">Address</span>
                <div class="highlight"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<div id="edit_corporate" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('unit_class_controller/update_corporate',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Corporate Here</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group form-md-line-input ">
                <input type="hidden" name="hid" id="hid">
                <input autocomplete="off" type="text" class="form-control" id="name1" name="name1" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Name *">
                <label></label>
                <div class="highlight"></div>
                <span class="help-block">Name *</span> </div>
            </div >
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="mobile1" name="g_contact_no1"  required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" placeholder="Mobile No">
                <label></label>
                <span class="help-block">Mobile No</span>
                <div class="highlight"></div>
              </div>
            </div>
           
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control" id="contract_discount1" name="contract_discount1" placeholder="Contract discount">
                <label></label>
                <span class="help-block">Contract discount</span>
                <div class="highlight"></div>
              </div>
            </div>
			
			<?php 
		$industry=$this->unit_class_model->all_industry_type();
		?>
		<div class="col-md-6">
			<div class="form-group form-md-line-input">
              <select class="form-control bs-select" name="industry_type" id="industry_type" required >
			  <option value="0" >Select</option>
			  <?php

			  if(isset($industry) && $industry){
				  foreach($industry as $ind){			  
			  
			  ?>
                <option value="<?php echo $ind->it_name ?>" ><?php echo $ind->it_name ?></option>
			  <?php }} ?>
              </select>
              <label></label>
              <span class="help-block">Industry type</span>
			</div>
			</div>
			
			
			 <div class="col-md-6"> 
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="field" name="field" onkeypress=" return onlyNos(event, this);" required="required" placeholder="Field of work">
              <label></label>
              <span class="help-block">Field of work</span>
            </div> 
        </div>
		
		<div class="col-md-6">
			<div class="form-group form-md-line-input">
              <select class="form-control bs-select" name="rating" id="rating" required >
			  <option value="0" >Select Rating</option>			  
                <option value="1" >1</option>
                <option value="2" >2</option>
                <option value="3" >3</option>
                <option value="4" >4</option>
                <option value="5" >5</option>			  
              </select>
              <label></label>
              <span class="help-block">Rating</span>
			</div>
			</div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control date-picker" id="ced1" name="contract_end_date1" placeholder="Contract end date">
                <label></label>
                <span class="help-block">Contract end date</span> </div>
            </div>						<div class="col-md-6">              <div class="form-group form-md-line-input">                <input autocomplete="off" type="text" class="form-control" id="gstin" name="gstin" required="required" placeholder="GSTIN No"/>                <label></label>                <span class="help-block">GSTIN No</span>                <div class="highlight"></div>              </div>            </div>
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="address1" name="address1" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Address">
                <label></label>
                <span class="help-block">Address</span>
                <div class="highlight"></div>
              </div>
            </div>						
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<script>
   function check_corporate(value){

       if(value =="corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
       }else{
           document.getElementById("c_name").style.display="none";
           document.getElementById("c_des").style.display="none";
       }
   }
    function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
</script> 
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}
$(document).on('blur', '#ced', function () {
	$('#ced').addClass('focus');
});


function edit_corporate(id){
		//alert(id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/edit_corporate",
                data:{id:id},
                success:function(data)
                {
     
                   $('#hid').val(data.hotel_corporate_id);
				   $('#name1').val(data.name);
				   $('#address1').val(data.address);
				   $('#mobile1').val(data.g_contact_no);
				   $('#industry1').val(data.industry);
				   $('#no_of_emp1').val(data.no_of_emp);
				   $('#contract_discount1').val(data.contract_discount);
				   $('#ced1').val(data.contract_end_date);
				   $('#no_of_booking').val('0');
				   $('#industry_type').val(data.industry_type).change();
				   $('#field').val(data.field);
				   $('#rating').val(data.rating).change();				   				   $('#gstin').val(data.gstin).change();
				   //$('#total_transaction').val(data.design);
				   		   
				   
				   
                    $('#edit_corporate').modal('toggle');
					
                }
            });
	}
</script>