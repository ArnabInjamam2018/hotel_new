    <?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
    <?php endif;?>
    <?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
    <?php endif;?>

<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-edit"></i>List of All DG sets </div>
		<div class="actions">
			<a class="btn btn-circle green btn-outline btn-sm" data-toggle="modal" href="#responsive"> <i class="fa fa-plus"></i>Add New </a>
		</div>
	</div>
	<div class="portlet-body">
		<table class="table table-striped table-bordered table-hover" id="sample_1">
			<thead>
				<tr>
					<!-- <th scope="col">
                                Select
                            </th>-->
					<th scope="col">Image</th>
					<th scope="col"> Generator Name </th>
					<th scope="col"> Asset Code </th>
					<th scope="col"> Make </th>
					<th scope="col"> Model </th>
					<th scope="col"> Ratings </th>
					<th scope="col"> Fuel </th>
					<th scope="col"> Cylinder </th>
					<th scope="col"> Design </th>
					<th scope="col"> Rotation </th>
					<th scope="col"> Alternator Make </th>
					<th scope="col"> Fuel Consumption</th>
					<th scope="col"> Status </th>
					<th scope="col"> Amc </th>

					<!--<th scope="col">
                                Id Proof
                            </th>-->
					<th scope="col"> Action </th>
				</tr>
			</thead>
			<tbody>
				<?php if(isset($dgset) && $dgset):
							
                            $i=1;
                            foreach($dgset as $dgs):
                                $class = ($i%2==0) ? "active" : "success";
                                $dgset_id=$dgs->dgset_id;
                                ?>
				<tr>
					<!-- <td width="50">
                                        <div class="md-checkbox pull-left">
                                            <input type="checkbox" id="checkbox1" class="md-check">
                                            <label for="checkbox1">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                            </label>
                                        </div>
                                    </td>-->

					<!-- <td>
                                   
                                    // <img  width="60px" height="60px" src="<?php echo base_url();?>upload/guest/<?php //if( $gst->g_photo_thumb== '') { echo "no_images.png"; } else { echo $gst->g_photo_thumb; }?>" alt=""/>
                                    
                                    
                                    <?php
										/*echo $gst->g_photo ;
										echo $gst->g_photo ."_thumb"; */
									?>
                                    
                                    </td> -->
					<td width="5%"><a class="single_2" href="<?php echo base_url();?>upload/dg_set/<?php if( $dgs->img== '') { echo " no_images.png "; } else { echo $dgs->img; }?>"><img src="<?php echo base_url();?>upload/dg_set/<?php if( $dgs->img== '') { echo "no_images.png"; } else { echo $dgs->img; }?>" alt="" style="width:100%;"/></a>
					</td>
					<td>
						<?php echo $dgs->gen_name; ?>
					</td>
					<td>
						<?php echo $dgs->asset_code;?>
					</td>
					<td>
						<?php
						echo $dgs->make;
						?>
					</td>
					<td>
						<?php echo $dgs->model ?>
					</td>
					<td>
						<?php echo $dgs->ratings ?>
					</td>
					<td>
						<?php echo $dgs->fuel ?>
					</td>
					<td>
						<?php echo $dgs->cylinders ?>
					</td>
					<!--  <td>
                                        <?php 
										
										// $now = time();
										// $date1 = strtotime($gst->g_dob);
										// $datediff = $now-$date1;
										// $datediff = floor($datediff/(60*60*24*365));
										// echo $datediff;
										
											//$var = $gst->g_dob;
											//echo $var;
											/*$var = date_create($gst->g_dob);
											echo $var;
											$d = date("Y/m/d");
											//echo $d;
											$current_date=date_create($d);
											//echo $current_date;
											$diff=date_diff($current_date, $var);
											
											echo $diff;*/
										
										?>
                                    </td> -->
					<td>
						<?php echo $dgs->rotation ?>
					</td>
					<td>
						<?php echo $dgs->design ?>
					</td>
					<td>
						<?php echo $dgs->alternator_make ?>
					</td>
					<td>
						<?php echo $dgs->fuel_consumption ?>
					</td>
					<td>
						<?php echo $dgs->status ?>
					</td>
					<td>
						<?php echo $dgs->amc ?>
					</td>
					<!-- <td>
                                        // <?php //$m= $gst->g_id_type;
                                        // if($m==1){
                                        //     echo "Passport";
                                        // }
                                        // elseif($m==2){ echo "PAN Card";}
                                        // elseif($m==3){echo "Voter Card";}
                                        // elseif($m==4){echo "Adhar Card";}
                                        // elseif($m==5){echo "Driving License";}
                                        // elseif($m==6){echo "Others";}


                                        ?>
                                    </td>-->

					<?php //echo $gst->g_photo ?>

					<!--<td>
                                        <img  width="100%" src="<?php //echo base_url();?>upload/<?php //if( $gst->g_id_proof== '') { echo "no_images.png"; } else { echo $gst->g_id_proof; }?>" alt=""/>
										<?php //echo $gst->g_id_proof ?>
                                    </td>-->

					<td class="ba">
						<div class="btn-group">
							<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li><a onclick="soft_delete('<?php echo $dgs->dgset_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a>
								</li>
								<li><a onclick="edit_dg_set1('<?php echo $dgs->dgset_id; ?>')" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a>
								</li>
							</ul>
						</div>
						<table>
							<tr>
								<td></td>

								<td></td>
							</tr>
						</table>
					</td>
				</tr>
				<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>

<script>
	function soft_delete( id ) {
		swal( {
			title: "Are you sure?",
			text: "All the releted transactions and data will be deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {





			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/delete_dgset?dgset_id=" + id,
				data: {
					a_id: id
				},
				success: function ( data ) {
					//alert("Checked-In Successfully");
					//location.reload();
					swal( {
							title: data.data,
							text: "",
							type: "success"
						},
						function () {

							location.reload();

						} );
				}
			} );



		} );
	}
</script>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
	<?php

	$form = array(
		'class' => 'form-body',
		'id' => 'form',
		'method' => 'post'
	);

	echo form_open_multipart( 'dashboard/add_dgset', $form );

	?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add DG set</h4>
			</div>
			<div class="modal-body">
				<div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
					<div class="row">
						<div class="form-group col-md-12" style="margin-bottom: 0;">
							<h3>Engine Intro</h3>
						</div>
						<div class="form-group form-md-line-input col-md-4">
							<input type="hidden" name="dgset_id">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" placeholder="Enter Generator Name" name="gen_name" required="required">
							<label></label>
							<span class="help-block">Enter Generator Name</span> </div>
						<div class="form-group form-md-line-input col-md-4">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" placeholder="Enter Asset Code of the product..." name="asset_code" required="required">
							<label></label>
							<span class="help-block">Enter Asset Code of the product</span> </div>
						<div class="form-group form-md-line-input col-md-4">
							<input autocomplete="off" type="text" class="form-control" id="make" name="make" placeholder="Enter the brand..." required="required" onblur="fetch_all_address()" onkeypress=" return onlyLtrs(event, this);">
							<label></label>
							<span class="help-block">Enter the brand</span> </div>
						<div class="form-group form-md-line-input col-md-4">
							<input autocomplete="off" type="text" class="form-control" id="model" name="model" placeholder="Enter the model no..." value="" required="required"/>
							<label></label>
							<span class="help-block">Enter the model no...</span> </div>
						<div class="form-group form-md-line-input col-md-4">
							<select class="form-control bs-select" name="ratings" required>
								<option value="">Select Ratings</option>
								<?php foreach($rating as $rat) 
				{
					$value=$rat->Rating." ".$rat->Unit;?>
								<option value="<?php echo $value;?>">
									<?php echo $value; ?>
								</option>
								<?php } ?>
							</select>
							<label></label>
							<span class="help-block">Select Ratings</span> </div>
						<div class="form-group form-md-line-input col-md-4">
							<select class="form-control bs-select" name="fuel" required>
								<option value="" selected>Select Fuel</option>
								<?php foreach($master_log as $mas) 
			  {
		echo "<option value='".$mas->fuel_name."'>".$mas->fuel_name."</option>";
			  }
		?>
							</select>
							<label></label>
							<span class="help-block">Select Fuel</span> </div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-12">
							<h3>Engine Details</h3>
						</div>
						<div id="c_choice" class="form-group form-md-line-input col-md-4">
							<select class="form-control bs-select" name="cylinder_choice">
								<option value="4">4</option>
								<option value="6">6</option>
								<option value="8">8</option>
								<option value="12">12</option>
							</select>
							<label></label>
							<span class="help-block">Select Fuel...</span>
						</div>
						<div id="r_choice" class="form-group form-md-line-input col-md-4">
							<select class="form-control bs-select" name="Rotation_choice">
								<option value="CW">CW</option>
								<option value="CWW">CWW</option>
							</select>
							<label></label>
							<span class="help-block">Select Rotation</span>
						</div>
						<div id="d_choice" class="form-group form-md-line-input col-md-4">
							<select class="form-control bs-select" name="Design_choice">
								<option value="Water Cooled"> Water Cooled</option>
								<option value="Direct Inject"> Direct Inject</option>
							</select>
							<label></label>
							<span class="help-block">Design</span>
						</div>
						<div class="form-group form-md-line-input col-md-4">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" name="alternator_make" required="required">
							<label> Alternator Make<span class="required">*</span></label>
							<span class="help-block">Enter Alternator make...</span> </div>
						<div class="form-group form-md-line-input col-md-4">
							<input autocomplete="off" type="text" class="form-control" id="fuel_consumption" name="fuel_consumption" placeholder="Fuel Consumption Per Litre" onkeyup="chk_if_num('fuel_consumption');" required="required">
							<label></label>
							<span class="help-block">Enter Fuel consumed per liter</span> </div>
						<div class="form-group form-md-line-input col-md-4">
							<select class="form-control bs-select" id="form_control_2" name="status" required>
								<option value="Active">Active</option>
								<option value="Faulty">Faulty</option>
								<option value="Under Maintanance">Under Maintanance</option>
							</select>
							<label></label>
							<span class="help-block">Status</span>
						</div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-4">
							<input autocomplete="off" type="text" class="form-control date-picker" id="amc-d" name="amc" placeholder="Next AMC Date">
							<label></label>
							<span class="help-block">Enter Annual Maintanance Date...</span> </div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-12">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
								<div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
									<?php echo form_upload('dg_image');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn default">Close</button>
				<button type="submit" class="btn green">Save</button>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<div id="edit_dg_set" class="modal fade" tabindex="-1" aria-hidden="true">
	<?php

	$form = array(
		'class' => 'form-body',
		'id' => 'form',
		'method' => 'post'
	);

	echo form_open_multipart( 'unit_class_controller/update_dgset', $form );

	?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit DG set</h4>
			</div>
			<div class="modal-body">
				<div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
					<div class="row">
						<div class="form-group form-md-line-input form-md-floating-label col-md-4">
							<input type="hidden" id="dgset_id1" name="dgset_id1">
							<input autocomplete="off" type="text" class="form-control" id="gen_name1" placeholder="Enter Generator Name" name="gen_name1" required="required">

							<span class="help-block">Enter Generator Name...</span> </div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-4">
							<input autocomplete="off" type="text" class="form-control" id="asset_code1" placeholder="Enter Asset Code of the product..." name="asset_code1" required="required">

							<span class="help-block">Enter Asset Code of the product...</span> </div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-4">
							<input autocomplete="off" type="text" class="form-control" id="make1" name="make1" placeholder="Enter the brand..." required="required" onblur="fetch_all_address()" onkeypress=" return onlyLtrs(event, this);">

							<span class="help-block">Enter the brand...</span> </div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-4">
							<input autocomplete="off" type="text" class="form-control" id="model1" name="model1" placeholder="Enter the model no..." value="" required="required"/>

							<span class="help-block">Enter the model no...</span> </div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-4">
							<select class="form-control bs-select" id="ratings1" name="ratings1" required>
								<option value="">---Select Ratings---</option>
								<?php foreach($rating as $rat) 
				{
					$value=$rat->Rating." ".$rat->Unit;?>
								<option value="<?php echo $value;?>">
									<?php echo $value; ?>
								</option>
								<?php } ?>
							</select>

							<span class="help-block">Select Ratings...</span> </div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-4">
							<select class="form-control bs-select" id="fuel1" name="fuel1" required>
								<option value="" selected>---Select Fuel---</option>
								<?php foreach($master_log as $mas) 
			  {
		echo "<option value='".$mas->fuel_name."'>".$mas->fuel_name."</option>";
			  }
		?>
							</select>
							<label></label>
							<span class="help-block">Select Fuel *</span> </div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-12">
							<h3>Engine Details</h3>
						</div>
						<div id="c_choice" class="form-group form-md-line-input form-md-floating-label col-md-4" placeholder="Select Number of Cylinders">
							<select class="form-control bs-select" id="cylinder_choice1" name="cylinder_choice1">
								<option value="4">4</option>
								<option value="6">6</option>
								<option value="8">8</option>
								<option value="12">12</option>
							</select>

						</div>
						<div id="r_choice" class="form-group form-md-line-input form-md-floating-label col-md-4">
							<select class="form-control bs-select" id="Rotation_choice1" name="Rotation_choice1">
								<option value="CW">CW</option>
								<option value="CWW">CWW</option>
							</select>
							<!-- <label>Select Rotation <span class="required">*</span></label>-->
						</div>
						<div id="d_choice" class="form-group form-md-line-input form-md-floating-label col-md-4">
							<select class="form-control bs-select" id="Design_choice" name="Design_choice">
								<option value="Water Cooled"> Water Cooled</option>
								<option value="Direct Inject"> Direct Inject</option>
							</select>
							<!-- <label>Design <span class="required">*</span></label>-->
						</div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-4">
							<input autocomplete="off" type="text" class="form-control focus" id="alternator_make1" name="alternator_make1" required="required">
							<label> Alternator Make<span class="required">*</span></label>
							<span class="help-block">Enter Alternator make...</span> </div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-4">
							<input autocomplete="off" type="text" class="form-control" id="fuel_consumption1" name="fuel_consumption1" placeholder="Fuel Consumption Per Litre" onkeyup="chk_if_num('fuel_consumption');" required="required">
							<!--<label>Fuel Consumption Per Litre <span class="required">*</span></label>-->
							<span class="help-block">Enter Fuel consumed per liter...</span> </div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-4">
							<select class="form-control bs-select" id="status1" name="status1" required>
								<option value="Active">Active</option>
								<option value="Faulty">Faulty</option>
								<option value="Under Maintanance">Under Maintanance</option>
							</select>
							<span class="help-block">Status *</span>
						</div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-4">
							<input autocomplete="off" type="text" class="form-control date-picker" id="amc1" name="amc1" placeholder="Next AMC Date">
							<!-- <label>Next AMC Date <span class="required">*</span></label>-->
							<span class="help-block">Enter Annual Maintanance Date...</span> </div>
						<div class="form-group form-md-line-input form-md-floating-label col-md-12">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img id="img1" src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
								<div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
									<?php echo form_upload('dg_image1');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn default">Close</button>
				<button type="submit" class="btn green">Save</button>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<script>
	function fetch_all_address() {

		var pin_code = document.getElementById( 'pincode' ).value;

		jQuery.ajax( {
			type: "POST",
			url: "<?php echo base_url(); ?>bookings/fetch_address",
			dataType: 'json',
			data: {
				pincode: pin_code
			},
			success: function ( data ) {
				//alert(data.country);
				document.getElementById( "g_country" ).focus();
				$( '#g_country' ).val( data.country );
				document.getElementById( "g_state" ).focus();
				$( '#g_state' ).val( data.state );
				document.getElementById( "g_city" ).focus();
				$( '#g_city' ).val( data.city );
			}

		} );
	}
</script>
<script>
	function chk_if_num( a ) {
		//alert($('#'+a+'').val());
		if ( isNaN( $( '#' + a + '' ).val() ) == true ) {
			//alert ("please enter a valid "+a);
			$( '#' + a + '' ).val( "" );

		}
	}

	function check_Cylinders( value ) {

		if ( value == "Cylinders" ) {

			document.getElementById( "c_choice" ).style.display = "block";
		} else {
			document.getElementById( "c_choice" ).style.display = "none";
		}
		if ( value == "Rotation" ) {

			document.getElementById( "r_choice" ).style.display = "block";
		} else {
			document.getElementById( "r_choice" ).style.display = "none";
		}
		if ( value == "Design" ) {

			document.getElementById( "d_choice" ).style.display = "block";
		} else {
			document.getElementById( "d_choice" ).style.display = "none";
		}
	}
	$( document ).on( 'blur', '#amc-d', function () {
		$( '#amc-d' ).addClass( 'focus' );
	} );


	function edit_dg_set1( id ) {
		//alert(id);
		//return false;
		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>unit_class_controller/edit_dg_set",
			data: {
				id: id
			},
			success: function ( data ) {
				alert( data.img );

				$( '#dgset_id1' ).val( data.dgset_id );
				$( '#gen_name1' ).val( data.gen_name );
				$( '#asset_code1' ).val( data.asset_code );
				$( '#make1' ).val( data.make );
				$( '#model1' ).val( data.model );
				$( '#ratings1' ).val( data.ratings ).change();
				$( '#fuel1' ).val( data.fuel ).change();
				$( '#cylinders1' ).val( data.cylinders );
				$( '#rotation1' ).val( data.rotation );
				$( '#design1' ).val( data.design );
				$( '#alternator_make1' ).val( data.alternator_make );
				$( '#fuel_consumption1' ).val( data.fuel_consumption );
				$( '#status1' ).val( data.status ).change();
				$( '#amc1' ).val( data.amc );
				$( '#cooling1' ).val( data.cooling );
				$( '#img1' ).attr( 'src', '<?php echo base_url().'
					upload / dg_set / ';  ?>' + data.img );


				$( '#edit_dg_set' ).modal( 'toggle' );

			}
		} );
	}
</script>