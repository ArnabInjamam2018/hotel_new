<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Events </div>
    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-6">
          <div class="btn-group">
            <button  class="btn green" onclick="add_event()"> Add New <i class="fa fa-plus"></i> </button>
          </div>
        </div>
        <div class="col-md-6">
          <div class="btn-group pull-right">
            <button class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-download"></i> </button>
            <ul class="dropdown-menu pull-right">
              <li> <a href="javascript:;"> Print </a> </li>
              <li> <a href="javascript:;"> Save as PDF </a> </li>
              <!--<li> <a href="javascript:;"> Export to Excel </a> </li>-->
            </ul>
          </div>
        </div>
      </div>
    </div>
    <table class="table table-striped table-hover table-bordered table-scrollable" id="sample_editable_1">
      <thead>
        <tr> 
          <!--<th scope="col">
						Select
					</th>-->
          <th scope="col"> Event Name </th>
          <th scope="col"> Event From </th>
          <th scope="col"> Event Upto </th>
          <th scope="col"> Event Color </th>
          <th scope="col"> Notify </th>
          <th scope="col" width="8%"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($events) && $events):
					$i=1;
					foreach($events as $event):
						$class = ($i%2==0) ? "active" : "success";

						$e_id=$event->e_id;
						?>
        <tr id="row_<?php echo $event->e_id;?>">
          <td align="left"><?php echo $event->e_name ?></td>
          <td align="left"><?php echo $event->e_from ?></td>
          <td align="left"><?php echo $event->e_upto ?></td>
          <td align="left"><span class="label" style="background-color:<?php echo $event->event_color ?>; color:<?php echo $event->event_text_color ?>;"><?php echo $event->e_name ?></span></td>
          <td align="left"><?php
								if($event->e_notify==1){
								echo "YES";}
								else{
									echo "NO";
								}?></td>
          <td align="center" class="ba">
          	<div class="btn-group">
              <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $event->e_id;?>')" data-toggle="modal" class="btn red btn-sm"><i class="fa fa-trash"></i></a></li>
                <li><a onclick="edit_event('<?php echo $event->e_id;?>')" data-toggle="modal" class="btn green btn-sm"><i class="fa fa-edit"></i></a> </li>
              </ul>
            </div>
          </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<div id="add_event" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('dashboard/add_event',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Events Type</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" class="form-control" name="e_name" required="required" placeholder="Event Name *">
                <span class="help-block">Event Name</span> </div>
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" required="required" name="e_from" class="form-control date-picker"  id="c_valid_from" placeholder="Event From *">
                <span class="help-block">Event From</span> </div>
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" required="required" name="e_upto" class="form-control date-picker" id="c_valid_upto" placeholder="Event Up to *">
                <span class="help-block">Event Up to *</span> </div>
              <div class="form-group form-md-line-input ">
                <div class="md-checkbox">
                  <input type="checkbox" id="checkbox1" class="md-check" name="e_notify" value="1">
                  <label for="checkbox1"> <span></span> <span class="check"></span> <span class="box"></span> Notify While Taking Booking? </label>
                </div>
              </div>
              <div class="form-group form-md-line-input">
                <div class="md-checkbox">
                  <input type="checkbox" id="checkbox2" class="md-check" name="checkbox2" value="1">
                  <label for="checkbox2"> <span></span> <span class="check"></span> <span class="box"></span> Is Seasonal Event? </label>
                </div>
              </div>
               <div class="form-group form-md-line-input last">
          <input type="text" name="e_event_color" required="required" placeholder="Mention the Color You want to Apply To Your Event *" id="pickcolor" class="form-control call-picker">
          <div class="color-holder call-picker"></div>
          <div class="color-picker" id="color-picker" style="display: none"></div>
          <label></label>
          <span class="help-block">Mention the Color You want to Apply To Your Event *</span> </div>
              <div class="form-group form-md-line-input">
                <input type="text" name="e_event_text_color" required="required" placeholder="Mention the Color You want to Apply To Your Text *" id="pickcolor2" class="form-control call-picker2">
                <div class="color-holder2 call-picker2"></div>
                <div class="color-picker" id="color-picker2" style="display: none"></div>
                <label></label>
                <span class="help-block">Mention the Color You want to Apply To Your Text *</span> </div>
            </div>
            <!--- col-sm-12 --> 
            
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>

<!-- end event -->

<div id="edit_events" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('dashboard/add_event',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Event Type</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" class="form-control" name="e_name1" id="e_name1" required="required" placeholder="Event Name *">
                <span class="help-block">Event Name</span> </div>
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" required="required" name="e_from1" id="e_from1" class="form-control date-picker"  id="c_valid_from" placeholder="Event From *">
                <span class="help-block">Event From</span> </div>
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" required="required" name="e_upto1" id="e_upto1" class="form-control date-picker" id="c_valid_upto" placeholder="Event Up to *">
                <span class="help-block">Event Up to *</span> </div>
              <div class="form-group form-md-line-input ">
                <div class="md-checkbox">
                  <input type="checkbox" id="checkbox1" class="md-check" name="e_notify" value="1">
                  <label for="checkbox1"> <span></span> <span class="check"></span> <span class="box"></span> Notify While Taking Booking? </label>
                </div>
              </div>
              <div class="form-group form-md-line-input">
                <div class="md-checkbox">
                  <input type="checkbox" id="checkbox2" class="md-check" name="checkbox2" value="1">
                  <label for="checkbox2"> <span></span> <span class="check"></span> <span class="box"></span> Is Seasonal Event? </label>
                </div>
              </div>
			  <div class="col-md-12">
       <div class="form-group form-md-line-input">
          <input type="text" name="e_event_color"  required="required" placeholder="Want Your Event to Be colorful? *" id="pickcolor11" class="form-control call-picker1">
          <div class="color-holder call-picker1"></div>
          <div class="color-picker" id="color-picker1" style="display: none"> </div>
          <label></label>
          <span class="help-block">Want Your Event to Be colorful? *</span> </div>
        </div>
        <div class="col-md-12">
        <div class="form-group form-md-line-input">
          <input type="text" name="e_event_text_color1" required="required" placeholder="Mention the Color You want to Apply To Your Text *" id="pickcolor12" class="form-control call-picker2">
          <div class="color-holder2 call-picker2"></div>
          <div class="color-picker" id="color-picker12" style="display: none"></div>
          <label></label>
          <span class="help-block">Mention the Color You want to Apply To Your Text *</span> </div>
        </div>
				
				
				
				
				
            </div>
            <!--- col-sm-12 --> 
            
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<script>

function add_event(){
	
	$('#add_event').modal('toggle');
}

function edit_event(id){
	 
	$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/edit_event?e_id="+id,
                data:{e_id:id},
                success:function(data)
                {
					if(data.e_notify==1){
						var checked="checked";
					}
                   $('#e_name1').val(data.e_name);
                   $('#e_from1').val(data.e_from);
                  $('#e_upto1').val(data.e_upto);
                  $('#e_notify').attr("checked",checked);
                  $('#e_seasonal').val(data.e_seasonal);
                  $('#pickcolor1').val(data.event_color);
                  $('#pickcolor2').val(data.event_text_color);
				  $('#edit_events').modal('show'); 
                }
            });
}


    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_event?e_id="+id,
                data:{e_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            $('#row_'+id).remove();

                        });
                }
            });



        });
    }
</script> 

<script>
    var colorList = ['000000', '993300', '333300', '003300', '003366', '000066', '333399', '333333',
        '660000', 'FF6633', '666633', '336633', '336666', '0066FF', '666699', '666666', 'CC3333', 'FF9933', '99CC33', '669966', '66CCCC', '3366FF', '663366', '999999', 'CC66FF', 'FFCC33', 'FFFF66', '99FF66', '99CCCC', '66CCFF', '993366', 'CCCCCC', 'FF99CC', 'FFCC99', 'FFFF99', 'CCffCC', 'CCFFff', '99CCFF', 'CC99FF', 'FFFFFF'
    ];
    var picker = $('#color-picker');

    for (var i = 0; i < colorList.length; i++) {
        picker.append('<li class="color-item" data-hex="' + '#' + colorList[i] + '" style="background-color:' + '#' + colorList[i] + ';"></li>');
    }

    $('body').click(function() {
        picker.fadeOut();
    });

    $('.call-picker').click(function(event) {
        event.stopPropagation();
        picker.fadeIn();
        picker.children('li').hover(function() {
            var codeHex = $(this).data('hex');

            $('.color-holder').css('background-color', codeHex);
            $('#pickcolor').val(codeHex);
        });
    });

    var picker2 = $('#color-picker2');

    for (var i2 = 0; i2 < colorList.length; i2++) {
        picker2.append('<li class="color-item" data-hex="' + '#' + colorList[i2] + '" style="background-color:' + '#' + colorList[i2] + ';"></li>');
    }

    $('body').click(function() {
        picker2.fadeOut();
    });

    $('.call-picker2').click(function(event) {
        event.stopPropagation();
        picker2.fadeIn();
        picker2.children('li').hover(function() {
            var codeHex = $(this).data('hex');
            $('.color-holder2').css('background-color', codeHex);
            $('#pickcolor2').val(codeHex);
        });
    });

   

    $( document ).ready(function() {
        var hex_1 = document.getElementById('pickcolor').value;
        $('.color-holder').css('background-color', hex_1);
        var hex_2 = document.getElementById('pickcolor2').value;
        $('.color-holder2').css('background-color', hex_2);
		 
    });
</script> 

<script>
    var colorList1 = ['000000', '993300', '333300', '003300', '003366', '000066', '333399', '333333',
        '660000', 'FF6633', '666633', '336633', '336666', '0066FF', '666699', '666666', 'CC3333', 'FF9933', '99CC33', '669966', '66CCCC', '3366FF', '663366', '999999', 'CC66FF', 'FFCC33', 'FFFF66', '99FF66', '99CCCC', '66CCFF', '993366', 'CCCCCC', 'FF99CC', 'FFCC99', 'FFFF99', 'CCffCC', 'CCFFff', '99CCFF', 'CC99FF', 'FFFFFF'
    ];
    var picker1 = $('#color-picker1');
	
    for (var i = 0; i < colorList1.length; i++) {
        picker1.append('<li class="color-item" data-hex="' + '#' + colorList1[i] + '" style="background-color:' + '#' + colorList1[i] + ';"></li>');
    }

    $('body').click(function() {
        picker1.fadeOut();
    });

    $('.call-picker1').click(function(event) {
        event.stopPropagation();
        picker1.fadeIn();
        picker1.children('li').hover(function() {
            var codeHex = $(this).data('hex');

            $('.color-holder').css('background-color', codeHex);
            $('#pickcolor11').val(codeHex);
        });
    });

	var picker4 = $('#color-picker12');
	
    for (var i = 0; i < colorList1.length; i++) {
        picker4.append('<li class="color-item" data-hex="' + '#' + colorList1[i] + '" style="background-color:' + '#' + colorList1[i] + ';"></li>');
    }

    $('body').click(function() {
        picker4.fadeOut();
    });

    $('.call-picker2').click(function(event) {
        event.stopPropagation();
        picker4.fadeIn();
        picker4.children('li').hover(function() {
            var codeHex = $(this).data('hex');

            $('.color-holder2').css('background-color', codeHex);
            $('#pickcolor12').val(codeHex);
        });
    });
	
   
</script>





<script>
 function getfocus() {
	//alert("ulala")
    document.getElementById("c_valid_from").focus();
	//e.prevent();
}

</script>