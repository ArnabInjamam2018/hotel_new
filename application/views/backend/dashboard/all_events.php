<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Events </div>
    <div class="actions">
    	<a class="btn btn-circle green btn-outline btn-sm" data-toggle="modal" href="#add_event"> <i class="fa fa-plus"></i>Add New  </a>
    </div>
  </div>
  <div class="portlet-body">
    
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <!--<th scope="col">
						Select
					</th>-->
          <th scope="col"> Sl </th>
          <th scope="col"> Event Name </th>
          <th scope="col"> From Date </th>
          <th scope="col"> Upto Date </th>
          <th scope="col"> Event Style </th>
          <th scope="col"> Notify? </th>
          <th align="center" scope="col" width="5%"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($events) && $events):
					$i=1;
					$sl = 0;
					foreach($events as $event):
						$sl++;
						$class = ($i%2==0) ? "active" : "success";						
						$e_id=$event->e_id;
						?>
        <tr id="row_<?php echo $event->e_id;?>">
		  <td><?php echo $sl; ?></td>
          <td align="left"><?php echo $event->e_name ?></td>
          <td align="left"><?php 
				$createDate = new DateTime($event->e_from);
				$strip = $createDate->format('l jS F Y');
				echo $strip;
			?>
	      </td>
          <td align="left"><?php 
				$createDate = new DateTime($event->e_upto);
				$strip = $createDate->format('l jS F Y');
				echo $strip;
				// My Test _temp for testing the function returning the non occupied rooms
				/*$date1 = '2016-10-8';
				$date2 = '2016-10-9';
				//$date2=date("Y-m-d", strtotime('-1 day', strtotime($date2)));
				$date1=date("Y-m-d",strtotime($date1));
				$date2=date("Y-m-d",strtotime($date2));
				$date2_ed=date("Y-m-d", strtotime('-1 day', strtotime($date2)));
				$rooms=$this->dashboard_model->all_rooms();
				$array1=array();
				$array2=array();
				$bookings_between= $this->dashboard_model->get_available_room($date1,$date2);
				$i=0;
				$j=0;
				foreach ($rooms as $room ) {
					$array1[$i]=$room->room_id;
					$i++;
				}
				echo '<br> Conflict - ';
				print_r($bookings_between);
				foreach ($bookings_between as $key ) {
					$array2[$j]=$key->room_id;
					$j++;
				}    
				$result = array_diff($array1, $array2);
				$data1="";
				echo '<br>';
				print_r($result);
				
				$data1='<div class="all-re"><div class="all-retitle uppercase">Available Room</div><div class="portlet-body form">';		
				foreach($result as $res) { 
					$room=$this->dashboard_model->get_room_number_exact($res);
					$room_number=$room->room_no;
					$data1 .=' <div class="btn-group"> <label class="btn grey-cascade" id="abc_'.$res.'"> <input class="noClick" id="ab_'.$res.'" style="display: none;" onclick="addRow(this.value,'.$room_number.')" value="'.$res.'" type="checkbox" > '.$room_number.' </label></div>';
				}
				$data1 .='</div></div>';
				
				$data2 .='</div></div>';
				echo $data = $data1.'~'.$data2;*/
				// END MY TEST
			?>
		  </td>
          <td align="left"><span class="label" style="background-color:<?php echo $event->	event_color ?>; color:<?php echo $event->	event_text_color ?>;"><?php echo $event->e_name ?> </span>
		  </td>
          <td align="left"><?php
				if($event->e_notify==1){
					echo '<span class="label" style="background-color:#378F79; color:#FFFFFF;">Yes</span>';
				}
				else{
					echo '<span class="label" style="background-color:#E36868; color:#FFFFFF;">No</span>';
				}
			?>
		  </td>
          <td align="center" class="ba"><div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $event->e_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
                <!--<li><a href="<?php echo base_url() ?>dashboard/fetch_event?e_id=<?php echo $event->e_id;?>" data-toggle="modal" class="btn green btn-sm"><i class="fa fa-edit"></i></a> </li>-->
                <li><a onclick="edit_event('<?php echo $event->e_id;?>')" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a> </li>
              </ul>
            </div></td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<div id="add_event" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('dashboard/add_event',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Events Type</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" class="form-control" name="e_name" required="required" placeholder="Event Name *">
                <label></label>
                <span class="help-block">Event Name</span> </div>
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" required="required" name="e_from" class="form-control date-picker"  id="c_valid_from" placeholder="Event From *">
                <label></label>
                <span class="help-block">Event From</span> </div>
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" required="required" name="e_upto" class="form-control date-picker" id="c_valid_upto" placeholder="Event Up to *">
                <label></label>
                <span class="help-block">Event Up to *</span> </div>
              <div class="form-group form-md-line-input ">
                <div class="md-checkbox">
                  <input type="checkbox" id="checkbox1" class="md-check" name="e_notify" value="1">
                  <label for="checkbox1"> <span></span> <span class="check"></span> <span class="box"></span> Notify While Taking Booking? </label>
                </div>
              </div>
              <div class="form-group form-md-line-input">
                <div class="md-checkbox">
                  <input type="checkbox" id="checkbox2" class="md-check" name="checkbox2" value="1">
                  <label for="checkbox2"> <span></span> <span class="check"></span> <span class="box"></span> Is Seasonal Event? </label>
                </div>
              </div>
              <div class="form-group form-md-line-input last">                
                <input type="text" id="position-bottom-right" name="e_event_color" required="required" class="form-control demo" data-position="bottom right" value="#0088cc" placeholder="Mention the Color You want to Apply To Your Event *">
                <label></label>
                <span class="help-block">Mention the Color You want to Apply To Your Event *</span> </div>
              <div class="form-group form-md-line-input">
              	<input type="text" id="position-bottom-right" name="e_event_text_color" required="required" class="form-control demo" data-position="bottom right" value="#0088cc" placeholder="Mention the Color You want to Apply To Your Event *">                
                <label></label>
                <span class="help-block">Mention the Color You want to Apply To Your Text *</span> </div>
            </div>
            <!--- col-sm-12 --> 
            
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>

  
  
<div id="edit_events" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('dashboard/update_event',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Event Type</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-12">
			<input type="hidden" name="e_id" id="e_id">
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" class="form-control" id="e_name1" name="e_name" required="required" placeholder="Event Name *">
                <span class="help-block">Event Name</span> </div>
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" required="required" id="e_from1" name="e_from" class="form-control date-picker"  id="c_valid_from" placeholder="Event From *">
            	<label></label>
                <span class="help-block">Event From</span> </div>
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" required="required"  name="e_upto" class="form-control date-picker" id="c_valid_upto1" placeholder="Event Up to *" >
            	<label></label>
                <span class="help-block">Event Up to *</span> </div>
              <div class="form-group form-md-line-input ">
                <div class="md-checkbox">
                  <input type="checkbox" id="checkbox11" class="md-check" name="e_notify" value="1">
                  <label for="checkbox11"> <span></span> <span class="check"></span> <span class="box"></span> Notify While Taking Booking? </label>
                </div>
              </div>
              <div class="form-group form-md-line-input">
                <div class="md-checkbox">
                  <input type="checkbox" id="e_seasonal" class="md-check" name="e_seasonal" value="1">
                  <label for="e_seasonal"> <span></span> <span class="check"></span> <span class="box"></span> Is Seasonal Event? </label>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group form-md-line-input" id="co">
                	<input type="text" id="e_event_color" name="e_event_color" required="required" class="form-control demo" data-position="bottom right"  placeholder="Mention the Color You want to Apply To Your Event *" >                  
                  <label></label>
                  <span class="help-block">Want Your Event to Be colorful? *</span> </div>
              </div>
              <div class="col-md-12">
                <div class="form-group form-md-line-input" id="ca">
                	<input type="text" id="e_event_text_color" name="e_event_text_color" value="" required="required" class="form-control demo" data-position="bottom right" placeholder="Mention the Color You want to Apply To Your Event *">                  
                  <label></label>
                  <span class="help-block">Mention the Color You want to Apply To Your Text *</span> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?>
   </div>
<script>

function edit_event(id){
	 
	$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/edit_event?e_id="+id,
                data:{e_id:id},
                success:function(data)
                {    //alert(data.e_notify); //return false;
					if(data.e_notify==1){
						var checked="checked";
					}
					if(data.e_seasonal==1){
						var checked1="checked";
					}
					$('#e_id').val(data.e_id);
					$('#e_name1').val(data.e_name);
                   $('#e_from1').val(data.e_from);
                   $('#c_valid_upto1').val(data.e_upto);
                   $('#checkbox11').attr('checked',checked);
                   $('#e_seasonal').attr('checked',checked1);
                  $('#e_event_color').val(data.event_color);
				   $("#co .minicolors-swatch-color").css("background-color", data.event_color);
                  $('#e_event_text_color').val(data.event_text_color);
				  $("#ca .minicolors-swatch-color").css("background-color", data.event_text_color);
				  $('#edit_events').modal('show'); 
                }
            });
}

   


    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

           $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_event?e_id="+id,
                data:{e_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            $('#row_'+id).remove();

                        });
                }
            });



        });
    }
</script>  
