<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light borderd">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Extra Charges</div>
    <div class="actions">
    	<a href="<?php echo base_url();?>setting/add_charges" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <th scope="col">#</th>
          <th scope="col">Charge Name</th>
          <th width="20%" scope="col">Charge Description</th>
          <th scope="col">Charge Tag</th>
          <th scope="col">Unit Price </th>
          <th scope="col">SGST </th>          <th scope="col">CGST </th>          <th scope="col">IGST </th>          <th scope="col">UTGST </th>          <th scope="col">Total Tax </th>
          <th scope="col">Unit Price Editable</th>
          <th scope="col">Tax Edtitable </th>
        
          <th scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($charges) && $charges):
                       // print_r($charges); 
                        $i=0;
                        foreach($charges as $ch):
                            $class = ($i%2==0) ? "active" : "success";
                           $i++;
                            ?>
        <tr id="ch_id_<?php echo $ch->hotel_extra_charges_id;?>"> 
         
          
          <td><?php echo $i; ?></td>
          <td>
			<?php 
				echo '<strong style="font-size:15px; color:#1F2E46;">'.$ch->charge_name.'</strong>'; ?></td>
          <td><?php echo $ch->charge_des;?></td>
          <td>
			<?php 
				if($ch->ec_tag > 0){
				$tag = $this->dashboard_model->get_charge_tag_by_id($ch->ec_tag);
				
					foreach($tag as $ta){
						if($ta->extra_charge_tag_icon)
							echo $ta->extra_charge_tag_icon.' ';
						if($ta->extra_charge_tag_name)
							echo $ta->extra_charge_tag_name;
						
					}
				}
				else
					echo '<span style="color:#AAAAAA;">None</span>';
				
			?>
		  </td>
          <td><?php echo $ch->unit_price;?></td>		  <td><?php				echo $ch->charge_sgst;            ?></td>		  <td><?php				echo $ch->charge_cgst;            ?></td>		  <td><?php				echo $ch->charge_igst;            ?></td>		  <td><?php				echo $ch->charge_utgst;            ?></td>			
          <td><?php
             echo $ch->charge_tax;?></td>
          <td>		  <?php  if($ch->unit_price_editable==0){
			  echo "Not Editable";
		  }else{
			   echo "Editable";
		  } ;?></td>
          <td><?php if($ch->tax_editable==0){
			  echo "Not Editable";
		  }else{
			   echo "Editable";
		  }?></td>
         
          <td class="ba">
            <div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $ch->hotel_extra_charges_id;?>')" data-toggle="modal"  class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
               <li><a onclick="edit_extra_charge('<?php echo $ch->hotel_extra_charges_id;?>')" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li>
              </ul>
            </div>
          </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Charge</h4>
      </div>
      
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
            <!-- <input type="text" class="form-control" name="name" id="name" required placeholder="Charge Name">-->
				<select class="form-control"  name="charge_id_drop" id="charge_id_drop" >
					 
					  <?php 
						$tag1 = $this->dashboard_model->get_all_charge_tag();
						//if(isset($tag)){
							foreach($tag1 as $tag){?>
								<option value="<?php echo $tag->extra_charge_tag_id ?>"><?php echo $tag->extra_charge_tag_name; ?></option>
							<?php
							//}	
						}
					  ?>
					 
					</select>
			
              <label></label>
              <span class="help-block">Extra
			  Charge Tag..</span> </div>
          </div>
		   <div class="col-md-4">
            <div class="form-group form-md-line-input">
             <input type="text" class="form-control" name="name" id="name" required placeholder="Charge Name">

			 
			
              <label></label>
              <span class="help-block">Charge Name..</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="ch_des" name="charge_des" placeholder="Enter Description ..."  >
			
              <label></label>
              <span class="help-block">Enter Description *</span> </div>
          </div>
        
		  <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="u_price" name="unit_price" placeholder="Unit Price ..."  maxlength="10" onkeypress=" return onlyNos(event, this);" >
              <label></label>
              <span class="help-block">Enter Unit Price *</span> </div>
          </div>
		  <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="charge_tax" name="charge_tax" placeholder="Tax ..."  maxlength="10" onkeypress=" return onlyNos(event, this);" >
              <label></label>
              <span class="help-block">Enter tax *</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select onchange="check(this.value)" class="form-control"  id="u_price_editable" name="u_price_editable" required >
               
                <option value="0">No</option>
                <option value="1">Yes</option>
              </select>
              <label></label>
              <span class="help-block">Unit Price Editable *</span> </div>
          </div>
		   <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select onchange="check(this.value)" class="form-control"  id="tax_editable" name="tax_editable" required >
               
                <option value="0">No</option>
                <option value="1">Yes</option>
              </select>
              <label></label>
              <span class="help-block">Tax Editable *</span> </div>
          </div>
        <input type="hidden" id="charge_id" value=""/>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="button" class="btn green" onclick="update_charge()">Save</button>
      </div>
       </div>
  </div>
</div>
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>setting/delete_extra_charges?ch_id="+id,
                data:{ch_id:id},
                success:function(data)
                {
                    //alert(data.data);
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){
							$('#ch_id_'+id).remove();
                          
                        });
                }
            });



        });
    }
		function edit_extra_charge(id){
		//alert(id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>setting/edit_charges",
                data:{id:id},
                success:function(data)
                {
                   // alert(data.unit_price_editable);
				// $('#name').append( '<option value="'+data.charge_name+'">'+data.charge_name+'</option>' );
				//alert(data);
				console.log(data);
				
				  $('#name').val(data.charge_name);
				  
				   $('#ch_des').val(data.charge_des);
				   $('#u_price').val(data.unit_price);
				   $('#u_price').val(data.unit_price);
				   $('#charge_tax').val(data.charge_tax);
				   
				   $('#tax_editable').val(data.tax_editable);
				   $('#u_price_editable').val(data.unit_price_editable);
				   $('#charge_id').val(data.hotel_extra_charges_id);
				   $('#charge_id_drop').val(data.ec_tag).change();
				   var extracharges = data.hotel_extra_charges_id;
				   if(extracharges){
					   
					  // alert('hello');
					  
				   }
				  
				   
				   
                    $('#responsive').modal('toggle');
					
                }
            });
	}
	
	function update_charge(){
		var id=$('#charge_id').val();
		var name=$('#name').val();
		var ch_des=$('#ch_des').val();
		var u_price=$('#u_price').val();
		var charge_tax=$('#charge_tax').val();
		var tax_editable=$('#tax_editable').val();
		var u_price_editable=$('#u_price_editable').val();
		var ec_tag= $('#charge_id_drop').val();
		
		//alert(id +'' +name +'' +ch_des +'' +u_price +'' +charge_tax+'' +tax_editable +'' +u_price_editable +'' +ec_tag);
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>setting/update_charges",
                data:{id:id,name:name,ch_des:ch_des,u_price:u_price,charge_tax:charge_tax,tax_editable:tax_editable,u_price_editable:u_price_editable,extra_tag:ec_tag},
                success:function(data)
                {
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
	
                    $('#responsive').modal('hide');
					
                }
            });
		
	}
</script> 
