
      <div class="portlet light borderd">
        <div class="portlet-title">
          <div class="caption" id="caption"> <i class="fa fa-file-archive-o"></i>Reports of All Transactions</div>
          <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
        </div>
        <div class="portlet-body">
          <div class="table-toolbar">
            <div class="row">
              <div class="col-md-10">
                  <?php

                            $form = array(
                                'class'       => 'form-inline',
                                'id'        => 'form_date',
                                'method'      => 'post'
                            );

                            echo form_open_multipart('dashboard/get_f_report_by_date',$form);

                            ?>
                    <div class="form-group">
                      <input type="text" autocomplete="off" required="required" id="t_dt_frm" value="<?php if(isset($start_date)){ echo $start_date;}?>" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date">                      
                    </div>
                    <div class="form-group">
                      <input type="text" autocomplete="off" required="required" name="t_dt_to"  value="<?php if(isset($end_date)){ echo $end_date;}?>" class="form-control date-picker" placeholder="End Date">
                    </div>
                <button class="btn submit" onclick="check_sub()" type="submit">Search</button>
                      <div style="padding-left: 10px;display: inline-block;">
                        <div class="form-group form-md-checkboxes">
                          <div class="md-checkbox">
                                <input type="checkbox" id="chkbx_tdy" class="md-check">
                                <label for="chkbx_tdy">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Today's Transaction </label>
                          </div>
                        </div>
                      </div>
                  <?php form_close(); ?>
              </div>
					  <?php 
					if(isset($start_date) && isset($end_date))
					{
						$s_d = $start_date;
                        $e_d  =$end_date;
					}	
					else
					{	
						
						$s_d = ""; 
                        $e_d ="";
					}						
					?>
            </div>              
          </div>
		  
		  <div class="portlet-title">
                                  
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#trasactions" data-toggle="tab"> Trasactions </a>
                                        </li>
                                        <li>
                                            <a href="#accountSummary" data-toggle="tab"> Account Summary </a>
                                        </li>
                                    </ul>
                                </div>
								</br>
								
		  <div class="portlet-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="trasactions">
                                            
											<table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
              <tr>
                <th> # </th>
				<th> Transaction ID </th>
                <th> Transaction Date </th>
                <th> Relatd ID </th>                
                <th> Note </th>                
                <th>  Type </th>
                <th> Transaction From </th>
                <th> Transaction To </th>
                <th> Payment Mode </th>
                <th> Income</th>
				<th> Expense </th>
                
              </tr>
			  
            </thead>
            <tbody>
              <?php 
					
					$daily_sum=0;
                    $broker_sum=0;
                    $cancelFees=0;
                    
					$vendor_amount=0;
					$government_amount=0;
					$expense=0;
					$income=0;
					$vendor_pay=0;
					$gov_pay=0;
					$broker_amount=0;
					$channel_sum=0;
					$g_fund_sum=0;
					$misc_sum=0;
					$rest_sum=0;
					$reten_sum=0;
					$comp_sum=0;
					$tax_sum=0;
					$ven_sum=0;
					$channel_pay=0;
					$misc_pay_sum=0;
					$sal_pay=0;
					$sal_amount=0;
					$misc_amount=0;
					$guestService_pay=0;
                    ?>
              <?php if(isset($transactions) && $transactions):
                       
					   $daily_sum=0;
                        $i=1;
                        $deposit=0;
						
						$srl_no=0;
						  // print_r ($transactions);
                        foreach($transactions as $transaction):
							           $srl_no++;


                            $class = ($i%2==0) ? "active" : "success";

                            $transaction_id='HM00TA0'.$transaction->t_id;
						
						
							$booking_id=$transaction->t_booking_id;
							//echo $booking_id;
						
							if(isset($booking_id) && $booking_id){
							$query=$this->dashboard_model->cust_name($booking_id);
							}
						  ?>
              <tr> 
                <!--<td><input type="checkbox" name="delete[]" value="<?php //echo $transaction->t_id;?>" class="form-control" onclick="this.form.submit();"/></td>-->                
				<td style="width: 27px;"><?php echo $srl_no; ?></td>
				<td><?php echo $transaction_id;?></td>
                <td><?php echo $transaction->t_date; ?></td>
                <td><?php if($transaction->t_booking_id>0){
				echo 'HM0'.$this->session->userdata('user_hotel').'00'.$transaction->t_booking_id;
				}
				elseif($transaction->t_group_id>0){
					echo 'HM0'.$this->session->userdata('user_hotel').'00GRP'.$transaction->t_group_id;
				}
				elseif($transaction->details_id>0){
					echo 'HM0D'.$this->session->userdata('user_hotel').'00'.$transaction->details_id;
				}
				
				?></td>
				 <?php
				 if(isset($types) && $types){
				 foreach($types as $type): ?>
				<?php if($type->hotel_transaction_type_id==$transaction->transaction_releted_t_id): ?>
				<td><?php if($type->hotel_transaction_type_name=="Vendor payment"){
					echo "<center>".$transaction->transaction_type."<center></br>";
					echo $transaction->transactions_detail_notes;
				}else{
							echo $transaction->transactions_detail_notes;
				}?></td>
				 <?php endif; ?>
                <?php endforeach;} ?>
                <?php 
				if(isset($types) && $types){
				foreach($types as $type): ?>
                <?php if($type->hotel_transaction_type_id==$transaction->transaction_releted_t_id): ?>
                <td><?php echo $type->hotel_transaction_type_name; ?></td>
                <?php endif; ?>
                <?php endforeach;} ?>
                <?php 
				if(isset($entity) && $entity){
				foreach($entity as $ent): //print_r($ent);?>
                <?php if($ent->hotel_entity_type_id==$transaction->transaction_from_id): ?>
                <td><?php echo $ent->entity_name; ?></td>
                <?php endif; ?>
                <?php endforeach; }?>
                <?php
			if(isset($entity) && $entity){
				foreach($entity as $ent): ?>
                <?php if($ent->hotel_entity_type_id==$transaction->transaction_to_id): ?>
                <td><?php echo $ent->entity_name; ?></td>
                <?php endif; ?>
			<?php endforeach; }?>
                <?php //print_r($transaction);?>
                <td><?php echo $transaction->t_payment_mode;?></td>
				<td><?php 
				if(isset($entity) && $entity){
				foreach($entity as $ent){
							
								if($ent->hotel_entity_type_id==$transaction->transaction_to_id){
								  $to_vendor=$ent->entity_name;
										
									if($to_vendor=="Vendor"){
										$vendor_amount=$vendor_amount+$transaction->t_amount;
										$vendor_pay=$transaction->t_amount;
									}
									if($to_vendor=="Government"){
										$government_amount=$government_amount+$transaction->t_amount;
										$gov_pay=$transaction->t_amount;
									}
									if($to_vendor=="Hotel"){
									$h_amount=$government_amount+$transaction->t_amount;
									echo $h_pay=$transaction->t_amount;
									}
									
									if($to_vendor!="Government" && $to_vendor!="Vendor" && $to_vendor!="Broker" && $to_vendor!="Channel" && $to_vendor!="Debtor" && $to_vendor!="Hotel" && $to_vendor!="Employee"){
										echo $income=$transaction->t_amount;
									}
									
									if($to_vendor!="Hotel"){
										echo 0;
										}
									
				}}}?></td>
                <td><?php 
				if(isset($entity) && $entity){
				foreach($entity as $ent){
								
					
								if($ent->hotel_entity_type_id==$transaction->transaction_to_id){
									 $to_vendor=$ent->entity_name;
									
									if($to_vendor=="Vendor"){
										$vendor_amount=$vendor_amount+$transaction->t_amount;
										echo $vendor_pay=$transaction->t_amount;
									}if($to_vendor=="Broker"){
										$broker_amount=$broker_amount+$transaction->t_amount;
										echo $broker_pay=$transaction->t_amount;
									}
									if($to_vendor=="Government"){
										$government_amount=$government_amount+$transaction->t_amount;
										echo $gov_pay=$transaction->t_amount;
									}
									if($to_vendor=="Channel"){
									$channel_amount=$channel_amount+$transaction->t_amount;
									echo $channel_pay=$transaction->t_amount;
									}
									if($to_vendor=="Employee"){
									$sal_amount=$sal_amount+$transaction->t_amount;
									echo $sal_pay=$transaction->t_amount;
									
									}
									if($to_vendor=="Debtor"){
									$misc_amount=$misc_amount+$transaction->t_amount;
									echo $misc_pay=$transaction->t_amount;
									}
									if($to_vendor!="Government" && $to_vendor!="Vendor" && $to_vendor!="Broker" && $to_vendor!="Channel" && $to_vendor!="Employee" && $to_vendor!="Debtor"){
										echo 0;
									}
									
								}
				}}
						
							?>
				</td>		
                
              </tr>
              <?php 

                           if($transaction->transaction_releted_t_id==1 ){
								$i++;
								//$daily_sum=0;								
								$daily_sum=$daily_sum+$transaction->t_amount;							
                            }
                            else if($transaction->transaction_releted_t_id==3){
								//$broker_sum=0;
								$broker_sum=$broker_sum+$transaction->t_amount;
                            }
							else if($transaction->transaction_releted_t_id==6){
								//$broker_sum=0;
								$cancelFees=$cancelFees+$transaction->t_amount;
                            }
							else if($transaction->transaction_releted_t_id==10){
								//$g_fund_sum=0;
								$channel_sum=$channel_sum+$transaction->t_amount;
							}
							else if($transaction->transaction_releted_t_id==2){
								//$g_fund_sum=0;
								$g_fund_sum=$g_fund_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==13){
								//$misc_sum=0;
								$misc_sum = $misc_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==14){
								
								//$rest_sum=0;
								$rest_sum=$rest_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==6){
							
								//$reten_sum=0;
							 $reten_sum=$reten_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==11){
							
								//$comp_sum=0;
								$comp_sum=$comp_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==4){
								
								//$tax_sum=0;
								$tax_sum=$tax_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==5){
								
								//$ven_sum=0;
								$ven_sum=$ven_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==12){
								
								//$misc_pay_sum=0;
								$misc_pay_sum=$misc_pay_sum+$transaction->t_amount;
						
							}else if($transaction->transaction_releted_t_id==16){
								
								//$misc_pay_sum=0;
							 $guestService_pay=$guestService_pay+$transaction->t_amount;
						
							}

						//exit;	
                            ?>
              <?php endforeach; ?>
			
              <?php endif;?>
              <?php 
							
					
						
			  ?>
            </tbody>
          </table>
											
                                        </div>
										
                                        <div class="tab-pane" id="accountSummary">
                                            
													  <div class="caption">
                                        <i class="icon-bubbles font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Account Summary</span>
                                    </div>
          <?php 
				if(isset($to_vendor)){		
				
				?>
          <div class="table-scrollable">
          <table class="table table-striped table-bordered table-hover" id="table">
            <?php 
			   ?>
            <thead>
              <tr>
                <th style="text-align:center;" scope="col">Type</th>
                <th style="text-align:center;" scope="col">Description</th>
                <th style="text-align:center;" scope="col">Income</th>
                <th style="text-align:center;" scope="col">Expense</th>
              </tr>
            </thead>
            <tbody>
              <?php
				
					
						 $transaction_type=$this->dashboard_model->transaction_type();
						$i=count($transaction_type);
						
				if(isset($transaction_type) && $transaction_type){
							
				foreach($transaction_type as $t_type){
								
						$bgcol = '';
						if($t_type->hotel_transaction_type_cat == 'Income')
							$bgcol = '#68A480';
						else
							$bgcol = '#D3393D';
						
					?>
				  <tr>
					<td align="center"><?php
							
									echo '<label class="label-flat" style="color:'.$bgcol.'; font-size:14px;">'.$t_type->hotel_transaction_type_name.'</label>';
							
					?></td>
					<td align="center"><?php echo $t_type->hotel_transaction_type_description; ?></td>
					<td align="center"><?php 
					
						//if($t_type->hotel_transaction_type_cat=="Income"){
							
					    if($t_type->hotel_transaction_type_name=="Booking payment"){
							echo  number_format($daily_sum);
						}
						else if($t_type->hotel_transaction_type_name=="Misc Income"){
							echo number_format($misc_sum);
						}
						else if($t_type->hotel_transaction_type_name=="Retension money"){
							echo  number_format($reten_sum);
						}else if($t_type->hotel_transaction_type_name=="Guest Payment"){
							echo  number_format($guestService_pay);
						}
						 
						else if($t_type->hotel_transaction_type_name=="Cancellation Fees"){
							echo  number_format($cancelFees);
						}
						
							else{echo "0";}
						
						//}
				
					
				?></td>
                <td align="center"><?php 
				
					//if($t_type->hotel_transaction_type_cat=="Expense"){
						if($t_type->hotel_transaction_type_name=="Broker payment"){
							echo number_format($broker_amount);
						}
						else if($t_type->hotel_transaction_type_name=="Channel payment"){
						echo number_format($channel_sum);
						}
						else if($t_type->hotel_transaction_type_name=="Guest refund"){
							echo number_format($g_fund_sum);
						}
						else if($t_type->hotel_transaction_type_name=="Vendor payment")
					{
					    	echo number_format($ven_sum);
						
					}
					else if($t_type->hotel_transaction_type_name=="Tax paymemnt"){
						echo number_format($tax_sum);
						//echo $government_amount;
						
					}
					else if($t_type->hotel_transaction_type_name=="Misc Payment"){
						echo number_format($misc_pay_sum);
						
					}
					else if($t_type->hotel_transaction_type_name=="Salary payment"){
						echo number_format($sal_amount);
						
					}
					else{
						echo "0";
					}
						
					//}
					
					
				
					
				
				
					?></td>
              </tr>
              <?php }}?>
            </tbody>
          </table>
          </div>
		  
          <?php } ?>
			<div class="row text-center">
            <div class="col-md-12" style="font-size: 150%;"> Total Income : <i class="fa fa-inr" style="font-size: 85%;"></i>
          <?php  
			$total_income=$daily_sum+$rest_sum+$reten_sum+$misc_sum+$guestService_pay;
			echo number_format($total_income);
		  ?>
            </div>
            <div class="col-md-12" style="font-size: 150%;"> Total Expense : <i class="fa fa-inr" style="font-size: 85%;"></i>
          <?php  
			$total_exp=0;
			$total_exp=$total_exp+$broker_amount+$channel_pay+$ven_sum+$tax_sum+$misc_pay_sum+$sal_amount;
			echo number_format($total_exp);
		  ?>
		  </div>
          <div class="col-md-12" style="font-size: 150%;"> Difference : <i class="fa fa-inr" style="font-size: 85%;"></i>
          <?php  
			$total_exp=0;
			$total_exp=$total_exp+$broker_amount+$channel_pay+$ven_sum+$tax_sum+$misc_pay_sum+$sal_amount;
			if(($total_income-$total_exp) >= 0){
				echo number_format($total_income-$total_exp).' (Profit)';
			}
			else
				echo number_format(($total_exp-$total_income)).' (Loss)';
			
		  ?>
		  </div>
          </div>
											
                                        </div>
                                    </div>
                                </div>
	
          
          

        </div>
      </div>
   
  

<script type="text/javascript">

$(document).ready(function(){
	
  var chk=$('#t_dt_frm').val();
  //alert(chk);
  if(!chk){
  $('#chkbx_tdy').prop('checked', true);
  	}
  
  });

function check_sub(){
  document.getElementById('form_date').submit();
}
</script>