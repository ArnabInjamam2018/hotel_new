<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong><?php echo $this->session->flashdata('err_msg');?></strong>
	</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong><?php echo $this->session->flashdata('succ_msg');?></strong>
	</div>
<?php endif;?>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-edit"></i>List of All Feedback's
        </div>
        <div class="actions">
             <a href="<?php echo base_url();?>dashboard/add_feedback" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
        </div>
    </div>
    <div class="portlet-body">
        
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
            <tr>
                <th scope="col">
                     Hotel Name
                 </th>
                <th scope="col">
                    Booking Id
                </th>
                <th scope="col">
                    Guest Name
                </th>
                <th scope="col">
                    Overall Quality
                </th>
                <!--<th scope="col">
                    Ease of booking
                </th>
                 <th scope="col">
                      Reception
                 </th>
                <th scope="col">
                   Staff
                </th>
                <th scope="col">
                     Cleanliness
                </th>
                <th scope="col">
                    Ambience
                </th>
                <th scope="col">
                    Sleep Quality
                </th>

                <th scope="col">
                    Room Quality
                </th>
                <th scope="col">
                   Food Quality
                </th>
                <th scope="col">
                    Environment Quality
                </th>

                <th scope="col">
                    Service
                </th>
                 <th scope="col">
                      Package
                 </th>
                <th scope="col">
                   Extra Amenities
                </th>-->
                <th scope="col">
                     Come back again
                </th>
                <th scope="col">
                    Refer friend
                </th>
                <th scope="col">
                    Reasonable price
                </th>

                <th scope="col">
                    Comment
                </th>
                <th scope="col">
                   Social Media
                </th>
                <th scope="col">
                    Action
                </th>
            </tr>
            </thead>
            <tbody>
            <?php if(isset($feedback) && $feedback):

                $i=1;
                foreach($feedback as $gst):
                    $class = ($i%2==0) ? "active" : "success";
                    $g_id=$gst->id;
                    ?>
                    <tr>
                        <td>
                            <?php echo $hotel_name->hotel_name; ?>
                        </td>
                        <td>
                            <?php $booking_id = $gst->booking_id;
                                  if($booking_id == '0'){
                                      echo 'N/A';
                                  }	else {
                                      echo "HM0".$this->session->userdata("user_hotel")."00".$booking_id;
                                  }
                            ?>
                        </td>
                        <td>
                            <?php echo $gst->guest_name; ?>
                        </td>
                        <td>
                        <label>
														<?php // Display Star _sb
															$ease = ceil($gst->tot/12);
                                for ($x = 1; $x <= 5; $x++) {


                                if($x <= $ease){
																	if($x == 1)
																		echo '<i class="fa fa-star"style="color:#F3565D;"></i>';
																	else if($x == 2)
																		echo '<i class="fa fa-star"style="color:#FF6E43;"></i>';
																	else if($x == 3)
																		echo '<i class="fa fa-star"style="color:#07608F;"></i>';
																	else if($x == 4)
																		echo '<i class="fa fa-star"style="color:#00BCD4;"></i>';
																	else if($x == 5)
																		echo '<i class="fa fa-star"style="color:#26A69A;"></i>';
                                    }
									else {
                                        echo '<i class="fa fa-star"style="color:#E4E4E1;"></i>';
                                    }

                                }
                            ?>
						</label>
                    </td>
                        <!--<td>
                            <?php $ease = $gst->ease_booking;
                                for ($x = 1; $x <= 5; $x++) {
                                    if($x <= $ease){
                                        echo '<i class="fa fa-star"style="color:red;"></i>';
                                    } else {
                                        echo '<i class="fa fa-star"style="color:black;"></i>';
                                    }
                                }
                            ?>
                        </td>
                       <td>
                            <?php $ease = $gst->reception;
                                for ($x = 1; $x <= 5; $x++) {
                                    if($x <= $ease){
                                        echo '<i class="fa fa-star"style="color:red;"></i>';
                                    } else {
                                        echo '<i class="fa fa-star"style="color:black;"></i>';
                                    }
                                }
                            ?>
                            </td>
                        <td>
                            <?php $ease = $gst->staff;
                                for ($x = 1; $x <= 5; $x++) {
                                    if($x <= $ease){
                                        echo '<i class="fa fa-star"style="color:red;"></i>';
                                    }
																		else {
                                        echo '<i class="fa fa-star"style="color:black;"></i>';
                                    }
                                }
                            ?>
                        </td>
                        <td>
                             <?php $ease = $gst->cleanliness;
                                for ($x = 1; $x <= 5; $x++) {
                                    if($x <= $ease){
                                        echo '<i class="fa fa-star"style="color:red;"></i>';
                                    } else {
                                        echo '<i class="fa fa-star"style="color:black;"></i>';
                                    }
                                }
                            ?>
                            </td>
                        <td>
                            <?php $ease = $gst->ambience;
                                for ($x = 1; $x <= 5; $x++) {
                                    if($x <= $ease){
                                        echo '<i class="fa fa-star"style="color:red;"></i>';
                                    } else {
                                        echo '<i class="fa fa-star"style="color:black;"></i>';
                                    }
                                }
                            ?>
                        </td>
                        <td>
                            <?php $ease = $gst->sleep_quality;
                                for ($x = 1; $x <= 5; $x++) {
                                    if($x <= $ease){
                                        echo '<i class="fa fa-star"style="color:red;"></i>';
                                    } else {
                                        echo '<i class="fa fa-star"style="color:black;"></i>';
                                    }
                                }
                            ?>
                        </td>
                        <td>
                            <?php $ease = $gst->room_quality;
                                for ($x = 1; $x <= 5; $x++) {
                                    if($x <= $ease){
                                        echo '<i class="fa fa-star"style="color:red;"></i>';
                                    } else {
                                        echo '<i class="fa fa-star"style="color:black;"></i>';
                                    }
                                }
                            ?>
                        </td>
                        <td>
                             <?php $ease = $gst->food_quality;
                                for ($x = 1; $x <= 5; $x++) {
                                    if($x <= $ease){
                                        echo '<i class="fa fa-star"style="color:red;"></i>';
                                    } else {
                                        echo '<i class="fa fa-star"style="color:black;"></i>';
                                    }
                                }
                            ?>
                        </td>
                       <td>
                            <?php $ease = $gst->env_quality;
                                for ($x = 1; $x <= 5; $x++) {
                                    if($x <= $ease){
                                        echo '<i class="fa fa-star"style="color:red;"></i>';
                                    } else {
                                        echo '<i class="fa fa-star"style="color:black;"></i>';
                                    }
                                }
                            ?>
                            </td>
                        <td>
                            <?php $ease = $gst->service;
                                for ($x = 1; $x <= 5; $x++) {
                                    if($x <= $ease){
                                        echo '<i class="fa fa-star"style="color:red;"></i>';
                                    } else {
                                        echo '<i class="fa fa-star"style="color:black;"></i>';
                                    }
                                }
                            ?>
                        </td>
                        <td>
                            <?php $ease = $gst->package;
                                for ($x = 1; $x <= 5; $x++) {
                                    if($x <= $ease){
                                        echo '<i class="fa fa-star"style="color:red;"></i>';
                                    } else {
                                        echo '<i class="fa fa-star"style="color:black;"></i>';
                                    }
                                }
                            ?>
                            </td>
                        <td>
                            <?php $ease = $gst->extra;
                                for ($x = 1; $x <= 5; $x++) {
                                    if($x <= $ease){
                                        echo '<i class="fa fa-star"style="color:red;"></i>';
                                    } else {
                                        echo '<i class="fa fa-star"style="color:black;"></i>';
                                    }
                                }
                            ?>
                        </td>-->
													<?php // Storing the colors & design for 'Yes', 'No' & 'Maybe'
															$yes='<span class="label-flat" style="color:#FFFFFF; background-color:#1C9A71;">Yes</span>';
															$maybe='<span class="label-flat" style="color:#FFFFFF; background-color:#8871B5;">Maybe</span>';
															$no='<span class="label-flat" style="color:#FFFFFF; background-color:#F3565D;">No</span>';
													?>
                        <td>
                             <?php $ease = $gst->come_back;

                                    if($ease == '2'){

                                        echo $maybe;
                                    } else if($ease == '1'){
                                        echo $yes;
                                    } else {
                                        echo $no;
                                    }

                            ?>
                        </td>
                        <td>
                              <?php $ease = $gst->refer_friend;

                                    if($ease == '2'){
                                        echo $maybe;
                                    } else if($ease == '1'){
                                        echo $yes;
                                    } else {
                                        echo $no;
                                    }

                            ?>
                        </td>
                        <td>
                             <?php $ease = $gst->reasonable_cost;

                                    if($ease == '2'){
                                        echo $maybe;
                                    } else if($ease == '1'){
                                        echo $yes;
                                    } else {
                                        echo $no;
                                    }

                            ?>
                        </td>
                        <td>
                              <?php echo $gst->comment; ?>
                            </td>


                       <td>
                             <?php $ease = $gst->add_social_media;

                                    if($ease == '1'){
                                        echo $yes;
                                    } else {
                                        echo $no;
                                    }

                            ?>
                            </td>


                    <td class="ba">
                    	<div class="btn-group">
                          <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                          <ul class="dropdown-menu pull-right" role="menu">
                            <li><a onclick="soft_delete('<?php echo $gst->id;?>')" data-toggle="modal"  class="btn red btn-xs" ><i class="fa fa-trash"></i></a></li>
                            <li><a href="<?php echo base_url() ?>dashboard/fetch_gst_by_id?fdback_id=<?php echo $gst->id;?>" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li>
                            <li><a href="<?php echo base_url() ?>dashboard/fetch2_gst_by_id?fdback_id=<?php echo $gst->id;?>" class="btn blue btn-xs " data-toggle="modal"><i class="fa fa-eye"></i></a></li>
                          </ul>
                        </div>
                    <table>
                    <tr><td>

                    </td>
                    <td>

                            </td>
                      <td>

                            <!--<a href="<?php echo base_url();?>dashboard/edit_feedback/<?php echo $gst->id; ?>" class="btn blue pull-right rgt" data-toggle="modal">Edit</a>-->
                            </td></tr>
                            </table>
                        </td>
                    </tr>


                <?php endforeach; ?>
            <?php endif; ?>


            </tbody>
        </table>

    </div>


</div>

<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){





            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_feedback?f_id="+id,
                data:{f_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
</script>
