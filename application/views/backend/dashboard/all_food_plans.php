<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
<?php endif;?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-edit"></i>List of All Food Plans </div>
		<div class="actions">
			<a href="<?php echo base_url();?>dashboard/add_food_plan" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New  </a>
		</div>
	</div>
	<div class="portlet-body">
		
		<table class="table table-striped table-bordered table-hover" id="sample_1">
			<thead>
				<tr>
					<th scope="col">Plan Name </th>
					<th scope="col">Description </th>
					<th scope="col">Plan Category </th>
					<th scope="col">Plan Class </th>
					<th scope="col">Price For Adult </th>
					<th scope="col">Price For Child </th>
					<th scope="col">Tax Applied </th>
					<th scope="col">Discount Applied</th>
					<th scope="col">Tax percentage </th>
					<!--<th scope="col"> Unit Type </th>
			<th scope="col"> Unit Class </th>
			<th scope="col" width="400"> Unit Description </th>-->
					<th scope="col"> Action </th>
				</tr>
			</thead>
			<tbody>
				<?php if(isset($unit) && $unit):

					  $i=1;
					  foreach($unit as $gst):
						  $class = ($i%2==0) ? "active" : "success";
						  $g_id=$gst->id;
						  ?>
				<tr id="row_<?php echo $gst->id;?>">
					<td>
						<?php echo $gst->fp_name; ?>
					</td>
					<td>
						<?php echo $gst->fp_description; ?>
					</td>
					<td>
						<?php echo $gst->fp_category; ?>
					</td>
					<td>
						<?php echo $gst->fp_class; ?>
					</td>
					<td>
						<?php echo $gst->fp_price_adult; ?>
					</td>
					<td>
						<?php echo $gst->fp_price_children; ?>
					</td>
					<td>
						<?php echo $gst->fp_tax_applied; ?>
					</td>
					<td>
						<?php echo $gst->fp_discount_applied; ?>
					</td>
					<td>
						<?php echo $gst->fp_tax_percentage; ?>
					</td>
					<!--<td align="center"><?php //echo $gst->unit_type; ?></td>
			<td align="center"><?php //echo $gst->unit_class;?></td>
			<td align="center"><?php //echo $gst->unit_desc;
								
								 ?></td>-->
					<!--<td align="center">  
			<a href="<?php //echo base_url() ?>dashboard/edit_unit_type/<?php //echo $gst->id;?>" class="btn blue"><i class="fa fa-edit"></i></a>            
			<a onclick="soft_delete('<?php //echo $g_id;?>')" data-toggle="modal"  class="btn red" ><i class="fa fa-trash"></i></a></td>-->


					<td align="center" class="ba">
						<div class="btn-group">
							<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li><a onclick="soft_delete('<?php echo $gst->id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a>
								</li>
								<li><a href="<?php echo base_url() ?>dashboard/edit_food_plan?id=<?php echo $gst->id;?>" class="btn green btn-xs"><i class="fa fa-edit"></i></a>
								</li>
								<li><a href="<?php echo base_url() ?>dashboard/view_food_plan?id=<?php echo $gst->id;?>" class="btn blue btn-xs"><i class="fa fa-eye"></i></a>
								</li>
							</ul>
						</div>
					</td>
				</tr>
				<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>
<script src="<?php echo base_url();?>assets/common/js/table_sort_style.js" type="text/javascript"></script>
<script type="text/javascript">
	function soft_delete( id ) {
		swal( {
			title: "Are you sure?",
			text: "All the releted transactions and data will be deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {
			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/delete_fp?fp_id=" + id,
				data: {
					m_id: id
				},
				success: function ( data ) {
					//alert("Checked-In Successfully");
					//location.reload();
					swal( {
							title: data.data,
							text: "",
							type: "success"
						},
						function () {
							//location.reload();
							$( '#row_' + id ).remove();
						} );
				}
			} );
		} );
	}
</script>