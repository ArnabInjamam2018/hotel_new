<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td{
	font-size:9px;
}
</style>

<div style="padding:15px 35px;">
<table>
		<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));?>
    <tr>
      <td align="left" valign="middle"><img src="upload/hotel/<?php
			 
			if(isset($hotel_name->hotel_logo_images_thumb)){echo $hotel_name->hotel_logo_images_thumb;}?>" alt="logo"/></td>
		<td align="right" valign="middle"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>';?></td>
        </tr>		
    
    <tr>
      <td width="100%" colspan="2"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td colspan="2"><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%" colspan="2">&nbsp;</td>
    </tr>
</table>
<table class="td-pad" width="100%">
  <thead>
    <tr style="background: #e5e5e5; color: #1b1b1b">
     
              <th> Group Booking Id </th>
              <th> Guest Name </th>
              <th> Status </th>
			  <th> Stay Duration </th>
              <th> Profit Centre </th>
              <th> Nature of visit </th>
              <th> Booking Source </th>
             <th> No of Guest </th>
              <th> No of Rooms </th>
              <th> Rooms </th>
              <th> Room Rent </th>
              <th> Room Tax </th>
              <th> Tax Applicable </th>
 			 <th> Booking Notes</th>
			 <th> Booking Preference</th>
			 <th>Arrival Mode</th>
			 <th> Food Inclusion </th>
			 <th> Food Inclusion Tax </th>
			 <th> Extra Charges </th>
			 <th> Per Day Rent </th>
			 <th> Total </th>		     
             <th> Pending </th>
		 	
    </tr>
  <tbody>
            <?php if(isset($groups) && $groups){ 
			//print_r($groups);
			foreach ($groups as $key ) {
				
			 $group_id=$key->id;
			
				$grp_status_id=$this->dashboard_model->get_bookings($group_id);
				$i=0;
				$count=0;
				$tmp=0;
				if(isset($grp_status_id) && $grp_status_id != ""){ 
				foreach($grp_status_id as $gsi)
				{
					if($i==0)
					{
						$tmp=$gsi->booking_status_id;
					}
					if($tmp!=$gsi->booking_status_id)
					{
						$count++;
					}
				$i++;	
				}
			}
			   ?>
			   
            <tr>
           
            <td><a><?php echo "HM0".$this->session->userdata('user_hotel')."GRP00".$key->id; ?></a></td>
            <td><?php if(isset($key->guestID) && $key->guestID >0){ 
				$val = $this->dashboard_model->get_guest_row($key->guestID);
				if(isset($val->g_name) && $val->g_name != ""){
					echo $val->g_name;
				}
			}?></td>
              
			  <td>
              <?php  if($count==0){
                  $status = $this->dashboard_model->get_bookings_status($tmp);
            ?>
              
              <span class="label" > <?php if(isset($status->booking_status) && ($status->booking_status != "")) {echo $status->booking_status; }?></span>
              <?php }
              
              else{?>
                    <span class="label" style="background-color:<?php echo "#f00";?>; color:<?php echo "#0ff" ?>;"> <?php echo "Partially Checked-Out"; ?></span>
              <?php }
              ?>
                
                </td>
				<td><?php $from=$key->start_date;
					$to=$key->end_date;
					$days = (strtotime($to)- strtotime($from))/24/3600;
					//echo "( ".$days." )";
					echo $key->start_date."-".$key->end_date.'</br>';
					echo  $days." (Days)"; ?>
					</td>
				<td><?php echo $key->p_center;?></td>
				<td><?php 
					$n_v=$key->nature_visit;
					if(isset($n_v) && $n_v >0){
						$nature=$this->Unit_class_model->data_booking_nature_visit($n_v);
						echo $nature->booking_nature_visit_name;
					}
					?></td>
				<td><?php echo $key->booking_source;?></td>
				
            
            <td><?php echo $key->number_guest; ?></td>
            <td><?php echo $key->number_room; ?></td>
            <td><?php $details=$this->dashboard_model->get_group_details($key->id); 
            foreach ($details as $room ) {
              # code...
              echo $room->roomNO.", ";
            }
            ?></td>
            <td><?php  
            $trr=0;
            foreach ($details as $room ) {
				 
			 $trr=$trr+($room->trr);
			 
			 $trr1=number_format($trr, 2,'.','');
              
            }
            echo $trr1;
            ?></td>
            <td><?php  
            $tax=0;
            foreach ($details as $room ) {
              
              $tax=$tax+($room->room_st)+($room->room_sc)+($room->room_vat);
            }
            echo $tax;
            ?> </td>
			<td><?php 
			
				echo $key->	tax_applicable;
             
			 ?>
			</td>
			<td><?php 
			
				echo $key->booking_notes;
             
			 ?></td>
			<td><?php 
			
				echo $key->booking_preference;
             
			 ?></td>
			
			<td><?php 
			
				echo $key->arrival_mode;
             
			 ?></td>
			
            <td><?php  
            $let2=0;
            foreach ($details as $room ) {
              # code...
              $let2=$let2+ ($room->afi*$room->adultNO)+($room->cfi*$room->childNO);
            }
            echo $let2;
            ?></td>
            <td>
			<?php
			 $fd_tax=0;
            foreach ($details as $room ) {
              # code...
              $fd_tax=$fd_tax+($room->food_vat);
            }
            echo $fd_tax;
			?>
			</td>
			
            <td><?php 
				
				echo $key->charges_cost;
				
            ?></td>
            <td><?php
			$pdr=0;			
			 foreach ($details as $room ) {
              # code...
              $pdr= $pdr+($room->pdr);
            }
            echo $pdr;
			
			?></td>
            <td><?php  $let4=0;
            foreach ($details as $rooms ) {
              # code...
              $let4=$let4+ ($rooms->price);
            }
            echo $let4;
            ?></td>
            <td><?php 
            $paid=0;
				if($key->id!=''){
                $transaction = $this->dashboard_model->all_transaction_group($key->id);
                if(isset($transaction) && $transaction){
                foreach ($transaction as $t) {
				$paid=$paid+$t->t_amount;
                }
				}
				$pending=$let4-$paid;
				 $due=number_format($pending, 2,'.','');
				echo $due;
				}
            ?></td>
		
            
            </tr>
           
            <?php }} ?>
          </tbody>
</table>
</div>
