<div class="portlet bordered light">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-users"></i>List of All Group Bookings </div>
		<div class="actions"> 
			<a href="<?php echo base_url();?>dashboard/add_group_booking" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-toolbar">
			<div class="row">
				<div class="col-md-6">
					<?php

					$form = array(
						'class' => 'form-inline',
						'id' => 'form_date',
						'method' => 'post'
					);

					echo form_open_multipart( 'dashboard/all_group_booking_by_date', $form );

					?>
					<div class="form-group">
						<input type="text" autocomplete="off" required="required" id="t_dt_frm" name="t_dt_frm" value="<?php if(isset($start_date)){echo $start_date;}?>" class="form-control date-picker" placeholder="Start Date">
					</div>
					<div class="form-group">
						<input type="text" autocomplete="off" required="required" name="t_dt_to" name="t_dt_to" value="<?php if(isset($end_date)){echo $end_date;}?>" class="form-control date-picker " placeholder="End Date">
					</div>
					<button class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search"></i></button>
					<?php form_close(); ?>
				</div>
				<script type="text/javascript">
					function check_sub() {
						document.getElementById( 'form_date' ).submit();
					}
				</script>
			</div>
		</div>
		<table class="table table-striped table-bordered table-hover" id="sample_3">
			<thead>
				<tr>
					<th> # </th>
					<th> Group Booking Id </th>
					<th> Status </th>
					<th> Guest Name </th>
					<th> Booking Date </th>
					<th> Stay Duration </th>
					<th width="8%"> P Centre, Nature & Source </th>
					<!-- <th> Nature </th>
              <th> Source </th> -->
					<th align="center"> PAX </th>
					<th align="center"> # Rooms </th>
					<th> Rooms </th>
					<th align="center"> TRR </th>
					<th align="center"> Room Tax </th>
					<!--<th> Tax? </th>-->
					<!--<th> Booking Notes</th>
			 <th> Booking Preference</th>			
			 <th> Arrival Mode</th>-->
					<th> Food Incl </th>
					<th> FI Tax </th>
					<th> Ex Charges </th>
					<th> POS Amt </th>
					<!-- <th> PDR </th>-->
					<th align="center"> Total </th>

					<th> Pending </th>
					<th align="center" width="5%" class="action"> Action </th>
				</tr>
			</thead>
			<tbody>
				<?php if(isset($groups) && $groups){ 
			//echo '<pre>';
			//print_r($groups);
			//exit;
			$msg='';
			$sl = 0;
			foreach ($groups as $key ) {
			 $sl++;
			 $group_id=$key->id;
			
				$grp_status_id=$this->dashboard_model->get_bookings($group_id);
				$i=0;
				$count=0;
				$tmp=0;
				if(isset($grp_status_id) && $grp_status_id != ""){ 
				foreach($grp_status_id as $gsi)
				{
					if($i==0)
					{
						$tmp=$gsi->booking_status_id;
					}
					if($tmp!=$gsi->booking_status_id)
					{
						$count++;
					}
				$i++;	
				}
			}
			if($key->tax_applicable!='YES'){
				$msg="No tax";
			}else{
				$msg="";
			}
			   ?>
				<tr>
					<td>
						<?php echo $sl;?>
					</td>
					<td>
						<!-- Booking ID -->
						<a href="<?php echo base_url(); ?>dashboard/booking_edit_group?group_id=<?php echo $key->id; ?>">
							<?php echo "HM0".$this->session->userdata('user_hotel')."GRP00".$key->id.' ';
					if($msg!=""){ 
						echo '<i class="fa fa-minus-circle" style="float:right; color:#E27979;"></i>';
					}
					$gb = 'gb';
					$pos = $this->bookings_model->pos_check($key->id, $gb);
					if($pos == 1) //if POS is in grp
					{
						echo '<i class="fa fa-cutlery" style="float:right; color:#0000B3;"></i>';	
					} 
					/*else 
					{
						
					}	*/			
				?>
						</a>

					</td>
					<td>
						<!-- Booking Status -->
						<?php if($count==0){
					  $status = $this->dashboard_model->get_bookings_status($tmp);
				?>

						<span class="label" style="background-color:<?php if(isset($status->bar_color_code)){echo $status->bar_color_code;}?>; color:<?php 
					if(isset($status->body_color_code)){
						echo $status->body_color_code;
					}
					
				  ?>;">
							<?php if(isset($status->booking_status) && ($status->booking_status != "")) {echo $status->booking_status; }?>
						</span>
						<?php }
				  
				  else
						{?>
						<span class="label" style="background-color:<?php echo " #f00 ";?>; color:<?php echo "#0ff " ?>;">
							<?php echo "Partially Checked-Out"; ?>
						</span>
						<?php }
				  ?>

					</td>
					<td>
						<!-- Guest Name -->
						<?php $res = $this->dashboard_model->get_guest_row($key->guestID); 
					if($res != false)
					{
						echo $res->g_name;
					}?>
					</td>

					<td>
						<?php // Booking Date
					//echo $key->booking_date;
					echo date("g:ia \-\n l jS F Y",strtotime($key->booking_date));
				?>
					</td>

					<td>
						<!-- Booking Dates -->
						<?php 
						$from=$key->start_date;
						//$dtf = date("d-M-Y", strtotime($key->start_date->date));
						$to=$key->end_date;
						$days = (strtotime($to)- strtotime($from))/24/3600;
						//echo "( ".$days." )";
						echo date("dS M Y",strtotime($key->start_date))." - ";
						If($days == 1)
							echo  '('.$days." (Day)"; 
						else
							echo  '('.$days." Days)";
						echo '<br>'.date("dS M Y",strtotime($key->end_date)).'</br>';
					?>
					</td>



					<td>
						<?php // Profit Center
					echo '<span style="font-weight:800">Profit Center: </span>'.$key->p_center.'<br/>';
					if(isset($key->nature_visit) && $key->nature_visit !=""){
										$n_v=$key->nature_visit;
										
										if($n_v){
											//echo $n_v;
											$nature=$this->unit_class_model->data_booking_nature_visit($n_v);
										//print_r($nature);
											if(isset($nature) && $nature->booking_nature_visit_name !=""){
												echo $nature->booking_nature_visit_name;
											}
											else 
												echo 'No info';
										}
										else{
											
											echo "ID Mismatch";
										}
									//	
											
											
										}
										echo '<br/>'.$key->booking_source;
										
				?>
					</td>
					<!--<td><?php 	// Nature of Visit
                                        if(isset($key->nature_visit) && $key->nature_visit !=""){
										$n_v=$key->nature_visit;
										
										if($n_v){
											//echo $n_v;
											$nature=$this->unit_class_model->data_booking_nature_visit($n_v);
										//print_r($nature);
											if(isset($nature) && $nature->booking_nature_visit_name !=""){
												echo $nature->booking_nature_visit_name;
											}
											else 
												echo 'No info';
										}
										else{
											
											echo "ID Mismatch";
										}
									//	
											
											
										}
										echo $key->booking_source;
										
					?>
			</td>
			<td>
				<?php // Bookig Source
					
				?>
			</td>-->


					<td align="center">
						<?php // No of Guest
					echo $key->number_guest; ?>
					</td>
					<td align="center">
						<?php // No of Room
					echo $key->number_room;
					//echo  "id".$key->id;
					?>

					</td>
					<td>
						<?php  // Rooms
				if(isset($key->id) && $key->id!=''){
					$details=$this->dashboard_model->get_group_details($key->id); 
					$detailslineitem=$this->bookings_model->get_grp_line_items($key->id); 
					
					//print_r($details);
					//exit;
				}
					foreach ($details as $room ) {
					  # code...
					  echo '<span class="label" style="background-color:#F8681A; color:#ffffff;">'.$room->roomNO.'</span>'.' ';
					}
				?>
					</td>
					<td align="center">
						<?php  // TRR
				$line_items =$this->bookings_model->line_charge_item_tax('gb',$key->id);
				$trr=0;
				$trrtax = 0;
				$mealcharge = 0;
				$mealchargetax =0;
				if(isset($detailslineitem) && is_array($detailslineitem)){
					foreach ($detailslineitem as $lineitem ) {
					 
					 $trr=$trr+($lineitem->room_rent);
					 $trrtax=$trrtax+($lineitem->rr_total_tax);
					 $mealcharge=$mealcharge+($lineitem->meal_plan_chrg);
					 $mealchargetax=$trrtax+($lineitem->mp_total_tax);
					  
					}
				}
				
				
				echo $trr;
				
				
				?>
					</td>
					<td align="center">
						<?php  //Room Tax
					
					
				echo $trrtax; 
				
				?>
					</td>


					<td align="center">
						<?php  //Food Inclution
					if($trrtax>0){
						echo '<span style="color:#B86953;">'.number_format($trrtax,2,'.','').'</span>';
					}
					else
						echo '<span style="color:#AAAAAA;">'.number_format($trrtax,2,'.','').'</span>';
				?>
					</td>
					<td align="center">
						<?php //Food Tax
					
					if($mealchargetax>0){
						echo '<span style="color:#B86953;">'.number_format($mealchargetax,2,'.','').'</span>';
					}
					else
						echo '<span style="color:#AAAAAA;">'.number_format($mealchargetax,2,'.','').'</span>';
				?>
					</td>
					<td align="center">
						<?php //Extra Charge
					$eCharge=$line_items['all_price']['exrr_chrg'];
					if($eCharge>0){
						echo '<span style="color:#4E85C5;">'.number_format($eCharge,2,'.','').'</span>';
					}
					else
						echo '<span style="color:#AAAAAA;">'.number_format($eCharge,2,'.','').'</span>';
					
				?>
					</td>
					<td>
						<?php //POS Amount
						$pos_sum = $this->dashboard_model->all_pos_booking_group($key->id);
						//print_r($pos_sum);
						$pos_amt = 0;
						foreach($pos_sum as $pos_s){
							$pos_amt = $pos_amt + $pos_s->total_amount;
						}
						if($pos_amt>0){
							echo '<span style="color:#4E85C5;">'.number_format($pos_amt,2,'.','').'</span>';
						}
						else
							echo '<span style="color:#AAAAAA;">'.number_format($pos_amt,2,'.','').'</span>';
					?>
					</td>



					<td align="center">
						<?php  //Total
					$let4=$trr +  $trrtax + $mealcharge + $mealchargetax;
						echo number_format(($trr +  $trrtax + $mealcharge + $mealchargetax),2,'.','');
				?>
					</td>
					<td>
						<?php // Pending
					$paid=0;
					if(isset($key->additional_discount) && ($key->additional_discount != NULL)){
						$disc = $key->additional_discount;
					}
					
					$transaction = $this->dashboard_model->all_transaction_group($key->id);
					if($transaction){
						foreach ($transaction as $t) {
						  # code...
						  $paid=$paid+$t->t_amount;
						}
					}
					$g_pend = number_format((($let4 + $pos_amt + ($key->charges_cost) - $paid - $disc)),2,'.','');
					if($g_pend<0)
						echo '<span style="color:#FF0074;">'.number_format(($g_pend),2,'.','').'</span>';
					else if($g_pend>0)
						echo '<span style="color:#03893B;">'.number_format(($g_pend),2,'.','').'</span>';
					else if($g_pend == 0)
						echo '<span class="label" style="background-color:#0BF800; color:#333333;">Paid</span>';
				?>
					</td>
					<td class="ba">
						<div class="btn-group">
							<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li> <a onclick="soft_delete('<?php echo $key->id ?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a> </li>
								<li><a href="<?php echo base_url(); ?>dashboard/booking_edit_group?group_id=<?php echo $key->id ?>" class="btn green btn-xs"><i class="fa fa-edit"></i></a>
								</li>
							</ul>
						</div>
					</td>

				</tr>

				<?php }} ?>
			</tbody>
		</table>

		<?php 
		if(isset($key->id) && $key->id ){
			
			$keyid = $key->id;
			$line_items =$this->bookings_model->line_charge_item_tax('gb',$keyid);
		}
		else{
			$keyid ='';
		}
		
				//echo "<pre>";
				//print_r($line_items); ?>
	</div>
</div>
<script>
	function soft_delete( id ) {
		alert( id );
		swal( {
			title: "Are you sure?",
			text: "All the releted transactions and data will be deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {
			// alert(id);





			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/soft_delete_group_booking?id=" + id,
				data: {
					id: id
				},
				success: function ( data ) {

				//	alert( data.data );
					//alert("Checked-In Successfully");
					//location.reload();
						if(data.data==0){
				     window.location.replace("<?php echo base_url()?>dashboard");
				}else{
				    	swal( {
							title: data.data,
							text: "",
							type: "success"
						},
						function () {

							location.reload();

						} );
				}
				}
			} );



		} );
	}
</script>
<!-- END CONTENT -->