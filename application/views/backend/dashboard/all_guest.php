<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
<?php endif;?>

<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-street-view" aria-hidden="true"></i> List of All Guests </div>
		<div class="actions">
			<a href="<?php echo base_url();?>dashboard/add_guest" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i> Add New  </a>
		</div>
	</div>
	<div class="portlet-body">
		<table class="table table-striped table-bordered table-hover" id="sample_1">
			<thead>
				<tr>
					<th scope="col"> # </th>
					<th scope="col" width="10%"> Photo </th>
					<th scope="col"> Name </th>
					<th scope="col"> Guest Type </th>
					<th width="8%" scope="col"> Contact Details </th>
					<th width="10%" scope="col"> Contact Details </th>
					<th width="7%" scope="col"> Gender </th>
					<th scope="col"> Age </th>
					<th width="10%" scope="col"> Professional Info </th>
					<th scope="col"> Marital Status </th>
					<th scope="col"> Discount Plan </th>

					<th width="10%"> Id Proof </th>

					<th align="center" ; width="5%" ; scope="col"> Action </th>
				</tr>
			</thead>
			<tbody>
				<?php

				if ( isset( $guest ) && $guest ):
					//echo "<pre>";
						//print_r($guest);
					$i = 1;
				$sl = 0;
				foreach ( $guest as $gst ):
					$sl++;
				$class = ( $i % 2 == 0 ) ? "active" : "success";
				$g_id = $gst->g_id;
				?>
				<tr>
					<td>
						<?php echo $sl;?>
					</td>
					<td><a class="single_2" href="<?php echo base_url();?>upload/guest/originals/photo/<?php
			  if( $gst->g_photo== '') {
				if($gst->g_id_proof== ''){
					
					if($gst->g_gender == 'Male'){
						echo "no_guest_imageM.jpg"; 
					}
					else if($gst->g_gender == 'Female'){
						echo "no_guest_imageG.jpg"; 						
					} else {
						echo "no_guest_image.jpg"; 
					}
				}
				else{
				  echo $gst->g_id_proof;
				}
			  }	else {
				  echo $gst->g_photo;
			  }?>"><img src="<?php echo base_url();?>upload/guest/thumbnail/photo/<?php
			  
			  if( $gst->g_photo_thumb== '') {
				if($gst->g_id_proof_thumb== ''){
					
					if($gst->g_gender == 'Male'){
						echo "no_guest_imageM.jpg"; 
					}
					else if($gst->g_gender == 'Female'){
						echo "no_guest_imageG.jpg"; 						
					} else {
						echo "no_guest_image.jpg"; 
					}
				}
				else{
				  echo $gst->g_id_proof_thumb;
				}
			  } else {
				  echo $gst->g_photo_thumb;
			  }?>" alt="" style="width:100%;"/></a>
			
			
			 <?php
						/*echo $gst->g_photo ;
						echo $gst->g_photo ."_thumb"; */
						?>
					</td>
					<td>
						<?php // Guest Name
					if($gst->g_gender == 'Male')
						echo 'Mr. ';
					else if($gst->g_gender == 'Female'){
						if($gst->g_married == 'married')
							echo 'Mrs. ';
						else
							echo 'Ms. ';
					}
					else
						echo ' ';
					echo '<strong>'.$gst->g_name.'</strong>'; 
				?>
					</td>
					<td>
						<?php 
					// Logic for showing the guest type _sb dt3-10-16
					if($gst->g_type == '' && $gst->g_type == NULL){
						echo '<span style="color:#AAAAAA;">No info</span>';
					}
					else{ // If The guest type is not blank					
						 
						if($gst->g_type=="Corporate"){
							if(isset($corporate)){					
								$c_id=$gst->corporate_id;
								$c_name=$this->dashboard_model->get_company_name($c_id);
								echo '<span style="color:#7A2A8C;">'.$gst->g_type.'</span>';					
							}
						  	
							  echo "<br>";
							  if($c_name!=''){
							  echo "(".$c_name->name." - ".$gst->c_des.")";
							  
							}
						}
						else if($gst->g_type=="Military"){
							echo '<span style="color:#587A04;">'.$gst->g_type.'</span>';
							  if($gst->g_type=="Military" && $gst->retired=="yes"){	
							  echo "<br>";
							  echo "(".$gst->rank." - Retired)";							  
							 }
							 
							  else if($gst->g_type=="Military" && $gst->retired=="no"){		 
							  echo "<br>";
							  echo "(".$gst->rank." - In Service)";						  
							 }
						}						

						else if($gst->g_type=="VIP" ){						 
							  echo '<i style="color:#FFC20E;" class="fa fa-star" aria-hidden="true"> </i><span style="color:#FFC20E;">'.$gst->g_type.'</span>';
							  echo "<br>";
							  echo "(".$gst->type." - ".$gst->class. ")";						  
						}				
						else if($gst->g_type=="Family"){						
							  echo '<span style="color:#4E85C5;">'.$gst->g_type.'</span>';
							  echo "<br>";
							  if($gst->f_size>0)
								echo "(size - ".$gst->f_size.")";
						}
						else if($gst->g_type=="Government_official"){
						  echo '<span style="color:#DB665A;">Government Official</span>';
						  echo "<br>";
						  echo "(".$gst->dep." - ".$gst->c_des.")";					  
						}
						else
							echo '<span style="color:#7A2A8C;">'.$gst->g_type.'</span>';
					}
					
				?>
					</td>
					<td>
						<?php   // Contact Details
						// Logic for showing Full Address properly _sb dt30_9_16
						echo '<i style"color:#AAAAAA;" class="fa fa-home" aria-hidden="true"></i>  &nbsp;';
							$fg = 10;
						if(isset($gst->g_address) && ($gst->g_address != '' && $gst->g_address != ' ')){
								echo ($gst->g_address);
								$fg = 0	;
								
						}
						if(isset($gst->g_city) && ($gst->g_city != '' && $gst->g_city != ' ')){
								if($fg == 0){
									echo ', ';
									
								}
									
								echo $gst->g_city;
								echo '<br>';
								$fg = 1;
						}
						else if($fg == 0)
								$fg = 5;
						if(isset($gst->g_state) && ($gst->g_state != '' && $gst->g_state != ' ')){
								if($fg == 5)
									echo '<br>';
								echo $gst->g_state;
								$fg = 2;
								
							}
						if(isset($gst->g_pincode) && $gst->g_pincode!= NULL){
								if($fg == 2 || $fg == 5){
									echo ', ';
								}									
								echo 'Postal Code - '.$gst->g_pincode;
								$fg = 3;
							}
						if(isset($gst->g_country) && $gst->g_country != '' && $gst->g_country != ' '){
								if($fg == 3 || $fg == 2)
									echo ', ';
								echo $gst->g_country;
								if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';
								$fg = 4;
							}
						if($fg == 10){
								echo '<span style="color:#AAAAAA;">No info</span>';
							}
					/*
					// Contact No
					echo '<br><i style"color:#AAAAAA;" class="fa fa-phone" aria-hidden="true"></i>  &nbsp;';
					if($gst->g_contact_no != NULL){
						echo $gst->g_contact_no;
					}
					else
						echo '<span style="color:#AAAAAA;">No info</span>';
					
					// Email
					echo '<br><i style"color:#AAAAAA;" class="fa fa-envelope" aria-hidden="true"></i>  &nbsp;';
					if($gst->g_email != NULL){
						echo $gst->g_email;
					}
					else
						echo '<span style="color:#AAAAAA;">No info</span>';
					*/		
				?>
					</td>

					<td>
						<?php  // Contact Details
					// Contact No
					echo '<i style"color:#AAAAAA;" class="fa fa-phone" aria-hidden="true"></i>  &nbsp;';
					if($gst->g_contact_no != NULL){
						echo $gst->g_contact_no;
					}
					else
						echo '<span style="color:#AAAAAA;">No info</span>';
					echo '<br>';
					// Email
					echo '<i style"color:#AAAAAA;" class="fa fa-envelope" aria-hidden="true"></i>  &nbsp;';
					if($gst->g_email != NULL){
						echo $gst->g_email;
					}
					else
						echo '<span style="color:#AAAAAA;">No info</span>';
				?>
					</td>
					<!--<td>
                <?php // Email
					echo $gst->g_email;
				?>
              </td>-->
					<td>
						<?php // Gender
					if(isset($gst->g_gender) && $gst->g_gender){
								if($gst->g_gender == 'Male') 
									echo '<label style="color:#0476F6"><i style"color:#0476F6;" class="fa fa-male" aria-hidden="true"></i></label>';
								else if($gst->g_gender == 'Female') 
									echo '<label style="color:#FF2A6A"><i style"color:#FF2A6A;" class="fa fa-female" aria-hidden="true"></i></label>';
								else if($gst->g_gender == 'Others') 
									echo '<label style="color:#808080"><i style"color:#808080;" class="fa fa-transgender" aria-hidden="true"></i></label>';
								
								echo '&nbsp; '.$gst->g_gender;
							}
					else{
						echo '<span style="color:#AAAAAA;">No info</span>';
					}						
					 
					?>
					</td>
					<td>
						<?php 
							date_default_timezone_set('Asia/Kolkata');
							if(isset($gst->g_dob) && $gst->g_dob >0){
														//echo $polices->g_dob;	
											$d1 = new DateTime($gst->g_dob);
											$d2 = new DateTime();

														$diff = $d2->diff($d1);
														// _sb
												if($diff->y == 0){
													echo '<span style="color:#AAAAAA;">n/a</span>';
												}
										else if($diff->y >= 60) {
											echo '<span class="label" style="background-color:#EEFFD0; color:#515A5E; font-size:10px;">'.'<i style"color:#808080;" class="fa fa-blind" aria-hidden="true"></i> '.$diff->y.'</span>';
										}	
										else
											echo $diff->y;
							} 
							else {
								echo '<span style="color:#AAAAAA;">n/a</span>';
							}
										
				?>
					</td>
					<td>
						<?php // Professional Info
					
					echo '<br><strong>Occupation: </strong>';
					if($gst->g_occupation != ''){
						echo $gst->g_occupation;
					}else
						echo '<span style="color:#AAAAAA;">No info</span>';
					 
					
					// Logic for showing the Highest Qualification
					$f=0;
					if($gst->g_highest_qual_level!=''){
						echo '<br><strong>Qualification: </strong>';
						if($gst->g_highest_qual_level == 9)
							echo '<span style="color:#AAAAAA;">No info</span>';
						else if($gst->g_highest_qual_level == 0)
							echo 'Bellow Secondary';
						else if($gst->g_highest_qual_level == 1)
							echo 'Secondary';
						else if($gst->g_highest_qual_level == 2)
							echo 'Higher Secondary';
						else if($gst->g_highest_qual_level == 3)
							echo 'Graduate';
						else if($gst->g_highest_qual_level == 4)
							echo 'Post Graduate';
						else if($gst->g_highest_qual_level == 5)
							echo 'Doctorate';
						
						$f=1;
					}
					if($gst->g_highest_qual_name){
						if($f == 1)
							echo '<br>';
						echo $gst->g_highest_qual_name;
					}
					// End Highest Qualification Logic
					
				?>
					</td>
					<td>
						<?php // Marital Status
					 
					if($gst->g_married != ''){
						if($gst->g_married == 'married')
							echo '<span style="color:#4F79BF; text-transform: capitalize;">'.$gst->g_married.'</span>';
						else if($gst->g_married == 'unmarried')
							echo '<span style="color:#436F61; text-transform: capitalize;">'.$gst->g_married.'</span>';
						else
							echo '<span style="color:#672626; text-transform: capitalize;">'.$gst->g_married.'</span>';
						$createDate = new DateTime($gst->g_anniv);
						$strip = $createDate->format('d M y');
						if($gst->g_anniv != '0000-00-00')
							echo '<br>'.$strip;
					}
					else
						echo '<span style="color:#AAAAAA;">No info</span>';
				?>
					</td>
					<td>
						<?php  // Discount Rule					
					// Logic for finding chars seperatd by comma & showing the discount plans _sb dt04_10_16
					$g=$gst->cp_discount_rule_id;
					$c = '';
					if($g != ''){						
						$arr = (explode(",",$g));
						$l = count($arr);						
						for($i=0;$i<$l;$i++){
							$dp = $this->unit_class_model->get_rule($arr[$i],$c);
							if(isset($dp)){
								echo '<span class="label" style="background-color:#72ECBE; color:#515A5E;">'.$dp->rule_name.'</span>';
							}
						}	
                        /*foreach($arr as $key){
							//echo $key;
							$dp = $this->unit_class_model->get_rule($key,$c);
							echo '<span class="label" style="background-color:#72ECBE; color:#515A5E;">'.$dp->rule_name.'</span>'.'<br><br>';							
						}*/						
					}
					/*echo '<br>';
					
					$rule= $this->unit_class_model->get_rule($g,$c);
					
					
					if(isset($rule) && $rule != '' && $rule != NULL){
						echo '<span class="label" style="background-color:#72ECBE; color:#515A5E;">'.$rule->rule_name.'</span>'; 
						foreach($rule as $rule_s){
							//print_r($rule_s);
							//echo $rule_s->rule_name;
						}
						//echo $rule;
						//print_r($rule);
					}*/
					else{
						echo '<span class="label" style="background-color:#E4D5EE; color:#515A5E;">None</span>';
					}
				?>
					</td>
					<!-- <td>
                                        <?php $m= $gst->g_id_type;
                                        if($m==1){
                                            echo "Passport";
                                        }
                                        elseif($m==2){ echo "PAN Card";}
                                        elseif($m==3){echo "Voter Card";}
                                        elseif($m==4){echo "Adhar Card";}
                                        elseif($m==5){echo "Driving License";}
                                        elseif($m==6){echo "Others";}


                                        ?>
                                    </td>-->

					<?php //echo $gst->g_photo ?>

					<td>
						<!--<img  width="100%" src="<?php //echo base_url();?>upload/<?php //if( $gst->g_id_proof== '') { echo "no_images.png"; } else { echo $gst->g_id_proof; }?>" alt=""/>
				-->

						<?php 
				if( $gst->g_id_proof== '') { $imgn="no_images.png"; } else { $imgn=$gst->g_id_proof; }
					$img_path=base_url().'upload/guest/originals/id_proof/'.$imgn;

				?>
						<?php 
				if($gst->g_id_type){echo $gst->g_id_type.' ';}
				 if($gst->g_id_number){echo "<br>".$gst->g_id_number.' ';}
				 if($imgn!='no_images.png'){echo '<a  href="'.$img_path.'" download><i class="fa fa-download" aria-hidden="true"></i></a>';}  
				 if($imgn!='no_images.png'){echo ' <a  href="'.$img_path.'" onclick="openimg('.$img_path.')" target="_blank" ><i class="fa fa-eye" aria-hidden="true"></i></a>';}  ?>



					</td>

					<td class="ba">
						<div class="btn-group">
							<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li><a onclick="soft_delete('<?php echo $gst->g_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a>
								</li>
								<li>
									<a href="<?php echo base_url();?>dashboard/edit_guest?g_id=<?php echo $gst->g_id; ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a>
								</li>
							</ul>
						</div>
					</td>
				</tr>

				<?php endforeach; endif; ?>
			</tbody>
		</table>
	</div>
</div>

<script>
	function openimg( vl ) {

		window.open( vl );
	}

	function soft_delete( id ) {
		swal( {
			title: "Are you sure?",
			text: "All the releted transactions and data will be deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {
			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/delete_guest?g_id=" + id,
				data: {
					booking_id: id
				},
				success: function ( data ) {
					//alert("Checked-In Successfully");
					//location.reload();
					swal( {
							title: data.data,
							text: "",
							type: data.type
						},
						function () {

							location.reload();

						} );
				}
			} );
		} );
	}
</script>