<div class="portlet light borderd">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-hotel"></i>List of All Hotels </div>
    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body">
    
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <!--<th scope="col"> Select </th>-->
          <th> Hotel </th>
          <th> Address </th>
          <th> No Of Rooms </th>
          <th> Phone </th>
          <th> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($hotels) && $hotels):
                            $i=1;
                            foreach($hotels as $hot): 
                            $class = ($i%2==0) ? "active" : "success";
                            
                            $hotel_id=$hot->hotel_id;
                            ?>
        <tr> 
          <!--<td><input type="checkbox" name="delete[]" value="<?php echo $hot->hotel_id;?>" class="form-control" onclick="this.form.submit();"/></td>-->
          <td><?php echo $hot->hotel_name;?></td>
          <td><?php echo $hot->hotel_street1." ".$hot->hotel_street2." ".$hot->hotel_landmark." ".$hot->hotel_district." ".$hot->hotel_pincode." , ".$hot->hotel_state." ".$hot->hotel_country;?></td>
          <td><?php echo $hot->hotel_rooms;?></td>
          <td><?php echo $hot->hotel_frontdesk_mobile;?></td>
          <td><!-- <a href="<?php //echo base_url();?>dashboard/edit_hotel/<?php //echo base64url_encode($hotel_id);?>" class="fa fa-edit"></a> --> 
            <a href="<?php echo base_url();?>setting/edit_hotel?hotel_id=<?php echo $hot->hotel_id ?>" class="btn blue btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></td>
        </tr>
        <?php $i++;?>
        <?php endforeach; ?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>
