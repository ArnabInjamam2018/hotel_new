<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>

<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Profit Center</h4>
      </div>
      <?php

	  $form = array(
		  'class' 			=> 'form-body',
		  'id'				=> 'form',
		  'method'			=> 'post'
	  );
	
	  echo form_open_multipart('dashboard/add_profit_center',$form);
	
	  ?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="pc_location" id="pc_location" required placeholder="Nature of visit Name">
              <label></label>
              <span class="help-block">Profit Center Location..</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="balance" name="balance" placeholder="Enter Balance  ..."  maxlength="10" onkeypress=" return onlyNos(event, this);" >
              <label></label>
              <span class="help-block">Enter Balance *</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select onchange="check(this.value)" class="form-control"  id="default" name="default" required="required" >
                <option value="" selected="selected" disabled="disabled">Default nature visit</option>
                <option value="0">No</option>
                <option value="1">Yes</option>
              </select>
              <label></label>
              <span class="help-block">Default nature visit *</span> </div>
          </div>
          <div class="col-md-12">
            <div class="form-group form-md-line-input">
              <textarea class="form-control" row="3" name="des" placeholder="Description" id="des"></textarea>
              <label></label>
              <span class="help-block">Enter Description...</span> </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
      <?php echo form_close(); ?> </div>
  </div>
</div>
<!--edit modal -->

<div id="edit_profit" class="modal fade" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Profit Center</h4>
      </div>
      <?php

	  $form = array(
		  'class' 			=> 'form-body',
		  'id'				=> 'form',
		  'method'			=> 'post'
	  );
	
	  echo form_open_multipart('dashboard/edit_profit_center',$form);
	
	  ?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="name1" id="name1" required placeholder="Nature of visit Name">
              <label></label>
              <span class="help-block">Profit Center Location..</span> </div>
          </div>
          <input type="hidden" name="hid1" id="hid1" >
          <input type="hidden" name="hid2" id="hid2" >
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="balance1" name="balance1" placeholder="Enter Balance  ..."  maxlength="10" onkeypress=" return onlyNos(event, this);" >
              <label></label>
              <span class="help-block">Enter Balance  ...</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select onchange="check1(this.value)" class="form-control"  id="default1" name="default1" required="required" >
                <option value="" selected="selected" disabled="disabled">Default nature visit</option>
                <option value="0">No</option>
                <option value="1">Yes</option>
              </select>
              <label></label>
              <span class="help-block">Default nature visit *</span> </div>
          </div>
          <div class="col-md-12">
            <div class="form-group form-md-line-input">
              <textarea class="form-control" row="3" name="des1" placeholder="Description" id="des1"></textarea>
              <label></label>
              <span class="help-block">Enter Description...</span> </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
      <?php echo form_close(); ?> </div>
  </div>
</div>
<div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption"> <span class="caption-subject bold uppercase">All Laundry Rates</span> </div>
        <div class="actions"> <a href="javascript:;" class="btn btn-circle btn-icon-only green" onClick='gbEdit()'><i class="fa fa-pencil-square-o"></i></a> </div>
      </div>
      <div class="portlet-body">
      	<div class="table-toolbar">
      <div class="row">
        <div class="col-md-6">
         
		<div class="btn-group">
            <button  class="btn green" onclick="reset_laundry_rates()" > Rest Laundry Rates <i class="fa fa-plus"></i> </button>
          </div>
		  
		 <!--<div class="btn-group">
            <button  class="btn green" data-toggle="modal" href="#responsive"> Add Profit Center <i class="fa fa-plus"></i> </button>
          </div>
          <div class="btn-group ">
            <button class="btn green" id="status123" onclick="profit_center()"> <span id="demo">Active Profit Center</span> </button>
          </div>-->
        </div>
        
      </div>
    </div>
            <div class="table-scrollable" id="gpSave"> 
              <!-- <form type="post" target="<?php echo base_url(); ?>dashboard/add_group_booking" >-->
              <table class="table table-striped table-hover table-bordered mass-book">
                <tbody>
                  <tr style="color:#F8681A;">
                    <th> ID </th>
                    <th scope="col">Cloth Type</th>
					<th scope="col">Fabric Type</th>
					<th scope="col">Laundry Rate</th>
					<th scope="col">Dry clining Rate</th>
					<th scope="col">Ironing Rate</th>
					<th scope="col">Default Type</th>
					<th scope="col"> Action </th>
					
                  </tr>
                    <?php 
				    //echo "<pre>";
				    //print_r($details);
				    $i=0; $total=0; 
					if(isset($rates) && $rates != ""){ 
				    foreach ($rates as $key  ) { 
						
					?>
                  <tr>
                    <td><?php echo $key->lr_id; ?></td>                    
                    <td><?php  
					
					$i=$key->lr_cloth_type_id;	
					if($i>0){					
						$cloth_type=$this->unit_class_model->get_cloth_type_by_id($i);
						echo $cloth_type->laundry_ct_name;
					}
					else{
						echo '--';
					}
					?>
					</td>
                    <td>
						<?php 
							$j=$key->lr_fabric_type_id;
							if($j>0){					
							$fabric=$this->unit_class_model->get_fabric_type_by_id($j);
							echo $fabric->fabric_type;
							}
							else{
								echo '--';
							}	
						?>
					
					
					</td>
                    <td><input class="form-control input-sm" value="<?php  echo $key->lr_laundry_rate; ?>" name="" onblur="abc('<?php echo $key->lr_id ?>',this.value,'lr_laundry_rate')" type="text" /></td>
                    <td><input class="form-control input-sm" value="<?php echo $key->lr_dry_cleaning_rate; ?>" name="" onblur="abc('<?php echo $key->lr_id ?>',this.value,'lr_dry_cleaning_rate')" type="text" /></td>
                     <td><input class="form-control input-sm" value="<?php echo $key->lr_ironing_rate; ?>" name="" onblur="abc('<?php echo $key->lr_id ?>',this.value,'lr_ironing_rate')" type="text" /></td>
                    <td>
						<?php echo $key->lr_default_type; ?></td>
                    <td></td>
                    
					
					<td>
						<?php
							/*$cloth_type=$this->unit_class_model->all_laundry_cloth_type();
							$fabric_type=$this->unit_class_model->all_laundry_fabric_type();
							foreach ($cloth_type as $ct){
								echo $ct->laundry_ct_name.'<br>';
							}*/
                    	?>
						<!--<table width="100%">
                        	<tr>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>-->
                    </td>
					<?php } ?>
                  </tr>
                  <?php //$total=$total+ $key->price+ $key->service;
				  } ?>
                </tbody>
              </table>
            </div>
		  	<?php
			$attrib = array('role' => 'form', 'id' => 'myForm');
			echo form_open_multipart("setting/edit_group_booking", $attrib);
			?>
			<div style="display:none;" id="gpEdit">
            <div id="loader" style="display:none;">
              <div class='loader'>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                
				<div class='door'></div>				
                <div class='hotel-sign'> <span>H</span> <span>O</span> <span>T</span> <span>E</span> <span>L</span> </div>
              </div>
            </div>
            <div class="form-body">
            <div class="grupedit" >
                <div class="row" style="margin-bottom:10px;">
                    <div class="col-md-12 bl-allroom" id="rmt">
						<div class="all-re">
							<div class="all-retitle uppercase">Available Room</div>
							<div class="portlet-body form">
							  <?php
								foreach($result as $res) { 
									$room=$this->dashboard_model->get_room_number_exact($res);
									$data="";
									$room_number=$room->room_no;
									echo $data =' <div class="btn-group"> <label class="btn grey-cascade" id="abc_'.$res.'"> <input id="ab_'.$res.'" class="noClick" style="display: none;" onclick="addRow(this.value,'.$room_number.')" value="'.$res.'" type="checkbox" > '.$room_number.' </label></div>';
								}						  
							  ?>			
							</div>
						</div>
                    </div>
                </div>
              <div id="bcid">
                <ul class="nav nav-tabs">
                  <li class="active"> <a href="#default" data-toggle="tab">Default Rates</a> </li>
                  <li> <a href="#individual" data-toggle="tab">Individual Rates</a> </li>
                </ul>
                <div class="tab-content">
                  <div class="row tab-pane fade active in" id="default">
                    <div class="col-md-12">
                        <div role="form" class="form-inline" style="overflow:hidden;">
							<div class="pull-left">
							   <label class="pad-s">Default Room Rent</label>
							   <div class="form-group">
								<input class="form-control input-small" type="text" id="trrID" value="">
							   </div>
							   <button class="btn btn-default" type="button" id="btn1">Add</button>
							</div>
							<div class="pull-right">
								<label class="pad-s">Default Food Inclusion</label>
							    <div class="form-group">
									<input class="form-control input-small" type="text" id="tfiID" value="">
							    </div>
							    <div class="form-group">
									<input class="form-control input-small" type="text" placeholder="Meal Plan" id="cmp1" style="display:none;">
									<select class="form-control" onchange="getChange1(this.value)">
									  <option  value="" selected="selected">Plan</option>
									  <option value="AP">AP</option>
									  <option value="MAP">MAP</option>
									  <option value="EP">EP</option>
									  <option value="CP">CP</option>
									</select>
							    </div>
							    <button class="btn btn-default" type="button" id="btn2">Add</button>
							</div>
                        </div>
                    </div>
                  </div>
                  <div class="row tab-pane fade" id="individual">
                    <div class="col-md-12">
                      <div role="form" class="form-inline" style="overflow:hidden;">
                        <div class="pull-left">
                          <label class="pad-s">Adult</label>
                          <div class="form-group">
                            <input class="form-control input-small" type="text" placeholder="Room Charge" id="arc" onblur="getARC(this.value)">
                          </div>
                          <div class="form-group">
                            <input class="form-control input-small" type="text" placeholder="Food Inclusions" id="afi" onblur="getAFI(this.value)">
                          </div>
                          <div class="form-group">
                            <select class="form-control" onchange="getChange(this.value)">
                              <option  value="" selected="selected">Plan</option>
                              <option value="AP">AP</option>
                              <option value="MAP">MAP</option>
                              <option value="EP">EP</option>
                              <option value="CP">CP</option>
                            </select>
                          </div>
                        </div>
                        <div class="pull-right">
                          <label class="pad-s">Kids</label>
                          <div class="form-group">
                            <input class="form-control input-small" type="text" placeholder="Room Charge" id="crc" onblur="getCRC(this.value)">
                          </div>
                          <div class="form-group">
                            <input class="form-control input-small" type="text" placeholder="Food Inclusions" id="cfi" onblur="getCFI(this.value)">
                          </div>
                          <div class="form-group">
                            <input class="form-control input-small" type="text" placeholder="Meal Plan" id="cmp">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="table-scrollable" > 
              <!-- <form type="post" target="<?php echo base_url(); ?>dashboard/add_group_booking" >-->
              <table class="table table-striped table-hover table-bordered mass-book" id="myTable">
                <tbody>
                  <tr>
                    <th> # </th>
                    <th> ID </th>
                    <th scope="col">Cloth Type</th>
					<th scope="col">Fabric Type</th>
					<th scope="col">Laundry Rate</th>
					<th scope="col">Dry clining Rate</th>
					<th scope="col">Ironing Rate</th>
					<th scope="col">Default Type</th>
					<th scope="col"> Action </th>
					
                    </tr>
                    <?php $i=0; $w=0; $total=0; 
					    if(isset($result) && $rates != ""){ 
					    foreach ($result as $key  ) { 
							
					?>
                    <tr>
                    <td><?php echo $key->lr_id; ?></td>                    
                    <td><?php echo $key->lr_cloth_type_id; ?></td>
                    <td><?php echo $key->lr_fabric_type_id; ?></td>
                    <td><?php echo $key->lr_laundry_rate; ?></td>
                    <input class="form-control input-sm" value="<?php echo $key->lr_id; ?>" name="" type="text" />
                    <td><?php echo $key->lr_ironing_rate; ?></td>
                    <td><?php echo $key->lr_default_type; ?></td>
                    <td></td>
                    
					
					<td>
                    	<table width="100%">
                        	<tr>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                  </tr>
                  <?php //$total=$total+ $key->price+ $key->service; 
				  
				  } }?>
                </tbody>
              </table>
            </div>
          </div>		  
          <div class="form-actions right" >
            <button class="btn btn-primary" id="editSubmit" type="submit">Save</button>
            <button class="btn btn-danger" id="editClose" type="button">Cancel</button>
          </div>
		  </div>
		  
		 </form> 
      </div>
    </div>
<!-- end edit modal --> 
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_pc?pc_id="+id,
                data:{},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){
					$('#row_'+id).remove();

                        });
                }
            });



        });
    }
	
	function gbEdit(){
	$("#gpSave").css("display", "none");
	$("#gpEdit").css("display", "block");
	//$(".edt").prop('disabled', false);
	//$(".edt").addClass('gbFocus');	
}
/*$(document).on('blur', '#ed', function () {
	$('#ed').addClass('focus');
});*/
$("#editClose").click(function(){
	$("#dpl").css("display", "none");
	$("#gpSave").css("display", "block");
	$("#gpEdit").css("display", "none");	
	//$(".edt").prop('disabled', true);
	//$(".edt").removeClass('gbFocus');
}); 
	function status(id){
		var hid=$('#hid').val();
		//alert(id);
		
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/profit_center_status",
                data:{id:id,status:hid},
                success:function(data)
                {
                   // alert(data);
                    //location.reload();
					document.getElementById("demostat"+id).innerHTML = data.data;
					//alert(id);
					$('#row_'+id).remove();
                    
					
                }
            });
		
	}
	
	
function check_sub(){
  document.getElementById('form_date').submit();
}	


function profit_center(){
		var a=document.getElementById("demo").innerHTML;
		if(a=="Active Profit Center"){
			//$("#status123").removeClass();
			// $("#status123").css('background-color',"blue");
			document.getElementById("demo").innerHTML ="Inactive Profit Center";			
			
		}else{			
			//$("#status123").removeClass();
			 //$("#status123").css('background-color',"green");
			document.getElementById("demo").innerHTML = "Active Profit Center";
		}
		
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/profit_center_filter",
                data:{status:a},
                 success:function(data)
                {
					//alert($data);
                  $('#table1').html(data);
				   $('#dataTable2').dataTable( {
						//"pageLength": 10 
    } );
	
                }
            });
		
	
	}
	function check(value){
		
		
			if(value==1){	
          $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/check_profit_center_default",
                data:{a:value},
                 success:function(data)
                {
					$('#responsive').modal('hide');
				swal({   title: "Are you sure?",
				text: data+"!",  
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Fix as Default!",   cancelButtonText: "No, cancel plz!", 
				closeOnConfirm: true,  
				closeOnCancel: true },
				function(isConfirm){   
				if (isConfirm) {     
				$("#default").val('1');$('#responsive').modal('show');
				} else {   
				$("#default").val('0');$('#responsive').modal('show');
				} });



				 
                }
            });
			}
		  
		  
		  //});
		
	}
	
	
	function check1(value){
		
		
			if(value==1){	
          $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/check_profit_center_default",
                data:{a:value},
                 success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                 
				$('#edit_profit').modal('hide');
				swal({   title: "Are you sure?",
				text: data+"!",  
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Fix as Default!",   cancelButtonText: "No, cancel plz!", 
				closeOnConfirm: true,  
				closeOnCancel: true },
				function(isConfirm){   
				if (isConfirm) {     
				$("#default1").val('1');$('#edit_profit').modal('show');
				} else {   
				$("#default1").val('0');$('#edit_profit').modal('show');
				} });



				 
                }
            });
			}
		  
		  
		  //});
		
	}
	
	function edit_profit(id){
		//alert(id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/get_edit_profit_center",
                data:{id:id},
                success:function(data)
                {
                    //alert(data.id);
				   $('#hid2').val(data.id);
                   $('#hid1').val(data.profit_center_location);
				   
				    $('#name1').val(data.profit_center_location);
				   $('#balance1').val(data.profit_center_balance);
				   $('#des1').val(data.profit_center_des);
				   $('#default1').val(data.profit_center_default);
				   
				   //$('#desc1').val(data.status);
				  // $('#unit_class1').val(data.change_date);
				  
				   
				   
                    $('#edit_profit').modal('toggle');
					
                }
            });
	}
	
	function reset_laundry_rates(){
		//alert('hello');
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/reset_laundry_rates",
                //data:{:id},
                success:function(data)
                {
				   $('#hid2').val(data.id);
                   $('#hid1').val(data.profit_center_location);
				   
				    $('#name1').val(data.profit_center_location);
				   $('#balance1').val(data.profit_center_balance);
				   $('#des1').val(data.profit_center_des);
				   $('#default1').val(data.profit_center_default);
				   
				   //$('#desc1').val(data.status);
				  // $('#unit_class1').val(data.change_date);
				  
				   
				   
                   
					
                }
            });
		
	}
	
	function abc(id,v,n){
		//alert(id+''+v+''+n);
	jQuery.ajax(
		{
			type: "POST",
			url: "<?php echo base_url(); ?>rate_plan/update_laundry_rate",
			data: {
				id:id,
				valu:v,
				field_name:n
			},
			success: function(data){
				//alert(data.status);
				//alert(data);
					
				
			} // end ajax success
		});

		
	}
	
</script> 