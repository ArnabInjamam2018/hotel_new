<?php if($this->session->flashdata('err_msg')):?>
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>All Laundry Service </div>
    <div class="actions">
    	<a href="<?php  echo base_url()?>unit_class_controller/add_laundry_service" class="btn btn-circle green btn-outline btn-sm" data-toggle="modal"><i class="fa fa-plus"></i>Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    
    <table class="table table-striped table-bordered table-hover" id="sample_3">
      <thead>
        <tr> 
         
          <th scope="col"> #</th>          <th scope="col"> Type</th>
          <th scope="col"> Guest Type </th>
		  <th scope="col"> Billing Preference</th>
          <th scope="col"> Receive Date </th>
          <th scope="col"> Delivery Date </th>
          <th scope="col"> Item Count </th>
          <th scope="col"> Job Status </th>
          <th scope="col"> Payment Status</th>
		   <th scope="col"> Total Amount Paid</th>
          <th scope="col"> Discount</th>
          <th scope="col"> CGST</th>
          <th scope="col"> SGST </th>
          <th scope="col"> IGST </th>		            <th scope="col"> UTGST </th>
          <th scope="col"> Grand Total </th>          
          <th scope="none">Special Request</th>
          <th scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>		
        <?php if(isset($laundry_service) && $laundry_service){
                        $i=1;						$cntr = 0;
                        foreach($laundry_service as $laundry){							$cntr++;
                            $class = ($i%2==0) ? "active" : "success";
                            ?>
        <tr id="row<?php echo $laundry->laundry_id; ?>">
				  <td><?php echo $cntr; ?></td>
          <td>
			<?php  //echo $laundry->laundry_booking_type;
				if($laundry->laundry_booking_type=='sb')
					{//echo "2";
						echo '<span style="color:#023358;"><i class="fa fa-user" aria-hidden="true"></i> Single Booking</span><br><span style="font-style:italic;"> - '.$laundry->booking_id.'</span>'; 
					}
				else if($laundry->laundry_booking_type=='gb')
					{//echo "1";
						echo '<span style="color:#F8681A;"><i class="fa fa-users" aria-hidden="true"></i> Group Booking</span><br><span style="font-style:italic;"> - '.$laundry->booking_id.'</span>'; 
					} 
				else if($laundry->laundry_booking_type=='n')
					{
						echo '<Span class="label" style="background-color:#3399ff; color:#ffffff;";> Ad Hoc </span>';;
					} 
			?>
		  </td>
          <td><?php echo $laundry->laundry_guest_name; ?></td>
		  <td>
			<?php 
				if($laundry->billing_preference == "Bill Seperately")
					echo '<Span class="label" style="background-color:#BEE0FF; color:#000000;";>'.$laundry->billing_preference.'</span>';
				else
					echo $laundry->billing_preference;
			?>
		  </td>
		  
          <td><?php echo $laundry->receiv_date;?></td>
          <td><?php echo $laundry->delivery_date;?></td>
          <td><?php   echo $laundry->total_item; ?></td>
								
								
          
          <td><?php echo $laundry->job_status ;?></td>
          <td><?php echo $laundry-> payment_status;?></td>
		  <td><?php echo $laundry-> total_amount;?></td>
          <td><?php echo $laundry->discount ;?></td>
          <td><?php echo $laundry->vat;?></td>
          <td><?php echo $laundry->service_tax;?></td>
          <td><?php echo $laundry->service_charge;?></td>		            <td><?php echo '0';?></td>
		  
          <td><?php echo '<i class="fa fa-inr" aria-hidden="true"></i> '.$laundry->grand_total;?></td>
          
          <td><?php echo $laundry->laundry_special_request;?></td>
          
          <td class="ba">
            <div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $laundry->laundry_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
                <li><a href="<?php echo base_url() ?>unit_class_controller/edit_laundry_service?id=<?php echo $laundry->laundry_id; ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a></li>
				<li><a href="<?php echo base_url() ?>unit_class_controller/get_laundry_pdf?id=<?php echo $laundry->laundry_id; ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-cloud-download"></i></a></li>
              </ul>
            </div>
          </td>
        </tr>
		<?php }} ?>
       
      </tbody>
    </table>
  </div>
</div>
<script>

$(document).on('blur', '#ced', function () {
	$('#ced').addClass('focus');
});
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/delete_laundry_service",
                data:{id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            $('#row'+id).remove();

                        });
                }
            });
        });
    }
</script>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('dashboard/add_corporate',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Corporate</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group form-md-line-input ">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="name" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Name *">
                <label></label>
                <div class="highlight"></div>
                <span class="help-block">Name *</span> </div>
            </div >
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="mobile" name="g_contact_no"  required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" placeholder="Mobile No">
                <label></label>
                <span class="help-block">Mobile No</span>
                <div class="highlight"></div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="industry" placeholder="Industry*">
                <label></label>
                <span class="help-block">Industry</span>
                <div class="highlight"></div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="no_of_emp" placeholder="No of employees">
                <label></label>
                <span class="help-block">No of employees</span>
                <div class="highlight"></div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="contract_discount" placeholder="Contract discount">
                <label></label>
                <span class="help-block">Contract discount</span>
                <div class="highlight"></div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control date-picker" id="ced" name="contract_end_date" placeholder="Contract end date">
                <label></label>
                <span class="help-block">Contract end date</span> </div>
            </div>
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="address" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Address">
                <label></label>
                <span class="help-block">Address</span>
                <div class="highlight"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<div id="edit_corporate" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('unit_class_controller/update_corporate',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Corporate</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group form-md-line-input ">
                <input type="hidden" name="hid" id="hid">
                <input autocomplete="off" type="text" class="form-control" id="name1" name="name1" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Name *">
                <label></label>
                <div class="highlight"></div>
                <span class="help-block">Name *</span> </div>
            </div >
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="mobile1" name="g_contact_no1"  required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" placeholder="Mobile No">
                <label></label>
                <span class="help-block">Mobile No</span>
                <div class="highlight"></div>
              </div>
            </div>
            <!--<div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control" id="industry1" name="industry1" placeholder="Industry*">
                <label></label>
                <span class="help-block">Industry</span>
                <div class="highlight"></div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control" id="no_of_emp1" name="no_of_emp1" placeholder="No of employees">
                <label></label>
                <span class="help-block">No of employees</span>
                <div class="highlight"></div>
              </div>
            </div>-->
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control" id="contract_discount1" name="contract_discount1" placeholder="Contract discount">
                <label></label>
                <span class="help-block">Contract discount</span>
                <div class="highlight"></div>
              </div>
            </div>
			
			<?php 
		$industry=$this->unit_class_model->all_industry_type();
		?>
		<div class="col-md-6">
			<div class="form-group form-md-line-input">
              <select class="form-control bs-select" name="industry_type" id="industry_type" required >
			  <option value="0" >Select</option>
			  <?php

			  if(isset($industry) && $industry){
				  foreach($industry as $ind){			  
			  
			  ?>
                <option value="<?php echo $ind->it_name ?>" ><?php echo $ind->it_name ?></option>
			  <?php }} ?>
              </select>
              <label></label>
              <span class="help-block">Industry type</span>
			</div>
			</div>
			
			
			 <div class="col-md-6"> 
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="field" name="field" onkeypress=" return onlyNos(event, this);" required="required" placeholder="Field of work">
              <label></label>
              <span class="help-block">Field of work</span>
            </div> 
        </div>
		
		<div class="col-md-6">
			<div class="form-group form-md-line-input">
              <select class="form-control bs-select" name="rating" id="rating" required >
			  <option value="0" >Select Rating</option>			  
                <option value="1" >1</option>
                <option value="2" >2</option>
                <option value="3" >3</option>
                <option value="4" >4</option>
                <option value="5" >5</option>			  
              </select>
              <label></label>
              <span class="help-block">Rating</span>
			</div>
			</div>
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input  autocomplete="off" type="text" class="form-control date-picker" id="ced1" name="contract_end_date1" placeholder="Contract end date">
                <label></label>
                <span class="help-block">Contract end date</span> </div>
            </div>
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="address1" name="address1" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Address">
                <label></label>
                <span class="help-block">Address</span>
                <div class="highlight"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<script>
   function check_corporate(value){

       if(value =="corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
       }else{
           document.getElementById("c_name").style.display="none";
           document.getElementById("c_des").style.display="none";
       }
   }
    function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
</script> 
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}
$(document).on('blur', '#ced', function () {
	$('#ced').addClass('focus');
});


function edit_corporate(id){
		//alert(id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/edit_corporate",
                data:{id:id},
                success:function(data)
                {
                    
					
                   $('#hid').val(data.hotel_corporate_id);
				   $('#name1').val(data.name);
				   $('#address1').val(data.address);
				   $('#mobile1').val(data.g_contact_no);
				   $('#industry1').val(data.industry);
				   $('#no_of_emp1').val(data.no_of_emp);
				   $('#contract_discount1').val(data.contract_discount);
				   $('#ced1').val(data.contract_end_date);
				   $('#no_of_booking').val('0');
				   
				   $('#industry_type').val(data.industry_type).change();
				   $('#field').val(data.field);
				   $('#rating').val(data.rating).change();
				   //$('#total_transaction').val(data.design);
				   		   
				   
				   
                    $('#edit_corporate').modal('toggle');
					
                }
            });
	}
</script>