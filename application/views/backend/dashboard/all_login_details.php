    <div class="portlet light borderd">
      <div class="portlet-title">
        <div class="caption" style="color:#F04646;"> <i class="fa fa-th-list"></i> <strong> User Login Report</strong> </div>
        <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            
            <div class="col-md-8">
              <?php

	                            $form = array(
	                                'class'       => 'form-inline ftop',
	                                'id'        => 'form_date',
	                                'method'      => 'post'
	                            );

	                            echo form_open_multipart('dashboard/get_booking_report_by_date',$form);

	                            ?>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
              </div>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
              </div>
			  
			  <div class="form-group">
              <button onclick="get_val();" class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search" aria-hidden="true"></i></button>
			  </div>
			 
                  
              <?php form_close(); ?>
            </div>
            <script type="text/javascript">
				function check_sub(){
				   document.getElementById('form_date').submit();
				}
			</script>
            
          </div>
        </div>
		
			      
				   
	<div id="table1">		
       <table class="table table-striped table-bordered table-hover" id="sample_1">
        <!--<table class="table table-striped">-->
          <thead>
            <tr>
              
			  <!--<th> Select </th>-->
              
              
              <th> # </th>
              <th> User Name  </th>
			  <th> Status </th>
              <th> Login Time </th>
              <th> Logout Time  </th>              
              <th> Cash Drawer  </th>              
              <th width="8%"> Browser Info  </th>              
              <th> IP Address  </th>              
              
             
            </tr>
          </thead>
          <tbody>
            <?php $i=0;
			
				if(isset($login_details) && $login_details){
					$i=0;
						
                    foreach($login_details as $login){
						$i++;
						
						$stl = '';
						if(isset($_SESSION['mach_id']) & $_SESSION['mach_id'] == $login->hls_id) {
							$stl = 'style="background-color:#E9FFD5;"';
						}
						
			?>
         
									
          <tr <?php echo $stl;?>>
            <td>  <?php 
					echo $i;
				  ?>
			 </td>
			<td>  <?php 
			$name = $this->unit_class_model->userName_details($login->user_id);
			if(isset($name) && $name){
				$f1=$name->admin_first_name;
			
			if(isset($f1) && $f1){
				$f1;
			}
			else{
				$f1='';
			}
			
			$f2=$name->admin_middle_name;
			if(isset($f2) && $f2){
				$f2;
			}
			else{
				$f2='';
			}
			if(isset($f3) && $f3){
				$f3;
			}
			else{
				$f3='';
			}
			 $f3=$name->admin_last_name;
			 
			}
			else
				$f1=$f2=$f3 = '';

					echo $f1.$f2.' '.$f3;
					echo '</br>('.$login->hls_id.')';
					//echo '</br>'.$this->session->userdata('login_session_id');
				  ?>
			</td>
			
			<td>
				<?php 
				
					$datetime1 = new DateTime('now');
					$datetime2 = new DateTime($login->lastUse);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%H') + $interval->format('%i')/60;
					
					if($login->status == 'Success' && $diff <= 0.8){
						$co='#36926A';
						$stat = 'Logged in';
					}						
					else if($login->status=='Success' && $diff > 0.8 && $diff < 2){
						$co='#EABE56';
						$stat = 'Away';
					}
					else if($login->status == 'Success'){
						$co='#833473';
						$stat = 'Idle';
					}						
					else if($login->status == 'Logout'){
						$co='#256F94';	
						$stat = 'Logout';						
					}
					else{
						$co='#DC2158';	
						$stat = 'Fail';
					}
					
					?>
				<span class="label" style="background-color:<?php echo $co ?>; color:<?php echo 'white' ?>;">

				  <?php 
						echo $stat;
				  ?>
			  </span>
			</td>
			
			<td>
				<?php 
					echo date("g:i A \-\n l jS F Y",strtotime($login->login_dateTime)).'</br>';
					if($login->lastUse > 0)
						echo '('.date("g:i A \-\n l jS F Y",strtotime($login->lastUse)).')';					
				?>
			</td>
			<td>
				  <?php 
				  
					$datetime11 = new DateTime($login->login_dateTime);
					$datetime21 = new DateTime($login->logout_dateTime);
					$interval1 = $datetime11->diff($datetime21);
					$diff1 = $interval1->format('%H').' Hr '.$interval1->format('%i').' min';
					
					if($login->autoLogout == 1)
						$al = '<span style="color:#40379F"> Auto Logout</span>';
					else
						$al = '';
					
					if($login->dubLogout == 1)
						$fl = '<span style="color:#40379F"> Forced Logout</span>';
					else
						$fl = '';						
						
					
					if($login->logout_dateTime > 0)
						echo date("g:i A \-\n l jS F Y",strtotime($login->logout_dateTime)).' </br>'.$diff1.$al.$fl;
					else
						echo '<span style="color:#C0C0C0">Session is active...</span>';
				  ?>
			  
			</td>
			
			<td>
			<?php 
			$cashdrawer_name=$this->unit_class_model->getCashdrawerName($login->cashdrawer);
					if(isset($cashdrawer_name) && $cashdrawer_name){
					echo $cashdrawer_name->cashDrawerName;
					}else{
						echo 'N/A';
					}
				  ?>
			</td>
			
			<td>
				<?php 
					echo $login->browser_info;
				  ?>
			  
			</td>
			<td>
				<?php 
					echo $login->ip_address;
					
					/*ini_set("allow_url_fopen", 1);
					$url = 'http://freegeoip.net/json/'.$login->ip_address;
					echo $url;
					$json = file_get_contents($url);
					$obj = json_decode($json);
					echo $obj;*/
					
					
				  ?>
			  
			  
			  
			</td>

			
            
           <!-- <td align="center" class="ba">
            	<div class="btn-group">
                  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    
					
					
                    <li><a href="<?php //echo base_url();?>dashboard/booking_edit?b_id=<?php 
					//echo $login->hls_id ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a>
					</li>
					
                    
					
                    
                  </ul>
                </div>
			  </td>-->
          </tr>
		   <input type="hidden" id="item_no" value="<?php //echo $item_no;?>"> </input>
		   
			<?php $i++; ?>
          
          <?php  
			}}
		  ?>
         
            </tbody>
          
        </table>
      
    </div>
    </div>
    </div>

<script>

   function fetch_data(val){
	   alert()
	   $.ajax({
                
                url: "<?php echo base_url()?>dashboard/get_no_tax_booking",
				success:function(data)
                { 
                   $('#table1').html(data);
				   $('#dataTable2').dataTable( {
						"pageLength": 10 
					} );
                 }
            });
   }
   
					
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            //alert(id);
		$.ajax({
                
                url: "<?php echo base_url()?>dashboard/soft_delete_booking?id="+id,
				type:"POST",
                data:{id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });

        });
    }
	
function download_pdf(booking_id) {
	
	
	
	//alert(booking_id);
	$("#dwn_link").attr("href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id);
	var f = $("#f");
            $.post(f.attr("action"), f.serialize(), function (result) {
            
                close(eval(result));
            });
	 //return false;
	
}
	
	
</script> 

<script>

$( document ).ready(function() {
	$("#select2_sample_modal_5").select2({
		
	/*	data: [
		 <?php $admins = $this->dashboard_model->fetch_admin();
					foreach($admins as $adm){?>
    {
      id: '<?php echo $adm->admin_id; ?>',
      text: '<?php echo $adm->admin_first_name." ".$adm->admin_last_name; ?>'
    },
	<?php } ?>
	]*/
		 
          /* tags: [ <?php $admins = $this->dashboard_model->fetch_admin();
					foreach($admins as $adm)
					{echo '"'.$adm->admin_id.'",';}?>]*/
        });
});

function get_val()
{
	$("#admin").val($("#select2_sample_modal_5").val());
	//alert($("#admin").val());
}
</script>
<!-- END CONTENT --> 
