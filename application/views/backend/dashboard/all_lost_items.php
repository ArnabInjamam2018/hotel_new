<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <strong><i class="fa fa-eye-slash" aria-hidden="true"></i></strong> List of All Lost Items </div>
    <div class="actions">
    	<a href="<?php echo base_url();?>dashboard/add_lost_item" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <!-- <th scope="col">
                            Select
                        </th>-->
          <th scope="col"> Reporting Date </th>
          <th scope="col"> Status </th>
          <th scope="col"> Booking Id </th>
          <th scope="col"> Type </th>
          <th scope="col"> Item Title </th>
          <th scope="col"> Item Description </th>
          <th scope="col"> Condition </th>
          <th scope="col"> Lost In </th>
          <th scope="col"> Lost Date </th>
          <th scope="col"> Lost Time </th>
          <th scope="col" class="none"> Lost By </th>
          <th scope="col" class="none"> Contact No </th>
          <th scope="col" class="none"> Admin Name </th>          
          <th scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($lost_item) && $lost_item):
                        
                        $i=1;
                        foreach($lost_item as $items):
                            $class = ($i%2==0) ? "active" : "success";
                            $l_id=$items->l_id;
                            ?>
        <tr> 
          <!-- <td width="50">
                                    <div class="md-checkbox pull-left">
                                        <input type="checkbox" id="checkbox1" class="md-check">
                                        <label for="checkbox1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                        </label>
                                    </div>
                                </td>--> 
          
          <!-- <td>
                               
                                // <img  width="60px" height="60px" src="<?php echo base_url();?>upload/guest/<?php //if( $gst->g_photo_thumb== '') { echo "no_images.png"; } else { echo $gst->g_photo_thumb; }?>" alt=""/>
                                
                                
                                <?php
                                    /*echo $gst->g_photo ;
                                    echo $gst->g_photo ."_thumb"; */
                                ?>
                                
                                </td> -->
          
          <td><?php echo $items->reporting_date; ?></td>
          <td><?php if($items->status=='lost'){echo "<span class='label label-sm label-danger' style='text-transform:uppercase;'> ".$items->status."</span>";}
          else
          {echo "<span class='label label-sm label-success' style='text-transform:uppercase;'> ".$items->status."</span>";}
          ?></td>
          <td><?php echo $items->booking_id; ?></td>
          <td><?php echo $items->type;?></td>
          <td><?php
                                   echo $items->item_title;
                                ?></td>
          <td><?php echo $items->item_des ;?></td>
          <td><?php echo $items->condition;?></td>
          <td><?php echo $items->lost_in;?></td>
          <td><?php echo $items->lost_date;?></td>
          <td><?php echo $items->lost_time;?></td>
          <td><?php echo $items->lost_by;?></td>
          <td><?php echo $items->g_contact_no;?></td>
          <td><?php echo $items->admin_name;?></td>
          
          <!--  <td>
                                    <?php 
                                    
                                    // $now = time();
                                    // $date1 = strtotime($gst->g_dob);
                                    // $datediff = $now-$date1;
                                    // $datediff = floor($datediff/(60*60*24*365));
                                    // echo $datediff;
                                    
                                        //$var = $gst->g_dob;
                                        //echo $var;
                                        /*$var = date_create($gst->g_dob);
                                        echo $var;
                                        $d = date("Y/m/d");
                                        //echo $d;
                                        $current_date=date_create($d);
                                        //echo $current_date;
                                        $diff=date_diff($current_date, $var);
                                        
                                        echo $diff;*/
                                    
                                    ?>
                                </td> --> 
          
          <!-- <td>
                                    // <?php //$m= $gst->g_id_type;
                                    // if($m==1){
                                    //     echo "Passport";
                                    // }
                                    // elseif($m==2){ echo "PAN Card";}
                                    // elseif($m==3){echo "Voter Card";}
                                    // elseif($m==4){echo "Adhar Card";}
                                    // elseif($m==5){echo "Driving License";}
                                    // elseif($m==6){echo "Others";}


                                    ?>
                                </td>-->
          
          <?php //echo $gst->g_photo ?>
          
          <!--<td>
                                    <img  width="100%" src="<?php //echo base_url();?>upload/<?php //if( $gst->g_id_proof== '') { echo "no_images.png"; } else { echo $gst->g_id_proof; }?>" alt=""/>
                                    <?php //echo $gst->g_id_proof ?>
                                </td>-->
          
          <td class="ba">
            <div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $items->l_id;?>')" data-toggle="modal"  class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
                <li><a href="<?php echo base_url() ?>dashboard/fetch_item_by_l_id?l_id=<?php echo $items->l_id;?>" class="btn  blue btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li>
              </ul>
            </div>
          </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_lost_item?l_id="+id,
                data:{a_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
</script> 
