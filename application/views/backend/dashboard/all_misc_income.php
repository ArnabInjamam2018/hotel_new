<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
	<div class="caption"> <i class="fa fa-edit"></i>List of All Miscellenious Income </div>
	<div class="actions">
		<a href="<?php echo base_url();?>dashboard/add_misc_income" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
	</div>
  </div>
  <div class="portlet-body">
	
	<table class="table table-striped table-bordered table-hover" id="sample_1">
	  <thead>
		<tr> 
		  <!-- <th scope="col">
							Select
						</th>-->
		  <th scope="col"> Sl </th>
		  <th scope="col"> Date </th>
		  <th scope="col"> Recived From </th>
		  <th scope="col"> Item </th>
		  <th scope="col"> Note </th>
		  <th scope="col"> Recived By </th>
		  <th scope="col"> Profit Center</th>
		  <th scope="col"> Mode Of Payment</th>
		  
		  <th scope="col"> Tax % </th>
		  <th scope="col"> Amount </th>
		  <th scope="col"> Total </th>
		  <th scope="col"> Admin </th>
		 
		  <th scope="col"> Action </th>
		</tr>
	  </thead>
	  <tbody>
		<?php if(isset($m_income) && $m_income):
						//echo "<pre>";	
						//print_r($m_income);
						
						$sl = 0;	
						$i = 1;
						foreach($m_income as $value):
							$sl++;
							$class = ($i%2==0) ? "active" : "success";
						   // $c_id=$value->hotel_corporate_id;
							?>
		<tr> 
		  
							   
							   
								
							 
		  <td><?php echo $sl; ?></td>
		  <td><?php echo $value->date; ?></td>
		  <td><?php echo $value->recived_from; ?></td>
		  <td><?php echo $value->item;?></td>
		  <td><?php echo $value->note;?></td>
		  <td><?php echo $value->recived_by; ?></td>
		  <td><?php echo $value->profit_center; ?></td>
		  <td><?php echo $value->mode_of_payment;;?></td>
		  
		  <td><?php echo $value->tax;?></td>
		  <td><?php echo $value->amount ;?></td>
		  <td><?php echo $value->total ;?></td>
		  <td><?php echo $value->admin ;?></td>
		  
		  <td class="ba">
          	<div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $value->hotel_misc_income_id;?>')" data-toggle="modal"  class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
                <li><a href="<?php echo base_url() ?>dashboard/edit_misc_income?m_id=<?php echo $value->hotel_misc_income_id;?>" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li>
              </ul>
            </div>
          </td>
		</tr>
		<?php endforeach; ?>
		<?php endif; ?>
	  </tbody>
	</table>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/common/js/table_sort_style.js"></script>
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_misc_income?m_id="+id,
                data:{m_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
	
</script> 
