

    <div class="portlet light borderd">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-th-list"></i> <strong> ALL AUDIT LOG</strong> </div>
        <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            
            <div class="col-md-8">
              <?php

	                            $form = array(
	                                'class'       => 'form-inline ftop',
	                                'id'        => 'form_date',
	                                'method'      => 'post'
	                            );

	                            echo form_open_multipart('dashboard/get_booking_report_by_date',$form);

	                            ?>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
              </div>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
              </div>
			  
			  <div class="form-group">
              <button onclick="get_val();" class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search" aria-hidden="true"></i></button>
			  </div>
			 
                  
              <?php form_close(); ?>
            </div>
            <script type="text/javascript">
				function check_sub(){
				   document.getElementById('form_date').submit();
				}
			</script>
            
          </div>
        </div>
		
			      
				   
	<div id="table1">		
       <table class="table table-striped table-bordered table-hover" id="sample_1">
        <!--<table class="table table-striped">-->
          <thead>
            <tr>
              
			  <!--<th> Select </th>-->
              
              <th> User Name  </th>
              <th> Audit Date  </th>
              <th> Last Audit Date  </th>              
              <th> Total Unit</th>
              <th> Total Guest </th>
              <th> Single Booking</th>
              <th> Group Booking </th>
              <th> Checkin</th>
              <th> Checkout</th>
              <th> Advance</th>
              <th> Confirm</th>
              <th> Cancel</th>
              <th> Total Guest Staying</th>
              
              <th width="5%" scope="col" class="action"> Options </th>
            </tr>
          </thead>
          <tbody>
            <?php $i=0;
			
			if(isset($nightAudit) && $nightAudit){
					//echo "<pre>";
						//print_r($nightAudit); exit;
						
                        foreach($nightAudit as $audit){
							
						
		?>
         
									
          <tr>
            <td>  <?php 
					echo $audit->user_id;
				  ?>
			 </td>
			<td>
				<?php 
					echo $audit->nadt_m_date;
				  ?>
			  
			</td>
			<td>
				<?php 
					echo $audit->nadt_m_last_audit_date;
				  ?>
			  
			</td>
            <td>
				<?php 
					echo $audit->nadt_m_total_unit;
				  ?>
			  
			</td>
            <td align="center">
				<?php 
					echo $audit->nadt_m_total_guest;
				  ?>
			  
			</td>
            	<td align="center">
				<?php 
					echo $audit->nadt_m_count_s_booking;
				  ?>
			  
			</td>	
			<td align="center">
				<?php 
					echo $audit->nadt_m_count_g_booking;
				  ?>
			  
			</td>
			<td align="center">
				<?php 
					echo $audit->nadt_m_count_checkin;
				  ?>
			  
			</td>
			<td align="center">
				<?php 
					echo $audit->nadt_m_count_checkout;
				  ?>
			  
			</td>
			<td align="center">
				<?php 
					echo $audit->nadt_m_count_advance;
				  ?>
			  
			</td>


			<td align="center">
				<?php 
					echo $audit->nadt_m_count_confirm;
				  ?>
			  
			</td>
			<td align="center">
				<?php 
					echo $audit->nadt_m_count_cancel;
				  ?>
			  
			</td>
			<td align="center">
				<?php 
					echo $audit->nadt_m_count_guest_staying;
				  ?>
			  
			</td>
            <td align="center" class="ba">
            	<div class="btn-group">
                  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    
					
					
                    <li><a href="#" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a>
					</li>
					
                    
					
                    
                  </ul>
                </div>
			  </td>
          </tr>
		   <input type="hidden" id="item_no" value="<?php //echo $item_no;?>"> </input>
		   
			<?php $i++; ?>
          
          <?php  
			}}
		  ?>
         
            </tbody>
          
        </table>
      
    </div>
    </div>
    </div>

<script>

   function fetch_data(val){
	   alert()
	   $.ajax({
                
                url: "<?php echo base_url()?>dashboard/get_no_tax_booking",
				success:function(data)
                { 
                   $('#table1').html(data);
				   $('#dataTable2').dataTable( {
						"pageLength": 10 
					} );
                 }
            });
   }
   
					
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            //alert(id);
		$.ajax({
                
                url: "<?php echo base_url()?>dashboard/soft_delete_booking?id="+id,
				type:"POST",
                data:{id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });

        });
    }
	
function download_pdf(booking_id) {
	
	
	
	//alert(booking_id);
	$("#dwn_link").attr("href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id);
	var f = $("#f");
            $.post(f.attr("action"), f.serialize(), function (result) {
            
                close(eval(result));
            });
	 //return false;
	
}
	
	
</script> 

<script>

$( document ).ready(function() {
	$("#select2_sample_modal_5").select2({
		
	/*	data: [
		 <?php $admins = $this->dashboard_model->fetch_admin();
					foreach($admins as $adm){?>
    {
      id: '<?php echo $adm->admin_id; ?>',
      text: '<?php echo $adm->admin_first_name." ".$adm->admin_last_name; ?>'
    },
	<?php } ?>
	]*/
		 
          /* tags: [ <?php $admins = $this->dashboard_model->fetch_admin();
					foreach($admins as $adm)
					{echo '"'.$adm->admin_id.'",';}?>]*/
        });
});

function get_val()
{
	$("#admin").val($("#select2_sample_modal_5").val());
	//alert($("#admin").val());
}
</script>
<!-- END CONTENT --> 
