<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
<?php endif;?>
<div class="portlet light borderd">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-edit"></i>
			<?php if(isset($stat) && $stat){echo $stat;}?>
		</div>
		<div class="actions">
			<a class="btn btn-circle green btn-outline btn-sm" data-toggle="modal" href="#responsive"> <i class="fa fa-plus"></i>Add Profit Center </a>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-toolbar">
			<div class="row">
				<div class="col-md-6">
					<div class="btn-group ">
						<button class="btn green" id="status123" onclick="profit_center()"> <span id="demo">Active Profit Center</span> </button>
					</div>
				</div>
			</div>
		</div>
		<div id="table1">
			<table class="table table-striped table-bordered table-hover" id="sample_1">
				<thead>
					<tr>
						<th scope="col">Profit Center Location </th>
						<th scope="col">Profit Center Balance </th>
						<th scope="col"> Status </th>
						<th scope="col"> Action </th>
					</tr>
				</thead>
				<tbody>
					<?php if(isset($pc) && $pc):
                    //print_r($pc);//exit;    
                        $i=1;
                        
                        foreach($pc as $profit_center):
                            $class = ($i%2==0) ? "active" : "success";
                            //$r_id=$items->l_id;
                            ?>
					<tr id="row_<?php echo $profit_center->id;?>">
						<td>
							<?php echo $profit_center->profit_center_location;
          if($profit_center->profit_center_default =='1'){?>
							<font color="#22FF11"> <i class="fa fa-check" aria-hidden="true"></i>
							</font>
							<?php  }
              ?>
						</td>
						<td>
							<?php echo $profit_center->profit_center_balance;?>
						</td>
						<td> <input type="hidden" id="hid" value="<?php echo $profit_center->status;?>">
							<button <?php if($profit_center->profit_center_default =='1'){
                echo "disabled"; }?> class="btn green btn-xs"  value="<?php echo $profit_center->id;?>" onclick="status(this.value)"> <span id="demostat<?php echo $profit_center->id;?>">
              <?php if($profit_center->status == 1){ echo  "Active";}
else{
echo "Inactive";
}	?>
              </span> </button>
						</td>
						<td class="ba">
							<div class="btn-group">
								<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li><a onclick="soft_delete('<?php echo $profit_center->id;?>')" data-toggle="modal" <?php if($profit_center->profit_center_default =='1'){ echo " disabled"; }?> class="btn red btn-xs"><i class="fa fa-trash"></i></a>
									</li>
									<li><a onclick="edit_profit('<?php echo $profit_center->id; ?>')" data-toggle="modal" class="btn green btn-xs"> <i class="fa fa-edit"></i></a>
									</li>
								</ul>
							</div>

						</td>
					</tr>
					<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add Profit Center</h4>
			</div>
			<?php

			$form = array(
				'class' => 'form-body',
				'id' => 'form',
				'method' => 'post'
			);

			echo form_open_multipart( 'dashboard/add_profit_center', $form );

			?>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input type="text" class="form-control" name="pc_location" id="pc_location" required placeholder="Nature of visit Name">
							<label></label>
							<span class="help-block">Profit Center Location..</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="balance" name="balance" placeholder="Enter Balance  ..." maxlength="10" onkeypress=" return onlyNos(event, this);">
							<label></label>
							<span class="help-block">Enter Balance *</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<select onchange="check(this.value)" class="form-control bs-select" id="default" name="default" required>
								<option value="" selected="selected" disabled="disabled">Default nature visit</option>
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
							<label></label>
							<span class="help-block">Default nature visit *</span> </div>
					</div>
					<div class="col-md-12">
						<div class="form-group form-md-line-input">
							<textarea class="form-control" row="3" name="des" placeholder="Description" id="des"></textarea>
							<label></label>
							<span class="help-block">Enter Description...</span> </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn default">Close</button>
				<button type="submit" class="btn green">Save</button>
			</div>
			<?php echo form_close(); ?> </div>
	</div>
</div>
<!--edit modal -->

<div id="edit_profit" class="modal fade" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit Profit Center</h4>
			</div>
			<?php

			$form = array(
				'class' => 'form-body',
				'id' => 'form',
				'method' => 'post'
			);

			echo form_open_multipart( 'dashboard/edit_profit_center', $form );

			?>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input type="text" class="form-control" name="name1" id="name1" required placeholder="Nature of visit Name">
							<label></label>
							<span class="help-block">Profit Center Location..</span> </div>
					</div>
					<input type="hidden" name="hid1" id="hid1">
					<input type="hidden" name="hid2" id="hid2">
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="balance1" name="balance1" placeholder="Enter Balance  ..." maxlength="10" onkeypress=" return onlyNos(event, this);">
							<label></label>
							<span class="help-block">Enter Balance  ...</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<select onchange="check1(this.value)" class="form-control bs-select" id="default1" name="default1" required>
								<option value="" selected="selected" disabled="disabled">Default nature visit</option>
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
							<label></label>
							<span class="help-block">Default nature visit *</span> </div>
					</div>
					<div class="col-md-12">
						<div class="form-group form-md-line-input">
							<textarea class="form-control" row="3" name="des1" placeholder="Description" id="des1"></textarea>
							<label></label>
							<span class="help-block">Enter Description...</span> </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn default">Close</button>
				<button type="submit" class="btn green">Save</button>
			</div>
			<?php echo form_close(); ?> </div>
	</div>
</div>

<!-- end edit modal --> 
<script>
	function soft_delete( id ) {
		swal( {
			title: "Are you sure?",
			text: "All the releted transactions and data will be deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {





			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/delete_pc?pc_id=" + id,
				data: {},
				success: function ( data ) {
					//alert("Checked-In Successfully");
					//location.reload();
					swal( {
							title: data.data,
							text: "",
							type: "success"
						},
						function () {
							$( '#row_' + id ).remove();

						} );
				}
			} );



		} );
	}


	function status( id ) {
		var hid = $( '#hid' ).val();
		//alert(id);


		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>unit_class_controller/profit_center_status",
			data: {
				id: id,
				status: hid
			},
			success: function ( data ) {
				// alert(data);
				//location.reload();
				document.getElementById( "demostat" + id ).innerHTML = data.data;
				//alert(id);
				$( '#row_' + id ).remove();


			}
		} );

	}


	function check_sub() {
		document.getElementById( 'form_date' ).submit();
	}


	function profit_center() {
		var a = document.getElementById( "demo" ).innerHTML;
		if ( a == "Active Profit Center" ) {
			//$("#status123").removeClass();
			// $("#status123").css('background-color',"blue");
			document.getElementById( "demo" ).innerHTML = "Inactive Profit Center";

		} else {
			//$("#status123").removeClass();
			//$("#status123").css('background-color',"green");
			document.getElementById( "demo" ).innerHTML = "Active Profit Center";
		}


		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>unit_class_controller/profit_center_filter",
			data: {
				status: a
			},
			success: function ( data ) {
				//alert($data);
				$( '#table1' ).html( data );
				$.getScript( '<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons-ex.min.js' );
				$.getScript( '<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js' );

			}
		} );


	}

	function check( value ) {


		if ( value == 1 ) {
			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/check_profit_center_default",
				data: {
					a: value
				},
				success: function ( data ) {
					//alert(data);
					//alert("Checked-In Successfully");
					//location.reload();

					$( '#responsive' ).modal( 'hide' );
					swal( {
							title: "Are you sure?",
							text: data + "!",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Fix as Default!",
							cancelButtonText: "No, cancel plz!",
							closeOnConfirm: true,
							closeOnCancel: true
						},
						function ( isConfirm ) {
							if ( isConfirm ) {
								$( "#default" ).val( '1' );
								$( '#responsive' ).modal( 'show' );
							} else {
								$( "#default" ).val( '0' );
								$( '#responsive' ).modal( 'show' );
							}
						} );




				}
			} );
		}


		//});

	}


	function check1( value ) {


		if ( value == 1 ) {
			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/check_profit_center_default",
				data: {
					a: value
				},
				success: function ( data ) {
					//alert(data);
					//alert("Checked-In Successfully");
					//location.reload();

					$( '#edit_profit' ).modal( 'hide' );
					swal( {
							title: "Are you sure?",
							text: data + "!",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Fix as Default!",
							cancelButtonText: "No, cancel plz!",
							closeOnConfirm: true,
							closeOnCancel: true
						},
						function ( isConfirm ) {
							if ( isConfirm ) {
								$( "#default1" ).val( '1' );
								$( '#edit_profit' ).modal( 'show' );
							} else {
								$( "#default1" ).val( '0' );
								$( '#edit_profit' ).modal( 'show' );
							}
						} );




				}
			} );
		}


		//});

	}

	function edit_profit( id ) {
		//alert(id);

		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>unit_class_controller/get_edit_profit_center",
			data: {
				id: id
			},
			success: function ( data ) {
				//alert(data.id);
				$( '#hid2' ).val( data.id );
				$( '#hid1' ).val( data.profit_center_location );

				$( '#name1' ).val( data.profit_center_location );
				$( '#balance1' ).val( data.profit_center_balance );
				$( '#des1' ).val( data.profit_center_des );
				$( '#default1' ).val( data.profit_center_default );

				//$('#desc1').val(data.status);
				// $('#unit_class1').val(data.change_date);



				$( '#edit_profit' ).modal( 'toggle' );

			}
		} );
	}
</script>