<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
	<div class="caption"> <i class="fa fa-edit"></i>List of All Purchases </div>
	<div class="tools">
		<a href="<?php echo base_url();?>dashboard/add_purchase" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
	</div>
  </div>
  <div class="portlet-body">
	<table class="table table-striped table-bordered table-hover" id="sample_1">
	  <thead>
		<tr> 
		  <!--<th scope="col">
						Select
					</th>-->
		  <th scope="col"> Photo </th>
		  <th scope="col"> Channel Name </th>
		  <th scope="col"> Contact Name </th>
		  <th scope="col"> Address </th>
		  <th scope="col" class="none"> Contact No </th>
		  <th scope="col" class="none"> Website </th>
		  <th scope="col" class="none"> Channel Commision </th>
		  <!--<th scope="col">
						Broker Address
					</th>
					<th scope="col">
						Broker Contact
					</th>
					<th scope="col">
						Broker Email
					</th>
					<th scope="col">
						Broker Website
					</th>

					<th scope="col">
						Broker Pan Id
					</th>
					<th scope="col">
						Broker Bank Account
					</th>
					<th scope="col">
						Broker Bank IFSC Code
					</th>
					<th scope="col">
						Broker Photo
					</th>-->
		  <th scope="col"> Action </th>
		</tr>
	  </thead>
	  <tbody>
		<?php if(isset($channel) && $channel):
					$i=1;
					foreach($channel as $channel):
						$class = ($i%2==0) ? "active" : "success";

						$channel_id=$channel->channel_id;
						?>
		<tr>
		  <td><img  width="60px" height="60px"   src="<?php echo base_url();?>upload/channel/<?php if( $channel->channel_photo_thumb == '') { echo "business-man-hi.png"; } else { echo $channel->channel_photo_thumb; }?>" alt=""/></td>
		  <td><?php echo $channel->channel_name ?></td>
		  <td><?php echo $channel->channel_contact_name ?></td>
		  <td><?php echo $channel->channel_address ?></td>
		  <td><?php echo $channel->channel_contact ?></td>
		  <td><?php echo $channel->channel_website ?></td>
		  <td><?php echo $channel->channel_commission ,"%"; ?></td>
          <td class="ba">
              	<div class="btn-group">
                  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li><a href="javascript:void(0);" onclick="delete_channel('<?php echo $channel->channel_id; ?>');" class="btn red btn-xs" data-toggle="modal">Delete</a></li>
                    <li><a href="<?php echo base_url() ?>dashboard/edit_channel?channel_id=<?php echo $channel->channel_id;?>" class="btn green btn-xs" data-toggle="modal">Edit</a></li>
                  </ul>
                </div>              	
              </td>
		</tr>
		<?php endforeach; ?>
		<?php endif; ?>
	  </tbody>
	</table>
  </div>
</div>
<script>
    function delete_channel(val){
        var result = confirm("Are you want to delete this Channel?");
        var linkurl = '<?php echo base_url() ?>dashboard/delete_channel/'+val;
        if (result) {
            //alert(linkurl);
            window.location.href= linkurl;
        }

    }
	
	    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_purchase?e_id="+id,
                data:{e_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }


</script>