<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
	<div class="caption"> <i class="fa fa-edit"></i>List of All Purchases </div>
	<div class="actions">
		<a href="<?php echo base_url();?>dashboard/add_purchase" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
	</div>
  </div>
  <div class="portlet-body">
	<table class="table table-striped table-bordered table-hover" id="sample_3">
	  <thead>
		<tr> 
		  <!--<th scope="col">
						Select
					</th>-->
		  <th scope="col"> # </th>
		  <th width="8%" scope="col"> Purchased Item </th>
		  <th scope="col"> Asset Id </th>
		  <th scope="col"> Purchase Date </th>
		  <th scope="col"> Purchase Supplier </th>
		  <th scope="col"> Profit Center </th>
		  <th scope="col"> Item Price </th>
		  <th scope="col"> Item Quantity </th>
		  <th scope="col" class="none"> Base Price </th>
		  <th scope="col" class="none"> Tax(%) </th>
		  <th scope="col" class="none"> Discount(%) </th>
		  <th scope="col" class="none"> Total Price </th>
		  <th scope="col"> Action </th>
		</tr>
	  </thead>
	  <tbody>
		<?php if(isset($purchases) && $purchases):
					$i=1;
					$j=0;
					foreach($purchases as $purchase):
						$class = ($i%2==0) ? "active" : "success";
						$j++;
						$p_id=$purchase->id;
						?>
		<tr>
		  <td>
			<?php echo $j ?>
		  </td>
		  <td>
			<span style="font-weight:Bold"><?php echo $purchase->p_i_name."</span> - <span>".$purchase->p_i_description."</span>"?>
		  </td>
		  <td><?php echo $purchase->a_id ?></td>
		  <td><?php echo date("jS F Y",strtotime($purchase->p_date))?></td>
		  <td><?php echo $purchase->p_supplier ?></td>
		  <td><?php echo $purchase->profit_center ?></td>
		  <td><?php echo $purchase->p_i_price ?></td>
		  <td><?php echo $purchase->p_i_quantity." ".$purchase->p_unit?></td>
		  <td><?php echo $purchase->p_price ?></td>
		  <td><?php echo $purchase->p_tax ?></td>
		  <td><?php echo $purchase->p_discount ?></td>
		  <td><?php echo $purchase->p_total_price ?></td>
		  <td align="center" class="ba">
          	<div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="javascript:void(0);" onclick="soft_delete('<?php echo $purchase->id; ?>');" class="btn red btn-xs" data-toggle="modal"><i class="fa fa-trash"></i></a></li>
                <li><a href="<?php echo base_url() ?>dashboard/edit_purchase/<?php echo $purchase->id;?>" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li>
              </ul>
            </div>
          </td>
		</tr>
		<?php endforeach; ?>
		<?php endif; ?>
	  </tbody>
	</table>
  </div>
</div>    
<script>

    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_purchase?p_id="+id,
                data:{p_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }



    function delete_channel(val){
        var result = confirm("Are you want to delete this Channel?");
        var linkurl = '<?php echo base_url() ?>dashboard/delete_channel/'+val;
        if (result) {
            //alert(linkurl);
            window.location.href= linkurl;
        }

    }
</script>