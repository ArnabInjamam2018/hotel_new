<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light borderd">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i> All Rates Plan
      <?php if(isset($stat) && $stat){echo $stat;}?>
    </div>    
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-6">
          <button  style="display:none;" class="btn red" onclick="all_rate_plans()" > Populate Data </button>
          <?php
			$p_id=1;
			$bs_id=1;
		  $rate_plan=$this->rate_plan_model->all_rate_plan_type();
			//print_r($rate_plan); exit;
		if($rate_plan){
				foreach($rate_plan as $r_p){
			?>
          <button <?php if($r_p->rate_plan_type_id==$p_id) { echo 'class="btn tabval purplene"'; $sid = $p_id;} else  echo 'class="btn tabval green"';?>  onclick="show_rp('<?php echo $r_p->rate_plan_type_id;?>')" value="<?php echo $r_p->rate_plan_type_id;?>" id="id_<?php echo $r_p->rate_plan_type_id;?>"><?php echo $r_p->name;?></button>
          <input type="hidden" id='p_type_id' value="<?php echo $p_id;?>">
		  <?php }}?>
		  
        </div>   
			<!--<div style="" id="loading">
				<p class="pull-right"><img src="<?php //echo base_url(); ?>/build/image/ajax-loader.gif" /></p>
			</div>-->
      </div>
    </div>
    <div class="tabbable-line tabbable-full-width">
      <ul class="nav nav-tabs">
        <?php $booking_source=$this->rate_plan_model->all_booking_source_type();
                //print_r($booking_source); exit;
                if($booking_source){
                foreach($booking_source as $bs){
          ?>
        <li <?php if($bs->bst_id==$bs_id){ echo 'class="active tabV"'; $pid = $bs_id;}else echo 'class="tabV"'?> id="bid_<?php echo $bs->bst_id;?>">
          <a onclick="get_rate('<?php echo $bs->bst_id?>')" > <?php echo $bs->bst_name;?> </a>
        </li>
        <?php }}?>
      </ul>
	  
      <div class="tab-content">
	  	<div id="table1" style="margin-top: -20px;">			
            <table class="table table-striped table-hover table-bordered mass-book short" id="sample_3">
              <thead>
                <tr style="color:#F8681A;">
                  <th rowspan="2" width="1%" style="padding: 58px 10px 10px;"> # </th>
                  <th rowspan="2" width="8%" style="padding: 58px 10px 10px;"> Unit Type </th>
                  <th rowspan="2" width="8%" style="padding: 58px 10px 10px;">Meal Plan</th>
                  <th rowspan="2" width="9%" style="padding: 58px 10px 10px; border-right: 1px solid #dddddd;">Occupancy</th>                
    			  
				  <th colspan="2" style="text-align:center; border-right: 1px solid #dddddd; background-color:#eeeeee !important; color:#26c281 !important; font-size:21px;">Base</th>
                  <th colspan="2" style="text-align:center; border-right: 1px solid #dddddd; background-color:#eeeeee !important; color:#e43a45 !important; font-size:21px;">Weekend</th>
                  <th colspan="2" style="text-align:center; background-color:#eeeeee !important; color:#337ab7 !important; font-size:21px;">Seasonal</th>
                </tr>
                <tr style="background-color: #e7ecf1;color: #333;">
					<th style="color:#26c281 !important;">Room<br/> Rent</th>
                   <!-- <th style="color:#26c281 !important;">RR <br/>Tax</th>-->
                    <th style="color:#26c281 !important;">Meal <br/>Plan</th>
                    <!-- <th style="border-right: 1px solid #dddddd;color:#26c281 !important;">MP <br/>Tax</th>-->
                    <th style="color:#e43a45 !important;">Room <br/>Rent</th>
                    <!--<th style="color:#e43a45 !important;">RR <br/>Tax</th>-->
                    <th style="color:#e43a45 !important;">Meal <br/>Plan</th>
                     <!--<th style="border-right: 1px solid #dddddd;color:#e43a45 !important;">MP <br/>Tax</th>-->
                    <th style="color:#337ab7 !important;">Room <br/>Rent</th>
                    <!--<th style="color:#337ab7 !important;">RR <br/>Tax</th>-->
                    <th style="color:#337ab7 !important;">Meal <br/>Plan</th>
                    <!-- <th style="color:#337ab7 !important;">MP <br/>Tax</th>-->
                 </tr>
              </thead>
              <tbody>				
                <?php   $sl_no=0;
                        $data = $this->rate_plan_model->get_rate_plan(1,1);
                       // print_r($data); exit;
						//echo (sizeof($data));
                        $put = '';
                        $pmp = '';
                        foreach($data as $data){
							$sl_no++;
                   ?>
                <tr>
                  <td><?php echo $sl_no;?></td>
                <td>
					  <?php 
                                if($put != $data->unit_name)
                                   echo '<span style="font-size:12px; font-weight:bold">'.$data->unit_name.'</span>';
								else
                                   echo '<span style="font-size:12px;">'.$data->unit_name.'</span>';
                                $put = $data->unit_name;
                      ?>
				  </td>
                  <td>
						<?php 
                                if($pmp != $data->meal_plan)
                                    echo '<span style="font-size:13px; font-weight:bold;">'.$data->meal_plan.'</span>';
								else
                                   echo '<span style="font-size:12px;">'.$data->meal_plan.'</span>';
                                $pmp = $data->meal_plan;
                        ?>
				  </td>
                  <td style="border-right: 1px solid #dddddd;">
						<?php 
                                
                                echo '<span style="font-size:12px">'.$data->occupancy_type.'</span>';
                                
                        ?>
				  </td>
                  
                  <td> <input type="text" id="br_c<?php echo $data->rate_plan_id;?>" class="form-control input-sm" value="<?php echo $data->b_room_charge;?>" onblur="update_plan('<?php echo $data->rate_plan_id;?>')" maxlength="10" ></td>
                 <!-- <td><input type="text" id="br_ct<?php echo $data->rate_plan_id;?>" disabled class="form-control input-sm" value="<?php echo $data->b_room_charge_tax;?>" onblur="update_plan('<?php echo $data->rate_plan_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);"></td>-->
                  <td><input type="text" <?php if($data->meal_plan_id==1) echo "disabled";?>  id="bm_c<?php echo $data->rate_plan_id;?>" class="form-control input-sm" value="<?php echo $data->b_meal_charge;?>" onblur="update_plan('<?php echo $data->rate_plan_id;?>')" maxlength="10" ></td>
                 <!-- <td style="border-right: 1px solid #dddddd;"><input type="text" disabled id="bm_ct<?php echo $data->rate_plan_id;?>" class="form-control input-sm" value="<?php echo $data->b_meal_charge_tax;?>" onblur="update_plan('<?php echo $data->rate_plan_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);"></td>-->
				  <td> <input type="text" id="wr_c<?php echo $data->rate_plan_id;?>" class="form-control input-sm" value="<?php echo $data->w_room_charge;?>" onblur="update_plan1('<?php echo $data->rate_plan_id;?>')" maxlength="10"></td>
                 <!-- <td><input type="text" id="wr_ct<?php echo $data->rate_plan_id;?>" disabled class="form-control input-sm" value="<?php echo $data->w_room_charge_tax;?>" onblur="update_plan1('<?php echo $data->rate_plan_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);"> </td>-->
                  <td><input type="text" <?php if($data->meal_plan_id==1) echo "disabled";?>  id="wm_c<?php echo $data->rate_plan_id;?>" class="form-control input-sm" value="<?php echo $data->w_meal_charge;?>" onblur="update_plan1('<?php echo $data->rate_plan_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);"></td>
                 <!-- <td style="border-right: 1px solid #dddddd;"><input type="text" disabled id="wm_ct<?php echo $data->rate_plan_id;?>" class="form-control input-sm" onblur="update_plan1('<?php echo $data->rate_plan_id;?>')" value="<?php echo $data->w_meal_charge_tax;?>" maxlength="10" onkeypress=" return onlyNos(event, this);"></td>-->
                  <td> <input type="text" id="sr_c<?php echo $data->rate_plan_id;?>" class="form-control input-sm" value="<?php echo $data->s_room_charge;?>" onblur="update_plan2('<?php echo $data->rate_plan_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);"></td>
                 <!-- <td><input type="text" id="sr_ct<?php echo $data->rate_plan_id;?>" disabled class="form-control input-sm" value="<?php echo $data->s_room_charge_tax;?>" onblur="update_plan2('<?php echo $data->rate_plan_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);"></td>-->
                  <td><input type="text" <?php if($data->meal_plan_id==1) echo "disabled";?>  id="sm_c<?php echo $data->rate_plan_id;?>" class="form-control input-sm" value="<?php echo $data->s_meal_charge;?>" onblur="update_plan2('<?php echo $data->rate_plan_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);"></td>
                 <!-- <td><input type="text" id="sm_ct<?php echo $data->rate_plan_id;?>" disabled class="form-control input-sm" value="<?php echo $data->s_meal_charge_tax;?>" onblur="update_plan2('<?php echo $data->rate_plan_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);"> </td>-->
                </tr>
                <?php }?>
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end edit modal --> 
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
			$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_pc?pc_id="+id,
                data:{},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){
					$('#row_'+id).remove();

                        });
                }
            });



        });
    }
	
	
</script> 
<script> 


var p_id=0;
function all_rate_plans(){
	//alert("dgdg"); 
	 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>rate_plan/add_rate_plan",
                
                success:function(data)
                {
                   
                     swal({
                            title: data.data,
                            text: "Data populated successfully!",
                            type: "success"
                        },
                        function(){
							//$('#row_'+id).remove();

                        });
                        
                }
            });

}

jQuery(document).ready(function() {
    window.sid = parseInt(<?php echo $sid;?>);
	window.pid = parseInt(<?php echo $pid;?>);
	loaderOff();
});

function show_rp(id){
	p_id=id;
	window.pid=id;
	$('.tabval').removeClass("purplene");
	$('.tabval').addClass("green");
	$('#id_'+id).removeClass("green");
	$('#id_'+id).addClass("purplene");
	get_rate(window.sid);
}

function get_rate(id){
//	alert(id);
	window.sid = id;
	//console.log(window.pid);
	//$('#bid_'+id).removeClass("active");
	
	
	if(window.pid === '' || window.sid === ''){
		swal("Please Select Season!", "", "warning") 
		return false;
	}
	else{
		
		$('.tabV').removeClass("active");
		$('#bid_'+id).addClass("active");
		
		loaderOn();
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>rate_plan/get_rate_plan",
                data:{p_id:window.pid,s_id:id},
                success:function(msg)
					{
					  
					$('#table1').html(msg);
					$('#sample_3').dataTable( {
							"pageLength": 20
					} );
					 
					}					
				});
			loaderOff();	
	}
	
}

function loaderOn() {
    $("#loading").show();
}

function loaderOff() {
    $("#loading").hide();
}

</script> 
<script> 
function update_plan(r_id){
	//alert(r_id);
	var br_c=parseFloat($("#br_c"+r_id).val());
	var br_ct=parseFloat($("#br_ct"+r_id).val());
	var bm_c=parseFloat($("#bm_c"+r_id).val());
	var bm_ct=parseFloat($("#bm_ct"+r_id).val());
	var br_ctax=23;
	var bm_ctax=23;
	/*alert(br_c);
	alert(br_ct);
	alert(bm_c);
	alert(bm_ct);*/
	br_cttx=br_c*br_ctax/100;
	bm_cttx=bm_c*bm_ctax/100;
	//alert(br_cttx);
	$("#br_ct"+r_id).val(br_cttx);
	$("#bm_ct"+r_id).val(bm_cttx);
	 loaderOn();
	 $.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>rate_plan/update_rate_plan",
                data:{r_id:r_id,br_c:br_c,br_ct:br_cttx,bm_c:bm_c,bm_ct:bm_cttx},
				
                success:function(data)
                {
                  
                }
            });
	 loaderOff();
	}
	
var wr_ct1='';
function update_plan1(r_id){
	//alert(r_id);
	var wr_c=parseFloat($("#wr_c"+r_id).val());
	var wr_ct=parseFloat($("#wr_ct"+r_id).val());
	 wr_ct1=parseFloat($("#wr_ct"+r_id).val());
	var wm_c=parseFloat($("#wm_c"+r_id).val());
	var wm_ct=parseFloat($("#wm_ct"+r_id).val());
	var wr_ctax=23;
	var wm_ctax=23;
	/*alert(wr_c);
	alert(wr_ct);
	alert(wm_c);
	alert(wm_ct);*/
	wr_cttx=wr_c*wr_ctax/100;
	wm_cttx=wm_c*wm_ctax/100;
	
	
	
	$("#wr_ct"+r_id).val(wr_cttx);
	$("#wm_ct"+r_id).val(wm_cttx);
	 $.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>rate_plan/update_rate_plan1",
                data:{r_id:r_id,wr_c:wr_c,wr_ct:wr_ct,wm_c:wm_c,wm_ct:wm_cttx},
                success:function(data)
                {
					
                }
            });
}





function update_plan2(r_id){
	//alert(r_id);
	var sr_c=parseFloat($("#sr_c"+r_id).val());
	var sr_ct=parseFloat($("#sr_ct"+r_id).val());
	var sm_c=parseFloat($("#sm_c"+r_id).val());
	var sm_ct=parseFloat($("#sm_ct"+r_id).val());
//	alert(sr_c);
//	alert(sr_ct);
//	alert(sm_c);
//	alert(sm_ct);
    var sr_ctax=23;
	var sm_ctax=23;
	sr_cttx=sr_c*sr_ctax/100;
	sm_cttx=sm_c*sm_ctax/100;
	$("#sr_ct"+r_id).val(sr_cttx);
	$("#sm_ct"+r_id).val(sm_cttx);
	 $.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>rate_plan/update_rate_plan2",
                data:{r_id:r_id,sr_c:sr_c,sr_ct:sr_ct,sm_c:sm_c,sm_ct:sm_ct},
                success:function(data)
                {
                   
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){
					//$('#row_'+id).remove();

                        });
                }
            });
}
</script> 