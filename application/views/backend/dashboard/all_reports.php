<script>
	function check_sub() {
		document.getElementById( 'form_date' ).submit();
	}
</script>


<div class="portlet light borderd">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-newspaper-o"></i>All Guest Report </div>
		<div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
	</div>
	<div class="portlet-body">
		<div class="table-toolbar">
			<div class="row">
				<div class="col-md-7">
					<?php
					$form = array(
						'class' => 'form-inline',
						'id' => 'form_date',
						'method' => 'post'
					);

					echo form_open_multipart( 'dashboard/report_search', $form );

					?>
					<table id="logo-name">
						<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel')); ?>
						<tr>
							<td align="left" valign="middle"><img src="<?php echo base_url();?>upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/>
							</td>
							<td align="right" valign="middle">
								<?php echo "<strong style='font-size:18px;'>".$hotel_name->hotel_name.'</font></strong>'?>
							</td>
						</tr>

						<tr>
							<td width="100%" colspan="2">
								<hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;">
							</td>
						</tr>
						<tr>
							<td colspan="2"><strong>Date:</strong>
								<?php echo date('D-M-Y'); ?>
							</td>
						</tr>
						<tr>
							<td width="100%" colspan="2">&nbsp;</td>
						</tr>
					</table>
					<div class="form-group">
						<select name="r_visit" id="nature_visit" class="form-control bs-select">
							<option value="" disabled="disabled" selected>Purpose of visit</option>
							<?php $n_v=$this->unit_class_model->booking_nature_visit();
					
					foreach($n_v as $nature_visit){
					?>
							<option value="<?php echo $nature_visit->booking_nature_visit_name;?>" <?php if(isset($r_visit) && $r_visit){if($nature_visit->booking_nature_visit_name==$r_visit){ echo "selected";}}?>>
								<?php echo $nature_visit->booking_nature_visit_name;?>
							</option>
							<?php }?>

						</select>
						<label for="nature_visit"></label>
					</div>

					<div class="form-group">
						<input type="text" class=" date-picker form-control" id="form_control_1" value="<?php if(isset($date_1)){echo $date_1;}?>" name="r_date_1" placeholder="Start Date">

					</div>
					<div class="form-group">
						<input type="text" class="date-picker form-control" id="form_control_1" value="<?php if(isset($date_2)){echo $date_2;}?>" name="r_date_2" placeholder="Start Date">

					</div>
					
					<?php 
					if(isset($start_date) && isset($end_date))
					{
						$s_d = $start_date;
                        $e_d  =$end_date;
					}	
					else
					{	
						/*$s_d = date("Y-m-d");
                        $e_d  = date("Y-m-d");*/
						$s_d = "";
                        $e_d ="";
					}						
					?>

					<button type="submit" onclick="check_sub()" class="btn blue">Search</button>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
		<table class="table table-striped table-bordered table-hover" id="sample_1">
			<thead>
				<tr>
					<th> Booking Id </th>
					<th> Name </th>
					<th> Guest Type </th>
					<th> Stay Duration </th>
					<th> Address </th>
					<th> Contact Number </th>
					<th> Room No </th>
					<th> Room Rent </th>
					<th> Amount Spent </th>
					<th> Amount Due </th>
					<th> Action </th>
				</tr>
			</thead>
			<tbody>
				<?php 
					
						
					if(isset($bookings) && $bookings):
					
                        $i=1;
                     // print_r($bookings);
                        foreach($bookings as $booking):
						
						
						   $guest_id=$booking->guest_id;
							
							$guest_details=$this->dashboard_model->get_guest_detail($guest_id);
							//echo "<pre>";
							//print_r($guest_details);
							if(isset($guest_details) && $guest_details):
								foreach($guest_details as $g_details):
							
							if($g_details->g_type == "corporate"){
								$c_id=$this->dashboard_model->get_company_name($g_details->corporate_id);
							}
							
                            $rooms=$this->dashboard_model->get_room_number($booking->room_id);
							//print_r($rooms);exit;
							
							if(isset($rooms) && $rooms):
                           $deposit=0.00;
						   foreach($rooms as $room):

								

                                if($room->hotel_id==$this->session->userdata('user_hotel')):
									

                                    if(isset($transactions) && $transactions){
										foreach($transactions as $transaction):
										
                                        if($transaction->t_booking_id==$booking->booking_id):
										  //$deposit=0;
										 
                                          $deposit=$transaction->t_amount;
                                        endif;
                                      endforeach;
									}


                                    /*Calculation start */
                                    	$room_number=$room->room_no;
                                    	$room_cost=$room->room_rent;
                                    /* Find Duration */
                                    	$date1=date_create($booking->cust_from_date);
                                    	$date2=date_create($booking->cust_end_date);
                                    	$diff=date_diff($date1, $date2);
                                    	$cust_duration= $diff->format("%a");

                                    
                                    $class = ($i%2==0) ? "active" : "success";

                                    $booking_id='HM0'.$this->session->userdata('user_hotel').'00'.$booking->booking_id;
                                    ?>
				<tr>
					<td>
						<?php echo $booking_id?>
					</td>
					<td>
						<?php echo $g_details->g_name;?>
					</td>
					<td>
						<?php 
										if($g_details->g_type == "corporate"){
											echo $g_details->g_type."<br>";
											echo "(".$c_id->name." -".$g_details->c_des.")";
										} else {
										echo $g_details->g_type;
										}?>
					</td>

					<td>
						<?php
						$from = $booking->cust_from_date_actual;
						$to = $booking->cust_end_date_actual;
						$days = ( strtotime( $to ) - strtotime( $from ) ) / 24 / 3600;
						//echo "( ".$days." )";
						echo $booking->cust_from_date_actual . "-" . $booking->cust_end_date_actual . '</br>';
						echo $days . " (Days)";
						?>
					</td>
					<td>
						<?php echo  $g_details->g_address.",".$g_details->g_city.",".$g_details->g_state.",Pin - ".$g_details->g_pincode;?>
					</td>
					<td>
						<?php echo  $g_details->g_contact_no;?>
					</td>
					<td>
						<?php echo $room_number; ?>
					</td>
					<td>
						<?php echo number_format($room_rent=$booking->room_rent_sum_total-$booking->discount, 2, '.', '');?>
					</td>
					<td>
						<?php echo $deposit; ?>
					</td>
					<td>
						<?php echo  number_format(($room_rent-$deposit),2,'.',''); ?>
					</td>
					<td align="center" class="ba">
						<div class="btn-group">
							<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
							<ul class="dropdown-menu pull-right" role="menu">
								<!-- <li><a href="<?php echo base_url();?>bookings/invoice_generate?booking_id=<?php echo $booking->booking_id;  ?>" target="_blank" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-eye"></i></a></li>-->
								<li><a href="<?php echo base_url();?>bookings/pdf_generate?booking_id=<?php echo $booking->booking_id;  ?>" class="btn blue btn-xs" data-toggle="modal"><i class="fa fa-download"></i></a>
								</li>
							</ul>
						</div>
					</td>
				</tr>
				<?php $i++;?>
				<?php endif; ?>
				<?php endforeach; ?>
				<?php endif; ?>
				<?php endforeach; ?>
				<?php endif;?>
				<?php endforeach; ?>
				<?php endif;?>
			</tbody>
		</table>
	</div>
</div>