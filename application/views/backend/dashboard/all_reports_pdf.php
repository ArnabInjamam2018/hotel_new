<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 14px;
	line-height: 1.42857143;
	color: #555555;
}
.well {
	border: 0;
	-webkit-box-shadow: none !important;
	-moz-box-shadow: none !important;
	box-shadow: none !important;
	min-height: 20px;
	margin-bottom: 20px;
	border-radius: 4px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad {
	font-size: 10px;
}
</style>
<?php 
	$chars = '0123456789';$r='';
	for($i=0;$i<7;$i++)
	{
		$r.=$chars[rand(0,strlen($chars)-1)];
	}
		$randomstring = $r."/"."  ";

 ?>
<?php echo "All_Report",date("d M Y") ?> 


<table class="td-pad" width="100%">
  <thead>
    <tr style="background: #00C5CD; color: white">
      <th> Sl.no </th>
      <th> Booking Id </th>
      <th> Name </th>
      <th> Room Number </th>
      <th> From and Upto Date </th>
      <th> Address </th>
      <th> Contact Number </th>
      <th> Total Amount </th>
      <th> Amount Due </th>
    </tr>
  </thead>
  <tbody style="background: #F2F2F2">
    <?php if(isset($bookings) && $bookings):
                        $i=1;
                        $deposit=0;
                        foreach($bookings as $booking):


                            $rooms=$this->dashboard_model->get_room_number($booking->room_id);
                            foreach($rooms as $room):



                                if($room->hotel_id==$this->session->userdata('user_hotel')):


                                    foreach($transactions as $transaction):

                                        if($transaction->t_booking_id=='HM0'.$this->session->userdata('user_hotel').'00'.$booking->booking_id):

                                            $deposit=$deposit+$transaction->t_amount;
                                        endif;
                                    endforeach;



                                    /*Calculation start */
                                    	$room_number=$room->room_no;
                                    	$room_cost=$room->room_rent;
                                    /* Find Duration */
                                    	$date1=date_create($booking->cust_from_date);
                                    	$date2=date_create($booking->cust_end_date);
                                    	$diff=date_diff($date1, $date2);
                                    	$cust_duration= $diff->format("%a");

                                    /*Cost Calculations*/
                                    $total_cost=$cust_duration*$room_cost;
                                    $total_cost=$total_cost+$total_cost*0.15;
                                    $due=$total_cost-$booking->cust_payment_initial-$deposit;

                                    //$status=$booking->cust_booking_status;

                                    /*if($status==0){

                                        $status_data="Booked";
                                    }
                                    else if($status==1){

                                        $status_data="Confirmed";
                                    }

                                    else if($status==2){

                                        $status_data="Checked In";
                                    }
                                    else if($status==3){

                                        $status_data="Checked Out";
                                    }

                                    /*Calculation End */


                                    $class = ($i%2==0) ? "active" : "success";

                                    $booking_id='HM0'.$this->session->userdata('user_hotel').'00'.$booking->booking_id;
                                    ?>
    <tr>
      <td><?php echo $i;?></td>
      <td><?php echo $booking_id?></td>
      <td><?php echo $booking->cust_name;?></td>
      <td><?php echo $room_number; ?></td>
      <td><?php $date_s =  date_create ($booking->cust_from_date);
										
										echo date_format($date_s,"d.M.Y")." (". substr ($booking->checkin_time,0,5).")";
										
										?>
        <br />
        <?php $date_e = date_create ($booking->cust_end_date);
										
										echo date_format($date_e,"d.M.Y")." (". substr ($booking->checkout_time,0,5).")";
										?></td>
      <td><?php echo $booking->cust_address;?></td>
      <td><?php echo $booking->cust_contact_no;?></td>
      <td><?php echo $total_cost; ?></td>
      <td><?php echo max($due,0); ?></td>
    </tr>
    <?php $i++;?>
    <?php endif; ?>
    <?php endforeach; ?>
    <?php endforeach; ?>
    <?php endif;?>
  </tbody>
</table>
