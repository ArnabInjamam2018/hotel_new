<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td{
	font-size:12px;
}
</style>
<div style="padding:15px 35px;">

<table width="100%">
	<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));?>
	
		  
		  <tr>
		  <td  align="lefft">
		  <?php if(isset($hotel_name->hotel_logo_images_thumb)){ ?>
		  
			
				<img src="upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/>
			
		  
		  <?php } ?>
		  <br><br>
		<?php if(isset($start_date)){echo "<strong>From Date:</strong>".$start_date." "."<strong>To Date:</strong>".$end_date;} else{ echo  "<strong>Date :</strong> ".date('D-M-Y'); }?></td>
		<td align="left"><strong><font size='3'>Police Report</font></strong>
		
			
		
		
		</td>
		<td>
		  <div align="right">
		  <?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?><br/>
          <?php echo  $hotel_contact['hotel_street1']; ?><br/>
          <?php echo  $hotel_contact['hotel_street2']; ?><br/>
          <?php echo  $hotel_contact['hotel_district']; ?> - <?php echo  $hotel_contact['hotel_pincode']; ?> <br/>
		  <?php echo  $hotel_contact['hotel_state']; ?> - <?php echo  $hotel_contact['hotel_country']; ?> <br/>
          <abbr title="Phone"><strong>Phone:</strong></abbr> <?php echo  $hotel_contact['hotel_frontdesk_mobile']; ?><br/>
          <abbr title="Phone"><strong>Email:</strong></abbr> <?php echo  $hotel_contact['hotel_frontdesk_email']; ?> </div>
		</td>  
		</tr>
		
</table>
<table class="td-pad" width="100%">
    <thead>
    <tr style="background: #e5e5e5; color: #1b1b1b">
        <th>
            Sl No.
        </th>
        <th>
            Room No
        </th>
		 <th>
            Status
        </th>
        <th>
            Customer Name
        </th>
		
        <!--<th scope="col" width="20%">
            Customer Pincode
        </th>-->
		 <th>
           Address
        </th>
        <th>
            No of Guest
        </th>
        <th>
            M
        </th>
        <th>
            F
        </th>
        <th>
            C
        </th>

        <th>
            Age
        </th>
        <th>
            Relationship
        </th>
        <th>
            Bed Capacity
        </th>
    </tr>
    </thead>
    <tbody  style="background: #F2F2F2">
    <?php //$guest=0; ?>
    <?php if(isset($bookings) && $bookings != ""){
		$i=1;
        foreach($bookings as $booking){
            if(isset($booking->booking_status_id) && ($booking->booking_status_id=='5' or $booking->booking_status_id=='6')){
		//echo "<pre>";
        //print_r($booking);
		//exit;
                //$rooms=$this->dashboard_model->get_room_number($booking->room_id);


                $date1=date_create($booking->cust_from_date);
				
                $date2=date_create($booking->cust_end_date);
                $current_date= new DateTime('now');
                $diff=date_diff($current_date, $date1);

                //if(isset($date1) && isset($date2) && $date1 <= $current_date && $date2 >= $current_date){

                    //$guest=$guest+$booking->no_of_guest;

                    //$polices = $this->dashboard_model->get_booking_details_unique($booking->booking_id);

                    
                    //foreach($polices as $police){

                        ?>
                        <tr>
                            <td align="center"><?php echo $i; ?></td>
                            <td align="center">
							<?php $room=$this->dashboard_model->get_room_number_exact($booking->room_id);

                                if(isset($room->room_no)){ echo $room->room_no;} ?>
							</td>
							<td  align="center">
							<?php 
							if(isset($booking->booking_status_id)){
							$status=$this->dashboard_model->get_bookings_status($booking->booking_status_id);
							}
							echo $status->booking_status;
							$guest_id = $booking->guest_id;												
							$guest_details = $this->dashboard_model->get_guest_detail($guest_id);
							/*if(isset($guest_details) && $guest_details){
								foreach($guest_details as $g_details){
									echo  $g_details->g_address.", ".$g_details->g_city.", ".$g_details->g_state.", Pin - ".$g_details->g_pincode;
								}
							}*/	
							?>
							</td>
                            <td align="center"><?php if(isset($booking->cust_name)){ echo $booking->cust_name;} ?></td>
                            <td align="center">
							<?php if(isset($guest_details) && $guest_details){
								  foreach($guest_details as $g_details){  ?>
							<?php if(isset($g_details->g_address)){ echo $g_details->g_address;} ?>, <?php if(isset($g_details->g_city)){ echo $g_details->g_city;} ?>, <?php if(isset($g_details->g_state)){ echo $g_details->g_state;} ?><br>
							Pin - <?php if(isset($g_details->g_pincode)){ echo $g_details->g_pincode;} } }?>
							</td>
							<!--<td align="center"><?php //if(isset($booking->cust_address)){ echo $booking->cust_address;} ?></td>-->
                            <td align="center"><?php if(isset($booking->no_of_guest)){ echo $booking->no_of_guest;} ?></td>
                            <td align="center"><?php if(isset($booking->no_of_adult_male)){ echo $booking->no_of_adult_male;} ?></td>
                            <td align="center"><?php if(isset($booking->no_of_adult_female)){ echo $booking->no_of_adult_female;} ?></td>
                            <td align="center"><?php if(isset($booking->no_of_child)){ echo $booking->no_of_child;} ?></td>
                            <td align="center">
							<?php
                            $polices = $this->dashboard_model->get_booking_details_unique($booking->booking_id);
							//echo "<pre>";
							//print_r($polices->g_dob);
							//exit;																			
							if(isset($polices->g_dob) && $polices->g_dob >0){
								//echo $polices->g_dob;	
								$d1 = new DateTime($polices->g_dob);
								$d2 = new DateTime();
								$diff = $d2->diff($d1);
								echo $diff->y; 
							} else {
								echo "N/A";
							} 
							?>
							</td>
                            <td align="center">Self</td>
                            <td align="center">
							<?php if(isset($room->room_bed)){ echo $room->room_bed;}?>
							</td>

                        </tr>
                        <?php $i++; //}  ?>

                <?php //} ?>
            <?php } ?>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
<table>
  <tr>
	<td height="70">&nbsp;</td>
  </tr>
</table>
<table>
  <tr>
	<td width="65%">&nbsp;</td>
	<td align="center" width="35%">______________________________<br />
	  Authorized Signature </td>
  </tr>
</table>
</div>