<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="glyphicon glyphicon-bed"></i>List of All Rooms </div>
    <div class="actions">
    	<a href="<?php echo base_url();?>dashboard/add_room" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i> Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    <!--<div class="table-toolbar">
      <div class="row">
        <div class="col-md-6">
          <div class="btn-group"> <a href="<?php echo base_url();?>dashboard/add_room">
            <button class="btn green"> Add New <i class="fa fa-plus"></i> </button>
            </a> </div>
        </div>
      </div>
    </div>-->
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr>
          <th width="2%" align="center" valign="middle" scope="col"> Sl </th>
          <th width="10%" align="center" valign="middle" scope="col"> Room Image </th>
          <th width="10%" align="center" valign="middle" scope="col"> Unit ID </th>
          <th width="10%" align="center" valign="middle" scope="col"> Unit Conf </th>
          <!--<th width="10%" align="center" valign="middle" scope="col"> Floor</th>-->
          <th width="18%" align="left" valign="middle" scope="col"> Unit Classification </th>
          <!--<th width="10%" align="left" valign="middle" scope="col"> Unit Class </th>
          <th width="10%" align="left" valign="middle" scope="col"> Unit Type </th>-->
          <th width="20%" align="right" valign="middle" scope="col"> Rates </th>
          <th width="19%" align="right" valign="middle" scope="col"> Occupancy Info </th>
          <th width="19%" align="right" valign="middle" scope="col"> Room Features </th>
          <!--<th width="20%" align="center" valign="middle" scope="col"> Note</th>-->
          <th width="5%" align="center" valign="middle" scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($rooms) && $rooms):
                            //print_r($rooms);
                            $i=1;
							$sl = 0;
							//print_r($rooms);
							//echo $this->session->userdata('user_hotel');
                            foreach($rooms as $rom):
							$sl++;
                            if($rom->hotel_id==$this->session->userdata('user_hotel')):
                            $class = ($i%2==0) ? "active" : "success";
                            
                            $room_id=$rom->room_id;
                            $unit_id=$rom->unit_id;
                            
                            $unit_details=$this->dashboard_model->get_unit_details($unit_id);
                          
                            
                            ?>
        <tr>
		  <td><?php echo $sl; ?></td>
          <td align="center" valign="middle" class="sorting_1"><a class="single_2" href="<?php echo base_url();?>upload/unit/thumb/image1/<?php 
		  if($rom->room_image_thumb == '')
			{ 
				echo "no_images_room.jpg"; 
			} 
		  else 
			{ 
				echo $rom->room_image_thumb; 
			} 
			
			?>"><img src="<?php echo base_url();?>upload/unit/thumb/image1/<?php 
				if($rom->room_image_thumb == '')
					{ 
						echo "no_images_room.jpg";
					} 
				else 
					{ 
						echo $rom->room_image_thumb;
					}
			?>" alt="" style="width:50%;"/></a></td>
          <td align="center" valign="middle">
			
			<?php // Unit ID
				   
			  if(isset($rom->room_name) && ($rom->room_name!="N/A") && ($rom->room_name!=NULL)){
				echo '<strong>'.$rom->room_no."</strong><br/>".'<span style="color:#000077;">'.$rom->room_name.'</span>';
			  } else {
				echo $rom->room_no;
			  }	
			    $var = $this->dashboard_model->get_color_by_houskeeping_status_id($rom->clean_id); 
				if($var){
			  ?>
              <br>
			  <span class="label-flat" style="background-color:<?php echo $var->color_primary?>; color:#ffffff;">
			
				<?php 
					$hks=$this->dashboard_model->get_all_housekeeping_status();
					if(isset($hks) && $hks){
					foreach($hks as $h){
						if ($h->status_id==$rom->clean_id) 
						echo $h->status_name;?>
				<?php } }}?>
			</span>
		  </td>
          <td align="left" valign="middle"><?php 
				// Bed Conf
				// Logic for showing the Bed Conf properly _sb dt5_10_16
				if($rom->room_bed == 1)
				{
					echo "Single Bed ";
				}
				else if($rom->room_bed == 2)
				{
					echo "Double Bed ";
				}
				else if($rom->room_bed == 3)
				{
					echo "Tripple Bed ";
				}
				else
					echo $rom->room_bed." Bed ";
				
				echo '<br>';
				
				// Floor No
					// Logic for showing the Floor No properly _sb dt5_10_16
					if($rom->floor_no == 0)
						echo 'Grnd Floor';
					else if($rom->floor_no%10 == 1)
						echo $rom->floor_no.'st Floor';
					else if($rom->floor_no%10 == 2)
						echo $rom->floor_no.'nd Floor';
					else if($rom->floor_no%10 == 3)
						echo $rom->floor_no.'rd Floor';
					else
						echo $rom->floor_no.'th Floor';
				
			?></td>
          <!--<td align="left" valign="middle"><?php 
					// Floor No
					// Logic for showing the Floor No properly _sb dt5_10_16
					if($rom->floor_no == 0)
						echo 'Grnd Floor';
					else if($rom->floor_no%10 == 1)
						echo $rom->floor_no.'st Floor';
					else if($rom->floor_no%10 == 2)
						echo $rom->floor_no.'nd Floor';
					else if($rom->floor_no%10 == 3)
						echo $rom->floor_no.'rd Floor';
					else
						echo $rom->floor_no.'th Floor';
			?></td>-->
          <td align="left" valign="middle">
			<?php // Unit Category
				$su = strtolower(substr($rom->unit_type,0,1));
				$str = strtolower((string)$rom->unit_type);
				echo '<strong style="color:#666666;">Cate - </strong>';
				if($su=='c' || $su=='v')
					echo '<span class="label" style="background-color:#CBEEDD; color:#000000;">'.$str.'</span>';
				else if($str != 'room')
					echo '<span class="label" style="background-color:#FFEB7F; color:#000000;">'.$str.'</span>';
				else
					echo '<span class="label" style="background-color:#7FFFD4; color:#000000;">'.$str.'</span>'; 
								
				// Unit Class
				echo '<br><strong style="color:#666666;">Class - </strong>';
				$su = strtolower(substr($unit_details['unit_class'],0,1));
				if($su == 'p')
					echo '<span style="font-style:italic; color:#0D447A;">'.$unit_details['unit_class'].'</span>';
				else if($unit_details['unit_class'] == 'Standard')
					echo '<span style="font-style:italic; color:#5A6D65;">'.$unit_details['unit_class'].'</span>';
				else
					echo $unit_details['unit_class'];  
				
				
				// Unit Type
				echo '<br><strong style="color:#666666;">Type - </strong>';
				$su = strtolower(substr($unit_details['unit_name'],0,2));
				if($su != 'ac')
					//echo '<span class="label" style="background-color:#FFEB7F; color:#000000;">'.$str.'</span>';
					echo $unit_details['unit_name'];
				else
					echo '<span class="label" style="background-color:#85EBFF; color:#000000;">'.$unit_details['unit_name'].'</span>'; 
			?>
		  </td>
          <!--<td align="left" valign="middle">
			<?php // Unit Class
				echo $unit_details['unit_class'];  
			?>
		  </td>
          <td align="left" valign="middle">
			<?php //Unit Type
				$su = strtolower(substr($unit_details['unit_name'],0,2));
				if($su != 'ac')
					//echo '<span class="label" style="background-color:#FFEB7F; color:#000000;">'.$str.'</span>';
					echo $unit_details['unit_name'];
				else
					echo '<span class="label" style="background-color:#85EBFF; color:#000000;">'.$unit_details['unit_name'].'</span>'; 
				  
				//echo $su;
			?>
		  </td>-->
          <td align="left" valign="middle">
			<?php // Rates
				echo '<strong style="color:#666666;">Plan </strong>'.'- General<br><br>';
				echo '<span style="color:#3C8361;">₹'.$rom->room_rent .' (Base)</span>';         
				if($rom->room_rent_weekend != 0)      
					echo '<br><span style="color:#600035;">'.'₹'.$rom->room_rent_weekend.' (Week End)</span>' ;
				if($rom->room_rent_seasonal != 0)
					echo '<br><span style="color:#F26522;">'.'₹'.$rom->room_rent_seasonal.' (Seasonal)</span>';
			?>
			
		  </td>
		  <td>
			<?php // Occupancy Info
				echo '<strong style="color:#666666;">Max Occupancy - </strong>'.$unit_details['max_occupancy'].' person';
				if($rom->rate_type == 0)
					echo '<br><strong style="color:#788385;">Rate Type - </strong><span style="color:#788385;">Fixed</span>';
				else{
					echo '<br><strong style="color:#666666;">Rate Type - </strong><span style="color:#6B67A1;">Depends</span>';
					echo '<br><strong style="color:#666666;">Adult - </strong>'.$rom->adult_rate;
					if($rom->adult_rate_type == 0)
						echo ' INR';
					else
						echo '%';
					echo '<br><strong style="color:#666666;">Kid - </strong>'.$rom->kid_rate;
					if($rom->kid_rate_type == 0)
						echo ' INR';
					else
						echo '%';
				}
				
			?>
		  </td>
		  <td>
			<?php // Room Features
				$features = $rom->room_features;	
				
				$arr = (explode(",",$features));
						$l = count($arr);
						$k = 0;
						for($i=0;$i<$l;$i++){
							if($arr[$i] != 0){
								$fe = $this->dashboard_model->get_room_feature_by_id($arr[$i]);
								if($arr[$i] == 8)
									echo '<span class="label" style="background-color:#E46A85; color:#FFFFFF;">'.$fe->room_feature_slug.'</span>'.' ';
								else
									echo '<span class="label" style="background-color:#50C381; color:#FFFFFF;">'.$fe->room_feature_slug.'</span>'.' ';
								$k++;
							}
						}	// end for loop
				if($k == 0)
					echo '<span style="color:#9C9C8F;">No info</span>'.'';
            
			?>
			
		  </td>
          <!--<td align="left" valign="middle"><?php echo $rom->note; ?></td>-->
          <td align="center" valign="middle" class="ba">
			<div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li> <a onclick="soft_delete('<?php echo $room_id ?>')" data-toggle="modal" class="btn red btn-xs"> <i class="fa fa-trash"></i></a> </li>
                <li> <a href="<?php echo base_url() ?>dashboard/edit_room?room_id=<?php echo $room_id ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a> </li>
              </ul>
            </div>
		  </td>
        </tr>
        <?php $i++;?>
        <?php endif; ?>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<!-- END PAGE CONTENT--> 
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#F27474",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          
 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_room?room_id="+id,
                data:{r_id:id},
                success:function(data)
                {
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
</script> 
