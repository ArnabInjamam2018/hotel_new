<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Salary Payments </div>
    <div class="tools"> 
      <!--<a href="javascript:;" class="collapse">
                </a>
                <a href="#portlet-config" data-toggle="modal" class="config">
                </a>
                <a href="javascript:;" class="reload">
                </a>--> 
      <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body" style="padding-top:70px;">
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <!-- <th scope="col">
                            Select
                        </th>-->
          <th scope="col"> Generate Date </th>
          <th scope="col"> Payment Type </th>
		  <th scope="col"> Status </th>
          <th scope="col"> Person Name </th>
		  <th scope="col"> Designation </th>
          <th scope="col"> Month </th>
          <th scope="col"> Others </th>
          <th scope="col"> P Center </th>
          <th scope="col"> Pay Mode </th>
          <th scope="col"> Tran Details </th>
          <!--<th scope="col"> Bank Name </th>-->
          <th scope="col"> Approved By </th>
          <th scope="col"> Paid By </th>
          <th scope="col"> Admin </th>
          <th scope="col"> Recvr's Name </th>
          <!--<th scope="col"> Reciever's Signature </th>-->
		  <th scope="col"> Desc </th>
          <th width="5%" scope="col"> Action </th>
          
          <!--<th scope="col">
                            Id Proof
                        </th>--> 
          <!-- <th scope="col">
                            Action
                        </th> --> 
        </tr>
      </thead>
      <tbody>
        <?php if(isset($salpay) && $salpay):
                        
                        $i=1;//var_dump($salpay);die;
                        foreach($salpay as $sp):
                            $class = ($i%2==0) ? "active" : "success";
                            $sal_pay_id=$sp->hotel_payment_id;
                            ?>
        <tr> 
         
          
          <td>
			<?php 
				$dt = date("d-M-Y", strtotime($sp->date));
				echo $dt; ?>
		  </td>
          <td><?php echo $sp->type_of_payment;?></td>
		  <td>
			  <?php // Status
					$stat = 1;
					if($stat == 1){
						echo '<span class="label" style="background-color:#0BF800; color:#333333;">Paid</span>';
					}
					else if($stat == 0){
						echo '<span class="label" style="background-color:#F488D3; color:#333333;">Due</span>';
					}
					else if($stat == 2){
						echo '<span class="label" style="background-color:#E9E9E9; color:#333333;">Partial</span>';
					}
					else
						echo '<span class="label" style="background-color:#E9E9E9; color:#333333;">N/A</span>';
			  ?>
		  </td>
          <td>
				<?php
                   echo $sp->person_name;
                ?>
		  </td>
         
          <td><?php echo $sp->designation; ?></td>
          <td><?php echo $sp->sal_month; ?></td>
          <td><?php echo $sp->sal_other; ?></td>
          
          <td><?php if(isset($sp->profit_center)){echo $sp->profit_center;} ?></td>
          <td><?php echo $sp->mode_of_payment; ?></td>
          <td>
			<?php 
				
				if($sp->check_bank_name!=""){
                    echo $sp->check_bank_name.' ';
                    }elseif($sp->draft_bank_name){
                    echo $sp->draft_bank_name.' '; 
                    }elseif($sp->ft_bank_name){
                    echo $sp->ft_bank_name.' '; 
                    }
				
				if($sp->check_no!=""){
					echo $sp->check_no;
                }
				elseif($sp->draft_no){    
					echo $sp->draft_no;
				}
				elseif($sp->ft_account_no){
                    echo $sp->ft_account_no."</br>";
                    echo "IFSC- ".$sp->ft_ifsc_code;
                }
				else
					echo 'No Info';
            ?>
		  </td>
          <!--<td><?php if($sp->check_bank_name!=""){
                    echo $sp->check_bank_name;
                    }elseif($sp->draft_bank_name){
                    echo $sp->draft_bank_name; 
                    }elseif($sp->ft_bank_name){
                    echo $sp->ft_bank_name; 
                    }
                    ?></td>-->
          <td><?php echo $sp->approved_by; ?></td>
          <td><?php echo $sp->paid_by; ?></td>
          <td><?php echo $sp->admin; ?></td>
          <td><?php echo $sp->recievers_name; ?></td>
          <!--<td><?php echo $sp->recievers_desig; ?></td>-->
          
          <td><?php echo $sp->description; ?></td>
          <td align="center" class="ba">
          	<div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="<?php echo base_url() ?>dashboard/delete_salpayments?salpay_id=<?php echo $sp->hotel_payment_id;?>" class="btn red btn-xs" data-toggle="modal"><i class="fa fa-trash"></i></a></li>
                <li><a href="<?php echo base_url();?>dashboard/edit_bill_payments?p_id=<?php echo $sp->hotel_payment_id; ?>" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li>
              </ul>
            </div>
          </td>
		  
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>    
<script  type="text/javascript" src="<?php echo base_url();?>assets/common/js/table_sort_style.js"></script>