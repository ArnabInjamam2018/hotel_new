<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Housekeeping Section </div>
    <div class="actions">
    	<a  class="btn btn-circle green btn-outline btn-sm" data-toggle="modal" href="#responsive" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i> Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr>
          <th align="center" valign="middle" scope="col"> Image </th>
          <th align="center" valign="middle" scope="col"> Name </th>          
          <th align="center" valign="middle" scope="col"> Hkp Status </th>
          <th align="center" valign="middle" scope="col"> Audit Status </th>
		  <th align="center" valign="middle" scope="col"> Desciption</th>
          <th align="center" valign="middle" scope="col"> Note </th>
          <!--<th align="center" valign="middle" scope="col"> Hotel Id </th>-->
          <th width="5%" align="center" valign="middle" scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php
				
			if(isset($section) && $section):
							//echo "<pre>";
						//	print_r($guest);
                           // $i=1;
                            foreach($section as $gst):
                               // $class = ($i%2==0) ? "active" : "success";
                                //$g_id=$gst->g_id;
                                ?>
        <tr id="row_<?php echo $gst->sec_id; ?>">
          <td align="center" valign="middle" width="10%"><a class="single_2" href="<?php echo base_url();?>upload/section/<?php
			  if( $gst->sec_img== '' || $gst->sec_img=='0') {
				   echo "no_images.png"; 
			  }
						  
			   else {
				  echo $gst->sec_img;
				  }
				  
				  ?>"><img src="<?php echo base_url();?>upload/section/<?php
			  if( $gst->sec_img== '' || $gst->sec_img=='0') {
				   echo "no_images.png"; 
			  }
						  
			   else {
				  echo $gst->sec_img;
				  }
				  
				  ?>" alt="" style="width:100%;"/></a>
            <?php
										/*echo $gst->g_photo ;
										echo $gst->g_photo ."_thumb"; */
									?></td>
          <td align="left" valign="middle"><?php echo $gst->sec_name ?></td>
          <td align="left" valign="middle">
		  <?php
		  if(isset($gst->sec_housekeeping_status) && $gst->sec_housekeeping_status!=''){
			  
			  $stat = $gst->sec_housekeeping_status;
		  }
		  else{
			  
			  $stat ='';
		  }
		  $colorarray = $this->dashboard_model->get_color_by_houskeeping_status_id($stat);  //->color_primary
		  if(isset($colorarray->color_primary) && $colorarray->color_primary!=''){
			  
			  $col = $colorarray->color_primary;
		  }
		  else{
			  
			  $col='';
		  }
		  ?>
			<span class="label" Style="background-color:<?php echo $col; ?>;">
			<?php echo $gst->sec_housekeeping_status;	?>
			 <?php 
					$hks=$this->dashboard_model->get_all_housekeeping_status();
					if(isset($hks) && $hks){
					foreach($hks as $h) 
					{
						if ($h->status_id==$gst->sec_housekeeping_status) 
						echo $h->status_name;?>
			<?php } }?>
			</span>
			
		  </td>
          <td align="left" valign="middle"><?php echo $gst->sec_audit_status; ?></td>
		  <td align="left" valign="middle">
			<?php 
				if((isset($gst->sec_desc)) && $gst->sec_desc!=NULL){
					echo $gst->sec_desc;
				}
				else 
					echo 'None';
			?>
		  </td>
          <td align="left" valign="middle"><?php echo $gst->note ?></td>
          <!--<td align="left" valign="middle"><?php echo $gst->sec_hotel_id; ?></td>-->
          <td align="center" valign="middle" class="ba">
          	<div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li> <a onclick="soft_delete('<?php echo $gst->sec_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a> </li>
                <li> <a onclick="edit_section('<?php echo $gst->sec_id; ?>')" data-toggle="modal" class="btn green btn-xs"> <i class="fa fa-edit"></i></a> </li>
              </ul>
            </div>
          </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/delete_section?id="+id,
                data:{id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                       function(){
                            //location.reload();
							$('#row_'+id).remove();
                        });	
                }
            });
        });
    }
	
	function edit_section(id){
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/fetch_section",
                data:{id:id},
                success:function(data)
                {
                    //alert(data.id);
					
                   $('#hid1').val(data.sec_id);
				   $('#name1').val(data.sec_name);
				   $('#desc1').val(data.sec_desc);
				   $('#Housekeeping_status1').val(data.sec_housekeeping_status);
				   $('#audit_status1').val(data.sec_audit_status);
				   $('#img1').attr('src','<?php echo base_url().'upload/section/';  ?>'+data.sec_img);
				   $('#note1').val(data.note);
				   $('#hotel_id1').val(data.sec_hotel_id);
				  /*alert(data.sec_name);
				  alert(data.sec_housekeeping_status);
				  alert(data.note);
				  alert(data.sec_id);
				  alert(data.sec_id);*/
				   
				   //$('#responsive').hide();
                    $('#editsection').modal('toggle');
					
                }
            });
	}
	
	
</script> 
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('unit_class_controller/add_hotel_section',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Unit Type</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" id="name" name="name" placeholder="Section Name" required="required">
                <label></label>
                <span class="help-block">Section Name</span> </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" id="Housekeeping_status" name="Housekeeping_status" placeholder="Housekeeping Status" required="required">
                <label></label>
                <span class="help-block">Housekeeping Status</span> </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input ">
                <input type="text" class="form-control" id="audit_status" name="audit_status" placeholder="Audit Status" required="required">
                <label></label>
                <span class="help-block">Audit Status</span> </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" id="hotel_id" placeholder="Section Hotel Id" name="hotel_id" required="required">
                <label></label>
                <span class="help-block">Section Hotel Id</span> </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <textarea autocomplete="off" type="text" class="form-control" placeholder="Note..." name="note" id="note" ></textarea>
                <label></label>
                <span class="help-block">Note...</span> </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <textarea autocomplete="off" type="text" class="form-control" placeholder="Description..."  name="desc" id="desc" ></textarea>
                <label></label>
                <span class="help-block">Description...</span> </div>
            </div>
            <div class="col-md-12">
              <div class="form-group form-md-line-input uploadss">
              	<label>Upload Images</label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                  <div> <span class="btn default btn-file"> <span class="fileinput-new"> Transfer Voucher image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('image');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn default">Close</button>
      <button type="submit" class="btn green">Save</button>
    </div>
      </div>
    </div>
    <?php echo form_close(); ?>
</div>
<div id="editsection" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form1 = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('unit_class_controller/edit_section',$form1);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Section</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-6">
              <input type="hidden" name="hid1" id="hid1">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" placeholder="Section Name" id="name1" name="name1" required="required">
                <label></label>
                <span class="help-block">Section Name</span> </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" id="Housekeeping_status1" placeholder="Housekeeping Status" name="Housekeeping_status1" required="required">
                <label></label>
                <span class="help-block">Housekeeping Status</span> </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" id="audit_status1" placeholder="Audit Status" name="audit_status1" required="required">
                <label></label>
                <span class="help-block">Audit Status</span> </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" id="hotel_id1" placeholder="Section Hotel Id" name="hotel_id1" required="required">
                <label></label>
                <span class="help-block">Section Hotel Id</span> </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <textarea autocomplete="off" type="text" class="form-control" placeholder="Note"  name="note1" id="note1" ></textarea>
                <label></label>
                <span class="help-block">Note...</span> </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <textarea autocomplete="off" type="text" class="form-control" placeholder="Description" name="desc1" id="desc1" ></textarea>
                <label></label>
                <span class="help-block">Description...</span> </div>
            </div>
            <div class="col-md-12">
              <div class="form-group form-md-line-input uploadss">
              	<label>Upload Image</label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img id="img1" src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                  <div> <span class="btn default btn-file"> <span class="fileinput-new"> Transfer Voucher image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('image');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>