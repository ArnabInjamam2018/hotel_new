<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Services </div>
    <div class="actions"> 
      <a href="<?php echo base_url();?>dashboard/add_service" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <!--<th scope="col">
                Select
            </th>-->
          <th> Id </th>
          <th> Image </th>
          <th data-sortable="true"> Name </th>
          <th data-sortable="true"> Category </th>
          <th data-sortable="true"> Description </th>
          <th data-sortable="true"> Members Allowed </th>
          <th data-sortable="true"> Periodicity </th>
          <th data-sortable="true"> Rules </th>
          <th data-sortable="true"> Price Assigned </th>
          <th data-sortable="true"> Weekend Price </th>
          <th data-sortable="true"> Holiday Price </th>
          <th data-sortable="true"> Special Price </th>
          <th data-sortable="true"> Service Tax Applied </th>
          <th data-sortable="true"> Service Tax Amount </th>
          <th data-sortable="true"> Service Discount Applied </th>
          <th data-sortable="true"> Service Discount Amount </th>
          <th> Actions </th>
        </tr>
      </thead>
      <tbody>
        <?php
            $x = 1;
            if(isset($service_list) && $service_list):
            foreach($service_list as $service):
                    $cat = $service->s_category;
                    if($cat=='')
                    {
                        $cat_val = 'N/A';
                    }
                    else
                    {
                        $cat_val = $service->s_category;
                    }

                    $des = $service->s_description;
                    if ($des=='') 
                    {
                        $des_val = 'N/A';
                    }
                    else{
                        $des_val = $service->s_description ;                
                    }

                    $mem = $service->s_no_member;
                    if ($mem == '0')
                    {
                        $mem_val = 'N/A';
                    }
                    else{
                        $mem_val = $service->s_no_member;
                    }
                    $tax = $service->s_tax_applied;
                    if ($tax == "--- Select ")
                    {
                        $tax_val = 'N/A';
                    }
                    else{
                        $tax_val = $service->s_tax_applied;
                    }
                    $tax_price = $service->s_tax;
                    if ($tax_price == '0') 
                    {
                        $tax_price_val = 'N/A';
                    }
                    else{
                        $tax_price_val = '₹ '.$service->s_tax;
                    }
                    $disc = $service->s_discount_applied;
                    if ($disc == "--- Select ") 
                    {
                        $disc_val = 'N/A';
                    }
                    else
                    {
                        $disc_val = $service->s_discount_applied;
                    }
                    $disc_prc = $service->s_discount;
                    if ($disc_prc == '0') 
                    {
                        $disc_prc_val = 'N/A';
                    }
                    else
                    {
                        $disc_prc_val = '₹ '.$service->s_discount;
                    }

                    $srv_prc = $service->s_price;
                    if ($srv_prc == '0')
                    {
                        $srv_prc_val = 'N/A';
                    }
                    else
                    {
                        $srv_prc_val = '₹ '.$service->s_price;
                    }

                    $srv_wknd = $service->s_price_weekend;
                    if ($srv_wknd == '0')
                    {
                        $srv_wknd_val = 'N/A';
                    }
                    else
                    {
                        $srv_wknd_val = '₹ '.$service->s_price_weekend;
                    }

                    $srv_holiday = $service->s_price_holiday;
                    if ($srv_holiday == '0')
                    {
                        $srv_hol_val = 'N/A';
                    }
                    else
                    {
                        $srv_hol_val = '₹ '.$service->s_price_holiday;
                    }

                    $srv_spl = $service->s_price_special;
                    if ($srv_spl == '0')
                    {
                        $srv_spl_val = 'N/A';
                    }
                    else
                    {
                        $srv_spl_val = '₹ '.$service->s_price_special;
                    }

                    $srv_prd = $service->s_periodicity;
                    if ($srv_prd == '0')
                    {
                        $srv_prd_val = 'N/A';
                    }
                    else
                    {
                        $srv_prd_val = $service->s_periodicity;
                    }
                ?>
        <tr>
          <td><?php echo $x; ?></td>
          <td><a class="single_2" href="<?php echo base_url();?>upload/service/<?php if( $service->s_image== '') { echo "no_image.png"; } else { echo $service->s_image; }?>"><img src="<?php echo base_url();?>upload/service/<?php if( $service->s_image== '') { echo "no_image.png"; } else { echo $service->s_image; }?>" alt="" style="width:100%;"/></a></td>
          <td><?php echo $service->s_name; ?></td>
          <td><?php echo $cat_val; ?></td>
          <td><?php echo $des_val; ?></td>
          <td><?php echo $mem_val; ?></td>
          <td><?php echo $srv_prd_val; ?></td>
          <td style="text-transform: uppercase; color: #8258FA; text-shadow: -0.3px 0.3px #FF0000;"><?php echo $service->s_rules; ?></td>
          <td><?php echo $srv_prc_val; ?></td>
          <td><?php echo $srv_wknd_val; ?></td>
          <td><?php echo $srv_hol_val; ?></td>
          <td><?php echo $srv_spl_val; ?></td>
          <td><?php echo $tax_val; ?></td>
          <td><?php echo $tax_price_val; ?></td>
          <td><?php echo $disc_val; ?></td>
          <td><?php echo $disc_prc_val; ?></td>
          <td class="ba">
            <div class="btn-group">
              <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $service->s_id?>')" data-toggle="modal" class="btn red btn-sm"><i class="fa fa-trash"></i></a></li>
                <li><a href="<?php echo base_url() ?>dashboard/edit_service?s_id=<?php echo $service->s_id?>" data-toggle="modal" class="btn green btn-sm"><i class="fa fa-edit"></i> </a></li>
              </ul>
            </div>
           </td>
        </tr>
        <?php $x++ ; 
            endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/edit_service?e_id="+id,
                data:{e_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
</script>