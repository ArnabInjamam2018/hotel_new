<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Shifts </div>
    <div class="actions">
    	<a href="<?php echo base_url();?>dashboard/add_shift" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a> 
		<a class="btn btn-circle green btn-outline btn-sm" id="status123" onclick="booking_nature_visit()" href="javascript:void(0);"><span id="demo">Active Shift</span></a>
    </div>
  </div>
  <div class="portlet-body">
    
    <div id="table1"><table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <th scope="col"> Shift Id </th>
          <th scope="col"> Shift Name </th>
          <th scope="col"> Shift Start Time </th>
          <th scope="col"> Shift End Time </th>
          <th scope="col"> Status </th>
          <th scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($shifts1) && $shifts1):
                $i=1;
                foreach($shifts1 as $shift):
                    $class = ($i%2==0) ? "active" : "success";


                    ?>
        <tr  id="row_<?php echo $shift->shift_id;?>">
          <td><?php echo $shift->shift_id ?></td>
          <td><?php echo $shift->shift_name ?></td>
          <td><?php echo $shift->shift_opening_time ?></td>
          <td><?php echo $shift->shift_closing_time ?></td>
		  <td><input type="hidden"  id="hid" value="<?php echo $shift->status;?>">
			 <button <?php  if($shift->status =='5'){
				echo "disabled "; }?> class="btn green btn-xs"  value="<?php echo $shift->shift_id;?>" onclick="status(this.value)"><span id="demostat<?php echo $shift->shift_id;?>"><?php if($shift->status == 1){ echo  "Active";}
else{
echo "Inactive";
}	?></span></button></td>
          <td class="ba" align="center">
          	<div class="btn-group no-bgbut">
			 
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <!--<li><a onclick="soft_delete('<?php //echo $shift->shift_id;?>')" data-toggle="modal" class="btn red btn-sm"><i class="fa fa-trash"></i></a> </li>-->
                <li><a href="<?php echo base_url();?>dashboard/edit_shift/<?php echo $shift->shift_id; ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a> </li>
              </ul>
            </div>
          </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
	</div>
  </div>
</div>
<script>




    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>Dashboard/delete_shift?id="+id,
                data:{id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            $('#row_'+id).remove();

                        });
                }
            });
        });
    }
	
	
	
	function booking_nature_visit(){
		var a=document.getElementById("demo").innerHTML;
		if(a=="Active Shift"){			
			document.getElementById("demo").innerHTML ="Inactive Shift";			
			
		}else{	
			document.getElementById("demo").innerHTML = "Active Shift";
		}
		
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/all_shift_filter",
                data:{status:a},
                 success:function(data)
                {
					//alert($data);
                  $('#table1').html(data);
				   $('#dataTable2').dataTable( {
						//"pageLength": 10 
    } );
	
                }
            });
		
	
	}
	
	function status(id){
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/shift_status",
                data:{id:id},
                success:function(data)
                {
                   // alert(data);
                   // location.reload();
				   document.getElementById("demostat"+id).innerHTML = data.data;
					$('#row_'+id).remove();
                    
					
                }
            });
		
	}
</script> 
