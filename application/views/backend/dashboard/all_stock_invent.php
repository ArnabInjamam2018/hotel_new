 
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption"> <i class="fa fa-file-archive-o"></i>All Stock & Inventory </div>
      <div class="actions">
      		<a href="<?php echo base_url('dashboard/def_stock_invent');?>" class="btn btn-circle btn-outline btn-sm blue-hoki">Define Stock & Inventory</a>
            <?php
            
                    if(isset($logs) && $logs){	 
              ?>
            <a href="<?php echo base_url('dashboard/update_stock_invent');?>" class="btn btn-circle btn-outline btn-sm green"><i class="fa fa-plus-square"></i> Add/ Purchase Stock & Inventory </a> 
            <a href="<?php echo base_url('dashboard/update1_stock_invent');?>" class="btn btn-circle btn-outline btn-sm btn-danger"><i class="fa fa-minus-square"></i> Substract Stock & Inventory</a>
            <?php }?>
      </div>
    </div>
    <div class="portlet-body">
      
      <table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
            <th> # </th>
            <th> Asset Image </th>
            <th>Items </th>
            <th>Type & Category  </th>            
            <th>Item Detail</th>
            <th  style="padding: 0 15px 0 0;"> <table class="table table-hover table-bordered" style="margin:0;">
					<thead>
						<tr>
							  <th style="text-align:right" width="64%"> Warehouse Name </th>
							  <th align="left" width="36%">Qty</th>
							 
						</tr>
						</thead>
						
					</table></th>
            <th  style="width:20%;">Other Info </th>
            <th>Action </th>
          </tr>
        </thead>
        <tbody>
          <?php
                    if(isset($logs) && $logs){
                    $srl_no=0;
                    //print_r($logs);
                    //exit;
                    foreach($logs as $log){
                    $srl_no++;
                  
                
                     
              ?>
          <tr>
            <td><?php echo $srl_no;?></td>
					
            <td width="5%"><a class="single_2" href="<?php echo base_url();?>upload/asset/<?php if( $log->a_image== '') { echo "no_images.png"; } else { echo $log->a_image; }?>"><img src="<?php echo base_url();?>upload/asset/<?php if( $log->a_image== '') { echo "no_images.png"; } else { echo $log->a_image; }?>" alt="" style="width:100%;"/></a></td>
            <td>
                <?php // Item
                    echo '<strong style="font-size:15px;">'.$log->a_name.'</strong>';
                    if($log->make != '')
                        echo '<br><span style="color:#007F7F;"> ('.$log->make.')</span>';
                ?>
            </td>
            <td>
                <?php // Type & Category
                    echo $log->item_type.' &nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;';
                    echo '<span class="label-flat" style="background-color:#E6E6FA; color:#000000;">'.$log->a_category.'</span>';
                ?>
            </td>
            <td>
					<?php // Item Details
                        if($log->u_price != '')
                            echo '<strong>Unit price:</strong> INR '.$log->u_price.'<br>';
                        if($log->qty != ''){
                          if($log->qty<$log->l_stock)
                            echo '<Span class="label glow" style="background-color:#E52828; color:#ffffff;";>Low Qty in stock:  </span> '.$log->qty.' '.$log->unit.'<br>';
                          else
                            echo '<strong>Qty in stock: </strong>'.$log->qty.' '.$log->unit.'<br>';
                        }	
                        if($log->l_stock != '')
                            echo '<strong>Low stock qty: </strong>'.$log->l_stock.' '.$log->unit;
                    ?>
            </td>
            <td>
					<table class="table table-bordered" style="margin:0;">
						<?php 
							$wharehouse=$this->dashboard_model->get_warehouse_details();
							if(isset($wharehouse) && $wharehouse){
								$stock_id=$this->dashboard_model->get_stock_id($log->a_name);
								if($stock_id != false){
								foreach($wharehouse as $wh){
									
									if($stock_id->hotel_stock_inventory_id == $wh->item_id){
										if($wh->qty != 0){
						?>
						
						<tr>
							<td align="right" width="60%" style="padding:0 8px">
								<?php // Warehouse name
								
								//echo $wh->warehouse_id;
								$wharehouse=$this->dashboard_model->get_warehouse( $wh->warehouse_id);
								if(isset($wharehouse) && $wharehouse){
									echo '<span style="color:#000C49">'.$wharehouse->name.': </span>';
								}
								?>
							</td>
							<td align="left" width="40%" style="padding:0 8px">
								<?php 
									echo $wh->qty.' <span style="color:#AAAAAA;">'.$log->unit.'</span>';
								?>
							</td>
							
						</tr>
					
						<?php				
									}
								}
							}}}
							//print_r($wharehouse);
						?>
					</table>
				</td>
            <td>
						<?php // Item Details
						echo "<strong>Importance: </strong>";
							if(isset($log->importance)){
								
									if($log->importance==1) {
										echo '<span class="label-flat" style="background-color:#3E3274; color:#ffffff;">High</span>';
										}
									else if($log->importance==2){
										echo '<span class="label-flat" style="background-color:#F6990A; color:#ffffff;">Medium</span>';
									}
									else if($log->importance==3){
										echo '<span class="label-flat" style="background-color:#52C6C9; color:#ffffff;">Low</span>';
									}
								
							}
							else
								echo '<span class="label-flat" style="background-color:#D2BDB3; color:#ffffff;">No Importance</span>';
							echo '<br>';
								
							
							if($log->note != '')
								echo "<strong>Note: </strong>".$log->note;
							if($log->item_des != '')
								echo "<br><strong>Desc:</strong> ".$log->item_des;
						?>
					</td>
            <td class="ba">
            	<div class="btn-group">
                  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li><a onclick="soft_delete('<?php echo $log->hotel_stock_inventory_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a> </li>
                    <li><a href="<?php echo base_url() ?>dashboard/edit_asset_stock?a_id=<?php echo $log->hotel_stock_inventory_id;?>" data-toggle="modal"  class="btn green btn-xs"><i class="fa fa-edit"></i></a> </li>
                  </ul>
                </div>
            </td>
          </tr>
          <?php }?>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">Items</h4>
        </div>
        <div class="modal-body">
          <div class="form-group form-md-line-input form-md-floating-label col-md-4" >
            <input type="text" autocomplete="off" required="required" name="date" class="form-control date-picker"  id="c_valid_from" >
            <label> Asset Category <span class="required">*</span></label>
            <label for="form_control_3"></label>
            <span class="help-block">Enter Date... </span> </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-4" >
            <input type="text" autocomplete="off" required="required" name="date" class="form-control date-picker"  id="c_valid_from" >
            <label> Asset name <span class="required">*</span></label>
            <label for="form_control_3"></label>
            <span class="help-block">Enter Date... </span> </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-4" >
            <input type="text" autocomplete="off" required="required" name="date" class="form-control date-picker"  id="c_valid_from" >
            <label> Make <span class="required">*</span></label>
            <label for="form_control_3"></label>
            <span class="help-block">Enter Date... </span> </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-4" >
            <input type="text" autocomplete="off" required="required" name="date" class="form-control date-picker"  id="c_valid_from" >
            <label> Description <span class="required">*</span></label>
            <label for="form_control_3"></label>
            <span class="help-block">Enter Date... </span> </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-4" >
            <input type="text" autocomplete="off" required="required" name="date" class="form-control date-picker"  id="c_valid_from" >
            <label> Unit <span class="required">*</span></label>
            <label for="form_control_3"></label>
            <span class="help-block">Enter Unit... </span> </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-4" >
            <input type="text" autocomplete="off" required="required" name="date" class="form-control date-picker"  id="c_valid_from" >
            <label> Unit Price <span class="required">*</span></label>
            <label for="form_control_3"></label>
            <span class="help-block">Enter Unit Price... </span> </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-4" >
            <input type="text" autocomplete="off" required="required" name="date" class="form-control date-picker"  id="c_valid_from" >
            <label> Qty <span class="required">*</span></label>
            <label for="form_control_3"></label>
            <span class="help-block">Enter Qty... </span> </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-4" >
            <input type="text" autocomplete="off" required="required" name="date" class="form-control date-picker"  id="c_valid_from" >
            <label> Warehouse <span class="required">*</span></label>
            <label for="form_control_3"></label>
            <span class="help-block">Warehouse... </span> </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-4" >
            <input type="text" autocomplete="off" required="required" name="date" class="form-control date-picker"  id="c_valid_from" >
            <label> Low stock <span class="required">*</span></label>
            <label for="form_control_3"></label>
            <span class="help-block">Low stock... </span> </div>
        </div>
        <div id="mdl1"> </div>
        <div class="modal-footer">
          <button type="button" class="btn default" data-dismiss="modal">Close</button>
          <button type="button" class="btn blue" onclick="update_stock_invent()">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
    
  </div>
<div id="basic1" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form1 = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('unit_class_controller/edit_unit_class',$form1);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Stock Inventory</h4>
      </div>
      <input type="hidden" name="hid1" id="hid1">
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="unit_class1" id="item_type" placeholder="Item_type" required="required">
                <span class="help-block">Item_type</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="unit_class1" id="a_category" placeholder="a_category" required="required">
                <span class="help-block">a_category</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="unit_class1" id="a_name" placeholder="a_name" required="required">
                <span class="help-block">a_name</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="unit_class1" id="make" placeholder="make" required="required">
                <span class="help-block">make</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="unit_class1" id="item_des" placeholder="item_des" required="required">
                <span class="help-block">item_des</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="unit_class1" id="u_price" placeholder="u_price" required="required">
                <span class="help-block">u_price</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <textarea class="form-control focus" row="0" name="desc1" id="unit" placeholder="unit"></textarea>
                <span class="help-block">unit</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="mode_of_payment" placeholder="mode_of_payment" required="required">
                <span class="help-block"> mode_of_payment</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="check_no" placeholder="check_no" required="required">
                <span class="help-block">check_no</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="check_bank_name" placeholder="check_bank_name" required="required">
                <span class="help-block">check_bank_name</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="draft_no" placeholder="draft_no" required="required">
                <span class="help-block">draft_no</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="draft_bank_name" placeholder="draft_bank_name" required="required">
                <span class="help-block">draft_bank_name</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="ft_bank_name" placeholder="ft_bank_name" required="required">
                <span class="help-block">ft_bank_name</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="ft_account_no" placeholder="ft_account_no" required="required">
                <span class="help-block">ft_account_no</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="ft_ifsc_code" placeholder="ft_ifsc_code" required="required">
                <span class="help-block">ft_ifsc_code</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="qty" placeholder="qty" required="required">
                <span class="help-block"> qty</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="opening_qty" placeholder="opening_qty" required="required">
                <span class="help-block"> opening_qty</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="closing_qty" placeholder="closing_qty" required="required">
                <span class="help-block"> closing_qty</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="warehouse" placeholder="warehouse" required="required">
                <span class="help-block"> warehouse</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="l_stock" placeholder="l_stock" required="required">
                <span class="help-block"> l_stock</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="note" placeholder="note" required="required">
                <span class="help-block"> note</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="importance" placeholder="importance" required="required">
                <span class="help-block"> importance</span> </div>
              <div class="form-group form-md-line-input col-md-6">
                <input type="text" class="form-control focus" name="mode_of_payment" id="a_image" placeholder="a_image" required="required">
                <span class="help-block">a_image</span> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<script>
function show(id){
	
	$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/get_stock_inventory",
                data:{id:id},
                success:function(data)
                {
                   // alert(data);

				   
                   $('#hotel_stock_inventory_id').val(data.hotel_stock_inventory_id);
                   $('#item_type').val(data.item_type);
                   $('#a_category').val(data.a_category);
                   $('#a_name').val(data.a_name);
                   $('#make').val(data.make);
                   $('#item_des').val(data.item_des);
                   $('#u_price').val(data.u_price);
                   $('#unit').val(data.unit);
                   //$('#mode_of_payment').val(data.mode_of_payment);
                   //$('#check_no').val(data.check_no);
                   //$('#check_bank_name').val(data.check_bank_name);
                   //$('#draft_no').val(data.draft_no);
                  // $('#draft_bank_name').val(data.draft_bank_name);
                   //$('#ft_bank_name').val(data.ft_bank_name);
                   //$('#ft_bank_name').val(data.ft_bank_name);
                   //$('#ft_account_no').val(data.ft_account_no);
                   //$('#ft_ifsc_code').val(data.ft_ifsc_code);
                   $('#qty').val(data.qty);
                   $('#opening_qty').val(data.opening_qty);
                   $('#closing_qty').val(data.closing_qty);
                   $('#warehouse').val(data.warehouse);
                   $('#l_stock').val(data.l_stock);
                   $('#note').val(data.note);
                   $('#importance').val(data.importance);
                   //$('#a_image').val(data.a_image);
				   
					
					
                }
            });
	$('#basic1').modal('toggle');
}

function update_stock_invent(){
	
}







function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_stock_invent?a_id="+id,
                data:{a_id:id},
                success:function(data)
                {
                  
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
</script> 
