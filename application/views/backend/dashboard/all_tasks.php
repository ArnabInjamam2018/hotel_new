
<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Tasks </div>
    <div class="actions">
    	<a class="btn btn-circle green btn-outline btn-sm" data-toggle="modal" href="#responsive"> <i class="fa fa-plus"></i>Add Task </a>
    </div>
    <div class="colr">
	  <ul >
		<li>
		  <span class="brdr " style="background:#f3565d;"></span>
		  High</li>
		&nbsp;&nbsp;
		<li>
		  <span class="brdr " style="background:#428bca;"></span>
		  Medium</li>
		&nbsp;&nbsp;
		<li>
		  <span class="brdr " style="background:#45b6af;"></span>
		  Low</li>
	  </ul>
	</div>
  </div>
  <div class="portlet-body">
    
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr>
          <th scope="col" aria-sort="descending"> Status </th>
          <th scope="col"> Task </th>
         <th scope="col"> Given on </th>
          <th scope="col"> Due Date </th>
          <!--<th scope="col"> Id </th>-->

          <th scope="col"> Assigned By </th>
          <th width="8%" scope="col"> Assigned To </th>
          <th width="5%" scope="col"> Task Priority </th>
        <!--  <th scope="col"> Task Details </th> -->
        </tr>
      </thead>
      <tbody>
        <?php // Status
                        $count = 1;
                        if(isset($tasks_pending) && $tasks_pending):
					        foreach($tasks_pending as $tasks_pending):
                            $task_status = $tasks_pending->status;
                            if ($task_status == "0") {
                                $task_status_desc = "Pending";
                            }
                            else{
                                $task_status_desc = "Completed";
                            }

                            $priority = $tasks_pending->priority;
                            $from = $tasks_pending->from_id;
                            $to = $tasks_pending->to_id;

                            $from_details = $this->dashboard_model->get_task_user($from);
                            $to_details = $this->dashboard_model->get_task_user($to);

                            if ($priority == '1')
                            {
                                $color = "#3E3274";
                                $priority = 'High';
                            }
                            else if ($priority == '2')
                            {
                                $color = "#F6990A";
                                $priority = 'Medium';
                            }
                            else
                            {
                                $color = "#52C6C9";
                                $priority = 'Low';
                            }

                    ?>
        <tr>
          <td align="center" style="vertical-align:top; padding-top: 13px;">
            <span <?php if($task_status==0){echo "class='fa fa-check-circle-o' style='color:#B7C5C5; font-size:20px;'";}
                else
                {echo "class='fa fa-check-circle-o' style='color:#7DCA90; font-size:20px;'";};
            ?>></span>
			<?php //echo $task_status_desc; ?>            
          </td>
        <td> <!-- Task -->
            <strong style="font-size:15px;">
            <a href="<?php echo base_url(); ?>dashboard/task_details?task_id=<?php echo $tasks_pending->id ;?>&task_asignee=<?php echo $tasks_pending->to_id; ?> "><?php

              echo  $tasks_pending->title;
                          ?></a></strong>
                          <?php
                            echo '<br>'.$tasks_pending->task;
                          ?>
        </td>

        <td><?php echo date("d-M-y",strtotime($tasks_pending->added_date));?></td>
          <td><?php echo date("d-M-y",strtotime($tasks_pending->due_date));?></td>
        <!--<td><?php echo $count; ?></td>-->

          <td>
            <?php

                $admin_name = $this->dashboard_model->get_admin($this->session->userdata('user_id'));
				//echo $admin_name; exit;
if(isset($admin_name) && $admin_name)  {             
			   foreach($admin_name as $a_name){
                 $admin=$a_name->admin_first_name .' '. $a_name->admin_middle_name .' '. $a_name->admin_last_name;
                }
}
                if(isset($from_details) && $from_details)
                                {
                                    foreach ($from_details as $from_details)
                                    {
                                   // if($from_details->admin_first_name .' '. $from_details->admin_middle_name .' '. $from_details->admin_last_name==$admin){
                                         //   echo "You";
                                           // }else{
                                                echo $from_details->admin_first_name .' '. $from_details->admin_middle_name .' '. $from_details->admin_last_name;
                                           // }

                                    }
                                }
                                ?>
            </td>
            <td>
              <?php
                  if(isset($to_details) && $to_details)
                  {
                      foreach ($to_details as $to_details)
                      {
                       //   if($to_details->admin_first_name .' '. $to_details->admin_middle_name .' '. $to_details->admin_last_name==$admin){
                         //   echo "You";
                         // }
                      //    else{
                            echo $to_details->admin_first_name .' '. $to_details->admin_middle_name .' '. $to_details->admin_last_name;
                      //    }
                      }
                  }

              ?>
		        </td>
            <td>
                <span class="label label-sm" style="background-color:<?php echo $color; ?>"><?php echo $priority; ?> </span>
            </td>
            <!--<td>
                <?php
                  echo $tasks_pending->task;
                ?>
            </td>    -->
        </tr>
        <?php
                        $count ++;
                        endforeach;
                        endif;
                    ?>
      </tbody>
    </table>
  </div>
</div>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
    <?php

                $form = array(
                    'class'             => 'form-body',
                    'id'                => 'form',
                    'method'            => 'post',

                );
                echo form_open_multipart('dashboard/add_task',$form);
            ?>
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
      <h4 class="modal-title">Assign a Task</h4>
    </div>
    <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group form-md-line-input">
              <input type="text" autocomplete="off" name="title" class="form-control" id="title" placeholder="Title *" required="required">
              <label></label><span class="help-block">Title *</span>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group form-md-line-input">
                <select name="asign_from" id="booking_rent" class="form-control bs-select" required>
                  <option value="" disabled selected>Assigner *</option>
                  <option value="<?php echo $this->session->userdata('user_id'); ?>"> <?php echo $this->session->userdata('user_name'); ?></option>
                </select>
                <label></label><span class="help-block">Assign From *</span>
            </div>
          </div>
          <div class="col-md-6">
             <div class="form-group form-md-line-input">
                <select name="asign_to" id="booking_rent" class="form-control bs-select" required>
                  <option value="" disabled selected>Assignee *</option>
                  <?php
                                                    if(isset($tasks_assigee) && $tasks_assigee):
                                                    foreach($tasks_assigee as $tasks_assigee):
                                                ?>
                  <option value="<?php echo $tasks_assigee->admin_id; ?>"> <?php echo $tasks_assigee->admin_first_name .' '. $tasks_assigee->admin_middle_name .' '. $tasks_assigee->admin_last_name; ?></option>
                  <?php
                                                    endforeach;
                                                    endif;
                                                ?>
                </select>
                <label></label><span class="help-block">Assign To *</span>
             </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group form-md-line-input">
                <select name="task_priority" id="task_priority" class="form-control bs-select" required>
                  <option value="" disabled selected>Select Prority</option>
                  <option value="1">High</option>
                  <option value="2">Medium</option>
                  <option value="3">Low</option>
                </select>
                <label></label><span class="help-block">Priority *</span>
             </div>
           </div>
           <div class="col-md-6">
            <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" name="due_date" class="form-control date-picker" id="due_date" required="required" placeholder="Due Date *">
                <label></label><span class="help-block">Due Date *</span>
              </div>
            </div>
			<div class="col-md-12">
            <div class="form-group form-md-line-input">
                <textarea name="task_desc" class="form-control" rows="6" required placeholder="Task Description *"></textarea>
                <label></label><span class="help-block">Task Description *</span>
            </div>
          </div>
			<div class="col-md-12">
            
            </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn default">Close</button>
      <button type="submit" class="btn green">Assign Task</button>
    </div>
    <?php form_close(); ?>
  </div>
</div>
</div>
