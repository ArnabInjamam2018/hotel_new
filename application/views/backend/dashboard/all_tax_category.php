<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>

<div class="portlet light borderd">
  <div class="portlet-title">
	<div class="caption"> <i class="fa fa-edit"></i>List of All Tax Category </div>
	<div class="actions"> 
		<a href="<?php echo base_url();?>dashboard/tax_type_category_add" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
	</div>
	</div>
	<div class="portlet-body">
	  
	  <table class="table table-striped table-bordered table-hover" id="sample_1">
		<thead>
		  <tr>
			<th width="2%" scope="col">Sl </th>
			<th scope="col">Category Name </th>
			<th scope="col">Category Type </th>
			<th scope="col">Description </th>
			<th scope="col">Tax Plans </th>
			<!--<th scope="col"> Unit Type </th>
			<th scope="col"> Unit Class </th>
			<th scope="col" width="400"> Unit Description </th>-->
			<th width="2%" align="center" scope="col"> Action </th>
		  </tr>
		</thead>
		<tbody>
		
		
		  <?php
		  
		  //print_r($tax_category);
 
					if(isset($tax_category) && $tax_category ){
					 $sl =0;
					  foreach($tax_category as $gst){
						$sl++;  
						  ?>
		  <tr id="row_<?php echo $gst->tc_id;?>">
			<td><?php echo $sl;?></td>
			<td><?php echo $gst->tc_name; ?></td>
			
			<td><?php  echo $gst->tc_type; ?></td>
			<td><?php echo $gst->tc_desc; ?></td>
			<td><!--<input type="button" name="cat_details"  id="cat_details" value="CategoryDetails" class="cat_details" onclick="tax_category('<?php //echo $gst->tc_type;?>')">-->
			<a href="<?php  echo base_url(); ?>dashboard/all_tax_rule/<?php echo $gst->tc_id; ?>" <button class="btn purplene btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
			</td>
			<td align="center" class="ba">
            	<div class="btn-group">
                  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
				  
				  <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $gst->tc_id;?>')" data-toggle="modal"  class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
               <li><a onclick="edit_tax_category('<?php echo $gst->tc_id;?>')" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li>
              </ul>
                </div>	
			</td>				
		  </tr>
		  <?php
					  }
					}  
					  ?>
		</tbody>
	  </table>
	</div>
</div>

<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Tax Category Settings</h4>
      </div>
       <input type="hidden"  name="taxid" id="taxid"  value=''>

      <div class="modal-body">
        <div class="row">
		   <div class="col-md-4">
            <div class="form-group form-md-line-input">
             <input type="text" class="form-control" name="tc_name" id="tc_name" required placeholder="Tax Name">

			 
			
              <label></label>
              <span class="help-block">Tax Name..</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
             
			  <!--<input type="text" class="form-control" name="tc_type" id="tc_type" required placeholder="Tax Name">-->
			<select class="form-control"  name="tc_type" id="tc_type">
					 
					  <option value="Booking">Booking</option>
					  <option value="Meal Plan">Meal Plan</option>
					  <option value="Extra Charge">Extra Charge</option>
					  <option value="Extra Service">Extra Service</option>
					  <option value="POS">POS</option>
					  <option value="Misc">Misc</option>
					 
					</select>
              <label></label>
              <span class="help-block">Tax Category Type *</span> </div>
          </div>
		  <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="tc_desc" id="tc_desc" required placeholder="Tax Name">
			   
              <label></label>
              <span class="help-block">Tax Description *</span> </div>
          </div>
		  
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="button" class="btn green" onclick="update_tax()">Save</button>
      </div>
       </div>
  </div>
</div>

<script>
 
 
 function soft_delete(tax_id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_tax_category",
                data:{taxname:tax_id},
                success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                           $('#row_'+id).remove();
						   //location.reload();

                        });
                }
            });



        });
    }
 
	</script>
	<script>
	
	function update_tax(){
		//alert('hello');
		var id=$('#taxid').val();
		var name=$('#tc_name').val();
		var tax_desc=$('#tc_desc').val();
		var tax_types=$('#tc_type').val();
		//alert(id + '' +name + '' +tax_desc + '' +tax_types);
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/update_tax_category",
                data:{taxid:id,taxname:name,tax_descrp:tax_desc,tax_cate_type:tax_types},
                success:function(data)
                {
					//alert(data);
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
	
                    $('#responsive').modal('hide');
					
                }
            });
		
	}
</script>

<script>
function edit_tax_category(tc_id){
		//alert(tc_id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/edit_tax_category",
                data:{"taxid":tc_id},
                success:function(data)
                {
                   // alert(data.unit_price_editable);
				// $('#name').append( '<option value="'+data.charge_name+'">'+data.charge_name+'</option>' );
				//alert(data);
				//console.log(data);
				
				 $('#tc_name').val(data.tax_category_name);
				  $('#taxid').val(data.tax_id);
				   $('#tc_type').val(data.tax_category_type).change();
				   $('#tc_desc').val(data.tax_desc);
				   
                    $('#responsive').modal('toggle');
					
                }
            });
	}
</script>


<script>
/*
function tax_category(tc_name){
	
	
		alert(tc_name);
		$.ajax({
			
			type:"POST",
			url:"<?php  echo base_url(); ?>dashboard/all_tax_rule/tax_rules",
			data:{tc_name:tc_name},
			success:function(data){
				
				alert(data);
				
				
			},
			
		})
		
	
	
}
</script>

