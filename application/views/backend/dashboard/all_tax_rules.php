<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light borderd">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Tax Rules</div>
    <div class="actions">
    	<a href="<?php echo base_url();?>setting/add_tax_rule" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <!-- <th scope="col">
                            Select
                        </th>-->
          
           <th scope="col">#</th>
		   <th scope="col">Tax Rule Name</th>
          <th scope="col">Category</th>
          
          <th scope="col">Description </th>
          <th scope="col">Start Price </th>
          <th scope="col">End Price</th>
          <th scope="col">Start Date </th>
          <th scope="col">End Date </th>
		   <th scope="col"> Elements </th>
          <th scope="col"> Action </th>
		 
        </tr>
      </thead>
      <tbody>
        <?php if(isset($tax_rule) && $tax_rule):
                       // print_r($tax_rule); exit;
                        $i=0;
                        foreach($tax_rule as $rule):
                            $class = ($i%2==0) ? "active" : "success";
                           $i++;
                            ?>
        <tr id="r_id_<?php echo $rule->r_id;?>" style= "text-transform:capitalize;"> 
         <?php
			$catename = $this->dashboard_model->get_specific_tax_category($rule->r_cat);
			//print_r($catename);
			//die();
			//$tc_name = 
		 ?>
          
          <td ><?php echo $i; ?></td>
		  <td><?php echo '<span style="color: #009688; font-weight:bold; font-size:16px;">'.$rule->r_name.'</span>';?></td>
          <td><?php if(isset($catename) && $catename ) echo $catename->tc_name; ?></td>          
          <td><?php echo $rule->r_desc;?></td>
          <td><?php echo $rule->r_start_price;?></td>
          <td><?php echo $rule->r_end_price;?></td>
          <td>
			<?php 
				echo date('d-M-Y',strtotime($rule->r_start_date));
			?>
		  </td>
          <td><?php echo date('d-M-Y',strtotime($rule->r_end_date));?></td>
		  <td>
		  <?php 
			   $tax_set =  $this->dashboard_model->get_all_tax_rule_component_by_rule_id($rule->r_id);
			   if($tax_set != false)
			   {
				$cls = 'label-primary';
				foreach($tax_set as $t_s)
				{
					$res_tax_name = $this->dashboard_model->get_tax_type_by_id($t_s->tax_type);
					if($res_tax_name)
					{
						$tax_name = $res_tax_name->tax_name;
					}
					else
					{
						$tax_name = "Undefined Tax";
						$cls = 'label-danger';
					}
	
					if($t_s->tax_mode == 'p')
					{
						$tax_mode = "percent";
						$tmCol = "#009688";
					}					
					else
					{
						$tax_mode = "inr";
						$tmCol = "#3F51B5";
					}
					
					if($t_s->calculated_on == 'n_p'){
						$vCol = '#FF5151';
					}
					else
						$vCol = '#607D8B';
					
					echo "<span class='badge label-info' style='padding:0px; margin:2px;'><span class='badge ".$cls."'>".$tax_name."</span><span style='background-color:".$vCol.";' class='badge label-warning'>".$t_s->tax_value."</span><span style='background-color:".$tmCol.";' class='badge fa fa-".$tax_mode."'></span></span><br>";
					
					
				}			
			   }
		   ?></td>
         
         
          <td class="ba">
            <div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $rule->r_id;?>')" data-toggle="modal"  class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
               <li><a href="<?php echo base_url();?>dashboard/edit_tax_rule/<?php echo $rule->r_id;?>" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li>
              </ul>
            </div>
          </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>

<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_tax_rule?r_id="+id,
                data:{r_id:id},
                success:function(data)
                {
                    //alert(data.data);
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){
							$('#r_id_'+id).remove();
                          
                        });
                }
            });



        });
    }
	
	
	
</script> 
