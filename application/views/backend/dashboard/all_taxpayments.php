<?php if($this->session->flashdata('err_msg')):?>

<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Tax Payments </div>
    <div class="tools"> 
      <!--<a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>--> 
      <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-8"> <a href="<?php echo base_url('dashboard/add_bill_payments');?>" class="btn green"> Add New <i class="fa fa-plus"></i></a> <a href="<?php echo base_url('dashboard/all_salPayments');?>" class="btn blue-hoki"> Salary Payments <i class="fa"></i></a> <a href="<?php echo base_url('dashboard/all_misPayments');?>" class="btn purple"> Miscelleneous Payments <i class="fa"></i></a> <a href="<?php echo base_url('dashboard/all_billPayments');?>" class="btn blue"> Bill Payments <i class="fa"></i></a> </div>
        <div class="col-md-4">
          <div class="btn-group pull-right">
            <button class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-download"></i> </button>
            <ul class="dropdown-menu pull-right">
              <li> <a href="javascript:;"> Print </a> </li>
              <li> <a href="javascript:;"> Save as PDF </a> </li>
              <!-- <li> <a href="javascript:;"> Export to Excel </a> </li>-->
            </ul>
          </div>
        </div>
      </div>
    </div>
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <!-- <th scope="col">
                                Select
                            </th>-->
          <th scope="col"> Date </th>
          <th scope="col"> Type of payment </th>
          <th scope="col"> Payment To </th>
           <th scope="col"> Month </th>
          <th scope="col"> Tax Type </th>
          <th scope="col"> Others </th>
          <th scope="col"> Description </th>
          <th scope="col"> Profit Center </th>
          <th scope="col"> Mode Of Payment </th>
          <th scope="col"> Check/Draft No./Fund Transfer No. </th>
          <th scope="col"> Bank Name </th>
          <th scope="col"> Account Number </th>
          <th scope="col"> IFSC code No. </th>
          <th scope="col"> Approved By </th>
          <th scope="col"> Paid By </th>
          <th scope="col"> Admin </th>
          <th scope="col"> Reciever's Name </th>
          <th scope="col"> Reciever's Signature </th>
          <th scope="col"> Action </th>
          
          <!--<th scope="col">
                                Id Proof
                            </th>--> 
          <!-- <th scope="col">
                                Action
                            </th> --> 
        </tr>
      </thead>
      <tbody>
        <?php if(isset($salpay) && $salpay):
							
                            $i=1;//var_dump($salpay);die;
                            foreach($salpay as $sp):
                                $class = ($i%2==0) ? "active" : "success";
                                $sal_pay_id=$sp->hotel_payment_id;
                                ?>
        <tr> 
          <!-- <td width="50">
                                        <div class="md-checkbox pull-left">
                                            <input type="checkbox" id="checkbox1" class="md-check">
                                            <label for="checkbox1">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                            </label>
                                        </div>
                                    </td>--> 
          
          <!-- <td>
                                   
                                    // <img  width="60px" height="60px" src="<?php echo base_url();?>upload/guest/<?php //if( $gst->g_photo_thumb== '') { echo "no_images.png"; } else { echo $gst->g_photo_thumb; }?>" alt=""/>
                                    
                                    
                                    <?php
										/*echo $gst->g_photo ;
										echo $gst->g_photo ."_thumb"; */
									?>
                                    
                                    </td> -->
          
          <td><?php echo $sp->date; ?></td>
          <td><?php echo $sp->type_of_payment;?></td>
          <td><?php echo $sp->payment_to; ?></td>
          <td><?php echo $sp->tax_month; ?></td>
          <td><?php echo $sp->tax_type; ?></td>
          <td><?php echo $sp->tax_other; ?></td>
          <td><?php echo $sp->description; ?></td>
          <td><?php if(isset($sp->profit_center)){echo $sp->profit_center;} ?></td>
          <td><?php echo $sp->mode_of_payment; ?></td>
          <td><?php if($sp->check_no!=""){
					echo $sp->check_no;
					}elseif($sp->draft_no){
		
				 echo $sp->draft_no;
				}elseif($sp->ft_account_no){
						echo $sp->ft_account_no."</br>";
						echo "<center>IFSC- ".$sp->ft_ifsc_code."<center>";
					}
				?></td>
          <td><?php if($sp->check_bank_name!=""){
						echo $sp->check_bank_name;
						}elseif($sp->draft_bank_name){
						echo $sp->draft_bank_name; 
						}elseif($sp->ft_bank_name){
						echo $sp->ft_bank_name; 
						}
						?></td>
          <td><?php echo $sp->ft_account_no; ?></td>
          <td><?php echo $sp->ft_ifsc_code; ?></td>
          <td><?php echo $sp->approved_by; ?></td>
          <td><?php echo $sp->paid_by; ?></td>
          <td><?php echo $sp->admin; ?></td>
          <td><?php echo $sp->recievers_name; ?></td>
          <td><?php echo $sp->recievers_desig; ?></td>
          <?php //echo $gst->g_photo ?>
          
          <!--<td>
                                        <img  width="100%" src="<?php //echo base_url();?>upload/<?php //if( $gst->g_id_proof== '') { echo "no_images.png"; } else { echo $gst->g_id_proof; }?>" alt=""/>
										<?php //echo $gst->g_id_proof ?>
                                    </td>-->
          
          <td align="center" class="ba">
          	<div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="<?php echo base_url() ?>dashboard/delete_taxpayments?taxpay_id=<?php echo $sp->hotel_payment_id;?>" class="btn red btn-xs" data-toggle="modal"><i class="fa fa-trash"></i></a></li>
                <li><a href="<?php echo base_url();?>dashboard/edit_bill_payments?p_id=<?php echo $sp->hotel_payment_id; ?>" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li>
              </ul>
          </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
