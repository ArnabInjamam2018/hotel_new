<style type="text/css" media="print">
    .portlet, .modal-footer{
		display:none;
    }
	#pay_recip{
		width: 500px;
		margin: 0 auto;
	}
</style>

	
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-file-archive-o"></i>List of All Transactions </div>
        <div class="actions">
        <!--	<a href="<?php //echo base_url();?>dashboard/add_transaction" class="btn btn-circle green btn-outline btn-sm disabled"> <i class="fa fa-plus"></i>Add New </a>-->
        </div>
      </div>
      <div class="portlet-body">
		 <?php  
			$tpa = 0;
	   ?>
        <table class="table table-striped table-hover table-bordered" id="sample_1">
			  <thead>
				<tr>
				  <th width="3%">#</th>	  
				  <th width="10%"> Transaction Id </th>
				  <th width="8%"> Date and Time </th>
				  <th width="10%"> Status </th>
				  <th width="10%"> User </th>
				  <th width="8%"> Profit Center </th>
				  <th width="10%"> Type </th>
				  <th class="none"> Transaction Details </th>
				  <th class="none"> Releted Id </th>
				  <th width="8%" class=""> Amount </th>              
				  <th width="10%" class=""> Payment Mode </th>
				  <th width="10%" class=""> Payment Details </th>
				  <th width="10%" class=""> Payment From </th>
				  <th width="10%" class=""> Payment to </th>
				  <th width="10%" class="none"> Transaction Note </th>
				  <th width="10%" class="none"> Hotel Name </th>
				  <th width="5%"> Action </th>             
				</tr>
			  </thead>
			<tbody>
            <?php if(isset($transactions) && $transactions){
						/*echo "<pre>";
						print_r($transactions); exit;*/

					   	$i=1;
                        $deposit=0;
						$sl=0;
						$adminName = '';

						$hotel_id = $this->session->userdata('user_hotel');
							if(isset($hotel_id) && $hotel_id > 0) {
								$hotel_name = $this->dashboard_model->get_hotel($hotel_id);
								$hotel_contact = $this->bookings_model->get_hotel_contact_details($hotel_id);
							}
							else
								$hotel_name = 'Hotel/ Lodge'; 
	

                        foreach($transactions as $transaction):

							//echo $transactions->hotel_id;
							
							$sl=$sl+1;
                            $class = ($i%2==0) ? "active" : "success";
							$d = date("d",strtotime($transaction->t_date)).date("m",strtotime($transaction->t_date)).date("y",strtotime($transaction->t_date));
                            $transaction_id='TA0'.$this->session->userdata('user_hotel').'/'.$d.'/'.$transaction->t_id;

							if($transaction->t_payment_mode == 'cash')
								$bg = '#FFFDD3';
							else
								$bg = '#F2F2F2';
							
                            ?>                         
			  <tr style="background: <?php echo $bg; ?>">
              	<td><?php echo $sl; ?></td>
				<td style="text-align: left;" valign="left">
				<a href="<?php echo base_url();?>Dashboard/edit_transaction/<?php echo $transaction->t_id; ?>" style="color:#242493;" data-toggle="modal">
				<?php 
					$dec = 'none';
					if($transaction->t_status == 'Cancel')
						$dec = 'line-through';
					$d = date("d",strtotime($transaction->t_date)).date("m",strtotime($transaction->t_date)).date("y",strtotime($transaction->t_date));
                    $transaction_id='TA0'.$transaction->hotel_id.'/'.$d.'/'.$transaction->t_id;
					echo '<span style="text-decoration:'.$dec.'">'.$transaction_id.'</span>';
					if($transaction->t_status == 'Done'){
						echo ' <i style="color:#9BCA3B;" class="fa fa-check" aria-hidden="true"></i>';
						$tpa = $tpa + $transaction->t_amount;
					}
					if($transaction->t_status == 'Pending')
						echo ' <i style="color:#FFAF4A;" class="fa fa-hourglass-start" aria-hidden="true"></i>';
					if($transaction->t_status == 'Cancel')
						echo ' <i style="color:#F5695E;" class="fa fa-times" aria-hidden="true"></i>';
				?>
				</a>
			  </td>
			  
			  <td id="tdate<?php echo $sl; ?>">
				<?php 
					echo date("g:i A \-\n l jS F Y",strtotime($transaction->t_date));
				?>
			  </td>
			  
			  <td>
				<?php 
					if($transaction->t_status == 'Done')
						echo '<sapn style="background-color:#9BCA3B;" class="label">Recieved</span>';
					if($transaction->t_status == 'Pending')
						echo '<sapn style="background-color:#FFAF4A;" class="label">'.$transaction->t_status.'</span>';
					if($transaction->t_status == 'Cancel')
						echo '<sapn style="background-color:#F5695E;" class="label">Delcined</span>';
				?>
			  </td>
			  
			  <td>
				<?php 
					if($transaction->user_id > 0) {
						$user = $this->dashboard_model->get_admin($transaction->user_id);
					
						if(isset($user)) {
							foreach($user as $user) {
								$adminName = $user->admin_first_name.' '.$user->admin_last_name;
								echo $adminName;
							}
						}
					}					
					else
						echo '<span style="color:#AAAAAA;">No info</span>';
					
				?>
			  </td>
			  
			  <td>
				<?php 
					echo $transaction->p_center; 
					/*echo "'".$transaction->t_amount."','".$adminName."','".$transaction->p_center."','".$transaction->transaction_type."','".$transaction->t_date."','".$transaction->t_status."','".$transaction_id."','".$transaction->t_payment_mode."','".$transaction->hotel_transaction_type_description."','".$sl."','".$transaction->transaction_from_id."','".$transaction->hotel_transaction_type_cat."'";*/
				?>
			  </td>
			  
              <td>
				<?php 
					echo $transaction->transaction_type; 
				?>
			  </td>
			  
              <td>
				<?php 
					echo $transaction->hotel_transaction_type_description; 
				?>
			  </td>
			  
              <!--<td><?php echo $transaction->transaction_to_id; ?></td>-->
              <td>
				<?php 
					if($transaction->details_id!=0){
						echo $transaction->details_type.' - 0'.$transaction->hotel_id.'/'.$transaction->details_id;
					}
					else
						echo '<span style="color:#AAAAAA;">No info</span>';
				?>
			  </td>
			  
              <td>
				<?php 
					if($transaction->t_amount > 10000)
						echo '<span style="color:#317256;">'.$transaction->t_amount.'</span>';
					else if($transaction->t_amount > 5000)
						echo '<span style="color:#242493;">'.$transaction->t_amount.'</span>';
					else if($transaction->t_amount < 0)
						echo '<span style="color:#FE5757;">'.$transaction->t_amount.'</span>';
					else
						echo '<i class="fa fa-inr" aria-hidden="true"></i> '.$transaction->t_amount;
				?>
			  </td>
              
              <td id="tpayMode<?php echo $sl; ?>">
				<?php 
					$pmode = $this->dashboard_model->get_p_mode_by_name($transaction->t_payment_mode);
					if($pmode){
						if($pmode->pm_icon != NULL)
							echo $pmode->pm_icon.' ';
						echo $pmode->p_mode_des;
					}
					else
						echo '<span style="color:#AAAAAA;">No info</span>';
					//echo $transaction->t_payment_mode;
				?>
			  </td>
			  
              <td id="tpaydetails<?php echo $sl; ?>">
				<?php // Payment Details
					
					if($transaction->t_payment_mode == 'cash'){
						echo '<span style="color:#AAAAAA;">No breakup available</span>';
					}
					else if($transaction->t_payment_mode == 'cards'){
						echo '<span style="color:#0E6767;">['.$transaction->t_bank_name.'] </span>'; 
						echo $transaction->t_card_type; 
						echo ' Card No: '.$transaction->t_card_no; 
						//echo 'Card No: '.$keyt->t_card_no; 
					}
					else if($transaction->t_payment_mode == 'fund'){
						echo '<span style="color:#0E6767;">['.$transaction->t_bank_name.'] </span>'; 
						echo ' Acc No: '.$transaction->ft_account_no;
						echo ' | IFSC: '.$transaction->ft_ifsc_code;
					}
					else if($transaction->t_payment_mode == 'cheque'){
						echo '<span style="color:#0E6767;">['.$transaction->t_bank_name.'] </span>'; 
						echo ' Cheque No: '.$transaction->checkno;
						echo '| Drw Name : '.$transaction->t_drw_name;
						
					}
					else if($transaction->t_payment_mode == 'draft'){
						echo '<span style="color:#0E6767;">['.$transaction->t_bank_name.'] </span>'; 
						echo ' Draft No: '.$transaction->draft_no;
						echo '| Drw Name : '.$transaction->t_drw_name;
					}
					else if($transaction->t_payment_mode == 'ewallet'){
						echo '<span style="color:#0E6767;">['.$transaction->t_w_name.'] </span>'; 
						echo ' Tran ID: '.$transaction->t_tran_id;
						echo '| Recv Acc : '.$transaction->t_recv_acc;
					}
					else
						echo '<span style="color:#AAAAAA;">No info</span>';
				?>
			  </td>

              <td width='50%' id="tfroe<?php echo $sl; ?>">
				<?php 
					echo $transaction->from_entity;
				?>
			  </td>

			  <td width='50%' id="ttoe<?php echo $sl; ?>">
				<?php  
					echo $transaction->to_entity;
				?>
			  </td>

			  <td width='50%' id="tnote<?php echo $sl; ?>">
				<?php 
					echo $transaction->transactions_detail_notes;
				?>
			  </td>

			  <td width='50%'>
				<?php 
					echo $hotel_name->hotel_name;
				?>
			  </td> 

			  <td class="ba">
              	<div class="btn-group">
                  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li><a onclick="soft_delete('<?php echo $transaction->t_id;?>')" class="btn red btn-xs" ><i class="fa fa-trash"></i></a></li>
                    <li><a href="<?php echo base_url();?>Dashboard/edit_transaction/<?php echo $transaction->t_id; ?>" class="btn green btn-xs"><i class="fa fa-edit"></i></a></li>
                    <li><a href="#pay_recip" class="btn blue btn-xs" data-toggle="modal" onclick="getRecptInfo(<?php echo "'".$transaction->t_amount."','".$adminName."','".$transaction->p_center."','".$transaction->transaction_type."','".$transaction->t_status."','".$transaction_id."','".$transaction->t_payment_mode."','".$transaction->hotel_transaction_type_description."','".$sl."','".$transaction->transaction_from_id."','".$transaction->hotel_transaction_type_cat."','".$transaction->transaction_to_id."'";?>);"><i class="fa fa-eye"></i></a></li>
                  </ul>
                </div>              	
              </td>
            </tr>
            <?php $i++; ?>
            <?php endforeach; ?>
            </tbody>
        </table> 
        <?php 
				echo '<i class="fa fa-info-circle" aria-hidden="true"></i> <span style="font-weight:bold; color:#FF007F;">Total Transaction Amount: INR '.number_format($tpa,2).'</span>';
		
			 }else{
				 echo '</tbody>
        </table> ';
				echo '<i class="fa fa-info-circle" aria-hidden="true"></i> <span style="font-weight:bold; color:#696969;">There are no Transactions performed</span>';
			$bg = '';
			
				}
			?>
            
      </div>
    </div>
	
	
<!-- END PAGE CONTENT-->
<script>

	$(document).ready(function(){

	});
	
    function soft_delete(id){
		
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
			
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_hotel_transaction?id="+id,
                data:{id:id},
                success:function(data)
                {

                   if(data.data==0){
                        window.location.replace("<?php echo base_url()?>dashboard");
                   }else{
                      swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        }); 
                   }
                }
            }); // End Ajax
        });
    }
	
	let getRecptInfo = (amt,admin,profitC,type,status,id,mode,desc,sl,tfrom,tincexp,tto) => {
		
		console.log('getRecptInfo');
		
		$('#amt').text('INR '+amt);
		$('#admin').text(admin);
		$('#profitC').text(profitC);
		$('#ttype').text(type);
		$('#tdate').text($('#tdate'+sl).text());
		$('#tstatus').text(status);
		$('#tid').text(id);
		$('#tmode').text($('#tpayMode'+sl).text());
		$('#tdesc').text(desc);
		$('#tfrom').text($('#tfroe'+sl).text());
		$('#tto').text($('#ttoe'+sl).text());
		$('#tincexp').text(tincexp);
		$('#tpaydetailsM').text($('#tpaydetails'+sl).text());
		$('#tnoteM').text($('#tnote'+sl).text());
	}

	function printDiv(div) {    
	
		window.print(); 

		return true;
	}
	
</script> 

<div class="modal fade" id="pay_recip" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<table width="100%">
					<tr>
						<td align="center">
							<strong style="font-size: 18px;">Payment Receipt </strong>
							<small id="tincexp" name="tincexp" style="float: right; padding-top: 6px; font-weight: bold; text-transform: uppercase; color: #4e9461;"> </small>
						</td>
					</tr>
					<tr>
						<td style="height: 10px;"></td>
					</tr>
					<tr>
						<td style="border-bottom: 1px solid #000000;"></td>
					</tr>
					<tr>
						<td style="height: 10px;"></td>
					</tr>
				</table>													
				<table width="100%">
					<tr>
						<td align="left" width="60%" style="font-size: 14px;">
							<strong><?php echo $hotel_name->hotel_name; ?></strong>
						</td>
						<td align="left" width="40%" style="font-size: 14px;">
							<strong> Name </strong>
						</td>
					</tr>
					<tr><td colspan="2" style="height: 4px"></td></tr>
					<tr>
						<td align="left">
							<?php echo  $hotel_contact['hotel_street1']; ?>
							<?php echo  $hotel_contact['hotel_street2'].', '; ?><br>
							<?php echo  $hotel_contact['hotel_district']; ?> -
							<?php echo  $hotel_contact['hotel_pincode']; ?>,<br>
							<?php echo  $hotel_contact['hotel_state']; ?> -
							<?php echo  $hotel_contact['hotel_country']; ?><br/>
							Phone:
							<?php echo  $hotel_contact['hotel_frontdesk_mobile']; ?><br/>
							Email:
							<?php echo  $hotel_contact['hotel_frontdesk_email']; ?><br/>
						</td>
						<td align="left">
							Address
						</td>
					</tr>
					<tr><td colspan="2" style="height: 4px"></td></tr>

					<tr>
						<td align="left">
							<label id="tdate" name="tdate"> </label>
						</td>
						<td align="left">
							<strong>
								<Strong><label id="tstatus" name="tstatus"> </label></strong>
							</strong>
						</td>
					</tr>
					<tr><td colspan="2" style="height: 4px"></td></tr>
					<tr>
						<td align="left">
							<label id="tid" name="tid"> </label>
						</td>
						<td align="left">&nbsp;
							
						</td>
					</tr>
					<tr>
						<td colspan="2" style="height: 10px;"></td>
					</tr>
					<tr>
						<td colspan="2" style="border-bottom: 1px solid #000000;"></td>
					</tr>
					<tr>
						<td colspan="2" style="height: 10px;"></td>
					</tr>
				</table>
				<table width="100%" class="td-pad">
					<tr>
						<td width="35%" align="left" valign="top">Amount:</td>
						<td align="left" valign="top">
							<label id="amt" name="amt"> </label> /-
						</td>
					</tr>
					<tr>
						<td align="left" valign="top">Mode:</td>
						<td align="left" valign="top">
							<label id="tmode" name="tmode"> </label>
						</td>
					</tr>
					
					<tr>
						<td align="left" valign="top">Payment Details:</td>
						<td align="left" valign="top">
							<label id="tpaydetailsM" name="tpaydetailsM"> </label>
						</td>
					</tr>
					
					<tr>
						<td align="left" valign="top">Transations Type:</td>
						<td align="left" valign="top">
							<label id="ttype" name="ttype"> </label>
						</td>
					</tr>		

					<tr>
						<td align="left" valign="top">Details:</td>
						<td align="left" valign="top">
							<label id="tdesc" name="tdesc"> </label>
						</td>
					</tr>
					
					<tr>
						<td align="left" valign="top">Admin:</td>
						<td align="left" valign="top">
							<label id="admin" name="admin"> </label>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top">From:</td>
						<td align="left" valign="top">
							<label id="tfrom" name="tfrom"> </label>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top">To:</td>
						<td align="left" valign="top">
							<label id="tto" name="tto"> </label>
						</td>
					</tr>
					
					<tr>
						<td align="left" valign="top">Note:</td>
						<td align="left" valign="top">
							<label id="tnoteM" name="tnoteM"> </label>
						</td>
					</tr>
					
					<tr>
						<td align="left" valign="top">Profit Center:</td>
						<td align="left" valign="top">
							<label id="profitC" name="profitC"> </label>
						</td>
					</tr>
					
					<tr>
						<td colspan="2" style="height: 10px;"></td>
					</tr>
					<tr>
						<td colspan="2" style="border-bottom: 1px solid #000000;"></td>
					</tr>
					<tr>
						<td colspan="2" style="height: 60px;"></td>
					</tr>
				</table>
				<table width="100%">
					<tr>								
						<td>--------------------------------</td>
						<td align="center">---------------------------------</td>
						<td align="right">-------------------------------</td>
					</tr>
					<tr>								
						<td>Paid By</td>
						<td align="center">Approved By</td>
						<td align="right">Recievd By</td>
					</tr>
				</table>		
			</div>
			<div class="modal-footer">
				<button type="button" class="btn dark btn-outline btn-xs" onclick="printDiv(pay_recip)">Print</button>
				<button type="button" class="btn green  btn-xs">Download</button>
			</div>
		</div>
	</div>
</div>

