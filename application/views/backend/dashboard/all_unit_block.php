<?php if($this->session->flashdata('err_msg')):?>



<div class="alert alert-danger alert-dismissible text-center" role="alert">

  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>

  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>

<?php endif;?>

<?php if($this->session->flashdata('succ_msg')):?>

<div class="alert alert-success alert-dismissible text-center" role="alert">

  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>

  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>

<?php endif;?>

<div class="portlet light bordered">

  <div class="portlet-title">

    <div class="caption"> <i class="glyphicon glyphicon-bed"></i> List of All Unit Blocks</div>

    <div class="actions">

    	<a href="<?php echo base_url();?>dashboard/add_unit_block" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i> Add New </a>

    </div>

  </div>

  <div class="portlet-body">

    <!--<div class="table-toolbar">

      <div class="row">

        <div class="col-md-6">

          <div class="btn-group">  </div>

        </div>        

      </div>

    </div>-->

    <table class="table table-striped table-bordered table-hover" id="sample_1">

      <thead>

        <tr>
          <th scope="col" width="15%"> Room No. </th>

          <th scope="col" width="15%"> Number of Beds </th>

          <th scope="col" width="15%"> Unit Type </th>

          <th scope="col" width="35%"> Price </th>

          <th scope="col" width="10%"> Action </th>

        </tr>

      </thead>

      <tbody>

        <?php if(isset($rooms) && $rooms):

                            $i=1;

                            foreach($rooms as $rom):

                            if($rom->hotel_id==$this->session->userdata('user_hotel')):

                            $class = ($i%2==0) ? "active" : "success";

                            

                            $room_id=$rom->room_id;

                            ?>

        <tr>

          <td align="left"><?php echo $rom->room_no;?></td>

          <td align="left"><?php echo $rom->room_bed." Beds ";?></td>

          <td align="left"><?php echo $rom->unit_type; ?></td>

          <td align="left">₹ <?php echo $rom->room_rent .'(General) </br>'. '₹ '. $rom->room_rent_seasonal.'(Seasonal)  </br>'. '₹ '. $rom->room_rent_weekend.'(Week End)' ;?></td>

          <td align="center" class="ba"><div class="btn-group">

              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>

              <ul class="dropdown-menu pull-right" role="menu">

                <li> <a onclick="soft_delete('<?php echo $room_id ?>')" data-toggle="modal" class="btn red btn-xs"> <i class="fa fa-trash"></i></a> </li>

                <li> <a href="<?php echo base_url() ?>dashboard/edit_room?room_id=<?php echo $room_id ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a> </li>

              </ul>

            </div></td>

        </tr>

        <?php $i++;?>

        <?php endif; ?>

        <?php endforeach; ?>

        <?php endif;?>

      </tbody>

    </table>

  </div>

</div>

<script>

    



    function soft_delete(id){

        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){



          







            $.ajax({

                type:"POST",

                url: "<?php echo base_url()?>dashboard/delete_room?room_id="+id,

                data:{r_id:id},

                success:function(data)

                {

                    //alert("Checked-In Successfully");

                    //location.reload();

                    swal({

                            title: data.data,

                            text: "",

                            type: "success"

                        },

                        function(){



                            location.reload();



                        });

                }

            });







        });

    }

</script> 