<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <strong><i class="fa fa-flag"></i></strong> List of All Unit Class </div>
    <div class="actions"> <a class="btn btn-circle green btn-outline btn-sm"  data-toggle="modal" href="#responsive" > <i class="fa fa-plus"></i>Add New </a> </div>
  </div>
  <div class="portlet-body">
    <!--<div class="table-toolbar">
      <div class="row">
        <div class="col-md-6">
          <div class="btn-group">
            
          </div>
        </div>
      </div>
    </div>-->
	
	<p class="font-green-sharp" style="font-weight:600;">*Example - Standard, Premium, Delux, Executive etc.</p> <p class="font-green-sharp">Please use the table below to navigate or filter the results. You can download the table as excel and pdf.</p>
	
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr>
          <!--<th scope="col"> Unit class Id </th>-->
          <!--<th scope="col"> Unit class hotel Id </th>-->
          <th scope="col"> Unit class name </th>
          <th scope="col"> Description </th>
          <th scope="col"> Date created </th>
          <th width="5%" scope="col"> Action</th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($unit_class) && $unit_class):

                      $i=1;
                      foreach($unit_class as $gst):
                          $class = ($i%2==0) ? "active" : "success";
                          $g_id=$gst->hotel_unit_class_id;
                          ?>
          <tr id="row_<?php echo $gst->hotel_unit_class_id;?>">
          <!--<td align="center"><?php echo $gst->hotel_unit_class_id; ?></td>-->
          <!--<td align="center"><?php echo $gst->hotel_unit_class_hotel_id; ?></td>-->
          <td align="left"><?php echo $gst->hotel_unit_class_name; ?></td>
          <td align="left">
          	<?php
				  
					if(($gst->hotel_unit_class_desc == NULL) || ($gst->hotel_unit_class_desc == '')){
						echo '<span style="color:#AAAAAA;">'."No Info".'</span>';
					}
					else{
						echo $gst->hotel_unit_class_desc;
					}
								
				?>
		  	
          </td>
          <td align="left"><?php echo date("g:ia \-\n l jS F Y",strtotime($gst->hotel_unit_class_date_created));?></td>
          <td align="center" class="ba">
          		<div class="btn-group">
                    <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li>
                            <a onclick="soft_delete('<?php echo $gst->hotel_unit_class_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a>
                        </li>
                        <li>
                            <a onclick="edit_unit_class('<?php echo $gst->hotel_unit_class_id; ?>')" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a>
                        </li>
                    </ul>
                </div>
          </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/delete_class_type?f_id="+id,
                data:{f_id:id},
                success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                           $('#row_'+id).remove();

                        });
                }
            });



        });
    }
	
	
	function edit_unit_class(id){
		//alert(id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/edit_unit_class1",
                data:{id:id},
                success:function(data)
                {
                    //alert(data.id);
					
                   $('#hid1').val(data.hotel_unit_class_id);
					$('#unit_class1').val(data.hotel_unit_class_name);
				     $('#desc1').val(data.hotel_unit_class_desc);				  
                    $('#editmodal').modal('toggle');
					
                }
            });
	}
</script>  
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('unit_class_controller/all_unit_class_add',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Unit Class</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="unit_class" id="unit_class" required="required" placeholder="Unit Class">
                <label></label>
                <span class="help-block">Unit Class</span> </div>
            </div>
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <textarea class="form-control"  name="desc" id="desc" placeholder="Description"></textarea>
                <label></label>
                <span class="help-block">Description</span> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<div id="editmodal" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form1 = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('unit_class_controller/edit_unit_class',$form1);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Unit Class</h4>
      </div>
      <input type="hidden" name="hid1" id="hid1">
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="unit_class1" id="unit_class1" placeholder="Unit Class" required="required">
                <span class="help-block">Unit Class</span> </div>
              </div>
              <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <textarea class="form-control" row="3" name="desc1"placeholder="Description" id="desc1" ></textarea>
                <span class="help-block">Description</span> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>