<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>


	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>


	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
<?php endif;?>

<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-edit"></i>List of All usage log
		</div>
		<div class="actions">
			<a href="<?php echo base_url();?>dashboard/usage_log" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New  </a>
		</div>
	</div>
	<div class="portlet-body">
		
		<table class="table table-striped table-bordered table-hover" id="sample_1">
			<thead>
				<tr>
					<th align="center" scope="col">
						#
					</th>
					<th scope="col">
						Generator Name
					</th>
					<th scope="col">
						Start
					</th>
					<th scope="col">
						Stop
					</th>

					</th>
					<th scope="col">
						Admin
					</th>
					<th scope="col">
						Total Hours
					</th>
					<th scope="col">
						Spec Cons
					</th>
					<th scope="col">
						Fuel Reading
					</th>
					<th scope="col">
						Diff
					</th>

					<th scope="col">
						Action
					</th>
				</tr>
			</thead>
			<tbody>
				<?php if(isset($usage_log) && $usage_log):
							
                            $i=1;
							$j = 0;
                            foreach($usage_log as $ul):
                                $class = ($i%2==0) ? "active" : "success";
                                $usage_id=$ul->usage_id;
								$j++;
                                ?>
				<tr>
					<!-- <td width="50">
                                        <div class="md-checkbox pull-left">
                                            <input type="checkbox" id="checkbox1" class="md-check">
                                            <label for="checkbox1">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                            </label>
                                        </div>
                                    </td>-->

					<!-- <td>
                                   
                                    // <img  width="60px" height="60px" src="<?php echo base_url();?>upload/guest/<?php //if( $gst->g_photo_thumb== '') { echo "no_images.png"; } else { echo $gst->g_photo_thumb; }?>" alt=""/>
                                    
                                    
                                    <?php
										/*echo $gst->g_photo ;
										echo $gst->g_photo ."_thumb"; */
									?>
                                    
                                    </td> -->

					<td align="center" valign="middle">
						<?php echo $j; ?>
					</td>
					<td align="Left" valign="middle">
						<?php echo $ul->gen_name; ?>
					</td>
					<td align="Left" valign="middle">
						<?php echo date("jS F Y",strtotime($ul->startdate)).' : '.$ul->starttime;?>
					</td>

					</td>
					<td align="Left" valign="middle">
						<?php echo date("jS F Y",strtotime($ul->stopdate)).' : '.$ul->stoptime ?>
					</td>

					<td align="Left" valign="middle">
						<?php echo $ul->admin ?>
					</td>
					<td align="Left" valign="middle">
						<?php echo $ul->totalhours ?>
					</td>

					<td align="left" valign="middle">
						<?php 
										if(isset($ul->fuelconsumption) && ($ul->fuelconsumption != NULL))
											echo number_format($ul->fuelconsumption,2);
										else
											echo '';
									?>
					</td>

					<td align="left" valign="middle">
						<?php echo $ul->openingfuel.' - '.$ul->closingfuel.' = '.$ul->differfuel ?>
					</td>

					<td align="left" valign="middle">
						<?php echo number_format(($ul->fuelconsumption-$ul->differfuel),2) ?>
					</td>

					<td align="center" valign="middle">
						<a onclick="soft_delete('<?php echo $ul->usage_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a>
					</td>
				</tr>


				<?php endforeach; ?>
				<?php endif; ?>


			</tbody>
		</table>

	</div>


</div>
<script>
	function soft_delete( id ) {
		swal( {
			title: "Are you sure?",
			text: "All the releted transactions and data will be deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {





			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/delete_usage_log?u_id=" + id,
				data: {
					u_id: id
				},
				success: function ( data ) {
					//alert("Checked-In Successfully");
					//location.reload();
					swal( {
							title: data.data,
							text: "",
							type: "success"
						},
						function () {

							location.reload();

						} );
				}
			} );



		} );
	}
</script>