<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Vendors </div>
    <div class="actions">
    	<a href="<?php echo base_url();?>dashboard/add_vendor" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
        <a href="javascript:void(0);" class="btn btn-circle green btn-outline btn-sm" id="status123" onclick="vendor()"><span id="demo">Active Vendor</span></a>
    </div>
  </div>
 
  <div class="portlet-body">
    
    <div id="table1">
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <!-- <th scope="col">
                            Select
                        </th>-->
            <th scope="col">Image</th>			
          <th scope="col"> Vendor Name </th>
          <th scope="col"> Contact Person</th>
          <th width="10%" scope="col"> Address </th>
          <th scope="col"> Purchase Count </th>
         <!-- <th scope="col"> Tax % </th>-->
          <th scope="col"> Discount % </th>
          <th scope="col"> Profit Center </th>
          <th scope="col"> Industry </th>
          <th scope="col"> Parent Vendor </th>
          <th scope="col"> Total Amount </th>
          <th scope="col"> Contract Status </th>          
          <th scope="col"> Status </th>
          <th scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
         <?php if(isset($vendor) && $vendor){
                        
                        $i=1;
                        foreach($vendor as $dgs):
                            $class = ($i%2==0) ? "active" : "success";
                            //$dgset_id=$dgs->dgset_id;
                            ?>
        <tr id="row_<?php echo 	$dgs->hotel_vendor_id;?>">               
            <td width="5%"><a class="single_2" href="<?php echo base_url();?>upload/vendor_detail/<?php if( $dgs->hotel_vendor_image== '') { echo "no_images.png"; } else { echo $dgs->hotel_vendor_image; }?>"><img src="<?php echo base_url();?>upload/vendor_detail/<?php if( $dgs->hotel_vendor_image== '') { echo "no_images.png"; } else { echo $dgs->hotel_vendor_image; }?>" alt="" style="width:100%;"/></a></td>
          <td><?php echo $dgs->hotel_vendor_name; ?></td>
          <td><?php echo $dgs->hotel_vendor_contact_person.' </br>'.$dgs->hotel_vendor_contact_no;?></td>
          <td><?php echo $dgs->hotel_vendor_address ?></td>
          <td><?php echo $dgs->hotel_vendor_purchase_count?></td>
          <!--<td><?php //echo $dgs->hotel_vendor_tax ?></td>-->
          <td><?php echo $dgs->hotel_vendor_discount ?></td>
          <td><?php echo $dgs->hotel_vendor_profit_center ?></td>
          <td><?php echo $dgs->hotel_vendor_industry ?></td>
          <td><?php echo $dgs->hotel_parent_vendor_id ?></td>
          <td><?php echo $dgs->hotel_vendor_total_amt ?></td>
          <td><?php echo $dgs->hotel_vendor_is_contracted ?></td>
          <td>
          <input type="hidden"  id="hid" value="<?php echo $dgs->status;?>">
             <button class="btn green btn-xs" id="status123" value="<?php echo $dgs->hotel_vendor_id;?>" onclick="status(this.value)"><span id="demostat<?php echo $dgs->hotel_vendor_id;?>"><?php if($dgs->status == '1'){ echo  "Active";}
else{
echo "Inactive";
}	?></span></button></td>
            <td align="center" valign="middle" class="ba">
          	<div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
               <li><a  onclick="soft_delete('<?php echo $dgs->hotel_vendor_id;?>')" <?php  if($dgs->status =='1'){
                echo "disabled"; }?> data-toggle="modal"  class="btn red btn-xs" ><i class="fa fa-trash"></i>
                  </a></li>
                <li><a href="<?php echo base_url();?>dashboard/edit_vendor/<?php echo $dgs->hotel_vendor_id; ?>" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li>
                <li><a target="_blank" href="<?php echo base_url();?>upload/vendor_detail/<?php echo $dgs->hotel_vendor_reg_form; ?>" class="btn blue btn-xs" data-toggle="modal"><i class="fa fa-download"></i></a></li>
                </ul>
            </div>
          </td>
        </tr>
        <?php endforeach; ?>
        <?php } else{ ?>
        <tr> 
          
            
            <td colspan="13">No Data Found</td>
          
        </tr>
        
        <?php } ?>
      </tbody>
    </table>
    </div>
  </div>
</div>
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_vendor?vendor_id="+id,
                data:{a_id:id},
                success:function(data)
                {
                    //alert('hello');
                    swal({
                            title: "deleted",
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
	
	
	
	function status(id){
		//var status=$('#hid').val();
		
		
		//alert(status);
		//alert(id);
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/all_vendor_status",
                data:{id:id},
                success:function(data)
                {
			//alert(data.data);
				   document.getElementById("demostat"+id).innerHTML = data.data;
					$('#row_'+id).remove();
                }
            });
		
	}
	
	function vendor(){
		var a=document.getElementById("demo").innerHTML;
		if(a=="Active Vendor"){
			//$("#status123").removeClass();
			// $("#status123").css('background-color',"blue");
			document.getElementById("demo").innerHTML ="Inctive Vendor";			
			
		}else{			
			//$("#status123").removeClass();
			 //$("#status123").css('background-color',"green");
			document.getElementById("demo").innerHTML = "Active Vendor";
		}
		
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/vendor_filter",
                data:{status:a},
                 success:function(data)
                {
					//alert($data);
                  $('#table1').html(data);
				   $('#dataTable2').dataTable( {
						//"pageLength": 10 
    } );
	
                }
            });
		
	
	}
	
		 
	
</script> 
