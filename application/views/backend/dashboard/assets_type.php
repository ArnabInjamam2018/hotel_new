<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Asset Type's </div>
    <div class="actions">
    	<a class="btn btn-circle green btn-outline btn-sm"  data-toggle="modal" href="#responsive"> <i class="fa fa-plus"></i>Add New  </a>
    </div>
  </div>
  <div class="portlet-body">
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr>
          <th scope="col"> Asset Icon </th>
          <th scope="col"> Asset Name </th>
          <th scope="col"> Asset Description </th>
          <th scope="col" width="100"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($asset) && $asset):

						//print_r($asset);
						//exit;
					foreach($asset as $asset):
						
						
						?>
        <tr>
          <td width="5%"><a class="single_2" href="<?php echo base_url();?>upload/asset/<?php if( $asset->asset_icon== '') { echo "no_images.png"; } else { echo $asset->asset_icon; }?>"><img src="<?php echo base_url();?>upload/asset/<?php if( $asset->asset_icon== '') { echo "no_images.png"; } else { echo $asset->asset_icon; }?>" alt="" style="width:100%;"/></a></td>
          <td><?php echo $asset->asset_type_name; ?></td>
          <td><?php echo $asset->asset_type_description; ?></td>
          <td align="center" class="ba">
          <div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $asset->asset_type_id;?>')" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
                <li><a onclick="edit_assets_type('<?php echo $asset->asset_type_id; ?>')" data-toggle="modal"  class="btn green btn-xs"><i class="fa fa-edit"></i></a></li>
              </ul>
            </div>
          </td>          
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php
 
                                $form = array(
                                    'class'             => 'form-body',
                                    'id'                => 'form',
                                    'method'            => 'post'
                                );

                                echo form_open_multipart('dashboard/asset_type',$form);

                                ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Asset Type</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="asset_type_name" id="unit_name" required="required" placeholder="Asset name">
                <label></label>
                <span class="help-block">Asset Name</span> </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="asset_type_description" id="unit_name" placeholder="Asset Description" required="required">
                <span class="help-block">Asset Description</span> </div>
            </div>
            <div class="col-md-12">
              <div class="form-group form-md-line-input uploadss">
              	<label>Upload Image</label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                  <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('asset_type');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo  form_close(); ?> </div>

<!-- /.modal End--> 

<!-- edit modal -->
<div id="edit_asset_type" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

                                $form = array(
                                    'class'             => 'form-body',
                                    'id'                => 'form',
                                    'method'            => 'post'
                                );

                                echo form_open_multipart('dashboard/update_Atype',$form);

                                ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Asset Type</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-6">
              	<input type="hidden" name="hid1" id="hid1">
              	<div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="unit_name1" id="unit_name1" required="required" placeholder="Asset name">
                <label></label>
                <span class="help-block">Asset Name</span> </div>
             </div>
             <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="asetdesc1" id="asetdesc1" placeholder="Asset Description" required="required">
                <label></label>
                <span class="help-block">Asset Description</span> </div>
             </div>
             <div class="col-md-12">
              <div class="form-group form-md-line-input uploadss">
              	<label>Upload Image</label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img  id="img1"  name="img1" src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                  <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('asset_type1');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<!-- --> 
<script>
function delete_unit_type(val){
    var result = confirm("Are you want to delete this unit?");
    var linkurl = '<?php echo base_url() ?>dashboard/delete_unit_type/'+val;
    if (result) {
        //alert(linkurl);
        window.location.href= linkurl;
    }
    
}

function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_a_type?a_id="+id,
                data:{a_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
	
	
	function edit_assets_type(id){
		//alert(id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/edit_assets_type",
                data:{id:id},
                success:function(data)
                {
                    //alert(data.id);
					
                   $('#hid1').val(data.asset_type_id);
				   
				   $('#img1').attr('src','<?php echo base_url().'upload/asset/';  ?>'+data.asset_icon);
				  // $('#img1').val(data.asset_icon);
				   
				   $('#unit_name1').val(data.asset_type_name);
				   $('#asetdesc1').val(data.asset_type_description);
				   
                   $('#edit_asset_type').modal('toggle');
					
                }
            });
	}
</script> 