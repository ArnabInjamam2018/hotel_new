<script>
    function auto_assign(){
        var abc='abc';

        $.ajax({
            url: '<?php echo base_url()?>dashboard/matrix_maid_auto',
            type: 'POST',
            data: {table: abc
            },
            success: function(data){
                swal({
                        title: data,
                        text: "",
                        type: "success"
                    },
                    function(){
                        //location.reload();
                        $( "#target" ).load( "<?php echo base_url() ?>dashboard/maid_matrix_load" );
                    });
            }
        });


    }
   function assign_maid_matrix(){

        var myTableArray = [];

        $("table#assignment tr").each(function() {
            var arrayOfThisRow = [];
            var tableData = $(this).find('td');
            if (tableData.length > 0) {
                tableData.each(function() { arrayOfThisRow.push($(this).text()); });
                myTableArray.push(arrayOfThisRow);
            }
        });

        $.ajax({
            url: '<?php echo base_url()?>dashboard/matrix_maid_add',
            type: 'POST',
            data: {table: myTableArray
            },
            success: function(data){
                swal({
                        title: data,
                        text: "",
                        type: "success"
                    },
                    function(){
                        //location.reload();
                        $( "#target" ).load( "<?php echo base_url() ?>dashboard/maid_matrix_load" );
                    });
            }
        });

        //alert(myTableArray);


    }

    function reset(){
        $( "#target" ).load( "<?php echo base_url() ?>dashboard/maid_matrix_load" );
    }

</script>

<style>
/**.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 30px !important;
    
}**/
.table-scrollable {
    width: 85%;
    overflow-x: auto;
    overflow-y: hidden;
    border: 1px solid #dddddd;
    margin: 10px 0 !important;
}
.left-side ul li {
   padding: 11px 5px 14px 5px;
    margin-top: 2px;
    list-style-type: none;
    font-size: 13px;
	color:white;
	
}

#drag1 {

  padding:10px;
  background-color:#666;
  text-align:center;
  color:white;
  
  cursor:move;
  position:relative; /* important (all position that's not `static`) */
  
}
#drag2 {

  padding:10px;
  background-color:#666;
  text-align:center;
  color:white;
  
  cursor:move;
  position:relative; /* important (all position that's not `static`) */
 
}
#drag3{

  padding:10px;
  background-color:#666;
  text-align:center;
  color:white;
  
  cursor:move;
  position:relative; /* important (all position that's not `static`) */
 
}
#drag4 {

  padding:10px;
  background-color:#666;
  text-align:center;
  color:white;
  
  cursor:move;
  position:relative; /* important (all position that's not `static`) */
 
}
#drag5 {

  padding:10px;
  background-color:#666;
  text-align:center;
  color:white;
  
  cursor:move;
  position:relative; /* important (all position that's not `static`) */

}
#drag6 {

  padding:10px;
  background-color:#666;
  text-align:center;
  color:white;
  
  cursor:move;
  position:relative; /* important (all position that's not `static`) */
  
}
#div1 {
	width:100px;
    padding: 2px;
	height: 62px;
	vertical-align: middle !important;
    
	}
	.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 31px;
   
    vertical-align:bottom;
    border-top: 1px solid #ddd;
}
</style>

<script>

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
	
}

function drop(ev,div_id) {
  ev.preventDefault();
  alert(div_id);
  var data = ev.dataTransfer.getData("text");
  //ev.target.appendChild(document.getElementById(data));
  ev.target.replaceChild(document.getElementById(data),ev.target.childNodes[0] );
  // ev.target.append(document.getElementById(data));
	var mashi_name = document.getElementById(data).innerHTML;
  //var node = document.getElementById(data);
  //document.getElementById("side_bar").appendChild(node);
  document.getElementById('side_bar').innerHTML += '<div id="drag1" ondragstart="drag(event)" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" style="margin-bottom:10px;" >'+ mashi_name +'</div>';
  //document.getElementById('side_bar').appendChild(document.getElementById(data));
}


</script>

<div class="row">
  <button type="button" id="btn_1" onclick="assign_maid_matrix()" class="btn blue pull-right" style="margin-right:193px;margin-bottom:20px;">save</button>
  <button type="button" id="btn_2" onclick="auto_assign()" class="btn green pull-right" style="margin-right:10px;margin-bottom:20px;">Auto Asign</button>
  <button type="button" id="btn_3" onclick="reset()" class="btn red pull-right" style="margin-right:8px;margin-bottom:20px;">Reset</button>
  <button type="button" id="btn_4" onclick="print_matrix()" class="btn yellow pull-right" style="margin-right:8px;margin-bottom:20px;">Print </button>
  <div class="clearfix"></div>

	<div class="col-md-12">
		<div class="col-md-2" id="side_bar" style="height:276px; background:;" ondrop="drop(event)" ondragover="allowDrop(event)" >
      <?php $maids=$this->dashboard_model->all_maids();
        if($maids){
          $num_maid=0;
          foreach($maids as $maid){?>
            <div class="drop" style="margin-bottom:10px">
				      <div id="drag<?php echo $maid->maid_id; ?>" ondragstart="drag(event)" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)">
                <div style="display: none"><?php echo $maid->maid_id; ?>
                </div>
              <?php echo $maid->maid_name ?>
              </div>
				    </div>
      <?php 
          $num_maid++;
          }
        }
      ?>
		</div>

    <div id="table_div">
  		<div class="col-md-8" style=" background:#1caf9a; padding:5px;" >
  		
  					<div class="left-side" style="float:left; width:15%; border:0px solid red">
  					<ul>
              <?php
              foreach($rooms as $room){
              ?>
  					  <li>Room: <?php echo $room->room_no ?></li>
               <?php } ?>
  					</ul>
  					
  					</div>
  		
  					<div class="table-scrollable" id="target" >
  						<table id="assignment" class="table table-striped table-bordered  table-advance  table-hover table-responsive">

  							<tbody>
                  <?php
                  foreach($rooms as $room){
                  ?>
                  <tr id="room<?php echo $room->room_id; ?>">
                    <td style="display: none"><?php echo $room->room_id; ?>
                    </td>

                    <?php  $rmm=$this->dashboard_model->room_maid_match($room->room_id);
                    if($rmm){
                    foreach($rmm as $assign){
                    ?>

                    <td style="display: none;">
                      <?php echo $assign->maid_id; ?>
                    </td>
  								 
  									<td class="highlight" id="div1" ondrop="drop(event,this.id)" ondragover="allowDrop(event)" >
                      <div style=" width:100%" id="drag<?php echo $assign->maid_id; ?>" ondragstart="drag(event)" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" > <?php echo $assign->maid_name ?>
                      </div>
  									</td>

                    <?php } }
                    else{
                      ?>
                    <td class="highlight" id="div1" ondrop="drop(event)" ondragover="allowDrop(event)" >
                        <div style="display: none"id="" value="null">
                    </td>

                    <?php
                    }?>
  								</tr>
                <?php } ?>
  							</tbody>
  						</table>
  					</div>
  			</div>
  	  </div>
		</div>
		
	</div>
</div>
<Script>
  function  print_matrix() {
    //var printContents = document.getElementById("table_div").innerHTML;
    //var originalContents = document.body.innerHTML;
    //document.body.innerHTML = printContents;
    document.getElementById('btn_1').style.display = 'none';
    document.getElementById('btn_2').style.display = 'none';
    document.getElementById('btn_3').style.display = 'none';
    document.getElementById('btn_4').style.display = 'none';
    document.getElementById('side_bar').style.display = 'none';
    window.print();
    document.getElementById('btn_1').style.display = 'block';
    document.getElementById('btn_2').style.display = 'block';
    document.getElementById('btn_3').style.display = 'block';
    document.getElementById('btn_4').style.display = 'block';
    document.getElementById('side_bar').style.display = 'block';
    document.body.innerHTML = originalContents;

  }
</Script>


 

