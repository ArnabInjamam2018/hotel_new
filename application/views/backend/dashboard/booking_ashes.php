<div class="modal fade" id="myModalNorm1" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content"> 
      
      <!-- Modal Header -->
      
      <div class="modal-header" style="background:#95A5A6; color:#ffffff;"> Search Guest
        <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true">&times;</span> <span class="sr-only">Close</span> </button>
      </div>
      
      <!-- Modal Body -->
      <div class="modal-body">
        <div class="form-group">
          <div class="input-group">
            <input type="email" class="form-control" id="guest_search" placeholder="Search Guest"/>
            <span class="input-group-btn">
            <button type="button" class="btn green" onclick="return_guest_search()">Search</button>
            </span> </div>
        </div>
        <div id="return_guest" style="overflow-y:scroll; height:300px; display:none;"> </div>
      </div>
      
      <!-- Modal Footer -->
      <div class="modal-footer">
        <button type="button" class="btn red"
                        data-dismiss="modal"> Close </button>
      </div>
    </div>
  </div>
</div>
<?php 
if(isset($bookingDetails->room_id)){
$details = $this->dashboard_model->get_hotel_room($bookingDetails->room_id);
}
$amountPaid = $this->dashboard_model->get_amountPaidByBookingID($_GET['b_id']);
//print_r($amountPaid);
if(isset($bookingDetails->booking_status_id_secondary)){
if($bookingDetails->booking_status_id_secondary > 0){
  $txtcol = $this->dashboard_model->get_booking_status($bookingDetails->booking_status_id_secondary);
}
}
if(isset($bookingDetails->booking_status_id)){
$bgcol = $this->dashboard_model->get_booking_status($bookingDetails->booking_status_id);
}
//print_r($bgcol);
$booking_from = $bookingDetails->cust_from_date_actual;
$booking_to = $bookingDetails->cust_end_date_actual;
$status_id=$bookingDetails->booking_status_id;
$status_id_secondary=$bookingDetails->booking_status_id_secondary;
$discount=$bookingDetails->discount;
$bookingstatusid=$bookingDetails->booking_status_id;

$bills=$this->bookings_model->all_folio($bookingDetails->booking_id);
	    	$item_no=0;
	    	foreach ($bills as $item) {
	    		# code...

	    		$item_no++;

	    	} ?>
<div class="row">
<?php if($this->session->flashdata('err_msg')):?>
<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<div class="col-md-12">
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Booking Id:
      <?php if(isset($bookingDetails->guest_id)){echo "HM0".$bookingDetails->hotel_id."00".$bookingDetails->booking_id;}?>
      </span> </div>
    <div data-toggle="modal" href="#change" class="actions">
      <?php if(isset($bookingDetails->booking_status_id_secondary) && $bookingDetails->booking_status_id_secondary > 0){ ?>
      <a style="background-color:<?php if(isset($bgcol->body_color_code)){echo $bgcol->body_color_code;}?>; color:<?php if(isset($txtcol->bar_color_code)){echo $txtcol->bar_color_code;}?>; font-size: 18px; padding: 0 5px; text-decoration:none;">
      <?php if(isset($bgcol->booking_status)){echo $bgcol->booking_status;}?>
      </a>
      <?php } else { ?>
      <a style="background-color:<?php if(isset($bgcol->body_color_code)){echo $bgcol->body_color_code; }?>; color:<?php if(isset($bgcol->bar_color_code)){echo $bgcol->bar_color_code;}?>; font-size: 18px; padding: 0 5px; text-decoration:none;">
      <?php if(isset($bgcol->booking_status)){echo $bgcol->booking_status;}?>
      </a>
      <?php } ?>
    </div>
    <div style="width:100%; text-align:center; padding: 10px 0; font-size: 16px; line-height:18px; font-weight:700;">
      <?php if(isset($details->hotel_name)){echo $details->hotel_name ;}?>
    </div>
  </div>
  <div id="change" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <?php 
                                      $from_date=$bookingDetails->cust_from_date_actual;

                                      date_default_timezone_set('Asia/Kolkata');

                                      $date=date("Y-m-d");

                                      $from_date_actual=date("Y-m-d",strtotime($from_date));

                                    ?>
        <div class="modal-header">
          <h4> Change Booking Status </h4>
        </div>
        <div class="modal-body">
          <?php if($bookingDetails->booking_status_id =="5"): ?>
          <select onchange="change_status(this.value)" name="status" id="status" class="form-control input-sm">
            <option disabled="disabled" selected="selected">--Change--</option>
            <option value="6">Check Out</option>
            <option value="7">Cancelled</option>
          </select>
          <?php endif; ?>
          <?php if(($bookingDetails->booking_status_id =="4")  ): ?>
          <select onchange="change_status(this.value)" name="status" id="status" class="form-control input-sm">
            <option disabled="disabled" selected="selected">--Change--</option>
            <option value="5">Check In</option>
            <option value="7">Cancelled</option>
          </select>
          <?php endif; ?>
          <?php if(($bookingDetails->booking_status_id =="2") ): ?>
          <select onchange="change_status(this.value)" name="status" id="status" class="form-control input-sm">
            <option disabled="disabled" selected="selected">--Change--</option>
            <option value="4">Confirmed</option>
            <option value="5">Check In</option>
            <option value="7">Cancelled</option>
          </select>
          <?php endif; ?>
          <?php if($bookingDetails->booking_status_id =="1"): ?>
          <select onchange="change_status(this.value)" name="status" id="status" class="form-control input-sm">
            <option disabled="disabled" selected="selected">--Change--</option>
            <option value="5">Check In</option>
            <option value="7">Cancelled</option>
          </select>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
          function change_status(value){
           
              $.ajax({
    type: "POST",
    dataType: 'json',
    url: "<?php echo base_url(); ?>dashboard/change_booking_status",
    data:{value:value, booking_id:<?php echo $_GET['b_id']; ?>},
    success: function(data){

       swal({
                                                title: "Room Status Changed",
                                                text: "Room status chnaged to "+data,
                                                type: "success"
                                            },
                                            function(){
                                                location.reload();
                                            });
     
    }
    });

          }
        </script>
  <div class="portlet-body form">
    <div class="form-body form-horizontal form-row-sepe">
      <div class="row">
        <div class="col-md-3 text-center">
          <?php if(isset($bookingDetails->g_photo_thumb) && $bookingDetails->g_photo_thumb != '') { ?>
          <img src="<?php echo base_url();?>upload/guest/<?php echo $bookingDetails->g_photo_thumb;?>" class="img-responsive" style="border:1px solid #dddddd; padding:5px; width:100%;"/>
          <?php } else { ?>
          <img src="<?php echo base_url();?>upload/guest/no_images.png" class="img-responsive" style="border:1px solid #dddddd; padding:5px; width:100%;"/>
          <?php } ?>
        </div>
        <div class="col-md-6" id="b-card">
          <h4><strong>Booking</strong></h4>
          <div class="form-group">
            <div class="col-md-6">
              <label><strong style="color: #2B3643;">Type:</strong></label>
              &nbsp;
              <label style="color:#008F6C">
                <?php if(isset($bookingDetails->guest_id)){ if($bookingDetails->group_id=='0'){echo "Single";}else{ ?>
                Group (<a href="<?php echo base_url(); ?>dashboard/booking_edit_group?group_id=<?php echo $bookingDetails->group_id; ?>"> HM0<?php echo $bookingDetails->hotel_id."GRP00".$bookingDetails->group_id;?></a> )
                <?php }}?>
              </label>
            </div>
            <div class="col-md-6">
              <label><strong style="color: #2B3643;">Center:</strong></label>
              &nbsp;
              <label style="color:#008F6C">
                <?php if($bookingDetails->p_center){echo $bookingDetails->p_center;}else echo "N/A";?>
              </label>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label><strong style="color: #2B3643;">Source:</strong></label>
              &nbsp;
              <label style="color:#008F6C">
                <?php if(isset($bookingDetails->booking_source)){echo $bookingDetails->booking_source ;}?>
              </label>
            </div>
            <div class="col-md-6">
              <label><strong style="color: #2B3643;">Taken on:</strong></label>
              &nbsp;
              <label style="color:#008F6C">
                <?php if(isset($bookingDetails->booking_taking_date)){echo $bookingDetails->booking_taking_date;}?>
              </label>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label><strong style="color: #2B3643;">Room No:</strong></label>
              &nbsp;
              <label style="color:#008F6C">
                <?php if(isset($details->room_no)){echo $details->room_no;}?>
              </label>
            </div>
            <div class="col-md-6">
              <label><strong style="color: #2B3643;">Food Plan:</strong></label>
              &nbsp;
              <label style="color:#008F6C">
                <?php if(isset($bookingDetails->food_plans)){ $data=$this->bookings_model->fetch_food_plan($bookingDetails->food_plans);if($data) echo $data->fp_name; else {echo "N/A";}} ?>
              </label>
            </div>
          </div>
          <div class="form-group col-md-12">
            <hr style="margin:0;padding:0; background:#eee;"/>
          </div>
          <h4><strong>Guest</strong></h4>
          <div class="form-group">
            <div class="col-md-6" >
              <label><strong style="color: #2B3643;">Name:</strong></label>
              &nbsp;
              <label style="color:#008F6C">
                <?php if(isset($bookingDetails->g_name))?>
                <a  href="<?php echo base_url();?>dashboard/edit_guest?g_id=<?php echo $bookingDetails->g_id; ?>" data-toggle="modal"><?php echo $bookingDetails->g_name;?></a> </label>
              <label style="color:#008F6C">
                <?php if(isset($bookingDetails->g_name))?>
                <a href="" id="addGuest"data-toggle="modal"><i class="fa fa-edit"></i></a> </label>
            </div>
            <div class="col-md-6">
              <label><strong style="color: #2B3643;">Type:</strong></label>
              &nbsp;
              <label style="color:#008F6C">
                <?php if($bookingDetails->g_type){echo $bookingDetails->g_type;} else {echo "N/A";}?>
              </label>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label><strong style="color: #2B3643;">Phone No:</strong></label>
              &nbsp;
              <label style="color:#008F6C">
                <?php if($bookingDetails->g_contact_no){echo $bookingDetails->g_contact_no;} else{echo "N/A";}?>
              </label>
            </div>
            <div class="col-md-6">
              <label><strong style="color: #2B3643;">Email:</strong></label>
              &nbsp;
              <label style="color:#008F6C">
                <?php if($bookingDetails->g_email){echo $bookingDetails->g_email;} else { echo "N/A";}?>
              </label>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label><strong style="color: #2B3643;">Address:</strong></label>
              &nbsp;
              <label style="color:#008F6C">
                <?php if(isset($bookingDetails->g_address)){echo $bookingDetails->g_address.",".$bookingDetails->g_city.",".$bookingDetails->g_state.",".$bookingDetails->g_pincode;}?>
              </label>
            </div>
          </div>
        </div>
        <div class="col-md-3 text-center"> <img src="" id="pay_symbol" class="img-responsive" style="padding:20px;" /> </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <hr style="margin:15px 0;padding:0; background:#eee;"/>
        </div>
        <div class="col-md-6">
          <label><strong style="color: #2B3643;">Check In</strong></label>
          &nbsp;
          <label style="color:#13ad2d">
            <?php  echo $booking_from;?>
          </label>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <label><strong style="color: #2B3643;">Check Out</strong></label>
          &nbsp;
          <label style="color:#f3565d">
            <?php  echo $booking_to;?>
          </label>
        </div>
        <div class="col-md-6 text-right">
          <label><strong style="color: #2B3643;">Total Amount :</strong></label>
          &nbsp;
          <label style="color:#13ad2d"> <i class="fa fa-inr" style="font-size:12px;"></i> <span id="tot_am"></span> </label>
        </div>
        <div class="col-md-12">
          <div class="progress" id="pr_div" style="margin:10px 0 0 0; height:14px; position:relative;">
            <div class="progress-bar" id="progress_bar" style="width: 0%; background:none; overflow:hidden; position:relative;"> <img id="pr_bar_img" src="<?php echo base_url();?>assets/dashboard/assets/admin/layout/img/bar.jpg" style="vertical-align:top; height:14px; width:800px;"> <span class="sr-only" id="due_par"></span> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Guest Details</span> </div>
    <div class="actions"> <a href="javascript:;" class="btn btn-circle btn-icon-only green" onClick='<?php if($bookingstatusid != 6) {?>tabel_show() <?php } else { echo "checkout_notification()";}?>'><i class="fa fa-pencil-square-o pull-right"></i></a> </div>
  </div>
  <div class="portlet-body form"> 
    
    <!--guest details edit field-->
    <?php
                $form2 = array(
                    'class'       => 'form-body form-horizontal form-row-sepe',
                    'id'        => 'form2',
                    'method'      => 'post',
                    'name'      => 'frm2'
                );
                echo form_open('dashboard/add_guest_info',$form2);
            ?>
    <div class="form-body">
      <div class="first-tabel" id="guest_details_first">
        <table class="table table-striped table-hover" id="guest_holder" >
          <thead>
            <tr>
              <th> Id </th>
              <th> Name </th>
              <th>Sex</th>
              <th> Check In Date </th>
              <th> Check Out Date </th>
              <th> Mobile Number </th>
              <th> Pincode </th>
              <th> Charge </th>
              <!--<th>
                      Action
                  </th>--> 
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><label>
                  <?php if(isset($bookingDetails->guest_id)){echo $bookingDetails->guest_id;}?>
                </label></td>
              <td><label>
                  <?php if(isset($bookingDetails->g_name)){echo $bookingDetails->g_name;}?>
                </label></td>
              <td><label>
                  <?php if(isset($bookingDetails->g_gender)){echo $bookingDetails->g_gender;} ?>
                </label></td>
              <td class="hidden-480"><label>
                  <?php if(isset($bookingDetails->cust_from_date)){echo date('d-m-Y',strtotime($bookingDetails->cust_from_date));} ?>
                </label></td>
              <td class="hidden-480"><label>
                  <?php if(isset($bookingDetails->cust_end_date)){echo date('d-m-Y',strtotime($bookingDetails->cust_end_date));} ?>
                </label></td>
              <td class="hidden-480"><label>
                  <?php if(isset($bookingDetails->g_contact_no)){echo $bookingDetails->g_contact_no;}?>
                </label></td>
              <td class="hidden-480"><label>
                  <?php if(isset($bookingDetails->g_pincode)){echo $bookingDetails->g_pincode;}?>
                </label></td>
              <td class="hidden-480"><label>Yes</label></td>
            </tr>
            <?php
                         $yesnocharge="No";
						$i=0;
                                    if($bookingDetails->guest_id_others !="") {
                                        $other_guests_array = explode(",", $bookingDetails->guest_id_others);


                                        foreach($other_guests_array as $guest_id){
                                            if($guest_id!="") {
                                                $guest = $this->dashboard_model->get_guest_row($guest_id);
                    
                                               ?>
            <tr>
              <td><label><?php echo $guest->g_id;?></label></td>
              <td><label><?php echo $guest->g_name;?></label></td>
              <td><label>
                  <?php if(isset($guest->g_gender)){echo $guest->g_gender;} ?>
                </label></td>
              <td class="hidden-480"><label>
                  <?php if(isset($bookingDetails->cust_from_date)){echo date('d-m-Y',strtotime($bookingDetails->cust_from_date));} ?>
                </label></td>
              <td class="hidden-480"><label>
                  <?php if(isset($bookingDetails->cust_end_date)){echo date('d-m-Y',strtotime($bookingDetails->cust_end_date));} ?>
                </label></td>
              <td class="hidden-480"><label><?php echo $guest->g_contact_no;?></label></td>
              <td class="hidden-480"><label><?php echo $guest->g_pincode;?></label></td>
              <td class="hidden-480"><label><?php echo $yesnocharge; ?></label></td>
            </tr>
            <?php
                          $yesnocharge="No";
                                            }}
                                    }
                                    ?>
          </tbody>
        </table>
      </div>
      <input type="hidden" id="occu_counter" value="0"/>
      <div class="edit_tabel" id="guest_details_edit" style="display:none;">
        <table class="table table-striped table-hover" id="guest_details">
          <thead>
            <tr>
              <th> Id </th>
              <th> Name </th>
              <th>Sex</th>
              <th> Check In Date </th>
              <th> Check Out Date </th>
              <th> Mobile Number </th>
              <th> Pincode </th>
              <th> Charge </th>
              <!--<th>
                                                Action
                                            </th>--> 
            </tr>
          </thead>
          <input type="hidden" name="booking_id" value="<?php echo $_GET['b_id'];?>">
          <input type="hidden" name="guest_id" value="<?php if(isset($bookingDetails->guest_id)){echo $bookingDetails->guest_id;}?>">
          <tbody>
            <tr>
              <td><label>
                  <?php if(isset($bookingDetails->guest_id)){echo $bookingDetails->guest_id ;}?>
                </label></td>
              <td class="form-group"><input type="text" id="g_name" value="" class="form-control input-sm" name="g_name" required="required"></td>
              <td class="form-group"><select id="g_gender" name="g_gender">
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                </select></td>
              <td class="hidden-480 form-group"><input type="text" id="check_in" name="check_in" class="form-control input-sm" readonly value="<?php if(isset($bookingDetails->cust_from_date)){echo date('d-m-Y',strtotime($bookingDetails->cust_from_date));} ?>"></td>
              <td class="hidden-480 form-group"><input type="text" id="check_out" name="check_out" class="form-control input-sm" readonly value="<?php if(isset($bookingDetails->cust_end_date)){echo date('d-m-Y',strtotime($bookingDetails->cust_end_date));} ?>"></td>
              <td class="hidden-480 form-group"><input type="text" class="form-control input-sm" value="" id="g_contact_no" name="g_contact_no" maxlength="10" onkeypress=" return onlyNos(event, this);" required="required"></td>
              <td class="hidden-480 form-group"><input type="text" class="form-control input-sm" value="" id="g_pincode" name="g_pincode" maxlength="10" onkeypress=" return onlyNos(event, this);" required="required"></td>
              <td class="hidden-480 form-group"><select id="g_charge" name="g_charge">
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select></td>
              <!--<td class="hidden-480">
                                                <button class="btn blue pull-left bam" >Edit</button>
                                                <button class="btn green pull-right dan">Save</button>
                                                </td>--> 
              
            </tr>
            <?php
                                        if($bookingDetails->guest_id_others !="") {
                                        $other_guests_array = explode(",", $bookingDetails->guest_id_others);
                    
                                        foreach($other_guests_array as $guest_id){
                                        if($guest_id!="") {
                                        $guest = $this->dashboard_model->get_guest_row($guest_id);
                    
                                        ?>
            <?php }}} ?>
          </tbody>
        </table>
        <!-- <div class="form-actions right">
              <button type="button" class="btn green" onclick="add_guest()">Add Guests</button>
             
            </div>--> 
      </div>
      <!--end of guest details edit field-->
      <h3>Preference</h3>
      <textarea id="preference" disabled="true" class="form-control" rows="3" name="preference"><?php if(isset($bookingDetails->preference)){echo $bookingDetails->preference;}?>
</textarea>
    </div>
    <div id='hd' style="display:none;">
      <div class="form-actions right">
        <button class="btn green" type="submit">Save</button>
        <button class="btn blue" onClick='tabel_show_hide()' type="button">Cancel</button>
      </div>
    </div>
    <?php echo form_close(); ?> </div>
</div>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Stay Details</span> </div>
    <?php if($status_id !="6" && $status_id_secondary !="9"){ ?>
    <div class="actions"> <a href="javascript:;" class="btn btn-circle btn-icon-only green" onClick='stay_show()'><i class="fa fa-pencil-square-o pull-right"></i></a> </div>
    <?php } ?>
  </div>
  <div class="portlet-body form">
    <?php
        $form = array(
          'class'       => 'form-body form-horizontal form-row-sepe',
          'id'        => 'form',
          'method'      => 'post',
          'name'      => 'frm'
        );
        echo form_open('dashboard/add_stay_details',$form);
      ?>
    <div class="form-body">
      <div class="stay_details" id="stay_details">
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th width="15%"> Check In Date </th>
              <th width="15%"> Check In Time </th>
              <th width="20%"> Confirmed Check In Time </th>
              <th width="15%"> Arrival Mode </th>
              <th width="15%"> Arrival Details </th>
              <th width="20%"> Coming From </th>
              <!--<th>
                                        Action
                                    </th>--> 
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><label>
                  <?php if(isset($bookingDetails->cust_from_date)){echo date('d-m-Y',strtotime($bookingDetails->cust_from_date));} ?>
                </label></td>
              <td><label>
                  <?php if(isset($bookingDetails->confirmed_checkin_time)){echo $bookingDetails->checkin_time;} //date('h : i',strtotime($bookingDetails->confirmed_checkin_time));?>
                </label></td>
              <td><label>
                  <?php if(isset($bookingDetails->confirmed_checkin_time)){echo $bookingDetails->confirmed_checkin_time;} //date('h : i',strtotime($bookingDetails->confirmed_checkin_time));?>
                </label></td>
              <td class="hidden-480"><label>
                  <?php if(isset($bookingDetails->arrival_mode)){echo $bookingDetails->arrival_mode;}?>
                </label></td>
              <td class="hidden-480"><label>
                  <?php if(isset($bookingDetails->dept_mode)){echo $bookingDetails->arrival_details;}?>
                </label></td>
              <td class="hidden-480"><label>
                  <?php if(isset($bookingDetails->coming_from)){echo $bookingDetails->coming_from;}?>
                </label></td>
            </tr>
          </tbody>
        </table>
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th width="15%"> Check out Date </th>
              <th width="15%"> Check Out Time </th>
              <th width="20%"> Confirmed Check Out Time </th>
              <th width="15%"> Departure Mode </th>
              <th width="15%"> Departure Details </th>
              <th width="20%"> Next Destination </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><label>
                  <?php if(isset($bookingDetails->cust_end_date)){echo date('d-m-Y',strtotime($bookingDetails->cust_end_date));} ?>
                </label></td>
              <td><label>
                  <?php if(isset($bookingDetails->confirmed_checkout_time)){echo $bookingDetails->checkout_time ;} //date('h : i',strtotime($bookingDetails->confirmed_checkout_time));?>
                </label></td>
              <td><label>
                  <?php if(isset($bookingDetails->confirmed_checkout_time)){echo $bookingDetails->confirmed_checkout_time ;} //date('h : i',strtotime($bookingDetails->confirmed_checkout_time));?>
                </label></td>
              <td class="hidden-480"><label>
                  <?php if(isset($bookingDetails->dept_mode)){echo $bookingDetails->dept_mode;}?>
                </label></td>
              <td class="hidden-480"><label>
                  <?php if(isset($bookingDetails->dept_mode)){echo $bookingDetails->departure_details;}?>
                </label></td>
              <td class="hidden-480"><label>
                  <?php if(isset($bookingDetails->next_destination)){echo $bookingDetails->next_destination ;}?>
                </label></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="stay_details_again" id="stay_details_again" style="display:none;">
        <input type="hidden" name="booking_id" value="<?php echo $_GET['b_id'];?>">
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th> Check In Date </th>
              <th> Check In Time </th>
              <th> Confirmed Check In Time </th>
              <th> Arrival Mode </th>
              <th> Arrival Details </th>
              <th> Coming From </th>
              <!--<th>
                                            Action
                                        </th>--> 
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="form-group"><input id="cust_from_date" class="form-control input-sm" oninput="alert_checkin();" onblur="return_checkin();" type="text" name="cust_from_date" value="<?php if(isset($bookingDetails->cust_from_date)){echo date('d-m-Y',strtotime($bookingDetails->cust_from_date));} ?>">
                <input type="hidden" id="trick1" class="form-control input-sm" value="<?php if(isset($bookingDetails->cust_from_date)){echo date('d-m-Y',strtotime($bookingDetails->cust_from_date));} ?>">
                </input></td>
              <td class="form-group"><input type="text" class="form-control input-sm" name="checkin_time" value="<?php if(isset($bookingDetails->checkin_time)){echo $bookingDetails->checkin_time;}?>"></td>
              <td class="form-group"><input type="text" class="form-control input-sm" name="confirmed_checkin_time" value="<?php if(isset($bookingDetails->confirmed_checkin_time)){echo $bookingDetails->confirmed_checkin_time;}?>"></td>
              <td class="hidden-480 form-group"><input type="text" class="form-control input-sm" name="arrival_mode" value="<?php if(isset($bookingDetails->arrival_mode)){echo $bookingDetails->arrival_mode;}?>"></td>
              <td class="hidden-480 form-group"><input type="text" class="form-control input-sm" name="arrival_details" value="<?php if(isset($bookingDetails->arrival_mode)){echo $bookingDetails->arrival_details;}?>"></td>
              <td class="hidden-480 form-group"><input type="text" class="form-control input-sm" name="coming_from" value="<?php if(isset($bookingDetails->coming_from)){echo $bookingDetails->coming_from;}?>"></td>
              <!--<td class="hidden-480">
                                          <div class="side">
                                            <button class="btn blue pull-left ">Edit</button>
                                            <button class="btn green pull-left ">Save</button>
                                        </div>
                                        </td>--> 
              
            </tr>
          </tbody>
        </table>
        <table class="table table-striped table-hover" style="margin:0;">
          <thead>
            <tr>
              <th> Check out Date </th>
              <th> Check Out Time </th>
              <th> Confirmed Check Out Time </th>
              <th> Departure Mode </th>
              <th> Departure Details </th>
              <th> Next Destination </th>
              <!--<th>
                                            Action
                                        </th>--> 
              
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="form-group"><input id="cust_end_date" class="form-control input-sm"  oninput="alert_checkout();" onblur="return_checkout();" type="text" name="cust_end_date" value="<?php if(isset($bookingDetails->cust_end_date)){echo date('d-m-Y',strtotime($bookingDetails->cust_end_date));} ?>">
                <input type="hidden" class="form-control input-sm" id="trick2" value="<?php if(isset($bookingDetails->cust_end_date)){echo date('d-m-Y',strtotime($bookingDetails->cust_end_date));} ?>">
                </input></td>
              <td class="form-group"><input type="text" class="form-control input-sm" name="checkout_time" value="<?php if(isset($bookingDetails->checkout_time)){echo $bookingDetails->checkout_time;}?>"></td>
              <td class="form-group"><input type="text" class="form-control input-sm" name="confirmed_checkout_time" value="<?php if(isset($bookingDetails->confirmed_checkout_time)){echo $bookingDetails->confirmed_checkout_time;}?>"></td>
              <td class="hidden-480 form-group"><input type="text" class="form-control input-sm" name="dept_mode" value="<?php if(isset($bookingDetails->dept_mode)){echo $bookingDetails->dept_mode;}?>"></td>
              <td class="hidden-480 form-group"><input type="text" class="form-control input-sm" name="departure_details" value="<?php if(isset($bookingDetails->dept_mode)){echo $bookingDetails->departure_details;}?>"></td>
              <td class="hidden-480 form-group"><input type="text" class="form-control input-sm" name="next_destination" value="<?php if(isset($bookingDetails->next_destination)){echo $bookingDetails->next_destination ;}?>"></td>
              <!--  <td class="hidden-480">
                                            <div class="side">
                                                <button class="btn blue pull-left" >Edit</button>
                                               <button class="btn green pull-left">Save</button>
                                           </div>
                                        </td>--> 
              
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="form-actions right" id="stay_details_again1" style="display:none;">
      <button type="submit" class="btn green" onclick="">Save</button>
      <button class="btn blue" type="button" onClick='stay_hide()'>Cancel</button>
    </div>
    <?php echo form_close(); ?> </div>
</div>
<!--------> 
<!--<div id="tab15">
        <div class="form-body">
            <div class="row">
            <div class="form-group col-xs-6">
                <input type="text" id="from_date" name="from_date" class="form-control date-picker"/>
            </div>    
            <div class="form-group col-xs-6">
                <input onBlur="check_availability()" type="text" id="end_date" name="end_date" class="form-control date-picker"/>
            </div>    
            <div id="rooms_data" class="form-group col-xs-6">
                
            </div>
               <div class="form-group col-xs-6" id="original_room_div" style="display: block;">
                <input value="<?php echo $bookingDetails->base_room_rent; ?>" disabled="disabled"  type="text" id="original_room_rent" name="original_room_rent" class="form-control "/>
            </div> 
             <div class="form-group col-xs-6" id="extra_room_div" style="display: none;">
                <input disabled="disabled"  type="text" id="extra_room_rent" name="extra_room_rent" class="form-control "/>
            </div> 
             <div class="form-group col-xs-6" id="" >
                Final Room Rent:
            </div>    
             <div class="form-group col-xs-6" id="" >
                <input  value="<?php echo $bookingDetails->room_rent_sum_total; ?>" type="text" id="room_rent_sum_total" name="room_rent_sum_total" class="form-control "/>
            </div>   
            <input type="hidden" id="room_id" name="room_id"></input>                                              
            </div>
        </div>
        <div class="form-actions right">
            <a onClick="split()" class="btn btn-primary btn-sm" id="dwn_link" style="margin-top:10px; float:right;"> Save <i class=""></i> </a> 
        </div>
    </div>--> 
<!--------> 
<script type="text/javascript">
    
    function check_availability(){
    
    
        var from_date=document.getElementById("from_date").value;
        var end_date=document.getElementById("end_date").value;
    
        jQuery.ajax(
    {
    type: "POST",
    url: "<?php echo base_url(); ?>dashboard/get_available_room",
    
    data: {date1:from_date,date2:end_date},
    success: function(data){
    
    //alert(data);
    
    $("#rooms_data").html(data);
    
    
    }
    });
    }
    
    
    function addRow(room_id,number){
    
    
            jQuery.ajax(
    {
    type: "POST",
    url: "<?php echo base_url(); ?>dashboard/get_room_details?id="+room_id,
    dataType : 'json',
    
    
    
    
    success: function(data){
    
    document.getElementById("extra_room_div").style.display="block";
    
    
    $.each(data, function(i, v) {
    
    
    document.getElementById("extra_room_rent").value=v.room_rent+" ("+v.unit_name+")";
    document.getElementById("room_rent_sum_total").value=parseInt(document.getElementById("room_rent_sum_total").value);//+(parseInt(v.room_rent)-parseInt(document.getElementById("original_room_rent").value));
    document.getElementById("room_id").value=room_id;
    
    
    });				
    
    
    }
    });
    
    }
    
    
    
    function split(){
        var from_date=$("#from_date").val();
         var end_date=$("#end_date").val();
          var room_id=$("#room_id").val();
         var booking_id= <?php echo $_GET["b_id"]; ?>;
         var rent=parseInt($("#room_rent_sum_total").val());

           jQuery.ajax(
    {
    type: "POST",
       dataType : 'json',
    url: "<?php echo base_url(); ?>bookings/add_split_booking",
    
    data: {from_date:from_date,end_date:end_date,booking_id:booking_id,rent:rent, room_id:room_id},
    success: function(data){

      alert(data.room_id);
    
   
    
    }
    });



    }
    </script>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">General Information</span> </div>
    <div class="actions"> <a href="javascript:;" class="btn btn-circle btn-icon-only green" onClick='<?php if($bookingstatusid != 6) {?>gnrl_info()<?php } else { echo "checkout_notification()";}?>'><i class="fa fa-pencil-square-o"></i></a> </div>
  </div>
  <div class="portlet-body form">
    <?php
        $form = array(
          'class'       => 'form-body form-horizontal form-row-sepe',
          'id'        => 'frm',
          'method'      => 'post',
          'name'      => 'frm',
        );
        echo form_open('dashboard/add_gi_details',$form);
            ?>
    <div class="form-body">
      <div id="genarel_information">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-4"><strong style="color: #2B3643;">Nature Of Visit:</strong></label>
              <label class="col-md-8" style="color:#008F6C">
                <?php if(isset($bookingDetails->nature_visit)){echo $bookingDetails->nature_visit ;}?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-4"><strong style="color: #2B3643;">Booking Source:</strong></label>
              <label class="col-md-8" style="color:#008F6C">
                <?php if(isset($bookingDetails->booking_source)){echo $bookingDetails->booking_source ;}?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-4"><strong style="color: #2B3643;">Number of Adult:</strong></label>
              <label class="col-md-8" style="color:#008F6C">
                <?php if(isset($bookingDetails->no_of_adult)){echo $bookingDetails->no_of_adult;}?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-4"><strong style="color: #2B3643;">Male:</strong></label>
              <label class="col-md-8" style="color:#008F6C">
                <?php if(($bookingDetails->no_of_adult_male)>0){echo $bookingDetails->no_of_adult_male;} else{echo "N/A";}?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-4"><strong style="color: #2B3643;">Female:</strong></label>
              <label class="col-md-8" style="color:#008F6C">
                <?php if(($bookingDetails->no_of_adult_female)>0){echo $bookingDetails->no_of_adult_female;} else{ echo "N/A";}?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-4"><strong style="color: #2B3643;">Number of Child:</strong></label>
              <label class="col-md-8" style="color:#008F6C">
                <?php if(isset($bookingDetails->no_of_child)){echo $bookingDetails->no_of_child ;}?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-4"><strong style="color: #2B3643;">Booking Note:</strong></label>
              <label class="col-md-8" style="color:#008F6C">
                <?php if(isset($bookingDetails->comment)){echo $bookingDetails->comment ;}?>
              </label>
            </div>
          </div>
        </div>
      </div>
      <div id="genarel_information_again" style="display:none;">
        <input type="hidden" name="booking_id" value="<?php echo $_GET['b_id'];?>">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-4 control-label">Nature Of Visit:</label>
              <div class="col-md-8">
                <?php $visit=$this->dashboard_model->getNatureVisit(); ?>
                <select   class="form-control input-sm" placeholder=" Booking Type" name="nature_visit">
                  <!---- <option value="Travel & Leisure" <?php if(isset($bookingDetails->nature_visit) && $bookingDetails->nature_visit=='Travel & Leisure'){ echo 'selected="selected"';}?>>Travel &amp; Leisure</option>
                      <option value="Business" <?php if(isset($bookingDetails->nature_visit) && $bookingDetails->nature_visit=='Business'){ echo 'selected="selected"';}?>>Business</option>
                    ----->
                  <?php foreach ($visit as $key ) {
                                  ?>
                  <option value="<?php echo $key->booking_nature_visit_name; ?>" <?php if(isset($bookingDetails->nature_visit) && $key->booking_nature_visit_name==$bookingDetails->nature_visit){ echo 'selected="selected"';}?>><?php echo $key->booking_nature_visit_name; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-4 control-label">Booking Source:</label>
              <div class="col-md-8">
                <select  class="form-control input-sm" name="booking_source">
                  <!---  <option value="" disabled="" selected="">Select Source</option>
                      <option value="Frontdesk" <?php if(isset($bookingDetails->booking_source) && $bookingDetails->booking_source=='Frontdesk'){ echo 'selected="selected"';}?>>Frontdesk</option>
                      <option value="Online" <?php if(isset($bookingDetails->booking_source) && $bookingDetails->booking_source=='Online Website'){ echo 'selected="selected"';}?>>Online Website</option>
                      <option value="Telephonic" <?php if(isset($bookingDetails->booking_source) && $bookingDetails->booking_source=='Telephonic'){ echo 'selected="selected"';}?>>Telephonic</option>
                      <option value="Broker" <?php if(isset($bookingDetails->booking_source) && $bookingDetails->booking_source=='Broker'){ echo 'selected="selected"';}?>>Broker</option>
                      <option value="Broker" <?php if(isset($bookingDetails->booking_source) && $bookingDetails->booking_source=='Booking Channel'){ echo 'selected="selected"';}?>>Booking Channel</option>
							-->
                  
                  <?php 
						  $dta=$this->unit_class_model->get_booking_source();
						  if($dta)
						  {
							  foreach($dta as $d) 
						  {
							  ?>
                  <option value="<?php echo $d->booking_source_name;?>" <?php if(isset($bookingDetails->booking_source) && $d->booking_source_name == $bookingDetails->booking_source){ echo 'selected="selected"';}?>><?php echo $d->booking_source_name;?></option>
                  <?php } }?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-4 control-label">Number of Adult:</label>
              <div class="col-md-8">
                <input type="text" class="form-control input-sm" name="no_of_adult" value="<?php if(isset($bookingDetails->no_of_adult)){echo $bookingDetails->no_of_adult ;}?>" id="text4" onkeypress="return IsNumeric3(event);" ondrop="return false;" onpaste="return false;">
                <span id="error3" style="color: Red; display: none">* Input digits</span> </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="col-md-4 control-label">M:</label>
              <div class="col-md-8">
                <input type="text" class="form-control input-sm" name="no_of_adult_male" value="<?php if(isset($bookingDetails->no_of_adult_male)){echo $bookingDetails->no_of_adult_male ;}?>" id="text3" onkeypress="return IsNumeric2(event);" ondrop="return false;" onpaste="return false;">
                <span id="error2" style="color: Red; display: none">* Input digits</span> </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="col-md-4 control-label">F:</label>
              <div class="col-md-8">
                <input type="text" class="form-control input-sm" name="no_of_adult_female" value="<?php if(isset($bookingDetails->no_of_adult_female)){echo $bookingDetails->no_of_adult_female ;}?>" id="text1" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;">
                <span id="error" style="color: Red; display: none">* Input digits</span>
                </li>
                <span id="error5" style="color: Red; display: none">* Summation of male and female should be the number of adults.</span> </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-4 control-label">Number of Child:</label>
              <div class="col-md-8">
                <input type="text" class="form-control input-sm" name="no_of_child" value="<?php if(isset($bookingDetails->no_of_child)){echo $bookingDetails->no_of_child ;}?>" id="text2" onkeypress="return IsNumeric1(event);" ondrop="return false;" onpaste="return false;">
                <span id="error1" style="color: Red; display: none">* Input digits</span> </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-4 control-label">Booking Note:</label>
              <div class="col-md-8">
                <input type="text" class="form-control input-sm"  name="comment" value="<?php if(isset($bookingDetails->comment)){echo $bookingDetails->comment ;}?>">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-actions right" id="genarel_information_again1" style="display:none;">
      <button type="button" id="btn" class="btn green">Save</button>
      <button type="button" class="btn blue" onClick='gnrl_info_hide()'>Cancel</button>
    </div>
    <?php echo form_close(); ?> </div>
</div>
<div class="portlet light bordered">
  <div  class="portlet-title">
    <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Broker/Channel Information</span> </div>
    <div class="col-md-4">
      <div class="form-group">
        <select class="form-control input-sm" onchange="brokerchange(this.value)">
          <option value="" disabled="disabled" selected="selected">--Select Broker or Channel--</option>
          <option value="broker">Broker</option>
          <option value="channel">Channel</option>
        </select>
      </div>
    </div>
    <script type="text/javascript">
          function brokerchange(value){
            if(value=="broker"){
              document.getElementById("brokbrok").style.display="block";
              document.getElementById("chanchan").style.display="none";
            }else{
               document.getElementById("brokbrok").style.display="none";
              document.getElementById("chanchan").style.display="block";

            }
          }
        </script> 
  </div>
  <div id="brokbrok"  class="portlet light bordered">
    <div  class="portlet-title">
      <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Broker Information</span> </div>
      <div class="actions"> <a href="javascript:;" class="btn btn-circle btn-icon-only green" onClick='<?php if($bookingstatusid != 6) {?>gnrl_info1()<?php } else { echo "checkout_notification()";}?>'><i class="fa fa-pencil-square-o"></i></a> </div>
    </div>
    <div class="portlet-body form">
      <div class="form-body form-horizontal form-row-sepe">
        <div class="form-body">
          <div id="genarel_information_broker">
            <div class="row">
              <?php if(isset($bookingDetails->booking_source) && $bookingDetails->booking_source == 'Broker'){
$broker = $this->dashboard_model->get_brokerNameByID($bookingDetails->broker_id);
if($broker){?>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="col-md-6">Broker Name:</label>
                  <label class="col-md-6"><?php echo $broker->b_name ;?></label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="col-md-6">% Commision:</label>
                  <label class="col-md-6"><?php echo $broker->broker_commission ; ?></label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="col-md-6">Broker Ammount:</label>
                  <label class="col-md-6"><?php echo $bookingDetails->broker_commission ;?></label>
                </div>
              </div>
              <?php } }?>
            </div>
          </div>
          <div id="genarel_information_again_broker" style="display: none;" >
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label class="col-md-6 control-label">Broker Name:</label>
                  <div class="col-md-6">
                    <input type="hidden" value="<?php if($bookingDetails->room_rent_sum_total !=0){echo $bookingDetails->room_rent_sum_total ;}else{
                                                                    echo $bookingDetails->room_rent_total_amount;
                                                                    } ?>" id="total_booking_amount">
                    </input>
                    <select  onchange="return_commission(this.value)" class="form-control input-sm">
                      <option  value="" disabled="true" selected="true">--Select Broker--</option>
                      <?php 
                                                                            $brokers=$this->dashboard_model->all_broker_limit();
                                                                            foreach ($brokers as $broker ) {
                                                                                # code...
                                                                            
                                                                            ?>
                      <option  value="<?php echo $broker->b_id; ?>"><?php echo $broker->b_name; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="col-md-6 control-label">% Commision:</label>
                  <div class="col-md-6">
                    <input id="commission" type="text" class="form-control input-sm"  >
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="col-md-6 control-label">Broker Ammount:</label>
                  <div class="col-md-6">
                    <input id="ammount" type="text" class="form-control input-sm"  >
                    <input type="hidden" id="broker_id" type="text">
                    <input type="hidden" id="show_id" type="text" value="<?php echo 'HM0'.$this->session->userdata('user_hotel').'00'.$bookingDetails->booking_id; ?>">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-actions right" id="genarel_information_again_broker1" style="display: none;">
          <button onclick="save_broker()" class="btn green">Save</button>
          <button class="btn blue" onClick='gnrl_info_hide1()'>Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <div  id="chanchan" style="display: none;"  class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Channel Information</span> </div>
      <div class="actions"> <a href="javascript:;" class="btn btn-circle btn-icon-only green" onClick='gnrl_info_channel()'><i class="fa fa-pencil-square-o"></i></a> </div>
    </div>
    <div class="portlet-body form">
      <div class="form-body form-horizontal form-row-sepe">
        <div class="form-body">
          <div id="genarel_information_channel">
            <div class="row">
              <?php if(isset($bookingDetails->booking_source) && $bookingDetails->booking_source == 'Channel'){
$broker = $this->dashboard_model->get_channelNameByID($bookingDetails->channel_id);?>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="col-md-6">Channel Name:</label>
                  <label class="col-md-6"><?php echo $broker->channel_name ;?></label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="col-md-6">% Commision:</label>
                  <label class="col-md-6"><?php echo $broker->channel_commission ; ?></label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="col-md-6">Channel Ammount:</label>
                  <label class="col-md-6"><?php echo $bookingDetails->channel_commission ;?></label>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
          <div id="genarel_information_again_channel" style="display: none;" >
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label class="col-md-6 control-label">Channel Name:</label>
                  <div class="col-md-6">
                    <input type="hidden" value="<?php if($bookingDetails->room_rent_sum_total !=0){echo $bookingDetails->room_rent_sum_total ;}else{
                                                                    echo $bookingDetails->room_rent_total_amount;
                                                                    } ?>" id="total_booking_amount2">
                    </input>
                    <select  onchange="return_commission2(this.value)" class="form-control input-sm">
                      <option  value="" disabled="true" selected="true">--Select Channel--</option>
                      <?php 
                                                                            $brokers=$this->dashboard_model->all_channel_limit();
                                                                            foreach ($brokers as $channel ) {
                                                                                # code...
                                                                            
                                                                            ?>
                      <option  value="<?php echo $channel->channel_id; ?>"><?php echo $channel->channel_name; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="col-md-6 control-label">% Commision:</label>
                  <div class="col-md-6">
                    <input id="commission2" type="text" class="form-control input-sm"  >
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="col-md-6 control-label">Channel Ammount:</label>
                  <div class="col-md-6">
                    <input id="ammount2" type="text" class="form-control input-sm"  >
                    <input type="hidden" id="channel_id" type="text">
                    <input type="hidden" id="show_id" type="text" value="<?php echo 'HM0'.$this->session->userdata('user_hotel').'00'.$bookingDetails->booking_id; ?>">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-actions right" id="genarel_information_again_channel1" style="display: none;">
          <button onclick="save_channel()" class="btn green">Save</button>
          <button class="btn blue" onClick='gnrl_info_hide1()'>Cancel</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Payment Information</span> </div>
    <div class="actions"> <a class="btn btn-circle btn-icon-only green" onClick='<?php if(( $bookingDetails->group_id=='0')){if(($bookingstatusid != 6)) {?>payment_info()<?php } else { echo "checkout_notification()";}}else {echo "single_group_alert();";}?>' href="javascript:;"><i class="fa fa-pencil-square-o"></i></a> </div>
  </div>
  <div class="portlet-body form">
    <?php
        $form1 = array(
          'class'       => 'form-body form-horizontal form-row-sepe',
          'id'        => 'form1',
          'method'      => 'post',
          'name'      => 'frm1'
        );
        echo form_open('dashboard/add_pay_info',$form1);
      ?>
    <div class="form-body">
      <div id="payment_information">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Room Rent Type:</strong></label>
              <label class="col-md-6" style="color:#008F6C">
                <?php $room_det=$this->dashboard_model->get_room($bookingDetails->room_id); 
				  if($bookingDetails->base_room_rent == $room_det->room_rent)
				  {
					echo "Basic Rate";  
				  }
				  elseif ($bookingDetails->base_room_rent == $room_det->room_rent_seasonal)
				  {
					  echo "Seasonal Rate";
				  }
				  elseif ($bookingDetails->base_room_rent == $room_det->room_rent_weekend)
				  {
					  echo "Weekend Rate";
				  }
				  else
				  {
					  echo "N/A";
				  }
				  
				  //print_r($room_det);
				  ?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Actual Room Rent Amount:</strong></label>
              <label class="col-md-6" style="color:#008F6C">
			                  <?php if(isset($bookingDetails->base_room_rent)){echo $bookingDetails->base_room_rent;?>
                / Day
                <?php } else {echo "N/A";}?>
</label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Room Rent Modifier:</strong></label>
              <label class="col-md-6" style="color:#008F6C">
                <?php if(isset($bookingDetails->mod_room_rent)){echo $bookingDetails->mod_room_rent;}?>
                <?php if(isset($bookingDetails->rent_mode_type)){if($bookingDetails->rent_mode_type == 'add'){ echo '(Premium)'; }else { echo '(Discount)'; }} else {echo "N/A";}?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Room Rent Amount:</strong></label>
              <label class="col-md-6" style="color:#008F6C">
                <?php 
                    if(isset($bookingDetails->rent_mode_type)){
                    if($bookingDetails->rent_mode_type == 'add'){ 
                      $val = $bookingDetails->base_room_rent+$bookingDetails->mod_room_rent;
                    } else {
                      $val = $bookingDetails->base_room_rent-$bookingDetails->mod_room_rent;
                    } 
                    echo $val;} else { echo "N/A"; }?>
                / Day 
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Number Of Days:</strong></label>
              <label class="col-md-6" style="color:#008F6C">
                <?php if(isset($bookingDetails->stay_days)){echo $bookingDetails->stay_days;}?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Total Room Rent:</strong></label>
              <label class="col-md-6" style="color:#008F6C">
                <?php if(isset($bookingDetails->base_room_rent)){echo $val*$bookingDetails->stay_days;}?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Tax Type:</strong></label>
              <label class="col-md-6" style="color:#008F6C">
                <?php //if(isset($bookingDetails->room_rent_tax_details))if($bookingDetails->service_tax==0 && $bookingDetails->service_charge==0 && $bookingDetails->luxury_tax==0){echo "No Tax";} elseif($bookingDetails->service_tax!=0 && $bookingDetails->service_charge!=0 && $bookingDetails->luxury_tax==0){ echo "Service Tax+Service Charge";}else{echo "Service Tax+Service Charge+Luxury Tax";}
					if(isset($bookingDetails->room_rent_tax_details)){
						echo $bookingDetails->room_rent_tax_details;
					}
				  ?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Tax Amount:</strong></label>
              <label class="col-md-6" style="color:#008F6C">
                <?php if(isset($bookingDetails->room_rent_tax_amount)){echo $bookingDetails->room_rent_tax_amount;}?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Discount:</strong></label>
              <label class="col-md-6" style="color:#008F6C">
                <?php if(isset($bookingDetails->discount)){echo $bookingDetails->discount;}?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Total Room Rent(Incl Tax):</strong></label>
              <label class="col-md-6" style="color:#008F6C">
                <?php if(isset($bookingDetails->room_rent_sum_total)&&($bookingDetails->room_rent_sum_total > 0)){echo number_format(($bookingDetails->room_rent_total_amount+$bookingDetails->room_rent_tax_amount),2,'.',',');}
					else
					{
						echo number_format(($bookingDetails->room_rent_total_amount+$bookingDetails->room_rent_tax_amount),2,'.',',');
					}
					?>
              </label>
            </div>
          </div>
        </div>
      </div>
      <div id="payment_information_again" style="display:none">
        <div class="row">
          <input type="hidden" name="booking_id" value="<?php echo $_GET['b_id'];?>">
          <?php

                            if(isset($bookingDetails->base_room_rent) && isset($bookingDetails->stay_days)){
                            $trr= $bookingDetails->base_room_rent*$bookingDetails->stay_days;
                            $tax = $bookingDetails->room_rent_tax_amount;
                            $tax_percent = ($tax/$trr)*100;
                            } else{
                            $tax_percent=0;
                            $val= 0;  
                            }
                            ?>
          <input type="hidden" name="tax_type" id="tax_type">
          </input>
          <input type="hidden" name="tax_percent" value="<?php echo $tax_percent;?>">
          <input type="hidden" name="actual_room_rent" value="<?php echo $val;?>">
          <input type="hidden" name="stay_days" value="<?php if(isset($bookingDetails->stay_days)){echo $bookingDetails->stay_days;}?>">
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6 control-label">Room Rent Modifier Type:</label>
              <div class="col-md-6">
                <select id="modifier_type" class="form-control input-sm" name="modifier_type">
                  <option value="" disabled="" selected="">--Modifier Type--</option>
                  <option value="add">Add</option>
                  <option value="substract">Substract</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6 control-label">Room Rent Modifier Ammount:</label>
              <div class="col-md-6">
                <input onblur="modify(this.value)" value="0" id="modifier_amount" type="text" class="form-control input-sm" name="modifier_amount">
              </div>
            </div>
          </div>
          <script type="text/javascript">
                function modify(value){
                  var days=<?php echo $bookingDetails->stay_days ?>;
                  var type=document.getElementById("modifier_type").value;
                  var rent=parseInt(document.getElementById("base_room_rent1").value);
                  var tax=parseInt(document.getElementById("room_rent_tax_amount").value);
                 
                  if(type=="add"){
                    document.getElementById("base_room_rent").value=rent+parseInt(value);
                    document.getElementById("room_rent_total_amount").value=(rent+parseInt(value))*parseInt(days); //document.getElementById("room_rent_total_amount").value=parseInt(document.getElementById("room_rent_total_amount").value)+(parseInt(days)*parseInt(value));
                    //document.getElementById("room_rent_sum_total").value=parseInt(   document.getElementById("room_rent_total_amount").value)+tax;
					document.getElementById("room_rent_sum_total").value=(rent+parseInt(value))*parseInt(days)+tax;


                  }else if(type=="substract"){

                     document.getElementById("base_room_rent").value=rent-parseInt(value);
                     document.getElementById("room_rent_total_amount").value=(rent-parseInt(value))*parseInt(days);
                  	 document.getElementById("room_rent_sum_total").value=(rent-parseInt(value))*parseInt(days)+tax;				 //document.getElementById("room_rent_total_amount").value=parseInt(document.getElementById("room_rent_total_amount").value)-(parseInt(days)*parseInt(value));
                       //document.getElementById("room_rent_sum_total").value=parseInt(   document.getElementById("room_rent_total_amount").value)+tax;

                  }else{
                    return false;
                  }
                }
              </script>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6 control-label">Base Room Rent :</label>
              <div class="col-md-6">
                <input value="<?php echo $bookingDetails->base_room_rent; ?>" id="base_room_rent" type="text" class="form-control input-sm" name="actual_room_rent">
                <input value="<?php echo $bookingDetails->base_room_rent; ?>" id="base_room_rent1" type="hidden" class="form-control input-sm" >				
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6 control-label">Room Rent Total Ammount:</label>
              <div class="col-md-6">
                <input value="<?php echo $bookingDetails->room_rent_total_amount; ?>" id="room_rent_total_amount" type="text" class="form-control input-sm" name="room_rent_total_amount">
                <input value="<?php echo $bookingDetails->room_rent_total_amount; ?>" id="room_rent_amount" type="hidden" class="form-control input-sm" name="room_rent_amount">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <input type="hidden" id="sc" name="sc" value="<?php echo $bookingDetails->service_charge; ?>">
              <input type="hidden" id="st" name="st" value="<?php echo $bookingDetails->service_tax; ?>">
              <input type="hidden" id="lx" name="lx" value="<?php echo $bookingDetails->luxury_tax; ?>">
              <label class="col-md-6 control-label">Tax Ammount:</label>
              <div class="col-md-6">
                <select onchange="change_tax(this.value)" name="booking_tax" id="booking_tax" class="form-control input-sm" >
                  <option value="" disabled selected>--tax Type--</option>
                  <option value="<?php echo 0;?>" <?php if(isset($bookingDetails->room_rent_tax_details) && $bookingDetails->room_rent_tax_details="No Tax") echo 'selected';?> >No Tax</option>
                  <option id="service" value="<?php echo "sum"; ?>" <?php if(isset($bookingDetails->room_rent_tax_details) && $bookingDetails->room_rent_tax_details="Service tax + Service Charge")  echo 'selected';?>>Service tax + Service Charge</option>
                  <option id="luxury" value="<?php echo "sum2"; ?>" <?php if(isset($bookingDetails->room_rent_tax_details) && $bookingDetails->room_rent_tax_details="Service tax + Service Charge + Luxury Tax")  echo 'selected';?>>Service tax + Service Charge + Luxury Tax</option>
                </select>
                <input type="hidden" name="hid" id="hid" value="">
              </div>
            </div>
          </div>
          <script type="text/javascript">
                function change_tax(value){
                  var base=parseInt(document.getElementById("base_room_rent").value);
                  //var tax_range=parseInt(document.getElementById("luxury_tax_slab1_range_to").value);
				  //alert(base);
				  if(base < 1100){
					  l=0; 
				  } else {
					  if(base>1100 && base<3000){
						l=5;
					  }else{
						l=10;
					  }
				  }
				  //alert(l);
                  //sc=3;
                 // st=8.7;
                  var total=document.getElementById("room_rent_total_amount").value;
				  //alert(total);
                  var service_charge=document.getElementById("sc").value;
                  var service_tax=document.getElementById("st").value;
                  var luxury_tax=document.getElementById("lx").value;
					//alert(service_charge);	
				if(value=="sum"){
                   // document.getElementById("room_rent_tax_amount").value=parseInt(total)*((service_charge)+(service_tax)/100);
                    document.getElementById("room_rent_tax_amount").value=parseInt(total)*((parseFloat(service_charge)+parseFloat(service_tax))/100);
                    document.getElementById("tax_type").value="Service Charge + Service Tax ";
					document.getElementById("hid").value="Service tax + Service Charge";

                  }else if(value=="sum2"){
                     document.getElementById("room_rent_tax_amount").value=parseInt(total)*((parseFloat(service_charge)+parseFloat(service_tax)+parseFloat(luxury_tax))/100);
                     document.getElementById("tax_type").value="Service Charge + Service Tax + "+l+"% Luxury Tax";
					 document.getElementById("hid").value="Service tax + Service Charge + Luxury Tax";
                  }else{
                     document.getElementById("room_rent_tax_amount").value=0;
                     document.getElementById("tax_type").value="No Tax";
					  document.getElementById("hid").value="No Tax";
                  }

                  document.getElementById("room_rent_sum_total").value=parseFloat(document.getElementById("room_rent_total_amount").value)+parseFloat(document.getElementById("room_rent_tax_amount").value);
                }

                 
              </script>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6 control-label">Room Rent Tax Ammount:</label>
              <div class="col-md-6">
                <input value="<?php echo $bookingDetails->room_rent_tax_amount; ?>" id="room_rent_tax_amount" type="text" class="form-control input-sm" name="room_rent_tax_amount">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6 control-label">Room Rent Sum Total :</label>
              <div class="col-md-6">
                <input value="<?php echo $bookingDetails->room_rent_sum_total; ?>" id="room_rent_sum_total" type="text" class="form-control input-sm" name="room_rent_sum_total">
              </div>
            </div>
          </div>
          <input type="hidden" id="shadowcost" value="<?php echo $bookingDetails->room_rent_sum_total; ?>">
          </input>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6 control-label">Discount :</label>
              <div class="col-md-6">
                <input onblur="give_dis(this.value)" value="<?php echo $bookingDetails->discount; ?>" id="discount" type="text" class="form-control input-sm" name="discount">
              </div>
            </div>
          </div>
          <script type="text/javascript">
                 function give_dis(val){
                 
                 var val_n=parseInt(val);
                  var val_t=parseInt(document.getElementById("shadowcost").value);
				 // alert(val_n);
                 alert(val_t+val_t);
                  document.getElementById("room_rent_sum_total").value=val_t+(val_t*(val_n/100));
                }
              </script> 
        </div>
      </div>
    </div>
    <div class="form-actions right" id="payment_information_again1" style="display:none">
      <button class="btn green" type="submit">Save</button>
      <button class="btn blue" onClick='payment_info_hide()' type="button">Cancel</button>
    </div>
    <?php echo form_close(); ?> </div>
</div>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">All Feedbacks</span> </div>
  </div>
  <div class="portlet-body form">
    <div class="form-body form-horizontal form-row-sepe">
      <div class="form-body">
        <table class="table table-striped table-hover" >
          <thead>
            <tr>
              <th> Guest Name </th>
              <th> Overall Quality </th>
              <th> Come back again </th>
              <th> Refer Friend </th>
              <th> Resonable Price </th>
              <th> Comment </th>
            </tr>
          </thead>
          <tbody>
            <?php 
                                    if(!empty($feedback)){
                                    foreach($feedback as $feed){
                                    ?>
            <tr>
              <td><label><?php echo $feed->guest_name;?></label></td>
              <td><label>
                  <?php $ease = ceil($feed->tot/12);
                                                    for ($x = 1; $x <= 5; $x++) {
                                                        if($x <= $ease){
                                                            echo '<i class="fa fa-star"style="color:red;"></i>';
                                                        } else {
                                                            echo '<i class="fa fa-star"style="color:black;"></i>';
                                                        }
                                                    }
                                                ?>
                </label></td>
              <td class="hidden-480"><label>
                  <?php $ease = $feed->come_back;
                                                    
                                                        if($ease == '2'){
                                                            echo 'Maybe';
                                                        } else if($ease == '1'){
                                                            echo 'Yes';
                                                        } else {
                                                            echo 'No';
                                                        }
                                                    
                                                ?>
                </label></td>
              <td><label>
                  <?php $ease = $feed->refer_friend;
                                                    
                                                        if($ease == '2'){
                                                            echo 'Maybe';
                                                        } else if($ease == '1'){
                                                            echo 'Yes';
                                                        } else {
                                                            echo 'No';
                                                        }
                                                    
                                                ?>
                </label></td>
              <td><label>
                  <?php $ease = $feed->reasonable_cost;
                                                    
                                                        if($ease == '2'){
                                                            echo 'Maybe';
                                                        } else if($ease == '1'){
                                                            echo 'Yes';
                                                        } else {
                                                            echo 'No';
                                                        }
                                                    
                                                ?>
                </label></td>
              <td class="hidden-480"><label><?php echo $feed->comment;?></label></td>
            </tr>
            <?php } } else {?>
            <tr>
              <td colspan="6"><label>No Feedback Available</label></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="form-actions right"> <a href="<?php echo base_url() ?>dashboard/add_feedback?bID=<?php echo $_GET['b_id'];?>" class="btn blue">Add Feadback</a> </div>
    </div>
  </div>
</div>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Extra Charge Added:</span> 
      <!--<i class="fa fa-pencil-square-o pull-right" style="color:gray;margin-top:-22px;font-size:20px;cursor:pointer;" onClick='extra_info()'></i>--> 
    </div>
  </div>
  <div class="portlet-body form">
    <div class="form-body form-horizontal form-row-sepe">
      <div class="form-body">
        <table class="table table-striped table-hover" >
          <thead>
            <tr>
              <th width="25%"> Description </th>
              <th width="15%"> Qty </th>
              <th width="20%"> Unit Price </th>
              <th width="15%"> Tax % </th>
              <th width="20%"> Total </th>
              <th width="5%"> Action </th>
            </tr>
          </thead>
          <tbody>
            <?php
                                    $bookings=$this->dashboard_model->get_booking_details($_GET['b_id']);
                    
                                        if(isset($bookings->booking_extra_charge_id) && $bookings->booking_extra_charge_id !='0') {
                                            $charge_id_array=explode(",",$bookings->booking_extra_charge_id);
                    
                                           for($i=1;$i<sizeof($charge_id_array);$i++) {
                    
                                               $charge_details=$this->dashboard_model->get_charge_details($charge_id_array[$i]);
                    
                                               ?>
            <tr>
              <td><label><?php echo $charge_details->crg_description ?></label></td>
              <td><label><?php echo $charge_details->crg_quantity ?></label></td>
              <td class="hidden-480"><label><?php echo $charge_details->crg_unit_price ?></label></td>
              <td class="hidden-480"><label><?php echo $charge_details->crg_tax ?></label></td>
              <td class="hidden-480"><label><?php echo $charge_details->crg_total ?></label></td>
              <td ><a <?php if($bookingstatusid != 6) {?> href="<?php echo base_url() ?>dashboard/delete_charge?c_id=<?php echo $charge_details->crg_id ?>&booking_id=<?php echo $bookingDetails->booking_id; ?>"<?php } else { echo "onclick='checkout_notification()'";}?> class="btn red btn-sm" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
            </tr>
            <?php
                                           }}else{?>
            <tr>
              <td colspan="6"><label style="color:red;">Not Availabe</label></td>
            </tr>
            <?php
                    
                                        }
                                  ?>
            <tr>
              <td  class="form-group"><input onkeyup="search_charge()" id="c_name" type="text" value="" class="form-control input-sm" placeholder="Description"></td>
              <td class="form-group"><input id="c_quantity" type="text" value="" class="form-control input-sm" placeholder="Qty"></td>
              <td class="hidden-480 form-group"><input onblur="calculation(this.value)" id="c_unit_price" type="text" value="" class="form-control input-sm" placeholder="Unit Price"></td>
              <td class="hidden-480 form-group"><input id="c_tax" onblur="calculation2(this.value)" type="text" value="" class="form-control input-sm" placeholder="Tax %"></td>
              <td class="hidden-480 form-group"><input id="c_total" type="text" value="" class="form-control input-sm" placeholder="Total"></td>
              <td class="form-group"><button class="btn green" onclick="<?php if($bookingstatusid != 6) {?>add_charge()<?php } else { echo "checkout_notification()";}?>"><i class="fa fa-plus" aria-hidden="true"></i></button>
                <br></td>
            </tr>
            <tr>
              <td style="padding:0px;"><div id="charges" style="display:none; z-index: 1200; max-height:200px;overflow:scroll;overflow-x: hidden"></div></td>
            </tr>
            <script>
          
function search_charge(){

  var value=document.getElementById("c_name").value;

  //alert(value);

    $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>dashboard/search_charge",
    data:{keyword:value},
    success: function(data){
		if(data != "")
		{
	  $("#charges").show();
      $('#charges').html(data);
		}
    }
    });
 }
//To select charge name
function selectCountry(val,unit_price,tax) 
{
$("#c_unit_price").val(unit_price);
$("#c_tax").val(tax);
$("#c_name").val(val);
$("#charges").hide();
}



                            function calculation(value) {
								if(value > '0'){
									var a=document.getElementById("c_quantity").value;
									var total=parseFloat(a)*parseFloat(value);
									document.getElementById("c_total").value=total;
								} else {
									document.getElementById("c_total").value='0'
									document.getElementById("c_tax").value='0'
								}
                                
                            }
                            function calculation2(value) {
								if(value > '0'){
									var a=document.getElementById("c_total").value;
									var total=parseFloat(a)+(parseInt(a)*(parseFloat(value)/100));
									document.getElementById("c_total").value=total;
								} else {
									var x=document.getElementById("c_unit_price").value;
									var y=document.getElementById("c_quantity").value;
									document.getElementById("c_total").value=parseInt(x)*parseInt(y);
								}
            
                            }
                        </script>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Extra service Added:</span> </div>
  </div>
  <!--<i class="fa fa-pencil-square-o pull-right" style="color:gray;margin-top:-22px;font-size:20px;cursor:pointer;" onClick='extra_info()'></i>-->
  <div class="portlet-body form">
    <div class="form-body form-horizontal form-row-sepe">
      <div class="form-body">
        <div id="extra" style="display:block;">
          <table class="table table-striped table-hover" >
            <thead>
              <tr>
                <th width="35%"> Extra Service Name </th>
                <th width="30%"> Extra Service Amount </th>
                <th width="30%"> Extra Service Tax (%) </th>
                <th width="5%"> Action </th>
              </tr>
            </thead>
            <tbody>
              <?php
                                        $bookings=$this->dashboard_model->get_booking_details($_GET['b_id']);
                        
                                            if(isset($bookings->service_id) && $bookings->service_id !='0') {
                                                $service_id_array=explode(",",$bookings->service_id);
                        
                                               for($i=1;$i<sizeof($service_id_array);$i++) {
                        
                                                   $service_details=$this->dashboard_model->get_service_details($service_id_array[$i]);
                        
                                                   ?>
              <tr>
                <td><label><?php echo $service_details->s_name ?></label></td>
                <td><label><?php echo $service_details->s_price ?></label></td>
                <td class="hidden-480"><label><?php echo $service_details->s_tax ?></label></td>
                <td ><a class="btn red btn-sm" href="<?php echo base_url() ?>dashboard/delete_service?s_id=<?php echo $service_details->s_id ?>&booking_id=<?php echo $bookingDetails->booking_id; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
              </tr>
              <?php
                                               }}else{?>
            <label style="color:red;">Not Availabe</label>
            <?php
                        
                                            }
                                      ?>
              </tbody>
            
          </table>
        </div>
        <div id="extra_again" style="display:block;">
          <h4 style="text-align:center; border-top:1px solid #eee; padding:10px 0 25px;"><strong>Available Services:</strong></h4>
          <table class="table table-striped table-hover" id="extra_service">
            <thead>
              <tr>
                <th width="35%"> Extra Service Name </th>
                <th width="30%"> Extra Service Amount </th>
                <th width="30%"> Extra Service Tax (%) </th>
                <th width="5%"> Action </th>
              </tr>
            </thead>
            <tbody>
              <?php
                                        $services=$this->dashboard_model->all_services();
                                        if(isset($services) && $services){
                                            foreach($services as $service){
                                                $match=   $this->dashboard_model->service_booking_match( $_GET['b_id'],$service->s_rules);
                                                if(isset($match)&& $match){
                        
                                                    ?>
              <tr>
                <td class="form-group"><input id="s_id" type="hidden" value="<?php echo  $service->s_id; ?>" class="form-control input-sm">
                  <input id="s_name" type="text" value="<?php echo  $service->s_name; ?>" class="form-control input-sm"></td>
                <td class="form-group"><input id="s_price" type="text" value="<?php echo  $service->s_price; ?>" class="form-control input-sm"></td>
                <td class="hidden-480 form-group"><input id="s_tax" type="text" class="form-control input-sm" value="<?php echo  $service->s_tax; ?>"></td>
                <td class="form-group"><button class="btn green" onclick="<?php if($bookingstatusid != 6) {?>add_service('<?php echo $service->s_id; ?>','<?php echo $service->s_price; ?>','<?php echo $service->s_tax; ?>')<?php } else { echo "checkout_notification()";}?>" ><i class="fa fa-plus" aria-hidden="true"></i></button>
                  <br></td>
              </tr>
              <?php
                                                }
                        
                                            }
                                        }
                                        ?>
            </tbody>
          </table>
          <!-- <button class="btn BLUE pull-right" onclick="add_row_again()" >Add Ano</button><br><br>--> 
        </div>
      </div>
    </div>
  </div>
</div>
<!--<div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Package Information</span> </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body form-horizontal form-row-sepe">
          <div class="form-body">
            <h4 style="color:red;margin-top:20px; text-align:center;">Not Availabe</h4>
          </div>
        </div>
      </div>
    </div>-->
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">POS Information</span> </div>
  </div>
  <div class="portlet-body form">
    <div class="form-body form-row-sepe">
      <div class="form-body">
        <div class="cards-main">
          <?php 
                $pos=$this->dashboard_model->all_pos($_GET["b_id"]);
                if($pos && isset($pos)){
                  foreach ($pos as $key ) {
                    # code...
                

              ?>
          
          <!--start loop-->
          
          <div class="card" id="card" style="text-align:center;">
            <?php if($key->total_due ==0){?>
            <a class="btn green" data-toggle="modal" href="#responsive-<?php echo $key->pos_id; ?>">
            <?php }else{ ?>
            <a class="btn red" data-toggle="modal" href="#responsive-<?php echo $key->pos_id; ?>">
            <?php } ?>
            <?php if($key->total_due ==0){ ?>
            <i class="fa fa-check btn green status"></i>
            <ul class="pull-left list-unstyled">
              <li><i class="fa fa-money"></i></li>
              <li><i class="fa fa-glass"></i></li>
              <li><i class="fa fa-gift"></i></li>
            </ul>
            <?php } else{?>
            <i class="fa fa-warning btn red status"></i>
            <ul class="pull-left list-unstyled">
              <li><i class="fa fa-money"></i></li>
              <li><i class="fa fa-glass"></i></li>
              <li><i class="fa fa-gift"></i></li>
            </ul>
            <?php } ?>
            <span class="pull-right"> <span class="in-n-d"><?php echo $key->invoice_number; ?> -- <?php echo $key->date; ?></span> <span class="deu">Due: <?php echo $key->total_due; ?> /-</span> <span class="pos-t"><?php echo "Paid from POS:".$key->total_paid_pos; ?> /-</span> </span> </a>
            <div id="responsive-<?php echo $key->pos_id; ?>" class="modal fade" tabindex="-1" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <div class="clearfix" style="margin-top:30px;"><span class="pull-left">Invoice Number: <?php echo $key->invoice_number; ?></span><span class="pull-right">Date: <?php echo $key->date; ?></span></div>
                    <h4>Guest Name: <?php echo $bookingDetails->cust_name; ?></h4>
                  </div>
                  <div class="modal-body">
                    <table class="table table-striped table-hover" id="package_details">
                      <thead>
                        <tr>
                          <th align="center" valign="middle"> Item  Name </th>
                          <th align="center" valign="middle"> Item Quantity </th>
                          <th align="center" valign="middle"> Item Price </th>
                          <th align="center" valign="middle"> Total Price </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $items=$this->dashboard_model->all_pos_items($key->pos_id);
                                     if($items && isset($items)){
                                      foreach ($items as $item ) {
                                        # code...
                                      
                                     ?>
                        <tr>
                          <td align="center" valign="middle"><?php echo $item->item_name; ?></td>
                          <td align="center" valign="middle"><?php echo $item->item_quantity; ?></td>
                          <td align="center" valign="middle" class="hidden-480"><?php echo $item->unit_price; ?></td>
                          <td align="center" valign="middle" class="hidden-480"><?php echo number_format($item->unit_price* $item->item_quantity, 2, '.',''); ?></td>
                        </tr>
                        <?php }} ?>
                      </tbody>
                    </table>
                    <div class="row">
                      <div class="col-xs-9" style="padding-left: 500px;">
                        <ul class="list-unstyled">
                          <li> <b>Bill Amt:</b> </li>
                        </ul>
                      </div>
                      <div class="col-xs-3 pull-right" style="text-align:center;">
                        <div class="dwn-two">
                          <ul class="list-unstyled" >
                            <li> <?php echo $key->bill_amount; ?> </li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-xs-9" style="padding-left: 500px;">
                        <ul class="list-unstyled">
                          <li> <b>Tax(%):</b> </li>
                        </ul>
                      </div>
                      <div class="col-xs-3 pull-right" style="text-align:center;">
                        <div class="dwn-two">
                          <ul class="list-unstyled" >
                            <li> <?php echo $key->tax; ?> </li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-xs-9" style="padding-left: 500px;">
                        <ul class="list-unstyled">
                          <li> <b>Discount(%):</b> </li>
                        </ul>
                      </div>
                      <div class="col-xs-3 pull-right" style="text-align:center;">
                        <div class="dwn-two">
                          <ul class="list-unstyled" >
                            <li> <?php echo $key->discount; ?> </li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-xs-9" style="padding-left: 500px;">
                        <ul class="list-unstyled">
                          <li> <b>Total Bill :</b> </li>
                        </ul>
                      </div>
                      <div class="col-xs-3 pull-right" style="text-align:center;">
                        <div class="dwn-two">
                          <ul class="list-unstyled" >
                            <li> <?php echo $key->total_amount; ?> </li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-xs-9" style="padding-left: 500px;">
                        <ul class="list-unstyled">
                          <li> <b>Paid :</b> </li>
                        </ul>
                      </div>
                      <div class="col-xs-3 pull-right" style="text-align:center;">
                        <div class="dwn-two">
                          <ul class="list-unstyled" >
                            <li> <?php echo $key->total_paid; ?> </li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-xs-9" style="padding-left: 500px;">
                        <ul class="list-unstyled">
                          <li> <b>Due :</b> </li>
                        </ul>
                      </div>
                      <div class="col-xs-3 pull-right" style="text-align:center;">
                        <div class="dwn-two">
                          <ul class="list-unstyled" >
                            <li> <?php echo $key->total_due; ?> </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                        <input type="text" autocomplete="off" class="form-control"  id="t_amount5" required="required">
                        <label> Amount<span class="required" id="b_contact_name">*</span></label>
                        <span class="help-block">Add Transaction Here</span> </div>
                      <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                        <select  autocomplete="off" class="form-control"  id="t_mode5" 
                                    required="required">
                          <option value=""></option>
                          <option value="cash">Cash</option>
                          <option value="cheque">Cheque</option>
                          <option value="card">Card</option>
                          <option value="bank transafer">Bank Transfer</option>
                          <option value="Bill To Company">Bill To Company</option>
                        </select>
                        <label> Payment Mode<span class="required" id="b_contact_name">*</span></label>
                        <span class="help-block">Add Payment Mode</span> </div>
                      <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                        <input type="text" autocomplete="off" class="form-control"  id="t_bank_name5" >
                        <label> Bank Name<span class="required" id="b_contact_name">*</span></label>
                        <span class="help-block">Bank Name</span> </div>
                      <!--<div class="form-group form-md-line-input form-md-floating-label col-md-1">
                          <button class="btn green pull-right" onclick="return add_pos_transaction('<?php echo $key->pos_id; ?>')" >Pay</button>
                        </div>--> 
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn green pull-right" onclick="return add_pos_transaction('<?php echo $key->pos_id; ?>')" disabled>Pay</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <!--end loop-->
          
          <?php }} 
			else 
			{
				echo "<center><h2><span class='label label-default'>No POS Bill has been added from</span><br><img src='".base_url()."upload/logos/yumm.png'></img></h2></center>";
			}
			?>
          </a> </div>
      </div>
      <?php if($pos && isset($pos))
		  {?>
      <div class="form-actions right">
        <button class="btn blue" onclick="pos_bill('<?php echo $_GET["b_id"]; ?>')" >View Bill</button>
      </div>
      <?php }?>
    </div>
  </div>
</div>
<div class="portlet light bordered">
<div class="portlet-title">
  <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Bill Tax Information</span> </div>
</div>
<div class="portlet-body form">
<div class="form-body form-horizontal form-row-sepe">
<div class="form-body">
<table class="table table-striped table-hover" width="100%">
<thead>
  <tr style="">
    <th align="center" valign="middle" > Folio# </th>
    <th align="center" valign="middle" > Item </th>
    <th align="center" valign="middle" > Guest Name </th>
    <th align="right" valign="middle"> Date </th>
  </tr>
</thead>
<tbody>
  <?php $bills_grp=$this->bookings_model->all_folio_grp($_GET['b_id']);

     if($bills_grp){ ?>
<h4> Folio List</h4>
<?php
      foreach ($bills_grp as $key ) {
        # code...
        ?>
<tr>
  <td align="center" valign="middle"><?php echo $key->invoice_no.$key->master_id; ?></td>
  <td align="center" valign="middle"><?php if($key->invoice_type=="booking"){echo $key->item_name." ";} echo $key->invoice_type; ?></td>
  <td align="center" valign="middle"><?php echo $key->cust_name; ?></td>
  <td align="center" valign="middle"><?php echo $key->date_generated; ?></td>
</tr>
<?php
      }
     }?>
<table class="table table-striped table-hover" width="100%">
  <thead>
    <tr style="">
      <th align="center" valign="middle" > # </th>
      <th align="center" valign="middle" > Date </th>
      <th align="center" valign="middle" > Description </th>
      <th align="center" valign="middle" > Folio# </th>
      <th align="right" valign="middle"> Discount </th>
      <th align="right" valign="middle"> Charges (include Tax) </th>
    </tr>
  </thead>
  <tbody>
    <?php
    $sum_bill=0;  
    $bills=$this->bookings_model->all_folio($_GET['b_id']);


    if($bills ){ $sum_bill=0; ?>
  <h4> Account Statements</h4>
  <?php foreach ($bills as $keyt ) {

    ?>
  <tr style="background: #F2F2F2">
    <td align="center" valign="middle"><input type="checkbox">
      </input></td>
    <td align="center" valign="middle"><?php echo $keyt->date_generated; ?></td>
    <td align="center" valign="middle"><?php $description =$keyt->item_description;
      if($description==""){
        $description=$keyt->item_name;
      }
      echo $description;
       ?></td>
    <td align="center" valign="middle"><?php echo $keyt->invoice_no.$keyt->master_id; ?></td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle"><?php echo $keyt->grand_total; ?></td>
  </tr>
  <?php $sum_bill=$sum_bill+$keyt->grand_total;  } 
    ?>
  <tr> </tr>
  <?php

    }else{?>
  <h4 style="color:red;margin-top:20px; text-align:center;">Bill Has Not yet been generated </h4>
  <?php } ?>
    </tbody>
  
</table>
<table class="table table-striped table-hover" width="100%">
  <thead>
    <tr style="">
      <th align="center" valign="middle" > # </th>
      <th align="center" valign="middle" > Date </th>
      <th align="center" valign="middle" > Description </th>
      <th align="center" valign="middle" > Folio# </th>
      <th align="right" valign="middle"> Discount </th>
      <th align="right" valign="middle"> Charges (include Tax) </th>
    </tr>
  </thead>
  <tbody>
    <?php if($bills  && $bills_grp) {?>
    <?php }?>
  </tbody>
</table>
Total Bill Amount: <?php echo $sum_bill; ?>
<div class="form-actions right">
  <?php $invoice=$this->dashboard_model->get_invoice($bookingDetails->booking_id);
					if($invoice == false )
					{
					?>
  <button onclick=" return folio_generate();" class="btn btn-primary" id="add_submit">Generate Invoice</button>
  <?php } ?>
  <a class="btn btn-primary" id="dwn_link" onClick="download_pdf();"> Download <i class="fa fa-download"></i> </a> </div>
</div>
</div>
</div>
</div>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Transactions</span> </div>
  </div>
  <div class="portlet-body form">
    <table class="table table-striped table-hover" width="100%">
      <thead>
        <tr style="">
          <th align="center" valign="middle" > # </th>
          <th align="center" valign="middle" > Transaction Date </th>
          <th align="center" valign="middle" > Payment Mode </th>
          <th align="center" valign="middle" > Bank name </th>
          <th align="right" valign="middle"> Amount </th>
        </tr>
      </thead>
      <tbody>
        <?php  if($transaction ){ foreach ($transaction as $keyt ) {
    if($keyt->t_booking_id==$_GET['b_id']){ ?>
        <tr>
          <td align="center" valign="middle"><?php echo $keyt->t_id; ?></td>
          <td align="center" valign="middle"><?php echo $keyt->t_date; ?></td>
          <td align="center" valign="middle"><?php echo $keyt->t_payment_mode; ?></td>
          <td align="center" valign="middle"><?php echo $keyt->t_bank_name; ?></td>
          <td align="right" valign="middle"><?php echo $keyt->t_amount; ?></td>
        </tr>
        <?php } } }
	$food_tax=0;
	if($bookingDetails->food_plans!="")
									{
									$data=$this->bookings_model->fetch_food_plan($bookingDetails->food_plans);
									if($data)
									 $food_tax=($data->fp_tax_percentage/100)*$bookingDetails->food_plan_price;
									}?>
      </tbody>
    </table>
  </div>
</div>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Take Payment</span> </div>
  </div>
  <!--<h4 style="color:red;margin-top:20px;">Not Availabe</h4>-->
  <div class="portlet-body form">
    <div class="form-body form-row-sepe">
      <div class="form-body">
        <div class="row" id="tab2">
          <div class="col-md-3">
            <div class="form-group">
              <label>Booking Id<span class="required">*</span></label>
              <input type="text" class="form-control input-sm" id="" name="card_number" value="<?php echo "HM0".$this->session->userdata('user_hotel')."000".$_GET['b_id'] ?>" 
                                                                        disabled="disabled"/>
            </div>
          </div>
          <?php  
                    $poses=$this->dashboard_model->all_pos_booking($_GET["b_id"]);
                    $pos_amount=0;
                    if($poses){
                    foreach ($poses as $key ) {

                      # code...
                      $pos_amount=$pos_amount+$key->total_due;
                    }}
                   // echo $pos_amount;
                  ?>
          <?php if($bookingDetails->group_id =="0"){ ?>
          <div class="col-md-3">
            <div class="form-group">
              <?php 

                                                                        $amm=$bookingDetails->room_rent_sum_total-$discount+$bookingDetails->food_plan_price+$food_tax;
                                                                        if($bookingDetails->room_rent_sum_total=="0"){
                                                                          $amm=$bookingDetails->room_rent_total_amount-$discount+$bookingDetails->food_plan_price+$food_tax+$bookingDetails->room_rent_tax_amount;

                                                                        }

                                                                        ?>
              <label>Total Amount Paybale<span class="required">*</span></label>
              <!--<select name="country" id="country_list" class="form-control" placeholder=" Booking Type"> 
                                                                       
                                                                            
                                                                            <option value="">----- Select Booking Id -----</option>
                                                                            <option value="">fdsdsdsds</option>
                                                                            
                                                                            
                                                                        </select>-->
              
              <input type="text" required class="form-control input-sm" id="" name="card_number" value="<?php echo number_format($amm+$bookingDetails->service_price+$bookingDetails->booking_extra_charge_amount+$pos_amount,2,'.',''); ?>" 
                                                                        disabled="disabled"/>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>Due Amount<span class="required">*</span></label>
              <!--<select name="country" id="country_list" class="form-control" placeholder=" Booking Type"> 
                                                                       
                                                                            
                                                                            <option value="">----- Select Booking Id -----</option>
                                                                            <option value="">fdsdsdsds</option>
                    
                    
                                                                            
                                                                            
                                                                        </select>-->
              
              <?php 
                                                                            if(isset($amountPaid) && $amountPaid) {
                                                                                $paid=$amountPaid->tm;
                                                                             }
                                                                             else{
                                                                              $paid=0;
                                                                            }
                    
                    
                                                                        ?>
              <input type="text" class="form-control input-sm" id="due_amount"  name="card_number" value="<?php echo number_format($amm+$bookingDetails->service_price+$bookingDetails->booking_extra_charge_amount+$pos_amount-$paid,2,'.','') ?>" 
                                                                        disabled="disabled"/>
            </div>
          </div>
          <?php }else{
                ?>
          <div class="col-md-3">
            <div class="form-group">
              <?php 

                                                                        $amm=$bookingDetails->room_rent_sum_total-$discount;
                                                                        if($bookingDetails->room_rent_sum_total=="0"){
                                                                          $amm=$bookingDetails->room_rent_total_amount-$discount;

                                                                        }

                                                                        ?>
              <label>Total Amount Paybale<span class="required">*</span></label>
              <!--<select name="country" id="country_list" class="form-control" placeholder=" Booking Type"> 
                                                                       
                                                                            
                                                                            <option value="">----- Select Booking Id -----</option>
                                                                            <option value="">fdsdsdsds</option>
                                                                            
                                                                            
                                                                        </select>-->
              
              <input type="text" required class="form-control input-sm" id="" name="card_number" value="<?php echo number_format((($bookingDetails->service_price+$bookingDetails->booking_extra_charge_amount+$pos_amount)-$discount),2,'.',''); ?>" 
                                                                        disabled="disabled"/>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>Due Amount<span class="required">*</span></label>
              <!--<select name="country" id="country_list" class="form-control" placeholder=" Booking Type"> 
                                                                       
                                                                            
                                                                            <option value="">----- Select Booking Id -----</option>
                                                                            <option value="">fdsdsdsds</option>
                    
                    
                                                                            
                                                                            
                                                                        </select>-->
              
              <?php 
                                                                            if(isset($amountPaid) && $amountPaid) {
                                                                                $paid=$amountPaid->tm;
                                                                             }
                                                                             else{
                                                                              $paid=0;
                                                                            }
                    
                    
                                                                        ?>
              <input type="text" class="form-control input-sm" id="due_amount"  name="card_number" value="<?php echo number_format(($bookingDetails->service_price+$bookingDetails->booking_extra_charge_amount+$pos_amount -($paid+$discount)),2,'.',''); ?>" 
                                                                        disabled="disabled"/>
            </div>
          </div>
          <?php 
                } ?>
          <div class="col-md-3">
            <div class="form-group">
              <label>Amount <span class="required"> * </span> </label>
              <input type="text" id="add_amount" class="form-control input-sm" required name="card_number" placeholder="Give Amount" onblur="check_amount();"  required="required" />
              <input type="hidden" id="booking_id" value="<?php echo $bookingDetails->booking_id; ?>">
              <input type="hidden" id="item_no" value="<?php echo $item_no;?>">
              <input type="hidden" id="booking_status_id" value="<?php echo $bookingDetails->booking_status_id; ?>">
            </div>
            <script type="text/javascript">
                                                                    function check_amount()
                    {
                    var amount = $("#add_amount").val();
                    //alert(amount);
                    var due_amount = $("#due_amount").val();
                    //alert(due_amount);
					var a=parseFloat(amount);
					var b=parseFloat(due_amount);
					//alert (a-b);
                    if(parseFloat(amount) > parseFloat(due_amount))
                    {
                    //alert("The amount should be less than the due amount Rs: "+due_amount);
                    swal({
                                                title: "Amount Should Be Smaller",
                                                text: "(The amount should be less than the due amount Rs: "+due_amount+")",
                                                type: "warning"
                                            },
                                            function(){
                                                //location.reload();
                                            });
                    $("#add_amount").val("");
                    return false;
                    }
                    else{
                    return true;
                    }
                    }
                    
                                                                </script> 
          </div>
          <div class="col-md-3">
            <label>Profit Center <span class="required"> * </span> </label>
            <select name="" class="form-control input-sm" id="p_center">
              <?php $pc=$this->dashboard_model->all_pc();?>
              <?php
						  $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
						  ?>
              <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
              <?php $pc=$this->dashboard_model->all_pc1();
							foreach($pc as $prfit_center){
							?>
              <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
              <?php }?>
            </select>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>Payment Mode <span class="required"> * </span> </label>
              <select name="country" class="form-control input-sm" placeholder=" Booking Type" id="pay_mode" onChange="paym(this.value);">
                <option value="">----- Select Payment Mode -----</option>
                <option value="cashs">Cash</option>
                <option value="cards">Debit /Credit Card</option>
                <option value="funds">Fund transfer</option>
              </select>
            </div>
          </div>
          <div id="cards" style="display:none;">
            <div class="col-md-3">
              <div class="form-group">
                <label>Bank Name <span class="required"> * </span> </label>
                <input type="text" class="form-control input-sm" placeholder="Bank name" id="bankname" >
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Card NO<span class="required"> * </span> </label>
                <input type="text" class="form-control input-sm" placeholder="Bank name" id="card_no" >
              </div>
            </div>
          </div>
          <div id="fundss" style="display:none;">
            <div class="col-md-3">
              <div class="form-group">
                <label>Bank Name <span class="required"> * </span> </label>
                <input type="text" class="form-control input-sm" placeholder="Bank name" id="bankname" >
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>A/C No<span class="required"> * </span> </label>
                <input type="text" class="form-control input-sm" placeholder="Bank name" id="ac_no" >
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>IFSC Code<span class="required"> * </span> </label>
                <input type="text" class="form-control input-sm" placeholder="Bank name" id="ifsc" >
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="form-actions right">
        <button onclick=" return ajax_hotel_submit();" class="btn btn-primary" id="add_submit">Submit</button>
      </div>
    </div>
  </div>
</div>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Charge Summary</span> </div>
  </div>
  <div class="portlet-body form">
    <div class="form-body form-horizontal form-row-sepe">
      <div class="form-body">
        <div class="row i-nr">
          <?php
                            $service_amount=$bookingDetails->service_price;
                            if($service_amount ==0){
                                $service_amount=0;
                            }else{
                                $service_amount=$service_amount;

                            }
                             $charge_amount=$bookingDetails->booking_extra_charge_amount;
                            if($charge_amount ==0){
                                $charge_amount=0;
                            }else{
                                $charge_amount=$charge_amount;

                            }
                            ?>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Room Rent Amount:</strong></label>
              <label class="col-md-6" style="color:#008F6C"> <i class="fa fa-inr"></i>
                <?php
                  if( isset($bookingDetails->room_rent_sum_total) && $bookingDetails->room_rent_sum_total !=0) {
                    $total=$bookingDetails->room_rent_total_amount;
                    echo $total;
                  }
                  else{ 
                     if(isset($bookingDetails->room_rent_total_amount)){
                    $total=$bookingDetails->room_rent_total_amount;
                    echo $total;
                     }
                  } ?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Room Rent Tax:</strong></label>
              <label class="col-md-6" style="color:#008F6C"><i class="fa fa-inr"></i> <?php echo $bookingDetails->room_rent_tax_amount; ?></label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Service Amount:</strong></label>
              <label class="col-md-6" style="color:#008F6C"><i class="fa fa-inr"></i> <?php echo $bookingDetails->service_price; ?></label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Charge Amount:</strong></label>
              <label class="col-md-6" style="color:#008F6C"><i class="fa fa-inr"></i> <?php echo $bookingDetails->booking_extra_charge_amount; ?> </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">POS Amount:</strong></label>
              <label class="col-md-6" style="color:#008F6C"><i class="fa fa-inr"></i>
                <?php 
                    $poses=$this->dashboard_model->all_pos_booking($_GET["b_id"]);
                    $pos_amount=0;
                    if($poses){
                    foreach ($poses as $key ) {

                      # code...
                      $pos_amount=$pos_amount+$key->total_due;
                    }}
                    echo $pos_amount;
                 ?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Food Plan:</strong></label>
              <label class="col-md-6" style="color:#008F6C"><i class="fa fa-inr"></i>
                <?php
				echo $bookingDetails->food_plan_price;
					?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Food Tax:</strong></label>
              <label class="col-md-6" style="color:#008F6C"><i class="fa fa-inr"></i>
                <?php 
									echo $food_tax;
									?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Total Amount:</strong></label>
              <label class="col-md-6" style="color:#008F6C"> <i class="fa fa-inr"></i>
                <?php
                  if( isset($bookingDetails->room_rent_sum_total) && $bookingDetails->room_rent_sum_total !=0) {
                    $total=$bookingDetails->room_rent_sum_total+$pos_amount+$bookingDetails->food_plan_price+$food_tax;
                    echo $bookingDetails->room_rent_sum_total+$service_amount+$charge_amount+$pos_amount+$bookingDetails->food_plan_price+$food_tax;//_sb +$bookingDetails->room_rent_tax_amount;
                  }
                  else{ 
                    if(isset($bookingDetails->room_rent_total_amount)){
                    $total=$bookingDetails->room_rent_total_amount+$pos_amount+$bookingDetails->food_plan_price+$bookingDetails->room_rent_tax_amount;
                    echo $total;
                     }
                  }

                  if(isset($amountPaid) && $amountPaid) {
                    $paid=$amountPaid->tm;
                  }
                  else{
                    $paid=0;
                  }?>
                <?php
                  $service_amount=$bookingDetails->service_price;
                  if($service_amount ==0){
                    $service_amount=0;
                  }else{
                    $service_amount=$service_amount;
                    $total=$total+$service_amount;
                  }


                     $charge_amount=$bookingDetails->booking_extra_charge_amount;
                  if($charge_amount ==0){
                    $charge_amount=0;
                  }else{
                    $charge_amount=$charge_amount;
                    $total=$total+$charge_amount;
                  }
                  ?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Discount:</strong></label>
              <label class="col-md-6" style="color:#008F6C"> <i class="fa fa-inr"></i><?php echo $discount;?></label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Net Payable:</strong></label>
              <label class="col-md-6" style="color:#008F6C"> <i class="fa fa-inr"></i><?php echo $payble=$total-$discount;?></label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Amount Paid:</strong></label>
              <label class="col-md-6" style="color:#008F6C"> <i class="fa fa-inr"></i><?php echo $paid;?></label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">Pending Amount:</strong></label>
              <label class="col-md-6" style="color:#008F6C"> <i class="fa fa-inr"></i>
                <?php 
                  if(isset($total)) {
                  $pa = $payble - $paid;
                      echo $pa;
                  } else {
                    echo  $pa= 0;
                  }   
                  ?>
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <?php
                                                   if(isset($total)) {
                                                    $totalAmount = $total+$bookingDetails->room_rent_tax_amount-$discount;
                                                    if(isset($amountPaid) && $amountPaid) {
                                                        $amountPaid = $amountPaid->tm;
                                                        $percentPaid = (($amountPaid / ($total-$discount)) * 100);
                                                        $percentPending = (($pa / ($total-$discount)) * 100);
                                                    }
                                                    else{
                                                       $percentPaid = 0;
                                                       $percentPending = 100;
                                                    }
                                                   } else {
                                                       $percentPaid = 0;
                                                       $percentPending = 0; 
                                                   }
                                                    ?>
              <label class="col-md-6"><strong style="color: #2B3643;">% Paid:</strong></label>
              <label class="col-md-6" style="color:#008F6C"><?php echo round($percentPaid);?></label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-6"><strong style="color: #2B3643;">% pending:</strong></label>
              <label class="col-md-6" style="color:#008F6C"><?php echo round($percentPending);?></label>
            </div>
          </div>
        </div>
        <div class="table-scrollable" >
          <table class="table table-striped table-hover table-bordered" width="100%">
            <tbody>
              <tr>
                <td width="90%" align="right">Total Amount:</td>
                <td><input class="form-control input-sm" type="text" value="<?php echo $total; ?>" id="totalAmount" readonly></td>
              </tr>
              <tr>
                <td width="90%" align="right">Discount:</td>
                <td><div class="input-group">
                    <input type="text" id="discountVal" class="form-control input-sm" value="<?php echo $bookingDetails->discount; ?>">
                    <span class="input-group-btn">
                    <button type="button" id="btnSubmit" class="btn blue btn-sm"><i class="fa fa-check"></i></button>
                    </span> </div></td>
              </tr>
              <tr>
                <td width="90%" align="right">Total Payable Amount:</td>
                <td><input class="form-control input-sm" type="text" value="<?php echo $total-$discount; ?>" id="totalPayableAmount" readonly></td>
              </tr>
              <tr>
                <td width="90%" align="right">Total Due:</td>
                <td><input class="form-control input-sm" type="text" value="<?php echo $pa; ?>" id="totalDue" readonly></td>
                <input class="form-control input-sm" type="hidden" value="<?php echo $pa; ?>" id="totalDue1">
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script>

function download_pdf() {
	//alert("hello");
	var booking_id = $('#booking_id').val();
	
	var item_no = $('#item_no').val();
	if(item_no=="0"){

		alert("No invoice has been generated. Generate in booking details page");

	}else{
	
	$("#dwn_link").attr("href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id);
	var f = $("#f");
            $.post(f.attr("action"), f.serialize(), function (result) {
            
                close(eval(result));
            });
	 return false;
	
}

}

    function paym(val){
        if(val == 'cards')
        { 
            document.getElementById('cards').style.display='block';
            
        }
    else{
      document.getElementById('cards').style.display='none';
    }
        if(val=='funds')
        {
            document.getElementById('fundss').style.display='block';
           
        }
    else{
      document.getElementById('fundss').style.display='none';
    }
    
        if(val=='cashs')
        {
            document.getElementById('cardss').style.display='none';
            document.getElementById('fundss').style.display='none';
        }        
    }
    </script> 
<script type="text/javascript">
  
        function add_service(id,price,tax){
           /* var id=  $("#s_id").val();
            var price=  $("#s_price").val();
            var tax=  $("#s_tax").val();*/
            var id= id;
            var price= price;
            var tax=  tax;
            var booking_id=  <?php echo $_GET['b_id'] ?>;

            jQuery.ajax(
                {
                    type: "POST",
                    url: "<?php echo base_url(); ?>dashboard/add_service_to_booking",
                    dataType: 'json',
                    data: {booking_id:booking_id,service_id:id,service_price:price,service_tax:tax },
                    success: function(data){
                        alert(data.return);
                        location.reload();
                    }
                });

        }

        function add_charge(){
           /* var id=  $("#s_id").val();
            var price=  $("#s_price").val();
            var tax=  $("#s_tax").val();*/
              var name= document.getElementById('c_name').value;
              var quantity= document.getElementById('c_quantity').value;
             var unit_price= document.getElementById('c_unit_price').value;
             var tax= document.getElementById('c_tax').value;
             var total= document.getElementById('c_total').value;

            // alert(name+quantity+unit_price+tax+total);
			
           if(name == ""){
			  alert("Please enter name.");
	          return false;
		   }
           if(quantity == ""){
			  alert("Please enter quantity.");
	          return false;
		   }
           if(unit_price == ""){
			  alert("Please enter price.");
	          return false;
		   }

            var booking_id=  <?php echo $_GET['b_id'] ?>;

           

            jQuery.ajax(
                {
                    type: "POST",
                    url: "<?php echo base_url(); ?>dashboard/add_charge_to_booking",
                    dataType: 'json',
                    data: {booking_id:booking_id,name:name,quantity:quantity,unit_price:unit_price, tax:tax, total:total },
                    success: function(data){
                        alert(data.return2);
                        location.reload();
                    }
                });

        }
		
	
		
    function add_row()
    {
var counter=$('#occu_counter').val();
var count = $('#guest_holder tr').length;
var max_holder_adult='<?php echo $bookingDetails->no_of_adult;  ?>';
var max_holder_child='<?php echo $bookingDetails->no_of_child;  ?>';
var from_date='<?php echo date('d-m-Y',strtotime($bookingDetails->cust_from_date));?>';
var to_date='<?php echo date('d-m-Y',strtotime($bookingDetails->cust_end_date));?>';
var max_occu=parseInt(max_holder_adult)+parseInt(max_holder_child);
 //exit;
var c=parseInt(count)+parseInt(counter);

//var to_date = $('#end_date').val();
//alert (from_date+"_"+to_date);
if((parseInt(max_holder_adult)+parseInt(max_holder_child)) <=(count-1)){
  alert("Maximum Guest Capacity Exceeds");
 return false;
}

	 $('#guest_details').append('<tr><td><label></label></td><td class="form-group"><input name="g_name2[]" type="text" class="form-control input-sm"></td><td class="form-control"><select name="g_sex2[]"><option value="male">Male</option><option value="female">Female</option></select></td><td class="hidden-480 form-group"><input type="text" class="form-control input-sm" value="'+from_date+'"></td><td class="hidden-480 form-group"><input type="text" class="form-control input-sm" value="'+from_date+'"></td><td class="hidden-480 form-group"><input name="g_contact_no2[]" type="text" maxlength="10" class="form-control input-sm" onkeypress=" return onlyNos(event, this);"></td><td class="hidden-480 form-group"><input name="g_pincode2[]" type="text" required="true" class="form-control input-sm" onkeypress=" return onlyNos(event, this);"></td><td class="hidden-480 form-control"><select name="g_sex2[]"><option value="yes">Yes</option><option value="no">No</option></select></td></tr>');
     $('#occu_counter').val(parseInt(counter)+1);



     
	}
  function add_row_again()
    {
      $('#extra_service').append('<tr><td><input type="text" ></td><td><input type="text" ></td><td class="hidden-480"><input type="text" ></td></tr>');
    }
    function add_row_package()
    {
      $('#package_details').append('<tr><td><input type="text" readonly></td><td><input type="text" readonly></td><td class="hidden-480"><input type="text" readonly></td><td class="hidden-480"><div class="side-two"><button class="btn blue pull-left bam">Edit</button><button class="btn green pull-left dana">Save</button></div></td></tr>');
    }   
function add_row_bill()
    {
      $('#bill_details').append('<tr><td><input type="text" readonly></td><td><input type="text" readonly></td><td class="hidden-480"><input type="text" readonly></td><td class="hidden-480"><div class="side-two"><button class="btn blue pull-left bam">Edit</button><button class="btn green pull-left dana">Save</button></div></td></tr>');
    }     
    function edit_name(){
      document.getElementById('guest_name').disabled = false;
    } 
    function edit_contact(){
      document.getElementById('guest_contact').disabled = false;
    }
    function edit_address(){
      document.getElementById('guest_address').disabled = false;
    }
    function edit_nature(){
      document.getElementById('guest_nature').disabled = false;
    }
    function edit_source(){
      document.getElementById('booking_source').disabled = false;
    }
    function edit_note(){
      document.getElementById('booking_note').disabled = false;
    }
    
    function edit_broker_name(){
      document.getElementById('broker_name').disabled = false;
    }
    function edit_commision(){
      document.getElementById('commision').disabled = false;
    }
    function edit_broker_ammount(){
      document.getElementById('broker_ammount').disabled = false;
    }
    function tabel_show(){
      document.getElementById('guest_details_edit').style.display= 'block';
      document.getElementById('guest_details_first').style.display= 'none';
      document.getElementById('hd').style.display= 'block';
      document.getElementById("preference").disabled='';
    }
    function tabel_show_hide(){
      document.getElementById('guest_details_edit').style.display= 'none';
          document.getElementById('guest_details_first').style.display= 'block';
      document.getElementById('hd').style.display= 'none';
      document.getElementById('preference').disabled="true";
    }   
    function stay_show(){
      document.getElementById('stay_details_again').style.display= 'block';
          document.getElementById('stay_details').style.display= 'none';
      document.getElementById('stay_details_again1').style.display= 'block';
    }   
    function stay_hide(){
      document.getElementById('stay_details_again').style.display= 'none';
          document.getElementById('stay_details').style.display= 'block';
      document.getElementById('stay_details_again1').style.display= 'none';
    }   
    function gnrl_info(){
      document.getElementById('genarel_information_again').style.display= 'block';
          document.getElementById('genarel_information').style.display= 'none';
      document.getElementById('genarel_information_again1').style.display= 'block';
    }
    function gnrl_info_hide(){
      document.getElementById('genarel_information_again').style.display= 'none';
          document.getElementById('genarel_information').style.display= 'block';
      document.getElementById('genarel_information_again1').style.display= 'none';
    }   
    function payment_info(){
      document.getElementById('payment_information_again').style.display= 'block';
          document.getElementById('payment_information').style.display= 'none';
      document.getElementById('payment_information_again1').style.display= 'block';
    }
    function payment_info_hide(){
      document.getElementById('payment_information_again').style.display= 'none';
          document.getElementById('payment_information').style.display= 'block';
      document.getElementById('payment_information_again1').style.display= 'none';
    }   
    function extra_info(){
      //document.getElementById('extra_again').style.display= 'block';
         // document.getElementById('extra').style.display= 'none';
    }


        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        function IsNumeric(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            document.getElementById("error").style.display = ret ? "none" : "inline";
            return ret;

        }
        function IsNumeric1(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            document.getElementById("error1").style.display = ret ? "none" : "inline";
            return ret;
        }
        function IsNumeric2(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            document.getElementById("error2").style.display = ret ? "none" : "inline";
            return ret;
        }
        function IsNumeric3(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            document.getElementById("error3").style.display = ret ? "none" : "inline";
            return ret;
        }

        $('#btn').click( function() {
          var adult = document.getElementById("text4").value;
          var male = document.getElementById("text3").value;
          var female = document.getElementById("text1").value;
          var val = +male + +female
          if(val > adult){
            document.getElementById("error5").style.display ="inline";
            return false; 
          } else {
                $('form#frm').submit();
          }   

        });

        function return_commission(value){
          //alert(value);
            var id=value;
            var amount=document.getElementById('total_booking_amount').value;
            //alert(id);

           jQuery.ajax(
                {
                    type: "POST",
                    url: "<?php echo base_url(); ?>dashboard/return_commission_broker",
                    dataType: 'json',
                   data: {b_id:id},
                    success: function(data){
                       // alert(data.commission);

                       document.getElementById('commission').value=data.commission;
                        document.getElementById('ammount').value=(parseInt(data.commission)*parseInt(amount))/100;
                        document.getElementById("broker_id").value=id;

                       // location.reload();
                    }
                });
        }
        function return_commission2(value){
          //alert(value);
            var id=value;
            var amount=document.getElementById('total_booking_amount2').value;
            //alert(id);

           jQuery.ajax(
                {
                    type: "POST",
                    url: "<?php echo base_url(); ?>dashboard/return_commission_channel",
                    dataType: 'json',
                   data: {b_id:id},
                    success: function(data){
                       // alert(data.commission);

                       document.getElementById('commission2').value=data.commission;
                        document.getElementById('ammount2').value=(parseInt(data.commission)*parseInt(amount))/100;
                        document.getElementById("channel_id").value=id;

                       // location.reload();
                    }
                });
        }
    
    function gnrl_info1(){

      document.getElementById('genarel_information_again_broker').style.display="block";
      document.getElementById('genarel_information_again_broker1').style.display="block";
      document.getElementById('genarel_information_broker').style.display="none";
      }
      function gnrl_info_channel(){

      document.getElementById('genarel_information_again_channel').style.display="block";
      document.getElementById('genarel_information_again_channel1').style.display="block";
      document.getElementById('genarel_information_channel').style.display="none";
      }
        function gnrl_info_hide1(){

          document.getElementById('commission').value="";
          document.getElementById('ammount').value="";
          $('#b_name').prop('selectedIndex',0);
          document.getElementById('genarel_information_again_broker').style.display="none";
      document.getElementById('genarel_information_again_broker1').style.display="none";
      document.getElementById('genarel_information_broker').style.display="block";
        }

         function save_broker(){




        var broker_id=document.getElementById("broker_id").value;
        var booking_id=document.getElementById("show_id").value;
        var broker_amount=document.getElementById("ammount").value;
        //alert(booking_id);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {

           // alert(xhttp.status);

            if (xhttp.readyState == 4 && xhttp.status == 200) {

               // alert('booking add is '+xhttp.response);

               swal({
                                    title: "Success",
                                    text: "Broker Add is Successfull",
                                    type: "success"
                                },
                                function(){
                                    location.reload();
                                });

                //document.getElementById("booking_id_final").value=percentage;

            }

        };
        xhttp.open("GET", "<?php echo base_url(); ?>dashboard/broker_booking?booking_id="+booking_id+"&broker_id="+broker_id+"&broker_amount="+broker_amount, true);
        xhttp.send();


    }

      function save_channel(){




        var broker_id=document.getElementById("channel_id").value;
        var booking_id=document.getElementById("show_id").value;
        var broker_amount=document.getElementById("ammount2").value;
        //alert(booking_id);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {

           // alert(xhttp.status);

            if (xhttp.readyState == 4 && xhttp.status == 200) {

               // alert('booking add is '+xhttp.response);

               swal({
                                    title: "Success",
                                    text: "Broker Add is Successfull",
                                    type: "success"
                                },
                                function(){
                                    location.reload();
                                });

                //document.getElementById("booking_id_final").value=percentage;

            }

        };
        xhttp.open("GET", "<?php echo base_url(); ?>dashboard/channel_booking?booking_id="+booking_id+"&channel_id="+broker_id+"&channel_amount="+broker_amount, true);
        xhttp.send();


    }

    

    function alert_checkin(){

      swal({   title: "CheckIn Date Can not be Edited",   text: "You will not be able to edit checkin date from this page. To do it please go to frontdesk",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Go to Frontdesk",   closeOnConfirm: false }, function(){  

           window.location="<?php echo base_url(); ?>dashboard/add_booking_calendar";

        });

    }

     function alert_checkout(){

      swal({   title: "CheckOut Date Can not be Edited",   text: "You will not be able to edit checkout date from this page. To do it please go to frontdesk",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Go to Frontdesk",   closeOnConfirm: false }, function(){  

           window.location="<?php echo base_url(); ?>dashboard/add_booking_calendar";

        });

    }

    function return_checkin(){

      document.getElementById('cust_from_date').value=document.getElementById('trick1').value;
    }

    function return_checkout(){

      document.getElementById('cust_end_date').value=document.getElementById('trick2').value;
    }


    
  </script> 
<script type="text/javascript">

function return_guest_search()
    {
		
        var guest_name = $('#guest_search').val();
        //alert(guest_name);

        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_search",
                datatype:'json',
                data:{guest:guest_name},
                success:function(data)
                {
                    var resultHtml = '';
                    resultHtml+='<table class="table table-bordered" cellpadding="0" cellspacing="0" ><thead><tr><th>Name</th><th>Address</th><th width="100">Mobile No</th></tr></thead><tbody>';
                    $.each(data, function(key,value){
                        resultHtml+='<tr  class="crsr collapsed" data-toggle="collapse" data-target="#toggleDemo'+value.g_id+'">';
                        resultHtml+='<td>'+ value.g_name +'</td>';
                        resultHtml+='<td>'+ value.g_address +'</td>';
                        resultHtml+='<td>'+ value.g_contact_no +'</td>';
                        resultHtml+='</tr><tr id="toggleDemo'+value.g_id+'"  class="mrgn">';
                        resultHtml+='<td class="no-marin"  colspan="3"  cellpadding="0" cellspacing="0">';
                        resultHtml += '<div class="side clearfix" style="width:100%;"><div class="col-xs-7 ex"><p><label style="font-size:13px;">' + value.g_name + '</label><br><label>';

                        resultHtml+='<input type="hidden" id="g_id_hide"  value="'+value.g_id+'" ></input>';
                        resultHtml+='<label style="font-size:13px;">Room No: '+value.room_no+'</label><br><label style="font-size:13px;">Last Check In Date: '+value.cust_from_date+'</label><br> <button class="act active btn blue btn-sm" name="g_id" id="g_id"" value="'+value.g_id+'" onclick="get_guest('+value.g_id+')" >Continue</button> </p></div>';
                        if(value.g_photo_thumb!='') {
                            resultHtml += '<div class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/' + value.g_photo_thumb + '" width="100px" height="92px"  > ' +
                            '</div>';
                        }
                        else{
                            resultHtml += '<div  class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/no_images.png" width="100px" height="92px" > ' +
                            '</div>';
                        }
                        //resultHtml+='<td><input type="radio" name="g_id" id="'+value.g_id+'" value="'+value.g_id+'" onclick="get_guest(this.value)" /></td>';
                        resultHtml+='</div></td></tr>';
                    });
                    resultHtml+='</tbody></table>';
                    $('#return_guest').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
			$("#return_guest").css("display", "block");
        return false;

	
    }
	
function get_guest(id){
        var g_id=id;
		var bk_id= <?php echo $_GET["b_id"]; ?>;
      // var bk_id=document.getElementById("show_id").value;;
	    //alert(bk_id);
	   
		jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>dashboard/update_bookings_guest_id",
                datatype:'json',
                data:{guest_id:g_id,booking_id:bk_id},
                success:function(data){ 
			alert(data);
					if(data !=""){
						location.reload();
					}
                  
                }
            });
        return false;
    }	
	


    function ajax_hotel_submit()
    {
		 var booking_status_id = $("#booking_status_id").val();
		 var due_amount = $("#due_amount").val();
		// alert (booking_status_id+"__"+due_amount);
		 //return false;
		 if((booking_status_id != 6)|| (due_amount > 0))
		 {
        //alert($("#add_amount").val());
		$('#add_submit').prop('disabled', true);
		
        var booking_id = $("#booking_id").val();
        var booking_status_id = $("#booking_status_id").val();
        var p_center = $("#p_center").val();
        //alert(p_center);
       // return false;
        var add_amount = $("#add_amount").val();
        var pay_mode   = $("#pay_mode").val();
        var bankname   = $("#bankname").val();
        var payment_status = $("#payment_status").val();
        if($("#add_amount").val() == null || $("#add_amount").val() ==0 )
        {
          //alert('Error');
          swal({
                                    title: "Error",
                                    text: "An Error Occurd",
                                    type: "warning"
                                },
                                function(){
                                    //location.reload();
                                });
          return false;
        }
        else{
        jQuery.ajax(
        {
          type: "POST",
          url: "<?php echo base_url(); ?>bookings/add_booking_transaction",
          dataType: 'json',
          data: {t_booking_id:booking_id,t_booking_status_id:booking_status_id,p_center:p_center,t_amount:add_amount,t_payment_mode:pay_mode,t_bank_name:bankname,t_status:payment_status },
          success: function(data){
            //alert('Add Successfull');
            swal({
                                    title: "Add Successfull",
                                    text: "Your Payment Is Taken",
                                    type: "success"
                                },
                                function(){
                  
                    
            $("#booking_id").val(' ');
            $("#booking_status_id").val(' ');
            $("#add_amount").val(' ');
            $("#pay_mode").val(' ');
            $("#bankname").val(' ');
            $("#payment_status").val(' ');
            $('#tab2').hide();
            location.reload();
                                    
                                });
          
          }
        });
        }
        return false;
    }
	else
	{
		checkout_notification();
	}
	}
  </script> 
<script type="text/javascript">
                      function add_pos_transaction(id){
                      

                          
                            var add_amount = $("#t_amount5").val();
                           

                          //alert(booking_id+booking_status_id+add_amount+pay_mode+bankname+payment_status);
                        

                          jQuery.ajax(
        {
          type: "POST",
          url: "<?php echo base_url(); ?>dashboard/add_pos_transaction",
          dataType: 'json',
          data: {amount: add_amount, pos_id:id },
          success: function(data){
            alert('transaction successfull');
            location.reload();

          
          
          }
        });



                          


                      }

                    </script> 
<script type="text/javascript">

$("#addGuest").click(function(){
	$('#myModalNorm1').modal('show');
	$('#return_guest').html("");
	$("#return_guest").css("display", "none");
});

                      function pos_bill(b_id){
                        var p_id=2;
                        window.location.href = "<?php echo base_url(); ?>dashboard/pos_grand_invoice?p_id="+p_id+"&b_id="+b_id;
                      }
                    </script> 
<script type="text/javascript">
                       window.onload = function(){

                      //  document.getElementsByClassName('btn-circle').style.pointerEvents = 'none';


                        };
                    </script> 
<script>
                      $("#btnSubmit").click(function(){
						  var bookingstatusid = <?php echo $bookingstatusid;?>;
						 if(bookingstatusid != 6)
						 {							 
  var discountVal = $("#discountVal").val();
  var totalAmount = $("#totalAmount").val();
  var totalPayableAmount = $("#totalPayableAmount").val();
  var totalDuee = $("#totalDue1").val();
 
  var amtDue = parseInt(totalDuee) - parseInt(discountVal);
  var amtPay = parseInt(totalAmount) - parseInt(discountVal);
  
  var groupID = '<?php echo $_GET['b_id']; ?>';
  $.ajax({        
    url: '<?php echo base_url(); ?>dashboard/getAdditionalDiscountSingle',
    type: "GET",
    dataType: "json",
    data: {discountVal:discountVal,groupID:groupID},
    success: function(data){    
      $("#totalPayableAmount").val(amtPay);
          $("#totalDue").val(amtDue);  
    location.reload();		  
    },
  }); 
						 }
						 else
						 {
							 checkout_notification();
						 }
  }); 
                    </script> 
<script >
                      function folio_generate(){

                         var booking_id = $("#booking_id").val();
                         
                          jQuery.ajax(
        {
          type: "POST",
          url: "<?php echo base_url(); ?>bookings/folio_generate",
          dataType: 'json',
          data: {booking_id:booking_id},
          success: function(data){
            
            swal({
                                    title: "Invoice Generated "+data,
                                    text: "Your invoice is successfully generated",
                                    type: "success"
                                },
                                function(){

                                  location.reload();
            
                                });
          
          }
        });
                      }
                    </script> 
<script>
					$( document ).ready(function() 
						{
							
							var tot_am=$('#totalAmount').val();
							var tot_due=$('#totalDue1').val();
							var tot_pay=$('#totalPayableAmount').val();
							 if(tot_due == 0)
							 {

								 $('#pay_symbol').attr('src', '<?php echo base_url(); ?>upload/paid.jpg');
							 }
							 else if(tot_due == tot_pay)
							 {

								$('#pay_symbol').attr('src', '<?php echo base_url(); ?>upload/due.jpg');
							 }
							 else 
							 {

								$('#pay_symbol').attr('src', '<?php echo base_url(); ?>upload/partial.jpg');
							 }
							$('#tot_am').text(parseInt(tot_am));
							 $('#pr_bar_img').width($('#pr_div').outerWidth());
							 var due_par=((tot_pay-tot_due)/tot_pay*100);
							 $('#progress_bar').css('width',due_par+'%');
							 $('#due_par').text('PAID '+parseInt(due_par)+'%');
						});
						
						$( window ).resize(function() {
							 $('#pr_bar_img').width($('#pr_div').outerWidth());
							var due_par=((tot_pay-tot_due)/tot_pay*100);
							 $('#progress_bar').css('width',due_par+'%');
							});
						function checkout_notification()
						{
							swal("Alert", "No Data Can Edited After Checkout")						
							}
							function single_group_alert()
							{
							swal("Alert", "This cannot be edited for single bookings under group")		
							}
					</script>