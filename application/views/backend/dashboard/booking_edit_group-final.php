<?php 
	$sb_row=$this->dashboard_model->get_sbID_detail($_GET["group_id"]);
	$bookingStatusID = $sb_row->booking_status_id;
	$posG=$this->dashboard_model->all_pos_booking_group($_GET["group_id"]);
	$posG_amount=0;
	$posGAMTT=0;
	$posGAMT=0;
	if($posG){
		foreach ($posG as $keyG ) {
		  $posG_amount=$posG_amount+$keyG->total_due;
		  $posGAMTT=$keyG->total_amount- $keyG->total_paid;
		  $posGAMT=$posGAMT+$posGAMTT;
		}
	}
 ?>

<div class="row">
  <div class="col-md-12">
    <?php foreach ($details2 as $key3 ) { $groupID = $key3->id;?>
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase"> Group Id: <?php echo "HM0".$this->session->userdata('user_hotel')."GRP00".$key3->id; ?> </span> </div>
        <?php
        foreach ($details3 as $det ) {
          
          if($det->booking_status_id =='4'){

            $txtcol="#0fd906";
            $bckcol="#c7F6c5";
            $tx="Confirmed";

          }elseif ($det->booking_status_id=='5') {
            
               $txtcol="#39B9A1";
            $bckcol="#EBF8F5";
            $tx="Checked In";
          }elseif ($det->booking_status_id=='6') {
            
                 $txtcol="#297CAC";
            $bckcol="#E9F1F6";
            $tx="Checked Out";
          }
        }
         ?>
        <div class="actions"> <a style="background-color:<?php echo $bckcol; ?>; color:<?php echo $txtcol; ?>; font-size: 18px; padding: 0 5px; text-decoration:none;"> <?php echo $tx; ?> </a> </div>
        <div style="width:100%; text-align:center; padding: 10px 0; font-size: 16px; line-height:18px; font-weight:700;"> <?php echo $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'))->hotel_name; ?> </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body form-horizontal form-row-sepe">
          <div class="form-body">
            <div class="row">
              <div class="col-md-3 text-center">
                <?php 
    
                      foreach ($details3 as $dum ) {
                     
                        $b_id=$dum->booking_id;
                      }
    
                      $guest = $this->dashboard_model->get_booking_details($b_id); 
                      ?>
                <?php if(isset($guest->g_photo_thumb) && trim($guest->g_photo_thumb) != '') { ?>
                <img src="<?php echo base_url();?>upload/guest/<?php echo $guest->g_photo_thumb;?>" class="img-responsive" style="border:1px solid #dddddd; padding:5px; width:100%"/>
                <?php } else { ?>
                <img src="<?php echo base_url();?>upload/guest/no_images.png" class="img-responsive" style="border:1px solid #dddddd; padding:5px; width:100%"/>
                <?php } ?>
              </div>
              <div class="col-md-6"  id="b-card">
                <h4><strong>Booking</strong></h4>
                <div class="form-group">
                  <div class="col-md-6">
                    <label><strong style="color: #2B3643;">Type:</strong></label>
                    &nbsp;
                    <label style="color:#008F6C"> Group </label>
                  </div>
                  <div class="col-md-6">
                    <label><strong style="color: #2B3643;">Center:</strong></label>
                    <label style="color:#008F6C"> <?php echo $key3->p_center; ?> </label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6">
                    <label><strong style="color: #2B3643;">Source:</strong></label>
                    <label style="color:#008F6C">
                      <?php if(isset($key3->booking_source)){echo $key3->booking_source ;}?>
                    </label>
                  </div>
                  <div class="col-md-6">
                    <label><strong style="color: #2B3643;">Taken on:</strong></label>
                    &nbsp;
                    <label style="color:#008F6C">
                      <?php if(isset($key3->booking_source)){echo $key3->booking_source ;}?>
                    </label>
                  </div>
                  <div class="col-md-6">
                    <label><strong style="color: #2B3643;">Nature Of Visit:</strong></label>
                    &nbsp;
                    <label style="color:#008F6C">
                      <?php if(isset($key3->nature_visit)){echo $key3->nature_visit ;}?>
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6">
                    <label><strong style="color: #2B3643;">Number of Guest:</strong></label>
                    <label style="color:#008F6C"> <?php echo $key3->number_guest; ?> </label>
                  </div>
                  <div class="col-md-6">
                    <label><strong style="color: #2B3643;">Number of Room:</strong></label>
                    <label style="color:#008F6C"> <?php echo $key3->number_room; ?> </label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6">
                    <label><strong style="color: #2B3643;">Food Plan:</strong></label>
                    <label style="color:#008F6C"> N/A</label>
                  </div>
                </div>
                <div class="form-group col-md-12">
                  <hr style="margin:0;padding:0; background:#eee;"/>
                </div>
                <h4><strong>Guest</strong></h4>
                <div class="form-group">
                  <div class="col-md-6">
                    <label><strong style="color: #2B3643;">Name: </strong></label>
                    <label style="color:#008F6C"><?php echo $guest->g_name; ?></label>
                  </div>
                  <div class="col-md-6">
                    <label><strong style="color: #2B3643;">Type:</strong></label>
                    &nbsp;
                    <label style="color:#008F6C"> General </label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6">
                    <label><strong style="color: #2B3643;">Phone No:</strong></label>
                    <label style="color:#008F6C">
                      <?php if(isset($guest->g_contact_no)){echo $guest->g_contact_no ;}?>
                    </label>
                  </div>
                  <div class="col-md-6">
                    <label><strong style="color: #2B3643;">Email</strong></label>
                    <label style="color:#008F6C"> <?php echo $guest->g_email; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <div  class="col-md-12">
                    <label><strong style="color: #2B3643;">Guest Address:</strong></label>
                    <label style="color:#008F6C">
                      <?php if(isset($guest->g_address)){echo $guest->g_address ;}?>
                    </label>
                  </div>
                </div>
              </div>
              <div class="col-md-3 text-center"> <img src="" id="pay_symbol" class="img-responsive" style="padding:20px;" /> </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <hr style="margin:15px 0;padding:0; background:#eee;"/>
              </div>
              <div class="col-md-6">
                <label><strong style="color: #2B3643;">Check In</strong></label>
                &nbsp;
                <label style="color:#13ad2d"> <?php echo date("dS M Y",strtotime($key3->start_date)); ?> </label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <label><strong style="color: #2B3643;">Check Out</strong></label>
                &nbsp;
                <label style="color:#f3565d"> <?php echo  date("dS M Y",strtotime($key3->end_date)); ?> </label>
              </div>
              <div class="col-md-6 text-right">
                <label><strong style="color: #2B3643;">Total Amount:</strong></label>
                &nbsp;
                <label style="color:#13ad2d"> <i class="fa fa-inr" style="font-size:12px;"></i> <span id="tot_am">4950</span> </label>
              </div>
              <div class="col-md-12">
                <div class="progress" id="pr_div" style="margin:10px 0 0 0; height:14px; position:relative;">
                  <div class="progress-bar" id="progress_bar" style="width: 0%; background:none; overflow:hidden; position:relative;"> <img id="pr_bar_img" src="<?php echo base_url();?>assets/dashboard/assets/admin/layout/img/bar.jpg" style="vertical-align:top; height:14px; width:800px;"> <span class="sr-only" id="due_par"></span> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Reservation Details</span> </div>
        <div class="actions"> <a href="javascript:;" class="btn btn-circle btn-icon-only green" onClick='gbEdit()'><i class="fa fa-pencil-square-o"></i></a> </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body  form-row-sepe">
          <div class="form-body">
            <div class="grupedit" style="display:none;">
              <div id="rmid" class="row">
                  <div class="col-md-12 bl-allroom" id="rmt">
                    <div class="portlet light bordered">
                      <div class="portlet-title">
                        <div class="caption"> <span class="caption-subject bold uppercase">Available Room</span> </div>
                      </div>
                      <div class="portlet-body form">
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                        <div class="btn-group">
                          <label class="btn grey-cascade">
                            <input class="noClick" id="" value="" type="checkbox">
                            203 </label>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div id="bcid">
                <ul class="nav nav-tabs">
                  <li class="active"> <a href="#default" data-toggle="tab">Default Rates</a> </li>
                  <li> <a href="#individual" data-toggle="tab">Individual Rates</a> </li>
                </ul>
                <div class="tab-content">
                  <div class="row tab-pane fade active in" id="default">
                    <div class="col-md-12">
                      <div role="form" class="form-inline" style="overflow:hidden;">
                        <div class="pull-left">
                          <label class="pad-s">Default Room Rent</label>
                          <div class="form-group">
                            <input class="form-control input-small" type="text" id="trrID" value="">
                          </div>
                          <button class="btn btn-default" type="button" id="btn1">Add</button>
                        </div>
                        <div class="pull-right">
                          <label class="pad-s">Default Food Inclusion</label>
                          <div class="form-group">
                            <input class="form-control input-small" type="text" id="tfiID" value="">
                          </div>
                          <div class="form-group">
                            <input class="form-control input-small" type="text" placeholder="Meal Plan" id="cmp1" style="display:none;">
                            <select class="form-control" onchange="getChange1(this.value)">
                              <option  value="" selected="selected">Plan</option>
                              <option value="AP">AP</option>
                              <option value="MAP">MAP</option>
                              <option value="EP">EP</option>
                              <option value="CP">CP</option>
                            </select>
                          </div>
                          <button class="btn btn-default" type="button" id="btn2">Add</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row tab-pane fade" id="individual">
                    <div class="col-md-12">
                      <div role="form" class="form-inline" style="overflow:hidden;">
                        <div class="pull-left">
                          <label class="pad-s">Adult</label>
                          <div class="form-group">
                            <input class="form-control input-small" type="text" placeholder="Room Charge" id="arc" onblur="getARC(this.value)">
                          </div>
                          <div class="form-group">
                            <input class="form-control input-small" type="text" placeholder="Food Inclusions" id="afi" onblur="getAFI(this.value)">
                          </div>
                          <div class="form-group">
                            <select class="form-control" onchange="getChange(this.value)">
                              <option  value="" selected="selected">Plan</option>
                              <option value="AP">AP</option>
                              <option value="MAP">MAP</option>
                              <option value="EP">EP</option>
                              <option value="CP">CP</option>
                            </select>
                          </div>
                        </div>
                        <div class="pull-right">
                          <label class="pad-s">Kids</label>
                          <div class="form-group">
                            <input class="form-control input-small" type="text" placeholder="Room Charge" id="crc" onblur="getCRC(this.value)">
                          </div>
                          <div class="form-group">
                            <input class="form-control input-small" type="text" placeholder="Food Inclusions" id="cfi" onblur="getCFI(this.value)">
                          </div>
                          <div class="form-group">
                            <input class="form-control input-small" type="text" placeholder="Meal Plan" id="cmp">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="table-scrollable" > 
              <!-- <form type="post" target="<?php echo base_url(); ?>dashboard/add_group_booking" >-->
              <table class="table table-striped table-hover table-bordered mass-book" id="myTable">
                <tbody>
                  <tr>
                    <th> # </th>
                    <th> Type </th>
                    <th> Room </th>
                    <th> Guest Name </th>
                    <th> Chk in </th>
                    <th> Chk Out </th>
                    <th> Days </th>
                    <th> Status </th>
                    <th> F_Plan </th>
                    <th> A_R_R </th>
                    <th> A_F_I </th>
                    <th> C_R_R </th>
                    <th> C_F_I </th>
                    <th> Adlt </th>
                    <th> Kid </th>
                    <th> T_R_R </th>
                    <th> Serivce Tax </th>
                    <th> Serivce Charge </th>
                    <th> Room Vat </th>
                    <th> T_F_I </th>
                    <th>Food VAT </th>
                    <th> P_D_R </th>
                    <th> Price </th>
                    <th> E_C_S </th>
                  </tr>
                  <?php $i=0; $total=0; if(isset($details) && $details != ""){ 
				  foreach ($details as $key  ) { 
				  
							if(isset($details3[$i]->booking_id) && ($details3[$i]->booking_id))
							{
                            $booking_id= $details3[$i]->booking_id;
							}

                                $poses=$this->dashboard_model->all_pos_booking($booking_id);
                                $pos_amount=0;
                                 if($poses){
                                     foreach ($poses as $keyy ) {

                                        # code...
                                        $pos_amount=$pos_amount+$keyy->total_due;
                                      }}

									if(isset($details3[$i]->booking_id) && ($details3[$i]->booking_id))
										{
                                      $service_amount=$details3[$i]->service_price;
                                       $charge_amount=$details3[$i]->booking_extra_charge_amount;
										}
                            $i++;
                            
                            ?>
                  <tr>
                    <td><?php echo $key->id; ?></td>
                    <td><?php echo $key->type; ?></td>
                    <td><a href="<?php echo base_url(); ?>dashboard/booking_edit?b_id=<?php echo $booking_id; ?>"><?php echo $key->roomNO; ?></a></td>
                    <td><?php echo $key->gestName; ?></td>
                    <td><input type="text" value="<?php echo $key->startDate; ?>" class="form-control date-picker input-sm edt" name="startDate[]" autocomplete="off" id="startDate" disabled></td>
                    <td><input type="text" value="<?php echo $key->endDate; ?>" class="form-control date-picker input-sm edt" name="endDate[]" autocomplete="off" id="endDate" disabled></td>
                    <td><?php echo $key->days; ?></td>
                    <td><?php echo "Confirmed"; ?></td>
                    <td><?php echo $key->mealPlan; ?></td>
                    <td><input id="arr" name="arr[]" type="text" value="<?php echo $key->adultRoomRent; ?>" disabled class="form-control input-sm edt" ></td>
                    <td><input id="afi" name="afi[]" type="text" value="<?php echo $key->afi; ?>" disabled class="form-control input-sm edt" ></td>
                    <td><input id="crr" name="crr[]" type="text" value="<?php echo $key->childRoomRent; ?>" disabled class="form-control input-sm edt" ></td>
                    <td><input id="cfi" name="cfi[]" type="text" value="<?php echo $key->cfi; ?>" disabled class="form-control input-sm edt" ></td>
                    <td><input id="adultNO" name="adultNO[]" type="text" value="<?php echo $key->adultNO; ?>" disabled class="form-control input-sm edt" ></td>
                    <td><input id="childNO" name="childNO[]" type="text" value="<?php echo $key->childNO; ?>" disabled class="form-control input-sm edt" ></td>
                    <td><?php echo $key->trr; ?></td>
                    <td><?php echo $key->room_st; ?></td>
                    <td><?php echo $key->room_sc; ?></td>
                    <td><?php echo $key->room_vat; ?></td>
                    <td><?php echo $key->tfi; ?></td>
                    <td><?php echo $key->food_vat; ?></td>
                    <td><?php echo $key->pdr; ?></td>
                    <td><?php echo $key->price; ?></td>
                    <td><?php echo $key->service; ?></td>
                  </tr>
                  <?php $total=$total+ $key->price+ $key->service; } }?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="form-actions right" style="display:none;" id="dpl">
            <button class="btn btn-primary" id="editSubmit">Save</button>
            <button class="btn btn-danger" id="editClose">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Extra Charges Added</span> </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body form-horizontal form-row-sepe">
          <div class="form-body">
            <table class="table table-striped table-hover" width="100%">
              <thead>
                <tr>
                  <th width="25%"> Description </th>
                  <th width="15%"> Qty </th>
                  <th width="20%"> Unit Price </th>
                  <th width="15%"> Tax % </th>
                  <th width="20%"> Total </th>
                  <th width="5%"> Action </th>
                </tr>
              </thead>
              <tbody>
                <?php
                
                                    foreach ($details2 as $groups ) {
                                      # code...
                
                                       $charge_string=$groups->charges_id;
                
                                        $charge_total_price=$groups->charges_cost;
                
                
                                    }
                
                                     
                                            $charge_sum=0;
                                if($charge_string!=""){
                                    ?>
                <?php
                
                                    $charge_array=explode(",", $charge_string);


                
                
                
                                    //print_r($charge_string); 
									//echo sizeof($charge_array);
                
                     for ($i=0; $i < sizeof($charge_array) ; $i++)
				{ 
                                        # code...
                
                        $charges=$this->dashboard_model->get_charge_details($charge_array[$i]);
						if(isset($charges) && $charges != '0')
						{
					
                               ?>
                <tr>
                  <td ><?php echo $charges->crg_description; ?></td>
                  <td ><?php echo $charges->crg_quantity; ?></td>
                  <td ><?php echo $charges->crg_unit_price; ?></td>
                  <td ><?php echo $charge_tax= ($charges->crg_tax ); 
                                        echo "%";?></td>
                  <td ><?php echo $charge_grand_total=$charges->crg_total; ?></td>
                  <td ><a <?php if($sb_row->booking_status_id != 6) {?>href="<?php echo base_url()."dashboard/remove_charge_from_booking_group/?c_id=".$charge_array[$i]."&booking_id=".$key3->id; ?>" <?php } else {echo "onclick='checkout_notification();'";}?> class="btn red btn-sm" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                </tr>
                <?php $charge_sum=$charge_sum+$charge_grand_total; 
						} 
				}
					} ?>
                <tr>
                  <td class="form-group"><input onkeyup="search_charge()" id="c_name" type="text" value="" class="form-control input-sm" placeholder="Description"></td>
                  <td class="form-group"><input id="c_quantity" type="text" value="" class="form-control input-sm" placeholder="Qty"></td>
                  <td class="hidden-480 form-group"><input onblur="calculation(this.value)" id="c_unit_price" type="text" value="" class="form-control input-sm" placeholder="Unit Price"></td>
                  <td class="hidden-480 form-group"><input id="c_tax" onblur="calculation2(this.value)" type="text" value="" class="form-control input-sm" placeholder="Tax %"></td>
                  <td class="hidden-480 form-group"><input id="c_total" type="text" value="" class="form-control input-sm" placeholder="Total"></td>
                  <td><button class="btn green" type="button" id="addCharge" <?php if($sb_row->booking_status_id != 6) echo "disabled'";?>><i class="fa fa-plus" aria-hidden="true"></i></button></td>
                </tr>
                <tr>
                  <td style="padding:0px;"><div id="charges" style="/*display:none;*/ z-index: 1200; max-height:200px;overflow:scroll;overflow-x: hidden"></div></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Individual Charges:</span> </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body form-horizontal form-row-sepe">
          <table class="table table-striped table-hover table-bordered mass-book" >
            <thead>
              <tr>
                <th width="25%"> Room Number </th>
                <th width="15%"> Booking Id </th>
                <th width="15%"> Extra charge Amt </th>
                <th width="15%"> Extra Service Amt </th>
                <th width="15%"> POS Amt </th>
                <th width="15%"> Amt Pending </th>
              </tr>
            </thead>
            <tbody>
              <?php $tot_due=0; foreach ($details3 as $dt ) {
                      # code...
                     ?>
              <tr>
                <td class="form-group"><?php echo $dt->room_no ?></td>
                <td class="form-group"><?php  $booking_id2=$dt->booking_id; 
                          echo "HM0".$this->session->userdata("user_hotel")."00".$dt->booking_id;
                      ?></td>
                <td class="form-group"><?php echo $dt->booking_extra_charge_amount ?></td>
                <td class="form-group"><?php echo $dt->service_price ?></td>
                <td class="form-group"><?php 
                           $poses=$this->dashboard_model->all_pos_booking($booking_id2);
                                $pos_amount=0;
                                 if($poses){
                                     foreach ($poses as $key ) {

                                        # code...
                                        $pos_amount=$pos_amount+$key->total_due;
                                      }}
                                      echo $pos_amount;

                      ?></td>
                <td class="form-group"><?php 

                      $amountPaid = $this->dashboard_model->get_amountPaidByBookingID($booking_id2);

                         if(isset($amountPaid) && $amountPaid) {
                    $paid=$amountPaid->tm;
                  }
                  else{
                    $paid=0;
                  }



                    echo $due=$dt->booking_extra_charge_amount +$dt->service_price+$pos_amount -$paid;
                    $tot_due=$tot_due+$due;

                    


                      ?></td>
              </tr>
              <?php } 

                       foreach ($details2 as $key3 ) { $groupID = $key3->id;

                       echo $this->dashboard_model->update_indi($tot_due,$groupID);
                    }
      
                    ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">POS Information</span> </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body form-row-sepe">
          <div class="form-body">
            <div class="cards-main">
              <?php 
                $pos=$this->dashboard_model->all_group_pos($_GET["group_id"]);
                if($pos && isset($pos)){
                  foreach ($pos as $key ) {
                    # code...
                

              ?>
              
              <!--start loop-->
              
              <div class="card" id="card" style="text-align:center;">
                <?php if($key->total_due ==0){?>
                <a class="btn green" data-toggle="modal" href="#responsive-<?php echo $key->pos_id; ?>">
                <?php }else{ ?>
                <a class="btn red" data-toggle="modal" href="#responsive-<?php echo $key->pos_id; ?>">
                <?php } ?>
                <?php if($key->total_due ==0){ ?>
                <i class="fa fa-check btn green status"></i>
                <ul class="pull-left list-unstyled">
                  <li><i class="fa fa-money"></i></li>
                  <li><i class="fa fa-glass"></i></li>
                  <li><i class="fa fa-gift"></i></li>
                </ul>
                <?php } else{?>
                <i class="fa fa-warning btn red status"></i>
                <ul class="pull-left list-unstyled">
                  <li><i class="fa fa-money"></i></li>
                  <li><i class="fa fa-glass"></i></li>
                  <li><i class="fa fa-gift"></i></li>
                </ul>
                <?php } ?>
                <span class="pull-right"> <span class="in-n-d"><?php echo $key->invoice_number; ?> -- <?php echo $key->date; ?></span> <span class="deu">Due: <?php echo $key->total_due; ?> /-</span> <span class="pos-t"><?php echo "Paid from POS:".$key->total_paid_pos; ?> /-</span> </span> </a>
                <div id="responsive-<?php echo $key->pos_id; ?>" class="modal fade" tabindex="-1" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <div class="clearfix" style="margin-top:30px;"><span class="pull-left">Invoice Number: <?php echo $key->invoice_number; ?></span><span class="pull-right">Date: <?php echo $key->date; ?></span></div>
                        <!--<h4>Guest Name: <?php //echo $bookingDetails->cust_name; ?></h4>--> 
                      </div>
                      <div class="modal-body">
                        <table class="table table-striped table-hover" id="package_details">
                          <thead>
                            <tr>
                              <th align="center" valign="middle"> Item  Name </th>
                              <th align="center" valign="middle"> Item Quantity </th>
                              <th align="center" valign="middle"> Item Price </th>
                              <th align="center" valign="middle"> Total Price </th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $items=$this->dashboard_model->all_pos_items($key->pos_id);
                                     if($items && isset($items)){
                                      foreach ($items as $item ) {
                            ?>
                            <tr>
                              <td align="center" valign="middle"><?php echo $item->item_name; ?></td>
                              <td align="center" valign="middle"><?php echo $item->item_quantity; ?></td>
                              <td align="center" valign="middle" class="hidden-480"><?php echo $item->unit_price; ?></td>
                              <td align="center" valign="middle" class="hidden-480"><?php echo number_format($item->unit_price* $item->item_quantity, 2, '.',''); ?></td>
                            </tr>
                            <?php }} ?>
                          </tbody>
                        </table>
                        <div class="row">
                          <div class="col-xs-9" style="padding-left: 500px;">
                            <ul class="list-unstyled">
                              <li> <b>Bill Amt:</b> </li>
                            </ul>
                          </div>
                          <div class="col-xs-3 pull-right" style="text-align:center;">
                            <div class="dwn-two">
                              <ul class="list-unstyled" >
                                <li> <?php echo $key->bill_amount; ?> </li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-xs-9" style="padding-left: 500px;">
                            <ul class="list-unstyled">
                              <li> <b>Tax(%):</b> </li>
                            </ul>
                          </div>
                          <div class="col-xs-3 pull-right" style="text-align:center;">
                            <div class="dwn-two">
                              <ul class="list-unstyled" >
                                <li> <?php echo $key->tax; ?> </li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-xs-9" style="padding-left: 500px;">
                            <ul class="list-unstyled">
                              <li> <b>Discount(%):</b> </li>
                            </ul>
                          </div>
                          <div class="col-xs-3 pull-right" style="text-align:center;">
                            <div class="dwn-two">
                              <ul class="list-unstyled" >
                                <li> <?php echo $key->discount; ?> </li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-xs-9" style="padding-left: 500px;">
                            <ul class="list-unstyled">
                              <li> <b>Total Bill :</b> </li>
                            </ul>
                          </div>
                          <div class="col-xs-3 pull-right" style="text-align:center;">
                            <div class="dwn-two">
                              <ul class="list-unstyled" >
                                <li> <?php echo $key->total_amount; ?> </li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-xs-9" style="padding-left: 500px;">
                            <ul class="list-unstyled">
                              <li> <b>Paid :</b> </li>
                            </ul>
                          </div>
                          <div class="col-xs-3 pull-right" style="text-align:center;">
                            <div class="dwn-two">
                              <ul class="list-unstyled" >
                                <li> <?php echo $key->total_paid; ?> </li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-xs-9" style="padding-left: 500px;">
                            <ul class="list-unstyled">
                              <li> <b>Due :</b> </li>
                            </ul>
                          </div>
                          <div class="col-xs-3 pull-right" style="text-align:center;">
                            <div class="dwn-two">
                              <ul class="list-unstyled" >
                                <li> <?php echo $key->total_due; ?> </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <!--<div class="row">
                        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                          <input type="text" autocomplete="off" class="form-control"  id="t_amount5" required="required">
                          <label> Amount<span class="required" id="b_contact_name">*</span></label>
                          <span class="help-block">Add Transaction Here</span> </div>
                        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                          <select  autocomplete="off" class="form-control"  id="t_mode5" 
                                    required="required">
                            <option value=""></option>
                            <option value="cash">Cash</option>
                            <option value="cheque">Cheque</option>
                            <option value="card">Card</option>
                            <option value="bank transafer">Bank Transfer</option>
                          </select>
                          <label> Payment Mode<span class="required" id="b_contact_name">*</span></label>
                          <span class="help-block">Add Payment Mode</span> </div>
                        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                          <input type="text" autocomplete="off" class="form-control"  id="t_bank_name5" >
                          <label> Bank Name<span class="required" id="b_contact_name">*</span></label>
                          <span class="help-block">Bank Name</span> </div>
                        <!--<div class="form-group form-md-line-input form-md-floating-label col-md-1">
                          <button class="btn green pull-right" onclick="return add_pos_transaction('<?php echo $key->pos_id; ?>')" >Pay</button>
                        </div>--> 
                        <!--</div>--> 
                      </div>
                      <!--<div class="modal-footer">
                      <button class="btn green pull-right" onclick="return add_pos_transaction('<?php //echo $key->pos_id; ?>')" disabled>Pay</button>
                    </div>--> 
                    </div>
                  </div>
                </div>
              </div>
              
              <!--end loop-->
              
              <?php }} ?>
              </a> </div>
          </div>
          <?php if($pos && isset($pos))
		  {?>
          <div class="form-actions right">
            <button class="btn blue" onclick="pos_bill('<?php echo $_GET["group_id"]; ?>')" >View Bill</button>
          </div>
          <?php }?>
        </div>
      </div>
    </div>
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Extra Charges Added</span> </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body form-horizontal form-row-sepe">
          <div class="form-body">
            <div class="table-scrollable" >
              <table class="table table-striped table-hover table-bordered" width="100%">
                <tbody>
                  <tr>
                    <td width="90%" align="right">Total Amount:</td>
                    <td><input class="form-control input-sm" type="text" value="<?php $grand=$charge_sum+$total; echo number_format(($grand + $posGAMT),2,'.',''); ?>" id="totalAmount" readonly></td>
                  </tr>
                  <?php 
                        $amountPaid = $this->dashboard_model->get_amountPaidByGroupID($_GET['group_id']);
						if(isset($amountPaid) && $amountPaid) {
							$paid=$amountPaid->tm;
						} else {
						  $paid=0;
						}
					?>
                  <tr>
                    <td width="90%" align="right">Discount:</td>
                    <td><div class="input-group">
                        <input type="text" id="discountVal" class="form-control input-sm" value="<?php foreach ($details2 as $key3 ) { echo $ad = $key3->additional_discount; }?>">
                        <span class="input-group-btn">
                        <button type="button" id="btnSubmit" class="btn blue btn-sm"><i class="fa fa-check"></i></button>
                        </span> </div></td>
                  </tr>
                  <tr>
                    <td width="90%" align="right">Total Payable Amount:</td>
                    <td><input class="form-control input-sm" type="text" value="<?php echo number_format(($payAmount = $grand-$ad + $posGAMT),2);?>" id="totalPayableAmount" readonly></td>
                  </tr>
                  <tr>
                    <td width="90%" align="right">Total Due:</td>
                    <td><input class="form-control input-sm" type="text" value="<?php echo number_format(($payAmount-$paid),2);?>" id="totalDue" readonly></td>
                    <input class="form-control input-sm" type="hidden" value="<?php echo number_format(($payAmount-$paid),2);?>" id="totalDue1">
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Transactions</span> </div>
      </div>
      <!--<h4 style="color:red;margin-top:20px;">Not Availabe</h4>-->
      <div class="portlet-body form">
        <table class="table table-striped table-hover" width="100%">
          <thead>
            <tr style="">
              <th align="center" valign="middle" > # </th>
              <th align="center" valign="middle" > Transaction Date </th>
              <th align="center" valign="middle" > Payment Mode </th>
              <th align="center" valign="middle" > Bank name </th>
              <th align="right" valign="middle"> Amount </th>
            </tr>
          </thead>
          <tbody>
            <?php  if($transaction){ foreach ($transaction as $keyt ) { ?>
            <tr style="background: #F2F2F2">
              <td align="center" valign="middle"><?php echo $keyt->t_id; ?></td>
              <td align="center" valign="middle"><?php echo $keyt->t_date; ?></td>
              <td align="center" valign="middle"><?php echo $keyt->t_payment_mode; ?></td>
              <td align="center" valign="middle"><?php echo $keyt->t_bank_name; ?></td>
              <td align="right" valign="middle"><?php echo $keyt->t_amount; ?></td>
            </tr>
            <?php  } }?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div id="payment" class="caption font-green-haze"> <span class="caption-subject bold uppercase">Take Payment</span> </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body form-row-sepe">
          <div class="form-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Group Id<span class="required">*</span></label>
                  <input type="text" class="form-control input-sm" id="" name="card_number" value="<?php echo $_GET['group_id'] ?>" 
                                                                        disabled="disabled"/>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Total Amount Paybale<span class="required">*</span></label>
                  <input type="text" required class="form-control input-sm" id="" name="card_number" value="<?php echo number_format(($payAmount),2); ?>" 
                                                                        disabled="disabled"/>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Due Amount<span class="required">*</span></label>
                  <?php 
                    $amountPaid = $this->dashboard_model->get_amountPaidByGroupID($_GET['group_id']);
					if(isset($amountPaid) && $amountPaid) {
						$paid=$amountPaid->tm;
					 } else {
					  $paid=0;
					}
				?>
                  <input type="text" class="form-control input-sm" id="due_amount"  name="card_number" value="<?php echo number_format(($due= $payAmount-$paid),2); ?>" disabled="disabled"/>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Amount <span class="required"> * </span> </label>
                  <input type="text" id="add_amount" class="form-control input-sm" name="card_number" placeholder="Give Amount" onblur="check_amount();"  required="required" />
                  <input type="hidden" id="booking_id" value="">
                  <input type="hidden" id="booking_status_id" value="">
                </div>
                <script type="text/javascript">
                    function check_amount()
                    {
                    var amount = $("#add_amount").val();
                    //alert(amount);
                    var due_amount  = $("#due_amount").val();
                    //alert(due_amount);
                    if(parseInt(amount) > parseInt(due_amount))
                    {
                    //alert("The amount should be less than the due amount Rs: "+due_amount);
                    swal({
                                                title: "Amount Should Be Smaller",
                                                text: "(The amount should be less than the due amount Rs: +due_amount)",
                                                type: "warning"
                                            },
                                            function(){
                                                //location.reload();
                                            });
                    $("#add_amount").val("");
                    return false;
                    }
                    else{
                    return true;
                    }
                    }
                    
                                                                </script> 
              </div>
              <div class="col-md-3">
                <label>Profit Center <span class="required"> * </span> </label>
                <select name="" class="form-control input-sm" id="p_center">
                   <?php $pc=$this->dashboard_model->all_pc();?>
						  <?php
						  $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
						  ?>
						  <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
                           <?php $pc=$this->dashboard_model->all_pc1();
							foreach($pc as $prfit_center){
							?>
						  
						  <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
							<?php }?>
                </select>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Payment Mode <span class="required"> * </span> </label>
                  <select name="country" class="form-control input-sm" placeholder=" Booking Type" id="pay_mode" onChange="paym(this.value);">
                    <option value="">Select Payment Mode</option>
                    <option value="cashs">Cash</option>
                    <option value="cards">Debit /Credit Card</option>
                    <option value="funds">Fund transfer</option>
                    <option value="Bill To Company">Bill To Company</option>
                  </select>
                </div>
              </div>
              <div id="cards" style="display:none;">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Bank Name <span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="bankname" >
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Card NO<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="card_no" >
                  </div>
                </div>
              </div>
              <div id="fundss" style="display:none;">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Bank Name <span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="bankname" >
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>A/C No<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="ac_no" >
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>IFSC Code<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="ifsc" >
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-actions right"> 
            <!---  <a target="_blank" href="<?php echo base_url();?>dashboard/generate_invoice_group/<?php echo $_GET["group_id"]; ?>_gb">
            <button  class="btn btn-success" id="">Add Group invoice</button></a>---->
            <button onclick=" return ajax_hotel_submit_grp();" class="btn btn-primary" id="add_submit">Submit</button>
          </div>
        </div>
      </div>
    </div>
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Charge Summary</span> </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body form-horizontal form-row-sepe">
          <div class="form-body">
            <div class="row i-nr">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-6"><strong style="color: #2B3643;">Booking Amount:</strong></label>
                  <label class="col-md-6" style="color:#008F6C"> <i class="fa fa-inr"></i> <?php echo  number_format(($total),2); ?></label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-6"><strong style="color: #2B3643;">Service Amount:</strong></label>
                  <label class="col-md-6" style="color:#008F6C"><i class="fa fa-inr"></i> <?php echo  number_format((0),2); ?></label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-6"><strong style="color: #2B3643;">Charge Amount:</strong></label>
                  <label class="col-md-6" style="color:#008F6C"><i class="fa fa-inr"></i> <?php echo number_format(($charge_sum),2); ?></label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-6"><strong style="color: #2B3643;">POS Amount:</strong></label>
                  <label class="col-md-6" style="color:#008F6C"><i class="fa fa-inr"></i> <?php echo number_format(($posG_amount),2); ?></label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-6"><strong style="color: #2B3643;">Discount Amount:</strong></label>
                  <label class="col-md-6" style="color:#008F6C"><i class="fa fa-inr"></i>
                    <?php foreach ($details2 as $key9 ) { echo ' '.number_format(($key9->additional_discount),2); }?>
                  </label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-6"><strong style="color: #2B3643;">Total Amount:</strong></label>
                  <label class="col-md-6" style="color:#008F6C"> <i class="fa fa-inr"></i> <?php echo number_format(($payAmount),2); ?></label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-6"><strong style="color: #2B3643;">Amount Paid:</strong></label>
                  <label class="col-md-6" style="color:#008F6C"><i class="fa fa-inr"></i> <?php echo  number_format(($paid=$payAmount-$due),2); ?></label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-6"><strong style="color: #2B3643;">Pending Amount:</strong></label>
                  <label class="col-md-6" style="color:#008F6C"> <i class="fa fa-inr"></i> <?php echo  number_format(($due),2); ?>
                    <?php
                    $per_paid=($paid/$payAmount)*100;
                    $per_due=($due/$payAmount)*100;
                   ?>
                  </label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-6"><strong style="color: #2B3643;">% Paid:</strong></label>
                  <label class="col-md-6" style="color:#008F6C"><?php echo round($per_paid,2) ?></label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-6"><strong style="color: #2B3643;">% pending:</strong></label>
                  <label class="col-md-6" style="color:#008F6C"><?php echo round($per_due,2); ?></label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-actions right">
            <button onclick=" return bill();" class="btn btn-primary" id="add_bill">Generate Invoice</button>
            <button onclick=" return bill2();" class="btn btn-primary" id="add_bill">Generate Invoice 2</button>
            <script type="text/javascript">

                       function bill(){
                        window.location="<?php echo base_url(); ?>dashboard/booking_group_pdf?group_id=<?php echo $_GET["group_id"]; ?>";
                       }
                       function bill2(){
                        window.location="<?php echo base_url(); ?>dashboard/booking_group_pdf2?group_id=<?php echo $_GET["group_id"]; ?>";
                       }
                    </script> 
          </div>
        </div>
      </div>
    </div>
    <?php foreach ($details3 as $key1 ) {

      ?>
    <input type="hidden" id="booking_id" value="<?php echo $key1->booking_id; ?>">
    </input>
    <?php
    } ?>
    <script type="text/javascript">
    function ajax_hotel_submit_grp()
    {
        //alert($("#add_amount").val());
		 var bookingStatusID = '<?php echo $bookingStatusID;?>';
	  //alert(bookingStatusID);
	  var due_smount = $('#due_amount').val();
	  //alert (due_smount);
	  //return false;
	  if((bookingStatusID != '6') || (due_smount > 0)){
		$('#add_submit').prop('disabled', true);
		
        var group_id = <?php echo $_GET['group_id'] ?>;
        var booking_status_id = 4;
        var p_center = $("#p_center").val();
        //alert(p_center);
       // return false;
	    var due_amount = $("#due_amount").val();
        var add_amount = $("#add_amount").val();
        var pay_mode   = $("#pay_mode").val();
        var bankname   = $("#bankname").val();
        var payment_status = "partial";
        if($("#add_amount").val() == null || $("#add_amount").val() ==0 )
        {
          //alert('Error');
          swal({
                                    title: "Error",
                                    text: "An Error Occurd",
                                    type: "warning"
                                },
                                function(){
                                    //location.reload();
                                });
          return false;
        }
        else{
        jQuery.ajax(
        {
          type: "POST",
          url: "<?php echo base_url(); ?>bookings/add_booking_transaction_grp",
          dataType: 'json',
          data: {t_group_id:group_id,t_booking_status_id:booking_status_id,p_center:p_center,t_amount:add_amount,t_payment_mode:pay_mode,t_bank_name:bankname,t_status:payment_status,due_amount:due_amount },
          success: function(data){
            //alert('Add Successfull');
            swal({
                                    title: "Add Successfull",
                                    text: "Your Payment Is Taken",
                                    type: "success"
                                },
                                function(){
                  
            location.reload();
                                    
                                });
          
          }
        });
        }
        return false;
    }
	else
	{
		checkout_notification();
	}
	}
$("#btnSubmit").click(function(){
	//alert("HERE"); exit;
	var bookingStatusID = '<?php echo $bookingStatusID;?>';
	  //alert(bookingStatusID);
	  if(bookingStatusID != '6')
	  {
	var discountVal = $("#discountVal").val();
	var totalAmount = $("#totalAmount").val();
	var totalPayableAmount = $("#totalPayableAmount").val();
	var totalDuee = $("#totalDue1").val();
	//alert(totalDuee);
	var amtDue = parseInt(totalDuee) - parseInt(discountVal);
	var amtPay = parseInt(totalAmount) - parseInt(discountVal);
	//alert(amtDue);
	var groupID = '<?php echo $groupID; ?>';
	jQuery.ajax({				
		url: '<?php echo base_url(); ?>dashboard/getAdditionalDiscount',
		type: "GET",
		dataType: "json",
		data: {discountVal:discountVal,groupID:groupID},
		success: function(data){		
		  $("#totalPayableAmount").val(amtPay);
          $("#totalDue").val(amtDue);
          location.reload();		  
		},
	}); 
	  }
	  else
	  {
		  checkout_notification();
	  }
	  
	  });	
  </script> 
  </div>
</div>
<script>

    function paym(val){
        if(val == 'cards')
        { 
            document.getElementById('cards').style.display='block';
            
        }
    else{
      document.getElementById('cards').style.display='none';
    }
        if(val=='funds')
        {
            document.getElementById('fundss').style.display='block';
           
        }
    else{
      document.getElementById('fundss').style.display='none';
    }
    
        if(val=='cashs')
        {
            document.getElementById('cardss').style.display='none';
            document.getElementById('fundss').style.display='none';
        }        
    }
    </script> 
<script type="text/javascript">
  $("#addCharge").click(function(){
	  var bookingStatusID = '<?php echo $bookingStatusID;?>';
	  //alert(bookingStatusID);
	  if(bookingStatusID != '6'){
            var name= document.getElementById('c_name').value;
            var quantity= document.getElementById('c_quantity').value;
            var unit_price= document.getElementById('c_unit_price').value;
            var tax= document.getElementById('c_tax').value;
            var total= document.getElementById('c_total').value;

            // alert(name+quantity+unit_price+tax+total);


            var booking_id=  <?php echo $_GET['group_id'] ?>;

           if(name == ""){
			  alert("Please enter name.");
	          return false;
		   }
           if(quantity == ""){
			  alert("Please enter quantity.");
	          return false;
		   }
           if(unit_price == ""){
			  alert("Please enter price.");
	          return false;
		   }		   
            jQuery.ajax(
                {
                    type: "POST",
                    url: "<?php echo base_url(); ?>dashboard/add_charge_to_booking_group",
                    dataType: 'json',
                    data: {booking_id:booking_id,name:name,quantity:quantity,unit_price:unit_price, tax:tax, total:total },
                    success: function(data){
                        alert(data.return2);
                        location.reload();
                    }
                });
	  }
	  else
	  {
		  checkout_notification();
	  }
  
}); 


/*function calculation(value) {
  var a=document.getElementById("c_quantity").value;
  var total=parseInt(a)*parseInt(value);
  document.getElementById("c_total").value=total;
  
}
function calculation2(value) {
  if(value > '0'){	
	  var a=document.getElementById("c_total").value;
	  var total=parseInt(a)+(parseInt(a)*(parseInt(value)/100));
	  document.getElementById("c_total").value=total;
  }
}*/

function calculation(value) {
	if(value > '0'){
		var a=document.getElementById("c_quantity").value;
		var total=parseFloat(a)*parseFloat(value);
		document.getElementById("c_total").value=total;
	} else {
		document.getElementById("c_total").value='0'
		document.getElementById("c_tax").value='0'
	}
	
}
function calculation2(value) {
	if(value > '0'){
		var a=document.getElementById("c_total").value;
		var total=parseFloat(a)+(parseInt(a)*(parseFloat(value)/100));
		document.getElementById("c_total").value=total;
	} else {
		var x=document.getElementById("c_unit_price").value;
		var y=document.getElementById("c_quantity").value;
		document.getElementById("c_total").value=parseInt(x)*parseInt(y);
	}

}



function pos_bill(b_id){
var p_id=2;
window.location.href = "<?php echo base_url(); ?>dashboard/pos_grand_invoice_group?b_id="+b_id;
}

$( document ).ready(function(){
		
		var tot_am=$('#totalAmount').val();
		var tot_due=$('#due_amount').val();
		var tot_pay=tot_am;
		 if(tot_due == 0)
		 {

			 $('#pay_symbol').attr('src', '<?php echo base_url(); ?>upload/paid.jpg');
		 }
		 else if(tot_due == tot_pay)
		 {

			$('#pay_symbol').attr('src', '<?php echo base_url(); ?>upload/due.jpg');
		 }
		 else 
		 {

			$('#pay_symbol').attr('src', '<?php echo base_url(); ?>upload/partial.jpg');
		 }
		$('#tot_am').text((tot_am));
		 $('#pr_bar_img').width($('#pr_div').outerWidth());
		 var due_par=((tot_pay-tot_due)/tot_pay*100);
		 $('#progress_bar').css('width',due_par+'%');
		 $('#due_par').text('PAID '+parseInt(due_par)+'%');
		// alert (tot_pay);
		 
	});
	
	$( window ).resize(function() {
		 $('#pr_bar_img').width($('#pr_div').outerWidth());
		var due_par=((tot_pay-tot_due)/tot_pay*100);
		 $('#progress_bar').css('width',due_par+'%');
		});
	function checkout_notification(){
		swal("Alert", "No Data Can Edited After Checkout")						
	}
		

function gbEdit(){
	$("#dpl").css("display", "block");
	$(".edt").prop('disabled', false);
	$(".edt").addClass('gbFocus');	
}
/*$(document).on('blur', '#ed', function () {
	$('#ed').addClass('focus');
});*/
$("#editClose").click(function(){
	$("#dpl").css("display", "none");
	$(".edt").prop('disabled', true);
	$(".edt").removeClass('gbFocus');
});
</script> 
<script>
function search_charge(){

  var value=document.getElementById("c_name").value;

  //alert(value);

    $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>dashboard/search_charge",
    data:{keyword:value},
    success: function(data){
		if(data != "")
		{
	  $("#charges").show();
      $('#charges').html(data);
		}
    }
    });
 }
//To select charge name
function selectCountry(val,unit_price,tax) 
{
$("#c_unit_price").val(unit_price);
$("#c_tax").val(tax);
$("#c_name").val(val);
$("#charges").hide();
}

</script> 
