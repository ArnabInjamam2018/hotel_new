<link href="<?php echo base_url(); ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	* {
		box-sizing: border-box;
		padding: 0;
		margin: 0;
	}
	
	body {
		font-family: Verdana, Geneva, sans-serif;
	}
	
	.list-unstyled {
		list-style: none;
	}
	
	tr {
		display: table-row;
		vertical-align: inherit;
		border-color: inherit;
	}
	
	table {
		border-spacing: 0;
		border-collapse: collapse;
		background-color: transparent;
		border-color: grey;
		display: table;
		width: 100%;
		max-width: 100%;
		margin-bottom: 20px;
		font-family: Verdana, Geneva, sans-serif;
		font-size: 11px;
		line-height: 1.42857143;
		color: #555555;
	}
	
	.td-pad th,
	.td-pad td {
		padding: 2px;
	}
	.fon-big, .fon-big table, .fon-big span{
		font-size: 16px !important;
		color: #000000 !important;
		font-family: Arial, Helvetica, sans-serif !important;
	}
	.fon-big tr, .fon-big tbody{
		background: none !important;
	}
	.fon-big hr{
		background-color: #000000 !important;
	}
	
</style>

<?php if($printer->printer_settings=='normal'){
	$print='';
}else{
	$print='fon-big';
} ?>

<div style="padding:10px 35px;" class="<?php echo $print ?> ">

	<?php 

	ini_set('memory_limit', '64M');


	if(isset($adjstAmt) && $adjstAmt) {
		$adjstAmt=$adjstAmt->amount;
	} else {
		$adjstAmt=0;
	}

//print_r($line_items);

$glo=0;

$glo1=0;

$stat=0;

$chk=$this->bookings_model->get_invoice_settings();



if(isset($chk->booking_source_inv_applicable)){

    $booking_source = $chk->booking_source_inv_applicable;

} else {

	$booking_source ="";

}

if(isset($chk->nature_visit_inv_applicable)){

    $nature_visit = $chk->nature_visit_inv_applicable;

} else {

	$nature_visit ="";

}

if(isset($chk->booking_note_inv_applicable)){

    $booking_note = $chk->booking_note_inv_applicable;

} else {

	$booking_note ="";

}

if(isset($chk->company_details_inv_applicable)){

    $company_details = $chk->company_details_inv_applicable;

} else {

	$company_details ="";

}

if(isset($chk->service_tax_applicable)){

    $service_tax = $chk->service_tax_applicable;

} else {

	$service_tax =0;

}

if(isset($chk->service_charg_applicable)){

    $service_charg = $chk->service_charg_applicable;

} else {

	$service_charg = 0;

}

if(isset($chk->luxury_tax_applicable)){

    $luxury_tax = $chk->luxury_tax_applicable;

} else {

	$luxury_tax =0;

}



if(isset($chk->invoice_pref) && isset($chk->invoice_suf)){	

	$suf=$chk->invoice_suf;

    $pref=$chk->invoice_pref;

} else {

	$suf="";

    $pref="";	

}



if(isset($chk->unit_no)){

    $unit_no = $chk->unit_no;

} else {

	$unit_no = 0;

}

if(isset($chk->unit_cat)){

    $unit_cat = $chk->unit_cat;

} else {

	$unit_cat = 0;

}

if(isset($chk->invoice_setting_food_paln)){

    $fd_plan = $chk->invoice_setting_food_paln;

} else {

	$fd_plan =0;

}



if(isset($chk->invoice_setting_service)){

    $service = $chk->invoice_setting_service;

} else {

	$service =0;

}



if(isset($chk->invoice_setting_extra_charge)){

    $extra_charge = $chk->invoice_setting_extra_charge;

} else {

	$extra_charge =0;

}



if(isset($chk->invoice_setting_laundry)){

    $laundry = $chk->invoice_setting_laundry;

} else {

	$laundry =0;

}

	if ( isset( $chk->logo ) ) {

		$logo = $chk->logo;

	} else {

		$logo = 0;

	}
	
	
	if ( isset( $chk->pos_app ) ) {

		$pos_settings = $chk->pos_app;

	} else {

		$pos_settings = 0;

	}
	
	if ( isset( $chk->adjust_app ) ) {

		$adjust_app = $chk->adjust_app;

	} else {

		$adjust_app = 0;

	}
	
	
	if ( isset( $chk->disc_app ) ) {

		$disc_app = $chk->disc_app;

	} else {

		$disc_app = 0;

	}
	
	
		if ( isset( $chk->fd_app ) ) {

		$fd_app = $chk->fd_app;

	} else {

		$fd_app = 0;

	}
	
	if ( isset( $chk->pos_seg ) ) {

		$pos_seg = $chk->pos_seg;

	} else {

		$pos_seg = 0;

	}
	
	if ( isset( $chk->paid_pos_item_show ) ) {

		$paid_pos_item_show = $chk->paid_pos_item_show;

	} else {

		$paid_pos_item_show = 0;

	}
	
		if ( isset( $chk->font_size ) ) {

		$font_sizeH = $chk->font_size;

	} else {

		$font_sizeH = 14;

	}
	
	if ( isset( $chk->hotel_color ) ) {

		$colorH = $chk->hotel_color;

	} else {

		$colorH = '#000000';

	}
	
		if ( isset( $chk->declaration ) ) {

		$declaration = $chk->declaration;

	} else {

		$declaration = '';

	}
	
		if ( isset( $chk->invoice_no ) ) {

		$invoice_no = $chk->invoice_no;

	} else {

		$invoice_no = 0;

	}
	
	if ( isset( $chk->show_bk_type ) ) {

		$show_bk_type = $chk->show_bk_type;

	} else {

		$show_bk_type = 'yes';

	}
	

foreach ($details2 as $key3 ) { ?>

	<table width="100%">

		<tr>

			<?php if($logo==1) {?>
			<td align="left" valign="middle"><img src="<?php echo base_url(); ?>upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>"/>
			</td>
			<?php }?>

			<td align="right" valign="middle">
			    <strong style="font-size:<?php echo $font_sizeH; ?>px; color:<?php echo $colorH; ?>;">
					<?php echo $hotel_name->hotel_name?>
				</strong>
			</td>

		</tr>

		<tr>

			<td colspan="2" width="100%">
				<hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;">
			</td>

		</tr>

	</table>

	<table width="100%">

		<?php 

			if(isset($tax) && $tax){

			foreach($tax as $tax_details){			

		?>

		<tr>

			<td width="70%" align="left" valign="top">
				<?php

				$paid = 0;

			
				//	$invoice = $this->dashboard_model->get_invoice_grp( $key3->id );
				$invoice = $this->dashboard_model->get_invoice_number( $key3->id ,'gb');
			

				if ( isset( $invoice ) && $invoice ) {

					$invno = $invoice->invoice_id_actual;

					$invid = $invoice->	invoice_id;

				} else {

					$invno = '';

					$invid = '';



				}

				?>

				<span style="color:#F8681A; font-weight:bold; font-size:12px; line-height:20px; display:block;">TAX INVOICE (Group Reservation)</span>
				<?php 
				
			 if($invoice_no==1){
                     	echo $pref.$invno.$invid.$suf."<br/>"; 
                }else{
                    	echo "<strong>".$pref."</strong><span>"." - ".$invid."  ".$suf."</span>"."<br/>";
                    }
            ?>
				<?php 

			date_default_timezone_set("Asia/Kolkata");

			echo "DATE:&nbsp;",date("g:i a \-\n l jS F Y") ?>
				
				<?php

				$guest_det = $this->dashboard_model->get_guest_details( $key3->guestID );

				foreach ( $guest_det as $gst )

				{

					if ( ( $key3->bill_to_com == 1 ) && ( $gst->g_type == 'Corporate' ) && ( $gst->corporate_id != NULL ) && ( $gst->corporate_id != 0 ) )

					// Check if Bill to Company is Possible

					{

						$corporate_details = $this->dashboard_model->fetch_c_id1( $gst->corporate_id );

						?>

				<ul class="list-unstyled">

					<li>
						<strong style="color:#6B67A1;">

							<?php

							if ( $corporate_details->legal_name )

								echo $corporate_details->legal_name;

							else

								echo $corporate_details->name;

							?>

						</strong>
					</li>

					<li>

						<?php 

                    $fg = 10;

                    if(isset($corporate_details->address) && ($corporate_details->address != '' && $corporate_details->address != ' ')){

                                            echo ($corporate_details->address);

                                            $fg = 0	;

                                            //echo ', ';

                                        }

                    if(isset($corporate_details->city) && ($corporate_details->city != '' && $corporate_details->city != ' ')){

                                            if($fg == 0){

                                                echo ', ';

                                                

                                            }

                                                

                                            echo $corporate_details->city;

                                            echo '<br>';

                                            $fg = 1;

                                        }

                    else if($fg == 0)

                                            $fg = 5;

                    if(isset($corporate_details->state) && ($corporate_details->state != '' && $corporate_details->state != ' ')){

                                            if($fg == 5)

                                                echo '<br>';

                                            echo $corporate_details->state;

                                            $fg = 2;

                                            //echo ', ';

                                        }

                    if(isset($corporate_details->pin_code) && $corporate_details->pin_code!= NULL){

                                            if($fg == 2 || $fg == 5){

                                                echo ', ';

                                            }									

                                            echo 'PIN - '.$corporate_details->pin_code;

                                            $fg = 3;

                                        }

                    if(isset($corporate_details->country) && $corporate_details->country != '' && $corporate_details->country != ' '){

                                            if($fg == 3)

                                                echo ', ';

                                            echo $corporate_details->country;

                                            /*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';

                                            $fg = 4;*/

                                        }

                    if($fg == 10){

                                            echo 'No Info';

                                        }

                ?>

					</li>

					<li>
						<?php echo '<strong>Phone: </strong>'.$corporate_details->g_contact_no; ?> </li>

					<li>
						<?php echo '<strong>Email: </strong>'.$corporate_details->corp_email; ?> </li>

					<li>
						<?php 
							
						echo '<strong>GSTIN: </strong> ';
						
						if(isset($corporate_details->gstin)){
							
							echo $corporate_details->gstin;
						} else {
							echo '<span style="color:#888; font-style: italic;">no info</span>';
						}

						?>
					</li>

					<li>&nbsp; &nbsp;</li>

					<li>

						<?php 

					echo '<strong style="font-size:15px; color:#6B67A1; text-transform: capitalize;">Guest Name: </strong>';
					
					if($gst->g_gender == 'male' || $gst->g_gender == 'Male'){
									 $sex='<label style="color:#0476F6"><i style"color:#0476F6;" class="fa fa-male" aria-hidden="true"></i></label>';
								}else if($gst->g_gender == 'female' || $gst->g_gender == 'Female'){
									$sex='<label style="color:#FF2A6A"><i style"color:#FF2A6A;" class="fa fa-female" aria-hidden="true"></i></label>';
								}else{
									$sex='';
								}
							
								if($gst->g_gender == 'Male' || $gst->g_gender == 'male')
									$salu = 'Mr. ';
								else if($gst->g_gender == 'Female' || $gst->g_gender == 'male'){
									if($gst->g_married == 'married')
										$salu = 'Mrs. ';
									else
										$salu = 'Ms. ';
								}
								else
									$salu = '';

							echo $salu.' '.$gst->g_name;

					if($gst->c_des)

						echo ' - '.$gst->c_des;

												

							?> (Group Owner) </li>

				</ul>

				<?php  } 

	 else{

	?>

				<ul class="list-unstyled">

					<li>
						<strong style="color:#6B67A1; font-size:15px; text-transform: capitalize;"> 

							<?php 

					if($gst->g_gender == 'male' || $gst->g_gender == 'Male'){
									 $sex='<label style="color:#0476F6"><i style"color:#0476F6;" class="fa fa-male" aria-hidden="true"></i></label>';
								}else if($gst->g_gender == 'female' || $gst->g_gender == 'Female'){
									$sex='<label style="color:#FF2A6A"><i style"color:#FF2A6A;" class="fa fa-female" aria-hidden="true"></i></label>';
								}else{
									$sex='';
								}
							
								if($gst->g_gender == 'Male' || $gst->g_gender == 'male')
									$salu = 'Mr. ';
								else if($gst->g_gender == 'Female' || $gst->g_gender == 'male'){
									if($gst->g_married == 'married')
										$salu = 'Mrs. ';
									else
										$salu = 'Ms. ';
								}
								else
									$salu = '';

							echo $salu.' '.$gst->g_name;		

							?>

						</strong>(Group Owner)</li>

					<li>

						<?php 

									

									// Logic for Guest Address

									$fg = 10;

									if(isset($gst->g_address) && ($gst->g_address != '' && $gst->g_address != ' ')){

										echo ($gst->g_address);

										$fg = 0	;

										//echo ', ';

									}

									if(isset($gst->g_city) && ($gst->g_city != '' && $gst->g_city != ' ')){

										if($fg == 0){

											echo ', ';

											

										}

											

										echo $gst->g_city;

										echo '<br>';

										$fg = 1;

									}

									else if($fg == 0)

										$fg = 5;

									if(isset($gst->g_state) && ($gst->g_state != '' && $gst->g_state != ' ')){

										if($fg == 5)

											echo '<br>';

										echo $gst->g_state;

										$fg = 2;

										//echo ', ';

									}

									if(isset($gst->g_pincode) && $gst->g_pincode!= NULL){

										if($fg == 2 || $fg == 5){

											echo ', ';

										}									

										echo 'PIN - '.$gst->g_pincode;

										$fg = 3;

									}

									if(isset($gst->g_country) && $gst->g_country != '' && $gst->g_country != ' '){

										if($fg == 3)

											echo ', ';

										echo $gst->g_country;

										/*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';

										$fg = 4;*/

									}

									if($fg == 10){

										echo 'No Info';

									}

								?>

					</li>

					<li><strong>Phone: </strong>

						<?php if($gst->g_contact_no) {echo $gst->g_contact_no;} else { echo '<span style="color:#AAAAAA;">No info</span>';} ?>

					</li>

					<li><strong>Email: </strong>

						<?php if($gst->g_email) {echo $gst->g_email;} else { echo '<span style="color:#AAAAAA;">No info</span>';} ?>

					</li>

					<li>
						<?php 
						
							echo '<strong>GSTIN: </strong> ';
							
							if(isset($gst->gstin)){
								echo $gst->gstin;
							} else {
								echo '<span style="color:#888; font-style: italic;">not applicable/ no info</span>';
							}			
						
						?>
					</li>

				</ul>

				<?php

				}







				}

				?>
				<ul class="list-unstyled">

					<li><strong>Group Id: </strong>
						<?php echo $key3->id; ?> </li>

					<li><strong>PAX: </strong> <?php echo $key3->number_guest; ?> | <strong>Rooms/ Units: </strong><?php echo $key3->number_room;?> (<?php
							if($details) { 
								$irmno = 0;
								foreach ($details as $key ) {
									if($irmno != 0)
										echo ', ';
									echo  $key->roomNO;
									$irmno++;
								}
							}
						?>) </li>

					<li><strong>Reservation Stay Dates: </strong>
						<?php echo date("l jS F Y",strtotime($key3->start_date)); ?> to <?php echo date("l jS F Y",strtotime($key3->end_date));?> </li>

					<?php if($booking_source == '1'){ ?>

					<li><strong>Booking Source: </strong>
						<?php echo $key3->booking_source; ?> 

					<?php } ?>

					<?php if($nature_visit == '1'){ ?>

					<?php if(isset($key3->nature_visit) && $key3->nature_visit != ""){

							$getNatureVisit = $this->dashboard_model->getNatureVisitById($key3->nature_visit);

					
								if(isset($getNatureVisit->booking_nature_visit_name) && $getNatureVisit->booking_nature_visit_name != "") { 

									echo '<strong> | Nature of visit: </strong>'.$getNatureVisit->booking_nature_visit_name; 

								

								} 

								else

									echo '<strong> | Nature of visit: </strong>No Info';

						 } 
						}
						
						$Mealdata = $this->dashboard_model->get_group_food_plan_by_grp_id($key3->id)->mealPlan; 

						if($Mealdata)	
							{

								echo "<strong> | Food Plan: </strong>".$Mealdata;

							}

						?>
					</li>

				</ul>
			</td>

			<td width="30%" align="left" valign="top">
				
				<?php if($company_details == '1'){ ?> GST Reg No.:

				<?php  echo $tax_details->service_tax_no;?>

				<br/> CIN No:

				<?php  echo $tax_details->cin_no; } else { ?>

				<?php } ?>
				<div class="well">
					<strong>
						<?php echo  $hotel_name->hotel_name; ?>
					</strong><br/>

					<?php echo  $hotel_contact['hotel_street1']; ?>
					<?php echo  $hotel_contact['hotel_street2'].', '; ?><br>

					<?php echo  $hotel_contact['hotel_district']; ?> -
					<?php echo  $hotel_contact['hotel_pincode']; ?>
					<?php echo  $hotel_contact['hotel_state']; ?> -
					<?php echo  $hotel_contact['hotel_country']; ?><br/>

					<strong>Contact No:</strong>
					<?php echo  '+91 '.$hotel_contact['land_phone_no'].'/ '.$hotel_contact['hotel_frontdesk_mobile']; ?><br/>

					<strong>Email:</strong>
					<?php echo  $hotel_contact['hotel_frontdesk_email']; ?><br/>
					
					<strong>Website:</strong>
					<?php echo  $hotel_contact['website']; ?><br/>

				</div>
				
			
			</td>
			
		</tr>

		<?php }}?>

	</table>

	<?php } ?>

	<span style="font-size:13px; font-weight:700; color:#F8681A; display:block; padding:5px 5px;">Booking Details</span>

	<hr style="background: #F8681A; border: none; height: 1px;">

	<table class="td-pad" width="100%">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th width="4%" align="center" valign="middle"> # </th>
				<th width="20%" align="left" valign="middle"> Product/ Service Name </th>
				<th width="8%" align="center" valign="middle"> HSN/ SAC </th>
				<th width="12%" align="left" valign="middle"> Stay Dates </th>
				<th width="4%" align="center" valign="middle"> Adlt </th>
				<th width="4%" align="center" valign="middle"> Kid </th>
				<th align="center" valign="middle"> Qty </th>
				<th align="center" valign="middle"> Rate </th>
				<th align="center" valign="middle"> Total </th>
				<?php if($service_tax == '1'){ ?>
				<th align="center" valign="middle"> SGST </th>
				<th align="center" valign="middle"> CGST </th>
				<th align="center" valign="middle"> IGST </th>
				<?php } ?>
				<th width="7% align="right" valign="middle"> S Total </th>

			</tr>

		</thead>

		<tbody style="background: #FAFAFA">

			<?php 

	$group=$this->dashboard_model->get_bookingLineItem_group_id($_GET['group_id']); // Geting The Unit Type

		//	echo '<pre>';

		//	print_r($details); //exit;

		$count_r = 0;
		$totalBkAm = 0;
		$totalMpAm = 0;
		$total=0; 

	if($details){ 
	
		$adltS = 0;
		$kidS = 0;
		$dayS = 0;
		$rateS = 0;
		$qtyS = 0;
		$totalS = 0;
		$gtotalS = 0;
		$cgstS = 0;
		$sgstS = 0;
		$igstS = 0;
		$utgstS = 0;
	
	foreach ($details as $index => $key ) { 
	
		$totalBkAmR = 0;
		$totalMpAmR = 0;
		
		
	?>



			<tr>

				<td align="center" valign="middle">
					<?php echo $details3[$index]->booking_id; ?>
				</td>

				<td align="left" valign="middle">

				<?php // Room Details

					$count_r++;

					$unit=$this->dashboard_model->get_unit_exact_by_unitno($key->roomNO); // Geting the Unit ID based on Room No

					$units=$this->dashboard_model->get_unit_exact($unit->unit_id); // Geting The Unit Type

					

					if(isset($units) && $units)

						$unit_name=$units->unit_name;


					if($unit_cat == '1'){

						echo $unit_name.' - '.$key->roomNO;

					} else {

						if($unit_no != '1')

							echo 'Unit '.$count_r;

					}

					if($unit_no == '1'){

						echo '</br><span style="text-transform: capitalize;"> ('.$key->gestName.')</span>';

					}
					
					

				?>

				</td>
				
				<td align="center" valign="middle">

					<?php 

						echo '996311';

					?>

				</td>
				
				<!--Room No-->

				<td align="lrft" valign="middle" style="font-size:10px">

					<?php 

						echo date("d-m-Y",strtotime($key->startDate)); 

						echo ' - ';

						echo date("d-m-Y",strtotime($key->endDate));

					?>

				</td>
				
				

				<td align="center" valign="middle">

					<?php 

						echo $key->adultNO; 
						$adltS = $adltS + $key->adultNO;

					?>

				</td>

				<td align="center" valign="middle">

					<?php 

						echo $key->childNO; 
						$kidS = $kidS + $key->childNO;
					?>

				</td>
				
				<td align="center" valign="middle">

					<?php 

						echo $key->days;
						$qtyS = $qtyS + $key->days;
						
						if($key->days > 1)
							echo '<span style="font-size:10px"> Days</span>';
						else if($key->days == 1)
							echo '<span style="font-size:10px"> Day</span>';

					?>

				</td>

				<td align="center" valign="middle">
					<?php 

					//total room rent

						echo $key->trr;
						$rateS = $rateS + $key->trr;
						$totalBkAmR = $totalBkAmR + $key->trr;

					?>
				</td>
				
				<td align="center" valign="middle">
					<?php 

					//total room rent

						echo $key->trr*$key->days;
						$totalS = $totalS + $key->trr*$key->days;

					?>
				</td>
				
				<?php 

						if($details3){ 
						
							$get_line_items = $this->bookings_model->line_charge_item_tax('sb',$details3[$index]->booking_id);
							
							foreach ( $get_line_items['rr_tax'] as $keyli => $valueli ) {

								if($keyli == 'CGST')
									$cgstL = $valueli;
								if($keyli == 'SGST')
									$sgstL = $valueli;
								if($keyli == 'IGST')
									$igstL = $valueli;								
							}
					
						}

				?>
				
				<td align="center" valign="middle">
					<?php 
						echo number_format(($sgstL), 2, '.','').'</br><span style="font-size:10px">@ '.round(($sgstL/($key->trr*$key->days)*100),2).'%</span>';
						$sgstS = $sgstS + number_format(($sgstL), 2, '.','');
					?>
				</td>
				
				<td align="center" valign="middle">
					<?php 

						echo number_format(($cgstL), 2, '.','').'</br><span style="font-size:10px">@ '.round(($cgstL/($key->trr*$key->days)*100),2).'%</span>';
						$cgstS = $cgstS + number_format(($cgstL), 2, '.','');					

					?>
				</td>
				
				
				<td align="center" valign="middle">
					<?php 

						echo number_format(($igstL), 2, '.','').'</br><span style="font-size:10px">@ '.round(($igstL/($key->trr*$key->days)*100),2).'%</span>'; 
						$igstS = $igstS + number_format(($igstL), 2, '.','');
						$totalBkAmR = $totalBkAmR + $key->room_st;

					?>
				</td>

				<td align="right" valign="middle">
					<?php 

						echo '<strong>'.number_format($totalBkAmR*$key->days, 2).'</strong>'; 
						
						$totalBkAm = $key->price + $totalBkAm;

					?>
				</td>

			</tr>

			<!-- Meal Plan -->
			
			<tr>
			
				<td>
					
				</td>
				
				<td>
					<?php
						echo "Meal/ Food Plan: ".$Mealdata;
					?>
				</td>
				
				<td align="center" valign="middle">
					<?php 

						echo '9963';

					?>
				</td>
				
				<td>
					-
				</td>
				
				
				
				<td align="center" valign="middle">

					<?php 

						echo $key->adultNO; 

					?>

				</td>

				<td align="center" valign="middle">

					<?php 

						echo $key->childNO; 

					?>

				</td>
				
				<td align="center" valign="middle">

					<?php 

						echo $key->days;
						
						if($key->days > 1)
							echo '<span style="font-size:10px"> Days</span>';
						else if($key->days == 1)
							echo '<span style="font-size:10px"> Day</span>';

					?>

				</td>
				
				<?php if($fd_plan == '1'){ ?>

				<td align="center" valign="middle">
					<?php 

						echo $key->tfi;
						$totalMpAmR = $totalMpAmR + $key->tfi;
						$rateS = $rateS + $key->tfi;

					?>
				</td>

				<td align="center" valign="middle">
					<?php 

						echo $key->tfi*$key->days;
						$totalS = $totalS + $key->tfi*$key->days;

					?>
				</td>
				
				<?php 

						if($details3){ 
							
							$cgstLm = 0;
							$sgstLm = 0;
							$igstLm = 0;
						
							foreach ( $get_line_items['mp_tax'] as $keylim => $valuelim ) {

								if($keylim == 'CGST')
									$cgstLm = $valuelim;
								if($keylim == 'SGST')
									$sgstLm = $valuelim;
								if($keylim == 'IGST')
									$igstLm = $valuelim;								
							}
					
						}

				?>

				<td align="center" valign="middle">
					<?php 

						echo number_format(($sgstLm), 2, '.','').'</br><span style="font-size:10px">@ '.($sgstLm/($key->tfi*$key->days)*100).'%</span>';
						$sgstS = $sgstS + number_format(($sgstLm), 2, '.','');


					?>
				</td>
				
				<td align="center" valign="middle">
					<?php 

						echo number_format(($cgstLm), 2, '.','').'</br><span style="font-size:10px">@ '.($cgstLm/($key->tfi*$key->days)*100).'%</span>'; 
						$cgstS = $cgstS + number_format(($cgstLm), 2, '.','');


					?>
				</td>
				
				<td align="center" valign="middle">
					<?php 

						echo number_format(($igstLm), 2, '.','').'</br><span style="font-size:10px">@ '.($igstLm/($key->tfi*$key->days)*100).'%</span>'; 
						$igstS = $igstS + number_format(($igstLm), 2, '.','');
						$totalMpAmR = $totalMpAmR + $key->food_vat;

					?>
				</td>
				
				<?php } ?>
				
				<td align="right" valign="middle">
					<?php 
						echo '<strong>'.number_format($totalMpAmR*$key->days,2).'</strong>'; 
					?>

				</td>
			</tr>
			
	<?php $total = $total + $key->price+ $key->service; } } ?>
	<hr style="background: #EDEDED; border: none; height: 1px;">
	
			<tr>		
				
				<td>
					<?php
						
					?>
				</td>
				<td>
					<?php
						
					?>
				</td>
				<td>
					<?php
						
					?>
				</td>
				<td>
					<?php
						
					?>
				</td>
				<td align="center" valign="middle" style="font-weight:800">
					<?php
						echo $adltS;
					?>
				</td>
				<td align="center" valign="middle" style="font-weight:800">
					<?php
						echo $kidS;
					?>
				</td>
				<td align="center" valign="middle" style="font-weight:800">
					<?php
						echo $qtyS;
					?>
				</td>
				<td align="center" valign="middle" style="font-weight:800">
					<?php
						echo $rateS;
					?>
				</td>
				<td align="center" valign="middle" style="font-weight:800">
					<?php
						echo $totalS;
					?>
				</td>
				<td align="center" valign="middle" style="font-weight:800">
					<?php
						echo $sgstS;
					?>
				</td>
				<td align="center" valign="middle" style="font-weight:800">
					<?php
						echo $cgstS;
					?>
				</td>
				<td align="center" valign="middle" style="font-weight:800">
					<?php
						echo $igstS;
					?>
				</td>
				
				<td align="right" valign="middle" style="font-weight:800">
					<?php 
						echo number_format($totalBkAm,2);
					?>
				</td>
			</tr>
	
	</tbody>
	</table>
	
	

	<?php

	foreach ( $details2 as $groups ) {

		# code...

		$charge_string = $groups->charges_id;



		$charge_total_price = $groups->charges_cost;

	}


	$charge_sum = 0;

	if ( $charge_string != "" ) {

	?>

	<span style="font-size:13px; font-weight:700; padding:5px 5px; display:block; color:#366B9B;">Extra Charge Details</span>

	<hr style="background: #366B9B; border: none; height: 1px;">

	<table class="td-pad" width="100%">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th width="4%" align="center" valign="middle"> No </th>
				<th align="center" width="8%"> Date </th>
				<th width="15%" align="left" valign="middle"> Product / Service Name </th>
				<th width="8%" align="left" valign="middle"> HSN/ SAC </th>
				<th width="5%" align="center" valign="middle"> Qty </th>

				<th width="5%" align="center" valign="middle"> Rate </th>

				<th width="8%" align="center" valign="middle"> Amount </th>
				
				<th width="5%" align="center"> SGST </th>				
				<th width="5%" align="center"> CGST </th>				
				<th width="5%" align="center"> IGST </th>				
			<!--	<th width="5%" align="center"> UTGST </th>	-->

				<th width="5%" align="center" valign="middle"> Tax </th>
				
				<th width="5%" align="center" valign="middle"> Disc </th>

				<th width="6%" align="right" valign="middle"> Total </th>

			</tr>

		</thead>

		<tbody style="background: #FAFAFA">

			<?php 



                    $charge_array=explode(",", $charge_string);



                    //print_r($service_array); 



                    for ($i=0; $i < sizeof($charge_array) ; $i++) { 

                        # code...



                        $charges=$this->dashboard_model->get_charge_details($charge_array[$i]);

                  

                        # code...

               if($charges && isset($charges)){   



               ?>

			<tr>

				<td align="center" valign="middle">
					<?php echo $charges->crg_id; ?>
				</td>
				<td align="center" valign="middle">
					<?php echo  date("g:ia dS M Y",strtotime($charges->aded_on));  ?>
				</td>
				<td align="left" valign="middle">
					<?php if(isset($charges->crg_description)){ echo $charges->crg_description; }?>
				</td>
				<td align="center" valign="middle">
					<?php if(isset($charges->hsn_sac)){ echo $charges->hsn_sac; }?>
				</td>

				<td align="center" valign="middle">
					<?php echo $charges->crg_quantity; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $charges->crg_unit_price; ?>
				</td>

				<td align="center" valign="middle">
					<?php $charge_total= ($charges->crg_unit_price * $charges->crg_quantity); echo number_format($charge_total, 2, '.',''); ?>
				</td>
				
				<td align="center">

					<?php echo $charges->cgst*$charge_total/100; ?>

				</td>
				
				<td align="center">

					<?php echo $charges->sgst*$charge_total/100; ?>

				</td>
				
				<td align="center">

					<?php echo $charges->igst*$charge_total/100; ?>

				</td>
				
			
				
				<td align="center">

					<?php echo $charges->crg_tax*$charge_total/100; ?>

				</td>
				
				<td align="center" valign="middle">
					<?php echo '0.00'; ?>
				</td>

				<td align="right" valign="middle">
					<?php echo $charge_grand_total=$charges->crg_total; ?>
				</td>

			</tr>

			<?php  $charge_sum=$charge_sum+$charge_grand_total;  } }?>
			<?php 
			$master=$this->unit_class_model->get_laundry_master($bid,'2');
			
			$totalLaundry=0;
			$discountLaundry=0;
			$i=0;
			  if(isset($master) && $master){
				 foreach($master as $mas){
						$i++;
			$totalLaundry=$totalLaundry+$mas->grand_total;
			$discountLaundry=$discountLaundry+$mas->discount;
			
			$line_item_info=$this->unit_class_model->get_line_item_info($mas->laundry_id);
			?>
			<tr>
			    
			    <td align="center"><?php echo $mas->laundry_id ?></td>
			     <td align="center"><?php echo date("g:ia dS M Y",strtotime($mas->dateAdded));  ?></td>
			     <td align="center"><?php echo  $line_item_info->qty; ?></td>
			      <td align="center"><?php echo $mas->total_item ?></td>
				  <td align="center"><?php echo 'N/A'; ?></td>
			       <td align="center"><?php echo $mas->grand_total+$mas->discount ?></td>
			          <td align="center"><?php echo '0.00%'; ?></td>
					  <td align="center"><?php echo $mas->discount ?></td>
			         
			            <td align="right"><?php echo $mas->grand_total; ?></td>
			    
			</tr>
			
			<tr>

				<td colspan="7">
					<hr style="background: #EEEEEE; border: none; height: 1px; ">
				</td>

			</tr>
            <?php
$charge_sum+=$mas->grand_total;
			}} ?> 
		</tbody>

	</table>

	<hr style="background: #EDEDED; border: none; height: 1px;">

	<?php } ?>

	<!-- pos start --->

	<?php $sum_pos=0; 

	$total_bill_amount = 0 ;

$total_tax_amount = 0 ;



if($pos && isset($pos)){  

?>

	<span style="font-size:13px; font-weight:700; color:#5AA650; padding:5px 5px; display:block;"> POS Details </span>

	<hr style="background: #5AA650; border: none; height: 1px;">

	<table width="100%" class="td-pad">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th width="5%" align="center" valign="middle"> Bill No </th>

				<th width="10%" align="center" valign="middle"> Date </th>
<?php 	if ($chk->fd_app==1 ) {?>
				<th width="30%" align="center" valign="middle"> Food Items </th>
<?php }?>
				<th  align="center" valign="middle"> Amount </th>

				<th  align="center" valign="middle"> CGST </th>

				<th align="center" valign="middle"> SGST </th>

				<th  align="center" valign="middle"> IGST </th>

				<th  align="center" valign="middle"> Discount </th>

				<th  align="center" valign="middle"> Total </th>

				<th  align="right" valign="middle"> Due </th>

			</tr>

		</thead>

		<tbody style="background: #fafafa">

			<?php $sum=0; $sum1=0; 	$posGAMTT=0; 

$posGAMT=0; 

$pos_taxt = 0;
$u_amt = 0;
$d = 0;
$sum_sgst = 0;
$sum_igst = 0;
$sum_cgst = 0;
$pos_item_show = 0;
$paid_pos = 0;
foreach ($pos as $key) {
						
						$pos_tax = 0;
						
			if($chk->paid_pos_item_show ==1)	{	

			 if($key->total_due >= 1){
			     
			     	$pos_sgst = $key->food_sgst + $key->liquor_sgst; 
						$pos_tax = $pos_tax + $pos_sgst;
						
						$pos_cgst = $key->food_cgst + $key->liquor_cgst; 
						$pos_tax = $pos_tax + $pos_cgst;
						
						$pos_igst = $key->food_igst + $key->liquor_igst; 
						$pos_tax = $pos_tax + $pos_igst;
						
						$pos_taxt = $pos_tax + $pos_taxt;
						$sum_sgst=$sum_sgst+$pos_sgst;
						$sum_cgst=$sum_cgst+$pos_cgst;
						$sum_igst=$sum_igst+$pos_igst;
						

						$u_amt=$key->total_amount-$pos_tax+$d; 

			 ?>

			<tr>

				<td align="center" valign="middle">
					<?php echo $key->invoice_number; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $key->date; ?>
				</td>
<?php if ($chk->fd_app==1) {?>
				<td align="center" valign="middle" class="hidden-480">
					<?php // Food Items

						$items=$this->dashboard_model->all_pos_items($key->pos_id);

						foreach ($items as $item ) {

							echo $item->item_name."(".$item->item_quantity.") ";

							echo "</br>";

						}

					?>
				</td>
<?php }?>
				<td align="center" valign="middle" class="hidden-480">
					<?php // Amount

					$amt=$key->total_amount-$pos_tax+$key->discount;

					echo number_format($amt, 2, '.','');

					?>
				</td>

				<td align="center" valign="middle" class="hidden-480">
					
					<?php  // sgst

					 

					 echo number_format($pos_sgst, 2, '.','');

					?>

				</td>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php 

					 

					 echo number_format($pos_cgst, 2, '.','');

					?>

				</td>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php 

					 echo number_format($pos_igst, 2, '.','');

					?>

				</td>

				<td align="center" valign="middle" class="hidden-480">
					<?php echo $key->discount; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $key->total_amount; ?>
				</td>

				<td align="right" valign="middle">
					<?php echo $key->total_due; 
					if($key->total_paid_pos == $key->total_amount){
					    
					    $paid_pos = $paid_pos + $key->total_paid_pos;
					}
					
					?>
				</td>

			</tr>
				

			<?php 
			
				 $sum=$sum+$key->total_due;

	    $sum1=$sum1+$key->total_amount;  

	    $posGAMTT=$key->total_amount- $key->total_paid; 

	    $posGAMT=$posGAMT+$posGAMTT;
    
		$total_bill_amount = $total_bill_amount + $key->bill_amount ;

		$total_tax_amount = $total_tax_amount + $key->tax ;	
			 }
			 
			 
			}else{
			    
			     	$pos_sgst = $key->food_sgst + $key->liquor_sgst; 
						$pos_tax = $pos_tax + $pos_sgst;
						
						$pos_cgst = $key->food_cgst + $key->liquor_cgst; 
						$pos_tax = $pos_tax + $pos_cgst;
						
						$pos_igst = $key->food_igst + $key->liquor_igst; 
						$pos_tax = $pos_tax + $pos_igst;
						
						$pos_taxt = $pos_tax + $pos_taxt;
						$sum_sgst=$sum_sgst+$pos_sgst;
						$sum_cgst=$sum_cgst+$pos_cgst;
						$sum_igst=$sum_igst+$pos_igst;
						

						$u_amt=$key->total_amount-$pos_tax+$d; 

			 ?>

			<tr>

				<td align="center" valign="middle">
					<?php echo $key->invoice_number; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $key->date; ?>
				</td>
<?php if ($chk->fd_app==1) {?>
				<td align="center" valign="middle" class="hidden-480">
					<?php // Food Items

						$items=$this->dashboard_model->all_pos_items($key->pos_id);

						foreach ($items as $item ) {

							echo $item->item_name."(".$item->item_quantity.") ";

							echo "</br>";

						}

					?>
				</td>
<?php }?>
				<td align="center" valign="middle" class="hidden-480">
					<?php // Amount

					$amt=$key->total_amount-$pos_tax+$key->discount;

					echo number_format($amt, 2, '.','');

					?>
				</td>

				<td align="center" valign="middle" class="hidden-480">
					
					<?php  // sgst

					 

					 echo number_format($pos_sgst, 2, '.','');

					?>

				</td>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php 

					 

					 echo number_format($pos_cgst, 2, '.','');

					?>

				</td>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php 

					 echo number_format($pos_igst, 2, '.','');

					?>

				</td>

				<td align="center" valign="middle" class="hidden-480">
					<?php echo $key->discount; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $key->total_amount; ?>
				</td>

				<td align="right" valign="middle">
					<?php echo $key->total_due; 
					if($key->total_paid_pos == $key->total_amount){
					    
					    $paid_pos = $paid_pos + $key->total_paid_pos;
					}
					
					?>
				</td>

			</tr>
				

			<?php 
			 
		 $sum=$sum+$key->total_due;

	    $sum1=$sum1+$key->total_amount;  

	    $posGAMTT=$key->total_amount- $key->total_paid; 

	    $posGAMT=$posGAMT+$posGAMTT;
    
		$total_bill_amount = $total_bill_amount + $key->bill_amount ;

		$total_tax_amount = $total_tax_amount + $key->tax ;	
			    
			}
			
		
//echo "total_bill_amount".$total_bill_amount;

	   } 

?>
</tbody>

	</table>
<!--	<table class="td-pad">
	    <tbody>
			<tr>

				<td colspan="2">
					<hr style="background: #EEEEEE; border: none; height: 1px; ">
				</td>

			</tr>

			<tr>

				<td  style="text-align:right; width:75%">
					Total Food Amount:
				</td>

				<td style="text-align:right">
					<?php //echo number_format($total_bill_amount - $pos_taxt, 2, '.','');?> 
				</td>

			</tr>

			
			
			<tr>

				<td  style="text-align:right; ">Total POS Tax:
				</td>

				<td  style="text-align:right">
						<?php// echo $pos_taxt ?>				
				</td>

			</tr>

			<tr>

				<td  style="text-align:right; "><strong>Total POS Amount:</strong>
				</td>

				<td  style="text-align:right">
					<strong>

					<?php 
					//	echo number_format(($total_bill_amount+$total_tax_amount), 2, '.','');						
					?>

					</strong>
				</td>
</tr>


		</tbody>

	</table>-->

	<hr style="background: #EEEEEE; border: none; height: 1px;">

	<?php } ?>

	<!-- pos end --->

	<!--charge end-->
	
	
	<?php if(isset($all_adjst) && $all_adjst){

	?>

	<span style="font-size:13px; font-weight:700; color:#D75C58; padding:5px 5px; display:block;"> Adjustment Details </span>

	<hr style="background: #D75C58; border: none; height: 1px;">

	<table width="100%" class="td-pad">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th> # </th>

				<th> Date </th>

				<th> Reason </th>

				<th> Amount</th>

				<th> Note </th>

			</tr>

		</thead>

		<tbody style="background: #fafafa">

			<?php $i=1;

     $m=0;

	

		//print_r($all_adjustment);

           foreach($all_adjst as $adjst){

			  ++$m; 

			 ?>

			<tr>

				<td style="text-align:left">
					<?php  echo $i; ?>
				</td>

				<td style="text-align:center">
					<?php echo date("g:ia dS M Y",strtotime($adjst->addedOn)); ?>
				</td>

				<td style="text-align:center">

					<?php echo $adjst->reason ;?>

				</td>

				<td style="text-align:center">
					<?php echo $adjst->amount; ?>
				</td>

				<td style="text-align:center">
					<?php echo $adjst->note; ?>
				</td>

			</tr>

			<tr>

				<td colspan="7">
					<hr style="background: #EEEEEE; border: none; height: 1px; ">
				</td>

			</tr>

			<?php $i++; }?>

		</tbody>

	</table>





	<?php }?>









	<?php   if($transaction){ ?>



	<span style="font-size:13px; font-weight:700; color:#D75C58; padding:5px 5px; display:block;"> Payment Details </span>

	<hr style="background: #D75C58; border: none; height: 1px;">

	<table width="100%" class="td-pad">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th width="5%" align="left"> # </th>

				<th width="5%" align="left"> Status </th>
				
				<th width="10%"> Bk Status </th>

				<th width="20%"> Transaction Date </th>

				<th width="10%"> Profit Center </th>

				<th width="15%"> Payment Mode </th>

				<th width="20%" align="center"> Transaction Details </th>

				<th width="10%" align="right" style="padding-right:8px;"> Amount </th>

			</tr>

		</thead>



		<tbody style="background: #fafafa">

			<?php   

			  $i = 0;

			  $paid=0;

			  foreach ($transaction as $keyt ) {

				  $paid=$paid+$keyt->t_amount;

			  $i++;

			  ?>

			<tr>

				<td>
					<?php echo $i;?>
				</td>

				<td align="center" valign="middle">
					<?php 

						if($keyt->t_status == 'Cancel')

							echo '<span style="color:red;">Declined</span>';

						else if($keyt->t_status == 'Pending')

							echo '<span style="color:orange;">Processing</span>';

						else

							echo '<span style="color:green;">Recieved</span>';

					?>
				</td>
				<td style="text-align:center">
					<?php 
							
							if(isset($keyt->t_booking_status) && $keyt->t_booking_status!=NULL){
							if($keyt->t_booking_status == 1){
								echo "Temporary Hold";
							}
							if($keyt->t_booking_status == 2){
								echo "Advance";
							}
							if($keyt->t_booking_status == 3){
								echo "Pending";
							}
							if($keyt->t_booking_status == 4){
								echo "Confirmed";
							}
							if($keyt->t_booking_status == 5){
								echo "Checked In";
							}
							if($keyt->t_booking_status == 6){
								echo "Checked Out";
							}
							if($keyt->t_booking_status == 7){
								echo "Cancelled";
							}
							if($keyt->t_booking_status == 8){
								echo "Incomplete";
							}
							}
					
					?>
				</td>

				<td align="center" valign="middle">
					<?php echo date("g:ia dS M Y",strtotime($keyt->t_date)); ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $keyt->p_center; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $keyt->t_payment_mode; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $keyt->t_bank_name; ?>
				</td>

				<td align="right" valign="middle">
					<?php echo $keyt->t_amount; ?>
				</td>

			</tr>

			<?php  } ?>

			<tr>

				<td colspan="8">
					<hr style="background: #EEEEEE; border: none; height: 1px; ">
				</td>

			</tr>

			<tr>

				<td colspan="7" style="text-align:right; padding-right:16%;"><strong>Total Paid Amount:</strong>
				</td>

				<td align="right" valign="middle">
					<strong>

						<?php 

							$amountPaid = $this->dashboard_model->get_amountPaidByGroupID($_GET['group_id']);

							if(isset($amountPaid) && $amountPaid) {

								$paid=$amountPaid->tm;

								echo $paid;

							} else {

								$paid=0;

								echo "Yet to pay";

							}

						?>

					</strong>
				</td>

			</tr>

		</tbody>

		<?php  } ?>

	</table>

	<table class="td-pad td-fon-size" width="100%">

		<tr>

			<td align="right" width="77%" valign="middle">Room Rent Charge:</td>

			<td align="right" valign="middle">
				<?php 

				//echo number_format(($total), 2, '.','');

					echo number_format(($line_items['all_price']['room_rent']), 2, '.','');

				?>
			</td>

		</tr>

			<?php

			//echo '<pre>';

			//print_r($line_items);

			unset( $line_items[ 'rr_tax' ][ 'r_id' ] );

			unset( $line_items[ 'rr_tax' ][ 'planName' ] );

			unset( $line_items[ 'rr_tax' ][ 'total' ] );

			foreach ( $line_items[ 'rr_tax' ] as $key => $value ) {

			?>

		<tr>

			<td align="right" valign="middle">Room Rent
				<?php echo $key;?>:
			</td>

			<td align="right" valign="middle">
				<?php 

					echo number_format(($value), 2, '.','');

				?>
			</td>

		</tr>

		<?php } 
		
		if($fd_plan == '1'){
		?>

		<tr>

			<td align="right" valign="middle"> Meal Plan Charge:</td>

			<td align="right" valign="middle">
				<?php 

			echo number_format(($line_items['all_price']['meal_plan_chrg']), 2, '.','');

			?>
			</td>

		</tr>

		<?php

			//echo '<pre>';

			//print_r($line_items);

			unset( $line_items[ 'mp_tax' ][ 'total' ] );

			unset( $line_items[ 'mp_tax' ][ 'r_id' ] );

			unset( $line_items[ 'mp_tax' ][ 'planName' ] );

			foreach ( $line_items[ 'mp_tax' ] as $key => $value ) {



		?>

		<tr>

			<td align="right" valign="middle">Meal Plan
				<?php echo $key?>:</td>

			<td align="right" valign="middle">
				<?php 

					echo number_format(($value), 2, '.','');

				?>
			</td>

		</tr>

			<?php }
		} 
		
		if($extra_charge==1 && $line_items['all_price']['exrr_chrg'] != 0){ ?>

		<tr>

			<td align="right" valign="middle">Ex Chrage Amount:</td>

			<td align="right" valign="middle">
			<?php 

				//echo number_format(($charge_sum), 2, '.','');

				echo number_format(($line_items['all_price']['exrr_chrg']), 2, '.','');

			?>
			</td>

		</tr>

		<?php }
		
		if($pos_settings==1 && $total_bill_amount != 0) {?>
		<tr>

			<td align="right" valign="middle">POS Amount:</td>

			<td align="right" valign="middle">
				<?php 
				
				$bill_without_tax = $total_bill_amount - ($sum_sgst + $sum_cgst + $sum_igst);
				echo number_format($bill_without_tax, 2, '.','');?>
			</td>

		</tr>
	  <?php }
		if ($chk->pos_seg==1 ) { 
	  
			if($sum_sgst != 0) {
	  ?> 
  
        <tr>
			<td align="right" valign="middle">POS SGST:</td>
			<td align="right" valign="middle">

				<?php echo number_format($sum_sgst,2); ?>
				
			</td>
		</tr>
		
		<?php } 
		if($sum_cgst != 0){ ?>
		
		<tr>
			<td align="right" valign="middle">POS CGST:</td>
			<td align="right" valign="middle">

				<?php echo number_format($sum_cgst,2); ?>
				
			</td>
		</tr>
		<?php } 
		if($sum_igst != 0){ ?>
		<tr>
			<td align="right" valign="middle">POS IGST:</td>
			<td align="right" valign="middle">

				<?php echo number_format($sum_igst,2); ?>
				
			</td>
		</tr>
	<?php 
		}
		if($paid_pos != 0){ ?>
		<tr>
			<td align="right" valign="middle"><strong>Paid From POS :</strong></td>
			<td align="right" valign="middle">

				<?php echo number_format($paid_pos,2); ?>
				
			</td>
		</tr>
		<?php
		} 
		}
		
	if($charge_sum!=0) { 
	?>
		<tr>

			<td align="right" valign="middle">Extra charge Amount:</td>

			<td align="right" valign="middle">
				<?php 
					if($charge_sum!=0){ 
						echo number_format($charge_sum, 2, '.','');
					} else { 
						echo number_format($charge_sum = 0, 2, '.',''); 
					}
				?>
			</td>

		</tr>
	<?php } ?>
		<tr>

			<td align="right" valign="middle"><strong>Total Amount:</strong>
			</td>

			<td align="right" valign="middle">
				<strong>

					<?php 

						if(isset($posGAMT) && $posGAMT !=""){
							$grand_total_amount= $total+$charge_sum+$posGAMT;
						} 
						else {
							$grand_total_amount= $total+$charge_sum; 
						}

						echo '<i class="fa fa-inr" style="font-size:12px; font-weight:normal;"></i> '.number_format($grand_total_amount, 2);	

					?>

				</strong>
			</td>

		</tr>
		

		<?php 

				$d=0;

				$discount_details=$this->unit_class_model->all_discount_group_details($_GET['group_id']);

				$lo=0;

				if(isset($discount_details)){

                foreach($discount_details as $discount){                	

                	$discount_amount=$discount->discount_amount;

					$lo+=$discount_amount;

					$glo1=$lo;

					++$d;

				}

				

				}

		if(isset($m) && $m){

			//$sum=0;

		}else{

			$m=0;

		}

		if($disc_app==1) {?>
		<tr>

			<td align="right" valign="middle">Discount:</td>

			<td align="right" valign="middle">
				<?php echo number_format($glo1,2);?>
				<?php echo " (". $d.")" ?>
			</td>

		</tr>
		<?php  } if($adjust_app==1 && $adjstAmt != 0) {?>
		<tr>

			<td align="right" valign="middle">Adjustment amount:</td>

			<td align="right" valign="middle">
				<?php 
											   
						if(isset($adjstAmt))
							echo number_format($adjstAmt,2); 
						else
							echo '0'; ?>
				<?php echo " (". $m.")" ?>
			</td>

		</tr>
		<?php }?>
		

		<tr>

			<td align="right"><strong style="font-size:13px; font-weight:600; color:#8876A8;">Total Payable Amount (Grand Total):</strong></td>

			<td align="right" valign="middle" style="font-size:13px; font-weight:700; color:#33A076;"><strong>
				<?php $payAmount = $grand_total_amount-$glo1+$adjstAmt; 

				echo '<i class="fa fa-inr" style="font-size:12px; font-weight:normal;"></i> '.number_format($payAmount, 2); ?>
			</strong></td>

		</tr>
		
		<tr>
			<td></td>
			<td align="right">
				<em style="text-transform: capitalize;"><?php 
				$net_payWords = $this->dashboard_model->getIndianCurrency($payAmount);
				if(isset($net_payWords)){
					echo ' </br>'.$net_payWords.' only';
				}
				 ?> </em>
			</td>
		</tr>
		
		<tr>

			<td align="right">Total Paid Amount:</td>

			<td align="right" valign="middle">
				<?php //$payAmount = $grand_total_amount; 

			 if(isset($paid) && $paid) {

				 echo number_format($paid, 2);

			 } else {

				  $paid=0;

				  echo 'none';

			 }

			  ?>
			</td>

		</tr>

		<tr>

			<td align="right" valign="middle"><span style="font-size:13px; font-weight:700; color:#8876A8;">Total Due:</span>
			</td>

			<td align="right" valign="middle">
				<?php 

					$td = $payAmount-$paid;

					$paid_c = number_format($td, 2);

					if($paid_c != 0){ 

				?>

				<span style="font-size:13px; font-weight:700; color:#F65656;"><i class="fa fa-inr" style="font-size:12px; font-weight:normal;"></i> <?php echo number_format($td,2);?></span>

				<?php

				} else {
				
				?>

				<span style="font-size:13px; font-weight:700; color:#7FBA67;">Full Paid</span>

				<?php } ?>
			</td>

		</tr>

	</table>

	<table width="100%">

		<tr>

			<td valign="bottom">

				<span style="font-size:9px; font-weight:400; color:#939292; display:block;">996311 - ACCOMODATION IN HOTEL/INN/GUEST HOUSE/ CLUB OR CAMP SITE ETC.SERVICE</span>
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;">996411 - TRANSPORT</span>
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;">996411 - PROGRAM,BUSINESS AUXILIARY SERVICE, SPA, DECORATION</span>
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;">9963 - RESTAURANT SERVICE</span>
			</td>

			<td align="center" width="35%">______________________________<br/> Authorized Signature </td>

		</tr>

		<tr>

			<td width="65%">
            <?php if($chk->declaration!='') {?>
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;"><?php echo $chk->declaration;?></span>
            <?php }?>
            </td>    
			<td align="center" width="35%">
				<?php   // Showing the Hotel Name from Session

					$hotel_n = $this->dashboard_model->get_hotel_name($this->session->userdata('user_hotel'));

					if(isset($hotel_n->hotel_name) && $hotel_n->hotel_name)

						echo 'For, '.$hotel_n->hotel_name;

				?>
			</td>

		</tr>

	</table>

	<hr style="background: #EEEEEE; border: none; height: 1px; margin-top:10px; width:100%;">

	<table width="100%" class="td-pad">
		<tr>
			<td align="center"><?php if($chk->ft_text!=""){ echo $chk->ft_text; }?></td>
			<td align="center">This is a Computer Generated Invoice & should be treated as signed by an authorized signatory</td>
		</tr>
	</table>
	<hr style="background: #00C5CD; border: none; height: 1px; margin-top:10px; width:100%;">
	<span style="font-size:9px; font-weight:400; color:#939292; display:block; text-align: center;"> Powered by BookMania. Thanks for your stay & kindly visit <?php echo $hotel_n->hotel_name; ?> again! </span>

</div>

<script>

	window.print();

</script>