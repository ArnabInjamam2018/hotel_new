<link href="<?php echo base_url();?>assets/layouts/layout/css/invoice-style.css" rel="stylesheet" type="text/css" />
<?php if($printer->printer_settings=='normal'){
	$print='';
}else{
	$print='fon-big';
} ?>
<div style="padding:10px 35px;" class="<?php echo $print ?> ">
	<?php 
	
	ini_set('memory_limit', '64M');
	$d=0;

  $charge_total=0;

$chk=$this->bookings_model->get_invoice_settings();

if(isset($chk->booking_source_inv_applicable)){

    $booking_source = $chk->booking_source_inv_applicable;

} else {

	$booking_source ="";

}

if(isset($chk->nature_visit_inv_applicable)){

    $nature_visit = $chk->nature_visit_inv_applicable;

} else {

	$nature_visit ="";

}

if(isset($chk->booking_note_inv_applicable)){

    $booking_note = $chk->booking_note_inv_applicable;

} else {

	$booking_note ="";

}

if(isset($chk->company_details_inv_applicable)){

    $company_details = $chk->company_details_inv_applicable;

} else {

	$company_details ="";

}

if(isset($chk->service_tax_applicable)){

    $service_tax = $chk->service_tax_applicable;

} else {

	$service_tax =0;

}

if(isset($chk->service_charg_applicable)){

    $service_charg = $chk->service_charg_applicable;

} else {

	$service_charg = 0;

}

if(isset($chk->luxury_tax_applicable)){

    $luxury_tax = $chk->luxury_tax_applicable;

} else {

	$luxury_tax =0;

}



if(isset($chk->invoice_pref) && isset($chk->invoice_suf)){

	$suf=$chk->invoice_suf;

    $pref=$chk->invoice_pref;

} else {

	$suf="";

    $pref="";	

}

	if ( isset( $chk->logo ) ) {

		$logo = $chk->logo;

	} else {

		$logo = 0;

	}
	
	if ( isset( $chk->adjust_app ) ) {

		$adjust_app = $chk->adjust_app;

	} else {

		$adjust_app = 0;

	}
	
	if ( isset( $chk->pos_app ) ) {

		$pos_settings = $chk->pos_app;

	} else {

		$pos_settings = 0;

	}
	
		if ( isset( $chk->adjust_app ) ) {

		$adjust_app = $chk->adjust_app;

	} else {

		$adjust_app = 0;

	}
	
		if ( isset( $chk->disc_app ) ) {

		$disc_app = $chk->disc_app;

	} else {

		$disc_app = 0;

	}
	
if ( isset( $chk->fd_app ) ) {

		$fd_app = $chk->fd_app;

	} else {

		$fd_app = 0;

	}
	
	if ( isset( $chk->pos_seg ) ) {

		$pos_seg = $chk->pos_seg;

	} else {

		$pos_seg = 0;

	}	
	
		if ( isset( $chk->font_size ) ) {

		$font_sizeH = $chk->font_size;

	} else {

		$font_sizeH = 14;

	}
	
	if ( isset( $chk->hotel_color ) ) {

		$colorH = $chk->hotel_color;

	} else {

		$colorH = '#000000';

	}
	
	if ( isset( $chk->declaration ) ) {

		$declaration = $chk->declaration;

	} else {

		$declaration = '';

	}
if ( isset( $chk->invoice_no ) ) {

		$invoice_no = $chk->invoice_no;

	} else {

		$invoice_no = 0;

	}
	
foreach ($details2 as $key3 ) { 

  $number_room=$key3->number_room;

  $number_guest=$key3->number_guest;



  ?>

	<table width="100%">

		<tr>

		<?php if($logo==1) {?>
			<td align="left" valign="middle"><img src="upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>"/>

			</td>
        <?php }?>

			<td align="right" valign="middle">
				<strong style="font-size:<?php echo $font_sizeH; ?>px !important; color:<?php echo $colorH; ?>;">
					<?php echo $hotel_name->hotel_name?>
				</strong>
			</td>

		</tr>

		<tr>

			<td colspan="2" width="100%">
				<hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;">
			</td>

		</tr>

	</table>

	<table>

		<?php 

			if(isset($tax) && $tax ){

			foreach($tax as $tax_details){

		?>

		<tr>

			<td width="70%" align="left" valign="top">
				<?php

			//	$invoice = $this->dashboard_model->get_invoice_grp( $key3->id );
				$invoice = $this->dashboard_model->get_invoice_number( $key3->id ,'gb');
			

				if ( isset( $invoice ) && $invoice ) {

					$invno = $invoice->invoice_id_actual;

					$invid = $invoice->	invoice_id;

				} else {

					$invno = '';

					$invid = '';



				}

				?>

				<span style="color:#F8681A; font-weight:bold; font-size:12px; line-height:20px; display:block;">TAX INVOICE (Group Reservation)</span>
				<?php 
				
			 if($invoice_no==1){
                     	echo $pref.$invno.$invid.$suf."<br/>"; 
                }else{
                    	echo "<strong>".$pref."</strong><span>"." - ".$invid."  ".$suf."</span>"."<br/>";
                    }
            ?>

        date_default_timezone_set("Asia/Kolkata");

			  	 echo "DATE:&nbsp;",date("g:ia dS M Y") ?>
				<?php

				$guest_det = $this->dashboard_model->get_guest_details( $key3->guestID );

				foreach ( $guest_det as $gst )

				{
					
					if ( ( $key3->bill_to_com == 1 ) && ( $gst->g_type == 'Corporate' ) && ( $gst->corporate_id != NULL ) && ( $gst->corporate_id != 0 ) ) {
						// Check if Bill to Company is Possible
						
						$corporate_details = $this->dashboard_model->fetch_c_id1( $gst->corporate_id );
						
					?>
					
				<ul class="list-unstyled">

					<li>
						<strong style="color:#6B67A1;">

							<?php

							if ( $corporate_details->legal_name )

								echo $corporate_details->legal_name;

							else

								echo $corporate_details->name;

							?>

						</strong>
					</li>

					<li>

						<?php 

                    $fg = 10;

                    if(isset($corporate_details->address) && ($corporate_details->address != '' && $corporate_details->address != ' ')){

                                            echo ($corporate_details->address);

                                            $fg = 0	;

                                            //echo ', ';

                                        }

                    if(isset($corporate_details->city) && ($corporate_details->city != '' && $corporate_details->city != ' ')){

                                            if($fg == 0){

                                                echo ', ';

                                                

                                            }

                                                

                                            echo $corporate_details->city;

                                            echo '<br>';

                                            $fg = 1;

                                        }

                    else if($fg == 0)

                                            $fg = 5;

                    if(isset($corporate_details->state) && ($corporate_details->state != '' && $corporate_details->state != ' ')){

                                            if($fg == 5)

                                                echo '<br>';

                                            echo $corporate_details->state;

                                            $fg = 2;

                                            //echo ', ';

                                        }

                    if(isset($corporate_details->pin_code) && $corporate_details->pin_code!= NULL){

                                            if($fg == 2 || $fg == 5){

                                                echo ', ';

                                            }									

                                            echo 'PIN - '.$corporate_details->pin_code;

                                            $fg = 3;

                                        }

                    if(isset($corporate_details->country) && $corporate_details->country != '' && $corporate_details->country != ' '){

                                            if($fg == 3)

                                                echo ', ';

                                            echo $corporate_details->country;

                                            /*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';

                                            $fg = 4;*/

                                        }

                    if($fg == 10){

                                            echo 'No Info';

                                        }

                ?>

					</li>

					<li>
						<?php echo '<strong>Phone: </strong>'.$corporate_details->g_contact_no; ?> </li>

					<li>
						<?php echo '<strong>Email: </strong>'.$corporate_details->corp_email; ?> </li>

					<li>
						<?php 
							
						echo '<strong>GSTIN: </strong> ';
						
						if(isset($corporate_details->gstin)){
							
							echo $corporate_details->gstin;
						}

						?>
					</li>

					<li>&nbsp; &nbsp;</li>

					<li>

						<?php 

					echo '<strong style="color:#06B4F5">Guest Name: </strong>';

					echo $gst->g_name;

					if($gst->c_des)

						echo ' - '.$gst->c_des;

												

							?> (Group Owner) </li>

				</ul>

				<?php  } else { ?>

				<ul class="list-unstyled">

					<li>
						<strong>

							<?php 

								echo '<span style="color:#6B67A1; text-size:12px;">'.$gst->g_name.'</span>';

								$guest_name = $gst->g_name;					

							?>

						</strong>(Group Owner)</li>

					<li>

						<li>

							<?php 

								
									// Logic for Guest Address

									$fg = 10;

									if(isset($gst->g_address) && ($gst->g_address != '' && $gst->g_address != ' ')){

										echo ($gst->g_address);

										$fg = 0	;

										//echo ', ';

									}

									if(isset($gst->g_city) && ($gst->g_city != '' && $gst->g_city != ' ')){

										if($fg == 0){

											echo ', ';

											

										}

											

										echo $gst->g_city;

										echo '<br>';

										$fg = 1;

									}

									else if($fg == 0)

										$fg = 5;

									if(isset($gst->g_state) && ($gst->g_state != '' && $gst->g_state != ' ')){

										if($fg == 5)

											echo '<br>';

										echo $gst->g_state;

										$fg = 2;

										//echo ', ';

									}

									if(isset($gst->g_pincode) && $gst->g_pincode!= NULL){

										if($fg == 2 || $fg == 5){

											echo ', ';

										}									

										echo 'PIN - '.$gst->g_pincode;

										$fg = 3;

									}

									if(isset($gst->g_country) && $gst->g_country != '' && $gst->g_country != ' '){

										if($fg == 3)

											echo ', ';

										echo $gst->g_country;

										/*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';

										$fg = 4;*/

									}

									if($fg == 10){

										echo 'No Info';

									}

								?>

						</li>

						<!--<li><?php echo $gst->g_address; ?> </li>

                    <li><?php echo $gst->g_place; ?> </li>

                    <li><?php echo $gst->g_city; ?> </li>

                    <li>PIN: <?php echo $gst->g_pincode; ?> </li>

                    <li><?php echo $gst->g_state." - ".$gst->g_country; ?> </li>-->

						<li><strong>Phone: </strong>
							<?php echo $gst->g_contact_no; ?> </li>

						<li><strong>Email: </strong>
							<?php echo $gst->g_email; ?> </li>

						<li>
							<?php 
						
						
						echo '<strong>GSTIN: </strong> ';
						
						if(isset($gst->gstin)){
							
							echo $gst->gstin;
						}
						
						
						?>
						</li>

				</ul>

				<?php } } ?>
				<ul class="list-unstyled">

					<li><strong>Group Id: </strong>
						<?php echo $key3->id; ?> </li>

					<li><strong>PAX: </strong>
						<?php echo $key3->number_guest; ?> | <strong>Rooms Qty: </strong>
						<?php echo $key3->number_room; ?> </li>

					<li><strong>Date: </strong>

						<?php 

						echo date("d-m-Y",strtotime($key3->start_date)); 

					?> to 
						<?php echo date("d-m-Y",strtotime($key3->end_date));  ?> </li>

					<?php if($booking_source == '1'){ ?>

					<li><strong>Booking Source: </strong>
						<?php echo $key3->booking_source; ?> </li>

					<?php } ?>

					<?php if($nature_visit == '1'){ ?>

					<?php if(isset($key3->nature_visit) && $key3->nature_visit != ""){

					$getNatureVisit = $this->dashboard_model->getNatureVisitById($key3->nature_visit);

				?>

					<li><strong>Nature of visit: </strong>

						<?php if(isset($getNatureVisit->booking_nature_visit_name) && $getNatureVisit->booking_nature_visit_name != "") { echo $getNatureVisit->booking_nature_visit_name; } else { echo 'No Info'; } ?>

					</li>

					<?php } } ?>

					<li>

						<?php 

				$data=$this->dashboard_model->get_group_food_plan_by_grp_id($key3->id); 

				if($data){

					echo "<strong>Food Plan: </strong>".$data->mealPlan;

				}

				?>

					</li>

				</ul>
			</td>

			<td width="30%" align="left" valign="top">				
			  	 <?php if($company_details == '1'){ ?> GST Reg No.:

				<?php  echo $tax_details->service_tax_no;?>

				<br/> CIN No:

				<?php  echo $tax_details->cin_no; } else { ?>


				<?php } ?>

				<div class="well">
					<strong>
						<?php echo  $hotel_name->hotel_name; ?>
					</strong><br/>

					<?php echo  $hotel_contact['hotel_street1']; ?>
					<?php echo  $hotel_contact['hotel_street2'].', '; ?><br/>

					<?php echo  $hotel_contact['hotel_district']; ?> -
					<?php echo  $hotel_contact['hotel_pincode']; ?>
					<?php echo  $hotel_contact['hotel_state']; ?> -
					<?php echo  $hotel_contact['hotel_country']; ?><br/>

					<strong>Phone:</strong>
					<?php echo  $hotel_contact['hotel_frontdesk_mobile']; ?><br/>

					<strong>Email:</strong>
					<?php echo  $hotel_contact['hotel_frontdesk_email']; ?><br/>

				</div>

			</td>

		</tr>

		<?php }} ?>
	</table>



	<?php } ?>



	<?php //echo $_GET['group_id'];

  

			//echo '<pre>';

			//print_r($line_items);

  

  ?>

	<span style="font-size:13px; font-weight:700; color:#F8681A; padding:5px 5px; display:block;">Booking Details</span>

	<hr style="background: #F8681A; border: none; height: 1px;">

	<table class="td-pad" width="100%">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th align="center" valign="middle"> # </th>

				<th align="center" valign="middle"> Rooms </th>

				<th align="center" valign="middle"> Days </th>

				<th align="center" valign="middle"> Pax </th>

				<th align="center" valign="middle"> Room Rent </th>

				<?php 

		unset($line_items['rr_tax']['r_id']);

			unset($line_items['rr_tax']['planName']);

			unset($line_items['rr_tax']['total']);

		foreach($line_items['rr_tax'] as $key => $value){

		?>

				<th align="center" valign="middle">
					<?php echo $key; ?>
				</th>

				<?php }?>

				<th align="center" valign="middle">Room Tax </th>

				<th align="right" valign="middle"> Total Amount </th>

			</tr>

		</thead>

		<tbody style="background: #fafafa">

			<?php $a=0; $b=0; $c=0; $i=0; $total=0; $trr=0; $ttax=0; $tfi=0; $tvat=0; if($details){ foreach ($details as $key ) { 



      $days=$key->days;

      ?>

			<?php  

    $trr=$trr+$key->trr;

    $ttax=$ttax+$key->room_st+$key->room_sc+$key->room_vat;

    $a=$a+$key->room_st;

     $b=$b+$key->room_sc;

      $c=$c+$key->room_vat;







    $total=$total+ $key->price+ $key->service;

    $i++; } }?>

			<tr>

				<td align="center" valign="middle">
					<?php echo 1; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $number_room; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $days; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $number_guest; ?>
				</td>

				<td align="center" valign="middle">
					<?php     

		//echo number_format($trr*$days, 2, '.','');

		echo number_format($line_items['all_price']['room_rent'], 2, '.','');

		?>
				</td>

				<?php 

		foreach($line_items['rr_tax'] as $key => $value){

		?>

				<td align="center" valign="middle">
					<?php echo number_format($value, 2, '.',''); ?>
				</td>

				<?php } ?>

				<td align="center" valign="middle">
					<?php

					//echo number_format($ttax*$days, 2, '.','');

					echo number_format( $line_items[ 'all_price' ][ 'rr_total_tax' ], 2, '.', '' );

					?>
				</td>

				<td align="right" valign="middle">
					<?php

					//$tds = ($trr+$ttax)*$days; echo number_format($tds, 2, '.','');

					$tax = $line_items[ 'all_price' ][ 'rr_total_tax' ];

					$rent = $line_items[ 'all_price' ][ 'room_rent' ];

					echo number_format( $tax + $rent, 2, '.', '' );

					?>
				</td>

			</tr>

		</tbody>

	</table>

	<hr style="background: #EDEDED; border: none; height: 1px;">



	<?php $tfi=0; if($details){ foreach ($details as $key ) { ?>

	<?php  

    $tfi=$tfi+$key->tfi;

    } }

   if($tfi>0)

  {

	  ?>

	<span style="font-size:13px; font-weight:700; color:#D75C58; padding:5px 5px; display:block;"> Food Inclusion </span>

	<hr style="background: #D75C58; border: none; height: 1px;">

	<table class="td-pad" width="100%">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th align="center" valign="middle"> # </th>

				<th align="center" valign="middle"> Rooms </th>

				<th align="center" valign="middle"> Days </th>

				<th align="center" valign="middle"> Pax </th>

				<th align="center" valign="middle"> Meal Charge </th>

				<?php 

		$a=$line_items['mp_tax']['total'];

		unset($line_items['mp_tax']['r_id']);

		unset($line_items['mp_tax']['planName']);

		unset($line_items['mp_tax']['total']);

		foreach($line_items['mp_tax'] as $key => $value){

		?>

				<th align="center" valign="middle">
					<?php echo $key?> </th>

				<?php }?>



				<th align="right" valign="middle"> Meal Plan Tax </th>

				<th align="right" valign="middle"> Total Meal Charge </th>

			</tr>

		</thead>

		<tbody style="background: #fafafa">

			<?php $i=0;$tfi=0; $ftax=0; $tfi=0; $tvat=0; if($details){ foreach ($details as $key ) { ?>

			<?php  

    $tfi=$tfi+$key->tfi;

    $ftax=$ftax+$key->food_vat;







   // $total=$total+ $key->price+ $key->service;

    $i++; } }?>

			<tr>

				<td align="center" valign="middle">
					<?php echo 1; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $number_room; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $days; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $number_guest; ?>
				</td>

				<td align="center" valign="middle">
					<?php 

		echo number_format($line_items['all_price']['meal_plan_chrg'], 2, '.','');

		?>
				</td>

				<?php 

		unset($line_items['mp_tax']['r_id']);

		unset($line_items['mp_tax']['planName']);

		//unset($line_items['mp_tax']['total']);

		foreach($line_items['mp_tax'] as $key => $value){

		?>

				<td align="center" valign="middle">
					<?php 

		echo number_format($value, 2, '.','');

		?>
				</td>

				<?php }?>

				<td align="center" valign="middle">
					<?php 

		echo number_format($a, 2, '.','');

		?>
				</td>





				<td align="right" valign="middle">
					<?php 

		$mealCharge=$line_items['all_price']['meal_plan_chrg'];

		$mealTax=$line_items['all_price']['mp_total_tax'];

		echo number_format($mealCharge+$mealTax, 2, '.','');

		?>
				</td>

			</tr>

		</tbody>

	</table>

	<hr style="background: #EDEDED; border: none; height: 1px;">



	<?php } ?>

	<!-- charge calculation -->

	<?php



	foreach ( $details2 as $groups ) {

		# code...



		$charge_string = $groups->charges_id;



		$charge_total_price = $groups->charges_cost;





	}





	$charge_sum = 0;

	if ( $charge_string != "" ) {

		?>

	<span style="font-size:13px; font-weight:700; padding:5px 5px; display:block; color:#366B9B;">Extra Charge Details</span>

	<hr style="background: #366B9B; border: none; height: 1px;">

	<table class="td-pad" width="100%">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th width="5%" align="center" valign="middle"> No </th>

				<th align="center" width="10%"> Date </th>
				<th width="33%" align="center" valign="middle"> Product / Service Name </th>

				<th width="10%" align="center" valign="middle"> Quantity </th>

				<th width="16%" align="center" valign="middle"> Unit Price </th>

				<th width="10%" align="center" valign="middle"> Amount </th>

				<th width="8%" align="center" valign="middle"> Total Tax </th>
				
				<th width="8%" align="center" valign="middle"> Discount </th>

				<th width="10%" align="right" valign="middle"> Total </th>

			</tr>

		</thead>

		<tbody style="background: #fafafa">

			<?php



			$charge_array = explode( ",", $charge_string );







			//print_r($service_array); 



			for ( $i = 0; $i < sizeof( $charge_array ); $i++ ) {

				# code...



				$charges = $this->dashboard_model->get_charge_details( $charge_array[ $i ] );



				if ( $charges && isset( $charges ) ) {

					?>

			<tr>

				<td align="center" valign="middle">
					<?php echo $charges->crg_id; ?>
				</td>
				<td align="center" valign="middle">&nbsp;</td>
				<td align="center" valign="middle">
					<?php echo $charges->crg_description; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $charges->crg_quantity; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $charges->crg_unit_price; ?>
				</td>

				<td align="center" valign="middle">
					<?php $charge_total= ($charges->crg_unit_price * $charges->crg_quantity); echo number_format($charge_total, 2, '.','');?>
				</td>

				<td align="center" valign="middle">
					<?php echo $charge_tax= ($charges->crg_tax ); 

                        echo "%";?>
				</td>
				<td align="center" valign="middle">
					0.00
				</td>
				<td align="right" valign="middle">
					<?php echo $charge_grand_total=$charges->crg_total; ?>
				</td>

			</tr>

			<?php $charge_sum=$charge_sum+$charge_grand_total; } } ?>
<?php 
			
			$master=$this->unit_class_model->get_laundry_master($bid,'2');
			
			$totalLaundry=0;
			$discountLaundry=0;
			$i=0;
			  if(isset($master) && $master){
				 foreach($master as $mas){
						$i++;
			$totalLaundry=$totalLaundry+$mas->grand_total;
			$discountLaundry=$discountLaundry+$mas->discount;
			
			$line_item_info=$this->unit_class_model->get_line_item_info($mas->laundry_id);
			?>
			<tr>
			    
			    <td align="center"><?php echo $mas->laundry_id ?></td>
			     <td align="center"><?php echo date("g:ia dS M Y",strtotime($mas->dateAdded));  ?></td>
			     <td align="center"><?php echo  $line_item_info->qty; ?></td>
			      <td align="center"><?php echo $mas->total_item ?></td>
				  <td align="center"></td>
			       <td align="center"><?php echo $mas->grand_total+$mas->discount ?></td>
			          <td align="center"><?php //echo $booking->laundry_item_qty*$booking->item_price; ?></td>
					  <td align="center"><?php echo $mas->discount ?></td>
			         
			            <td align="right"><?php echo $mas->grand_total; ?></td>
			    
			</tr>
            <?php
$charge_total+=$mas->grand_total;
$charge_sum+=$mas->grand_total;
			}} ?> 
		</tbody>

	</table>

	<hr style="background: #EDEDED; border: none; height: 1px;">



	<?php } ?>



	<!-- pos start --->

	<?php $sum_pos=0; 

			$total_bill_amount = 0 ;

            $total_tax_amount = 0 ;	

	if($pos && isset($pos)){  ?>

	<span style="font-size:13px; font-weight:700; color:#5AA650; padding:5px 5px; display:block;"> POS Details </span>

	<hr style="background: #5AA650; border: none; height: 1px;">

	<table width="100%" class="td-pad">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th width="5%" align="center" valign="middle"> Bill No </th>

				<th width="10%" align="center" valign="middle"> Date </th>
<?php if($fd_app==1) {?>
				<th width="30%" align="center" valign="middle"> Food Items </th>
<?php }?>
				<th  align="center" valign="middle"> Amount </th>
				<th  align="center" valign="middle"> CGST </th>
				<th  align="center" valign="middle"> SGST </th>
				<th  align="center" valign="middle"> IGST </th>

				<th  align="center" valign="middle"> Discount </th>

				<th  align="center" valign="middle"> Total </th>

				<th  align="right" valign="middle"> Due </th>

			</tr>

		</thead>

		<tbody style="background: #fafafa">

			<?php 

				$sum=0; $sum1=0; $posGAMTT=0; $pos_taxt = 0; $u_amt = 0; $posGAMT=0; $d = 0;
				$sum_sgst = 0;$sum_igst = 0;$sum_cgst = 0;

				foreach ($pos as $key) {

                        $pos_tax = 0;
						
						$pos_sgst = $key->food_sgst + $key->liquor_sgst; 
						$pos_tax = $pos_tax + $pos_sgst;
						
						$pos_cgst = $key->food_cgst + $key->liquor_cgst; 
						$pos_tax = $pos_tax + $pos_cgst;
						
						$pos_igst = $key->food_igst + $key->liquor_igst; 
						$pos_tax = $pos_tax + $pos_igst;
						
						$u_amt=$key->total_amount-$pos_tax+$d; 
						
						$pos_taxt = $pos_taxt + $pos_tax;
						$sum_sgst=$sum_sgst+$pos_sgst;
						$sum_cgst=$sum_cgst+$pos_cgst;
						$sum_igst=$sum_igst+$pos_igst; 
			?>

			<tr>

				<td align="center" valign="middle">
					<?php echo $key->invoice_number; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $key->date; ?>
				</td>
<?php if($fd_app==1) {?>
				<td align="center" valign="middle" class="hidden-480">
					<?php 

                                    $items=$this->dashboard_model->all_pos_items($key->pos_id);

                                    foreach ($items as $item ) {

									
                                        echo $item->item_name."(".$item->item_quantity.") ";

                                        echo "</br>";

                                    }

                                ?>
				</td>
<?php }?>
				<td align="center" valign="middle" class="hidden-480">
					<?php 

								$amt=$key->total_amount-$pos_tax;+$d;

								echo number_format($amt, 2, '.','');

								?>
				</td>

				<td align="center" valign="middle" class="hidden-480">
					
					<?php 

					 

					 echo number_format($pos_sgst, 2, '.','');

					?>

				</td>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php 

					 

					 echo number_format($pos_cgst, 2, '.','');

					?>

				</td>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php 

					 

					 echo number_format($pos_igst, 2, '.','');

					?>

				</td>

				<td align="center" valign="middle" class="hidden-480">
					<?php echo $d; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $key->total_amount; ?>
				</td>

				<td align="right" valign="middle">
					<?php echo $key->total_due; ?>
				</td>

			</tr>

			<?php $sum=$sum+$key->total_due;

                       $sum1=$sum1+$key->total_amount;  

					   $posGAMTT=$key->total_amount - $key->total_paid; 

					   $posGAMT=$posGAMT+$posGAMTT; 

					$total_bill_amount = $total_bill_amount + $key->bill_amount ;

					

					$total_tax_amount = $total_tax_amount + $key->tax ;						   

					   } 

			?>
</tbody>

	</table>
	<table class="td-pad">
	    <tbody>
			<tr>

					<td colspan="2">
					<hr style="background: #EEEEEE; border: none; height: 1px; ">
				</td>

			</tr>

			<tr>

				<td  style="text-align:right; width:75%;">Total Food Amount:</td>

				<td  style="text-align:right">
					<?php echo number_format($total_bill_amount - $pos_taxt, 2, '.','');?> </td>

			</tr>

			
			<tr>

				<td  style="text-align:right; ">Total POS Tax:
				</td>

				<td  style="text-align:right">

						<?php echo  $pos_taxt; ?> 

				</td>

			</tr>

			<tr>
				<td  style="text-align:right; ">
					<strong>Total POS Amount:</strong>
				</td>

				<td style="text-align:right">
					<strong>
						<?php 
							echo number_format(($total_bill_amount+$total_tax_amount), 2, '.','');
						?>
					</strong>
				</td>

			</tr>
		</tbody>
		
	</table>

	<hr style="background: #EEEEEE; border: none; height: 1px;">

	<?php } ?>

	<!-- pos end --->

	<!--charge end-->



	<span style="font-size:13px; font-weight:700; color:#D75C58; padding:5px 5px; display:block;"> Payment Details </span>

	<hr style="background: #D75C58; border: none; height: 1px;">

	<table class="td-pad" width="100%">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th width="5%" align="left"> # </th>

				<th width="5%" align="left"> Status </th>
				
				<th width="10%"> Bk Status </th>

				<th width="20%"> Transaction Date </th>

				<th width="10%"> Profit Center </th>

				<th width="15%"> Payment Mode </th>

				<th width="20%" align="center"> Transaction Details </th>

				<th width="10%" align="right" style="padding-right:8px;"> Amount </th>

			</tr>

		</thead>

		<?php  if($transaction){ ?>

		<tbody style="background: #fafafa">

			<?php  

	  $i = 0;

	  foreach ($transaction as $keyt ) { 

	  $i++;

	  ?>

			<tr>

				<td>
					<?php echo $i;?>
				</td>

				<td align="center" valign="middle">
					<?php 

						if($keyt->t_status == 'Cancel')

							echo '<span style="color:red;">Declined</span>';

						else if($keyt->t_status == 'Pending')

							echo '<span style="color:orange;">Processing</span>';

						else

							echo '<span style="color:green;">Recieved</span>';

					?>
				</td>
				<td style="text-align:center">
					<?php 
							
							if(isset($keyt->t_booking_status) && $keyt->t_booking_status!=NULL){
							
							if($keyt->t_booking_status == 1){
								echo "Temporary Hold";
							}
							if($keyt->t_booking_status == 2){
								echo "Advance";
							}
							if($keyt->t_booking_status == 3){
								echo "Pending";
							}
							if($keyt->t_booking_status == 4){
								echo "Confirmed";
							}
							if($keyt->t_booking_status == 5){
								echo "Checked In";
							}
							if($keyt->t_booking_status == 6){
								echo "Checked Out";
							}
							if($keyt->t_booking_status == 7){
								echo "Cancelled";
							}
							if($keyt->t_booking_status == 8){
								echo "Incomplete";
							}
							}
					
					?>
				</td>

				<td align="center" valign="middle">
					<?php echo $keyt->t_date; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $keyt->p_center; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $keyt->t_payment_mode; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $keyt->t_bank_name; ?>
				</td>

				<td align="right" valign="middle">
					<?php echo $keyt->t_amount; ?>
				</td>

			</tr>

			<?php  } ?>

			<tr>

				<td colspan="8">
					<hr style="background: #EEEEEE; border: none; height: 1px; ">
				</td>

			</tr>

			<tr>

				<td colspan="7" style="text-align:right; padding-right:16%;"><strong>Total Paid Amount:</strong>
				</td>

				<td align="right" valign="middle">
					<strong>

						<?php 

	$amountPaid = $this->dashboard_model->get_amountPaidByGroupID($_GET['group_id']);

	if(isset($amountPaid) && $amountPaid) {

		$paid=$amountPaid->tm;

		echo $paid;

	} 

	else {

		$paid=0;

		echo "Yet to pay";

	}

	

?>

					</strong>
				</td>

			</tr>

		</tbody>

		<?php  } ?>

	</table>

	<?php 

	$ab=$_GET['group_id']; 

	

	$co=0;

	$disc1= $this->unit_class_model->get_discount_details_group($ab);

	if(isset($disc1) && $disc1){

		foreach($disc1 as $ds){

			$co++;

			$d+=$ds->discount_amount;

	}}

	?>

	<hr style="background: #EEEEEE; border: none; height: 1px;">



	<table class="td-pad td-fon-size" width="100%">

		<tr>

			<td align="right" width="76%" valign="middle">Room Rent Amount:</td>

			<td align="right" valign="middle">
				<?php





				//echo number_format(($total+$charge_sum), 2, '.','');

				echo number_format( $line_items[ 'all_price' ][ 'room_rent' ], 2, '.', '' );



				?>


			</td>


		</tr>
		<?php if($line_items['all_price']['rr_total_tax'] != 0) { ?>
		<tr>

			<td align="right" valign="middle">Room Rent Tax:</td>

			<td align="right" valign="middle">

				<?php  echo number_format( $line_items['all_price']['rr_total_tax'], 2, '.','');    ?>

			</td>

		</tr>
		<?php } ?>
		<tr>

			<td align="right" valign="middle">Meal Charge:</td>

			<td align="right" valign="middle">

				<?php  echo number_format( $line_items['all_price']['meal_plan_chrg'], 2, '.','');    ?>

			</td>

		</tr>



		<tr>

			<td align="right" valign="middle">Meal Plan Tax:</td>

			<td align="right" valign="middle">

				<?php  echo number_format( $line_items['all_price']['mp_total_tax'], 2, '.','');    ?>

			</td>

		</tr>


		<?php if($charge_total > 0) { ?>
		<tr>

			<td align="right" valign="middle">Extra Chrage Amount:</td>

			<td align="right" valign="middle">

				<?php  

				  echo number_format($charge_total, 2, '.','');

				?>

			</td>

		</tr>
		
		<?php }?>

	<?php if($pos_settings==1) {?>
		<tr>

			<td align="right" valign="middle">POS Amount:</td>

			<td align="right" valign="middle">
				<?php echo number_format($total_bill_amount, 2, '.','');?>
			</td>

		</tr>

<?php }?>
<?php if($pos_seg==1) {?>
<tr>
			<td align="right" valign="middle">POS SGST:</td>
			<td align="right" valign="middle">

				<?php echo number_format($sum_sgst,2); ?>
				
			</td>
			</tr>
			<tr>
			<td align="right" valign="middle">POS CGST:</td>
			<td align="right" valign="middle">

				<?php echo number_format($sum_cgst,2); ?>
				
			</td>
		</tr>
		<tr>
			<td align="right" valign="middle">POS IGST:</td>
			<td align="right" valign="middle">

				<?php echo number_format($sum_igst,2); ?>
				
			</td>
		</tr>
<?php }?>
		<tr>

			<td align="right" valign="middle"><strong>Total Amount:</strong>
			</td>

			<td align="right" valign="middle">
				<strong>

					<?php 

	  if(isset($posGAMT) && $posGAMT !=""){

		  $grand_total_amount= $total+$charge_sum+$posGAMT;

		  

      } else {

		  $grand_total_amount= $total+$charge_sum; 

	  }	

      echo 'INR '.number_format($grand_total_amount, 2);	  

	  ?>

				</strong>
			</td>

		</tr>		
		<?php if($disc_app==1 &&  $d != 0){ ?>
		<tr>

			<td align="right" valign="middle">Discount:</td>

			<td align="right" valign="middle">
				<?php echo $d." (".$co.")"; ?>
			</td>

		</tr>
		<?php } 
		if($adjust_app==1) {
			
			if(isset($adjstAmt) && $adjstAmt) {
				$adjstAmt=$adjstAmt->amount;
			} else {
				$adjstAmt=0;
			}
			?>
		<tr>

			<td align="right" valign="middle">Adjustment amount:</td>

			<td align="right" valign="middle">
				<?php echo $adjstAmt;?>
				<?php //echo " (". $m.")" ?>
			</td>

		</tr>
		<?php }?>
		
		<tr>

			<td align="right">Total Payable Amount:</td>

			<td align="right" valign="middle">
				<?php $payAmount = $grand_total_amount-$d+$adjstAmt; 

	  echo number_format($payAmount, 2);?>
			</td>

		</tr>
		<!--<tr>
			<td></td>
			<td align="right">
				<em style="text-transform: capitalize;">Twelve Thousands Six Hundred And Ninety Rupees Only</em>
			</td>
		</tr>-->

		<?php 

            $amountPaid = $this->dashboard_model->get_amountPaidByGroupID($_GET['group_id']);

                 if(isset($amountPaid) && $amountPaid) {

                                                                                $paid=$amountPaid->tm;

                                                                             }

                                                                             else{

                                                                              $paid=0;

                                                                            }

                    

                    

                                                                        ?>

		<tr>

			<td align="right" valign="middle"><span style="font-family: 'Exo 2', sans-serif; font-size:13px; font-weight:700; color:#8876A8;">Total Due:</span>
			</td>

			<td align="right" valign="middle">
				<?php 

			$td = $payAmount-$paid;

			$paid_c = number_format($td, 2, '.','');

			if($paid_c != 0){ ?>

				<span style="font-family: 'Exo 2', sans-serif; font-size:13px; font-weight:700; color:#F65656;">INR <?php echo number_format($td, 2, '.','');?></span>

				<?php

				} else {
					?>

				<span style="font-family: 'Exo 2', sans-serif; font-size:13px; font-weight:700; color:#7FBA67;">Full Paid</span>

				<?php }?>
			</td>

		</tr>

	</table>

	<table width="100%">

		<tr>

			<td style="padding:100px 0 0;" colspan="2"></td>

		</tr>

		<tr>

			<td valign="bottom">

				<span style="font-size:9px; font-weight:400; color:#939292; display:block;">0441070 - ACCOMODATION IN HOTEL/INN/GUEST HOUSE/ CLUB OR CAMP SITE ETC.SERVICE</span>			

			</td>

			<td align="center" width="35%">______________________________<br/> Authorized Signature </td>

		</tr>

		<tr>

			<td width="65%"><span style="font-size:9px; font-weight:400; color:#939292; display:block;">0441067 - RESTAURANT SERVICE</span></td>

            <?php if($chk->declaration!='') {?>
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;"><?php echo $chk->declaration;?></span>
                <?php }?>
				
			<td align="center" width="35%">
				<?php   // Showing the Hotel Name from Session

                

        $hotel_n = $this->dashboard_model->get_hotel_name($this->session->userdata('user_hotel'));

        if(isset($hotel_n->hotel_name) && $hotel_n->hotel_name)

            echo 'For, '.$hotel_n->hotel_name;

    ?>
			</td>

		</tr>

	</table>

	<hr style="background: #00C5CD; border: none; height: 1px; margin-top:10px; width:100%;">

	<table width="100%" class="td-pad">
		<tr>
			<td align="center"><?php if($chk->ft_text!=""){ echo $chk->ft_text; }?></td>
		</tr>
		<tr>
			<td align="center">This is a Computer Generated Invoice Signature Not Requierd</td>
		</tr>
	</table>
	<hr style="background: #00C5CD; border: none; height: 1px; margin-top:10px; width:100%;">
	<span style="font-size:9px; font-weight:400; color:#939292; display:block; text-align: center;"> Powered by BookMania.  Thank you for your stay! </span>
	
</div>