<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>

<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>

<div class="portlet light borderd">
  <div class="portlet-title">
	<div class="caption"> <i class="fa fa-edit"></i> Discount Report </div>
	<div class="tools"> 
	  <a href="javascript:;" class="reload"></a> </div>
	</div>
	<div class="portlet-body">  
	  <div class="table-toolbar">
      	<div class="row">
        	<div class="col-md-4 form-inline">
            	<div class="form-group" id="defaultrange">
                   <input name="r_start_dates" id="r_start_dates" type="text" class="form-control" placeholder="Filter Date Range">
                   <button class="btn btn-default" type="submit" id="date_search"><i class="fa fa-search"></i></button>
				   <span id="date_show"></span>
                </div>
            </div>
        </div>
      </div>
	  <div id="report">
	  <table class="table table-striped table-bordered table-hover" id="sample_1">
		<thead>
		  <tr>
			<th width="2%">Sl </th>
			<th style="width:10%;">Discount Date </th>
			<th>Admin Name </th>
			<th>Hotel Name </th>
			<th>Discount Given to </th>
			<th>Guest Name </th>
			<th class="none">Discount plan </th>	
			<th class="none">Discount For </th>
			<th class="none">Discount % </th>
			<th>Discount Amount </th>
			<th>Amount Due </th>
			<th>Total Amount </th>
			<th class="none">IP Address </th>
			<!--<th> Unit Type </th>
			<th> Unit Class </th>
			<th width="400"> Unit Description </th>-->
			
		  </tr>
		</thead>
		
		<tbody>
		
		
		  <?php
		  
		 //print_r($booking);
//exit();
 
					if(isset($booking) && $booking ){
					 $sl =0;
					 $d = '';
					  foreach($booking as $book){
						$sl++;  
						  ?>
		  <tr id="row_<?php echo $sl;?>">
			<td><?php echo $sl;?></td>
			<td style="width:10%;">
				<?php 
					if($d != date("jS M Y",strtotime($book->add_date))){
						echo '<span style="color:#0277BD; font-weight:bold;">'.date("jS M Y",strtotime($book->add_date)).'</span>';
					}
					else{
						echo '<span style="color:#0277BD;">'.date("jS M Y",strtotime($book->add_date)).'</span>';
					}
					
					$d = date("jS M Y",strtotime($book->add_date));
				?>
			</td>
			<td><?php echo $book->admin_name; ?></td>
			<td><?php echo $book->hotel_name; ?></td>
			<td>
				<?php 
					if($book->related_type == 'group_booking'){
						echo '<span style="color:#F8681A;"><i class="fa fa-users" aria-hidden="true"></i> Group Booking</span>';
					}
					else if($book->related_type == 'single_booking'){
						echo '<span style="color:#023358;"><i class="fa fa-user" aria-hidden="true"></i> Single Booking</span>';
					}
					else
						echo $book->related_type;
					
					echo '<br><span style="font-style:italic;"> - '.$book->related_id.'</span>';
				?>
			</td>
			<td><?php echo $book->g_name; ?></td>
			<td>
				<?php
					if($book->discount_plan_id == 0)
						echo '<span style="color:#AAAAAA;">None</span>';
					else
						echo $book->rule_name; 
				?>
			</td>
			
			<td>
				<?php 
					echo $book->discount_for; 
				?>
			</td>
			<td>
				<?php 
					if($book->discount_percentage > 25)						
						echo '<span style="color:#E52828;">'.$book->discount_percentage.' <i style="color:#E52828;" class="fa fa-percent" aria-hidden="true"></i>';
					else if($book->discount_percentage > 10)						
						echo '<span style="color:#3E3274;">'.$book->discount_percentage.' <i style="color:#3E3274;" class="fa fa-percent" aria-hidden="true"></i>';
					else
						echo $book->discount_percentage.' <i style="color:#AAAAAA;" class="fa fa-percent" aria-hidden="true"></i>'; 
				?>
			</td>
			<td>
				<?php 
					echo '<i class="fa fa-inr" aria-hidden="true"></i> '.$book->discount_amount; 
				?>
			</td>
			<td>
				<?php 
					echo $book->amt_due; 
				?>
			</td>
			<td><?php echo $book->totalAmt; ?></td>
			<td><?php echo $book->ip_address; ?></td>
		  </tr>
		  <?php
					  }
					}  
					  ?>
		</tbody>
		</div>
	  </table>
	</div>
</div>

<script>
$(document).ready(function(){
	
	$('#date_search').on('click',function(){

		var search = $('#r_start_dates').val();
		$('#date_show').html(search);
		//alert(search);
		$.ajax({
			
			type:"POST",
			url:"<?php echo base_url();?>reports/discount_report/manual",
			data:{search_val:search},
			success:function(data){
				
				
				//alert(data);
				$('#report').html(data);
				
			}
			
		});
		
	});
	
	
});

</script>
	
