<div class="row all_bk">
  <div class="form-body"> 
    <!-- 17.11.2015-->
    <?php if($this->session->flashdata('err_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-danger alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
    <?php if($this->session->flashdata('succ_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-success alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
  </div>
  <div class="col-md-12"> 
    <!-- BEGIN SAMPLE TABLE PORTLET-->
    
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-edit"></i>Booking Log </div>
        <div class="tools"> 
          <!--<a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>--> 
          <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            <div class="col-md-6"> </div>
            <div class="col-md-6">
              <div class="btn-group pull-right">
                <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu pull-right">
                  <li> <a href="javascript:;"> Print </a> </li>
                  <li> <a href="javascript:;"> Save as PDF </a> </li>
                  <li> <a href="javascript:;"> Export to Excel </a> </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="sample_1">
          <thead>
            <tr> 
              <!-- <th scope="col">
                                Select
                            </th>-->
              
              <th scope="col"> Booking Taking Date </th>
              <th scope="col"> Booking Taking Time </th>
              <th scope="col"> Checkin Time </th>
              <th scope="col"> Checkout Time </th>
              <th scope="col"> View Details </th>
            </tr>
          </thead>
          <tbody>
            <?php if(isset($bookinglog) && $bookinglog):
							
                            $i=1;//var_dump($salpay);die;
                            foreach($bookinglog as $value):
                                $class = ($i%2==0) ? "active" : "success";
                                //$sal_pay_id=$sp->hotel_payment_id;
                                ?>
            <tr>
              <td><?php echo $value->booking_taking_date; ?></td>
              <td><?php echo $value->booking_taking_time; ?></td>
              <td><?php
						echo $value->checkin_time;
						?></td>
              <td><?php
						echo $value->checkout_time;
							?></td>
              <td align="center"><?php 
								$date=$value->booking_taking_date;
					
					?>
                <a href="<?php echo base_url('dashboard/guestlog/'.$date);?>">
                <button  class="btn green"> Guest Details </button>
                </a></td>
            </tr>
            <?php endforeach; ?>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    
    <!-- END SAMPLE TABLE PORTLET--> 
    
  </div>
</div>
<!-- END PAGE CONTENT-->
</div>
</div>
<!-- END CONTENT --> 
