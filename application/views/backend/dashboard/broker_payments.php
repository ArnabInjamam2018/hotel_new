<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-inr"></i>Broker/ Agent Payments </div>
    <div class="actions">
    	<a href="<?php echo base_url();?>dashboard/add_broker" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
    </div>
  </div>
  <div class="portlet-body">    		  
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
        
          <th scope="col"> Picture Of Broker </th>
          <th scope="col"> Broker Type </th>
          <th scope="col"> Broker Name </th>
          <th scope="col"> Total Booking </th>
          <th scope="col"> Booking This Month </th>		            <th scope="col"> Total Commision </th>
          <th scope="col"> Amount Payed </th>
          <th scope="col"> Amount Pending </th>
          <th scope="col"> Amount Pending This Month </th>
          <th scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($broker) && $broker):
                    $i=1;
                     
                        //print_r($broker);
						$tot = 0;
						$paid = 0;
						$pen = 0;
						$rem = 0;
                    foreach($broker as $brk):
					$brk->broker_commission_total- $brk->broker_commission_payed;										$totComms = $this->reports_model->getBrokerComms($brk->b_id,'agent');
						if($totComms->amt)
							$tot = $tot + $totComms->amt;
						if($brk->broker_commission_payed)
							$paid = $brk->broker_commission_payed;					
						$pen = $tot - $paid;
                        $class = ($i%2==0) ? "active" : "success";
                        $b_id=$brk->b_id;
                        $booking=$this->dashboard_model->get_booking_by_b_id($b_id);
                    for($i=1;$i<=$maxDays;$i++){
						if($i<10){
						  $val= $y.'-'.$m.'-'.'0'.$i;  
						}else{
						  $val= $y.'-'.$m.'-'.$i; 
						}					} 
                    $monthly_bking=$this->dashboard_model->get_monthly_booking($val);
                    ?>
        <tr> 
          
          <td width="20">			<a class="single_2" href="<?php echo base_url();?>upload/broker/image/<?php if( $brk->b_photo_thumb == '') { echo "business-man-hi.png"; } else { echo $brk->b_photo_thumb; }?>"><img src="<?php echo base_url();?>upload/broker/image/<?php if( $brk->b_photo_thumb == '') { echo "business-man-hi.png"; } else { echo $brk->b_photo_thumb; }?>" alt="" style="width:100%"/></a>
          </td>
          <td><?php if($brk->b_agency == 5)
                                { echo "Agency";
                                }else{
                                    echo "Self";
                                }?></td>
          <td><?php echo $brk->b_name ?></td>
          <td><?php echo $booking ?></td>
          <td><?php echo $monthly_bking ?></td>		  		  <td>			<?php 															echo $totComms->amt; 			?>		  </td>
          <td><?php echo $brk->broker_commission_payed; ?></td>		  
          <td><?php echo ( $totComms->amt - $brk->broker_commission_payed); ?></td>
          <td></td>
         
          <td class="ba">
          	<div class="btn-group no-bgbut">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a data-toggle="modal" href="#Make_Payment" onclick="return give_broker_id(<?php echo $brk->b_id ?>, <?php echo $brk->broker_commission_payed; ?>, <?php echo $totComms->amt; ?>,<?php echo $brk->broker_commission; ?>)" class="btn red btn-xs"><i class="fa fa-money"></i></a></li>
                <li><a data-toggle="modal" href="#Add_Booking" onclick="return give_broker_commission('<?php echo $brk->b_id ?>','<?php echo $brk->broker_commission ?>');" class="btn green btn-xs"><i class="fa fa-plus"></i></a></li>
              </ul>
            </div>
          </td>		  
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
	
	<!-- My Test Block-->
	
   <div class="portfolio-block">
    <div class="portfolio-stat pull-right">
      <div class="portfolio-info"> Total Brokerage <span> <?php echo "INR ".number_format($tot,2); ?> </span> </div>
      <div class="portfolio-info"> Total Paid <span> <?php echo "INR ".number_format($paid,2); ?> </span> </div>
      <div class="portfolio-info"> Total Pending <span> <?php echo "INR ".number_format($pen,2); ?> </span> </div>      
    </div>    
  </div>
	
	
  </div>
</div>
<div id="Make_Payment" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

                            $form = array(
                                'class' 			=> 'form-body',
                                'id'				=> 'form',
                                'method'			=> 'post'
                            );

                            echo form_open_multipart('dashboard/broker_payments',$form);

                            ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Make Payment</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:200px" data-always-visible="1" data-rail-visible1="1">
            <div class="row">
              <div class="col-md-6">
                  <div class="form-group form-md-line-input">
                      <input type="hidden" name="b_id" value="" id="broker_id_final" >
                      <input type="text" class="form-control" id="broker_payed" name="b_pending" value="" disabled="disabled"/>
                      <label></label>
                      <span class="help-block">Total Pending Amount *</span>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group form-md-line-input">
                      <input type="text" class="form-control" id=""  name="b_last_payment" disabled="disabled"/>
                      <label></label>
                      <span class="help-block">Last Payment Made </span>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group form-md-line-input">
                      <input type="text" class="form-control" id=""  name="b_booking_m" disabled="disabled"/>
                      <label></label>
                      <span class="help-block">Bookings This Month</span>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group form-md-line-input">
                      <input type="text" class="form-control" id=""  name="b_pending_m" disabled="disabled"/>
                      <label></label>
                      <span class="help-block">Pending This Month</span>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group form-md-line-input">
                      <input type="text" id="add_amount" class="form-control"  placeholder="Enter Amount *" name="b_amount" required />
                      <input type="hidden" id="booking_id" >
                      <input type="hidden" id="booking_status_id" >
                      <label></label>
                      <span class="help-block">Enter amount to be paid *</span>
                  </div>
              </div>
              <div class="col-md-6" id="mop">
                 <div class="form-group form-md-line-input" >
                      <select class="form-control" placeholder="Payment mode" name="b_payment_mode" onchange="payment_mode_change(this.value);" required >
                        <option value="" disabled selected>Select Payment Mode</option>
                        <?php $mop = $this->dashboard_model->get_payment_mode_list();
								if($mop!=''){
									
									foreach($mop as $mp){
								
							?>
						 <option value="<?php echo $mp->p_mode_name; ?>" ><?php echo $mp->p_mode_des; ?></option>
									<?php }}?>
					</select>	
				 </div>
			 </div>
			
              <div class="col-md-6" id="bank_name" style="display:none;">
                  <div class="form-group form-md-line-input">
                      <input type="text"  onkeypress="return onlyLtrs();" class="form-control" name="b_bank_name" placeholder="Bank Name" />
                      <label></label>
                      <span class="help-block">Bank Name *</span>
                  </div>
				</div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
        <button type="submit" class="btn green">Pay</button>
      </div>
    </div>
  </div>
  <?php form_close(); ?>
</div>
<div id="Add_Booking" class="modal fade" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Booking</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:150px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
          	<div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <select class="form-control" name="booking_id" id="t_payment_mode" onchange="booking_mode_show(this.value);" onblur="return calculator(this.value);">
                      <option value="" disabled selected>Select Booking Id</option>
                      <?php
    
                                                           $all_bookings=$this->dashboard_model->all_bookings();
    
                                                           if($all_bookings && isset($all_bookings)){
    
                                                           foreach($all_bookings as $booking){
    
                                                               $booking_id='HM0'.$this->session->userdata('user_hotel').'00'.$booking->booking_id;
    
    
                                                           ?>
                      <option  value="<?php echo  $booking_id; ?>"><?php echo $booking_id; ?></option>
                      <?php }} ?>
                    </select>
                    <label></label>
                    <span class="help-block">Select Booking Id *</span>
                </div>
            </div>
            <div class="col-md-6">
            	<div class="form-group form-md-line-input">
                    <input type="text" id="show_id" class="form-control"  placeholder="Booking Id" name="booking_id" />
                    <label></label>
                    <span class="help-block">Booking Id *</span>
                </div>
            </div>
            <div class="col-md-6">
            	<div class="form-group form-md-line-input">
                    <input type="text" id="commission" class="form-control"  placeholder="% Commision" name="" value=""/>
                    <label></label>
                    <span class="help-block">% Commision *</span>
              	</div>
            </div>
            <div class="col-md-6">
            	<div class="form-group form-md-line-input">
                    <input type="text" id="total_brok" class="form-control" value=""  placeholder="Calculated Amount" name="broker_amount" />
                    <input type="hidden" name="booking_id_final" id="booking_id_final" >
                    <input type="hidden" name="broker_id" id="broker_id" value="<?php echo $brk->b_id; ?>">
                    <label></label>
                    <span class="help-block">The Calculated Amount *</span>
              	</div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="button" class="btn green" onclick="return save_broker()">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE LEVEL SCRIPTS --> 
<script>
 function payment_mode_change(y)
    {
       
        if(y=="cash")
        {
			
			 $('#bank_name').css('display','none');
			
        }
        else
        {
           $('#bank_name').css('display','block');
        }
    }
    function give_broker_id(data,payed,total,commission){
        //alert(data);
        document.getElementById("broker_id_final").value=data;
        document.getElementById("broker_payed").value=parseInt(total)-parseInt(payed);
        document.getElementById("broker_total").value=total;
        document.getElementById("commission").value=commission;
    }

 function give_broker_commission(id,commission){
     //alert(data);
     document.getElementById("commission").value=commission;
     document.getElementById("broker_id").value=id;
 }
	function booking_mode_show(x)
    {
        
        //alert(x);
		document.getElementById('show_id').value=x;
    }
</script> 
<script>
    function calculator(id){


        var b_id=id;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {

           // alert(xhttp.status);

            if (xhttp.readyState == 4 && xhttp.status == 200) {

                //alert(xhttp.response);

               var percentage=(parseInt(xhttp.response)*parseInt(document.getElementById("commission").value))/100;

                document.getElementById("total_brok").value=percentage;
                //document.getElementById("booking_id_final").value=percentage;

                }

        };
        xhttp.open("GET", "<?php echo base_url(); ?>dashboard/get_amount_guest?booking_id="+b_id, true);
        xhttp.send();
    }

    function save_broker(){

        var broker_id=document.getElementById("broker_id").value;
        var booking_id=document.getElementById("show_id").value;
        var broker_amount=document.getElementById("total_brok").value;
        //alert(booking_id);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {

           // alert(xhttp.status);

            if (xhttp.readyState == 4 && xhttp.status == 200) {

                swal('booking add is '+xhttp.response);

            }
        };
        xhttp.open("GET", "<?php echo base_url(); ?>dashboard/broker_booking?booking_id="+booking_id+"&broker_id="+broker_id+"&broker_amount="+broker_amount, true);
        xhttp.send();
    }
</script>