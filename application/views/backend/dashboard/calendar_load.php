<script src="<?php echo base_url();?>assets/dashboard/daypilot/js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/dashboard/daypilot/media/layout.css" />
<script src="<?php echo base_url();?>assets/dashboard/daypilot/js/daypilot/daypilot-all.min.js" type="text/javascript"></script>
<div id="home">
	<div class="row">
        <div class="col-md-12">
          	<div id="booking_calendar"> 
                <div class="portlet light bordered">
                  <div class="portlet-title">
                    <div class="caption font-green"> <i class="icon-pin font-green"></i> <span class="caption-subject bold uppercase" style="letter-spacing: 1px;"> Reservation Management</span> </div>
                  </div>
                  <div class="portlet-body form">
                    <form role="form">
                      <div class="form-body">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="portlet">
                              <div class="btn-group" data-toggle="buttons">
                                <input type="hidden" id="hidden_field1" value="">
                                <label class="btn green dark-stripe">
                                  <input id="5" type="checkbox" onchange="calendar()" class="toggle">
                                  Detailed View</label>
                                <label class="btn default blue-stripe" style="margin-left:10px;">
                                  <input id="7" type="checkbox" onchange="calendar2()" class="toggle">
                                  Detailed View 2 </label>
                                <label class="btn default yellow-stripe" style="margin-left:10px;">
                                  <input id="6" type="checkbox" onchange="snapshot()" class="toggle">
                                  Snapshot View </label>
                              </div>
                              <button class="btn btn-warning pull-right" id="showfilter" type="button" style="padding:0;"><span class="tooltips"   data-style="default" data-container="body" data-original-title="Booking Filter" style="padding: 7px 14px; display:block;"><i class="fa fa-filter"></i></span></button>
                            </div>
                          </div>
                          <script type="text/javascript">
							   function calendar(){
								 window.location="<?php echo base_url(); ?>dashboard/add_booking_calendar";
							   }
							   function calendar2(){
								 window.location="<?php echo base_url(); ?>dashboard/calendar_load_2";
							   }
							   function snapshot(){
								 window.location="<?php echo base_url(); ?>dashboard/snapshot_load";
							   }
							   $('#showfilter').click(function(){
								   if($('#bookfilter').css('opacity') == 1)
								   {
									$('#bookfilter').css('position','absolute');
									$('#bookfilter').css('opacity','0');
								   }
								   else
								   {
									$('#bookfilter').css('position','static');
									$('#bookfilter').css('opacity','1');
								   }
								});
                          </script>
                          <div id="bookfilter" style="position:absolute; opacity:0; top:30%;">
                              <div class="col-md-6 col-sm-6">
                                <div class="portlet light bordered">
                                  <div class="row">
                                    <div class="col-md-4">
                                      <div class="form-group">
                                        <label>Unit Type:</label>
                                        <?php $units=$this->dashboard_model->all_units(); ?>
                                        <select  id="filter2" class="form-control input-sm" >
                                          <option value="all">All</option>
                                          <option value="Room">Room</option>
                                          <option value="Bunglow">Bunglow</option>
                                          <option value="Villa">Villa</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="form-group">
                                        <label>No Of Beds:</label>
                                        <select id="filter4" class="form-control input-sm">
                                          <option value="all">All</option>
                                          <option value="1">1</option>
                                          <option value="2">2</option>
                                          <option value="3">3</option>
                                          <option value="4">4</option>
                                          <option value="1">5</option>
                                          <option value="2">6</option>
                                          <option value="3">7</option>
                                          <option value="4">8</option>
                                          <option value="1">9</option>
                                          <option value="2">10</option>
                                          <!--<option value="4">Family</option>-->
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="form-group">
                                        <label>Unit Category :</label>
                                        <?php $units=$this->dashboard_model->all_units(); ?>
                                        <select  id="filter1" class="form-control input-sm">
                                          <?php if(isset($units) && $units): ?>
                                          <option value="all">All</option>
                                          <?php foreach($units as $unit): ?>
                                          <option value="<?php echo $unit->id; ?>"><?php echo $unit->unit_name; ?></option>
                                          <?php endforeach; ?>
                                          <?php endif; ?>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="form-group">
                                        <label>Unit Class:</label>
                                        <select id="filter3" class="form-control input-sm">
                                          <option value="all" >All</option>
                                          <?php 
                                                    $unit_class= $this->dashboard_model->get_unit_class_list();
                                                    foreach($unit_class as $uc)
                                    {?>
                                          <option value="<?php echo $uc->hotel_unit_class_name;?>"><?php echo $uc->hotel_unit_class_name; ?></option>
                                          <?php } ?>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="form-group">
                                        <label>Start date:</label>
                                        <div class="input-group">
                                          <input class="form-control input-sm" id="start" type="text">
                                          <span class="input-group-btn"> <a class="btn blue btn-sm" onclick="picker.show(); return false;"><i class="fa fa-calendar-o"></i></a> </span> </div>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="form-group">
                                        <label>Time range:</label>
                                        <select id="timerange" class="form-control input-sm">
                                          <option value="week">Week</option>
                                          <option value="2weeks">2 Weeks</option>
                                          <option value="month" selected>Month</option>
                                          <option value="2months">2 Months</option>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6 col-sm-6">
                                <div class="portlet light bordered">
                                  <h5 style="font-size:14px; font-weight:400;">Price Range</h5>
                                  <div class="range" style="padding:5.8px 0;">
                                    <input id="range_1" type="text" name="range_1" value=""/>
                                  </div>
                                  <button type="button" onclick="price_range()" Class="btn blue btn-sm">Search</button>
                                </div>
                              </div>
                          </div>
                          <div class="col-md-12">
                            <div class="portlet clearfix">
                              <div class="col-md-5" style="padding-left:0;">
                                <div class="form-group">
                                  <label class="col-md-3 control-label" style="padding-left:0;">Room No:</label>
                                  <div class="col-md-6">
                                    <input id="filter_room_no" type="text" class="form-control input-sm">
                                    </input>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-7" style="padding-right:0;">
                                <div class="btn-group pull-right">
                                  <label for="autocellwidth" class="auto_cl btn grey-cascade btn-sm" id="auto_cl_id"  >
                                    <input type="checkbox" id="autocellwidth" class="toggle" style="display:none;">
                                    Auto Cell Width</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <script type="text/javascript">
                                    $(document).ready(function() {
                
                                        dp.cellWidth = 100;
                                        dp.cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                
                                        
                    
                                        dp.update();
                                        //dp.cell.enabled = false;
                                        /*if (start.getTime() < today.getTime())
                                         { dp.Cell.Enabled = false; e.Day.IsSelectable = false; }*/
                                        //alert(start.getTime());
                
                                    });
                                    var picker = new DayPilot.DatePicker({
                                        target: 'start',
                                        pattern: 'M/d/yyyy',
                                        date: new DayPilot.Date().firstDayOfMonth(),
                                        onTimeRangeSelected: function(args) {
                                            //dp.startDate = args.start;
                                            loadTimeline(args.start);
                                            loadEvents();
                                        }
                                    });
                
                                    $("#timerange").change(function() {
                                        switch (this.value) {
                                            case "week":
                                                dp.days = 7;
                                                break;
                                            case "2weeks":
                                                dp.days = 14;
                                                break;
                                            case "month":
                                                dp.days = dp.startDate.daysInMonth();
                                                break;
                                            case "2months":
                                                dp.days = dp.startDate.daysInMonth() + dp.startDate.addMonths(1).daysInMonth();
                                                break;
                                        }
                                        loadTimeline(DayPilot.Date.today());
                                        loadEvents();
                                    });
                
                                    $("#autocellwidth").click(function() {
                                        dp.cellWidth = 100;  // reset for "Fixed" mode
                                        dp.cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                                        document.getElementById('auto_cl_id').style.backgroundColor = $(this).is(":checked") ? "#00CC99" : "#A5A5A5";
                                        dp.update();
                
                                    });
                                </script>
                        <div style="position:relative;">
                          <div class="bok">Booking</div>
                          <div id="dp"> </div>
                        </div>
                        <script>
                                var dp = new DayPilot.Scheduler("dp");
                                var clean;
                
                                dp.rowMaxHeight = 10000;
                
                                dp.allowEventOverlap = false;
                                dp.borderColor = "red";
                                //dp.scale = "Day";
                                //dp.startDate = new DayPilot.Date().firstDayOfMonth();
                                dp.days = dp.startDate.daysInMonth();
                                //loadTimeline(DayPilot.Date.today(-3));
                                loadTimeline(DayPilot.Date.today().addDays(-3));
                
                                dp.eventDeleteHandling = "Update";
                
                
                                //cell rendering
                
                               dp.onBeforeCellRender = function(args) {
                                  
                                    if (args.cell.start < DayPilot.Date.today())
                                    {
                                        args.cell.backColor = "#EEEEEE";
                                       //args.cell.backColor = "black";
                                    }
                                    else
                                    {
                                        var dayOfWeek = args.cell.start.getDayOfWeek();
                                        if (dayOfWeek === 6 || dayOfWeek === 0) {
                                            args.cell.backColor = "#F6F6F6";
                                        }
                                    }
                
                                      if (args.cell.start <= DayPilot.Date.today() && DayPilot.Date.today() < args.cell.end)
                                    {
                                        args.cell.backColor = "#dff4f2";
                                       // alert(args.cell.start+"   "+args.cell.end);
                                        //args.cell.backColor = "black";
                                    }
                                    
                
                                    if (args.cell.resource === "unit") {
                
                                        args.cell.backColor = "#EEEEEE";
                                       // args.cell.html += " (loaded dynamically)";
                                        //args.cell.left.enabled = false;
                                        //args.cell.right.enabled = false;
                                       // args.right.html = "You can't create an event here";
                
                                        args.cell.allowed = false;
                
                                    }
                                };
                
                
                
                
                                dp.onBeforeTimeHeaderRender = function(args) {
                                   // alert(DayPilot.Date.today() +"sas" +args.header.end);
                                    //args.header.start.setHours(15);
                                    //DayPilot.Date.addHours(10);
                                    var d = new Date();
                                    var dayOfWeek = args.header.start.getDayOfWeek();
                                    var dayOfMonth = args.header.start.getDayOfWeek();
                                    //if (args.header.start == DayPilot.Date.today()) {
                                    if(args.header.start <= DayPilot.Date.today() && DayPilot.Date.today() < args.header.end){
                                        args.header.fontColor  = "white";
                                        args.header.backColor  = "#D2D2D2";
                                        //args.header.fontSize = "35pt";
                
                                    }
                                    else if (dayOfWeek === 6 || dayOfWeek === 0) {
                
                                        args.header.backColor  = "#E2EFDA";
                                    }
                                    else{
                                        args.header.backColor  = "#EDEDED";
                                    }
                
                                };
                				
                                dp.timeHeaders = [
                                    { groupBy: "Month", format: "MMMM yyyy" },
                                    { groupBy: "Day", format: "d" },
                                    { groupBy: "Day", format: "dddd" },
                                ];
								
                                dp.eventHeight = 50;
                                dp.bubble = new DayPilot.Bubble({});
                                
                                dp.rowHeaderColumns = [
                                    {title: "Engine", width: 100},
                                    //{title: "Room", width: 80},
                                    //{title: "Status", width: 80}
                                ];
                                    
                                dp.onBeforeResHeaderRender = function(args) {
                                   var beds = function(count) {
                                        return count + " bed" + (count > 1 ? "s" : "");
                                    };
                
                                };
                
                                // http://api.daypilot.org/daypilot-scheduler-oneventmoved/
                                dp.onEventMoved = function (args) {
									var modal = new DayPilot.Modal();
									modal.closed = function() {
										dp.clearSelection();

										// reload all events
										var data = this.result;
										if (data && data.result === "OK") {
											loadEvents();
										}
									};									
                                    if ( args.newStart < DayPilot.Date.today() ) {
                                        swal({
                                                title: "Message",
                                                text: "Sorry !! Booking Events can not be moved in past dates",
                                                type: "warning"
                                            },
                                            function(){
                                                $( "#booking_calendar" ).load( "<?php echo base_url() ?>dashboard/calendar_load" );
                                            });
                                        return false;
                
									} else {
										modal.showUrl("<?php echo base_url();?>bookings/hotel_booking_move?id="+args.e.id()+"&newStart="+args.newStart.toString()+"&newEnd="+args.newEnd.toString()+"&newResource="+args.newResource+"");
										return false;
									}
                
                                    /*$.post("<?php echo base_url();?>bookings/booking_backend_move",
                                        {
                                            id: args.e.id(),
                                            newStart: args.newStart.toString(),
                                            newEnd: args.newEnd.toString(),
                                            newResource: args.newResource
                                        },
                                        function(data) {
                                           // dp.message(data.message);
                                           location.reload();
                                        });*/
                                };
                
                                // http://api.daypilot.org/daypilot-scheduler-oneventresized/
                                dp.onEventResized = function (args) {
                                    var modal = new DayPilot.Modal();
                                    modal.closed = function() {
                                        dp.clearSelection();
                
                                        // reload all events
                                        var data = this.result;
                                        if (data && data.result === "OK") {
                                            loadEvents();
                                        }
                                    };
                                    if ( !1 ) {
                                        swal({
                                                title: "Message",
                                                text: "Sorry !! Booking Events can not be resized in past dates",
                                                type: "warning"
                                            },
                                            function(){
                                                $( "#booking_calendar" ).load( "<?php echo base_url() ?>dashboard/calendar_load" );
                                            });
                
                                        return false;
                
                                    }
                                    else
                                    {
                                        modal.showUrl("<?php echo base_url();?>bookings/hotel_booking_resize?id="+args.e.id()+"&newStart="+args.newStart.toString()+"&newEnd="+args.newEnd.toString()+"");
                
                
                
                                    }
                
                                };
                
                                dp.onEventDeleted = function(args) {
                                    $.post("<?php echo base_url();?>bookings/booking_delete",
                                        {
                                            id: args.e.id()
                                        },
                                        function() {
                                            dp.message("Deleted.");
                                        });
                                };
                                dp.onTimeRangeSelected = function (args) {
                                    //var name = prompt("New event name:", "Event");
                                    //if (!name) return;
                
                                    var modal = new DayPilot.Modal();
                                    modal.closed = function() {
                
                                        dp.clearSelection();
                
                                        // reload all events
                                        var data = this.result;
                                        if (data && data.result === "OK") {
                                            loadEvents();
                                        }
                                    };
                                  
                
                
                
                                    if ( args.start >= DayPilot.Date.today() && DayPilot.Date.today() < args.end    )
                                    {
                
                                        $.ajax({
                                            type:"GET",
                                            url: "<?php echo base_url()?>bookings/clean_check?resource="+args.resource,
                                            data:{resource_id:'asd'},
                                            success:function(data)
                                            {
                                                if(data.say=="no") {
                                                    swal({
                                                        title: "Are you sure?",
                                                        text: "This room is not clean. Are you sure you want to take booking?",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "Yes, Take Booking!",
                                                        closeOnConfirm: true
                                                    }, function () {
                
                                                        modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
                
                
                                                    });
                                                }else if(data.say=="yes") {
                
                                                    modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
                
                
                
                                                }
                                            }
                                        });
                
                                        }
                                    else
                                    {
                                        swal({
                                                title: "Invalid Date",
                                                text: "No Booking Can be taken in past dates",
                                                type: "warning"
                                            },
                                            function(){
                                                $( "#booking_calendar" ).load( "<?php echo base_url() ?>dashboard/calendar_load" );
                                            });
                                        return false;
                                    }
                                };
                
                                dp.onEventClick = function(args) {
                                    var modal = new DayPilot.Modal();
                                    modal.closed = function() {
                                        // reload all events
                                        var data = this.result;
                                        if (data && data.result === "OK") {
                                            loadEvents();
                                        }
                                    };
                                    modal.showUrl("<?php echo base_url();?>bookings/hotel_edit_booking?id=" + args.e.id());
                                };
                              
                                dp.onRowClick=function(args){
                
                                    //alert(args.resource.id);
                
                                    var modal = new DayPilot.Modal();
                                    modal.closed = function() {
                                        // reload all events
                                        var data = this.result;
                                        if (data && data.result === "OK") {
                                            loadEvents();
                                        }
                                    };
                                    
                                    modal.showUrl("<?php echo base_url();?>bookings/pop_new_room?id=" + args.resource.id);
                
                
                
                
                                }
                
                                dp.onBeforeEventRender = function(args) {
                                    var start = new DayPilot.Date(args.e.start);
                                    var end = new DayPilot.Date(args.e.end);
                
                                    var today = new DayPilot.Date().getDatePart();
                
                                    //args.e.html = args.e.text + " (" + start.toString("M/d/yyyy") + " - " + end.toString("M/d/yyyy") + ")";
                                    args.e.html = args.e.cust_name;
                                    //args.e.html = args.e.html + "<br /><span style=''>" + args.e.cust_name + "</span>";
                                    switch (args.e.status) {
                                        case "7":
                                            /*var in2days = today.addDays(1);
                
                                             if (start.getTime() < in2days.getTime()) {
                                             args.e.barColor = args.e.bar_color_code;
                                             args.e.toolTip = args.e.booking_status;
                                             args.e.backColor = args.e.body_color_code;
                                             }
                                             else {*/
                                            args.e.barColor = args.e.bar_color_code;
                                            args.e.toolTip = args.e.booking_status;
                                            args.e.backColor = args.e.body_color_code;
                                            // }
                                            break;
                                        case "1":
                                            /*var in2days = today.addDays(1);
                
                                             if (start.getTime() < in2days.getTime()) {
                                             args.e.barColor = args.e.bar_color_code;
                                             args.e.toolTip = args.e.booking_status;
                                             args.e.backColor = args.e.body_color_code;
                                             }
                                             else {*/
                                            args.e.barColor = args.e.bar_color_code;
                                            if(args.e.booking_status_secondary!='') {
                                                args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
                                            }
                                            else {
                                                args.e.toolTip = args.e.booking_status;
                                            }
                                            args.e.backColor = args.e.body_color_code;
                                            // }
                                            break;
                                        case "2":
                                            /*var in2days = today.addDays(1);
                
                                             if (start.getTime() < in2days.getTime()) {
                                             args.e.barColor = args.e.bar_color_code;
                                             args.e.toolTip = args.e.booking_status;
                                             args.e.backColor = args.e.body_color_code;
                                             }
                                             else {*/
                                            args.e.barColor = args.e.bar_color_code;
                                            if(args.e.booking_status_secondary!='') {
                                                args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
                                            }
                                            else {
                                                args.e.toolTip = args.e.booking_status;
                                            }
                                            args.e.backColor = args.e.body_color_code;
                                            // }
                                            break;
                                        case "3":
                
                                            args.e.barColor = args.e.bar_color_code;
                                            args.e.toolTip = args.e.booking_status;
                                            args.e.backColor = args.e.body_color_code;
                
                                            break;
                                        case "5":
                                            args.e.barColor = args.e.bar_color_code;
                                            if(args.e.booking_status_secondary!='') {
                                                args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
                                            }
                                            else {
                                                args.e.toolTip = args.e.booking_status;
                                            }
                                            args.e.backColor = args.e.body_color_code;
                
                                            break;
                                        case "8":
                                            args.e.barColor = args.e.bar_color_code;
                                            args.e.toolTip = args.e.booking_status;
                                            args.e.backColor = args.e.body_color_code;
                
                                            break;
                                        case "4":
                                            /* var arrivalDeadline = today.addHours(18);
                
                                             if (start.getTime() < today.getTime() || (start.getTime() === today.getTime() && now.getTime() > arrivalDeadline.getTime())) { // must arrive before 6 pm
                                             args.e.barColor = args.e.bar_color_code;
                                             args.e.toolTip = args.e.booking_status;
                                             args.e.backColor = args.e.body_color_code;
                                             }
                                             else {*/
                                            args.e.barColor = args.e.bar_color_code;
                                            args.e.toolTip = args.e.booking_status;
                                            args.e.backColor = args.e.body_color_code;
                                            //args.e.children ='dqwd';
                                            //}
                                            break;
                                        /*case 'Arrived': // arrived
                                         var checkoutDeadline = today.addHours(10);
                
                                         if (end.getTime() < today.getTime() || (end.getTime() === today.getTime() && now.getTime() > checkoutDeadline.getTime())) { // must checkout before 10 am
                                         args.e.barColor = "#f41616";  // red
                                         args.e.toolTip = "Late checkout";
                                         }
                                         else
                                         {
                                         args.e.barColor = "#1691f4";  // blue
                                         args.e.toolTip = "Arrived";
                                         }
                                         break;*/
                                        case '6': // checked out
                                            args.e.barColor = args.e.bar_color_code;
                                            args.e.toolTip = args.e.booking_status;
                                            args.e.backColor = args.e.body_color_code;
                                            break;
                                        /*default:
                                         args.e.toolTip = "Unexpected state";
                                         break; */
                                    }
                
                                    args.e.html = args.e.html + "<br /><span style='color:gray'>" + args.e.toolTip + "</span>";
                
                                    var paid = args.e.paid;
                                    var paidColor = args.e.paid_color;
                
                                    args.e.areas = [
                                        //{ bottom: 10, right: 4, html: "<div style='color:" + paidColor + "; font-size: 8pt;'>Paid: " + paid + "</div>", v: "Visible"},
                                        { left: 4, bottom: 8, right: 4, height: 2, html: "<div style='background-color:" + paidColor + "; height: 100%; width:" + paid + "'></div>", v: "Visible" }
                                    ];
                
                                };
                
                
                                dp.init();
                
                                loadResources();
                                loadEvents();
                
                                function loadTimeline(date) {
                                    dp.scale = "Manual";
                                    dp.timeline = [];
                                    var start = date.getDatePart().addHours(0);
                
                                    for (var i = 0; i < dp.days; i++) {
                                        dp.timeline.push({start: start.addDays(i), end: start.addDays(i+1)});
                                    }
                                    dp.update();
                                }
                
                                function loadEvents() {
                                    var start = dp.visibleStart();
                                    var end = dp.visibleEnd();
                
                                    $.post("<?php echo base_url();?>bookings/hotel_backend_events",
                                        {
                                            start: start.toString(),
                                            end: end.toString()
                                        },
                                        function(data) {
                                            dp.events.list = data;
                                            dp.update();
                                        }
                                    );
                                }
                                dp.treeEnabled = true;
                                function loadResources() {
                                    $.post("<?php echo base_url();?>bookings/hotel_backend_rooms_tree",
                                        { capacity: $("#filter").val() },
                                        function(data) {                                   
                                            dp.resources = data;
                                            dp.update();
                                        });
                                }
                
                             
                
                                $(document).ready(function() {
                
                                //  $( "#booking_calendar").load( "<?php echo base_url() ?>dashboard/calendar_load" );
                                    $("#filter1").change(function() {
                
                                        $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_unit_tree",
                                            { unit_id: $("#filter1").val() },
                                            function(data) {
                                                dp.resources = data;
                                                dp.update();
                                                $('#filter2').prop('selectedIndex',0);
                                                $('#filter3').prop('selectedIndex',0);
                                                $('#filter4').prop('selectedIndex',0);
                                            });
                                    });
                
                                    $("#filter2").change(function() {
                
                                        $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_unittype_tree",
                                            { unit_type: $("#filter2").val() },
                                            function(data) {
                                                dp.resources = data;
                                                dp.update();
                                                $('#filter1').prop('selectedIndex',0);
                                                $('#filter3').prop('selectedIndex',0);
                                                $('#filter4').prop('selectedIndex',0);
                                            });
                                    });
                                    $("#filter3").change(function() {
                
                                        $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_unitclass_tree",
                                            { unit_class: $("#filter3").val() },
                                            function(data) {
                                                dp.resources = data;
                                                dp.update();
                                                $('#filter2').prop('selectedIndex',0);
                                                $('#filter1').prop('selectedIndex',0);
                                                $('#filter4').prop('selectedIndex',0);
                                            });
                                    });
                                    $("#filter4").change(function() {
                
                                        $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_roombed_tree",
                                            { no_bed: $("#filter4").val() },
                                            function(data) {
                                                dp.resources = data;
                                                dp.update();
                                                $('#filter2').prop('selectedIndex',0);
                                                $('#filter3').prop('selectedIndex',0);
                                                $('#filter1').prop('selectedIndex',0);
                                            });
                                    });
                                    $("#filter_room_no").keyup(function() {
                                        var no=$("#filter_room_no").val();
                                        if(no !="") {
                                            $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_roomno_tree",
                                                {room_no: $("#filter_room_no").val()},
                                                function (data) {
                                                    dp.resources = data;
                                                    dp.update();
                                                    $('#filter2').prop('selectedIndex', 0);
                                                    $('#filter3').prop('selectedIndex', 0);
                                                    $('#filter1').prop('selectedIndex', 0);
                                                    $('#filter4').prop('selectedIndex', 0);
                                                });
                                        }else{
                
                                            //normal distribution --> start
                
                                            $.post("<?php echo base_url();?>bookings/hotel_backend_rooms_tree",
                                                { capacity: $("#filter").val() },
                                                function(data) {
                                                    // alert(JSON.stringify(data));
                                                    /*   dp.resources = [
                                                     { name: data.unit_name, id: "Tools", expanded: true, children:[
                                                     { name : "Tool 1", id : "Tool1" },
                                                     { name : "Tool 2", id : "Tool2" }
                                                     ]
                                                     },
                                                     ];*/
                                                    dp.resources = data;
                                                    dp.update();
                                                });
                
                                            //normal distribution -->end
                
                
                
                                        }  });
                                });
                
                            </script> 
                      </div>
                    </form>
                  </div>
                </div>
			</div>
		</div>
	</div>
</div>

<script>
jQuery(document).ready(function() {       
Demo.init(); // init demo features  // set current page
   ComponentsIonSliders.init();
});
    function price_range(){
        var range=document.getElementById("range_1").value;

        $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_roomprice_tree",
            { range: range },
            function(data) {
                dp.resources = data;
                dp.update();
            });
       // alert(range);

    }
</script>