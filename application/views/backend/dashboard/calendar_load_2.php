<script src="<?php echo base_url();?>assets/global/plugins/daypilot/js/daypilot/daypilot-all.min.js" type="text/javascript"></script>
<div id="home">
  <div class="row">
    <div class="col-md-12">
      <div id="booking_calendar">
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-green"> <i class="icon-pin font-green"></i> <span class="caption-subject bold uppercase">Reservation Management</span> </div>
          </div>
          <div class="portlet-body form">
            <form role="form">
              <div class="form-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="clearfix" style="margin-bottom:35px;">
                      <div class="btn-group" data-toggle="buttons">
                        <input type="hidden" id="hidden_field1" value="">
                        <label class="btn default green-stripe">
                          <input id="5" type="checkbox" onchange="calendar()" class="toggle">
                          Detailed View </label>
                        </a>
                        <label class="btn blue dark-stripe" style="margin-left:10px;">
                          <input id="7" type="checkbox" onchange="calendar2()" class="toggle">
                          Detailed View 2 </label>
                        <label class="btn default yellow-stripe" style="margin-left:10px;">
                          <input id="6" type="checkbox" onchange="snapshot()" class="toggle">
                          Snapshot View </label>
                      </div>
                    </div>
                  </div>
                  <script type="text/javascript">
                                   function calendar(){
                                     window.location="<?php echo base_url(); ?>dashboard/add_booking_calendar";
                                   }
                                   function calendar2(){
                                     window.location="<?php echo base_url(); ?>dashboard/calendar_load_2";
                                   }
                                   function snapshot(){
                                     window.location="<?php echo base_url(); ?>dashboard/snapshot_load";
                                   }
                               </script>
                  <div class="col-md-12">
                    <div class="form-inline" style="margin-bottom:35px;">
                      <div class="form-group form-md-line-input">
                        <input id="filter_room_no" type="text" class="form-control" placeholder="Search Room">
                        </input>
                        <label></label>
                        <span class="help-block">Search Room</span> </div>
                      <div class="btn-group pull-right">
                        <label for="autocellwidth" class="auto_cl btn grey-cascade" id="auto_cl_id">
                          <input type="checkbox" id="autocellwidth" class="toggle" style="display:none;">
                          <span id="disp">Expanded View</span></label>
                      </div>
                    </div>
                  </div>
                </div>
                <script type="text/javascript">
                                    $(document).ready(function() {
                                        //dp.cellWidth = 100;
                                        //dp.cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                                        //dp.update();
                                        //dp.cell.enabled = false;
                                        /*if (start.getTime() < today.getTime())
                                         { dp.Cell.Enabled = false; e.Day.IsSelectable = false; }*/
                                        //alert(start.getTime());
            
                                    });
                                    var picker = new DayPilot.DatePicker({
                                        target: 'start',
                                        pattern: 'M/d/yyyy',
                                        date: new DayPilot.Date().firstDayOfMonth(),
                                        onTimeRangeSelected: function(args) {
                                            //dp.startDate = args.start;
                                            loadTimeline(args.start);
                                            loadEvents();
                                        }
                                    });
            
                                    $("#timerange").change(function() {
                                        switch (this.value) {
                                            case "week":
                                                dp.days = 7;
                                                break;
                                            case "2weeks":
                                                dp.days = 14;
                                                break;
                                            case "month":
                                                dp.days = dp.startDate.daysInMonth();
                                                break;
                                            case "2months":
                                                dp.days = dp.startDate.daysInMonth() + dp.startDate.addMonths(1).daysInMonth();
                                                break;
                                        }
                                        loadTimeline(DayPilot.Date.today());
                                        loadEvents();
                                    });
            
                                    $("#autocellwidth").click(function() {
										dp.cellWidth = 100;  // reset for "Fixed" mode
										dp.cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
										document.getElementById('auto_cl_id').style.backgroundColor = $(this).is(":checked") ? "#00CC99" : "#A5A5A5";
										var a = $(this).is(":checked") ? "Compact View" : "Expanded View";
									
										$('#disp').text(a);
										dp.update();
				
									});	
                                </script>
                <div style="position:relative;">
                  <div class="bok">Booking</div>
                  <div id="dp"> </div>
                </div>
                <script>
                                    var dp = new DayPilot.Scheduler("dp");
            
                                    dp.allowEventOverlap = false;
                                    dp.borderColor = "red";
            
                                    //dp.scale = "Day";
                                    //dp.startDate = new DayPilot.Date().firstDayOfMonth();
                                    dp.days = dp.startDate.daysInMonth();
                                    //loadTimeline(DayPilot.Date.today(-3));
                                    loadTimeline(DayPilot.Date.today().addDays(-3));
            
                                    dp.eventDeleteHandling = "Update";
            
                                    dp.onBeforeTimeHeaderRender = function(args) {
                                        // alert(DayPilot.Date.today() +"sas" +args.header.end);
                                        //args.header.start.setHours(15);
                                        //DayPilot.Date.addHours(10);
                                        var d = new Date();
                                        var dayOfWeek = args.header.start.getDayOfWeek();
                                        var dayOfMonth = args.header.start.getDayOfWeek();
                                        //if (args.header.start == DayPilot.Date.today()) {
                                        if(args.header.start <= DayPilot.Date.today().addHours(10) && DayPilot.Date.today().addHours(10) < args.header.end){
                                            args.header.fontColor  = "white";
                                            args.header.backColor  = "#33D0E1";
                                            //args.header.fontSize = "35pt";
            
                                        }
                                        else if (dayOfWeek === 6 || dayOfWeek === 0) {
            
                                            args.header.backColor  = "#E2EFDA";
                                        }
                                        else{
                                            args.header.backColor  = "#EDEDED";
                                        }
            
                                    };
            
                                    dp.timeHeaders = [
                                        { groupBy: "Month", format: "MMMM yyyy" },
                                        { groupBy: "Day", format: "d" },
                                        { groupBy: "Day", format: "dddd" },
                                    ];
            
                                    dp.eventHeight = 50;
                                    dp.bubble = new DayPilot.Bubble({});
            
                                    dp.rowHeaderColumns = [
                                        {title: "Engine", width: 100},
                                        //{title: "Room", width: 80},
                                        //{title: "Status", width: 80}
                                    ];
            
                                    dp.onBeforeResHeaderRender = function(args) {
                                        var beds = function(count) {
                                            return count + " bed" + (count > 1 ? "s" : "");
                                        };
            
                                    };
            
                                    // http://api.daypilot.org/daypilot-scheduler-oneventmoved/
                                    dp.onEventMoved = function (args) {
                                        if ( args.newStart < DayPilot.Date.today() ) {
                                            swal({
                                                    title: "Message",
                                                    text: "Sorry !! Booking Events can not be moved in past dates",
                                                    type: "warning"
                                                },
                                                function(){
                                                    $( "#booking_calendar" ).load( "<?php echo base_url() ?>dashboard/calendar_load" );
                                                });
                                            return false;
            
                                        }
            
                                        $.post("<?php echo base_url();?>bookings/booking_backend_move",
                                            {
                                                id: args.e.id(),
                                                newStart: args.newStart.toString(),
                                                newEnd: args.newEnd.toString(),
                                                newResource: args.newResource
                                            },
                                            function(data) {
                                                dp.message(data.message);
                                            });
                                    };
            
                                    // http://api.daypilot.org/daypilot-scheduler-oneventresized/
                                    dp.onEventResized = function (args) {
                                        var modal = new DayPilot.Modal();
                                        modal.closed = function() {
                                            dp.clearSelection();
            
                                            // reload all events
                                            var data = this.result;
                                            if (data && data.result === "OK") {
                                                loadEvents();
                                            }
                                        };
            
            
            
            
            
                                        if ( args.newStart < DayPilot.Date.today() ) {
                                            swal({
                                                    title: "Message",
                                                    text: "Sorry !! Booking Events can not be resized in past dates",
                                                    type: "warning"
                                                },
                                                function(){
                                                    $( "#booking_calendar" ).load( "<?php echo base_url() ?>dashboard/calendar_load" );
                                                });
            
                                            return false;
            
                                        }
                                        else
                                        {
                                            modal.showUrl("<?php echo base_url();?>bookings/hotel_booking_resize?id="+args.e.id()+"&newStart="+args.newStart.toString()+"&newEnd="+args.newEnd.toString()+"");
            
            
            
                                        }
            
                                    };
            
                                    dp.onEventDeleted = function(args) {
                                        $.post("<?php echo base_url();?>bookings/booking_delete",
                                            {
                                                id: args.e.id()
                                            },
                                            function() {
                                                dp.message("Deleted.");
                                            });
                                    };
            
            
            
            
                                    //cell rendering
            
                                    dp.onBeforeCellRender = function(args) {
                                        if (args.cell.start <= DayPilot.Date.today() && DayPilot.Date.today() < args.cell.end)
                                        {
                                            args.cell.backColor = "#EAFAFC";
                                        }
                                        if (args.cell.start < DayPilot.Date.today())
                                        {
                                            args.cell.backColor = "#f5f5dc";
                                        }
                                        else
                                        {
                                            var dayOfWeek = args.cell.start.getDayOfWeek();
                                            if (dayOfWeek === 6 || dayOfWeek === 0) {
                                                args.cell.backColor = "#F5F8EE";
                                            }
                                        }
            
                                        if (args.cell.resource === "unit") {
            
                                            args.cell.backColor = "#EEEEEE";
                                            // args.cell.html += " (loaded dynamically)";
                                            //args.cell.left.enabled = false;
                                            //args.cell.right.enabled = false;
                                            // args.right.html = "You can't create an event here";
            
                                            args.cell.allowed = false;
            
                                        }
                                    };
            
            
            
            
            
            
            
            
                                    // event creating
                                    // http://api.daypilot.org/daypilot-scheduler-ontimerangeselected/
                                    dp.onTimeRangeSelected = function (args) {
                                        //var name = prompt("New event name:", "Event");
                                        //if (!name) return;
            
                                        var modal = new DayPilot.Modal();
                                        modal.closed = function() {
                                            dp.clearSelection();
            
                                            // reload all events
                                            var data = this.result;
                                            if (data && data.result === "OK") {
                                                loadEvents();
                                            }
                                        };
                                       
            
                                        if ( args.start >= DayPilot.Date.today() && DayPilot.Date.today() < args.end    )
                                        {
                                            //args.cell.backColor = "#EAFAFC";
                                            //}
                                            //alert(args.resource);
                                            modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
                                        }
                                        else
                                        {
                                            swal({
                                                    title: "Invalid Date",
                                                    text: "No Booking Can be taken in past dates",
                                                    type: "warning"
                                                },
                                                function(){
                                                    $( "#booking_calendar" ).load( "<?php echo base_url() ?>dashboard/calendar_load" );
                                                });
                                            return false;
                                        }
                                    };
            
                                    dp.onEventClick = function(args) {
                                        var modal = new DayPilot.Modal();
                                        modal.closed = function() {
                                            // reload all events
                                            var data = this.result;
                                            if (data && data.result === "OK") {
                                                loadEvents();
                                            }
                                        };
                                        modal.showUrl("<?php echo base_url();?>bookings/hotel_edit_booking?id=" + args.e.id());
                                    };
            
                                    
            
                                    dp.onBeforeEventRender = function(args) {
                                        var start = new DayPilot.Date(args.e.start);
                                        var end = new DayPilot.Date(args.e.end);
            
                                        var today = new DayPilot.Date().getDatePart();
            
                                        //args.e.html = args.e.text + " (" + start.toString("M/d/yyyy") + " - " + end.toString("M/d/yyyy") + ")";
                                        args.e.html = "";//args.e.cust_name;
                                        //args.e.html = args.e.html + "<br /><span style=''>" + args.e.cust_name + "</span>";
                                        switch (args.e.status) {
                                            case "7":
                                                /*var in2days = today.addDays(1);
            
                                                 if (start.getTime() < in2days.getTime()) {
                                                 args.e.barColor = args.e.bar_color_code;
                                                 args.e.toolTip = args.e.booking_status;
                                                 args.e.backColor = args.e.body_color_code;
                                                 }
                                                 else {*/
                                                args.e.barColor = args.e.bar_color_code;
                                                args.e.toolTip = args.e.booking_status;
                                                args.e.backColor = args.e.body_color_code;
                                                // }
                                                break;
                                            case "1":
                                                /*var in2days = today.addDays(1);
            
                                                 if (start.getTime() < in2days.getTime()) {
                                                 args.e.barColor = args.e.bar_color_code;
                                                 args.e.toolTip = args.e.booking_status;
                                                 args.e.backColor = args.e.body_color_code;
                                                 }
                                                 else {*/
                                                args.e.barColor = args.e.bar_color_code;
                                                if(args.e.booking_status_secondary!='') {
                                                    args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
                                                }
                                                else {
                                                    args.e.toolTip = args.e.booking_status;
                                                }
                                                args.e.backColor = args.e.body_color_code;
                                                // }
                                                break;
                                            case "2":
                                                /*var in2days = today.addDays(1);
            
                                                 if (start.getTime() < in2days.getTime()) {
                                                 args.e.barColor = args.e.bar_color_code;
                                                 args.e.toolTip = args.e.booking_status;
                                                 args.e.backColor = args.e.body_color_code;
                                                 }
                                                 else {*/
                                                args.e.barColor = args.e.bar_color_code;
                                                if(args.e.booking_status_secondary!='') {
                                                    args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
                                                }
                                                else {
                                                    args.e.toolTip = args.e.booking_status;
                                                }
                                                args.e.backColor = args.e.body_color_code;
                                                // }
                                                break;
                                            case "3":
            
                                                args.e.barColor = args.e.bar_color_code;
                                                args.e.toolTip = args.e.booking_status;
                                                args.e.backColor = args.e.body_color_code;
            
                                                break;
                                            case "5":
                                                args.e.barColor = args.e.bar_color_code;
                                                if(args.e.booking_status_secondary!='') {
                                                    args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
                                                }
                                                else {
                                                    args.e.toolTip = args.e.booking_status;
                                                }
                                                args.e.backColor = args.e.body_color_code;
            
                                                break;
                                            case "8":
                                                args.e.barColor = args.e.bar_color_code;
                                                args.e.toolTip = args.e.booking_status;
                                                args.e.backColor = args.e.body_color_code;
            
                                                break;
                                            case "4":
                                                /* var arrivalDeadline = today.addHours(18);
            
                                                 if (start.getTime() < today.getTime() || (start.getTime() === today.getTime() && now.getTime() > arrivalDeadline.getTime())) { // must arrive before 6 pm
                                                 args.e.barColor = args.e.bar_color_code;
                                                 args.e.toolTip = args.e.booking_status;
                                                 args.e.backColor = args.e.body_color_code;
                                                 }
                                                 else {*/
                                                args.e.barColor = args.e.bar_color_code;
                                                args.e.toolTip = args.e.booking_status;
                                                args.e.backColor = args.e.body_color_code;
                                                //args.e.children ='dqwd';
                                                //}
                                                break;
                                            /*case 'Arrived': // arrived
                                             var checkoutDeadline = today.addHours(10);
            
                                             if (end.getTime() < today.getTime() || (end.getTime() === today.getTime() && now.getTime() > checkoutDeadline.getTime())) { // must checkout before 10 am
                                             args.e.barColor = "#f41616";  // red
                                             args.e.toolTip = "Late checkout";
                                             }
                                             else
                                             {
                                             args.e.barColor = "#1691f4";  // blue
                                             args.e.toolTip = "Arrived";
                                             }
                                             break;*/
                                            case '6': // checked out
                                                args.e.barColor = args.e.bar_color_code;
                                                args.e.toolTip = args.e.booking_status;
                                                args.e.backColor = args.e.body_color_code;
                                                break;
                                            /*default:
                                             args.e.toolTip = "Unexpected state";
                                             break; */
                                        }
            
                                        args.e.html ="";// args.e.html + "<br /><span style='color:gray'>" + args.e.toolTip + "</span>";
            
                                        var paid = args.e.paid;
                                        var paidColor = args.e.paid_color;
            
                                        args.e.areas = [
                                            //{ bottom: 10, right: 4, html: "<div style='color:" + paidColor + "; font-size: 8pt;'>Paid: " + paid + "</div>", v: "Visible"},
                                            { left: 4, bottom: 8, right: 4, height: 2, html: "<div style='background-color:" + paidColor + "; height: 100%; width:" + paid + "'></div>", v: "Visible" }
                                        ];
            
                                    };
            
            
                                    dp.init();
            
                                    loadResources();
                                    loadEvents();
            
                                    function loadTimeline(date) {
                                        dp.scale = "Manual";
                                        dp.timeline = [];
                                        var start = date.getDatePart().addHours(0);
            
                                        for (var i = 0; i < dp.days; i++) {
                                            dp.timeline.push({start: start.addDays(i), end: start.addDays(i+1)});
                                        }
                                        dp.update();
									dp.cellWidth = 100;
									dp.cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                                    }
            
                                    function loadEvents() {
                                        var start = dp.visibleStart();
                                        var end = dp.visibleEnd();
            //alert('here');
                                        /*$.post("<?php echo base_url();?>bookings/hotel_backend_events",
                                            {
                                                start: start.toString(),
                                                end: end.toString()
                                            },
                                            function(data) {
                                                dp.events.list = data;
                                                dp.update();
                                            }
                                        );*/
									var st= start.toString();
									var ed= end.toString();					
								//console.log(start+' | '+end);
								dp.events.load("<?php echo base_url();?>bookings/hotel_backend_events?st="+st+" & ed="+ed);
                                    }
                                    dp.treeEnabled = true;
                                    function loadResources() {
										dp.resources=[];
                                        /*$.post("<?php echo base_url();?>bookings/hotel_backend_rooms_tree_2",
                                            { capacity: $("#filter").val() },
                                            function(data) {
                                                // alert(JSON.stringify(data));
                                                /*   dp.resources = [
                                                 { name: data.unit_name, id: "Tools", expanded: true, children:[
                                                 { name : "Tool 1", id : "Tool1" },
                                                 { name : "Tool 2", id : "Tool2" }
                                                 ]
                                                 },
                                                 ];*/
                                                /*dp.resources = data;
                                                dp.update();
                                            });*/
											dp.rows.load("<?php echo base_url();?>bookings/hotel_backend_rooms_tree_2");
                                    }
            
                                    $(document).ready(function() {
                                        $("#filter_room_no").keyup(function() {
										var no=$("#filter_room_no").val();
										if(no !="") {
											$.post("<?php echo base_url();?>bookings/hotel_backend_onchange_roomno_tree",
												{room_no: $("#filter_room_no").val()},
												function (data) {
													dp.resources = data;
													dp.update();
													$('#filter2').prop('selectedIndex', 0);
													$('#filter3').prop('selectedIndex', 0);
													$('#filter121').prop('selectedIndex', 0);
													$('#filter4').prop('selectedIndex', 0);
												});
										}else{
				
											//normal distribution --> start
				                            dp.resources=[];
											/*$.post("<?php echo base_url();?>bookings/hotel_backend_rooms_tree",
												{ capacity: $("#filter").val() },
												function(data) {
													// alert(JSON.stringify(data));
													/*   dp.resources = [
													 { name: data.unit_name, id: "Tools", expanded: true, children:[
													 { name : "Tool 1", id : "Tool1" },
													 { name : "Tool 2", id : "Tool2" }
													 ]
													 },
													 ];*/
													/*dp.resources = data;
													dp.update();
												});*/
				
											//normal distribution -->end
				                            dp.rows.load("<?php echo base_url();?>bookings/hotel_backend_rooms_tree_2"); 
				
				
										}   
									});
                                    });
            
                                </script> 
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
