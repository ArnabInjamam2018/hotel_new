<?php
/*echo "<pre>";
print_r($data);
echo "</pre>";
exit;*/
?>
    <div class="portlet light borderd">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-ban" aria-hidden="true"></i> <strong> CANCELLED BOOKINGS REPORT</strong> </div>
        <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            <div class="col-md-2">
              <div class="btn-group"> <a href="<?php echo base_url();?>dashboard/all_bookings">
                <button class="btn green"> All Bookings <i class="fa fa-table"></i> </button>
                </a> </div>
            </div>
            <div class="col-md-8">
              <?php
		//$daterange='daterange';
	                            $form = array(
	                                'class'       => 'form-inline ftop',
	                                'id'        => 'form_date',
	                                'method'      => 'post'
	                            );

	                            echo form_open('reports/cancel_report_by_date',$form);

	                            ?>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
              </div>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" id="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
              </div>
			
			  <div class="form-group">
              <button  class="btn btn-default" type="submit" ><i class="fa fa-search" aria-hidden="true"></i></button>
			  </div>
			 
                  
              <?php form_close(); ?>
            </div>
            <script type="text/javascript">
				/*function check_sub(){
				   document.getElementById('form_date').submit();
				}*/
			</script>
            
          </div>
        </div>
		
			      
				   
			
       <table class="table table-striped table-bordered table-hover" id="sample_3">
        <!--<table class="table table-striped">-->
          <thead>
            <tr>
              <th width="5%"> # </th>
              <th width="10%"> Booking ID </th>
			  <th width="10%"> Cancellation Date </th>
			  <th class="none"> Cancellation Reason </th>			  
              <th> Last Status </th>
           <!--   <th align="center"> Units </th>-->
			  <th align="center"> PAX</th>
              <th> Source </th>
              <th> Nature</th>
              <th width="10%"> Reservation Span </th>			  
			  <th> Guest Name </th>
			  
			  <th class="none"> Guest Refund </th>
			  <th> Cancelation Charge </th>
            </tr>
          </thead>
		  
		  
          <tbody>
            <?php if(isset($bookings) && $bookings){
						//echo "<pre>";
						//print_r($bookings); exit;
						$i=1;
						$j=0;
                        $deposit=0; 
						$item_no=0;
						$msg="";
                        foreach($bookings as $booking){
						
						$j++;
		?>
         <?php
		 if($booking->type == 0){
			 $booking_id='BK0'.$this->session->userdata('user_hotel').'00'.$booking->bid;
			 
		 }
		 else{
			 $grp_id='BKGRP0'.$this->session->userdata('user_hotel').'00'.$booking->bid;
		 }
								
								
									//$book_id=$booking->booking_id;
									
									if($booking->c_reason == ''){
										$color = '';
										
									}
									else
										$color = '#ffb9de';
									
									if($booking->bid_previous == '4'){
										$color = '#F1EDA5';
									}
									
								
		 ?>
									
          <tr style="background-color:<?php echo $color;?>;">
		  
            <td><?php echo $j; ?></td>
			
            <td>
				<?php 
					//echo $booking_id.'<br/>'; 
					if($booking->type=0)
						{echo "<i style='color:#F8681A;' class='fa fa-users' aria-hidden='true'></i> <a>".$booking->bid.'</a> (<a>'.$booking->bid.'</a>)';}
					else 
						{echo "<i style='color:#023358;' class='fa fa-user' aria-hidden='true'></i> <a>".$booking->bid."</a>";}
				?>
			</td>
			
			<td>
				<?php
				if(isset($booking->cnd) && $booking->cnd!=''){
					echo date("jS F Y",strtotime($booking->cnd));
				}else{
					
					echo "<span style='color:#AAA;'>No info</span>";
				}
				?>
			</td>
			
			<td>
				<?php 
					if(isset ($booking->c_reason) && $booking->c_reason){ 
					
						$bcol = '#FFFFFF';
						$col = '#000000';
						if($booking->c_reason == 'No Show'){
							$bcol = '#E32016';
							$col = '#FFFFFF';
						}
						
						echo '<span class="label" style="background-color: '.$bcol.';color:'.$col.';">'.$booking->c_reason.'</span>';
					}
					else
						{echo "<span style='color:#AAA;'>No info</span>";} 
				?>
			</td>
			
			<td>
			<?php
			if(isset($booking->bid_previous) && $booking->bid_previous!=''){
				if($booking->bid_previous == 0 || $booking->bid_previous==null){
				?>
				<span class="label" style="background-color:#9B5C72; color:#F9F9F9;">
				  Unknown Status
				</span>
				<?php
			}
			else{
				$statusrow = $this->dashboard_model->get_booking_status($booking->bid_previous);
				?>
				<span class="label" style="background-color:<?php if(isset($statusrow->bar_color_code)){echo $statusrow->bar_color_code;}?>; color:<?php 
					if(isset($statusrow->body_color_code)){
						echo $statusrow->body_color_code;
					}
					
				  ?>;"> 
				  <?php 
				  
				  echo $statusrow->booking_status;
				  ?>
				</span>
			<?php	
			}
						}
			  ?>
			</td>
			
			<!--<td>-->
				<?php 
					/*$room_no=$this->dashboard_model->get_room_no($booking->room_id);
					//print_r($room_no);
					if(isset ($room_no) && $room_no){
					echo $room_no->room_no;}*/
				?>
			<!--</td>-->
			
			<td><?php if(isset ($booking->nog) && $booking->nog){ echo $booking->nog;} ?></td>
			
			<td><?php if(isset ($booking->bsource) && $booking->bsource){ echo $booking->bsource;} ?></td>
			
		   <td><?php if(isset ($booking->nature) && $booking->nature){ echo $booking->nature; }?></td>
		   
           <td>
				<?php 
						$from=$booking->fdate;
						$to=$booking->tdate;
						$days = (strtotime($to)- strtotime($from))/24/3600;
				echo date("dS-M-Y",strtotime($booking->fdate));?>
				<div style="color:#1F897F;">-to-</div>
				<?php 
				
				echo date("dS-M-Y",strtotime($booking->tdate));?>
			</td>
			
			
			<td>
				<?php 
				$guest=$this->dashboard_model->get_guest_detail($booking->gid);
				//print_r($guest);
				if(isset ($guest) && $guest){
					foreach($guest as $g){
						
						echo $g->g_name;
						
					}
				}
				/*
				if($g->g_address!='' && $g->g_city!='' &&  $g->g_state!='' && $g->g_country!='' && $g->g_pincode!=''){
					
					echo "<td>";				
					echo $g->g_address;
					echo ",";
					echo $g->g_city;
					echo ",";
					echo $g->g_state;
					echo ",";
					echo $g->g_country;
					echo ",";
					echo $g->g_pincode;
					echo "</td>";
					
				}else{
					
					echo "<td>";
					echo 'Not Specified';
					echo "</td>";
				}
				
				echo "<td>";
				echo $g->g_contact_no;
				echo "</td>";
				}
				}
				else{
					
					echo "<td>";
				echo "No Info";
				echo "</td>";
				echo "<td>";
				echo "No Info";
				echo "</td>";
				echo "<td>";
				echo "No Info";
				echo "</td>";
					
				}
				*/?>
			</td>	
			<td>
			<?php
			
			if($booking->type == 0){
			$transaction_row  = $this->dashboard_model->get_transaction_refund_single($booking->bid);
			 
		 }
		 else{
			 $transaction_row  = $this->dashboard_model->get_transaction_refund_by_grp($booking->bid);
		 }
			

			
			if(isset($transaction_row) && $transaction_row!=''){
				
				echo $transaction_row->t_amount; 
			}
			else{
				
				echo "N/A";
			}
			?>
			
			</td>
			<td><?php
			
			if($booking->type == 0){
			$transaction_row  = $this->dashboard_model->get_transaction_cancel($booking->bid);
			 
		 }
		 else{
			 $transaction_row  = $this->dashboard_model->get_transaction_cancel_by_grp($booking->bid);
		 }
			

			
			if(isset($transaction_row) && $transaction_row!=''){
				
				echo $transaction_row->t_amount; 
			}
			else{
				
				echo "N/A";
			}
			?>
			</td>
		 </tr>
		  
		   
			<?php $i++; ?>
          
          <?php  
			}}
		  ?>
         
            </tbody>
          
        </table>
      
  
    </div>
    </div>

<script>

   function fetch_data(val){
	   alert()
	   $.ajax({
                
                url: "<?php echo base_url()?>dashboard/get_no_tax_booking",
				success:function(data)
                { 
                   $('#table1').html(data);
				   $('#dataTable2').dataTable( {
						"pageLength": 10 
					} );
                 }
            });
   }
   
					
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            //alert(id);
		$.ajax({
                
                url: "<?php echo base_url()?>dashboard/soft_delete_booking?id="+id,
				type:"POST",
                data:{id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });

        });
    }
	
function download_pdf(booking_id) {
	
	
	
	//alert(booking_id);
	$("#dwn_link").attr("href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id);
	var f = $("#f");
            $.post(f.attr("action"), f.serialize(), function (result) {
            
                close(eval(result));
            });
	 //return false;
	
}
	
	
</script> 

<script>

$( document ).ready(function() {
	$("#select2_sample_modal_5").select2({
		
	/*	data: [
		 <?php $admins = $this->dashboard_model->fetch_admin();
					foreach($admins as $adm){?>
    {
      id: '<?php echo $adm->admin_id; ?>',
      text: '<?php echo $adm->admin_first_name." ".$adm->admin_last_name; ?>'
    },
	<?php } ?>
	]*/
		 
          /* tags: [ <?php $admins = $this->dashboard_model->fetch_admin();
					foreach($admins as $adm)
					{echo '"'.$adm->admin_id.'",';}?>]*/
        });
});

function get_val()
{
	$("#admin").val($("#select2_sample_modal_5").val());
	
	var t_dt_frm = $('#t_dt_frm').val();
	var t_dt_to = $('#t_dt_to').val();
	//alert($("#admin").val());
	
	/*$.ajax({
		
		url:"<?php echo base_url(); ?>dashboard/cancel_report",
		type:"POST",
		data:{t_dt_frm:t_dt_frm,t_dt_to:t_dt_to},
		success:function(data){
			
			
			
		}*/
		
}

</script>
<!-- END CONTENT --> 
