

      
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption"> <i class="fa fa-file-archive-o"></i>Cancelled Reservation Report </div>
          <!--
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
                --> 
        </div>
        <div class="portlet-body">
          <div class="table-toolbar">
            <div class="row">
              <div class="col-md-10">
                <div class="row">
                  <?php

                            $form = array(
                                'class'       => 'form-inline',
                                'id'        => 'form_date',
                                'method'      => 'post'
                            );

                            echo form_open_multipart('dashboard/',$form);

                            ?>
				      <div class="col-md-7">
                      <div class="form-group">
                        <input type="text" autocomplete="off" required="required" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date">
                      </div>
                      <div class="form-group">
                        <input type="text" autocomplete="off" required="required" name="t_dt_to" class="form-control date-picker" placeholder="End Date">
                      </div>
					   
                    <button class="btn btn-default" onclick="check_sub()" type="submit">Search</button>
                  </div>
                 
                  <?php form_close(); ?>
                </div>
              </div>
              <div class="col-md-2">
                <div class="btn-group pull-right">
                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                  <ul class="dropdown-menu pull-right">
                    <li> <a href="javascript:window.print();"> Print </a> </li>
                    <li> <a href="<?php echo base_url();?>dashboard/pdf_reports_financial" target="_blank"> Save as PDF </a> </li>
                    <!--<li> <a href="javascript:;"> Export to Excel </a> </li>-->
                  </ul>
                </div>
              </div>
            </div>
          </div>
         <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
              <tr> 
                <th> # </th>
				<th> Res# </th>
				<th> Group </th>
                <th> Guest Name</th>
                <th> Room No</th>
                <th> Room Type </th>
                <th> check In</th>
                <th> check Out</th>
                <th> Night</th>
                <th> Cancelled On </th>
				<th> Booking Amount</th>
                <th> Cancelled Charge</th>
			    <th> Other Charged</th>
			    <th> Amount Paid</th>
				<th> Balance</th>
				</tr>
            </thead>
            
          <tbody>
            
             
              <?php 
			if(isset($reports)){
				$srl_no=0;
				$srl_no1=0;
				foreach($reports as $report){
				//print_r($report);
				$srl_no++;
				$group_id=$report->group_id;
				
				$group_details=$this->dashboard_model->get_group_detalis($group_id);
				if(isset($group_details)){
				
				foreach($group_details as $group){
				$srl_no1++;
				print_r($group);
			    $from_days=$group->start_date;
				$end=$group->end_date;
				$days = (strtotime($end) - strtotime($from_days)) / (60 * 60 * 24);
				
		?>		
		<tr>
			<td><?php echo $srl_no.".".$srl_no1;?></td>
			<td><?php echo $group->name;?></td>
			<td>N/A</td>
			<td><?php echo $group->number_room;?></td>
			<td><?php echo $days;?></td>
			<td>fgjhnfgt</td>
			<td><?php echo $group->number_guest;?></td>
			<td><?php echo $report->room_rent_sum_total?></td>
			<td><?php echo $report->room_rent_tax_amount;?></td>
			<td><?php echo $group->charges_cost;?></td>
			<td><?php 
			$total=$report->room_rent_sum_total+$report->room_rent_tax_amount+$group->charges_cost;
			echo $total;?></td>
		</tr>
		<?php			
				}}}	}
		?>		
            </tbody>
          </table>
		 
		</div>
		</div>
											
		  
<script type="text/javascript">

$(document).ready(function(){
  $('#chkbx_tdy').prop('checked', true);
});

function check_sub(){
  document.getElementById('form_date').submit();
}
</script>