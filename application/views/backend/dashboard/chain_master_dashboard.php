<div id="cmd">
	<div class="page-toolbar">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat blue-madison">
					<div class="visual"> <i class="fa fa-users" style="color: #242424;"></i> </div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="1349">0</span> </div>
						<div class="desc"> Guest at present staying </div>
					</div>
					<a class="more" href="#"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat blue-madison">
					<div class="visual"> <i class="fa fa-bar-chart" style="color: #242424;"></i> </div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="50">0</span> </div>
						<div class="desc"> Todays booking revenue </div>
					</div>
					<a class="more" href="#"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat blue-madison">
					<div class="visual"> <i class="fa fa-history" style="color: #242424;"></i> </div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="651">0</span> </div>
						<div class="desc"> Todays booking count </div>
					</div>
					<a class="more" href="#"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat blue-madison">
					<div class="visual"> <i class="fa fa-bed" style="color: #242424;"></i> </div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="263">0</span> </div>
						<div class="desc"> Room occupancy </div>
					</div>
					<a class="more" href="#"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat blue-madison">
					<div class="visual"> <i class="fa fa-inr" style="color: #242424;"></i> </div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="324">0</span> </div>
						<div class="desc"> Average room rent </div>
					</div>
					<a class="more" href="#"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat blue-madison">
					<div class="visual"> <i class="fa fa-thumbs-up" style="color: #242424;"></i> </div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="544">0</span> </div>
						<div class="desc"> Active booking count </div>
					</div>
					<a class="more" href="#"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat blue-madison">
					<div class="visual"> <i class="fa fa-cloud-upload" style="color: #242424;"></i> </div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="64">0</span> </div>
						<div class="desc"> CheckIn count </div>
					</div>
					<a class="more" href="#"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat blue-madison">
					<div class="visual"> <i class="fa fa-cloud-download" style="color: #242424;"></i> </div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="665">0</span> </div>
						<div class="desc"> CheckOut count </div>
					</div>
					<a class="more" href="#"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
			</div>
		</div>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"> <a href="#CRS" data-toggle="tab" aria-expanded="true"> <i class="fa fa-calendar" aria-hidden="true"></i> CRS </a> </li>
		<li class="" id="dash_bo"> <a href="#quickInfo" data-toggle="tab" aria-expanded="false"> <i class="fa fa-tachometer" aria-hidden="true"></i> DASHBOARD </a> </li>
	</ul>
	<div class="tab-content" style="padding-top:15px;">
		<div class="tab-pane active" id="CRS">
			<div class="row">
				<div class="chain-date form-horizontal col-md-4">
					<label class="control-label col-md-4">Date Ranges</label>
					<div class="col-md-8">
						<div class="input-group" id="defaultrange1">
							<input type="text" class="form-control input-sm">
							<span class="input-group-btn">
								<button class="btn default date-range-toggle btn-sm" type="button">
									<i class="fa fa-calendar"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2 col-sm-2 col-xs-2" style="padding-right: 0;">
					<ul class="nav nav-tabs tabs-left">
						<li class="active">
							<a href="#tab_6_1" data-toggle="tab"> Hotel 1 </a>
						</li>
						<li>
							<a href="#tab_6_2" data-toggle="tab"> Hotel 2 </a>
						</li>
						<li>
							<a href="#tab_6_3" data-toggle="tab"> Hotel 3 </a>
						</li>
						<li>
							<a href="#tab_6_4" data-toggle="tab"> Hotel 4 </a>
						</li>
					</ul>
				</div>
				<div class="col-md-10 col-sm-10 col-xs-10" style="padding-left: 0;">
					<div class="tab-content">
						<div class="tab-pane active" id="tab_6_1">
							<div class="parent">
								<table class="fixTable table table-striped table-hover table">
									<thead style="background-color:#F9F9F9 !important;">
										<tr>
											<th align="center">July</th>
											<th colspan="30"><span style="position: relative;">&nbsp;</span>
											</th>
											<th>&nbsp;</th>
											<th colspan="30"><span style="position: relative;">Aug</span>
											</th>
										</tr>
										<tr>
											<th>Date</th>
											<th>1</th>
											<th>2</th>
											<th>3</th>
											<th>4</th>
											<th>5</th>
											<th>6</th>
											<th>7</th>
											<th>8</th>
											<th>9</th>
											<th>10</th>
											<th>11</th>
											<th>12</th>
											<th>13</th>
											<th>14</th>
											<th>15</th>
											<th>16</th>
											<th>17</th>
											<th>18</th>
											<th>19</th>
											<th>20</th>
											<th>21</th>
											<th>22</th>
											<th>23</th>
											<th>24</th>
											<th>25</th>
											<th>26</th>
											<th>27</th>
											<th>28</th>
											<th>29</th>
											<th>30</th>
											<th>&nbsp;</th>
											<th>1</th>
											<th>2</th>
											<th>3</th>
											<th>4</th>
											<th>5</th>
											<th>6</th>
											<th>7</th>
											<th>8</th>
											<th>9</th>
											<th>10</th>
											<th>11</th>
											<th>12</th>
											<th>13</th>
											<th>14</th>
											<th>15</th>
											<th>16</th>
											<th>17</th>
											<th>18</th>
											<th>19</th>
											<th>20</th>
											<th>21</th>
											<th>22</th>
											<th>23</th>
											<th>24</th>
											<th>25</th>
											<th>26</th>
											<th>27</th>
											<th>28</th>
											<th>29</th>
											<th>30</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><strong>Walkins</strong>
											</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>&nbsp;</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
											<td>2</td>
										</tr>
										<tr>
											<td><strong>OTA</strong>
											</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>&nbsp;</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
											<td>5</td>
										</tr>
										<tr>
											<td><strong>Website</strong>
											</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>&nbsp;</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
										</tr>
										<tr>
											<td><strong>Agent</strong>
											</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>&nbsp;</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
										</tr>
										<tr>
											<td><strong>Total Booking</strong>
											</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>&nbsp;</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
											<td>8</td>
										</tr>
										<tr>
											<td><strong>Revenue</strong>
											</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td>&nbsp;</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
											<td><i class="fa fa-inr"></i> 21736</td>
										</tr>
										<tr>
											<td><strong>ARR</strong>
											</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td>&nbsp;</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
											<td><i class="fa fa-inr"></i> 1780</td>
										</tr>
										<tr>
											<td><strong>Logged in User</strong>
											</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>&nbsp;</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
											<td>3</td>
										</tr>
										<tr>
											<td><strong>Occupancy</strong>
											</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>&nbsp;</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
											<td>37% <i class="fa fa-arrow-down"></i> (-3.8%)</td>
										</tr>
										<tr>
											<td><strong>Active Rooms</strong>
											</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>&nbsp;</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
											<td>22</td>
										</tr>
										<tr>
											<td><strong>Bookings</strong>
											</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>&nbsp;</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
											<td>9(1)</td>
										</tr>
										<tr>
											<td><strong>Staying Guests</strong>
											</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>&nbsp;</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
											<td>21(15+6)</td>
										</tr>
										<tr>
											<td><strong>Total Income Today</strong>
											</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td>&nbsp;</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
											<td><i class="fa fa-inr"></i> 24480</td>
										</tr>
										<tr>
											<td><strong>Total Expense</strong>
											</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td>&nbsp;</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
											<td><i class="fa fa-inr"></i> 17682</td>
										</tr>
										<tr>
											<td><strong>Total Receivable as of Today</strong>
											</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td>&nbsp;</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
											<td><i class="fa fa-inr"></i> 13791</td>
										</tr>
										<tr>
											<td><strong>Total Payable as of today</strong>
											</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td>&nbsp;</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
											<td><i class="fa fa-inr"></i> 44855</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane fade" id="tab_6_2">

						</div>
						<div class="tab-pane fade" id="tab_6_3">

						</div>
						<div class="tab-pane fade" id="tab_6_4">

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="quickInfo">

		</div>
	</div>
</div>
<script>
	$( document ).ready( function () {
		$( ".fixTable" ).tableHeadFixer();
	} );
</script>