<div style="border: 1px solid #607D8B; border-top: 0;" class="portlet box blue">
  <div class="portlet-title" style="background-color:#607D8B;">
    <div class="caption"> <i syle="font-size:18px;" class="fa fa-bar-chart" aria-hidden="true"></i> <span class="caption-subject bold uppercase">Chart Implementation Examples</span> <span class="pull-right" style="font-size:12px;"> &nbsp for reference purpose</span></div>
  </div>
  <div class="portlet-body form">
 <?php 
 //echo "<pre>";
//print_r($chart_reports);

$chartDatajson= json_encode($chart_reports);
//echo $chartDatajson;
//die;
 ?>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
       <script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/amcharts.js"></script>
       <script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/serial.js"></script>
        <link rel="stylesheet" href="http://192.168.1.3/hotelobjects_dev/assets/amcharts/style.css" type="text/css">
      

        <script>
            var chart;

            var chartData = [
                {
                    "year": "1995",
                    "cars": 1567,
                    "motorcycles": 683,
                    "bicycles": 146
                },
                {
                    "year": "1996",
                    "cars": 1617,
                    "motorcycles": 691,
                    "bicycles": 138
                },
                {
                    "year": "1997",
                    "cars": 1630,
                    "motorcycles": 642,
                    "bicycles": 127
                },
                {
                    "year": "1998",
                    "cars": 1660,
                    "motorcycles": 699,
                    "bicycles": 105
                },
                {
                    "year": "1999",
                    "cars": 1683,
                    "motorcycles": 721,
                    "bicycles": 109
                },
                {
                    "year": "2000",
                    "cars": 1691,
                    "motorcycles": 737,
                    "bicycles": 112
                },
                {
                    "year": "2001",
                    "cars": 1298,
                    "motorcycles": 680,
                    "bicycles": 101
                },
                {
                    "year": "2002",
                    "cars": 1275,
                    "motorcycles": 664,
                    "bicycles": 97
                },
                {
                    "year": "2003",
                    "cars": 1246,
                    "motorcycles": 648,
                    "bicycles": 93
                },
                {
                    "year": "2004",
                    "cars": 1218,
                    "motorcycles": 637,
                    "bicycles": 101
                },
                {
                    "year": "2005",
                    "cars": 1213,
                    "motorcycles": 633,
                    "bicycles": 87
                },
                {
                    "year": "2006",
                    "cars": 1199,
                    "motorcycles": 621,
                    "bicycles": 79
                },
                {
                    "year": "2007",
                    "cars": 1110,
                    "motorcycles": 210,
                    "bicycles": 81
                },
                {
                    "year": "2008",
                    "cars": 1165,
                    "motorcycles": 232,
                    "bicycles": 75
                },
                {
                    "year": "2009",
                    "cars": 1145,
                    "motorcycles": 219,
                    "bicycles": 88
                },
                {
                    "year": "2010",
                    "cars": 1163,
                    "motorcycles": 201,
                    "bicycles": 82
                },
                {
                    "year": "2011",
                    "cars": 1180,
                    "motorcycles": 285,
                    "bicycles": 87
                },
                {
                    "year": "2012",
                    "cars": 1159,
                    "motorcycles": 277,
                    "bicycles": 71
                }
            ];

            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();

                chart.dataProvider = chartData;
                chart.categoryField = "year";

                chart.addTitle("Traffic incidents per year", 15);

                // AXES
                // Category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridAlpha = 0.07;
                categoryAxis.axisColor = "#DADADA";
                categoryAxis.startOnAxis = true;

                // Value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.title = "percent"; // this line makes the chart "stacked"
                valueAxis.stackType = "100%";
                valueAxis.gridAlpha = 0.07;
                chart.addValueAxis(valueAxis);

                // GRAPHS
                // first graph
                var graph = new AmCharts.AmGraph();
                graph.type = "line"; // it's simple line graph
                graph.title = "Cars";
                graph.valueField = "cars";
                graph.lineAlpha = 0;
                graph.fillAlphas = 0.6; // setting fillAlphas to > 0 value makes it area graph
                graph.balloonText = "<img src='images/car.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>";
                chart.addGraph(graph);

                // second graph
                graph = new AmCharts.AmGraph();
                graph.type = "line";
                graph.title = "Motorcycles";
                graph.valueField = "motorcycles";
                graph.lineAlpha = 0;
                graph.fillAlphas = 0.6;
                graph.balloonText = "<img src='images/motorcycle.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>";
                chart.addGraph(graph);

                // third graph
                graph = new AmCharts.AmGraph();
                graph.type = "line";
                graph.title = "Bicycles";
                graph.valueField = "bicycles";
                graph.lineAlpha = 0;
                graph.fillAlphas = 0.6;
                graph.balloonText = "<img src='images/bicycle.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>";
                chart.addGraph(graph);

                // LEGEND
                var legend = new AmCharts.AmLegend();
                legend.align = "center";
                legend.valueText = "[[value]] ([[percents]]%)";
                legend.valueWidth = 100;
                legend.valueAlign = "left";
                legend.equalWidths = false;
                legend.periodValueText = "total: [[value.sum]]"; // this is displayed when mouse is not over the chart.
                chart.addLegend(legend);

                // CURSOR
                var chartCursor = new AmCharts.ChartCursor();
                chartCursor.zoomable = false; // as the chart displayes not too many values, we disabled zooming
                chartCursor.cursorAlpha = 0;
                chartCursor.valueZoomable = true;
                chartCursor.pan = true;
                chart.addChartCursor(chartCursor);

                //  VALUE SCROLLBAR
                chart.valueScrollbar = new AmCharts.ChartScrollbar();

                // WRITE
                chart.write("chartdiv");
            });
        </script>
        
        
      
       <h1> 4. 100% Stacked Column Chart</h1>
       <div id="chartdiv" style="width: 100%; height: 400px;"></div>
      
       <script>

            var chart1;
            var data1 = [
                {
                    "title": "Website visits",
                    "value": 300
                },
                {
                    "title": "Downloads",
                    "value": 123
                },
                {
                    "title": "Requested price list",
                    "value": 98
                },
                {
                    "title": "Contaced for more info",
                    "value": 72
                },
                {
                    "title": "Purchased",
                    "value": 35
                },
                {
                    "title": "Contacted for support",
                    "value": 15
                },
                {
                    "title": "Purchased additional products",
                    "value": 8
                }
            ];

            AmCharts1.ready(function () {

                chart1 = new AmCharts1.AmFunnelChart();
                chart1.titleField = "title";
                chart1.balloon.cornerRadius = 0;
                chart1.marginRight = 220;
                chart1.marginLeft = 15;
                chart1.labelPosition = "right";
                chart1.funnelAlpha = 0.9;
                chart1.valueField = "value";
                chart1.dataProvider = data1;
                chart1.startX = 0;
                chart1.balloon.animationTime = 0.2;
                chart1.neckWidth = "40%";
                chart1.startAlpha = 0;
                chart1.neckHeight = "30%";
                chart1.balloonText = "[[title]]:<b>[[value]]</b>";

                chart1.creditsPosition = "top-right";
                chart1.write("chartdiv1");
            });
        </script>
       
         <h1>14. Funnel chart</h1>
       <div id="chartdiv1" style="width: 100%; height: 400px;"></div>
    
  </div>
    
    
    
    
  <!-- END CONTENT --> 
</div>
