<div style="border: 1px solid #607D8B; border-top: 0;" class="portlet box blue">
  <div class="portlet-title" style="background-color:#607D8B;">
    <div class="caption"> <i syle="font-size:18px;" class="fa fa-bar-chart" aria-hidden="true"></i> <span class="caption-subject bold uppercase">Chart Implementation Examples</span> <span class="pull-right" style="font-size:12px;"> &nbsp for reference purpose</span></div>
  </div>
  <div class="portlet-body form">
 <?php 
 //echo "<pre>";
//print_r($chart_reports);

$chartDatajson= json_encode($chart_reports);
//echo $chartDatajson;
//die;
 ?>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
       <script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/amcharts.js"></script>
<!--       <script type="text/javascript" src="http://122.163.127.3/hotelobjects_dev/assets/amcharts/serial.js"></script>-->
        <link rel="stylesheet" href="http://192.168.1.3/hotelobjects_dev/assets/amcharts/style.css" type="text/css">
<!--      <script src="http://122.163.127.3/hotelobjects_dev/assets/amcharts/funnel.js" type="text/javascript"></script>-->
       <script src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/pie.js" type="text/javascript"></script>
	   <script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/amcharts.js"></script>
       <script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/serial.js"></script>
        <link rel="stylesheet" href="http://192.168.1.3/hotelobjects_dev/assets/amcharts/style.css" type="text/css">
 <script>
            var chart;
            var legend;

            var chartData = [
                {
                    "country": "Czech Republic",
                    "litres": 156.9
                },
                {
                    "country": "Ireland",
                    "litres": 131.1
                },
                {
                    "country": "Germany",
                    "litres": 115.8
                },
                {
                    "country": "Australia",
                    "litres": 109.9
                },
                {
                    "country": "Austria",
                    "litres": 108.3
                },
                {
                    "country": "UK",
                    "litres": 65
                },
                {
                    "country": "Belgium",
                    "litres": 50
                }
            ];
			

            AmCharts.ready(function () {
                // PIE CHART
                chart = new AmCharts.AmPieChart();
                chart.dataProvider = chartData;
                chart.titleField = "country";
                chart.valueField = "litres";
                chart.gradientRatio = [0, 0, 0 ,-0.2, -0.4];
                chart.gradientType = "radial";

                // LEGEND
                legend = new AmCharts.AmLegend();
                legend.align = "center";
                legend.markerType = "circle";
                chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                chart.addLegend(legend);

                // WRITE
                chart.write("chartdiv");
            });

            // changes label position (labelRadius)
            function setLabelPosition() {
                if (document.getElementById("rb1").checked) {
                    chart.labelRadius = 30;
                    chart.labelText = "[[title]]: [[value]]";
                } else {
                    chart.labelRadius = -30;
                    chart.labelText = "[[percents]]%";
                }
                chart.validateNow();
            }


            // makes chart 2D/3D
            function set3D() {
                if (document.getElementById("rb3").checked) {
                    chart.depth3D = 10;
                    chart.angle = 10;
                } else {
                    chart.depth3D = 0;
                    chart.angle = 0;
                }
                chart.validateNow();
            }

            // changes switch of the legend (x or v)
            function setSwitch() {
                if (document.getElementById("rb5").checked) {
                    legend.switchType = "x";
                } else {
                    legend.switchType = "v";
                }
                legend.validateNow();
            }
        </script>
        
      
       <h1>1.  Pie Chart With Legend</h1>
      <div id="chartdiv" style="width: 100%; height: 400px;"></div>
        <table align="center" cellspacing="20">
            <tr>
                <td>
                    <input type="radio" checked="true" name="group" id="rb1" onclick="setLabelPosition()">labels outside
                    <input type="radio" name="group" id="rb2" onclick="setLabelPosition()">labels inside</td>
                <td>
                    <input type="radio" name="group2" id="rb3" onclick="set3D()">3D
                    <input type="radio" checked="true" name="group2" id="rb4" onclick="set3D()">2D</td>
                <td>Legend switch type:
                    <input type="radio" checked="true" name="group3" id="rb5"
                    onclick="setSwitch()">x
                    <input type="radio" name="group3" id="rb6" onclick="setSwitch()">v</td>
            </tr>
        </table>
      
    
  </div>
    
    
    
    
  <!-- END CONTENT --> 
</div>
