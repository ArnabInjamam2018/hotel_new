<div style="border: 1px solid #607D8B; border-top: 0;" class="portlet box blue">
  <div class="portlet-title" style="background-color:#607D8B;">
    <div class="caption"> <i syle="font-size:18px;" class="fa fa-bar-chart" aria-hidden="true"></i> <span class="caption-subject bold uppercase">Chart Implementation Examples</span> <span class="pull-right" style="font-size:12px;"> &nbsp for reference purpose</span></div>
  </div>
  <div class="portlet-body form">
 <?php 
// echo "<pre>";
//print_r($chart_reports4);

$chartDatajson= json_encode($chart_reports4);
//echo $chartDatajson;
//die;
 ?>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
       <script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/amcharts.js"></script>
       <script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/serial.js"></script>
        <link rel="stylesheet" href="http://192.168.1.3/hotelobjects_dev/assets/amcharts/style.css" type="text/css">
        
        
      <script>
            var chart;
            var chartData = <?php echo $chartDatajson; ?>;


            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();

                chart.dataProvider = chartData;
                chart.categoryField = "Booking_Source_Name";
                chart.startDuration = 1;

                chart.handDrawn = true;
                chart.handDrawnScatter = 3;

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridPosition = "start";
                categoryAxis.labelRotation = 90;
              
                // value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.axisAlpha = 0;
                chart.addValueAxis(valueAxis);

                // GRAPHS
                // column graph
                var graph1 = new AmCharts.AmGraph();
                graph1.type = "column";
                graph1.title = "COUNT";
                graph1.lineColor = "#a668d5";
                graph1.valueField = "COUNT";
                graph1.lineAlpha = 1;
                graph1.fillAlphas = 1;
                graph1.dashLengthField = "dashLengthColumn";
                graph1.alphaField = "alpha";
                graph1.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
                chart.addGraph(graph1);

                // line
                var graph2 = new AmCharts.AmGraph();
                graph2.type = "line";
                graph2.title = "No_Of_Guest_Sum";
                graph2.lineColor = "#fcd202";
                graph2.valueField = "No_Of_Guest_Sum";
                graph2.lineThickness = 3;
                graph2.bullet = "round";
                graph2.bulletBorderThickness = 3;
                graph2.bulletBorderColor = "#fcd202";
                graph2.bulletBorderAlpha = 1;
                graph2.bulletColor = "#ffffff";
                graph2.dashLengthField = "dashLengthLine";
                graph2.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
                chart.addGraph(graph2);

                // LEGEND
                var legend = new AmCharts.AmLegend();
                legend.useGraphSettings = true;
                chart.addLegend(legend);

                // WRITE
                chart.write("chartdiv");
            });
        </script>
      
       <h1>Combined bullet/column and line graphs with multiple value axes</h1>
       <div id="chartdiv" style="width: 100%; height: 400px;"></div>
      
    
  </div>
    
    
    
    
  <!-- END CONTENT --> 
</div>
