<div style="border: 1px solid #607D8B; border-top: 0;" class="portlet box blue">
  <div class="portlet-title" style="background-color:#607D8B;">
    <div class="caption"> <i syle="font-size:18px;" class="fa fa-bar-chart" aria-hidden="true"></i> <span class="caption-subject bold uppercase">Chart Implementation Examples</span> <span class="pull-right" style="font-size:12px;"> &nbsp for reference purpose</span></div>
  </div>
  <div class="portlet-body form">
 <?php 
 //echo "<pre>";
//print_r($chart_reports);

$chartDatajson= json_encode($chart_reports);
//echo $chartDatajson;
//die;
 ?>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

     <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/funnel.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>

<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<!-- Chart code -->
<script>
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "funnel",
  "theme": "light",
  "dataProvider": [ {
    "title": "Website visits",
    "value": 300
  }, {
    "title": "Downloads",
    "value": 123
  }, {
    "title": "Requested prices",
    "value": 98
  }, {
    "title": "Contacted",
    "value": 72
  }, {
    "title": "Purchased",
    "value": 35
  }, {
    "title": "Asked for support",
    "value": 25
  }, {
    "title": "Purchased more",
    "value": 18
  } ],
  "titleField": "title",
  "marginRight": 160,
  "marginLeft": 15,
  "labelPosition": "right",
  "funnelAlpha": 0.9,
  "valueField": "value",
  "startX": 0,
  "neckWidth": "40%",
  "startAlpha": 0,
  "outlineThickness": 1,
  "neckHeight": "30%",
  "balloonText": "[[title]]:<b>[[value]]</b>",
  "export": {
    "enabled": true
  }
} );
</script>
   <style>
#chartdiv {
    margin-left: 100px;
  width: 80%;
  height: 500px;
}														
</style>   
       <h1>1. Funnel chart</h1>
       <div id="chartdiv"></div>
      
    
  </div>
    
    
    
    
  <!-- END CONTENT --> 
</div>
