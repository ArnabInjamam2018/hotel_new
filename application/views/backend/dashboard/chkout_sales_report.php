<?php ini_set('memory_limit' , '64M'); 

?>
<div class="portlet light borderd">
  <div class="portlet-title">
    <div class="caption"> <i class="glyphicon glyphicon-bed"></i>Daily Checkout Report</div>
    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-8">
          <?php		

                        $form = array(
                            'class'       => 'form-inline',
                            'id'        => 'form_date',
                            'method'      => 'post'
                        );

                        echo form_open_multipart('reports/checkout_report',$form);
	
                        ?>
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" id="t_dt_frm" value="<?php if(isset($start_date)){ echo $start_date;}?>"  name="t_dt_frm" class="form-control date-picker" placeholder="Start Date">
          </div>
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" name="t_dt_to" value="<?php if(isset($end_date)){ echo $end_date;}?>"  class="form-control date-picker" placeholder="End Date">
          </div>
          <button class="btn btn-default" onclick="check_sub()" type="submit">Search</button>
          <?php form_close(); ?>
        </div>
      </div>
    </div>
    <table class="table table-striped table-hover table-bordered" id="sample_4">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Reservation Id</th>
          <th scope="col">Invoice No</th>
          <th scope="col">Invoice No Det</th>
          <th scope="col" >Arrival Dates</th>
          <th scope="col" >Departure Dates</th>
          <th scope="col">Booking Source</th>
          <th scope="col">Room No</th>
          <th scope="col">Room Type</th>
          <th scope="col">Guest Name</th>
          <th scope="col">Bill to Company</th>
          <th scope="col">Room Rent</th>		  		  
		  <th scope="col">RR SGST</th>          
		  <th scope="col">RR CGST</th>          
		  <th scope="col">RR IGST</th>          
		  <th scope="col">RR UTGST</th>
          <th scope="col">S.Charges</th>
          <th scope="col">Meal Plan Rent</th>		  		  
		  <th scope="col">MP SGST</th>          
		  <th scope="col">MP CGST</th>          
		  <th scope="col">MP IGST</th>          
		  <th scope="col">MP UTGST</th>
        <!--  <th scope="col">MP Total Tax</th>-->
          
          <th scope="col">POS Amount</th>
		  <th style="color:#006881" scope="col">POS SGST</th>
          <th style="color:#006881" scope="col">POS CGST</th>
          <th style="color:#006881" scope="col">POS IGST</th>
          <th style="color:#006881" scope="col">POS UTGST</th>
          <!--<th style="color:#006881" scope="col">POS Tax</th>-->
          <th style="color:#006881" scope="col">Extra Charge Amount</th>
          <th style="color:#006881" scope="col">Extra Charge SGST</th>
          <th style="color:#006881" scope="col">Extra Charge CGST</th>
          <th style="color:#006881" scope="col">Extra Charge IGST</th>
          <th style="color:#006881" scope="col">Extra Charge UTGST</th>
          <th style="color:#006881" scope="col">Extra Charge Tax</th>
          <th scope="col">Extra Service(Incl Tax)</th>
          <th scope="col">Discount</th>
          <th scope="col">Adjustment Cr Amount</th>
          <th scope="col">Adjustment Dr Amount</th>
          <th scope="col">Total Amount</th>
          <th style="color:#ff2626" scope="col">Paid Amount</th>
          <th style="color:#ff2626" scope="col">Cash</th>
		  <th style="color:#ff2626" scope="col">Draft</th>
          <th style="color:#ff2626" scope="col">Card</th>
          <th style="color:#ff2626" scope="col">Fund Tran</th>
          <th style="color:#ff2626" scope="col">cheque</th>
          <th style="color:#ff2626" scope="col">ewallet</th>
          <th style="color:#ff2626" scope="col">Bill To Company Amount</th>
        </tr>
      </thead>
      <tbody>
        <?php 
        
        
	
          //print_r($test); exit;
          if($sales && isset($sales)){
			  //print_r($sales); exit;
				$pos_tot_amount=0;
				$rr_tax=0;	
                $mp_ch=0;	
                $mp_tax=0;	
                $ex_ch=0;	
                $exmp_ch=0;	
                $exmp_tax=0;
                $srl_no=0;
                $gbbookings=0;
                $tot_rr=0;								
				$rr_sgst = 0;				
				$rr_cgst = 0;				
				$rr_igst = 0;				
				$rr_utgst = 0;
				$mp_sgst = 0;				
				$mp_cgst = 0;				
				$mp_igst = 0;				
				$mp_utgst = 0;				
                $l_tax=0;
                $s_tax=0;
                $s_ch=0;
                $tot_mp=0;			 
                $tot_mp_tax=0;			 
                $tot_ex_ch=0;			 
                $tot_exmp_ch=0;			 
                $tot_exmp_tax=0;			 
                $Tot_total=0;					
                $Tot_paid=0;
				$pos_tot_amt=0;
				$pos_cgst_tot=0;
				$pos_sgst_tot=0;
				$pos_igst_tot=0;
				$pos_utgst_tot=0;
				
				$tot_pos_tax=0;	 
				$adj_amt1=0;	
				$adj_amt=0;	
				$Tot_adjst=0;	
				$tot_dis=0;			
				$tot_chg=0;			
				$tot_ch1=0;	
				$tot_charg=0;
				$tot_charg_tax=0;
				$total_charge_tax_sgst=0;
				$total_charge_tax_cgst=0;
				$total_charge_tax_igst=0;
				$total_charge_tax_utgst=0;
				$single_total=0;
				$grp_total = 0;
				$tot_dr=0;		
				$tot_cr=0;
				$cashT = 0; $draftT = 0; $cardT = 0; $fundT = 0; $chequeT = 0; $ewalletT = 0;
				
			foreach($sales as $sl){
				
					$pos_tax=0;				
					$pos_tax1=0;	
            		$pos_amount=0;
            		$pos_amount1=0;
					$discount_amount=0;				
					$discount_amount1=0;
						$adj_amt_dr	=0;			
						$adj_amt_cr	=0;
						$adj_amt_dr1=0;			
						$adj_amt_cr1=0;
						$pos_tot_amount1=0;
						$paid1 =0;				
				        $paid =0;	
						$tot_chg=0;			
				$tot_ch1=0;	
				
				$tot_ch_tax=0;
				$tot_ch_tax_cgst=0;
				$tot_ch_tax_sgst=0;
				$tot_ch_tax_igst=0;
				$tot_ch_tax_utgst=0;
				$tot_ch_tax=0;
				$tot_chg_tax=0;
			//echo "<pre>";		
			//print_r($sales);// exit;
                 $srl_no++;
            $booking_id=$sl->booking_id; 
            $type1=$sl->type; 
			$type='sb';
			
            if($type1==0){
                    
                $bookings=$this->dashboard_model->get_booking_details($booking_id);
                $transaction = $this->bookings_model->get_total_payment($booking_id);
                $transaction1 = $this->dashboard_model->all_transactions_byID('sb',$booking_id);

				$paybillBycmp = $this->dashboard_model->paybillBycmp('sb',$booking_id);
                
                $company_name= $paybillBycmp->company_name;
                $cancel_amt= $paybillBycmp->total_amt;
				
				if(isset($transaction) && $transaction){
					//echo '<pre>';
					//print_r($transaction);
                    foreach($transaction as $ts){
						$paid=$ts->t_amount;
					}
                }
                
            $poses=$this->dashboard_model->all_pos_booking($booking_id);
            $pos_amount=0;
			$pos_tax=0;
            $pos_totAMT=0;
            $pos_cgst =0;
			$pos_sgst =0;
			$pos_igst= 0;
			$pos_utgst =0;
			$pos_tot_amount=0;
			//$pos_totAMT = 0;
                if($poses){
					
                foreach ($poses as $key ) {
					
					
					if($key->total_amount != $key->total_paid_pos){

					
                  $pos_amount=$pos_amount+$key->total_due;
				   $pos_cgst = $pos_cgst + $key->food_cgst + $key->liquor_cgst;
				  $pos_sgst  = $pos_sgst  + $key->food_sgst + $key->liquor_sgst;
				  $pos_igst = $pos_igst + $key->food_igst + $key->liquor_igst;
				  $pos_utgst = $pos_utgst + $key->food_utgst + $key->liquor_utgst;
                  $pos_tax=$pos_tax+$key->food_sgst+$key->food_cgst+$key->food_igst+$key->food_utgst+$key->liquor_sgst+$key->liquor_cgst+$key->liquor_igst+$key->liquor_utgst;
			//	  print_r($key->total_amount);
                  $pos_tot_amount=$pos_tot_amount+$key->total_amount;
                //  $pos_tax=$key->tax;
               
                
				
					}
					
				}
				$pos_totAMT=$pos_totAMT+$pos_tot_amount-$pos_tax;
		
				}
                $adjuset_amt=$this->dashboard_model->get_adjuset_amtAll($booking_id);
					//print_r($adjuset_amt);	
				if(isset($adjuset_amt) && $adjuset_amt){
						foreach($adjuset_amt as $adAmt){
							if($adAmt->amount>0){
								$adj_amt_dr=$adj_amt_dr+$adAmt->amount;
							}else{
								$adj_amt_cr=$adj_amt_cr+$adAmt->amount;
							}
						}
				
				 }
                
			$discount_details=$this->unit_class_model->all_discount_details($booking_id);	
				
				if(isset($discount_details) && $discount_details){
				
					foreach($discount_details as $discount){
				$discount_amount=$discount_amount+$discount->discount_amount;
				}
				}else{
					$discount_amount=0;
				}
				
				if(isset($sl->booking_extra_charge_id) && $sl->booking_extra_charge_id){
					
										$tot_chg=0;
										$tot_chg_tax=0;
										$tot_chg_tax_cgst=0;
										$tot_chg_tax_sgst=0;
										$tot_chg_tax_igst=0;
										$tot_chg_tax_utgst=0;
			 $charge_id_array=explode(",",$sl->booking_extra_charge_id);
											//$booking_id=$bookings->booking_id;
                                           for($i=1;$i<sizeof($charge_id_array);$i++) {
                    
                                        $charge_details=$this->dashboard_model->get_charge_details($charge_id_array[$i]);
										if(isset($charge_details) && $charge_details){
									//	$tot_chg=$tot_chg+ $charge_details->crg_total;
										$tot_chg=$tot_chg+($charge_details->crg_quantity*$charge_details->crg_unit_price);
												$tot_chg_tax=$tot_chg_tax+(($charge_details->crg_quantity*$charge_details->crg_unit_price)*$charge_details->crg_tax/100);
												
												$tot_chg_tax_cgst = $tot_chg_tax_cgst+(($charge_details->crg_quantity*$charge_details->crg_unit_price)*$charge_details->cgst/100);
												$tot_chg_tax_sgst = $tot_chg_tax_sgst+(($charge_details->crg_quantity*$charge_details->crg_unit_price)*$charge_details->sgst/100);
												$tot_chg_tax_igst = $tot_chg_tax_igst+(($charge_details->crg_quantity*$charge_details->crg_unit_price)*$charge_details->igst/100);
												$tot_chg_tax_utgst = $tot_chg_tax_utgst+(($charge_details->crg_quantity*$charge_details->crg_unit_price)*$charge_details->utgst/100);
										
										
										
										//
										}
										   }
								}else{
										$tot_chg=0;
										$tot_chg_tax=0;
										$tot_chg_tax_cgst=0;
										$tot_chg_tax_sgst=0;
										$tot_chg_tax_igst=0;
										$tot_chg_tax_utgst=0;
								}										   
				
				
				
                
                }// end of single
				else{
                          $pos_totAMT=0;
						  $pos_tot_amount = 0;
						  
						   $paybillBycmp_gb = $this->dashboard_model->paybillBycmp('gb',$booking_id);
                        $company_name= $paybillBycmp_gb->company_name;
                        $cancel_amt= $paybillBycmp_gb->total_amt;
						  
						  
						if(isset($sl->booking_extra_charge_id) && $sl->booking_extra_charge_id){
							$tot_ch1=0;
								$tot_ch_tax=0;
								$tot_ch_tax_cgst=0;
								$tot_ch_tax_sgst=0;
								$tot_ch_tax_igst=0;
								$tot_ch_tax_utgst=0;
						$charge_array=explode(",", $sl->booking_extra_charge_id);
						//print_r($charge_string); 
						//echo sizeof($charge_array);
                    for ($i=0; $i < sizeof($charge_array) ; $i++){ 
                        $charges=$this->dashboard_model->get_charge_details($charge_array[$i]);
							if(isset($charges) && $charges){
								
								$tot_ch1=$tot_ch1+($charges->crg_quantity*$charges->crg_unit_price);
							$tot_ch_tax=$tot_ch_tax+(($charges->crg_quantity*$charges->crg_unit_price)*$charges->crg_tax/100);
							
							$tot_ch_tax_cgst=$tot_ch_tax_cgst+(($charges->crg_quantity*$charges->crg_unit_price)*$charges->cgst/100);
							$tot_ch_tax_sgst=$tot_ch_tax_sgst+(($charges->crg_quantity*$charges->crg_unit_price)*$charges->sgst/100);
							$tot_ch_tax_igst=$tot_ch_tax_igst+(($charges->crg_quantity*$charges->crg_unit_price)*$charges->igst/100);
							$tot_ch_tax_utgst=$tot_ch_tax_utgst+(($charges->crg_quantity*$charges->crg_unit_price)*$charges->utgst/100);
								
							//$tot_ch1=$tot_ch1+$charges->crg_total;
							}
					}
						}else{
								$tot_ch1=0;
								$tot_ch_tax=0;
								$tot_ch_tax_cgst=0;
								$tot_ch_tax_sgst=0;
								$tot_ch_tax_igst=0;
								$tot_ch_tax_utgst=0;
							}	
						
					 $str='';
					 
                    $gbbookings=$this->dashboard_model->get_group($booking_id);
               //    print_r($gbbookings); exit;
                    $adjuset_amt1=$this->dashboard_model->get_adjuset_amt_grpAll($booking_id);
					
			if(isset($adjuset_amt1) && $adjuset_amt1){
						foreach($adjuset_amt1 as $adAmt1){
							if($adAmt1->amount>0){
								$adj_amt_dr1=$adj_amt_dr1+$adAmt1->amount;
							}else{
								$adj_amt_cr1=$adj_amt_cr1+$adAmt1->amount;
							}
						}
				
				 }
                    $transaction_gb = $this->bookings_model->get_total_payment_gp($booking_id);
					$transaction1 = $this->dashboard_model->all_transactions_byID('gb',$booking_id);
                
            if(isset($transaction_gb) && $transaction_gb){
                    foreach($transaction_gb as $tn){
                  $paid1=$tn->t_amount;
                }
                }
                    
               foreach($gbbookings as $gb){
                        //echo	$gb->name;
                        $unit_id=$this->bookings_model->get_unitId1($gb->id);
        $str='';
		if(isset($unit_id) && $unit_id){
        foreach($unit_id as $u_id){
            $rm_No=$this->bookings_model->get_rmNo1($u_id->room_id);
			$str.=" ".$rm_No->room_no.',';
        }
        //foreach($rm_No as $value){
			
		}
       }
       
       $poses_gb=$this->dashboard_model->all_pos_booking_group($booking_id);
            $pos_amount1=0;
			$pos_tax1=0;
            $pos_tot_amount_grp=0;	  
				  
				  $pos_cgst_gb = 0;
				  $pos_sgst_gb  = 0;
				  $pos_igst_gb = 0;
				  $pos_utgst_gb = 0;
               $pos_tot_amount1 = 0;
                if($poses_gb){
                foreach ($poses_gb as $key1 ) {

				
				if($key1->total_amount != $key1->total_paid_pos){
                  $pos_amount1=$pos_amount1+$key1->total_due;
				    $pos_cgst_gb = $pos_cgst_gb + $key1->food_cgst + $key1->liquor_cgst;
				  $pos_sgst_gb  = $pos_sgst_gb  + $key1->food_sgst + $key1->liquor_sgst;
				  $pos_igst_gb = $pos_igst_gb + $key1->food_igst + $key1->liquor_igst;
				  $pos_utgst_gb = $pos_utgst_gb + $key1->food_utgst + $key1->liquor_utgst;
                  $pos_tax1=$pos_tax1+$key1->food_sgst+$key1->food_cgst+$key1->food_igst+$key1->food_utgst+$key1->liquor_sgst+$key1->liquor_cgst+$key1->liquor_igst+$key1->liquor_utgst;
                 $pos_tot_amount1=$pos_tot_amount1+$key1->total_amount;
				//  $pos_tax1=$key1->tax;
				 
				
                }
				}
				$pos_tot_amount_grp=$pos_tot_amount_grp+$pos_tot_amount1-$pos_tax1;
				}
                
				
				$discount_details1=$this->unit_class_model->all_discount_group_details($booking_id);	
				
				if(isset($discount_details1) && $discount_details1){
					
					foreach($discount_details1 as $discount1){
				$discount_amount1=$discount_amount1+$discount1->discount_amount;
				}
				}else{
					$discount_amount1=0;
				}
            } // End else
            if($type1==0){	
				$type='sb';
				$tax=$this->bookings_model->line_charge_item_tax($type,$booking_id);
                //echo '<pre>';				//print_r($tax);					//exit; 
            } else {
                $type='gb';
                $tax=$this->bookings_model->line_charge_item_tax($type,$booking_id);								//echo '<pre>';
				//print_r($tax);	
				//exit; 
            }
              if(isset($tax['rr_tax']['Luxury tax']) && $tax['rr_tax']['Luxury tax'] && isset($tax['rr_tax']['Service Tax']) && $tax['rr_tax']['Service Tax'] && isset($tax['rr_tax']['Service Charge']) && $tax['rr_tax']['Service Charge'] ) { 
                $lux_tax= $tax['rr_tax']['Luxury tax'];			 
				$ser_tax= $tax['rr_tax']['Service Tax'];			 
				$ser_ch= $tax['rr_tax']['Service Charge'];				
			  } else {
			    $lux_tax= 0;			 
				$ser_tax= 0;			 
				$ser_ch= 0;
			  }			  			  
			  if(isset($tax['rr_tax']['SGST']) && $tax['rr_tax']['SGST'])				
				  $rr_sgst_tax = $tax['rr_tax']['SGST'];			  
			  else				
				  $rr_sgst_tax = 0;			  			  
			  if(isset($tax['rr_tax']['CGST']) && $tax['rr_tax']['CGST'])				
				  $rr_cgst_tax = $tax['rr_tax']['CGST'];			  
			  else				
				  $rr_cgst_tax = 0;			  			  
			  if(isset($tax['rr_tax']['IGST']) && $tax['rr_tax']['IGST'])				$rr_igst_tax = $tax['rr_tax']['IGST'];			  else				$rr_igst_tax = 0;			  			  if(isset($tax['mp_tax']['UTGST']) && $tax['mp_tax']['UTGST'])				$rr_utgst_tax = $tax['rr_tax']['UTGST'];			  else				$rr_utgst_tax = 0;						  if(isset($tax['mp_tax']['SGST']) && $tax['mp_tax']['SGST'])				$mp_sgst_tax = $tax['mp_tax']['SGST'];			  else				$mp_sgst_tax = 0;			  			  if(isset($tax['mp_tax']['CGST']) && $tax['mp_tax']['CGST'])				$mp_cgst_tax = $tax['mp_tax']['CGST'];			  else				$mp_cgst_tax = 0;			  			  if(isset($tax['mp_tax']['IGST']) && $tax['mp_tax']['IGST'])				$mp_igst_tax = $tax['mp_tax']['IGST'];			  else				$mp_igst_tax = 0;			  			  if(isset($tax['mp_tax']['UTGST']) && $tax['mp_tax']['UTGST'])				$mp_utgst_tax = $tax['mp_tax']['UTGST'];			  else				$mp_utgst_tax = 0;			  			  			  
		if(isset($sl->rm_total) && $sl->rm_total){
			    $rr_tot=$sl->rm_total+$sl->exrr;	
                $rr_tax=$sl->rr_tot_tax;	
                $mp_ch=$sl->mp_tot+$sl->exmp;	
                $mp_tax=$sl->mp_tax;	
                $ex_ch=$sl->exrr;	
                $exmp_ch=$sl->exmp;	
                $exmp_tax=$sl->exmp_tax;	
		}else{
			    $rr_tot=0;	
                $rr_tax=0;	
                $mp_ch=0;	
                $mp_tax=0;	
                $ex_ch=0;	
                $exmp_ch=0;	
                $exmp_tax=0;
		}
   
		$total = $rr_tot + $tax['rr_tax']['SGST'] + $tax['rr_tax']['CGST'] + $tax['rr_tax']['IGST'] + $tax['rr_tax']['UTGST'] + $mp_ch +
		$tax['mp_tax']['CGST'] + $tax['mp_tax']['SGST'] + $tax['mp_tax']['IGST'] + $tax['mp_tax']['UTGST'] + $tot_chg +
		$tot_chg_tax +
		$tot_ch1 +
		$tot_ch_tax +
		$pos_totAMT +
		$pos_tax1 +
		$pos_tax +
		$pos_tot_amount_grp +
		$adj_amt_dr +
		$adj_amt_cr +
		$adj_amt_dr1 +
		$adj_amt_cr1 -
		$discount_amount1 -
		$discount_amount;
	//	
	 
	
	 $tot_rr = $tot_rr + $rr_tot;		 $rr_sgst = $rr_sgst + $rr_sgst_tax;	 $rr_cgst = $rr_cgst + $rr_cgst_tax;	 $rr_igst = $rr_igst + $rr_igst_tax;	 $rr_utgst = $rr_utgst + $rr_utgst_tax;	 $mp_sgst = $mp_sgst + $mp_sgst_tax;	 $mp_cgst = $mp_cgst + $mp_cgst_tax;	 $mp_igst = $mp_igst + $mp_igst_tax;	 $mp_utgst = $mp_utgst + $mp_utgst_tax;	 
     $l_tax = $l_tax + $lux_tax;			 
     $s_tax=$s_tax+ $ser_tax;			 
     $s_ch=$s_ch+ $ser_ch;			 
     $tot_mp=$tot_mp+$mp_ch;			 
     $tot_mp_tax=$tot_mp_tax+$mp_tax;			 
     $tot_ex_ch=$tot_ex_ch+$ex_ch;			 
     $tot_exmp_ch=$tot_exmp_ch+$exmp_ch;			 
     $tot_exmp_tax=$tot_exmp_tax+$exmp_tax;			 

     $Tot_total = $Tot_total + $total;
	 $tot_cancel_amt=$tot_cancel_amt+$cancel_amt;

     ?>
        <tr>
          <td align="center"><?php echo $srl_no ;?></td>
          <td align="center"><?php if($type1!=0){ echo '<i style="color:#F8681A;" class="fa fa-group"></i> GBK0'.$this->session->userdata('user_hotel')."0".$booking_id;} else { echo '<i style="color:#023358;" class="fa fa-user"></i> BK0'.$this->session->userdata('user_hotel').'0'.$bookings->booking_id;}?></td>
		  
          <td align="center">
			<?php
				$chk = $this->bookings_model->get_invoice_settings();
				$suf = $chk->invoice_suf;
				$pref = $chk->invoice_pref;
				
				if($type1!=0){ 
					$invId = $this->dashboard_model->get_invoice_grp($booking_id);
					if($invId->id != '' && $invId->id != null)
						echo 'GBKINV-'.$invId->id." ".$suf;
				} else { 
					$invId = $this->dashboard_model->get_invoice($bookings->booking_id);
					if($invId->id != '' && $invId->id != null)
						echo 'BKINV-'.$invId->id." ".$suf;
				}
			?>
		  </td>
		  
		  <td align="center">
			<?php
				$chk = $this->bookings_model->get_invoice_settings();
				$suf = $chk->invoice_suf;
				$pref = $chk->invoice_pref;
				
				if($type1!=0){ 
					$invId = $this->dashboard_model->get_invoice_grp($booking_id);
					if($invId->id != '' && $invId->id != null)
						echo $invId->id;
				} else { 
					$invId = $this->dashboard_model->get_invoice($bookings->booking_id);
					if($invId->id != '' && $invId->id != null)
						echo $invId->id;
				}
			?>
		  </td>
		  
          <td align="center"><?php if($type1!=0){ echo date('d M, Y',strtotime($sl->cust_from_date_actual)) ;} else { echo date('d M, Y',strtotime($bookings->cust_from_date_actual));}?></td>
           <td align="center"> <?php if($type1!=0){ echo date('d M , Y',strtotime($sl->cust_end_date_actual));} else { echo date('d M , Y',strtotime($bookings->cust_end_date_actual));}?></td>
          <td align="center">
			<?php
            if($type1!=0){ echo $sl->booking_source ;
			}
			else {	
				$sc_id=$bookings->booking_source;
				if(isset($sc_id) && $sc_id){
					$bk=$this->dashboard_model->getsourceId($sc_id);
					if(isset($bk) && $bk) {	
						echo $bk->booking_source_name;
					}
				} else {
					echo "N/A";
				}
            }
            ?>
		  </td>
          <td align="center"><?php  
              if($type1!=0){ echo $str ;} else {
              
              $rm_name=$this->dashboard_model->get_room_by_id($sl->room_id);
                    foreach($rm_name as $rm){
                    
                        echo $rm->room_no."</br>";
                    }
              }
              ?>
		  </td>
          <td><?php if($type1!=0){ echo "N/A"; } else {echo $rm->unit_name;}?></td>
          <td align="center">
			<?php 
			 $g_id=$sl->guest_id;
	 $guest_det=$this->dashboard_model->get_guest_details($g_id);
     
     	if(isset($guest_det) && $guest_det){
	     foreach($guest_det as $guest){
	        echo $guest->g_name;
          }  
     	    
     	}
			?>
		  </td>
		  <td align="center">
			<?php 
				if($sl->bill_to_com == 1)
					echo 'yes';
				else
					echo 'no';
			?>
		  </td>
		  
		  <td align="center"><?php echo $rr_tot;?></td>					  
		  
		  <!-- Room Rent GST -->
		  
          <td align="center">			
			  <?php 				
				  if(isset($tax['rr_tax']['SGST']) && $tax['rr_tax']['SGST']){ 					
					echo number_format($tax['rr_tax']['SGST'],2,".","");				
				  } else { 					
					echo "0";
				  }			
			  ?>		 
		  </td>		
		  
		  <td align="center">			
			<?php 				
				if(isset($tax['rr_tax']['CGST']) && $tax['rr_tax']['CGST']){ 					
					echo number_format($tax['rr_tax']['CGST'],2,".","");				
				} else { 					
					echo "0";				}			
			?>		  
		    
			</td>	
			
			<td align="center">			
				<?php 				
					if(isset($tax['rr_tax']['IGST']) && $tax['rr_tax']['IGST']){ 					
						echo number_format($tax['rr_tax']['IGST'],2,".","");				
					} else { 					
						echo "0";				
					}			
				?>		  
			</td>		  
			
			<td align="center">			<?php 				if(isset($tax['rr_tax']['UTGST']) && $tax['rr_tax']['UTGST']){ 					echo number_format($tax['rr_tax']['UTGST'],2,"."," ");				} else { 					echo "0";				}			?>		  </td>		  
          
		  <td align="center"><?php if(isset($tax['rr_tax']['Service Charge']) && $tax['rr_tax']['Service Charge']){ echo number_format($tax['rr_tax']['Service Charge'],2,".","");} else{ echo 0;}?></td>
          <td align="center"><?php  if($mp_ch>0){ echo  $mp_ch ;}else{ echo "0";}  ?></td>		  		  <!-- Meal Plan GST -->		  <td align="center">			<?php 				if(isset($tax['mp_tax']['SGST']) && $tax['mp_tax']['SGST']){ 					echo number_format($tax['mp_tax']['SGST'],2,".","");				} else { 					echo "0";				}			?>		  </td>		  <td align="center">			<?php 				if(isset($tax['mp_tax']['CGST']) && $tax['mp_tax']['CGST']){ 					echo number_format($tax['mp_tax']['CGST'],2,".","");				} else { 					echo "0";				}			?>		  </td>		  <td align="center">			<?php 				if(isset($tax['mp_tax']['IGST']) && $tax['mp_tax']['IGST']){ 					echo number_format($tax['mp_tax']['IGST'],2,".","");				} else { 					echo "0";				}			?>		  </td>		  <td align="center">			<?php 				if(isset($tax['mp_tax']['UTGST']) && $tax['mp_tax']['UTGST']){ 					echo number_format($tax['mp_tax']['UTGST'],2,".","");				} else { 					echo "0";				}			?>		  </td>
		<!--  <td align="center"><?php  if($mp_tax>0){ echo  $mp_tax ;}else{ echo "0";}?></td>-->
          <!--<td align="center">
		  <?php 
		  /*if($type1!=0){
		  foreach ($poses_gb as $key1 ) {
				
					$pos_item_details = $this->dashboard_model->get_pos_items($key1->pos_id);
					if(isset($pos_item_details) && $pos_item_details!=''){
						foreach($pos_item_details as $pos_itm){
							
			echo '</br><span style="font-size:10px">'.$pos_itm->item_name.' ('.$pos_itm->item_quantity.' X '.$pos_itm->unit_price.')</span>';				
						}
					}
				}
		  
		  }else{
			  
				 foreach ($poses as $key ) {
				
					$pos_item_details = $this->dashboard_model->get_pos_items($key->pos_id);
					if(isset($pos_item_details) && $pos_item_details!=''){
						foreach($pos_item_details as $pos_itm){
							
			echo '</br><span style="font-size:10px">'.$pos_itm->item_name.' ('.$pos_itm->item_quantity.' X '.$pos_itm->unit_price.')</span>';				
						}
					}
				}
		  }*/
		  ?></td>-->
		  
          <td align="center">
			  <?php 
				if($type1!=0){ echo $pos_tot_amount_grp; $pos_tot_amt=$pos_tot_amt+$pos_tot_amount_grp ;
				}
				else {
					echo  $pos_totAMT; $pos_tot_amt=$pos_tot_amt+$pos_totAMT;} 
			  ?>
		  </td>
		  
		   <td align="center">
			<?php 
				if($type1!=0) { 
						echo $pos_sgst_gb; $pos_sgst_tot=$pos_sgst_tot+$pos_sgst_gb;
				} else { 
						echo $pos_sgst; $pos_sgst_tot=$pos_sgst_tot+$pos_sgst;
				}
		    ?>
		  </td>
		  <td align="center">
			<?php 
				if($type1!=0) { 
					echo $pos_cgst_gb; $pos_cgst_tot=$pos_cgst_tot+$pos_cgst_gb;
				} else { 
						echo $pos_cgst; $pos_cgst_tot=$pos_cgst_tot+$pos_cgst;
				}
		    ?>
		  </td>
		  <td align="center">
			<?php 
				if($type1!=0) { 
					echo $pos_igst_gb; $pos_igst_tot=$pos_igst_tot+$pos_igst_gb;
				} else { 
						echo $pos_igst; $pos_igst_tot=$pos_igst_tot+$pos_igst;
				}
		    ?>
		  </td>
		  <td align="center">
			<?php 
				if($type1!=0) { 
						echo $pos_utgst_gb; $pos_utgst_tot=$pos_utgst_tot+$pos_utgst_gb;
				} else { 
						echo $pos_utgst; $pos_utgst_tot=$pos_utgst_tot+$pos_utgst;
				}
		    ?>
		  </td>
        <!--  <td align="center">
			<?php 
				//if($type1!=0){ echo $pos_tax1; $tot_pos_tax=$tot_pos_tax+$pos_tax1 ;} else { echo  $pos_tax; $tot_pos_tax=$tot_pos_tax+$pos_tax;} 
			?>
		  </td>-->
		  
		  <td align="center">
			<?php 
				if($type1!=0){ echo $tot_ch1; $tot_charg=$tot_charg+$tot_ch1;}else{ echo $tot_chg; $tot_charg=$tot_charg+$tot_chg;}
		    ?>
		  </td>
		  <td align="center">
			<?php 
				if($type1!=0) { 
					echo $tot_ch_tax_sgst; $total_charge_tax_sgst=$total_charge_tax_sgst+$tot_ch_tax_sgst;
				} else { 
					echo $tot_chg_tax_sgst; $total_charge_tax_sgst=$total_charge_tax_sgst+$tot_chg_tax_sgst;
				}
		    ?>
		  </td>
		  <td align="center">
			<?php 
				if($type1!=0) { 
					echo $tot_ch_tax_cgst; $total_charge_tax_cgst=$total_charge_tax_cgst+$tot_ch_tax_cgst;
				} else { 
					echo $tot_chg_tax_cgst; $total_charge_tax_cgst=$total_charge_tax_cgst+$tot_chg_tax_cgst;
				}
		    ?>
		  </td>
		  <td align="center">
			<?php 
				if($type1!=0) { 
					echo $tot_ch_tax_igst; $total_charge_tax_igst=$total_charge_tax_igst+$tot_ch_tax_igst;
				} else { 
					echo $tot_chg_tax_igst; $total_charge_tax_igst=$total_charge_tax_igst+$tot_chg_tax_igst;
				}
		    ?>
		  </td>
		  <td align="center">
			<?php 
				if($type1!=0) { 
					echo $tot_ch_tax_utgst; $total_charge_tax_utgst=$total_charge_tax_utgst+$tot_ch_tax_utgst;
				} else { 
					echo $tot_chg_tax_utgst;  $total_charge_tax_utgst=$total_charge_tax_utgst+$tot_chg_tax_utgst;
				}
		    ?>
		  </td>
		   <td align="center">
			<?php 
				if($type1!=0) { 
					echo $tot_ch_tax;
					$tot_charg_tax=$tot_charg_tax+$tot_ch_tax;
				} else { 
					echo $tot_chg_tax;
					$tot_charg_tax=$tot_charg_tax+$tot_chg_tax;
				}
		    ?>
		  </td>
		  <td align="center"><?php  echo  0;?></td> 
			 
		  <td align="center">
		  <?php if($type1!=0){ echo $discount_amount1; $tot_dis=$tot_dis+$discount_amount1;}else{ echo $discount_amount;$tot_dis=$tot_dis+$discount_amount; }?></td>
		  
		  <td align="center">
			<?php if($type1!=0){ echo $adj_amt_cr1 ;  $tot_cr= $tot_cr+$adj_amt_cr1;} else{ echo $adj_amt_cr; $tot_cr= $tot_cr+$adj_amt_cr;}?></td>
		  
		  <td align="center">
			<?php if($type1!=0){ echo $adj_amt_dr1; $tot_dr= $tot_dr+$adj_amt_dr1; } else{ echo $adj_amt_dr;  $tot_dr= $tot_dr+$adj_amt_dr;}?></td>
          
		  <td align="center">
			<?php 
				
				echo number_format($total,2,".","");
			?>
		  </td>
		  
          <td align="center">
			<?php 
				if($paid>0 || $paid1>0) {
					if($type1!=0) { 
						echo $paid1 ;  $Tot_paid=$Tot_paid+ $paid1;
					} else { 
						echo $paid; 
						$Tot_paid=$Tot_paid+$paid;
					}
				} else { 
					echo "0" ;
				} 
				
				
				if(isset($transaction1) && $transaction1){

					$cash = 0;
					$draft = 0;
					$card = 0;
					$fund = 0;
					$cheque = 0;
					$ewallet = 0;

					//echo '<span style="font-size:10px">';
                    foreach($transaction1 as $ts){
						//echo '</br>';
						//echo $ts->t_amount.' - '.$ts->t_payment_mode;
						
						if($paid1 != 0 || $paid != 0){
							if($ts->t_payment_mode == 'cash')
							$cash = $cash + $ts->t_amount;
							if($ts->t_payment_mode == 'draft')
							$draft = $draft + $ts->t_amount;
							if($ts->t_payment_mode == 'cards')
								$card = $card + $ts->t_amount;
							if($ts->t_payment_mode == 'fund')
								$fund = $fund + $ts->t_amount;
							if($ts->t_payment_mode == 'cheque')
								$cheque = $cheque + $ts->t_amount;
							if($ts->t_payment_mode == 'ewallet')
								$ewallet = $ewallet + $ts->t_amount;
						}
					}
					//echo '</span>';
                }
			?>
		  </td >
		  
		  <td align="center">
			<?php
				// Cash Payment
				if($paid1 != 0 || $paid != 0){
					echo $cash;
					$cashT = $cashT + $cash;
				} else echo 0;
			?>
		  </td>
		  <td align="center">
			<?php
				// Cash Payment
				if($paid1 != 0 || $paid != 0){
					echo $draft;
					$draftT = $draftT + $draft;
				} else echo 0;
			?>
		  </td>
		  <td align="center">
			<?php
				if($paid1 != 0 || $paid != 0){
					echo $card;
					$cardT = $cardT + $card;
				} else echo 0;
			?>
		  </td>
		  <td align="center">
			<?php
				if($paid1 != 0 || $paid != 0){
					echo $fund;
					$fundT = $fundT + $fund;
				} else echo 0;
			?>
		  </td>
		  <td align="center">
			<?php
				if($paid1 != 0 || $paid != 0){
					echo $cheque;
					$chequeT = $chequeT + $cheque;
				} else echo 0;
			?>
		  </td>
		  <td align="center">
			<?php
				if($paid1 != 0 || $paid != 0){
					echo $ewallet;
					$ewalletT = $ewalletT + $ewallet;
				} else echo 0;
			?>
		  </td>
		  <td align="center">
			<?php 
				if($cancel_amt>0) { echo $cancel_amt;}else{ echo 0 ;}
			?>
		  </td>
		  
        </tr>
		  <?php  }} ?>
      </tbody>
      <tfoot>
        <tr>
        	<td></td>
        	<td></td>
        	<td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          <td style="font-weight:700; color:#666666; text-align:center;"> TOTAL </td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_rr) && $tot_rr)  echo number_format($tot_rr,2,".",","); else echo "0";?>" /></td>		  		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($rr_sgst) && $rr_sgst) echo  number_format($rr_sgst,2,".",","); else echo "0";?>" /></td>		  		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($rr_cgst) && $rr_cgst) echo  number_format($rr_cgst,2,".",","); else echo "0";?>" /></td>		  		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($rr_igst) && $rr_igst) echo  number_format($rr_igst,2,".",","); else echo "0";?>" /></td>		  		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($rr_utgst) && $rr_utgst) echo  number_format($rr_utgst,2,".",","); else echo "0";?>" /></td>		  
         <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($s_ch) && $s_ch) echo number_format($s_ch,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_mp) && $tot_mp) echo number_format($tot_mp,2,".",","); else echo "0";?>" /></td>		  		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($mp_sgst) && $mp_sgst) echo  number_format($mp_sgst,2,".",","); else echo "0";?>" /></td>		  		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($mp_cgst) && $mp_cgst) echo  number_format($mp_cgst,2,".",","); else echo "0";?>" /></td>		  		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($mp_igst) && $mp_igst) echo  number_format($mp_igst,2,".",","); else echo "0";?>" /></td>		  		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($mp_utgst) && $mp_utgst) echo  number_format($mp_utgst,2,".",","); else echo "0";?>" /></td>
        <!--  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php //if(isset($tot_mp_tax) && $tot_mp_tax) echo number_format($tot_mp_tax,2,".",","); else echo "0";?>" /></td>-->
          
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($pos_tot_amt) && $pos_tot_amt) echo number_format($pos_tot_amt,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($pos_sgst_tot) && $pos_sgst_tot) echo number_format($pos_sgst_tot,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($pos_cgst_tot) && $pos_cgst_tot) echo number_format($pos_cgst_tot,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($pos_igst_tot) && $pos_igst_tot) echo number_format($pos_igst_tot,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($pos_utgst_tot) && $pos_utgst_tot) echo number_format($pos_utgst_tot,2,".",","); else echo "0";?>" /></td>
         <!-- <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php //if(isset($tot_pos_tax) && $tot_pos_tax) echo number_format($tot_pos_tax,2,".",","); else echo "0";?>" /></td>-->
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_charg) && $tot_charg) echo number_format($tot_charg,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($total_charge_tax_sgst) && $total_charge_tax_sgst) echo number_format($total_charge_tax_sgst,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($total_charge_tax_cgst) && $total_charge_tax_cgst) echo number_format($total_charge_tax_cgst,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($total_charge_tax_igst) && $total_charge_tax_igst) echo number_format($total_charge_tax_igst,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($total_charge_tax_utgst) && $total_charge_tax_utgst) echo number_format($total_charge_tax_utgst,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_charg_tax) && $tot_charg_tax) echo number_format($tot_charg_tax,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_dis) && $tot_dis) echo number_format($tot_dis,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_cr) && $tot_cr) echo number_format($tot_cr,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_dr) && $tot_dr) echo number_format($tot_dr,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php  if(isset($Tot_total) && $Tot_total) echo number_format($Tot_total,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($Tot_paid) && $Tot_paid) echo number_format($Tot_paid,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($cashT) && $cashT) echo number_format($cashT,2,".",","); else echo "0";?>" /></td>
		  <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($draftT) && $draftT) echo number_format($draftT,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($cardT) && $cardT) echo number_format($cardT,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($fundT) && $fundT) echo number_format($fundT,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($chequeT) && $chequeT) echo number_format($chequeT,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($ewalletT) && $ewalletT) echo number_format($ewalletT,2,".",","); else echo "0";?>" /></td>
		   <td>	<input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_cancel_amt) && $tot_cancel_amt)  echo number_format($tot_cancel_amt,2,".",","); else echo "0";?>" />
		  </td>	
	   </tr>
      </tfoot>
    </table>
  </div>
</div>
<script>

$(document).ready(function(){
  $('#chkbx_tdy').prop('checked', true);
});

function check_sub(){
  document.getElementById('form_date').submit();
}
</script> 
