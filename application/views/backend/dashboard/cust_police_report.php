<?php if($this->session->flashdata('err_msg')):?>
    <div class="form-group">
        <div class="col-md-12 control-label">
            <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong><?php echo $this->session->flashdata('err_msg');?></strong>
            </div>
        </div>
    </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
    <div class="form-group">
        <div class="col-md-12 control-label">
            <div class="alert alert-success alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong><?php echo $this->session->flashdata('succ_msg');?></strong>
            </div>
        </div>
    </div>
<?php endif;?>
<div class="portlet light borderd">
    <div class="portlet-title">
        <div class="caption" id="report">
            <i class="glyphicon glyphicon-bed"></i> Guest Police Report
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" class="config">
            </a>
            <a href="javascript:;" class="reload">
            </a>
            <a href="javascript:;" class="remove">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                	<div class="form-inline">
                <input type="text" autocomplete="off" required="required" value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date">
                <input type="text" autocomplete="off" required="required" value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" class="form-control date-picker" placeholder="End Date">
            <button class="btn btn-default" onclick="check_sub()" type="submit">Search</button>
                	</div>
                </div>
                <?php 
            if(isset($start_date) && isset($end_date))
            {
                $s_d = $start_date;
                $e_d  =$end_date;
            }	
            else
            {	
                
                $s_d = "";
                $e_d ="";
            }						
            ?>
            </div>
        </div>
        <table class="table table-striped table-hover table-bordered" id="sample_1">
            <thead>
            <tr>
                <!-- <th scope="col">
                        Select
                    </th> -->
                <th>
                    Sl No.
                </th>
                <th>
                    Room No
                </th>
                <th>
                    Status
                </th>
                <th>
                   Guest Name
                </th>
                <th>
                    Guest Address
                </th>
                <th>
                    No of Guest
                </th>
                <th>
                    M
                </th>
                <th>
                    F
                </th>
                <th>
                    C
                </th>

                <th>
                    Age
                </th>
                <th>
                    Relation
                </th>
                <th>
                    Bed Capacity
                </th>
            </tr>
            </thead>
            <tbody>
            
            <?php $guest=0; ?>
            
            
            
            
            <?php if(isset($bookings) && $bookings):
                $i=0;
                //echo "<pre>";
                    //print_r($bookings);exit;
                $deposit=0;

            foreach($bookings as $booking):
            if( $booking->booking_status_id==5 || $booking->booking_status_id==6):
                        

                        $rooms=$this->dashboard_model->get_room_number($booking->room_id);
                        
                        
                         $booking->booking_status_id;

                                $date1=date_create($booking->cust_from_date);
                                $date2=date_create($booking->cust_end_date);
                                $current_date= new DateTime('now');
                                $diff=date_diff($current_date, $date1);

                             // if($date1 <=$current_date && $date2<=$current_date):

                                    $guest=$guest+$booking->no_of_guest;

                                    $polices = $this->dashboard_model->get_booking_details_unique($booking->booking_id);
                                    // _sb
                                    //print_r($polices);exit;
                                        
                                if($polices && isset($polices))
                                {		
                                    foreach($polices as $police):

                                    ?>
                                    <tr>
                                        <td><?php echo $i+1; ?></td>
                                        <td><?php $room=$this->dashboard_model->get_room_number_exact($polices->room_id);

                                            $rNo=$room->room_no; if($rNo){echo $rNo;}else{ echo ''; } ?></td>
                                            <td><?php 
                                            if(isset($booking->booking_status_id)){
                                            $status=$this->dashboard_model->get_bookings_status($booking->booking_status_id);
                                            }
                                            echo $status->booking_status;
                                            ?></td>
                                        <td><?php echo $polices->cust_name ?></td>
                                        <td><?php 
                                        // _sb
                                        $guest_id = $polices->guest_id;												
                                        /*$guest_details = $this->dashboard_model->get_guest_detail($guest_id);
                                        if(isset($guest_details) && $guest_details){
                                            foreach($guest_details as $g_details){
                                                echo  $g_details->g_address.", ".$g_details->g_city.", ".$g_details->g_state.", Pin - ".$g_details->g_pincode;
                                            }
                                        }*/
                                        ?><?php if(isset($polices->g_address)){ echo $polices->g_address;} ?>, <?php if(isset($polices->g_city)){ echo $polices->g_city;} ?>, <?php if(isset($polices->g_state)){ echo $polices->g_state;} ?><br>
                                        Pin - <?php if(isset($polices->g_pincode)){ echo $polices->g_pincode;} ?>
                                        </td>
                                        <td><?php echo $polices->no_of_guest ?></td>
                                        <td><?php echo $polices->no_of_adult_male ?></td>
                                        <td><?php echo $polices->no_of_adult_female ?></td>
                                        <td><?php echo $polices->no_of_child ?></td>
                                        <td><?php
                                                if(isset($polices->g_dob) && $polices->g_dob >0){
                                                //echo $polices->g_dob;	
                                                $d1 = new DateTime($polices->g_dob);
                                                $d2 = new DateTime();

                                                $diff = $d2->diff($d1);
                                                // _sb
                                                if($diff->y == 0){
                                                    echo "N/A";
                                                }
                                                else {
                                                    echo $diff->y;
                                                }													 
                                            } else {
                                                echo "N/A";
                                            }
                                            ?></td>
                                        <td>Self</td>
                                        <td><?php $room=$this->dashboard_model->get_room_number_exact($polices->room_id);
                                    
                                            $bedCapa=$room->room_bed;
											if($bedCapa){echo $bedCapa;}else{ echo '';}
											
											?></td>

                                    </tr>
                                <?php break; endforeach;  }?>


                                    <?php $i++;?>

                                <?php endif; ?>
                <?php //endif; ?>
                <?php endforeach; ?>
            <?php endif;?>
            </tbody>
        </table>
    </div>

</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/common/js/table_sort_style.js"></script>
<script>


function check_sub(){
  document.getElementById('form_date').submit();
}
</script>
