<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="glyphicon glyphicon-bed"></i>Daily Sales Report </div>
    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-8">
          <?php

                        $form = array(
                            'class'       => 'form-inline',
                            'id'        => 'form_date',
                            'method'      => 'post'
                        );

                        echo form_open_multipart('dashboard/daily_sales_report',$form);

                        ?>
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" id="t_dt_frm" value="<?php if(isset($start_date)){ echo $start_date;}?>"  name="t_dt_frm" class="form-control date-picker" placeholder="Start Date">
          </div>
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" name="t_dt_to" value="<?php if(isset($end_date)){ echo $end_date;}?>"  class="form-control date-picker" placeholder="End Date">
          </div>
          <button class="btn btn-default" onclick="check_sub()" type="submit">Search</button>
          <?php form_close(); ?>
        </div>
      </div>
    </div>
    <table class="table table-striped table-hover table-bordered" id="sample_3">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Booking Id</th>
          <th scope="col">Date</th>
          <th scope="col">Booking Source</th>
          <th scope="col">Room No</th>
          <th scope="col">Room Type</th>
          <th scope="col">Guest Name</th>
          <th scope="col">Room Rent</th>
          <th scope="col">L.Tax</th>
          <th scope="col">S.Tax</th>
          <th scope="col">S.Charges</th>
          <th scope="col">Meal Plan Rent</th>
          <th scope="col">MP Tax</th>
          <th scope="col">Ex Meal</th>
          <th scope="col">Ex Tax</th>
          <th scope="col">POS Amount</th>
          <th scope="col">POS Tax</th>
          <th scope="col">Total Amount</th>
          <th scope="col">Paid Amount</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          
          if($sales && isset($sales)){
            $total=0;
            $pos_tot_amount=0;
            $rr_tax=0;	
                $mp_ch=0;	
                $mp_tax=0;	
                $ex_ch=0;	
                $exmp_ch=0;	
                $exmp_tax=0;
                $srl_no=0;
                $gbbookings=0;
                $str='';
                $tot_rr=0;
                $l_tax=0;
                $s_tax=0;
                $s_ch=0;
                $tot_mp=0;			 
                $tot_mp_tax=0;			 
                $tot_ex_ch=0;			 
                $tot_exmp_ch=0;			 
                $tot_exmp_tax=0;			 
                $pos_amount=0;
                $Tot_total=0;					
                $Tot_paid=0;
				$pos_tot_amt=0;	
				$pos_tax=0;				
				$pos_tax1=0;	
				$tot_pos_tax=0;	
                $rr_tot=0;				
                foreach($sales as $sl){
            
                 $srl_no++;
             $booking_id=$sl->t_booking_id; 
             $gp_id=$sl->t_group_id; 

                if($booking_id>0 && $booking_id){
                    
                $bookings=$this->dashboard_model->get_booking_details($booking_id);
                
                $transaction = $this->bookings_model->get_total_payment($booking_id);

            if(isset($transaction) && $transaction){
                    foreach($transaction as $ts){
                    $paid=$ts->t_amount;
                }
                }
                
            $poses=$this->dashboard_model->all_pos_booking($booking_id);
            $pos_amount=0;
            
                if($poses){
                foreach ($poses as $key ) {

                  $pos_amount=$pos_amount+$key->total_due;
                  $pos_tot_amount=$key->total_amount;
                  $pos_tax=$key->tax;
                }}	
                
                
                
                }else{
                    $gbbookings=$this->dashboard_model->get_group($gp_id);
                   
                    
                    $transaction_gb = $this->bookings_model->get_total_payment_gp($gp_id);
                
            if(isset($transaction_gb) && $transaction_gb){
                    foreach($transaction_gb as $tn){
                    $paid1=$tn->t_amount;
                }
                }
                    
                foreach($gbbookings as $gb){
                        //echo	$gb->name;
                        $unit_id=$this->bookings_model->get_unitId1($gb->id);
        
        foreach($unit_id as $u_id){
            $rm_No[]=$this->bookings_model->get_rmNo1($u_id->room_id);
        }
        foreach($rm_No as $value){
			
         $str.=" ".$value->room_no.',';
       }
       
       }
       
       $poses_gb=$this->dashboard_model->all_pos_booking_group($gp_id);
            $pos_amount1=0;
            
                if($poses_gb){
                foreach ($poses_gb as $key1 ) {

                  $pos_amount1=$pos_amount1+$key1->total_due;
                  $pos_tot_amount1=$key1->total_amount;
				  $pos_tax1=$key1->tax;
                }}
                
                }
                
            if($gp_id==0){	
            $type='sb';
            $tax=$this->bookings_model->line_charge_item_tax($type,$booking_id);
                //print_r($tax);	
              
            
            }else{
                $type='gb';
                $tax=$this->bookings_model->line_charge_item_tax($type,$gp_id);
            
            }
              if(isset($tax['mp_tax']['total']) && isset($tax['all_price']['room_rent']) && isset($tax['rr_tax']['total']) && isset($tax['rr_tax']['total']) && isset($tax['all_price']['meal_plan_chrg']) && isset($tax['mp_tax']['total']) && isset($tax['all_price']['exrr_chrg']) && isset($tax['all_price']['exmp_chrg']) && isset($tax['all_price']['exmp_total_tax'])) { 
             $rr_tot=$tax['all_price']['room_rent'];	
               $rr_tax=$tax['rr_tax']['total'];	
                $mp_ch=$tax['all_price']['meal_plan_chrg'];	
                $mp_tax=$tax['mp_tax']['total'];	
                $ex_ch=$tax['all_price']['exrr_chrg'];	
                $exmp_ch=$tax['all_price']['exmp_chrg'];	
                $exmp_tax=$tax['all_price']['exmp_total_tax'];
			  }            
		
                

		$total=$rr_tot+
		$rr_tax+
		$mp_ch+
		$mp_tax+
		$ex_ch+
		$exmp_ch+
		$exmp_tax+
		$pos_tot_amount;
		
   $tot_rr=$tot_rr+ $rr_tot;			 
     $l_tax=$l_tax+ $tax['rr_tax']['Luxury tax'];			 
     $s_tax=$s_tax+ $tax['rr_tax']['Service Tax'];			 
     $s_ch=$s_ch+ $tax['rr_tax']['Service Charge'];			 
     $tot_mp=$tot_mp+$mp_ch;			 
     $tot_mp_tax=$tot_mp_tax+$mp_tax;			 
     $tot_ex_ch=$tot_ex_ch+$ex_ch;			 
     $tot_exmp_ch=$tot_exmp_ch+$exmp_ch;			 
     $tot_exmp_tax=$tot_exmp_tax+$exmp_tax;			 
  	 
     $Tot_total=$Tot_total+$total;	
     
	
         ?>
        <tr>
          <td align="center"><?php echo $srl_no ;?></td>
          <td align="center"><?php if($gbbookings!=''){ echo "BKGP006".$gb->id;} else{ echo $bookings->booking_id_actual;}?></td>
          <td align="center"><?php if($gbbookings!=''){ echo $gb->start_date.' to '.$gb->start_date;} else {echo $bookings->cust_from_date_actual." to ".$bookings->cust_end_date_actual ;}?></td>
          <td align="center"><?php

            if($gbbookings!=''){ echo $gb->booking_source ;} else {	


            $sc_id=$bookings->booking_source;
            if(isset($sc_id) && $sc_id){
            $bk=$this->dashboard_model->getsourceId($sc_id);
            echo $bk->	booking_source_name;
            }else{
                echo "N/A";
            }
            }
            ?></td>
          <td align="center"><?php  
              if($gbbookings!=''){ echo $str ;} else {
              
              $rm_name=$this->dashboard_model->get_room_by_id($bookings->room_id);
                    foreach($rm_name as $rm){
                    
                        echo $rm->room_no."</br>";
                    }
              }
              ?></td>
          <td><?php if($gbbookings!=''){ echo "N/A"; } else {echo $rm->unit_name;}?></td>
          <td align="center"><?php if($gbbookings!=''){ echo $gb->name ;} else echo $bookings->cust_name;?></td>
          <td align="center"><?php echo $rr_tot;?></td>
          <td align="center"><?php  echo number_format($tax['rr_tax']['Luxury tax'],2,"."," ");?></td>
          <td align="center"><?php  echo number_format($tax['rr_tax']['Service Tax'],2,"."," ");?></td>
          <td align="center"><?php echo number_format($tax['rr_tax']['Service Charge'],2,"."," ")?></td>
          <td align="center"><?php   echo $mp_ch;?></td>
          <td align="center"><?php   echo  $mp_tax ;?></td>
          <td align="center"><?php  echo $exmp_ch; ?></td>
          <td align="center"><?php  echo  $exmp_tax;?></td>
          <td align="center"><?php if($gbbookings!=''){ echo $pos_amount1 ; $pos_tot_amt=$pos_tot_amt+$pos_amount1 ;} else { echo  $pos_amount; $pos_tot_amt=$pos_tot_amt+$pos_amount;} ?></td>
          <td align="center"><?php if($gbbookings!=''){ echo $pos_tax1; $tot_pos_tax=$tot_pos_tax+$pos_tax1 ;} else { echo  $pos_tax; $tot_pos_tax=$tot_pos_tax+$pos_tax;} ?></td>
          <td align="center"><?php echo $total  ; ?></td>
          <td align="center"><?php if($gbbookings!=''){ echo $paid1 ;$Tot_paid=$Tot_paid+ $paid1;} else { echo $paid; $Tot_paid=$Tot_paid+$paid;} ?></td>
        </tr>
        <?php  }}?>
      </tbody>
      <tfoot>
        <tr>
        	<td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          <td style="font-weight:700; color:#666666; text-align:center;"> TOTAL </td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_rr) && $tot_rr)  echo number_format($tot_rr,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($l_tax) && $l_tax) echo  number_format($l_tax,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($s_tax) && $s_tax) echo number_format($s_tax,2,".",","); else echo "0";?>"  /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($s_ch) && $s_ch) echo number_format($s_ch,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_mp) && $tot_mp) echo number_format($tot_mp,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_mp_tax) && $tot_mp_tax) echo number_format($tot_mp_tax,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_ex_ch) && $tot_ex_ch) echo number_format($tot_ex_ch,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_exmp_tax) && $tot_exmp_tax) echo number_format($tot_exmp_tax,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($pos_tot_amt) && $pos_tot_amt) echo number_format($pos_tot_amt,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($tot_pos_tax) && $tot_pos_tax) echo number_format($tot_pos_tax,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php  if(isset($Tot_total) && $Tot_total) echo number_format($Tot_total,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:center;" value="<?php if(isset($Tot_paid) && $Tot_paid) echo number_format($Tot_paid,2,".",","); else echo "0";?>" /></td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>
<script>

$(document).ready(function(){
  $('#chkbx_tdy').prop('checked', true);
});

function check_sub(){
  document.getElementById('form_date').submit();
}
</script> 
