<!-- BEGIN PAGE CONTENT-->
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>

<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Stock/Inventory</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/def_stock_invent',$form);

                        ?>
    <div class="form-body"> 
      <!-- 17.11.2015-->
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <!-- 17.11.2015-->
      
      <div class="row">
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select"  name="a_type" required >
              <option value="" disabled="disabled" selected="selected">Select Type</option>
              <?php 
                    if(isset($type)){
                        //print_r($type);
                        //exit;
                    foreach($type as $types){
                            
            ?>
              <option value="<?php echo $types->asset_type_name;?>"><?php echo $types->asset_type_name;?></option>
              <?php }}?>
            </select>
            <label></label>
            <span class="help-block">Type *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select"  name="a_category" required >
              <option value="" disabled="disabled" selected="selected">Select Category</option>
              <?php 
                if(isset($category)){
                    //print_r($category);
                    //exit;
                foreach($category as $categorys){
                        
        ?>
              <option value="<?php echo $categorys->name;?>"><?php echo $categorys->name;?></option>
              <?php }}?>
            </select>
            <label></label>
            <span class="help-block">Category *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="a_name" name="a_name" onblur="chk_name(this.value)" placeholder="Assets Name">
          <label></label>
          <span class="help-block">Assets Name</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="make" placeholder="Make">
              <label></label>
              <span class="help-block">Make *</span></div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="i_des" placeholder="Item Desription">
              <label></label>
              <span class="help-block">Item Desription</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="u_price" required="required" placeholder="Unit Cost Price">
          <label></label>
          <span class="help-block">Unit cost Price</span> </div>
		 
        </div> <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="s_price" required="required" placeholder="Unit Selling Price">
          <label></label>
          <span class="help-block">Unit Selling Price</span> </div>
		 
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="unit" required="required" placeholder="Unit">
          <label></label>
          <span class="help-block">Unit</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <select class="form-control bs-select"  name="warehouse" required >
          	<option value="" disabled="disabled" selected="selected">Select Warehouse</option>
            <?php if(isset($warehouse)){
            //print_r($warehouse);
            foreach($warehouse as $wh){
          ?>
            <option value="<?php echo $wh->id;?>"><?php echo $wh->name;?></option>
            <?php }}?>
          </select>
          <label></label>
          <span class="help-block">Select Warehouse *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="l_stock" placeholder="Low Stock Alert Qty">
          <label></label>
          <span class="help-block">Low Stock Alert Qty</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="note" placeholder="Note">
          <label></label>
          <span class="help-block">Note</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select name="importance" id="priority" class="form-control bs-select">
              	<option value="" disabled="disabled" selected="selected">Importance</option>
                <option value="1">High</option>
                <option value="2">Medium</option>
                <option value="3">Low</option>
              </select>
              <label></label>
          	  <span class="help-block">Importance</span>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
            	<div class="col-md-4">
                <div class="form-group form-md-line-input uploadss">
                	<label>Upload Images</label>
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                    <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('a_image');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                  </div>
                </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn submit" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>
<script>

function chk_name(value){
	var name=value;
	//alert(name);
	
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/chk_duplicate_name",
	   data: {data:name},
	   success: function(msg){
		   if(msg.data=="1"){
			   swal({
                            title: "Duplicate Stock Name ",
                            text: "",
                            type: "warning"
                        },
                        function(){
							 document.getElementById("a_name").value = "";
                           // location.reload();

                        });
		   }
		   // $("#hd").html(msg);
	   }
	});
	
	}

   function check_corporate(value){

       if(value =="corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
       }else{
           document.getElementById("c_name").style.display="none";
           document.getElementById("c_des").style.display="none";
       }
   }
    function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});

function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	   //alert(y);
	    
	   if(y=="check"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fundtransfer"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }
</script> 
