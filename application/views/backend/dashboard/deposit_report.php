<!---->
<div class="row">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">            
            <div class="portlet-body form">
                
                    <div class="form-body">
                    	<div class="form-group">
                        	<div class="row">
                                <div class="col-md-6">
                                	<div class="row">
									<?php

                            $form = array(
                                'class' 			=> 'form-body',
                                'id'				=> 'form',
                                'method'			=> 'post',								
                            );
							
							

                            echo form_open_multipart('dashboard/monthly_summary_report/',$form);

                            ?>
                                        <label class="col-md-3 control-label">Month</label>
                                        <div class="col-md-4">
                                            <select class="form-control input-sm" name="month">
											 <option value="<?php $d=strtotime("-6 months");echo date('m',$d);?>">
												<?php $d=strtotime("-6 Months");echo date('M',$d);?>
												</option>
                                                 <option value="<?php $d=strtotime("-5 months");echo date('m',$d);?>">
												<?php $d=strtotime("-5 Months");echo date('M',$d);?>
												</option>
                                                <option value="<?php $d=strtotime("-4 months");echo date('m',$d);?>">
												<?php $d=strtotime("-4 Months");echo date('M',$d);?>
												</option>
											 <option value="<?php $d=strtotime("-3 months");echo date('m',$d);?>">
												<?php $d=strtotime("-3 Months");echo date('M',$d);?>
												</option>
                                                 <option value="<?php $d=strtotime("-2 months");echo date('m',$d);?>">
												<?php $d=strtotime("-2 Months");echo date('M',$d);?>
												</option>
                                                <option value="<?php $d=strtotime("-1 months");echo date('m',$d);?>">
												<?php $d=strtotime("-1 Months");echo date('M',$d);?>
												</option>
                                                <option value="<?php echo date('m');?>" selected="selected"><?php echo date('M');?></option>
                                                <option value="<?php $d=strtotime("+1 months");echo date('m',$d);?>">
												<?php $d=strtotime("+1 Months");echo date('M',$d);?>
												</option>
                                                 <option value="<?php $d=strtotime("+2 months");echo date('m',$d);?>">
												<?php $d=strtotime("+2 Months");echo date('M',$d);?>
												</option>
                                                <option value="<?php $d=strtotime("+3 months");echo date('m',$d);?>">
												<?php $d=strtotime("+3 Months");echo date('M',$d);?>
												</option>
											 <option value="<?php $d=strtotime("+4 months");echo date('m',$d);?>">
												<?php $d=strtotime("+4 Months");echo date('M',$d);?>
												</option>
                                                 <option value="<?php $d=strtotime("+5 months");echo date('m',$d);?>">
												<?php $d=strtotime("+5 Months");echo date('M',$d);?>
												</option>
                                                
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control input-sm" name="year">
												<option value="<?php $d=strtotime("-2 Years");echo date('Y',$d);?>">
												<?php $d=strtotime("-2 Years");echo date('Y',$d);?>
												</option>
												<option value="<?php $d=strtotime("-1 Years");echo date('Y',$d);?>">
												<?php $d=strtotime("-1 Years");echo date('Y',$d);?>
												</option>
												 <option value="<?php echo date('Y');?>" selected="selected"><?php echo date('Y');?></option>
                                                <option value="<?php $d=strtotime("+1 Years");echo date('Y',$d);?>">
												<?php $d=strtotime("+1 Years");echo date('Y',$d);?>
												</option>
                                                
                                            </select>
											
                                        </div>
										 </div>
										 </div>
										<div class="form-group" >
                            <label>Checkboxes</label>
                            <div class="checkbox-list" class="col-md-3">
                                <label>
                                <input type="checkbox" name="ECR"> Exclude Complementary Rooms </label>
                                <label>
                                <input type="checkbox" name="ICO"> Include No Shows/Cancelletions/Other room charge in RoomCount</label>
                                <label>
								 <input type="checkbox" name="IPP"> Include POS Purchase(Other Charges)</label>
                                <label>
                                <input type="checkbox" disabled> Disabled </label>
                            </div>
                        </div>
                                   
                                
								</div>
                               
                    </div>
                    <div class="form-actions right">
                        <!--<button type="submit" class="btn blue">Submit</button>-->
						<input type="submit" class="btn blue" value="Submit">
                    </div>
					<?php form_close(); ?>
                
				
            </div>
        </div>
    </div>
</div>
<div class="row all_bk">
  <div class="col-md-12"> 
    <!-- BEGIN SAMPLE TABLE PORTLET-->    
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="glyphicon glyphicon-bed"></i>List of All Rooms </div>
        
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
							 
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            <div class="col-md-12">
              <div class="btn-group pull-right">
                <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu pull-right">
                  <li> <a href="javascript:;"> Print </a> </li>
                  <li> <a href="javascript:;"> Save as PDF </a> </li>
                 <!-- <li> <a href="javascript:;"> Export to Excel </a> </li>-->
                </ul>
              </div>
            </div>
          </div>
        </div>         
        <table class="table table-striped table-bordered table-hover" id="sample_1">
          <thead>
            <tr>
              <th scope="col">Days</th>
              <th scope="col">Guestname</th>
              <th scope="col">Checked In</th>
			  <th scope="col">Checked Out</th>
			  <th scope="col">Nights</th>
              <th scope="col">Lodging</th>
              <th scope="col">Other Charges</th>
              <th scope="col">Taxes</th>
			  <th scope="col">Bad Debts</th>
			  <th scope="col">Cash</th>
			  <th scope="col">Credit Card</th>
			  <th scope="col">Online Payment Gateway</th>
			  <th scope="col">Total Paid</th>
			  <th scope="col">Dep.Date</th>
            </tr>
          </thead>
          
          <tbody>
          	<tr>
			
             <?php
				//Day
				/*echo $number = cal_days_in_month(CAL_GREGORIAN, 02, 2016);
                    $y= date("Y");
                    $m= date("m"); 
                    $maxDays=date('t');*/
					$sqlTotal=0;
					$sum2=0;
					$sum3=0;
					$tax=0;
					$gross=0;
                    for($i=1; $i<=$maxDays; $i++){
                        if($i<10){
                          $val= $y.'-'.$m.'-'.'0'.$i;  
                        }else{
                          $val= $y.'-'.$m.'-'.$i; 
                        } 
                        $sql= $this->dashboard_model->roomcount($val);
                        $avgdailyrent=$this->dashboard_model->avgdailyrent($val);
						$sqlTotal +=$sql;
						
							
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$sum2=$sum2+$av->room_rent_total_amount;//exit();
								}
							}
							
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$sum3=$sum3+$av->service_price;//exit();
								}
							}
							
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$tax=$tax+$av->room_rent_tax_amount;//exit();
								}
							}
							
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$gross=$gross+$av->room_rent_tax_amount+$av->room_rent_total_amount+$av->service_price;//exit();
								}
							}
							
							
							
							
							
					}
						//$sum=0;
						//if(isset($avgdailyrent) && $avgdailyrent){
						//foreach($avgdailyrent as $avg){
						//echo $sum=$sum+$avg->room_rent_total_amount;
						
						?>     
            	<td align="center"><?php echo $maxDays;?></td>
                <td align="center">  <?php //Room Count
										echo $sqlTotal;?></td>
                <td align="center"><?php
					//Occupancy
										echo round(($sqlTotal*100)/($maxDays*$totalrooms),2).'%';?></td>
                <td align="center"><?php 
					//Room rent total
							echo $sum2;?></td>
                <td align="center"><?php
					//Room Total service tax
					echo $sum3;?></td>
                <td align="center"><?php echo round(($avg=$sum2/$sqlTotal),2);?></td>
                <td align="center"><?php
					//Total Tax
						echo $tax;?></td>
                <td align="center"><?php echo $gross;?></td>
				<td align="center"><?php echo $gross;?></td>
				<td align="center"><?php echo $gross;?></td>
				<td align="center"><?php echo $gross;?></td>
				<td align="center"><?php echo $gross;?></td>
				<td align="center"><?php echo $gross;?></td>
				<td align="center"><?php echo $gross;?></td>
                
						
            </tr>
           
          </tbody>
        </table>
      </div>
    </div>
    <!--<div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="glyphicon glyphicon-bed"></i>List of All Rooms </div>        
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" class="config">
            </a>
            <a href="javascript:;" class="reload">
            </a>
            <a href="javascript:;" class="remove">
            </a>
        </div>		 
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">            
            <div class="col-md-12">
              <div class="btn-group pull-right">
                <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu pull-right">
                  <li> <a href="javascript:;"> Print </a> </li>
                  <li> <a href="javascript:;"> Save as PDF </a> </li>
                  <li> <a href="javascript:;"> Export to Excel </a> </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="sample_editable_2">
        	<thead>
            <tr>
              <th scope="col">Day</th>
              <th scope="col">Room Count</th>
              <th scope="col">Occupancy %</th>
			  <th scope="col">Room Rent</th>
			  <th scope="col">Room Inclusion</th>
              <th scope="col">Avg.Daily Rate (Incl.Inclusion)</th>
              <th scope="col">Total Taxes</th>
              <th scope="col">Gross total</th>
            </tr>
          </thead>
            <tbody>
                <?php
				//Day
                    /*$y= date("Y");
                    $m= date("m"); 
                    $maxDays=date('t');*/
					
                    for($i=1; $i<=$maxDays; $i++){
                        if($i<10){
                          $val= $y.'-'.$m.'-'.'0'.$i;  
                        }else{
                          $val= $y.'-'.$m.'-'.$i; 
                        } 
                        $sql= $this->dashboard_model->roomcount($val);
                        $avgdailyrent=$this->dashboard_model->avgdailyrent($val);
						
						//$sum=0;
						//if(isset($avgdailyrent) && $avgdailyrent){
						//foreach($avgdailyrent as $avg){
							//echo $sum=$sum+$avg->room_rent_total_amount;
						
                ?>      

            <tr>
                <td align="center">
								<?php //Day
										echo $i; ?>
				</td>
                <td align="center">
								<?php //Room Count
										echo $sql;?>
				</td>
                <td align="center">
				<?php //Occupancy  
						echo round((($sql/$totalrooms)*100), 2).'%'; ?>
				</td>
				<td align="center"><?php 
				//Room Rent
								$sum=0; 
								if(isset($avgdailyrent) && $avgdailyrent){		
									foreach($avgdailyrent as $avg){		
							    $sum=$sum+$avg->room_rent_total_amount;
							}
					}	
					if($sum == "0"){
						echo "0";
					}else{
						echo round($sum,2);
					}
				?></td>
				<td align="center"><?php 
					//Room Inclusion
							$sum2=0;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$sum2=$sum2+$av->service_price;//exit();
								}
							}
							if($sum2 == "0" || $sum2 == ""){
								echo "0";
							}
							else{
								echo $sum2;
							}?></td>
                <td align="center">
				<?php 
						//Avg.Daily Rate (Incl.Inclusion)
							if($sql=="0"){
								echo $avg="0";
							}else{
								echo round(($avg=$sum/$sql),2);
							}
				?>
				</td>
				
                <td align="center"><?php 
					//Room Tax
							$tax=0;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$tax=$tax+$av->room_rent_tax_amount;//exit();
								}
							}
							if($tax == "0"){
								echo "0";
							}
							else{
								echo $tax;
							}?>
				</td>
                <td align="center"><?php 
					//Gross Amount
							$gross=0;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$gross=$gross+$av->room_rent_tax_amount+$av->room_rent_total_amount+$av->service_price;//exit();
								}
							}
							if($gross == "0"){
								echo "0";
							}
							else{
								echo $gross;
							}?></td>
                
                
            </tr>
					<?php  }?>
          
            </tbody>
		</table>
      </div>-->
    </div>
  </div>
</div>
<!-- END PAGE CONTENT-->


