<?php if($this->session->flashdata('err_msg')):?>
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>All Discount Plans </div>
    <div class="actions">
    	<a class="btn btn-circle green btn-outline btn-sm" data-toggle="modal" href="#responsive" ><i class="fa fa-plus"></i>Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-6">
         <!--<div class="btn-group"><a href="<?php  echo base_url()?>dashboard/add_corporate">-->
		  
         <!-- </div>-->
        </div>        
      </div>
    </div>
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
         


          <th width="1%" scope="col"> Sl </th>
          <th width="10%" scope="col"> Rule Name </th>
          <th width="25%" scope="col"> Plan Description </th>
          
          <th width="10%" scope="col"> Discount Range % </th>
          <th width="10%" scope="col"> Current Disc %</th>
          <th width="10%" scope="col"> Rm rent only? </th>
          <th width="10%" scope="col"> Food Chrg only </th>         
          <th width="10%" scope="col"> Type </th>         
          <th width="10%" scope="col"> Applicable to</th>         
          <th width="4%" scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($discount_rule) && $discount_rule):
                        
                        $i=1;
						$sl = 0;
                        foreach($discount_rule as $discount):
                          $sl++; 
                           
                            ?>
        <tr id="row<?php echo $discount->id; ?>">
		  <td><?php echo $sl; ?></td>
		  <td>
			<?php 
				echo '<strong style="font-size:15px">'.$discount->rule_name.'</strong>';
			?>
		  </td>
          <td>
			<?php 
				echo $discount->description; 
			?>
		  </td>
          
          <td>
			<?php 
				echo $discount->discount_start_percentage.'<span style="color:#AAAAAA;"> to </span>'; 
				echo $discount->discount_end_percentage; 
			?>
		  </td>
          <td>
			<?php 
				echo '<span style="font-weight:bold;">'.$discount->dp_discount.'</span> <i class="fa fa-percent" aria-hidden="true"></i>'; 
			?>
		  </td>
          <td>
			<?php 
				if($discount->discount_room_rent_applicable == 1)
					echo '<span class="label-flat" style="color:#FFFFFF; background-color:#1C9A71;">Yes</span>';
				else
					echo '<span style="color:#AAAAAA;">No</span>';
			?>
		  </td>         
          <td>
			<?php 
				if($discount->food_charge_applicable == 1)
					echo '<span class="label-flat" style="background-color:#1C9A71;">Yes</span>';
				else
					echo '<span style="color:#AAAAAA;">No</span>';
			?>
		  </td>
		  
		  <td>Specific</td>
		  
          <td>
			<?php
				if($discount->applicable){
					//echo $discount->applicable;
					$features = $discount->applicable;
								
						$arr = (explode(",",$features));
						$l = count($arr);
						for($i=0;$i<$l;$i++){
							if($arr[$i] != ''){
								//$fe = $this->dashboard_model->get_room_feature_by_id($arr[$i]);
								if($arr[$i] == 'All Guest')
									echo '<span class="label-flat" style="background-color:#E46A85; color:#FFFFFF;">'.$arr[$i].'</span>'.' ';
								else
									echo '<span class="label-flat" style="background-color:#50C381; color:#FFFFFF;">'.$arr[$i].'</span>'.' ';
								
							}
						}	// end for loop
				}
				else
					echo '<span style="color:#AAAAAA;">No info</span>';
				/*if($k == 0)
					echo '<span style="color:#9C9C8F;">No info</span>'.'';*/
			?>
		  </td>
          
          <td class="ba">
            <div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $discount->id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
                <li><a onclick="edit_discount('<?php echo $discount->id; ?>')" href="#edit_corporate" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a></li>
              </ul>
            </div>
          </td>
		  
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<script>

$(document).on('blur', '#ced', function () {
	$('#ced').addClass('focus');
});
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/delete_discount_rule",
                data:{id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            $('#row'+id).remove();

                        });
                }
            });
        });
    }
</script>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('unit_class_controller/add_discount_rules',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" name="rule_name" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
		  <div class="col-md-6">
              <div class="form-group form-md-line-input ">
                <input autocomplete="off" type="text" name="rule_name" class="form-control" id="form_control_1" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Name *">
                <label></label>
                <div class="highlight"></div>
                <span class="help-block">Name</span> </div>
            </div >
		  
            		
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="discount_Start_parcentage" name="discount_Start_parcentage"  required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" placeholder="Discount Start Percentage">
                <label></label>
                <span class="help-block">Discount Start Percentage </span>
                <div class="highlight"></div>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="discount_End_parcentage" name="discount_End_parcentage"  required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" placeholder="Discount End Percentage">
                <label></label>
                <span class="help-block">Discount End Percentage </span>
                <div class="highlight"></div>
              </div>
            </div>
			
			
			<div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="discount" name="discount" onblur="discount_check(this.value)"  required="required" maxlength="10"  placeholder="Discount">
                <label></label>
                <span class="help-block">Discount</span>
                <div class="highlight"></div>
              </div>
            </div>
			
			
			
			
			
			
			
			
			
			
			<div class="col-md-6">
            <div class="form-group form-md-line-input">
              
				  <select class="form-control bs-select"   name="applicable[]" id="" required multiple>
                  <option value="All Guest">All Guest</option>
				  <option value="Corporate">Corporate</option>
				  <option value="All Family">All Family</option>
				  <option value="All Bachelor">All Bachelor</option>
				  <option value="All sr Citizen">All sr Citizen</option>
				  <option value="All Military">All Military</option>
				  <option value="All Military">All Retire Military</option>
				  <option value="All Tourist">All Tourist</option>
				  <option value="All Vip">All Vip</option>
				  <option value="All Internal Staff">All Internal Staff</option>
				  <option value="All Woman">All Woman</option>
				  

                </select>
              <span class="help-block">Applicable For</span> </div>
        </div>
			
			
            
            <div class="col-md-6">
               <div class="form-group form-md-line-input">
                <select class="form-control bs-select"   name="room_rent" id="unit_type" required >
                  <option value="1">Yes</option>
				  <option value="0">No</option>
                  
                </select>
                <label></label>
                <span class="help-block">Discount applicable for room rent only</span> 
              </div>
            </div>
			 <div class="col-md-6">
               <div class="form-group form-md-line-input">
                <select class="form-control bs-select"   name="food_charge" id="unit_type" required >
                  <option value="0"  selected="">Yes</option>
				  <option value="0"  selected="">No</option>
                  
                </select>
                <label></label>
                <span class="help-block">Applicable for food charges only</span> 
              </div>
            </div>
			
			<div class="col-md-12">
              <div class="form-group form-md-line-input ">
                <input autocomplete="off" type="text" name="description" class="form-control" id="form_control_1" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Description *">
                <label></label>
                <div class="highlight"></div>
                <span class="help-block">Description</span> </div>
            </div >
			
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<div id="edit_corporate" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('unit_class_controller/update_discount_rule',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Corporate</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
		  <div class="col-md-6">
              <div class="form-group form-md-line-input ">
                <input autocomplete="off" type="text" name="rule_name1" class="form-control" id="rule_name1" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Name *">
                <label></label>
                <div class="highlight"></div>
                <span class="help-block">Name</span> </div>
            </div >
		  
            			
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="discount_start_percentage1" name="discount_start_percentage1"  required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" placeholder="Discount Start Percentage ">
                <label></label>
                <span class="help-block">Discount Start Percentage </span>
                <div class="highlight"></div>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="discount_end_percentage1" name="discount_end_percentage1"  required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" placeholder="Discount End Percentage ">
                <label></label>
                <span class="help-block">Discount End Percentage </span>
                <div class="highlight"></div>
              </div>
            </div>
			
			
			 <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="discount1" name="discount1"  required="required" maxlength="10" onblur="discount_check_edit(this.value)" placeholder="Discount">
                <label></label>
                <span class="help-block">Discount</span>
                <div class="highlight"></div>
              </div>
            </div>
			
			
			
			
			
            <div class="col-md-6">
			  <div class="form-group form-md-line-input">              
				 <select class="form-control bs-select"   name="applicable1[]" id="applicable1" required multiple>
				  <option value="All Guest">All Guest</option>
                  <option value="Corporate">Corporate</option>
				  <option value="All Family">All Family</option>
				  <option value="All Bachelor">All Bachelor</option>
				  <option value="All Sr Citizen">All sr Citizen</option>
				  <option value="All Military">All Military</option>
				  <option value="All Military">All Retire Military</option>
				  <option value="All Tourist">All Tourist</option>
				  <option value="All Vip">All Vip</option>
				  <option value="All Internal Staff">All Internal Staff</option>
				  <option value="All Woman">All Woman</option>
				  
                </select>
				<span class="help-block">Applicable For</span> 
			  </div>
			</div>
			
            <div class="col-md-6">
               <div class="form-group form-md-line-input">
                <select class="form-control bs-select"   name="room_rent1" id="room_rent1" required >
                  <option value="1">Yes</option>
				  <option value="0">No</option>
                  
                </select>
                <label></label>
                <span class="help-block">Discount applicable for room rent only</span> 
              </div>
            </div>
			<input type="hidden" name="id1" id="id1">
			
			 <div class="col-md-6">
               <div class="form-group form-md-line-input">
                <select class="form-control bs-select"   name="food_charge1" id="food_charge1" required >
                  <option value="0"  selected="">Yes</option>
				  <option value="0"  selected="">No</option>
                  
                </select>
                <label></label>
                <span class="help-block">Applicable for food charges only</span> 
              </div>
            </div>
			
			<div class="col-md-12">
              <div class="form-group form-md-line-input ">
                <input autocomplete="off" type="text" name="description1" class="form-control" id="description1" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Name *">
                <label></label>
                <div class="highlight"></div>
                <span class="help-block">Description</span> </div>
            </div >
			
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<script>
   function check_corporate(value){

       if(value =="corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
       }else{
           document.getElementById("c_name").style.display="none";
           document.getElementById("c_des").style.display="none";
       }
   }
    function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
</script> 
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}
$(document).on('blur', '#ced', function () {
	$('#ced').addClass('focus');
});


function edit_discount(id){
		//alert(id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/edit_discount_rule",
                data:{id:id},
                success:function(data)
                {
                   var a=data.applicable;
				    var b=a.split(',');//array
					 for(var i=0;i<b.length;i++){
					//alert(b[i]);
					// $('#applicable1:selected').val(b[i]).change();
					  $("#applicable1").append(b[i]).change();
					}
					
                   $('#id1').val(data.id);
				   $('#rule_name1').val(data.name);
				   $('#description1').val(data.description);				   
				   $('#discount1').val(data.dp_discount);
				   $('#discount_start_percentage1').val(data.discount_start_percentage);
				   $('#discount_end_percentage1').val(data.discount_end_percentage);
				   $('#room_rent1').val(data.room_rent);
				   $('#food_charge1').val(data.food_charge);
				   
				  
				  
				  
				  
				  
				  
                }
            });
	}
</script>
<script>
			function discount_check(v){
				
				var start=$("#discount_Start_parcentage").val();
				var end=$("#discount_End_parcentage").val();
				v=parseFloat(v);
				start=parseFloat(start);
				end=parseFloat(end);
				
				if(v<start ){
					swal("Please Enter Greater value!", "", "error");
					$('#discount').val('');
				}else if( v>end){
					swal("Please Enter Lower value!", "", "error");
					$('#discount').val('');
				}
				
			}
			
			
			function discount_check_edit(v){
				
				var start=$("#discount_start_percentage1").val();
				var end=$("#discount_end_percentage1").val();
				v=parseFloat(v);
				start=parseFloat(start);
				end=parseFloat(end);
				if(v<start ){
					swal("Please Enter Greater value!", "", "error");
					$('#discount1').val('');
				}else if( v>end){
					swal("Please Enter Lower value!", "", "error");
					$('#discount1').val('');
				}
				
			}
			
			
			</script>