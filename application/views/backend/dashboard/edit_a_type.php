<!-- BEGIN PAGE CONTENT-->
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Edit Assets Type</span> </div>
  </div>
  <div class="portlet-body form">
        <?php

        $form = array(
            'class' 			=> '',
            'id'				=> 'form',
            'method'			=> 'post',								
        );
        
        

        echo form_open_multipart('dashboard/update_Atype',$form);

        ?>
          <div class="form-body"> 
          <!-- 17.11.2015-->
          <?php if($this->session->flashdata('err_msg')):?>
          <div class="form-group">
            <div class="col-md-12 control-label">
              <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
            </div>
          </div>
          <?php endif;?>
          <?php if($this->session->flashdata('succ_msg')):?>
          <div class="form-group">
            <div class="col-md-12 control-label">
              <div class="alert alert-success alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
            </div>
          </div>
          <?php endif;?>
              <div class="row">
              <?php if(isset($type)){
                 // print_r($type);
                  foreach($type as $types){
                      
                  }
              }?>
              	<div class="col-md-4">
                    <div class="form-group form-md-line-input" >
                      <input type="text" autocomplete="off" required="required" name="name" class="form-control" value="<?php echo $types->asset_type_name;?>" placeholder="Assets Type Name *">
                      <label></label>
                      <span class="help-block">Assets Type Name *</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-md-line-input" >
                      <input type="text" value="<?php echo $types->asset_type_id;?>" name="a_id" class="form-control" placeholder="Assets ID *">
                      <label></label>
                      <span class="help-block">Assets ID *</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                      <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="a_des" value="<?php echo $types->asset_type_description;?>" placeholder="Assets Description">
                      <label></label>
                      <span class="help-block">Assets Description</span>
                    </div> 
                </div>
                <div class="col-md-12">
                <div class="form-group form-md-line-input uploadss"> 
                	<label>Uploads Image</label> 
                 <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/>
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                    </div>
                    <div>
                        <span class="btn default btn-file">
                        <span class="fileinput-new">
                        Select image </span>
                        <span class="fileinput-exists">
                        Change </span>
                        <?php echo form_upload('asset_type');?>
                        </span>
                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                        Remove </a>
                    </div>
                  </div>   
                </div>
                </div>
              </div>
          </div>
          <div class="form-actions right">
            <button type="submit" class="btn blue" >Submit</button>
          </div>
        <?php form_close(); ?>
    </div>
  </div>
<script>
   function check_corporate(value){

       if(value =="corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
       }else{
           document.getElementById("c_name").style.display="none";
           document.getElementById("c_des").style.display="none";
       }
   }
    function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});
</script> 
