<!-- 17.11.2015-->

<script src="<?php echo base_url();?>assets/global/plugins/typeahead1/jquery.typeahead.js"></script>

<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">

	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>

	<strong>

		<?php echo $this->session->flashdata('err_msg');?>

	</strong>

</div>

<?php endif;?>

<?php if($this->session->flashdata('succ_msg')):?>

<div class="alert alert-success alert-dismissible text-center" role="alert">

	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>

	<strong>

		<?php echo $this->session->flashdata('succ_msg');?>

	</strong>

</div>

<?php endif;?>

<!-- 19.07.2016-->
<?php

/*echo "<pre>";
print_r($Ahb_line_data);
echo "</pre>";
echo "****";
echo "<pre>";
print_r($Ahb_data);
exit;
*/

?>
<div class="portlet box blue">

	<div class="portlet-title" style="background-color:#4F6785;">

		<div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Edit AdHoc Billing</span> </div>

	</div>

	<div class="portlet-body form">

		<?php



		$form = array(

			'class' => '',

			'id' => 'form',

			'method' => 'post',

		);

		echo form_open_multipart( 'dashboard/edit_adhoc_bill', $form );

		?>
<?php
if(isset($Ahb_data) && $Ahb_data!=''){
	
	foreach($Ahb_data as $ahb){
?>		
		<div class="form-body">
		

			<div class="row">
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input required autocomplete="off" type="text" class="form-control  date-picker" id="received_date" name="received_date" placeholder="Bill Date" value=<?php echo date('m/d/y',strtotime($ahb->bill_date));  ?>>
						<input type="hidden" id="ahb_id" name="ahb_id" value="<?php echo $ahb->ahb_id;  ?>">
						<label></label>
						<span class="help-block">Bill Date</span> </div>
				</div>
				
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control" id="invoice_no" name="invoice_no" placeholder="Additional Invoice No" value=<?php echo $ahb->invoice_no;  ?>>
						<label></label>
						<span class="help-block">Additional Invoice No</span> 
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" id="service_type" name="service_type" onchange="" required placeholder="Service Type">
							<option <?php if($ahb->invoice_no == 'Custom'){ echo selected;}  ?>value="Custom">Custom</option>
							<option value="SPA & Parlour" <?php if($ahb->invoice_no == 'SPA & Parlour'){ echo selected;}?>>SPA & Parlour</option>
							<option value="Indoor Games" <?php if($ahb->invoice_no == 'Indoor Games'){ echo selected;}?>>Indoor Games</option>
							<option value="Outdoor Games" <?php if($ahb->invoice_no == 'Outdoor Games'){ echo selected;}?>>Outdoor Games</option>
							<option value="Adventure Sports" <?php if($ahb->invoice_no == 'Adventure Sports'){ echo selected;}?>>Adventure Sports</option>
							<option value="Travel" <?php if($ahb->invoice_no == 'Travel'){ echo selected;}?>>Travel</option>
							<option value="Hookah" <?php if($ahb->invoice_no == 'Hookah'){ echo selected;}?>>Hookah</option>
							<option value="Food" <?php if($ahb->invoice_no == 'Food'){ echo selected;}?>>Food</option>
							<option value="Tour" <?php if($ahb->invoice_no == 'Tour'){ echo selected;}?>>Tour</option>
							<option value="Others" <?php if($ahb->invoice_no == 'Others'){ echo selected;}?>>Others</option>
						</select>
						<label></label>
						<span class="help-block">Service Type *</span> 
					</div>
				</div>
				
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" id="counter" name="counter" onchange="" placeholder="Counter">
							<option selected disabled value="">Select Counter</option>
						</select>
						<label></label>
						<span class="help-block">Select Counter </span> 
					</div>
				</div>
				
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" id="guest_type" name="guest_type" onchange="" placeholder="Guest Type">
							<option value="flying" <?php if($ahb->guest_type == 'flying'){ echo selected;}?>>Flying Customer</option>
							<option value="hotel" <?php if($ahb->guest_type == 'hotel'){ echo selected;}?>>Hotel Guest</option>
							<option value="internal" <?php if($ahb->guest_type == 'internal'){ echo selected;}?>>Internal</option>
						</select>
						<label></label>
						<span class="help-block">Select Guest Type </span> 
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<div class="typeahead__container">
							<div class="typeahead__field">
								<span class="typeahead__query">										
                                    <input required class="js-typeahead-user_v1 form-control" name="g_name[query]" id="cust_name"  type="search" placeholder="Enter or search Guest here..." onblur="get_guest_name(this.value)" autocomplete="off" value="<?php echo $ahb->guest_name; ?>">
                                    <label></label>
									<span class="help-block">Enter or search Guest here</span>
								</span>
							</div>
						</div>
						
						<input class="form-control" type="hidden" id="gn" name="gn" value="<?php echo $ahb->guest_name; ?>">

						<input type="hidden" name="Guest_name" id="Guest_name">

						<input type="hidden" name="Guest_name_new" id="Guest_name_new">
						
						<script>

							function get_guest_name(name){
									$("#gn").val(name);
							}

							function get_guest(id) {
								var g_id = id;
								jQuery.ajax({
									type: "POST",
									url: "<?php echo base_url(); ?>bookings/return_guest_get",
									datatype: 'json',
									data: {
										guest_id: g_id
									},
									success: function(data) {
										//alert(JSON.stringify(data));
										$("#myModalNorm1").modal("hide")
										$('#gn').val(data.g_name);
										$('#guestID').val(data.g_id);
										$('#gp').val(data.g_pincode);
										$('#g_phn').val(data.g_contact_no);
										$('#g_email').val(data.g_email);
										$('#gp').addClass('focus');
										$('#guest_search').val("");
									}
								});
								return false;
							}
							
						</script>
						
					</div>
				</div>
				<?php
				
				$guest_details = $this->dashboard_model->get_guest_detail_row($ahb->guest_id);
			//	print_r($guest_details);
				?>
				<div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input class="form-control" type="text" id="g_phn" name="g_phn" placeholder="Guest Contact No">
                  <input type="hidden" id="guestID" name="guestID" 
				  value=<?php if(isset($guest_details) && $guest_details->g_contact_no!=''){echo $guest_details->g_contact_no;} ?>>
                  <label></label>
                  <span class="help-block">Guest Contact No</span> 
                </div>
            </div>	
			
			<div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input class="form-control" type="text" id="g_email" name="g_email" placeholder="Email Id" 
				  value=<?php if(isset($guest_details) && $guest_details->g_email!=''){echo $guest_details->g_email;} ?>>
                  <label></label>
                  <span class="help-block">Guest Email Id</span> 
                </div>
            </div>
			
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input class="form-control" type="text" id="pincode" name="pincode"  onblur="fetch_all_address()"  placeholder="Guest Pincode" 
				  value=<?php if(isset($guest_details) && $guest_details->g_pincode!=''){echo $guest_details->g_pincode;} ?>>
                  <label></label>
                  <span class="help-block">Guest Pincode</span> 
                </div>
            </div>	
			
			
			<div class="col-md-3">       
			<div class="form-group form-md-line-input">     
			<!--<input class="form-control" type="text" id="g_country" name="g_country" placeholder="Country" required="required">    -->
			<select class="form-control bs-select" id="g_country" name="g_country" data-live-search="true" data-size="8" data-show-subtext="true">
			<option value="" disabled="disabled">Select Country Name </option>
			<?php $allCountry = $this->setting_model->getAllCountry();
			
					if(isset($allCountry) && $allCountry){
						$i = 0;
						$prevReg = '';
						$defC = 'INDIA';
						$selectC = '';
						$defC = strtoupper($defC);
						
							foreach($allCountry as $country){
							
								if($country->Region != $prevReg){
									echo '<optgroup label="'.$country->Region.'">';
								}
							
								if(trim($defC) == trim(strtoupper($country->Country))){
									$selectC = 'selected="selected"';
								} else {
									$selectC = '';
								}
							
								if(isset($country->Country)){
							?>
									<option value="<?php echo trim(strtoupper($country->Country)); ?>" <?php echo $selectC; ?>>
										<?php echo $country->Country; ?>
									</option>
							<?php
								}

								$prevReg = $country->Region;
							}
							}
							
							?>
			</select>
			<label></label> 
			<span class="help-block">Country *</span>   
			</div>        
			</div>	
			
			<div class="col-md-3" id="g_state_div1" >         
			<div class="form-group form-md-line-input" > 
			<!--<input class="form-control" type="text" id="g_state" name="g_state" placeholder="State" required="required">	-->
			<input  type="hidden" id="g_state_code" name="g_state_code" placeholder="State" style="display:none;" >   
			<select class="form-control bs-select" id="g_state" name="g_state" data-live-search="true" data-size="8" data-show-subtext="true">
							<option value=""  disabled="disabled">Select State Name </option>
							<?php
							
							$allState = $this->setting_model->getAllState();
				
						//print_r($allState);
						//exit;
					
					if(isset($allState) && $allState){
						$i = 0;
						$prevTyp = '';
						$defS = 'WEST BENGAL';
						$selectS = '';
						$defS = strtoupper($defS);
						
						foreach($allState as $state){
							
							if($state->Type != $prevTyp)
								echo '<optgroup label="'.$state->Type.'">';
							
							if(trim($defS) == trim(strtoupper($state->Name))){
								$selectS = 'selected="selected"';
							} else {
								$selectS = '';
							}
							
							if(isset($state->Name)){
								?>
							<option value="<?php echo trim(strtoupper($state->Name)); ?>" <?php echo $selectS; ?>>
								<?php echo $state->Name; ?>
							</option>
							<?php
							}

							$prevTyp = $state->Type;
							}
							}
							?>
						<!--	<option value=""> Other Country State </option>-->
							</select>
							
			<label></label>  
			<span class="help-block">State *</span>      
			</div>          
			</div>

				<div class="col-md-3">

					<div class="form-group form-md-line-input">

						<input autocomplete="off" type="text" class="form-control  date-picker" id="delivery_date" name="delivery_date" placeholder="Consumption Date" value=<?php echo date('m/d/y',strtotime($ahb->cons_date)); ?>>

						<label></label>

						<span class="help-block"></span> </div>

				</div>

				<div class="col-md-3">
					<div class="form-group form-md-line-input">
					
						<select class="form-control bs-select" id="status_type" name="status_type" onchange="" required placeholder="Status">
							<option value="closed" <?php if($ahb->guest_type == 'closed'){ echo selected;}?>>Closed</option>
							<option value="advance" <?php if($ahb->guest_type == 'advance'){ echo selected;}?>>Advance</option>
							<option value="live" <?php if($ahb->guest_type == 'live'){ echo selected;}?>>Live</option>
							<option value="cancelled"  <?php if($ahb->guest_type == 'cancelled'){ echo selected;}?>>cancelled</option>
						</select>

						<label></label>
						<span class="help-block">Status *</span> 
					</div>
				</div>
				
				<div class="col-md-12">

					<div class="form-group form-md-line-input">

						<input autocomplete="off" type="text" class="form-control" id="bill_note" name="bill_note" placeholder="Bill Note" value=<?php echo $ahb->bill_note; ?>>

						<label></label>

						<span class="help-block">Bill Note</span> </div>

				</div>
				
				
				</div>
				<div class="row">



				<div class="col-md-12">

					<h4 style="margin:45px 0 10px; color:#3EB9C6; font-size:20px"> <i class="fa fa-shirtsinbulk" aria-hidden="true"></i>&nbsp;Bill Line Items </h4>

				</div>
				
				<div id="ex_chrgs">
					
			    </div>
				
				<div class="tab-pane active" id="ex_cha_add">

				<div class="form-body form-horizontal form-row-sepe">

					<table class="table table-striped table-hover table-bordered" width="100%" id="bill_line">

					  <thead>

						<tr>
							<th width="25%"> Service/ Product Name </th>
							<th width="8%"> HSN/ SAC </th>
							<th width="6%"> Qty </th>
							<th width="6%"> Unit Rate </th>
							<th width="6%"> SGST % </th>
							<th width="6%"> CGST % </th>
							<th width="6%"> IGST % </th>
							<th width="6%"> UTGST % </th>
							<th width="6%"> Tax % </th>
							<th width="6%"> Disc </th>
							<th width="6%"> Adjst </th>
							<th width="6%"> Total </th>
							<th width="8%"> Note </th>
							<th width="5%"> Action </th>
						</tr>

					  </thead>
					  
					  <tbody>
						<?php
						 $ahb_line_total_value = 0;
						 $row_count =0;
if(isset($Ahb_line_data) && $Ahb_line_data!=''){
//	print_r($Ahb_line_data);
	$final_row_count = 0;
	foreach($Ahb_line_data as $ahb_line){
//	    echo $ahb_line->total;
	    $ahb_line_total_value =  $ahb_line_total_value + $ahb_line->total;
	    $row_count++;
	    $final_row_count = $final_row_count  + $row_count;
?>
						<tr id="row_id_<?php echo $row_count; ?>">
						
						  <td class="form-group">
							  <div class="typeahead__container">
								<div class="typeahead__field">
									<span class="typeahead__query">

										<input class="js-typeahead-user_v2" name="s_name[query]" id="s_name_<?php echo $row_count; ?>" type="search" placeholder="Search 
Service/ Product Name" autocomplete="off" value=<?php echo $ahb_line->name; ?>>

									</span>

								</div>

							  </div>

						  </td>
						  
						  <td class="form-group">
							
							<input style="display:none" id="hsn_sac_<?php echo $row_count; ?>" name="hsn_sac" type="text" class="form-control input-sm" placeholder="hsn/ sac">
							<select name="" class="form-control input-sm bs-select" id="hsn_sacSlct">
								<option value="" disabled selected>Select Code</option>
								<option value="996311" <?php if($ahb_line->hsn_sac == '996311'){ echo selected;}?>>996311</option>
								<option value="996411"  <?php if($ahb_line->hsn_sac == '996411'){ echo selected;}?>>996411</option>
								<option value="996414"  <?php if($ahb_line->hsn_sac == '996414'){ echo selected;}?>>996414</option>
								<option value="996337"  <?php if($ahb_line->hsn_sac == '996337'){ echo selected;}?>>996337</option>
								<option value="9963"  <?php if($ahb_line->hsn_sac == '9963'){ echo selected;}?>>9963</option>
								<option value=""  <?php if($ahb_line->hsn_sac == ''){ echo selected;}?>>Others</option>
								</option>
						    </select>
						  </td>

						  <td class="form-group">
						     
						      	<input id="ahl_id_<?php echo $row_count; ?>" name="ahl_id[]" onblur="excTax()" type="hidden"  class="form-control input-sm"  value=<?php echo $ahb_line->ahl_id; ?>>
							<input id="c_quantity_<?php echo $row_count; ?>" name="c_quantity[]" onblur="excTax()" type="number" value="1" min="0" class="form-control input-sm" placeholder="Qty" value=<?php echo $ahb_line->qty; ?>>
						  </td>

						  <td class="hidden-480 form-group">
							<input onblur="excTax()" id="c_unit_price_<?php echo $row_count; ?>"  name="c_unit_price[]" type="number"  class="form-control input-sm" placeholder="Unit Price" value=<?php echo $ahb_line->rate; ?>>
						  </td>
						  
						  <td class="hidden-480 form-group">
							<input id="c_sgst_<?php echo $row_count; ?>" name="c_sgst[]" type="number"  onblur="excTax(1)" class="form-control input-sm" placeholder="SGST %" value=<?php echo $ahb_line->sgst; ?>>
						  </td> 
						  <td class="hidden-480 form-group">
							<input id="c_cgst_<?php echo $row_count; ?>" name="c_cgst[]" type="number" onblur="excTax(2)" class="form-control input-sm" placeholder="CGST %" value=<?php echo $ahb_line->cgst; ?>>
						  </td>
						  <td class="hidden-480 form-group">
							<input id="c_igst_<?php echo $row_count; ?>" name="c_igst[]" type="number"  onblur="excTax(3)" class="form-control input-sm" placeholder="IGST %" value=<?php echo $ahb_line->igst; ?>>
						  </td>
						  <td class="hidden-480 form-group">
							<input id="c_utgst_<?php echo $row_count; ?>" name="c_utgst[]" type="number" onblur="excTax(4)" class="form-control input-sm" placeholder="UTGST %" value=<?php echo $ahb_line->utgst; ?>>
						  </td>

						  <td class="hidden-480 form-group"><input id="c_tax_<?php echo $row_count; ?>" name="c_tax[]" type="text"  class="form-control input-sm" placeholder="Tax %" readonly value=<?php echo $ahb_line->total_tax; ?>></td>
						  
						  <td class="hidden-480 form-group"><input id="c_disc_<?php echo $row_count; ?>" name="c_disc[]" type="number" onblur="excTax()" class="form-control input-sm" placeholder="Discount" value=<?php echo $ahb_line->disc; ?>></td>
						  
						  <td class="hidden-480 form-group"><input id="c_adj_<?php echo $row_count; ?>" name="c_adj[]" type="number"  onblur="excTax()" class="form-control input-sm" placeholder="Adjustment" value=<?php echo $ahb_line->adjustment; ?>></td>

						  <td class="hidden-480 form-group"><input id="c_total_<?php echo $row_count; ?>" name="c_total[]" type="text"  class="form-control input-sm" placeholder="Total" readonly value=<?php echo $ahb_line->total; ?>></td>
						  
						  <td class="hidden-480 form-group"><input id="c_note_<?php echo $row_count; ?>" name="c_note[]" type="text"  class="form-control input-sm" placeholder="Note" value=<?php echo $ahb_line->note; ?>></td>

						  <td><button class="btn red btn-sm" type="button" id="deletebillline_<?php echo $row_count; ?>" onclick="delete_adhoc_line(<?php echo $row_count; ?>,<?php echo $ahb_line->ahl_id ?>,<?php echo $ahb_line->total; ?>)"><i class="fa fa-trash" aria-hidden="true"></i></button>
						  <input type="hidden" id="adhoc_line_row_count" name="adhoc_line_row_count" value="<?php echo $final_row_count; ?>">
						  
						  </td>

						</tr>

					 
<?php
	}
	
}
?>
<!--  start of new add of bill line item -->
						<tr>
						
						  <td class="form-group">
							  <div class="typeahead__container">
								<div class="typeahead__field">
									<span class="typeahead__query">

										<input class="js-typeahead-user_v2" name="s_name[query]" id="s_name" type="search" placeholder="Search 
Service/ Product Name" autocomplete="off">

									</span>

								</div>

							  </div>

						  </td>
						  
						  <td class="form-group">
							
							<input style="display:none" id="hsn_sac" name="hsn_sac" type="text" class="form-control input-sm" placeholder="hsn/ sac">
							<select name="" class="form-control input-sm bs-select" id="hsn_sacSlct">
								<option value="" disabled selected>Select Code</option>
								<option value="996311">996311</option>
								<option value="996411">996411</option>
								<option value="996414">996414</option>
								<option value="996337">996337</option>
								<option value="9963">9963</option>
								<option value="">Others</option>
								</option>
						    </select>
						  </td>

						  <td class="form-group">
							<input id="c_quantity" name="c_quantity[]" onblur="excTax()" type="number" value="1" min="0" class="form-control input-sm" placeholder="Qty" value="">
						  </td>

						  <td class="hidden-480 form-group">
							<input onblur="excTax()" id="c_unit_price"  name="c_unit_price[]" type="number"  class="form-control input-sm" placeholder="Unit Price" value="">
						  </td>
						  
						  <td class="hidden-480 form-group">
							<input id="c_sgst" name="c_sgst[]" type="number"  onblur="excTax(1)" class="form-control input-sm" placeholder="SGST %" value="">
						  </td> 
						  <td class="hidden-480 form-group">
							<input id="c_cgst" name="c_cgst[]" type="number" onblur="excTax(2)" class="form-control input-sm" placeholder="CGST %" value="">
						  </td>
						  <td class="hidden-480 form-group">
							<input id="c_igst" name="c_igst[]" type="number"  onblur="excTax(3)" class="form-control input-sm" placeholder="IGST %" value="">
						  </td>
						  <td class="hidden-480 form-group">
							<input id="c_utgst" name="c_utgst[]" type="number" onblur="excTax(4)" class="form-control input-sm" placeholder="UTGST %" value="">
						  </td>

						  <td class="hidden-480 form-group"><input id="c_tax" name="c_tax[]" type="text"  class="form-control input-sm" placeholder="Tax %"  value=""></td>
						  
						  <td class="hidden-480 form-group"><input id="c_disc" name="c_disc[]" type="number" onblur="excTax()" class="form-control input-sm" placeholder="Discount" value=""></td>
						  
						  <td class="hidden-480 form-group"><input id="c_adj" name="c_adj[]" type="number"  onblur="excTax()" class="form-control input-sm" placeholder="Adjustment"value=""></td>

						  <td class="hidden-480 form-group"><input id="c_total" name="c_total[]" type="text"  class="form-control input-sm" placeholder="Total" readonly value=""></td>
						  
						  <td class="hidden-480 form-group"><input id="c_note" name="c_note[]" type="text"  class="form-control input-sm" placeholder="Note" value=<?php echo $ahb_line->note; ?>></td>

						  <td><button class="btn green btn-xs" type="button" id="addbillline"><i class="fa fa-plus" aria-hidden="true"></i></button></td>

						</tr>
<!--  end of new add of bill line item -->
 </tbody>
					</table>

				</div>
				</div>
				


				<div class="col-md-12" align="right" style="font-size: 120%;"> Total Bill Amount : <i class="fa fa-inr" style="font-size: 85%;"> </i> <span id="total_amount"><?php echo $ahb_line_total_value; ?></span>

					<input type="hidden" name="hidden_total" id="hidden_total" value="<?php echo $ahb_line_total_value; ?>">

				</div>

				<input type="hidden" name="total_amount_hidden" id="total_amount_hidden">

				<div style="display:none" class="col-md-3">

					<div class="form-group form-md-line-input">

						<input autocomplete="off" type="decimal" class="form-control" id="discount" name="discount" onkeyup="g_total(this.value)" placeholder="Discount" readonly>

						<label></label>
						<span class="help-block">Discount</span>
					</div>

				</div>
				
				<div style="display:none" class="col-md-3">

					<div class="form-group form-md-line-input">
						<input autocomplete="off" type="decimal" class="form-control" id="adjustment" name="adjustment" onkeyup="g_total(this.value)" placeholder="Adjustment" readonly>
						<label></label>
						<span class="help-block">Enter adjustment</span>
					</div>

				</div>

				<div id="div_tax_type" class="col-md-3 form-horizontal"></div>

				<div class="col-md-3">

					<div class="form-group form-md-line-input">

						<input autocomplete="off" type="text" class="form-control" id="grand_total" name="grand_total" placeholder="Grand Total" readonly value="<?php echo $ahb_line_total_value; ?>">
						
						<label></label>

						<span class="help-block">Grand total</span>

					</div>

				</div>

				<div class="col-md-12" id="pay_id" style="text-align:center;margin-top: 20px; display:none">

					<div class="btn-group" >

						<label for="autocellwidth" class="auto_cl btn grey-cascade" id="auto_cl_id" style="background-color:#39B9A1;">

              <input type="checkbox" id="autocellwidth" class="toggle" style="display:none;">

             <span id="disp">Pay Now</span></label>

					
					</div>

				</div>

				<div id="pay" style="display:none;">

					<div class="col-md-3">

						<div class="form-group form-md-line-input">

							<input type="text" class="form-control" id="pay_amount" name="card_number" value="" readonly/>

							<label></label>

							<span class="help-block">Total Amount Paybale *</span>

						</div>

					</div>

					<div class="col-md-3">

						<div class="form-group form-md-line-input">

				<?php 
					
					$tranData = $this->dashboard_model->all_transaction_adhoc_byID($ahb->ahb_id);
					$tranL = 0;
					
					if(isset($tranData)){
						
						foreach($tranData as $keyTr){
							
							if($keyTr->t_status == 'Done' || $keyTr->t_status == 'done')
								$tranL = $tranL + $keyTr->t_amount;
							
						}
					}
					
				//	echo $tranL; 
				?>

							<input type="number" class="form-control" id="due_amount" name="due_amount" value="<?php echo $ahb_line_total_value - $tranL ?>" readonly>

							<label></label>

							<span class="help-block">Due Amount *</span>

						</div>

					</div>

					<div class="col-md-3">

						<div class="form-group form-md-line-input">

							<input type="number" id="add_amount" class="form-control" name="pay_amount" placeholder="Enter Amount" onblur="check_amount(this.value);"/>

							<input type="hidden" id="booking_id" value="">

							<input type="hidden" id="booking_status_id" value="">

							<label></label>

							<span class="help-block">Amount *</span>

						</div>
						
						<input type="hidden" value="unpaid" id="payment_status" name="payment_status">

						<script type="text/javascript">

							function check_amount( v ) {
								
								v = $( "#add_amount" ).val();
								
								if(isNaN(v) || v == '') v = 0;

								var f = 0;

								var due_amount = $( "#due_amount" ).val();
								if(isNaN(due_amount) || due_amount == '') due_amount = 0;

								if (parseFloat( due_amount ) < parseFloat( v )) {

									//alert("The amount should be less than the due amount Rs: "+due_amount);

									swal( {

											title: "Amount Should Be Smaller",

											text: "The amount should be less than the due amount Rs."+due_amount+" , please check!",

											type: "info"

										},

										function () {

											$( "#due_amount_ad" ).val('');

										} );

									$( "#add_amount" ).val( "" );

									return false;

								} else {

									f = parseFloat( due_amount ) - parseFloat( v );
									f = f.toFixed( 2 )
									
									if($('#p_status_w').val() == 'Done')
										$( "#due_amount_ad" ).val(f);
									else
										$( "#due_amount_ad" ).val(due_amount);

									return true;
								}

							}

						</script>

					</div>

					<div class="col-md-3">

						<div class="form-group form-md-line-input">

							<select name="p_center" class="form-control bs-select" id="p_center">

								<?php $pc=$this->dashboard_model->all_pc();?>

								<?php

								$defProfit = $this->unit_class_model->profit_center_default();

								if ( isset( $defProfit ) && $defProfit ) {

									$defPro = $defProfit->profit_center_location;

								} else {

									$defPro = "Select";

								}

								?>

								<option value="<?php echo $defPro;  ?>" selected>

									<?php echo $defPro; ?>

								</option>

								<?php $pc=$this->dashboard_model->all_pc1();

						foreach($pc as $prfit_center){

						?>

								<option value="<?php echo $prfit_center->profit_center_location;?>">

									<?php echo  $prfit_center->profit_center_location;?>

								</option>

								<?php }?>

							</select>

							<label></label>

							<span class="help-block">Profit Center *</span>

						</div>

					</div>

					<div class="col-md-3">

						<div class="form-group form-md-line-input">

							<select class="form-control bs-select" placeholder=" Booking Type" id="pay_mode" name="pay_mode" onChange="paym(this.value);">

								<option value="" disabled selected>Select Payment Mode</option>

									<?php 

										$mop = $this->dashboard_model->get_payment_mode_list();

										if($mop != false){
											$selPM = '';
											foreach($mop as $mp){
												if($mp->p_mode_des == 'Cash')
													$selPM = 'selected';
												else
													$selPM = '';
										
									?>

								<option <?php echo $selPM; ?> value="<?php echo $mp->p_mode_name; ?>">

									<?php echo $mp->p_mode_des; ?>

								</option>

								<?php } } ?>

							</select>

							<label></label>

							<span class="help-block">Payment Mode *</span>

						</div>

					</div>

					<div id="cards" style="display:none;">

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<select name="card_type" class="form-control bs-select" placeholder="Card type" id="card_type">

									<option value="" disabled selected>Select Card Type</option>

									<option value="Cr">Credit Card</option>

									<option value="Dr">Debit Card</option>

									<option value="gift">Gift Card</option>

								</select>

								<label></label>

								<span class="help-block">Card type *</span>

							</div>

						</div>

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" name="card_bank_name" class="form-control" placeholder="Bank name" id="bankname">

								<label></label>

								<span class="help-block">Bank Name *</span>

							</div>

						</div>

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<label>Card NO<span class="required"> * </span> </label>

								<input type="text" name="card_no" class="form-control" placeholder="Card no" id="card_no">

								<label></label>

								<span class="help-block">Card NO *</span>

							</div>

						</div>

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="Name on Card" name="card_name" id="card_name">

								<label></label>

								<span class="help-block">Name on Card *</span>

							</div>

						</div>

						<div class="col-md-3">

							<div class="row">

								<div class="col-md-6">

									<div class="form-group form-md-line-input">

										<select name="card_expm" class="form-control bs-select" placeholder="Month" id="card_expm">

											<option value="" disabled selected>Select Month</option>

											<?php 

					$MonthArray = array(

					"1" => "January", "2" => "February", "3" => "March", "4" => "April",

					"5" => "May", "6" => "June", "7" => "July", "8" => "August",

					"9" => "September", "10" => "October", "11" => "November", "12" => "December",);



						for($i=1;$i<13;$i++)

						{

					?>

											<option value="<?php echo $MonthArray[$i]; ?>">

												<?php echo $MonthArray[$i]; ?>

											</option>

											<?php }  ?>

										</select>

										<label></label>

										<span class="help-block">Select Month *</span>

									</div>

								</div>

								<div class="col-md-6">

									<div class="form-group form-md-line-input">

										<select name="card_expy" class="form-control bs-select" placeholder="Year" id="card_expy">

											<option value="" disabled selected>Select year</option>

											<?php 

											for($i=2016;$i<2075;$i++) {

											?>

								<option value="<?php echo $i; ?>">

									<?php echo $i; ?>

								</option>

						<?php 
						}
						?>

										</select>

										<label></label>

										<span class="help-block">Exp year *</span>

									</div>

								</div>

							</div>

						</div>

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="CVV" name="card_cvv" id="card_cvv">

								<label></label>

								<span class="help-block">CVV *</span>

							</div>

						</div>

					</div>

					<div id="fundss" style="display:none;">

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="Bank name" name="ft_bank_name" id="bankname_f">

								<label></label>

								<span class="help-block">Bank Name *</span>

							</div>

						</div>

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="Acc no" name="ft_account_no" id="ac_no">

								<label></label>

								<span class="help-block">A/C No *</span>

							</div>

						</div>

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="IFSC code" name="ft_ifsc_code" id="ifsc">

								<label></label>

								<span class="help-block">IFSC Code *</span>

							</div>

						</div>

					</div>

					<div id="cheque" style="display:none;">

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="Bank name" name="chk_bank_name" id="bankname_c">

								<label></label>

								<span class="help-block">Bank Name *</span>

							</div>

						</div>

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="Cheque no" name="checkno" id="chq_no">

								<label></label>

								<span class="help-block">Cheque No *</span>

							</div>

						</div>

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="Drawer Name" name="chk_drawer_name" id="drw_name_c">

								<label></label>

								<span class="help-block">Drawer Name *</span>

							</div>

						</div>

					</div>

					<div id="draft" style="display:none;">

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="Bank name" name="draft_bank_name" id="bankname_d">

								<label></label>

								<span class="help-block">Bank Name *</span>

							</div>

						</div>

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="Draft no" name="draft_no" id="drf_no">

								<label></label>

								<span class="help-block">Draft No *</span>

							</div>

						</div>

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="Drawer Name" name="draft_drawer_name" id="drw_name_d">

								<label></label>

								<span class="help-block">Drawer Name *</span>

							</div>

						</div>

					</div>

					<div id="ewallet" style="display:none;">

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="Wallet Name" name="wallet_name" id="w_name">

								<label></label>

								<span class="help-block">Wallet Name *</span>

							</div>

						</div>

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="Transaction ID" name="wallet_tr_id" id="tran_id">

								<label></label>

								<span class="help-block">Transaction ID *</span>

							</div>

						</div>

						<div class="col-md-3">

							<div class="form-group form-md-line-input">

								<input type="text" class="form-control" placeholder="Recieving Acc" name="wallet_rec_acc" id="recv_acc">

								<label></label>

								<span class="help-block">Recieving Acc *</span>

							</div>

						</div>

					</div>

					<div class="col-md-3">

						<div class="form-group form-md-line-input">

							<select name="p_status" class="form-control bs-select" placeholder="Payment Status" id="p_status_w" onchange="check_amount()">

								<option value="Done" selected>Payment Recieved</option>

								<option value="Pending">Payment Processing</option>

								<option value="Cancel">Transaction Declined</option>

							</select>

							<label></label>

							<span class="help-block">Payment Status *</span>

						</div>

					</div>

				</div>

				<div id="pay1" class="col-md-3">

					<div class="form-group form-md-line-input">

						<input type="number" class="form-control" id="due_amount_ad" name="due_amount_ad" placeholder="Pending Payment" readonly>

						<label></label>

						<span class="help-block">Pending Payment</span> </div>

				</div>
				
				<div id="pay1" class="col-md-3">

					<div class="form-group form-md-line-input">

						<input autocomplete="off" type="text" class="form-control date-picker" id="payments_due_date" name="payments_due_date" placeholder="payments Due Date">

						<label></label>

						<span class="help-block">Payments Due Date</span> </div>

				</div>

			</div>

		</div>

		<div class="form-actions right">

			<!--<button type="submit" class="btn neongreen">Submit</button>-->
			<input class="btn neongreen" type="button" value="Generate Adhoc Bill" id="btnSubmit">

			<!-- 18.11.2015  -- onclick="return check_mobile();" -->

			<button type="reset" class="btn default">Reset</button>

		</div>

		<input type="hidden" name="hid">

	<?php 
	form_close(); 
	
	}
	}// end of for
	?>

		<!-- END CONTENT -->


	</div>
	</div>

</div>

<script>

	function disbtn(){
		$("#btnSubmit").attr('disabled',true);
	}

	let excTax = (typ) => {
		
		console.log('excTax('+typ+')');
		
		let sgst = parseFloat($('#c_sgst').val());
		let cgst = parseFloat($('#c_cgst').val());
		let igst = parseFloat($('#c_igst').val());
		let utgst = parseFloat($('#c_utgst').val());
		let unitP = parseFloat($('#c_unit_price').val());
		let qty = parseFloat($('#c_quantity').val());
		let totTax = parseFloat($('#c_tax').val());
		let disc = parseFloat($('#c_disc').val());
		let adjst = parseFloat($('#c_adj').val());
		
		if(isNaN(sgst))
			sgst = 0;
		if(isNaN(cgst))
			cgst = 0;
		if(isNaN(igst))
			igst = 0;
		if(isNaN(disc))
			disc = 0;
		if(isNaN(adjst))
			adjst = 0;

		if ((!isNaN(sgst) || !isNaN(cgst)) && (typ == 1 || typ == 2)){
			$('#c_igst').val(0);
			$('#c_utgst').val(0);
			totTax = sgst + cgst;
		} else if(!isNaN(igst) && typ == 3 && igst > 0){
			console.log('check '+igst > 0);
			$('#c_sgst').val(0);
			$('#c_cgst').val(0);
			$('#c_utgst').val(0);
			totTax = igst;
		} else if(!isNaN(utgst) && typ == 4 && utgst > 0){
			$('#c_sgst').val(0);
			$('#c_cgst').val(0);
			$('#c_igst').val(0);
			totTax = utgst;
		}
		$('#c_tax').val(totTax);
		
	    if((!isNaN($('#c_unit_price').val()) && $('#c_unit_price').val() != "") && (!isNaN($('#c_quantity').val()) && $('#c_quantity').val() != "")){
			let total = ((qty*unitP - disc) * (totTax/100 + 1)) - adjst;
			$('#c_total').val(total);
		} else {
			$('#c_total').val(0);
		}
		
	}; // End function excTax
	
	let excTax2 = (typ,x) => {

		console.log('excTax('+typ+','+x+')');

		let sgst = parseFloat($('#sgst'+x).val());
		let cgst = parseFloat($('#cgst'+x).val());
		let igst = parseFloat($('#igst'+x).val());
		let utgst = parseFloat($('#utgst'+x).val());
		let unitP = parseFloat($('#price'+x).val());
		let qty = parseFloat($('#qty'+x).val());
		let totTax = parseFloat($('#tax'+x).val());
		let totalP = parseFloat($('#total'+x).val());
		let disc = parseFloat($('#disc'+x).val());
		let adjst = parseFloat($('#adjst'+x).val());

		if(isNaN(sgst))
			sgst = 0;
		if(isNaN(cgst))
			cgst = 0;
		if(isNaN(igst))
			igst = 0;
		if(isNaN(disc))
			disc = 0;
		if(isNaN(adjst))
			adjst = 0;

		if ((!isNaN(sgst) || !isNaN(cgst)) && (typ == 1 || typ == 2)){
			$('#igst'+x).val(0);
			$('#utgst'+x).val(0);
			totTax = sgst + cgst;
		} else if(!isNaN(igst) && typ == 3 && igst > 0){
			//console.log('check '+igst > 0);
			$('#sgst'+x).val(0);
			$('#cgst'+x).val(0);
			$('#utgst'+x).val(0);
			totTax = igst;
		} else if(!isNaN(utgst) && typ == 4 && utgst > 0){
			$('#sgst'+x).val(0);
			$('#cgst'+x).val(0);
			$('#igst'+x).val(0);
			totTax = utgst;
		}
		$('#tax'+x).val(totTax);
		
	    if((!isNaN($('#price'+x).val()) && $('#price'+x).val() != "") && (!isNaN($('#qty'+x).val()) && $('#qty'+x).val() != "")){
			let total = ((qty*unitP - disc) * (totTax/100 + 1)) - adjst;
			$('#total'+x).val(total);
		} else {
			$('#total'+x).val(0);
		}
				console.log(total+' '+totalP);
				var totAm = parseFloat($( "#total_amount" ).text()) + parseFloat($('#total'+x).val()) - totalP;
				console.log(totAm);
				$("#total_amount" ).text(totAm.toFixed(2));
				$("#pay_amount").val(totAm.toFixed(2));
				$("#due_amount").val(totAm.toFixed(2));
				$("#grand_total").val(totAm.toFixed(2));

				var discT = parseFloat($("#discount").val());
				if(isNaN(discT))
					discT = parseFloat(disc);
				else
					discT = parseFloat(discT + disc);
				$("#discount").val(parseFloat(discT).toFixed(2));
					
				var adjT = parseFloat($("#adjustment").val());
				if(isNaN(adjT))
					adjT = parseFloat(adjst);
				else
					adjT = adjT.toFixed(2) + adjst.toFixed(2);
				$("#adjustment").val(adjT);
		
	}; // End function excTax
	
	
	$( "#addbillline" ).click( function () {
		
		
		 flag = $('#adhoc_line_row_count').val();
		 alert(flag);
		 	flag++;
			var x = 0;
			var sum = 0;
			var item_name = $( '#s_name' ).val();
			var hsn_sacSlct = $( '#hsn_sacSlct' ).val();
			var qty = $( '#c_quantity' ).val();
			var price = $( '#c_unit_price' ).val();
			var sgst = $( '#c_sgst' ).val();
			var cgst = $( '#c_cgst' ).val();
			var igst = $( '#c_igst' ).val();
			var utgst = $('#c_utgst').val();
			var tax = $('#c_tax').val();
			var total = parseFloat($('#c_total').val());
			var disc = $( '#c_disc' ).val();
			var adjst = $( '#c_adj' ).val();
			var cls = $( '#cls' ).val();
			var note = $( '#c_note' ).val();
			
			if ( tax == '' ) tax = 0;
			if ( disc == '' ) disc = 0;
			if ( adjst == '' ) adjst = 0;
			if ( sgst == '' ) sgst = 0;
			if ( cgst == '' ) cgst = 0;
			if ( igst == '' ) igst = 0;
			if ( utgst == '' ) utgst = 0;

			if ( product != '' && qty != '' && price != '' ) {
			    
			   

				 $('#bill_line tbody tr:last').before( '<tr id="row_' + flag + '" class="ss">' +
					'<td><input name="product[]" id="product' + flag + '"  type="text" value="' + item_name + '" class="form-control input-sm" ></td>' +
					'<td><input name="sac[]" id="sac' + flag + '" type="text" value="' + hsn_sacSlct + '" class="form-control input-sm" ></td>' +
					'<td><input name="qty[]" id="qty' + flag + '" type="text" value="' + qty + '" onchange="excTax2(0,' + flag + ')" class="form-control input-sm" ></td>' +
					'<td><input name="price[]" id="price' + flag + '" type="text" value="' + price + '" onchange="excTax2(0,' + flag + ')" class="form-control input-sm" ></td>' +
					'<td><input name="sgst[]" id="sgst' + flag + '" type="text" value="' + sgst + '" onchange="excTax2(1,' + flag + ')" class="form-control input-sm" ></td>' +
					'<td><input name="cgst[]" id="cgst' + flag + '" type="text" value="' + cgst + '" onchange="excTax2(2,' + flag + ')" class="form-control input-sm" ></td>' +
					'<td><input name="igst[]" id="igst' + flag + '" type="text" value="' + igst + '" onchange="excTax2(3,' + flag + ')" class="form-control input-sm" ></td>' +
					'<td><input name="utgst[]" id="utgst' + flag + '" type="text" value="' + utgst + '" onchange="excTax2(4,' + flag + ')" class="form-control input-sm" ></td>' +
					'<td><input name="tax[]" id="tax' + flag + '" type="text" value="' + tax + '" class="form-control input-sm" readonly ></td>' +
					'<td><input name="disc[]" id="disc' + flag + '" type="text" value="' + disc + '" onchange="excTax2(0,' + flag + ')" class="form-control input-sm" ></td>' +
					'<td><input name="adjst[]" id="adjst' + flag + '" type="text" value="' + adjst + '" onchange="excTax2(0,' + flag + ')" class="form-control input-sm" ></td>' +
					'<td><input name="total[]" id="total' + flag + '"  type="text" value="' + total + '" class="form-control input-sm" readonly ></td>' +
					'<td><input name="note[]" id="note' + flag + '" type="text" value="' + note + '" class="form-control input-sm" ></td>' +
					'<td><a href="javascript:void(0);" class="btn red btn-sm" onclick="removeRow(' + flag + ',' + total + ')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>' );

				x = x + 1;

				sum = sum + parseFloat( $( '#total1' + flag ).val() );

				$( '#sp' ).text( sum );

				

				var product = $( '#product' ).val( '' );

				$( '#s_name' ).val( '' );
				$( '#c_quantity' ).val( '' );
				$( '#c_unit_price' ).val( '' );
				$( '#hsn_sacSlct' ).val('');
				$( '#c_sgst' ).val( '' );
				$( '#c_igst' ).val( '' );
				$( '#c_cgst' ).val( '' );
				$( '#c_utgst' ).val( '' );
				$( '#c_tax' ).val( '' );
				$( '#c_total' ).val( '' );
				$( '#c_disc' ).val( '' );
				$( '#c_adj' ).val( '' );
				$( '#c_note' ).val( '' );
				$( "#payments_due" ).val( sum );
				$( "#payments_due1" ).val( sum );

				toastr.success( 'Item '+item_name+' added Successfully!', 'success!' );
				
				//console.log($( "#total_amount" ).text());
				console.log(disc);

				var totAm = parseFloat($( "#hidden_total" ).val()) + total;
				$("#total_amount" ).text(totAm.toFixed(2));
				$("#pay_amount").val(totAm.toFixed(2));
				
				var due_tot_add = $('#due_amount').val() - total;
				$("#due_amount").val(due_tot_add.toFixed(2));
				$("#grand_total").val(totAm.toFixed(2));
				$('#hidden_total').val(totAm.toFixed(2));
				var discT = parseFloat($("#discount").val());
				if(isNaN(discT))
					discT = parseFloat(disc);
				else
					discT = parseFloat(discT + disc);
				$("#discount").val(parseFloat(discT).toFixed(2));
					
				var adjT = parseFloat($("#adjustment").val());
				if(isNaN(adjT))
					adjT = parseFloat(adjst);
				else
					adjT = adjT.toFixed(2) + adjst.toFixed(2);
				$("#adjustment").val(adjT);

			} else {
				
				if(item_name == ""){
				    swal(
						'Incomplete Data',
						'Please enter product or service name!',
						'info'
					)
					return false;
				} else if(qty == "") {
					swal(
						'Incomplete Data',
						'Please enter quantity!',
						'info'
					)
					return false;
				} else if(price == ""){
					swal(
						'Incomplete Data',
						'Please enter unit price!',
						'info'
					)
					return false;
				}
				
				if(isNaN($("#c_total").val()) || $("#c_total").val() == ''){
					swal(
						'Please check total',
						'It seems there is some issue with the total, check if the cell is blank or NaN!',
						'info'
					)
					return false;
				}
			}
	});
	
	function removeRow( x, total ) {

			swal( {

				title: "Are you sure?",

				text: "Do you want to remove the row!",

				type: "warning",

				showCancelButton: true,

				confirmButtonColor: "#DD6B55",

				confirmButtonText: "Yes, delete it!",

				closeOnConfirm: true

			}, function () {
				
				
				var new_sum = parseFloat( total1 ) - parseFloat( total );

				var totAm = parseFloat($('#hidden_total').val()) - total;
				$('#total_amount').text(totAm.toFixed(2));
				$("#pay_amount").val(totAm.toFixed(2));
			
				
				 var due_tot_remove = $('#due_amount').val()-total;;
	                
			    $("#due_amount").val(due_tot_remove.toFixed(2));
				
				$("#grand_total").val(totAm.toFixed(2));
				$('#hidden_total').val(totAm.toFixed(2));
				
				var discT = parseFloat($("#discount").val());
				if(isNaN(discT))
					discT = 0;
				else
					discT = (discT.toFixed(2) - parseFloat($("#disc"+x).val()).toFixed(2));
				$("#discount").val(discT);
				
				var adjT = parseFloat($("#adjustment").val());
				if(isNaN(adjT))
					adjT = 0;
				else
					adjT = (adjT.toFixed(2) - parseFloat($("#adjst"+x).val()).toFixed(2));
				$("#adjustment").val(adjT);
					
				
				$( '#sp' ).text( new_sum );

				$( '#payments_due' ).val( new_sum );

				toastr.error( 'Deleted the line item.! -', 'success!' );
				$( '#row_' + x ).remove();

			});



			var total1 = parseFloat( $( '#sp' ).text() );

	}

	$("#btnSubmit").click(function() {

		if(parseFloat($("#total_amount").text()) != '' && $("#received_date").val() != '' && $("#cust_name").val() != '') {
			$('form#form').submit();
			disbtn();
		}
		else
			swal('Incomplete Data');
	});
	
	function paym( val ) {

		if ( val == 'cards' ) {

			document.getElementById( 'cards' ).style.display = 'block';

		} else {

			document.getElementById( 'cards' ).style.display = 'none';

		}

		if ( val == 'fund' ) {

			document.getElementById( 'fundss' ).style.display = 'block';

		} else {

			document.getElementById( 'fundss' ).style.display = 'none';

		}

		if ( val == 'cheque' ) {

			document.getElementById( 'cheque' ).style.display = 'block';

		} else {

			document.getElementById( 'cheque' ).style.display = 'none';

		}

		if ( val == 'draft' ) {

			document.getElementById( 'draft' ).style.display = 'block';

		} else {

			document.getElementById( 'draft' ).style.display = 'none';

		}

		if ( val == 'ewallet' ) {

			document.getElementById( 'ewallet' ).style.display = 'block';

		} else {

			document.getElementById( 'ewallet' ).style.display = 'none';

		}

		if ( val == 'cash' ) {

			document.getElementById( 'cardss' ).style.display = 'none';

			document.getElementById( 'fundss' ).style.display = 'none';

			document.getElementById( 'ewallet' ).style.display = 'none';

			document.getElementById( 'cheque' ).style.display = 'none';

			document.getElementById( 'draft' ).style.display = 'none';

		}

	}

	var quantity = 0;



	function cal_amount( a ) {

		//$('#pay_amount').val()

		if ( a == '' ) {

			a = 0;

		}

		var b = $( '#hidden_grand_total' ).val();

		a = parseFloat( a );

		b = parseFloat( b );

		a = b - a;

		$( '#payments_due' ).val( a );

	}



	function modesofpayments() {

		var y = document.getElementById( "mop" ).value;

		// alert(y);



		if ( y == "checks" ) {

			document.getElementById( "check" ).style.display = "block";

		} else {

			document.getElementById( "check" ).style.display = "none";

		}

		if ( y == "cards" ) {

			document.getElementById( "cards" ).style.display = "block";

		} else {

			document.getElementById( "cards" ).style.display = "none";

		}

		if ( y == "draft" ) {

			document.getElementById( "draft" ).style.display = "block";

		} else {

			document.getElementById( "draft" ).style.display = "none";

		}

		if ( y == "fund" ) {

			document.getElementById( "fundtransfer" ).style.display = "block";

		} else {

			document.getElementById( "fundtransfer" ).style.display = "none";

		}

	}

	$( document ).ready( function () {

		cellWidth = 100;

		cellWidthSpec = $( this ).is( ":checked" ) ? "Auto" : "Fixed";
		
		
	

	} );



	$( "#autocellwidth" ).click( function () {


		cellWidth = 100; // reset for "Fixed" mode

		cellWidthSpec = $( this ).is( ":checked" ) ? "Auto" : "Fixed";



		document.getElementById( 'auto_cl_id' ).style.backgroundColor = $( this ).is( ":checked" ) ? "#297CAC" : "#39B9A1";

		var a = $( this ).is( ":checked" ) ? "Pay Later" : "Pay Now";

		if ( a == 'Pay Later' ) {

			document.getElementById( 'pay' ).style.display = 'block';

			document.getElementById( 'pay1' ).style.display = 'block';

		} else {



			document.getElementById( 'pay' ).style.display = 'none';

			document.getElementById( 'pay1' ).style.display = 'block';

		}

		$( '#disp' ).text( a );

	} );



	function co() {

		cellWidth = 100; // reset for "Fixed" mode

		cellWidthSpec = $( this ).is( ":checked" ) ? "Auto" : "Fixed";

		document.getElementById( 'auto_cl_id' ).style.backgroundColor = $( this ).is( ":checked" ) ? "#297CAC" : "#39B9A1";

		var a = $( this ).is( ":checked" ) ? "Pay Later" : "Pay Now";

		if ( a == 'Pay Later' ) {

			document.getElementById( 'pay' ).style.display = 'block';

			document.getElementById( 'pay1' ).style.display = 'block';

		} else {



			document.getElementById( 'pay' ).style.display = 'none';

			document.getElementById( 'pay1' ).style.display = 'block';

		}

		$( '#disp' ).text( a );

	}

	function g_total( discount ) {

		if ( window.grandtotal < discount ) {

			swal( "Denied!", "Discount Should not more than Total Bill!", "error" );

			$( '#discount' ).val( '' );

		}

		var ss = $( '#tax1' ).val();

		var hidden_grand_total = $( '#hidden_total' ).val();

		///alert(ss);

		if ( ss == 'Yes' ) {

			if ( discount != '' ) {

				discount = parseFloat( discount );

				var gtt1 = parseFloat( $( '#grand_total' ).val() );



				if ( discount > window.total ) {

					swal( "Denied!", "Discount Should not more than Total Bill!", "error" );

				} else {

					//discount=gtt1 - discount;

					//alert(window.grandtotal);

					window.grandtotal = window.grandtotal1 - discount;

					$( '#grand_total' ).val( window.grandtotal ); //alert();

					$( '#pay_amount' ).val( window.grandtotal );

					$( '#due_amount' ).val( window.grandtotal );



				}



			} else {

				window.grandtotal = window.total;

				window.grandtotal = window.grandtotal1;

				$( '#grand_total' ).val( window.grandtotal );

				$( '#pay_amount' ).val( window.grandtotal );

			}



		} else {



			var discount = parseFloat( $( '#discount' ).val() );



			var grand_total = $( '#grand_total' ).val();

			if ( discount >= 0 ) {

				var total = parseFloat( hidden_grand_total ) - parseFloat( discount );

				//alert('total :- ' +total);

				$( '#grand_total' ).val( total );

				$( '#due_amount' ).val( total );

			} else {

				$( '#grand_total' ).val( hidden_grand_total );

				$( '#due_amount' ).val( hidden_grand_total );

			}

		}

	}


	var flag = 0,

		flag1 = 0;

	sum = 0;

	function is_married( value ) {

		if ( value == "Yes" ) {

			document.getElementById( "g_anniv" ).style.display = "block";

		} else {

			document.getElementById( "g_anniv" ).style.display = "none";

		}
	}

	$( document ).on( 'blur', '#form_control_11', function () {

		$( '#form_control_11' ).addClass( 'focus' );

	} );

	$( document ).on( 'blur', '#form_control_12', function () {

		$( '#form_control_12' ).addClass( 'focus' );

	} );



	//.............tax rule addition dynamically.....



	var ff = flag;

	var check = $( '#cloth_type' + ff ).val();

	var a = $( '#cloth_type' ).val();

	//if(check<=a){

	//var color=0;

	function check_date() {



		var a = $( '#contract_start_date' ).val();

		a = new Date( a );

		var b = $( '#contract_end_date' ).val();

		b = new Date( b );

		if ( a >= b && b != '' ) {

			alert( 'Date Should Getter Than Start Date ' );

			$( '#contract_end_date' ).val( '' );

		}



		var c = $( '#contract_end_date' + ( flag - 1 ) ).val();

		// alert(c);

		c = new Date( c );

		if ( c > a ) {



			alert( 'Please Enter Valid Date' );

			$( '#contract_start_date' ).val( '' );

			$( '#contract_end_date' ).val( '' );

		}







	}



	$( document ).ready( function () {

	//	$( '#total_amount' ).text( '0' );

		$( '#div_tax_type' ).hide();



		window.countRow = 0;

		window.total = 0;

		window.grandtotal = 0;

		window.grandtotal1 = 0;

		window.totalquantity = 0;

		$( '#pay_id' ).show();

		var dd = $( '#grand_total' ).val();

		$( '#pay_amount' ).val( dd );

		$( '#billing_pref' ).val( 'Bill Seperately' ).change();

	} );



	function fetch_all_address() {



		var pin_code = document.getElementById( 'pincode' ).value;



		jQuery.ajax( {

			type: "POST",

			url: "<?php echo base_url(); ?>bookings/fetch_address",

			dataType: 'json',

			data: {

				pincode: pin_code

			},

			success: function ( data ) {

				//alert(data);

				document.getElementById( "g_country" ).focus();

				$( '#g_country' ).val( data.country );

				document.getElementById( "g_state" ).focus();

				$( '#g_state' ).val( data.state );

			}

		} );









	}

	$( "#addGuest" ).click( function () {

		$( '#myModalNorm1' ).modal( 'show' );

		$( '#return_guest' ).html( "" );

		$( "#return_guest" ).css( "display", "none" );

	} );



	function return_guest_search() {

		var guest_name = $( '#guest_search' ).val();

		//alert(guest_name);

		jQuery.ajax( {

			type: "POST",

			url: "<?php echo base_url(); ?>bookings/return_guest_search",

			datatype: 'json',

			data: {

				guest: guest_name

			},

			success: function ( data ) {

				var resultHtml = '';

				resultHtml += '<table class="table table-bordered" cellpadding="0" cellspacing="0" ><thead><tr><th>Name</th><th>Address</th><th width="100">Mobile No</th></tr></thead><tbody>';

				$.each( data, function ( key, value ) {

					resultHtml += '<tr  class="crsr collapsed" data-toggle="collapse" data-target="#toggleDemo' + value.g_id + '">';

					resultHtml += '<td>' + value.g_name + '</td>';

					resultHtml += '<td>' + value.g_address + '</td>';

					resultHtml += '<td>' + value.g_contact_no + '</td>';

					resultHtml += '</tr><tr id="toggleDemo' + value.g_id + '"  class="mrgn">';

					resultHtml += '<td class="no-marin"  colspan="3"  cellpadding="0" cellspacing="0">';

					resultHtml += '<div class="side clearfix" style="width:100%;"><div class="col-xs-7 ex"><p><label style="font-size:13px;">' + value.g_name + '</label><br><label>';



					resultHtml += '<input type="hidden" id="g_id_hide"  value="' + value.g_id + '" ></input>';

					resultHtml += '<label style="font-size:13px;">Room No: ' + value.room_no + '</label><br><label style="font-size:13px;">Last Check In Date: ' + value.cust_from_date + '</label><br> <button class="act active btn blue btn-sm" name="g_id" id="g_id"" value="' + value.g_id + '" onclick="get_guest(' + value.g_id + ')" >Continue</button> </p></div>';

					if ( value.g_photo_thumb != '' ) {

						resultHtml += '<div class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +

							'<img src="<?php echo base_url() ?>upload/guest/' + value.g_photo_thumb + '" width="100px" height="92px"  > ' +

							'</div>';

					} else {

						resultHtml += '<div  class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +

							'<img src="<?php echo base_url() ?>upload/guest/no_images.png" width="100px" height="92px" > ' +

							'</div>';

					}

					//resultHtml+='<td><input type="radio" name="g_id" id="'+value.g_id+'" value="'+value.g_id+'" onclick="get_guest(this.value)" /></td>';

					resultHtml += '</div></td></tr>';

				} );

				resultHtml += '</tbody></table>';

				$( '#return_guest' ).html( resultHtml );

				//alert(data);

				// console.log(data);

				//$('#dtls').html(data);

			}

		} );

		$( "#return_guest" ).css( "display", "block" );

		return false;

	}

</script>

<script>

	function get_guest_by_id( id ) {

		//alert(id);





		jQuery.ajax( {

			type: "POST",

			url: "<?php echo base_url(); ?>bookings/return_guest_get",

			datatype: 'json',

			data: {

				guest_id: id

			},

			success: function ( data ) {

				if ( id ) {

					$( '#tab11' ).hide();

					$( '#tab12' ).show();

					$( '#id_guest2' ).val( id );

					//alert(data.booking_id);

					//alert(data.g_name);

					$( '#g_name_hid' ).val( data.g_name );

					$( '#Guest_name' ).val( data.g_name );

					$( '#guest_type1' ).val( data.g_type ).change();

					$( '#hidden_id' ).val( id );

				}





			}

		} );



	}





	function get_guest( id ) {

		//var g_id = $('#g_id_hide').val();

		var g_id = id;

		// alert(g_id);

		jQuery.ajax( {

			type: "POST",

			url: "<?php echo base_url(); ?>bookings/return_guest_get",

			datatype: 'json',

			data: {

				guest_id: g_id

			},

			success: function ( data ) {







				$( '#tab11' ).hide();

				$( '#tab12' ).show();

				$( '#id_guest2' ).val( id );



				$( '#Guest_name' ).val( data.g_name );

				$( '#guest_type1' ).val( data.g_type ).change();



				$( '#hidden_id' ).val( data.g_id );

				/* $('#cust_contact_no').val(data.g_contact_no);

					$('#cust_mail').val(data.g_email);

					*/

				$( '#myModalNorm1' ).modal( 'hide' );



			}

		} );

		return false;

	}





	function get_price( id ) {

		var cloth = $( '#cloth_type' ).val();

		//var service=$('#service').val();

		var fabric = $( '#fabric' ).val();



		jQuery.ajax( {

			type: "POST",

			url: "<?php echo base_url(); ?>unit_class_controller/get_price_by_clothID_fabricId",

			datatype: 'json',

			data: {

				cloth: cloth,

				fab: fabric,

				service: id

			},

			success: function ( data ) {

				//alert(data.dry);

				if ( service == '1' )

					$( '#price' ).val( data.dry );

				else if ( service == '2' )

					$( '#price' ).val( data.laundry );

				else

					$( '#price' ).val( data.iron );



				/*alert(data.id);

                  alert(data.laundry);

                  alert(data.dry);

                  alert(data.iron);*/

				//alert(data.id);



				//$('#tab11').hide();





			}

		} );







	}





	
	function editRow( a ) {

		//alert(a);

		$( '#row_' + a ).hide();

		//$('#line').show(); return "<html>" + $("html").html() + "</html>";

		$( '#row_' + a ).after( '<tr id="row_' + a + '">' +

			//'<td class="hidden-480"><input name="cloth_type[]" id="cloth_type'+a+'" type="text" value="'+a+'" class="form-control input-sm" ></td></tr></table>');

			//$("html").html() + "</html>");

			$( '#abc1' ).html() );



	}

</script>

<script>

	function calculation( id ) {



		var qty = $( '#qty' + id ).val();

		var price = $( '#price' + id ).val();

		var tax = $( '#tax1' ).val();

		var total = $( '#total' + id ).val();

		var note = $( '#note' + id ).val();

		//alert(qty+price+total+tax+note);

		var amount = parseFloat( qty ) * parseFloat( price ) + parseFloat( note );



		$( '#total' + id ).val( amount );

		$( '#tax' + id ).val( 'dis' ).change();

		$( '#note' + id ).val( '0' );







	}



	function total_amt() {

		var pr = parseFloat( $( '#price' ).val() );

		var qty = parseFloat( $( '#qty' ).val() );

		//alert(pr);

		//alert(qty);

		if ( qty > 0 && pr > 0 ) {

			pr = pr * qty;

			//alert('wrong');

			$( '#total' ).val( pr );

		} else {

			$( '#total' ).val( '' );

		}

	}
	
	
	function delete_adhoc_line(row,adh_id,total){
	    
	   // alert('row'+row);
	  //  alert('adh_id'+adh_id);
	    $.ajax({
	        
	        type:"POST",
	        url:"<?php echo base_url(); ?>unit_class_controller/delete_adhoc_line_master",
	        data:{adh_id:adh_id},
	        success:function(data){
	        //    alert(data);
	            if(data=="success"){
	                
	                $('table#bill_line tbody').find('tr#row_id_'+row).remove();
	                var h_tot = parseFloat($('#hidden_total').val())-parseFloat(total);
	                $('#hidden_total').val(h_tot.toFixed(2));
	                $('#total_amount').text(h_tot.toFixed(2));
	                $("#pay_amount").val(h_tot.toFixed(2));
	                
	                var due_tot = $('#due_amount').val()-total;;
	                
			    	$("#due_amount").val(due_tot.toFixed(2));
				    $("#grand_total").val(h_tot.toFixed(2));
	                
	            }
	            else{
	                
	                swal('Problem in delete this item','warning');
	            }
	            
	        },
	    })
	    
	}



	function get_laundry_rates() {

		var cloth = $( '#cloth_type' ).val();

		var fabric = $( '#fabric' ).val();

		var service = $( '#service' ).val();

		var qt = parseFloat( $( '#qty' ).val() );

		if ( cloth != '' && service != '' && service > 0 ) {

			jQuery.ajax( {

				type: "POST",

				url: "<?php echo base_url(); ?>rate_plan/get_laundry_rates",

				data: {

					cloth: cloth,

					fabric: fabric,

					service: service,

				},

				success: function ( data ) {

						//alert(data.price);



						$( '#price' ).val( data.price );

						if ( qt > 0 ) {

							qt = qt * data.price;

							$( '#total' ).val( qt );

						}







					} // end ajax success

			} );



		}

		//alert('amt');

		total_amt();

	}



	$.typeahead( {

		input: '.js-typeahead-user_v1',

		minLength: 1,

		order: "asc",

		dynamic: true,

		delay: 200,

		backdrop: {

			"background-color": "#fff"

		},



		source: {

			project: {

				display: "name",

				ajax: [ {

					type: "GET",

					url: "<?php echo base_url();?>dashboard/test_typehead",

					data: {

						q: "{{query}}"

					}

				}, "data.name" ],

				template: '<div class="clearfix">' +

					'<div class="project-img">' +

					'<img class="img-responsive" src="{{image}}">' +

					'</div>' +

					'<div class="project-information">' +

					'<span class="pro-name" style="font-size:15px;"> {{name}}</span><span><i class="fa fa-phone"></i> <strong>Contact no:</strong> {{ph_no}} </span><span><i class="fa fa-envelope"></i> <strong>Email:</strong> {{email}}</span></span>' +

					'</span>' +

					'</div>' +

					'</div>'

			}

		},

		callback: {

			onClick: function ( node, a, item, event ) {

			$("#gp").val(item.pincode);
			//$("#g_address_child").val(item.address);
			$("#g_phn").val(item.ph_no);
			$("#g_email").val(item.email);
			$("#gn").val(item.name);
			$('#guestID').val(item.id);
			var st = item.state;
			var state_g = st.toUpperCase();
			$('#g_state').val(state_g).change();
			var cnt = item.country;
			var country_g = cnt.toUpperCase();
			$('#g_country').val(country_g).change();
			//$('#g_state_code').val(item.state_code);

				$( "#Guest_name" ).val( item.name );

				$( "#Guest_name_new" ).val( item.name );

				//$('#id_guest2').val(item.id);



			},

			onSendRequest: function ( node, query ) {

				console.log( 'request is sent' )



			},

			onReceiveRequest: function ( node, query ) {

				//console.log('request is received')

				//alert("defesddsfdsfs");

				if ( query != '' ) {

					$( "#Guest_name" ).val( '' );



					/*$("#cust_address").val('');

			$("#g_address_child").val('');

			$("#cust_contact_no").val('');

			$("#cust_mail").val('');

			$('#id_guest2').val('');*/

				}

			},

			onCancel: function ( node, query ) {

				//console.log('request is received')

				alert( "defesddsfdsfs" );

				if ( query != '' ) {

					$( "#Guest_name" ).val( '' );
				$("#gp").val('');
		//	$("#g_address_child").val(item.address);
			$("#g_phn").val('');
			$("#g_email").val('');
			$("#gn").val('');
			$('#guestID').val('');
			var st = item.state;
			var state_g = st.toUpperCase();
			$('#g_state').val('');
			var cnt = item.country;
			var country_g = cnt.toUpperCase();
			$('#g_country').val('');
			//$('#g_state_code').val(item.state_code);
				}

			}

		},

		debug: false

	}); // End typehead

</script>