<!-- BEGIN PAGE CONTENT-->
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<div class="portlet box blue">
<div class="portlet-title">
  <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Edit Admin Roles</span> </div>
</div>
<div class="portlet-body form">
    <?php
	$form = array(
		'id'				=> 'form',
		'method'			=> 'post'
	);
			$upt=$this->dashboard_model->get_upt($_REQUEST['id']);
			$getVal = explode(',',$upt->user_permission_type);
			//echo '<pre>';
			//print_r(explode(',',$upt->user_permission_type));	
	if(isset($permission) && $permission){
		foreach($permission as $per){
			$admin_permission=$this->dashboard_model->get_permission_by_label($per->permission_label);
			//echo '<pre>';
			//print_r($admin_permission);
			if(isset($admin_permission) && $admin_permission){
				$i=0;
				foreach($admin_permission as $admin_perm){
					$permission_types[$per->permission_label][$i]=array(
						'value'=>$admin_perm->permission_value,
						'id'=>$admin_perm->permission_id
					);
					$i++;
				}
			}
		}
	}
	$key=array_keys($permission_types);
	//echo form_open_multipart('dashboard/edit_admin_role',$form);
	?>
	<form action="<?php echo base_url();?>dashboard/edit_admin_role?id=<?php echo $_REQUEST["id"];?>" id="form" method="post" enctype="multipart/form-data" accept-charset="utf-8">
  <div class="form-body">
    <div class="row">
    	<div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="admin_roll_name" type="text" class="form-control" id="form_control_1" required="required" onkeypress=" return onlyLtrs(event,this);" onblur="saveDB(this.value,'admin_roll_name')" placeholder="Admin Role Name *" value="<?php echo $upt->admin_roll_name;?>">                
            <label></label><span class="help-block">Admin Role Name *</span>
          </div>
      </div>
	  <input type="hidden" name="id" value="<?php echo $_REQUEST["id"];?>">
      <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="admin_type" class="form-control bs-select" onchange="saveDB(this.value,'user_type')">
              <option value="SUPA" <?php if($upt->user_type == 'SUPA'){ echo 'selected="selected"';}?>>Super Admin</option>
              <option value="AD" <?php if($upt->user_type == 'AD'){ echo 'selected="selected"';}?>>Admin</option>
              <option value="SUBA" <?php if($upt->user_type == 'SUBA'){ echo 'selected="selected"';}?>>Sub Admin</option>
            </select>
            <label></label><span class="help-block">Admin Type *</span>
          </div>
      </div>
      <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="max_discount" type="text" class="form-control" id="form_control_1" required="required" placeholder="Max Discount Percentage" value="<?php echo $upt->max_discount;?>" onblur="saveDB(this.value,'max_discount')">                
            <label></label><span class="help-block">Max Discount Percentage</span>
          </div>
      </div>
      <div class="col-md-12">
      	<h3 class="subhead">Modules Privilages</h3>
      </div>
      <div class="col-md-12">
          <div class="row">
              <?php 
			  //echo "<pre>";
			  //print_r($permission_types);
			  foreach($key as $keys):?>
                  <div class="col-md-6 form-horizontal">
                      <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label" for="form_control_1"><?php echo $keys;?></label>
                        <div class="col-md-8">
                          <div class="md-checkbox-inline" >
                            <?php $i=1; ?>
                            
                            <?php foreach($permission_types[$keys] as $per=>$value):?>
                            <div class="md-checkbox" >
                              <input onclick="singleClick(this,'<?php echo $value['id'];?>')" class="md-check" id="checkbox<?php echo $value['id'];?>" type="checkbox" name="permission[]" value="<?php echo $value['id'];?>" <?php if( in_array( $value['id'] ,$getVal ) ){echo 'checked="checked"';} ?> />
                              <label for="checkbox<?php echo $value['id'];?>"> <span></span> <span class="check"></span> <span class="box"></span> <?php echo $value['value'];?></label>
                            </div>
                            <?php $i++; ?>
                            <?php endforeach;?>
                          </div>
                        </div>
                      </div>
                  </div>
              <?php endforeach;?>
          </div>
      </div> 
      <div class="col-md-12">
      	<h3 class="subhead">Other Privilages</h3>
      </div> 
      <div class="col-md-12">
          <div class="row">             
              <div class="col-md-6 form-horizontal">
                  <div class="form-group form-md-line-input">
                    <label class="col-md-6 control-label" for="form_control_1">Can take bookings (Single & Group)</label>
                    <div class="col-md-6">
                    	<div class="md-radio-inline">
                            <div class="switch">
                                <?php if($upt->take_booking == '1') { ?>
                                     <input type="checkbox" onclick="handleClick(this,'take_booking')" id="chk1" class="md-check cmn-toggle cmn-toggle-round checkbox" name="take_booking" checked>
                                <?php } else { ?>
                                     <input type="checkbox" onclick="handleClick(this,'take_booking')" id="chk1" class="md-check cmn-toggle cmn-toggle-round checkbox" name="take_booking">
                                <?php }	?>
                                    <label for="chk1"></label>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-md-6 form-horizontal">
                  <div class="form-group form-md-line-input">
                    <label class="col-md-6 control-label" for="form_control_1">Can Edit & Delete Bookings</label>
                    <div class="col-md-6">
                        <div class="md-radio-inline">
                            <div class="switch">
                                <?php if($upt->edit_booking == '1') { ?>
                                     <input type="checkbox" onclick="handleClick(this,'edit_booking')" id="chk2" class="md-check cmn-toggle cmn-toggle-round checkbox" name="edit_booking" checked>
                                <?php } else { ?>
                                     <input type="checkbox" onclick="handleClick(this,'edit_booking')" id="chk2" class="md-check cmn-toggle cmn-toggle-round checkbox" name="edit_booking">
                                <?php }	?>
                                    <label for="chk2"></label>
                            </div>                           
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-md-6 form-horizontal">
                  <div class="form-group form-md-line-input">
                    <label class="col-md-6 control-label" for="form_control_1">Can interact with booking Calander</label>
                    <div class="col-md-6">
                        <div class="md-radio-inline">
                            <div class="switch">
                                <?php if($upt->access_booking_engine == '1') { ?>
                                     <input type="checkbox" onclick="handleClick(this,'access_booking_engine')" id="chk3" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_booking_engine" checked>
                                <?php } else { ?>
                                     <input type="checkbox" onclick="handleClick(this,'access_booking_engine')" id="chk3" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_booking_engine">
                                <?php }	?>
                                    <label for="chk3"></label>
                            </div>                       
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-md-6 form-horizontal">
                  <div class="form-group form-md-line-input">
                    <label class="col-md-6 control-label" for="form_control_1">Can access settings</label>
                    <div class="col-md-6">
                        <div class="md-radio-inline">
                            <div class="switch">
                                <?php if($upt->access_setting == '1') { ?>
                                     <input type="checkbox" onclick="handleClick(this,'access_setting')" id="chk4" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_setting" checked>
                                <?php } else { ?>
                                     <input type="checkbox" onclick="handleClick(this,'access_setting')" id="chk4" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_setting">
                                <?php }	?>
                                    <label for="chk4"></label>
                            </div>                            
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-md-6 form-horizontal">
                  <div class="form-group form-md-line-input">
                    <label class="col-md-6 control-label" for="form_control_1">Can add Admin</label>
                    <div class="col-md-6">
                        <div class="md-radio-inline">
                            <div class="switch">
                                <?php if($upt->add_admin == '1') { ?>
                                     <input type="checkbox" onclick="handleClick(this,'add_admin')" id="chk5" class="md-check cmn-toggle cmn-toggle-round checkbox" name="add_admin" checked>
                                <?php } else { ?>
                                     <input type="checkbox" onclick="handleClick(this,'add_admin')" id="chk5" class="md-check cmn-toggle cmn-toggle-round checkbox" name="add_admin">
                                <?php }	?>
                                    <label for="chk5"></label>
                            </div>                   
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-md-6 form-horizontal">
                  <div class="form-group form-md-line-input">
                    <label class="col-md-6 control-label" for="form_control_1">Can access reports</label>
                    <div class="col-md-6">
                        <div class="md-radio-inline">
                            <div class="switch">
                                <?php if($upt->access_report == '1') { ?>
                                     <input type="checkbox" onclick="handleClick(this,'access_report')" id="chk6" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_report" checked>
                                <?php } else { ?>
                                     <input type="checkbox" onclick="handleClick(this,'access_report')" id="chk6" class="md-check cmn-toggle cmn-toggle-round checkbox" name="access_report">
                                <?php }	?>
                                    <label for="chk6"></label>
                            </div>                        
                        </div>
                    </div>
                </div>
              </div>
          </div>
      </div>  
    </div>
  </div>
    <div class="form-actions right">
        <div class="row">
            <div class="col-md-12">
              <!--<button type="submit" class="btn blue">Update</button>-->
              <a href="<?php echo base_url();?>dashboard/all_admin_roles" class="btn red">Cancel</a>
            </div>
        </div>
    </div>
  <?php echo form_close(); ?> 
</div>
</div>
<script>
function saveDB(cb,col) {
	var id = '<?php echo $_REQUEST["id"];?>';
	$.ajax({
		url: "<?php echo base_url()?>dashboard/inlineEditRole",
		type: "POST",
		data:'column='+col+'&editval='+cb+'&id='+id,
		success: function(data){
		}        
    });
}
function handleClick(cb,col) {
    if(cb.checked){
		var editableObj = '1';
    } else {
		var editableObj = '0';
	}
	var id = '<?php echo $_REQUEST["id"];?>';
	//alert(editableObj);
	//alert(col);
	//alert(id);
	$.ajax({
		url: "<?php echo base_url()?>dashboard/inlineEditRole",
		type: "POST",
		data:'column='+col+'&editval='+editableObj+'&id='+id,
		success: function(data){
		}        
    });
}

function singleClick(cb,val) {
    if(cb.checked){
		var editableObj = '1';
		//alert('checked');
    } else {
		var editableObj = '0';
		//alert('Not checked');
	}
	var id = '<?php echo $_REQUEST["id"];?>';
	$.ajax({
		url: "<?php echo base_url()?>dashboard/inlineEditPermission",
		type: "POST",
		data:'column='+val+'&editval='+editableObj+'&id='+id,
		success: function(data){
		}        
    });
}
</script>