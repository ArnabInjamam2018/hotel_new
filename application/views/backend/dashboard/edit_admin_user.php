<!-- BEGIN PAGE CONTENT-->
<div class="portlet box blue">
<div class="portlet-title">
  <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Update Admin User</span> </div>
</div>
<div class="portlet-body form">
    <?php
		$form = array(
			'id'				=> 'form',
			'method'			=> 'post'
		);
		echo form_open_multipart('setting/edit_admin_user',$form);
		//echo "<pre>";
		//print_r($ad);
    ?>
	<input type="hidden" name="admin_id" value="<?php echo $ad->admin_id;?>">
	<input type="hidden" name="login_id" value="<?php echo $ad->login_id;?>">
    <div class="form-body">
      <div class="row">
    	  <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="fName" type="text" class="form-control" id="" required="required" placeholder="First Name *" value="<?php echo $ad->admin_first_name;?>">                
            <label></label><span class="help-block">First Name *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="lName" type="text" class="form-control" id="" required="required" placeholder="Last Name *" value="<?php echo $ad->admin_last_name;?>">                
            <label></label><span class="help-block">Last Name *</span>
          </div>
        </div>       
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="uName" type="text" class="form-control" id="uName" required="required" placeholder="Usename *" value="<?php echo $ad->user_name;?>" readonly>                
            <label></label><span class="help-block">Usename *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="contact" type="number" class="form-control" id="" required="required" placeholder="Contact No *" value="<?php echo $ad->admin_phone1;?>">                
            <label></label><span class="help-block">Contact No *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="alt_contact" type="number" class="form-control" id="" placeholder="Alt Contact No" value="<?php echo $ad->admin_phone2;?>">                
            <label></label><span class="help-block">Alt Contact No</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="email" type="email" class="form-control" id="" required="required" placeholder="Email *" value="<?php echo $ad->admin_email;?>">                
            <label></label><span class="help-block">Email *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="dept" type="text" class="form-control" id="" placeholder="Department" value="<?php echo $ad->dept;?>">                
            <label></label><span class="help-block">Department</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="dept_role" class="form-control bs-select">
            	<option value="" disabled="disabled" selected="selected">Role</option>
				<option value="Cashier" <?php if($ad->dept_role == "Cashier"){echo 'selected="selected"';}?>>Cashier</option>
                <option value="Accountant" <?php if($ad->dept_role == "Accountant"){echo 'selected="selected"';}?>>Accountant</option>
                <option value="Store Manager" <?php if($ad->dept_role == "Store Manager"){echo 'selected="selected"';}?>>Store Manager</option>
            </select>
            <label></label><span class="help-block">Role *</span>
          </div>
        </div>
         <input type="hidden" name="hotel_id" value="<?php echo $this->session->userdata('user_hotel'); ?>">
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="admin_type" class="form-control bs-select" required>
            	<option value="" disabled="disabled" selected="selected">Admin level</option>
				<!--<option value="1" <?php //if($ad->user_type_id == "1"){echo 'selected="selected"';}
				?>>Super Admin</option>-->
                <option value="2" <?php if($ad->user_type_id == "2"){echo 'selected="selected"';}?>>Admin</option>
                <option value="3" <?php if($ad->user_type_id == "3"){echo 'selected="selected"';}?>>Sub Admin</option>
            </select>
            <label></label><span class="help-block">Admin level *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="admin_role_id" class="form-control bs-select" required>
            	<option value="" disabled="disabled" selected="selected">Admin Role</option>
              <?php 
              $ar=$this->setting_model->allAdminRole();
              foreach($ar as $admin_role){
              ?>          
              <option value="<?php echo $admin_role->user_permission_id;?>" <?php if($admin_role->user_permission_id == $ad->admin_role_id){echo 'selected="selected"';}?>><?php echo  $admin_role->admin_roll_name;?></option>
              <?php } ?>
            </select>
            <label></label><span class="help-block">Admin Role *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="profit_center[]" class="form-control bs-select" required multiple>
              <?php 
			  $apc = explode(',',$ad->profit_center);
			  //print_r($apc);
              $pc=$this->setting_model->allProfitCenter();
              foreach($pc as $prfit_center){
              ?>          
              <option value="<?php echo $prfit_center->profit_center_location;?>" <?php if(in_array($prfit_center->profit_center_location,$apc)){echo 'selected="selected"';}?>><?php echo  $prfit_center->profit_center_location;?></option>
              <?php } ?>
            </select>
            <label></label><span class="help-block">Profit Centre *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="employee_id" type="text" class="form-control" id="" placeholder="Employee id" value="<?php echo $ad->employee_id;?>">                
            <label></label><span class="help-block">Employee id</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="class" class="form-control bs-select" required>
            	<option value="" disabled="disabled" selected="selected">Class</option>
				<option value="Frontdesk" <?php if($ad->class == "Frontdesk"){echo 'selected="selected"';}?>>Frontdesk</option>
                <option value="Cashier" <?php if($ad->class == "Cashier"){echo 'selected="selected"';}?>>Cashier</option>
                <option value="Supervisor" <?php if($ad->class == "Supervisor"){echo 'selected="selected"';}?>>Supervisor</option>
                <option value="Executive" <?php if($ad->class == "Executive"){echo 'selected="selected"';}?>>Executive</option>
                <option value="Accountant" <?php if($ad->class == "Accountant"){echo 'selected="selected"';}?>>Accountant</option>
                <option value="Facility Management" <?php if($ad->class == "Facility Management"){echo 'selected="selected"';}?>>Facility Management</option>
                <option value="Stakeholder" <?php if($ad->class == "Stakeholder"){echo 'selected="selected"';}?>>Stakeholder</option>
            </select>
            <label></label><span class="help-block">Class *</span>
          </div>
        </div>
		        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="admin_status" class="form-control bs-select" required>
            	
				<option value="A" <?php if($ad->user_status == "A"){echo 'selected="selected"';}?>>Active</option>
                <option value="I" <?php if($ad->user_status == "I"){echo 'selected="selected"';}?>>Inactive</option>
            </select>
            <label></label><span class="help-block">Admin status *</span>
          </div>
        </div>
        <!--<div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="password" type="Password" class="form-control" id="" required="required" placeholder="Password *">                
            <label></label><span class="help-block">Password *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="cpassword" type="Password" class="form-control" id="" required="required" placeholder="Confirm Password *">                
            <label></label><span class="help-block">Confirm Password *</span>
          </div>
        </div>-->
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
          	<textarea name="about_me" class="form-control" cols="" rows="" placeholder="About Me"><?php echo $ad->about_me;?></textarea>
            <label></label><span class="help-block">About Me</span>
          </div>
        </div>
        <!--<div class="col-md-4">
              <div class="form-group form-md-line-input uploadss">
              <label>Profile Image</label>
              <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/dashboard/assets/global/img/no-img.png" alt=""/> </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php //echo form_upload('image_photo');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
              </div>
              </div>
        </div>-->
    </div>
  </div>
    <div class="form-actions right">
        <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn submit">Submit</button>
              <a href="<?php echo base_url();?>setting/all_admin_user" class="btn red">Cancel</a>
            </div>
        </div>
    </div>
  
  <?php echo form_close(); ?> 
</div>
</div>