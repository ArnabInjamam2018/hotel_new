<!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<!-- 17.11.2015-->
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green"> <i class="icon-pin font-green"></i> <span class="caption-subject bold uppercase"> Edit Area Code</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
if(isset($datas) && $datas){
							$id=$datas->id;
                            $form = array(
                                'class' 			=> '',
                                'id'				=> 'form',
                                'method'			=> 'post',								
                            );
							
						
                            echo form_open_multipart('unit_class_controller/edit_area_code/'.$id,$form);

                            ?>
    <div class="form-body">
      <div class="row">
      	<div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" name="pincode" value="<?php echo $datas->pincode; ?>" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Enter Pin code *">
          <label></label>
          <span class="help-block">Enter Pin code *</span> </div>
        </div>
        <div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" name="country" value="<?php echo $datas->area_0; ?>" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Country *">
          <label></label>
          <span class="help-block">Country *</span> </div>
        </div>
        <div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" name="state" value="<?php echo $datas->area_1; ?>" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="State *">
          <label></label>
          <span class="help-block">Enter State *</span> </div>
        </div>
        <div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text"  class="form-control" id="form_control_1" name="city" value="<?php echo $datas->area_3; ?>" required="required" placeholder="City *">
          <label></label>
          <span class="help-block">City *</span> </div>
        </div>
        <input type="hidden" name="hid" value="<?php echo $datas->id; ?>">
        <div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" name="locality1" value="<?php echo $datas->area_4; ?>" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Locality Name *">
          <label></label>
          <span class="help-block">Locality Name *</span> </div>
        </div>
        <div class="col-md-3">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" name="locality2" value="<?php echo $datas->area_5; ?>" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Locality 2 *">
          <label></label>
          <span class="help-block">Locality Name 2 *</span> </div>
        </div>
      </div>
      <?php }?>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn blue" >Submit</button>
    </div>
    <?php form_close(); ?>
    <!-- END CONTENT --> 
    
  </div>
</div>
<script language="JavaScript">

   function check(value){/*
		//alert(value);
		
			if(value==1){	
          $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/check_nature_visit_default",
                data:{a:value},
                 success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                 

				swal({   title: "Are you sure?",
				text: data+"!",  
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Fix as Default!",   cancelButtonText: "No, cancel plz!", 
				closeOnConfirm: true,  
				closeOnCancel: true },
				function(isConfirm){   
				if (isConfirm) {     
				$("#default").val('1');
				} else {   
				$("#default").val('0');
				} });



				 
                }
            });
			}
		  
		  
		  //});
		
	}*/
	

		
 $(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});   
</script> 
