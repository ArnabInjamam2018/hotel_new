<!-- 17.11.2015-->
<script src="<?php echo base_url();?>assets/global/plugins/typeahead1/jquery.typeahead.js"></script>
<style>
.red {
	background-color: red;
}
</style>
<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<!-- 17.11.2015-->
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Bill Payments</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
    $form = array(
        'class' 			=> '',
        'id'				=> 'form',
        'method'			=> 'post',
            
    );
    echo form_open_multipart('dashboard/edit_bill_payments',$form);
    
    
    ?>
    <div class="form-body">
      <?php if(isset($bill_payments)){
		//echo "<pre>";
		// print_r($bill_payments); exit;
			foreach($bill_payments as $payments){
		}
		}?>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="date" name="date" value="<?php echo date("Y/m/d");?>" readonly placeholder="Date *">
            <label></label>
            <span class="help-block">Date *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select"  id="top" name="top"  required="required" onchange="typesofpayments();">
              <option value="" selected="selected" disabled="disabled">Types Of Payment</option>
              <option value="Bill payments" <?php if(isset($payments->type_of_payment)){if($payments->type_of_payment=="Bill payments"){ echo "selected";}}?>>Bill Payments</option>
              <option value="Salary/Wage Payments" <?php if(isset($payments->type_of_payment)){if($payments->type_of_payment=="Salary/Wage Payments"){ echo "selected";}}?>>Salary/Wage Payments</option>
              <option value="Miscellaneous Payments" <?php if(isset($payments->type_of_payment)){if($payments->type_of_payment=="Miscellaneous Payments"){ echo "selected";}}?>>Miscellaneous Payments</option>
              <option value="Tax/Compliance Payment" <?php if(isset($payments->type_of_payment)){if($payments->type_of_payment=="Tax/Compliance Payment"){ echo "selected";}}?>>Tax/Compliance Payment</option>
            </select>
            <label></label>
            <span class="help-block">Select Payment Type *</span> </div>
        </div>
        <input type="hidden" id="hidVal" value="<?php echo $payments->type_of_payment;?>">
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" readonly id="hotel_name" name="hotel_name" placeholder="Hotel Name" value="<?php echo $hotel_name->hotel_name;?>">
            <label></label>
            <span class="help-block">Hotel Name</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" id="profit_center" name="profit_center" required="required" >
              <option value="" selected="selected" disabled="disabled">Profit Center *</option>
              <?php $pc=$this->dashboard_model->all_pc();?>
              <?php
                      $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
                      ?>
              <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
              <?php $pc=$this->dashboard_model->all_pc1();
                        foreach($pc as $prfit_center){
                        ?>
              <option value="<?php echo $prfit_center->profit_center_location;?>" <?php if($payments->profit_center==$prfit_center->profit_center_location) echo "selected"; ?>><?php echo  $prfit_center->profit_center_location;?></option>
              <?php }?>
            </select>
            <label></label>
            <span class="help-block">Profit Center *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control date-picker" id="bill_date" onchange="check_date()" name="bill_date" value="<?php echo $payments->bill_date?>"  placeholder="Bill Date *">
            <label></label>
            <span class="help-block">Bill Date*</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select"  id="paymentt" name="payment_term" required="required" onchange="paymentterm(this.value);">
              <option value="" selected="selected" disabled="disabled">Select Payment Term</option>
              <option value="immediate">Immediate</option>
              <option value="pia">PIA (Paid in advance)</option>
              <option value="net7">Net 7 (7 day credit)</option>
              <option value="net14">Net 14 (14 day credit)</option>
              <option value="net30">Net 30 (30 day credit)</option>
              <option value="net60">Net 60 (60 day credit)</option>
              <option value="net90">Net 90 (90 day credit)</option>
              <option value="cod">COD</option>
              <option value="billofex">Bill of exchange</option>
              <option value="contra">Contra</option>
              <option value="other">Specify Date</option>
            </select>
            <label></label>
            <span class="help-block">Payment Term</span> </div>
        </div>
        <div id="bill_other_dt" class="col-md-3" style="display:none;">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control date-picker" id="specific_date" name="specific_date"  placeholder="Specific Payment Date *">
            <label></label>
            <span class="help-block">Specific Payment Date *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control"  name="ref_no" placeholder="Referance No" value="<?php if(isset($payments->ref_no) && $payments->ref_no) echo $payments->ref_no;?>">
            <label></label>
            <span class="help-block">Referance No</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control"  name="bill_no" placeholder="Bill No" value="<?php if(isset($payments->bill_no) && $payments->bill_no) echo $payments->bill_no;?>">
            <label></label>
            <span class="help-block">Bill No</span> </div>
        </div>
        <?php   
		      if(isset($payments->vendor_id) && $payments->vendor_id){
			  $v_id=$payments->vendor_id;
				$vendor_name=$this->dashboard_model->data_vendor_details($v_id);
			  }
		?>
        <div id="billpayments" style='display:none;'>
          &nbsp;
          &nbsp;
          <h3 class="form-heading">&nbsp;&nbsp;&nbsp;Bill Details</h3>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <div class="typeahead__container">
                <div class="typeahead__field"> <span class="typeahead__query">
                  <input class="js-typeahead-user_v1 form-control" name="vendor[query]" id="vendor" value="<?php if(isset($vendor_name) && $vendor_name) echo $vendor_name->hotel_vendor_name;?>" type="search" placeholder="Search" autocomplete="off">
                  </span> </div>
              </div>
            </div>
          </div>
          <div class="col-md-3" >
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control date-picker" id="bill_due_date" onchange="check_date()" name="bill_due_date" value="<?php if(isset($payments->bill_due_date) && $payments->bill_due_date) echo $payments->bill_due_date?>"  placeholder="Due Date *">
              <label></label>
              <span class="help-block">Due Date*</span> </div>
          </div>
          <div class="col-md-3" >
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="address" name="address" value="<?php if(isset($vendor_name) && $vendor_name) echo $vendor_name->hotel_vendor_address; ?>"  placeholder="Address*">
              <label></label>
              <span class="help-block">Address*</span> </div>
          </div>
          <div class="col-md-3" >
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control"  id="ph_no"  name="ph_no" value="<?php if(isset($vendor_name) && $vendor_name) echo $vendor_name->hotel_vendor_contact_no; ?>" placeholder="Phone No *">
              <label></label>
              <span class="help-block">Phone No*</span> </div>
          </div>
          <div class="col-md-3" >
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control"  name="email"  placeholder="Email *">
              <label></label>
              <span class="help-block">Email Id*</span> </div>
          </div>
          <div class="col-md-3" >
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select"  id="payforbill" name="billpayfor" onchange="paymentforbill(this.value);">
                <option value=""  disabled="disabled">Payment For</option>
                <option value="month" <?php if(isset($payments->bill_month)){if($payments->bill_month!=''){ echo "selected";}}?>>Select Month</option>
                <option value="other" <?php if(isset($payments->bill_other)){if($payments->bill_other!=''){ echo "selected";}}?>>Others</option>
              </select>
              <label></label>
              <span class="help-block">Payment For *</span> </div>
          </div>
          <div id="bill_month" class="col-md-4" style="display:none;">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select" name="bill_month">
                <option value="<?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php echo date('F-Y');?>" selected="selected"><?php echo date('F-Y');?></option>
                <option value="<?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>
                </option>
              </select>
              <label></label>
              <span class="help-block">Select Month *</span> </div>
          </div>
          <div id="bill_other" class="col-md-4" style="display:none;">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="billother" placeholder="Other" value="<?php if(isset($payments->bill_other) && $payments->bill_other) echo $payments->bill_other;?>">
              <label></label>
              <span class="help-block">Other</span> </div>
          </div>
        </div>
        <div id="salarypayments" style='display:none;'>
        	&nbsp;
            &nbsp;
          <h3 class="form-heading">&nbsp;&nbsp;&nbsp;Salary Details</h3>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="person_name" placeholder="Person Namey" value="<?php echo $payments->person_name?>" />
              <label></label>
              <span class="help-block">Person Name</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="designation" placeholder="Designation/Role *" value="<?php echo $payments->designation?>">
              <label></label>
              <span class="help-block">Designation/Role *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select"  id="payforsall" name="payforsall" onchange="paymentforsal(this.value);">
                <option value="" selected="selected" disabled="disabled">Payment For</option>
                <option value="month" <?php if(isset($payments->sal_month)){if($payments->sal_month!=''){ echo "selected";}}?>>Select Month</option>
                <option value="other" <?php if(isset($payments->sal_other)){if($payments->sal_other!=''){ echo "selected";}}?>>Others</option>
              </select>
              <label></label>
              <span class="help-block">Payment For *</span> </div>
          </div>
          <div id="sal_month" class="col-md-3" style="display:none">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select" name="sal_month">
                <option value="<?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php echo date('F-Y');?>" selected><?php echo date('F-Y');?></option>
                <option value="<?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>
                </option>
              </select>
              <label></label>
              <span class="help-block">Select Month *</span> </div>
          </div>
          <div class="col-md-3" id="sal_other" style="display:none;">
          	<div class="form-group form-md-line-input">
            <input type="text" class="form-control" id="form_control_1" name="sal_other" placeholder="Other" value="<?php echo $payments->sal_other;?>">
            <label></label>
            <span class="help-block">Other</span> </div>
        </div>
        </div>
        <!--end div for salary payments--> 
        <!--Start of misc payments div-->
        <div id="miscpayments" style='display:none;'> &nbsp;
          <h3 class="form-heading">&nbsp;&nbsp;&nbsp;Misc Payment Details</h3>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="payment_to" placeholder="Payment to *" value="<?php echo $payments->payment_to;?>">
              <label></label>
              <span class="help-block">Payment to *</span> </div>
          </div>
          <!--<div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="note_misc" >
                <label></label>
                <span class="help-block">Note *</span>
              </div>
          </div>-->
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select"  id="miscpayfor"  name="miscpayfor" onchange="paymentfor();">
                <option value="" selected disabled="disabled">Payment For</option>
                <option value="month" <?php if(isset($payments->misc_month)){if($payments->misc_month!=''){ echo "selected";}}?>>Select Month</option>
                <option value="other" <?php if(isset($payments->misc_other)){if($payments->misc_other!=''){ echo "selected";}}?>>Others</option>
              </select>
              <label></label>
              <span class="help-block">Payment For *</span> </div>
          </div>
          <div id="misc_month" class="col-md-3" style="display:none;">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select" name="misc_month">
                <!--  <option value="<?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>">
                  <?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>
                  </option> -->
                <option value="<?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php echo date('F-Y');?>" selected><?php echo date('F-Y');?></option>
                <option value="<?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>
                </option>
              </select>
              <label></label>
              <span class="help-block">Select Month *</span> </div>
          </div>
          <div class="col-md-3" id="misc_other" style="display:none">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="miscother" placeholder="Other" value="<?php echo $payments->misc_other?>">
              <label></label>
              <span class="help-block">Other</span> </div>
          </div>
        </div>
        <!--End of Misc Payment div--> 
        <!--Start of Tax/complience div-->
        <div id="taxpayments" style='display:none;'> &nbsp;
          <h3 class="form-heading">&nbsp;&nbsp;&nbsp;Tax/Compliance Details</h3>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select"  name="tax_type" maxlength="3" onkeypress=" return onlyNos(event, this);">
                <option value="" selected="selected" disabled="disabled">Payment To *</option>
                <option value="Tax" <?php if($payments->tax_type=="Tax") echo "selected";?>>Tax</option>
                <option value="Complience"  <?php if($payments->tax_type=="Complience") echo "selected";?>>Complience</option>
              </select>
              <label></label>
              <span class="help-block">Payment For *</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="payment_to" placeholder="Payment to *" value="<?php echo $payments->payment_to ?>">
              <label></label>
              <span class="help-block">Payment to</span> </div>
          </div>
          <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select"  id="payfortax"  onchange="paymentfortax();" name="payfortax">
                <option value=""  selected disabled="disabled">Payment For</option>
                <option value="month" <?php if(isset($payments->tax_month)){if($payments->tax_month!=''){ echo "selected";}}?> >Select Month</option>
                <option value="other" <?php if(isset($payments->tax_other)){if($payments->tax_other!=''){ echo "selected";}}?>>Others</option>
              </select>
              <label></label>
              <span class="help-block">Payment For</span> </div>
          </div>
          <div id="tax_month" class="col-md-3" style="display:none;">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select" name="tax_month">
                <!--  <option value="<?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>">
              <?php $d=strtotime("-7 Months");echo date('F-Y',$d);?>
              </option> -->
                <option value="<?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-6 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-5 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-4 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-3 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-2 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("-1 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php echo date('F-Y');?>" selected><?php echo date('F-Y');?></option>
                <option value="<?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+1 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+2 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+3 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+4 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+5 Months");echo date('F-Y',$d);?>
                </option>
                <option value="<?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>">
                <?php $d=strtotime("+6 Months");echo date('F-Y',$d);?>
                </option>
              </select>
              <label></label>
              <span class="help-block">Select Month</span> </div>
          </div>
          <div class="col-md-3" id="tax_other" style="display:none">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="tax_other" placeholder="Other" value="<?php echo $payments->tax_other;?>">
              <label></label>
              <span class="help-block">Other</span> </div>
          </div>
        </div>
        <!--End of Tax/complience Div--> 
        <!--Start of common div-->
        <div id="common">
          <div class="col-md-6">
            <div class="form-group form-md-line-input">
            	<textarea autocomplete="off" class="form-control"  id="form_control_1" name="description"  placeholder="Description"><?php echo $payments->description;?></textarea>
              <!--<input autocomplete="off" type="text" class="form-control" id="form_control_1" name="description" placeholder="Description" value="<?php echo $payments->description;?>">-->
              <label></label>
              <span class="help-block">Description</span> </div>
          </div>
          <div class="col-md-6">
            <div class="form-group form-md-line-input" id="unitClass">
              <textarea autocomplete="off" class="form-control"  id="memo" name="memo"  placeholder="Memo"></textarea>
              <label></label>
              <span class="help-block">Memo</span> </div>
          </div>
          <div class="col-md-12">
            <h2> Items Details </h2>
            <table class="table table-striped table-hover table-bordered" id="items">
              <thead>
                <tr> 
                  <!--<th width="10%">Image</th>-->
                  <th width="15%">Product/ Service Name</th>
                  <th width="5%">QTY </th>
                  <th width="8%">Unit Price</th>
                  <th width="5%">Tax %</th>
                  <th width="5%">Discount %</th>
                  <th width="10%">Total</th>
                  <th width="5%">Class</th>
                  <th width="10%">Note</th>
                  <th align="center" width="5%">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
				$j=0;
				    $id=$_GET['p_id'];
					$sum=0;
					$j=0;
					$item_details=$this->dashboard_model->get_item_details($_GET['p_id']);
					if(isset($item_details) && $item_details){
						$newid = 0;
					foreach($item_details as $itm){
				 $sum=$sum+$itm->total;
				 $j+=$sum;
				?>
                <tr id="row_<?php echo $itm->line_items_id?>">
                  <td class="hidden-480"><label>
                      <input id="product" name="product1[]" onchange="check_duplicate(this.value)"  value="<?php echo $itm->product;?>" type="text"  class="form-control input-sm" >
                    </label></td>
                  <td class="hidden-480"><label>
                      <input id="qty1<?php echo $itm->line_items_id?>" name="qty1[]" value="<?php echo $itm->qty ?>" type="text"  class="form-control input-sm " onblur="calculation1('<?php echo $itm->line_items_id?>');" >

                    </label></td>
                  <td class="hidden-480"><label>
                      <input id="price1<?php echo $itm->line_items_id?>" name="price1[]"  value="<?php echo $itm->price ?>" type="text"  class="form-control input-sm" onblur="calculation1('<?php echo $itm->line_items_id?>');" >
                    </label></td>
                  <td class="hidden-480"><label>
                      <input  id="tax1<?php echo $itm->line_items_id?>" name="tax1[]" type="text" value="<?php echo $itm->tax ?>" onkeypress=" return onlyNos(event, this);"  onblur="calculation1('<?php echo $itm->line_items_id?>')"  class="form-control input-sm"  placeholder="Tax" >
                    </label></td>
                  <td class="hidden-480"><label>
                      <input type="text" autocomplete="off" class="form-control input-sm" id="disc1<?php echo $itm->line_items_id?>" name="disc1[]" value="<?php echo $itm->discount ?>"  onblur="calculation1('<?php echo $itm->line_items_id?>')"  placeholder="Discount"  onkeypress=" return onlyNos(event, this);">
                    </label></td>
                  <td class="hidden-480"><label>
                      <input type="text" autocomplete="off" class="form-control input-sm" id="total1<?php echo $itm->line_items_id?>" name="total1[]" value="<?php echo $itm->total ?>" placeholder="Total *" onkeypress=" return onlyNos(event, this);">
                    </label></td>
                  <td class="hidden-480"><label>
                      <input id="cls" name="cls1[]" type="text" value="<?php //echo $itm->class ?>" placeholder="Class" class="form-control input-sm" >
                    </label></td>
                  <td class="hidden-480"><label>
                      <input id="note1" name="note1[]" type="text" value="<?php echo $itm->note ?>" placeholder="Note" class="form-control input-sm" >
                    </label></td>
                  <td><a class="btn red btn-sm" onclick="delete_itm('<?php echo $itm->line_items_id;?>','<?php echo $id;?>')"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                  <input type="hidden" name="line_items_id[]" id="line_items_id" value="<?php echo $itm->line_items_id;?>">
                </tr>
                <?php 	}}?>
              <input type="hidden" name="sum" id="sum" value="<?php echo $sum;?>">
              <input type="hidden" name="payment_id" id="payment_id" value="<?php echo $_GET['p_id'];?>">
              <tr>
                <td class="form-group"><input id="nw_product" name="product[]" onchange="check_duplicate(this.value)" type="text" value="" placeholder="Product/ Service Name *" class="form-control input-sm" ></td>
                <td class="form-group"><input  id="qty" name="qty[]" type="text" value="" class="form-control input-sm" onkeypress=" return onlyNos(event, this);"  placeholder="QTY *"  ></td>
                <td class="form-group"><input  id="price" name="price[]" type="text" value="" onkeypress=" return onlyNos(event, this);" class="form-control input-sm" onblur="calculation(this.value)"  placeholder="Price *" ></td>
                  </td>
                <td class="form-group"><input  id="tax" name="tax[]" type="text" value="" onkeypress=" return onlyNos(event, this);"  onblur="calculation(this.value)"  class="form-control input-sm"  placeholder="Tax" ></td>
                <td class="form-group"><input type="text" autocomplete="off" class="form-control input-sm" id="disc" name="disc[]"  onblur="calculation(this.value)"  placeholder="Discount"  onkeypress=" return onlyNos(event, this);"></td>
                <td class="form-group"><input type="text" autocomplete="off" class="form-control input-sm" id="total" name="total[]"  placeholder="Total *" onkeypress=" return onlyNos(event, this);"></td>
                <td class="form-group"><input id="cls" name="cls[]" type="text" value="" placeholder="Class" class="form-control input-sm" ></td>
                <td class="form-group"><input id="note1" name="note1[]" type="text" value="" placeholder="Note" class="form-control input-sm" ></td>
                <td><a class="btn blue btn-sm"  id="additems"><i class="fa fa-plus" aria-hidden="true"></i></a></td>
              </tr>
                </tbody>
              
            </table>
          </div>
          <div class="col-md-12" align="right" style="font-size: 150%; color:#666666;"> Total Amount : <i class="fa fa-inr" style="font-size: 80%; color:#CD4B4B;"></i> <span style="color:#CD4B4B;" id="sp"></span><input type="hidden" name="present_total" id ="present_total" ></div>
          <div class="col-md-12" align="right" style="font-size: 150%; color:#666666;"> Total Due Amount : <i class="fa fa-inr" style="font-size: 80%; color:#CD4B4B;"></i> <span style="color:#CD4B4B;" id="totdue"><?php echo $payments->payments_due?></span> </div>
          <div class="col-md-12">
           <h3>Transactions</h3>
              <?php  
	  $tpa = 0;
	  if($transaction){ ?>
              <table class="table table-striped table-hover table-bordered" width="100%">
                <thead>
                  <tr style="">
                    <th width="8%" align="center" valign="middle" > Transaction ID </th>
                    <th width="15%" align="center" valign="middle" > Transaction Date </th>
                    <th width="8%" align="center" valign="middle" > User </th>
                    <th width="8%" align="center" valign="middle" > Payment Mode </th>
                    <th width="25%" align="center" valign="middle" > Details </th>
                    <th width="8%" align="right" valign="middle"> Amount </th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
		
		foreach ($transaction as $keyt ) {
			if($keyt->t_payment_mode == 'cash')
					$bg = '#FFFDD3';
				else
					$bg = '#F2F2F2';
		
	?>
                  <!--<tr>
          <td align="center" valign="middle"><?php echo $keyt->t_id; ?></td>-->
                  <tr style="background: <?php echo $bg; ?>">
                    <td style="text-align: left;" valign="left"><a href="<?php echo base_url();?>Dashboard/edit_transaction/<?php echo $keyt->t_id; ?>" style="color:#242493;" data-toggle="modal">
                      <?php 
					$dec = 'none';
					if($keyt->t_status == 'Cancel')
						$dec = 'line-through';
					$d = date("d",strtotime($keyt->t_date)).date("m",strtotime($keyt->t_date)).date("y",strtotime($keyt->t_date));
                    $transaction_id='TA0'.$keyt->hotel_id.'/'.$d.'/'.$keyt->t_id;
					echo '<span style="text-decoration:'.$dec.'">'.$transaction_id.'</span>';
					if($keyt->t_status == 'Done'){
						echo ' <i style="color:#9BCA3B;" class="fa fa-check" aria-hidden="true"></i>';
						$tpa = $tpa + $keyt->t_amount;
					}
					if($keyt->t_status == 'Pending')
						echo ' <i style="color:#FFAF4A;" class="fa fa-hourglass-start" aria-hidden="true"></i>';
					if($keyt->t_status == 'Cancel')
						echo ' <i style="color:#F5695E;" class="fa fa-times" aria-hidden="true"></i>';
				?>
                      </a></td>
                    <td style="text-align: left;" valign="left"><?php 
					echo date("g:i A \-\n l jS F Y",strtotime($keyt->t_date)); 
				?></td>
                    <td style="text-align: left;" valign="left"><?php  
					if($keyt->user_id > 0){
						$user = $this->dashboard_model->get_admin($keyt->user_id);
					
						if(isset($user)){
							foreach($user as $user){
							echo $user->admin_first_name.' '.$user->admin_last_name;
							//print_r($user);
							}
						}
					}					
					else
						echo '<span style="color:#AAAAAA;">No info</span>';
					
				?></td>
                    <td align="center" valign="middle"><?php //echo $keyt->t_payment_mode; 
		  $pmode = $this->dashboard_model->get_p_mode_by_name($keyt->t_payment_mode);
		  if($pmode){
						if($pmode->pm_icon != NULL)
							echo $pmode->pm_icon.' ';
						echo $pmode->p_mode_des;
					}
					else
						echo '<span style="color:#AAAAAA;">No info</span>';?></td>
                    <!--<td align="center" valign="middle"><?php echo $keyt->t_bank_name; ?></td>-->
                    <td style="text-align: left;" valign="left"><?php 
					
					if($keyt->t_payment_mode == 'cash'){
						echo '<span style="color:#AAAAAA;">No breakup available</span>';
					}
					else if($keyt->t_payment_mode == 'cards'){
						echo '<span style="color:#0E6767;">['.$keyt->t_bank_name.'] </span>'; 
						echo $keyt->t_card_type; 
						echo ' Card No: '.$keyt->t_card_no; 
						//echo 'Card No: '.$keyt->t_card_no; 
					}
					else if($keyt->t_payment_mode == 'fund'){
						echo '<span style="color:#0E6767;">['.$keyt->t_bank_name.'] </span>'; 
						echo ' Acc No: '.$keyt->ft_account_no; 
						echo ' | IFSC: '.$keyt->ft_ifsc_code; 
					}
					else if($keyt->t_payment_mode == 'cheque'){
						echo '<span style="color:#0E6767;">['.$keyt->t_bank_name.'] </span>'; 
						echo ' Cheque No: '.$keyt->checkno;
						echo '| Drw Name : '.$keyt->t_drw_name;
						
					}
					else if($keyt->t_payment_mode == 'draft'){
						echo '<span style="color:#0E6767;">['.$keyt->t_bank_name.'] </span>'; 
						echo ' Draft No: '.$keyt->draft_no;
						echo '| Drw Name : '.$keyt->t_drw_name;
					}
					else if($keyt->t_payment_mode == 'ewallet'){
						echo '<span style="color:#0E6767;">['.$keyt->t_w_name.'] </span>'; 
						echo ' Tran ID: '.$keyt->t_tran_id;
						echo '| Recv Acc : '.$keyt->t_recv_acc;
					}
					else
						echo '<span style="color:#AAAAAA;">No info</span>';
				?></td>
                    <!-- <td align="right" valign="middle"><?php echo $keyt->t_amount; ?></td>-->
                    <td style="text-align: left;" valign="left"><?php 
					if($keyt->t_amount > 10000)
						echo '<span style="color:#E74C3C;">'.'INR '.$keyt->t_amount.'</span>';
					else if($keyt->t_amount > 5000)
						echo '<span style="color:#242493;">'.'INR '.$keyt->t_amount.'</span>';
					else
						echo '<span>'.'INR '.$keyt->t_amount.'</span>';
				?></td>
                  </tr>
                  <?php  } 
	?>
                </tbody>
              </table>
              <?php
			echo '<i class="fa fa-info-circle" aria-hidden="true"></i> <span style="font-weight:bold; color:#FF007F;">Total Paid Amount: INR '.number_format($tpa,2).'</spam>';
			}
			else 
				echo '<i class="fa fa-info-circle" aria-hidden="true"></i> <span style="font-weight:bold; color:#696969;">There are no payment added for this group booking</span>';
			$bg = '';
			?>
          </div>
          <div class="col-md-12">
            <h2> Account Details </h2>
            <table class="table table-striped table-hover table-bordered table-scrollable" id="accounts">
              <thead>
                <tr> 
                  <!--<th scope="col">
						Select
					</th>-->
                  <th scope="col"> Account </th>
                  <th scope="col"> Description </th>
                  <th scope="col"> Amount </th>
                  <th scope="col"> Class </th>
                  <th scope="col"> Vendor/Reciver </th>
                  <th align="center" scope="col" width="5%"> Action </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="form-group"><input id="account" name="account[]" type="text" value="" placeholder="Account" class="form-control input-sm" ></td>
                  <td class="form-group"><input  id="des" name="des[]" type="text" value="" class="form-control input-sm"  placeholder="Description *" ></td>
                    </td>
                  <td class="form-group"><input  id="amount" name="amount1[]" type="text" value="" class="form-control input-sm"  placeholder="Amount *" ></td>
                  <td class="form-group"><input type="text" autocomplete="off" class="form-control input-sm" id="cls1" name="cls1[]" placeholder="Class *"></td>
                  <td class="form-group"><input type="text" autocomplete="off" class="form-control input-sm" id="vendor1" name="vendor1[]"  placeholder="Vendor *"></td>
                  <td><a class="btn green btn-sm"  id="addaccounts"><i class="fa fa-plus" aria-hidden="true"></i></a></td>
                </tr>
              </tbody>
            </table>
          </div>
          <?php  $due_pay=$payments->payments_due;
						$due_date=$payments->payments_due_date;  
							// $payment_details=$this->dashboard_model->get_pay_details($id);
						if($payments->payments_status==0 || $payments->payments_status==2){
							$color="#2883b7";
		  ?>
          <div class="col-md-12" style="text-align:center">
            <div class="btn-group">
              <label for="autocellwidth" class="auto_cl btn grey-cascade" id="auto_cl_id"  style="background-color:<?php echo $color;?>;">
                <input type="checkbox" id="autocellwidth" checked class="toggle" style="display:none;">
                <span id="disp">Pay Later</span></label>
            </div>
          </div>
          <?php }?>
          <div id="pay" style="display:none;">
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="pay_amount" name="pay_amount" placeholder="Amount"  onchange="chnge_amt(this.value)">
                <label></label>
                <span class="help-block">Amount</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <select class="form-control bs-select"  id="mop" name="mop" onchange="modesofpayments();">
                  <option value="" disabled selected>Mode Of Payment</option>
                  <?php $mop = $this->dashboard_model->get_payment_mode_list();
			  if($mop != false)
			  {
				  foreach($mop as $mp)
				  {
				?>
                  <option value="<?php echo $mp->p_mode_name; ?>"><?php echo $mp->p_mode_des; ?></option>
                  <?php }
			  } ?>
                </select>
                <label></label>
                <span class="help-block">Modes of Payments *</span> </div>
            </div>
            <div id="check" style='display:none'>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="checkno" placeholder="Check no. *"  onkeypress=" return onlyNos(event, this);">
                  <label></label>
                  <span class="help-block">Check no. *</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="check_bank_name" placeholder="Bank Name *">
                  <label></label>
                  <span class="help-block">Bank Name *</span> </div>
              </div>
            </div>
            <!--End div of Payment mode by check--> 
            <!--Start of payment mode by draft-->
            <div id="draft" style='display:none'>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="draft_no" placeholder="Draft no *"  onkeypress=" return onlyNos(event, this);">
                  <label></label>
                  <span class="help-block">Draft no</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="draft_bank_name" placeholder="Bank Name *">
                  <label></label>
                  <span class="help-block">Bank Name *</span> </div>
              </div>
            </div>
            <!--End of payment mode by draft--> 
            <!--Start of fundtranfer under common div-->
            <div id="fundtransfer" style='display:none'>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_bank_name" placeholder="Bank Name">
                  <label></label>
                  <span class="help-block">Bank Name</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_account_no" placeholder="Account No *" maxlength="15" onkeypress=" return onlyNos(event, this);">
                  <label></label>
                  <span class="help-block">Account No *</span> </div>
              </div>
              <div class="col-md-4">
                <div class="form-group form-md-line-input">
                  <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_ifsc_code" placeholder="IFSC Code">
                  <label></label>
                  <span class="help-block">IFSC Code</span> </div>
              </div>
            </div>
            <!--End of fundtranfer under common div-->
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="approved_by"  placeholder="Approved By *">
                <label></label>
                <span class="help-block">Approved By *</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="recievers_name"  placeholder="reciver Name  *">
                <label></label>
                <span class="help-block">reciver Name *</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="paid_by" placeholder="Reciever Name">
                <label></label>
                <span class="help-block">Paid By</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="recievers_desig" placeholder="Reciever's Designation *">
                <label></label>
                <span class="help-block">Reciever's Designation *</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <select class="form-control bs-select" id="profit_center1" name="profit_center1">
                  <option value="" selected="selected" disabled="disabled">Profit Center *</option>
                  <?php $pc=$this->dashboard_model->all_pc();?>
                  <?php
                      $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
                      ?>
                  <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
                  <?php $pc=$this->dashboard_model->all_pc1();
                        foreach($pc as $prfit_center){
                        ?>
                  <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                  <?php }?>
                </select>
                <label></label>
                <span class="help-block">Profit Center *</span> </div>
            </div>
          </div>
          <div id="pay1">
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control date-picker"   id="payments_due_date" name="payments_due_date" placeholder="payments Due Date" value="<?php echo $due_date?>">
                <label></label>
                <span class="help-block">Payments Due Date</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" readonly id="payments_due" onmouseover="check_due(this.value)" name="payments_due" placeholder="Payments Due" value="<?php echo $due_pay?>">
                <input autocomplete="off" type="hidden" class="form-control" readonly id="payments_due1" name="payments_due1" placeholder="Payments Due"  value="<?php echo $due_pay?>">
                <label></label>
                <span class="help-block">Payments Due</span> </div>
            </div>
          </div>
        </div>
      </div>
      <input autocomplete="off" type="hidden" class="form-control" id="vendor_id" name="vendor_id" value="<?php echo $payments->vendor_id;?>">
    </div>
    <div class="form-actions right">
      <input type="submit" class="btn blue" value="Submit">
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
  </div>
</div>
<script>
 function check_due(val){
	 //alert(val);
	 var due=$("#payments_due").val(val).val();
	 var pay_amt=$('#pay_amount').val();
	 var ch_pay_amt=$('#chng_pay_amt').val();
	 
	 	//alert(ch_pay_amt);
	if(val==due){
		$("#paments_status").attr('value','0').change();
	}
 }
           $(document).ready(function() {
                        cellWidth = 100;
                        cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
						
                       
                    });

            $("#autocellwidth").click(function() {
                      cellWidth = 100;  // reset for "Fixed" mode
                        cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                      document.getElementById('auto_cl_id').style.backgroundColor = $(this).is(":checked") ? "#297CAC" : "#39B9A1";
                        var a = $(this).is(":checked") ? "Pay Later" : "Pay Now";
						if(a == 'Pay Later')
						{	
							document.getElementById('pay').style.display = 'block';
	                        document.getElementById('pay1').style.display = 'block';
						}
						else
						{	
							
						    document.getElementById('pay').style.display = 'none';
	                        document.getElementById('pay1').style.display = 'block';
						}
						
						$('#disp').text(a);

					});
					
					

   function typesofpayments()
   {
	  var x= document.getElementById("top").value;
	   
	   if(x=="Bill payments"){
		   document.getElementById("billpayments").style.display="block";
	   }else{
		   document.getElementById("billpayments").style.display="none";
	   }
	   if(x=="Salary/Wage Payments"){
		   document.getElementById("salarypayments").style.display="block";
	   }else{
		   document.getElementById("salarypayments").style.display="none";
	   }
	   if(x=="Miscellaneous Payments"){
		   document.getElementById("miscpayments").style.display="block";
	   }else{
		   document.getElementById("miscpayments").style.display="none";
	   }
     if(x=="Tax/Compliance Payment"){
       document.getElementById("taxpayments").style.display="block";
     }else{
       document.getElementById("taxpayments").style.display="none";
     }
   }
   function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	   //alert(y);
	    
	   if(y=="check"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fund"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }
   function paymentfor(){
    var y= document.getElementById("payfor").value;
    //alert(y);
    if(y=="month"){
       document.getElementById("misc_month").style.display="block";
       document.getElementById("misc_other").style.display="none";
     }
     if(y=="other"){
       document.getElementById("misc_other").style.display="block";
       document.getElementById("misc_month").style.display="none";
     }
   }
   function paymentforsal(){
    var y= document.getElementById("payforsal").value;
  
     if(y=="month"){
       document.getElementById("sal_month").style.display="block";
       document.getElementById("sal_other").style.display="none";
     }
     if(y=="other"){
       document.getElementById("sal_other").style.display="block";
       document.getElementById("sal_month").style.display="none";
     }
   }
   
   function paymentforbill(){
    var y= document.getElementById("payforbill").value;
   //alert(y);
     if(y=="month"){
       document.getElementById("bill_month").style.display="block";
       document.getElementById("bill_other").style.display="none";
     }
     if(y=="other"){
       document.getElementById("bill_other").style.display="block";
       document.getElementById("bill_month").style.display="none";
     }
   }
   
   function paymentterm(){
    var y = document.getElementById("paymentt").value;
     if(y=="other"){
		document.getElementById("bill_other_dt").style.display="block";
     }
	 else{
		document.getElementById("bill_other_dt").style.display="none"; 
	 }
   } // _sb dt24_10_16
   
   function paymentfortax(){
    var y= document.getElementById("payfortax").value;
   // alert(y);
     if(y=="month"){
       document.getElementById("tax_month").style.display="block";
       document.getElementById("tax_other").style.display="none";
     }
     if(y=="other"){
       document.getElementById("tax_other").style.display="block";
       document.getElementById("tax_month").style.display="none";
     }
   }
   
 function removeRow(id,total){
	
	swal({   title: "Are you sure?",   text: "Do you want to remove the row!",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){   swal("Deleted!", "Yes delete the row.", "success");$('#row_'+id).remove(); });
	
	var total1=parseFloat($('#sp').text());
	//alert(total);
	//alert(total1);
	//alert(new_sum);
	new_sum=parseFloat(total1)-parseFloat(total);
	
	$('#sp').text(new_sum);
	}
	
   var sum=0; 
  var flag=0;
  var flag1=0;
  var presnt_total = 0;
   $("#additems").click(function(){
	
	var x=0;
	var product = $('#nw_product').val();
	var qty =$('#qty').val();
	var price = $('#price').val();
	var tax = $('#tax').val();
	var total = $('#total').val();
	var disc = $('#disc').val();
	var cls = $('#cls').val();
	var note = $('#note1').val();
	var sum_total=$('#sum').val();
	alert(product);
	if(tax==''){
		tax=0;
	} if(disc==''){
		disc=0;
	}if(cls==''){
		cls=0;
	}
	if(product!='' && qty!='' && price!=''){
	
	//$('#ids').val( it+','+ i_name) ;
	$('#items tbody tr:first').before('<tr id="row_'+flag+'">'+
	'<td><input name="product1[]" id="product11'+flag+'"  type="text" value="'+product+'" onblur="calculation2('+flag+');" class="form-control input-sm" ></td>'+
	'<td><input name="qty1[]" id="qty11'+flag+'" type="text" value="'+qty+'" onblur="calculation2('+flag+');"  class="form-control input-sm" ></td>'+
	'<td><input name="price1[]" id="price11'+flag+'" type="text" value="'+price+'" onblur="calculation2('+flag+');"    class="form-control input-sm" ></td>'+
	'<td><input name="tax1[]" id="tax11'+flag+'" type="text" value="'+tax+'"  onblur="calculation2('+flag+')"   class="form-control input-sm" ></td>'+
	
	'<td><input name="disc1[]" id="disc11'+flag+'" type="text" value="'+disc+'"  onblur="calculation2('+flag+')"  class="form-control input-sm" ></td>'+
	'<td><input name="total1[]" id="total11'+flag+'"  type="text" value="'+total+'"  class="form-control input-sm" ></td>'+
	
	'<td><input name="cls1[]" id="cls1'+flag+'" type="text" value="'+cls+'" class="form-control input-sm" ></td>'+
	'<td><input name="note1[]" id="note1'+flag+'" type="text" value="'+note+'" class="form-control input-sm" ></td>'+
	'<td><a href="javascript:void(0);" class="btn red btn-sm" onclick="removeRow('+flag+','+total+')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>');	
	
	x=x+1;
	
	 sum=sum + parseFloat($('#total11'+flag).val())+parseFloat(sum_total);
	 alert('pt' + $('#present_total').text());
	 alert('total11'+ parseFloat(sum_total));
	 presnt_total = parseFloat($('#present_total').text()) +  parseFloat($('#total11'+flag).val());
	 alert(presnt_total);
	 $('#sp').text(presnt_total);
	 $('#present_total').text(presnt_total);
	flag++;
	
	 var product = $('#nw_product').val('');
	$('#qty').val('');
	$('#price').val('');
	$('#tax').val('');
	$('#total').val('');
	$('#disc').val('');
	$('#cls').val('');
	$('#note1').val('');
	$("#payments_due").val(sum);   
	$("#payments_due1").val(sum);   
	
	  
	}else{alert('enter value');}});

	
	$("#addaccounts").click(function(){
	 
	var x1=0;
	var account = $('#account').val();
	var des =$('#des').val();
	var amount = $('#amount').val();
	var cls1 = $('#cls1').val();
	var vendor1 = $('#vendor1').val();
	
	
	
	if(account!='' && des!='' && amount!='' && cls1!='' && vendor!=''){
	$('#accounts tr:first').after('<tr id="row_'+flag1+'">'+
	'<td><input name="account[]" id="contract_start_date'+flag1+'" type="text" value="'+account+'" class="form-control input-sm" readonly></td>'+
	'<td><input name="des[]" id="contract_start_date'+flag1+'" type="text" value="'+des+'" class="form-control input-sm" readonly></td>'+
	'<td><input name="amount[]" id="contract_start_date'+flag1+'" type="text" value="'+amount+'" class="form-control input-sm" readonly></td>'+
	'<td><input name="cls1[]" id="contract_start_date'+flag1+'" type="text" value="'+cls1+'" class="form-control input-sm" readonly></td>'+
	
	'<td><input name="vendor1[]" id="contract_start_date'+flag1+'" type="text" value="'+vendor1+'" class="form-control input-sm" readonly></td>'+
	
	'<td><a href="javascript:void(0);" class="btn red btn-sm" onclick="removeRow('+flag1+')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>');	
	x1=x1+1;
	flag1++;
	 //  var product = $('#new_product').val('');
	$('#account').val('');
	$('#des').val('');
	$('#tax').val('');
	$('#amount').val('');
	$('#cls1').val('');
	$('#vendor1').val('');
	  
	}else{alert('enter value');}});
	
	
	function calculation(val){
			//alert(val);
	var qty =$('#qty').val();
	var price = $('#price').val();
	var tax = $('#tax').val();
	var disc = $('#disc').val();
	
	var amount=qty*parseFloat(price)+(qty*parseFloat(price)*tax/100);
	var t_amount=amount-(qty*parseFloat(price)*disc/100);
		if(val==0){
			$("#total").val(amount);
		}else{
	$("#total").val(t_amount);
		}
	}
	
	function calculation1(id){
			//alert(val);
	var qty =$('#qty1'+id).val();
	var price = $('#price1'+id).val();
	var tax = $('#tax1'+id).val();
	var disc = $('#disc1'+id).val();
	
	var amount=qty*parseFloat(price)+(qty*parseFloat(price)*tax/100);
	var t_amount=parseFloat(amount-(qty*parseFloat(price)*disc/100));
		
	$("#total1"+id).val(t_amount);
	$('#sp').text(t_amount);
	$('#present_total').text(t_amount);
	}
	function calculation2(id){
			
	var qty =$('#qty11'+id).val();
	var price = $('#price11'+id).val();
	var tax = $('#tax11'+id).val();
	var disc = $('#disc11'+id).val();
	
	var myTotal = 0; 
	var len = total1.length;
	for(var i = 0; i < len; i++) {
		myTotal += total1[i];
	}
	alert(myTotal);
	
	var amount=qty*parseFloat(price)+(qty*parseFloat(price)*tax/100);
	var t_amount=parseFloat(amount-(qty*parseFloat(price)*disc/100));
		
	$("#total11"+id).val(t_amount);
	$('#sp').text(t_amount);
	$('#present_total').text(t_amount);
	}
	
	
function show_modal(){
	$('#basic1').modal('show');
}	
  

function set_vendor(a,b,c,d){
	//alert(d)
	$("#select_l_itm").val(a);
	$("#address").val(b);
	$("#ph_no").val(c);
	$("#vendor_id").val(d);
	
	$('#basic1').modal('toggle');
	
}	
	
	
</script> 
<script>

function search_vendor(){

  var vendor_name=$("#check_vendor").val();

  
	
    $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>dashboard/search_vendor",
    data:{data:vendor_name},
    success: function(data){
		if(data != "")
		{
			//alert(data);
			return false;
	  $("#services").show();
     $('#services').html(data);
	  //alert($('#service').html());
		}
    }
    });
 }
 
 function chnge_amt(val){
	 alert(val);
	 if(val==''){
		 val=0;
	}
	
	 var due_pay=$('#payments_due1').val();
	 var pay_amt=$('#pay_amount').val();
	  alert(due_pay);
	 var due_amt=parseFloat(due_pay)-parseFloat(val);
	if(parseFloat(val)<=due_pay){
	var pay_due=$("#payments_due").val(due_amt);  
	var pay_amt1=$('#payments_due').val();
	//alert(pay_amt1);
	}else{
		
		swal("Amount must be lesser then Due amount!","", "error")
		$('#pay_amount').val('');
		//pay_amt=$('#pay_amount').val(due_pay);
		
	}
	
	if(due_amt==0){
		$('#payments_due_date').css('display','none');
	}else{
		$('#payments_due_date').css('display','block');
	}
 }

 function check_date(){
	
	var a=$('#bill_date').val();
	 a=new Date(a);
	   var b=$('#bill_due_date').val();
	   b=new Date(b);
	   if(a>=b && b!='' ){
		  // alert('End Date sould getter tan Start Date ');
		   swal("Due Date sould getter than Bill Date!", "", "error")
		   $('#bill_due_date').val('');
	   }
		  
	
}

function check_duplicate(val){
	 $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>dashboard/duplicate_product",
    data:{data:val},
    success: function(data){
			
		if(data.data== "y")
		{
			 swal("Duplicate product name!", "", "error")
			 $('#product').val('');
		}
    }
    });
}



$(document).ready(function() {
	var hVal = $("#hidVal").val();
	var payfor=$("#payforbill").val();
	var payfor_sal=$("#payforsall").val();
	var payfor_misc=$("#miscpayfor").val();
	var payfor_tax=$("#payfortax").val();
	var mop=$("#mop").val();
	//var tax_other=$("#t_other").val();
	//alert(mop);
		//return false;
		var sum_total=$('#sum').val();
		$('#sp').text(sum_total);
		$('#present_total').text(sum_total);
		//$('#payments_due').val(sum_total);
	if(mop=='Draft'){
		$("#draft").css("display","block");
		}else{
			$("#draft").css("display","none");
			}
	if(mop=='Check'){
		$("#check").css("display","block");
		}else{
			$("#check").css("display","none");
			}
	if(mop=='Fundtransfer'){
		$("#fundtransfer").css("display","block");
		}else{
			$("#fundtransfer").css("display","none");
			}

	if(hVal == 'Bill payments'){
		$("#billpayments").css("display", "block");
	}else{
         $("#billpayments").css("display", "none");
		}
if(hVal == 'Salary/Wage Payments'){
		$("#salarypayments").css("display", "block");
	}else{
         $("#salarypayments").css("display", "none");
		}
if(hVal == 'Miscellaneous Payments'){
		$("#miscpayments").css("display", "block");
	}else{
         $("#miscpayments").css("display", "none");
		}

if(hVal == 'Tax/Compliance Payment'){
		$("#taxpayments").css("display", "block");
	}else{
         $("#taxpayments").css("display", "none");
		}

if(payfor =='month'){
		$("#bill_month").css("display", "block");
	}else{
         $("#bill_other").css("display", "block");
		}
if(payfor_sal =='month'){
		$("#sal_month").css("display", "block");
	}else{
         $("#sal_other").css("display", "block");
		}
if(payfor_misc =='month'){
		$("#misc_month").css("display", "block");
	}else{
         $("#misc_other").css("display", "block");
		$("#misc_month").css("display", "none");
		}
	
	if(payfor_tax =='month'){
		$("#tax_month").css("display", "block");
	}else{
         $("#tax_other").css("display", "block");
		 $("#tax_month").css("display", "none");
		}
	
	
	
});

function delete_itm(id,i_id){
	alert(id);
	alert(i_id); 
 swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

    $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>dashboard/delete_itm",
    data:{data:id,i_id:i_id},
    success: function(data){
			
		swal({                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
				title: data.data,
				text: "",
				type: "success"
			},
			function(){
				//location.reload();

				$('#row_'+id).remove();

			});
    }
    });



        });
	 
	 
	 
	 
	 
}


</script> 
<script>

$.typeahead({
    input: '.js-typeahead-user_v1',
    minLength: 1,
    order: "asc",
    dynamic: true,
    delay: 200,
    backdrop: {
        "background-color": "#fff"
    },
   
    source: {
       project: {
            display: "name",
            ajax: [{
                type: "GET",
                url: "<?php echo base_url();?>dashboard/search_vendor",
                data: {
                    q: "{{query}}"
                }
            }, "data.name"],
            template: '<div class="clearfix">' +
						'<span class="project-img">' +
                    '<img src="{{image}}">' +
                '</span>' +
			          '<div class="project-information">' +
                    '<span class="pro-name" style="font-size:15px;"> {{name}}</span>' +
					 '<span class="pro-name" style="font-size:15px;"> {{icon}}</span>'+
                '</div>' +
            '</div>'
        }
    },
    callback: {
        onClick: function (node, a, item, event) {
         // alert(JSON.stringify(item));
		// $("#c_unit_price").val(item.unitprice);
		$("#vendor_id").val(item.id);
		$("#vendor_id").val(item.id);
		
		
		 
        },
        onSendRequest: function (node, query) {
            console.log('request is sent')
			
        },
        onReceiveRequest: function (node, query) {
            //console.log('request is received')
	
			
        }
    },
    debug: true
});

</script>