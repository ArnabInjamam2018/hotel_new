
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green"> <span class="caption-subject bold uppercase"><i class="glyphicon glyphicon-bed"></i>&nbsp; Update Unit Type</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
	//print_r($datas);
if(isset($datas)){
			 
				
				$id=$datas->booking_nature_visit_id;
                                $form = array(
                                    'class' 			=> 'form-body',
                                    'id'				=> 'form',
                                    'method'			=> 'post'
                                );
								//$id=$edit_id;
                                echo form_open_multipart('unit_class_controller/edit_booking_nature_visit/'.$id,$form);
								/*$unit_type= $UnitDetail->unit_type;
								$unit_name= $UnitDetail->unit_name;
								$unit_class= $UnitDetail->unit_class;
								$unit_desc= $UnitDetail->unit_desc;*/
                                ?>
    <div class="form-body">
      <div class="row">
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="text" class="form-control focus" name="name" required="required" value="<?php echo $datas->booking_nature_visit_name;?>">
          <label>Booking nature of visit<span class="required">*</span></label>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="text" autocomplete="off" class="form-control focus" id="description" name="description"  value="<?php echo $datas->booking_nature_visit_description ;?>">
          <label>Booking nature of description<span class="required">*</span></label>
       </div>
	   <input type="hidden" name="id" value="<?php echo $datas->booking_nature_visit_id ; ?>">
       
		 <input type="hidden" id="hid" value="<?php echo $datas->status; ?>">
		 <div class="form-group form-md-line-input form-md-floating-label col-md-4">
            <select onchange="check_value(this.value)" class="form-control focus"  id="default" name="default" required="required" >
			  <option value="0">No</option>
              <option value="1">Yes</option>          
              
            </select>
            <label for="form_control_2">Default nature visit<span class="required">*</span></label>
          </div>
      </div>
    </div>
    <div class="form-actions right">
    	<input type="submit" class="btn green" value="update">
    </div>
    <?php form_close(); ?>
    <?php }?>
    <script>
function check(){
	var a=$("#add_admin").val();
	var b=isNaN(a);
	if(b){
		alert("enter number only");
		$("#add_admin").val('')
	}
}


function check_value(value){
		
		var stat=$("#hid").val();
		//alert(def);
		
			if(value==1 && stat==1){	
          $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/check_nature_visit_default",
                data:{a:value},
                 success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                 

				swal({   title: "Are you sure?",
				text: data+"!",  
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Fix as Default!",   cancelButtonText: "No, cancel plz!", 
				closeOnConfirm: true,  
				closeOnCancel: true },
				function(isConfirm){   
				if (isConfirm) {     
				$("#default").val('1');
				} else {   
				$("#default").val('0');
				} });



				 
                }
            });
			}
			
			else if(value==1){
				swal({
						title: 'Inactive status!',
						text: 'Save change as active to set default.',
					timer: 2000
						});
						
						$("#default").val('0');
						

			}
			
			
		
	}







</script> 
  </div>
</div>
