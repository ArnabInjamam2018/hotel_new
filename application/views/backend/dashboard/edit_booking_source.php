
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green"> <span class="caption-subject bold uppercase"><i class="glyphicon glyphicon-bed"></i>&nbsp; Update Booking Source</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
if(isset($data)){
			foreach($data as $value) {
				$id=$value->booking_source_id;
                                $form = array(
                                    'class' 			=> 'form-body',
                                    'id'				=> 'form',
                                    'method'			=> 'post'
                                );
								//$id=$edit_id;
                                echo form_open_multipart('unit_class_controller/edit_booking_source/'.$id,$form);
								/*$unit_type= $UnitDetail->unit_type;
								$unit_name= $UnitDetail->unit_name;
								$unit_class= $UnitDetail->unit_class;
								$unit_desc= $UnitDetail->unit_desc;*/
                                ?>
    <div class="form-body">
      <div class="row">
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="text" class="form-control focus" name="name" required="required" value="<?php echo $value->booking_source_name;?>">
          <label>Booking source name<span class="required">*</span></label>
        </div>
		
		<input type="hidden" name="Hid" value="<?php echo $id;?>">
		
		<!--<div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="text" class="form-control focus" name="hotel_id"  value="<?php //echo $value->booking_source_name;?>">
          <label>Booking source hotel id<span class="required">*</span></label>
        </div>-->
		
		
		
		<div class="form-group form-md-line-input form-md-floating-label col-md-4">
          
		   <textarea class="form-control focus" name="desc" id="desc" ><?php echo $value->booking_source_description;?> </textarea>
          <label>Booking source description</label>
        </div>
		
		<input type="hidden" id="hid" value="<?php echo $value->status;?>">
		
		
 <!--<div class="form-group form-md-line-input form-md-floating-label col-md-4">
            <select class="form-control focus" id="ta" name="ta" required="required" >
              <option value="0">Yes</option>
              <option value="1">No</option>
              
            </select>
            <label for="form_control_2">Booking source is ta<span class="required">*</span></label>
          </div>-->
       
		<div class="form-group form-md-line-input form-md-floating-label col-md-4">
            <select class="form-control focus" onchange="check_default(this.value)" id="default" name="default" required="required" >
              <option value="0">No</option>
              <option value="1">Yes</option>
              
            </select>
            <label for="form_control_2">Booking default<span class="required">*</span></label>
          </div>
		
      </div>
    </div>
    <div class="form-actions right">
    	<input type="submit" class="btn green" value="Submit">
    </div>
    <?php form_close(); ?>
    <?php }}?>
    
  </div>
</div>
<script>
	
	 function check_default(value){
		
		var stat=$("#hid").val();
		//alert(stat);
		
			if(value==1 && stat==1){	
          $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/check_profit_center_default",
                data:{a:value},
                 success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                 

				swal({   title: "Are you sure?",
				text: data+"!",  
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Fix as Default!",   cancelButtonText: "No, cancel plz!", 
				closeOnConfirm: true,  
				closeOnCancel: true },
				function(isConfirm){   
				if (isConfirm) {     
				$("#default").val('1');
				} else {   
				$("#default").val('0');
				} });



				 
                }
            });
			}
			
			else if(value==1){
				swal({
						title: 'Inactive status!',
						text: 'Save change as active to set default.',
					timer: 2000
						});
						
						$("#default").val('0');
						

			}
			
			
		
	}
	
	
function check(){
	var a=$("#add_admin").val();
	var b=isNaN(a);
	if(b){
		alert("enter number only");
		$("#add_admin").val('')
	}
}
</script> 