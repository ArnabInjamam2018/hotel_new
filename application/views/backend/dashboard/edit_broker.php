
<script type="text/javascript">
	
	$(document).ready(function(){
    	 $("#pic").hide();
		 if($('#agency_yn').val() == '5')
		 {
			 //alert($('#agency_yn').val());
			 //$("#b_agency_name").show();
		 }
		 else{
			$("#chk").hide();
		 	$("#b_agency_name").hide();
		 	$("#ct_name").attr("required","true");	
		 }
    });
	
	function hideShow(t)
	{
		if($('#agency_yn').val() == '5')
		{
			$("#chk").show();
			$("#b_agency_name").show();
			$("#b_contact_name").hide();
			$("#ct_name").removeAttr("required");
			$("#ag_name").attr("required","true");
		 	//$("#ct_name").attr("required","false");
		}
		else
		{
			$("#chk").hide();
			$("#b_agency_name").show();
			$("#b_contact_name").show();
			//$("#ag_name").attr("required","false");
		 	$("#ct_name").attr("required","true")
			$("#ag_name").removeAttr("required");
			$("#ag_name").val("");
		}
	}
	function showChange()
	{
		$("#pic").show();
		$("#btnchange").hide();
		return false;
	}
	
	$(function () {
    $("#uploadFile").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = $("#imagePreview");
            dvPreview.html("");
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function () {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = $("<img />");
                        img.attr("style", "height:100px;width: 100px");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    alert(file[0].name + " is not a valid image file.");
                    dvPreview.html("");
                    return false;
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
});
	
</script>
<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Edit Broker</span> </div>
  </div>
  <div class="portlet-body form">
  <?php

                $form = array(
                    'class' 			=> '',
                    'id'				=> 'form',
                    'method'			=> 'post'
                );

                echo form_open_multipart('dashboard/edit_broker',$form);
                
                ?>
  
    <div class="form-body">
      
      <?php
                            foreach($value as $broker_edit)
                            {
                    
                                    if($broker_edit->b_agency==5){
                                    $agnc = "Yes";}
                                    else{
                                    $agnc = "No";
                            }
                            ?>
      <div class="row">
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <select class="form-control bs-select" id="agency_yn" name="b_agency" onchange="hideShow();" >
            <option value="<?php echo$broker_edit->b_agency; ?>"><?php echo $agnc; ?></option>
            <?php if($broker_edit->b_agency == 5 ){ ?>
            <option value="6">No</option>
            <?php } else {?>
            <option value="5">Yes</option>
            <?php } ?>
          </select>
          <label></label>
          <span class="help-block">Agency Name *</span>
        </div>
        </div>
        <div class="col-md-4" id="chk">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="ag_name" name="b_agency_name" onkeypress="return onlyLtrs(event, this);" value="<?php echo $broker_edit->b_agency_name; ?>" placeholder="Agency Name *">
          <label></label>
          <span class="help-block">Agency Name *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="ct_name" name="b_name"  onkeypress="return onlyLtrs(event, this);" value="<?php echo $broker_edit->b_name; ?>" placeholder="Contact Name">
          <label></label>
          <span class="help-block">Contact Name *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="b_address" required value="<?php echo $broker_edit->b_address; ?>" placeholder="Address *">
          <label></label>
          <span class="help-block">Address *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="b_contact" required onkeypress="return onlyNos(event, this);" maxlength="10" value="<?php echo $broker_edit->b_contact; ?>" placeholder="Mobile No *">
          <label></label>
          <span class="help-block">Mobile No *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  type="email" autocomplete="off" class="form-control" id="form_control_1" name="b_email" value="<?php echo $broker_edit->b_email; ?>" placeholder="Email *">
          <label></label>
          <span class="help-block">Email *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="form_control_1" name="b_website" value="<?php echo $broker_edit->b_website; ?>" placeholder="Website">
          <label></label>
          <span class="help-block">Website</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="form_control_1" name="b_pan" value="<?php echo $broker_edit->b_pan; ?>" placeholder="Pan Card No.">
          <label></label>
          <span class="help-block">Pan Card No.</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="form_control_1" name="b_bank_acc_no"  onkeypress="return onlyNos(event, this);" value="<?php echo $broker_edit->b_bank_acc_no; ?>" placeholder="Bank Account No.">
          <label></label>
          <span class="help-block">Bank Account No.</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="b_bank_ifsc" value="<?php echo $broker_edit->b_bank_ifsc; ?>" placeholder="IFSC Code">
              <label></label>
              <span class="help-block">IFSC Code</span>
              <input type="hidden" name="broker_id" value="<?php echo $broker_edit->b_id; ?>" />
            </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="b_commission" value="<?php echo $broker_edit->broker_commission; ?>">
          <label></label>
          <span class="help-block">Commission</span> </div>
        </div>
        <div class="col-md-12">
			<div class="row">
				<div class="col-md-3">
				<div class="form-group form-md-line-input uploadss">
				  <label>Photo Proof</label>
				  <div class="fileinput fileinput-new" data-provides="fileinput">
					<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
						<img src="<?php echo base_url();?>upload/broker/image/<?php if( $broker_edit->b_photo_thumb== '') { echo "no_images.png"; } else { echo $broker_edit->b_photo_thumb; }?>" alt=""/>
					</div>
					<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
					</div>
					<div>
						<span class="btn default btn-file">
						<span class="fileinput-new">
						Select image </span>
						<span class="fileinput-exists">
						Change </span>
						<input  type="file" id="uploadFile" name="image_photo" />
						</span>
						<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
						Remove </a>
					</div>
				</div>
				</div>
				</div>
				<div class="col-md-3">
				<div class="form-group form-md-line-input uploadss">
				  <label>Other Document</label>
				  <div class="fileinput fileinput-new" data-provides="fileinput">
					<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
					   <?php 
					   $broker_doc=$broker_edit->b_doc;
					   $broker_ext=explode(".", $broker_doc);
					   if($broker_ext[1]=="jpg" || $broker_ext[1]=="png" || $broker_ext[1]=="gif" || $broker_ext[1]=="jpeg" ){
					   ?>
					   <img src="<?php echo base_url();?>upload/broker/image/<?php if( $broker_edit->b_doc== '') { echo "no_images.png"; } else { echo $broker_edit->b_doc; }?>" alt=""/>
					   <?php 
					   }else{
						   echo "<pre>";
						   echo $broker_edit->b_doc;
					   }
					   ?>

					</div>
					<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
					</div>
					<div>
						<span class="btn default btn-file">
						<span class="fileinput-new">
						Select image </span>
						<span class="fileinput-exists">
						Change </span>
						<input  type="file" id="docfile" name="doc_photo" />
						</span>
						<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
						Remove </a>
					</div>
				</div>
				</div>
				</div>
			</div>
        </div>
      </div>
     </div>
      <div class="form-actions right">
        <button type="submit" class="btn submit">Submit</button>
      </div>
    
    <?php 
                        }
                        form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>
