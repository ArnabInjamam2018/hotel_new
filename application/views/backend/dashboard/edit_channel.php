<!-- BEGIN PAGE CONTENT-->
<!--<script src="../../../../assets/common/js/jquery.min.js" type="text/javascript"></script>-->

<<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script type="text/javascript">
	
	$(document).ready(function(){
    	 $("#pic").hide();
		 if($('#agency_yn').val() == '5')
		 {
			 //alert($('#agency_yn').val());
			 //$("#b_agency_name").show();
		 }
		 else{
			$("#chk").hide();
		 	$("#b_agency_name").hide();
		 	$("#ct_name").attr("required","true");	
		 }
    });
	
	function hideShow(t)
	{
		if($('#agency_yn').val() == '5')
		{
			$("#chk").show();
			$("#b_agency_name").show();
			$("#b_contact_name").hide();
			$("#ct_name").removeAttr("required");
			$("#ag_name").attr("required","true");
		 	//$("#ct_name").attr("required","false");
		}
		else
		{
			$("#chk").hide();
			$("#b_agency_name").show();
			$("#b_contact_name").show();
			//$("#ag_name").attr("required","false");
		 	$("#ct_name").attr("required","true")
			$("#ag_name").removeAttr("required");
			$("#ag_name").val("");
		}
	}
	function showChange()
	{
		$("#pic").show();
		$("#btnchange").hide();
		return false;
	}
	
	  
	  
	   $(function () {
    $("#uploadFile").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = $("#imagePreview");
            dvPreview.html("");
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function () {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = $("<img />");
                        img.attr("style", "height:100px;width: 100px");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    alert(file[0].name + " is not a valid image file.");
                    dvPreview.html("");
                    return false;
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
});
	
</script>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Edit Channel</span> </div>
  </div>
  <div class="portlet-body form">
  <?php

                $form = array(
                    'class' 			=> '',
                    'id'				=> 'form',
                    'method'			=> 'post'
                );

                echo form_open_multipart('dashboard/edit_channel',$form);

                ?>
  <input type="hidden" name="channel_id" value="<?php echo $channel->channel_id; ?>" />  
    <div class="form-body">
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <div class="row">
      	<div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="ct_name" name="channel_name"  onkeypress="return onlyLtrs(event, this);" value="<?php echo $channel->channel_name;?>" placeholder="Channel Name *"/>
          <label></label>
          <span class="help-block">Channel Name *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="ct_name" name="channel_contact_name"  value="<?php echo $channel->channel_contact_name;?>" onkeypress="return onlyLtrs(event, this);" placeholder="Contact Name *">
          <label></label>
          <span class="help-block">Contact Name *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="channel_address" required="required" value="<?php echo $channel->channel_address;?>" placeholder="Address *">
              <label></label>
              <span class="help-block">Address *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="channel_contact" value="<?php echo $channel->channel_contact;?>" required="required" onkeypress="return onlyNos(event, this);" maxlength="10" placeholder="Mobile No *">
          <label></label>
          <span class="help-block">Mobile No *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  type="email" autocomplete="off" class="form-control" id="form_control_1" name="channel_email" value="<?php echo $channel->channel_email;?>" placeholder="Email">
          <label></label>
          <span class="help-block">Email</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="form_control_1" name="channel_website" value="<?php echo $channel->channel_website;?>" placeholder="Website">
          <label></label>
          <span class="help-block">Website</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="form_control_1" name="channel_pan" value="<?php echo $channel->channel_pan;?>" >
          <label></label>
          <span class="help-block">Pan Card No.</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" autocomplete="off" class="form-control" id="form_control_1" name="channel_bank_acc_no" value="<?php echo $channel->channel_bank_acc_no;?>"  onkeypress="return onlyNos(event, this);" placeholder="Bank Account No.">
          <label></label>
          <span class="help-block">Bank Account No.</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="channel_bank_ifsc" value="<?php echo $channel->channel_bank_ifsc;?>" placeholder="IFSC Code">
          <label></label>
          <span class="help-block">IFSC Code</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text"  autocomplete="off" class="form-control" id="form_control_1" name="channel_commission" value="<?php echo $channel->channel_commission;?>" placeholder="Commission %">
          <label></label>
          <span class="help-block">Commission %</span> </div>
        </div>
        <div class="col-md-3">
        <div class="form-group form-md-line-input uploadss">
          <label>Photo Proof</label>
          <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>upload/channel/image/<?php if( $channel->channel_photo_thumb== '') { echo "no_images.png"; } else { echo $channel->channel_photo_thumb; }?>" alt=""/> </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
            <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
              <input  type="file" id="uploadFile" name="image_photo" />
              </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
          </div>
        </div>
        </div>
		<div class="col-md-3">
        <div class="form-group form-md-line-input uploadss">
          <label>other Proof or Document</label>
          <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> 
			<?php 
			   $channel_doc=$channel->channel_doc;
			   $channel_ext=explode(".", $channel_doc);
			   if($channel_ext[1]=="jpg" || $channel_ext[1]=="png" || $channel_ext[1]=="gif" || $channel_ext[1]=="jpeg" ){
			?>
			<img src="<?php echo base_url();?>upload/channel/image/<?php if( $channel->channel_doc== '') { echo "no_images.png"; } else { echo $channel->channel_doc; }?>" alt=""/> 
			<?php 
			   }else{
				   echo "<pre>";
				   if( $channel->channel_doc!= '') { echo $channel->channel_doc; }else{ echo "";}
			   }
			   ?>
			</div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
            <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
              <input  type="file" id="uploadFile1" name="doc_photo" />
              </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
          </div>
        </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn blue">Submit</button>
    </div>
    <?php form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>
