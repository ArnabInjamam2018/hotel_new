<!-- BEGIN PAGE CONTENT-->
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$(document).ready(function(){
    $("form").submit(function(){
        
		$.valid_from = Date.parse($('#c_valid_from').val());
		$.valid_upto = Date.parse($('#c_valid_upto').val());
		$.renewal = Date.parse($('#c_renewal').val());
		$.value = $.now();
		
		if($.valid_from > $.value)
		{
			//alert('Valid Date Should Be Lesser Than The System Date');
			//alert($.valid_upto - $.value);
			swal({
                                title: "Valid-from date shouldn't be greater than the system date!",
                                text: "",
                                type: "warning",
								confirmButtonColor: "#F27474"
                            })
			return false;
		}else if($.valid_upto < $.value)+
		{
			//alert('Valid Upto Date Should Be Greater Than The Todays Date');
			swal({
                                title: "Valid-upto date should be greater than the System Date!",
                                text: "",
                                type: "warning",
								confirmButtonColor: "#F27474"
                            })
			return false;
		}
		else if ($.renewal < $.value)
		{
			//alert('Renewal Date Should Be Greater Than The Todays Date');
			swal({
                                title: "Renewal date should be greater than the System Date!",
                                text: "",
                                type: "warning",
								confirmButtonColor: "#F27474"
                            })
			return false;
		}
		else
		{
			return true;
		}
		
    	});
	});
	
</script>
<?php if($this->session->flashdata('err_msg')):?>
            <div class="form-group">
                <div class="col-md-12 control-label">
                    <div class="alert alert-danger alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong><?php echo $this->session->flashdata('err_msg');?></strong>
                    </div>
                </div>
            </div>
            <?php endif;?>
            <?php if($this->session->flashdata('succ_msg')):?>
            <div class="form-group">
                <div class="col-md-12 control-label">
                    <div class="alert alert-success alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong><?php echo $this->session->flashdata('succ_msg');?></strong>
                    </div>
                </div>
            </div>
            <?php endif;?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-pin"></i>
            <span class="caption-subject bold uppercase"> Edit Certificate</span>
        </div>

    </div>
    <div class="portlet-body form">

            <?php

            $form = array(
            'id'				=> 'form',
            'method'			=> 'post'
            );

            echo form_open_multipart('dashboard/edit_compliance',$form);
				//print_r($value); exit;
            foreach($value as $c_edit)
                        {
							
			?>
            <div class="form-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <select class="form-control bs-select" id="form_control_2" name="c_valid_for">
                            <option value="" disabled="disabled" selected="selected">Valid for</option>
                            <option value="Self" <?php if(isset($c_edit->c_valid_for)){ if($c_edit->c_valid_for=='Self'){ echo "selected";}}?>>Self </option>
                            <option value="Hotel" <?php if(isset($c_edit->c_valid_for)){ if($c_edit->c_valid_for=='Hotel'){ echo "selected";}}?>>Hotel</option>
                        </select>
                        <label></label>
                        <span class="help-block">Valid for *</span>
                    </div>
                </div>
                <input type="hidden" name="compliance_id" value="<?php echo $c_edit->c_id; ?>" />
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <select class="form-control bs-select" id="form_control_2">
						<option value="" disabled>Hotel Name</option>
                           <?php $hotels=$this->dashboard_model->all_hotels();
                            if(isset($hotels) && $hotels){
                                foreach($hotels as $hotel){

                                    ?>
              <option value="<?php echo $hotel->hotel_id ?>" <?php if(isset($c_edit->hotel_id)){ if($hotel->hotel_id==$c_edit->hotel_id){ echo "selected";}}?>><?php echo $hotel->hotel_name ?></option>
              <?php }} ?>
                        </select>
                        <label></label>
                        <span class="help-block">Hotel Name *</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <input type="text" autocomplete="off" class="form-control" name="c_name" value="<?php echo $c_edit->c_name?>" required="required" placeholder="Certificate Name *">
                        <label></label>
                        <span class="help-block">Certificate Name *</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <input type="text" autocomplete="off" class="form-control" name="c_authority" required="required" value="<?php echo $c_edit->c_authority?>" placeholder="Certificate Authority *">
                        <label></label>
                        <span class="help-block">Certificate Authority *</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <input type="text" value="<?php echo $c_edit->c_owner?>" autocomplete="off" class="form-control" name="c_owner" required="required" placeholder="Certificate Owner *">
                        <label></label>
                        <span class="help-block">Certificate Owner *</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <select class="form-control bs-select" onchange="other(this.value)" id="form_control_2" name="c_type">
                            <option value="Pollution">Pollution</option>
                            <option value="Fire">Fire</option>
                            <option value="Trade">Trade</option>
                            <option value="Food">Food</option>
                            <option value="Income Tax">Income Tax</option>
                            <option value="Others">Others </option>
                        </select>
                        <label></label>
                        <span class="help-block">Certificate Type *</span>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group form-md-line-input">
                        <textarea  class="form-control" name="c_description" required="required"><?php echo $c_edit->c_description?></textarea>
                        <label></label>
                        <span class="help-block">Description *</span>
                    </div>
                </div>
               
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <select class="form-control bs-select" id="form_control_2" name="c_importance">
                           
                            <option value="y" <?php if($c_edit->c_importance=="y"){ echo "selected";}?>>Low</option>
                            <option value="o" <?php if($c_edit->c_importance=="o"){ echo "selected";}?>>Medium</option>
                            <option value="r" <?php if($c_edit->c_importance=="r"){ echo "selected";}?>>High</option>
                        </select>
                        <label></label>
                        <span class="help-block">Importance *</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <input value="<?php echo $c_edit->c_valid_from ;?>" type="text" autocomplete="off" required="required" name="c_valid_from" class="form-control date-picker"  id="c_valid_from" placeholder="Valid From *">
                         <label></label>
                         <span class="help-block">Valid From *</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <input value="<?php echo $c_edit->c_valid_upto ;?>" type="text" autocomplete="off" required="required" name="c_valid_upto" class="form-control date-picker" id="c_valid_upto" placeholder="Valid Up to *">
                        <label></label>
                        <span class="help-block">Valid Up to *</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <input value="<?php echo $c_edit->c_renewal?>" type="text" autocomplete="off" name="c_renewal" required="required" class="form-control date-picker" id="c_renewal" placeholder="Renewal Date *">
                        <label></label>
                        <span class="help-block">Renewal Date *</span>
                    </div>
                </div>
				<div class="col-md-12">
				<div class="col-md-3">
                            <div class="form-group form-md-line-input uploadss">
                                <label>Primary Notification period (Days) <span class="required">*</span></label>
                                <input name="c_primary" class="knob" data-width="40%" value="<?php echo $c_edit->c_primary_notif_period; ?>"  data-rotation="clockwise">
    
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-md-line-input uploadss">
                            <label>Secondary  Notification period (Hours) <span class="required">*</span></label>
                            <input name="c_secondary" class="knob" data-width="40%" data-fgColor="#66EE66" data-rotation="clockwise" value="<?php echo $c_edit->c_secondary_notif_period; ?>">

                            </div>
                        </div>
                        
					
               
                    
                        <div class="col-md-3">
                            <div class="form-group form-md-line-input uploadss">
                                <label>Certificate Attachment</label>   
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px;">
                                        <img src="<?php echo base_url();?>upload/certificate/thumb/cer_1/<?php if( $c_edit->c_file_thumb== '') { echo "no_images.png"; } else { echo $c_edit->c_file_thumb; }?>" alt=""/>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                    </div>
                                    <div>
                                        <span class="btn default btn-file">
                                        <span class="fileinput-new">
                                        Select image </span>
                                        <span class="fileinput-exists">
                                        Change </span>
                                        <?php echo form_upload('image_compliance');?>
                                        </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                        Remove </a>
                                    </div>
                                </div>                        
                                
                                <?php //echo form_upload('image_compliance');?>
                            </div>
                        </div>
						<div class="col-md-3">
                            <div class="form-group form-md-line-input uploadss">
                                <label>Certificate 2 Attachment</label>   
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px;">
                                        <img src="<?php echo base_url();?>upload/certificate/thumb/cer_2/<?php if( $c_edit->c_file_2_thumb== '') { echo "no_images.png"; } else { echo $c_edit->c_file_2_thumb; }?>" alt=""/>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                    </div>
                                    <div>
                                        <span class="btn default btn-file">
                                        <span class="fileinput-new">
                                        Select image </span>
                                        <span class="fileinput-exists">
                                        Change </span>
                                        <?php echo form_upload('image_compliance2');?>
                                        </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                        Remove </a>
                                    </div>
                                </div>                        
                                
                                <?php //echo form_upload('image_compliance');?>
                            </div>
                        </div>
                        
                    
              </div>
            </div>                    
            </div>
            <div class="form-actions right">
                <button type="submit" class="btn blue" >Submit</button>
            </div>
                        <?php }
                        form_close(); ?>
        </div>

</div>
<script>
function other(id){
	
	if(id=='Others'){
		$('#Certificate').show();
	}
	else{
		$('#Certificate').hide();
	}
}
;
</script>