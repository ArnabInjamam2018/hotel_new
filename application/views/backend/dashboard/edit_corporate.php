<!-- BEGIN PAGE CONTENT-->
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Edit Corporate</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/update_corporate',$form);

                        ?>
    <div class="form-body"> 
      <!-- 17.11.2015-->
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong>		  </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <!-- 17.11.2015-->
      <?php if(isset($corporate))
                                {
                                    foreach ($corporate as$value) {
                                            # code...
                                              
                              ?>
      <div class="row">
      	<div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="hidden" name="c_id" value="<?php echo $value->hotel_corporate_id;?>">
          <input autocomplete="off" type="text" value="<?php echo $value->name?>"class="form-control" id="form_control_1" name="name" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Name *">
          <label></label>
          <span class="help-block">Name *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" value="<?php echo $value->address?>" type="text" class="form-control" id="form_control_1" name="address" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Address *">
          <label></label>
          <span class="help-block">Address *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" value="<?php echo $value->g_contact_no?>" type="text" class="form-control" id="mobile" name="g_contact_no"  required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" placeholder="Mobile No *">
          <label></label>
          <span class="help-block">Mobile No *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" value="<?php echo $value->industry?>" type="text" class="form-control" id="form_control_1" name="industry" placeholder="Industry *">
          <label></label>
          <span class="help-block">Industry *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" value="<?php echo $value->no_of_emp?>" type="text" class="form-control" id="form_control_1" name="no_of_emp" placeholder="No Of Employees *">
          <label></label>
          <span class="help-block">No Of Employees *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" value="<?php echo $value->contract_discount?>" type="text" class="form-control" id="form_control_1" name="contract_discount" placeholder="Contract Discount *">
          <label></label>
          <span class="help-block">Contract Discount *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" value="<?php echo $value->contract_end_date?>" type="text" class="form-control date-picker" id="form_control_1" name="contract_end_date" placeholder="Contract End Date *">
          <label></label>
          <span class="help-block">Contract End Date *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" value="<?php echo $value->no_of_booking?>" type="text" class="form-control" id="form_control_1" name="no_of_booking" placeholder="No Of Booking">
          <label></label>
          <span class="help-block">No Of Booking</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" value="<?php echo $value->total_transaction?>" type="text" class="form-control" id="form_control_1" name="total_transaction" placeholder="Total Tansaction">
          <label></label>
          <span class="help-block">Total Tansaction</span> </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn blue" >Submit</button>
    </div>
    <?php 
    }}
form_close(); ?>
    <!-- END CONTENT --> 
    
  </div>
</div>
<script>
   function check_corporate(value){

       if(value =="corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
       }else{
           document.getElementById("c_name").style.display="none";
           document.getElementById("c_des").style.display="none";
       }
   }
    function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
</script> 
