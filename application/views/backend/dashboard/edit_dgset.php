<!-- BEGIN PAGE CONTENT-->
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>

<div class="row ds">
  <div class="col-md-12">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green"> <i class="icon-pin font-green"></i> <span class="caption-subject bold uppercase"> Edit DG set</span> </div>
      </div>
      <div class="portlet-body form">
        <?php

                            $form = array(
                                'class' 			=> 'form-body',
                                'id'				=> 'form',
                                'method'			=> 'post',								
                            );
							
							

                            echo form_open_multipart('dashboard/update_dgset',$form);

                            ?>
        <div class="form-body"> 
          <!-- 17.11.2015-->
          <?php if($this->session->flashdata('err_msg')):?>
          <div class="form-group">
            <div class="col-md-12 control-label">
              <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
            </div>
          </div>
          <?php endif;?>
          <?php if($this->session->flashdata('succ_msg')):?>
          <div class="form-group">
            <div class="col-md-12 control-label">
              <div class="alert alert-success alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
            </div>
          </div>
          <?php endif;?>
          <!-- 17.11.2015-->
          <div class="row">
            <?php 
										if(isset($d) && $d)
										{
											{
										
									?>
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <input type="hidden" name="dgset_id" value="<?php echo $d->dgset_id;?>">
              <input  autocomplete="off" type="text" class="form-control focus" id="form_control_1" name="gen_name" onkeypress=" return onlyLtrs(event, this);" required="required" value="<?php echo $d->gen_name;?>">
              <label>Generator Name <span class="required">*</span></label>
              <span class="help-block">Enter Generator Name...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <input value="<?php echo $d->asset_code;?>" autocomplete="off" type="text" class="form-control focus" id="form_control_1" name="asset_code" required="required" >
              <label>Asset Code <span class="required">*</span></label>
              <span class="help-block">Enter Asset Code of the product...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <input value="<?php echo $d->make;?>" autocomplete="off" type="text" class="form-control focus" id="make" name="make"  required="required" onblur="fetch_all_address()" onkeypress=" return onlyLtrs(event, this);">
              <label>Make<span class="required">*</span></label>
              <span class="help-block">Enter the brand...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <input value="<?php echo $d->model;?>" autocomplete="off" type="text" class="form-control focus" id="model" name="model" value=""  onkeypress=" return onlyLtrs(event, this);" 
                                        required= "required" />
              <label>Model<span class="required">*</span></label>
              <span class="help-block">Enter the model no...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <select class="form-control focus"  name="ratings" required >
                <option value="">---Select Ratings---</option>
				<?php foreach($rating as $rat) 
				{
					$value=$rat->Rating." ".$rat->Unit;?>
				<option value="<?php echo $value;?>" <?php if($value == $d->ratings ) echo "selected"; ?>><?php echo $value; ?></option>
				<?php } ?>
               <!--- <option value="7.5 Kva">7.5 Kva</option>
                <option value="10.5 Kva">10.5 Kva</option>
                <option value="12.5 Kva">12.5 Kva</option>
                <option value="15 Kva">15 Kva</option>
                <option value="20 Kva">20 Kva</option>
                <option value="25 Kva">25 Kva</option>
                <option value="35 Kva">35 Kva</option>
                <option value="45 Kva">45 Kva</option>
                <option value="50 Kva">50 Kva</option>
                <option value="75 Kva">75 Kva</option>
                <option value="100 Kva">100 Kva</option>
                <option value="125 Kva">125 Kva</option>
                <option value="150 Kva">150 Kva</option>
                <option value="200 Kva">200 Kva</option>
                <option value="300 Kva">300 Kva</option>
                <option value="350 Kva">350 Kva</option>
                <option value="400 Kva">400 Kva</option>---->
              </select>
              <label>Ratings <span class="required">*</span></label>
              <span class="help-block">Select Ratings...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <select class="form-control focus"  name="fuel" required >
                <option value="">---Select Fuel---</option>
                <?php foreach($master_log as $mas) 
			  { if($mas->fuel_name == $d->fuel ) $sel="selected"; else $sel=""; 
		echo "<option value='".$mas->fuel_name."'".$sel.">".$mas->fuel_name."</option>";
			  }
		?>
              </select>
              <label>Fuel <span class="required">*</span></label>
              <span class="help-block">Select Fuel...</span> </div>
         <!------ <div style="display:none;" class="form-group form-md-line-input form-md-floating-label col-md-6">            
            <select onchange="return check_Cylinders(this.value);" class="form-control focus" id="form_control_2" name="engine_type" required="required">
              <option value="">--Select a Engine Type--</option>
              <option value="Cylinders" >Cylinders</option>
              <option value="Rotation">Rotation</option>
              <option value="Design">Design</option>
            </select>
            <label>Engine Details <span class="required">*</span></label>
            <span class="help-block">Enter Engine Details Type...</span> </div>----->
          <div style="/*display: none;*/" id="c_choice" class="form-group form-md-line-input form-md-floating-label col-md-6">            
            <select class="form-control focus" name="cylinder_choice">
              <option value="4" <?php if ($d->cylinders='4') echo "selected";?> >4</option>
              <option value="6" <?php if ($d->cylinders='6') echo "selected";?> >6</option>
              <option value="8" <?php if ($d->cylinders='8') echo "selected";?> >8</option>
              <option value="12" <?php if ($d->cylinders='12') echo "selected";?> >12</option>
            </select>
            <label>Select Number of Cylinders <span class="required">*</span></label>
          </div>
          <div style="/*display: none;*/" id="r_choice" class="form-group form-md-line-input form-md-floating-label col-md-6">            
            <select class="form-control focus" name="Rotation_choice">
              <option value="CW" <?php if ($d->rotation='CW') echo "selected";?> >CW</option>
              <option value="CWW" <?php if ($d->rotation='CWW') echo "selected";?>>CWW</option>
            </select>
            <label>Select Rotation <span class="required">*</span></label>
          </div>
          <div style="/*display: none*/" id="d_choice" class="form-group form-md-line-input form-md-floating-label col-md-6">
            <select class="form-control" name="Design_choice">
              <option value="Water Cooled" <?php if ($d->cooling='Water Cooled') echo "selected";?> > Water Cooled</option>
              <option value="Direct Inject" <?php if ($d->cooling='Direct Inject') echo "selected";?>> Direct Inject</option>
            </select>
            <label>Design <span class="required">*</span></label>
          </div>
          
          <!--<div class="form-group form-md-line-input form-md-floating-label col-md-4">
										<input type="text" class="form-control" id="form_control_1" >
										<label>Coming From <span class="required">*</span></label>
										<span class="help-block">Enter Coming From...</span>
									</div>
									<div class="form-group form-md-line-input form-md-floating-label col-md-6">
									
										<input type="text" class="form-control" id="form_control_1" >
										<label>Going To <span class="required">*</span></label>
										<span class="help-block">Enter Going To...</span>
									</div>-->
          
          <div class="form-group form-md-line-input form-md-floating-label col-md-6">
            <input  autocomplete="off" type="text" class="form-control focus" id="form_control_1" name="alternator_make" value="<?php echo $d->alternator_make?>" required="required">
            
            <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
            <label> Alternator Make<span class="required">*</span></label>
            <span class="help-block">Enter Alternator make...</span> </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-6">
            <input value="<?php echo $d->fuel_consumption?>" autocomplete="off" type="text" class="form-control focus" id="form_control_1" name="fuel_consumption" required="required" >
            <label>Fuel Consumption (Litre per hour) <span class="required">*</span></label>
            <span class="help-block">Fuel Consumption (Litre per hour)</span> </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-6">
            <select class="form-control focus" id="form_control_2" name="status" required>
              <option value="Active" <?php if ($d->status='Active') echo "selected";?> >Active</option>
              <option value="Faulty"<?php if ($d->status='Faulty') echo "selected";?>>Faulty</option>
              <option value="Under Maintanance" <?php if ($d->status='Under Maintanance') echo "selected";?>>Under Maintanance</option>
            </select>
            <label for="form_control_2">Status <span class="required">*</span></label>
          </div>
          <div id="g_anniv" class="form-group form-md-line-input form-md-floating-label col-md-6">
            <input value="<?php echo $d->amc?>" autocomplete="off" type="text" class="form-control date-picker focus" id="form_control_1" name="amc" >
            <label>Next AMC Date <span class="required">*</span></label>
            <span class="help-block">Enter Annual Maintanance Charge...</span> </div>
			<input type="hidden" name="image_record" id="image_record" value="<?php echo $d->img; ?>" />
			 
			 
			 
			 <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
				<?php if($d->img)
				{?>
                    <img src="<?php echo base_url()."upload/dg_set/".$d->img;?>" alt="DG_SET Image"/>
				<?php }
				else
				{?>
			 <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/>
				<?php }?>
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                </div>
                <div>
                    <span class="btn default btn-file">
                    <span class="fileinput-new">
                    Select image </span>
                    <span class="fileinput-exists">
                    Change </span>
                    <?php echo form_upload('dg_image');?>
                    </span>
                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                    Remove </a>
                </div>
              </div>
          
          <!--<div class="form-group form-md-line-input form-md-floating-label col-md-4">
									
										<input type="text" class="form-control" id="form_control_1" >
										<label id="form_control_1">Others <span class="required">*</span></label>
										<span class="help-block">Enter Others Option...</span>
									</div>--> 
          <!-- <div class="form-group form-md-line-input form-md-floating-label col-md-6 dwn">
									<p><strong>Upload Photo</strong><span class="required">*</span></p>
			<!--<div action="http://localhost/hotel//assets/dashboard/assets/global/plugins/dropzone/upload.php" class="dropzone" id="my-dropzone">
					</div>--> <!-- 17.11.2015 --> 
          <!--<?php echo form_upload('image_photo');?>--> 
          <!-- 17.11.2015 --> 
          </div>
        </div>
		
        <div class="form-group form-md-line-input form-md-floating-label col-md-6 rght"> 
          <!-- <p><strong>Upload ID Proof</strong><span class="required">*</span></p>
                                      <?php echo form_upload('image_idpf');?> --> 
          <!--<div action="http://localhost/hotel//assets/dashboard/assets/global/plugins/dropzone/upload.php" class="dropzone" id="my-dropzone">
					</div>--> 
          <!-- 17.11.2015 --> 
          
          <!-- 17.11.2015 --> 
        </div>
        <div class="form-actions right">
          <button type="submit" class="btn blue" >Submit</button>
          <!-- 18.11.2015  -- onclick="return check_mobile();" -->
          <button  type="reset" class="btn default">Reset</button>
        </div>
        <?php }}form_close(); ?>
      </div>
      <!-- END CONTENT --> 
    </div>
  </div>
</div>
<script>
   function check_Cylinders(value){
   		
       if(value =="Cylinders"){

           document.getElementById("c_choice").style.display="block";
       }else{
           document.getElementById("c_choice").style.display="none";
       }
      	if(value =="Rotation"){

           document.getElementById("r_choice").style.display="block";
       }else{
           document.getElementById("r_choice").style.display="none";
       }
       if(value =="Design"){

           document.getElementById("d_choice").style.display="block";
       }else{
           document.getElementById("d_choice").style.display="none";
       }
   }
   
</script> 