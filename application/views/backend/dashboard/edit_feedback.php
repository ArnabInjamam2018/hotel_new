<!-- BEGIN PAGE CONTENT-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/dashboard/js/star-rating.css"/>
<script>
$(document).ready(function() {
$('input.typeahead-devs').typeahead({
name: 'gName',
//local:['Sunday']
remote : '<?php echo base_url(); ?>dashboard/get_guest_name/%QUERY'
});
});		  

</script>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase">Edit Feedback</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
            $form = array(
                'class' 			=> '',
                'id'				=> 'form',
                'method'			=> 'post',
            );
            echo form_open_multipart('dashboard/edit_feedback',$form);
            ?>
    <div class="form-body">
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <div class="row">
        <?php
                        if(isset($fdback)){
                            
                            foreach($fdback as $fd){
                            
                        ?>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="text" value="<?php echo  $hotel_name->hotel_name; ?>" class="form-control input-sm" readonly="readonly" placeholder="Hotel Name">
          <label></label>
          <span class="help-block">Hotel Name</span>
        </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" value="<?php 
                      if(isset($_GET['fdback_id'])){
                          //echo $_GET['bID'];
                          
                          if($fd->booking_id==0){
                          echo "N/A";
                      }	
                      else{
                          echo $fd->booking_id;
                          
                      }
                      } 
                    ?>" class="form-control input-sm"  readonly="readonly" placeholder="Booking Id">
              <label></label>
              <span class="help-block">Booking Id</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <?php 
                        if(isset($_GET['bID'])){
                            echo '<input type="text" name="gName" class="form-control input-sm" placeholder="Please Enter Name" value="' .$guest->g_name.' " readonly="readonly" placeholder="Guest Name">';
                            
                            
                        } else {
                            echo '<input type="text" name="gName" class="form-control input-sm" placeholder="Please Enter Name" value="'.$fd->guest_name.'" readonly="readonly" placeholder="Guest Name">';
                        }
                    ?>
              <label></label>
              <span class="help-block">Guest Name</span>
            </div>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Ease of booking</label>
          <input type="hidden" name="f_id" value="<?php echo $fd->id ?>">
          <input id="input-2c2" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w1" value="<?php echo $fd->ease_booking; ?>" required="required" >
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Reception</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w2" value="<?php echo $fd->reception; ?>" >
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Staff</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w3" value="<?php echo $fd->staff; ?>" >
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Cleanliness</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w4" value="<?php echo $fd->cleanliness; ?>" >
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Ambience</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w5" value="<?php echo $fd->ambience; ?>" >
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Sleep Quality</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w6" value="<?php echo $fd->sleep_quality; ?>" >
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Room Quality</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w7" value="<?php echo $fd->room_quality; ?>" >
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Food Quality</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w8" value="<?php echo $fd->food_quality; ?>" >
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Environment Quality</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w9" value="<?php echo $fd->env_quality; ?>" >
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Service</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w10" value="<?php echo $fd->service; ?>" >
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Package</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w11" value="<?php echo $fd->package; ?>" >
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Extra Amenities</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w12" value="<?php echo $fd->extra; ?>" >
        </div>
        <div class="form-group form-md-radios form-md-line-input col-md-4">
          <label>Will you come back again?</label>
          <div class="md-radio-inline">
            <div class="md-radio">
              <input type="radio" id="radio1" name="or1" value="1" class="md-radiobtn" <?php if($fd->come_back == '1') { echo 'checked="checked"';}?> >
              <label for="radio1"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio2" name="or1" class="md-radiobtn" value="2" <?php if($fd->come_back == '2') { echo 'checked="checked"';}?>>
              <label for="radio2"> <span></span> <span class="check"></span> <span class="box"></span> Maybe </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio3" name="or1" class="md-radiobtn" value="0" <?php if($fd->come_back == '0') { echo 'checked="checked"';}?>>
              <label for="radio3"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
            </div>
          </div>
        </div>
        <div class="form-group form-md-radios form-md-line-input col-md-4">
          <label>Will you refer to a friend?</label>
          <div class="md-radio-inline">
            <div class="md-radio">
              <input type="radio" id="radio4" name="or2" value="1" class="md-radiobtn" <?php if($fd->refer_friend == '1') { echo 'checked="checked"';}?> >
              <label for="radio4"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio5" name="or2" class="md-radiobtn" value="2" <?php if($fd->refer_friend == '2') { echo 'checked="checked"';}?>>
              <label for="radio5"> <span></span> <span class="check"></span> <span class="box"></span> Maybe </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio6" name="or2" class="md-radiobtn" value="0" <?php if($fd->refer_friend == '0') { echo 'checked="checked"';}?>>
              <label for="radio6"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
            </div>
          </div>
        </div>
        <div class="form-group form-md-radios form-md-line-input col-md-4">
          <label>Was the price reasonable?</label>
          <div class="md-radio-inline">
            <div class="md-radio">
              <input type="radio" id="radio7" name="or3" value="1" class="md-radiobtn" <?php if($fd->reasonable_cost== '1') { echo 'checked="checked"';}?> >
              <label for="radio7"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio8" name="or3" class="md-radiobtn" value="2" <?php if($fd->reasonable_cost== '2') { echo 'checked="checked"';}?>>
              <label for="radio8"> <span></span> <span class="check"></span> <span class="box"></span> Maybe </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio9" name="or3" class="md-radiobtn" value="0" <?php if($fd->reasonable_cost== '0') { echo 'checked="checked"';}?>>
              <label for="radio9"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
            </div>
          </div>
        </div>
        <div class="form-group form-md-radios form-md-line-input col-md-4">
          <label>Do you have a suggestion?</label>
          <div class="md-radio-inline">
            <div class="md-radio">
              <input type="radio" id="radio10" name="or4" value="1" class="md-radiobtn" <?php if($fd->suggestion== '1') { echo 'checked="checked"';}?> >
              <label for="radio10"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio11" name="or4" class="md-radiobtn" value="0" <?php if($fd->suggestion== '0') { echo 'checked="checked"';}?>>
              <label for="radio11"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
            </div>
          </div>
        </div>
        <div class="form-group form-md-radios form-md-line-input col-md-4">
          <label>Add to Social Media</label>
          <div class="md-radio-inline">
            <div class="md-radio">
              <input type="radio" id="radio12" name="or5" value="1" class="md-radiobtn" <?php if($fd->add_social_media== '1') { echo 'checked="checked"';}?> >
              <label for="radio12"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio13" name="or5" class="md-radiobtn" value="0" <?php if($fd->add_social_media== '0') { echo 'checked="checked"';}?>>
              <label for="radio13"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
            </div>
          </div>
        </div>
        <div class="col-md-12">
        <div class="form-group form-md-line-input">
          <textarea autocomplete="off" row="3" type="text" class="form-control input-sm" id="form_control_1" name="comment" placeholder="Comment"><?php echo $fd->comment;?></textarea>              
          <label></label>
          <span class="help-block">Comment</span>
        </div>
        </div>
      </div>
      <?php } }?>
      </div>
      <div class="form-actions right">
        <button type="submit" class="btn blue">Submit</button>
      </div>
    
    <?php form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>
