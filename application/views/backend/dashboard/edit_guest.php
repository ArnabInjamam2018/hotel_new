<!-- BEGIN PAGE CONTENT-->
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;

	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			$('#g_country').val(data.country);
			$('#g_state').val(data.state);
			$('#g_city').val(data.city);
		}

	});
}

</script>
<!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('err_msg');?></strong>
  </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<!-- 17.11.2015-->
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Edit Guest Nicely</span> </div>
      </div>
      <div class="portlet-body form">
        <?php

                            $form = array(
                                'class' 			=> '',
                                'id'				=> 'form',
                                'method'			=> 'post'
                            );
							
							

                            echo form_open_multipart('dashboard/edit_guest',$form);
							
                            ?>
        <div class="form-body">
          <div class="row">
           <div class="col-md-12">
			<h3 class="form-heading">Guest Details</h3>
		   </div>
            <?php 
								if (isset($value))

								{ 
										
									foreach ($value as $g_edit) { 
										//echo $g_edit->cp_discount_rule_id;
										
									?>
            <div class="col-md-4">
            <div class="form-group form-md-line-input" id="gtype">
              <select onchange="check_corporate(this.value)" class="form-control bs-select required" id="form_control_2" name="g_type" required="required" >
                <option value="" <?php if($g_edit->g_type==""){echo 'selected="selected"';} ?>>select guest type</option>
                <option value="Family" <?php if($g_edit->g_type=="Family"){echo 'selected="selected"';} ?>>Family</option>
                <option value="Bachelor" <?php if($g_edit->g_type=="Bachelor"){echo 'selected="selected"';} ?> >Bachelor</option>
				<option value="Tourist"  <?php if($g_edit->g_type=="Tourist"){echo 'selected="selected"';} ?>>Tourist</option>
            <!--<option value="Sr_citizen" <?php if($g_edit->g_type=="Sr_citizen"){echo 'selected="selected"';} ?>>Sr Citizen</option>-->
                <option value="Corporate" <?php if($g_edit->g_type=="Corporate"){echo 'selected="selected"';} ?>>Corporate</option>
                <option value="VIP" <?php if($g_edit->g_type=="VIP"){echo 'selected="selected"';} ?>>VIP</option>
                <option value="Government_official" <?php if($g_edit->g_type=="Government_official"){echo 'selected="selected"';} ?>>Government Official</option>
                <option value="Military"  <?php if($g_edit->g_type=="Military"){echo 'selected="selected"';} ?>>Military </option>
                <option value="Internal_staff" <?php if($g_edit->g_type=="Internal_staff"){echo 'selected="selected"';} ?>>Internal Staff</option>
              </select>
              <input type="hidden" id="hidVal" value="<?php echo $g_edit->g_type;?>">
              <label></label>
              <span class="help-block">Guest Type *</span> </div>
            </div>
            <div class="col-md-4" style="display: none" id="c_name">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select"  name="c_name"  >
                <option value="">Select Name</option>
                <?php 
								if (isset($corporate) && $corporate)
								{
									
								foreach ($corporate as $value){ ?>
                <option value="<?php echo $value->hotel_corporate_id ?>" <?php if($g_edit->corporate_id==$value->hotel_corporate_id){echo 'selected="selected"';}?>><?php echo $value->name ?></option>
                <?php }}?>
              </select>
              <label></label>
              <span class="help-block">Select Company Name...</span> </div>
            </div>
            <div class="col-md-4" style="display: none" id="type">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="type"  value="<?php echo $g_edit->type; ?>" onkeypress=" return onlyLtrs(event, this);" placeholder="Type *">
              <label></label>
              <span class="help-block">Type *</span> </div>
            </div>
            <div class="col-md-4" id="military" style="display: none">
            <div class="form-group form-md-line-input">
              <label class="col-md-6 control-label" for="form_control_1" style="padding-top: 5px;"> Retired?<span class="required" id="b_contact_name">*</span></label>
              <div class="col-md-6">
                <div class="md-radio-inline" style="margin: 8px 0 1px;">
                  <div class="md-radio">
                    <input type="radio" id="radio51" class="md-check" name="retired" value="yes"<?php if($g_edit->retired=="yes"){echo " checked ";}; ?>>
                    <label for="radio51"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
                  </div>
                  <div class="md-radio">
                    <input type="radio" id="radio50" class="md-check" name="retired" value="no" <?php if($g_edit->retired=="no"){echo " checked ";}; ?>>
                    <label for="radio50"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="col-md-4" style="display: none" id="class">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text"  value="<?php echo $g_edit->class; ?>" class="form-control" id="form_control_1" name="class" onkeypress=" return onlyLtrs(event, this);"  placeholder="Class *">
              <label></label>
              <span class="help-block">Class *</span> </div>
            </div>
            <div class="col-md-4" style="display: none" id="f_size">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control"  name="f_size" value="<?php echo $g_edit->f_size;?>" maxlength="10" placeholder="Family Size *">
              <label></label>
              <span class="help-block">Family Size *</span> </div>
            </div>
            <div class="col-md-4" style="display: none" id="rank">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" value="<?php echo $g_edit->rank;?>" name="rank" onkeypress=" return onlyLtrs(event, this);" placeholder="Rank *">
              <label></label>
              <span class="help-block">Rank *</span> </div>
            </div>
            <div class="col-md-4" style="display: none" id="dep">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" value="<?php echo $g_edit->dep;?>"  name="dep" onkeypress=" return onlyLtrs(event, this);" placeholder="Department *">
              <label></label>
              <span class="help-block">Department *</span> </div>
            </div>
            <div class="col-md-4" style="display: none" id="c_des">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1"  value="<?php echo $g_edit->c_des;?>"  name="c_des"  onkeypress=" return onlyLtrs(event, this);" placeholder="Designation *">
              <label></label>
              <span class="help-block">Designation *</span> </div>
            </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="g_name" onkeypress=" return onlyLtrs(event, this);"  value="<?php echo $g_edit->g_name; ?>" placeholder="Full Name *">
              <label></label>
              <span class="help-block">Full Name *</span> </div>
            </div>
			<div class="col-md-4" id="corporateId" hidden>
            <input autocomplete="off" type="hidden" class="form-control" id="form_control_1" name="corporateId"    value="<?php echo $g_edit->corporate_id; ?>" placeholder="Corporate ID">
            </div>
            <div class="col-md-12">
			<h3 class="form-heading">Contact Details</h3>
		   </div>
			<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="qualificationName"    value="<?php echo $g_edit->g_highest_qual_name; ?>" placeholder="Highest Qualification">
              <label></label>
              <span class="help-block">Highest Qualification Name </span> </div>
            </div>
			<?php  $lebel_value=$g_edit->g_highest_qual_level; ?>
			<div class="col-md-4">
            <div class="form-group form-md-line-input">
               <select class="form-control bs-select" name="qualificationLevel" id="qualificationLevel">
                              <option value="" <?php if($lebel_value==NULL){echo "selected";} ?>>Qualification</option>
                              <option value="9" <?php if($lebel_value ==9){echo 'selected';} ?>>No info</option>
							  <option value="0" <?php if($lebel_value ==0){echo 'selected';} ?>>Bellow Secondary</option>
							  <option value="1" <?php if($lebel_value ==1){echo 'selected';} ?>>Secondary</option>
							  <option value="2" <?php if($lebel_value ==2){echo 'selected';} ?>>Higher Secondary</option>
							  <option value="3" <?php if($lebel_value ==3){echo 'selected';} ?>>Graduate</option>
							  <option value="4" <?php if($lebel_value ==4){echo 'selected';} ?>>Post Graduate</option>
							  <option value="5"<?php if($lebel_value ==5){echo 'selected';} ?>>Doctorate</option>
                             
                </select>
              <span class="help-block">Highest Qualification Label </span> </div>
            </div>
			
			
			
		
			
            <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="g_address"    value="<?php echo $g_edit->g_address; ?>" placeholder="Address *">
              <label></label>
              <span class="help-block">Address *</span> </div>
            </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="pincode" name="g_place"  required="required" onblur="fetch_all_address()"   value="<?php echo $g_edit->g_pincode; ?>">
              <label></label>
              <span class="help-block">Pincode *</span> </div>
            </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="g_city" name="g_city"  value="<?php echo $g_edit->g_city; ?>" required="required" placeholder="City/ Town/ Village *" />
              <label></label>
              <span class="help-block">City/ Town/ Village *</span> </div>
            </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="g_state"  name="g_state"   value="<?php echo $g_edit->g_state; ?>" required ="required" placeholder="State Name *"/>
              <label></label>
              <span class="help-block">State Name *</span> </div>
            </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="g_country" name="g_country"  value="<?php echo $g_edit->g_country; ?>"  required="required" placeholder="Country *"/>
              <label></label>
              <span class="help-block">Country *</span> </div>
            </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="mobile" name="g_contact_no"   maxlength="10" onkeypress=" return onlyNos(event, this);"  value="<?php echo $g_edit->g_contact_no; ?>" placeholder="Mobile No. ">
              <label></label>
              <span class="help-block">Mobile No. </span> </div>
            </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="email" class="form-control" id="form_control_1" name="g_email"  value="<?php echo $g_edit->g_email; ?>" placeholder="Email *">
              <label></label>
              <span class="help-block">Email *</span> </div>
            </div>
            <div class="col-md-12">
			<h3 class="form-heading">Other Info</h3>
		   </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select"  name="g_gender" >
                
                <option value="Male" <?php if(isset($g_edit->g_gender)){ if($g_edit->g_gender=='Male') echo "selected";} ?>>Male</option>
                <option value="Female" <?php if(isset($g_edit->g_gender)){ if($g_edit->g_gender=='Female') echo "selected";} ?>>Female</option>
                <option value="Others" <?php if(isset($g_edit->g_gender)){ if($g_edit->g_gender=='Others') echo "selected";} ?>>Others</option>
                
              </select>
              </select>
              <label></label>
              <span class="help-block">Gender </span> </div>
            </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input">
			
			<?php
			if($g_edit->g_dob!=''){
			$dob = date("d-m-Y", strtotime($g_edit->g_dob));
			}else{
				$dob='';
			}
			?>
              <input autocomplete="off" type="text" class="form-control  date-picker" id="form_control_1"  value="<?php if($dob!='') echo $dob; ?>" name="g_dob" placeholder="DOB *">
              <label></label>
              <span class="help-block">DOB *</span> </div>
            </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="g_occupation"  value="<?php echo $g_edit->g_occupation; ?>" placeholder="Occupation">
              <label></label>
              <span class="help-block">Occupation *</span> </div>
            </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select onchange="is_married(this.value)" class="form-control bs-select" id="married" name="g_married">
                <option value="married" <?php if($g_edit->g_married=='married')echo "selected"; ?>>Married</option>
                <option value="divorced" <?php if($g_edit->g_married=='divorced')echo "selected"; ?>>Divorced</option>
                <option value="widowed" <?php if($g_edit->g_married=='widowed')echo "selected"; ?>>Widowed</option>
                <option value="unmarried" <?php if($g_edit->g_married=='unmarried')echo "selected"; ?>>Unmarried</option>
              </select>
              <label></label>
              <input type="hidden" id="mstatus" value="<?php echo $g_edit->g_married;?>">
              <input type="hidden" name="guest_id" value="<?php echo $g_edit->g_id; ?>" />
              <span class="help-block">Martial Status *</span>
            </div>
            </div>
            <div class="col-md-4" id="g_anniv" style="display: none">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control date-picker" value="<?php echo $g_edit->g_anniv; ?>" id="ane" name="g_anniv">
              <label></label>
              <span class="help-block">Enter Anniversary...</span> </div>
            </div>
			
			
			<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select" id="form_control_2" name="g_id_type" onchange="onOthers(this.value)">
                <option value="" selected="selected" disabled="disabled">Select ID Proof</option>
                <option value="Passport" <?php if($g_edit->g_id_type=="Passport"){echo 'selected="selected"';} ?>>Passport</option>
                <option value="PAN card" <?php if($g_edit->g_id_type=="PAN card"){echo 'selected="selected"';} ?>>PAN card</option>
                <option value="Voter card" <?php if($g_edit->g_id_type=="Voter card"){echo 'selected="selected"';} ?>>Voter card</option>
                <option value="Adhar card" <?php if($g_edit->g_id_type=="Adhar card"){echo 'selected="selected"';} ?>>Adhar card</option>
                <option value="Driving License" <?php if($g_edit->g_id_type=="Driving License"){echo 'selected="selected"';} ?>>Driving License</option>
                <option value="Company ID Card" <?php if($g_edit->g_id_type=="Company ID Card"){echo 'selected="selected"';} ?>>Company ID Card</option>
                <option value="Ration Card" <?php if($g_edit->g_id_type=="Ration Card"){echo 'selected="selected"';} ?>>Ration Card</option>
                <option value="Other Govt ID" <?php if($g_edit->g_id_type=="Other Govt ID"){echo 'selected="selected"';} ?>>Other Govt ID</option>
                <option value="Others" <?php if($g_edit->g_id_type!="Passport" && $g_edit->g_id_type!="PAN card" && $g_edit->g_id_type!="Voter card" && $g_edit->g_id_type!="Adhar card" && $g_edit->g_id_type!="Driving License" && $g_edit->g_id_type!="Company ID Card" && $g_edit->g_id_type!="Ration Card" && $g_edit->g_id_type!="Other Govt ID"){echo 'selected="selected"';} ?>>Others</option>
              </select>
              <span class="help-block">Select ID Proof</span> </div>
        </div> 
		
		<div class="col-md-4" id="others_id"  style="display:none;">
        <div class="form-group form-md-line-input">
          <input autocomplete="off"  type="text" class="form-control" id="other_id" name="other_id" placeholder="your Id proof" value="<?php if($g_edit->g_id_type!="Passport" && $g_edit->g_id_type!="PAN card" && $g_edit->g_id_type!="Voter card" && $g_edit->g_id_type!="Adhar card" && $g_edit->g_id_type!="Driving License" && $g_edit->g_id_type!="Company ID Card" && $g_edit->g_id_type!="Ration Card" && $g_edit->g_id_type!="Other Govt ID"){echo $g_edit->g_id_type;} ?>">
          <label></label>
          <span class="help-block">Other Id Proof</span> </div>
        </div>
		 <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off"  type="text" class="form-control" id="g_id_number" name="g_id_number" placeholder="Id Ref Number" value="<?php if(isset($g_edit->g_id_number)){echo $g_edit->g_id_number;}else echo "N/A"; ?>">
          <label></label>
          <span class="help-block">Id Ref Number</span> </div>
        </div>
		
			<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <?php
			  $discount_rule=$this->unit_class_model->discount_rules();	
			  ?>
			  
			   <select class="form-control bs-select"   name="discount_rule[]" id=""  multiple>
			  <?php
			  
			   $apc = explode(',',$g_edit->cp_discount_rule_id);
					
				 
				 foreach($discount_rule as $rule){ ?>
                  
				  <option value="<?php echo $rule->id;?>" <?php if(in_array($rule->id,$apc)){echo 'selected="selected"';}?>><?php echo  $rule->rule_name;?></option>
				
                  <?php } ?>
                </select>
              <span class="help-block">Discount Rule</span> 		</div>
        </div>				<div class="col-md-4">			
        <div class="form-group form-md-line-input">			  
        <input autocomplete="off" type="text" class="form-control" id="gstin" name="gstin" value="<?php if(isset($g_edit->gstin) && $g_edit->gstin)  echo $g_edit->gstin; ?>" placeholder="GSTIN No">			  <label></label>			  <span class="help-block">GSTIN No</span> </div>        </div>
           <div class="col-md-12">
			<h3 class="form-heading">Upload Section</h3>
		   </div>
            <div class="col-md-12">
            <div class="row">
            <div class="col-md-4">
            <div class="form-group form-md-line-input uploadss">
                <label>Photo Proof</label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>upload/guest/thumbnail/photo/<?php if( $g_edit->g_photo_thumb== '') { echo "no_images.png"; } else { echo $g_edit->g_photo_thumb; }?>" alt=""/> </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                  <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('image_photo');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input uploadss">
                <label>ID Proof</label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>upload/guest/thumbnail/id_proof/<?php if( $g_edit->g_id_proof_thumb== '') { echo "no_images.png"; } else { echo $g_edit->g_id_proof_thumb; }?>" alt=""/> </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                  <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('image_idpf');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                </div>
            </div>
            </div>
            </div>
            </div>
          </div>
        </div>
        <div class="form-actions right">
          <input type="submit" class="btn blue" value="Submit">
        </div>
        <?php 
								}
							}

									
form_close(); ?>
        <!-- END CONTENT --> 
      </div>
    </div>
  </div>
</div>
<script>
function check_corporate(value){
	 
	 if(value=="VIP"){
		
		 document.getElementById("type").style.display="block";
		 document.getElementById("class").style.display="block";
	}
	else{
		 document.getElementById("type").style.display="none";
		 document.getElementById("class").style.display="none";
	}
	 
	 
	   if(value=="Government_official"){
			
		 document.getElementById("dep").style.display="block";
		 document.getElementById("c_des").style.display="block";
	}
	else{
		
		 document.getElementById("c_des").style.display="none";
	}
	   
	    if(value=="Internal_staff"){
			
		 document.getElementById("dep").style.display="block";
		 document.getElementById("c_des").style.display="block";
	}
	
	   
	   
	   
		if(value=="Military"){
			
		 document.getElementById("military").style.display="block";
		 document.getElementById("rank").style.display="block";
		  
	}
	else{
		 document.getElementById("military").style.display="none";
		 document.getElementById("rank").style.display="none";
	}
	
	
	if(value=="Family"){
			
		 document.getElementById("f_size").style.display="block";
	}
	else{
		 document.getElementById("f_size").style.display="none";
	}
   
       if(value =="Corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
           document.getElementById("corporateId").style.display="block";
		  
       }
	   else{
           document.getElementById("c_name").style.display="none";
           
			
	  }
   }
/*
function check_corporate(value){
      if(value=="VIP"){		
		 document.getElementById("type").style.display="block";
		 document.getElementById("class").style.display="block";
	}
	else{
		 document.getElementById("type").style.display="none";
		 document.getElementById("class").style.display="none";
	}
	 
	 
	if(value=="Government_official"){
			
		 document.getElementById("dep").style.display="block";
		 document.getElementById("c_des").style.display="block";
	}
	else{
		
		 document.getElementById("c_des").style.display="none";
		  document.getElementById("dep").style.display="none";
	}
	   
	if(value=="Military"){
			
		 document.getElementById("military").style.display="block";
		 document.getElementById("rank").style.display="block";
		  
	}
	else{
		 document.getElementById("military").style.display="none";
		 document.getElementById("rank").style.display="none";
	}
	
	
	if(value=="Family"){

		 document.getElementById("f_size").style.display="block";
	}
	else{
		 document.getElementById("f_size").style.display="none";
	}
   
    if(value =="Corporate"){

	   document.getElementById("c_name").style.display="block";
	   document.getElementById("c_des").style.display="block";
		  
   }
   else{
	   document.getElementById("c_name").style.display="none";
	   document.getElementById("c_des").style.display="none";
		
  }
  if(value=="Internal_staff"){
			
		 document.getElementById("dep").style.display="block";
		 document.getElementById("c_des").style.display="block";
	}
	
	else{
		
		 document.getElementById("dep").style.display="none";
	}  
}*/
	  
function is_married(value){

        if(value =="married"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
$(document).ready(function() {
	var married = $("#married").val();
	
	if(married =="married"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
	
	var hVal = $("#hidVal").val();
	//alert(hVal);
	var value=hVal;
	/*
	if(hVal == 'Family'){
		$("#f_size").css("display", "block");
	}
	if(hVal == 'VIP'){
		$("#type").css("display", "block");
		$("#class").css("display", "block");
	}
	if(hVal == 'Government_official'){
		$("#dep").css("display", "block");
		$("#c_des").css("display", "block");
	}
	
	if(hVal == 'Military'){
		$("#military").css("display", "block");
		$("#rank").css("display", "block");
	}
	if(hVal == 'Military'){
		$("#c_name").css("display", "block");
		$("#c_des").css("display", "block");
	}
	if(hVal == 'Internal_staff'){
		$("#dep").css("display", "block");
		$("#c_des").css("display", "block");
	}
	*/
	 if(value=="VIP"){
		
		 document.getElementById("type").style.display="block";
		 document.getElementById("class").style.display="block";
	}
	else{
		 document.getElementById("type").style.display="none";
		 document.getElementById("class").style.display="none";
	}
	 
	 
	   if(value=="Government_official"){
			
		 document.getElementById("dep").style.display="block";
		 document.getElementById("c_des").style.display="block";
	}
	else{
		
		 document.getElementById("c_des").style.display="none";
	}
	   
	    if(value=="Internal_staff"){
			
		 document.getElementById("dep").style.display="block";
		 document.getElementById("c_des").style.display="block";
	}
	
	   
	   
	   
		if(value=="Military"){
			
		 document.getElementById("military").style.display="block";
		 document.getElementById("rank").style.display="block";
		  
	}
	else{
		 document.getElementById("military").style.display="none";
		 document.getElementById("rank").style.display="none";
	}
	
	
	if(value=="Family"){
			
		 document.getElementById("f_size").style.display="block";
	}
	else{
		 document.getElementById("f_size").style.display="none";
	}
   
       if(value =="Corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
		  
       }
	   else{
           document.getElementById("c_name").style.display="none";
           
			
	  }
});
	function onOthers(v){
	if(v=='Others'){
		document.getElementById( "others_id" ).style.display = "block";
		$('#others_id').show();
		
	}else{
		
		document.getElementById( "others_id" ).style.display = "none";
	}
}
</script> 
