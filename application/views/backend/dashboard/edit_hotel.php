<script type="text/javascript">
	$( document ).ready( function () {
		//$("#service_tax").hide();
	//	$( "#upload_logo" ).hide();
	//	$( "#upload_text" ).hide();
	} );

	function showDiv( elem ) {
		if ( elem.value == 'Yes' ) {
			document.getElementById( 'service_tax' ).style.display = "block";
		} else {
			document.getElementById( 'service_tax' ).style.display = "none";
		}
	}

	function hideShowLogo() {
		//alert(document.getElementById('st_logo'));
		if ( $( '#st_logo' ).val() == 'Yes' ) {
			$( '#upload_logo' ).show();
			$( '#upload_text' ).hide();
			$( "#text" ).removeAttr( "required" );
			$( "#logo" ).attr( "required", "true" );
		} else if ( $( '#st_logo' ).val() == 'No' ) {
			$( '#upload_logo' ).hide();
			$( '#upload_text' ).show();
			$( "#logo" ).removeAttr( "required" );
			$( "#text" ).attr( "required", "true" );
		} else {
			$( '#upload_logo' ).hide();
			$( '#upload_text' ).hide();
			$( "#logo" ).removeAttr( "required" );
			$( "#text" ).removeAttr( "required" );
		}
	}

	function hideShow() {
		if ( $( '#tax' ).val() == 'Yes' ) {
			$( "#service_tax" ).show();
			$( "#service_tax1" ).show();

		} else {
			$( "#service_tax" ).hide();
			$( "#service_tax1" ).hide();
		}
	}
</script>
<script>
	function fetch_all_address_hotel( pincode, g_country, g_state, g_city ) {
		var pin_code = document.getElementById( pincode ).value;
		//alert(pincode + "/" + g_country + "/" + g_state + "/" + g_city+"=" + pin_code);
		jQuery.ajax( {
			type: "POST",
			url: "<?php echo base_url(); ?>bookings/fetch_address",
			dataType: 'json',
			data: {
				pincode: pin_code
			},
			success: function ( data ) {
				//alert(data.country);
				$( g_country ).val( data.country );
				$( g_state ).val( data.state );
				$( g_city ).val( data.city );
			}
		} );
	}
</script>
<script type="text/javascript">
	function submit_form() {
		var chk_in_hours = $( '#chk_in_hour' ).val();
		var chk_in_mints = $( '#chk_in_mint' ).val();
		var chk_in_median = $( '#hotel_check_in_time_fr' ).val();
		var chk_out_hours = $( '#chk_out_hour' ).val();
		var chk_out_mints = $( '#chk_out_mint' ).val();
		var chk_out_median = $( '#hotel_check_out_time_fr' ).val();
		if ( chk_in_median == 'AM' && chk_out_median == 'AM' ) {
			//alert(chk_in_hours);
			if ( chk_in_hours > chk_out_hours ) {
				alert( chk_in_hours );
				document.getElementById( 'submit_form' ).submit();
			} else if ( chk_in_hours == chk_out_hours && chk_in_mints > chk_out_mints ) {
				alert( chk_in_hours );
				document.getElementById( 'submit_form' ).submit();
			} else {
				swal( "Alert", "Enter proper time", "warning" );
			}
		} else if ( chk_in_median == 'PM' && chk_out_median == 'PM' ) {
			if ( chk_in_hours > chk_out_hours ) {
				alert( chk_in_hours );
				document.getElementById( 'submit_form' ).submit();
			} else if ( chk_in_hours == chk_out_hours && chk_in_mints > chk_out_mints ) {
				alert( chk_in_hours );
				document.getElementById( 'submit_form' ).submit();
			} else {
				swal( "Alert", "Enter proper time", "warning" )
			}

		} else if ( chk_in_median == 'PM' && chk_out_median == 'AM' ) {
			document.getElementById( 'submit_form' ).submit();
		} else {
			swal( "Alert", "Enter proper time", "warning" );
		}
	}
</script>

<!-- BEGIN PAGE CONTENT-->

<div class="portlet box blue" id="form_wizard_1">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-pin font-green"></i> Edit HOTELS - <span class="step-title"> Step 1 of 4 </span>
		</div>
	</div>
	<div class="portlet-body form">
		<form action="<?php echo base_url();?>dashboard/edit_hotel" class="" enctype="multipart/form-data" id="submit_form" method="POST" style="margin: 0;">
			<?php
		//	 echo "<pre>";
		//	print_r($value);exit;
			if ( isset( $value ) && $value ) {
				foreach ( $value as $h_edit ) {
					$hotel_type = $h_edit->hotel_type;
					$type = explode( ',', $hotel_type );
					?>
			<div class="form-wizard">
				<div class="form-body">
					<ul class="nav nav-pills nav-justified steps">
						<li>
							<a href="#tab1" data-toggle="tab" class="step">
							  <span class="number"> 1 </span>
							  <span class="desc">
							  <i class="fa fa-check"></i> Account Setup </span>
							 </a>
						</li>
						<li>
							<a href="#tab2" data-toggle="tab" class="step">
								  <span class="number"> 2 </span>
								  <span class="desc">
								  <i class="fa fa-check"></i> Profile Setup </span>
							  </a>
						</li>
						<li>
							<a href="#tab3" data-toggle="tab" class="step active">
								  <span class="number"> 3 </span>
								  <span class="desc">
								  <i class="fa fa-check"></i> Billing Setup </span>
							  </a>
						</li>
						<li>
							<a href="#tab4" data-toggle="tab" class="step">
								  <span class="number"> 4 </span>
								  <span class="desc">
								  <i class="fa fa-check"></i> Preferences </span>
							  </a>
						</li>
						<li>
							<a href="#tab5" data-toggle="tab" class="step">
								  <span class="number"> 5 </span>
								  <span class="desc">
								  <i class="fa fa-check"></i> Confirm </span>
							  </a>
						</li>
					</ul>
					<div id="bar" class="progress progress-striped" role="progressbar">
						<div class="progress-bar progress-bar-success"></div>
					</div>
					<div class="tab-content">
						<div class="alert alert-danger display-none">
							<button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
						<div class="alert alert-success display-none">
							<button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
						<div class="tab-pane" id="tab1">
							<div class="row">
								<h3 class="block">General Details</h3>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">									
										<input type="text" class="form-control" name="hotel_name" value="<?php echo $h_edit->hotel_name; ?>" placeholder="Hotel Name *" required/>
										<label></label>
										<span class="help-block">Hotel Name *</span>
									</div>
								</div>
								
								<div class="col-md-3">
                         <div class="form-group form-md-line-input">
                        <input type="text" class="form-control" name="hotel_gpname" value="<?php echo $h_edit->hotel_gpname; ?>" placeholder="Hotel Group Name"/>
                        <label></label>
                        <span class="help-block">Hotel Group Name</span> </div>
                       </div>
								
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="hotel_year_established" value="<?php echo $h_edit->hotel_year_established; ?>" required maxlength="4" onkeypress="return onlyNos(event,this)" placeholder="Year Established *">
										<label></label>
										<span class="help-block">Year Established *</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="hotel_floor" value="<?php echo $h_edit->hotel_floor; ?>" required maxlength="4" onkeypress="return onlyNos(event,this)" placeholder="No of floor *">
										<label></label>
										<span class="help-block">No of floor *</span>
									</div>
								</div>
								<div class="col-md-4">
									<input type="hidden" value="<?php echo $h_edit->hotel_id; ?>" name="h_id">
									<div class="form-group form-md-line-input">
										<select class="form-control bs-select" name="hotel_ownership_type" required>
										 <option selected="true" value="<?php echo $h_edit->hotel_ownership_type; ?>">
										 <?php echo $h_edit->hotel_ownership_type; ?>
										 </option>
										 <option value="Sole Proprietorship">Sole Proprietorship</option>
										 <option value="Partnership">Partnership</option>
										 <option value="Cooperative Business">Cooperative Business</option>
										</select>	
										<label></label>							
										<span class="help-block">Hotel Ownership Type *</span>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group form-md-line-input">
										<label class="control-label col-md-2">Hotel Type<span class="required">* </span></label>
										<div class="col-md-10">
											<div class="md-checkbox-inline">
												<div class="md-checkbox">
													<input type="checkbox" value="Business Hotel" <?php if(isset($type[ '0'])){if($type[ '0']=="Business Hotel" ){ echo 'checked';}}?> id="checkbox1" class="md-check" name="hotel_type[]">
													<label for="checkbox1">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> Business Hotel</label>
												</div>
												<div class="md-checkbox ">
													<input type="checkbox" value="Airport Hotel" <?php if(isset($type[ '1'])){if($type[ '1']=="Airport Hotel" ){ echo 'checked';}}?> id="checkbox2" class="md-check" name="hotel_type[]" >
													<label for="checkbox2">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> Airport Hotel </label>
												</div>
												<div class="md-checkbox ">
													<input type="checkbox" value="Suite Hotel" <?php if(isset($type[ '2'])){if($type[ '2']=="Suite Hotel" ){ echo 'checked';}}?> id="checkbox3" class="md-check" name="hotel_type[]">
													<label for="checkbox3">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> Suite Hotel</label>
												</div>
												<div class="md-checkbox ">
													<input type="checkbox" value="Extended Stay Hotel" <?php if(isset($type[ '3'])){if($type[ '3']=="Extended Stay Hotel" ){ echo 'checked';}}?> id="checkbox4" class="md-check" name="hotel_type[]">
													<label for="checkbox4">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> Extended Stay Hotel</label>
												</div>
												<div class="md-checkbox ">
													<input type="checkbox" value="Resort Hotel" <?php if(isset($type[ '4'])){if($type[ '4']=="Resort Hotel" ){ echo 'checked';}}?> id="checkbox5" class="md-check" name="hotel_type[]">
													<label for="checkbox5">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> Resort Hotel</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<select class="form-control bs-select" id="" name="hotel_rooms" required>
										 <option selected="true" value="<?php echo $h_edit->hotel_rooms; ?>"><?php echo $h_edit->hotel_rooms; ?></option>
										 <option value="Under 20">Under 20</option>
										 <option value="20-49">20 - 49</option>
										 <option value="50-99">50 - 99</option>
										 <option value="100-199">100 - 199</option>
										 <option value="200-399">200 - 399</option>
										 <option value="400-699">400 - 699</option>
										 <option value="More than 700">More than 700</option>
										</select>
										<label></label>
										<span class="help-block">No. Of Rooms *</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<select class="form-control bs-select" id="st_logo" onchange="return hideShowLogo();">
											<option value="" disabled>Logo Applied?</option>
											<?php

											if($h_edit->hotel_logo_images!=''){											
												$style_img = "display:block";
												$style_text = "display:none";
												?>
												<option value="Yes" selected>As an Image</option>
												<option value="No">As a Text </option>
												<?php
											}
											else if($h_edit->hotel_logo_text!=''){
												$style_text = "display:block";
												$style_img = "display:none";											
												?>
												<option value="Yes" >As an Image</option>
												<option value="No" selected>As a Text </option>
											<?php

											}
											else{
												$style_img = "display:none";
												$style_text = "display:none";									
												?>
												<option value="Yes" >As an Image</option>
												<option value="No" selected>As a Text </option>
												<?php
											}
											?>
										</select>
										<label></label>
										<span class="help-block">Logo Applied?</span>
									</div>
								</div>
								<div id="upload_text" style="display: none;">
									<div class="col-md-4">
										<div class="form-group form-md-line-input uploadss">
											<input type="text" class="form-control" value="<?php echo $h_edit->hotel_logo_text; ?>" id="text" name="images_text" placeholder="Logo Text *">
											<span class="help-block">Logo Text *</span>
										</div>
									</div>
								</div>
								<div id="upload_logo" style=<?php echo $style_img; ?>>
									<div class="col-md-12">
										<div class="form-group form-md-line-input uploadss">
											<label>Upload Logo</label>
											<div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
													<img width="30%" src="<?php echo base_url();?>upload/hotel/<?php if( $h_edit->hotel_logo_images_thumb== '') { echo " no_images.png "; } else { echo $h_edit->hotel_logo_images_thumb; }?>" alt=""/>
												</div>
												<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
												<div>
													<span class="btn default btn-file">
													<span class="fileinput-new"> Select image </span>
													<span class="fileinput-exists"> Change </span>
													<input type="file" class="form-control" id="logo" name="image_photo">
													</span>
													<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane active" id="tab2">
							<div class="row">
								<div class="col-md-12">
									<h3 class="form-heading">Hotel Address</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="hotel_street1" value="<?php echo $h_edit->hotel_street1; ?>" placeholder="Street Details Line 1" required="required"/>
										<label></label>
										<span class="help-block">Street Details Line 1 </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="hotel_street2" value="<?php echo $h_edit->hotel_street2; ?>" placeholder="Street Details Line 2" required="required"/>
										<label></label>
										<span class="help-block">Street Details Line 2 </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_landmark; ?>" name="hotel_landmark" placeholder="Landmark"/>
										<label></label>
										<span class="help-block">Landmark</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" maxlength="6" value="<?php echo $h_edit->hotel_pincode; ?>" name="hotel_pincode" id="pincode" placeholder="Pincode" onkeypress="return onlyNos(event,this);" required onblur='fetch_all_address_hotel("pincode","#g_country","#g_state","#g_city")'/>
										<label></label>
										<span class="help-block">Pincode</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="g_city" value="<?php echo $h_edit->hotel_district; ?>" name="hotel_district" placeholder="District" required="required"/>
										<label></label>
										<span class="help-block">District</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="g_state" value="<?php echo $h_edit->hotel_state; ?>" name="hotel_state" placeholder="State" required>
										<label></label>
										<span class="help-block">State</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="g_country" value="<?php echo $h_edit->hotel_country; ?>" name="hotel_country" placeholder="Country" required/>
										<label></label>
										<span class="help-block">Country</span>
									</div>
								</div>
								<div class="col-md-12">
									<h3 class="form-heading">Branch Office Address</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_branch_street1; ?>" name="hotel_branch_street1" placeholder="Street Details Line 1"/>
										<label></label>
										<span class="help-block">Street Details Line 1 </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_branch_street2; ?>" name="hotel_branch_street2" placeholder="Street Details Line 2"/>
										<label></label>
										<span class="help-block">Street Details Line 2 </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_branch_landmark; ?>" name="hotel_branch_landmark" placeholder="Landmark"/>
										<label></label>
										<span class="help-block">Landmark</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" maxlength="6" value="<?php echo $h_edit->hotel_branch_pincode; ?>" name="hotel_branch_pincode" id="pincode_branch" placeholder="Pincode" onkeypress="return onlyNos(event,this)" onblur='fetch_all_address_hotel("pincode_branch","#branch_country","#branch_state","#branch_city")'/>
										<label></label>
										<span class="help-block">Pincode</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_branch_district; ?>" id="branch_city" name="hotel_branch_district" placeholder="District"/>
										<label></label>
										<span class="help-block">District</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="branch_state" value="<?php echo $h_edit->hotel_branch_state; ?>" name="hotel_branch_state" placeholder="State"/>
										<label></label>
										<span class="help-block">State</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="branch_country" value="<?php echo $h_edit->hotel_branch_country; ?>" name="hotel_branch_country" placeholder="Country"/>
										<label></label>
										<span class="help-block">Country</span>
									</div>
								</div>
								<div class="col-md-12">
									<h3 class="form-heading">Booking Office Address</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_booking_street1; ?>" name="hotel_booking_street1" placeholder="Street Details Line 1"/>
										<label></label>
										<span class="help-block">Street Details Line 1 </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_booking_street2; ?>" name="hotel_booking_street2" placeholder="Street Details Line 2"/>
										<label></label>
										<span class="help-block">Street Details Line 2 </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_booking_landmark; ?>" name="hotel_booking_landmark" placeholder="Landmark"/>
										<label></label>
										<span class="help-block">Landmark</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" maxlength="6" id="pincode_booking" value="<?php echo $h_edit->hotel_booking_pincode; ?>" name="hotel_booking_pincode" placeholder="Pincode" onkeypress="return onlyNos(event,this)" onblur='fetch_all_address_hotel("pincode_booking","#booking_country","#booking_state","#booking_city")'/>
										<label></label>
										<span class="help-block">Pincode</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="booking_city" value="<?php echo $h_edit->hotel_booking_district; ?>" name="hotel_booking_district" placeholder="District"/>
										<label></label>
										<span class="help-block">District</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_booking_state; ?>" id="booking_state" name="hotel_booking_state" placeholder="State"/>
										<label></label>
										<span class="help-block">State</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="booking_country" value="<?php echo $h_edit->hotel_booking_country; ?>" name="hotel_booking_country" placeholder="Country"/>
										<label></label>
										<span class="help-block">Country</span>
									</div>
								</div>
								<div class="col-md-12">
									<h3 class="form-heading">Front Desk</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_frontdesk_name; ?>" name="hotel_frontdesk_name" placeholder="Contact Person" required/>
										<label></label>
										<span class="help-block">Contact Person</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" required value="<?php echo $h_edit->hotel_frontdesk_mobile; ?>" name="hotel_frontdesk_mobile" placeholder="Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10">
										<label></label>
										<span class="help-block">Mobile No.</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="hotel_frontdesk_mobile_alternative" value="<?php echo $h_edit->hotel_frontdesk_mobile_alternative; ?>" required="required" placeholder="Alternative Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
										<label></label>
										<span class="help-block">Alternative Mobile No.</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="email" class="form-control" value="<?php echo $h_edit->hotel_frontdesk_email; ?>" name="hotel_frontdesk_email" required placeholder="Email"/>
										<label></label>
										<span class="help-block">Email</span>
									</div>
								</div>
								<div class="col-md-12">
									<h3 class="form-heading">Owner</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_owner_name; ?>" name="hotel_owner_name" required placeholder="Contact Person"/>
										<label></label>
										<span class="help-block">Contact Person</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_owner_mobile; ?>" name="hotel_owner_mobile" placeholder="Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
										<label></label>
										<span class="help-block">Mobile No.</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_owner_mobile_alternative; ?>" name="hotel_owner_mobile_alternative" placeholder="Alternative Mobile No." onkeypress="return onlyNos(event,this)" maxlength="10"/>
										<label></label>
										<span class="help-block">Alternative Mobile No.</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="email" required="required" class="form-control" value="<?php echo $h_edit->hotel_owner_email; ?>" name="hotel_owner_email" placeholder="Email"/>
										<label></label>
										<span class="help-block">Email</span>
									</div>
								</div>
								<div class="col-md-12">
									<h3 class="form-heading">HR</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_hr_name; ?>" name="hotel_hr_name" placeholder="Contact Person"/>
										<label></label>
										<span class="help-block">Contact Person</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_hr_mobile; ?>" name="hotel_hr_mobile" placeholder="Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
										<label></label>
										<span class="help-block">Mobile No.</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_hr_mobile_alternative; ?>" name="hotel_hr_mobile_alternative" placeholder="Alternative Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
										<label></label>
										<span class="help-block">Alternative Mobile No.</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_hr_email; ?>" name="hotel_hr_email" placeholder="Email"/>
										<label></label>
										<span class="help-block">Email</span>
									</div>
								</div>
								<div class="col-md-12">
									<h3 class="form-heading">Accounts</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_accounts_name; ?>" name="hotel_accounts_name" placeholder="Contact Person"/>
										<label></label>
										<span class="help-block">Contact Person</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_accounts_mobile; ?>" name="hotel_accounts_mobile" placeholder="Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
										<label></label>
										<span class="help-block">Mobile No.</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="hotel_accounts_mobile_alternative" value="<?php echo $h_edit->hotel_accounts_mobile_alternative; ?>" placeholder="Alternative Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
										<label></label>
										<span class="help-block">Alternative Mobile No.</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="hotel_accounts_email" value="<?php echo $h_edit->hotel_accounts_email; ?>" placeholder="Email"/>
										<label></label>
										<span class="help-block">Email</span>									
									</div>
								</div>
								<div class="col-md-12">
									<h3 class="form-heading">Nearest Police Station</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_near_police_name; ?>" name="hotel_near_police_name" placeholder="Contact Person"/>
										<label></label>
										<span class="help-block">Contact Person</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_near_police_mobile; ?>" name="hotel_near_police_mobile" placeholder="Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
										<label></label>
										<span class="help-block">Mobile No.</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="hotel_near_police_mobile_alternative" value="<?php echo $h_edit->hotel_near_police_mobile_alternative; ?>" placeholder="Alternative Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
										<label></label>
										<span class="help-block">Alternative Mobile No.</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="hotel_near_police_email" value="<?php echo $h_edit->hotel_near_police_email; ?>" placeholder="Email"/>
										<label></label>
										<span class="help-block">Email</span>
									</div>
								</div>
								<div class="col-md-12">
									<h3 class="form-heading">Nearest Medial Establishment</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_near_medical_name; ?>" name="hotel_near_medical_name" placeholder="Contact Person"/>
										<label></label>
										<span class="help-block">Contact Person</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->hotel_near_medical_mobile; ?>" name="hotel_near_medical_mobile" placeholder="Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
										<label></label>
										<span class="help-block">Mobile No.</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="hotel_near_medical_mobile_alternative" value="<?php echo $h_edit->hotel_near_medical_mobile_alternative; ?>" placeholder="Alternative Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
										<label></label>
										<span class="help-block">Alternative Mobile No.</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="hotel_near_medical_email" value="<?php echo $h_edit->hotel_near_medical_email; ?>" placeholder="Email"/>
										<label></label>
										<span class="help-block">Email</span>
									</div>
								</div>
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<h3 class="form-heading">Fax</h3>
												</div>
												<div class="col-md-12">
													<div class="form-group form-md-line-input">
														<input type="text" class="form-control" name="hotel_fax" value="<?php echo $h_edit->hotel_fax; ?>" placeholder="Enter Fax..."/>
														<label></label>
														<span class="help-block">Enter Fax... </span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<h3 class="form-heading">Website</h3>
												</div>
												<div class="col-md-12">
													<div class="form-group form-md-line-input">
														<input type="text" class="form-control" name="hotel_website" value="<?php echo $h_edit->hotel_website; ?>" placeholder="Enter Website..."/>
														<label></label>
														<span class="help-block">Enter Website... </span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab3">
							<div class="row">
								<div class="col-md-12">
									<h3 class="form-heading">Tax Details</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<select id="tax" onchange="showDiv(this)" name="hotel_tax_applied" class="form-control bs-select">
											<option value="Yes" <?php if($h_edit->hotel_tax_applied == 'Yes'){ echo 'selected'; } ?> >Yes</option>
											<option value="No" <?php if($h_edit->hotel_tax_applied == 'No'){ echo 'selected'; } ?> >No</option>
										</select>
										<label></label>
										<span class="help-block">Tax Applied?</span>
									</div>
								</div>
								<?php if($h_edit->hotel_tax_applied=='Yes'){?>
								<div id="service_tax">
									<?php } else { ?>
									<div id="service_tax" style="display:none;">
										<?php } ?>
										<div class="col-md-4">
											<div class="form-group form-md-line-input">
												<input type="text" value="<?php echo $h_edit->hotel_service_tax; ?>" class="form-control" name="hotel_service_tax" placeholder="Service Tax *"/>
												<label></label>
												<span class="help-block">Service Tax *</span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group form-md-line-input">
												<input type="text" value="<?php echo $h_edit->hotel_service_charge; ?>" class="form-control" name="hotel_service_charge" placeholder="Service Charge %"/>
												<label></label>
												<span class="help-block">Service Charge %</span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group form-md-line-input">
												<?php if(isset($h_edit->hotel_stander_tac) && $h_edit->hotel_stander_tac){
													$stan_tax=$h_edit->hotel_stander_tac;	
												}else{
													$stan_tax='';
												}?>
												<input type="text" value="<?php echo $stan_tax; ?>" class="form-control" name="hotel_stander_tac" placeholder="Standard TAX %"/>
												<label></label>
												<span class="help-block">Standard TAX % </span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group form-md-line-input">
												<?php if(isset($h_edit->hotel_vat_tax) && $h_edit->hotel_vat_tax){
													$h_tax=$h_edit->hotel_vat_tax;
												}else{
													$h_tax='';
												} ?>
												<input type="text" value="<?php echo $h_tax; ?>" class="form-control" name="hotel_vat_tax" placeholder="Vat TAX %"/>
												<label></label>
												<span class="help-block">Vat TAX % </span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group form-md-line-input">
												<?php if(isset($h_edit->hotel_tax_food_vat) && $h_edit->hotel_tax_food_vat){
													$h_food_tax=$h_edit->hotel_tax_food_vat;
												}else{
													$h_food_tax='';
												} ?>
												<input type="text" value="<?php echo $h_food_tax; ?>" class="form-control" name="hotel_tax_food_vat" placeholder="Food Vat  %"/>
												<label></label>
												<span class="help-block">Food Vat  % </span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group form-md-line-input">
												<?php if(isset($h_edit->hotel_tax_liquor_vat) && $h_edit->hotel_tax_liquor_vat){
													$h_lvat=$h_edit->hotel_tax_liquor_vat;
												}else{
													$h_lvat='';
												} ?>
												<input type="text" value="<?php echo $h_lvat; ?>" class="form-control" name="hotel_tax_liquor_vat" placeholder="Liquor Vat %"/>
												<label></label>
												<span class="help-block">Liquor Vat % </span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group form-md-line-input">
												<?php if(isset($h_edit->luxury_tax_slab1_range_to) && $h_edit->luxury_tax_slab1_range_to){
													$ltaxslab=$h_edit->luxury_tax_slab1_range_to;
												}else{
													$ltaxslab='';
												} ?>
												<input type="text" class="form-control" name="luxury_tax_slab1_range_to" value="<?php echo $ltaxslab; ?>" placeholder="Luxury Tax Slab 1 range (Take to)"/>
												<label></label>
												<span class="help-block">Luxury Tax Slab 1 range (Take to) </span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group form-md-line-input">
												<input type="text" name="luxury_tax_slab1_range_from" class="form-control" value="<?php echo $h_edit->luxury_tax_slab1_range_from; ?>" placeholder="Luxury Tax Slab 1 range (From value)"/>
												<label></label>
												<span class="help-block">Luxury Tax Slab 1 range (From value) </span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group form-md-line-input">
												<input type="text" name="luxury_tax_slab1" class="form-control" value="<?php echo $h_edit->luxury_tax_slab1; ?>" placeholder="Luxury Tax Slab 1%"/>
												<label></label>
												<span class="help-block">Luxury Tax Slab 1%</span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group form-md-line-input">
												<input type="text" name="luxury_tax_slab2_range_to" class="form-control" value="<?php echo $h_edit->luxury_tax_slab2_range_to; ?>" placeholder="Luxury Tax Slab 2 range (Take to)"/>
												<label></label>
												<span class="help-block">Luxury Tax Slab 2 range (Take to) </span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group form-md-line-input">
												<input type="text" name="luxury_tax_slab2_range_from" class="form-control" value="<?php echo $h_edit->luxury_tax_slab2_range_from; ?>" placeholder="Luxury Tax Slab 2 range (From value)"/>
												<label></label>
												<span class="help-block">Luxury Tax Slab 2 range (From value)</span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group form-md-line-input">
												<input type="text" name="luxury_tax_slab2" class="form-control" value="<?php echo $h_edit->luxury_tax_slab2; ?>" placeholder="Luxury Tax Slab 2 %"/>
												<label></label>
												<span class="help-block">Luxury Tax Slab 2 %</span>
											</div>
										</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab4">
							<div class="row">
								<div class="col-md-12">
									<h3 class="form-heading">Billing Settings</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" value="<?php echo $h_edit->billing_name; ?>" name="billing_name" required placeholder="Name On Invoice"/>
										<label></label>
										<span class="help-block">Name On Invoice</span>
									</div>
								</div>
								<div class="col-md-12">
									<h3 class="form-heading">Address on Invoice</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="billing_street1" value="<?php echo $h_edit->billing_street1; ?>" required placeholder="Street Details Line 1"/>
										<label></label>
										<span class="help-block">Street Details Line 1 </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="billing_street2" value="<?php echo $h_edit->billing_street2; ?>" placeholder="Street Details Line 2" required/>
										<label></label>
										<span class="help-block">Street Details Line 1 </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="billing_landmark" value="<?php echo $h_edit->billing_landmark; ?>" placeholder="Landmark"/>
										<label></label>
										<span class="help-block">Landmark</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="billing_district" value="<?php echo $h_edit->billing_district; ?>" required id="billing_city" placeholder="District"/>
										<label></label>
										<span class="help-block">District</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" maxlength="6" name="billing_pincode" value="<?php echo $h_edit->billing_pincode; ?>" id="pincode_billing" required onkeypress="return onlyNos(event,this);" placeholder="Pincode" onblur='fetch_all_address_hotel("pincode_billing","#billing_country","#billing_state","#billing_city")'/>
										<label></label>
										<span class="help-block">Pincode</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" required value="<?php echo $h_edit->billing_state; ?>" id="billing_state" name="billing_state" placeholder="State"/>
										<label></label>
										<span class="help-block">State</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="billing_country" value="<?php echo $h_edit->billing_country; ?>" id="billing_country" required placeholder="Country"/>
										<label></label>
										<span class="help-block">Country</span>
									</div>
								</div>
								<div class="col-md-12">
									<h3 class="form-heading">Other detail of Invoice</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="email" required class="form-control" value="<?php echo $h_edit->billing_email; ?>" name="billing_email" placeholder="Enter Email ..."/>
										<label></label>
										<span class="help-block">Enter Email </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="billing_phone" value="<?php echo $h_edit->billing_phone; ?>" placeholder="Enter Phone ..." required maxlength="10" onkeypress="return onlyNos(event,this)"/>
										<label></label>
										<span class="help-block">Enter Phone </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="billing_fax" value="<?php echo $h_edit->billing_fax; ?>" placeholder="Enter Fax No ..."/>
										<label></label>
										<span class="help-block">Enter Fax No</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="billing_vat" value="<?php echo $h_edit->billing_vat; ?>" placeholder="Enter VAT No ..."/>
										<label></label>
										<span class="help-block">Enter VAT No ... </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="billing_bank_name" value="<?php echo $h_edit->billing_bank_name; ?>" required placeholder="Enter Bank Name ..."/>
										<label></label>
										<span class="help-block">Enter Bank Name ... </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="billing_account_no" value="<?php echo $h_edit->billing_account_no; ?>" required onkeypress="return onlyNos(event,this);" placeholder="Enter Account No ... "/>
										<label></label>
										<span class="help-block">Enter Account No ... </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="billing_ifsc_code" value="<?php echo $h_edit->billing_ifsc_code; ?>" placeholder="Enter IFSC Coad ..."/>
										<label></label>
										<span class="help-block">Enter IFSC Coad ... </span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="luxury_tax_reg_no" value="<?php echo $h_edit->luxury_tax_reg_no; ?>" placeholder="Enter Luxury Tax Reg No ..."/>
										<label></label>
										<span class="help-block">Luxury Tax Reg No</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="service_tax_no" value="<?php echo $h_edit->service_tax_no; ?>" placeholder="Enter Service Tax No ..."/>
										<label></label>
										<span class="help-block">Service Tax No</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="vat_reg_no" value="<?php echo $h_edit->vat_reg_no; ?>" placeholder="Enter Vat Reg No ..."/>
										<label></label>
										<span class="help-block">Vat Reg No</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="cin_no" value="<?php echo $h_edit->cin_no; ?>" placeholder="Enter CIN No ..."/>
										<label></label>
										<span class="help-block">CIN No</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="tin_no" value="<?php echo $h_edit->tin_no; ?>" placeholder="Enter TIN No ..."/>
										<label></label>
										<span class="help-block">TIN No</span>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab5">
							<h3 class="block">General Preferences</h3>
							<div class="row">
								<div class=" col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5">Default Check In Time</label>
										<div class="col-md-2">
											<input type="text" class="form-control" value="<?php echo $h_edit->hotel_check_in_time_hr; ?>" name="hotel_check_in_time_hr" id="chk_in_hour" placeholder="HH"/>
										</div>
										<div class="col-md-2">
											<input type="text" class="form-control" value="<?php echo $h_edit->hotel_check_in_time_mm; ?>" name="hotel_check_in_time_mm" id="chk_in_mint" placeholder="MM"/>
										</div>
										<div class="col-md-3">
											<select class="form-control" name="hotel_check_in_time_fr" id="hotel_check_in_time_fr">
												<?php 

						if($h_edit->hotel_check_in_time_fr =='AM')

						{

						?>
												<option value="AM" selected>AM</option>
												<option value="PM">PM</option>
												<?php }

					else if($h_edit->hotel_check_in_time_fr =='PM'){?>
												<option value="AM">AM</option>
												<option value="PM" selected>PM</option>
												<?php } else{?>
												<option value="AM">AM</option>
												<option value="PM">PM</option>
												<?php }?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-5">Default Check Out Time</label>
										<div class="col-md-2">
											<input type="text" class="form-control" name="hotel_check_out_time_hr" id="chk_out_hour" value="<?php echo $h_edit->hotel_check_out_time_hr; ?>" placeholder="HH"/>
										</div>
										<div class="col-md-2">
											<input type="text" class="form-control" id="chk_out_mints" value="<?php echo $h_edit->hotel_check_out_time_mm; ?>" name="hotel_check_out_time_mm" placeholder="MM"/>
										</div>
										<div class="col-md-3">
											<select class="form-control" name="hotel_check_out_time_fr" id="hotel_check_out_time_fr">
												<?php 

																		if($h_edit->hotel_check_out_time_fr =='AM')

																		{

																		?>
												<option value="AM" selected>AM</option>
												<option value="PM">PM</option>
												<?php }

																		else if($h_edit->hotel_check_out_time_fr =='PM'){?>
												<option value="AM">AM</option>
												<option value="PM" selected>PM</option>
												<?php } else{?>
												<option value="AM">AM</option>
												<option value="PM">PM</option>
												<?php }?>
											</select>
										</div>
									</div>
								</div>

								<!-- add new field on 30-11-16---->

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Buffer Hour<span class="required">* </span></label>
										<div class="col-md-8">
											<input name="buffer_hour" id="buffer_hour" type="number" min="0" max="12" class="form-control" placeholder="Ending Price Range" value="<?php if(isset($h_edit->hotel_buffer_hour) && $h_edit->hotel_buffer_hour!='')echo $h_edit->hotel_buffer_hour;else " 0 " ?>" required/>
											<span class="help-block">Select Buffer Hour ... </span>
										</div>
									</div>
								</div>

								<!--  end ---------->

								<div class="col-md-12">
									<div class="form-group form-md-line-input">
										<label class="control-label col-md-2"> Guest<span class="required">* </span></label>
										<div class="col-md-10">
											<div class="md-checkbox-inline">
												<div class="md-checkbox">
													<input type="checkbox" name="guest[]" value="Guest must be present in system for booking purpose" id="checkbox33" checked class="md-check"/>
													<label for="checkbox33">
		  <span></span>
		  <span class="check"></span>
		  <span class="box"></span> Guest must be present in system for booking purpose </label>

												</div>
												<div class="md-checkbox">
													<input type="checkbox" name="guest[]" value="Take Duplicate Entry" id="checkbox34" class="md-check"/>
													<label for="checkbox34">
		  <span></span>
		  <span class="check"></span>
		  <span class="box"></span> Take Duplicate Entry </label>

												</div>
												<div class="md-checkbox">
													<input type="checkbox" name="guest[]" value="Photo Mandatory" id="checkbox35" class="md-check"/>
													<label for="checkbox35">
		  <span></span>
		  <span class="check"></span>
		  <span class="box"></span> Photo Mandatory </label>

												</div>
												<div class="md-checkbox">
													<input type="checkbox" name="guest[]" value="Id Mandatory" id="checkbox36" class="md-check"/>
													<label for="checkbox36">
		  <span></span>
		  <span class="check"></span>
		  <span class="box"></span> Id Mandatory </label>

												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12" style="padding-bottom:15px;">
									<div class="form-group form-md-line-input">
										<label class="control-label col-md-2">Broker<span class="required">* </span></label>
										<div class="col-md-10">
											<div class="md-checkbox-inline">
												<div class="md-checkbox">
													<input type="checkbox" checked name="broker[]" id="checkbox37" class="md-check"/>
													<label for="checkbox37">
		  <span></span>
		  <span class="check"></span>
		  <span class="box"></span> Broker must be present in system for booking purpose </label>

												</div>
												<div class="md-checkbox">
													<input type="checkbox" name="broker[]" value="Photo Mandatory" id="checkbox38" class="md-check"/>
													<label for="checkbox38">
		  <span></span>
		  <span class="check"></span>
		  <span class="box"></span> Photo Mandatory </label>

												</div>
												<div class="md-checkbox">
													<input type="checkbox" name="broker[]" value="Id Mandatory" id="checkbox39" class="md-check"/>
													<label for="checkbox39">
		  <span></span>
		  <span class="check"></span>
		  <span class="box"></span> Id Mandatory </label>

												</div>
												<div class="md-checkbox">
													<input type="checkbox" name="broker[]" value="Change Commission%" id="checkbox40" class="md-check"/>
													<label for="checkbox40">
		  <span></span>
		  <span class="check"></span>
		  <span class="box"></span> Change Commission% </label>

												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Date Format<span class="required">* </span></label>
										<div class="col-md-8">
											<select class="form-control" required name="hotel_date_format">
												<option>---Select Date Format---</option>
												<option>DD-MM-YYYY</option>
												<option>MM-DD-YYYY</option>
												<option>YYYY-MM-DD</option>
											</select>
											<span class="help-block">Select Date Format ... </span>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group ">
										<label class="control-label col-md-5">Default Check Out Time<span class="required">* </span></label>
										<div class="col-md-7">
											<select class="form-control" required name="hotel_time_format">
												<option>Select Date Format</option>
												<option>12 Hours</option>
												<option>24 Hours</option>
											</select>
											<span class="help-block">Select Time Format... </span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>						
				</div>
				<div class="form-actions right">
					<a href="javascript:;" class="btn default button-previous" style="display:none;">
 <i class="m-icon-swapleft"></i> Back </a>

					<a href="javascript:;" class="btn blue button-next" onclick="check();"> Continue <i class="m-icon-swapright m-icon-white"></i>
 </a>
					<a href="javascript:;" class="btn green button-submit" onclick="submit_form()" style="display:none;"> Submit <i class="m-icon-swapright m-icon-white"></i>
 </a>
				</div>
			</div>
			<?php } } ?>
		</form>
	</div>
</div>

	<!-- END PAGE CONTENT-->