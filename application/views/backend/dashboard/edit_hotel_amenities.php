<!-- BEGIN PAGE CONTENT-->
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green"> <span class="caption-subject bold uppercase"><i class="fa fa-plus-square"></i>&nbsp; Add Amenities</span> </div>
  </div>
  <div class="portlet-body form">
    
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
        <?php
                  $form = array(
                      'class'       => 'form-body',
                      'id'        => 'form',
                      'method'      => 'post',
      
                  ); 
                    echo form_open_multipart('unit_class_controller/hotel_amenities_edit',$form);
                  ?>
        <div class="form-body">
        <div class="row">
        <?php if(isset($input) && $input){ 
		
			foreach($input as $value) {
		?>
		<input type="hidden" value="<?php echo $value->id ; ?>" name="hid">
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="text" autocomplete="off" class="form-control" id="name" name="name" required="required" value="<?php echo $value->name ;?>">
          <label> Amenities Name<span class="required" id="b_contact_name">*</span></label>
          <span class="help-block">Asset Name</span> </div>

        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="text" autocomplete="off" class="form-control" id="ct_descname" name="desc" required="required" value="<?php echo $value->description ;?>">
          <label> Description<span class="required" id="b_contact_name">*</span></label>
          <span class="help-block">Asset Name</span> </div>

<div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="checkbox"  id="general" name="general" required="required" value="<?php echo $value->hotel_general ;?>">
          <label> Hotel General<span class="required" id="b_contact_name">*</span></label>
          <span class="help-block">Asset Name</span> </div>

</div> <div class="row">
          <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="checkbox"  id="ct_name" name="units" required="required" value="<?php echo $value->units ;?>">
          <label> Units<span class="required" id="b_contact_name">*</span></label>
          <span class="help-block">Units</span> </div>
         

          <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <select class="form-control focus" id="type1" onchange="condition_open()"  name="type" required value="<?php echo $value->type ;?>">
                <option value="0">---Select Category---</option>
                <option value="1">Free to all</option>
                <option value="2">Free for some</option>
                <option value="3">Restrivted</option>
              </select>
              <label>Type <span class="required">*</span></label>
              <span class="help-block">Category...</span> </div>

          
          <div id="AMC"><div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="text" autocomplete="off" class="form-control" onkeyup="check()" id="charge" name="charge" required="required" value="<?php echo $value->charge ;?>">
          <label> Charge<span class="required" id="b_contact_name">*</span></label>
          <span class="help-block">Asset Name</span> </div>

        
       
        

              <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="text" autocomplete="off" class="form-control" id="tax" name="tax" required="required" value="<?php echo $value->tax ;?>">
          <label> Tax<span class="required" id="b_contact_name">*</span></label>
          <span class="help-block">Asset Name</span> </div></div>


       
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <p><strong>Upload Asset Image</strong></p>
              <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/>
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                </div>
                <div>
                    <span class="btn default btn-file">
                    <span class="fileinput-new">
                    Select image </span>
                    <span class="fileinput-exists">
                    Change </span>
					<?php echo form_upload('image');?>
                    </span>
                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                    Remove </a>
                </div>
              </div>
		<?php } }?> 
              <!--<input type="file"  name="a_asset_image" />--> 
            </div>
            
            
            </div>
        </div>
        </div>
      </div>
        <div class="form-actions right">
          <button type="submit" class="btn blue">Submit</button>
          <button  type="reset" class="btn default">Reset</button>
        </div>
        <?php  form_close();  ?>      
  </div>
</div>

<!-- END CONTENT --> 
 <script>
 var gv;
 function condition_open(){
	 
	 var gv=$("#type1").val();
 
 if(gv=='1'){
	 document.getElementById('AMC').style.display='none';
 }
 else{
	  document.getElementById('AMC').style.display='block';
 }
 }
 
 
function check(){
  var a=$("#charge").val();
  var b=$("#tax").val();
  
  if(isNaN(a)){
	  alert("enter number");
   	$("#charge").val("");
  }
 if(isNaN(b)){
	  alert("enter number");
   	$("#tax").val("");
  }
}
</script>  
