<!-- BEGIN PAGE CONTENT-->

<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <span class="caption-subject bold uppercase"><i class="fa fa-plus-square"></i>&nbsp; Add Amenities</span> </div>
  </div>
  <div class="portlet-body form">
    <?php if($this->session->flashdata('err_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-danger alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
    <?php if($this->session->flashdata('succ_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-success alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
    <?php
                  if(isset($data) && $data){ 
		
			foreach($data as $value) {
				$id=$value->sec_id;
				$form=array(
                      'class'=> '',
                      'id'=> 'form',
                      'method'=> 'post',
      
                  ); 
                    echo form_open_multipart('unit_class_controller/edit_section/'.$id,$form);
                  ?>
    <div class="form-body">
      <div class="row">
        <?php 
		?>
        <input type="hidden" value="<?php echo $value->sec_id ; ?>" name="hid">
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="name" name="name" required="required" value="<?php echo $value->sec_name ;?>" placeholder="Section Name *">
            <label></label>
            <span class="help-block">Section Name *</span> </div>
        </div>        
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="house_keeping_staus" name="house_keeping_staus" required="required" value="<?php echo $value->sec_housekeeping_status ;?>" placeholder="Housekeeping Status *">
            <label></label>
            <span class="help-block">Housekeeping Status *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" onkeyup="check()" id="audit_status" name="audit_status" required="required" value="<?php echo $value->sec_audit_status ;?>" placeholder="Audit Status *">
            <label></label>
            <span class="help-block">Audit Status *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="hotel_id" name="hotel_id" required="required" value="<?php echo $value->sec_hotel_id ;?>" placeholder="Hotel Id *">
            <label></label>
            <span class="help-block">Hotel Id *</span> </div>
        </div>
        <div class="col-md-6">
          <div class="form-group form-md-line-input">
            <!--<input type="text" autocomplete="off" class="form-control" id="note" name="note" required="required" value="" placeholder="Note *">-->
            <textarea class="form-control" row="3" name="note" id="note" placeholder="Note"><?php echo $value->note ;?></textarea>
            <label></label>
            <span class="help-block">Note *</span> </div>
        </div>



        <div class="col-md-6">
          <div class="form-group form-md-line-input">
           <!-- <input type="text" autocomplete="off" class="form-control" id="desc" name="desc" required="required" value="" placeholder="Description *">-->
           <textarea class="form-control" row="3" name="desc" id="desc" placeholder="Note"><?php echo $value->sec_desc ;?></textarea>
            <label></label>
            <span class="help-block">Description *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input uploadss">
            <label>Upload Asset Image</label>
            <div class="fileinput fileinput-new" data-provides="fileinput">
              <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
              <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('image');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn blue">Submit</button>
    </div>
    <?php  form_close();  ?>
    <?php } }?>
  </div>
</div>

<!-- END CONTENT --> 
<script>
 var gv;
 function condition_open(){
	 
	 var gv=$("#type1").val();
 
 if(gv=='1'){
	 document.getElementById('AMC').style.display='none';
 }
 else{
	  document.getElementById('AMC').style.display='block';
 }
 }
 
 
function check(){
  var a=$("#charge").val();
  var b=$("#tax").val();
  
  if(isNaN(a)){
	  alert("enter number");
   	$("#charge").val("");
  }
 if(isNaN(b)){
	  alert("enter number");
   	$("#tax").val("");
  }
}
</script> 
