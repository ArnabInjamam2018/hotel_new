<?php if($this->session->flashdata('err_msg')):?>
		<div class="alert alert-danger alert-dismissible text-center" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
			<strong>
				<?php echo $this->session->flashdata('err_msg');?>
			</strong>
		</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
		<div class="alert alert-success alert-dismissible text-center" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
			<strong>
				<?php echo $this->session->flashdata('succ_msg');?>
			</strong>
		</div>
<?php endif;?>
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"> <span class="caption-subject bold uppercase"><i class="fa fa-plus-square"></i>&nbsp;  Invoice Settings</span> </div>
	</div>
	<div class="portlet-body form">
		<?php
		$form = array(
			'class' => '',
			'id' => 'form',
			'method' => 'post',

		);
		echo form_open_multipart( 'dashboard/invoice_settings', $form );
		?>
		<div class="form-body">
			<div class="row">
				<?php if(isset($invoice_settings) && $invoice_settings){
			//print_r($invoice_settings);exit;
		   foreach($invoice_settings as $invoice){
			   
		   }
		   }?>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="booking_source" required>
							<option value="1" <?php if($invoice->booking_source_inv_applicable==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->booking_source_inv_applicable==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Booking Sourcesss Invoice *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="nature_visit" required>
							<option value="1" <?php if($invoice->nature_visit_inv_applicable==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->nature_visit_inv_applicable==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Nature Of Visit Invoice *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="booking_note" required>
							<option value="1" <?php if($invoice->booking_note_inv_applicable==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->booking_note_inv_applicable==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Booking Note Invoice *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="company_details" required>
							<option value="1" <?php if($invoice->company_details_inv_applicable==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->company_details_inv_applicable==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Company Details Invoice *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="service_tax" required>
							<option value="1" <?php if($invoice->service_tax_applicable==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->service_tax_applicable==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Service Tax Invoice *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="service_charg" required>
							<option value="1" <?php if($invoice->service_charg_applicable==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->service_charg_applicable==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Service Charge Invoice *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="luxury_tax" required>
							<option value="1" <?php if($invoice->luxury_tax_applicable==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->luxury_tax_applicable==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Luxary Tax Invoice *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="authorized_signatory" required>
							<option value="1" <?php if($invoice->authorized_signatory_applicable==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->authorized_signatory_applicable==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Authorized Signatory *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<input type="text" class="form-control" name="invoice_pref" value="<?php echo $invoice->invoice_pref; ?>" required="required"/>
						<label></label>
						<span class="help-block">Invoice Prefix *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<input type="text" class="form-control" name="invoice_suf" value="<?php echo $invoice->invoice_suf; ?>" required="required"/>
						<label></label>
						<span class="help-block">Invoice Suffix *</span> </div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="unit_no" required>
							<option value="1" <?php if($invoice->unit_no==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->unit_no==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Unit Number *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="unit_cat" required>
							<option value="1" <?php if($invoice->unit_cat==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->unit_cat==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Unit Category *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="fd_plan" required>
							<option value="1" <?php if($invoice->invoice_setting_food_paln==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->invoice_setting_food_paln==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Food Plan *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="service" required>
							<option value="1" <?php if($invoice->invoice_setting_service==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->invoice_setting_service==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Service *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="extra_charge" required>
							<option value="1" <?php if($invoice->invoice_setting_extra_charge==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->invoice_setting_extra_charge==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Extra Charge *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="laundry" required>
							<option value="1" <?php if($invoice->invoice_setting_laundry==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->invoice_setting_laundry==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Laundry *</span> </div>
				</div>




				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<span class="form-control" contenteditable="true" onBlur="saveToDatabase(this,'test','<?php echo $invoice->id; ?>')">
							<?php echo $invoice->test; ?>
						</span>
						<label></label>
						<span class="help-block">text</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<input type="text" class="form-control" name="total_tax" value="<?php echo $invoice->total_tax; ?>" required="required" placeholder="Total Tax *"/>
						<label></label>
						<span class="help-block">Total Tax *</span> </div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="printer" required>
							<option value="normal" <?php if($invoice->printer_settings=='normal')echo "selected";?>>Ink-jet/Laser Printer</option>
							<option value="dotMatrix" <?php if($invoice->printer_settings=='dotMatrix')echo "selected";?>>dotMatrix Printer</option>
						</select>
						<label></label>
						<span class="help-block">Printer *</span> </div>
				</div>
                
                	<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="logo" required>
							<option value="1" <?php if($invoice->logo==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->logo==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Logo *</span> </div>
				</div>
				<!-- add subtext-->
	<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="hotel_sub_text" required>
							<option value="1" <?php if($invoice->hotel_sub_text==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->hotel_sub_text==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Hotel Group Text *</span> </div>
				</div>
				
				
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="font_size_gp" required>
							<option value="10" <?php if($invoice->font_size_gp==10) echo "selected";?>>10px</option>
							<option value="11" <?php if($invoice->font_size_gp==11) echo "selected";?>>11px</option>
							<option value="12" <?php if($invoice->font_size_gp==12) echo "selected";?>>12px</option>
							<option value="13" <?php if($invoice->font_size_gp==13) echo "selected";?>>13px</option>
							<option value="14" <?php if($invoice->font_size_gp==14) echo "selected";?>>14px</option>
							<option value="15" <?php if($invoice->font_size_gp==15) echo "selected";?>>15px</option>
							<option value="16" <?php  if($invoice->font_size_gp==16) echo "selected";?>>16px</option>
							<option value="17" <?php  if($invoice->font_size_gp==17) echo "selected";?>>17px</option>
							<option value="18" <?php  if($invoice->font_size_gp==18) echo "selected";?>>18px</option>
							<option value="19" <?php  if($invoice->font_size_gp==19) echo "selected";?>>19px</option>
							<option value="20" <?php  if($invoice->font_size_gp==20) echo "selected";?>>20px</option>
							<option value="21" <?php  if($invoice->font_size_gp==21) echo "selected";?>>21px</option>
							<option value="22" <?php  if($invoice->font_size_gp==22) echo "selected";?>>22px</option>
							<option value="23" <?php  if($invoice->font_size_gp==23) echo "selected";?>>23px</option>
							<option value="24" <?php  if($invoice->font_size_gp==24) echo "selected";?>>24px</option>
							<option value="25" <?php  if($invoice->font_size_gp==25) echo "selected";?>>25px</option>
							<option value="26" <?php if($invoice->font_size_gp==26) echo "selected";?>>26px</option>
							<option value="27" <?php if($invoice->font_size_gp==27) echo "selected";?>>27px</option>
							<option value="28" <?php if($invoice->font_size_gp==28) echo "selected";?>>28px</option>
							<option value="29" <?php if($invoice->font_size_gp==29) echo "selected";?>>29px</option>
							<option value="30" <?php if($invoice->font_size_gp==30) echo "selected";?>>30px</option>
							<option value="31" <?php if($invoice->font_size_gp==31) echo "selected";?>>31px</option>
							<option value="32" <?php  if($invoice->font_size_gp==32) echo "selected";?>>32px</option>
							<option value="33" <?php  if($invoice->font_size_gp==33) echo "selected";?>>33px</option>
							<option value="34" <?php  if($invoice->font_size_gp==34) echo "selected";?>>34px</option>
							<option value="35" <?php  if($invoice->font_size_gp==35) echo "selected";?>>35px</option>
							<option value="36" <?php  if($invoice->font_size_gp==36) echo "selected";?>>36px</option>
							<option value="37" <?php  if($invoice->font_size_gp==37) echo "selected";?>>37px</option>
							<option value="38" <?php  if($invoice->font_size_gp==38) echo "selected";?>>38px</option>
							<option value="39" <?php  if($invoice->font_size_gp==39) echo "selected";?>>39px</option>
							<option value="40" <?php  if($invoice->font_size_gp==40) echo "selected";?>>40px</option>
						</select>
						<label></label>
						<span class="help-block">HOtel Group Font Size *</span> </div>
				</div>


					<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="pos_app" required>
							<option value="1" <?php if($invoice->pos_app==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->pos_app==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">POS Settings *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="paid_pos_item_show" required>
							<option value="1" <?php if($invoice->paid_pos_item_show==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->paid_pos_item_show==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">POS Paid Item Show Settings *</span> </div>
				</div>
					<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="adjust_app" required>
							<option value="1" <?php if($invoice->adjust_app==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->adjust_app==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Adjustments Settings *</span> </div>
				</div>
				<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="disc_app" required>
							<option value="1" <?php if($invoice->disc_app==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->disc_app==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Discount Settings *</span> </div>
				</div>
					<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="pos_seg" required>
							<option value="1" <?php if($invoice->pos_seg==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->pos_seg==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">POS Segregation Settings *</span> </div>
				</div>
					<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="fd_app" required>
							<option value="1" <?php if($invoice->fd_app==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->fd_app==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Food Name Settings *</span> </div>
				</div>
				
					<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="invoice_no" required>
							<option value="1" <?php if($invoice->invoice_no==1)echo "selected";?>>Applicable</option>
							<option value="0" <?php if($invoice->invoice_no==0)echo "selected";?>>Not Applicable</option>
						</select>
						<label></label>
						<span class="help-block">Invoice No *</span> </div>
				</div>
            <div class="col-md-4"> 
         	<div class="form-group form-md-line-input">
                <input type="text" id="position-bottom-right" name="hotel_color" class="form-control demo" data-position="bottom right" value="<?php
				if(isset($invoice->hotel_color)) {
					
					if($invoice->hotel_color!='') echo $invoice->hotel_color; else  echo "#0088cc";
				} 
				else {
					echo "#0088cc";
				} ?>" placeholder="Mention the Color You want to Apply To Your Event *">
                <label></label>
                <span class="help-block">Mention the Color You want to Apply To Your Hotel Name *</span> </div>

			</div>
			
			<div class="col-md-4">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="font_size" required>
							<option value="10" <?php if($invoice->font_size==10) echo "selected";?>>10px</option>
							<option value="11" <?php if($invoice->font_size==11) echo "selected";?>>11px</option>
							<option value="12" <?php if($invoice->font_size==12) echo "selected";?>>12px</option>
							<option value="13" <?php if($invoice->font_size==13) echo "selected";?>>13px</option>
							<option value="14" <?php if($invoice->font_size==14) echo "selected";?>>14px</option>
							<option value="15" <?php if($invoice->font_size==15) echo "selected";?>>15px</option>
							<option value="16" <?php  if($invoice->font_size==16) echo "selected";?>>16px</option>
							<option value="17" <?php  if($invoice->font_size==17) echo "selected";?>>17px</option>
							<option value="18" <?php  if($invoice->font_size==18) echo "selected";?>>18px</option>
							<option value="19" <?php  if($invoice->font_size==19) echo "selected";?>>19px</option>
							<option value="20" <?php  if($invoice->font_size==20) echo "selected";?>>20px</option>
							<option value="21" <?php  if($invoice->font_size==21) echo "selected";?>>21px</option>
							<option value="22" <?php  if($invoice->font_size==22) echo "selected";?>>22px</option>
							<option value="23" <?php  if($invoice->font_size==23) echo "selected";?>>23px</option>
							<option value="24" <?php  if($invoice->font_size==24) echo "selected";?>>24px</option>
							<option value="25" <?php  if($invoice->font_size==25) echo "selected";?>>25px</option>
							<option value="26" <?php if($invoice->font_size==26) echo "selected";?>>26px</option>
							<option value="27" <?php if($invoice->font_size==27) echo "selected";?>>27px</option>
							<option value="28" <?php if($invoice->font_size==28) echo "selected";?>>28px</option>
							<option value="29" <?php if($invoice->font_size==29) echo "selected";?>>29px</option>
							<option value="30" <?php if($invoice->font_size==30) echo "selected";?>>30px</option>
							<option value="31" <?php if($invoice->font_size==31) echo "selected";?>>31px</option>
							<option value="32" <?php  if($invoice->font_size==32) echo "selected";?>>32px</option>
							<option value="33" <?php  if($invoice->font_size==33) echo "selected";?>>33px</option>
							<option value="34" <?php  if($invoice->font_size==34) echo "selected";?>>34px</option>
							<option value="35" <?php  if($invoice->font_size==35) echo "selected";?>>35px</option>
							<option value="36" <?php  if($invoice->font_size==36) echo "selected";?>>36px</option>
							<option value="37" <?php  if($invoice->font_size==37) echo "selected";?>>37px</option>
							<option value="38" <?php  if($invoice->font_size==38) echo "selected";?>>38px</option>
							<option value="39" <?php  if($invoice->font_size==39) echo "selected";?>>39px</option>
							<option value="40" <?php  if($invoice->font_size==40) echo "selected";?>>40px</option>
						</select>
						<label></label>
						<span class="help-block">Font Size *</span> </div>
				</div>
			
			<div class="col-md-12">
					<div class="form-group form-md-line-input">
						<textarea name="declaration" class="form-control" placeholder=" Declaration Text"><?php if(isset( $invoice->declaration)) echo  stripslashes($invoice->declaration);?></textarea>
						<label></label>
						<span class="help-block">Declaration Text *</span>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group form-md-line-input">
						<textarea name="ft_text" class="form-control" placeholder=" SB Footer Text"><?php if(isset( $invoice->ft_text)) echo  stripslashes($invoice->ft_text);?></textarea>
						<label></label>
						<span class="help-block">SB Footer Text *</span>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group form-md-line-input" id="Gbtext">
						<textarea name="GBft_text" class="form-control" placeholder="GP Footer Text"><?php if(isset( $invoice->GBft_text)) echo  $invoice->GBft_text;?></textarea>
				        <label></label>
				     	<span class="help-block">GB Footer Text *</span>
					</div>
				</div>
			</div>
		</div>
		<div class="form-actions right">
			<button type="submit" class="btn blue" onclick="sbtext();gbtext()"> Update Settings</button>
		</div>
		<?php form_close(); ?>
	</div>
</div>

<!-- END CONTENT -->
<script>
	function amc_open() {
		document.getElementById( 'AMC' ).style.display = 'block';
	}

	function amc_close() {
		document.getElementById( 'AMC' ).style.display = 'none';
	}
	$( document ).on( 'blur', '#c_valid_from', function () {
		$( '#c_valid_from' ).addClass( 'focus' );
	} );
	$( document ).on( 'blur', '#ct_name2', function () {
		$( '#ct_name2' ).addClass( 'focus' );
	} );
	$( document ).on( 'blur', '#ct_name1', function () {
		$( '#ct_name1' ).addClass( 'focus' );

	} );

	function saveToDatabase( editableObj, column, id ) {
		$( editableObj ).css( "background", "#FFF url(<?php echo base_url()?>upload/loaderIcon.gif) no-repeat right" );
		$.ajax( {
			url: "<?php echo base_url()?>setting/inlineEdit",
			type: "POST",
			data: 'column=' + column + '&editval=' + editableObj.innerHTML + '&id=' + id,
			success: function ( data ) {
				$( editableObj ).css( "background", "#FDFDFD" );
			}
		} );
	}

	$( ".checkbox" ).change( function () {
		if ( this.checked ) {
			var editableObj = '1';
		} else {
			var editableObj = '0';
		}
		var col = 'color';
		var id = '1';
		$.ajax( {
			url: "<?php echo base_url()?>setting/inlineEdit",
			type: "POST",
			data: 'column=' + col + '&editval=' + editableObj + '&id=' + id,
			success: function ( data ) {}
		} );
	} );
</script>