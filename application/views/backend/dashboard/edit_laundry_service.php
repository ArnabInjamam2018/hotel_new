<!-- 17.11.2015-->
<script src="<?php echo base_url();?>assets/global/plugins/typeahead1/jquery.typeahead.js"></script>
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <!-- 19.07.2016-->
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Edit Laundry Service</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

	$form = array(
		'class' 			=> '',
		'id'				=> 'form',
		'method'			=> 'post',								
	);
	
	

	echo form_open_multipart('unit_class_controller/edit_laundry_service',$form);

	?>
    <div class="form-body"> 
      <div class="row">
		<?php 
//echo "<Pre>";
//print_r($laundry_service);
		$type=$laundry_service->laundry_booking_type;
		if($type=="sb")
			$type="1";
		else if($type=="gb")
			$type="2";
		else
			$type="0";
		?>
		<input type="hidden" name="htype" id="htype" value="<?php echo $type; ?>">
		<div class="col-md-4">
            <div class="form-group form-md-line-input" ><!-- bs-select  -->
              <select  class="form-control bs-select" value="<?php echo $type; ?>" id="booking_type" name="booking_type" onchange="booking_type1(this.value)" required="required" placeholder="Guest Type">
                <option value="0" <?php if($type=="0")echo "selected"; ?> >Ad Hoc</option>
                <option value="1" <?php if($type=="1")echo "selected"; ?> >Single Booking</option>
                <option value="2" <?php if($type=="2")echo "selected"; ?> >Group Booking</option>
				
              </select>
              <label></label>
			  
              <span class="help-block">Booking Type*</span> </div>
        </div>
	<input type="hidden" name="guest_booking_id" id="guest_booking_id"> 
	<input type="hidden" name="quantity" id="quantity"> 
	 <input type="hidden"  value="<?php echo $laundry_service->laundry_booking_id;?>" name="name">
		
<?php $single_booking=$this->unit_class_model->get_single_booking(); 

if(isset($laundry_service)){
	if($laundry_service->booking_id!=''){
		$b_id=$laundry_service->booking_id;
	}
	else{
		$b_id="0";
	}
}
//echo "<pre>";
//print_r($laundry_service);

?>



<input type="hidden" id="h_g_laundry_id" name="h_g_laundry_id" value="<?php echo $laundry_service->laundry_id?>">
<input type="hidden" id="h_g_guest_id" value="<?php echo $laundry_service->guest_id?>">
<input type="hidden" id="h_g_due" value="<?php echo $laundry_service->payment_due?>">
<input type="hidden" id="h_g_id" value="<?php echo $laundry_service->laundry_id?>">
<input type="hidden" id="h_g_name" value="<?php echo $laundry_service->laundry_guest_name?>">
<input type="hidden" id="h_g_type" value="<?php echo $laundry_service->laundry_guest_type?>">
<input type="hidden" id="h_g_receive" value="<?php echo $laundry_service->receiv_date?>">
<input type="hidden" id="h_g_delivery" value="<?php echo $laundry_service->delivery_date?>">
<input type="hidden" id="h_g_request" value="<?php echo $laundry_service->laundry_special_request?>">
<input type="hidden" id="h_g_bill" value="<?php echo $laundry_service->billing_preference?>">


<input type="hidden" id="h_g_tax" value="<?php echo $laundry_service->service_tax?>">
<input type="hidden" id="h_g_vat" value="<?php echo $laundry_service->vat?>">
<input type="hidden" id="h_g_discount" value="<?php echo $laundry_service->discount?>">
<input type="hidden" id="h_tax" value="<?php echo $laundry_service->tax?>">
<input type="hidden" id="h_g_charge" value="<?php echo $laundry_service->service_charge?>">

		<div class="col-md-4" id="single_booking1" hidden>
            <div class="form-group form-md-line-input" >
			
              <select  class="form-control bs-select" id="single_booking" onchange="get_guest_by_booking_id(this.value)"  name="single_booking"  required="required" placeholder="booking" >
                <option value="0" > Select</option>
				<?php
				if(isset($single_booking) && $single_booking){
					
					foreach($single_booking as $single){
				?>
				<option value="<?php echo $single->booking_id;  ?>" <?php if($single->booking_id==$b_id) echo "selected"; ?>  ><?php echo $single->booking_id.' - '.$single->cust_name ?></option>
				<?php }}  ?>
              </select>
              <label></label>
              <span class="help-block">Single Booking</span> </div>
        </div>
		
		<?php $group_booking=$this->unit_class_model->get_group_booking();//print_r($group_booking);
		if(isset($group_booking) && $group_booking){
		
		?>
		<div class="col-md-4" id="group_booking1" hidden>
            <div class="form-group form-md-line-input" >
			
              <select  class="form-control bs-select" id="group_booking" onchange="get_guest_by_booking_id(this.value)" name="group_booking"    required="required" placeholder="booking" >
				<option value="0" > Select</option>
			  <?php
				foreach($group_booking as $group){
				?>
				<option value="<?php echo $group->id ?>" > <?php echo $group->id.' - '.$group->name ?></option>
				<?php }}  ?>
              </select>
              <label></label>
              <span class="help-block">Group Booking</span> </div>
        </div>
	
	<input type="hidden" id="hidden_id" name="hidden_id" >
			 <div class="col-md-3" id="gname_adhoc" hidden>
          <div class="form-group form-md-line-input">
            <!--<div class="input-group">
              <input type="text" class="form-control" id="Guest_name" name="Guest_name" onblur="get_guest_by_id(this.value)"  required="required" placeholder="Guest Name" value="">
              <span class="input-group-btn">
              <button class="btn green" type="button" id="addGuest"><i class="fa fa-eye" aria-hidden="true"></i></button>
              </span> </div>-->
			  <div class="typeahead__container">
                                <div class="typeahead__field">
                                    <span class="typeahead__query">
										
                                        <input class="js-typeahead-user_v1 form-control input-circle-right" name="g_name[query]" id="Guest_name1"  type="search" placeholder="search guest here..." autocomplete="off" value="<?php echo $laundry_service->laundry_guest_name ?>">
                                    </span>
									<label for="form_control_1">Input Group</label>
                                </div>
                            </div>
							
							 	
												
			  <input type="hidden" name="Guest_name" id="Guest_name">
			  <input type="hidden" name="Guest_name_new" id="Guest_name_new">
          </div>
        </div>
            

       
		<!--<div class="col-md-4"><!--onchange="check_corporate(this.value)"
            <div class="form-group form-md-line-input" >
              <select  class="form-control bs-select" id="guest_type1" name="guest_type" required="required">
                <option value="" disabled="disabled" selected="selected">Guest Type</option>
                <option value="Family" >Family</option>
                <option value="Bachelor">Bachelor</option>
                <option value="Tourist">Tourist</option>
                <!--<option value="Sr_citizen">Sr Citizen</option>
                <option value="Corporate">Corporate</option>
                <option value="VIP">VIP</option>
                <option value="Government_official">Government Official</option>
                <option value="Military">Military </option>
                <option value="internal_staff">Internal Staff</option>
              </select>
              <label></label>
              <span class="help-block">Guest Type *</span> </div>
        </div>-->
		
       
	    <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control  date-picker" id="received_date" name="received_date" placeholder="Recieved Date">
              <label></label>
              <span class="help-block">Recieved Date</span> </div>
        </div>
		
		 <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control  date-picker" id="delivery_date" name="delivery_date" placeholder="Delevery Date">
              <label></label>
              <span class="help-block">Delevery Date</span>
			  </div>
        </div>
		
			<div class="col-md-3" id="Billing_Preference">
          <div class="form-group form-md-line-input" >
            <select  class="form-control bs-select" onchange="billing(this.value)" id="billing_pref" name="billing_pref" required="required">
              <option value="" disabled="disabled" selected="selected">Billing Preference</option>
              <option value="Bill Seperately" >Bill Seperately</option>
              <option value="Add to Booking Bill">Add to Booking Bill</option>
            </select>
            <label></label>
            <span class="help-block">Billing Preference</span> </div>
        </div>
		
        <div class="col-md-4"> 
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="special_request" name="special_request"   placeholder="Special Request">
              <label></label>
              <span class="help-block">Special Request</span> 
            </div> 
        </div>
		
      
			
     <div class="col-md-12" style="padding-top:30px;">
          <div class="">
            <table class="table table-striped table-hover table-bordered mass-book" id="items">
              <thead>
                <tr>
                  <th width="10%">Cloth Type</th>
                  <th width="10%">Fabric</th>
                  <th width="7%">Size</th>
                  <th width="10%">Condition</th>
                  <th width="10%">Service</th>
                  <th width="7%">Qty</th>
                  <th width="10%">Color</th>
                  <th width="7%">Brand</th>
                  <th width="7%">Price</th>
                  <th width="7%">Total</th>
                  <!-- <th width="7%">Tax</th>
				  <th width="10%">Note</th>-->
                  <th width="5%">Action</th>
                </tr>
              </thead>
              <tbody>
			  <?php
				$total=0;
				$total_item=0;

			  if(isset($l_items) && $l_items){
				 // echo "<pre>";
				 // print_r($l_items);
				  foreach($l_items as $itms){
					 
			  $total+=$itms->item_price*$itms->laundry_item_qty;
			  $total_item+=$itms->laundry_item_qty;
			  ?>
			  <input type="hidden" name="quantity1" id="quantity1" value="<?php echo $total_item; ?>">
			 
			  <tr id="row2_<?php echo $itms->laundry_id  ?>">
			 <?php
			 $a=$this->unit_class_model->get_cloth_type_by_id($itms->cloth_type_id);
		$b=$this->unit_class_model->get_fabric_type_by_id($itms->fabric_type_id);
		$c=$this->unit_class_model->get_service_type_by_id($itms->laundry_service_type_id);
		if($itms->laundry_item_color!=0){	
		$d=$this->unit_class_model->get_color_by_id($itms->laundry_item_color);
		$s=$d->color_name;
		}
		else{
			$s='none';
		}
		
		
		
            $name =$a->laundry_ct_name;
            $fabric =$b->fabric_type;
            $service =$c->laundry_type_name;
            $color =$s;
      ?>
			  <td><?php echo $name; ?></td>
			  <td><?php echo $fabric; ?></td>
			  <td><?php echo $itms->item_size; ?></td>
			  <td><?php echo $itms->item_condition; ?></td>
			  <td><?php echo $service; ?></td>
			  <td><?php echo $itms->laundry_item_qty; ?></td>
			  <td><?php echo $color; ?></td>
			  <td><?php echo $itms->laundry_item_brand; ?></td>
			  <td><?php echo $itms->item_price; ?></td>
			 	<!--laundry_id-->
			  
			  <td><?php echo $itms->item_price*$itms->laundry_item_qty; ?></td>
			  <td><a  class="btn red btn-sm" onclick="removeRow_Ajx('<?php echo $itms->laundry_id ?>','<?php echo $itms->item_price*$itms->laundry_item_qty; ?>','<?php echo $itms->laundry_item_qty; ?>')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
			  </tr>
			  <?php }}?>
			   <input type="hidden" name="htotal" id="htotal" value='<?php echo $total;  ?>'>
                <tr id="abc1">
                  <td class="hidden-480 form-group"><?php 
					$cloth_type=$this->unit_class_model->all_laundry_cloth_type();					
				  ?>
                    <select class="form-control bs-select"   name="cloth_type[]" id="cloth_type" onchange="get_laundry_rates();total_amt()"  placeholder="Cloth Type" >
                      <option value="0">Select</option>
                      <?php foreach($cloth_type as $cloth){ ?>
                      <option value="<?php echo $cloth->laundry_ct_id;?>"><?php echo $cloth->laundry_ct_name?></option>
                      <?php } ?>
                    </select></td>
                  <td class="hidden-480 form-group"><?php 
					$fabric_type=$this->unit_class_model->all_laundry_fabric_type();					
				  ?>
                    <select class="form-control bs-select"  onchange="get_laundry_rates();total_amt()" name="fabric[]" id="fabric"  placeholder="Fabric Type" >
                      <option value="0">Select</option>
                      <?php foreach($fabric_type as $fabric){ ?>
                      <option value="<?php echo $fabric->fabric_id ?>"><?php echo $fabric->fabric_type ?></option>
                      <?php } ?>
                    </select></td>
                  <td class="hidden-480 form-group"><?php 
					$discount_rule=$this->unit_class_model->discount_rules();					
				  ?>
                    <select class="form-control bs-select"   name="size[]" id="size"  placeholder="Size" >
                      <option value="0"  selected="">Select Size</option>
                      <option value="kid">Kid</option>
                      <option value="Teen">Teen</option>
                      <option value="Adult">Adult</option>
                      <option value="N/A">N/A</option>
                    </select></td>
                  <td class="hidden-480 form-group"><select class="form-control bs-select"   name="condition[]" id="condition"  placeholder="Condition" >
                      <option value="New">New</option>
                      <option value="Used">Used</option>
                      <option value="Old">Old</option>
                    </select></td>
                  <td class="hidden-480 form-group"><?php 
					$service_type=$this->unit_class_model->all_laundry_service_type();					
				  ?>
                    
                    <!--onchange="get_price(this.value)"-->
                    
                    <select class="form-control bs-select" onchange="get_laundry_rates();total_amt()"  name="service[]"  id="service"  placeholder="Laundry Service" >
                      <option value="0"  selected="">Select Laundry Service</option>
                      <?php foreach($service_type as $service){ ?>
                      <option value="<?php echo $service->laundry_type_id ?>"><?php echo $service->laundry_type_name ?></option>
                      <?php } ?>
                    </select></td>
                  <td class="hidden-480 form-group"><input autocomplete="off" type="text" class="form-control" name="qty[]" onblur="total_amt()"  id="qty"  placeholder="Qty" onkeypress='return onlyNos(event, this);' ></td>
                  <td class="hidden-480 form-group"><?php 
					$laundry_color=$this->unit_class_model->all_laundry_color();					
				  ?>
                    <select class="form-control bs-select"   name="color[]" id="color"  placeholder="Color" >
                      <option value="0">Select</option>
                      <?php foreach($laundry_color as $color){ ?>
                      <option value="<?php echo $color->color_id ?>"><?php echo $color->color_name ?></option>
                      <?php } ?>
                    </select></td>
                  <td class="hidden-480 form-group"><input autocomplete="off" type="text" class="form-control" id="brand" name="brand[]"  placeholder="Brand"></td>
                  <td class="hidden-480 form-group"><input autocomplete="off" type="text" class="form-control" id="price" name="price[]" onblur="total_amt()"  placeholder="Price"></td>
                  <td class="hidden-480 form-group"><input autocomplete="off" type="text" class="form-control"  id="total" name="total[]"   placeholder="Total" readonly></td>
                  <!--<td class="hidden-480 form-group">
				<select class="form-control bs-select"   name="tax[]" id="tax"  placeholder="Tax" required>
				<option value="dis">select</option>
                  <option value="Yes">Yes</option>
                  <option value="NO">NO</option>
                  
                  
                  
                </select>
				</td>
				<td class="hidden-480 form-group">
              <input autocomplete="off" type="text" class="form-control" id="note" name="note[]"   placeholder="Note(Tax value)" readonly>
				</td>-->
                  <td><a class="btn green btn-xs"  id="additems"><i class="fa fa-plus" aria-hidden="true"></i></a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-md-12" align="right" style="font-size: 120%;"> Total Bill Amount : <i class="fa fa-inr" style="font-size: 85%;"><span id="total_amount"></span></i>
          <input type="hidden" name="hidden_total" id="hidden_total">
        </div>
        <input type="hidden" name="total_amount_hidden" id="total_amount_hidden">
        <div class="col-md-2">
        	<div class="form-group form-md-line-input">
              <select class="form-control bs-select"   name="tax[]" id="tax1"   placeholder="Tax" required>
                <option value="dis">select</option>
                <option value="Yes">Yes</option>
                <option value="No">NO</option>
              </select>
              <label></label>
              <span class="help-block">Select Tax</span>
          	</div>
        </div>
        <div class="col-md-2">
        	<div class="form-group form-md-line-input">
          		<input autocomplete="off" type="text" class="form-control"  id="discount" name="discount"  onkeypress='return onlyNos(event, this);' onkeyup="g_total1(this.value)"  placeholder="Discount">
                <label></label>
                <span class="help-block">Discount</span>
            </div>
        </div>
        <div  id="div_tax_type"></div>
        
        <div class="col-md-2">
        	<div class="form-group form-md-line-input">
          		<input autocomplete="off" type="text" class="form-control"  id="grand_total" name="grand_total"  placeholder="Grand Total" readonly>
                <label></label>
                <span class="help-block">Grand total</span>
          	</div>
        </div>        
        <div class="col-md-12" id="pay_id" style="text-align:center;margin-top: 20px; display:none" >
          <div class="btn-group">
            <label for="autocellwidth" class="auto_cl btn grey-cascade" id="auto_cl_id" style="background-color:#39B9A1;">
              <input type="checkbox" id="autocellwidth" class="toggle" style="display:none;">
              <span id="disp">Pay Now</span></label>
          </div>
        </div>
        <div id="pay" style="display:none;">
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label>Total Amount Paybale<span class="required">*</span></label>
                <input type="text"  class="form-control input-sm" id="pay_amount"  value="" readonly/>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Due Amount<span class="required">*</span></label>
                <input type="text" class="form-control input-sm" id="due_amount"  name="due_amount" value="<?php echo $laundry_service->payment_due?>" readonly>
                <input type="hidden" id="due_amount_hid" value="<?php echo $laundry_service->payment_due?>">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Amount <span class="required"> * </span> </label>
                <input type="number" id="add_amount" class="form-control input-sm" name="pay_amount" placeholder="Enter Amount" onkeyup="check_amount(this.value);" min=0   />
                <input type="hidden" id="booking_id" value="">
                <input type="hidden" id="booking_status_id" value="">
              </div>
              <script type="text/javascript">
                    function check_amount(v){
						var f=0;
						//alert(v);
						//var amount = $("#pay_amount").val();
						var amount = $("#due_amount").val();
						var amount1 = $("#due_amount_hid").val();
						//alert(amount);
					//	alert(amount1);
						
						
						
						if(v){
						if(parseInt(amount1) < parseInt(v)) {
								
									swal({
										title: "Amount Should Be Smaller",
										text: "(The amount should be less than the due amount Rs: +due_amount)",
										type: "warning"
									},
									function(){
										
									});
									$("#add_amount").val("");
									$("#due_amount").val(amount1);
									return false;
						} else {
							f=parseFloat(amount1) - parseFloat(v);
							if(v){
							$("#due_amount").val(f);
							}else{
								$("#due_amount").val(amount1);
							}
							
							return true;
						}
						}else{
							//alert('blank');
							var du_amo=$('#due_amount_hid').val();
							//$("#due_amount").val(du_amo);
							$("#due_amount").val(amount1);
						}
                    }       
              </script> 
            </div>
            <div class="col-md-3">
              <label>Profit Center <span class="required"> * </span> </label>
              <select name="p_center" class="form-control input-sm" id="p_center">
                <?php $pc=$this->dashboard_model->all_pc();?>
                <?php
						  $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
						  ?>
                <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
                <?php $pc=$this->dashboard_model->all_pc1();
							foreach($pc as $prfit_center){
							?>
                <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                <?php }?>
              </select>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Payment Mode <span class="required"> * </span> </label>
                <select  class="form-control input-sm" placeholder=" Booking Type" id="pay_mode" name="pay_mode" onChange="paym(this.value);" >
                  <option value="" disabled selected>Select Payment Mode</option>
                  <?php 
						$mop = $this->dashboard_model->get_payment_mode_list();
						
						
						if($mop != false){
							foreach($mop as $mp){
				    ?>
                  <option value="<?php echo $mp->p_mode_name; ?>"><?php echo $mp->p_mode_des; ?></option>
                  <?php } } ?>
                </select>
              </div>
            </div>
            <div id="cards" style="display:none;">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Card type <span class="required"> * </span> </label>
                  <select name="card_type" class="form-control input-sm" placeholder="Card type" id="card_type">
                    <option value="" disabled selected>Select Card Type</option>
                    <option value="Cr">Credit Card</option>
                    <option value="Dr">Debit Card</option>
                    <option value="gift">Gift Card</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Bank Name <span class="required"> * </span> </label>
                  <input type="text" name="card_bank_name" class="form-control input-sm" placeholder="Bank name" id="bankname" >
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Card NO<span class="required"> * </span> </label>
                  <input type="text" name="card_no" class="form-control input-sm" placeholder="Card no" id="card_no" >
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Name on Card<span class="required"> </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="Name on Card" name="card_name" id="card_name" >
                </div>
              </div>
              <div class="col-md-3">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Exp Month<span class="required"> </span> </label>
                      <select name="card_expm" class="form-control input-sm" placeholder="Month" id="card_expm">
                        <option value="" disabled selected>Select Month</option>
                        <?php 
                            $MonthArray = array(
                            "1" => "January", "2" => "February", "3" => "March", "4" => "April",
                            "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                            "9" => "September", "10" => "October", "11" => "November", "12" => "December",);
                            
                                for($i=1;$i<13;$i++)
                                {
                            ?>
                        <option value="<?php //echo $MonthArray[$i];
						echo $i;
						?>"><?php echo $MonthArray[$i]; ?></option>
                        <?php }  ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Exp year<span class="required"> </span> </label>
                      <select name="card_expy" class="form-control input-sm" placeholder="Year" id="card_expy">
                        <option value="" disabled selected>Select year</option>
                        <?php 
                                for($i=2016;$i<2075;$i++)
                                {
                            ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php }  ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>CVV<span class="required"> </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="CVV" name="card_cvv" id="card_cvv" >
                </div>
              </div>
              
              <!--<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="payment_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_ca">
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Transaction Declined</option>
					 </select>                  
                  </div>
                </div>--> 
              
            </div>
            <div id="fundss" style="display:none;">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Bank Name <span class="required"> * </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="Bank name" name="ft_bank_name" id="bankname_f" >
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>A/C No<span class="required"> * </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="Acc no" name="ft_account_no" id="ac_no" >
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>IFSC Code<span class="required"> * </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="IFSC code" name="ft_ifsc_code" id="ifsc" >
                </div>
              </div>
              
              <!--<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="payment_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_f">
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Declined</option>
					 </select>                  
                  </div>
                </div>--> 
              
            </div>
            <div id="cheque" style="display:none;">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Bank Name <span class="required"> * </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="Bank name" name="chk_bank_name" id="bankname_c" >
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Cheque No<span class="required"> * </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="Cheque no" name="checkno" id="chq_no" >
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Drawer Name<span class="required"> * </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="Drawer Name" name="chk_drawer_name" id="drw_name_c" >
                </div>
              </div>
              
              <!--<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="payment_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_c" >
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Bounced</option>
					 </select>                  
                  </div>
                </div>--> 
              
            </div>
            <div id="draft" style="display:none;">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Bank Name <span class="required"> * </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="Bank name" name="draft_bank_name" id="bankname_d" >
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Draft No<span class="required"> * </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="Draft no" name="draft_no" id="drf_no" >
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Drawer Name<span class="required"> * </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="Drawer Name" name="draft_drawer_name" id="drw_name_d" >
                </div>
              </div>
              
              <!--<div class="col-md-3">
                  <div class="form-group">
                    <label>Payment Status<span class="required"> * </span> </label>
					 <select name="payment_type" class="form-control input-sm" placeholder="Payment Status" id="p_status_d" >
						<option value="Done" selected>Payment Recieved</option>
						<option value="Pending">Payment Processing</option>
						<option value="Cancel">Bounced</option>
					 </select>                  
                  </div>
                </div>--> 
              
            </div>
            <div id="ewallet" style="display:none;">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Wallet Name <span class="required"> * </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="Wallet Name" name="wallet_name" id="w_name" >
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Transaction ID<span class="required"> * </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="Transaction ID" name="wallet_tr_id" id="tran_id" >
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Recieving Acc<span class="required"> * </span> </label>
                  <input type="text" class="form-control input-sm" placeholder="Recieving Acc" name="wallet_rec_acc" id="recv_acc" >
                </div>
              </div>
              <!--<div class="col-md-3">
                <div class="form-group">
                  <label>Payment Status<span class="required"> * </span> </label>
                  <select name="p_status_w" class="form-control input-sm" placeholder="Payment Status" id="p_status_w" >
                    <option value="Done" selected>Payment Recieved</option>
                    <option value="Pending">Payment Processing</option>
                    <option value="Cancel">Transaction Declined</option>
                  </select>
                </div>
              </div>-->
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Payment Status<span class="required"> * </span> </label>
                <select name="p_status" class="form-control input-sm" placeholder="Payment Status" id="p_status_w" >
                  <option value="Done">Payment Recieved</option>
                  <option value="Pending" selected>Payment Processing</option>
                  <option value="Cancel">Transaction Declined</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div id="pay1" style="display:block">
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control date-picker"   id="payments_due_date" name="payments_due_date" placeholder="payments Due Date">
              <label></label>
              <span class="help-block">Payments Due Date</span> </div>
          </div>
          <!--<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" readonly id="payments_due"  name="payments_due" placeholder="Payments Due">
             
              <input autocomplete="off" type="hidden" class="form-control" readonly id="payments_due1" name="payments_due1" placeholder="Payments Due">
              <label></label>
              <span class="help-block">Payments Due</span> </div>
          </div>-->
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn blue" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <input type="hidden" name="hid">
    <?php form_close(); ?>
    <!-- END CONTENT --> 
    
  </div>
</div>
<div class="modal fade" id="myModalNorm1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content"> 
      
      <!-- Modal Header -->
      
      <div class="modal-header" style="background:#95A5A6; color:#ffffff;"> Search Guest
        <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true">&times;</span> <span class="sr-only">Close</span> </button>
      </div>
      
      <!-- Modal Body -->
      <div class="modal-body">
        <div class="form-group">
          <div class="input-group">
            <input type="email" class="form-control" id="guest_search" placeholder="Search Guest"/>
            <span class="input-group-btn">
            <button type="button" class="btn green" onclick="return_guest_search()">Search</button>
            </span> </div>
        </div>
        <div id="return_guest" style="overflow-y:scroll; height:300px; display:none;"> </div>
      </div>
      
      <!-- Modal Footer -->
      <div class="modal-footer">
        <button type="button" class="btn red" data-dismiss="modal"> Close </button>
      </div>
    </div>
  </div>
</div>

<script>

function paym(val){
	//alert(val);
        if(val == 'cards'){ 
            document.getElementById('cards').style.display='block';         
        } else {
			document.getElementById('cards').style.display='none';
        }
        if(val=='fund') {
            document.getElementById('fundss').style.display='block';  
        } else {
			document.getElementById('fundss').style.display='none';
        }
        if(val=='cheque'){
            document.getElementById('cheque').style.display='block';  
        } else {
			document.getElementById('cheque').style.display='none';
        } 
		if(val=='draft'){
            document.getElementById('draft').style.display='block';  
        } else {
			document.getElementById('draft').style.display='none';
        }
		if(val=='ewallet'){
            document.getElementById('ewallet').style.display='block';  
        } else {
			document.getElementById('ewallet').style.display='none';
        }
		if(val=='cash'){
            document.getElementById('cardss').style.display='none';
            document.getElementById('fundss').style.display='none';
			document.getElementById('ewallet').style.display='none';
			document.getElementById('cheque').style.display='none';
			document.getElementById('draft').style.display='none';
        }		
    }

var quantity=0;
function cal_amount(a){
	//$('#pay_amount').val()
	if(a==''){
		a=0;
	}
	var b=$('#hidden_grand_total').val();
	a=parseFloat(a);
	b=parseFloat(b);
	a=b-a;
	$('#payments_due').val(a);
}
function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	  // alert(y);
	    
	   if(y=="checks"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="cards"){
		   document.getElementById("cards").style.display="block";
	   }else{
		   document.getElementById("cards").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fund"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }
   
   
   
   
   
   
   
   
 $(document).ready(function() {
	 
	 var b=$('#booking_type').val();
	 
	 if(b=='2'){
		 $('#single_booking1').show(); 
	 }
	 else if(b=='3'){
		 $('#group_booking1').show(); 
	 }
	 //alert(b);
	 var a=$('#htype').val();
	 booking_type1(a);
	 var h_g_name=$('#h_g_name').val();
	 var h_g_type=$('#h_g_type').val();
	 var h_g_receive=$('#h_g_receive').val();
	 var h_g_delivery=$('#h_g_delivery').val();
	 var h_g_bill=$('#h_g_bill').val();
	 var h_g_request=$('#h_g_request').val();
	 var due=$('#h_g_due').val();
	 
	 //h_g_tax,h_g_vat,h_g_discount,h_g_charge
	 var tax=$('#h_g_tax').val();
	 var vat=$('#h_g_vat').val();
	 //var discount=$('#h_g_discount').val();
	 var charge=$('#h_g_charge').val();
	 var discount=$('#h_g_discount').val();
	 var h_tax=$('#h_tax').val();
	 //alert(h_tax);
	 
	 var get_guest_id=$('#h_g_guest_id').val();
	 $('#hidden_id').val(get_guest_id);
	 
	 
	 $('#vat').val(vat);
	 $('#service_tax').val(tax);
	 $('#service_charge').val(charge);
	 //$('#discount').val(discount);
	 $('#discount').val(0);
	 
	 $('#tax1').val(h_tax).change();
	 
	// var dd= $('#tax1').val();
	//if(dd==h_tax){
		
	//}
	 /*alert(tax);
	 alert(vat);
	 alert(discount);
	 alert(charge);*/
	
	
	
	// alert(due);
	/* alert(h_g_type);
	 alert(h_g_receive);
	 alert(h_g_delivery);
	 alert(h_g_bill);
	 alert(h_g_request);*/
	// $('#total_amount').text(due);
	 $('#grand_total').val(due);
	 $('#payments_due').val(due);
	 $('#Guest_name').val(h_g_name);
	 $('#guest_type1').val(h_g_type).change();
	 
	 //h_g_receive,h_g_delivery,h_g_request,h_g_bill
	 $('#received_date').val(h_g_receive);
	 $('#delivery_date').val(h_g_delivery);
	 $('#billing_pref').val(h_g_bill).change();
	 $('#special_request').val(h_g_request);
	 var tt1=parseFloat($('#htotal').val());
	 /*alert(discount+'before discount')
	 if(discount){
		 g_total2(discount,h_tax);
		 
		 }alert(discount+'after discount');*/
	 tax2(h_tax,tt1);
                        cellWidth = 100;
                        cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                       
                    });

            $("#autocellwidth").click(function() {
						
						
                      cellWidth = 100;  // reset for "Fixed" mode
                        cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                      
					document.getElementById('auto_cl_id').style.backgroundColor = $(this).is(":checked") ? "#297CAC" : "#39B9A1";
                        var a = $(this).is(":checked") ? "Pay Later" : "Pay Now";
						if(a == 'Pay Later')
						{	
							document.getElementById('pay').style.display = 'block';
	                        document.getElementById('pay1').style.display = 'block';
						}
						else
						{	
							
						    document.getElementById('pay').style.display = 'none';
	                        document.getElementById('pay1').style.display = 'block';
						}
						
						$('#disp').text(a);

					});
					
					

function g_total(){
		
	var g=$('#hidden_grand_total').val();	
	g=parseFloat(g);
	//v=parseFloat(v);
	
	var discount=$('#discount').val();	
	var vat=$('#vat').val();	
	var service_tax=$('#service_tax').val();	
	var service_charge=$('#service_charge').val();
	
	/*vat=parseFloat(vat);
	service_tax=parseFloat(service_tax);
	service_charge=parseFloat(service_charge);
	*/
	if(vat == '' ){
		vat=0;
		
	}else{	
	vat=parseFloat(vat);
	vat =(parseFloat(g) * vat)/100;
	}
	
	if(service_tax == '' ){
		service_tax=0;
		
	}else{	
	service_tax=parseFloat(service_tax);
	service_tax =(parseFloat(g) * service_tax)/100;
	}
	
	if(service_charge == '' ){
		service_charge=0;
		
	}else{	
	service_charge=parseFloat(service_charge);
	service_charge =(parseFloat(g) * service_charge)/100;
	}
	//service_tax =(g * service_tax)/100;
	//service_charge =(g * service_charge)/100;
	
	grand_total=g+vat+service_tax+service_charge;
	
	
	if(discount == '' ){
		discount=0;
		
	}else{	
	discount=parseFloat(discount);
	grand_total =(grand_total- discount);
	}
	$('#grand_total').val(grand_total);
	$('#payments_due').val(grand_total);
}


var flag=0,flag1=0;sum=0;
function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});


		var ff=flag;
  var check=$('#cloth_type'+ff).val();
  var a=$('#cloth_type').val();
  //if(check<=a){
  //var color=0;
  
   $("#additems").click(function(){
	   var cloth_type=$('#cloth_type').val();
	   var size=$('#size').val();
	   var condition=$('#condition').val();
	   var service=$('#service').val();
	   var qty=$('#qty').val();
	   var color=$('#color').val();
	   var fabric=$('#fabric').val();
	   var brand=$('#brand').val();
	   var price=parseFloat($('#price').val());
	   var total=parseFloat($('#total').val());
	   
	  
		/*alert(cloth_type);
		alert(service);
		alert(fabric);
		alert(price);
		alert(total);*/
		
	   if(cloth_type>0 && service>0 && fabric>0 && price>0 && total>0 ){
	var x=0;
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>unit_class_controller/get_laundrydata_by_id",
		dataType: 'json',
		data: {cloth_type:cloth_type,fabric:fabric,service:service,color:color},
		success: function(data){
			//alert(data.name);
			//$('#cloth_type'+flag).val(data.name);
	//$('#ids').val( it+','+ i_name) ;
	$('#items tr:first').after('<tr id="row_'+flag+'">'+
	'<td class="hidden-480"><input name="cloth_type[]" id="cloth_type'+flag+'" type="text" value="'+data.name+'" class="form-control input-sm" readonly></td>'+
	'<td class="hidden-480"><input name="fabric[]" id="fabric'+flag+'" type="text" value="'+data.fabric+'" class="form-control input-sm" readonly></td>'+	
	'<td class="hidden-480"><input name="size[]" id="fabric'+flag+'" type="text" value="'+size+'" class="form-control input-sm" readonly></td>'+	
	'<td class="hidden-480"><input name="condition[]" id="condition'+flag+'" type="text" value="'+condition+'" class="form-control input-sm" readonly ></td>'+
	'<td class="hidden-480"><input name="service[]" id="service'+flag+'" type="text" value="'+data.service+'" class="form-control input-sm" readonly></td>'+
	'<td class="hidden-480"><input name="qty[]" id="qty'+flag+'" type="text" value="'+qty+'" onblur="calculation('+flag+')" class="form-control input-sm" readonly></td>'+
	'<td class="hidden-480"><input name="color[]" id="color'+flag+'" type="text" value="'+data.color+'" class="form-control input-sm" readonly></td>'+
	'<input type="hidden" name="h_color[]" value="'+color+'"><input type="hidden" name="h_cloth_type[]" value="'+cloth_type+'"><input type="hidden" name="h_fabric[]" value="'+fabric+'"><input type="hidden" name="h_service[]" value="'+service+'">'+
	'<td class="hidden-480"><input name="brand[]" id="brand'+flag+'" type="text" value="'+brand+'" class="form-control input-sm" readonly></td>'+
	'<td class="hidden-480"><input name="price[]" id="price'+flag+'" type="text" value="'+price+'"  class="form-control input-sm" readonly></td>'+
	'<td class="hidden-480"><input name="total[]" id="total'+flag+'" type="text" value="'+total+'"  class="form-control input-sm" readonly></td>'+
	'<td><table><tr><td><a  class="btn red btn-sm" onclick="removeRow('+flag+','+total+','+qty+')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>');	
	/*'<td class="hidden-480"><select class="form-control bs-select"   name="tax[]" id="tax1'+flag+'" onchange="tax1('+flag+')"  placeholder="Tax" required><option value="dis">select</option>'+
    '<option value="Yes">Yes</option><option value="NO">NO</option></select>'+
	'<td class="hidden-480"><input name="note[]" id="note'+flag+'" type="text" value="'+note+'" onblur="calculation('+flag+')" class="form-control input-sm"  readonly ></td>'+*/
	flag++;
	window.countRow++;
	 window.totalquantity= parseFloat(window.totalquantity)+parseFloat(qty)	;
	 $('#quantity').val(window.totalquantity);
	 //alert(window.totalquantity);
	 //alert(window.total);
	 //alert(total);
	window.total=parseFloat(window.total)+parseFloat(total);
	//alert(window.total);
	$('#total_amount').text(window.total);
	$('#pay_amount').val(window.total);
	var a=parseFloat($('#due_amount').val());
	alert(a);
	alert(total);
	alert(a);
	$('#tax1').val(h_tax).change();
//alert(h_tax);
var h_tax=$('#h_tax').val();
	
jquery_tax(h_tax);
				var payAmo=parseFloat($('#pay_amount').val());
				var dueAmo=parseFloat($('#due_amount').val());
				
				if(a){
	
	$('#due_amount').val(parseFloat(a)+parseFloat(total));
	$('#due_amount_hid').val(parseFloat(a)+parseFloat(total));
	}else{
	//	alert('here');
		$('#due_amount').val(payAmo);
		$('#due_amount_hid').val(payAmo);
	}
	
	$('#total_amount_hid').val(window.total);
	$('#hidden_total').val(window.total);
	$('#total_amount_hidden').val(window.total);
	//$('#tax1').val('dis').change();
	$('#discount').val('');
	$("#div_tax_type").html('');
	
	
	/*if(h_tax='No'){
		$("#div_tax_type").html('<div></div>');
	}*/

	
		
		
	
	var tm1=0;
	var tm=parseFloat($('#total_amount').text());
	//$('#grand_total').val(tm);
	var gtt=parseFloat(window.total);
	$('#grand_total').val(gtt);
	var gt1=parseFloat($('#grand_total').val());
	//$('#pay_amount').val("");
		//$('#grand_total').val("");
	
	
	}});   }
	   else{
		/*   if(tax=='dis'){
			    alert("Select TAX ");
		   }*/
		   swal("warning!", "Please fillup all fields!", "error")
	   }
	   //alert(note);
		if(cloth_type>0 && service>0 && fabric>0){
		
	   
	   $('#cloth_type').val('0').change();
	   $('#size').val('0').change();
	   $('#condition').val('0');
	   $('#service').val('0').change();
	   $('#qty').val('');
	   $('#color').val('0').change();
	   $('#fabric').val('0').change();
	   $('#brand').val('');
	   $('#price').val('');	   
	   $('#total').val('');
		}
	
		});
		
		
		
function check_date(){
	
	var a=$('#contract_start_date').val();
	 a=new Date(a);
	   var b=$('#contract_end_date').val();
	   b=new Date(b);
	   if(a>=b && b!='' ){
		   alert('Date Should Getter Than Start Date ');
		   $('#contract_end_date').val('');
	   }
	  
	  var c=$('#contract_end_date'+(flag-1)).val();
	 // alert(c);
	    c=new Date(c);
		if(c>a){
			
			alert('Please Enter Valid Date');
			$('#contract_start_date').val('');
			$('#contract_end_date').val('');
		}
		
	  
	   
}



function fetch_all_address()
{
	
	var pin_code = document.getElementById('pin_code').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("country").focus();
			$('#country').val(data.country);
			document.getElementById("state").focus();
			$('#state').val(data.state);
			document.getElementById("city").focus();
			$('#city').val(data.city);
		}

	});
	
	
	
	
}
	   $("#addGuest").click(function(){
		$('#myModalNorm1').modal('show');
		$('#return_guest').html("");
		$("#return_guest").css("display", "none");
	});
	   
 function return_guest_search(){		
        var guest_name = $('#guest_search').val();
        //alert(guest_name);
        jQuery.ajax({
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_search",
                datatype:'json',
                data:{guest:guest_name},
                success:function(data)
                {
                    var resultHtml = '';
                    resultHtml+='<table class="table table-bordered" cellpadding="0" cellspacing="0" ><thead><tr><th>Name</th><th>Address</th><th width="100">Mobile No</th></tr></thead><tbody>';
                    $.each(data, function(key,value){
                        resultHtml+='<tr  class="crsr collapsed" data-toggle="collapse" data-target="#toggleDemo'+value.g_id+'">';
                        resultHtml+='<td>'+ value.g_name +'</td>';
                        resultHtml+='<td>'+ value.g_address +'</td>';
                        resultHtml+='<td>'+ value.g_contact_no +'</td>';
                        resultHtml+='</tr><tr id="toggleDemo'+value.g_id+'"  class="mrgn">';
                        resultHtml+='<td class="no-marin"  colspan="3"  cellpadding="0" cellspacing="0">';
                        resultHtml += '<div class="side clearfix" style="width:100%;"><div class="col-xs-7 ex"><p><label style="font-size:13px;">' + value.g_name + '</label><br><label>';

                        resultHtml+='<input type="hidden" id="g_id_hide"  value="'+value.g_id+'" ></input>';
                        resultHtml+='<label style="font-size:13px;">Room No: '+value.room_no+'</label><br><label style="font-size:13px;">Last Check In Date: '+value.cust_from_date+'</label><br> <button class="act active btn blue btn-sm" name="g_id" id="g_id"" value="'+value.g_id+'" onclick="get_guest('+value.g_id+')" >Continue</button> </p></div>';
                        if(value.g_photo_thumb!='') {
                            resultHtml += '<div class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/' + value.g_photo_thumb + '" width="100px" height="92px"  > ' +
                            '</div>';
                        }
                        else{
                            resultHtml += '<div  class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/no_images.png" width="100px" height="92px" > ' +
                            '</div>';
                        }
                        //resultHtml+='<td><input type="radio" name="g_id" id="'+value.g_id+'" value="'+value.g_id+'" onclick="get_guest(this.value)" /></td>';
                        resultHtml+='</div></td></tr>';
                    });
                    resultHtml+='</tbody></table>';
                    $('#return_guest').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
			$("#return_guest").css("display", "block");
        return false;
    }
	
	
	
</script> 


<script>
function get_guest_by_id(id){
	//alert(id);
	
	
	 jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_get",
                datatype:'json',
                data:{guest_id:id},
                success:function(data)
                {
					if(id){
                    $('#tab11').hide();
                    $('#tab12').show();
                    $('#id_guest2').val(id);
					//alert(data.booking_id);
					//alert(data.g_name);
                    $('#Guest_name').val(data.g_name);
					$('#guest_type1').val(data.g_type).change();
				}
                    
					
                }
            });
	
}


  function get_guest(id)
    {
        //var g_id = $('#g_id_hide').val();
        var g_id=id;
       // alert(g_id);
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_get",
                datatype:'json',
                data:{guest_id:g_id},
                success:function(data)
                { 
					
                  
				
                    $('#tab11').hide();
                    $('#tab12').show();
                    $('#id_guest2').val(id);
					
                    $('#Guest_name').val(data.g_name);
					$('#guest_type1').val(data.g_type).change();
					
                    $('#hidden_id').val(data.g_id);
                   /* $('#cust_contact_no').val(data.g_contact_no);
					$('#cust_mail').val(data.g_email);
					*/
					$('#myModalNorm1').modal('hide');
					
                }
            });
        return false;
    }


function get_price(id){
	var cloth=$('#cloth_type').val();
	//var service=$('#service').val();
	var fabric=$('#fabric').val();
	
	 jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>unit_class_controller/get_price_by_clothID_fabricId",
                datatype:'json',
                data:{cloth:cloth,fab:fabric,service:id},
                success:function(data)
                {  
				//alert(data.dry);
				if(service=='1')
					$('#price').val(data.dry);
				else if(service=='2')
					$('#price').val(data.laundry);
				else
					$('#price').val(data.iron);
					
				/*alert(data.id);
                  alert(data.laundry);
                  alert(data.dry);
                  alert(data.iron);*/
                  //alert(data.id);
				
                    //$('#tab11').hide();
                    
					
                }
            });
	
	
	
}

function booking_type1(id){
	//alert('1');
if(id==1){
$("#single_booking1").show();
$("#group_booking1").hide();
$("#Guest_name").prop( "disabled", true );

}
else if(id==2){
$("#group_booking1").show();
$("#single_booking1").hide();
$("#Guest_name").prop( "disabled", true );
}else{
$("#Guest_name").prop( "disabled", false );	
$("#group_booking1").hide();
$("#single_booking1").hide();
}
}


function removeRow(a,b){		
//alert('hello');
alert(a);
alert(b);
		var c=$('#total_amount').text();
		var a;
		b=parseFloat(c)-b;
		$('#total_amount').text(b);
		$('#grand_total').val(b);
		$('#due_amount').val(b);
		$('#hidden_grand_total').val(b);
		
		if(b<1){
		$('#vat').val(0);
		$('#service_tax').val(0);
		$('#service_charge').val(0);
		$('#discount').val(0);
		$('#grand_total').val(0);
		
		}
		$('#row_'+a).remove();
		
	}
	
		function get_guest_by_booking_id(id){
			//alert(id);
			var a=$('#booking_type').val();//single=2 group3
			//alert(a);
			jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>unit_class_controller/get_guest_by_booking_id",
                datatype:'json',
                data:{booking_id:id,booking_type:a},
                success:function(data)
                { 
					/*alert(data.id);
					alert(data.name);					
					alert(data.type);*/
					$('#Guest_name').val(data.name);
					$('#guest_type1').val(data.type).change();
					$('#guest_booking_id').val(id);
				
                }
            });
			
		}
		</script>
<script>
function total_amt(){
var pr=parseFloat($('#price').val());
	var qty=parseFloat($('#qty').val());
pr=pr*qty;

$('#total').val(pr);
}

</script>

	<script>
function check_date(){
	
	var a=$('#contract_start_date').val();
	 a=new Date(a);
	   var b=$('#contract_end_date').val();
	   b=new Date(b);
	   if(a>=b && b!='' ){
		   alert('Date Should Getter Than Start Date ');
		   $('#contract_end_date').val('');
	   }
	  
	  var c=$('#contract_end_date'+(flag-1)).val();
	 // alert(c);
	    c=new Date(c);
		if(c>a){
			
			alert('Please Enter Valid Date');
			$('#contract_start_date').val('');
			$('#contract_end_date').val('');
		}
		
	  
	   
}

$( document ).ready(function() {
	var tt=parseFloat($('#htotal').val());
	//alert(tt);
	if(tt>=0){
$('#total_amount').html(tt);
$('#grand_total').val(tt);
//$('#due_amount').val(tt);
	}else{
		tt=0;
		$('#total_amount').html(tt);
		$('#grand_total').val(tt);
		//$('#due_amount').val(tt);
	}
//alert(tt);
	
	
	$('#div_tax_type').hide();
	
    window.countRow=0;
    window.total=tt;
    window.grandtotal=0;
    window.grandtotal1=0;
    window.totalquantity=0;
});

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pin_code').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("country").focus();
			$('#country').val(data.country);
			document.getElementById("state").focus();
			$('#state').val(data.state);
			document.getElementById("city").focus();
			$('#city').val(data.city);
		}

	});
	
	
	
	
}
	   $("#addGuest").click(function(){
		$('#myModalNorm1').modal('show');
		$('#return_guest').html("");
		$("#return_guest").css("display", "none");
	});
	   
 function return_guest_search(){		
        var guest_name = $('#guest_search').val();
        //alert(guest_name);
        jQuery.ajax({
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_search",
                datatype:'json',
                data:{guest:guest_name},
                success:function(data)
                {
                    var resultHtml = '';
                    resultHtml+='<table class="table table-bordered" cellpadding="0" cellspacing="0" ><thead><tr><th>Name</th><th>Address</th><th width="100">Mobile No</th></tr></thead><tbody>';
                    $.each(data, function(key,value){
                        resultHtml+='<tr  class="crsr collapsed" data-toggle="collapse" data-target="#toggleDemo'+value.g_id+'">';
                        resultHtml+='<td>'+ value.g_name +'</td>';
                        resultHtml+='<td>'+ value.g_address +'</td>';
                        resultHtml+='<td>'+ value.g_contact_no +'</td>';
                        resultHtml+='</tr><tr id="toggleDemo'+value.g_id+'"  class="mrgn">';
                        resultHtml+='<td class="no-marin"  colspan="3"  cellpadding="0" cellspacing="0">';
                        resultHtml += '<div class="side clearfix" style="width:100%;"><div class="col-xs-7 ex"><p><label style="font-size:13px;">' + value.g_name + '</label><br><label>';

                        resultHtml+='<input type="hidden" id="g_id_hide"  value="'+value.g_id+'" ></input>';
                        resultHtml+='<label style="font-size:13px;">Room No: '+value.room_no+'</label><br><label style="font-size:13px;">Last Check In Date: '+value.cust_from_date+'</label><br> <button class="act active btn blue btn-sm" name="g_id" id="g_id"" value="'+value.g_id+'" onclick="get_guest('+value.g_id+')" >Continue</button> </p></div>';
                        if(value.g_photo_thumb!='') {
                            resultHtml += '<div class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/' + value.g_photo_thumb + '" width="100px" height="92px"  > ' +
                            '</div>';
                        }
                        else{
                            resultHtml += '<div  class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/no_images.png" width="100px" height="92px" > ' +
                            '</div>';
                        }
                        //resultHtml+='<td><input type="radio" name="g_id" id="'+value.g_id+'" value="'+value.g_id+'" onclick="get_guest(this.value)" /></td>';
                        resultHtml+='</div></td></tr>';
                    });
                    resultHtml+='</tbody></table>';
                    $('#return_guest').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
			$("#return_guest").css("display", "block");
        return false;
    }
	
	
	
</script> 
<script>
function get_guest_by_id(id){
	//alert(id);
	
	
	 jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_get",
                datatype:'json',
                data:{guest_id:id},
                success:function(data)
                {
					if(id){
                    $('#tab11').hide();
                    $('#tab12').show();
                    $('#id_guest2').val(id);
					//alert(data.booking_id);
					//alert(data.g_name);
                    $('#g_name_hid').val(data.g_name);
                    $('#Guest_name').val(data.g_name);
					$('#guest_type1').val(data.g_type).change();
					$('#hidden_id').val(id);
				}
                    
					
                }
            });
	
}


  function get_guest(id)
    {
        //var g_id = $('#g_id_hide').val();
        var g_id=id;
       // alert(g_id);
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_get",
                datatype:'json',
                data:{guest_id:g_id},
                success:function(data)
                { 
					
                  
				
                    $('#tab11').hide();
                    $('#tab12').show();
                    $('#id_guest2').val(id);
					
                    $('#Guest_name').val(data.g_name);
					$('#guest_type1').val(data.g_type).change();
					
                    $('#hidden_id').val(data.g_id);
                   /* $('#cust_contact_no').val(data.g_contact_no);
					$('#cust_mail').val(data.g_email);
					*/
					$('#myModalNorm1').modal('hide');
					
                }
            });
        return false;
    }


function get_price(id){
	var cloth=$('#cloth_type').val();
	//var service=$('#service').val();
	var fabric=$('#fabric').val();
	
	 jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>unit_class_controller/get_price_by_clothID_fabricId",
                datatype:'json',
                data:{cloth:cloth,fab:fabric,service:id},
                success:function(data)
                {  
				//alert(data.dry);
				if(service=='1')
					$('#price').val(data.dry);
				else if(service=='2')
					$('#price').val(data.laundry);
				else
					$('#price').val(data.iron);
					
				/*alert(data.id);
                  alert(data.laundry);
                  alert(data.dry);
                  alert(data.iron);*/
                  //alert(data.id);
				
                    //$('#tab11').hide();
                    
					
                }
            });
	
	
	
}

function booking_type1(id){
	
	if(id==0){
$("#gname_adhoc").show();	
$("#Guest_name").prop( "disabled", false );	
$("#group_booking1").hide();
$("#single_booking1").hide();
//$("#Billing_Preference").show();
 //$("#Billing_Preference option[value='Add to Booking Bill']").attr('disabled', true);
}else if(id==1){
$("#single_booking1").show();
$("#group_booking1").hide();
$("#Guest_name").prop( "disabled", true );
$("#gname_adhoc").hide();
//$("#Billing_Preference").show();
//$("#Billing_Preference option[value='Add to Booking Bill']").attr('disabled', false);
}
else if(id==2){
$("#group_booking1").show();
$("#single_booking1").hide();
$("#Guest_name").prop( "disabled", true );
$("#gname_adhoc").hide();
//$("#Billing_Preference").show();
//$("#Billing_Preference option[value='Add to Booking Bill']").attr('disabled', false);
}
}


function removeRow(a,b,q){
	//alert(b);
swal({
				  title: "Are you sure?",
				  text: "You will not be able to recover this Row!",
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonColor: "#DD6B55",
				  confirmButtonText: "Yes, delete it!",
				  cancelButtonText: "No, cancel plx!",
				  closeOnConfirm: false,
				  closeOnCancel: false
				},
				function(isConfirm){
				  if (isConfirm) {					  
					swal("Deleted!", "The Row has been deleted.", "success");
					var c=$('#total_amount').text();
		window.total=window.total-b;
		window.totalquantity=window.totalquantity-q;
		$('#quantity').val(window.totalquantity);
		
		var h_tax1=$('#h_tax').val();
					$('#tax1').val(h_tax1).change();
					/*if(h_tax1=='No'){
						$("#div_tax_type").html('<div></div>');
					}*/
						jquery_tax(h_tax1,b);
						
		
		b=parseFloat(c)-b;
		$('#total_amount').text(b);
		$('#due_amount').val(b);
		$('#row_'+a).remove();
		
		
						//alert(window.total);
					$('#tax1').val(h_tax1).change();
						if(window.total==0){
						$("#div_tax_type").empty();
						}else{
							jquery_tax(h_tax1,window.total);	
						}
					
				  } else {
					swal("Cancelled", "Your imaginary file is safe :)", "error");
				  }
				});
	
		
		
	}
	function removeRow_Ajx(a,b,quantity){	
//alert(a);	
//alert(b);	
		jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>unit_class_controller/delete_hotel_laundry_line_item1",
                datatype:'json',
                data:{id:a,quantity:quantity,lid:'<?php echo $laundry_service->laundry_id?>'},
                success:function(data)
                { if(data.data='sucess'){
					
					
					
					
									swal({
				  title: "Are you sure?",
				  text: "You will not be able to recover this Row!",
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonColor: "#DD6B55",
				  confirmButtonText: "Yes, delete it!",
				  cancelButtonText: "No, cancel plx!",
				  closeOnConfirm: false,
				  closeOnCancel: false
				},
				function(isConfirm){
				  if (isConfirm) {					  
					swal("Deleted!", " The Row has been deleted.", "success");					
					window.total=window.total-parseFloat(b);
					
					var h_tax1=$('#h_tax').val();
				alert(h_tax1);
					$('#tax1').val(h_tax1).change();
						

						//$('#grand_total').val();
						//$('#pay_amount').val();
						//$('#due_amount').val();
						
						//alert(window.total);
						if(window.total==0){
							alert('delete');$("#div_tax_type").empty();
						}else{
							jquery_tax(h_tax1,window.total);	
						}
					$('#total_amount').text(window.total);
					if(window.total==0){
						$("#div_tax_type").html('<div></div>');
					}
					$('#row2_'+a).remove();
					
				  } else {
					swal("Cancelled", "Your imaginary file is safe :)", "error");
				  }
				});
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				}
				
				
                }
            });
		
		
		
	}
	
	
	
	
	function editRow(a){		
		//alert(a);
		$('#row_'+a).hide();
		//$('#line').show(); return "<html>" + $("html").html() + "</html>";
		$('#row_'+a).after('<tr id="row_'+a+'">'+
	    //'<td class="hidden-480"><input name="cloth_type[]" id="cloth_type'+a+'" type="text" value="'+a+'" class="form-control input-sm" ></td></tr></table>');
		//$("html").html() + "</html>");
		$('#abc1').html());
		
	}
		function get_guest_by_booking_id(id){
			//alert(id);
			var a=$('#booking_type').val();//single=2 group3
			//alert(a);
			jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>unit_class_controller/get_guest_by_booking_id",
                datatype:'json',
                data:{booking_id:id,booking_type:a},
                success:function(data)
                { 
					//alert(data.id);
					//alert(data.name);					
					//alert(data.type);
					//$('#Guest_name').text(data.name);
					$('#Guest_name').val(data.name);
					$('#guest_type1').val(data.type).change();
					$('#guest_booking_id').val(id);
					alert(id);
					$('#booking_id_hid').val(id);
				
                }
            });
			
		}
		</script> 
<script>

	function calculation(id){
		
	var qty =$('#qty'+id).val(); 
	var price = $('#price'+id).val();
	var tax = $('#tax1').val();
	var total = $('#total'+id).val();
	var note = $('#note'+id).val();
	//alert(qty+price+total+tax+note);
	var amount=parseFloat(qty)*parseFloat(price)+parseFloat(note);
	
	$('#total'+id).val(amount);
	$('#tax'+id).val('dis').change();
	$('#note'+id).val('0');
	
	
		
	}
function total_amt(){
var pr=parseFloat($('#price').val());
var qty=parseFloat($('#qty').val());
//alert(pr);
//alert(qty);
if(qty>0 && pr>0){
	pr=pr*qty;
//alert('wrong');
$('#total').val(pr);
}else{
	$('#total').val('');
}
}
function get_laundry_rates(){
		var cloth=$('#cloth_type').val();
		var fabric=$('#fabric').val();
		var service=$('#service').val();
	if(cloth !='' && service!='' && service>0 ){
		jQuery.ajax(
		{
			type: "POST",
			url: "<?php echo base_url(); ?>rate_plan/get_laundry_rates",
			data: {
				cloth:cloth,
				fabric:fabric,
				service:service,
			},
			success: function(data){
				//alert(data.price);
				$('#price').val(data.price);
					
				
			} // end ajax success
		});

	}
	}
	
	
	

$('select#tax1').on('change',function(){
	
	var tax_val = $(this).val();
	var l_price = parseFloat($('#total_amount').text());
	var args = 'a';
	var l_date = $('#received_date').val();
	if(l_date!=''){
	if(tax_val == 'Yes'){
		$.ajax({
			
			type:"POST",
			url:"<?php  echo base_url();?>dashboard/showtaxplan1",
			data:{"price":l_price,"type1":"Extra Service","dates":l_date,"args":args},
			success:function(data){
				
				//alert(data.total);
				var narr=data;
				var to=l_price+data.total;
				$('#grand_total').val(to);
				window.grandtotal=to;			
				window.grandtotal1=to;	
				
				var payAmo1=parseFloat($('#pay_amount').val());
				var dueAmo1=parseFloat($('#due_amount').val());
				if(payAmo1==dueAmo1){
	
	$('#due_amount').val(to);
	$('#due_amount_hid').val(to);
	}				
				$('#pay_amount').val(to);
				delete narr['r_id'];
				delete narr['planName'];
				delete narr['total'];
				$('#div_tax_type').show();
				$.each( narr, function( key, value ) {//key + ": " + value
						$("#div_tax_type").append('<div class="col-md-2">'+key+' = '+value+'</div>');
				});
				$('#hidTax').val(tax_val);
				var stringData = JSON.stringify(narr);
				$('#taxresponse').val(stringData);
				
				
				console.log(data);
				//alert(data);
				
			},
			
		});
		
	}
	else{
		var ab;
		$('#taxresponse').val('');
		$('#hidTax').val(tax_val);
		$('#div_tax_type').hide();
		$("#div_tax_type").html('');
		var gtt=parseFloat($('#grand_total').val());
$('#grand_total').val(l_price);
			window.grandtotal=l_price;			
			window.grandtotal1=l_price;
			$('#pay_amount').val(l_price);
			$('#due_amount').val(l_price);
		if(gtt>l_price){
			$('#grand_total').val(l_price)
			
			
		}
		
		//alert('noooo');
	}
	}else{
			swal("Please Select Date!");
	}
	
});

function jquery_tax(htax, vv=0){
	
	var tax_val = htax;
	if(vv==0){
	var l_price = parseFloat($('#total_amount').text());
	}else{
		var l_price = parseFloat(vv);
	}
	var args = 'a';
	var l_date = $('#received_date').val();
	if(l_date!=''){
	if(tax_val == 'Yes'){
		$.ajax({
			
			type:"POST",
			url:"<?php  echo base_url();?>dashboard/showtaxplan1",
			data:{"price":l_price,"type1":"Extra Service","dates":l_date,"args":args},
			success:function(data){
				
				alert('total data'+data.total);
				var narr=data;
				var to=l_price+data.total;
				$('#grand_total').val(to);
				window.grandtotal=to;			
				window.grandtotal1=to;		
				var payAmo=parseFloat($('#pay_amount').val());
				var dueAmo=parseFloat($('#due_amount').val());
				if(payAmo==dueAmo){
					alert('tax');
					$('#due_amount').val(to);
				}/*else{
					
					$('#due_amount').val(dueAmo+data.total);
				}*/
				
				$('#pay_amount').val(to);
				delete narr['r_id'];
				delete narr['planName'];
				delete narr['total'];
				$('#div_tax_type').show();
				$("#div_tax_type").html('<div></div>');
				$.each( narr, function( key, value ) {//key + ": " + value
						$("#div_tax_type").append('<div class="col-md-2">'+key+' = '+value+'</div>');
				});
				$('#hidTax').val(tax_val);
				var stringData = JSON.stringify(narr);
				$('#taxresponse').val(stringData);
				
				
				console.log(data);
				//alert(data);
				
			},
			
		});
		
	}
	else{
		var ab;
		$('#taxresponse').val('');
		$('#hidTax').val(tax_val);
		$('#div_tax_type').hide();
		$("#div_tax_type").html('');
		var gtt=parseFloat($('#grand_total').val());
$('#grand_total').val(l_price);
			window.grandtotal=l_price;			
			window.grandtotal1=l_price;
			$('#pay_amount').val(l_price);
			$('#due_amount').val(l_price);
		if(gtt>l_price){
			$('#grand_total').val(l_price)
			
			
		}
		
		//alert('noooo');
	}
	}else{
			swal("Please Select Date!");
	}
	
}




function tax2(h_tax,tt1){
	//alert(h_tax);
	
	var tax_val = h_tax;
	var l_price = parseFloat(tt1);
	
	var args = 'a';
	var l_date = $('#received_date').val();
	if(l_date!=''){
	if(tax_val == 'Yes'){
		$.ajax({
			
			type:"POST",
			url:"<?php  echo base_url();?>dashboard/showtaxplan1",
			data:{"price":l_price,"type1":"Extra Service","dates":l_date,"args":args},
			success:function(data){
				
			//	alert(data.total);
				var narr=data;
				var to=l_price+data.total;
				$('#grand_total').val(to);
				window.grandtotal=to;			
				window.grandtotal1=to;			
				$('#pay_amount').val(to);
				delete narr['r_id'];
				delete narr['planName'];
				delete narr['total'];
				$('#div_tax_type').show();
				$.each( narr, function( key, value ) {//key + ": " + value
						$("#div_tax_type").append('<div class="col-md-2">'+key+' = '+value+'</div>');
				});
				$('#hidTax').val(tax_val);
				var stringData = JSON.stringify(narr);
				$('#taxresponse').val(stringData);
				
				
				console.log(data);
				//alert(data);
				
			},
			
		});
		
	}
	else{
		var ab;
		$('#taxresponse').val('');
		$('#hidTax').val(tax_val);
		$('#div_tax_type').hide();
		$("#div_tax_type").html('');
		var gtt=parseFloat($('#grand_total').val());
		if(gtt>l_price){
			$('#grand_total').val(l_price)
			
			
		}
		
		//alert('noooo');
	}
	}else{
			swal("Please Select Date!");
	}
	
}
function billing(a){
	//alert(a);
	if(a=="Bill Seperately"){
		$('#pay_id').show();
		var dd=$('#grand_total').val();
		$('#pay_amount').val(dd);
	}
	else{
		$('#pay_id').hide();
	}
	
}
function g_total1(discount){
	var ss=$('#tax1').val();
	var hidden_grand_total = $('#hidden_total').val();
	//aalert(ss);
	if(ss=='Yes'){
	if(discount!=''){
	discount=parseFloat(discount);
	var gtt1=parseFloat($('#grand_total').val());
	
	if(discount>window.total){
		swal("Denied!", "Discount Should not more than Total Bill!", "error");
	}
	else{
	//discount=gtt1 - discount;
		
		window.grandtotal=window.grandtotal1-discount;
		$('#grand_total').val(window.grandtotal);
		$('#pay_amount').val(window.grandtotal);
		$('#due_amount').val(window.grandtotal);
		
	}
	
	}else{
		window.grandtotal=window.total;
		window.grandtotal=window.grandtotal1;
		$('#grand_total').val(window.grandtotal);
		$('#pay_amount').val(window.grandtotal);
		$('#due_amount').val(window.grandtotal);
	}
	
}
else{
	
	var discount = parseInt($('#discount').val());
	
	var grand_total = $('#grand_total').val();
	if(discount >= 0){
	var total = parseInt(hidden_grand_total) - parseInt(discount);
	alert('dis' +total +'' +grand_total);
	$('#grand_total').val(total);
	$('#due_amount').val(total);
	}
	else{
		$('#grand_total').val(hidden_grand_total);
		$('#due_amount').val(hidden_grand_total);
	}
}

}

function g_total2(discount,ss){
	//var ss=$('#tax1').val();
	var hidden_grand_total = $('#hidden_total').val();
	//aalert(ss);
	if(ss=='Yes'){
	if(discount!=''){
	discount=parseFloat(discount);
	var gtt1=parseFloat($('#grand_total').val());
	
	if(discount>window.total){
		swal("Denied!", "Discount Should not more than Total Bill!", "error");
	}
	else{
	//discount=gtt1 - discount;
		
		window.grandtotal=window.grandtotal1-discount;
		$('#grand_total').val(window.grandtotal);
		$('#pay_amount').val(window.grandtotal);
		$('#due_amount').val(window.grandtotal);
		
	}
	
	}else{
		window.grandtotal=window.total;
		window.grandtotal=window.grandtotal1;
		$('#grand_total').val(window.grandtotal);
		$('#pay_amount').val(window.grandtotal);
		$('#due_amount').val(window.grandtotal);
	}
	
}
else{
	
	var discount = parseInt($('#discount').val());
	
	var grand_total = $('#grand_total').val();
	if(discount >= 0){
	var total = parseInt(hidden_grand_total) - parseInt(discount);
	alert('dis' +total +'' +grand_total);
	$('#grand_total').val(total);
	}
	else{
		$('#grand_total').val(hidden_grand_total);
	}
}

}

$.typeahead({
    input: '.js-typeahead-user_v1',
    minLength: 1,
    order: "asc",
    dynamic: true,
    delay: 200,
    backdrop: {
        "background-color": "#fff"
    },
   
    source: {
       project: {
            display: "name",
            ajax: [{
                type: "GET",
                url: "<?php echo base_url();?>dashboard/test_typehead",
                data: {
                    q: "{{query}}"
                }
            }, "data.name"],
            template: '<div class="clearfix">' +
                '<div class="project-img">' +
                    '<img class="img-responsive" src="{{image}}">' +
                '</div>' +
                '<div class="project-information">' +
                    '<span class="pro-name" style="font-size:15px;"> {{name}}</span><span><i class="fa fa-phone"></i> <strong>Contact no:</strong> {{ph_no}} </span><span><i class="fa fa-envelope"></i> <strong>Email:</strong> {{email}}</span></span>' +
                '</span>' +
				'</div>' +
            '</div>'
        }
    },
    callback: {
        onClick: function (node, a, item, event) {
         // alert(JSON.stringify(item));
		 
			//$("#cust_address").val(item.pincode);
			//$("#g_address_child").val(item.address);
			//$("#cust_contact_no").val(item.ph_no);
			//$("#cust_mail").val(item.email);
			$("#Guest_name").val(item.name);
			$("#Guest_name_new").val(item.name);
			//$('#id_guest2').val(item.id);
		 
        },
        onSendRequest: function (node, query) {
            console.log('request is sent')
			
        },
        onReceiveRequest: function (node, query) {
            //console.log('request is received')
			//alert("defesddsfdsfs");
			if(query!=''){
				$("#Guest_name").val('');
				
				/*$("#cust_address").val('');
			$("#g_address_child").val('');
			$("#cust_contact_no").val('');
			$("#cust_mail").val('');
			$('#id_guest2').val('');*/
			}
        },
         onCancel: function (node, query) {
            //console.log('request is received')
			alert("defesddsfdsfs");
			if(query!=''){
				$("#Guest_name").val('');
			/**	$("#cust_address").val('');
			$("#g_address_child").val('');
			$("#cust_contact_no").val('');
			$("#cust_mail").val('');
			$('#id_guest2').val('');*/
			}
        }		
    },
    debug: true
});
</script>

