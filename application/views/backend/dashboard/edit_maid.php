 <div class="row all_bk">     
    <div class="col-md-12">       
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption"> <i class="fa fa-file-archive-o"></i>Edit Staff Details </div>
        </div>
        <div class="portlet-body form">       
                  <?php
                    $form = array(
                      'class'       => '',
                      'id'        => 'form',
                      'method'      => 'post'
                    );
                    echo form_open_multipart('dashboard/edit_maid',$form);
                  ?>
                  <div class="form-body">
                  <div class="row">
                  	<div class="col-md-4">
                        <div class="form-group form-md-line-input">
                          <input type="text" autocomplete="off" value="<?php echo $maid_info->maid_name; ?>" class="form-control" name="maid_name" required="required" placeholder="Staff Name *">
                          <label></label>
                          <span class="help-block">Staff Name *</span> 
                        </div>
                    </div>
               		<input type="hidden"name="hid" value="<?php echo $maid_info->maid_id; ?>">
                    <div class="col-md-4">
                        <div class="form-group form-md-line-input">
                          <select class="form-control bs-select"  name="type" required="required" >
                            <option value="" disabled="disabled" selected="selected">Select Type</option>
                            <option <?php if ($maid_info->type == 'Maid'): ?>
                              selected
                            <?php endif ?> value="Maid">Maid</option>
                            <option <?php if ($maid_info->type == 'Auditor'): ?>
                              selected
                            <?php endif ?> value="Auditor">Auditor</option>
                            <option <?php if ($maid_info->type == 'Technician'): ?>
                              selected
                            <?php endif ?> value="Technician">Technician</option>
                            <option <?php if ($maid_info->type == 'Supervisor'): ?>
                              selected
                            <?php endif ?> value="Supervisor">Supervisor</option>                        
                          </select>
                          <label></label>
                          <span class="help-block">Type *</span> </div>
                	  </div>
                      <div class="col-md-4">
                          <div class="form-group form-md-line-input">
                            <input type="text" value="<?php echo $maid_info->maid_address; ?>" autocomplete="off" class="form-control" name="maid_address" placeholder="Staff Address *">
                            <label></label>
                            <span class="help-block">Staff Address *</span> 
                          </div>
					  </div>
                      <div class="col-md-4">
                          <div class="form-group form-md-line-input">
                            <input type="text"  value="<?php echo $maid_info->maid_contact; ?>" autocomplete="off" class="form-control" name="maid_contact" required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" placeholder="Staff Phone *">
                            <label></label>
                            <span class="help-block">Staff Phone *</span> 
                          </div>
					 </div>
                     <div class="col-md-4">
                         <div class="form-group form-md-line-input">
                            <input type="text" value="<?php echo $maid_info->section; ?>" autocomplete="off" class="form-control" name="maid_sec" required="required" placeholder="Staff Section *">
                            <label></label>
                            <span class="help-block">Staff Section *</span> 
                          </div>
                	 </div>
					 <?php $staff_id=$_GET['maid_id'] ;?>
                     <div class="col-md-4">
                         <div class="form-group form-md-line-input">
                          <select class="form-control bs-select" id="availability_select" onchange="check_avai(<?php $staff_id ?>)" name="status" required="required" >
                            <option value="">Select Availability</option>
                            <option <?php if ($maid_info->staff_availability == 'Available'): ?>
                              selected
                            <?php endif ?> value="Available">Available</option>
                            <option <?php if ($maid_info->staff_availability == 'Not Available'): ?>
                              selected
                            <?php endif ?> value="Not available"> Not Available</option>
                          </select>
                          <label></label>
                          <span class="help-block">Select Availability *</span> 
                         </div>
                     </div>
                  </div>
                  </div>
                  <div class="form-actions right">
                    <button type="submit" class="btn blue" >Update</button>
                  </div>
                  <?php form_close(); ?>
          </div>
        </div>
      </div>
    </div>
          <input type="hidden" id="staff_if" value="<?php echo $maid_info->maid_id; ?>">
		  <?php
		  $m_id=$maid_info->maid_id;
				$currentAllocation = $this->dashboard_model->current_allocation($m_id); 
		  ?>
		  <input type="hidden" id="count" value="<?php echo $currentAllocation; ?>">
<script>


function check_avai(id){
	
	var a=$('#count').val();
	a=parseInt(a);
	if(a){
		
		swal("!Edit Conflict", "Staff unable to change availability while tasks are pending!", "error");
		$('#availability_select').val("Available");
		
	}
	
}
</script>




          