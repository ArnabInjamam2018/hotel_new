<!-- BEGIN PAGE CONTENT-->
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
<!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>

<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<!-- 17.11.2015-->
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Edit Miscellenious Income</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                            $form = array(
                                'class' 			=> '',
                                'id'				=> 'form',
                                'method'			=> 'post',								
                            );
							
							

                            echo form_open_multipart('dashboard/update_misc_income',$form);

                            ?>
    <div class="form-body">
      <?php
										if(isset($m_income)){
											//print_r($m_income);
											foreach($m_income as $value){
										
										}}	
								?>
      <div class="row">
        <div class="col-md-4">
          <div  class="form-group form-md-line-input">
            <input type="hidden" name="m_id" value="<?php echo $value->hotel_misc_income_id;?>">
            <input autocomplete="off" type="text" class="form-control date-picker" id="form_control_1" name="date" required="required" value="<?php echo $value->date?>" placeholder="Select Date *">
            <label></label>
            <span class="help-block">Select Date *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="recived_from" value="<?php echo $value->recived_from?>" placeholder="Recived From *">
            <label></label>
            <span class="help-block">Recived From *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="item"  required="required" value="<?php echo $value->item?>" placeholder="Item *">
            <label></label>
            <span class="help-block">Item *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="note" value="<?php echo $value->note?>" placeholder="Enter Note *">
            <label></label>
            <span class="help-block">Enter Note *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="recived_by" value="<?php echo $value->recived_by?>" placeholder="Recived By *">
            <label></label>
            <span class="help-block">Recived By *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="tax" name="tax"value="<?php echo $value->tax?>" placeholder="Tax %">
            <label></label>
            <span class="help-block">Tax %</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control" id="amount" name="amount" placeholder="Enter Amount *" value="<?php echo $value->amount?>">
            <label></label>
            <span class="help-block">Enter Amount *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="total" name="total"  required="required"  onkeypress=" return onlyNos(event, this);" value="<?php echo $value->total?>" placeholder="Enter Total *">
            <label></label>
            <span class="help-block">Enter Total *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select  class="form-control bs-select"  name="admin_name" required="required">
              <?php if(isset($admin)&& $admin){

											foreach ($admin as $value) {
												# code...
										?>
              <option value="<?php echo $value->admin_first_name." ".$value->admin_middle_name." ".$value->admin_last_name ?>"><?php echo $value->admin_first_name." ".$value->admin_middle_name." ".$value->admin_last_name?></option>
              <?php
										}}
									?>
            </select>
            <label></label>
            <span class="help-block">Admin Name *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" id="profit_center" name="profit_center" required="required" >
              <?php $pc=$this->dashboard_model->all_pc();?>
              <?php
						  $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
						  ?>
              <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
              <?php $pc=$this->dashboard_model->all_pc1();
							foreach($pc as $prfit_center){
							?>
              <option value="<?php echo $prfit_center->profit_center_location;?>" ><?php echo  $prfit_center->profit_center_location;?></option>
              <?php }?>
            </select>
            <label></label>
            <span class="help-block">Profit Center *</span> </div>
        </div>
        <?php foreach ($m_income as $value) {  ?>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select"  id="mop" name="mop" required="required" onchange="modesofpayments();">
              <?php
												
												$mop = $this->dashboard_model->get_payment_mode_list();
							  if($mop != false)
							  {
								  foreach($mop as $mp)
								  {
								?>
              <option value="<?php echo $mp->p_mode_name; ?>"  <?php if(isset($value->mode_of_payment) && $value->mode_of_payment){ if($value->mode_of_payment==$mp->p_mode_name) echo "selected";} ?>><?php echo $mp->p_mode_des; ?></option>
              <?php }
							  } ?>
            </select>
            <label></label>
            <span class="help-block">Modes of Payments *</span> </div>
        </div>
        <div id="check" style="display:none">
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="checkno"value="<?php //foreach ($admin as $value) { echo $value->draft_bank_name;}?>" placeholder="Check No *">
              <label></label>
              <span class="help-block">Check No *</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="check_bank_name" value="<?php if(isset($value->check_no) && $value->check_no) echo $value->check_no; ?>" placeholder="Bank Name *">
              <label></label>
              <span class="help-block">Bank Name *</span> </div>
          </div>
        </div>
        <!--End div of Payment mode by check--> 
        <!--Start of payment mode by draft-->
        <div id="draft" style="display:none">
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="draft_no" value="<?php if(isset($value->draft_no)&& $value->draft_no)echo $value->draft_no;?>">
              <label>Draft no.<span class="required">*</span></label>
              <span class="help-block">Enter draft no...</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control focus" id="form_control_1" name="draft_bank_name" value="<?php  if(isset($value->draft_bank_name)&& $value->draft_bank_name){echo $value->draft_bank_name;}?>" placeholder="Bank Name *">
              <label></label>
              <span class="help-block">Bank Name *</span> </div>
          </div>
        </div>
        <!--End of payment mode by draft--> 
        <!--Start of fundtranfer under common div-->
        <div id="fundtransfer" style="display:none">
          <div class="col-md-4">
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <input autocomplete="off" type="text" class="form-control focus" id="form_control_1" name="ft_bank_name" value="<?php if(isset($value->ft_bank_name)&& $value->ft_bank_name)echo $value->ft_bank_name;?>" placeholder="Enter Bank Name *">
              <label></label>
              <span class="help-block">Enter Bank Name *</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="ft_account_no" value="<?php if(isset($value->ft_account_no)&& $value->ft_account_no)echo $value->ft_account_no;?>" placeholder="Account no *">
              <label></label>
              <span class="help-block">Account no *</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control focus" id="form_control_1" name="ft_ifsc_code" value="<?php if(isset($value->ft_ifsc_code)&& $value->ft_ifsc_code)echo $value->ft_ifsc_code;?>" placeholder="IFSC code *">
              <label></label>
              <span class="help-block">IFSC code *</span> </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn blue" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>
<script>

$(document).ready(function(){
	modesofpayments();
});
</script> 
<script>

function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	   //alert(y);
	    
	   if(y=="check"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fund"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }
	
function total(){
	
	
}
</script> 
