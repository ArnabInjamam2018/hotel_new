
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green"> <span class="caption-subject bold uppercase"><i class="glyphicon glyphicon-bed"></i>&nbsp; Edit Profit Center</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
	
if(isset($datas)){
			foreach($datas as $value) {
				$id=$value->id;
                                $form = array(
                                    'class' 			=> 'form-body',
                                    'id'				=> 'form',
                                    'method'			=> 'post'
                                );
								//$id=$edit_id;
                                echo form_open_multipart('dashboard/edit_profit_center/'.$id,$form);
								/*$unit_type= $UnitDetail->unit_type;
								$unit_name= $UnitDetail->unit_name;
								$unit_class= $UnitDetail->unit_class;
								$unit_desc= $UnitDetail->unit_desc;*/
                                ?>
   <div class="form-body"> 
          <!-- 17.11.2015-->
          
          <div class="form-group">
            <div class="col-md-12 control-label">
             <!-- <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>-->
            </div>
          </div>
         
          <?php if($this->session->flashdata('succ_msg')):?>
          <div class="form-group">
            <div class="col-md-12 control-label">
              <div class="alert alert-success alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
            </div>
          </div>
          <?php endif;?>
          <!-- 17.11.2015-->
          
          <div class="row">
         <div class="form-group form-md-line-input form-md-floating-label col-md-4 ">
              <input autocomplete="off" type="text" class="form-control focus" id="form_control_1" name="pc_location" onkeypress=" return onlyLtrs(event, this);" required="required" value="<?php echo $value->profit_center_location ?>" >
              <label>Profit Center Location  <span class="required">*</span></label>
              <span class="help-block">Profit Center Location...</span> </div>
				<div   class="form-group form-md-line-input form-md-floating-label col-md-4">
               <input autocomplete="off" type="text" class="form-control focus"  name="balance"  maxlength="10" onkeypress=" return onlyNos(event, this);" value="<?php echo $value->profit_center_balance ?>" >
               <label>Enter Balance  </label>
               <span class="help-block">Enter Balance  ...</span> </div>
          
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <input autocomplete="off" type="text" class="form-control focus" id="form_control_1" name="des" value="<?php echo $value->profit_center_des ?>">
              <label>Description</label>
              <span class="help-block">Enter Description...</span> </div>
			  <input type="hidden" id="hid" value="<?php echo $value->status; ?>">
			   <div class="form-group form-md-line-input form-md-floating-label col-md-4">
            <select onchange="check_value(this.value)" class="form-control focus"  id="default" name="default" required="required" >
			  <option value="0">No</option>
              <option value="1">Yes</option>          
              
            </select>
            <label for="form_control_2">Default profit center<span class="required">*</span></label>
          </div>
           
          </div>
          </div>
          <div class="form-actions right">
            <button type="submit" class="btn blue" >Submit</button>
            <!-- 18.11.2015  -- onclick="return check_mobile();" -->
            <button  type="reset" class="btn default">Reset</button>
          </div>
    <?php form_close(); ?>
    <?php }}?>
    <script>
function check(){
	var a=$("#add_admin").val();
	var b=isNaN(a);
	if(b){
		alert("enter number only");
		$("#add_admin").val('')
	}
}

 function check_value(value){
		
		var stat=$("#hid").val();
		//alert(def);
		
			if(value==1 && stat==1){	
          $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/check_profit_center_default",
                data:{a:value},
                 success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                 

				swal({   title: "Are you sure?",
				text: data+"!",  
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Fix as Default!",   cancelButtonText: "No, cancel plz!", 
				closeOnConfirm: true,  
				closeOnCancel: true },
				function(isConfirm){   
				if (isConfirm) {     
				$("#default").val('1');
				} else {   
				$("#default").val('0');
				} });



				 
                }
            });
			}
			
			else if(value==1){
				swal({
						title: 'Inactive status!',
						text: 'Save change as active to set default.',
					timer: 2000
						});
						
						$("#default").val('0');
						

			}
			
			
		
	}
</script> 
  </div>
</div>
