
  <?php if($this->session->flashdata('err_msg')):?>
  <div class="form-group">
    <div class="col-md-12 control-label">
      <div class="alert alert-danger alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
    </div>
  </div>
  <?php endif;?>
  <?php if($this->session->flashdata('succ_msg')):?>
  <div class="form-group">
    <div class="col-md-12 control-label">
      <div class="alert alert-success alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
    </div>
  </div>
  <?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Edit Purchase</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

            $form = array(
                'class' 			=> '',
                'id'				=> 'form',
                'method'			=> 'post'
            );

            echo form_open_multipart('dashboard/edit_purchase/'.$this->uri->segment(3),$form);

            ?>
    <div class="form-body">
      <div class="row">
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" autocomplete="off" class="form-control" name="p_id" required="required" value="<?php echo $purchase->p_id;?>" placeholder="Purchase Id *">
              <label></label>
              <span class="help-block">Purchase Id *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select name="a_id" id="a_id" class="form-control bs-select" required="required">
                <option value="0" disabled selected>Select A Value</option>
                <?php $assets=$this->dashboard_model->all_assets_limit();
                                if($assets && isset($assets)){
                                foreach($assets as $asset){
                                ?>
                <option value="<?php echo $asset->a_id ?>" <?php if($purchase->a_id == $asset->a_id){ echo "selected";}?>> <?php echo $asset->a_name ?></option>
                <?php }} ?>
              </select>
              <label></label>
              <span class="help-block">Select a value *</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input" onclick="getfocus()">
              <input type="text" autocomplete="off" required="required" value="<?php list($a,$b)= explode(" ",$purchase->p_date); list($y,$m,$d)= explode("-",$a); echo $m.'/'.$d.'/'.$y;?>" name="p_date" class="form-control date-picker"  id="c_valid_from" data-required="required" placeholder="Purchase Date *">
              <label></label>
              <span class="help-block">Purchase Date *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" autocomplete="off" class="form-control" name="p_supplier" required="required"  value="<?php echo $purchase->p_supplier;?>">
              <label></label>
              <span class="help-block">Supplier Name *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" autocomplete="off" class="form-control" name="p_i_name" required="required"  value="<?php echo $purchase->p_i_name;?>">
              <label></label>
              <span class="help-block">Item Name *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" autocomplete="off" class="form-control" name="p_i_description"  value="<?php echo $purchase->p_i_description;?>" placeholder="Item Description *">
              <label></label>
              <span class="help-block">Item Description *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" autocomplete="off" class="form-control" id="p_i_price" name="p_i_price" required="required"  value="<?php echo $purchase->p_i_price;?>" placeholder="Item Price *">
              <label></label>
              <span class="help-block">Item Price *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" autocomplete="off" onblur="calculate_price(this.value)" class="form-control" name="p_i_quantity" required="required"  value="<?php echo $purchase->p_i_quantity;?>" placeholder="Item Quantity *">
              <label></label>
              <span class="help-block">Item Quantity *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" autocomplete="off" class="form-control" name="p_unit" required="required"  value="<?php echo $purchase->p_unit;?>" placeholder="Item Unit *">
              <label></label>
              <span class="help-block">Item Unit *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" autocomplete="off" class="form-control" id="p_price" name="p_price" required="required"  value="<?php echo $purchase->p_price;?>" >
              <label></label>
              <span class="help-block">Item Price *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input onblur="final_price_1(this.value)" type="text" autocomplete="off" class="form-control" name="p_tax" required="required"  value="<?php echo $purchase->p_tax;?>" placeholder="Tax(%) *">
              <label></label>
              <span class="help-block">Tax(%) *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input onblur="final_price_2(this.value)" type="text" autocomplete="off" class="form-control" name="p_discount" required="required"  value="<?php echo $purchase->p_discount;?>" placeholder="Discount(%) *">
              <label></label>
              <span class="help-block">Discount(%) *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" autocomplete="off" class="form-control" id="p_total_price" name="p_total_price" required="required" value="<?php echo $purchase->p_total_price;?>" placeholder="Item Total Price *">
              <label></label>
              <span class="help-block">Item Total Price *</span> </div>
        </div>
        <div class="col-md-4">  
          <div class="form-group form-md-line-input">
         <select class="form-control bs-select" id="profit_center" name="profit_center" required="required" >
         	<option value="" selected="selected" disabled="disabled">Profit Center</option>
            <?php $pc=$this->dashboard_model->all_pc();?>
                      <?php
                      $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
                      ?>
                      <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
                       <?php $pc=$this->dashboard_model->all_pc1();
                        foreach($pc as $prfit_center){
                        ?>
                      
                      <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                        <?php }?>
          </select>
          <label></label>
          <span class="help-block">Profit Center *</span> </div>
        </div>          
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn blue" >Submit</button>
    </div>
    <?php form_close(); ?>
  </div>
</div>
<div class="clearfix">
  <?php  $this->session->flashdata('succ_msg') ?>
</div>
<script>
    function calculate_price(id){

        var unit_price=document.getElementById("p_i_price").value;
       // alert(unit_price);
        document.getElementById("p_price").focus();
        document.getElementById('p_price').value =parseInt(unit_price)*parseInt(id);
    }

    function final_price_1(value){

        var item_price= document.getElementById('p_price').value;

        var item_price_int=parseInt(item_price);
        var value_int=parseInt(value);
        document.getElementById("p_total_price").focus();
        document.getElementById('p_total_price').value=item_price_int+(item_price_int*(value_int/100));

    }

    function final_price_2(value){

        var item_price= document.getElementById('p_total_price').value;

        var item_price_int=parseInt(item_price);
        var value_int=parseInt(value);

        document.getElementById('p_total_price').value=item_price_int-(item_price_int*(value_int/100));

    }
$(document).on('blur', '#c_valid_from', function () {
	$('#c_valid_from').addClass('focus');
});
</script>