<!-- BEGIN PAGE CONTENT-->
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
 <!-- 17.11.2015-->
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <!-- 17.11.2015-->
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Release Item</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                            $form = array(
                                'class' 			=> '',
                                'id'				=> 'form',
                                'method'			=> 'post',								
                            );
							
							

                            echo form_open_multipart('dashboard/update_release_item',$form);

                            ?>
    <div class="form-body"> 
      <div class="row">
        <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Found Item</h4>
              </div>
              <div class="modal-body">
                <?php 
                                	if(isset($r_item)){
										//print_r($r_item);
										//exit;
                                		foreach ($r_item as $value) {
											//$id=$value->hotel_release_item_id;
											
											
												# code...
											//echo "<pre>";
											//print_r($value);
										}
                                	}
                                ?>
                <a class="btn default blue" data-toggle="modal" href="" onclick="fetch_item('Electronics')"> Electronics Items</a> <a class="btn default blue" data-toggle="modal" href="" onclick="fetch_item('Jewelery')"> Jewelery</a> <a class="btn default blue" data-toggle="modal" href="" onclick="fetch_item('Id Card')"> Id Card</a> <a class="btn default blue" data-toggle="modal" href="" onclick="fetch_item('Bag')"> Bag</a> <a class="btn default blue" data-toggle="modal" href="" onclick="fetch_item('Others')"> Other</a> </div>
              <!--<ul>
                                        	<li><a href="#" class="btn default">iosedrknw</a></li>
                                            <li><a href="#" class="btn default">iosedrknw</a></li>
                                            <li><a href="#" class="btn default">iosedrknw</a></li>
                                            <li><a href="#" class="btn default">iosedrknw</a></li>
                                            <li><a href="#" class="btn default">iosedrknw</a></li>
                                            <li><a href="#" class="btn default">iosedrknw</a></li>
                                            <li><a href="#" class="btn default">iosedrknw</a></li>
                                            <li><a href="#" class="btn default">iosedrknw</a></li>
                                            <li><a href="#" class="btn default">iosedrknw</a></li>
                                        </ul>-->
              <div id="mdl1"> </div>
              <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content --> 
          </div>
          <!-- /.modal-dialog --> 
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input type="hidden" name="r_id" value="<?php echo $value->hotel_release_item_id?>">
          <input autocomplete="off" type="text" class="form-control date-picker" id="form_control_1" name="date" value="<?php echo $value->date?>" placeholder="Select Date">
          <label></label>
          <span class="help-block">Select Date</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="select_itm" name="item" data-toggle="modal" href="#basic" value="<?php echo $value->item?>" placeholder="Select item *">
              <label></label>
              <span class="help-block">Select item *</span>
            </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="release_by"value="<?php echo $value->release_by?>" placeholder="Release By *">
          <label></label>
          <span class="help-block">Release By *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" type="text" class="form-control date-picker" id="form_control_1" name="release_date" value="<?php echo $value->release_date?>" placeholder="Release Date *">
          <label></label>
          <span class="help-block">Release Date *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" type="text" class="form-control timepicker timepicker-24" id="form_control_1" name="release_time" value="<?php echo $value->release_time?>" placeholder="Release Time">
          <label></label>
          <span class="help-block">Release Time</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="claim_by" value="<?php echo $value->claim_by?>" placeholder="Claim By Name">
          <label></label>
          <span class="help-block">Claim By Name</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="mobile" name="g_contact_no"  required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" value="<?php echo $value->g_contact_no?>" placeholder="Mobile No *">
          <label></label>
          <span class="help-block">Mobile No *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="booking_id" value="<?php echo $value->booking_id?>" placeholder="Booking Id">
          <label></label>
          <span class="help-block">Booking Id</span> </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn blue" >Submit</button>
    </div>
    <?php form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>
<script>
	function fetch_item(value){
		
		       // alert("ggg");
		 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/fetch_item",
                data:{item: value},
                success:function(data1)
                {	
						//alert(data1);
						$('#mdl1').html(data1);
					//alert(data.itm);
                }
            });

	}


   
    
	function getVal(val){
		document.getElementById("select_itm").value=val;
		$('#basic').modal('hide');
	}
</script> 
