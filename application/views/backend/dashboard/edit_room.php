<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"> <span class="caption-subject bold uppercase"><i class="glyphicon glyphicon-bed"></i>&nbsp; Update Room</span> </div>
	</div>
	<div class="portlet-body form">
		<?php
		$form = array(
			'class' => '',
			'id' => 'form',
			'method' => 'post'
		);
		echo form_open_multipart( 'dashboard/edit_room', $form );

		?>
		<div class="form-body">
			<div class="row">
				<?php 
		if (isset($value)){
			foreach ($value as $room_edit) { 
			$unit = $this->dashboard_model->get_unit_details($room_edit->unit_id);
			//print_r($unit);
	?>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<select name="unit_type" id="unit_type" class="form-control bs-select" required onchange="get_unit_name(this.value)">
							<option value=""  selected="">Unit Category</option>
							
							<?php
							$f = 0;
							$cate = $this->unit_class_model->all_unit_type_category_data();
							
							foreach ( $cate as $cat ) {
								if ( $unit[ 'unit_type' ] == $cat->name ) {
									$f = 1;
								}
								?>
							<option value="<?php echo $cat->name;?>" <?php if($f==1 ){ echo 'selected="selected"'; $f=0;} ?>>
								<?php echo $cat->name;?>
							</option>
							<?php
							}
							?>
							
            
						</select>
						<label></label>
						<span class="help-block">Unit Category *</span> </div>
				</div>

				<div class="col-md-3">
					<div class="form-group form-md-line-input" id="unit_nty">
						<select class="form-control bs-select" id="unitName" name="unit_name" required onchange="get_unit_class(this.value)">
					<option value="" disabled="" selected="">Unit Type</option>
					<?php //$type = $this->unit_class_model->all_unit_type1();
						//if(isset($type) && $type){
							//foreach($type as $ty){
						?>
							
							
							<option value="<?php echo $unit['id'];?>" selected="selected"><?php echo $unit['unit_name'];?></option>
							
							
						
						<?php //}} ?>
						</select>
						<label></label>
						<span class="help-block">Unit Type *</span> </div>
				</div>





				<div class="col-md-3">
					<div class="form-group form-md-line-input" id="unitClass">
						<input type="text" autocomplete="off" class="form-control" readonly id="unit_class" name="unit_class" value="<?php echo $unit['unit_class'];?>" required="required">
						<label></label>
						<span class="help-block">Unit Class *</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input" id="unitClass">
						<input type="text" autocomplete="off" class="form-control" id="room_name" name="room_name" placeholder="Room Name" value="<?php echo $room_edit->room_name; ?>">
						<label></label>
						<span class="help-block">Room Name *</span>
					</div>
				</div>				
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input type="text" autocomplete="off" class="form-control" id="ct_name" name="room_no"  required="required" value="<?php echo $room_edit->room_no; ?>" placeholder="Room No *">
						<label></label>
						<span class="help-block">Room No *</span> </div>
				</div>
				<input type="hidden" name="room_id" value="<?php echo $room_edit->room_id; ?>"/>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<?php $floors=$this->dashboard_model->get_floor();
            foreach($floors as $floor){
                $floor_no=$floor->hotel_floor;
            }
            $i=0;
    
            //Number builder start
    
            //$locale = 'en_US';
            // $nf = new NumberFormatter($locale, NumberFormatter::ORDINAL);
            //number builder end
    
    
    
            ?>
						<select class="form-control bs-select" id="agency_yn" name="floor_no" value="lala" required>
							<option disabled selected> Select The Floor </option>
							<!--<option value="select the floor" disabled="disabled">--Select The Floor--</option>-->
							<?php while($i<$floor_no){ ?>
							<option value="<?php echo $i ?>" <?php if($room_edit->floor_no == $i){ echo 'selected="selected"'; } ?>>
								<?php  echo $i.substr(date('jS', mktime(0,0,0,1,($i%10==0?9:($i%100>20?$i%10:$i%100)),2000)),-2); ?>
							</option>
							<?php $i++; } ?>
						</select>
						<label></label>
						<span class="help-block">Select The Floor</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" id="agency_yn" name="room_bed" required>
							<option value="" disabled="disabled" selected="selected">Bed No</option>
							<option value="1" <?php if($room_edit->room_bed == '1'){ echo 'selected="selected"'; } ?>>1</option>
							<option value="2" <?php if($room_edit->room_bed == '2'){ echo 'selected="selected"'; } ?>>2</option>
							<option value="3" <?php if($room_edit->room_bed == '3'){ echo 'selected="selected"'; } ?>>3</option>
							<option value="4" <?php if($room_edit->room_bed == '4'){ echo 'selected="selected"'; } ?>>4</option>
						</select>
						<label></label>
						<span class="help-block">Bed No</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input type="type" autocomplete="off" class="form-control" id="form_control_1" name="room_rent" required="required" onkeypress="return onlyNos(event, this);" value="<?php echo $room_edit->room_rent; ?>" placeholder="Base Room Rent *">
						<label></label>
						<span class="help-block">Base Room Rent *</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input type="type" autocomplete="off" class="form-control" id="form_control_1" name="room_rent_weekend" required="required" onkeypress="return onlyNos(event, this);" value="<?php echo $room_edit->room_rent_weekend; ?>" placeholder="Weekend Room Rent *">
						<label></label>
						<span class="help-block">Weekend Room Rent *</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input type="type" autocomplete="off" class="form-control" id="form_control_1" name="room_rent_seasonal" required="required" onkeypress="return onlyNos(event, this);" value="<?php echo $room_edit->room_rent_seasonal; ?>" placeholder="Seasonal Room Rent *">
						<label></label>
						<span class="help-block">Seasonal Room Rent *</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input value="<?php echo $room_edit->max_discount; ?>" type="type" autocomplete="off" class="form-control" id="form_control_1" name="max_discount" required="required" onkeypress="return onlyNos(event, this);" placeholder="Maximum Discount *">
						<label></label>
						<span class="help-block">Maximum Discount *</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<select id="rate_type" class="form-control bs-select" name="rate_type" onchange="get_additional_value(this.value)">
							<option value="" disabled="disabled" selected="selected">Rate type</option>
							<option value="0" <?php if($room_edit->rate_type == '0'){ echo "selected";}?>>Fixed</option>
							<option value="1" <?php if($room_edit->rate_type == '1'){ echo "selected";}?>>Depends on person</option>
						</select>
						<label></label>
						<span class="help-block">Rate type *</span> </div>
				</div>
				<div class="col-md-12">
					<div class="form-group form-md-line-input" id="unitClass">
						<input type="text" autocomplete="off" class="form-control" id="note" name="note" placeholder="Note" value="<?php echo $room_edit->note;?>">
						<label></label>
						<span class="help-block">Note</span>
					</div>
				</div>

				<?php if($room_edit->rate_type == '1'){?>
				<div id="smt" class="col-md-12">
					<?php } else{ ?>
					<div style="display:none;" id="smt" class="col-md-12">
						<?php } ?>
						<div class="row">
							<label class="col-md-4" style="padding-top: 27px;">Additional Adult (Age>=18) Rate</label>
							<div class="col-md-4" style="padding-top: 21px;">
								<div class="md-radio-inline">
									<div class="md-radio">
										<input type="radio" name="adult_radio" id="adult_radio1" class="md-radiobtn" value="1" <?php if($room_edit->adult_rate_type == '1'){ echo "checked";}?>>														   
										<label for="adult_radio1">
											<span></span>
											<span class="check"></span>
											<span class="box"></span> Percentage(%) 
										</label>
									</div>
									<div class="md-radio">
										<input type="radio" name="adult_radio" id="adult_radio2" class="md-radiobtn" value="0" <?php if($room_edit->adult_rate_type == '0'){ echo "checked";}?>>														   
										<label for="adult_radio2">
											<span></span>
											<span class="check"></span>
											<span class="box"></span> Fixed 
										</label>
									</div>
								</div>								
							</div>
							<div class="col-md-4">
								<div class="form-group form-md-line-input">
									<input id="adult_rate" name="adult_rate" class="form-control focus" type="text" value="<?php echo $room_edit->adult_rate; ?>" placeholder="Rate *">
									<label></label>
									<span class="help-block">Rate *</span> 
								</div>
							</div>
							<label class="col-md-4" style="padding-top: 27px;">Additional Kid (age>6) Rate</label>
							<div class="col-md-4" style="padding-top: 21px;">
								<div class="md-radio-inline">
									<div class="md-radio">
										<input type="radio" name="kid_radio" id="kid_radio1" class="md-radiobtn" value="1" <?php if($room_edit->kid_rate_type == '1'){ echo "checked";}?>>	<label for="kid_radio1">
											<span></span>
											<span class="check"></span>
											<span class="box"></span> Percentage(%) 
										</label>
									</div>
									<div class="md-radio">
										<input type="radio" name="kid_radio" id="kid_radio2" class="md-radiobtn" value="0" <?php if($room_edit->kid_rate_type == '0'){ echo "checked";}?>>	<label for="kid_radio2">
											<span></span>
											<span class="check"></span>
											<span class="box"></span> Fixed 
										</label>
									</div>
								</div>	
							</div>
							<div class="col-md-4">
								<div class="form-group form-md-line-input">
									<input id="kid_rate" name="kid_rate" class="form-control focus" type="text" value="<?php echo $room_edit->kid_rate; ?>" placeholder="Rate *">
									<label></label>
									<span class="help-block">Rate *</span> 
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group form-md-checkboxes form-md-line-input">
							<label style=" font-size:16px; padding: 30px 0 10px;">Available Features</label>
							<div class="add-roo">
								<div class="row">
									<?php 
				//echo "<pre>";
				//print_r($rf);
				if(isset($rf) && !empty($rf)){		
				foreach($rf as $q){
					$arr[]=$q['room_feature_id'];
				}
				}
				//echo "<pre>";
				//print_r($arr);
				if(isset($room_feature)	&& $room_feature!=''){
				foreach($room_feature as $qry){ 
				//echo "<pre>";
				//print_r($qry);
				
				?>
									<div class="col-md-3">
										<div class="form-group form-md-checkboxes" style="margin-bottom: 0;">
											<div class="md-checkbox">
											<input type="checkbox" id="chk_<?php echo $qry->room_feature_id; ?>" class="md-check" name="room_feature[]" value="<?php echo $qry->room_feature_id;?>" onclick="showType('<?php echo $qry->room_feature_id;?>')" <?php if(isset($arr) && in_array($qry->room_feature_id, $arr)) { echo "checked"; } ?>>
											<label for="chk_<?php echo $qry->room_feature_id; ?>"> <span></span> <span class="check"></span> <span class="box"></span> <?php echo $qry->room_feature_name; ?> </label>
											</div>
									</div>
											<?php if(isset($arr) && in_array($qry->room_feature_id, $arr)) { 
															   $room_feature_type = $this->dashboard_model->get_room_feature_type($qry->room_feature_id,$_GET['room_id']); 
															   //echo $room_feature_type->room_feature_type;
															   //} ?>
											<div class="showhidetarget" id="featureType_<?php echo $qry->room_feature_id;?>" <?php if(isset($arr) && in_array($qry->room_feature_id, $arr)) { echo 'style=display:block'; } else { echo 'style=display:none'; } ?>>
												<div class="form-group form-md-radios" style="margin-bottom: 0;">
													<div class="md-radio-inline">
														<div class="md-radio">
													   		<input type="radio" name="feature_type_<?php echo $qry->room_feature_id; ?>" id="feature_type_<?php echo $qry->room_feature_id; ?>_1" value="0" onclick="getVal('<?php echo $qry->room_feature_id;?>',this.value)" <?php if(isset($room_feature_type->room_feature_type) && $room_feature_type->room_feature_type == '0'){ echo 'checked="checked"'; } ?> class="md-radiobtn">														   
															<label for="feature_type_<?php echo $qry->room_feature_id;?>_1">
																<span></span>
																<span class="check"></span>
																<span class="box"></span> Free </label>
														</div>
														<div class="md-radio">
													   		<input type="radio" name="feature_type_<?php echo $qry->room_feature_id; ?>" onclick="getVal('<?php echo $qry->room_feature_id;?>',this.value)" id="feature_type_<?php echo $qry->room_feature_id; ?>_2" value="1" <?php if(isset($room_feature_type->room_feature_type) && $room_feature_type->room_feature_type == '1'){ echo 'checked="checked"'; } ?> class="md-radiobtn">
															<label for="feature_type_<?php echo $qry->room_feature_id;?>_2">
																<span></span>
																<span class="check"></span>
																<span class="box"></span> Chargeable </label>
														</div>
													</div>
												</div>
											
												<input type="hidden" name="uamt_<?php echo $qry->room_feature_id;?>" id="uamt_<?php echo $qry->room_feature_id;?>" value="<?php echo $room_feature_type->room_feature_type;?>">
												<?php if(isset($room_feature_type->room_feature_type) && $room_feature_type->room_feature_type == '1'){
		      //echo $qry->room_feature_id;
			  //echo "<br>";
			  //echo $_REQUEST['room_id'];
			  $room_feature_charge = $this->dashboard_model->get_room_feature_charge($qry->room_feature_id,$_REQUEST['room_id']); 
			  $val = $room_feature_charge->room_feature_charge;
		  ?>
												<div id="samt_<?php echo $qry->room_feature_id;?>" style="margin: 0 0 10px 0; padding: 0;" class="form-group form-md-line-input">
													<?php } else {  $val = 0; ?>
													<div id="samt_<?php echo $qry->room_feature_id;?>" style="display:none; margin: 0 0 10px 0; padding: 0;" class="form-group form-md-line-input">
														<?php } ?>
														<input type="text" value="<?php echo $val;?>" name="amt_<?php echo $qry->room_feature_id;?>" id="amt_<?php echo $qry->room_feature_id;?>" class="form-control" placeholder="Chargeable Amount" style="font-size:13px;">
														<label></label>
													</div>
											</div>
											<?php } else {?>
											<div class="showhidetarget" id="featureType_<?php echo $qry->room_feature_id;?>" <?php if(isset($arr) && in_array($qry->room_feature_id, $arr)) { echo 'style=display:block'; } else { echo 'style=display:none'; } ?>>
												<div class="form-group form-md-radios" style="margin-bottom: 0;">
													<div class="md-radio-inline">
														<div class="md-radio">
													   		<input type="radio" name="feature_type_<?php echo $qry->room_feature_id; ?>" id="feature_type_<?php echo $qry->room_feature_id; ?>_1" onclick="getVal('<?php echo $qry->room_feature_id;?>',this.value)" value="0" checked="checked" class="md-radiobtn">
															<label for="feature_type_<?php echo $qry->room_feature_id;?>_1">
																<span></span>
																<span class="check"></span>
																<span class="box"></span> Free </label>
														</div>
														<div class="md-radio">
													   		<input type="radio" name="feature_type_<?php echo $qry->room_feature_id; ?>" onclick="getVal('<?php echo $qry->room_feature_id;?>',this.value)" id="feature_type_<?php echo $qry->room_feature_id; ?>_2" value="1" class="md-radiobtn">
															<label for="feature_type_<?php echo $qry->room_feature_id;?>_2">
																<span></span>
																<span class="check"></span>
																<span class="box"></span> Chargeable </label>
														</div>
													</div>
												</div>
												<input type="hidden" name="uamt_<?php echo $qry->room_feature_id;?>" id="uamt_<?php echo $qry->room_feature_id;?>" value="0">
												<div id="samt_<?php echo $qry->room_feature_id;?>" style="display:none; margin: 0 0 10px 0; padding: 0;" class="form-group form-md-line-input">
                    <input type="text" name="amt_<?php echo $qry->room_feature_id;?>" id="amt_<?php echo $qry->room_feature_id;?>" class="form-control" placeholder="Chargeable Amount" style="font-size:13px;">
                    <label></label>
                    </div>
												</div>
											<?php } ?>
									</div>
									<?php } 
				}
			  ?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input uploadss">
							<label>Room Image</label>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px;"> <img src="<?php echo base_url();?>upload/unit/thumb/image1/<?php if($room_edit->room_image_thumb == '') { echo "no_images.png "; } else { echo $room_edit->room_image_thumb; } ?>" alt=""/> </div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
								<div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
									<?php echo form_upload('image1');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input uploadss">
							<label>Room Image 2</label>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px;"> <img src="<?php echo base_url();?>upload/unit/thumb/image2/<?php if($room_edit->room_image_2_thumb == '') { echo "no_images.png "; } else { echo $room_edit->room_image_2_thumb; } ?>" alt=""/> </div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
								<div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
									<?php echo form_upload('image2');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input uploadss">
							<label>Room Image 3</label>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px;"> <img src="<?php echo base_url();?>upload/unit/thumb/image3/<?php if($room_edit->room_image_3_thumb == '') { echo "no_images.png "; } else { echo $room_edit->room_image_3_thumb; } ?>" alt=""/> </div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
								<div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
									<?php echo form_upload('image3');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input uploadss">
							<label>Room Image 4</label>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px;"> <img src="<?php echo base_url();?>upload/unit/thumb/image4/<?php if($room_edit->room_image_4_thumb == '') { echo "no_images.png "; } else { echo $room_edit->room_image_4_thumb; } ?>" alt=""/> </div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
								<div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
									<?php echo form_upload('image4');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-actions right">
				<button type="submit" class="btn blue">Update</button>
			</div>
			<!-- END CONTENT -->
			<?php 
                            }
                        }

                                                
            form_close(); ?>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<script>
		/*function get_unit_name(val){
			//alert(val);
			$.ajax({
			   type: "POST",
			   url: "<?php //echo base_url();?>dashboard/get_unit_name",
			   data: "val="+val,
			   success: function(msg){
				    $("#unitName").html(msg);
			   }
			});
		}*/
		function get_unit_name( val ) {
			//alert(val);
			$.ajax( {
				type: "POST",
				url: "<?php echo base_url();?>dashboard/get_unit_name",
				data: "val=" + val,
				success: function ( msg ) {
					//alert(msg);
					if ( msg == 0 ) {
						swal( {
								title: "Select another category!",
								text: "This category does not have any unit type defined under it.",
								type: "error",
								confirmButtonColor: "#F27474"
							} )
							//swal("Select another category!", "This category does not have any unit type defined under it.", "error");
					}
					$( "#unit_nty" ).html( msg );
					$.getScript('<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js');
					$.getScript('<?php echo base_url();?>assets/pages/scripts/components-select2.min.js');
				}
			} );
		}

		function get_unit_class( val ) {
			//alert(val);
			$.ajax( {
				type: "POST",
				url: "<?php echo base_url();?>dashboard/get_unit_class",
				data: "val=" + val,
				success: function ( msg ) {
					$( "#unitClass" ).html( msg );
				}
			} );
		}

		function showType( val ) {
			//alert(val);
			$( "#featureType_" + val ).toggle();
		}
		$( document ).ready( function () {
			//$('.showhidetarget').hide();
		} );

		function getVal( id, val ) {
			//alert(id);
			//alert(val);
			if ( val == '1' ) {
				document.getElementById( "samt_" + id ).style.display = "block";
				document.getElementById( "uamt_" + id ).value = "1";
			} else {
				document.getElementById( "uamt_" + id ).value = "0";
				document.getElementById( "amt_" + id ).value = "";
				document.getElementById( "samt_" + id ).style.display = "none";

			}
		}

		function get_additional_value( val ) {
			if ( val == '1' ) {
				document.getElementById( "smt" ).style.display = "block";
			} else {
				document.getElementById( "smt" ).style.display = "none";

			}
		}
	</script>
	<!-- END CONTENT -->
	<!--All Code done before-->
</div> 
<!-- modal add under-->