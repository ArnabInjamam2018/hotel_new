<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Edit Shift</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

if(isset($datas) && $datas){
foreach($datas as $data){ $id=$data->shift_id;
        $form = array(
            'class' 			=> '',
            'id'				=> 'form',
            'method'			=> 'post'
        );

        echo form_open_multipart('Dashboard/edit_shift/'.$id,$form);

        ?>
    <div class="form-body">
        <div class="row">
        	<div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" class="form-control" name="shift_name" required="required" value="<?php echo $data->shift_name;?>">
                <label></label>
                <span class="help-block">Shift Name *</span> </div>
          </div>
		 
          <div class="col-md-4">
              <input type="hidden" name="hid" value="<?php echo $data->shift_id;?>">
              <div class="form-group form-md-line-input" >
                <input type="text" autocomplete="off" required="required" name="shift_opening_time" class="form-control timepicker timepicker-24"  id="shift_opening_time" value="<?php echo $data->shift_opening_time;?>" placeholder="Shift From *">
                <label></label>
                <span class="help-block">Shift From *</span> </div>
          </div>
          <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" required="required" name="shift_closing_time" class="form-control timepicker timepicker-24" id="shift_closing_time" alue="<?php echo $data->shift_closing_time;?>" placeholder="Shift Up to *">
            <label></label>
            <span class="help-block">Shift Up to *</span> </div>
          </div>
        </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn blue" >Submit</button>
      <button type="submit" class="btn default">Reset</button>
    </div>
<?php }} form_close(); ?>
  </div>
</div>
<script>
    //function getfocus() {
        //alert("ulala")
       // document.getElementById("c_valid_from").focus();
        //e.prevent();
   // }

</script>