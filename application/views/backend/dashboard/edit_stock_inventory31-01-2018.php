<!-- BEGIN PAGE CONTENT-->
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>

<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Stock/Inventory</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/update_asset',$form);

                        ?>
    <div class="form-body"> 
      <!-- 17.11.2015-->
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <!-- 17.11.2015-->
      
      <div class="row">
        <?php
            
            
        if(isset($assets) && $assets){
	   foreach($assets as $asset){
    }	
        
    }?>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select"  name="a_type" required="required" >
              <option value="" selected="selected" disabled="disabled">Select Type</option>
              <?php 
                if(isset($type) && $type){
           
                foreach($type as $types){
                        
            ?>
              <option value="<?php echo $types->asset_type_name;?>" <?php if($asset->item_type==$types->asset_type_name) echo "selected";?>><?php echo $types->asset_type_name;?></option>
              
              <?php }}?>
            </select>
            <label></label>
            <span class="help-block">Select Type *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select"  id="a_category" name="a_category" required="required" onchange="get_item_name()" >
              <option value="" selected="selected" disabled="disabled">Select Category</option>
              <?php 
                if(isset($category)){
                    //print_r($category);
                    //exit;
                foreach($category as $categorys){
                    foreach($assets as $asset){
                        
        ?>
              <option <?php if($categorys->name == $asset->a_category){echo "selected"; }?> value="<?php echo $categorys->name; ?>"><?php echo $categorys->name; ?></option>
              <?php 
                
                }}	}	
                ?>
            </select>
            <label></label>
            <span class="help-block">Select Category *</span> </div>
        </div>
        <input type="hidden" name="id" value="<?php echo $asset->hotel_stock_inventory_id;?>">
        <input type="hidden" name="qty" value="<?php echo $asset->qty;?>">
        <input type="hidden" name="opening_qty" value="<?php echo $asset->opening_qty;?>">
        <input type="hidden" name="closing_qty" value="<?php echo $asset->closing_qty;?>">
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="a_name" value="<?php echo $asset->a_name;?>" placeholder="Assets Name">
            <label></label>
            <span class="help-block">Assets Name</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="make" value="<?php echo $asset->make;?>" placeholder="Make">
            <label></label>
            <span class="help-block">Make</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="i_des" value="<?php echo $asset->item_des;?>" placeholder="Item Desription">
            <label></label>
            <span class="help-block">Item Desription</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="u_price" required="required" value="<?php echo $asset->u_price;?>" placeholder="Unit Price *">
            <label></label>
            <span class="help-block">Unit Price *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="unit" required="required" value="<?php echo $asset->unit;?>" >
            <label></label>
            <span class="help-block">Unit *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select"  name="warehouse" required="required" >
              <?php if(isset($warehouse)){
            //print_r($warehouse);
            foreach($warehouse as $wh){
          ?>
              <option value="<?php echo $wh->id;?>" <?php if($asset->warehouse == $wh->id) { echo 'selected="selected"';}?>><?php echo $wh->name;?></option>
              <?php }}?>
            </select>
            <label></label>
            <span class="help-block">Select Warehouse *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="l_stock" value="<?php echo $asset->l_stock;?>" placeholder="Low Stock Alert Qty" />
            <label></label>
            <span class="help-block">Low Stock Alert Qty *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="note" value="<?php echo $asset->note;?>" placeholder="Note">
            <label></label>
            <span class="help-block">Note</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="importance" id="priority" class="form-control bs-select">
              <option value="" selected="selected" disabled="disabled">Importance</option>
              <option value="1">High</option>
              <option value="2">Medium</option>
              <option value="3">Low</option>
            </select>
            <label></label>
            <span class="help-block">Importance</span> </div>
        </div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input uploadss">
            <label>Upload Image</label>
            <div class="fileinput fileinput-new" data-provides="fileinput">
              <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>upload/asset/<?php if($asset->a_image=='') { echo "no_images.png"; } else { echo $asset->a_image;}?>" alt=""/> </div>
              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
              <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('a_image');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn blue" >Submit</button>
    </div>
    <?php form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>
<script>
   function check_corporate(value){

       if(value =="corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
       }else{
           document.getElementById("c_name").style.display="none";
           document.getElementById("c_des").style.display="none";
       }
   }
    function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});

function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	   //alert(y);
	    
	   if(y=="check"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fundtransfer"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }
</script> 
