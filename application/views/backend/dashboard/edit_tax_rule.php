<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Edit Tax Rule</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
                            $form = array(
                                'class' 			=> '',
                                'id'				=> 'form',       
                                'method'			=> 'post',								
                            );
                            echo form_open_multipart('dashboard/update_tax_rule',$form);
    ?>
    <div class="form-body">      
      <div class="row">
	  
	  <?php 
	  $rule_id='';
	  if(isset($tax_rule) && $tax_rule){
		 //foreach($tax_rule as $tr)
		// print_r($tax_rule);
		// die();
		 $rule_id = $tax_rule->r_id;
		 ?>
	<!-- start editing -->
	<input type="hidden" id="rules_id" name="rules_id" value="<?php echo $rule_id; ?>">
	<div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="r_name" id="r_name" type="text" class="form-control" placeholder="Rule name"  value="<?php echo $tax_rule->r_name; ?>" required/>
            <label></label>
            <span class="help-block">Rule name*</span> </div>
        </div>
		
		
	  <div class="col-md-4">
	  <div class="form-group form-md-line-input">
	  <select class="form-control input-sm" name="tax_cat" id="tax_cat" required>
			
			<?php $tax = $this->dashboard_model->get_distinct_tax_category();  

				//print_r($tax);
				if($tax != false) {
					foreach($tax as $tx)
					{
				?>
				<option value="<?php echo $tx->tc_id; ?>" <?php if($tax_rule->r_cat == $tx->tc_id) echo "selected";?> ><?php echo $tx->tc_name." "." "."(".$tx->tc_type.")"; ?></option>
				<?php
					}			
				}				?>
			</select>
		</div>
		</div>	
		<?php
			$date_range = $tax_rule->r_start_date." "."~"." ".$tax_rule->r_end_date ;
			//die();
		?>
		<div class="col-md-4">
          <div class="form-group form-md-line-input" id="defaultrange1">
            <input name="r_start_dates" id="r_start_dates" type="text" class="form-control" placeholder="Date Range"  value="<?php echo $date_range; ?>">
          </div>
        </div>      
		<div class="col-md-4">        
          <div class="form-group form-md-line-input">
            <input name="r_start_price" id="r_start_price" type="number" min="0" class="form-control" placeholder="Starting Price Range" value="<?php echo $tax_rule->r_start_price; ?>" required />
            <label></label>
            <span class="help-block">Starting Price Range*</span> </div>
        </div>
		<div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="r_end_price" id="r_end_price" type="number" min="0" class="form-control" placeholder="Ending Price Range" onblur="check_price_range()" value="<?php echo $tax_rule->r_end_price; ?>" required />
            <label></label>
            <span class="help-block">Ending Price Range*</span>		  </div>
        </div>
		
		<div class="col-md-4">							<?php 						$ss_select = '';						$ds_select = '';						if($tax_rule->same_state == 1) {							$ss_select = 'selected';						} else if($tax_rule->same_state == 0) {							$ds_select = 'selected';						}					?>
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="gst" id="gst" required >
							<option value="" disabled selected>Select SGST/ IGST * </option>
							<option value="1" <?php echo $ss_select; ?>>SGST </option>
							<option value="0" <?php echo $ds_select; ?>>IGST </option>
						</select>
						<label></label>
						<span class="help-block">Select SGST/ IGST *</span>
					</div>
		</div>
		
		<div class="col-md-12">
          <div class="form-group form-md-line-input">
            <input name="r_desc" id="r_desc" type="text" class="form-control" placeholder="Rule Description" value="<?php echo $tax_rule->r_desc; ?>" required/>
            <label></label>
            <span class="help-block">Rule Description*</span> </div>
        </div>
	<!-- end	-->
		
		
		    			
		
		
	  <?php }
	  
	  ?>
	  
	 
		<div class="col-md-12">
		 <br></br>
	   <h4 style="color:#00564D;"><i class="fa fa-book" aria-hidden="true"></i> Tax Elements</h4>
       <div class="form-group form-md-line-input"> 
	   
	  <table class="table table-striped table-bordered table-hover table-scrollable" id="tax_items">
	  <thead>
	  <tr >
	  <th style="text-align:left;">Tax Element</th>
	  <th style="text-align:left;">Tax Mode</th>  
	  <th style="text-align:left;">Applied On</th>
	  <th style="text-align:left;">Calculated On</th>
	  <th style="text-align:left;">Value</th>
	  <th style="text-align:left;">Action</th>
	  <!--<th style="text-align:left;">Action</th>-->
	  </tr>
	  </thead>
	  <tbody>
	 
	  <tr><td colspan="6"></td></tr>
	     <tr>
	  <td >
        
            <select class="form-control input-sm" name="tax_type_5" id="tax_type_5" >
			<option value="" disabled selected>Select Tax Type</option>
				<?php $tax = $this->dashboard_model->get_distinct_tax_type();  // Getting the tax types  
					if($tax != false)
					{
						foreach($tax as $tx)
						{
						?>
						<option value="<?php echo $tx->tax_id; ?>"><?php echo $tx->tax_name; ?></option>
						<?php
						}			
					}
				?>
			</select>
            
		</td>
		<td class="form-group">
            <select class="form-control input-sm" name="tax_mode_5" id="tax_mode_5" >
				<option value="" disabled selected >Select mode</option>
				<option value="f" >Fixed</option>
				<option value="p" >Percentage (%)</option>
			</select>
		</td>
		
		<td class="form-group">
            <select class="form-control input-sm" name="applied_on_5" id="applied_on_5" >
			<option  value="" disabled selected>Applied on</option>
			<option value="b_a" >Booking Amount</option>
			<option value="p_a" >Pos Amount</option>
			<option value="f_p" disabled >Food Plan</option>         
			<option value="t_a" >Total Amount</option>
			</select>        
		</td>
		
		<td class="form-group">	
            <select class="form-control input-sm" name="calculated_on_5" id="calculated_on_5" >
			<option value="" disabled selected >Calculated on</option>
			<option value="n_r" >Net rate</option>
			<option value="n_p" >Net rate + Prev. tax</option>
			<option value="n_p_d" >Net rate + Prev. tax - Discount</option>
			</select> 
		</td>
		
		<div id="value_change">
			<td class="form-group ">
			   <input type="text" min="0" name="tax_value_5" class="form-control input-sm" placeholder="Value" id="tax_value_5"  onkeypress=" return onlyNos(event, this);"/ >
			</td>
		</div> 
	  <td class="form-group ">
			<a class="btn green btn-md" id="add_tax_rule" onclick="add_tax_item(<?php echo $rule_id;  ?>)"><i class="fa fa-plus"></i></a>
		</td>
		
        </tr>
	  	 
	  <?php $tax_components = $this->dashboard_model->get_all_tax_rule_component_by_rule_id($rule_id); 
	  $tax_rule_set = "";
	  if($tax_components != false)
	  {
		$c = 0;
		foreach($tax_components as $tc)
		{
			$c++;
	  ?>
	 
	  <tr><td colspan="6"></td></tr>
	  <tr id = "row_<?php echo $tc->id ;?>">
	  
	  <td class="form-group">
            <select class="form-control input-sm" name="tax_types_5[]" id="tax_types_5" disabled>
			<option value="" disabled selected>Select Tax Type</option>
			<?php $tax = $this->dashboard_model->get_all_tax_type();    
				if($tax != false)
				{
					foreach($tax as $tx)
					{
				?>	
						<option value="<?php echo $tx->tax_id; ?>" <?php if($tc->tax_type == $tx->tax_id) echo "selected";?>><?php echo $tx->tax_name; ?></option>
				<?php
					}			
				}?>
			</select>
		</td>
		<input type="hidden" name="rule_comp_id[]" class="rule_comp_id[]" value="<?php echo $tc->id; ?>">
		<td class="form-group">
		
            <select  class="form-control input-sm" name="tax_modes_5[]" id="tax_modes_5[]" disabled>
			<option value="" disabled selected >Select mode</option>
			<option value="f" <?php if($tc->tax_mode == "f") echo "selected";?>>Fixed</option>
			<option value="p" <?php if($tc->tax_mode == "p") echo "selected";?>>Percentage (%)</option>
			</select>
            
		</td>
		<td class="form-group">
            <select  class="form-control input-sm" name="applied_ons_5[]" id="applied_ons_5[]" disabled>
			<option  value="" disabled selected>Applied on</option>
			<option value="b_a" <?php if($tc->applied_on == "b_a") echo "selected";?> >Booking Amount</option>
			<option value="p_a" <?php if($tc->applied_on == "p_a") echo "selected";?> >Pos Amount</option>
			<option value="f_p" <?php if($tc->applied_on == "f_p") echo "selected";?> >Food Plan</option>         
			<option value="t_a" <?php if($tc->applied_on == "t_a") echo "selected";?> >Total Amount</option>
			</select> 
		</td>
		<td class="form-group">
		
            <select  class="form-control input-sm" name="calculated_ons_5[]" id="calculated_ons_5[]" disabled>
			<option value="" disabled selected >Calculated on</option>
			<option value="n_r" <?php if($tc->calculated_on == "n_r") echo "selected";?> >Net rate</option>
			<option value="n_p" <?php if($tc->calculated_on == "n_p") echo "selected";?> >Net rate + Prev. tax</option>
			<option value="n_p_d" <?php if($tc->calculated_on == "n_p_d") echo "selected";?> >Net rate + Prev. tax - Discount</option>
			</select>
           
		</td>
		
		<td class="form-group ">
		
           <input type="text" min="0"  name="tax_values_5[]" id="tax_values_5[]" class="form-control input-sm"  placeholder="Value" value="<?php echo $tc->tax_value; ?>" readonly/>
            
		</td>
		<!-- comment remove --->
		<td class="form-group">
		<a class="btn red btn-md" id="del_tax_rule" onclick="del_tax_item(<?php echo $tc->id; ?>,<?php echo $tc->tax_type; ?>);"><i class="fa fa-trash"></i></a>
		</td>
        </tr>
		
		
		
		
	  <?php
		$tax_rule_set = $tax_rule_set .",".$tc->tax_type;
	  } } ?>
	
		</tbody>
		</table>
		<div class="col-md-12">
       <input type="text" id="taxes" name="taxes" style="display:none;" value="<?php echo $tax_rule_set; ?>" />
      </div>
    </div>
	</div>
    </div></div>
    <div class="form-actions right">
      <button type="submit" class="btn blue" >Submit</button>
    </div>
    <?php form_close(); ?>
   
  </div>
</div>
<script>
var x = <?php echo $c;?>;

function add_tax_item(taxid)
{
	
	var tax_type = $("#tax_type_5").val();
	var tax_type_text = $("#tax_type_5 option:selected").text()
	
	var tax_mode =$("#tax_mode_5").val();
	var tax_mode_text = $("#tax_mode_5 option:selected").text()
	
	var tax_value = $("#tax_value_5").val();
	
	var calculated_on = $("#calculated_on_5").val();
	var calculated_on_text = $("#calculated_on_5 option:selected").text()
	
	var applied_on = $("#applied_on_5").val();
	var applied_on_text = $("#applied_on_5 option:selected").text();
	
	//alert("main taxid" +taxid);
	//alert(tax_type+"_"+tax_mode+"_"+tax_value+"_calculated:"+calculated_on+"_"+applied_on);
	
	//return false;
	var flag = 0;
		var it=$('#taxes').val();
		//alert(it);
		var res = it.split(',');
		
		var l = 0;
    for(l = 0; l < res.length ; l++)
		{
			//alert(res[l]);
			if (res[l] == tax_type)
			{
				
				flag++;
			}
		}
	if( flag == 0)
	{
		if($("#tax_type_5").val() != null &&
		$("#tax_mode_5").val() != null &&
		$("#tax_value_5").val() != "" &&
		$("#calculated_on_5").val() != null &&
		$("#applied_on_5").val() != null)
		{
			$.ajax({
				
				type:"POST",
				url:"<?php  echo base_url(); ?>dashboard/add_tax_rule_when_update",
				data:{tax_main_id:taxid,tax_type:tax_type,tax_mode:tax_mode,tax_val:tax_value,tax_calculated:calculated_on,tax_applied:applied_on},
				success:function(data){
					
					if(data >0){
						
						//alert(data)	;
						//----
						
						x++;
			
			$('#tax_items tr:last').after('<tr id="row_'+x+'" >'+
			'<td><select readonly name="tax_types_5[]" class="form-control input-sm" disabled><option value="'+tax_type+'" selected>'+tax_type_text+'</option></select></td>'+
			'<td><select readonly name="tax_modes_5[]" class="form-control input-sm" disabled><option value="'+tax_mode+'" selected  >'+tax_mode_text+'</option></select></td>'+'<td ><select readonly name="applied_ons_5[]" class="form-control input-sm" disabled ><option value="'+applied_on+'" selected>'+applied_on_text+'</option></select></td>'+
			'<td ><select readonly name="calculated_ons_5[]" class="form-control input-sm" disabled ><option value="'+calculated_on+'" selected>'+calculated_on_text+'</option></select></td>'+
			'<td ><input name="tax_values_5[]" type="text" value="'+tax_value+'" class="form-control input-sm" readonly></td>'+
			'<td><a href="javascript:void(0);" class="btn red btn-md" onclick="removeRow('+x+','+tax_type+')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>'+
			
			'</tr>');	
			//alert(a);
			
			x=x+1;
			
			swal("Success", "Tax rule added", "success")
		
			$('#taxes').val($('#taxes').val()+','+tax_type);
			
			$('#tax_type_5').prop('selectedIndex',0);
			$('#tax_mode_5').prop('selectedIndex',0);
			$("#tax_value_5").val("");
			$('#calculated_on_5').prop('selectedIndex',0);
			$('#applied_on_5').prop('selectedIndex',0);
		
						
						
						//----
						
					}//end if 
					else
						{
							swal("Check Carefully", "Please Fill all the fields preoperly", "info");
						}
				}//end of success function
			});
			
		
		
	} else {
		toastr["error"]("Please Fill all the fields preoperly");
	}
	
	}
	else
	{
		//alert('helo');
		toastr["error"]("Dublicate Tax Element, This Tax type is already added");	
	}
}		
</script>        
<script>
function removeRow(a,tax_type)
{
	swal({   title: "Are you sure?",   text: "You will not be able to recover this record!",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){ 
			 text = $('#taxes').val();
			 text = text.replace(","+tax_type,"");
			 $('#taxes').val(text); 
			 $('#row_'+a+'').remove();  
			 swal("Deleted!", "The tax Rule has been deleted", "success"); });
	
}

</script>
<script>
function check_price_range()
{
	if(parseInt($('#r_start_price').val()) >= parseInt($('#r_end_price').val()))
	{
		$('#r_end_price').val("");
		swal("Alert!", "Starting Price cannot be greater or equal to ending price", "error");
		
	}
}
</script>
<script>
function del_tax_item(a,tax_type)
{
	alert(a);
	alert(tax_type);
	swal({   title: "Are you sure?",   text: "You will not be able to recover this record!",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
			x--;
			text = $('#taxes').val();
			 text = text.replace(","+tax_type,"");
			 $('#taxes').val(text); 
			 $.ajax({
				 
					type:"POST",
					url:"<?php  echo base_url(); ?>dashboard/delete_tax_rule_on_update",
					data:{tax_rule_id:a},
					success:function(data){
					
					if(data >0){
						
						alert(data)	;
						 $('#row_'+a+'').remove(); 
						//----
					}
					
					}
			 })
			 
			 $('#row_'+a+'').remove();  
			 swal("Deleted!", "The tax Rule has been deleted", "success"); });
	
}
</script>
<script>

$(document).ready(function(){
	
	
	var cat='';
	var date='';
	var	start_price='';
	var	end_price='';
	$('#r_start_price').focus();
	$("#r_start_dates").on('click',function(){
		
		$('#r_start_price').val('');
		
	});
	
	
  
	$("#r_start_price, #r_end_price").bind("change blur",function() {
 
		cat = $('#tax_cat').val();
		date  = $('#r_start_dates').val();
		start_price = $('#r_start_price').val();
		//alert(date);
		//alert(start_price);
		end_price = $('#r_end_price').val();
		if(parseInt(start_price) >= parseInt(end_price)){
			
			$('#r_end_price').val("");
			swal("Alert!", "Starting Price cannot be greater or equal to ending price", "error");
		}
		else{
			//alert(date);
				$.ajax({
				
				type:"POST",
				url:"<?php echo base_url();?>dashboard/tax_rule_ajax",
				data:{category:cat,start_date:date,start_price:start_price,end_price:end_price},
				success:function(data){
				//	alert(data);
					if(data=='EXSISTS'){
					swal("Alert!",  "Already rule is defined under this category", "error");
					$('#r_start_dates').val('');
					$('#r_start_price').val("");
					$('#r_end_price').val("");
					$('#r_start_dates').focus();
					}
					$('#responsive').modal('show');
					
				},
			});
			
		}
	
});

		
	
});
</script>
