<!-- BEGIN PAGE CONTENT-->
<?php if($this->session->flashdata('err_msg')):?>

<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <span class="caption-subject bold uppercase"><i class="fa fa-plus-square"></i>&nbsp; Edit Transaction</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
                 if(isset($value) && $value){ 
		
			//foreach($data as $value) {
			$id1=$value->t_id; 
			
			$form = array(
                      'class'       => '',
                      'id'        => 'form',
                      'method'      => 'post',
      
                  ); 
                    echo form_open_multipart('Dashboard/edit_transaction/'.$id1,$form);
					//print_r($value);
                  ?>
    <div class="form-body">
      <div class="row">
        <input type="hidden" value="<?php echo $value->t_id ; ?>" name="hid">
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="transaction_type" name="transaction_type"  value="<?php echo $value->transaction_type ;?>" placeholder="Transaction Type">
            <label></label>
            <span class="help-block">Transaction Type</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="t_booking_id" placeholder="Booking Id" name="t_booking_id"  value="<?php echo $value->t_booking_id;?>">
            <label></label>
            <span class="help-block">Booking Id</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="t_group_id" name="t_group_id"   value="<?php echo $value->t_group_id ;?>" placeholder="Group Id">
            <label></label>
            <span class="help-block">Group Id</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="purchase_id" name="purchase_id"  value="<?php echo $value->purchase_id ;?>" placeholder="Purchase Id">
            <label></label>
            <span class="help-block">Purchase Id</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="payment_id" name="payment_id"  value="<?php echo $value->payment_id ;?>" placeholder="Payment Id">
            <label></label>
            <span class="help-block">Payment Id</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="details_id" name="details_id"  value="<?php echo $value->details_id ;?>" placeholder="Detaild Id">
            <label></label>
            <span class="help-block">Detaild Id</span> </div>
        </div>
        <!--<div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="t_status" name="t_status"  value="<?php echo $value->t_status ;?>" placeholder="Status">
            <label></label>
            <span class="help-block">Status</span> </div>
        </div>-->
		<?php 
			$dne = '';
			$pen = '';
			$can = '';
			if($value->t_status == 'Done')
				$dne = 'selected';
			else if($value->t_status == 'Pending')
				$pen = 'selected';
			else if($value->t_status == 'Cancel')
				$can = 'selected';
		?>
		<div class="col-md-4">	
			<div class="form-group form-md-line-input">
                 <select name="t_status" class="form-control bs-select" placeholder="Payment Status" id="t_status" required="required">
                    <option value="Done" <?php echo $dne;?>>Payment Recieved</option>
                    <option value="Pending" <?php echo $pen;?>>Payment Processing</option>
                    <option value="Cancel" <?php echo $can;?>>Transaction Declined</option>
                 </select> 
                 <label></label>					 
                 <span class="help-block">Payment Status</span>
			</div>
        </div>
        <?php
		$id=$value->transaction_from_id;
		$t_from=$this->unit_class_model->transaction_from_entity($id);
		if(isset($t_from->entity_name)){
			$name=$t_from->entity_name;
		}
		else{
			$name='';
		}
		
		$id1=$value->transaction_to_id;
		$t_from1=$this->unit_class_model->transaction_from_entity($id1);
		if(isset($t_from1->entity_name)){
		$name1=$t_from1->entity_name;
		}
		else{
			$name1='';
		}
		?>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="transaction_from_id" name="transaction_from_id"  value="<?php echo $name ;?>" placeholder="Transaction From Id" readonly>
            <label></label>
            <span class="help-block">Transaction from Id</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="transaction_to_id" name="transaction_to_id"  value="<?php echo $name1 ;?>" placeholder="Transaction to Id" readonly>
            <label></label>
            <span class="help-block">Transaction to Id</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="transaction_releted_t_id" name="transaction_releted_t_id"  value="<?php echo $value->transaction_releted_t_id ;?>" placeholder="Related Id">
            <label></label>
            <span class="help-block">Related Id</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="t_hotel_id" name="t_hotel_id"  value="<?php echo $value->hotel_id ;?>" placeholder="Hotel id">
            <label></label>
            <span class="help-block">Hotel id</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="transactions_detail_notes" name="transactions_detail_notes"  value="<?php echo $value->transactions_detail_notes ;?>" placeholder="Detail Notes">
            <label></label>
            <span class="help-block">Detail Notes</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" value="<?php echo $value->p_center ;?>"   id="p_center" name="p_center" required="required" >
              <?php $pc=$this->dashboard_model->all_pc(); 
						//print_r($pc);
						if(isset($pc) && $pc){
							foreach($pc as $profit){	
						?>
              <option value="<?php echo $profit->profit_center_location; ?>" <?php if($profit->profit_center_location==$value->p_center){ echo "selected";}?> ><?php echo $profit->profit_center_location; ?></option>
              <?php }} ?>
            </select>
            <label></label>
            <span class="help-block">Profit Center</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="details_type" name="details_type"  value="<?php echo $value->details_type ;?>" placeholder="Details Type">
            <label></label>
            <span class="help-block">Details Type</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" class="form-control" id="t_amount" name="t_amount"  value="<?php echo $value->t_amount ;?>" placeholder="Amount">
            <label></label>
            <span class="help-block">Amount</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select"  id="mop" name="t_payment_mode" required="required" onchange="modesofpayments();">
              <option value="" selected="selected" disabled="disabled">Mode Of Payment</option>
              <?php $mop = $this->dashboard_model->get_payment_mode_list();
			  if($mop != false)
			  {
				  foreach($mop as $mp)
				  {
				?>
              <option value="<?php echo $mp->p_mode_name; ?>" <?php if($value->t_payment_mode == $mp->p_mode_name) echo "selected"; ?>><?php echo $mp->p_mode_des; ?></option>
              <?php }
			  } ?>
            </select>
            <label> </label>
            <input type="hidden" name="mode" id="mode" value="<?php echo $value->t_payment_mode; ?>" >
            <span class="help-block">Modes of Payments *</span> </div>
        </div>
        <div id="check" style="display:none">
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control focus" id="check1" name="checkno" value="<?php if(isset($value->checkno) && $value->checkno)echo $value->checkno; ?>" placeholder="Check No" onkeypress=" return onlyNos(event, this);">
              <label> </label>
              <span class="help-block">Check No</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control focus" id="check2" name="check_bank_name" value="<?php if(isset($value->t_bank_name) && $value->t_bank_name)echo $value->t_bank_name; ?>" placeholder="Bank Name">
              <label> </label>
              <span class="help-block">Bank Name</span> </div>
          </div>
        </div>
        <div id="draft" style='display:none'>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control focus" id="draft1" name="draft_no" value="<?php if(isset($value->draft_no) && $value->draft_no)echo $value->draft_no; ?>" placeholder="Draft No">
              <label> </label>
              <span class="help-block">Draft No</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control focus" id="draft2" name="draft_bank_name" value="<?php if(isset($value->t_bank_name) && $value->t_bank_name)echo $value->t_bank_name; ?>"  placeholder="Bank Name">
              <label> </label>
              <span class="help-block">Bank Name</span> </div>
          </div>
        </div>
        <div id="fundtransfer" style='display:none'>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control focus" id="fund1" name="ft_bank_name" value="<?php if(isset($value->ft_bank_name) && $value->ft_bank_name)echo $value->ft_bank_name; ?>" placeholder="Bank Name">
              <label> </label>
              <span class="help-block">Bank Name</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control focus" id="fund2" name="ft_account_no" value="<?php if(isset($value->ft_account_no) && $value->ft_account_no)echo $value->ft_account_no; ?>" placeholder="Account No">
              <label> </label>
              <span class="help-block">Account No</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control focus" id="fund3" name="ft_ifsc_code"value="<?php if(isset($value->ft_ifsc_code) && $value->ft_ifsc_code)echo $value->ft_ifsc_code; ?>" placeholder="IFSC code">
              <label> </label>
              <span class="help-block">IFSC code</span> </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn blue"> Submit </button>
    </div>
    <?php  form_close();  ?>
  </div>
</div>

<!-- END CONTENT --> 
<script>
 var gv;
 function condition_open(){
	 
	 var gv=$("#type1").val();
 
 if(gv=='1'){
	 document.getElementById('AMC').style.display='none';
 }
 else{
	  document.getElementById('AMC').style.display='block';
 }
 }
 
 
function check(){
  var a=$("#charge").val();
  var b=$("#tax").val();
  
  if(isNaN(a)){
	  alert("enter number");
   	$("#charge").val("");
  }
 if(isNaN(b)){
	  alert("enter number");
   	$("#tax").val("");
  }
}
</script> 
<script type="text/javascript">
	 
	
	function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	   //alert(y);
	    
	   if(y=="check"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fund"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }
   
   
$(document).ready(function() {
	var hVal = $("#mode").val();
	//alert(hVal);
	if(hVal == 'check' || hVal == 'Check'){
		$("#check").css("display", "block");
		
	}
	
	if(hVal == 'draft' || hVal == 'Draft'){
		$("#draft").css("display", "block");
		
	}
	
	
	if(hVal == 'fund' || hVal == 'fund'){
		$("#fundtransfer").css("display", "block");
		$("#fund1").css("display", "block");
		$("#fund2").css("display", "block");
		$("#fund3").css("display", "block");
		
		
	}
	
	
	
});
	
</script> 