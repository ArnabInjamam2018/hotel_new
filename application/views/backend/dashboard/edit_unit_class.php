<!-- BEGIN PAGE CONTENT-->

<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <span class="caption-subject bold uppercase"><i class="glyphicon glyphicon-bed"></i>&nbsp; Update Unit Type</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
  if(isset($input)){
foreach($input as $value) {
	$id=$value->hotel_unit_class_id;
                                $form = array(
                                    'class' 			=> '',
                                    'id'				=> 'form',
                                    'method'			=> 'post'
                                );
								//$id=$edit_id;
                                echo form_open_multipart('unit_class_controller/edit_unit_class/'.$id,$form);
								/*$unit_type= $UnitDetail->unit_type;
								$unit_name= $UnitDetail->unit_name;
								$unit_class= $UnitDetail->unit_class;
								$unit_desc= $UnitDetail->unit_desc;*/
                                ?>
    <div class="form-body">
      <div class="row">
      	<div class="col-md-6">
            <div class="form-group form-md-line-input">
                <input type="text" class="form-control focus" name="class_name" required="required" value="<?php echo $value->hotel_unit_class_name;?>" placeholder="Unit Class *">
                <label></label>
                <span class="help-block">Unit Class *</span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group form-md-line-input">
                <input type="text" class="form-control focus" name="class_desc"  value="<?php echo $value->hotel_unit_class_desc;?>">
                <input type="hidden" name="id" required="required" value="<?php echo $id;  ?>" placeholder="Description *">
                <label></label>
                <span class="help-block">Description *</span>
            </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
        <input type="submit" class="btn green" value="update">
    </div>
  <?php  form_close(); ?>
  <?php }}?>
</div>
</div>
