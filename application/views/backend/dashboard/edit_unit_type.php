<!-- BEGIN PAGE CONTENT-->

<div class="portlet box blue">
<div class="portlet-title">
  <div class="caption"> <span class="caption-subject bold uppercase"><i class="glyphicon glyphicon-bed"></i>&nbsp; Update Unit Type</span> </div>
</div>
<div class="portlet-body form">
  <?php

                                $form = array(
                                    'class' 			=> '',
                                    'id'				=> 'form',
                                    'method'			=> 'post'
                                );
								$id=$edit_id;
                                echo form_open_multipart('dashboard/edit_unit_type/'.$id,$form);
								$unit_type= $UnitDetail->unit_type;
								$unit_name= $UnitDetail->unit_name;
								$unit_class= $UnitDetail->unit_class;
								$unit_desc= $UnitDetail->unit_desc;
                                ?>
  <div class="form-body">
    <div class="row">
    	<div class="col-md-4">
            <div class="form-group form-md-line-input">
                <select name="unit_type" id="unit_type" class="form-control focus" required="required">
                  <option value="" disabled="" selected="">Unit Type</option>
                  <?php 
                                    
                                
                      foreach($unitTypeName as $uName){ ?>
                  <option value="<?php echo $uName->name;?>" <?php if ($uName->name==$unit_type) echo "selected=''";?>><?php echo $uName->name;?></option>
                  <?php } ?>
                </select>
                <label></label>
                <span class="help-block">Unit Type *</span> 
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
                <input type="text" class="form-control focus" name="unit_name" id="unit_name" value="<?php echo $unit_name;?>" required="required" placeholder="Unit Name *">
                <label></label>
                <span class="help-block">Unit Name *</span> 
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
                <select name="unit_class" id="unit_class" class="form-control focus" required="required">
                  <option value="" disabled="">Unit Class</option>
                  <option value="Premium" <?php if ($unit_class=="Premium") echo "selected=''";?>>Premium</option>
                  <option value="Delux" <?php if ($unit_class=="Delux") echo "selected=''";?>>Delux</option>
                  <option value="Super" <?php if ($unit_class=="Super") echo "selected=''";?>>Super</option>
                  <option value="Regular" <?php if ($unit_class=="Regular") echo "selected=''";?>>Regular</option>
                </select>
                <label></label>
                <span class="help-block">Unit Class *</span> 
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group form-md-line-input">
                <textarea class="form-control focus" row="3" name="desc" id="desc" placeholder="Description"><?php echo $unit_desc;?></textarea>
                <label></label>
                <span class="help-block">Description</span>
            </div>
        </div>
    </div>
  </div>
  <div class="form-actions right">
    <button type="submit" class="btn green">Save</button>
  </div>
  <?php form_close(); ?>
</div>
</div>
