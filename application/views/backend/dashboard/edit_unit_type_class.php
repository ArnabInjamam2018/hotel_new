<!-- BEGIN PAGE CONTENT-->

<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <span class="caption-subject bold uppercase"><i class="glyphicon glyphicon-bed"></i>&nbsp; Update Unit Type</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                                $form = array(
                                    'class' 			=> '',
                                    'id'				=> 'form',
                                    'method'			=> 'post'
                                );
								//$id=$edit_id;
                                echo form_open_multipart('unit_class_controller/all_unit_type_category_controller',$form);
								/*$unit_type= $UnitDetail->unit_type;
								$unit_name= $UnitDetail->unit_name;
								$unit_class= $UnitDetail->unit_class;
								$unit_desc= $UnitDetail->unit_desc;*/
                                ?>
    <?php

			foreach($input as $value) {
				# code...?>
    <div class="form-body">
      <div class="row">
      	<div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control focus" name="name" required="required" value="<?php echo $value->name;?>" placeholder="Unit Class *">
              <label></label>
              <span class="help-block">Unit Class *</span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control focus" name="cat_desc"  value="<?php echo $value->cat_desc;?>" placeholder="Cat Desc *">
              <label></label>
              <span class="help-block">Cat Desc *</span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control focus" name="date_added" disabled required="required" value="<?php echo $value->date_added;?>" placeholder="Date Added *">
              <label></label>
              <span class="help-block">Date Added *</span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control focus" name="add_admin" required="required" value="<?php echo $value->add_admin;?>">
              <input type="hidden" name="id" required="required" value="<?php echo $value->id;  ?>">
              <label></label>
              <span class="help-block">Add Admin *</span>
            </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <input type="submit" class="btn green" value="update">
    </div>
    <?php }?>
    <?php  form_close(); ?>
  </div>
</div>
