<!-- 17.11.2015-->
<script>
function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-industry"></i> <span class="caption-subject bold uppercase"> Add Vendor Details</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        
							 if(isset($data) && $data){
								 
								 //print_r($data);
							//	 die();
							foreach($data as $val){
								$id=$val->hotel_vendor_id;
								$is_con = $val->hotel_vendor_is_contracted;

                        echo form_open_multipart('dashboard/update_vendor',$form);

                        ?>
    <div class="form-body">
      <div class="row">
        <div class="col-md-12">
          <h3 class="form-heading">General Details</h3>
          <input type="hidden" name="vendor_id" value="<?php echo $val->hotel_vendor_id; ?>">
          <!--<hr style="height:0.5px; border:none; color:#AAAAAA; background-color:#AAAAAA;">--> 
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control" id="vendor_name" name="vendor_name"  required="required" placeholder="Vendor Name *" value="<?php echo $val->hotel_vendor_name ?>">
            <label></label>
            <span class="help-block">Vendor Name *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control" id="vendor_legal_name" name="vendor_legal_name"  required="required" placeholder="Vendor Name *" value="<?php echo $val->hotel_vendor_name ?>">
            <label></label>
            <span class="help-block">Vendor Legal Name *</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" name="reg_type" id="reg_type" required >
              <!-- <option value="" disabled="disabled">Select incorporation types</option>-->
              
              <option value="propitor" <?php if($val->hotel_vendor_regtype == 'propitor') echo "selected";?>>propitor</option>
              <option value="Partnership" <?php if($val->hotel_vendor_regtype == 'Partnership') echo "selected";?>>Partnership</option>
              <option value="LLC" <?php if($val->hotel_vendor_regtype == 'LLC') echo "selected";?>>LLC</option>
              <option value="Priivately Limited" <?php if($val->hotel_vendor_regtype == 'Priivately Limited') echo "selected";?>>Priivately Limited</option>
              <option value="Limited" <?php if($val->hotel_vendor_regtype == 'Limited') echo "selected";?>>Limited</option>
            </select>
            <label></label>
            <span class="help-block">Types of Reg</span> </div>
        </div>
        <?php 
		$industry=$this->unit_class_model->all_industry_type();
		?>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" name="industry_type" id="industry_type" required >
              <option value="0" >Select industry type</option>
              <?php

			  if(isset($industry) && $industry){
				  foreach($industry as $ind){			  
			  
			  ?>
              <option value="<?php echo $ind->it_name ?>" <?php if($ind->it_name == $val->hotel_vendor_industry) echo "selected";?> ><?php echo $ind->it_name ?></option>
              <?php }} ?>
            </select>
            <label></label>
            <span class="help-block">Industry type</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="field" name="field" required="off" placeholder="Field of work" value="<?php echo $val->hotel_vendor_field; ?>">
            <label></label>
            <span class="help-block">Field of work</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" name="rating" id="rating" required >
              <option value="1" <?php if($val->hotel_vendor_rating == '1') echo "selected";?> >1 Star</option>
              <option value="2" <?php if($val->hotel_vendor_rating == '2') echo "selected";?>>2 Star</option>
              <option value="3" <?php if($val->hotel_vendor_rating == '3') echo "selected";?> >3 Star</option>
              <option value="4" <?php if($val->hotel_vendor_rating == '4') echo "selected";?>>4 Star</option>
              <option value="5" <?php if($val->hotel_vendor_rating == '5') echo "selected";?>>5 Star</option>
            </select>
            <label></label>
            <span class="help-block">Star Rating</span> </div>
        </div>
        <div class="col-md-3 form-horizontal">
          <div class="form-group form-md-line-input" style="padding-top:20px;">
            <label class="col-md-6 control-label" for="form_control_1">Is Child Supplier *</label>
            <div class="col-md-6">
              <div class="md-radio-inline">
                <div class="md-radio">
                  <input type="radio" id="radio53" name="child_supp" class="md-radiobtn" value="yes">
                  <label for="radio53"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
                </div>
                <div class="md-radio">
                  <input type="radio" id="radio54" name="child_supp" class="md-radiobtn" value="no" checked="checked">
                  <label for="radio54"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3" id="parent">
          <div class="form-group form-md-line-input parent">
            <select class="form-control bs-select" name="parent_supp" id="parent_supp1" required >
              <option value="0" >Select Parent Supplier</option>
              <?php
				$vendor = $this->dashboard_model->all_vendors();	
			  if(isset($vendor) && $vendor){
				  foreach($vendor as $vnd){			  
			  
			  ?>
              <option value="<?php echo $vnd->hotel_vendor_id ?>" ><?php echo $vnd->hotel_vendor_name ?></option>
              <?php }} ?>
            </select>
            <label></label>
            <span class="help-block">Select Parent Supplier</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select"  name="is_contract" id="is_contract" required >
              <option value="" disabled="disabled">Is Contracted</option>
              <option value="y" <?php if($is_con == 'y') echo "selected";?>>Yes</option>
              <option value="n" <?php if($is_con == 'n') echo "selected";?>>No</option>
            </select>
            <label></label>
            <span class="help-block">Is Contracted *</span> </div>
        </div>
        <div class="col-md-12" style="padding-top:25px;">
          <h3 class="form-heading">Contact Details</h3>
          <!-- <hr style="height:0.5px; border:none; color:#AAAAAA; background-color:#AAAAAA;"> --> 
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control" id="address" name="address" placeholder="Address*" value="<?php echo $val->hotel_vendor_address; ?>">
            <label></label>
            <span class="help-block">Address</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control" id="pin_code" onblur="fetch_all_address()" name="pin_code" onkeypress=" return onlyNos(event, this);" placeholder="pin code" value="<?php echo $val->hotel_vendor_pin; ?>">
            <label></label>
            <span class="help-block">pin code</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="country" name="country" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Country" value="<?php echo $val->hotel_vendor_country; ?>">
            <label></label>
            <span class="help-block">Country</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control  " id="state" name="state" placeholder="state" value="<?php echo $val->hotel_vendor_state; ?>">
            <label></label>
            <span class="help-block">state</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control" id="city" name="city" placeholder="City" value="<?php echo $val->hotel_vendor_city; ?>">
            <label></label>
            <span class="help-block">City</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="contact_person" name="contact_person" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Contact Person" value="<?php echo $val->hotel_vendor_contact_person; ?>">
            <label></label>
            <span class="help-block">Contact Person</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control  " id="contact_no" onkeypress=" return onlyNos(event, this);" name="contact_no" placeholder="Contract No" value="<?php echo $val->hotel_vendor_contact_no; ?>">
            <label></label>
            <span class="help-block">Contract No</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="contact_email" name="contact_email" required="required" placeholder="Contact Email" value="<?php echo $val->hotel_vendor_email; ?>">
            <label></label>
            <span class="help-block">Contact Email</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="director_name" name="director_name"  required="required" placeholder="Director Name" value="<?php echo $val->hotel_vendor_owner_name; ?>">
            <label></label>
            <span class="help-block">Director Name</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" onkeypress=" return onlyNos(event, this);" class="form-control  " id="manager_no" name="manager_no" placeholder="Manager Contact no" value="<?php echo $val->hotel_vendor_owner_no; ?>">
            <label></label>
            <span class="help-block">Manager Contact no</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control  " id="corp_manager_email" name="corp_manager_email" placeholder="Manager Email" value="<?php echo $val->hotel_vendor_owner_email; ?>">
            <label></label>
            <span class="help-block">Manager Email</span> </div>
        </div>
        <div class="col-md-12" style="padding-top:25px;">
          <h3 class="form-heading">Financial Info</h3>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control  " id="profit_center" name="profit_center" placeholder="Profit Center" value="<?php echo $val->hotel_vendor_profit_center; ?>">
            <label></label>
            <span class="help-block">Profit Center</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="bank_name" name="bank_name"  required="required" placeholder="Bank Name" value="<?php echo $val->hotel_vendor_bank; ?>">
            <label></label>
            <span class="help-block">Bank Name</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input  autocomplete="off" type="text" class="form-control" id="bank_account" name="bank_account" onkeypress=" return onlyNos(event, this);"  placeholder="Bank Acc No" value="<?php echo $val->hotel_vendor_account; ?>">
            <label></label>
            <span class="help-block">Bank Acc No</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="bank_ifsc" name="bank_ifsc"   required="required" placeholder="IFSC Code" value="<?php echo $val->hotel_vendor_ifsc; ?>">
            <label></label>
            <span class="help-block">IFSC Code</span> </div>
        </div>
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" name="tax_plan" id="tax_plan" required >
              <option value="0" >Select Tax Plan</option>
            </select>
            <label></label>
            <span class="help-block">Select Tax Plan</span> </div>
        </div>
        <div class="col-md-12">
			<h3 class="form-heading">Upload Section</h3>
			
			<!--<hr style="height:0.5px; border:none; color:#AAAAAA; background-color:#AAAAAA;">-->
		</div>
       <div class="col-md-3">
        	<div class="form-group form-md-line-input uploadss">
             	<label>Upload Image</label>
              <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>upload/vendor_detail/<?php if( $val->hotel_vendor_image== '') { echo "no_images.png"; } else { echo $val->hotel_vendor_image; }?>" alt=""/> </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('vendor_image');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
              </div>
          	</div>
        </div>
        <div class="col-md-3" id="contract_document">
          <div class="form-group uploadss">
            <label>Upload File</label>
            <div class="fileinput fileinput-new" data-provides="fileinput">
              <div class="input-group input-small"> 
                <!--<input type="file" name="pdf" />
                      <span class="fileinput-new"> Select Vendor file </span> -->
                <div class="form-control uneditable-input input-fixed" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                <input type="file" name="pdf" />
                </span> <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
    <?php }} ?>
    <div class="form-actions right">
      <button type="submit" class="btn submit" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <!--<button  type="reset" class="btn default">Reset</button>-->
    </div>
    <input type="hidden" name="hid">
    <?php form_close(); ?>
    <!-- END CONTENT --> 
    
  </div>
</div>
<script>
var flag=0,flag1=0;
function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});

function modesofpayments()
   {
	   var y= document.getElementById("mop").value;
	   //alert(y);
	    
	   if(y=="check"){
		   document.getElementById("check").style.display="block";
	   }else{
		   document.getElementById("check").style.display="none";
	   }
	   if(y=="draft"){
		   document.getElementById("draft").style.display="block";
	   }else{
		   document.getElementById("draft").style.display="none";
	   }
	   if(y=="fundtransfer"){
		   document.getElementById("fundtransfer").style.display="block";
	   }else{
		   document.getElementById("fundtransfer").style.display="none";
	   }
   }
   var ff=flag;
  var check=$('#contract_start_date'+ff).val();
  var a=$('#contract_end_date').val();
  //if(check<=a){
  
  
   $("#additems").click(function(){
	   $a=$('#contract_start_date').val();
	   $b=$('#contract_end_date').val();
	   $c=$('#contract_discount_start').val();
	   $d=$('#contract_discount_end').val();
	  $e=$('#default_rule').val();
	  //alert($e);
	  
	   if($a!='' && $b!='' && $c!='' && $d!='' && $e!=''){
	var x=0;
	var contract_start_date = $('#contract_start_date').val();
	var contract_end_date =$('#contract_end_date').val();
	var contract_discount_start = $('#contract_discount_start').val();
	var default_rule =$('#default_rule').val();
	
	//var contract_discount_end = $('#contract_discount_end').val();
	
	
	//$('#ids').val( it+','+ i_name) ;
	$('#items tr:first').after('<tr id="row_'+flag+'"><td>'+
	'<input name="contract_start_date[]" id="contract_start_date'+flag+'" type="text" value="'+contract_start_date+'" class="form-control input-sm" readonly></td><td><input name="contract_end_date[]" id="contract_end_date'+flag+'" type="text" value="'+contract_end_date+'" class="form-control input-sm" readonly></td><td class="hidden-480"><input name="contract_discount_start[]" id="contract_discount_start'+flag+'" type="text" value="'+contract_discount_start+'" class="form-control input-sm" readonly></td><td class="hidden-480"><input name="default_rule[]" id="contract_discount_start'+flag+'" type="text" value="'+default_rule+'" class="form-control input-sm" readonly></td><td><a  class="btn red btn-sm" onclick="removeRow('+flag+')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>');	
	
	
	
	x=x+1;
	flag1=flag;
	//alert(flag);
	flag++;
	   $('#contract_start_date').val('');
	   $('#contract_end_date').val('');
	   $('#contract_discount_start').val('');
	   //$('#contract_discount_end').val('');
	   }
	   else{
		   alert("Enter data");
	   }
		});
		
function check_date(){
	
	var a=$('#contract_start_date').val();
	 a=new Date(a);
	   var b=$('#contract_end_date').val();
	   b=new Date(b);
	   if(a>=b && b!='' ){
		   alert('End Date sould getter tan Start Date ');
		   $('#contract_end_date').val('');
	   }
	  
	  var c=$('#contract_end_date'+(flag-1)).val();
	 // alert(c);
	    c=new Date(c);
		if(c>a){
			
			alert('Please enter valid date');
			$('#contract_start_date').val('');
			$('#contract_end_date').val('');
		}
		
	  
	   
}

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pin_code').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("country").focus();
			$('#country').val(data.country);
			document.getElementById("state").focus();
			$('#state').val(data.state);
			document.getElementById("city").focus();
			$('#city').val(data.city);
		}

	});
}
	   
	   

</script> 
<script>function removeRow(a){
		$('#row_'+a).remove();
		
	}</script> 
<script>
	
	$(document).ready(function(){
		
		$('#parent').hide();
		
		$("input[name=child_supp]:radio").change(function () {
			
			var child_supplier = $(this).val();
	//		alert(child_supplier);
			if(child_supplier == 'yes'){
				
						//alert(child_supplier);
						$('#parent').show();
						//$("#parent_supp1").empty();
//$("#parent_supp1").append('<option value="Not">No</option>');

				
			}
			if(child_supplier == 'no'){
				
						$('#parent').hide();
				
			}
			
		});
		
	})
	
	</script> 
<script>
	
	$(document).ready(function(){
		
	var is_con = $('#is_contract').val();
	if(is_con == 'y'){
		
		$('#contract_document').show();
		
	}
	else{
		$('#contract_document').hide();
	}	
		
		$('#is_contract').on('change',function(){
			
			var is_con = $('#is_contract').val();
			//alert(is_con);
			if(is_con == 'y'){
				
				$('#contract_document').show();
			}
			else{
				
				$('#contract_document').hide();
			}
			
		});
		
	});
	
	</script>