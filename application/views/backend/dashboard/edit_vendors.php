<!-- BEGIN PAGE CONTENT-->
<script>
function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
<!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<!-- 17.11.2015-->
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green"> <i class="icon-pin font-green"></i> <span class="caption-subject bold uppercase"> Edit Vendor</span> </div>
      </div>
      <div class="portlet-body form">
        <?php

                            $form = array(
                                'class' 			=> 'form-body',
                                'id'				=> 'form',
                                'method'			=> 'post',								
                            );
							 
							 
							 if(isset($data) && $data){
							foreach($data as $val){
								$id=$val->hotel_vendor_id;
							

                            echo form_open_multipart('dashboard/edit_vendor/'.$id,$form);

                            ?>
        <div class="form-body">           
          <div class="row">
		    <input type="hidden" name="hid" value="<?php echo $id; ?>">
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
             <!-- <input type="hidden" name="dgset_id" >--->
              <input  autocomplete="off" type="text" class="form-control focus" id="vendor_name" name="vendor_name" value="<?php echo $val->hotel_vendor_name;?>" required="required">
              <label>Vendor Name <span class="required">*</span></label>
              <span class="help-block">Enter Vendor Name...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <input autocomplete="off" type="text" class="form-control focus" id="address" name="address" 	value="<?php echo $val->hotel_vendor_address;?>"  >
              <label>Address</label>
              <span class="help-block">Enter Vendor Address...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <input  autocomplete="off" type="text" class="form-control focus" id="contact_person" name="contact_person" value="<?php echo $val->hotel_vendor_contact_person;?>" >
              <label>Contact</label>
              <span class="help-block">Enter Contact Person...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
			<input  autocomplete="off" type="text" class="form-control focus" id="contact_no" name="contact_no" value="<?php echo $val->hotel_vendor_contact_no;?>"  onkeypress=" return onlyNos(event, this);" maxlength=10>
              <label>Contact No</label>
              <span class="help-block">Enter contact no...</span> </div>
			  
			  <div class="form-group form-md-line-input form-md-floating-label col-md-6">
			<input  autocomplete="off" type="text" class="form-control focus" id="industry" name="industry"value="<?php echo $val->hotel_vendor_industry;?>" >
              <label>Industry</label>
              <span class="help-block">Enter vendor Industry...</span> </div>

			
  <!--<div class="form-group form-md-line-input form-md-floating-label col-md-6">
			<input  autocomplete="off" type="text" class="form-control focus" id="profit_center" name="profit_center" value="<?php //echo $val->hotel_vendor_profit_center;?>" required= "required" />
              <label>Profit Center<span class="required">*</span></label>
              <span class="help-block">Enter vendor Profit center...</span> </div>-->
			  
			  
			  
			  <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <select class="form-control focus" id="profit_center" name="profit_center" required="required" >
				<?php
				
				$pro=$val->hotel_vendor_profit_center;
				$pc=$this->dashboard_model->all_pc();?>
						  <?php
						  $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
						  ?>
						  <option value="<?php echo $defPro;  ?>"selected><?php echo $pro; ?></option>
                           <?php $pc=$this->dashboard_model->all_pc1();
							foreach($pc as $prfit_center){
							?>
						  
						  <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
							<?php }?>
              </select>
              <label>Profit Center<span class="required">*</span></label>
              <span class="help-block">Enter vendor Profit center...</span> </div>
			  
			 <!-- <div class="form-group form-md-line-input form-md-floating-label col-md-6">
			<input  autocomplete="off" type="text" class="form-control focus" id="vendor_tax_per"  name="vendor_tax_per" value="<?php //echo $val->hotel_vendor_tax;?>" required= "required" />
              <label>Vendor Tax<span class="required">*</span></label>
              <span class="help-block">Enter vendor Tax Percentage...</span> </div>-->
			  
			  <div class="form-group form-md-line-input form-md-floating-label col-md-6">
			<input  autocomplete="off" type="text" class="form-control focus" id="vendor_tax_per" name="vendor_discount_per" value="<?php echo $val->hotel_vendor_discount;?>" required= "required" />
              <label>Vendor Discount<span class="required">*</span></label>
              <span class="help-block">Enter vendor Discount Percentage...</span> </div>
            
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <select class="form-control focus"  name="is_child" id="is_child"   onchange="get_parent()">
				<option value="n" selected>No</option>
				<option value="y" >Yes</option>
				
              </select>
              <label>Is Child Vendor </label>
              <span class="help-block">Select Yes/No...</span> </div>
          
          <div class="form-group form-md-line-input form-md-floating-label col-md-6" id="par_vendor_div">            
            <select class="form-control focus" name="par_vendor" id="par_vendor" >
              <option  value="" selected disabled>Select Parent vendor</option>
			  <?php
			  foreach($parent as $par)
			  {
				echo "<option value='".$par->hotel_vendor_id."'>".$par->hotel_vendor_name."</option> ";
			  }
			  ?>
            </select>
            <label>Select Parent Vendor</label>
          </div>
		  
		  <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <select class="form-control focus"  name="fcon_stat" required="required" >
				<option value="y" >Yes</option>
				<option value="n" selected>No</option>
              </select>
              <label>Is Contracted </label>
              <span class="help-block">Select Yes/No...</span> </div>
			  
			  
			  <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    <img src="<?php echo base_url();?>upload/vendor_detail/<?php if( $val->hotel_vendor_image== '') { echo "no_images.png"; } else { echo $val->hotel_vendor_image; }?>" alt=""/>
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                </div>
                <div>
                    <span class="btn default btn-file">
                    <span class="fileinput-new">
                    Select Vendor image </span>
                    <span class="fileinput-exists">
                    Change </span>
                    <?php echo form_upload('vendor_image');?>
                    </span>
                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                    Remove </a>
                </div>
				
		  
              </div>
			  
					
			   <div class="fileinput fileinput-new" data-provides="fileinput">
			  
			  <input type="file" name="pdf" />
			  
                <!--<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    <img src="<?php echo base_url();?>assets/dashboard/assets/global/img/no-img.png" alt=""/>
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                </div>--->
               <!--- <div>
                    <span class="btn default btn-file">
                    <span class="fileinput-new">
                    Select Vendor application(pdf only) </span>
                    <span class="fileinput-exists">
                    Change </span>
                    <?php echo form_upload('vendor_application');?>
                    </span>
                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                    Remove </a>
                </div>----->
              </div>
			  
			  <?php }} ?>
			  
			  
			  
			  
			  
			  
		
          
          
      <div class="form-actions right">
        <button type="submit" class="btn blue" >Submit</button>
        <!-- 18.11.2015  -- onclick="return check_mobile();" -->
        <button  type="reset" class="btn default">Reset</button>
      </div>    
    <?php form_close(); ?>
    </div>
    <!-- END CONTENT --> 
  </div>
</div>
</div>
<script>
function chk_if_num( a )
		{
			//alert($('#'+a+'').val());
				if(isNaN($('#'+a+'').val())==true)
				{
					//alert ("please enter a valid "+a);
					$('#'+a+'').val("");
					
				}			
		}
		
   function check_Cylinders(value){
   		
       if(value =="Cylinders"){

           document.getElementById("c_choice").style.display="block";
       }else{
           document.getElementById("c_choice").style.display="none";
       }
      	if(value =="Rotation"){

           document.getElementById("r_choice").style.display="block";
       }else{
           document.getElementById("r_choice").style.display="none";
       }
       if(value =="Design"){

           document.getElementById("d_choice").style.display="block";
       }else{
           document.getElementById("d_choice").style.display="none";
       }
   }
 $(document).on('blur', '#amc-d', function () {
	$('#amc-d').addClass('focus');
});  

function get_parent()
{
	//alert($("#is_child").val());
	if($("#is_child").val()=='y')
	{
	$("#par_vendor_div").show();
	//$("#par_vendor").attr('required');
	$("#par_vendor").prop('required',true);
	
	}
	else
	{
	$("#par_vendor_div").hide();
	//$("#par_vendor").removeAttr('required');.
	$("#par_vendor").prop('required',false);
	}
}
$( document ).ready(function() {
    $("#par_vendor_div").hide();
	//$("#par_vendor_div").removeAttr('required');
});

</script> 
