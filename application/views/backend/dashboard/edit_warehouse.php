<!-- BEGIN PAGE CONTENT-->

<div class="portlet box blue">
<div class="portlet-title">
  <div class="caption"> <span class="caption-subject bold uppercase"><i class="glyphicon glyphicon-bed"></i>&nbsp; Update Unit Type</span> </div>
</div>
<div class="portlet-body form">
	<?php
        if(isset($data)){								
        foreach($data as $uName){ 
        $form = array(
            'class' 			=> 'form-body',
            'id'				=> 'form',
            'method'			=> 'post'
        );
        $id=$uName->id;
        echo form_open_multipart('unit_class_controller/edit_warehouse/'.$id,$form);								
    ?>
    <div class="form-body">
  		<div class="row">
        <div class="col-md-12">
            <div class="form-group form-md-line-input">
                <input type="hidden" name="id"  value="<?php echo $uName->id;?>">
                <input type="text" class="form-control focus" name="name" id="name" value="<?php echo $uName->name;?>" required="required" placeholder="Name">
                <label></label>     
                <span class="help-block">Name *</span>     
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group form-md-line-input">
              <textarea class="form-control focus" row="3" name="desc" id="desc" placeholder="Description"><?php echo  $uName->description;?></textarea>
              <label></label>
              <span class="help-block">Description</span> 
            </div>
        </div>
     </div>
  </div>
  <div class="form-actions right"> 
    <button type="submit" class="btn green">Save</button>
  </div>
  <?php form_close(); } } ?>
</div>
