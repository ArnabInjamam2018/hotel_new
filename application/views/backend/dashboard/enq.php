<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}
</script>
 <!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Guest</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/add_guest',$form);

                        ?>
    
   
    <div class="form-body">
      <div class="row">
        <div class="col-md-4">
            <div class="form-group form-md-line-input" id="select">
              <select onchange="check_corporate(this.value)" class="form-control bs-select" id="form_control_2" name="g_type" required="required">
                <option value="" disabled="disabled" selected="selected">Guest Type</option>
                <option value="Family" >Family</option>
                <option value="Bachelor">Bachelor</option>
                <option value="Tourist">Tourist</option>
                <!--<option value="Sr_citizen">Sr Citizen</option>-->
                <option value="Corporate">Corporate</option>
                <option value="VIP">VIP</option>
                <option value="Government_official">Government Official</option>
                <option value="Military">Military </option>
                <option value="internal_staff">Internal Staff</option>
              </select>
              <label></label>
              <span class="help-block">Guest Type *</span> </div>
        </div>
        <div class="col-md-4" style="display: none;" id="c_name">
        <div class="form-group form-md-line-input" >
          <select class="form-control bs-select" id="c_name1"  name="c_name"  >
            <option value="" selected="selected" disabled="disabled">Select Company Name</option>
            <?php 
                            if (isset($corporate) && $corporate)
                            {
                                
                            foreach ($corporate as $value){ ?>
            <option value="<?php echo $value->hotel_corporate_id;?>"><?php echo $value->name ?></option>
            <?php }}?>
          </select>
          <label></label>
          <span class="help-block">Select Company Name</span> </div>
        </div>
        <div class="col-md-4 form-horizontal" id="military" style="display: none">
            <div class="form-group form-md-line-input">
              <label class="col-md-4 control-label"> Retired?</label>
              <div class="col-md-6">
                <div class="md-radio-inline" style="margin: 8px 0 1px;">
                  <div class="md-radio">
                    <input type="radio" id="radio51" class="md-check" name="retired" value="yes">
                    <label for="radio51"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
                  </div>
                  <div class="md-radio">
                    <input type="radio" id="radio50" class="md-check" name="retired" value="no">
                    <label for="radio50"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="col-md-4" style="display: none;" id="type">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="type" onkeypress=" return onlyLtrs(event, this);" placeholder="Type">
              <label></label>
              <span class="help-block">Type</span> </div>
        </div>
        <div class="col-md-4" style="display: none;" id="class">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="class" onkeypress=" return onlyLtrs(event, this);" placeholder="Class">
              <label></label>
              <span class="help-block">Class</span> </div>
        </div>
        <div class="col-md-4" style="display: none;" id="f_size">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control"  name="f_size"  maxlength="10" onkeypress=" return onlyNos(event, this);" placeholder="Family Size">
              <label></label>
              <span class="help-block">Family Size </span> </div>
        </div>
        <div class="col-md-4" style="display: none" id="rank">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="rank" onkeypress=" return onlyLtrs(event, this);" placeholder="Rank">
              <label></label>
              <span class="help-block">Enter Rank...</span> </div>
        </div>
        <div class="col-md-4" style="display: none" id="dep">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="dep" onkeypress=" return onlyLtrs(event, this);"  placeholder="Department">
              <label></label>
              <span class="help-block">Department</span> </div>
        </div>
        <div class="col-md-4" style="display: none" id="c_des">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="c_des" onkeypress=" return onlyLtrs(event, this);"  placeholder="Designation">
              <label></label>
              <span class="help-block">Designation</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="g_name" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Full Name *">
              <div class="highlight"></div>
              <label></label>
              <span class="help-block">Full Name *</span> </div>
        </div>
		<div class="col-md-4" id="corporateId" hidden>
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="corporateId"    value="" placeholder="Corporate ID">
              <label></label>
              <span class="help-block">Corporate ID </span> </div>
            </div>
			
			<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="qualificationName"   value="" placeholder="Highest Qualification">
              <label></label>
              <span class="help-block">Highest Qualification Name </span> </div>
            </div>
		<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select" id="highestQualification" name="highestQualification">
                <option value="" selected="selected" disabled="disabled">Select Highest Qualification </option>
                <option value="0">Bellow Secondary</option>
                <option value="1">Secondary</option>
                <option value="2">Higher Secondary</option>
                <option value="3">Graduate</option>
                <option value="4">Post Graduate</option>
                <option value="5">Doctorate</option>                
                <option value="9">No Info</option>
              </select>
              <span class="help-block">Highest Qualification </span> </div>
        </div> 
		
        <div class="col-md-4">
            <div class="form-group form-md-line-input add">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="g_address" required="required" placeholder="Address *">
              <label></label>
              <div class="highlight"></div>
              <span class="help-block">Address *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="pincode" name="g_place"  required="required" onblur="fetch_all_address()" onkeypress=" return onlyNos(event, this);" placeholder="Pincode *">
              <label></label>
              <div class="highlight"></div>
              <span class="help-block">Pincode *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="g_city" name="g_city" value=""  onkeypress=" return onlyLtrs(event, this);" required= "required" placeholder="City/ Town/ Village *"/>
              <label></label>
              <span class="help-block">City/ Town/ Village *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="g_state"  name="g_state"  value="" required ="required"/ placeholder="State Name *">
          <label></label>
          <span class="help-block">State Name *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="g_country" name="g_country" value=""  required="required" placeholder="Country *"/>
              <label></label>
              <span class="help-block">Country *</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="mobile" name="g_contact_no"   maxlength="30" onblur="onlyNum(this.value);" placeholder="( 0-9 + , / - ) ">
              <label></label>
              <span class="help-block">Mobile No </span> </div>
        </div>
	
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" onchange="check_email()" type="email" class="form-control" id="email" name="g_email"  placeholder="Email">
              <label></label>
              <span class="help-block">Email</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <select class="form-control bs-select"  name="g_gender"  >
            <option value="" selected="selected" disabled="disabled">Select Gender</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
            <option value="Others">Others</option>
          </select>
          <label></label>
          <span class="help-block">Select Gender </span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control  date-picker" id="dob" name="g_dob" placeholder="DOB">
              <label></label>
              <span class="help-block">DOB</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="g_occupation" placeholder="Occupation">
          <label></label>
          <span class="help-block">Occupation</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select onchange="is_married(this.value)" class="form-control bs-select" id="form_control_2" name="g_married" >
                <option value="" selected="selected" disabled="disabled">Marital Status</option>
                <option value="married">Married</option>
                <option value="divorced">Divorced</option>
                <option value="widowed">Widowed</option>
                <option value="unmarried">Unmarried</option>
              </select>
              <span class="help-block">Marital Status</span> </div>
        </div>
        <div class="col-md-4" id="g_anniv" style="display: none;">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control date-picker" id="ane" name="g_anniv" placeholder="Anniversary">
              <label></label>
              <span class="help-block">Anniversary</span> </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select" id="form_control_2" name="g_id_type" onclick="show_gID_num()">
                <option value="" selected="selected" disabled="disabled">Select ID Proof</option>
                <option value="1">Passport</option>
                <option value="2">PAN card</option>
                <option value="3">Voter card</option>
                <option value="4">Adhar card</option>
                <option value="5">Driving License</option>
                <option value="6">Company ID Card</option>
                <option value="7">Ration Card</option>
                <option value="8">Other Govt ID</option>
                <option value="9">Others</option>
              </select>
              <span class="help-block">Select ID Proof</span> </div>
        </div> 
		 <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off"  type="text" class="form-control" id="g_id_number" name="g_id_number" placeholder="Id Ref Number">
          <label></label>
          <span class="help-block">Id Ref Number</span> </div>
        </div>
		
		<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <?php 
					$discount_rule=$this->unit_class_model->discount_rules();					
				  ?>
				  <select class="form-control bs-select"   name="discount_rule[]" id="" required="required" multiple>
				
				 <?php foreach($discount_rule as $rule){ ?>
                  <option value="<?php echo $rule->id;?>"><?php echo $rule->rule_name?></option>
				 
                  <?php } ?>
                </select>
              <span class="help-block">Discount Rule</span> </div>
        </div>
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-4"> <!-- Image Upload -->
              <div class="form-group form-md-line-input uploadss">
              <label>Upload Photo</label>
              <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                <div> 
					<span class="btn default btn-file"> 
						<span class="fileinput-new"> Select image </span> 
						<span class="fileinput-exists"> Change </span> 
						<?php echo form_upload('image_photo');?> 
					</span> 
					<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> 
				</div>
              </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input uploadss">
              <label>Upload ID Proof</label>
              <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('image_idpf');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" id="btnSubmit" class="btn blue" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
  </div>
  <?php 
    echo form_close(); ?>
  <!-- END CONTENT --> 
</div>
<script>
function chk(){
	
	
	var c_name= document.getElementById("c_name1").value;
	var g_type= document.getElementById("form_control_2").value;
	if(g_type=="Corporate"){
		if(c_name==0){
			alert("select Company Name");
			return false;
		}
	}
	
	
	
	

}

   function check_corporate(value){
	 
	 if(value=="VIP"){
		
		 document.getElementById("type").style.display="block";
		 document.getElementById("class").style.display="block";
	}
	else{
		 document.getElementById("type").style.display="none";
		 document.getElementById("class").style.display="none";
	}
	 
	 
	   if(value=="Government_official"){
			
		 document.getElementById("dep").style.display="block";
		 document.getElementById("c_des").style.display="block";
	}
	else{
		
		 document.getElementById("c_des").style.display="none";
	}
	   
	    if(value=="Internal_staff"){
			
		 document.getElementById("dep").style.display="block";
		 document.getElementById("c_des").style.display="block";
	}
	
	   
	   
	   
		if(value=="Military"){
			
		 document.getElementById("military").style.display="block";
		 document.getElementById("rank").style.display="block";
		  
	}
	else{
		 document.getElementById("military").style.display="none";
		 document.getElementById("rank").style.display="none";
	}
	
	
	if(value=="Family"){
			
		 document.getElementById("f_size").style.display="block";
	}
	else{
		 document.getElementById("f_size").style.display="none";
	}
   
       if(value =="Corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
		   document.getElementById("corporateId").style.display="block";
		  
       }
	   else{
           document.getElementById("c_name").style.display="none";
           
			
	  }
   }
   
    function is_married(value){
      
        if(value =="married"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
$(document).on('blur', '#dob', function () {
	$('#dob').addClass('focus');
});
$(document).on('blur', '#ane', function () {
	$('#ane').addClass('focus');
});

function check_email(){
	 var a=$('#email').val();
	 var v1 = a.indexOf("@");
	 //alert(v1);
	 var v2 = a.indexOf(".");
	 //alert(v2);
	 var v3=a.length;
	 if((v1<v2)&&(v2-v1)<12 && (v3-v2)<=5){
	 }
	 else{
		 $('#email').val("");
	 }
}


function show_gID_num(){
	//alert("kfguyjg");
	//$("#g_id_number").css('display','block');
}

</script> 

