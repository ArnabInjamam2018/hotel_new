<?php 

date_default_timezone_set('Asia/Kolkata');

$total_rent=0;
if(isset($taxes) && $taxes!=''){
foreach ($taxes as $tax) {
	$hotel_service_tax=$tax->hotel_service_tax;
	$hotel_luxury_tax=$tax->hotel_luxury_tax;
	
}
}
else{
	
	$hotel_service_tax=0;
	$hotel_luxury_tax=0;
}
date_default_timezone_set('Asia/Kolkata');

$start_dt = date("d-m-Y", strtotime($start));

if(isset($times) && $times){
foreach($times as $time) {
        if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
            $tym = ($time->hotel_check_in_time_hr + 12) . ":" . $time->hotel_check_in_time_mm;
        }
        else{
            $tym = ($time->hotel_check_in_time_hr) . ":" . $time->hotel_check_in_time_mm;
        }
    }
}else{
	$tym =0;
}
if($start_dt==date("d-m-Y") && ($tym < date("H:i:s"))){

	

    $checkin_time=date("H:i:s");
}
else{

    foreach($times as $time) {
        if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
            $checkin_time = ($time->hotel_check_in_time_hr + 12) . ":" . $time->hotel_check_in_time_mm;
        }
        else{
            $checkin_time = ($time->hotel_check_in_time_hr) . ":" . $time->hotel_check_in_time_mm;
        }
    }
}
if(isset($times) && $times){
foreach($times as $time) {
    if($time->hotel_check_out_time_fr=='PM' && $time->hotel_check_out_time_hr !="12") {
        $checkout_time = ($time->hotel_check_out_time_hr + 12) . ":" . $time->hotel_check_out_time_mm;
    }
    else{
        $checkout_time = ($time->hotel_check_out_time_hr) . ":" . $time->hotel_check_out_time_mm;
    }
}}

$end_dt = date("d-m-Y", strtotime($end));
$start_d = new DateTime($start_dt);
$end_d =  new DateTime($end_dt);
$dStart = new DateTime($start_dt);
   $dEnd  = new DateTime($end_dt);
   $dDiff = $dStart->diff($dEnd);
   
   $datediff= $dDiff->days;
//echo $start_dt;
//echo $end_dt;
//exit();


     $diff= floor($datediff/(60*60*24));

$start_time = date("H:i:s", strtotime($start));


$end_time = date("H:i:s", strtotime($end));

$event_name="No events today";
$event_color_bg="white";
$event_color_text="#b4bcc8";


?>

<div style="position:relative; height: 100%;"> 
  <script>
    setTimeout(function() 
    { 
        document.getElementById("loader").style.display = "none"; 
        document.getElementById("body").style.display = "block"; 
    }, 1500);
    
    
    </script>

  <div class="page-container" id="body" style="display:none;">
    <div class="portlet box grey-cascade" style="margin-bottom:0px;">
      <div class="portlet-title">
        <div class="caption"><i class="icon-pin font-white"></i> Enquiry Form </div>
        <div class="tools" style="display: inline-block; float: right; padding: 12px 0 8px;"> <a onclick="cancel_booking();" style="color:#ffffff;"> <!--href="javascript:close();"--><i class="fa fa-times"> </i></a> </div>
        <div id="short_info" style="font-size:12px; text-align:center; width:100%; padding:11px 0;"></div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
          <div class="clearfix long">
            <ul class="nav wizard-nav-list">
              <li class="wizard-nav-item" id="istli"><a class="wizard-nav-link active" ><span class="glyphicon glyphicon-chevron-right"></span> Guest Details</a></li>
              <li class="wizard-nav-item" id="2ndli"><a class="wizard-nav-link"><span class="glyphicon glyphicon-chevron-right"></span> Booking Preference</a></li>
              <!--<li class="wizard-nav-item" id="3rdli"><a class="wizard-nav-link"><span class="glyphicon glyphicon-chevron-right"></span> Broker / Channel</a></li>-->
              <!--<li class="wizard-nav-item" id="4thli"><a class="wizard-nav-link"><span class="glyphicon glyphicon-chevron-right"></span> Summary</a></li>-->
              <li class="wizard-nav-item" id="5thli"><a class="wizard-nav-link red"><span class="glyphicon glyphicon-chevron-right"></span> Booking Details</a></li>
            </ul>
            <div class="new-bookarea">
              
             
              <!-- child booking-->
              <div id="tab_child"  style="display:none; background: lightcyan; padding:10px;">
                <form action="" class="form-horizontal" id="formchild" method="POST">
                  <div class="form-body">
                    <input type="hidden" id="id_guest" name="id_guest" value="" />
                    <input type="hidden" id="room_id" name="room_id" value="<?php echo $resource; ?>" />
					
                    <div class="form-group">
                      <label class="control-label col-md-3">Master Booking Id: <span class="required"> * </span> </label>
                      <div class="row">
                        <div class="col-xs-2"> <b style="margin-left: 45%; margin-top: 550px; color: #a9a9a9"> HM0<?php echo $this->session->userdata('user_hotel') ?>00</b> </div>
                        <div class="col-xs-6">
                          <input type="text" required onkeyup="master_id_hint(this.value)" onkeypress="return onlyNos(event, this); " class="form-control" name="master_id" id="master_id" value="" placeholder="Master Booking Id"/>
                        </div>
                        <div class="col-xs-3" id="master_id_validate"> </div>
                        <input type="hidden" id="master_id_validate_token">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3">Guest Name: <span class="required"> * </span> </label>
                      <div class="col-md-4">
                        <input type="text" id="g_name_child" required onkeypress="return onlyLtrs(event, this);" class="form-control" name="cust_name" id="cust_name" placeholder="Guest Name" value="" />
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label class="control-label col-md-3"> Address: <span class="required"> * </span> </label>
                          <div class="col-md-4">
                            <input id="g_address_child" type="text" required class="form-control" name="cust_address" 
                                      id="cust_address" placeholder="Guest Address" value=""/>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label class="control-label col-md-3">Mobile Number: <span class="required"> * </span> </label>
                          <div class="col-md-4">
                            <input id="g_number_child" type="text" required maxlength="10" onkeypress="return onlyNos(event,this);" class="form-control" name="cust_contact_no" id="cust_contact_no"placeholder="Guest Number" value=""/>
													
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="line" style="float:left; width:100%;">
                      <div class="row">
                        <div class="col-xs-6">
                          <div class="form-group ">
                            <label class="control-label col-md-3">Check in date: <span class="required"> * </span> </label>
                            <div class="col-md-4">
                              <input type="text" required  class="form-control" id="startDate"  name="start_dt" value="<?php echo $start_dt; ?>"  />
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-6">
                          <div class="form-group  ">
                            <label class="control-label col-md-3">Check in Time: <span class="required"> * </span> </label>
                            <div class="col-md-4">
                              <input type="text" id="checkin_id" required class="form-control"  name="start_time" value="<?php echo $checkin_time;?>" />
                              <!--<input type="hidden"  class="form-control" name="start_time" value="<?php echo $start_time; ?>" />--> 
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                    <div class="line" style="float:left; width:100%;">
                      <div class="row">
                        <div class="col-xs-6">
                          <div class="form-group  ">
                            <label class="control-label col-md-3">Check Out date: <span class="required"> * </span> </label>
                            <div class="col-md-4">
                              <input onblur="check_date(this.value)"  required="required" id="checkout_date_child" type="text" class="form-control" name="end_dt"
                                                            value="<?php echo $end_dt; ?>"/>
                              <input type="hidden" id="checkout_date_child_confirmed" value="">
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-6">
                          <div class="form-group  ">
                            <label class="control-label col-md-3">Check Out Time: <span class="required"> * </span> </label>
                            <div class="col-md-4">
                              <input type="text" id="checkout_time_child" required class="form-control" name="end_time"
                                                           value="<?php echo $checkout_time;?>"/>
                              <!--<input type="hidden"  class="form-control" name="end_time" value="<?php echo $checkout_time; ?>" />--> 
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4" id="checkout_date_child_span"> </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label class="control-label col-md-3">Nature of visit:</label>
                          <div class="col-md-4">
                            <select name="nature_visit" id="nature_visit" class="form-control bs-select" placeholder=" Booking Type">
                              <?php
                                //$nv=$this->Dashboard_model->all_booking_nature_visit();
                                //if(isset($nv)){
                                    //foreach($nv as $value) {
                                                                    
                              ?>
                              <option value="<?php echo 'hello';//$value->booking_nature_visit_id ?>"><?php echo 'hello';//$value->booking_nature_visit_name;?></option>
                              <?php //}} ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label class="control-label col-md-3">Next Destination <span class="required"> * </span> </label>
                          <div class="col-md-4">
                            <input type="text" required class="form-control" name="next_destination"  placeholder="Next Destination"/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <input name="Submit1" id="submit1" type="submit" class="btn btn-primary" value="Continue" />
                      <a href="javascript:close();" class="btn btn-default"> <i class="m-icon-swapright"></i> Cancel </a> 
                      
                      <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Send message</button>--> 
                    </div>
                  </div>
                </form>
              </div>
              <!-- child booking-->
              <div id="tab12" style="display:block; min-height: 514px; background:#F1F3F2; border:1px solid #e5e5e5; position:relative;">
                <form action="" id="form1st" method="POST">
                  <div class="form-body" style="padding-bottom:86px;">
				  <input type="hidden" id="number_of_room" name="number_of_room" value="<?php echo $number_of_room; ?>" />
                    <input type="hidden" id="id_guest2" name="id_guest" value="" />
                    <input type="hidden" id="room_id" name="room_id" value="<?php echo $resource; ?>" />
					
                    <div class="row">
					<div style="margin-bottom:10px; font-size:14px; color:#008F6C; text-align:center;">  </div>
                      <div class="col-xs-12">
                          <div class="form-group">
                            <div class="typeahead__container ">
                                <div class="typeahead__field">            
                                    <span class="typeahead__query">
                                        <input class="js-typeahead-user_v1" name="g_name[query]" id="g_name" type="search" placeholder="search guest here..." autocomplete="off">
                                    </span>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>Profit Center <span class="required"> * </span> </label>
                          <select name="p_center" class="form-control input-sm bs-select" id="" required>
                            <?php $pc=$this->dashboard_model->all_pc();?>
                            <?php
                              $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
                              ?>
                            <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
                            <?php $pc=$this->dashboard_model->all_pc1();
                                foreach($pc as $prfit_center){
                                ?>
                            <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                            <?php }?>
                          </select>
                        </div>
                      </div>
					   <div class="col-xs-6">
                        <div class="form-group">
                          <label>Guest Name: <span class="required"> * </span> </label>
                          <input type="text" required onkeypress="return onlyLtrs(event, this);" class="form-control input-sm" name="cust_name" id="cust_name" placeholder="Guest Name" value="" />
                        </div>
                      </div>
                       <div class="col-xs-6">
                        <div class="form-group">
                          <label> Pin Code: </label>
                          <input type="text" class="form-control input-sm" name="cust_address" id="cust_address" placeholder="Guest Pincode" onblur="fetch_all_address()" value="" />
                          <input type="text"  name="cust_full_address" id="cust_full_address" style="display:none;"/>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>Mobile Number: <span class="required"> * </span> </label>
                          <input type="text" maxlength="30" onblur="onlyNum(this.value);" class="form-control input-sm" name="cust_contact_no" id="cust_contact_no" placeholder="Guest Number" value=""/> 
						
					   </div>
                      </div>
					  	<script>
						function onlyNum(num){
						 
							  var filter = /^[0-9-+/, ]+$/;

						if (filter.test(num)) {

						return true;
						}else {
						swal("Please enter number properly!", "", "warning");
						$("#cust_contact_no").val("");
						$("#cust_contact_no").focus();
						}

					}
		</script>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>Email Id: </label>
                          <input type="text" required class="form-control input-sm" name="cust_mail" id="cust_mail" placeholder="Email Id" value="" />
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>Nature of visit: <span class="required"> * </span></label>
                          <?php $visit=$this->dashboard_model->getNatureVisit1(); ?>
                          <select name="nature_visit" id="nature_visit" class="form-control input-sm bs-select" placeholder="Booking Type" required>
                            <?php $defNature=$this->dashboard_model->default_nature_visit(); if(isset($defNature)&& $defNature){$defname=$defNature->booking_nature_visit_name;}else{$defname="Select";}  ?>
                            <option value="<?php echo $defname; ?>"selected><?php echo $defname;?></option>
                            <?php foreach ($visit as $key ) {
                                        # code...
                                      ?>
                            <option value="<?php echo $key->booking_nature_visit_name; ?>"><?php echo $key->booking_nature_visit_name; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group ">
                          <label>Check in date: <span class="required"> * </span> </label>
                          <div class="input-group date date-picker">
                            <input type="text" required id="start_dt"  class="form-control input-sm" name="start_dt" value="<?php echo $start_dt; ?>" readonly onblur="change_stay_span(this.value)" >
                            <span class="input-group-btn">
                            <button class="btn default btn-sm" type="button" style="padding: 5px 10px 4px;" onclick="make_date_readable('start_dt')"><i class="fa fa-edit"></i></button>
                            </span> </div>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>Check in Time: <span class="required"> * </span> </label>
                          <input type="text" id="checkin_id" required  onclick="check_booking(this.value)" class="form-control input-sm" name="start_time" value="<?php echo $checkin_time;?>" />
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group  ">
                          <label>Check Out date: <span class="required"> * </span> </label>
                          <div class="input-group date date-picker">
                            <input  required="required" type="text" class="form-control input-sm" id="end_dt" name="end_dt" value="<?php echo $end_dt; ?>" readonly />
                            <span class="input-group-btn">
                            <button class="btn default btn-sm" type="button" style="padding: 5px 10px 4px;" onclick="make_date_readable('end_dt')"><i class="fa fa-edit"></i></button>
                            </span> </div>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group  ">
                          <label>Check Out Time: <span class="required"> * </span> </label>
                          <input type="text" required class="form-control input-sm" name="end_time" id="end_time" value="<?php echo $checkout_time;?>"/>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>Coming From</label>
                          <input type="text" class="form-control input-sm" name="coming_from" placeholder="Coming From"/>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>Marketing Personnel: <span class="required"> * </span></label>
                          <?php $marketing=$this->unit_class_model->all_marketing_personel();   ?>
                          <select name="marketing_personnel" id="marketing_personnel" class="form-control input-sm bs-select" placeholder="Marketing Personnel" required>
                            <option value="0">...Select Marketing Personnel...</option>
                            <?php foreach ($marketing as $key ) {
                                        # code...
                                      ?>
                            <option value="<?php echo $key->name; ?>"><?php echo $key->name; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>Next Destination<span class="required"> </span> </label>
                          <input type="text"  class="form-control input-sm" name="next_destination" placeholder="Next Destination"/>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-actions right" style="position:absolute; bottom:0; width: 100%;">
                    <input name="Submit1" id="submit1" type="submit" class="btn btn-primary" value="Continue" />
                    <a href="javascript:close();" class="btn btn-default"> <i class="m-icon-swapright"></i> Cancel </a> <a href="javascript:void(0);" onclick="prev();" class="btn btn-default"> <i class="m-icon-swapleft"></i> Previous </a> 
                    
                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Send message</button>--> 
                  </div>
                </form>
              </div>
			  
			  
			  
			  
			  
              <div class="frm2 form" id="tab2" style="display:none; min-height: 514px; background:#F1F3F2; border:1px solid #e5e5e5; padding:10px; position:relative;">
                <form action="" id="form2nd" method="POST">
                  <div class="form-body" style="padding-bottom:86px;">
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group form-md-radios">
                          <div class="md-radio-inline">
                            <div class="md-radio" style="margin-right: 15px;">
                              <input type="radio" id="radio7" name="booking_type" onclick="get_season()" class="md-radiobtn" value="advance" required>
                              <label for="radio7"> <span></span> <span class="check"></span> <span class="box"></span> <i class="fa fa-calendar" aria-hidden="true"></i> Advance </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-4">
                        <div class="form-group">
                          <label>Booking Source:</label>
                          <select name="booking_source"  id="booking_source" class="form-control input-sm bs-select" >
                            <?php $defBookingSource=$this->unit_class_model->default_booking_source(); 
                                
                                if(isset($defBookingSource) && $defBookingSource){$defname=$defBookingSource->booking_source_name ;}else{$defname="Select";}
                                ?>
                            <option value="<?php echo $defBookingSource->bk_source_type_id;?>" selected><?php echo $defname;?></option>
                            <?php 
                              $dta=$this->unit_class_model->get_booking_source1();
                              if($dta)
                              {
                                  foreach($dta as $d) 
                              {
                                  ?>
                            <option value="<?php echo $d->bk_source_type_id;?>"><?php echo $d->booking_source_name;?></option>
                            <!----<option value="Frontdesk">Frontdesk</option>
                              <option value="Online">Online</option>
                              <option value="Telephonic">Telephonic</option>
                              <option value="Broker">Broker</option>
                              <option value="Channel">Channel</option>---->
                            <?php } }?>
                          </select>
                          <input type="hidden" id="season_id" />
                          <input type="hidden" id="u_type_id" value="<?php echo $resource; ?>"/>
                          <input type="hidden" id="o_id" />
                        </div>
                      </div>
                      <input id="brokerchannel" value="" type="hidden"/>
                      <div class="col-xs-4">
                        <div class="form-group">
                          <label>No. of Adult: <span class="required"> * </span> </label>
                          <input onblur="check_occupancy()" id="no_of_adult" type="text" required  class="form-control input-sm" name="adult" value=""  />
                        </div>
                      </div>
                      <div class="col-xs-4">
                        <div class="form-group">
                          <label>No. of Child:<span class="required"> * </span> </label>
                          <input onblur="check_occupancy()" id="no_of_child" type="text" required class="form-control input-sm" name="child" value="" />
                        </div>
                      </div>
                      <input type="hidden" id="occupancy" value="<?php echo $mo; ?>"/>
                      <input type="hidden" id="room_bed" value="<?php echo $do; ?>"/>
                      <div class="col-xs-4">
                        <div class="form-group">
                          <label>Charge Type:</label>
                          <select name="booking_rent" id="booking_rent" class="form-control input-sm bs-select" onchange="get_meal_plan(this.value)" required="required">
                            <option value="" disabled selected>Select Charge Type</option>
                            <option value="b_room_charge" >Base Room Rent</option>
                            <option value="w_room_charge" >Weekend Room Rent</option>
                            <option value="s_room_charge" >Seasonal Room Rent</option>
                          </select>
                        </div>
                      </div>
					  
                      <?php $meal_plan = $this->bookings_model->get_meal_plan(); ?>
                      <div class="col-xs-4">
                        <div class="form-group" id="mp">
                          <label>Meal Plan</label>
                          <select name="plan_id"  id="plan_id" onchange="get_rmCh(taxCal)" class="form-control input-sm" required="required" >
                            <option value="" selected disabled>Select meal plan *</option>
                            <?php 
                            if(isset($meal_plan) && $meal_plan){	
                             foreach ($meal_plan as $meal_plan) {
                                ?>
                            <option value="<?php echo $meal_plan->hotel_meal_plan_cat_id; ?>"><?php echo $meal_plan->name; ?></option>
                            <?php
                            }}
                            ?>
                          </select>
                        </div>
                      </div>
                      <div id="extra_charge" style="display: none;">
                        <div class="col-xs-4" >
                          <div class="form-group">
                            <label>Extra Charge Type:</label>
                            <div class="radio-list re-ac">
                              <label class="radio-inline add1">
                                <input class="addition3" type="radio" name="charge_mode_type" id="charge_mode_type" value="adult" onclick="set_extra_charge(this.value)" style="display:none;">
                                <i class="fa fa-male"></i> Adult</label>
                              <label class="radio-inline sub1">
                                <input class="addition4" type="radio" name="charge_mode_type" id="charge_mode_type" value="child" onclick="set_extra_charge(this.value)" style="display:none;">
                                <i class="fa fa-child"></i> Child</label>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-4"> </div>
                        <div class="col-xs-4" id="extra_charge_modifier" >
                          <div class="form-group">
                            <label>Extra Charge:</label>
                            <input onblur="charge_modifier(this.value)" readonly type="text" class="form-control input-sm" id="extra_charge_amount" name="extra_charge_amount"   value="0"/>
                          </div>
                        </div>
                      </div>
                      <div id="rrMod">
                        <div class="col-xs-4">
                          <div class="form-group">
                            <label>Rent modifier<span class="required"></span></label>
                            <div class="radio-list re-modi">
                              <label id="lbl" class="checkbox-inline perc" style="padding:0">
                                <input class="addition33" type="checkbox" id="perc" name="perc" value="percent" style="display:none;">
                                <i class="fa fa-percent"></i></label>
                              <label class="radio-inline add" style="padding:0">
                                <input class="addition1" type="radio" name="rent_mode_type" id="rent_mode_type" value="add" style="display:none;">
                                <i class="fa fa-plus-square"></i></label>
                              <label class="radio-inline sub" style="padding:0; margin-left:7px;">
                                <input class="addition2" type="radio" name="rent_mode_type" id="rent_mode_type" value="substract" style="display:none;">
                                <i class="fa fa-minus-square"></i></label>
                              <input type="hidden" id="perc1" name="perc1" value="0">
                              <input type="hidden" id="modf_rr" name="modf_rr" value="0">
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-4" style="display:none;" id="dp">
                          <div class="form-group" id="spn">
                            <label>Modifier Amount <span class="required"> * </span></label>
                            <input type="text"  class="form-control input-sm" min="0" id="mod_room_rent" name="mod_room_rent" value="" onkeypress="return onlyNos(event,this);" onblur="amount_check(this.value)" />
                            <span class="error-val" style="display:none;">Max value 100</span> </div>
                        </div>
                      </div>
                      <!-- End rrMod Div -->
                      
                      <div class="col-xs-4">
                        <div class="form-group">
                          <label>Meal modifier<span class="required"></span></label>
                          <div class="radio-list re-modi">
                            <label id="lbl1" class="checkbox-inline perc" style="padding:0">
                              <input class="addition34" type="checkbox" id="mperc" name="mperc" value="percent" style="display:none;">
                              <i class="fa fa-percent"></i></label>
                            <label class="radio-inline add11" style="padding:0">
                              <input class="addition11" type="radio" name="m_rent_mode_type" id="m_rent_mode_type" value="add" style="display:none;">
                              <i class="fa fa-plus-square"></i></label>
                            <label class="radio-inline sub11" style="padding:0; margin-left:7px;">
                              <input class="addition22" type="radio" name="m_rent_mode_type" id="m_rent_mode_type" value="substract" style="display:none;">
                              <i class="fa fa-minus-square"></i></label>
                            <input type="hidden" id="m_perc1" name="m_perc1" value="0">
                            <input type="hidden" id="modf_mp" name="modf_mp" value="0">
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-4" style="display:none;" id="dp1">
                        <div class="form-group" id="spn">
                          <label>Meal Mod Amt <span class="required"> * </span></label>
                          <input type="text"  class="form-control input-sm" min="0" id="mod_meal_rent" name="mod_meal_rent" value="" onkeypress="return onlyNos(event,this);" onblur="m_amount_check(this.value)" />
                          <span class="error-val" style="display:none;">Max value is 100</span> </div>
                      </div>
                      <div id="charges" class="clearfix" style="margin-bottom: 15px;">
                        <div class="col-xs-12">
                          <div class="row">
                            <div class="col-xs-6">
                              <label> Room Rent: </label>
                              <label>Rs. <span id="target"></span></label>
                            </div>
                            <div class="col-xs-6">
                              <label> Room Rent (Tax): </label>
                              <label>Rs. <span id="target_tr"></span></label>
							  <input type="hidden" value="0" id="rr_total_tax">
							  <input type="hidden" value="0" id="mp_total_tax">
                            </div>
                            <input type="hidden" id="dumb_rate" value="0"/>
                   <input type="hidden" class="form-control input-sm" id="base_room_rent1" name="base_room_rent1"  value="0"/>
                   
                  
<input type="hidden" class="form-control input-sm" id="chk_mod" value="0"/>
                            <input type="hidden" class="form-control input-sm" id="room_id" name="room_id"  value="<?php //echo $room_id; ?>"/>
                            <input type="hidden" class="form-control input-sm" id="diff" name="stay_span"  value="<?php echo $datediff; ?>"/>
                          </div>
                        </div>
                        <div class="col-xs-12">
                          <div class="row">
                            <div class="col-xs-6">
                              <label> Extra Person RR: </label>
                              <label>Rs. <span id="target_ex"></span></label>
                            </div>
                            <div class="col-xs-6">
                              <label> Extra Person RR Tax: </label>
                              <label>Rs. <span id="target_extx"></span></label>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12">
                          <div class="row">
                            <div class="col-xs-6">
                              <label> Meal Plan: </label>
                              <label>Rs. <span id="target_mp"></span></label>
                            </div>
                            <div class="col-xs-6">
                              <label> Meal Plan(Tax): </label>
                              <label>Rs. <span id="target_tm"></span></label>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12">
                          <div class="row">
                            <div class="col-xs-6">
                              <label> Extra Person MP: </label>
                              <label>Rs. <span id="target_exmp"></span></label>
                            </div>
                            <div class="col-xs-6">
                              <label> Extra Person MP Tax: </label>
                              <label>Rs. <span id="target_exmptx"></span></label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- End of #charges div --> 
                      
                      <!-- Meal Modifier was here -->
                      
                      <input type="hidden" class="form-control input-sm" id="base_room_rent" name="base_room_rent"  value="0"/>
					  <input type="hidden" class="form-control input-sm" id="unit_room_rent" name="unit_room_rent"  value="0"/>
                      <input type="hidden" class="form-control input-sm" id="ex_adult_r" name="ex_adult_r"  value="0"/>
                      <input type="hidden" class="form-control input-sm" id="ex_child_r" name="ex_child_r"  value="0"/>
                      <input type="hidden" class="form-control input-sm" id="ex_u_price" name="ex_u_price"  value="0"/>
                      <input type="hidden" class="form-control input-sm" id="ex_u_mprice" name="ex_u_mprice"  value="0"/>
                      <input type="hidden" class="form-control input-sm" id="ex_adult_m" name="ex_adult_m"  value="0"/>
                      <input type="hidden" class="form-control input-sm" id="ex_child_m" name="ex_child_m"  value="0"/>
                      <input type="hidden" id="price" name="plan_price" class="form-control input-sm" value="0"/>
                      <input type="hidden" id="price2" name="plan_tprice" class="form-control input-sm" value="0"/>
                      <input type="hidden" id="price1" name="plan_price1" class="form-control input-sm" value="0"/>
                      <div class="col-xs-12">
                        <div class="form-group">
                          <label>Comment for booking</label>
                          <textarea class="form-control" rows="1" name="comment"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-actions right" style="position:absolute; bottom:0; width: 100%;">
                    <input type="hidden" name="booking_1st" id="booking_1st" value="" />
                    <a href="javascript:close();" class="btn btn-default"> <i class="m-icon-swapright"></i> Cancel </a> <a href="javascript:void(0);" onclick="prev1();" class="btn btn-default"> <i class="m-icon-swapleft"></i> Previous </a>
                    <input name="Submit2" id="submit2" onclick="check_sum3('sum2')"  type="submit" class="btn green popovers" value="Continue" data-container="body" data-trigger="hover" data-placement="top" data-content="Please check all the entered data before proceeding" data-original-title="Check Carefully" aria-describedby="popover692586"/>
                  </div>
                </form>
              </div>
			  
			  

              <div class="frm3 form" id="tab3" style="display:none; min-height: 514px; background:#F1F3F2; border:1px solid #e5e5e5; position:relative;">
                <form action="" id="form3rd" method="POST">
                  <div class="form-body" style="padding-bottom:86px;">
                    <div class="row">
                      <div class="col-xs-6">
                        <div class="form-group" id="broker_name">
                          <label>Broker/Channel Name<span class="required"> * </span> </label>
                          <select name="broker_id" id="broker_id" class="form-control bs-select" onchange="get_commision()" >
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group" id="broker_commision">
                          <label>Commision <span class="required"> * </span> </label>
                          <input value="" type="text" class="form-control" name="broker_commission" id="broker_commission" placeholder="Commision" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-actions right" style="position:absolute; bottom:0; width: 100%;">
                    <input type="hidden" name="booking_1st12" id="booking_1st12" value="" />
                    <input name="Submit3" id="submit3" type="submit" class="btn btn-primary" value="Continue" />
                    <a href="javascript:close();" class="btn btn-default"> <i class="m-icon-swapright"></i> Cancel </a> <a href="javascript:void(0);" onclick="prev12();" class="btn btn-default"> <i class="m-icon-swapleft"></i> Previous </a> </div>
                </form>
              </div>
              <div class="frm4 form" id="tab4" style="display:none; min-height: 514px; background:#F1F3F2; border:1px solid #e5e5e5; position:relative;">
                <form action="" id="form4th" method="POST">
                  <div class="form-body" style="padding-bottom:86px;">
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <?php if(isset($tax) && $tax):?>
                          <input type="hidden" id="stax" value="<?php echo $tax->hotel_service_tax; ?>" />
                          <input type="hidden" id="ltax" value="<?php echo $tax->luxury_tax_slab1; ?>" />
                          <input type="hidden" id="scharge" value="<?php echo $tax->hotel_service_charge; ?>" />
                          <input type="hidden" id="servicet" value="<?php echo $tax->hotel_service_tax; ?>" name="servicet"/>
                          <input type="hidden" id="luxuryt" value="<?php echo $tax->luxury_tax_slab1; ?>" name="luxuryt"/>
                          <input type="hidden" id="servicec" value="<?php echo $tax->hotel_service_charge; ?>" name="servicec"/>
                          <input type="hidden" id="dblt1" value="<?php echo $tax->luxury_tax_slab1; ?>"/>
                          <input type="hidden" id="dblt2" value="<?php echo $tax->luxury_tax_slab2; ?>"/>
                          <input type="hidden" id="dbSlab1" value="<?php echo $tax->luxury_tax_slab1_range_to; ?>"/>
                          <input type="hidden" id="dbSlab2" value="<?php echo $tax->luxury_tax_slab1_range_from; ?>"/>
                          <input type="hidden" id="dbSlab3" value="<?php echo $tax->luxury_tax_slab2_range_to; ?>"/>
                          <input type="hidden" id="dbSlab" value="<?php echo $tax->luxury_tax_slab2_range_from; ?>"/>
                          <?php endif; ?>
                          <select name="booking_tax" id="booking_tax" required="required" class="form-control input-sm bs-select" onchange="noTax(this.value)" ><!--onchange="check_sum3(this.value)">-->
                            <option value="" disabled selected>Tax Type</option>
                            <option value="notax" >No Tax</option>
                            <option id="luxury" value="tax"  selected>Tax</option>
                          </select>
						  <input type="hidden" id="tax_track">
                          <input type="hidden" id="booking_tax_type" name="booking_tax_type" onchange="check_sum3(this.value)" value="No Tax"/>
						  <input type="hidden" id="booking_tax_types" name="booking_tax_types"  value=""/>
                        <!--<input type="hidden" id="booking_rm_no" name="booking_rm_no" value="<?php //echo $room_no; ?>"/>-->
                        </div>
                      </div>
                      <div class="clearfix" style="margin-bottom:15px;">
                      <div class="col-xs-12">
                        <div class="row">                         
                          <div class="col-xs-6">
                            <label>Total Rent:</label>
                          </div>
                          <div class="col-xs-6">
                            <label  style="color:#008F6C;" id="target2" > </label>
                          </div>
                     	</div>
                      </div>
                      <div class="col-xs-12">
                        <div class="row">
                          <div class="col-xs-6">
                            <label>Meal Price:</label>
                          </div>
                          <div class="col-xs-6">
                            <label style="color:#008F6C;" id="m_target2" > </label>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12">
                        <div class="row">
                          <div class="col-xs-6">
                            <label>Extra Person Room Rent:</label>
                          </div>
                          <div class="col-xs-6">
                            <label style="color:#008F6C;" id="ex_target2" > </label>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12">
                        <div class="row">
                          <div class="col-xs-6">
                            <label>Extara Person Meal Price:</label>
                          </div>
                          <div class="col-xs-6">
                            <label style="color:#008F6C;" id="m_targetEx" > </label>
                          </div>
                        </div>
                      </div>
                      <!--<div class="col-xs-12">
                        <div class="row">
                          <div class="col-xs-6">
                            <label>Tax Details :</label>
                          </div>
                          <div class="col-xs-6" id="tax_details_area">
                            
                          </div>
                        </div>
                      </div>-->
                      <div class="col-xs-12">
                        <div class="row">
                          <div class="clearfix"></div>
                          <div class="col-xs-6">
                            <label> Room Rent Tax: </label>
                              <label>Rs. <span id="target_tr"></span></label>
                          </div>
                          <div class="col-xs-6">
                            <label id="target5" style="display:block; color:#DC166A;"></label>
                          </div>
                        </div>
						<div class="row">
                          <div class="clearfix"></div>
                          <div class="col-xs-6">
                            <label> Extra Room Rent Tax: </label>
                              <label>Rs. <span id="ext_target_tr1"></span></label>
                          </div>
                          <div class="col-xs-6">
                            <label id="ext_target_tr" style="display:block; color:#DC166A;"></label>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12">
                        <div class="row">
                          <div class="col-xs-6">
                            <label> Meal Tax :</label>
                          </div>
                          <div class="col-xs-6">
                            <label id="total_m_tax" style="display:block; color:#DC166A;"></label>
                          </div>
                        </div>
						<div class="row">
                          <div class="col-xs-6">
                            <label> Extra Meal Tax :</label>
                          </div>
                          <div class="col-xs-6">
                            <label id="ext_total_m_tax" style="display:block; color:#DC166A;"></label>
                          </div>
                        </div>
                      </div>
                     <!--<div class="col-xs-12">
                        <div class="row">
                          <div class="col-xs-6">
                            <label> Extra Person Room Tax :</label>
                          </div>
                          <div class="col-xs-6">
                            <label id="ex_tax" style="display:block; color:#DC166A;"></label>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12">
                        <div class="row">
                          <div class="col-xs-6">
                            <label> Extra Person Meal Tax :</label>
                          </div>
                          <div class="col-xs-6">
                            <label id="exM_tax" style="display:block; color:#DC166A;"></label>
                          </div>
                        </div>
                      </div>-->
					  
					  
					  <input type="hidden" id="room_rent_total_amount_5" name="room_rent_total_amount_5" value=""/>
                      <input type="hidden" id="room_rent_tax_amount_5" name="room_rent_tax_amount_5" value=""/>
                      <input type="hidden" id="food_plan_price_5" name="food_plan_price_5" value=""/>
                      <input type="hidden" id="food_plan_tax_5" name="food_plan_tax_5" value=""/>
                      <input type="hidden" id="ex_per_price_5" name="ex_per_price_5" value=""/>
                      <input type="hidden" id="ex_per_tax_5" name="ex_per_tax_5" value=""/>
                      <input type="hidden" id="ex_per_m_price_5" name="ex_per_m_price_5" value=""/>
					  <input type="hidden"  id="ex_per_m_tax_5" name="ex_per_m_tax_5"  value=""/>
					  <input type="hidden"  id="room_rent_sum_total_5" name="room_rent_sum_total_5"  value=""/>
                      <div class="col-xs-12">
                        <div class="row">
                          <div class="col-xs-6">
                            <label style="font-weight:bold;">Sum Total:</label>
                          </div>
                          <div class="col-xs-6">
                            <label style="font-weight:bold;" id="target3"></label>
                          </div>
                        </div>
                      </div>
                      </div>
                      <input type="hidden" id="tax" name="tax"/>
                      <input type="hidden" id="total" name="total"/>
                      <input type="hidden" id="ex_per_price" name="ex_per_price"/>
                      <input type="hidden" id="ex_per_tax" name="ex_per_tax"/>
                      <input type="hidden" id="ex_per_m_price" name="ex_per_m_price"/>
                      <input type="hidden" id="ex_per_m_tax" name="ex_per_m_tax"/>
                      <input type="hidden" id="total_m_tax1" name="plan_m_tax" class="form-control input-sm"/>
					   <input type="hidden"  id="extra_ch_no" name="extra_ch_no"  value="0"/>
                      <input type="hidden"  id="extra_ad_no" name="extra_ad_no"  value="0"/>
                      <div class="col-xs-12 form-group">
                        <!--<input name="Submit5" id="submit5" type="submit" class="btn pink" value="Take Payment"  onclick="shows_hiden()"/>-->
                        <a onclick= "openview1();" class="btn green lst" id="hide-btn" style="margin-top:0px;float: right;margin-right: 10px display:block;"> Continue </a> </div>
                      <div class="col-xs-6" style="display:none;" id="paymm">
                        <div class="form-group">
                          <label>Payment Mode</label>
                          <select name="t_payment_mode" id="t_payment_mode" class="form-control input-sm bs-select" placeholder="Booking Type" onchange="payment_mode_change();" onclick="paymentamount_show">
                            <option value="Select The Payment Mode" disabled selected>Select The Payment Mode</option>
                            <!--- <option value="Cash">Cash</option>
                              <option value="Debit Card">Debit Card</option>
                              <option value="Credit Card">Credit Card</option>
                              <option value="Fund Transfer">Fund Transfer</option>-->
                            <?php $mop = $this->dashboard_model->get_payment_mode_list();
                  if($mop != false)
                  {
                      foreach($mop as $mp)
                      {
                    ?>
                            <option value="<?php echo $mp->p_mode_name; ?>" ><?php echo $mp->p_mode_des; ?></option>
                            <?php }
                  } ?>
                            <!--<option value="Bill To Company">Bill To Company</option>-->
                          </select>
                        </div>
                      </div>
                      <div id="payment_amnt" style="display:none;" >
                        <div class="form-group col-xs-6">
                          <label>Profit Center <span class="required"> * </span> </label>
                          <select name="p_center" class="form-control input-sm bs-select" id="">
                            <?php $pc=$this->dashboard_model->all_pc();?>
                            <?php
                              $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
                              ?>
                            <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
                            <?php $pc=$this->dashboard_model->all_pc1();
                                foreach($pc as $prfit_center){
                                ?>
                            <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                            <?php }?>
                          </select>
                        </div>
                        <div class="form-group col-xs-6">
                          <label>Payment Amount <span class="required"> * </span> </label>
                          <input type="text" id="payment_input" onkeypress="return onlyNos(event, this);" required value=""  class="form-control hlf" name="t_amount" placeholder= "Payment Amount " autocomplete="off" />
                          <span class="help-block"> </span> </div>
                      </div>
                      <div id="bank">
                        <div class="form-group col-xs-6">
                          <label>Bank Name <span class="required"> * </span> </label>
                          <input type="text"  required="required" onkeypress="return onlyLtrs();" class="form-control" name="t_bank_name" placeholder="Bank Name" />
                          <span class="help-block"> </span> </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-actions" style="position:absolute; bottom:0; width: 100%;">
                    <input type="hidden" name="booking_3rd" id="booking_3rd" value="" />
                    <input name="Submit4" id="submit4" type="submit" class="btn btn-primary" value="Submit"  onclick="show_hiden()" style="display:none;"/>
                  </div>
                </form>
                <div style="position: absolute; bottom: 18px; right: 12px;">
                  <form action="<?php echo base_url();?>bookings/popup_close" class="form-horizontal" id="f" method="POST">
                    <div class="new-btn" id="print_tab">
                      <input type="hidden" name="booking_3rd" id="booking_3rd" value="" />
                      <div id="hidden-div" style="display:none;"> 
					  
           <?php $invoice = 1;
		if($invoice == 1) {
	?>
            <a onclick="return folio_generate();" class="btn purplene pull-right" id="add_submit1">Generate Invoice</a>
            <?php } ?>	
          
					  <a class="btn green" id="dwn_link"  href=""  onclick="download_pdf(); "> Download <i class="glyphicon glyphicon-download"></i> </a>
					  <a onclick="openview();" class="btn yellow"> View <i class="fa fa-eye"></i></a>
					  <a onclick= "openview2();" class="btn blue lst" > Submit </a> </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" name="" id="response">
<input type="hidden" name="" id="BookingResponse">
<input type="hidden" name="" id="mealResponse">

<script src="<?php echo base_url();?>assets/global/plugins/typeahead1/jquery.typeahead.js"></script>

<script type="text/javascript">

window.expType = 'c';
  var bk_id = $('#booking_3rd').val();
    function close(result) {
        if (parent && parent.DayPilot && parent.DayPilot.ModalStatic) {
            parent.DayPilot.ModalStatic.close(result);
        }
    }

    /*function submit_1st() {
     var f1 = $("#form1st");
     $.ajax({
     type: "POST",
     url: "bin/process.php",
     data: dataString,
     success: function() {
     //display message back to user here
     }
     });
     return false;

     //return false;
     }*/
    $(document).ready(function() {
		
		
		
        //$('#tab11').hide();
       // $('#tab_child').hide();
        $('#tab12').show();
        $('#tab2').hide();
        $('#tab3').hide();
        $('#tab4').hide();
        //$('#broker_name').hide();
        //$('#broker_commision').hide();


		 
		 
		 
        $('#bank').hide();
        $('#istli').addClass("active");


        $("#form1st").validate({
            submitHandler: function() {

                $.post("<?php echo base_url();?>enquiry/hotel_backend_create",
                    $("#form1st").serialize(),
                    function(data){
                        $('#booking_1st').val(data.bookings_id);
                        $('#tab12').hide();
                        $('#tab2').show();
                        $('#istli').removeClass("active");
                        $('#2ndli').addClass("active");
                        window.parent.document.getElementById('txd').value=$('#booking_1st').val();
				   });
                return false; //don't let the page refresh on submit.

            }
			
			
        });

        $("#formchild").validate({
            submitHandler: function() {

                $.post("<?php echo base_url();?>enquiry/hotel_backend_create",
                    $("#formchild").serialize(),
                    function(data){
                        $('#booking_1st').val(data.bookings_id);
                        $('#tab_child').hide();
                        $('#tab2').show();
                        $('#istli').removeClass("active");
                        $('#2ndli').addClass("active");
                        document.getElementById('short_info').innerHTML="<b>Booking Id:</b> HM0<?php echo $this->session->userdata('user_hotel') ?>00"+data.bookings_id+" | <b>Guest Name:</b> "+data.guest_name.substring(0, 15)+"";
                    });
                return false; //don't let the page refresh on submit.

            }
        });

        $("#form2nd").validate({
			//if(){
				//alert("HERE");
			//}
            submitHandler: function() {
				//console.log("HERE");
                /*var booking_type = $("input[name=booking_type]:checked").val();
                 if()
                 return false;*/
                $.post("<?php echo base_url();?>enquiry/hotel_backend_create2",
                    $("#form2nd").serialize(),
                    function(data){
                        var booking_source = data.booking_source;
						<?php
						//$booking_source_ta_stat=$this->dashboard_model->get_booking_source_ta_stat();
						?>
						//var booking_source_ta_stat=0;
                        if(booking_source == 'Broker')
                        {
                            $('#booking_1st12').val(data.bookings_id);
                            $('#brokerchannel').val('0');
                            $('#tab2').hide();
                            $('#tab3').show();
                            $('#print_tab').hide();
                            $('#2ndli').removeClass("active");
                            $('#3rdli').addClass("active");
                            return_broker();
                        }
                        else if(booking_source == 'Channel'){
                            $('#booking_1st12').val(data.bookings_id);
                            $('#brokerchannel').val('1');
                            $('#tab2').hide();
                            $('#tab3').show();
                            $('#print_tab').hide();
                            $('#2ndli').removeClass("active");
                            $('#3rdli').addClass("active");
                            return_channel();

                        }
                        else
                        {
                            $('#booking_3rd').val(data.bookings_id);
                            $('#tab2').hide();
                            $('#tab4').show();
                            $('#2ndli').removeClass("active");
                            $('#5thli').addClass("active");
                        }

                    });
                return false; //don't let the page refresh on submit.

            }
        });

        $("#form3rd").validate({
            submitHandler: function() {

                var flag=document.getElementById("brokerchannel").value;
                if(flag=='0'){
                    var url='hotel_backend_create_broker';
                }
                else if(flag=='1'){
                    var url='hotel_backend_create_channel';
                }

                var bookings_id = $('#booking_1st12').val();

                $.post("<?php echo base_url();?>bookings/"+url+"?booking_id_broker="+bookings_id,
                    $("#form3rd").serialize(),
                    function(data){
                        $('#booking_3rd').val(data.bookings_id);
                        $('#tab3').hide();
                        $('#tab4').show();
                        $('#3rdli').removeClass("active");
                        $('#5thli').addClass("active");

                    });
                return false;

               /* $('#booking_3rd').val(bookings_id);
                $('#tab3').hide();
                $('#tab4').show();
                $('#3rdli').removeClass("active");
                $('#5thli').addClass("active");


                return false; //don't let the page refresh on submit.*/

            }
        });
        $("#form4th").validate({
            submitHandler: function() {
                $.post("<?php echo base_url();?>enquiry/hotel_backend_create4",
                    $("#form4th").serialize(),
                    function(data){
                        $('#booking_3rd').val(data.bookings_id);
                        $("#submit4").prop("disabled", true);
                        $('#print_tab').show();
						 window.parent.document.getElementById('txd').value='0';

                    });
                return false; //don't let the page refresh on submit.

            }
        });

    });

    function returning_form() {

        $('#tab1').hide();
        $('#tab11').show();

    }
    function child_form() {

        $('#tab1').hide();
        $('#tab_child').show();

    }
    function showbroker() {

        $('#broker_name').show();
        return_broker();
        $('#broker_commision').show();

    }

    function hidebroker() {

        $('#broker_name').hide();
        $('#broker_commision').hide();

    }
    function new_form()
    {
        $('#tab1').hide();
        $('#tab12').show();
    }

    $("#f").submit(function () {
        var f = $("#f");
        $.post(f.attr("action"), f.serialize(), function (result) {
            close(eval(result));
        });
        return false;
    });

	
    function prev(){
        $('#tab12').hide();
        $('#tab11').show();
    }	

    function prev1(){
        $('#tab2').hide();
        $('#tab12').show();
    }	
	
	
	
    $(document).ready(function () {
        $("#name").focus();
    });

</script> 
<script>
$(".addition1").on("click", function(){
	    var md = 0;
        document.getElementById("chk_mod").value = md;
        $("label.add").addClass("active");
		$("label.sub").removeClass("active");
		$("#dp").css('display', 'block');

});
$(".addition2").on("click", function(){
	    var md = 0;
        document.getElementById("chk_mod").value = md;
		$("label.sub").addClass("active");
		$("label.add").removeClass("active");
		$("#dp").css('display', 'block');
		
});

$(".addition11").on("click", function(){
	    var md = 0;
        document.getElementById("chk_mod").value = md;
        $("label.add11").addClass("active");
		$("label.sub11").removeClass("active");
		$("#dp1").css('display', 'block');

});
$(".addition22").on("click", function(){
	    var md = 0;
        document.getElementById("chk_mod").value = md;
		$("label.sub11").addClass("active");
		$("label.add11").removeClass("active");
		$("#dp1").css('display', 'block');
		
});





$(".addition3").on("click", function(){

        $("label.add1").addClass("active");
		$("label.sub1").removeClass("active");
		window.expType = 'a';
});

$(".addition4").on("click", function(){

		$("label.sub1").addClass("active");
		$("label.add1").removeClass("active");
		window.expType = 'c';

});

$(".addition33").on("click", function(){

		let rr = $("#base_room_rent1").val();
		let mod = $("#mod_room_rent").val();
		if($('#perc'). prop("checked") == true){
		    $("#lbl").addClass("active");
			$("#perc1").val(1);
			$("#modf_rr").val(mod * rr / 100);
				
		} else {
			$("#lbl").removeClass("active");
			$("#perc1").val(0);
			$("#modf_rr").val(mod);
		}
		

});

$(".addition34").on("click", function(){

		if($('#mperc'). prop("checked") == true){
		    $("#lbl1").addClass("active");
			$("#m_perc1").val('1');				
		}else {
			$("#lbl1").removeClass("active");
			$("#m_perc1").val('0');
		}

});




    function submit_form()
    {

        var f = $("#f");
        $.post(f.attr("action"), f.serialize(), function (result) {
            close(eval(result));
        });
        return false;

    }
   /* function download_pdf() {
        var booking_id = $('#booking_3rd').val();
		//alert(booking_id);
        //$.post("<?php echo base_url();?>bookings/pdf_generate");
		jQuery.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>bookings/folio_generate",
          dataType: 'json',
          data: {booking_id:'3521'},
          success: function(data){
			  
			 // alert(data);
            swal({
				title: "Generated Invoice ID - "+ data.invoice_id,
				text: "Your invoice is successfully generated",
				type: "success"
			},
			function(){
			 // location.reload();
			 $('#add_submit').hide();
			 $('#dwn_link').attr("disabled",false);
			});
          }
        });
        $("#dwn_link").attr("href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id);
        var f = $("#f");
        $.post(f.attr("action"), f.serialize(), function (result) {
            close(eval(result));
        });
        return false;
    }*/
	function download_pdf() {
        var booking_id = '3524';
  //alert(booking_id);
        //$.post("<?php echo base_url();?>bookings/pdf_generate");
  jQuery.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>bookings/folio_generate",
          dataType: 'json',
          data: {booking_id:booking_id},
          success: function(data){
     
    // alert(data);
            swal({
    title: "Generated Invoice ID - "+ data.invoice_id,
    text: "Your invoice is successfully generated",
    type: "success"
   },
   function(){
    // location.reload();
    $('#add_submit').hide();
    $('#dwn_link').attr("disabled",false);
   });
          }
        });
        $("#dwn_link").attr("href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id);
        var f = $("#f");
        $.post(f.attr("action"), f.serialize(), function (result) {
            close(eval(result));
        });
        return false;
    }

    function payment_mode_change()
    {
        var p = $('#t_payment_mode').val();
        //alert(p);
        if(p=='cash')
        {
            $('#bank').hide();
			
        }
        else
        {
            $('#bank').show();
        }
		
		document.getElementById('payment_amnt').style.display= 'block';
    }

    function openview() {
        var booking_id = $('#booking_3rd').val();
		//var booking_id = '3524';
        window.open("<?php echo base_url();?>bookings/invoice_generate?booking_id=" + booking_id);
        //$('#f').submit();
        var f = $("#f");
        $.post(f.attr("action"), f.serialize(), function (result) {
            close(eval(result));
        });
        return false;
        //alert(booking_id);
        //submit_form();


        /*$("#f").submit(function () {
         var f = $("#f");
         $.post(f.attr("action"), f.serialize(), function (result) {
         close(eval(result));
         });
         return false;
         });*/
    }

    function openview1() {
        //var booking_id = $('#booking_3rd').val();
        //window.open("<?php echo base_url();?>bookings/invoice_generate?booking_id=" + booking_id);
        //$('#f').submit();
		 //window.parent.document.getElementById('txd').value='0';
        var f = $("#f");
        $.post(f.attr("action"), f.serialize(), function (result) {

            $.post("<?php echo base_url();?>enquiry/hotel_backend_create5",
                $("#form4th").serialize(),
                function(data){
                    //alert(data.bookings_id);
                    $('#booking_3rd').val(data.bookings_id);
                    window.location.href="<?php echo base_url();?>enquiry/mailEnquiry/"+data.bookings_id;

                });

            //close(eval(result));
        });
        return false;
    }
    function openview2() {
        window.parent.document.getElementById('txd').value='0';
        var f = $("#f");
        $.post(f.attr("action"), f.serialize(), function (result) {

            close(eval(result));
        });
        return false;
    }
	
	function cancel_booking(){
		 window.parent.document.getElementById('txd').value='0';
		  var booking_id = $('#booking_3rd').val();
		if( booking_id > 0 ){
			//alert(booking_id);
			//return false;
			 $.ajax({
			  url: "<?php echo base_url()?>bookings/cancel_booking",
			  type: "POST",
			  data:{booking_id:booking_id},
			  success: function(data){
				//console.log(data);
			  }        
			  });				
		}
		 var f = $("#f");
        $.post(f.attr("action"), f.serialize(), function (result) {

            close(eval(result));
        });
        return false;
		
	}
	
</script> 
<script>
    function onlyNos(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        catch (err) {
            alert(err.Description);
        }
    }
    /* 11.17.2015*/
    function onlyLtrs(e, t)
    {

        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }
            if ( charCode > 32 && (charCode < 64 &&  charCode < 90)) {
                return false;
            }
            return true;
        }
        catch (err) {
            alert(err.Description);
        }
    }

    function return_broker()
    {
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_broker",
                datatype:'json',
                success:function(data)
                {
                    var resultHtml = '';
                    resultHtml+='<option value="">Select Broker</option>'
                    $.each(data, function(key,value){

                        resultHtml+='<option value="'+ value.b_id +'">'+ value.b_name +'</option>';

                    });
                    $('#broker_id').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
        return false;


    }

    function return_channel()
    {
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_channel",
                datatype:'json',
                success:function(data)
                {
                    var resultHtml = '';
                    resultHtml+='<option value="">Select Channel</option>'
                    $.each(data, function(key,value){

                        resultHtml+='<option value="'+ value.channel_id +'">'+ value.channel_name +'</option>';

                    });
                    $('#broker_id').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
        return false;


    }


    function return_guest_search()
    {
        var guest_name = $('#cust_search').val();
        //alert(guest_name);

        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_search",
                datatype:'json',
                data:{guest:guest_name},
                success:function(data)
                {

                    var resultHtml = '';
                    resultHtml+='<div class="return_guestinn"><table class="table table-striped table-bordered table-hover"><thead><tr><th>Name</th><th>Address</th><th width="100">Mobile No</th></tr></thead><tbody>';
                    $.each(data, function(key,value){
                        resultHtml+='<tr  class="crsr collapsed" data-toggle="collapse" data-target="#toggleDemo'+value.g_id+'">';
                        resultHtml+='<td>'+ value.g_name +'</td>';
                        resultHtml+='<td>'+ value.g_address +'</td>';
                        resultHtml+='<td>'+ value.g_contact_no +'</td>';
                        resultHtml+='</tr><tr>';
                        resultHtml+='<td colspan="3"  cellpadding="0" cellspacing="0" style="padding:0;"><div id="toggleDemo'+value.g_id+'" class="mrgn" style="padding:10px;">';
                        resultHtml += '<div class="clearfix"><div class="col-xs-8 cl-in"><span style="font-size:12px; margin-bottom:6px;"> No of Visits: '+value.visits+' </span>';

                        resultHtml+='<input type="hidden" id="g_id_hide"  value="'+value.g_id+'" ></input>';
                        resultHtml+='<span style="font-size:12px;">Total Transections: <i class="fa fa-inr" style="font-size: 11px;"></i> '+value.amount_spent+'</span><span style="font-size:12px;">Last visit: On '+value.cust_from_date+' at '+value.room_no+'</span><button class="btn blue btn-xs" style="margin-top:5px;" name="g_id" id="g_id"" value="'+value.g_id+'" onclick="get_guest('+value.g_id+')" >Continue</button></div><div class="col-xs-4 sm">';
                        if(value.g_photo_thumb!='') {
                            resultHtml += '<div class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/' + value.g_photo_thumb + '" > ' +
                            '</div>';
                        }
                        else{
                            resultHtml += '<div class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/no_images.png" > ' +
                            '</div>';
                        }
                        //resultHtml+='<td><input type="radio" name="g_id" id="'+value.g_id+'" value="'+value.g_id+'" onclick="get_guest(this.value)" /></td>';
                        resultHtml+='</div></div></td></tr>';
                    });
                    resultHtml+='</tbody></table></div>';
                    $('#return_guest').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
        return false;


    }

    function check_cur_date()
    {
        var start_dt = '<?php echo date('Y-m-d',strtotime($start_dt)); ?>';
        var current_dt = '<?php echo date('Y-m-d'); ?>';
        if(start_dt != current_dt)
        {
            $("input:radio").attr("checked", false);
            swal('Checkin can only be taken from Current Date');
        }
    }
	function check_temp_date()
    {
        var start_dt = '<?php echo date('Y-m-d',strtotime($start_dt)); ?>';
        var current_dt = '<?php echo date('Y-m-d'); ?>';
        if(start_dt != current_dt)
        {
            $("input:radio").attr("checked", false);
            swal('Temporary Booking can only be taken from Current Date');
        }
    }

    function check_advance_date()
    {
        var start_dt = '<?php echo date('Y-m-d',strtotime($start_dt)); ?>';
        var current_dt = '<?php echo date('Y-m-d'); ?>';
        if(start_dt == current_dt)
        {
            $("input:radio").attr("checked", false);
            swal('Advance Booking can not be taken from Current Date');
        }
    }

    function get_guest(id)
    {
        //var g_id = $('#g_id_hide').val();
        var g_id=id;
        //alert(g_id);
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_get",
                datatype:'json',
                data:{guest_id:g_id},
                success:function(data)
                {  //alert(id);
                    $('#tab11').hide();
                    $('#tab12').show();
                    $('#id_guest2').val(id);
                    $('#cust_name').val(data.g_name);
                    $('#cust_address').val(data.g_address);
                    $('#cust_contact_no').val(data.g_contact_no);
					$('#cust_mail').val(data.g_email);
                }
            });
        return false;
    }

    function get_commision()
    { var flag=document.getElementById("brokerchannel").value;
        //alert(flag);
        var b_id = $('#broker_id').val();
        var booking_id = $('#booking_1st12').val();

            if(flag==0){
                var url='get_commision';
            }
        else if(flag==1){
            var url='get_commision2';
        }
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/"+url+"",
                datatype:'json',
                data:{b_id:b_id,booking_id:booking_id},
                success:function(data)
                {
                    var room_rent = parseInt(data.base_room_rent);
                    var commision = parseInt(data.broker_commission);

                    var tot_commision = (room_rent * (commision/100));
                    $('#broker_commission').val(tot_commision);
                }
            });


        return false;
    }

</script> 
<script>
    $("#returning").hover(function() {
        //alert("nnn");
        $("#returning").css("background-color", "");
    });
</script> 
<script>



function check_sum2(value2){
		
		var diff=document.getElementById("diff").value;
		var d=parseInt(diff);
		
		document.getElementById("base_room_rent").value=value2;
		document.getElementById("dumb_rate").value = value2;
		document.getElementById("target").innerHTML=value2;
		document.getElementById("target2").innerHTML=value2*d;
		document.getElementById("base_room_rent1").value=value2;
		
			
}
	
	
    function amount_check(value){ 
        tax=0;	
		let rr = $("#base_room_rent1").val();
		let mod = $("#mod_room_rent").val();
		if($('#perc'). prop("checked") == true){
		   	$("#modf_rr").val(mod * rr / 100);
				
		} else {
			$("#modf_rr").val(mod);
		}
		
	    //alert(value);
		taxCal();
		var chk = document.getElementById("chk_mod").value;
		//swal(chk);
		var md = value;
		var radio_val = document.getElementsByName('rent_mode_type');
		var base_rent = document.getElementById("base_room_rent1").value;
		var days = parseInt($("#diff").val());
		//alert(chk);
		//console.log('1st If - '+((value >= 0) && (value != '')));
        if ((value > 0) && (value != '')){
			if ( chk != value ){		
				//console.log('Inside Second if, '+chk+' != '+value);
				var diff=document.getElementById("diff").value;
				var d=parseInt(diff);
		
			 if($('#perc'). prop("checked") == true){
			 //alert("Checkbox is checked.");
				if(value > 100 ){				
					$("#spn .error-val").css('display', 'block');
					return false;
				} else {
					var md = 0;
					document.getElementById("chk_mod").value = md;				
					value = parseInt((base_rent*value)/100);
					ttx=(value*tax/100);
					$('#target_tr').text(ttx*days);
					$("#spn span").css('display', 'none');
				}			
			 }else {
				var md = 0;
				document.getElementById("chk_mod").value = md;			
				//alert("Checkbox is unchecked.");
				value = value;
				$("#spn span").css('display', 'none');
			 }		

			for (var i = 0, length = radio_val.length; i < length; i++) {
			
				if (radio_val[i].checked) {
					
					var radio_selected = radio_val[i].value; 
					//alert(radio_selected);

					if (radio_selected == "add") 
					{
						//alert('Add Function Call' + base_rent);
						var sum_val = (parseInt(base_rent) + parseInt(value))*days;
						document.getElementById("base_room_rent1").value = base_rent;
						document.getElementById("base_room_rent").value = sum_val;
						document.getElementById("dumb_rate").value = sum_val;
						ttx=(sum_val*tax/100);
						document.getElementById("target").innerHTML = document.getElementById("base_room_rent").value+" ("+(base_rent*days)+")";
						document.getElementById("target2").innerHTML=parseInt(document.getElementById("base_room_rent").value)*d;
						//$('#target_tr').text(ttx);
						
						//alert(sum_val);
						document.cookie="rate="+document.getElementById("base_room_rent").value;	
					taxCal();
					}
					else
					{
						//alert('Substract Function Call' + base_rent);
						var sum_val = (parseInt(base_rent) - parseInt(value))*days;
						document.getElementById("base_room_rent1").value = base_rent;
						document.getElementById("base_room_rent").value = sum_val;
						document.getElementById("dumb_rate").value = sum_val;
						ttx=(sum_val*tax/100);
						document.getElementById("target").innerHTML = document.getElementById("base_room_rent").value+" ("+(base_rent*days)+")";
						document.getElementById("target2").innerHTML=parseInt(document.getElementById("base_room_rent").value)*d;
						//$('#target_tr').text(ttx);
						//alert(sum_val);
						document.cookie="rate="+document.getElementById("base_room_rent").value;	
					taxCal();	
						
					}
					break;
					
				} // End IF
			
			} // End For
			} //End Outer If
		} // End First If 
		else{
			$('#target').text(base_rent*days);
			taxCal();
			//$('#target_tr').text(base_rent*days*0.23);
		}
        /*

        var radio_val = document.getElementsByName('rent_mode_type').value();

        if (radio_val == "add") 
        {
            alert('Add Function Call');
        }
        else
        {
            alert('Substract Function Call');
        }
        
        */
		document.getElementById("chk_mod").value = md;
    } // End function amount_check
	
	
	function m_amount_check(value){ 
        tax=0;	
		
	    //alert(value);
		
		let mod = $("#mod_meal_rent").val();
		var chk = document.getElementById("chk_mod").value;
		var md = value;
		var radio_val = document.getElementsByName('m_rent_mode_type');
		let meal_rent = $("#price1").val();
		var days = parseInt($("#diff").val());
		
	if($('#mperc'). prop("checked") == true){
			alert("hello");	
		   $("#modf_mp").val(mod * meal_rent / 100);
			
		} else {
			
			$("#modf_mp").val(mod);
				
		}
        if ((value > 0) && (value != '')){
			if ( chk != value ){		
				//console.log('Inside Second if, '+chk+' != '+value);
				var diff=document.getElementById("diff").value;
				var d=parseInt(diff);
		
			 if($('#mperc'). prop("checked") == true){
			 //alert("Checkbox is checked.");
				if(value > 100 ){				
					$("#spn .error-val").css('display', 'block');
					return false;
				} else {
					var md = 0;
					document.getElementById("chk_mod").value = md;				
					value = parseInt((meal_rent*value)/100);
					ttx=(value*tax/100);
					//$('#target_tm').text(ttx*days);;
					$("#spn span").css('display', 'none');
				}			
			 }else {
				var md = 0;
				document.getElementById("chk_mod").value = md;			
				//alert("Checkbox is unchecked.");
				value = value;
				$("#spn span").css('display', 'none');
			 }		

			for (var i = 0, length = radio_val.length; i < length; i++) {
			
				if (radio_val[i].checked) {
					
					var radio_selected = radio_val[i].value; 
					//alert(radio_selected);

					if (radio_selected == "add") 
					{
						//alert('Add Function Call' + base_rent);
						var sum_val = (parseInt(meal_rent) + parseInt(value))*days;
						
						document.getElementById("price1").value = meal_rent;
						document.getElementById("price").value = sum_val;
						document.getElementById("dumb_rate").value = sum_val;
						ttx=(sum_val*tax/100);
						document.getElementById("target_mp").innerHTML = document.getElementById("price").value+" ("+(meal_rent*days)+")";
						//document.getElementById("target_tm").innerHTML=parseInt(document.getElementById("price").value)*d;
						//$('#target_tm').text(ttx);
						//alert(sum_val
						taxCal();
					}
					else
					{
						//alert('Substract Function Call' + base_rent);
						var sum_val = (parseInt(meal_rent) - parseInt(value))*days;
						document.getElementById("price1").value = meal_rent;
						document.getElementById("price").value = sum_val;
						document.getElementById("dumb_rate").value = sum_val;
						ttx=(sum_val*tax/100);
						document.getElementById("target_mp").innerHTML = document.getElementById("price").value+" ("+(meal_rent*days)+")";
						//document.getElementById("target_tm").innerHTML=parseInt(document.getElementById("price").value)*d;
						//document.getElementById("m_target_2").innerHTML=parseInt(document.getElementById("price").value)*d;
						//$('#target_tm').text(ttx);
					//	$('#m_target_2').text(ttx);
						//alert(sum_val);
						//document.cookie="rate="+document.getElementById("base_room_rent").value;	
						taxCal();
						
					}
					break;
					
				} // End IF
			
			} // End For
			} //End Outer If
		} // End First If 
		else{
			$('#target_mp').text(meal_rent*days);
			//$('#target_tm').text(meal_rent*days*0.23);
			taxCal();
		}
        /*

        var radio_val = document.getElementsByName('rent_mode_type').value();

        if (radio_val == "add") 
        {
            alert('Add Function Call');
        }
        else
        {
            alert('Substract Function Call');
        }
        
        */
		document.getElementById("chk_mod").value = md;
    }
	
	//last step calculations
	function check_sum3(tax){
		//alert(tax);
		//alert('check_sum3');
		var totRR = parseFloat($('#target').text());		
		var totExRR = parseFloat($('#target_ex').text());
		var totMP = parseFloat($('#target_mp').text());
		var totExMP = parseFloat($('#target_exmp').text());
		
		
		var room_tax= parseFloat($('#target_tr').text());	
		var meal_tax= parseFloat($('#target_tm').text());
		
		var ext_room_rent= parseFloat($('#target_extx').text());	
		var ext_meal= parseFloat($('#target_exmptx').text());	
		/*alert(room_tax);
		alert(meal_tax);
		alert(totRR);		
		alert(totMP);*/
		var sum_total=parseFloat(room_tax)+parseFloat(meal_tax)+parseFloat(totRR)+parseFloat(totMP);
//alert(sum_total);
		if(isNaN(totExRR)){
			totExRR = 0;
		}
		if(isNaN(totExMP)){
			totExMP = 0;
		}
		var tot = totRR+totExRR+totMP+totExMP;
		//console.log(totRR);
		var roomRent1 = document.getElementById("base_room_rent").value;
		var dbSlab1 = document.getElementById("dbSlab1").value;
		var dbSlab2 = document.getElementById("dbSlab2").value;
		var dbSlab3 = document.getElementById("dbSlab3").value;
		var dbSlab = document.getElementById("dbSlab").value;
		var ltax;
				
		if((parseFloat(roomRent1) >= parseInt(dbSlab1)) && (parseInt(roomRent1) <= parseInt(dbSlab2))){
			ltax = document.getElementById("dblt1").value;
			document.getElementById("luxuryt").value=ltax;
		}
		else if ((parseInt(roomRent1) >= parseInt(dbSlab3)))
		{
			ltax = document.getElementById("dblt2").value;
			document.getElementById("luxuryt").value=ltax;
		}
		else
		{
			ltax=0;
			document.getElementById("luxuryt").value=ltax;
		}
		
        if(tax =="sum"){
            var early = tax;
           	var stax1 = document.getElementById('stax').value;
			var ltax1 = document.getElementById('ltax').value;
			document.getElementById('servicet').value = stax1;
			document.getElementById('servicec').value = '0';
            tax = parseFloat(document.getElementById('servicet').value)+parseFloat(ltax);			
        }
		else if(tax == "sum2"){
        	var early = tax;
			var stax1 = document.getElementById('stax').value;
			var ltax1 = document.getElementById('ltax').value;
            var totalFixedTax =23;
            var scharge1 = parseFloat(totalFixedTax) -(parseFloat(stax1) + parseFloat(ltax));			
			scharge1=scharge1.toFixed(2);
			document.getElementById('servicet').value=stax1;
			document.getElementById('servicec').value=scharge1;			
			tax=parseFloat(document.getElementById('servicet').value)+parseFloat(document.getElementById('servicec').value)+parseFloat(ltax);
        } 
		else {
			/*document.getElementById('servicet').value = '0';
			document.getElementById('servicec').value = '0';
			document.getElementById('luxuryt').value = '0';*/
		}
		
		var diff=document.getElementById("diff").value;
		var base_rate=document.getElementById("base_room_rent").value;
		var meal_price=document.getElementById("price").value;
		var ex_price=document.getElementById("extra_charge_amount").value;
		var ex_m_price=$("#target_exmptx").text();
		
		if(isNaN(ex_m_price)){
			ex_m_price = 0.00;
		}
		if(isNaN(ex_price)){
			ex_price = 0.00;
		}

		var total_rate = parseFloat(base_rate)+( parseFloat(base_rate)*(parseFloat(tax)/100))+parseFloat(meal_price)+( parseFloat(meal_price)*(parseFloat(tax)/100));
		var total_tax = (parseFloat(base_rate)*(parseFloat(tax)/100))*parseFloat(diff);
		document.getElementById("total").value=total_rate.toFixed(2);
		
		total_rate="<i class='fa fa-inr'></i> "+parseFloat(total_rate*parseFloat(diff)).toFixed(2);
		
		//document.getElementById("target3").innerHTML=total_rate;
		
        if(early=="sum"){
            /*document.getElementById("target4").innerHTML="Room Service Tax @ "+document.getElementById('servicet').value+"%"; 
			document.getElementById("target6").innerHTML="Room Service Charge @ 0%";
			document.getElementById("target7").innerHTML="Room Luxury Tax @ "+ltax+"%";
			document.getElementById("target8").innerHTML="Food Vat @ "+'14.5'+"%";
			document.getElementById("booking_tax_type").value = 'Service tax + Service Charge';*/
        }else if(early == "sum2"){

        	/*document.getElementById("target4").innerHTML="Room Service Tax @ "+document.getElementById('servicet').value+"%"; 
			document.getElementById("target6").innerHTML="Room Service Charge @ "+document.getElementById('servicec').value+"%";
			document.getElementById("target7").innerHTML="Room Luxury Tax @ "+ltax+"%";
        	document.getElementById("target8").innerHTML="Food Vat @ "+'14.5'+"%";*/
            document.getElementById("booking_tax_type").value = 'Tax';

        } 
		else{
			/*document.getElementById("target4").innerHTML="Room Service Tax @ "+tax+"%";
			document.getElementById("target6").innerHTML="Room Service Charge @ "+tax+"%";
			document.getElementById("target7").innerHTML="Room Luxury Tax @ "+tax+"%";
			document.getElementById("target8").innerHTML="Food Vat @ "+'14.5'+"%";*/
			document.getElementById("booking_tax_type").value = 'No Tax';
		
    	}
			if(tax!=0){
			$('#target2').text(totRR.toFixed(2));
			//alert(room_tax);
			$('#ext_target_tr').text(ext_room_rent);
			$('#ext_total_m_tax').text(ext_meal);
			
			$('#target5').text(room_tax);
			$('#m_target2').text(totMP.toFixed(2));
			$('#m_targetEx').text(totExMP.toFixed(2));
			$('#total_m_tax').text(meal_tax);	
			
			
			document.getElementById("tax").value = parseFloat(total_tax).toFixed(2);
			$('#ex_target2').text(totExRR.toFixed(2));
			$('#ex_tax').text((totExRR*tax/100).toFixed(2));
			$('#exM_tax').text((totExMP*tax/100).toFixed(2));
			//$('#target3').text((tot*(tax+100)/100).toFixed(2));	
			$('#total').val((tot*(tax+100)/100).toFixed(2));	
			$('#total_m_tax1').val((totMP*tax/100).toFixed(2));	
			$('#ex_per_price').val(totExRR.toFixed(2));
			$('#ex_per_tax').val((totExRR*tax/100).toFixed(2));
			$('#ex_per_m_price').val(totExMP.toFixed(2));
			$('#ex_per_m_tax').val((totExMP*tax/100).toFixed(2));
			//$('#base_room_rent').val(totRR.toFixed(2));
			$('#tax').val((totRR*tax/100).toFixed(2));
			
		
			//$('#target3').text(sum_total);
var r1=$('#target2').text();
var m1=$('#m_target2').text();
var er=$('#ex_target2').text();
var em=$('#m_targetEx').text();

var rrt=$('#target5').text();
var mpt=$('#total_m_tax').text();

var e_room_tax= parseFloat($('#target_extx').text());	
var e_meal_tax= parseFloat($('#target_exmptx').text());
//var  room_rent_sum_total_5=parseFloat($('#target3').text());

r1=parseFloat(r1);
m1=parseFloat(m1);
er=parseFloat(er);
em=parseFloat(em);
rrt=parseFloat(rrt);
mpt=parseFloat(mpt);

$('#room_rent_total_amount_5').val(r1);
$('#room_rent_tax_amount_5').val(rrt);
$('#food_plan_price_5').val(m1);
$('#food_plan_tax_5').val(mpt);
$('#ex_per_price_5').val(er);
$('#ex_per_tax_5').val(e_room_tax);
$('#ex_per_m_price_5').val(em);
$('#ex_per_m_tax_5').val(e_meal_tax);					  
					  
					  

 $('#target3').text(r1+m1+er+em+rrt+mpt+e_room_tax+e_meal_tax);
 $('#room_rent_sum_total_5').val(r1+m1+er+em+rrt+mpt+e_room_tax+e_meal_tax);
 $('#booking_tax_types').val('Tax');
			}
	}

	
	
</script> 
<script type="text/javascript">
   function show_hiden()
{
	
	document.getElementById('hidden-div').style.display= 'block';
	document.getElementById('hide-btn').style.display= 'none';
}
   function shows_hiden()
{
	
	document.getElementById('paymm').style.display= 'block';
}

$(function() {
    $('#payment_input').keypress(function() {
        var v = document.getElementById('payment_input').value;
		v = parseInt(v);
		if (v == ''){
			document.getElementById('submit4').style.display = "none";
		}
		else{
			document.getElementById('submit4').style.display = "block";
		}
    });
	
	
	
	$('#payment_input').blur(function() {
        var v = document.getElementById('payment_input').value;
		v = parseInt(v);
		if (v == ''){
			document.getElementById('submit4').style.display = "none";
		}
		else{
			document.getElementById('submit4').style.display = "block";
		}
    });
	
});

    function master_id_hint(s){

        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/master_id_validate",
                datatype:'json',
                data:{booking_id:s},
                success:function(data)
                {
                    document.getElementById("master_id_validate").innerHTML=data.valid;
                    document.getElementById("master_id_validate_token").value=data.token;
                    document.getElementById("checkout_date_child").value=data.end_dt;
                    document.getElementById("checkout_date_child_confirmed").value=data.end_dt;
                    document.getElementById("checkout_time_child").value=data.end_time;
                    document.getElementById("g_name_child").value=data.g_name;
                    document.getElementById("g_address_child").value=data.g_address;
                    document.getElementById("g_number_child").value=data.g_number;
                }
            });




    }

    function check_date(date){
        var date_master=document.getElementById("checkout_date_child_confirmed").value;
        if(date_master<date){
            document.getElementById("checkout_date_child").value=date_master;
            swal({
                    title: "Checkout Date Greater",
                    text: "Child checkout date must be less than or equal master checkout date",
                    type: "warning"
                },
                function(){
                    //location.reload();
                });
            return false;

        }
    }


</script> 
<script type="text/javascript">
function check_occupancy2() { /* code here */

    var adult = $("#no_of_adult").val();

    var child = $("#no_of_child").val();

    var occupancy = $("#occupancy").val();

    var extra= (parseInt(adult)+parseInt(child)) - parseInt(occupancy); 

    if(parseInt(occupancy) < (parseInt(adult)+parseInt(child)) ){
      swal("occupancy limit exceeds. MAX value: "+occupancy);
     document.getElementById("no_of_adult").value="";
     document.getElementById("no_of_child").value="";
     
    

    }

    
    var adult = $("#no_of_adult").val();

    var child = $("#no_of_child").val();

    var occupancy = $("#room_bed").val();

    var extra = (parseInt(adult)+parseInt(child)) - parseInt(occupancy); 

    if(parseInt(occupancy) < (parseInt(adult)+parseInt(child)) ){
      swal("Bed limit exceeds. MAX value: "+occupancy);
	  $("#no_of_adult").val('');
			$("#no_of_child").val('');
			$("#o_id").val('');
      document.getElementById("extra_charge").style.display="block";
      //document.getElementById("no_of_adult").value="";
      //document.getElementById("no_of_child").value="";

    }else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))==1){
			$("#o_id").val(1);
		}else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))==2){
			$("#o_id").val(2);
		}else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))==3){
			$("#o_id").val(3);
		}else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))>3){
			$("#o_id").val(4);
		}

   }

   function check_occupancy() { 
		var flag = 0;
		var adult = $("#no_of_adult").val();
		var child = $("#no_of_child").val();
        var maxo = parseInt($("#occupancy").val());
		var occupancy = $("#room_bed").val();
		var extra= (parseInt(adult)+parseInt(child)) - parseInt(occupancy); 
		
		
   if((parseInt(adult)+parseInt(child)) > 0){
		if(parseInt(maxo) < (parseInt(adult)+parseInt(child))){
			swal("Max limit reached! MAX value: "+maxo);
			$("#no_of_adult").val('');
			$("#no_of_child").val('');
			$("#o_id").val('');
			$("#extra_charge_amount").val('');
			document.getElementById("extra_charge").style.display="none";
			$("#charges").hide();
			flag = 0;
		}
		else if(parseInt(occupancy) < (parseInt(adult)+parseInt(child))){
			swal("Bed limit reached! MAX value: "+occupancy);
			document.getElementById("extra_charge").style.display="block";
			flag = 0;	
			$("#charges").show();
			$("#o_id").val(occupancy);
		}	
		else{
			$("#extra_charge_amount").val('');
			document.getElementById("extra_charge").style.display="none";
			flag = 1;
			$("#charges").show();
			
		}
		
		
		if(flag === 1){
				
			if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))==1){
			$("#o_id").val(1);
			}else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))==2){
				$("#o_id").val(2);
			}else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))==3){
				$("#o_id").val(3);
			}else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))>3){
				$("#o_id").val(4);
			}
			else{
				$("#charges").hide();
			}
		}
		
		}
		else{
			$("#charges").hide();
			$("#o_id").val('');
			document.getElementById("extra_charge").style.display="none";
			$("#extra_charge_amount").val('');
		}
		
	 }

	 function set_extra_charge(val){
		//tax=23;
		var diff=document.getElementById("diff").value;
			var d=parseInt(diff);
			var ex_cno=0;
			var ex_ano=0;
		 if(val=='child'){
			 ex_cno=paxe;
			 ex_ano=0;
			window.expType = 'c';
			 var ex_ch=$('#ex_child_r').val();
			 var ex_ch_mp=$('#ex_child_m').val();
			  $('#ex_u_mprice').val(ex_ch_mp);
			 $('#ex_u_price').val(ex_ch);
			var ex_mp= $('#target_exmp').text();
			 var ex_ch=parseFloat(ex_ch);
			 
			 $('#extra_charge_amount').val(ex_ch*paxe*d);
			 $('#extra_ch_no').val(ex_cno);
			 $('#extra_ad_no').val(0);
			 $('#target_ex').text(ex_ch*paxe*d);
			 //mtx=parseInt(ex_ch)*paxe*tax/100*d;
			  mptx=parseInt(ex_ch_mp)*paxe*d;
			//mtx=parseInt(ex_ch)+parseInt(ex_ch)*tax/100;
			//$('#target_extx').text(mtx);
			$('#target_exmp').text(mptx);
			//$('#target_exmptx').text(mptx*tax/100);
			 
		 }
		 if(val=='adult'){
			  ex_ano=paxe;
			  ex_cno=0;
			  // alert(ex_ano);
			  // alert(ex_cno);
			 window.expType = 'a';
			 var ex_ad_mp=$('#ex_adult_m').val();
			 $('#ex_u_mprice').val(ex_ad_mp);
			var ex_ad=$('#ex_adult_r').val();
			 $('#ex_u_price').val(ex_ad);
			  $('#extra_ad_no').val(ex_ano);
			  $('#extra_ch_no').val(0);
			 $('#extra_charge_amount').val(ex_ad*paxe*d);
			 $('#target_ex').text(ex_ad*paxe*d);
			 mtx=parseInt(ex_ad)*paxe*tax/100*d;
			 mptx=parseInt(ex_ad_mp)*paxe*d;
			//$('#target_extx').text(mtx);
			$('#target_exmp').text(mptx);
			//$('#target_exmptx').text(mptx*tax/100);
			
		}

	 /*
	 	var adult = $("#no_of_adult").val();

		var child = $("#no_of_child").val();

		var occupancy = $("#occupancy").val();

		var extra= (parseInt(adult)+parseInt(child)) - parseInt(occupancy); 
        var selector= document.getElementById("charge_mode_type").value;
	 	var room_id=<?php echo $room_id ?>;

	 		var base_rent = document.getElementById("dumb_rate").value;
			

	 	//alert(room_id+selector);
	 	
	 		$.ajax({
			type:"POST",
			url: "<?php echo base_url()?>dashboard/get_booking_extra_charge",
			data:{selector:selector, room_id:room_id},
			success:function(data)
			{
				
			document.getElementById("extra_charge_modifier").style.display="block";
			//document.getElementById("extra_charge_amount").value=parseInt(data)*extra;
			var sum_val = parseInt(base_rent) + (parseInt(data)*extra);
            document.getElementById("base_room_rent").value = sum_val;
			//document.getElementById("target").innerHTML=document.getElementById("base_room_rent").value;
			document.getElementById("target2").innerHTML=parseInt(document.getElementById("base_room_rent").value)*d;
					
			
			}
		});*/
		
		taxCal();
	 }

function charge_modifier(value){
        window.tax=23;
	 	/*var adult = $("#no_of_adult").val();
		var child = $("#no_of_child").val();
		var occupancy = $("#occupancy").val();
		var extra= (parseInt(adult)+parseInt(child)) - parseInt(occupancy); 

	 	var selector= document.getElementById("charge_mode_type").value;
	 	var room_id=<?php echo $room_id ?>;

	 		var base_rent = document.getElementById("dumb_rate").value;
			var diff=document.getElementById("diff").value;
			var d=parseInt(diff);

			var sum_val = parseInt(base_rent) + parseInt(value);*/
            // document.getElementById("base_room_rent").value = sum_val;
			//document.getElementById("target").innerHTML=document.getElementById("base_room_rent").value;
			//document.getElementById("target2").innerHTML=parseInt(document.getElementById("base_room_rent").value)*d;
}
	 
</script> 
<script>
function make_date_readable(a)
{
	var b= $('#'+a+'').val();
	//alert (b);
	$('#'+a+'').removeAttr('readonly');
	//$('#'+a+'').addClass(' date date-picker');
}

function fetch_all_address(){
	var pin_code = document.getElementById('cust_address').value;
    if(pin_code != ""){
		jQuery.ajax(
		{
			type: "POST",
			url: "<?php echo base_url(); ?>bookings/fetch_address",
			dataType: 'json',
			data: {pincode: pin_code},
			success: function(data){
				var full_address = (data.city+","+data.state+","+data.country);
				//alert (full_address);
				$('#cust_full_address').val(full_address);
				//alert ($('#cust_full_address').val());
			}
		});
	}
}

function prev11(){
        $('#tab11').hide();
        $('#tab1').show();
    }
	
function prev12(){
        $('#tab3').hide();
        $('#tab2').show();
}
</script> 
<script>
function change_stay_span(a)
{
			var cid = a.split("-");
			var cod = $('#end_dt').val().split("-");
			
			var check_in_date = new Date(cid[2],cid[1],cid[0]);
            var check_out_date = new Date(cod[2],cod[1],cod[0]);
		    var stay_days = (check_out_date.getTime()- check_in_date.getTime())/(1000*3600*24);
			//alert(stay_days);
			 $('#diff').val(stay_days);
	
}


function check_booking(val){
	var start_dt=$('#start_dt').val();
	var end_dt=$('#end_dt').val();
	var chkout_time=$('#end_time').val();
		
			jQuery.ajax(
		{
			type: "POST",
			url: "<?php echo base_url(); ?>bookings/check_booking",
			dataType: 'json',
			data: {start_dt:start_dt,end_dt:end_dt,chkin_time:val,chkout_time:chkout_time},
			success: function(data){
				if(data.data==1){
					swal("Booking already Exists!");
				}else{
					
				}
				
			}
		});
}
//$( document ).ready(function() {
	//var val = '1500.00';
    //check_sum2(val);
//});


function get_season(){
	var start_dt=$('#start_dt').val();
	var end_dt=$('#end_dt').val();
	jQuery.ajax(
		{
			type: "POST",
			url: "<?php echo base_url(); ?>rate_plan/get_season",
			data: {start_dt:start_dt,end_dt:end_dt},
			success: function(data){
				$('#season_id').val(data);
			}
		});
}

function get_meal_plan(){
	var bc = parseInt($("#booking_source").val());
	var adult = parseInt($("#no_of_adult").val());
	var child = parseInt($("#no_of_child").val());
	if(!isNaN(child) && !isNaN(adult) && !isNaN(bc) && (adult+child)>0){
		//$('#plan_id').removeAttr('disabled');
	//	$("#plan_id").prop("disabled", false);
		$("#charges").show();
		//console.log('(!isNaN(child) && !isNaN(adult)) TRUE');
	}
	else{
	//	$("#plan_id").prop("disabled", true);
		$("#charges").hide();
		//console.log('(!isNaN(child) && !isNaN(adult)) FALSE');
			
	}		
}

$( "#no_of_adult, #no_of_child" ).change(function() {
	var rmCh = $('#booking_rent').val();
	
	check_occupancy();
	if(rmCh != undefined || rmCh != null){
		get_meal_plan();
		get_rmCh();
	}	
});

$( "#booking_source, #booking_rent" ).change(function() {
  var mplan = $('#plan_id').val();
    //alert(mplan);
  if((mplan != undefined) && (mplan != '')){
	  get_meal_plan();
	  get_rmCh();
	  //console.log('mplan != undefined');
  }
});

$( "#booking_rent" ).change(function() {
  var mplan = $('#plan_id').val();
    //alert(mplan);
  if((mplan != undefined) && (mplan != '')){
	  get_meal_plan();
	  get_rmCh();
	  //console.log('mplan != undefined');
  }
});

function popLast(){
	var totRent = parseFloat($('#target').val());
	console.log('totRent = '+totRent);	
	
	parseFloat($('#target2').val(totRent));
	
} // End function popLast

function get_rmCh(callback){
	
	get_season();
	$("#mod_room_rent").val('');
	var rmCh = $('#booking_rent').val();
	var mplan = $('#plan_id').val();
	var bc = parseInt($("#booking_source").val());
	var s_id = parseInt($("#season_id").val());
	var u_id = parseInt($("#u_type_id").val());
	var o_id = parseInt($("#o_id").val());
	var days = parseInt($("#diff").val());
	//swal('Type - '+rmCh+' | Season - '+s_id+' | Unit Type - '+u_id+' | Occupancy - '+o_id+' | Source - '+bc+' | Meal - '+mplan);
	var adult = parseInt($("#no_of_adult").val());
	var child = parseInt($("#no_of_child").val());
	var defo = parseInt($("#room_bed").val());
	var maxo = parseInt($("#occupancy").val());
	var pax = adult + child;
	 window.paxe = 0;
	var rr;
	var mr;
	var ar;
	var cr;
	
//	alert(defo+' - '+maxo);
	if(pax>defo){
		paxe = pax - defo;
	}
	//alert(paxe);
	if(!isNaN(child) && !isNaN(adult) && (pax > 0) &&(rmCh!="")){
	//alert('hello');
	 jQuery.ajax(
		{
			type: "POST",
			url: "<?php echo base_url(); ?>rate_plan/get_rmCh",
			data: {
				chType:rmCh,
				mplan:mplan,
				source:bc,
				season:s_id,
				unitType:u_id,
				pax:o_id
			},
			success: function(data){
				
				//alert(data);
			//	alert('asd'+data.room_charge);
				$("#base_room_rent").val(data.room_charge);
				$("#base_room_rent1").val(data.room_charge);
				$("#unit_room_rent").val(data.room_charge);
				$("#price").val(data.meal_charge);
				$("#price1").val(data.meal_charge);
				$("#ex_child_r").val(data.room_charge_ec);
				$("#ex_adult_r").val(data.room_charge_ea);	
				$("#ex_adult_m").val(data.meal_charge_ea);	
				$("#ex_child_m").val(data.meal_charge_ec);	
				rr = $("#base_room_rent").val();
				mr = $("#price").val();
				ar = $("#ex_adult_r").val();
				arm = $("#ex_adult_m").val();
				cr = $("#ex_child_r").val();
				crm = $("#ex_child_m").val();
				//alert('Room Rent '+days*rr);
				$("#target").text(days*rr);		
				//$("#target").text(rr);		
				$("#target_mp").text(days*mr);
				$("#price2").val(days*mr);
				var tt=parseFloat(data.room_charge);
			 
				var tm=parseFloat(data.meal_charge);
				

				if(paxe>0){
					if(expType === 'c'){
						//alert('ggggggggggggg'+paxe*cr);
						$("#target_ex").text(paxe*cr*days);
						//$("#target_extx").text(paxe*cr*days*0.23);
						$("#target_exmp").text(paxe*crm*days);
						//$("#target_exmptx").text(paxe*crm*days*23/100);
						//console.log('crm = '+crm);
					}
					else if(expType === 'a'){
						//$("#target_ex").text(paxe*ar*days);						
					//	$("#target_exmp").text(paxe*arm*days);
						
					}
				}
				else{
					$("#target_ex").text('0');
					$("#target_extx").text('0');
					$("#target_exmp").text('0');
					$("#target_exmptx").text('0');
				}
				
				if(callback && typeof(callback) === 'function'){
					callback();
					//alert('callback');
				}
				
			} // end ajax success
		}); // end ajax
	}
	else{
		swal('Please enter the PAX Properly!');
	}	
	
	//let rmchPromise = new Promise();
	
	/*setTimeout(function() 
    { 
        taxCal();
    }, 1000);*/
	
	
}	// END function get_rmCh

/*get_rmCh().then(function(){
	taxCal();
	alert('Promise');
});*/
var taxMarge = new Array();
var obj = {};

var flgR=0;
	var flgM=0;
function taxCal(){
	
	let roomRent = parseFloat($('#target').text());
	let ex_roomRent = 0;
	let ex_mealCharge =0;
	
	ex_roomRent = parseFloat($('#target_ex').text());
	if(ex_roomRent == ''){ex_roomRent=0;}
	
	let mealCharge = parseFloat($('#target_mp').text());
	ex_mealCharge = parseFloat($('#target_exmp').text());
	if(ex_mealCharge == ''){ex_roomRent=0;}
	
	let total_roomRent=parseFloat(roomRent)+parseFloat(ex_roomRent);
	let total_meal=parseFloat(mealCharge)+parseFloat(ex_mealCharge);
	
	get_tax('Booking',roomRent,0);	
	get_tax('Meal Plan',mealCharge,0);
	
	get_tax('Booking',ex_roomRent,1);
	get_tax('Meal Plan',ex_mealCharge,1);
	
	
	
	get_tax('Booking',total_roomRent,2);
	get_tax('Meal Plan',total_meal,2);

	
	
}
var mp = [];
var rr = [];
var hhh=1;
var ppptax = [];
var abc=0;
function get_tax(type1,amt,ex){
	//alert('get_tax('+type1+' '+ex+')');
	//alert(abc);
	var target;
	var tttt=0;
	target=amt;
	var startDate=$('#startDate').val();
	var tdays = parseInt($("#diff").val());
	var price=parseFloat(amt);
	if(tdays){tdays;}else{tdays=1;}
	price=price/tdays;
	
	//console.log('show tax plan1 '+price);
	$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>dashboard/showtaxplan1",
    data:{price:price,args:'a',type1:type1,dates:startDate},
    success: function(data){
		if(data)
		{
			//alert(data.total);//return false;
			var data1=data;
			var tax_response=data;
			var tax_rule_id=data.r_id;
			var tax_total=data.total;
			var k=[];
			
			$.each(data, function(key, value) {
		
				if(key !='total' && key!='r_id' && key!='planName'){
					//k[key]=value;
					//alert(key+'='+value);
					var va=parseFloat(tdays)*parseFloat(value);
					

					if((type1=='Meal Plan' && ex==1)  || (type1=='Booking' && ex==1) ||(type1=='Meal Plan' && ex==0)  || (type1=='Booking' && ex==0)){
						////$("#tax_details_area").append('<label id="target4" style="display:block;">'+key+':'+va+'</label>');
					if(type1 == 'Booking'){
						if(!(key in rr)){
							rr[key]= va;
						} else {
							rr[key]= rr[key] + va;
						}
					} else {
						if(!(key in mp)){
							mp[key]= va;
						} else {
							mp[key]= mp[key] + va;
						}						
					}	
					}
					//console.log(rr);
					//console.log(mp);
	
				}
			
			//-----------------
			/*$.each(data, function( key1, value1 ) {
				if(kay1==kay){}
				
			});*/
		   //-----------------
	  
	});
				for (var key2 in rr){
					if (typeof rr[key2] !== 'function') {
						 // $("#tax_details_area").append('<label id="target4" style="display:block;">'+key2+':'+rr[key2]+'</label>');
					}
				}	
				for (var key1 in mp){
					if (typeof mp[key1] !== 'function') {
						  //$("#tax_details_area").append('<label id="target4" style="display:block;">'+key1+':'+mp[key1]+'</label>');
					}
				}

	//$("#tax_details_area").html(a);
	//alert('total amount ='+amt+'total tax'+tax_total);
	
	if(type1 == 'Meal Plan'){	
		if(ex == 1){
			tax_total=parseFloat(tdays)*parseFloat(tax_total);
			$('#target_exmptx').text(tax_total);
		}
		else if(ex == 0)
		{
			tax_total=parseFloat(tdays)*parseFloat(tax_total);
			$('#target_tm').text(tax_total);
		}
		
		
		$('#total_m_tax').text(tax_total);
	}
	else {
		if(ex == 0){
			tax_total=parseFloat(tdays)*parseFloat(tax_total);
			$('#target_tr').text(tax_total);
		}
		else if(ex == 1)
		{
			var aa=$('#target_ex').text();
			if(aa>0){
		tax_total=parseFloat(tdays)*parseFloat(tax_total);
			$('#target_extx').text(tax_total);
}else{
tax_total=0;
			$('#target_extx').text('0');
}
		}
		
		
	}
	
		 
	}
		var stringData = JSON.stringify(data);
		//alert(type1+' - '+stringData);
		 
		if((ex==2 || roomRent) && (ex==2 || mealCharge)){
			
				if(type1=='Booking'){
					$('#response').val(stringData);									
					$('#BookingResponse').val(stringData);
					$('#rr_total_tax').val(tax_total*tdays);
					
					
							
				}
				
				if(type1=='Meal Plan'){
					//alert('meal'+stringData);
					$('#mealResponse').val(stringData);
					$('#mp_total_tax').val(tax_total*tdays);
			
				}
				
				if(type1=='Booking' || type1=='Meal Plan'){
				delete data['total'];
				delete data['r_id'];
				delete data['planName'];
				
				
				
				
		}
		if(flgR ==0 &&  type1=='Booking'){
		$.each(data, function(key, value) {
			
			$("#tax_details_area").append('<label id="target4" style="display:block;">'+key+':'+'<span class="room0">'+value*tdays+'</span></label>');
			abc=parseFloat(abc)+value;
			
			});
		flgR=flgR+1;	
		ppptax[key]=value*tdays;
		
		}
		if(flgM ==0 &&  type1=='Meal Plan'){
		$.each(data, function(key, value) {
			$("#tax_details_area").append('<label id="target4" style="display:block;">'+key+':'+'<span class="meal0">'+value*tdays+'</span></label>');
			abc=parseFloat(abc)+value;
			});
		flgM=flgM+1;	
		ppptax[key]=value*tdays;
		
		}
		
		
		
    }

		
		
		
		
		
		
		
		hhh=hhh+1;
		//else{
			//$("#tax_details_area").html('<label id="target4" style="display:block;"></label>');
		//}
		
		
		}
	// End Success
    }); // End Ajax
	            for (var key2 in rr){
					if (typeof rr[key2] !== 'function') {
						  //$("#tax_details_area").append('<label id="target4" style="display:block;">'+key2+':'+rr[key2]+'</label>');
					}
				}	
				for (var key1 in mp){
					if (typeof mp[key1] !== 'function') {
						  //$("#tax_details_area").append('<label id="target4" style="display:block;">'+key1+':'+mp[key1]+'</label>');
					}
				}
} // function get_tax

function formatDate(dt) {
	
	var at = dt.split("-");
    var month = at[0];
    var day = at[1];
    var year = at[2];

    //if (month.length < 2) month = '0' + month;
    //if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
}

function dateformat_increase(date,co) {
	//alert('this is date'+date);
    var d = new Date(date),
        month = '' + (d.getMonth()+1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
//day_sta=parseInt(a);
day=parseInt(day);
day=day+parseInt(co);
day=day.toString();
if (day.length < 2) day = '0' + day;
    return [ year,  month,day ].join('-');
}
function parseDate(str) {
    var mdy = str.split('-')
    return new Date(mdy[2], mdy[0]-1, mdy[1]);
}


function parseDate1(str) {
    var mdy = str.split('-')
    return new Date(mdy[0]-1, mdy[2],  mdy[1]);
}




function bookingChargeLineItem(){
	
	//alert('bookingChargeLineItem'); 
	//var tax=$('#booking_tax').val();
	var tax=$('#tax_track').val();
	//alert(tax);
	var barray=[];
	var marray=[];
	var startDate=$('#start_dt').val();
	var startDate_increase=$('#start_dt').val();
	var endDate=$('#end_dt').val();

startDate=formatDate(startDate);
endDate=formatDate(endDate);

 startDate = parseDate(startDate);
  endDate = parseDate(endDate);


 var diff  = new Date(endDate - startDate);
    var days  = diff/1000/60/60/24;
//alert('total  '+days);
  days=parseFloat(days);
	
	var room_rent=parseFloat($('#target').text());
	var extra_parsen_rom_rent=parseFloat($('#target_ex').text());
	var meal_plan=parseFloat($('#target_mp').text());
	var extra_meal_plan=parseFloat($('#target_exmp').text());
	
	if(tax!='0'){
	//var room_rent_tax=parseFloat($('#target_tr').text());
	var room_rent_tax=parseFloat($('#rr_total_tax').val());
	var extra_parsen_rom_rent_tax=parseFloat($('#target_extx').text());
	//var meal_plan_tax=parseFloat($('#target_tm').text());
	var meal_plan_tax=parseFloat($('#mp_total_tax').val());
	var extra_meal_plan_tax=parseFloat($('#target_exmptx').text());
	var BookingResponse=$('#BookingResponse').val();
	var mealResponse=$('#mealResponse').val();
	}else{
	var room_rent_tax=0;
	var extra_parsen_rom_rent_tax=0;
	var meal_plan_tax=0;
	var extra_meal_plan_tax=0;
	
	var BookingResponse=0;
	var mealResponse=0;
	
	/*
	var BookingResponse=$('#BookingResponse').val();
	var mealResponse=$('#mealResponse').val();
	var bjson = JSON.parse(BookingResponse);
	var mjson = JSON.parse(mealResponse);
	alert(bjson);
	alert(mjson);
	console.log(bjson);
	$.each(bjson, function( index, value ) {
		
		alert( bjson[index] + ":" + 0 );
		bjson[index] = 0;
		alert("New value"+value);
		//barray[]=index;
});*/





	}
	var booking_id=parseFloat($('#booking_1st').val());
	//var response=$('#response').val();
	
	var i=0;
	room_rent=room_rent/days;
	//alert('room rent'+room_rent);
	
	room_rent_tax=room_rent_tax/days;
	//alert('room_rent_tax'+room_rent_tax);
	
	extra_parsen_rom_rent=extra_parsen_rom_rent/days;
	//alert('extra_parsen_rom_rent'+extra_parsen_rom_rent);
	
	extra_parsen_rom_rent_tax=extra_parsen_rom_rent_tax/days;
	//alert('extra_parsen_rom_rent_tax'+extra_parsen_rom_rent_tax);
	
	meal_plan=meal_plan/days;
	//alert('meal plan'+meal_plan);
	
	meal_plan_tax=meal_plan_tax/days;
	//alert('meal plan tax'+meal_plan_tax);
	
	extra_meal_plan=extra_meal_plan/days;
	//alert('extra meal plan'+extra_meal_plan);
	
	extra_meal_plan_tax=extra_meal_plan_tax/days;
	//alert('extra meal plan tax'+extra_meal_plan_tax);
	
	for(i=0;i<days;i++){
for_a=formatDate(startDate_increase);

for_a=parseDate(for_a);
date11=dateformat_increase(for_a,i);
//alert(room_rent);
	 jQuery.ajax(
		{
			type: "POST",
			url: "<?php echo base_url(); ?>bookings/bookingChargeLineItem_insert",
			data: {
				date11:date11,
				booking_id:booking_id,
				room_rent:room_rent,
				room_rent_tax:room_rent_tax,
				
				extra_parsen_rom_rent:extra_parsen_rom_rent,
				extra_parsen_rom_rent_tax:extra_parsen_rom_rent_tax,
				
				meal_plan:meal_plan,
				meal_plan_tax:meal_plan_tax,
				
				extra_meal_plan:extra_meal_plan,
				extra_meal_plan_tax:extra_meal_plan_tax,
				//response:response
				BookingResponse:BookingResponse,
				mealResponse:mealResponse
				
				
			},
			success: function(data){
				
				//alert(data.data);
			} // end ajax success
		}); // end ajax

}
	
	
} // End function bookingChargeLineItem

</script>

<script> //typehead 
var aa;
var mtax;
var rm;
var rrt;
var mpt;
var ex_rr;
var ex_mp;

var room_rent_total_amount_5;
var room_rent_tax_amount_5;
var food_plan_price_5;
var food_plan_tax_5;
var ex_per_price_5;
var ex_per_tax_5;
var ex_per_m_price_5;
var ex_per_m_tax_5;
var room_rent_sum_total_5;
function noTax(v){
	//alert(v);
var r1=$('#target2').text();
var m1=$('#m_target2').text();
var er=$('#ex_target2').text();
var em=$('#m_targetEx').text();



/* alert(ex_rr);
 alert(ex_mp);
 alert(ex_rr);
 alert(ex_rr);*/
 
r1=parseFloat(r1);
m1=parseFloat(m1);
er=parseFloat(er);
em=parseFloat(em);
rrt=parseFloat(rrt);
//mpt=parseFloat(mpt);
 mpt=parseFloat($('#target5').text());
if(v=="notax"){
	aa = $("#tax_details_area").html();
	
	rrt=parseFloat($('#total_m_tax').text());

	rm=parseFloat($('#target5').text());
	mtax=parseFloat($('#total_m_tax').text());
	ex_rr=parseFloat($('#ext_target_tr').text());
	ex_mp=parseFloat($('#ext_total_m_tax').text());
	
//HIDDEN TEXT FIELD	
room_rent_total_amount_5=$('#room_rent_total_amount_5').val();
room_rent_tax_amount_5=$('#room_rent_tax_amount_5').val();
food_plan_price_5=$('#food_plan_price_5').val();
food_plan_tax_5=$('#food_plan_tax_5').val();
ex_per_price_5=$('#ex_per_price_5').val();
ex_per_tax_5=$('#ex_per_tax_5').val();
ex_per_m_price_5=$('#ex_per_m_price_5').val();
ex_per_m_tax_5=$('#ex_per_m_tax_5').val();
//END HIDDEN TEXT FIELD	
	//alert(aa);
var f=$('#room0').text();
$('.room0').text(0);
$('.meal0').text(0);
$('#tax_track').val(0);
$('#total_m_tax').text(0);
$('#target5').text(0);
$('#ext_target_tr').text(0);
$('#ext_total_m_tax').text(0);


//hidden field set 0 for no tax
//$('#room_rent_total_amount_5').val(0);
$('#room_rent_tax_amount_5').val(0);
//$('#food_plan_price_5').val(0);
$('#food_plan_tax_5').val(0);
//$('#ex_per_price_5').val(0);
$('#ex_per_tax_5').val(0);
//$('#ex_per_m_price_5').val(0);
$('#ex_per_m_tax_5').val(0);	

//end hidden



 $('#target3').text(r1+m1+er+em);
 $('#room_rent_sum_total_5').val(r1+m1+er+em);
 $('#booking_tax_types').val('No Tax');
 
}
else{
	
	//alert(rm);
	  $("#tax_details_area").html(aa);
	$('#ext_target_tr').text(ex_rr);
	$('#ext_total_m_tax').text(ex_mp);
	
	$('#total_m_tax').text(mtax);
	$('#target5').text(rm);
	$('#tax_track').val(1);
	
	//hidden field set value for no tax
//$('#room_rent_total_amount_5').val(0);
$('#room_rent_tax_amount_5').val(room_rent_tax_amount_5);
//$('#food_plan_price_5').val(0);
$('#food_plan_tax_5').val(food_plan_tax_5);
//$('#ex_per_price_5').val(0);
$('#ex_per_tax_5').val(ex_per_tax_5);
//$('#ex_per_m_price_5').val(0);
$('#ex_per_m_tax_5').val(ex_per_m_tax_5);	

//end hidden
	
$('#target3').text(r1+m1+er+em+rm+mtax+ex_rr+ex_mp);//+rrt+mpt+ex_rr+ex_mp
$('#room_rent_sum_total_5').val(r1+m1+er+em+rm+mtax+ex_rr+ex_mp);//+rrt+mpt+ex_rr+ex_mp
 $('#booking_tax_types').val('Tax');

}
}

$.typeahead({
    input: '.js-typeahead-user_v1',
    minLength: 1,
    order: "asc",
    dynamic: true,
    delay: 200,
    backdrop: {
        "background-color": "#fff"
    },
   
    source: {
       project: {
            display: "name",
            ajax: [{
                type: "GET",
                url: "<?php echo base_url();?>dashboard/test_typehead",
                data: {
                    q: "{{query}}"
                }
            }, "data.name"],
            template: '<div class="clearfix">' +
                '<div class="project-img">' +
                    '<img class="img-responsive" src="{{image}}">' +
                '</div>' +
                '<div class="project-information">' +
                    '<span class="pro-name" style="font-size:15px;"> {{name}}</span><span><i class="fa fa-phone"></i> <strong>Contact no:</strong> {{ph_no}} </span><span><i class="fa fa-envelope"></i> <strong>Email:</strong> {{email}}</span></span>' +
                '</span>' +
				'</div>' +
            '</div>'
        }
    },
    callback: {
        onClick: function (node, a, item, event) {
         // alert(JSON.stringify(item));
		 
			$("#cust_address").val(item.pincode);
			$("#g_address_child").val(item.address);
			$("#cust_contact_no").val(item.ph_no);
			$("#cust_mail").val(item.email);
			$("#cust_name").val(item.name);
			$('#id_guest2').val(item.id);
		 
        },
        onSendRequest: function (node, query) {
            console.log('request is sent')
			
        },
        onReceiveRequest: function (node, query) {
            //console.log('request is received')
			//alert("defesddsfdsfs");
			if(query!=''){
				$("#cust_name").val('');
				$("#cust_address").val('');
			$("#g_address_child").val('');
			$("#cust_contact_no").val('');
			$("#cust_mail").val('');
			$('#id_guest2').val('');
			}
        },
         onCancel: function (node, query) {
            //console.log('request is received')
			//alert("defesddsfdsfs");
			if(query!=''){
				$("#cust_name").val('');
				$("#cust_address").val('');
			$("#g_address_child").val('');
			$("#cust_contact_no").val('');
			$("#cust_mail").val('');
			$('#id_guest2').val('');
			}
        }		
    },
    debug: true
});

	
function check_email(){
	 var a=$('#email').val();
	 var v1 = a.indexOf("@");
	 //alert(v1);
	 var v2 = a.indexOf(".");
	 //alert(v2);
	 var v3=a.length;
	 if((v1<v2)&&(v2-v1)<12 && (v3-v2)<=5){
	 }
	 else{
		 $('#email').val("");
	 }
}
</script>