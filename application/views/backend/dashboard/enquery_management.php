<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet bordered light">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>
      <?php if(isset($stat) && $stat ){echo $stat;}else{echo "List of All Unit Type's";}?>
    </div>
    <div class="actions">
    	<a class="btn btn-circle green btn-outline btn-sm" data-toggle="modal" href="#responsive"><i class="fa fa-plus"></i>Enquiry Management  </a>
    	<button class="btn btn-circle green btn-outline btn-sm" id="status123" onclick="enquery_management()"><span id="demo">Active Enquiry</span></button>
    </div>
  </div>
  <div class="portlet-body">    
    <div id="table1">
      <table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
            <th> Id </th>
            <th>Status</th>
            <th align="left" width="18%"> Guest Details</th>
            <!--<th> Guest type </th>-->
            <th align="left" width="25%"> Guest Contact Details</th>
            <th align="left" width="18%">Estd Date Range</th>
            <!--<th> To date </th>-->
            <th>Room Type</th>
            <th>No Of Rooms</th>
            <th>Adult</th>
            <th>Child</th>
            <th>Importance</th>
            <th>Preference</th>
            <th>Comment</th>
            <th>Marketing Executive</th>
            <th>Nature of Visit</th>
            <th>Booking Source</th>
            <th>Profit Center</th>
            <th> Action</th>
          </tr>
        </thead>
        <tbody>
          <?php if(isset($data) && $data):
					  foreach($data as $gst):
						  
						  ?>
          <tr id="row_<?php echo $gst->id;?>">
            <td align="center"><?php // #
					echo $gst->id; 
				?></td>
            <?php if($gst->status=='1'){$a='#4a638a; color:#a0f3eb;';} else if($gst->status=='2'){$a='#38ba93; color:#85f3b4;';} else if($gst->status=='2'){$a='#38b000; color:#ffffff;';} else {$a='#38b000; color:#fdgasd;';} ?>
            <td align="center"><span class="label" style="background-color:<?php echo $a;?>" >
              <?php if($gst->status=='1'){
				  echo 'Open';?>
              <?php }else echo 'Close'; ?>
              </span></td>
            <td align="left"><?php // Guest Details
				  $guest = $this->dashboard_model->get_guest_details($gst->guestId);
				  if($guest){
				  foreach($guest as $guest){
						//echo $guest->g_name;
					
					
					echo '<strong>'.$guest->g_name.'</strong><br>';
					//echo '<br>'.$guest->g_type;
					if($guest->g_type == '' && $guest->g_type == NULL){
						echo '<span style="color:#AAAAAA;">No info</span>';
					}
					else{ // If The guest type is not blank					
						 
						if($guest->g_type=="Corporate"){
												
								$c_id=$guest->corporate_id;
								$c_name=$this->dashboard_model->get_company_name($c_id);
								echo '<span style="color:#7A2A8C;">'.$guest->g_type.'</span>';					
							
						  	
							echo "<br>";
							if($c_name!=''){
								echo "(".$c_name->name." - ".$guest->c_des.")";
							  
							}
						}
						else if($guest->g_type=="Military"){
							echo '<span style="color:#587A04;">'.$guest->g_type.'</span>';
							  if($guest->g_type=="Military" && $guest->retired=="yes"){	
							  echo "<br>";
							  echo "(".$guest->rank." - Retired)";							  
							 }
							 
							  else if($guest->g_type=="Military" && $guest->retired=="no"){		 
							  echo "<br>";
							  echo "(".$guest->rank." - In Service)";						  
							 }
						}						

						else if($guest->g_type=="VIP" ){						 
							  echo '<i style="color:#FFC20E;" class="fa fa-star" aria-hidden="true"> </i><span style="color:#FFC20E;">'.$guest->g_type.'</span>';
							  echo "<br>";
							  echo "(".$guest->type." - ".$guest->class. ")";						  
						}				
						else if($guest->g_type=="Family"){						
							  echo '<span style="color:#4E85C5;">'.$guest->g_type.'</span>';
							  echo "<br>";
							  if($guest->f_size>0)
								echo "(size - ".$guest->f_size.")";
						}
						else if($guest->g_type=="Government_official"){
						  echo '<span style="color:#DB665A;">Government Official</span>';
						  echo "<br>";
						  echo "(".$guest->dep." - ".$guest->c_des.")";					  
						}
						else
							echo '<span style="color:#7A2A8C;">'.$guest->g_type.'</span>';
					}
				  }
				?></td>
            <td><?php  // Guest Contact details
					$fg = 10;
					echo '<i style"color:#AAAAAA;" class="fa fa-home" aria-hidden="true"></i>  &nbsp;';
							/*if(isset($guest->g_address) && ($guest->g_address != '' && $guest->g_address != ' ')){
								echo ($guest->g_address);
								$fg = 0	;
								//echo ', ';
							}*/ //Uncomment this section to show the address
							if(isset($guest->g_city) && ($guest->g_city != '' && $guest->g_city != ' ')){
								if($fg == 0){
									echo ', ';
									
								}
									
								echo $guest->g_city;
								echo ', ';
								$fg = 1;
							}
							else if($fg == 0)
								$fg = 5;
							if(isset($guest->g_state) && ($guest->g_state != '' && $guest->g_state != ' ')){
								if($fg == 5)
									echo '<br>';
								echo $guest->g_state;
								$fg = 2;
								//echo ', ';
							}
							if(isset($guest->g_pincode) && $guest->g_pincode!= NULL){
								if($fg == 2 || $fg == 5){
									echo ', ';
								}									
								echo 'Postal Code - '.$guest->g_pincode;
								$fg = 3;
							}
							if(isset($guest->g_country) && $guest->g_country != '' && $guest->g_country != ' '){
								if($fg == 3)
									echo ', ';
								echo $guest->g_country;
								
								$fg = 4;
							}
							if($fg == 10){
								echo '<span style="color:#AAAAAA;">No info</span>';
							}
					echo '<br><i style"color:#AAAAAA;" class="fa fa-phone" aria-hidden="true"></i>  &nbsp;';
					if($guest->g_contact_no != NULL){
						echo $guest->g_contact_no;
					}
					else
						echo '<span style="color:#AAAAAA;">No info</span>';
					echo '<br>';
					// Email
					echo '<i style"color:#AAAAAA;" class="fa fa-envelope" aria-hidden="true"></i>  &nbsp;';
					if($guest->g_email != NULL){
						echo $guest->g_email;
					}
					else
						echo '<span style="color:#AAAAAA;">No info</span>';
					
				  }
				?></td>
            <!--<td align="center"><?php echo $gst->guest_type; ?></td>-->
            <td align="left"><?php 
					$createDate1 = new DateTime($gst->form_date);
					$createDate2 = new DateTime($gst->to_date);
					$strip1 = $createDate1->format('d M y');
					$strip2 = $createDate2->format('d M y');
					echo '<strong style="color:#666666;">From - </strong>'.$strip1.'<br>'; 
					echo '<strong style="color:#666666;">To - </strong>'.$strip2;
				?></td>
            <!--<td align="center"><?php echo $gst->to_date; ?></td>-->
            <td align="center"><?php echo $gst->room_type; ?></td>
            <td align="center"><?php echo $gst->room_no; ?></td>
            <td align="center"><?php echo $gst->adult_no; ?></td>
            <td align="center"><?php echo $gst->child_no;?></td>
            <?php if($gst->importance=='4'){$i='#4a638a; color:#a0f3eb;';} else if($gst->importance=='3'){$i='#38ba93; color:#85f3b4';}else if($gst->importance=='2'){$i='#438368; color:#aec1e6;';} else{{$i='#438000; color:#aec1e6;';}}?>
            <td align="center"><span class="label" style="background-color:<?php echo $i;?>" >
              <?php if($gst->importance=='4'){echo "Critical";} else if($gst->importance=='3'){echo 'High';}else if($gst->importance=='2'){echo 'Med';} else{echo 'Low';}?>
              </span></td>
            <td align="center"><?php echo $gst->preference; ?></td>
            <td align="center"><?php echo $gst->comment; ?></td>
            <?php 
			  $a=$gst->nature_visit;
			  $name=$this->dashboard_model->getNatureVisitById($a);
			  $nature=$name->booking_nature_visit_name;
			  
			  $b=$gst->source;
			  $name1=$this->dashboard_model->getsourceId($b);
			  $source_name=$name1->booking_source_name;
			  
			  $c=$gst->marketing_executive;
			  $name2=$this->dashboard_model->getmarketingId($c);
			  if(isset($name2) && $name2)
			  $p_name=$name2->name;
			  
			  $d=$gst->profit_center;
			  $name3=$this->unit_class_model->getProfitCenterById($d);
			  if(isset($name3) && $name3){
				  $profit_center_name=$name3->profit_center_location;
			  }else{
				  $profit_center_name='';
			  }
			  
			  ?>
            <td align="center"><?php 
					echo $p_name;
				?></td>
            <td align="center"><?php echo $nature ;?></td>
            <td align="center"><?php echo $source_name;?></td>
            <td align="center"><?php echo $profit_center_name;?></td>
            <td align="center" class="ba"><div class="btn-group">
                <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                <ul class="dropdown-menu pull-right" role="menu">
                  <li><a  onclick="soft_delete('<?php echo $gst->id;?>')" data-toggle="modal"  class="btn red btn-xs" ><i class="fa fa-trash"></i></a></li>
                  <li><a onclick="edit_enquery('<?php echo $gst->id; ?>')" data-toggle="modal" class="btn green btn-xs"> <i class="fa fa-edit"></i> </a></li>
                </ul>
              </div></td>
          </tr>
          <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div id="responsive" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Enquery</h4>
      </div>
      <?php

	  $form = array(
		  'class' 			=> 'form-body',
		  'id'				=> 'form',
		  'method'			=> 'post'
	  );
	
	  echo form_open_multipart('Unit_class_controller/add_enquery_management',$form);
	
	  ?>
      <div class="modal-body">
        <div class="scroller" style="height:380px" data-always-visible="1" data-rail-visible1="1">
          <div class="row"> 
            <!--<div class="com-md-12">-->
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <div class="input-group">
                  <input class="form-control" type="text" placeholder="Guest Name" id="gn" name="guest_name1" onkeypress=" return onlyLtrs(event, this);">
                  <span class="input-group-btn">
                  <input type="hidden" id="guestId" name="guestId">
                  <button class="btn green" type="button" id="addGuest"><i class="fa fa-eye" aria-hidden="true"></i></button>
                  </span> </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <select  class="form-control bs-select" id="guest_type1" name="guest_type1" required>
                  <option value="" disabled="disabled" selected="selected">Guest Type</option>
                  <option value="Family" >Family</option>
                  <option value="Bachelor">Bachelor</option>
                  <option value="Tourist">Tourist</option>
                  <option value="Sr_citizen">Sr Citizen</option>
                  <option value="Corporate">Corporate</option>
                  <option value="VIP">VIP</option>
                  <option value="Government_official">Government Official</option>
                  <option value="Military">Military </option>
                  <option value="internal_staff">Internal Staff</option>
                </select>
                <span class="help-block">Type</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="address"  id="address"  required placeholder="Address">
                <label></label>
                <span class="help-block">Address</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="pin"  id="pin"  required placeholder="Pin">
                <label></label>
                <span class="help-block">Pin</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="phone"  id="phone"  required placeholder="Phone">
                <label></label>
                <span class="help-block">Phone</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="email"  id="email"  required placeholder="Email">
                <label></label>
                <span class="help-block">Email</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control date-picker" name="from1" onselect="date_check(this.value)" id="from1"  required placeholder="From date">
                <label></label>
                <span class="help-block">From</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control date-picker" name="to1" onselect="date_check(this.value)" id="to123" required placeholder="To date">
                <label></label>
                <span class="help-block">To</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <select class="form-control bs-select"  id="type1" name="type1" required >
                  <option value="" selected="selected" disabled="disabled">Room type</option>
                  <?php if(isset($unit_class) && $unit_class){
                      foreach($unit_class as $gst){ 
                          ?>
                  <option value="<?php echo $gst->hotel_unit_class_name; ?>"  ><?php echo $gst->hotel_unit_class_name; ?></option>
                  <?php }} ?>
                </select>
                <label></label>
                <span class="help-block">Room Type</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="room1"  id="room1" onkeypress=" return onlyNos(event, this);" required placeholder="Number of room">
                <label></label>
                <span class="help-block">No of Room</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="adult1"  id="" onkeypress=" return onlyNos(event, this);" required placeholder="Number of adult">
                <label></label>
                <span class="help-block">No of Adult</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="child1" onkeypress=" return onlyNos(event, this);"  id="" required placeholder="No of Child">
                <label></label>
                <span class="help-block">No of Child</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="comming_from"   id="comming_from" required placeholder="Comming from">
                <label></label>
                <span class="help-block">Comming form</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <?php $visit=$this->dashboard_model->getNatureVisit1(); ?>				
                <select name="nature_visit" id="nature_visit" class="form-control bs-select" placeholder="Booking Type" required>
                  <option value="">Select</option>
                  <?php foreach ($visit as $key ) { ?>
                  <option value="<?php echo $key->booking_nature_visit_id; ?>" <?php if($key->nature_visit_default == '1'){ echo 'selected="selected"';} ?>><?php echo $key->booking_nature_visit_name; ?>
                  </option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <select  id="source" name="source1" class="form-control bs-select" >
                  <?php $defBookingSource=$this->unit_class_model->default_booking_source(); 
                                
                                if(isset($defBookingSource) && $defBookingSource){$defname=$defBookingSource->booking_source_name ;}else{$defname="Select";}
                                ?>
                  <option value="<?php echo $defBookingSource->booking_source_id;?>" selected><?php echo $defname;?></option>
                  <?php 
                              $dta=$this->unit_class_model->get_booking_source1();
                              if($dta)
                              {
                                  foreach($dta as $d) 
                              {
                                  ?>
                  <option value="<?php echo $d->booking_source_id;?>"><?php echo $d->booking_source_name;?></option>
                  <?php } }?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <select name="profit_center" class="form-control bs-select" id="" required>
                  <?php $pc=$this->dashboard_model->all_pc();
                              
                              $defProfit=$this->unit_class_model->profit_center_default();
							  if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro=" Select ";}
                              ?>
                  <option value="<?php echo $defProfit->id;  ?>"selected><?php echo $defPro; ?></option>
                  <?php $pc=$this->dashboard_model->all_pc1();
                                foreach($pc as $prfit_center){
                                ?>
                  <option value="<?php echo $prfit_center->id;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <select class="form-control bs-select" name="importance1" id="importance1" required >
                  <option value="" selected="selected" disabled="disabled">Importance</option>
                  <option value="1">Critical</option>
                  <option value="2">High</option>
                  <option value="3">Med</option>
                  <option value="4">Low</option>
                </select>
                <label></label>
                <span class="help-block">Importance</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <select class="form-control bs-select" id="status" name="status" required >
                  <option value="" selected="selected" disabled="disabled">Status</option>
                  <option value="1">Open</option>
                  <option value="2">Pending</option>
                  <option value="2">Resolved</option>
                  <option value="3">Closed</option>
                  <option value="4">Canceled</option>
                </select>
                <label></label>
                <span class="help-block">Status</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="preferrence1" id="name" required placeholder="Preference">
                <label></label>
                <span class="help-block">Preference</span> </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-md-line-input">
                <select  id="marketing" name="marketing" class="form-control bs-select" placeholder="Select Marketing Personel">
                  <?php $marketing=$this->unit_class_model->all_marketing_personnel(); 
                                
                                if(isset($marketing) && $marketing){$defname=$marketing->name ;}else{$defname="Select";}
                                ?>
                  <option value="<?php echo $defname;?>" selected><?php echo $defname;?></option>
                  <?php 
                              $dta=$this->unit_class_model->all_marketing_personnel();
                              if($dta)
                              {
                                  foreach($dta as $d) 
                              {
                                  ?>
                  <option value="<?php echo $d->id;?>"><?php echo $d->name;?></option>
                  <?php } }?>
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <textarea class="form-control" row="3" name="comment1" placeholder="Comment" id="Comment"></textarea>
                <label></label>
                <span class="help-block">Comment</span> </div>
            </div>            
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit"  class="btn green">Check</button>
        <button type="submit" class="btn green">Check & Save</button>
        <button type="submit" class="btn green">Block in Booking</button>
        <button  data-toggle="modal" href="#cancell_modal" class="btn green">Cancel</button>
      </div>
      <?php echo form_close(); ?> </div>
  </div>
</div>
<div id="edit_enquery" class="modal fade bs-modal-lg" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Enquery Edit</h4>
      </div>
      <?php

	  $form = array(
		  'class' 			=> 'form-body',
		  'id'				=> 'form',
		  'method'			=> 'post'
	  );
	
	  echo form_open_multipart('Unit_class_controller/edit_enquery_management',$form);
	
	  ?>
      <div class="modal-body">
        <div class="row"> 
          <!--<div class="com-md-12">-->
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <div class="input-group">
                <input class="form-control" type="text" placeholder="Guest Name" id="gn1" name="guest_name2" onkeypress=" return onlyLtrs(event, this)">
                <span class="input-group-btn">
                <button class="btn green" type="button" id="addGuest1"><i class="fa fa-eye" aria-hidden="true"></i></button>
                </span> </div>
            </div>
          </div>
          <input type="hidden" name="hid" id="hid">
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select" id="edit_guest_type1" name="guest_type2" required>
                <option value="" disabled="disabled" selected="selected">Guest Type</option>
                <option value="Family" >Family</option>
                <option value="Bachelor">Bachelor</option>
                <option value="Tourist">Tourist</option>
                <option value="Sr_citizen">Sr Citizen</option>
                <option value="Corporate">Corporate</option>
                <option value="VIP">VIP</option>
                <option value="Government_official">Government Official</option>
                <option value="Military">Military </option>
                <option value="internal_staff">Internal Staff</option>
              </select>
              <span class="help-block">Type</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="address1"  id="address1"  required placeholder="Address">
              <label></label>
              <span class="help-block">Address</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="pin1"  id="pin1"  required placeholder="Pin">
              <label></label>
              <span class="help-block">Pin</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="phone1"  id="phone1"  required placeholder="Phone">
              <label></label>
              <span class="help-block">Phone</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="email1"  id="email1"  required placeholder="Email">
              <label></label>
              <span class="help-block">Email</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="comming_from1"   id="comming_from1" required placeholder="Comming from">
              <label></label>
              <span class="help-block">Comming form</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control date-picker" name="from2" onselect="date_check(this.value)" id="from2" on required placeholder="From date">
              <label></label>
              <span class="help-block">From</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control date-picker" name="to2" onselect="date_check(this.value)" id="to2" required placeholder="To date">
              <label></label>
              <span class="help-block">To</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select class="form-control"  id="type2" name="type2" required >
                <option value="" selected="selected" disabled="disabled">Room type</option>
                <?php if(isset($unit_class) && $unit_class){
                      foreach($unit_class as $gst){ 
                          ?>
                <option value="<?php echo $gst->hotel_unit_class_name; ?>"  ><?php echo $gst->hotel_unit_class_name; ?></option>
                <?php }} ?>
              </select>
              <label></label>
              <span class="help-block">Room Type</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="room2"  id="room2" onkeypress=" return onlyNos(event, this);" required placeholder="Number of room">
              <label></label>
              <span class="help-block">No of Room</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="adult2"  id="adult2" onkeypress=" return onlyNos(event, this);" required placeholder="Number of adult">
              <label></label>
              <span class="help-block">No of Adult</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="child2" onkeypress=" return onlyNos(event, this);"  id="child2" required placeholder="No of Child">
              <label></label>
              <span class="help-block">No of Child</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <?php $visit=$this->dashboard_model->getNatureVisit1(); ?>
              <select name="nature_visit2" id="nature_visit2" class="form-control input-sm" placeholder="Booking Type" required>
                <?php $defNature=$this->dashboard_model->default_nature_visit(); if(isset($defNature)&& $defNature){$defname=$defNature->booking_nature_visit_name;}else{$defname="Select";}  ?>
                <option value="<?php echo $defNature->booking_nature_visit_id; ?>"selected><?php echo $defname;?></option>
                <?php foreach ($visit as $key ) {
                                        # code...
                                      ?>
                <option value="<?php echo $key->	booking_nature_visit_id; ?>"><?php echo $key->booking_nature_visit_name; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select  id="source2" name="source2" class="form-control input-sm" >
                <?php $defBookingSource=$this->unit_class_model->default_booking_source(); 
                                
                                if(isset($defBookingSource) && $defBookingSource){$defname=$defBookingSource->booking_source_name ;}else{$defname="Select";}
                                ?>
                <option value="<?php echo $defBookingSource->booking_source_id;?>" selected><?php echo $defname;?></option>
                <?php 
                              $dta=$this->unit_class_model->get_booking_source1();
                              if($dta)
                              {
                                  foreach($dta as $d) 
                              {
                                  ?>
                <option value="<?php echo $d->booking_source_id;?>"><?php echo $d->booking_source_name;?></option>
                <?php } }?>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select name="profit_center1" id="profit_center1" class="form-control input-sm"  required>
                <?php $pc=$this->dashboard_model->all_pc();
                              
                              $defProfit=$this->unit_class_model->profit_center_default();
							  if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro=" Select ";}
                              ?>
                <option value="<?php echo $defProfit->id;  ?>" selected><?php echo $defPro; ?></option>
                <?php $pc=$this->dashboard_model->all_pc1();
                                foreach($pc as $prfit_center){
                                ?>
                <option value="<?php echo $prfit_center->id;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select class="form-control" name="importance2" id="importance2"  required="required" >
                <option value="" selected="selected" disabled="disabled">Importance</option>
                <option value="1">Critical</option>
                <option value="2">High</option>
                <option value="3">Med</option>
                <option value="4">Low</option>
              </select>
              <label></label>
              <span class="help-block">Importance</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select class="form-control" id="status2" name="status2" required >
                <option value="" selected="selected" disabled="disabled">Status</option>
                <option value="1">Open</option>
                <option value="2">Pending</option>
                <option value="2">Resolved</option>
                <option value="3">Closed</option>
                <option value="4">Canceled</option>
              </select>
              <label></label>
              <span class="help-block">Status</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="preferrence2" id="preferrence2" required placeholder="Preference">
              <label></label>
              <span class="help-block">Preference</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <textarea class="form-control" row="3" name="comment2" placeholder="Comment" id="comment2"></textarea>
              <label></label>
              <span class="help-block">Comment</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <select  id="marketing2" name="marketing2" class="form-control input-sm" placeholder="Select Marketing Personel">
                <?php $marketing=$this->unit_class_model->all_marketing_personnel(); 
                                
                                if(isset($marketing) && $marketing){$defname=$marketing->name ;}else{$defname="Select";}
                                ?>
                <option value="<?php echo $defname;?>" selected><?php echo $defname;?></option>
                <?php 
                              $dta=$this->unit_class_model->all_marketing_personnel();
                              if($dta)
                              {
                                  foreach($dta as $d) 
                              {
                                  ?>
                <option value="<?php echo $d->id;?>"><?php echo $d->name;?></option>
                <?php } }?>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit"  class="btn green">Check</button>
        <button type="submit" class="btn green">Check & Save</button>
        <button type="submit" class="btn green">Block in Booking</button>
        <button type="submit" class="btn green">Cancel</button>
      </div>
      <?php echo form_close(); ?> </div>
  </div>
</div>
<div id="guest_name" class="modal fade" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Unit Type</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="form-group form-md-line-input form-md-floating-label col-md-12">
            <?php if(isset($enquery_management) && $enquery_management){
                      foreach($enquery_management as $gst){                         
                  ?>
            <button class="btn green" onclick="guest_set(this.value)" id="m_guest_name" value="<?php echo $gst->g_name; ?>"><?php echo $gst->g_name; ?></button>
            <?php }} ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalNorm1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content"> 
      <!-- Modal Header -->
      <div class="modal-header" style="background:#95A5A6; color:#ffffff;"> Search Guest
        <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true">&times;</span> <span class="sr-only">Close</span> </button>
      </div>
      
      <!-- Modal Body -->
      <div class="modal-body">
        <div class="form-group">
          <div class="input-group">
            <input type="email" class="form-control" id="guest_search" placeholder="Search Guest"/>
            <span class="input-group-btn">
            <button type="button" class="btn green" onclick="return_guest_search()">Search</button>
            </span> </div>
        </div>
        <div id="return_guest" style="overflow-y:scroll; height:300px; display:none;"> </div>
      </div>
      
      <!-- Modal Footer -->
      <div class="modal-footer">
        <button type="button" class="btn red"
                        data-dismiss="modal"> Close </button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalNorm2" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content"> 
      <!-- Modal Header -->
      <div class="modal-header" style="background:#95A5A6; color:#ffffff;"> Search Guest
        <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true">&times;</span> <span class="sr-only">Close</span> </button>
      </div>
      
      <!-- Modal Body -->
      <div class="modal-body">
        <div class="form-group">
          <div class="input-group">
            <input type="email" class="form-control" id="guest_search" placeholder="Search Guest"/>
            <span class="input-group-btn">
            <button type="button" class="btn green" onclick="return_guest_search2()">Search</button>
            </span> </div>
        </div>
        <div id="return_guest2" style="overflow-y:scroll; height:300px; display:none;"> </div>
      </div>
      
      <!-- Modal Footer -->
      <div class="modal-footer">
        <button type="button" class="btn red" data-dismiss="modal"> Close </button>
      </div>
    </div>
  </div>
</div>
<script>


function return_guest_search2()
    {
		
        var guest_name = $('#guest_search').val();
        //alert(guest_name);

        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_search",
                datatype:'json',
                data:{guest:guest_name},
                success:function(data)
                {
                    var resultHtml = '';
                    resultHtml+='<table class="table table-bordered" cellpadding="0" cellspacing="0" ><thead><tr><th>Name</th><th>Address</th><th width="100">Mobile No</th></tr></thead><tbody>';
                    $.each(data, function(key,value){
                        resultHtml+='<tr  class="crsr collapsed" data-toggle="collapse" data-target="#toggleDemo'+value.g_id+'">';
                        resultHtml+='<td>'+ value.g_name +'</td>';
                        resultHtml+='<td>'+ value.g_address +'</td>';
                        resultHtml+='<td>'+ value.g_contact_no +'</td>';
                        resultHtml+='</tr><tr id="toggleDemo'+value.g_id+'"  class="mrgn">';
                        resultHtml+='<td class="no-marin"  colspan="3"  cellpadding="0" cellspacing="0">';
                        resultHtml += '<div class="side clearfix" style="width:100%;"><div class="col-xs-7 ex"><p><label style="font-size:13px;">' + value.g_name + '</label><br><label>';

                        resultHtml+='<input type="hidden" id="g_id_hide"  value="'+value.g_id+'" ></input>';
                        resultHtml+='<label style="font-size:13px;">Room No: '+value.room_no+'</label><br><label style="font-size:13px;">Last Check In Date: '+value.cust_from_date+'</label><br> <button class="act active btn blue btn-sm" name="g_id" id="g_id"" value="'+value.g_id+'" onclick="get_guest2('+value.g_id+')" >Continue</button> </p></div>';
                        if(value.g_photo_thumb!='') {
                            resultHtml += '<div class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/' + value.g_photo_thumb + '" width="100px" height="92px"  > ' +
                            '</div>';
                        }
                        else{
                            resultHtml += '<div  class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/no_images.png" width="100px" height="92px" > ' +
                            '</div>';
                        }
                        
                        resultHtml+='</div></td></tr>';
                    });
                    resultHtml+='</tbody></table>';
                    $('#return_guest2').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
			$("#return_guest2").css("display", "block");
        return false;

	
    }
	
	
	 function get_guest2(id){
        var g_id=id;
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_get",
                datatype:'json',
                data:{guest_id:g_id},
                success:function(data){ 
					//alert(JSON.stringify(data));
                    $("#myModalNorm2").modal("hide");
					$('#edit_guest_type1').val(data.g_type).change();
                    $('#gn1').val(data.g_name);
                    $('#guestID').val(data.g_id);
                    $('#gp').val(data.g_pincode);
					$('#gp').addClass('focus');
                }
            });
        return false;
    }
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/delete_enquery_management?f_id="+id,
                data:{id:id},
                success:function(data)
                {
					
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                          //document.getElementById("demostat"+id).innerHTML = data.data;
					$('#row_'+id).remove();

                        });
                }
            });



        });
    }
	
	function status(id){
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/booking_nature_visit_status",
                data:{id:id},
                success:function(data)
                {
                   // alert(data);
                   // location.reload();
				   document.getElementById("demostat"+id).innerHTML = data.data;
					$('#row_'+id).remove();
                    
					
                }
            });
		
	}
	
	function enquery_management(){
		var a=document.getElementById("demo").innerHTML;
		if(a=="Active Enquery"){			
			document.getElementById("demo").innerHTML ="Inactive Enquery";			
			
		}else{			
			//$("#status123").removeClass();
			 //$("#status123").css('background-color',"green");
			document.getElementById("demo").innerHTML = "Active Enquery";
		}
		
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/enquery_list_filter",
                data:{status:a},
                 success:function(data)
                {
					//alert($data);
                  $('#table1').html(data);
				   $('#sample_1').dataTable( {
						//"pageLength": 10 
    } );
	
                }
            });
		
	
	}
	
	function check(value){
			if(value==1){	
          $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/check_nature_visit_default",
                data:{a:value},
                 success:function(data)
                {
					
					 $('#responsive').modal('hide');
				swal({   title: "Are you sure?",
				text: data+"!",  
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Fix as Default!",   cancelButtonText: "No, cancel plz!", 
				closeOnConfirm: true,  
				closeOnCancel: true },
				function(isConfirm){   
				if (isConfirm) {     
				$("#default").val('1'); $('#responsive').modal('show');

				} else {   
				$("#default").val('0'); $('#responsive').modal('show');

				} });

				
				 
                }
            });
			}
		  
		  
		  //});
		
	}
	
	
	function edit_enquery(id){
		//alert(id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/get_enquery_management",
                data:{id:id},
                success:function(data)
                {
                    //alert(data.profit_center);
				   $('#hid').val(data.id);
				   
                   $('#gn1').val(data.guest_name);
				   $('#edit_guest_type1').val(data.guest_type).change();
				   $('#from2').val(data.form_date);
				   $('#to2').val(data.to_date);
				   $('#type2').val(data.room_type).change();
				   $('#room2').val(data.room_no);
				   $('#adult2').val(data.adult_no);
				   $('#child2').val(data.child_no);
				   $('#importance2').val(data.importance).change();
				   $('#preferrence2').val(data.preference);
				   $('#comment2').val(data.	comment);
				   
				   
				   $('#address1').val(data.address);
				   $('#pin1').val(data.pin);
				   $('#phone1').val(data.phone);
				   $('#email1').val(data.email);
				   $('#comming_from1').val(data.comming_from);
				   $('#nature_visit2').val(data.nature_visit);
				   $('#source2').val(data.source);
				   $('#marketing2').val(data.marketing_executive);
				   $('#status2').val(data.status);
				   
				   $('#profit_center1').val(data.profit_center);				   
				   
                    $('#edit_enquery').modal('toggle');
					
                }
            });
	}
	
	

	
	function guest_name(value){
		 $('#guest_name').modal('toggle');
		 
		
	}
	
	function guest_set(r){
		$('#guest_name').modal('toggle');		
		$('#guest_name1').val(r)
		
	}
$("#addGuest").click(function(){
	$('#myModalNorm1').modal('show');
	$('#return_guest').html("");
	$("#return_guest").css("display", "none");
});


/*  edit */
	function guest_name(value){
		 $('#guest_name').modal('toggle');		 
		
	}	
	function guest_set(r){
		$('#guest_name1').modal('toggle');		
		$('#guest_name2').val(r)
		
	}
$("#addGuest1").click(function(){
	$('#myModalNorm2').modal('show');
	$('#return_guest2').html("");
	$("#return_guest2").css("display", "none");
});



/*   */

function return_guest_search()
    {
		
        var guest_name = $('#guest_search').val();
        //alert(guest_name);

        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_search",
                datatype:'json',
                data:{guest:guest_name},
                success:function(data)
                {
                    var resultHtml = '';
                    resultHtml+='<table class="table table-bordered" cellpadding="0" cellspacing="0" ><thead><tr><th>Name</th><th>Address</th><th width="100">Mobile No</th></tr></thead><tbody>';
                    $.each(data, function(key,value){
                        resultHtml+='<tr  class="crsr collapsed" data-toggle="collapse" data-target="#toggleDemo'+value.g_id+'">';
                        resultHtml+='<td>'+ value.g_name +'</td>';
                        resultHtml+='<td>'+ value.g_address +'</td>';
                        resultHtml+='<td>'+ value.g_contact_no +'</td>';
                        resultHtml+='</tr><tr id="toggleDemo'+value.g_id+'"  class="mrgn">';
                        resultHtml+='<td class="no-marin"  colspan="3"  cellpadding="0" cellspacing="0">';
                        resultHtml += '<div class="side clearfix" style="width:100%;"><div class="col-xs-7 ex"><p><label style="font-size:13px;">' + value.g_name + '</label><br><label>';

                        resultHtml+='<input type="hidden" id="g_id_hide"  value="'+value.g_id+'" ></input>';
                        resultHtml+='<label style="font-size:13px;">Room No: '+value.room_no+'</label><br><label style="font-size:13px;">Last Check In Date: '+value.cust_from_date+'</label><br> <button class="act active btn blue btn-sm" name="g_id" id="g_id"" value="'+value.g_id+'" onclick="get_guest('+value.g_id+')" >Continue</button> </p></div>';
                        if(value.g_photo_thumb!='') {
                            resultHtml += '<div class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/' + value.g_photo_thumb + '" width="100px" height="92px"  > ' +
                            '</div>';
                        }
                        else{
                            resultHtml += '<div  class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD; margin-top:14px;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/no_images.png" width="100px" height="92px" > ' +
                            '</div>';
                        }
                        
                        resultHtml+='</div></td></tr>';
                    });
                    resultHtml+='</tbody></table>';
                    $('#return_guest').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
			$("#return_guest").css("display", "block");
        return false;

	
    }


	
</script> 
<script>

	$(document).ready(function(){
    $("form").submit(function(){
        
		$.valid_from = Date.parse($('#from1').val());
		$.valid_upto = Date.parse($('#to1').val());
		//$.renewal = Date.parse($('#c_renewal').val());
		$.value = $.now();
		
		if($.valid_from > $.value)
		{
			alert('Valid Date Should Be Lesser Than The System Date');
			return false;
		}else if($.valid_upto < $.value)
		{
			alert('Valid Upto Date Should Be Greater Than The Todays Date');
			return false;
		}
		
		else
		{
			return true;
		}
		
    	});
        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);		
		$('.date-picker').datepicker({
                orientation: "left",
				startDate: today,
                autoclose: true,
				todayHighlight: true
		});
	
});
    function get_guest(id){
        var g_id=id;
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_get",
                datatype:'json',
                data:{guest_id:g_id},
                success:function(data){ 
					//alert(JSON.stringify(data));
                    $("#myModalNorm1").modal("hide");		
					$('#guest_type1').val(data.g_type).change();
                    $('#gn').val(data.g_name);
                    $('#guestID').val(data.g_id);
                    $('#guestId').val(data.g_id);
                    $('#gp').val(data.g_pincode);
					
                    $('#pin').val(data.g_pincode);
                    $('#email').val(data.g_email);
                    $('#address').val(data.g_address);
					$('#gp').addClass('focus');
                }
            });
        return false;
    }

function insert_commision(val){
	alert(val);
}	
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
  //alert(target);
  if (target = '#individual'){
	  $("#trrID").val("");
	  $("#tfiID").val("");
  }
  if (target = '#default'){
	  $("#arc").val("");
	  $("#afi").val("");
	  $("#crc").val("");
	  $("#cfi").val("");	  
  }  
});	

$("#newBtn").click(function(){
	var noRoom = $("#nr").val();
	alert(noRoom);
	return false
	var x1 = $("#trrID").val();
	var x2 = $("#arc").val();
	var x3 = $("#crc").val();
	var x4 = $("#tfiID").val();
	var ng = parseInt($("#ng").val());
	
    for (x = 1; x <= noRoom; x++) { 
    var days = parseInt($("#days_"+x).val());	
	if($("#arc1_"+x).val() !=""){
	    var arc1 = parseInt($("#arc1_"+x).val());
	} else {
		var arc1 = '0';
	}

	if($("#afi1_"+x).val() !=""){
	    var afi1 = parseInt($("#afi1_"+x).val());
	} else {
		var afi1 = '0';
	}

	if($("#crc1_"+x).val() !=""){
	    var crc1 = parseInt($("#crc1_"+x).val());
	} else {
		var crc1 = '0';
	}
	
	if($("#cfi1_"+x).val() !=""){
	    var cfi1 = parseFloat($("#cfi1_"+x).val());
	} else {
		var cfi1 = '0';
	}
	
	
	if($("#ao_"+x).val() !=""){
	    var ao = parseInt($("#ao_"+x).val());
	} else {
		var ao = '0';
	}
	if($("#co_"+x).val() !=""){
	    var co = parseInt($("#co_"+x).val());
	} else {
		var co = '0';
	}
	
	var to = parseInt($("#ao_"+x).val())+parseInt($("#co_"+x).val());
	var mo =$("#mo_"+x).val();
	if(x4 == ""){
	if(x1 == "" && x2 == "" && x3 == ""){
			//alert("HERE1");
			var fi = (ao * afi1) +(co * cfi1) ;
			var rr = $("#trr_"+x).val();
			var tx = $("#tax_"+x).val();
			var tv = parseFloat(fi) + parseFloat(rr) + parseFloat(tx);
			var ttv = days * parseFloat(tv); 
			$("#pdr_"+x).val(tv);
			$("#price_"+x).val(ttv);
			var vat = (parseFloat(fi)*1000)/1145;
			var m = parseFloat(fi) - parseFloat(vat);
			$("#tfi_"+x).val(vat);
			$("#vat_"+x).val(m);				
	} else {
			//alert("HERE");
			var val1 =(ao * arc1) +(ao * afi1) ;
			var val2 =(co * crc1) +(co * cfi1) ;
			if(x1 == ""){
				var val11 =(ao * arc1) + (co * crc1);
			} else {
			    var val11 = x1;
			}
			var val22 = (ao * afi1) +(co * cfi1) ;
			
			/*alert(val1);
			alert(val2);
			alert(val11);
			alert(val22);*/
			
			$("#trr_"+x).val(val11);
			var taxApplicable = $("#tax_applicable").val();
	        if(taxApplicable == 'YES' ){
				/*if(val11 < slab){
					var tax = (parseFloat(val11) * parseFloat(dbTax1))/100;
				} else {
					var tax = (parseFloat(val11) * parseFloat(dbTax2))/100;
				}*/
				
				if((val11 >= slab1) && (val11 <= slab2)){
				var tax = (parseFloat(val11) * parseFloat(dbTax1))/100;
			} 
			else if ((val11 >= slab) /*&& (val11 <= slab3)*/)
			{
				var tax = (parseFloat(val11) * parseFloat(dbTax2))/100;
			}
			else
			{
				var tax = (parseFloat(val11) * parseFloat(dbTax3))/100;
			}
			} else {
				var tax = 0;
			}
            if(x1 == ""){			
			      var val3 = parseFloat(val1) + parseFloat(val2) + parseFloat(tax);
			} else {
				  var val3 = parseFloat(x1) + parseFloat(val22) + parseFloat(tax);
			}
			var val4 = days * parseFloat(val3);
			$("#tax_"+x).val(tax);
			
			$("#pdr_"+x).val(val3);
			$("#price_"+x).val(val4);
			var vat = (parseFloat(val22)*1000)/1145;
			var m = parseFloat(val22) - parseFloat(vat);
			$("#tfi_"+x).val(vat);
			$("#vat_"+x).val(m);

	}
}

}
});

function date_check(a){
	 var a=new Date($('#from1').val()).getTime();
	 var b=new Date($('#to1').val()).getTime();
	if(a>b){
		$('#responsive').modal('hide');
		swal({
			title:"To date should grater than from date!",
			text:"",
		type:"warning"
		},			
		function(){  
		$('#responsive').modal('show');
		$('#to1').val('');
		});
		
	}
}
</script>
<?php 
$val = $this->uri->segment(3);
if(isset($val) && $val > 0) { ?>
<script>
$(window).load(function(){
        $('#myModalNorm').modal('show');
    });	
</script>
<?php } ?>
<div id="cancell_modal" class="modal fade" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Enquery</h4>
      </div>
      <?php

	  $form = array(
		  'class' 			=> 'form-body',
		  'id'				=> 'form',
		  'method'			=> 'post'
	  );
	
	  //echo form_open_multipart('Unit_class_controller/add_enquery_management',$form);
	
	  ?>
      <div class="modal-body">
        <div class="row"> 
          <!--<div class="com-md-12">-->
          <div class="col-md-12">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="reason" id="reason" required placeholder="Reason for cancellation">
              <label></label>
              <span class="help-block">Reason for cancellation</span> </div>
          </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-12">
            <div class="row">
              <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                <button type="submit"  class="btn green">Ok</button>
              </div>
            </div>
            
            <!-- </div>--> 
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--<div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>--> 
<?php echo form_close(); ?>
</div>
</div>
