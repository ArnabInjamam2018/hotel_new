<div class="row all_bk">
    <div class="form-body">
        <!-- 17.11.2015-->
        <?php if($this->session->flashdata('err_msg')):?>
            <div class="form-group">
                <div class="col-md-12 control-label">
                    <div class="alert alert-danger alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong><?php echo $this->session->flashdata('err_msg');?></strong>
                    </div>
                </div>
            </div>
        <?php endif;?>
        <?php if($this->session->flashdata('succ_msg')):?>
            <div class="form-group">
                <div class="col-md-12 control-label">
                    <div class="alert alert-success alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong><?php echo $this->session->flashdata('succ_msg');?></strong>
                    </div>
                </div>
            </div>
        <?php endif;?>
    </div>

    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <?php /*<div class="portlet box purple">
						<div class="portlet-title">

							<div class="caption">
								<i class="fa fa-info-circle"></i>All Rooms
							</div>
							<div class="tools col-sm-6">
								<!--<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a><a href="javascript:;" class="remove">
								</a>-->
								<input type="submit" name="submit" value="Delete All" class="btn btn-danger pull-right">
								<?php
							$form = array(
										   'class' 			=> 'form-horizontal',
										   'method'			=> 'post'
                                          );

							$dropdown = array(
										  '5'  				=> '5',
										  '10'    			=> '10',
										  '25'   			=> '25',
										  '50'				=> '50',
										  '100'				=> '100'
										);

							$js = 'class="form-control" onChange="this.form.submit();"';

							$pages = ($this->session->flashdata('pages')) ? $this->session->flashdata('pages') : 5;

							echo form_open('dashboard/all_rooms',$form);
							?>
								<div class="col-sm-4 pull-right"><div class="dataTables_length" id="sample_1_length">
								<label style=" margin:0px;">
								<!--<select name="sample_1_length" aria-controls="sample_1" class="form-control input-xsmall input-inline">
								<option value="5">5</option><option value="15">15</option>
								<option value="20">20</option>
								<option value="-1">All</option>
								</select> -->
								<?php echo form_dropdown('pages', $dropdown, $pages, $js);?>

								</label>records
								</div></div>
								 <?php echo form_close();?>
								<!--<a href="javascript:;" class="reload">
								</a>-->

							</div>
						</div>

						<div class="portlet-body">

							<div class="table-scrollable">
								<table class="table table-striped table-bordered table-hover" >
								<thead>
								<tr>
									<th scope="col">
										Select
									</th>
									<th scope="col" width="20%">
										 Room No.
									</th>
									<th scope="col" width="20%">
										Room Type
									</th>
									<th scope="col" width="20%">
										Price
									</th>
									<th scope="col" width="20%">
										Room Image
									</th>
									<th scope="col" width="50%">
										 Action
									</th>
								</tr>
								</thead>
								<tbody>
								<?php if(isset($rooms) && $rooms):
								$i=1;
								foreach($rooms as $rom):
								if($rom->hotel_id==$this->session->userdata('user_hotel')):
								$class = ($i%2==0) ? "active" : "success";

								$room_id=$rom->room_id;
								?>
								<tr>
									<td width="50">
										<div class="md-checkbox pull-left">
											<input type="checkbox" id="checkbox<?php echo $room_id; ?>" class="md-check">
											<label for="checkbox<?php echo $room_id; ?>">
											<span></span>
											<span class="check"></span>
											<span class="box"></span>
											</label>
										</div>
									</td>
									 <td><?php echo $rom->room_no;?></td>
                                     <td><?php echo $rom->room_bed." Beds ";?></td>
                                     <td>₹ <?php echo $rom->room_rent;?></td>
                                     <td><img src="<?php echo base_url();?>upload/thumb/<?php echo $rom->room_image_thumb;?>" alt=""/></td>


									<td>
									<button class="btn btn-danger pull-right" data-toggle="confirmation" data-singleton="true">Delete</button>
									<a href="#myModal3" class="btn blue pull-right" data-toggle="modal">Edit</a>
									</td>
								</tr>
								  <?php $i++;?>
                                <?php endif; ?>
                                <?php endforeach; ?>
                                <?php endif;?>

								</tbody>
								</table>
							</div>
						</div>
					</div>   */ ?>




        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="glyphicon glyphicon-bed"></i>All Expense Report
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-10">
                             <div class="btn-group">
                                  <?php

                            $form = array(
                                'class'             => 'form-body',
                                'id'                => 'form',
                                'method'            => 'post',
                                    
                            );
                            
                            

                            echo form_open_multipart('dashboard/filter_expense_report',$form);

                            ?>
                                 
                                <div class="form-group form-md-line-input col-md-4">
                                  <select class="form-control"  id="top" name="top"  required="required" >
                                    <option value="">Types Of Payment</option>
                                    <option value="Bill Payments">Bill Payments</option>
                                    <option value="Salary/Wage Payments">Salary/Wage Payments</option>
                                    <option value="Miscellaneous Payments">Miscellaneous Payments</option>
                                    <option value="Tax/Compliance Payment">Tax/Compliance Payment</option>
                                    <option value="Purchase">Purchase</option>
                                  </select>
                              </div>
                              
                                  <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                                      <input autocomplete="off" type="text" class="form-control date-picker" id="date" name="f_date" >
                                      <label> From Date</label>
                                  </div>
                                  <div class="form-group form-md-line-input form-md-floating-label col-md-3">
                                      <input autocomplete="off" type="text" class="form-control date-picker" id="date" name="t_date" >
                                      <label> To Date</label>
                                  </div>
                                  

                                  <div class="form-group form-md-line-input col-md-2">
                                <button class="btn green" type="submit">
                                      <b> Select</b> 
                                   </button>

                               </div>
                               </div>  
                               <?php form_close(); ?>
                            </div>
                             
                       
                       
                        <div class="col-md-2">
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" onclick="javascript:window.print();">
                                            Print </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>dashboard/pdf_reports_police" target="_blank">
                                            Save as PDF </a>
                                    </li>
                                    <!--<li>
                                        <a href="javascript:;">
                                            Export to Excel </a>
                                    </li>-->
                                </ul>
                            </div>
                        </div>
                   </div>
                </div>
                <?php //print_r($payments); //print_r($purchases); ?>
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                    <tr>
                        <!--<th scope="col">
                                Select
                            </th>-->
                        <th scope="col" width="20%">
                            Transanction Id
                        </th>
                        <th scope="col" width="20%">
                            Payment /Purchase
                        </th>
                        <th scope="col" width="20%">
                            From
                        </th>
                        <th scope="col" width="20%">
                            To
                        </th>
                        <th scope="col" width="20%">
                            Date
                        </th>
                        <th scope="col" width="20%">
                            Amount
                        </th>
                         <th scope="col" width="20%">
                            Delete
                        </th>

                    </tr>
                    </thead>
                    <tbody>

                    <?php


                                        ?>
                                        
                                             <?php if(isset($allpayments) && $allpayments){
                                                    foreach($allpayments as $ap){
                                                        if($ap->transaction_from_id=="4"){
                                                            $from_id="Hotel";
                                                        } if($ap->transaction_to_id=="5"){
                                                            $to_id="Government";
                                                        }else if($ap->transaction_to_id=="7"){
                                                            $to_id="Employee";
                                                        }else if($ap->transaction_to_id=="6"){
                                                            $to_id="Vendor";
                                                        }else if($ap->transaction_to_id=="11"){
                                                            $to_id="Debtor";
                                                        }
                                                ?> <tr>
                                            <td><?php echo $ap->t_id?></td>
                                            <td><?php echo $ap->transaction_type?></td>
                                            <td><?php echo $from_id?></td>
                                            <td><?php echo $to_id?></td>
                                            <td><?php echo $ap->t_date?></td>
                                            <td><?php echo $ap->t_amount?></td>
                                            <td>
                                            <a onclick="soft_delete('<?php echo $ap->t_id;?>')" data-toggle="modal"  class="btn red pull-right"><i class="fa fa-trash"></i></a>
                                            </td>
                                           

                                        </tr>
                                         <?php }}?>

                    </tbody>

                </table>
                 <div class="row">

                
                <div class="col-md-10" style="text-align:right;">
                      <h3><strong>Total Payments : </strong></h3>
                    </div><?php $daily_sum=0;?>
                    <?php if(isset($allpayments) && $allpayments){
                        foreach($allpayments as $ap){
                        $daily_sum=$daily_sum+$ap->t_amount;
                        }
                        
                    }?>
               <div class="col-md-2"><h3> <i class="fa fa-inr" style="font-size: 21px;"> </i> <?php echo $daily_sum; ?> </h3></div></div>
                
               
                

        </div>
            </div>

        </div>

        <!-- END SAMPLE TABLE PORTLET-->


        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <!--
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-eye-slash"></i>All Rooms
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
                        	<?php
        $form = array(
            'class' 			=> 'form-horizontal',
            'method'			=> 'post'
        );

        $dropdown = array(
            '5'  				=> '5',
            '10'    			=> '10',
            '25'   			=> '25',
            '50'				=> '50',
            '100'				=> '100'
        );

        $js = 'class="form-control" onChange="this.form.submit();"';

        $pages = ($this->session->flashdata('pages')) ? $this->session->flashdata('pages') : 5;

        echo form_open('dashboard/all_rooms',$form);
        ?>
                        	<div class="form-group">
                                <label class="col-md-3 control-label">
                                    Rooms Per Page
                                </label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <?php echo form_dropdown('pages', $dropdown, $pages, $js);?>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close();?>
                            <?php if($this->session->flashdata('err_msg')):?>

                            <div class="alert alert-danger alert-dismissible text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong><?php echo $this->session->flashdata('err_msg');?></strong>
                            </div>

                            <?php endif;?>
                            <?php if($this->session->flashdata('succ_msg')):?>

                            <div class="alert alert-success alert-dismissible text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong><?php echo $this->session->flashdata('succ_msg');?></strong>
                            </div>

                            <?php endif;?>
                            <div id="status_msg">

                             </div>
                            <?php echo form_open('dashboard/room_delete');?>
							<div class="table-scrollable">
								<table class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>
										 Select
									</th>
									<th>
										 Room No
									</th>
									<th>
										 Number of Beds
									</th>
									<th>
										 Rent
									</th>
									<th>
										 Image
									</th>
                                    <th>
										 Action
									</th>
								</tr>
								</thead>
								<tbody>
								<?php if(isset($rooms) && $rooms):
            $i=1;
            foreach($rooms as $rom):
                if($rom->hotel_id==$this->session->userdata('user_hotel')):
                    $class = ($i%2==0) ? "active" : "success";

                    $room_id=$rom->room_id;
                    ?>

                                    <tr class="<?php echo $class;?>">
                                        <td><input type="checkbox" name="delete[]" value="<?php echo $rom->room_id;?>" class="form-control" onclick="this.form.submit();"/></td>
                                        <td><?php echo $rom->room_no;?></td>
                                        <td><?php echo $rom->room_bed;?></td>
                                        <td>₹ <?php echo $rom->room_rent;?></td>
                                        <td><img src="<?php echo base_url();?>upload/thumb/<?php echo $rom->room_image_thumb;?>" alt=""/></td>
                                        <td>

                                        	<a href="<?php echo base_url();?>dashboard/edit_room/<?php echo base64url_encode($room_id);?>" class="fa fa-edit"></a>
                                        <td>
                                    </tr>
                                <?php $i++;?>
                                <?php endif; ?>
                                <?php endforeach; ?>
                                <?php endif;?>
								</tbody>
								</table>
                                <p class="pull-right" style="padding-top:10px;">
                                    <input type="submit" name="submit" value="Delete" class="btn btn-danger" onclick="javascript:return confirm('Are you sure that you want to delete the selected records?')"/>
                            </p>

                            <?php echo form_close();?>
								<?php if(isset($pagination) && $pagination)
            echo $pagination; ?>
							</div>
						</div>
					</div>
					-->
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT-->
</div>
</div>
<!-- END CONTENT -->
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_income_report?r_id="+id,
                data:{booking_id:id},
                success:function(data)
                {
					//alert (data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
</script>

