 
      <div class="portlet light borderd">
        <div class="portlet-title">
          <div class="caption"> <i class="fa fa-file-archive-o"></i> EXtra Charge Reports </div>
          
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
                 
        </div>
        <div class="portlet-body">
          <div class="table-toolbar">
            <div class="row">
              <div class="col-md-7">
                  <?php

                            $form = array(
                                'class'       => 'form-inline',
                                'id'        => 'form_date',
                                'method'      => 'post'
                            );

                            echo form_open_multipart('dashboard/extra_charge_report',$form);
							
								if(isset($start_date) && isset($end_date))
								{
								$s_date=$start_date;
								$e_date=$end_date;
								}
								else
								{
								$s_date="";
								$e_date="";
								}
						
                            ?>
                      <div class="form-group">
                        <input type="text" autocomplete="off" required="required" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" <?php if($s_date!="" && $e_date!=""){ 
						echo "value='".$s_date."'";} else {echo "placeholder='Start Date'";}?>>
                      </div>
                      <div class="form-group">
                        <input type="text" autocomplete="off" required="required" name="t_dt_to" class="form-control date-picker" <?php if($s_date!="" && $e_date!=""){echo "value='".$e_date."'";} else {echo "placeholder='End Date'";}?>>
                      </div>
					   
                    <button class="btn btn-default" onclick="check_sub()" type="submit">Search</button>
                  <?php form_close(); ?>
              </div>
            </div>
          </div>
          <table class="table table-striped table-hover table-bordered" id="sample_1">
            <thead>
              <tr> 
                <th> # </th>
				<th>Booking ID </th>
				<th>Status</th>
				<th>From Date </th>
				<th>To Date </th>
				<th style="padding:0;" width="40%">
                	<table class="table table-hover table-bordered" style="margin:0; border:none; table-layout:fixed;">
                        <tr style="background-color:#d7d4d4;">
                        	<th width='28%'>Extra Charge Name</th><th width='20%'>Added on</th><th width='15%'>Unit Price</th><th width='14%'>Tax %</th><th width='22%'>Total Cost</th>
                        </tr>
                    </table>
                </th>
			    <th>Total</th>
			    
              </tr>
            </thead>
            <tbody>
              <?php
						if(($bookings)){
						$srl_no=0;
	
						foreach($bookings as $report){
							
						$status_id=$report->booking_status_id;	
						if($report->booking_extra_charge_id)
						{
						$srl_no++;
						$extra_ch_id = explode(",",$report->booking_extra_charge_id);
						if($extra_ch_id)
						{
						
						$ecs="";
						$ecs_cost=0;
						for($i=0;$i<count($extra_ch_id);$i++)
						{
						$extra_charge=$this->dashboard_model->get_extra_charge_by_id($extra_ch_id[$i]);
						foreach($extra_charge as $ec)
						{
							if(isset($start_date) && isset($end_date))
							{
								if(isset($ec->aded_on)&& $ec->aded_on != "")
								{
									$date_part=explode(" ",$ec->aded_on);
						$date1 = date_create($date_part[0]);
						
						$date2 = date_create($start_date);
						$date3= date_create($end_date);
						
						$dDiff = date_diff($date1,$date2);
						$dif = $dDiff->format("%R%a");
						
						$dDiff2 = date_diff($date1,$date3);
						$dif2 = $dDiff2->format("%R%a");
					
					if($dif<=0 && $dif2>=0)
					{
						$addon = date('g:ia \ l jS M Y',strtotime($ec->aded_on));
						$ecs= $ecs."<tr><td width='28%'>$ec->crg_description</td><td width='20%'>$addon</td><td width='15%'>$ec->crg_unit_price</td><td width='14%'>$ec->crg_tax %</td><td width='22%'>$ec->crg_total </td></tr>";
						$ecs_cost=$ecs_cost + $ec->crg_total;
								
					}			
								}
							}
							else 
							{
								$addon = date('g:ia \ jS M Y',strtotime($ec->aded_on));
								$dif=0;
								$dif2=0;
								$ecs= $ecs."<tr><td width='28%'>$ec->crg_description</td><td width='20%'>$addon</td><td width='15%'>$ec->crg_unit_price</td><td width='14%'>$ec->crg_tax %</td><td width='22%'>$ec->crg_total </td></tr>";
						$ecs_cost=$ecs_cost + $ec->crg_total;
							}
						
						}
												
						}
						}
						
						$status=$this->dashboard_model->get_status_by_id($status_id);					
			  ?>
             <tr>
             
				<td><?php echo $srl_no;?></td>
				
				<td>
					<?php // Booking ID
						if(isset($report->booking_id_actual) && (trim($report->booking_id_actual)!= "")){
							echo '<span style="color:#023358;" class="fa fa-user" ><span style="font-family:Open Sans,sans-serif; font-size:12px;padding-left:5px;">'.$report->booking_id_actual.'</span></span>';
						} 
						else 
						{
							echo '<span style="color:#F8681A;" class="fa fa-group" ><span style="font-family:Open Sans,sans-serif; font-size:12px;padding-left:5px;">HM0'.$this->session->userdata('user_hotel').'00'.$report->booking_id."</span></span>";
						}
					?>
				</td>
				
				<td><?php if(!empty($status)){
					 foreach($status as $st){?>
				<span class="label" style="background-color:<?php echo $st->bar_color_code;?>; color:<?php echo $st->body_color_code ?>;"> <?php echo $st->booking_status; }}?></span>
				</td>	
				
				<td><?php echo $report->cust_from_date;?></td>
				
				<td><?php echo $report->cust_end_date;?></td>
				
				<td style="padding:0;">
                	<table class="table table-hover table-bordered" style="margin:0; border:none;  table-layout:fixed;">
						<?php if(isset($ecs))echo $ecs; ?>
                	</table>
				</td>
				
				<td><?php if(isset($ecs_cost))echo number_format((float)$ecs_cost,2,'.',',');?></td>
				
			  </tr>
					 <?php 
						}}}?>
						
						<?php 
						
					 if(isset($groups) && $groups){
						 
						foreach($groups as $grp){
							
						if($grp->charges_id)
						{
						$srl_no++;
						$extra_ch_id = explode(",",$grp->charges_id);
						if($extra_ch_id)
						{
						
						$ecs="";
						$ecs_cost=0;
						for($i=0;$i<count($extra_ch_id);$i++)
						{
						$extra_charge=$this->dashboard_model->get_extra_charge_by_id($extra_ch_id[$i]);
						foreach($extra_charge as $ec)
						{
							if(isset($start_date) && isset($end_date))
							{
								if(isset($ec->aded_on)&& $ec->aded_on != "")
								{
									$date_part=explode(" ",$ec->aded_on);
					$date1 = date_create($date_part[0]);
					
					$date2 = date_create($start_date);
					$date3= date_create($end_date);
					
					$dDiff = date_diff($date1,$date2);
					$dif = $dDiff->format("%R%a");
					
					$dDiff2 = date_diff($date1,$date3);
					$dif2 = $dDiff2->format("%R%a");
					if($dif<=0 && $dif2>=0)
					{
					$ecs= $ecs."<tr><td width='20%'>$ec->crg_description</td><td width='20%'>$ec->aded_on </td><td width='20%'>$ec->crg_unit_price</td><td width='20%'>$ec->crg_tax %</td><td width='20%'>$ec->crg_total </td></tr>";
						$ecs_cost=$ecs_cost + $ec->crg_total;
								
					}			
								}
							}
							else 
							{
								$dif=0;
								$dif2=0;
								$ecs= $ecs."<tr><td width='20%'>$ec->crg_description</td><td width='20%'>$ec->aded_on </td><td width='20%'>$ec->crg_unit_price</td><td width='20%'>$ec->crg_tax %</td><td width='20%'>$ec->crg_total </td></tr>";
						$ecs_cost=$ecs_cost + $ec->crg_total;
							}
						
						}
					
						}
					
			?>
						<tr>
             
				<td><?php echo $srl_no;?></td>
				
				<td><?php if(isset($grp->id) && (trim($grp->id)!= "")){echo '<span class="fa fa-group" style="color:#009999;" ><span style="font-family:Open Sans,sans-serif; font-size:12px;padding-left:5px; ">HM0'.$this->session->userdata('user_hotel').'GRP00'.$grp->id.'</span></span>';}?></td>	
				<td><?php echo $grp->start_date;?></td>
				<td><?php echo $grp->start_date;?></td>
				<td style="padding:0;">
                	<table class="table table-hover table-bordered" style="margin:0; border:none;">
						<?php if(isset($ecs))echo $ecs; ?>
                	</table>
				</td>
				<td><?php if(isset($ecs_cost))echo number_format((float)$ecs_cost,2,'.',',');?></td>
				<td><?php 
					 
					 $sb_row=$this->dashboard_model->get_sbID_detail($grp->id);
					$bookingStatusID = $sb_row->booking_status_id;
					 $status=$this->dashboard_model->get_status_by_id($bookingStatusID);
					 foreach($status as $st)
					 {
					 ?>
	<span class="label" style="background-color:<?php echo $st->bar_color_code;?>; color:<?php echo $st->body_color_code ?>;"> <?php echo $st->booking_status; ?></span>
					 <?php } ?>
					 </td>
				
			  </tr>
				
						<?php 
						}
						}
						}
						}
						?>
            </tbody>
          </table>
		 
		</div>
		</div>
<script>
$(document).ready(function(){
  $('#chkbx_tdy').prop('checked', true);
});

function check_sub(){
  document.getElementById('form_date').submit();
}
</script>