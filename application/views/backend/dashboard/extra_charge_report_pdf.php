<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
	border: 1px solid #CCC;
}
.td-pad th .td-pad tr:first-child th, .td-pad td .td-pad tr:first-child td{
	border-top:none;
}
.td-pad th .td-pad tr:last-child th, .td-pad td .td-pad tr:last-child td{
	border-bottom:none;
}
.td-pad th .td-pad tr th:first-child, .td-pad td .td-pad tr td:first-child{
	border-left:none;
}
.td-pad th .td-pad tr th:last-child, .td-pad td .td-pad tr td:last-child{
	border-right:none;
}
.td-pad th, .td-pad td{
	font-size:12px;
	font-weight:normal;
}
</style>
<?php	if(isset($start_date) && isset($end_date))
	{
	$s_date=$start_date;
	$e_date=$end_date;
	}
	else
	{
	$s_date="";
	$e_date="";
	}

?>
<div style="padding:15px 35px;">
<table width="100%">
    <tr>
      <td align="left" valign="middle"><img src="upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" /></td>
      <td colspan="2" align="center" valign="middle"><font size='13'>Extra Charge Report</font></td>
	  <td align="right" valign="middle"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?></td>
    </tr>
    <tr>
      <td colspan="4"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr>
      <td colspan="3" align="left" valign="middle"><?php if(isset($start_date) && isset($end_date)){echo "<b>From : </b>".$start_date." <b>To : </b>".$end_date;}?></td>
      <td align="right" valign="middle"><?php date_default_timezone_set('Asia/Kolkata'); echo date('D-M-Y h:i:sa'); ?></td>
    </tr>
    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
</table>
<table class="td-pad">
<thead>
  <tr style="background: #e5e5e5; color: #1b1b1b"> 
    <th align="center" valign="middle"> # </th>
    <th align="center" valign="middle">Booking ID </th>
    <th align="center" valign="middle">From Date </th>
    <th align="center" valign="middle">To Date </th>
    <th align="center" valign="middle" style="padding:0;">
        <table class="td-pad">
        	<thead>
            <tr style="background: #e5e5e5; color: #1b1b1b">
                <th width='20%' align="center" valign="middle">Extra Charge Name</th><th width='20%' align="center" valign="middle">Added on</th><th width='20%' align="center" valign="middle">Unit Price</th><th width='20%' align="center" valign="middle">Tax %</th><th width='20%' align="center" valign="middle">Total Cost</th>
            </tr>
            </thead>
        </table>
    </th>
    <th align="center" valign="middle">Total</th>
    <th align="center" valign="middle">Status</th>
  </tr>
</thead>
<tbody  style="background: #F2F2F2">
  <?php
            if(($bookings)){
            $srl_no=0;

            foreach($bookings as $report){
                //echo "<pre>";
            //print_r($report);
                //exit;
            $status_id=$report->booking_status_id;	
            if($report->booking_extra_charge_id)
            {
            $srl_no++;
            $extra_ch_id = explode(",",$report->booking_extra_charge_id);
            if($extra_ch_id)
            {
            //print_r($extra_ch_id);	
            $ecs="";
            $ecs_cost=0;
            for($i=0;$i<count($extra_ch_id);$i++)
            {
            $extra_charge=$this->dashboard_model->get_extra_charge_by_id($extra_ch_id[$i]);
            foreach($extra_charge as $ec)
            {
                if(isset($start_date) && isset($end_date))
                {
                    if(isset($ec->aded_on)&& $ec->aded_on != "")
                    {
                        $date_part=explode(" ",$ec->aded_on);
        $date1 = date_create($date_part[0]);
        
        $date2 = date_create($start_date);
        $date3= date_create($end_date);
        
        $dDiff = date_diff($date1,$date2);
        $dif = $dDiff->format("%R%a");
        
        $dDiff2 = date_diff($date1,$date3);
        $dif2 = $dDiff2->format("%R%a");
        if($dif<=0 && $dif2>=0)
        {
        $ecs= $ecs."<tr><td width='20%' align='center' valign='middle' height='26'>$ec->crg_description</td><td width='20%' align='center' valign='middle'>$ec->aded_on </td><td width='20%' align='center' valign='middle'>$ec->crg_unit_price</td><td width='20%' align='center' valign='middle'>$ec->crg_tax %</td><td width='20%' align='center' valign='middle'>$ec->crg_total </td></tr>";
            $ecs_cost=$ecs_cost + $ec->crg_total;
                    
        }			
                    }
                }
                else 
                {
                    $dif=0;
                    $dif2=0;
                    $ecs= $ecs."<tr><td width='20%' align='center' valign='middle' height='26'>$ec->crg_description</td><td width='20%'  align='center' valign='middle'>$ec->aded_on </td><td width='20%' align='center' valign='middle'>$ec->crg_unit_price</td><td width='20%' align='center' valign='middle'>$ec->crg_tax %</td><td width='20%' align='center' valign='middle'>$ec->crg_total </td></tr>";
            $ecs_cost=$ecs_cost + $ec->crg_total;
                }
            
            }
                                    
            }
            }
            
        
            $status=$this->dashboard_model->get_status_by_id($status_id);
                
            
  ?>
 <tr>
 
    <td align="center" valign="middle"><?php echo $srl_no;?></td>
    
    <td align="center" valign="middle"><?php if(isset($report->booking_id_actual) && (trim($report->booking_id_actual)!= ""))echo $report->booking_id_actual; else echo /*'HM0'.$this->session->userdata('user_hotel').'00'.*/$report->booking_id;/**/?></td>	
    <td align="center" valign="middle"><?php echo $report->cust_from_date;?></td>
    <td align="center" valign="middle"><?php echo $report->cust_end_date;?></td>
    <td align="center" valign="middle" style="padding:0;">
        <table class="td-pad" style="min-height:150px;">
            <?php if(isset($ecs))echo $ecs; ?>
        </table>
    </td>
    <td align="center" valign="middle"><?php if(isset($ecs_cost))echo number_format((float)$ecs_cost,2,'.',',');?></td>
    <td align="center" valign="middle"><?php if(!empty($status)){
         foreach($status as $st){?>
    <span style="background-color:<?php echo $st->bar_color_code;?>; color:<?php echo $st->body_color_code ?>;">
     <?php echo $st->booking_status; }}?></span>
    </td>
    
    
  </tr>
         <?php 
            }}}?>
</tbody>
</table>		 
</div>		
											
