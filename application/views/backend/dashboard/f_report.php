<!-- BEGIN PAGE CONTENT-->

<div class="row">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">            
            <div class="portlet-body form">
                <form role="form">
                    <div class="form-body">
                    	<div class="form-group">
                        	<div class="row">
                                <div class="col-md-6">
                                	<div class="row">
                                        <label class="col-md-3 control-label">Small Select</label>
                                        <div class="col-md-4">
                                            <select class="form-control input-sm">
                                                <option>Option 1</option>
                                                <option>Option 2</option>
                                                <option>Option 3</option>
                                                <option>Option 4</option>
                                                <option>Option 5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control input-sm">
                                                <option>Option 1</option>
                                                <option>Option 2</option>
                                                <option>Option 3</option>
                                                <option>Option 4</option>
                                                <option>Option 5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                	<div class="row">
                                        <label class="col-md-3 control-label">Small Select</label>
                                        <div class="col-md-4">
                                            <select class="form-control input-sm">
                                                <option>Option 1</option>
                                                <option>Option 2</option>
                                                <option>Option 3</option>
                                                <option>Option 4</option>
                                                <option>Option 5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control input-sm">
                                                <option>Option 1</option>
                                                <option>Option 2</option>
                                                <option>Option 3</option>
                                                <option>Option 4</option>
                                                <option>Option 5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                        	<div class="row">
                                <div class="col-md-6">
                                	<div class="row">
                                        <label class="col-md-3 control-label">date</label>
                                        <div class="col-md-4">
                                            <input class="form-control form-control-inline input-sm date-picker" type="text" value="" size="16">
											<span class="help-block">to</span>
                                        </div>
                                        <div class="col-md-4">
                                            <input class="form-control form-control-inline input-sm date-picker" type="text" value="" size="16">
											<span class="help-block">form</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                	<div class="row">
                                        <label class="col-md-3 control-label">Small Select</label>
                                        <div class="col-md-4">
                                            <select class="form-control input-sm">
                                                <option>Option 1</option>
                                                <option>Option 2</option>
                                                <option>Option 3</option>
                                                <option>Option 4</option>
                                                <option>Option 5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control input-sm">
                                                <option>Option 1</option>
                                                <option>Option 2</option>
                                                <option>Option 3</option>
                                                <option>Option 4</option>
                                                <option>Option 5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                        	<div class="row">
                                <div class="col-md-6">
                                	<label>zxdvv</label>
                                	<div class="row">                                        
                                        <div class="col-md-4">
                                            <input class="form-control form-control-inline input-sm" type="text" value="" size="16">
											<span class="help-block">to</span>
                                        </div>
                                        <div class="col-md-4">
                                            <input class="form-control form-control-inline input-sm" type="text" value="" size="16">
											<span class="help-block">form</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                	<label>Small Select</label>
                                	<div class="row">                                        
                                        <div class="col-md-4">
                                            <select class="form-control input-sm">
                                                <option>Option 1</option>
                                                <option>Option 2</option>
                                                <option>Option 3</option>
                                                <option>Option 4</option>
                                                <option>Option 5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control input-sm">
                                                <option>Option 1</option>
                                                <option>Option 2</option>
                                                <option>Option 3</option>
                                                <option>Option 4</option>
                                                <option>Option 5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Checkboxes</label>
                            <div class="checkbox-list">
                                <label>
                                <input type="checkbox"> Checkbox 1 </label>
                                <label>
                                <input type="checkbox"> Checkbox 2 </label>
                                <label>
                                <input type="checkbox" disabled> Disabled </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Inline Checkboxes</label>
                            <div class="checkbox-list">
                                <label class="checkbox-inline">
                                <input type="checkbox" id="inlineCheckbox1" value="option1"> Checkbox 1 </label>
                                <label class="checkbox-inline">
                                <input type="checkbox" id="inlineCheckbox2" value="option2"> Checkbox 2 </label>
                                <label class="checkbox-inline">
                                <input type="checkbox" id="inlineCheckbox3" value="option3" disabled> Disabled </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Radio</label>
                            <div class="radio-list">
                                <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked> Option 1</label>
                                <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2"> Option 2 </label>
                                <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled> Disabled </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Inline Radio</label>
                            <div class="radio-list">
                                <label class="radio-inline">
                                <input type="radio" name="optionsRadios" id="optionsRadios4" value="option1" checked> Option 1 </label>
                                <label class="radio-inline">
                                <input type="radio" name="optionsRadios" id="optionsRadios5" value="option2"> Option 2 </label>
                                <label class="radio-inline">
                                <input type="radio" name="optionsRadios" id="optionsRadios6" value="option3" disabled> Disabled </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions right">
                        <button type="submit" class="btn blue">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row all_bk">
  <div class="col-md-12"> 
    <!-- BEGIN SAMPLE TABLE PORTLET-->    
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="glyphicon glyphicon-bed"></i>List of All Rooms </div>
        
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
							 
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            <div class="col-md-12">
              <div class="btn-group pull-right">
                <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu pull-right">
                  <li> <a href="javascript:;"> Print </a> </li>
                  <li> <a href="javascript:;"> Save as PDF </a> </li>
                  <!--<li> <a href="javascript:;"> Export to Excel </a> </li>-->
                </ul>
              </div>
            </div>
          </div>
        </div>         
        <table class="table table-striped table-bordered table-hover" id="sample_1">
          <thead>
            <tr>
              <th scope="col">Day</th>
              <th scope="col">Room Count</th>
              <th scope="col">Occupancy %</th>
              <th scope="col">Avg.Daily Rate (Incl.Inclusion)</th>
              <th scope="col">Avg.Daily Rate (Excl.Inclusion)</th>
              <th scope="col">Room Rent</th>
              <th scope="col">Room Inclusion</th>
              <th scope="col">Total Taxes</th>
              <th scope="col">Direct Sale</th>
              <th scope="col">Direct Sale Tax</th>
              <th scope="col">Gross total</th>
            </tr>
          </thead>
          
          <tbody>
          	<tr>
              <?php ?>
            	<td align="center"></td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
            </tr>
            <tr>
            	<td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
            </tr>
            <tr>
            	<td align="center">Debayan</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="glyphicon glyphicon-bed"></i>List of All Rooms </div>        
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" class="config">
            </a>
            <a href="javascript:;" class="reload">
            </a>
            <a href="javascript:;" class="remove">
            </a>
        </div>		 
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">            
            <div class="col-md-12">
              <div class="btn-group pull-right">
                <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu pull-right">
                  <li> <a href="javascript:;"> Print </a> </li>
                  <li> <a href="javascript:;"> Save as PDF </a> </li>
                  <li> <a href="javascript:;"> Export to Excel </a> </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="sample_editable_2">
        	<thead>
            <tr>
              <th scope="col">Day</th>
              <th scope="col">Room Count</th>
              <th scope="col">Occupancy %</th>
              <th scope="col">Avg.Daily Rate (Incl.Inclusion)</th>
              <th scope="col">Avg.Daily Rate (Excl.Inclusion)</th>
              <th scope="col">Room Rent</th>
              <th scope="col">Room Inclusion</th>
              <th scope="col">Total Taxes</th>
              <th scope="col">Direct Sale</th>
              <th scope="col">Direct Sale Tax</th>
              <th scope="col">Gross total</th>
            </tr>
          </thead>
            <tbody>
            <tr>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
            </tr>
            <tr>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
            </tr>
            <tr>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
                <td align="center">asf</td>
            </tr>
            </tbody>
		</table>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT-->


