<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}
</script>
<?php
  if (isset($get_finance_setting) && $get_finance_setting) {
    $currency = $get_finance_setting->fin_curr_initial;
    $profit_c = $get_finance_setting->fin_profit_center;
    $cih = $get_finance_setting->fin_cash_in_hand;
    $cib = $get_finance_setting->fin_cash_bank;
    $tax = $get_finance_setting->fin_tax_applied;
    $tax_fields = $get_finance_setting->tax_fields;
    $new_tx_ary = explode(",",$tax_fields);
    $ary_siz = sizeof($new_tx_ary);
  }

  $taxation = $this->dashboard_model->get_all_tax();
?>

<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

    $form = array(
        'class'       => '',
        'id'        => 'form1',
        'method'      => 'post'
    );

    echo form_open_multipart('dashboard/add_tax_details',$form);

    ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Tax</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
          	<div class="col-md-12">
              <div class="form-group form-md-line-input">
                  <input type="text" class="form-control" name="tax_name_insert" id="tax_name_insert" required="required" placeholder="Tax Name *">
                  <label></label>
            	  <span class="help-block">Tax Name *</span>
              </div>
              <div class="form-group form-md-line-input">
                  <select name="tax_type_insert" id="tax_type_insert" class="form-control bs-select" required>
                    <option value="" disabled="" selected="">Tax Type</option>
                    <option value="Rs.">Fixed(Rs.)</option>
                    <option value="%">Variable</option>
                  </select>
                  <label></label>
            	  <span class="help-block">Tax Type *</span>
              </div>
              <div class="form-group form-md-line-input">
                  <input type="text" class="form-control" name="tax_value_insert" id="tax_value_insert" required="required" placeholder="Tax Value *">
                  <label></label>
            	  <span class="help-block">Tax Value *</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php form_close(); ?>
</div>
<!-- 17.11.2015-->
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <!-- 17.11.2015-->
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i><span class="caption-subject bold uppercase">Master Setting</span></div>
    <div class="actions">
      <a class="btn btn-circle default btn-outline btn-sm"  data-toggle="modal" href="#responsive"> <i class="fa fa-plus"></i>Add Tax </a>
    </div>
  </div>
  <div class="portlet-body form">
    <?php

      $form = array(
          'class' 			=> '',
          'id'				=> 'form',
          'method'			=> 'post',								
      );
      echo form_open_multipart('dashboard/finance_setup',$form);
      ?>
    <div class="form-body"> 
      <div class="row">
        <div class="col-md-4">
          <div class="form-group form-md-line-input" id="gtype">
            <select class="form-control bs-select" id="form_control_2" name="c_code" >
              <option value="" disabled="disabled" selected="selected">Select Currency</option>
              <option <?php if ($currency == "Rs."){?>
                    selected
                  <?php } ?> value="Rs.">Rupees(Rs.)</option>
              <option <?php if ($currency == "$") {
                    ?> selected <?php
                  }
                  ?> value="$">Dollar($)</option>
              <option <?php if ($currency == "€") {
                    ?> selected <?php
                  }
                  ?> value="€">Euro (€)</option>
              <option
                  <?php if ($currency == "£") {
                    ?> selected <?php
                  }
                  ?> value="£">Pound (£)</option>
            </select>
            <label></label>
            <span class="help-block">Select Currency</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input" id="gtype">
            <select class="form-control bs-select" id="form_control_2" name="c_pro_centr" >
              <option value="" selected="selected" disabled="disabled">Select Profit Center</option>
              <?php 
                    foreach ($get_profit_center as $get_profit_center) {
                      ?>
              <option <?php 
                          if ($profit_c == $get_profit_center->id) {
                            ?> selected <?php
                          }
                        ?> value="<?php echo $get_profit_center->id; ?>"><?php echo $get_profit_center->profit_center_location; ?></option>
              <?php
                    }
                  ?>
            </select>
            <label></label>
            <span class="help-block">Select Profit Center</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" value="<?php echo $cih; ?>" name="c_in_hand" onkeypress=" return onlyNos(event, this);" required="required" placeholder="Cash In Hand *">
            <label></label>
            <span class="help-block">Cash In Hand *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input autocomplete="off" type="text" class="form-control" id="form_control_1" value="<?php echo $cib; ?>" name="c_in_bank" onkeypress=" return onlyNos(event, this);" required="required" placeholder="Cash In Bank *">
            <label></label>
            <span class="help-block">Cash In Bank *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input" id="gtype">
            <select class="form-control bs-select" onchange="tax_for_change(this.value)" id="tax_chng" name="c_tax" >
              <option value=""></option>
              <option <?php if ($tax == '1') {
                ?>selected <?php
              } ?> value="1">Yes</option>
              <option <?php if ($tax == '0') {
                ?>selected<?php
              } ?> value="0">No</option>
            </select>
            <label></label>
            <span class="help-block">Tax Applied?</span> </div>
        </div>
        <div class="col-md-12">
            <div id="tx" style="display: none;" class="form-group form-md-line-input">
              <div class="md-checkbox-inline">
                <?php $i=1; ?>
                <?php 
                if(isset($taxation) && $taxation){
                foreach($taxation as $tax){?>
                <?php 
                  $ttype = $tax->tax_type;
                  if ($ttype == 'Rs.') {
                    $ttwrt = $tax->tax_type .' '.$tax->tax_value;
                  }
                  else{
                    $ttwrt = $tax->tax_value.' '.$tax->tax_type;
                  }
                ?>
                <div class="md-checkbox">
                  <input class="md-check" id="checkbox<?php echo $tax->id;?>" type="checkbox" name="tax[]" value="<?php echo $tax->id;?>"
                  <?php 
                    for ($i=0; $i < $ary_siz ; $i++) { 
                      if ($new_tx_ary[$i] == $tax->id) {
                        ?>
                          checked
                        <?php
                      }
                    }
                  ?>
                  />
                  <label for="checkbox<?php echo $tax->id;?>"> <span></span> <span class="check"></span> <span class="box"></span> <?php echo $tax->tax_name.'('.$ttwrt.')';?></label>
                </div>
                <?php $i++; ?>
                <?php }}?>
              </div>
            </div>
        </div>
      </div>
    </div>
    <input type="hidden" name="flag" value="<?php if (!empty($cih)){echo "1";}else{ echo "0";} ?>">
    <div class="form-actions right">
      <?php if (!empty($cih)) {
            ?>
      <button type="submit" class="btn blue" >Update</button>
      <?php
          }
          else{
            ?>
      <button type="submit" class="btn blue" >Submit</button>
      <?php
          }
          ?>
      <!-- 18.11.2015 onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php 
    form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>
<script type="text/javascript">
  function tax_for_change(xc){
    if (xc == '1') {
      document.getElementById('tx').style.display = 'block';
    }
    else{
      document.getElementById('tx').style.display = 'none';
    }
  }

  $( document ).ready(function() {
    var x =document.getElementById('tax_chng').value;
    if (x == '1') {
      document.getElementById('tx').style.display = 'block';
    }
    else{
      document.getElementById('tx').style.display = 'none';
    }
  });
</script>