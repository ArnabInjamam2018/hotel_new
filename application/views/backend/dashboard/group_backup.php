<div class="row">
  <div class="col-md-12">
    <?php foreach ($details2 as $key3 ) { $groupID = $key3->id;


      ?>

    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase"> Group Id: <?php echo "HM0".$this->session->userdata('user_hotel')."GRP00".$key3->id; ?> </span> 
        </div>
        <?php
        foreach ($details3 as $det ) {
          # code...
          if($det->booking_status_id =='4'){

            $txtcol="#0fd906";
            $bckcol="#c7F6c5";
            $tx="Confirmed";

          }elseif ($det->booking_status_id=='5') {
            # code...
               $txtcol="#39B9A1";
            $bckcol="#EBF8F5";
            $tx="Checked In";
          }elseif ($det->booking_status_id=='6') {
            # code...
                 $txtcol="#297CAC";
            $bckcol="#E9F1F6";
            $tx="Checked Out";
          }
        }
         ?>
         <div class="actions">
           <a style="background-color:<?php echo $bckcol; ?>; color:<?php echo $txtcol; ?>; font-size: 18px; padding: 0 5px; text-decoration:none;">
 <?php echo $tx; ?>
    </a>
    </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body form-horizontal form-row-sepe">
          <div class="form-body">
          <div class="row">
           <div class="col-md-9">
            <div class="form-group">
              <label class="col-md-3">Profit Center:</label>
              <label class="col-md-9"> <?php echo $key3->p_center; ?> </label>

            </div>		  
            <div class="form-group">
              <label class="col-md-3">Group Name:</label>
              <label class="col-md-9"> <?php echo $key3->name; ?> </label>
            </div>
            <div class="form-group">
              <label class="col-md-3">Number of Guest:</label>
              <label class="col-md-9"> <?php echo $key3->number_guest; ?> </label>
            </div>
            <div class="form-group">
              <label class="col-md-3">Number of Room:</label>
              <label class="col-md-9"> <?php echo $key3->number_room; ?> </label>
            </div>
            <div class="form-group">
              <label class="col-md-3">Start Date:</label>
              <label class="col-md-9"> <?php echo date("dS M Y",strtotime($key3->start_date)); ?> </label>
            </div>
            <div class="form-group">
              <label class="col-md-3">End Date:</label>
              <label class="col-md-9"> <?php echo  date("dS M Y",strtotime($key3->end_date)); ?> </label>
            </div>
            </div>
             <div class="col-md-3 text-center">
             <div class="row">
                  <div class="col-md-12">
                  <?php 

                  foreach ($details3 as $dum ) {
                 
                    $b_id=$dum->booking_id;
                  }

                  $guest = $this->dashboard_model->get_booking_details($b_id); 
                  ?>
                    <?php if(isset($guest->g_photo_thumb) && $guest->g_photo_thumb != '') { ?>
                    <img src="<?php echo base_url();?>upload/guest/<?php echo $guest->g_photo_thumb;?>" style="height:95px;"/>
                    <?php } else { ?>
                    <img src="<?php echo base_url();?>upload/guest/no_images.png" style="height:95px;"/>
                    <?php } ?>
                  </div>
                  
                </div>
             </div>
            </div>
          </div>
        </div>
     
      </div>

    </div>
    <?php } ?>
    <div class="portlet light bordered">
    	<div class="portlet-title">
        	<div class="caption font-green-haze"> 
            	<span class="caption-subject bold uppercase">Reservation Details</span> 
            </div>
        </div>
        <div class="portlet-body form">
        	<div class="form-body form-horizontal form-row-sepe">
            	<div class="form-body">
                    <div class="table-scrollable" > 
                      <!-- <form type="post" target="<?php echo base_url(); ?>dashboard/add_group_booking" >-->
                      <table class="table table-striped table-hover table-bordered mass-book" id="myTable">
                        <tbody>
                          <tr>
                            <th> # </th>
                            <th> Type </th>
                            <th> Room </th>
                            <th> Guest Name </th>
                            <th> Chk in </th>
                            <th> Chk Out </th>
                            <th> Days </th>
                            <th> Status </th>
                            <th> F_Plan </th>
                            <th> A_R_R </th>
                            <th> A_F_I </th>
                            <th> C_R_R </th>
                            <th> C_F_I </th>
                            <th> Adlt </th>
                            <th> Kid </th>
                            <th> T_R_R </th>
                            <th> Serivce Tax </th>
                            <th> Serivce Charge </th>
                            <th> Room Vat </th>
                            <th> T_F_I </th>
                            <th>Food VAT </th>
                            <th> P_D_R </th>
                            <th> Price </th>
                            <th> E_C_S </th>
                          </tr>
                          <?php $i=0; $total=0; if($details){ foreach ($details as $key  ) { 

                            $booking_id= $details3[$i]->booking_id;


                                $poses=$this->dashboard_model->all_pos_booking($booking_id);
                                $pos_amount=0;
                                 if($poses){
                                     foreach ($poses as $key ) {

                                        # code...
                                        $pos_amount=$pos_amount+$key->total_due;
                                      }}


                                      $service_amount=$details3[$i]->service_price;
                                       $charge_amount=$details3[$i]->booking_extra_charge_amount;

                      
                   



                            $i++;
                            
                            ?>
                          <tr>
                            <td><?php echo $key->id; ?></td>
                            <td><?php echo $key->type; ?></td>
                            <td><a href="<?php echo base_url(); ?>dashboard/booking_edit?b_id=<?php echo $booking_id; ?>"><?php echo $key->roomNO; ?></a></td>
                            <td><?php echo $key->gestName; ?></td>
                            <td><?php echo $key->startDate; ?></td>
                            <td><?php echo $key->endDate; ?></td>
                            <td><?php echo $key->days; ?></td>
                            <td><?php echo "Confirmed"; ?></td>
                            <td><?php echo $key->mealPlan; ?></td>
                            <td><?php echo $key->adultRoomRent; ?></td>
                            <td><?php echo $key->afi; ?></td>
                            <td><?php echo $key->childRoomRent; ?></td>
                            <td><?php echo $key->cfi; ?></td>
                            <td><?php echo $key->adultNO; ?></td>
                            <td><?php echo $key->childNO; ?></td>
                            <td><?php echo $key->trr; ?></td>
                            <td><?php echo $key->room_st; ?></td>
                            <td><?php echo $key->room_sc; ?></td>
                            <td><?php echo $key->room_vat; ?></td>
                            <td><?php echo $key->tfi; ?></td>
                            <td><?php echo $key->food_vat; ?></td>
                            <td><?php echo $key->pdr; ?></td>
                            <td><?php echo $key->price; ?></td>
                            <td><?php echo $key->service; ?></td>
                          </tr>
                          <?php $total=$total+ $key->price+ $key->service; } }?>
                        </tbody>
                      </table>
                    </div>

                    <h4 style="padding:10px 0 5px">Extra Charges Added</h4>
                    <div class="table-scrollable" >
                      <?php
                
                                    foreach ($details2 as $groups ) {
                                      # code...
                
                                       $charge_string=$groups->charges_id;
                
                                        $charge_total_price=$groups->charges_cost;
                
                
                                    }
                
                                     
                                            $charge_sum=0;
                                if($charge_string!=""){
                                    ?>
                      <table class="table table-striped table-hover table-bordered mass-book" width="100%">
                        <thead>
                          <tr>
                            <th width="5%" align="left"> No </th>
                            <th width="33%"> Product / Service Name </th>
                            <!--<th class="hidden-480">
                                         Amount
                                    </th>-->
                            <th width="10%"> Quantity </th>
                            <th width="16%"> Unit Price </th>
                            <th width="10%"> Amount </th>
                            <th width="16%"> Total Tax </th>
                            <th width="10%" align="right"> Total </th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                
                                    $charge_array=explode(",", $charge_string);


                
                
                
                                    //print_r($service_array); 
                
                                    for ($i=1; $i < sizeof($charge_array) ; $i++) { 
                                        # code...
                
                                        $charges=$this->dashboard_model->get_charge_details($charge_array[$i]);
                
                
                
                
                                         
                
                                   
                                        # code...
                                    
                
                               ?>
                          <tr>
                            <td ><?php echo $charges->crg_id; ?></td>
                            <td ><?php echo $charges->crg_description; ?></td>
                            <td ><?php echo $charges->crg_quantity; ?></td>
                            <td ><?php echo $charges->crg_unit_price; ?></td>
                            <td ><?php echo $charge_total= ($charges->crg_unit_price * $charges->crg_quantity); ?></td>
                            <td ><?php echo $charge_tax= ($charges->crg_tax ); 
                                        echo "%";?></td>
                            <td ><?php echo $charge_grand_total=$charges->crg_total; ?></td>
                          </tr>
                          <?php $charge_sum=$charge_sum+$charge_grand_total;  } ?>
                        </tbody>
                      </table>
                      <?php } ?>
                    </div>
                      <div class="portlet light bordered">
   <h4 style="text-align:center; border-top:1px solid #eee; padding:10px 0 25px;"><strong>Add Charges:</strong></h4>
                <table class="table table-striped table-hover" >
                  <thead>
                    <tr>
                      <th width="25%"> Description </th>
                      <th width="15%"> Qty </th>
                      <th width="20%"> Unit Price </th>
                      <th width="15%"> Tax % </th>
                      <th width="20%"> Total </th>
                      <th width="5%"> Action </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="form-group"><input id="c_name" type="text" value="" class="form-control input-sm"></td>
                      <td class="form-group"><input id="c_quantity" type="text" value="" class="form-control input-sm"></td>
                      <td class="hidden-480 form-group"><input onblur="calculation(this.value)" id="c_unit_price" type="text" value="" class="form-control input-sm"></td>
                      <td class="hidden-480 form-group"><input id="c_tax" onblur="calculation2(this.value)" type="text" value="" class="form-control input-sm"></td>
                      <td class="hidden-480 form-group"><input id="c_total" type="text" value="" class="form-control input-sm"></td>
                      <td><button class="btn green" type="button" id="addCharge"><i class="fa fa-plus" aria-hidden="true"></i></button>
                        <br></td>
                    </tr>                
                  </tbody>
                </table>
  </div>
                    <div class="table-scrollable" >                      
                      <table class="table table-striped table-hover table-bordered" width="100%">
                        <tbody>                          
                          <tr>
                            <td width="90%" align="right">Total Amount:</td>
                            <td><input class="form-control input-sm" type="text" value="<?php echo $grand=$charge_sum+$total; ?>" id="totalAmount" readonly></td>
                          </tr>
                          	 <?php 
                                      $amountPaid = $this->dashboard_model->get_amountPaidByGroupID($_GET['group_id']);
                                                                                            if(isset($amountPaid) && $amountPaid) {
                                                                                                $paid=$amountPaid->tm;
                                                                                             }
                                                                                             else{
                                                                                              $paid=0;
                                                                                            }
                                    
                                    
                                                                                        ?>

                          <tr>
                            <td width="90%" align="right">Discount:</td>
                            <td>
                            	<div class="input-group">
											<input type="text" id="discountVal" class="form-control input-sm" value="<?php foreach ($details2 as $key3 ) { echo $ad = $key3->additional_discount; }?>">
											<span class="input-group-btn">
											<button type="button" id="btnSubmit" class="btn blue btn-sm" style="padding:7px;"><i class="fa fa-check"></i></button>
											</span>
										</div>
                            </td>
                          </tr>
                          <tr>
                            <td width="90%" align="right">Total Payable Amount:</td>
                            <td><input class="form-control input-sm" type="text" value="<?php echo $payAmount = $grand-$ad;?>" id="totalPayableAmount" readonly></td>
                          </tr>
						 <tr>
                            <td width="90%" align="right">Total Due:</td>
                            <td><input class="form-control input-sm" type="text" value="<?php echo $payAmount-$paid;?>" id="totalDue" readonly></td>
                          <input class="form-control input-sm" type="hidden" value="<?php echo $payAmount-$paid;?>" id="totalDue1">
						  </tr>
						  
						  
                        </tbody>
                      </table>
                    </div>
                </div>
                <div class="form-actions right">
        			<button onclick=" return bill();" class="btn btn-primary" id="add_bill">Generate Bill</button>
        			<button onclick=" return bill2();" class="btn btn-primary" id="add_bill">Generate Bill 2</button>
                    <script type="text/javascript">
                       function bill(){
                        window.location="<?php echo base_url(); ?>dashboard/booking_group_pdf?group_id=<?php echo $_GET["group_id"]; ?>";
                       }
                       function bill2(){
                        window.location="<?php echo base_url(); ?>dashboard/booking_group_pdf2?group_id=<?php echo $_GET["group_id"]; ?>";
                       }
                    </script>
                </div>  	
            </div>
    	</div>
	</div>

    <div class="portlet light bordered">
        <div class="portlet-title">
        	<div class="caption font-green-haze"> 
            	<span class="caption-subject bold uppercase">Individual Charges:</span> 
            </div>
        </div>
        <div class="portlet-body form">
        	<div class="form-body form-horizontal form-row-sepe">
                <table class="table table-striped table-hover table-bordered mass-book" >
                  <thead>
                    <tr>
                      <th width="25%"> Room Number </th>
                      <th width="15%"> Booking Id </th>
                      <th width="15%"> Extra charge Amt </th>
                      <th width="15%"> Extra Service Amt </th>
                      <th width="15%"> POS Amt </th>
                       <th width="15%"> Amt Pending </th>
                    
                    </tr>
                  </thead>
                  <tbody>
                   <?php $tot_due=0; foreach ($details3 as $dt ) {
                      # code...
                     ?>
                    <tr>
                   
                      <td class="form-group"><?php echo $dt->room_no ?></td>
                      <td class="form-group"><?php  $booking_id2=$dt->booking_id; 
                          echo "HM0".$this->session->userdata("user_hotel")."00".$dt->booking_id;
                      ?></td>
                      <td class="form-group"><?php echo $dt->booking_extra_charge_amount ?></td>
                      <td class="form-group"><?php echo $dt->service_price ?></td>
                      <td class="form-group"><?php 
                           $poses=$this->dashboard_model->all_pos_booking($booking_id2);
                                $pos_amount=0;
                                 if($poses){
                                     foreach ($poses as $key ) {

                                        # code...
                                        $pos_amount=$pos_amount+$key->total_due;
                                      }}
                                      echo $pos_amount;

                      ?></td>
                      <td class="form-group"><?php 

                      $amountPaid = $this->dashboard_model->get_amountPaidByBookingID($booking_id2);

                         if(isset($amountPaid) && $amountPaid) {
                    $paid=$amountPaid->tm;
                  }
                  else{
                    $paid=0;
                  }



                    echo $due=$dt->booking_extra_charge_amount +$dt->service_price+$pos_amount -$paid;
                    $tot_due=$tot_due+$due;

                    


                      ?></td>
                      
                     
                    </tr>   
                    <?php } 

                       foreach ($details2 as $key3 ) { $groupID = $key3->id;

                       echo $this->dashboard_model->update_indi($tot_due,$groupID);
                    }
      
                    ?>             
                  </tbody>
                </table>
            </div>
    	</div>
    </div>

   <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Transactions</span> </div>
      </div>
      <!--<h4 style="color:red;margin-top:20px;">Not Availabe</h4>-->
      <div class="portlet-body form">

  <table class="table table-striped table-hover" width="100%">
  <thead>
    <tr style="">
      <th align="center" valign="middle" > # </th>
      <th align="center" valign="middle" > Transaction Date </th>
      <th align="center" valign="middle" > Payment Mode </th>
      <th align="center" valign="middle" > Bank name </th>
      <th align="right" valign="middle"> Amount </th>
    </tr>
  </thead>
  <tbody>
    <?php  if($transaction){ foreach ($transaction as $keyt ) { ?>
    <tr style="background: #F2F2F2">
      <td align="center" valign="middle"><?php echo $keyt->t_id; ?></td>
      <td align="center" valign="middle"><?php echo $keyt->t_date; ?></td>
      <td align="center" valign="middle"><?php echo $keyt->t_payment_mode; ?></td>
      <td align="center" valign="middle"><?php echo $keyt->t_bank_name; ?></td>
      <td align="right" valign="middle"><?php echo $keyt->t_amount; ?></td>
    </tr>
    <?php  } }?>
    
  </tbody>
</table>
</div>
</div>


    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Take Payment</span> </div>
      </div>
      <!--<h4 style="color:red;margin-top:20px;">Not Availabe</h4>-->
      <div class="portlet-body form">
        <div class="form-body form-row-sepe">
          <div class="form-body">
            <div class="row" id="tab2">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Group Id<span class="required">*</span></label>
                  <!--<select name="country" id="country_list" class="form-control" placeholder=" Booking Type"> 
                                                                       
                                                                            
                                                                            <option value="">----- Select Booking Id -----</option>
                                                                            <option value="">fdsdsdsds</option>
                                                                            
                                                                            
                                                                        </select>-->
                  <input type="text" class="form-control input-sm" id="" name="card_number" value="<?php echo $_GET['group_id'] ?>" 
                                                                        disabled="disabled"/>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Total Amount Paybale<span class="required">*</span></label>
                  <!--<select name="country" id="country_list" class="form-control" placeholder=" Booking Type"> 
                                                                       
                                                                            
                                                                            <option value="">----- Select Booking Id -----</option>
                                                                            <option value="">fdsdsdsds</option>
                                                                            
                                                                            
                                                                        </select>-->
                  <input type="text" required class="form-control input-sm" id="" name="card_number" value="<?php echo $payAmount; ?>" 
                                                                        disabled="disabled"/>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Due Amount<span class="required">*</span></label>
                  <?php 
                      $amountPaid = $this->dashboard_model->get_amountPaidByGroupID($_GET['group_id']);
                                                                            if(isset($amountPaid) && $amountPaid) {
                                                                                $paid=$amountPaid->tm;
                                                                             }
                                                                             else{
                                                                              $paid=0;
                                                                            }
                    
                    
                                                                        ?>
                  <input type="text" class="form-control input-sm" id="due_amount"  name="card_number" value="<?php echo $due= $payAmount-$paid; ?>" 
                                                                        disabled="disabled"/>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Amount <span class="required"> * </span> </label>
                  <input type="text" id="add_amount" class="form-control input-sm" required name="card_number" placeholder="Give Amount" onblur="check_amount();"  required="required" />
                  <input type="hidden" id="booking_id" value="">
                  <input type="hidden" id="booking_status_id" value="">
                </div>
                <script type="text/javascript">
                                                                    function check_amount()
                    {
                    var amount = $("#add_amount").val();
                    //alert(amount);
                    var due_amount  = $("#due_amount").val();
                    //alert(due_amount);
                    if(parseInt(amount) > parseInt(due_amount))
                    {
                    //alert("The amount should be less than the due amount Rs: "+due_amount);
                    swal({
                                                title: "Amount Should Be Smaller",
                                                text: "(The amount should be less than the due amount Rs: +due_amount)",
                                                type: "warning"
                                            },
                                            function(){
                                                //location.reload();
                                            });
                    $("#add_amount").val("");
                    return false;
                    }
                    else{
                    return true;
                    }
                    }
                    
                                                                </script> 
              </div>
              <div class="col-md-3">
                <label>Profit Center <span class="required"> * </span> </label>
                <select name="" class="form-control" id="p_center">
                  <option value="">----- Select Profit Center -----</option>
                  <option value="mondermoni">Mondermoni</option>
                  <option value="kolkata">Kolkata</option>
                  <option value="digha">Digha</option>
                </select>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Payment Mode <span class="required"> * </span> </label>
                  <select name="country" class="form-control" placeholder=" Booking Type" id="pay_mode" onChange="paym(this.value);">
                    <option value="">----- Select Payment Mode -----</option>
                    <option value="cashs">Cash</option>
                    <option value="cards">Debit /Credit Card</option>
                    <option value="funds">Fund transfer</option>
                  </select>
                </div>
              </div>
              <div id="cards" style="display:none;">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Bank Name <span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="bankname" >
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Card NO<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="card_no" >
                  </div>
                </div>
              </div>
              <div id="fundss" style="display:none;">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Bank Name <span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="bankname" >
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>A/C No<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="ac_no" >
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>IFSC Code<span class="required"> * </span> </label>
                    <input type="text" class="form-control input-sm" placeholder="Bank name" id="ifsc" >
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-actions right">
            <button onclick=" return ajax_hotel_submit_grp();" class="btn btn-primary" id="add_submit">Submit</button>
          </div>
        </div>
      </div>
    </div>
    <?php foreach ($details3 as $key1 ) {

      ?>
    <input type="hidden" id="booking_id" value="<?php echo $key1->booking_id; ?>">
    </input>
    <?php
    } ?>
    <script type="text/javascript">
    function ajax_hotel_submit_grp()
    {
        //alert($("#add_amount").val());
        var group_id = <?php echo $_GET['group_id'] ?>;
        var booking_status_id = 4;
        var p_center = $("#p_center").val();
        //alert(p_center);
       // return false;
        var add_amount = $("#add_amount").val();
        var pay_mode   = $("#pay_mode").val();
        var bankname   = $("#bankname").val();
        var payment_status = "partial";
        if($("#add_amount").val() == null || $("#add_amount").val() ==0 )
        {
          //alert('Error');
          swal({
                                    title: "Error",
                                    text: "An Error Occurd",
                                    type: "warning"
                                },
                                function(){
                                    //location.reload();
                                });
          return false;
        }
        else{
        jQuery.ajax(
        {
          type: "POST",
          url: "<?php echo base_url(); ?>bookings/add_booking_transaction_grp",
          dataType: 'json',
          data: {t_group_id:group_id,t_booking_status_id:booking_status_id,p_center:p_center,t_amount:add_amount,t_payment_mode:pay_mode,t_bank_name:bankname,t_status:payment_status },
          success: function(data){
            //alert('Add Successfull');
            swal({
                                    title: "Add Successfull",
                                    text: "Your Payment Is Taken",
                                    type: "success"
                                },
                                function(){
                  
            location.reload();
                                    
                                });
          
          }
        });
        }
        return false;
    }
	
$("#btnSubmit").click(function(){
	var discountVal = $("#discountVal").val();
	var totalAmount = $("#totalAmount").val();
	var totalPayableAmount = $("#totalPayableAmount").val();
	var totalDuee = $("#totalDue1").val();
	//alert(totalDuee);
	var amtDue = parseInt(totalDuee) - parseInt(discountVal);
	var amtPay = parseInt(totalAmount) - parseInt(discountVal);
	//alert(amtDue);
	var groupID = '<?php echo $groupID; ?>';
	$.ajax({				
		url: '<?php echo base_url(); ?>dashboard/getAdditionalDiscount',
		type: "GET",
		dataType: "json",
		data: {discountVal:discountVal,groupID:groupID},
		success: function(data){		
		  $("#totalPayableAmount").val(amtPay);
          $("#totalDue").val(amtDue);		  
		},
	}); 
});	
  </script> 
  </div>
</div>
<script type="text/javascript">
  $("#addCharge").click(function(){
  var name= document.getElementById('c_name').value;
              var quantity= document.getElementById('c_quantity').value;
             var unit_price= document.getElementById('c_unit_price').value;
             var tax= document.getElementById('c_tax').value;
             var total= document.getElementById('c_total').value;

            // alert(name+quantity+unit_price+tax+total);


            var booking_id=  <?php echo $_GET['group_id'] ?>;

           

            jQuery.ajax(
                {
                    type: "POST",
                    url: "<?php echo base_url(); ?>dashboard/add_charge_to_booking_group",
                    dataType: 'json',
                    data: {booking_id:booking_id,name:name,quantity:quantity,unit_price:unit_price, tax:tax, total:total },
                    success: function(data){
                        alert(data.return2);
                        location.reload();
                    }
                });

  
}); 


function calculation(value) {
  var a=document.getElementById("c_quantity").value;
  var total=parseInt(a)*parseInt(value);
  document.getElementById("c_total").value=total;
  
}
function calculation2(value) {
  var a=document.getElementById("c_total").value;
  var total=parseInt(a)+(parseInt(a)*(parseInt(value)/100));
  document.getElementById("c_total").value=total;

}
</script>
 
    <div class="portlet light bordered">
      <div class="portlet-body form">
        <div class="form-body form-horizontal form-row-sepe">
          <div class="form-body">
            <div class="row">
              <div class="col-md-6">
              
                <div class="form-group">
                  <label class="col-md-6">Booking Amount:</label>
                  <label class="col-md-6"> <i class="fa fa-inr"><?php echo $total; ?></i>
                   
                  </label>
                </div>
                <div class="form-group">
                  <label class="col-md-6">Service Amount:</label>
                  <label class="col-md-6"><i class="fa fa-inr">0</i> </label>
                </div>
                <div class="form-group">
                  <label class="col-md-6">Charge Amount:</label>
                  <label class="col-md-6"><i class="fa fa-inr"><?php echo $charge_sum; ?></i> </label>
                </div>
                <div class="form-group">
                  <label class="col-md-6">POS Amount:</label>
                  <label class="col-md-6"><i class="fa fa-inr"><?php echo $pos_amount; ?></i>
                  
                  </label>
                </div>
                <div class="form-group">
                  <label class="col-md-6">Total Amount:</label>
                  <label class="col-md-6"> <i class="fa fa-inr"><?php echo $payAmount; ?></i>
                  
                  </label>
                </div>
                <div class="form-group">
                  <label class="col-md-6">Amount Paid:</label>
                  <label class="col-md-6"><i class="fa fa-inr"><?php echo $paid=$payAmount-$due; ?></i></label>
                </div>
                <div class="form-group">
                  <label class="col-md-6">Pending Amount:</label>
                  <label class="col-md-6"> <i class="fa fa-inr"><?php echo $due; ?></i>
                  <?php
                    $per_paid=($paid/$payAmount)*100;
                    $per_due=($due/$payAmount)*100;
                   ?>
                   
                  </label>
                </div>
              </div>
              <div class="col-md-6">
          
                <div class="form-group">
                  <label class="col-md-6">% Paid:</label>
                  <label class="col-md-6"><?php echo round($per_paid,2) ?></label>
                </div>
                <div class="form-group">
                  <label class="col-md-6">% pending:</label>
                  <label class="col-md-6"><?php echo round($per_due,2); ?></label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>