
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption"> <i class="fa fa-file-archive-o"></i>Group Stay Summary </div>
          <!--
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
                --> 
        </div>
        <div class="portlet-body">
          <div class="table-toolbar">
            <div class="row">
              <div class="col-md-10">
                <div class="row">
                  <?php

                            $form = array(
                                'class'       => 'form-inline',
                                'id'        => 'form_date',
                                'method'      => 'post'
                            );

                            echo form_open_multipart('dashboard/group_report',$form);

                            ?>
				      <div class="col-md-7">
                      <div class="form-group">
                        <input type="text" autocomplete="off" required="required" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date">
                      </div>
                      <div class="form-group">
                        <input type="text" autocomplete="off" required="required" name="t_dt_to" class="form-control date-picker" placeholder="End Date">
                      </div>
					   
                    <button class="btn btn-default" onclick="check_sub()" type="submit">Search</button>
                  </div>
                 
                  <?php form_close(); ?>
                </div>
              </div>
              <div class="col-md-2">
                <div class="btn-group pull-right">
                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                  <ul class="dropdown-menu pull-right">
                    <li> <a href="javascript:window.print();"> Print </a> </li>
                    <li> <a href="<?php echo base_url();?>dashboard/pdf_reports_financial" target="_blank"> Save as PDF </a> </li>
                    <!--<li> <a href="javascript:;"> Export to Excel </a> </li>-->
                  </ul>
                </div>
              </div>
            </div>
          </div>
         <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
              <tr> 
                <th> # </th>
				<th>Group</th>
				<th> Group </th>
                <th> <!---Res# </th>
                <th> Room </th>
                <th> Stay Details </th>
                <th> Rate/pakage</th>
                <th> Guest </th>
                <th> Room Charge </th>
                <th> Tax </th>
				<th> Other Charge</th>
               <th> Total---></th>
              </tr>
            </thead>
            
          <tbody>
            
             
              <?php 
			if(isset($reports)){
				$srl_no=1;
				$srl_no1=0;
				foreach($reports as $report){
				
				//print_r($report);
				
			 //echo $group_id=$report->group_id."<br>";
			 
				
				{
					if(isset($group_id) && $group_id != 0)
			 $group_details=$this->dashboard_model->get_group_detalis($group_id);
			//print_r($group_details);
			//exit;
			if(isset($group_details))
				$i=1;
			foreach($group_details as $group)
			{
				
			?>
			<tr>
			<td><?php echo $i;?></td>
			<td><span style="background-color:#abc"><?php echo $group_id;?></span></td>
			<td><?php echo $group->name;?></td>
			<td>
			<!--<table>
			<?php 
			//$grp_details=$this->dashboard_model->get_grp_details($group->id);

			 foreach($grp_details as $grp){
			?>
			<tr>
			<td><?php echo $i;?></td>
			<td></td>
			<td><?php echo $group->roomNO;?></td>
			<td></td>
			<td><?php $grp->number_guest;?></td>
			<td><?php $group->room_rent_sum_total;?></td>
			<td><?php $group->room_rent_tax_amount;?></td>
			<td><?php $group->charges_cost;?></td>
			<td><?php 
			$total=$group->room_rent_sum_total+$group->room_rent_tax_amount+$grp->charges_cost;
			echo $total;?></td>
			<td></td>
			</tr>
			<?php }?>
			</table>-->
				
			</tr>
			
		
			
			<?php $i=$i+1;}}}}?>
            </tbody>
          </table>
		 </div>
		 </div>
		
											
		  
<script type="text/javascript">

$(document).ready(function(){
  $('#chkbx_tdy').prop('checked', true);
});

function check_sub(){
  document.getElementById('form_date').submit();
}
</script>