<!-- BEGIN PAGE CONTENT-->
<div class="portlet light borderd">
	<div class="portlet-title">
		<div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Guest View</span> </div>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-3 guest-vl">
				<div class="guest-image">
					<div class="fileinput fileinput-new" data-provides="fileinput">
						<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
							<img src="<?php echo base_url();?>upload/guest/originals/photo/<?php echo $guest->g_photo?>" alt=""/> </div>
						<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
						<div>
							<span class="btn red btn-outline btn-file btn-xs">
								<span class="fileinput-new"> Edit </span>
							<span class="fileinput-exists"> Change </span>
							<input type="file" name="..." id="file"  onchange="guest_pic_change(this.value,<?php echo $guest->g_id; ?>)" > </span>
							<a href="javascript:;" class="btn red btn-outline btn-xs fileinput-exists" data-dismiss="fileinput"> Remove </a>
						</div>
					</div>
					<div class="form-group">
						<input id="input-2c2" class="rating" min="0" max="5" step="0.5" data-size="sm" data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w1" value="" required="required">
					</div>
				</div>
				<div class="guest-Idproof">
					<div class="fileinput fileinput-new" data-provides="fileinput">
						<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
							<img src="<?php echo base_url();?>upload/guest/originals/id_proof/<?php echo $guest->g_id_proof?>" alt=""/> </div>
						<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
						<div>
							<span class="btn red btn-outline btn-file btn-xs">
								<span class="fileinput-new"> Edit </span>
							<span class="fileinput-exists"> Change </span>
							<input type="file" name="..." id="file1"  onchange="guest_idproof_change(this.value,<?php echo $guest->g_id; ?>)"> </span>
							<a href="javascript:;" class="btn red btn-outline btn-xs fileinput-exists" data-dismiss="fileinput"> Remove </a>
						</div>
					</div>
				</div>
				<div class="guest-ds-top">
					<h5>Billing</h5>
					<ul>
						<li><strong>Room Booked</strong> : 6</li>
						<li><strong>Days Spent</strong> : 30</li>
						<li><strong>Total Bill</strong> : 999999999.00</li>
						<li><strong>Total Paid</strong> : 999999999.00</li>
						<li><strong>Out Standing</strong> : 999999999.00</li>
					</ul>
				</div>
				<div class="guest-ds-mid">
					<h5>Stayed With</h5>
					<ul>
					
					<?php 
					
					$stay_guest=$this->unit_class_model->stay_guest_by_guestID($guest->g_id);
					foreach($stay_guest as $stay){
					?>
						<li><span class="label" style="background-color:#0aa547; color:#ddff64;"><?php echo $guest->booking_id ?></span> - <?php echo $stay->g_name ?></li>
					<?php }?>
					</ul>
				</div>
				<div class="guest-ds-bot">
					<h5>Check Out / In</h5>
					<ul>
					<?php 
					
					$bookings_guest_stat=$this->unit_class_model->booking_guestID($guest->g_id);
					$i=0;
					foreach($bookings_guest_stat as $chk_stat){
						$i++;
					?>
					<li><strong>Last CheckIn</strong> : <?php echo $chk_stat->checkin_date ?></li>
					<li><strong>Last CheckOut</strong> : <?php echo $chk_stat->checkout_date?></li>
						
						
						
					<?php }?>
					</ul>
				</div>
			</div>
			<div class="col-md-9 guest-vr">
				<div class="guest-db-top">
					<a href="javascript:;" class="btn red btn-outline btn-xs" data-dismiss="fileinput" id="guest_info_edit"> Edit </a>
					<div class="row" id="gdb-text">
						<h3 class="g-nam col-md-12"><i class="fa fa-male"></i> <?php if(isset($guest->g_name) && $guest->g_name){echo $guest->g_name;} ?></h3>
						<div class="col-md-6">
							<h4>Guest Info</h4>
							<p><i class="fa fa-birthday-cake" aria-hidden="true"></i> <?php echo $guest->g_age; ?> years Old <?php echo $guest->g_dob; ?> </p>
							<p><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Married : 21/05/2012</p>
							<p><i class="fa fa-users" aria-hidden="true"></i> <?php echo $guest->g_type.' : '.$guest->f_size?></p> 
							<p><i class="fa fa-graduation-cap" aria-hidden="true"></i> <?php echo $guest->g_highest_qual_name; ?></p> 
							<p><i class="fa fa-industry" aria-hidden="true"></i> <?php echo $guest->g_occupation; ?></p>
						</div>
						<div class="col-md-6">
							<h4>Guest Contacts</h4>
									
							<p><i class="fa fa-address-card-o" aria-hidden="true"></i> 
							<?php echo $guest->g_country.', '.$guest->g_state.', '.$guest->g_city.', PIN: '.$guest->g_pincode; ?>
							<!--123 Test Road, Kolkata, West Bengale, Pin : 700091, India-->
							</p> 
							<p><i class="fa fa-phone" aria-hidden="true"></i> <?php if(isset($guest->g_contact_no) && $guest->g_contact_no){echo $guest->g_contact_no;} ?></p>
							<p><i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo $guest->g_email; ?></p>
							<p><i class="fa fa-id-card" aria-hidden="true"></i> ID : <?php echo $guest->g_id_type.'('.$guest->g_id_number.')'; ?></p>
						</div>
					</div>
					<div class="panel panel-default" id="gdb-edit" style="display:none">
						<div class="panel-heading">
							<h3 class="panel-title">Edit Guest Details</h3>
						</div>
						<div class="panel-body"> Panel content </div>
						<div class="panel-footer text-right"> 
							<a href="javascript:;" class="btn blue" data-dismiss="fileinput" id="guest_info_save"> Save </a> 
							<a href="javascript:;" class="btn red" data-dismiss="fileinput" id="guest_info_cancel"> Cancel </a>
						</div>
					</div>
				</div>
				<div class="guest-db-bot">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Discount Plans</h3>
						</div>
						<div class="panel-body"> 
						
						<?php if(isset($guest->cp_discount_rule_id) && $guest->cp_discount_rule_id){
							// $guest->cp_discount_rule_id;
							// get_discount_plan_by_id
							$a='';
							 $rule=explode(",",$guest->cp_discount_rule_id);
							 foreach($rule as $ru){
								 $discount_rule=$this->unit_class_model->get_discount_plan_by_id($ru);
								 $a.=$discount_rule->rule_name.',';
							 }
							 echo $a;
						}
						
						
						?>

						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Bookings</h3>
						</div>
						<div class="panel-body">
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">Booking ID</th>
										<th scope="col">Room No</th>
										<th scope="col">CheckIn</th>
										<th scope="col">CheckOut</th>
										<th scope="col">Source</th>
										<th scope="col">Purpose</th>
										<th scope="col">Amount</th>
									</tr>
								</thead>
								<tbody>
								<?php 
					
					$bookings_guest=$this->unit_class_model->booking_guestID($guest->g_id);
					$i=0;
					foreach($bookings_guest as $bguest){
						$i++;
					?>
									<tr>
										<td><?php echo $i;?></td>
										<td><span class="label" style="background-color:#0aa547; color:#ddff64;"><?php echo $bguest->booking_id;?></span></td>
										<td><?php echo $bguest->room_no ?></td>
										<td><?php echo $bguest->checkin_date ?></td>
										<td><?php echo $bguest->checkout_date ?></td>
										<td><?php echo $bguest->booking_source_name ?></td>
										<td><?php echo $bguest->g_type ?></td>
										<td><?php echo $bguest->room_rent+$bguest->rr_total_tax + $bguest->exrr_chrg + $bguest->exrr_total_tax + $bguest->exmp_chrg + $bguest->exmp_total_tax + $bguest->meal_plan_chrg+$bguest->meal_plan_chrg?></td>
									</tr>
					<?php }?>
								</tbody>
							</table>
						</div>
						<div class="panel-footer text-right"> 
							<a href="javascript:;" class="btn blue" data-dismiss="fileinput"> Invoice </a> 
						</div>
					</div>
						<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Transaction</h3>
						</div>
						<div class="panel-body">
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">ID</th>
										<th scope="col">Type</th>
										<th scope="col">Mode</th>
										<th scope="col">Amount</th>
										<th scope="col">Details</th>
										
									</tr>
								</thead>
								<tbody>
								<?php 
					
					$bookings_guest=$this->unit_class_model->booking_guestID($guest->g_id);
					$i=0;
					foreach($bookings_guest as $bguest){
						$i++;
					?>
									<tr>
										<td><?php echo $i;?></td>
										<td><span class="label" style="background-color:#0aa547; color:#ddff64;"><?php echo $bguest->t_id;?></span></td>
										<td><?php echo $bguest->transaction_type ?></td>
										<td><?php echo $bguest->t_payment_mode ?></td>
										<td><?php echo $bguest->t_amount ?></td>
										<td><?php echo $bguest->details_type ?></td>
										
									</tr>
					<?php }?>
								</tbody>
							</table>
						</div>
						<div class="panel-footer text-right"> 
							<a href="javascript:;" class="btn blue" data-dismiss="fileinput"> Invoice </a> 
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Feedback</h3>
						</div>
						<div class="panel-body"> Panel content </div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Complains</h3>
						</div>
						<div class="panel-body"> Panel content </div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Lost & Found Items</h3>
						</div>
						<div class="panel-body"> 
						
						
						Panel content 
						
						
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Extra Charges / Laundry / POS</h3>
						</div>
						<div class="panel-body"> Panel content </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

	$('#guest_info_edit').on('click' , function(){
		$('#gdb-text').hide();
		$('#guest_info_edit').hide();
		$('#gdb-edit').show();
	});
	
	$('#guest_info_cancel').on('click' , function(){	
		$('#gdb-edit').hide();
		$('#guest_info_edit').show();
		$('#gdb-text').show();
	});
		
		
	function guest_pic_change(id,admin_id){
	//alert(admin_id);
	//alert('hello world');
	 var input = document.getElementById("file");
  file = input.files[0];
  if(file != undefined){
    formData= new FormData();
    if(!!file.type.match(/image.*/)){
      formData.append("image", file);
      $.ajax({
        url: "<?php echo base_url()?>unit_class_controller/guest_pic_change/"+admin_id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
			//alert(data);
           // alert('success');
		   toastr.success('Update Successfully! - '+data, 'success!');
			
        }
      });
    }else{
      alert('Not a valid image!');
    }
  }else{
    alert('Input something!');
  }
		
}

function guest_idproof_change(id,admin_id){
	//alert(admin_id);
	//alert('hello world');
	 var input = document.getElementById("file1");
  file = input.files[0];
  if(file != undefined){
    formData= new FormData();
    if(!!file.type.match(/image.*/)){
      formData.append("image", file);
      $.ajax({
        url: "<?php echo base_url()?>unit_class_controller/guest_idproof_change/"+admin_id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
			//alert(data);
           // alert('success');
		   toastr.success('Update Successfully! - '+data, 'success!');
			
        }
      });
    }else{
      alert('Not a valid image!');
    }
  }else{
    alert('Input something!');
  }
		
}


</script>