<!-- BEGIN PAGE CONTENT-->
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green"> <span class="caption-subject bold uppercase"><i class="fa fa-plus-square"></i>&nbsp; Add Amenities</span> </div>
  </div>
  <div class="portlet-body form">
    
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
        <?php
                  $form = array(
                      'class'       => 'form-body',
                      'id'        => 'form',
                      'method'      => 'post',
      
                  ); 
                    echo form_open_multipart('unit_class_controller/add_hotel_amenities',$form);
                  ?>
        <div class="form-body">
        <div class="row">
        
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="text" autocomplete="off" class="form-control" id="name" name="name" required="required">
          <label> Amenities Name<span class="required" id="b_contact_name">*</span></label>
          <span class="help-block">Asset Name</span> </div>

        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="text" autocomplete="off" class="form-control" id="ct_descname" name="desc" required="required">
          <label> Description<span class="required" id="b_contact_name">*</span></label>
          <span class="help-block">Asset Name</span> </div>

<div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="checkbox"  id="general" name="general" required="required">
          <label> Hotel General<span class="required" id="b_contact_name">*</span></label>
          <span class="help-block">Asset Name</span> </div>

</div> <div class="row">
          <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="checkbox"  id="ct_name" name="units" required="required">
          <label> Units<span class="required" id="b_contact_name">*</span></label>
          <span class="help-block">Units</span> </div>
         

          <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <select class="form-control focus" id="type1" onchange="condition_open()"  name="type" required >
                <option value="0">---Select Category---</option>
                <option value="1">Free to all</option>
                <option value="2">Free for some</option>
                <option value="3">Restrivted</option>
              </select>
              <label>Type <span class="required">*</span></label>
              <span class="help-block">Category...</span> </div>

          
          <div id="AMC"><div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="text" autocomplete="off" class="form-control" onkeyup="check()" id="charge" name="charge">
          <label> Charge<span class="required" id="b_contact_name">*</span></label>
          <span class="help-block">Asset Name</span> </div>

        
       
        

              <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <input type="text" autocomplete="off" class="form-control" id="tax" name="tax">
          <label> Tax<span class="required" id="b_contact_name">*</span></label>
          <span class="help-block">Asset Name</span> </div></div>


        <!-- <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label class="col-md-6 control-label" for="form_control_1" style="padding-top: 5px;"> Asset AMC<span class="required" id="b_contact_name">*</span></label>
          <div class="col-md-6">
            <div class="md-radio-inline" style="margin: 8px 0 1px;">
                <div class="md-radio">
                    <input type="radio" id="radio52" class="md-check" onclick="amc_open()" name="a_amc" value="yes">
                    <label for="radio52">
                    <span></span>
                    <span class="check"></span>
                    <span class="box"></span>
                    Yes </label>
                </div>  
                <div class="md-radio">
                    <input type="radio" id="radio53" class="md-check" onclick="amc_close()" name="a_amc" value="no">
                    <label for="radio53">
                    <span></span>
                    <span class="check"></span>
                    <span class="box"></span>
                    No </label>
                </div>            
            </div>            
          </div>
        </div>
          <div id="AMC" style="display:none">
          <div class="form-group form-md-line-input form-md-floating-label col-md-4">
            <input type="text" autocomplete="off" class="form-control" id="ct_name" name="a_amc_agency_name" >
            <label>Asset AMC Agency Name</label>
            <span class="help-block">Asset AMC Agency Name</span> </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-4">
            <input type="text" autocomplete="off" onkeypress="return onlyNos(event,this)" class="form-control" id="ct_name" name="a_amc_reg_contact_no" maxlength="10">
            <label>Asset AMC Contact Number</label>
            <span class="help-block">Asset AMC Contact Number</span> </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-4">
            <input type="text" autocomplete="off" class="form-control date-picker" id="ct_name1" name="a_amc_renewal_date">
            <label>Asset AMC Renewal Date</label>
            <span class="help-block">Asset AMC Renewal Date</span> </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-4">
            <input type="text" autocomplete="off" class="form-control" id="ct_name" name="a_amc_charge">
            <label>Asset AMC Renewal Charge</label>
            <span class="help-block">Asset AMC Renewal Charge</span> </div>
        </div> -->
        <!-- <div class="col-md-12">
          <div class="row"> -->
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <p><strong>Upload Asset Image</strong></p>
              <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/>
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                </div>
                <div>
                    <span class="btn default btn-file">
                    <span class="fileinput-new">
                    Select image </span>
                    <span class="fileinput-exists">
                    Change </span>
					<?php echo form_upload('image');?>
                    </span>
                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                    Remove </a>
                </div>
              </div>
              <?php //echo form_upload('a_asset_image');?> 
              <!--<input type="file"  name="a_asset_image" />--> 
            </div>
            
            
            </div>
        </div>
        </div>
      </div>
        <div class="form-actions right">
          <button type="submit" class="btn blue">Submit</button>
          <button  type="reset" class="btn default">Reset</button>
        </div>
        <?php form_close(); ?>      
  </div>
</div>

<!-- END CONTENT --> 
 <script>
 var gv;
 function condition_open(){
	 
	 var gv=$("#type1").val();
 
 if(gv=='1'){
	 document.getElementById('AMC').style.display='none';
 }
 else{
	  document.getElementById('AMC').style.display='block';
 }
 }
 
 /*function AMC_open(gv){
   document.getElementById('AMC').style.display='block';
 }
 function amc_close(){
   document.getElementById('AMC').style.display='none';
 }
 $(document).on('blur', '#c_valid_from', function () {
   $('#c_valid_from').addClass('focus');
 });
 $(document).on('blur', '#ct_name2', function () {
   $('#ct_name2').addClass('focus');
 });
 $(document).on('blur', '#ct_name1', function () {
   $('#ct_name1').addClass('focus');
  
 });*/
function check(){
  var a=$("#charge").val();
  var b=$("#tax").val();
  
  if(isNaN(a)){
	  alert("enter number");
   	$("#charge").val("");
  }
 if(isNaN(b)){
	  alert("enter number");
   	$("#tax").val("");
  }
}
</script>  
