<table id="logo-name">
	<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel')); ?>
	<tr>
		<td align="left" valign="middle"><img src="<?php echo base_url();?>upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/>
		</td>
		<td align="right" valign="middle">
			<?php echo "<strong style='font-size:14px;'>".$hotel_name->hotel_name.'</font></strong>'?>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2">
			<hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;">
		</td>
	</tr>
	<tr>
		<td colspan="2"><strong>Date:</strong>
			<?php echo date('D-M-Y'); ?>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2">&nbsp;</td>
	</tr>
</table>
<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
<?php endif;?>
<div class="portlet light borderd">
	<div class="portlet-title">
		<div class="caption" id="report"> <i class="glyphicon glyphicon-bed"></i>Hotel Area Code </div>
		<div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
	</div>
	<div class="portlet-body">
		<div class="table-toolbar">
			<div class="row">
				<div class="col-md-4">
					<div class="input-group">
						<select class="form-control bs-select" onChange="filter_state(this.value)">
							<option value="select">Select</option>
							<?php 
				 $state=$this->unit_class_model->state();
				 if(isset($state)&& $state){
					 foreach($state as $st){  ?>
							<option value="<?php echo $st->area_1;?>">
								<?php echo $st->area_1;?>
							</option>
							<?php }} ?>
						</select>
						<span class="input-group-btn">
							<button class="btn green" data-toggle="modal" href="#responsive"> Edit Area Code <i class="fa fa-plus"></i> </button>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div id="table1">
			<table class="table table-striped table-hover table-bordered" id="sample_1">
				<thead>
					<tr>
						<!--<th scope="col">
						Select
					</th>-->
						<th scope="col" width="20%"> Pivcode. </th>
						<th scope="col" width="20%"> Country </th>
						<th scope="col" width="20%"> Status </th>
						<th scope="col" width="20%"> City </th>
						<th scope="col" width="20%"> Locality 1 </th>
						<th scope="col" width="20%"> Locality 2 </th>
						<th scope="col" width="20%"> Action </th>
					</tr>
				</thead>
				<tbody id="tbody1">
					<?php $guest=0; ?>
					<?php if(isset($data) && $data):
													
									foreach($data as $datas):

									?>
					<tr>
						<td>
							<?php echo $datas->pincode; ?>
						</td>
						<td>
							<?php echo $datas->area_0; ?>
						</td>
						<td>
							<?php echo $datas->area_1; ?>
						</td>
						<td>
							<?php echo $datas->area_3; ?>
						</td>
						<td>
							<?php echo $datas->area_4; ?>
						</td>
						<td>
							<?php echo $datas->area_5; ?>
						</td>
						<td class="ba">
							<div class="btn-group">
								<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li><a onclick="soft_delete('<?php echo $datas->id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a>
									</li>
									<li><a class="btn green btn-xs" data-toggle="modal" onclick="edit_area_code('<?php echo $datas->id;?>')"><i class="fa fa-edit"></i></a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
					<?php 
								endforeach;  ?>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add Area Code</h4>
			</div>
			<?php

			$form = array(
				'class' => 'form-body',
				'id' => 'form',
				'method' => 'post'
			);

			echo form_open_multipart( 'unit_class_controller/add_hotel_area_code', $form );

			?>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" onchange="tst()" class="form-control" placeholder="Country" id="form_control_1" name="country" onkeypress=" return onlyLtrs(event, this);" required="required">
							<label></label>
							<span class="help-block">Country name...</span> </div>
					</div>
					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" onchange="tst()" class="form-control" placeholder="State" id="form_control_1" name="state" onkeypress=" return onlyLtrs(event, this);" required="required">
							<label></label>
							<span class="help-block">State name...</span> </div>
					</div>
					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" placeholder="City" name="city" onkeypress=" return onlyLtrs(event, this);" required="required">
							<label></label>
							<span class="help-block">City name...</span> </div>
					</div>
					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="form_control_1" placeholder="Pincode " name="pincode" required="required">
							<label></label>
							<span class="help-block">Pincode</span> </div>
					</div>
					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" name="locality1" onkeypress=" return onlyLtrs(event, this);" placeholder="Locality name..." required="required">
							<label></label>
							<span class="help-block">Locality name...</span> </div>
					</div>
					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" onchange="tst()" class="form-control" id="form_control_1" name="locality2" onkeypress=" return onlyLtrs(event, this);" placeholder="Locality name..." required="required">
							<label></label>
							<span class="help-block">Locality name...</span> </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn default">Close</button>
				<button type="submit" class="btn green">Save</button>
			</div>
			<?php echo form_close(); ?> </div>
	</div>
</div>
<script>
	function edit_area_code( id ) {

		//$("#responsive1").modal('show');
		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>unit_class_controller/get_edit_area_code",
			data: {
				id: id
			},
			success: function ( data ) {
				//alert(data.id);
				$( '#hid2' ).val( data.id );
				$( '#hid1' ).val( data.profit_center_location );

				$( '#pincode' ).val( data.pincode );
				$( '#country' ).val( data.area_0 );
				$( '#state' ).val( data.area_1 );
				$( '#city' ).val( data.area_3 );
				$( '#locality1' ).val( data.area_4 );
				$( '#locality2' ).val( data.area_5 );

				//$('#desc1').val(data.status);
				// $('#unit_class1').val(data.change_date);



				$( '#responsive1' ).modal( 'toggle' );

			}
		} );
	}
</script>
<div id="responsive1" class="modal fade" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit area code</h4>
			</div>
			<?php

			$form = array(
				'class' => 'form-body',
				'id' => 'form',
				'method' => 'post'
			);

			echo form_open_multipart( 'unit_class_controller/edit_area_code/', $form );

			?>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" onchange="tst()" class="form-control" id="pincode" name="pincode" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Enter Pin code *">
							<label></label>
							<span class="help-block">Enter Pin code *</span> </div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" onchange="tst()" class="form-control" id="country" name="country" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Country *">
							<label></label>
							<span class="help-block">Country *</span> </div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" onchange="tst()" class="form-control" id="state" name="state" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="State *">
							<label></label>
							<span class="help-block">Enter State *</span> </div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="city" name="city" required="required" placeholder="City *">
							<label></label>
							<span class="help-block">City *</span> </div>
					</div>
					<input type="hidden" name="hid" value="<?php echo $datas->id; ?>">
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" onchange="tst()" class="form-control" id="locality1" name="locality1" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Locality Name *">
							<label></label>
							<span class="help-block">Locality Name *</span> </div>
					</div>
					<div class="col-md-3">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" onchange="tst()" class="form-control" id="locality2" name="locality2" onkeypress=" return onlyLtrs(event, this);" required="required" placeholder="Locality 2 *">
							<label></label>
							<span class="help-block">Locality Name 2 *</span> </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn default">Close</button>
				<button type="submit" class="btn green">Save</button>
			</div>
			<?php echo form_close(); ?> </div>
	</div>
</div>
<script>
	function check_sub() {
		document.getElementById( 'form_date' ).submit();
	}

	function filter_state( state ) {
		// swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
		alert( state );
		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>unit_class_controller/hotel_area_code_filter",
			data: {
				state: state
			},
			success: function ( data ) {
				$( '#table1' ).html( data );
				$.getScript( '<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons-ex.min.js' );
				$.getScript( '<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js' );
			}

		} );



		//});
	}

	function soft_delete( id ) {
		//alert(id);
		// alert(id);
		swal( {
			title: "Are you sure?",
			text: "All the releted transactions and data will be deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {





			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>unit_class_controller/delete_hotel_area_code?code=" + id,
				data: {},
				success: function ( data ) {
					//alert("Checked-In Successfully");
					//location.reload();
					swal( {
							title: data.data,
							text: "",
							type: "success"
						},
						function () {

							location.reload();

						} );
				}
			} );



		} );
	}
</script>