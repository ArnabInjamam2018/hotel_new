
<?php 

date_default_timezone_set('Asia/Kolkata');
if(isset($group_id) && $group_id){ 
?>
<input type="hidden" id="group_id" value="<?php echo $group_id; ?>" />
<?php 
} 

$current_date=date("Y-m-d");
if(isset($newEnd) && $newEnd !="0"){
	if(strtotime(date("Y-m-d",strtotime($newEnd))) < strtotime($current_date) ){
		echo "Past Booking events can not be changed";
		exit();
	}
}

	
    $hotel_id = $this->session->userdata('user_hotel');
	
?>
<head>
<link href="https://fonts.googleapis.com/css?family=Exo+2:400,700|Monda:400,700|Open+Sans:400,700|Roboto:400,700" rel="stylesheet">
<link href="<?php echo base_url();?>assets/layouts/layout/css/font.css" rel="stylesheet" type="text/css" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo base_url();?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url();?>assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?php echo base_url();?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/themes/light2.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
</head>
<body style="background:none;">
<div class="portlet box grey-cascade" style="margin-bottom:0px;">
	<div class="portlet-title">
        <div class="caption">
            <i class="icon-pin font-white"></i> Move Booking Event
        </div>
        <div class="tools" style="display: inline-block; float: right; padding: 12px 0 8px;">
            <a onClick="closem()" style="color:#ffffff;"> <i class="fa fa-times"> </i></a>
        </div>
    </div> 
    <div class="portlet-body form">
	
<?php
	
	
//	else {
	if(isset($group_id) && $group_id !="0"){
	} 
	else 	{
	if(isset($details) && $details !="0")	{
    $currentRoom = $this->bookings_model->hotelWiseRoomDetails($hotel_id,$details->room_id); 
    $currentRoomType = $this->bookings_model->getUnitType($currentRoom->unit_id); 
    $NewRoomType = $this->bookings_model->getUnitType($rooms->unit_id);
   
	if(isset($rr_tax_response['total']) && $rr_tax_response['total'] !='') {
		$tax = $rr_tax_response['total'];
	}
	else{
		$tax= 0;
	}
	
//	$mealtax = $mp_total;
	if(isset($mp_tax_response['total']) && $mp_tax_response['total'] !='') {
		$mealtax = $mp_tax_response['total'];
	}
	else{
		$mealtax = 0;
	}
	?>
	<div class="form-body" style="padding-bottom:0;">
    <table class="table table-hover pop-edittable mass-book" style="background:#F1F3F2; border:1px solid #ddd; margin-bottom:0;">
        <?php if($events && isset($events)){
		foreach ($events as $event) {
		if(date("Y-m-d",strtotime($event->e_from)) < $newEnd && date("Y-m-d",strtotime($event->e_upto)) > $newStart){
		?>
		<tr>
			<td>
				<span style="font-size:14px; padding:4px 10px; text-transform: capitalize;background:<?php echo $event->event_color; ?>;color:<?php echo  $event->event_text_color; ?>; margin-right:10px;"><?php echo $event->e_name; ?></span>
				<span style="margin-right:10px;"><?php echo date("d-m-Y",strtotime($event->e_from))?> </span>
				--
				<span style="margin-left:10px;"><?php echo date("d-m-Y",strtotime($event->e_upto)) ?></span>
			</td>
		</tr>
        <?php } } }?>     
	</table>
	</div>
            <form action="<?php echo base_url() ?>bookings/booking_backend_move" method="post" style="margin-bottom:0;">
                <div class="form-body">
                    <input type="hidden" value="<?php echo $newStart ?>" name="newStart">
                    <input type="hidden" value="<?php echo $newEnd?>" name="newEnd">
                    <input type="hidden" value="<?php echo $id ?>" name="id" id="bid">
					<input type="hidden" value="<?php echo $newResource ?>" name="newResource">
					<input type="hidden" value="<?php echo $details->stay_days ?>" name="stay_days">
                    <table class="table table-hover pop-edittable mass-book" style="background:#F1F3F2; border:1px solid #ddd;">
                    	<tr>
                        	<td><span>Current Room : </span><?php echo $currentRoom->room_no;?> -- <?php echo  $currentRoomType->unit_name;?> </td>
                            <td><span>New Room : </span><?php echo $rooms->room_no;?>
                        	-- <?php echo $NewRoomType->unit_name;?></td>                        	
                        </tr>						
                    	<tr>
                        	<td><span>Current Booking Date:</span> <?php echo date('jS F Y',strtotime($details->cust_from_date_actual))." TO ". date('jS F Y',strtotime($details->cust_end_date_actual));?></td>
                        	<td><span>New Booking Date:</span> <?php echo date('jS F Y',strtotime($newStart))." TO ".date('jS F Y',strtotime($newEnd));?></td>
                        </tr>
						<tr>
						<?php
						if($currentbookedRoom->exmp_chrg >0){
							
							$currentmealrate = $currentbookedRoom->meal_plan_chrg + $currentbookedRoom->exmp_chrg;
							
						}
						else{
							
							$currentmealrate = $currentbookedRoom->meal_plan_chrg + $currentbookedRoom->exmp_chrg;
						}
						?>
							<td><span> Current Meal Plan Rate per Day:</span> <i class="fa fa-inr"></i> <?php echo $currentmealrate;?></td>
							<?php
							$exmp = $meal_charge + $extra_person_rate_price;
							if(isset($details->mod_room_rent) && $details->mod_room_rent!=''){
								if($details->rent_mode_type == 'add'){
									$modifier_new_rent = $room_charge +$extra_person_room_rent + $details->mod_room_rent;
									}								
									else{																		$modifier_new_rent = $room_charge +$extra_person_room_rent - $details->mod_room_rent;								
									}							
									}else{
										
										$modifier_new_rent = $room_charge +$extra_person_room_rent;							}							
										if(isset($details->food_plan_mod_rent) &&  $details->food_plan_mod_rent!=''){
											if($details->food_plan_mod_type == 'add'){
												$modifier_new_meal = $exmp + $details->food_plan_mod_rent;								
												}								
											else{
													
												
									$modifier_new_meal = $exmp - $details->food_plan_mod_rent;
									}							
								}
								else{	
								
									$modifier_new_meal = $exmp;							
								}															
							?>
                        	<td><span> New Meal Plan Rate per Day:</span> <i class="fa fa-inr"></i> <input type="text" class="form-control input-sm final_sum_amount_meal" name="final_sum_amount_meal" id="final_sum_amount_meal" value="<?php echo $modifier_new_meal ?>" readonly style="display:inline-block; width:auto;">
							 <input type="hidden" id="meal_rate_actual" name="meal_rate_actual" value="<?php echo $modifier_new_meal ?>">
							</td>
                        </tr>
						<tr>
							<td>
						<label>Meal modifier<span class="required"></span></label>
                          <div class="radio-list re-modi" style="display: inline-block;">
                         <!--   <label id="lbl1" class="checkbox-inline perc" style="padding:0">
                              <input class="addition34" type="checkbox" id="mperc" name="mperc" value="percent" style="display:none;">
                              <i class="fa fa-percent"></i></label>-->
                            <label class="radio-inline add11" style="padding:0">
                              <input class="addition11" type="radio" name="m_rent_mode_type" id="m_rent_mode_type" value="add" style="display:none;">
                              <i class="fa fa-plus-square"></i></label>
                            <label class="radio-inline sub11" style="padding:0; margin-left:7px;">
                              <input class="addition22" type="radio" name="m_rent_mode_type" id="m_rent_mode_type" value="substract" style="display:none;">
                              <i class="fa fa-minus-square"></i></label>
                            <input type="hidden" id="m_perc1" name="m_perc1" value="0">
                            <input type="hidden" id="modf_mp" name="modf_mp" value="0">
                          </div>							
							</td>
							<td>
							<span>Meal Modifier Amount:</span> <i class="fa fa-inr"></i> <input type="text" class="form-control input-sm meal_modifier" name="meal_modifier" id="meal_modifier" value='0'  style="display:inline-block; width:auto;">
							</td>
						</tr>
                    	<tr>
                        	<?php
						if($currentbookedRoom->exrr_chrg >0){
							
							$currentroomrent = $currentbookedRoom->room_rent + $currentbookedRoom->exrr_chrg;
							
						}
						else{
							
							$currentroomrent = $currentbookedRoom->room_rent + $currentbookedRoom->exrr_chrg;
						}
						
						$updatedroomrent = $room_charge +$extra_person_room_rent;
						?>
							<td><span>Current Room Rent Per Day:</span> <i class="fa fa-inr"></i>
							<?php echo $currentroomrent;?></td>						
                        	<td><span>New Room Rent Per Day:</span> <i class="fa fa-inr"></i> <input type="text" class="form-control input-sm" name="final_sum_amount_room" id="final_sum_amount_room" value="<?php echo $modifier_new_rent; ?>" onblur="getnewtaxresponseroom();" readonly style="display:inline-block; width:auto;">
							<input type="hidden" name="room_rate_actual" id="room_rate_actual" value="<?php echo $updatedroomrent; ?>">		<input type="hidden" name="exrr_chrg" id="exrr_chrg" value="<?php echo $currentbookedRoom->exrr_chrg."opo".$currentbookedRoom->room_rent;?>">
                            </td> 
						
							<input type="hidden" value="<?php echo date("Y-m-d",strtotime($newStart));?>" name="newStartDatehidden" id="newStartDatehidden">							
							<input type="hidden" value="<?php echo ($modifier_new_rent - $extra_person_room_rent);?>" name="amt">
							<input type="hidden" value="<?php echo $details->mod_room_rent;?>" name="room_price_modifier">
							<input type="hidden" value="<?php echo $details->food_plan_mod_rent;?>" name="meal_price_modifier">
							
							<input type="hidden" value="<?php echo $details->rent_mode_type;?>" name="rent_mode_type">
							<input type="hidden" value="<?php echo $details->food_plan_mod_type;?>" name="food_plan_mod_type" id="food_plan_mod_type">
							
							<input type="hidden" value="<?php echo $details->stay_days;?>" name="sd">
							<input type="hidden" value="<?php echo $tax;?>" name="tax" id="tax">
							<input type="hidden" value="<?php echo $mealtax;?>" name="mealtax" id="mealtax">
							<input type="hidden" value="<?php echo ($exmp - $extra_person_rate_price);?>" name="meal_price">
							<input type="hidden" value="<?php echo $extra_person_room_rent;?>" name="extra_person_room_rent">
							<input type="hidden" value="<?php echo $extra_person_rate_price;?>" name="extra_person_rate_price">
							<input type="hidden" value="<?php echo $room_charge_tax_exp;?>" name="room_charge_tax_exp">
							<input type="hidden" value="<?php echo $meal_charge_tax_exp;?>" name="meal_charge_tax_exp">
							<?php
							header('Content-Type: application/json');
	
							$rr_tax_responses = json_encode($rr_tax_response);
							$mp_tax_responses  =  json_encode($mp_tax_response);
							header("Content-Type: text/html");
							?>
							<input type="hidden" value='<?php echo $rr_tax_responses;?>' name="rr_tax_responsetax" id="rr_tax_responsetax">
							<input type="hidden" value='<?php echo $mp_tax_responses;?>' name="mp_tax_responsetax" id="mp_tax_responsetax">
                        </tr>
						<tr>
							<td>
							<label>Rent modifier<span class="required"></span></label>
                            <div class="radio-list re-modi" style="display: inline-block;">
        
                              <label class="radio-inline add add12" style="padding:0">
                                <input class="addition1" type="radio" name="rent_mode_type" id="rent_mode_type" value="add" style="display:none;">
                                <i class="fa fa-plus-square"></i></label>
                              <label class="radio-inline sub sub12" style="padding:0; margin-left:7px;">
                                <input class="addition2" type="radio" name="rent_mode_type" id="rent_mode_type" value="substract" style="display:none;">
                                <i class="fa fa-minus-square"></i></label>
                              
                            </div>							
							</td>
							<td>
							<span>Rent Modifier Amount:</span> <i class="fa fa-inr"></i> <input type="text" class="form-control input-sm rent_modifier" name="rent_modifier" id="rent_modifier" value='0'  style="display:inline-block; width:auto;">
							<input type="hidden" class="form-control input-sm" id="chk_mod" value="0"/>
							<input type="hidden" class="form-control input-sm" id="chk_mod_rent" value="0"/>
							</td>
						</tr>
                     <!--   <tr>
                        	<td colspan="2"><span>Updated Total Room Rent will be:</span> <i class="fa fa-inr"></i> <input type="text" class="form-control input-sm" name="final_sum_amount" value="<?php echo round($updatedroomrent*$details->stay_days, 2); ?>" style="display:inline-block; width:auto;"></td>
                        </tr>
						<?php $pay = ($updatedroomrent*$details->stay_days*$tax)/100; ?>
                        <!--<tr>
                        	<td><span>Updated total Tax will be:</span> <i class="fa fa-inr"></i> <input type="text" class="form-control input-sm" name="tax2" value="<?php echo round($pay, 2); ?>" style="display:inline-block; width:auto;"></td>
                        </tr>-->						
                    </table>
                </div>
                <div class="form-actions right">
                	<input class="btn btn-success" type="submit" id="finalyes" value="Yes" />
                	<button id="not" type="button" class="btn btn-danger" onClick="closem()" > No </button>
                </div>
            </form>
	<?php }
	} 
	//}
	
	?>
        <?php if(isset($message)): ?>
		<div class="form-body">
        <h3 style="text-align:center;"><?php echo $message; ?></h3>
		</div>
		<div class="form-actions right">
			<button id="not" type="button" class="btn btn-danger" onClick="closem()" > OK </button>
		</div>
        <?php endif; ?>
		
    </div>
</div>

<script>
//javascript: window.parent.location.reload();

$('#not').click(function(){
//alert("asd");
var val=2;
 close(eval(val));
return false;
});

/*
 function close(result) {
        if (parent && parent.DayPilot && parent.DayPilot.ModalStatic) {
           
            parent.DayPilot.Scheduler.update(result);
             parent.DayPilot.ModalStatic.close(result);
        }
    }*/

    function closem() {
       
                 //window.parent.location.reload();

                //window.parent.$( "#booking_calendar").load( "<?php echo base_url() ?>dashboard/calendar_load" );
                parent.DayPilot.ModalStatic.close();
            }

    
</script> 
<script type="text/javascript">
  /*  $(function() {
    var group_id=document.getElementById("group_id").value;
    if(group_id !="0"){
        parent.window.location.reload();
    //  alert("yes");
    }
});
*/

    function close(){
         parent.window.location.reload();

    }
</script>
<script>

	function getnewtaxresponse(){
	
	//	alert('FM');
	//	alert('hello');
		var finalmeal = $('.final_sum_amount_meal').val();
		var newStartDatehidden = $('#newStartDatehidden').val();
		var type = 'Meal Plan';
		alert(finalmeal);
		alert(newStartDatehidden);
		//alert();
		$.ajax({
			
			type:"POST",
			url:"<?php echo base_url(); ?>dashboard/showtaxplan1",
			data:{price:finalmeal,args:'a',type1:type,dates:newStartDatehidden},
			async:false,
			success:function(data){
				
				//alert(data);
				//console.log(data);
				let taxR = JSON.stringify(data);
					//$('#txrspns
					
					
					//console.log('Tax Response - '+taxR);
					tax = data.total;
				//	roomId = data.room_id;
					if(type === 'Booking'){
						$('#tax').val(tax);
						console.log('Success! getTax - Booking');
						$('#rr_tax_responsetax').val(taxR);
					}
					else if(type === 'Meal Plan'){
						$('#mealtax').val(tax);
						console.log('Success! getTax - Meal Plan');
						$('#mp_tax_responsetax').val(taxR);
					}
					
			},
		});
	}


	function getnewtaxresponseroom(){
	
	//	alert('FM');
	//	alert('hello');
		var finalmeal = $('#final_sum_amount_room').val();
		var newStartDatehidden = $('#newStartDatehidden').val();
		var type = 'Booking';
		alert(finalmeal);
		alert(newStartDatehidden);
		$.ajax({
			
			type:"POST",
			url:"<?php echo base_url(); ?>dashboard/showtaxplan1",
			data:{price:finalmeal,args:'a',type1:type,dates:newStartDatehidden},
			async:false,
			success:function(data){
				
			//	alert(data);
			//	console.log(data);
				let taxR = JSON.stringify(data);
					//$('#txrspns
					
					
					//console.log('Tax Response - '+taxR);
					tax = data.total;
				//	roomId = data.room_id;
					if(type === 'Booking'){
						$('#tax').val(tax);
						console.log('Success! getTax - Booking');
						$('#rr_tax_responsetax').val(taxR);
					}
					else if(type === 'Meal Plan'){
						$('#mealtax').val(tax);
						console.log('Success! getTax - Meal Plan');
						$('#mp_tax_responsetax').val(taxR);
					}
					
			},
		});
	}
	
	
	$(".addition1").on("click", function(){
	 //   var md = 0;
    //    document.getElementById("chk_mod").value = md;
        $("label.add").addClass("active");
		$("label.sub").removeClass("active");
		
	 var fsm = $('table.mass-book tr td').find('#room_rate_actual').val();
	   alert(fsm);
        $("label.add12").addClass("active");
		$("label.sub12").removeClass("active");
		var rent_modifier_value = $('table.mass-book tr td').find('.rent_modifier').val();
		if(rent_modifier_value <= 0){
			
			alert('Modifier value must be greater than 0');
			
		}
		else{
		
			var modifiedvalue = parseInt(fsm) + parseInt($('table.mass-book tr td').find('.rent_modifier').val());
			var bookingid = $('#bid').val();
			alert('XXXXXX'+modifiedvalue);
			alert('BID'+bookingid);
			$('table.mass-book tr td').find('#final_sum_amount_room').val(modifiedvalue);
			$('table.mass-book tr td').find('#amt').val(modifiedvalue);
			$('table.mass-book tr td').find('#chk_mod_rent').val('add');
			getnewtaxresponseroom();
		}

});
$(".addition2").on("click", function(){
	
	 var fsm = $('table.mass-book tr td').find('#room_rate_actual').val();
	   alert(fsm);
        $("label.sub12").addClass("active");
		$("label.add12").removeClass("active");
		var rent_modifier_value = $('table.mass-book tr td').find('.rent_modifier').val();
		if(rent_modifier_value <= 0){
			
			alert('Modifier value must be greater than 0');
			$("label.add12").removeClass("active");
			$("label.sub12").removeClass("active");
			
		}
		else{
		
			var modifiedvalue = parseInt(fsm) - parseInt($('table.mass-book tr td').find('.rent_modifier').val());
			var bookingid = $('#bid').val();
			alert('XXXXXX'+modifiedvalue);
			alert('BID'+bookingid);
			$('table.mass-book tr td').find('#amt').val(modifiedvalue);
			$('table.mass-book tr td').find('#final_sum_amount_room').val(modifiedvalue);
			$('table.mass-book tr td').find('#chk_mod_rent').val('substract');
			getnewtaxresponseroom();
		}
		
});

$(".addition11").on("click", function(){
	   // var md = 0;
       // document.getElementById("chk_mod").value = md;
	   
	   var fsm = $('table.mass-book tr td').find('#meal_rate_actual').val();
	   alert(fsm);
        $("label.add11").addClass("active");
		$("label.sub11").removeClass("active");
		var meal_modifier_value = $('table.mass-book tr td').find('.meal_modifier').val();
		if(meal_modifier_value <= 0){
			
			alert('Modifier value must be greater than 0');
			$("label.add11").removeClass("active");
		    $("label.sub11").removeClass("active");
			
		}
		else{
		
			var modifiedvalue = parseInt(fsm) + parseInt($('table.mass-book tr td').find('.meal_modifier').val());
			var bookingid = $('#bid').val();
			//alert('XXXXXX'+modifiedvalue);
			//alert('BID'+bookingid);
			$('table.mass-book tr td').find('#final_sum_amount_meal').val(modifiedvalue);
			$('table.mass-book tr td').find('#chk_mod').val('add');
			getnewtaxresponse();
		}
		

});

$('#finalyes').on('click',function(){
	
	
	var bookingid = $('#bid').val();
			
		var modifiedvaluemeal =	$('table.mass-book tr td').find('#meal_modifier').val();
		var modifiedtype =   $('table.mass-book tr td').find('#chk_mod').val();
		var modifiedvalueroom =	$('table.mass-book tr td').find('#rent_modifier').val();
		var modifiedtyperoom =   $('table.mass-book tr td').find('#chk_mod_rent').val();
		if(modifiedvaluemeal > 0){
		alert('XXXXXX'+modifiedvaluemeal);
		alert('BID'+bookingid);
		alert('Mtype'+modifiedtype);
	$.ajax({
				
				url:"<?php echo base_url() ?>bookings/meal_update_modifier",
				type:"POST",
				data:{food_plan_mod_type:modifiedtype,food_plan_mod_rent:modifiedvaluemeal,bookingId:bookingid},
				success:function(data){
					
					alert(data);
					
				},
			})
}
 if(modifiedvalueroom > 0){
	
	alert('XXXXXX'+modifiedvalueroom);
		alert('BID'+bookingid);
		alert('Mtype'+modifiedtyperoom);
	$.ajax({
				
				url:"<?php echo base_url() ?>bookings/rent_update_modifier",
				type:"POST",
				data:{rent_mode_type:modifiedtyperoom,mod_room_rent:modifiedvalueroom,bookingId:bookingid},
				success:function(data){
					
					alert(data);
					
				},
			})
	
}
});

$(".addition22").on("click", function(){
	    //var md = 0;
      //  document.getElementById("chk_mod").value = md;
		$("label.sub11").addClass("active");
		$("label.add11").removeClass("active");
		var fsm = $('table.mass-book tr td').find('#meal_rate_actual').val();
		alert(fsm);
		var meal_modifier_value = $('table.mass-book tr td').find('.meal_modifier').val();
		if(meal_modifier_value <= 0){
			
			alert('Modifier value must be greater than 0');
			$("label.sub11").removeClass("active");
			$("label.add11").removeClass("active");
			
		}
		else{
		
			var modifiedvalue = parseInt(fsm) - parseInt($('table.mass-book tr td').find('.meal_modifier').val());
			var bookingid = $('#bid').val();
			alert('XXXXXX'+modifiedvalue);
			alert('BID'+bookingid);
			$('table.mass-book tr td').find('#final_sum_amount_meal').val(modifiedvalue);
			$('table.mass-book tr td').find('#chk_mod').val('substract');
			getnewtaxresponse();
		}
		
		
});





</script>
</body>