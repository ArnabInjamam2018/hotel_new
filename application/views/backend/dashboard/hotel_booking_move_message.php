<!DOCTYPE html>
<html lang="en">
<head>
<link href='https://fonts.googleapis.com/css?family=Exo+2:400,600,700,800,500' rel='stylesheet' type='text/css'>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo base_url();?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url();?>assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?php echo base_url();?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/themes/light2.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
</head>
<body style="background:none;">
<div class="portlet box grey-cascade" style="margin-bottom:0px;">
	<div class="portlet-title">
        <div class="caption">
            <i class="icon-pin font-white"></i> Booking change event
        </div>
        <div class="tools" style="display: inline-block; float: right; padding: 12px 0 8px;">
            <a onClick="closem()" style="color:#ffffff;"> <i class="fa fa-times"> </i></a>
        </div>
    </div> 
    <div class="portlet-body form">
		<div class="form-body">
        <h3 style="text-align:center;">You can not change the date in checkin status.</h3>
		</div>
		<div class="form-actions right">
			<button id="not" type="button" class="btn btn-danger" onClick="closem()" > OK </button>
		</div>

		
    </div>
</div>
<script>
//javascript: window.parent.location.reload();

$('#not').click(function(){
//alert("asd");
var val=2;
 close(eval(val));
return false;
});

/*
 function close(result) {
        if (parent && parent.DayPilot && parent.DayPilot.ModalStatic) {
           
            parent.DayPilot.Scheduler.update(result);
             parent.DayPilot.ModalStatic.close(result);
        }
    }*/

    function closem() {
       
                 //window.parent.location.reload();

                //window.parent.$( "#booking_calendar").load( "<?php echo base_url() ?>dashboard/calendar_load" );
                parent.DayPilot.ModalStatic.close();
            }

    
</script> 
<script type="text/javascript">
    $(function() {
    var group_id=document.getElementById("group_id").value;
    if(group_id !="0"){
        parent.window.location.reload();
    //  alert("yes");
    }
});


    function close(){
         parent.window.location.reload();

    }
</script>
</body>
</html>