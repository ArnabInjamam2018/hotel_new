<?php if(isset($group_id) && $group_id){ ?>

<input type="hidden" id="group_id" value="<?php echo $group_id; ?>">
</input>
<?php } ?>
<?php
$roomrenttot = 0;
$roomrenttottax =0;
$mealratetot =0;
$mealratetottax = 0;
$extraroomrent = 0;
$extramealrate = 0;

if(isset($room_rent_sum_total)){
foreach($room_rent_sum_total as $total){

    if($total->room_rent_sum_total!=0){
        $total_sum= $total->room_rent_sum_total;
    }
    else{
        $total_sum= $total->room_rent_total_amount;
    }
}
}

if(isset($line_item_details_all)){
	
	foreach($line_item_details_all as $tot){
		
		$roomrenttot = $roomrenttot + $tot->room_rent;
		$roomrenttottax = $roomrenttottax + $tot->rr_total_tax;
		$mealratetot  = $mealratetot + $tot->meal_plan_chrg;
		$mealratetottax =  $mealratetottax + $tot->mp_total_tax;
		$extraroomrent =   $extraroomrent + $tot->exrr_chrg;
		$extramealrate =   $extramealrate + $tot->exmp_chrg;
	}
	
}

?>
<link href="https://fonts.googleapis.com/css?family=Exo+2:400,700|Monda:400,700|Open+Sans:400,700|Roboto:400,700" rel="stylesheet">
<link href="<?php echo base_url();?>assets/layouts/layout/css/font.css" rel="stylesheet" type="text/css" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo base_url();?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url();?>assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?php echo base_url();?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/themes/light2.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<body style="background:none;">
<div class="portlet box grey-cascade" style="margin-bottom:0px;">
	<div class="portlet-title">
        <div class="caption">
            <i class="icon-pin font-white"></i> Extend Reservation
        </div>
        <div class="tools" style="display: inline-block; float: right; padding: 12px 0 8px;">
            <a onClick="closem()" style="color:#ffffff;"> <i class="fa fa-times"> </i></a>
        </div>
    </div> 
	<?php  if(isset($room_rent_sum_total) && isset($ratio)){
		if($group_id !="0"){ ?>
	    <div class="portlet-body form">
			<div class="form-body">
        <h3 style="text-align:center;">Group Booking events can not be resized</h3>
		</div>
		<div class="form-actions right">
			<button id="not" type="button" class="btn btn-danger" onClick="closem()" > OK </button>
		</div>
		</div>
    <?php    } else { ?>
    <div class="portlet-body form">
        <?php      
        date_default_timezone_set('Asia/Kolkata');
        $current_date1=date("Y-m-d");
        $current_date = date('Y-m-d', strtotime('-1 day', strtotime($current_date1)));
        if(date("Y-m-d",strtotime($end)) <=$current_date ){ ?>
		<div class="form-body">
        <h3 style="text-align:center;"><?php echo "Past Booking events can not be resized."; ?></h3>
		</div>
		<div class="form-actions right">
			<button id="not" type="button" class="btn btn-danger" onClick="closem()" > OK </button>
		</div>
        <?php } elseif (isset($checkout) && $checkout=="1") { ?>
		<div class="form-body">
        <h3 style="text-align:center;"><?php echo "Checked out Booking events can not be resized."; ?></h3>
		</div>
		<div class="form-actions right">
			<button id="not" type="button" class="btn btn-danger" onClick="closem()" > OK </button>
		</div>        
        
        <?php } else { ?>
        <div class="form-body" style="padding-bottom:0;">
          	<table class="table table-hover pop-edittable mass-book" style="background:#F1F3F2; border:1px solid #ddd; margin-bottom:0;">
          <?php if($events && isset($events)){
			foreach ($events as $event) {
			if(date("Y-m-d",strtotime($event->e_from))<$end && date("Y-m-d",strtotime($event->e_upto))> $start){

		   ?>
          
            <tr>
            	<td>
                    <span style="font-size:14px; padding:4px 10px; text-transform: capitalize;background:<?php echo $event->event_color; ?>;color:<?php echo  $event->event_text_color; ?>; margin-right:10px;"><?php echo $event->e_name; ?></span>
                    <span style="margin-right:10px;"><?php echo date("d-m-Y",strtotime($event->e_from))?> </span>
                    --
                    <span style="margin-left:10px;"><?php echo date("d-m-Y",strtotime($event->e_upto)) ?></span>
            	</td></tr>
          <?php
                                }
                    
                                }
                    
                                }?>
             
            </table>
            </div>
            <form action="<?php echo base_url() ?>bookings/booking_backend_resize" method="post" style="margin-bottom:0;">
                <div class="form-body">
                    <input type="hidden" value="<?php echo $start ?>" name="newStart">
                    <input type="hidden" value="<?php echo $end?>" name="newEnd">
                    <input type="hidden" value="<?php echo $id ?>" name="id">
					<input type="hidden" value="<?php echo $line_item_details->meal_plan_chrg ?>" name="meal_plan_chrg">
					<input type="hidden" value="<?php echo $line_item_details->room_rent ?>" name="room_rent">
					<input type="hidden" value="<?php echo $line_item_details->exrr_chrg ?>" name="exrr_chrg">
					<input type="hidden" value="<?php echo $line_item_details->exmp_chrg ?>" name="exmp_chrg">
					<input type="hidden" value='<?php echo $line_item_details->rr_tax_response ?>' name="rr_tax_response">
					<input type="hidden" value='<?php echo $line_item_details->mp_tax_response ?>' name="mp_tax_response">
					<input type="hidden" value="<?php echo $line_item_details->rr_total_tax ?>" name="rr_total_tax">
					<input type="hidden" value="<?php echo $line_item_details->mp_total_tax ?>" name="mp_total_tax">
                    <table class="table table-hover pop-edittable mass-book" style="background:#F1F3F2; border:1px solid #ddd;">
						<tr>
						<?php
					   
						$total_meal_price = $mealratetot + $mealratetottax + $extramealrate;
						$total_rent_price = $roomrenttot + $roomrenttottax + $extraroomrent;
					   ?>
							<td><span>Current Booking Date:</span> <i class="fa fa-inr"></i> <?php echo date('jS M Y',strtotime($details->cust_from_date_actual))." to ".date('jS M Y',strtotime($details->cust_end_date_actual)); ?></td>
                        	<td><span>New Booking Date:</span> <i class="fa fa-inr"></i> <?php echo date('jS M Y',strtotime($start))." to ".date('jS M Y',strtotime($end)); ?></td>
                        </tr>
						<tr>
                        	<td><span>Stay Duration will be changed from:</span> <?php echo $days_prev." days" ?> to <?php echo $days_updated." days" ?></td>
							<td>
							</td>
                        </tr>
						 <tr>
						
                        	<td><span>Current Total Meal Rate:</span> <i class="fa fa-inr"></i> <?php echo $total_meal_price; ?></td>
                       
                        	<td><span>Updated total meal plan rate will be:</span> <i class="fa fa-inr"></i><input type="text" class="form-control input-sm" name="final_sum_amount" value="<?php echo round($total_meal_price*$ratio, 2); ?>" style="display:inline-block; width:20%;"></td>
                        </tr>
                        
                        <tr>
						
                        	<td><span>Current Total Room Rent :</span> <i class="fa fa-inr"></i> <?php echo $total_rent_price; ?></td>
                       
                        	<td><span>Updated total room rent will be:</span> <i class="fa fa-inr"></i> <input type="text" class="form-control input-sm" name="final_sum_amount" value="<?php echo round($total_rent_price*$ratio, 2); ?>" style="display:inline-block; width:20%;"></td>
                        </tr>
                    </table>
                </div>
                <div class="form-actions right">
                	<input class="btn btn-success" type="submit" value="Yes" />
                	<button id="not" type="button" class="btn btn-danger" onClick="closem()" > No </button>
                </div>
            </form>
        <?php }  ?>

    </div>
	<?php } }?>
        <?php if(isset($message)): ?>
		<div class="portlet-body form">
		<div class="form-body">
        <h3 style="text-align:center;"><?php echo $message; ?></h3>
		</div>
		<div class="form-actions right">
			<button id="not" type="button" class="btn btn-danger" onClick="closem()" > OK </button>
		</div>
		</div>
        <?php endif; ?>	
</div>

<script>
//javascript: window.parent.location.reload();

$('#not').click(function(){
//alert("asd");
var val=2;
 close(eval(val));
return false;
});

/*
 function close(result) {
        if (parent && parent.DayPilot && parent.DayPilot.ModalStatic) {
           
            parent.DayPilot.Scheduler.update(result);
             parent.DayPilot.ModalStatic.close(result);
        }
    }*/

    function closem() {      
		//window.parent.location.reload();
		//window.parent.$("#booking_calendar").load( "<?php echo base_url() ?>dashboard/calendar_load" );
		parent.DayPilot.ModalStatic.close();
    }

    
</script> 
<script type="text/javascript">
    $(function() {
    var group_id=document.getElementById("group_id").value;
    if(group_id !="0"){
        parent.window.location.reload();
    //  alert("yes");
    }
});


    function close(){
         parent.window.location.reload();

    }
</script>
</body>