<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
	<div class="caption"> <i class="fa fa-edit"></i><?php if(isset($stat) && $stat){echo $stat;}?></div>
	<div class="tools"> 
	  <a href="javascript:;" class="reload"></a> </div>
	</div>
	<div class="portlet-body">
	  <div class="table-toolbar">
		<div class="row">
		  <div class="col-md-6">
			 <div class="btn-group">
			<button  class="btn green" data-toggle="modal" href="#responsive"> Add booking source <i class="fa fa-plus"></i> </button>
			</div>			
			<div class="btn-group ">
			<button class="btn green" id="status123" onclick="booking_source()"><spam id="demo">Active Booking Source</spam></button>
			</div>
		  </div>
		</div>
	  </div>
	 <div id="table1"> 
     <table class="table table-striped table-bordered table-hover" id="sample_1">
		<thead>
		  <tr>
			<th scope="col"> Id </th>
			<th scope="col"> Booking Source Name </th>
			<th scope="col"> Booking Source Date </th>
		   <!-- <th scope="col">  Booking Source Hotel id </th>-->
			<th scope="col">  Booking Source is ta </th>
			 <th scope="col">  Source type </th>
			 <th scope="col">  Booking Source Description </th>
			 <th scope="col"> Status</th>
			<th scope="col"> Action</th>
		  </tr>
		</thead>
		<tbody>
		  <?php 
		  $id=0;
		  if(isset($data) && $data):

					  $i=1;
					  foreach($data as $gst):
						  $class = ($i%2==0) ? "active" : "success";
						  $id=$gst->booking_source_id;
						  ?>
		  <tr id="row_<?php echo $gst->booking_source_id;?>">
		  <td align="center"><?php if(isset($gst->booking_source_id))echo $gst->booking_source_id; ?></td>
		  <td align="left"><?php echo $gst->booking_source_name;

		  if($gst->booking_default =='1'){
			  
			  ?>
			 <font color="#22FF11"> <i class="fa fa-check"  aria-hidden="true"></i></font>
			 <?php  }?></td>
		  <td align="left"><?php echo $gst->booking_source_date; ?></td>
		 <!-- <td align="center"><?php //echo $gst->booking_source_hotel_id; ?></td>-->
		  <td align="left"><?php echo $gst->booking_source_is_ta; ?></td>
		  <td align="left"><?php echo $gst->source_type	; ?></td>
		  <td align="left"><?php echo $gst->booking_source_description	; ?></td>
			 

		  <td align="left"> <button <?php  if($gst->booking_default =='1'){
				echo "disabled "; }?> class="btn green"  value="<?php echo $gst->booking_source_id;?>" onclick="status(this.value)"><span id="demostat<?php echo $gst->booking_source_id;?>"><?php if($gst->status == 1){ echo  "Active";}
else{
echo "Inactive";
}	?></span></button></td>
			<td align="center" class="ba">  
				<div class="btn-group">
                  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li><a <?php if($gst->booking_default == '1'){echo "disabled ";}  ?> <?php   if(($gst->booking_source_name!='Broker') && ($gst->booking_source_name!='Channel')&& ($gst->booking_source_name!='Corporate')&& ($gst->booking_source_name!='Hotel Website'))
			{?>onclick="soft_delete('<?php echo $gst->booking_source_id;?>')" data-toggle="modal"  <?php } else {echo "onClick='my_alert();'";}?>class="btn red btn-xs" ><i class="fa fa-trash"></i></a></li>
                    <li><a <?php if(($gst->booking_source_name!='Broker') && ($gst->booking_source_name!='Channel') && ($gst->booking_source_name!='Corporate')&& ($gst->booking_source_name!='Hotel Website'))
			{ ?> onclick="edit_booking_source('<?php echo $gst->booking_source_id; ?>')" data-toggle="modal"

				<?php } else {echo "onClick='my_alert();'";}?> class="btn green btn-xs"><i class="fa fa-edit"></i></a> </li>
                  </ul>
                </div>
			
			           
			
			

		
			</td>
		  
		 
		  </tr>
		  <?php endforeach; ?>
		  <?php endif; ?>
		</tbody>
	  </table>
     </div>
	</div>
</div>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">  
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Booking Source</h4>
      </div>
      <?php

	  $form = array(
		  'class' 			=> 'form-body',
		  'id'				=> 'form',
		  'method'			=> 'post'
	  );
	
	  echo form_open_multipart('Unit_class_controller/add_booking_source',$form);
	
	  ?>
      <div class="modal-body">
          <div class="row">
              <div class="form-group form-md-line-input form-md-floating-label col-md-6">                
                  <input type="text" class="form-control" name="name" id="name" required placeholder="Nature of visit Name">
                  <label></label>
				  <span class="help-block">Booking source name </span> 
				  </div>
              
              <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                  <textarea class="form-control" row="3" name="desc" placeholder="Description" id="desc"></textarea>
                  <label></label>
				  <span class="help-block">Description:</span> 
				  </div>
				  
				   <div class="form-group form-md-line-input form-md-floating-label col-md-6">
					<select onchange="check(this.value)" class="form-control"  id="default" name="default" required="required" >
						<option value="" selected="selected" disabled="disabled">Source type</option>
						<?php $source_type=$this->dashboard_model->get_all_source();
							foreach($source_type as $source){
						?>
					  <option value="<?php echo $source->booking_source_id; ?>"><?php echo $source->booking_source_name; ?></option>
							<?php }?>
					</select>
					<label></label>
					<span class="help-block">Booking Source type</span>
				</div>
				  
				  <div class="form-group form-md-line-input form-md-floating-label col-md-6">
					<select onchange="check(this.value)" class="form-control"  id="default" name="default" required="required" >
						<option value="" selected="selected" disabled="disabled">Default nature visit</option>
					  <option value="0">No</option>
					  <option value="1">Yes</option>
					</select>
					<label></label>
					<span class="help-block">Default nature visit *</span>
				</div>
				  
              </div>
         
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
<div id="edit_booking_source" class="modal fade" tabindex="-1" aria-hidden="true">  
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add booking source</h4>
      </div>
      <?php

	  $form = array(
		  'class' 			=> 'form-body',
		  'id'				=> 'form',
		  'method'			=> 'post'
	  );
	
	  echo form_open_multipart('Unit_class_controller/edit_booking_source',$form);
	
	  ?>
      <div class="modal-body">
          <div class="row">
              <div class="form-group form-md-line-input form-md-floating-label col-md-12">                
                  <input type="text" class="form-control" name="name1" id="name1" required placeholder="Nature of visit Name">
                  <label></label>
				  <span class="help-block">Booking source name </span> 
				  </div>
              
              <div class="form-group form-md-line-input form-md-floating-label col-md-12">
                  <textarea class="form-control" row="3" name="desc1" placeholder="Description" id="desc1"></textarea>
                  <label></label>
				  <span class="help-block">Description:</span> 
				  </div>
				    <input type="hidden" name="hid1" id="hid1" >
				  <input type="hidden" name="hid2" id="hid2" >
				  
				  <div class="form-group form-md-line-input form-md-floating-label col-md-12">
					<select onchange="check1(this.value)" class="form-control"  id="default1" name="default1" required="required" >
						<option value="" selected="selected" disabled="disabled">Default nature visit</option>
					  <option value="0">No</option>
					  <option value="1">Yes</option>
					</select>
					<label></label>
					<span class="help-block">Default nature visit *</span>
				</div>
				  
              </div>
         
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/delete_booking_source?f_id="+id,
                data:{id:id},
                success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            $('#row_'+id).remove();

                        });
                }
            });



        });
		
		

    }
	
	
	
	
	
	
	
	

  
function status(id){
		var hid=$('#hid').val();
		//alert(id);
		
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/booking_source_status",
                data:{id:id,status:hid},
                success:function(data)
                {
                   // alert(data);
                   // location.reload();
				   document.getElementById("demostat"+id).innerHTML = data.data;
					$('#row_'+id).remove();
                    
					
                }
            });
		
	}
	
	
	function booking_source(){
		var a=document.getElementById("demo").innerHTML;
		if(a=="Active Booking Source"){			
			document.getElementById("demo").innerHTML ="Inactive Booking Source";			
			
		}else{			
			//$("#status123").removeClass();
			 //$("#status123").css('background-color',"green");
			document.getElementById("demo").innerHTML = "Active Booking Source";
		}
		
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/booking_source_filter",
                data:{status:a},
                 success:function(data)
                {
					//alert(data);
                  $('#table1').html(data);
				   $('#sample_1').dataTable( {
						//"pageLength": 10 
    } );
	
                }
            });
		
	
	}
	
	
   function check(value){
		
		
			if(value==1){	
          $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/check_booking_source_default",
                data:{a:value},
                 success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                 
				$('#responsive').modal('hide');
				swal({   title: "Are you sure?",
				text: data+"!",  
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Fix as Default!",   cancelButtonText: "No, cancel plz!", 
				closeOnConfirm: true,  
				closeOnCancel: true },
				function(isConfirm){   
				if (isConfirm) {     
				$("#default").val('1');$('#responsive').modal('show');
				} else {   
				$("#default").val('0');$('#responsive').modal('show');
				} });



				 
                }
            });
			}
		  
		  
		  //});
		
	}
	
	
	function edit_booking_source(id){
		//alert(id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/get_edit_booking_source",
                data:{id:id},
                success:function(data)
                {
                    //alert(data.id);
				   $('#hid2').val(data.booking_source_id);
                   $('#hid1').val(data.status);
				   
				   $('#name1').val(data.booking_source_name);
				   $('#desc1').val(data.booking_source_description);				   
				   //$('#desc1').val(data.booking_source_hotel_id);
				   $('#default1').val(data.booking_default);
				   
				  // $('#default1').val(data.booking_source_is_ta);					 
					 //$('#default1').val(data.status);
				   //$('#desc1').val(data.status);
				  // $('#unit_class1').val(data.change_date);
				  
				   
				   
                    $('#edit_booking_source').modal('toggle');
					
                }
            });
	}
	
	
	
	 function check1(value){
		
		
			if(value==1){	
          $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/check_booking_source_default",
                data:{a:value},
                 success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                 
				$('#edit_booking_source').modal('hide');
				swal({   title: "Are you sure?",
				text: data+"!",  
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Fix as Default!",   cancelButtonText: "No, cancel plz!", 
				closeOnConfirm: true,  
				closeOnCancel: true },
				function(isConfirm){   
				if (isConfirm) {     
				$("#default").val('1');$('#edit_booking_source').modal('show');
				} else {   
				$("#default").val('0');$('#edit_booking_source').modal('show');
				} });



				 
                }
            });
			}
		  
		  
		  //});
		
	}
</script> 
<script>
	function my_alert()
	{
		//alert("hello");
		swal("Alert", "This Item Cannot Be Deleted / Edited", "error");
	}
	
	</script>

