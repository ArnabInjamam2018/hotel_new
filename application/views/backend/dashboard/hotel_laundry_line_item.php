<div class="row">
  <div class="form-body"> 
    <!-- 17.11.2015-->
    <?php if($this->session->flashdata('err_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-danger alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
    <?php if($this->session->flashdata('succ_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-success alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
  </div>
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-edit"></i>List of All Laundry Fabric Type's </div>
        <div class="tools"> 
          <a href="javascript:;" class="reload"></a> </div>
        </div>
        <div class="portlet-body">
          <div class="table-toolbar">
            <div class="row">
              <div class="col-md-6">
                <div class="btn-group">
                  <button class="btn green"  data-toggle="modal" href="#responsive"> Add New <i class="fa fa-plus"></i> </button>
                </div>
              </div>
            </div>
          </div>
          <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
              <tr>
			    <th scope="col"> Id </th>
                <th scope="col"> Guest id</th>
				<th scope="col"> Item Name</th>
				<th scope="col"> Item Description </th>
				<th scope="col"> Color</th>
				<th scope="col"> Fabric Type </th>
				<th scope="col"> Condition</th>
				<th scope="col"> Delivery Date</th>
				<th scope="col"> Laundry Type</th>
				<th scope="col"> Action</th>
              </tr>
            </thead>
            <tbody>
              <?php if(isset($unit_class) && $unit_class):

                          $i=1;
                          foreach($unit_class as $gst):
                              //$class = ($i%2==0) ? "active" : "success";
                              //$g_id=$gst->hotel_unit_class_id;
                              ?>
              <tr>
			  <td align="center"><?php echo $gst->laundry_id; ?></td>
			  <td align="center"><?php echo $gst->guest_id; ?></td>
			  <td align="center"><?php echo $gst->item_name; ?></td>
			  <td align="center"><?php echo $gst->item_desc; ?></td>
			  <td align="center"><?php echo $gst->color; ?></td>
			  <td align="center"><?php echo $gst->fabric_type; ?></td>
			  <td align="center"><?php echo $gst->item_condition; ?></td>
			  <td align="center"><?php echo $gst->delivery_date; ?></td>
			  <td align="center"><?php echo $gst->laundry_type; ?></td>
			  
               
                <td align="center">  
				
                <a href="<?php echo base_url() ?>unit_class_controller/edit_laundry_fabric_type/<?php echo $gst->laundry_id;?>" class="btn blue"><i class="fa fa-edit"></i></a>            
                <a onclick="soft_delete('<?php echo $gst->laundry_id;?>')" data-toggle="modal"  class="btn red" ><i class="fa fa-trash"></i></a></td>
              </tr>
              <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
    </div>
    
    <!-- END SAMPLE TABLE PORTLET--> 
  </div>
</div>
<!-- /.modal -->
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('unit_class_controller/add_hotel_laundry_fabric_type',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Laundry Type</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
            <div class="col-md-12">
              
			  
			  
              <div class="form-group"style="margin-top:10px;">
                <label class="control-label pull-left" style="margin-left:15px;">Guest id :</label>
                <div class="col-md-12">
                  <input type="text" class="form-control" name="guest_id" id="guest_id" required="required">
                </div>
              </div>
              <br>
              <br>
			  
			  <div class="form-group"style="margin-top:10px;">
                <label class="control-label pull-left" style="margin-left:15px;">Item Name :</label>
                <div class="col-md-12">
                  <input type="text" class="form-control" name="item_name" id="item_name" required="required">
                </div>
              </div>
              <br>
              <br>
			  
			  <div class="form-group"style="margin-top:10px;">
                <label class="control-label pull-left" style="margin-left:15px;">Description:</label>
                <div class="col-md-12">
                  <textarea class="form-control" row="3" name="desc" id="desc" required="required"></textarea>
                </div>
              </div>
			  <br>
			  <br>
			  
			   <div class="form-group"style="margin-top:10px;">
                <label class="control-label pull-left" style="margin-left:15px;">Colour :</label>
                <div class="col-md-12">
                  <input type="text" class="form-control" name="color" id="color" required="required">
                </div>
              </div>
              <br>
              <br>
			  
			   
                <div class="form-group"style="margin-top:10px;">
                <label class="control-label pull-left" style="margin-left:15px;">Fabric Type:</label>
                <div class="col-md-12">
                  <select name="fabric_type" id="fabric_type" class="form-control" required="required">
                    <option value="" disabled="" selected="">--Select Fabric--</option>
					<?php //foreach($unit_class as $uc)
					//{?>
					<option value="<?php //echo $uc->hotel_unit_class_name;?>"><?php //echo $uc->hotel_unit_class_name; ?></option>
					<?php //} ?>
                  </select>
                </div>
              </div>
              <br>
              <br>
			  
			  <div class="form-group"style="margin-top:10px;">
                <label class="control-label pull-left" style="margin-left:15px;">Item Condition:</label>
                <div class="col-md-12">
                  <input type="text" class="form-control" name="item_condition" id="item_condition" required="required">
                </div>
              </div>
              <br>
              <br>
			  
			  <div class="form-group"style="margin-top:10px;">
                <label class="control-label pull-left" style="margin-left:15px;">Delivery date:</label>
                <div class="col-md-12">
                  <input type="date" class="form-control" name="delivery_date" id="delivery_date" required="required">
                </div>
              </div>
              <br>
              <br>
			  
			   <div class="form-group"style="margin-top:10px;">
                <label class="control-label pull-left" style="margin-left:15px;">Laundry Type:</label>
                <div class="col-md-12">
                  <select name="laundry_id" id="laundry_type" class="form-control" required="required">
                    <option value="" disabled="" selected="">--Select Fabric--</option>
					<?php //foreach($unit_class as $uc)
					//{?>
					<option value="<?php //echo $uc->hotel_unit_class_name;?>"><?php //echo $uc->hotel_unit_class_name; ?></option>
					<?php //} ?>
                  </select>
                </div>
              </div>
			  
			            
			 
			  
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php form_close(); ?>
</div>

<!-- /.modal End--> 

<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/delete_hotel_laundry_line_item?f_id="+id,
                data:{f_id:id},
                success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
</script> 
