<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://fonts.googleapis.com/css?family=Exo+2:400,700|Monda:400,700|Open+Sans:400,700|Roboto:400,700" rel="stylesheet">
<link href="<?php echo base_url();?>assets/layouts/layout/css/font.css" rel="stylesheet" type="text/css" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-sweetalert.min.js" type="text/javascript"></script> 
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo base_url();?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url();?>assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?php echo base_url();?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/themes/light2.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
</head>
<body style="background:none;">
<div class="portlet box grey-cascade" style="margin-bottom:0px;">
	<div class="portlet-title">
        <div class="caption">
            <i class="icon-pin font-white"></i> Move Booking Events
        </div>
        <div class="tools" style="display: inline-block; float: right; padding: 12px 0 8px;">
            <a onClick="closem()" style="color:#ffffff;"> <i class="fa fa-times"> </i></a>
        </div>
    </div> 
    <div class="portlet-body form">
			
<?php
	 if (isset($occupancyfailed) && $occupancyfailed=="1") {?>
			<div class="form-body">
        <h3 style="text-align:center;"><?php echo "You Cannot Move in this Room occupancy failed"; ?></h3>
		</div>
		<div class="form-actions right">
			<button id="not" type="button" class="btn btn-danger" onClick="closem()" > OK </button>
		</div>	
    <?php } 
	
	 if (isset($sameroom) && $sameroom=="1") {?>
			<div class="form-body">
        <h3 style="text-align:center;"><?php echo "You Cannot Move in to the same room"; ?></h3>
		</div>
		<div class="form-actions right">
			<button id="not" type="button" class="btn btn-danger" onClick="closem()" > OK </button>
		</div>	
	<?php
	
	 }
	 
	  if (isset($grp_move) && $grp_move=="1") {?>
			<div class="form-body">
        <h3 style="text-align:center;"><?php echo "This feature unavailable please go to details page"; ?></h3>
		</div>
		<div class="form-actions right">
			<button id="not" type="button" class="btn btn-danger" onClick="closem()" > OK </button>
		</div>	
	<?php
	
	 }
	 
	 
	  if (isset($checkout) && $checkout=="1") {?>
			<div class="form-body">
        <h3 style="text-align:center;"><?php echo "This Booking events can not be change"; ?></h3>
		</div>
		<div class="form-actions right">
			<button id="not" type="button" class="btn btn-danger" onClick="closem()" > OK </button>
		</div>	
    <?php }
	

	if (isset($backresize) && $backresize=="1") {?>
			<div class="form-body">
        <h3 style="text-align:center;"><?php echo "You cannot move in back date"; ?></h3>
		</div>
		<div class="form-actions right">
			<button id="not" type="button" class="btn btn-danger" onClick="closem()" > OK </button>
		</div>	
    <?php }
	?>	
    </div>
</div>
<script>
//javascript: window.parent.location.reload();

$('#not').click(function(){
//alert("asd");
var val=2;
 close(eval(val));
return false;
});

/*
 function close(result) {
        if (parent && parent.DayPilot && parent.DayPilot.ModalStatic) {
           
            parent.DayPilot.Scheduler.update(result);
             parent.DayPilot.ModalStatic.close(result);
        }
    }*/

    function closem() {
       
                 //window.parent.location.reload();

                //window.parent.$( "#booking_calendar").load( "<?php echo base_url() ?>dashboard/calendar_load" );
                parent.DayPilot.ModalStatic.close();
            }

    
</script> 
<script type="text/javascript">
    $(function() {
    var group_id=document.getElementById("group_id").value;
    if(group_id !="0"){
        parent.window.location.reload();
    //  alert("yes");
    }
});


    function close(){
         parent.window.location.reload();

    }
</script>
</body>
</html>