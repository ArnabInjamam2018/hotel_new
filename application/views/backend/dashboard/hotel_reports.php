<div style="border: 1px solid #607D8B; border-top: 0;" class="portlet box blue">
  <div class="portlet-title" style="background-color:#607D8B;">
    <div class="caption"> <i syle="font-size:18px;" class="fa fa-bar-chart" aria-hidden="true"></i> <span class="caption-subject bold uppercase"> Reports</span> <span class="pull-right" style="font-size:12px;"> &nbsp click on any report to view it</span></div>
  </div>
   <?php if($this->session->flashdata('warning')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('warning');?></strong> </div>
	  <?php endif;?>
  <div class="portlet-body form">
    <div class="form-body">
      <div class="row" style="padding-bottom:20px;">
	 
	  
	  <div class="col-md-3">
        	<div class="link-block">
                <h4><i style="color:#61BBC2;" class="fa fa-bed" aria-hidden="true"></i> Reservation Reports</h4>
                <div class="link-blockinn">

					<a href="<?php echo base_url();?>reports/reservation_entry_summary" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Summary Report on bookings taken day wise." data-original-title="Daily Reservation Summary" "><i class="fa fa-th-list" aria-hidden="true"></i> Reservation Entry Summary </a> </br>
					
					<a href="<?php echo base_url();?>dashboard/all_bookings" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Entire Month Financial Report" data-original-title="Monthly Summary Reports" "><i class="glyphicon glyphicon-list-alt"></i> All Bookings </a> </br>
					
					<a href="<?php echo base_url();?>reports/monthly_summary_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Entire Month Financial Report" data-original-title="Monthly Summary Reports" "><i class="glyphicon glyphicon-list-alt"></i> Monthly Reservation Summary</a> </br>
					
					<a href="<?php echo base_url();?>reports/monthly_reservation_summary_totals" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Entire Month Financial Report" data-original-title="Monthly Summary Reports" "><i class="glyphicon glyphicon-list-alt"></i> Daily Reservation Summary</a> </br>
					
                    <a href="<?php echo base_url();?>reports/reservation_summary" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Reservation Report from where you can get all type of reservation satus respect of Booking" data-original-title="Reservation Summary" "><i class="fa fa-th-list" aria-hidden="true"></i> Reservation Summary</a></br>
					
                    <a href="<?php echo base_url();?>reports/reservation_list" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Reservation Report from where you can get all type of reservation satus" data-original-title="Reservation List" "><i class="fa fa-th-list" aria-hidden="true"></i> Reservation List</a></br>
					
					<a href="<?php echo base_url();?>reports/checkout_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Report of all Temporary Booking" data-original-title="Temporary Reservations" "><i class="fa fa-th-list" aria-hidden="true"></i> Checkedout Reservations </a> </br>
					
                    <a href="<?php echo base_url();?>reports/reservation_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="An average daily rate (ADR) is a metric widely used in the hospitality industry to indicate the average realized room rental per day. Average daily rate is one of the key performance indicators (KPI)." data-original-title="Reservation(ADR) Report" "><i class="fa fa-th-list" aria-hidden="true"></i> Reservation(ADR) Report</a> </br>
					
					<a href="<?php echo base_url();?>reports/adr_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content=" Average rental income per paid occupied Room on Daily basis" data-original-title="Daily ADR Report" "><i class="glyphicon glyphicon-list-alt"></i> Daily ADR Report </a> </br>
					
                    <a href="<?php echo base_url();?>dashboard/revpar_room_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="RevPar Room Report" data-original-title="RevPar Room Report" "><i class="glyphicon glyphicon-bitcoin"></i> RevPar Room Report</a></br>
                    
					<a href="<?php echo base_url();?>reports/checkInOut_summary" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Report of all Temporary Booking" data-original-title="Temporary Reservations" "><i class="fa fa-th-list" aria-hidden="true"></i> Checkin & Checkout Summary </a></br>
		
                </div>
            </div>
        </div>
		
		<div class="col-md-3">
        	<div class="link-block">
                <h4> <i style="color:#61BBC2;" class="fa fa-bed" aria-hidden="true"></i> Reservation Reports (Additional)</h4>
                <div class="link-blockinn">
					
					<a href="<?php echo base_url();?>reports/discount_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Detalis Discount that given from users has Elaborate on this report " data-original-title="Discount Report" "><i class="fa fa-th-list" aria-hidden="true"></i>  Discount Report</a> </br>
					
                    <a href="<?php echo base_url();?>reports/adjustment_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Detalis Discount that given from users has Elaborate on this report " data-original-title="Discount Report" "><i class="fa fa-th-list" aria-hidden="true"></i>  Adjustment Report</a></br>
					
					<a href="<?php echo base_url();?>reports/extra_charge_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content=" Extra Charges Report is all about extra charge / Servicess that Guest has taken on that Particular time frame" data-original-title="Extra Charge Report" "><i class="fa fa-th-list" aria-hidden="true"></i> Extra Charge Report</a></br>
					
                    <a href="<?php echo base_url();?>reports/note_preference_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="This Report is content of any kind of Preference that Guest want" data-original-title="Note & Preference Report" "><i class="fa fa-th-list" aria-hidden="true"></i> Note & Preference Report</a> </br>
					
					<a href="<?php echo base_url();?>reports/cancel_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="All Cancel Booking have been listed on this Report show You can find out the reason for Cancellation" data-original-title="Cancellation Report" "><i class="fa fa-scissors" aria-hidden="true"></i> Cancellation Report</a> </br>
                    
                    <a href="<?php echo base_url();?>reports/" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="No Show Report" data-original-title="No Show Report" "><i class="fa fa-question" aria-hidden="true"></i> No Show Report </a> </br>
                   
                    
					<a href="<?php echo base_url();?>reports/" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Report of all Temporary Booking" data-original-title="Temporary Reservations" "><i class="fa fa-th-list" aria-hidden="true"></i> Temporary Reservations </a> </br>
					
					
					<a href="<?php echo base_url();?>dashboard/pending_checkin_checkout" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="This Report is content about when payments was pending but Guest already check-out, like Bill-to- company" data-original-title="Pending Payments" "><i class="fa fa-th-list" aria-hidden="true"></i> Pending Payments (Checkout) </a> </br>
					
					<a href="<?php echo base_url();?>dashboard/pending_checkin_checkout" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Report of all Temporary Booking" data-original-title="Temporary Reservations" "><i class="fa fa-th-list" aria-hidden="true"></i> Pending Check in & outs </a> </br>
                </div>
            </div>
        </div>
	  
      	<div class="col-md-3">
        	<div class="link-block">
                <h4><i style="color:#FF4A4A;" class="fa fa-money" aria-hidden="true"></i> Financial Reports</h4>
                <div class="link-blockinn">
                    
					
					
					<a href="<?php echo base_url();?>reports/admin_transaction_summary" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="User Wise Transaction Report" data-original-title="Admin Transaction Summary" "><i class="fa fa-book" aria-hidden="true"></i> Admin Transaction Summary</a> </br>
					
					<a href="<?php echo base_url();?>reports/transaction_summary" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Transaction Summary report" data-original-title="Transaction Summary report" "><i class="fa fa-book" aria-hidden="true"></i> Transaction Summary report </a> </br>
					
                    <a href="<?php echo base_url();?>reports/all_f_reports" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Day Wise Financial Report" data-original-title="Daily Financial Report" "><i class="fa fa-book" aria-hidden="true"></i> Daily Financial Report</a> </br>
                    
					<a href="<?php echo base_url();?>reports/daily_transactional_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Day Wise Financial Report" data-original-title="Daily Transactional Report" "><i class="fa fa-book" aria-hidden="true"></i> Daily Transactional Report</a> </br>
					
                    <a href="<?php echo base_url();?>reports/booking_sales_report2" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Daily Sales Report with all type of Transaction" data-original-title="Sales Report" "><i class="fa fa-book" aria-hidden="true"></i> Sales Report</a> </br>
					
                    <a href="<?php echo base_url();?>reports/checkout_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Daily Sales Report with all type of Transaction" data-original-title="Sales Report" "><i class="fa fa-book" aria-hidden="true"></i> CheckOut Sales Report</a> </br>
					
                    
                </div>
            </div>
        </div>
		
		<div class="col-md-3">
        	<div class="link-block">
               <h4><i style="color:#357864;" class="fa fa-motorcycle" aria-hidden="true"></i> Guest Reports</h4>
                <div class="link-blockinn">
				
                    <a href="<?php echo base_url();?>reports/all_reports" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Detalis Report of Customer" data-original-title="Customer Report" "><i class="fa fa-th-list" aria-hidden="true"></i> Customer Report</a> </br>
					
                    <a href="<?php echo base_url();?>reports/cust_police_reports" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Customer Report that Require in Police Station" data-original-title="Customer Police Report" "> <i class="fa fa-th-list" aria-hidden="true"></i> Customer Police Report</a>  
                    <a href="<?php echo base_url();?>reports/guest_stay" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Guest in house List" data-original-title="Guest in house List" "> <i class="fa fa-th-list" aria-hidden="true"></i> Guest Stay Report </a>  </br>
					
                    <a href="<?php echo base_url();?>" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Guest in house List" data-original-title="Guest in house List" "> <i class="fa fa-th-list" aria-hidden="true"></i> Guest in house List </a>  </br>
					
                    <a href="<?php echo base_url();?>" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Report of VIP Guest" data-original-title="VIP Guest Report" "> <i class="fa fa-th-list" aria-hidden="true"></i> VIP Guest Report </a>  
					
					<a href="<?php echo base_url();?>reports/occupied_room_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Report of VIP Guest" data-original-title="VIP Guest Report" "> <i class="fa fa-th-list" aria-hidden="true"></i> Daily Occupancy Report </a>
                </div>
            </div>
        </div>
        		
      </div>
	  
	  <div class="row" style="padding-bottom:20px;">	  
		
		<div class="col-md-3">
        	<div class="link-block">
                <h4><i style="color:#333876;" class="fa fa-user-circle-o" aria-hidden="true"></i> User Reports</h4>
                <div class="link-blockinn">
				
                    <a href="<?php echo base_url();?>reports/admin_transaction_summary" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="User wise Transaction Summary Report" data-original-title="User Transaction Summary" "><i class="fa fa-book" aria-hidden="true"></i> User Transaction Summary</a> </br>
					
					<a href="<?php echo base_url();?>" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="User Wise Booking Summary Report" data-original-title="User Booking Summary" "><i class="fa fa-book" aria-hidden="true"></i> User Booking Summary</a> </br>
					
					<a href="<?php echo base_url()?>reports/all_login_reports" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Detalis of all User loging Report" data-original-title="User Login Report" "><i class="fa fa-book" aria-hidden="true"></i> User Login Report</a> </br>	
					
					<a href="<?php echo base_url()?>reports/invoice_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Detalis of all User loging Report" data-original-title="User Login Report" "><i class="fa fa-book" aria-hidden="true"></i> Invoice Report</a> </br>	
                </div>
            </div>
        </div>
	  
		<div class="col-md-3">
        	<div class="link-block">
                <h4><i style="color:#E11515;" class="fa fa-book" aria-hidden="true"></i> Tax Reports</h4>
                <div class="link-blockinn">
				
                    <a href="<?php echo base_url();?>reports/tax_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="A complete Tax Report of Hotel" data-original-title="Tax Report" "><i class="fa fa-th-list" aria-hidden="true"></i> Tax Report</a> </br>
					
                    <a href="<?php echo base_url();?>reports/lodging_tax_report" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Detalis Tax Reports for Lodging" data-original-title="Lodging Tax Report" "><i class="fa fa-th-list" aria-hidden="true"></i> Lodging Tax Report</a>
					
                </div>
            </div>
        </div>
		
		<div class="col-md-3">
        	<div class="link-block">
                <h4> <i style="color:#37A55A;" class="fa fa-cubes" aria-hidden="true"></i> Stock & Inventory </h4>
                <div class="link-blockinn">
				
                    <a href="<?php echo base_url();?>dashboard/stock_invent_log" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="A complete Tax Report of Hotel" data-original-title="Tax Report" "><i class="fa fa-cube" aria-hidden="true"></i> Stock & Inventory Log </a></br>
					
					<a href="<?php echo base_url();?>reports/" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content=" Welcome Kit & Inventory Report" data-original-title=" Kit & Inventory Report" "><i class="fa fa-shopping-basket" aria-hidden="true"></i> Kit & Inventory Report</a>
                     
                </div>
            </div>
        </div>
		
		<div class="col-md-3">
        	<div class="link-block">
                <h4><i style="color:#2A799B;" class="fa fa-tripadvisor" aria-hidden="true"></i> Travel Agent </h4>
                <div class="link-blockinn">
                    <a href="<?php echo base_url();?>reports/travelagentcommission" class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="A complete Tax Report of Hotel" data-original-title="Tax Report" "><i class="fa fa-th-list" aria-hidden="true"></i> Travel Agent Commision</a></br>
                     
                </div>
            </div>
        </div>
		
		
		
	  </div>
    </div>
  </div>
  <!-- END CONTENT --> 
</div>
