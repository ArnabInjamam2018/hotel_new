<?php

$rooms = $this->dashboard_model->all_rooms();

$maid_countation = $maids = $this->dashboard_model->all_maid();

$maid_count = count( $maid_countation );

$room_count = count( $rooms );

$restriction = ceil( $room_count / $maid_count );

//echo $restriction;

?>



<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">

	<div class="modal-dialog">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

				<h4 class="modal-title">Add Maid </h4>

			</div>

			<?php

			$form = array(

				'class' => '',

				'id' => 'form',

				'method' => 'post'

			);

			echo form_open_multipart( 'dashboard/add_maid', $form );

			?>





			<div class="modal-body">

				<div class="row">

					<div class="col-md-4">

						<div class="form-group form-md-line-input">

							<input type="text" autocomplete="off" class="form-control" name="maid_name" placeholder="Staff Name *">

							<label></label>

							<span class="help-block">Staff Name *</span> </div>

					</div>

					<div class="col-md-4">

						<div class="form-group form-md-line-input">

							<select class="form-control bs-select" name="type" required="required">

								<option value="">Select Type</option>

								<?php $maids=$this->dashboard_model->get_all_staff_type();

                  if(isset($maids) && $maids)

                  {

                    foreach($maids as $maid)

                    {

                    ?>

								<option value="<?php echo $maid->housekeeping_staff_type_name ?>">
									<?php echo $maid->housekeeping_staff_type_name ?>
								</option>

								<?php

								}

								}

								?>

							</select>

							<label></label>

							<span class="help-block">Select Type *</span> </div>

					</div>

					<div class="col-md-4">

						<div class="form-group form-md-line-input">

							<input type="text" autocomplete="off" class="form-control" name="max_alo_unit" placeholder="Max allocation count *">

							<label></label>

							<span class="help-block">Max allocation count *</span> </div>

					</div>

					<div class="col-md-4">

						<div class="form-group form-md-line-input">

							<input type="text" autocomplete="off" class="form-control" name="maid_contact" required="required" maxlength="10" onKeyPress="return onlyNos(event, this);" placeholder="Staff Phone No *">

							<label></label>

							<span class="help-block">Staff Phone No *</span> </div>

					</div>

					<div class="col-md-4">

						<div class="form-group form-md-line-input">

							<input type="text" autocomplete="off" class="form-control" name="maid_sec" required="required" placeholder="Staff Section *">

							<label></label>

							<span class="help-block">Staff Section *</span> </div>

					</div>

					<div class="col-md-4">

						<div class="form-group form-md-line-input">

							<select class="form-control bs-select" name="status" required="required">

								<option value="" selected="selected" disabled="disabled">Select Availability</option>

								<option value="Available">Available</option>

								<option value="Not available"> Not Available</option>

							</select>

							<label></label>

							<span class="help-block">Select Availability *</span> </div>

					</div>

					<div class="col-md-12">

						<div class="form-group form-md-line-input">

							<textarea cols="" rows="" autocomplete="off" class="form-control" name="maid_address" placeholder="Staff Address *"></textarea>

							<label></label>

							<span class="help-block">Staff Address *</span> </div>

					</div>

				</div>

			</div>

			<div class="modal-footer">

				<button type="submit" class="btn blue">Submit</button>

				<button type="submit" class="btn default">Reset</button>

			</div>

			<?php form_close(); ?>

		</div>

	</div>

</div>



<div id="responsive2" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">

	<div class="modal-dialog">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" id="clickclick" data-dismiss="modal" aria-hidden="true"></button>

				<h4 class="modal-title">Assign Staff</h4>

			</div>

			<?php



			$form = array(

				'class' => '',

				'id' => 'form',

				'method' => 'post'

			);



			echo form_open_multipart( 'dashboard/assign_maid', $form );



			?>

			<div class="modal-body" id="popupmodal" style="height:225px;">

				<div class="row">

					<div class="col-md-4">

						<div class="form-group form-md-line-input">

							<input type="text" autocomplete="off" class="form-control" name="room_no" id="room_no" placeholder="Room No *">

							<input type="hidden" autocomplete="off" class="form-control" name="room_id" id="room_id">

							<input type="hidden" autocomplete="off" class="form-control" name="auditor_id" id="auditr_indtity">

							<input type="hidden" autocomplete="off" class="form-control" name="prop_type" id="prop_type"/>

							<label></label>

							<span class="help-block">Room No *</span> </div>

					</div>

					<div class="col-md-4">

						<div class="form-group form-md-line-input">

							<select class="form-control bs-select" id="maid_id" name="maid_id" onchange="get_staff(this.value)">

								<option value="" selected="selected">Select Staff Type</option>

								<?php $maids=$this->dashboard_model->get_all_staff_type();

                  if(isset($maids) && $maids)

                  {

                    foreach($maids as $maid)

                    {

                    ?>

								<option value="<?php echo $maid->housekeeping_staff_type_name ?>">
									<?php echo $maid->housekeeping_staff_type_name ?>
								</option>

								<?php

								}

								}

								?>

							</select>

							<label></label>

							<span class="help-block">Staff Type *</span>

						</div>

					</div>



					<div class="col-md-4">

						<div class="form-group form-md-line-input">

							<input type="text" autocomplete="off" class="form-control" name="note" id="note" placeholder="Note">



							<label></label>

							<span class="help-block">Note</span> </div>

					</div>



					<div class="col-md-4">

						<div class="form-group form-md-line-input" id="hd">

						</div>

					</div>

				</div>

			</div>

			<div class="modal-footer">

				<button type="submit" onclick="assign_maid();autoRefresh_div();return false;" class="btn submit">Assign</button>

			</div>

			<?php form_close(); ?>

		</div>

	</div>

</div>





<div class="form-body">

	<!-- 17.11.2015-->

	<?php if($this->session->flashdata('err_msg')):?>

	<div class="alert alert-danger alert-dismissible text-center" role="alert">

		<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>

		<strong>
			<?php echo $this->session->flashdata('err_msg');?>
		</strong>
	</div>

	<?php endif;?>

	<?php if($this->session->flashdata('succ_msg')):?>

	<div class="alert alert-success alert-dismissible text-center" role="alert">

		<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>

		<strong>
			<?php echo $this->session->flashdata('succ_msg');?>
		</strong>
	</div>

	<?php endif;?>

</div>

<div id="refresh">

	<div class="portlet light portlet-fit bordered">

		<div class="portlet-title">

			<div class="caption"> <i class=" icon-layers font-green"></i> <span class="caption-subject font-green bold uppercase">Unit Housekeeping Status</span> <span class="caption-helper">Units</span> </div>

			<div class="actions">

				<div class="btn-group">

					<a class="btn btn-circle btn-default " href="javascript:;" data-toggle="dropdown" aria-expanded="false">

                                                <i class="fa fa-level-down" aria-hidden="true"></i> Units/ Sections

                                                <i class="fa fa-angle-down"></i>

                                            </a>

				





					<ul class="dropdown-menu pull-right">



						<li>

							<a href="javascript:;" style="'.$stl.'">

                                                <i class="fa fa-tag" aria-hidden="true"></i>Units</a>

						



						</li>



						<li>

							<a href="javascript:;" style="'.$stl.'">

                                                <i class="fa fa-tag" aria-hidden="true"></i>Housekeeping Sections</a>

						



						</li>



					</ul>

				</div>



				<div class="btn-group">

					<a class="btn btn-circle btn-default " href="javascript:;" data-toggle="dropdown" aria-expanded="false">

                                                <i class="fa fa-level-down" aria-hidden="true"></i> Filters & Options

                                                <i class="fa fa-angle-down"></i>

                                            </a>

				





					<ul class="dropdown-menu pull-right">

						<?php

						$hks = $this->dashboard_model->get_all_housekeeping_status();

						$stl = '';

						if ( isset( $hks ) && $hks ) {



							foreach ( $hks as $h ) {



								$stl = 'color:' . $h->color_primary;



								echo '<li>';

								echo '<a href="javascript:;" style="' . $stl . '">';

								echo '<i class="fa fa-tag" aria-hidden="true"></i> ' . $h->status_name . '</a>';

								echo '</li>';



							}

						}

						?>

						<li class="divider"> </li>

						<li>

							<a href="javascript:;"> Add Housekeeping Staff </a>

						</li>

						<li>

							<a href="javascript:;"> Housekeeping Log </a>

						</li>

						<li>

							<a href="javascript:;"> Room Housekeeping Status </a>

						</li>



					</ul>

				</div>



			</div>



		</div>



		<div class="portlet-body">

			<div class="row">

				<?php

				if ( isset( $rooms ) && $rooms ) {

					foreach ( $rooms as $room ) {

						$maid_to_assigned = $this->dashboard_model->room_maid_match_row( $room->room_id );

						$date1 = date( "Y-m-d", strtotime( date( "Y-m-d" ) ) );

						$array = $this->dashboard_model->get_booking_details_by_date( $date1, $this->session->userdata( 'user_hotel' ) );

						/*print_r($array);

						 exit();*/



						if ( !empty( $maid_to_assigned->maid_name ) ) {

							$m_name = $maid_to_assigned->maid_id;

							$audi_id = $maid_to_assigned->auditor_id;

						}

						$out = 0;

						if ( $array != false )

						{

							foreach ( $array as $ar )

							{

								if ( ( $ar->room_id == $room->room_id ) && ( $ar->confirmed_checkin_time != "00:00:00" ) ) {
									$out++;
								}

							}

						}

						$bgColor = $this->dashboard_model->get_color_by_houskeeping_status_id( $room->clean_id );

						?>

				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 30px;">

					<div class="dashboard-stat green-seagreen" style="background-color:<?php if(isset($bgColor->color_primary)) {echo $bgColor->color_primary ;} else echo '#666666';?>; overflow:visible;">

						<div class="clearfix keep-deta">

							<div class="maid-name col-md-5" style="padding-right:0;">

								<ul class="maid-name-listeee">

			<?php  

                  $i=0;

                  $maids=$this->dashboard_model->get_all_staff_type();

                  if(isset($maids) && $maids) {

                    $count=0;

                    foreach($maids as $maid) {

						$rmm = $this->dashboard_model->get_housekeeping_pending_asso_staff($room->room_id,$maid->housekeeping_staff_type_name,'room');
						
						//echo $room->room_id.' '.$maid->housekeeping_staff_type_name.'</br>';
						//print_r($rmm);

							//print_r($rmm);

						if(isset($rmm) && $rmm) {

                                    foreach($rmm as $assign) {

										if($assign->staff_id>0){

                                            $maidName=$this->dashboard_model->get_maid_by_id($assign->staff_id);

										}//echo $assign->staff_id;	

									
										//print_r($maidName);

			?>

									<li class='mnl-list' style="background-color:#<?php echo ($i+0).($i+0).($i+0);?>">
										<a href="#responsive3" data-toggle="modal" onclick="show_maid('<?php echo $maid->housekeeping_staff_type_name;?>','<?php echo $maidName->maid_name;?>','<?php echo $assign->assigned_date;?>')">

											<?php if(isset($maidName))echo "<span>".$maidName->maid_name."</span>"."<span><strong> ".$maid->housekeeping_staff_type_name."</strong></span>";?>

										</a>

									</li>

									<?php

									}

									$count++;

									}

									$i = $i + 2;

									}

									if ( $count == 0 )

										echo "<li style='background-color:#abc'><span>No Staff Assigned!</span></li>";

									}
									?>

								</ul>



							</div>

							<div class="room-icon col-md-7 text-right" style="padding-bottom: 8px;">

								<a data-toggle="modal" href="#responsive2" onclick="set_value('<?php echo $room->room_id; ?>','<?php echo $room->room_no; ?>','<?php if(isset($m_name)){ echo $m_name; }?>','<?php echo " room "; ?>');" class="room-no">

									<?php if(isset($room->room_no) && $room->room_no ) echo $room->room_no; ?>

								</a>

								<h6 style="color:#ffffff;"><strong>

					<?php 

					$utbi =  $this->dashboard_model->get_unit_type_by_id($room->unit_id); 

					//print_r($utbi);

					if(isset($utbi)){

						echo $utbi->unit_name;

					}

					?>

					</strong></h6>

							

								<?php if($room->room_audit_status=='ongoing'){

                  echo "<i class='fa fa-eye'></i>";

                 }

                 else

                 {

					echo "<i class='fa fa-check'></i>";

                 }

                 if($out>0){?>

								<i class="fa fa-bed"></i>

								<?php $out=0;}

                 ?>

							</div>

						</div>

						<div class="form-group clearfix" style="clear:both; padding-top:5px;">

							<label class="col-md-4 control-label" style="color:#ffffff; padding-top:8px;">Status</label>

							<div class="col-md-8">

								<select onchange="status_change('<?php echo $room->room_id; ?>',this.value,'room');" class="form-control input-sm bs-select">

									<?php 

                  //$hks=$this->dashboard_model->get_all_housekeeping_status();

                  if(isset($hks) && $hks ){

					echo "<option disabled selected>Select Status</option>";

                  foreach($hks as $h) 

                  {?>

									<option value="<?php echo $h->status_id;?>" <?php if ($h->status_id==$room->clean_id) echo "selected";?>>
										<?php echo $h->status_name;?>
									</option>

									<?php }} ?>

								</select>

							</div>

						</div>

					</div>

				</div>

				<?php }}?>

			</div>

		</div>

	</div>

	<div class="portlet box blue">

		<div class="portlet-title">

			<div class="caption"> <i class=" icon-layers"></i> <span class="caption-subject bold uppercase">Housekeeping Sections</span> </div>

		</div>

		<div class="portlet-body form">

			<div class="form-body">

				<div class="row" style="padding-top: 30px;">

					<?php

					$sec = $this->dashboard_model->get_section();

					if ( isset( $sec ) && $sec ) {

						foreach ( $sec as $sc ) {



							?>

					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 30px;">

						<div class="dashboard-stat green-seagreen" Style="background-color:<?php 

		$color_primary=$this->dashboard_model->get_color_by_houskeeping_status_id($sc->sec_housekeeping_status);

		if(isset($color_primary) && $color_primary)

		echo $color_primary->color_primary;?>; overflow:visible;">

							<div class="clearfix keep-deta">

								<div class="maid-name col-md-5" style="padding-right:0;">

									<ul class="maid-name-listeee">

										<?php  

                  $i=0;

                  $maids=$this->dashboard_model->get_all_staff_type();

                  if(isset($maids) && $maids)

                  {

                      $count=0;

                    foreach($maids as $maid)

                    {

                        

                  

                  $rmm=$this->dashboard_model->get_housekeeping_pending_asso_staff($room->room_id,$maid->housekeeping_staff_type_name,'room');

							//print_r($rmm);

						 if(isset($rmm) && $rmm)

                                {

                                                

                                            foreach($rmm as $assign)

                                            {   if($assign->staff_id>0){

                                                $maidName=$this->dashboard_model->get_maid_by_id($assign->staff_id);

											}//echo $assign->staff_id;	

											

										//print_r($maidName);

												 ?>

										<li class='mnl-list' style="background-color:#<?php echo ($i+0).($i+0).($i+0);?>">

											<a href="#responsive3" data-toggle="modal" onclick="show_maid('<?php echo $maid->housekeeping_staff_type_name;?>','<?php echo $maidName->maid_name;?>','<?php echo $assign->assigned_date;?>')">

												<?php if(isset($maidName))echo "<span>".$maidName->maid_name."</span>"."<span><strong> ".$maid->housekeeping_staff_type_name."</strong></span>";?>

											</a>

										</li>

										<?php

										}

										$count++;

										}

										$i = $i + 2;

										}

										if ( $count == 0 )

											echo "<li style='background-color:#abc'><span>No Staff Assigned !!</span></li>";

										}
										?>

									</ul>

								</div>



								<div class="room-icon col-md-7 text-right" style="padding-bottom: 8px;">

									<a data-toggle="modal" href="#responsive2" onclick="set_value('<?php if(isset($sc->sec_id)) { echo $sc->sec_id;} ?>','<?php if(isset($sc->sec_name) && $sc->sec_name) echo $sc->sec_name; ?>','<?php if(isset($m_name) && $m_name){ echo $m_name; }?>','section');" class="room-no " style="padding-left:0;">

										<?php if(isset($sc->sec_name)) { echo $sc->sec_name; } ?>

									</a>

									<h6 style="color:#ffffff;"><strong><?php if(isset($sc->sec_desc) && $sc->sec_desc) echo $sc->sec_desc; ?></strong></h6>

									<?php if($sc->sec_audit_status=='ongoing'){

              echo "<i class='fa fa-eye'></i>";

             }

             else

             {

             echo "<i class='fa fa-check'></i>";

                }



             ?>

								</div>

							</div>

							<div class="form-group clearfix" style="clear:both;">

								<label class="col-md-4 control-label" style="color:#ffffff">Status</label>

								<div class="col-md-8">

									<select onchange="status_change('<?php echo $sc->sec_id; ?>',this.value,'section');" class="form-control input-sm bs-select">

										<?php 

              $hks=$this->dashboard_model->get_all_housekeeping_status();

                if(isset($hks) && $hks){

             foreach($hks as $h) 

              {?>

										<option value="<?php if($h->status_id);?>" <?php if ($h->status_id==$sc->sec_housekeeping_status) echo "selected";?>>
											<?php echo $h->status_name;?>
										</option>

										<?php } }?>

									</select>

								</div>

							</div>

						</div>

					</div>

					<?php }}?>

				</div>

			</div>

		</div>

	</div>

</div>

<div id="responsive3" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">

	<div class="modal-dialog">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

				<h4 class="modal-title">Assigned Staff </h4>

			</div>

			<div class="modal-body">

				<div class="row">

					<div class="col-md-4">

						<div class="form-group form-md-line-input">

							<input type="text" autocomplete="off" class="form-control" name="note" id="assign_date" placeholder="Assign Date" readonly disabled>



							<label></label>

							<span class="help-block">Assign Date</span> </div>

					</div>
					<div class="col-md-4">

						<div class="form-group form-md-line-input">

							<input type="text" autocomplete="off" class="form-control" name="note" id="maid_name" placeholder="Maid Name" readonly disabled>



							<label></label>

							<span class="help-block">Maid Name</span> </div>

					</div>

					<div class="col-md-4">

						<div class="form-group form-md-line-input">

							<input type="text" autocomplete="off" class="form-control" name="note" id="maid_type" placeholder="Maid Type" readonly disabled>



							<label></label>

							<span class="help-block">Maid Type</span> </div>

					</div>

				</div>



			</div>

			<div class="modal-footer">

				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>



			</div>



		</div>

	</div>

</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
	function autoRefresh_div()

	{

		$( "#popupmodal" ).load( location.href + " #popupmodal>*", "" ); // a function which will load data from other file after x seconds

	}



	setInterval( 'autoRefresh_div()', 10000 ); // refresh div after 10 secs
</script>

<script>
	function status_change( r_id, r_status, prop_type ) {

		$.ajax( {

			type: "POST",

			url: "<?php echo base_url()?>dashboard/change_room_status",

			data: {
				room_id: r_id,
				clean_status: r_status,
				prop_type: prop_type
			},

			success: function ( data )

			{ //alert(data);

				swal( {

						title: data,

						text: "",

						type: "success"

					},

					function () {



						// location.reload("<?php echo base_url() ?>dashboard/housekeeping");

						$( "#refresh" ).load( "<?php echo base_url() ?>dashboard/room_status" );

						document.getElementById( "clickclick" ).click();



					} );

			}

		} );

	}

	function assign_maid() {
		
		var room_id = document.getElementById( 'room_id' ).value;
		var maid_id = document.getElementById( 'maid_id' ).value;
		var staff_name = $( "#staff_name" ).val();
		var prop_type = $( "#prop_type" ).val();
		var note = $( "#note" ).val();

		if ( room_id && maid_id && staff_name && prop_type ) {

			console.log('calling assign_staff with room_id = '+room_id+', staff_id = '+staff_name+', maid_id = '+maid_id+', prop_type ='+prop_type+'');
			
			$.ajax({
				cache: false,
				type: "POST",
				url: "<?php echo base_url()?>dashboard/assign_staff",

				data: {
					room_id: room_id,
					maid_id: maid_id,
					staff_id: staff_name,
					prop_type: prop_type,
					note: note
				},

				success: function ( data ) {
					$( '#responsive2' ).modal( 'hide' );

					swal({
							title: data.msg,
							text: "",
							type: data.status
						},

						function () {
							$( '#hd' ).css( "display", "none" );
							$( "#refresh" ).load( "<?php echo base_url() ?>dashboard/room_status" );

							document.getElementById( "clickclick" ).click();

							$.getScript( '<?php echo base_url();?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' );
							$.getScript( '<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js' );
						});

				}

			});

		} else {

			$( '#responsive2' ).modal( 'hide' );

			swal( "Error!", "Please enter all information!", "warning" );

			//$('#responsive2').modal('show');

		}

	}

	function show_modal() {

		document.getElementById( 'responsive' ).style.display = "block";

	}

	function set_value( id, no, name, prop_type ) {

		//alert(id+' '+no);

		//alert(prop_type);

		document.getElementById( 'prop_type' ).value = prop_type;

		document.getElementById( 'room_id' ).value = id;

		document.getElementById( 'room_no' ).value = no;

		//document.getElementById('default_maid_id').value=name;



		//$('#prop_type').value(prop_type);



	}

	function check_name() {

		var idntity = new Array();

		var maid_identity = document.getElementById( 'maid_id' );

		var deflt = document.getElementById( 'default_maid_id' ).value;

		//alert(maid_identity + deflt);

		for ( i = 1; i < maid_identity.options.length; i++ ) {

			maid_identity.options[ i ].disabled = false;

		}



		for ( i = 1; i < maid_identity.options.length; i++ ) {

			idntity[ i ] = maid_identity.options[ i ].value;

			if ( maid_identity.options[ i ].value == deflt )

			{

				maid_identity.options[ i ].disabled = true;

			}

		}

	}



	function auditor_oc( auditr_id ) {

		//alert("HERE");

		document.getElementById( 'auditr_indtity' ).value = auditr_id;

	}

	function get_staff( a )	{

		//alert( a );

		$.ajax({

			type: "POST",
			url: "<?php echo base_url()?>dashboard/get_staff_by_type",
			data: {
				type: a
			},

			success: function ( data ) { 

				$.getScript( '<?php echo base_url();?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' );
				$.getScript( '<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js' );

				var op = "<select class='form-control bs-select' onchange='auditor_oc(this.value)' name='staff_name' id='staff_name' >" + data + "</select><span class='help-block'>Staff Name *</span>";

				$( '#hd' ).html( op );

			}

		});

	}
</script>

<script>
	function show_maid( type, staff, date ) {



		$( "#maid_name" ).val( staff );

		$( "#maid_type" ).val( type );

		$( "#assign_date" ).val( date );

	}
</script>