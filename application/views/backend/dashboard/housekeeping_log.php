<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-file-archive-o"></i>Housekeeping Logs </div>
  </div>
  <div class="portlet-body">    
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <th>Log Index</th>
          <th>Room No</th>
          <th>Staff Type</th>
          <th>Staff Name</th>
          <th>Assignment Type</th>
          <th>Assigned Date</th>
          <th>Resolved Date </th>
          <th>Log Status </th>
          <th>Notes</th>
        </tr>
      </thead>
      <tbody>
        <?php 
                    if(isset($logs) && $logs){
                    $srl_no = 0;
                    //print_r($maids);
                    foreach($logs as $maids){
                    $srl_no++;
                     
              ?>
      <tr>
       
                <td><?php echo $maids->log_id;?></td>
                <td>
					<?php // Room No
						$rm_no=$this->dashboard_model->get_room_no($maids->room_id);
						if($rm_no!=''){
							echo $rm_no->room_no;
						}
					?>
				</td>	
                <td>
					<?php // Staff Type
						if($maids->staff_type!=''){
							echo $maids->staff_type;
						}
					?>
				</td>
                <td>
					<?php // Staff Name
						$maid_name1 =$this->dashboard_model->get_maid_by_id($maids->staff_id);
						if(isset($maid_name1)){
							echo $maid_name1->maid_name;
					}?>
				</td>
                <td>
					<?php // Assignment type
						$h_n_w= $this->dashboard_model->get_assignment_type_by_id($maids->assignment_type);
						if(isset($h_n_w) && $h_n_w != NULL){
							echo $h_n_w->housekeeping_nature_work_name;	
						}else{
							echo "Undefined";
						}
					?>
				</td>
                <td>
					<?php 
						echo date("g:ia \-\n jS M Y",strtotime($maids->assigned_date));
					?>
				</td>
                <td>
					<?php 
						if($maids->compleated_date!=0){
						echo date("g:ia \-\n jS M Y",strtotime($maids->compleated_date));
						}
					?>
				</td>
                <td>
					<?php 
						if($maids->status == 'Done')
							echo '<span class="label" style="background-color:#1C9A71;">'.$maids->status.'</span>';
						else 
							echo '<span class="label" style="background-color:#F3565D;">'.$maids->status.'</span>';
					?>
				</td>
                <td>
					<?php 
						if($maids->notes!=''){
							echo $maids->notes;
						}else{
							echo "None";
						}
					?>
				</td>
          </tr>
             <?php }}?>
      </tbody>
    </table>
  </div>
</div>
											
