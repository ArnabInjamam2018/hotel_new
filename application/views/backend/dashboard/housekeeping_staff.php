<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-file-archive-o"></i>Housekepping Staff </div>
		<div class="actions">
			<a class="btn btn-circle green btn-outline btn-sm hidden-print" data-toggle="modal" href="#responsive" id="dwn_link" onclick="show_modal();"> <i class="fa fa-plus-circle"></i>Add Maid  </a>
		</div>
	</div>
	<div class="portlet-body">
		
		<table class="table table-striped table-bordered table-hover" id="sample_1">
			<thead>
				<tr>
					<th> # </th>
					<th>Name </th>
					<th>Availability </th>
					<th>Type </th>
					<th>Contact No </th>
					<th>Address </th>
					<th>Section </th>
					<th>Status </th>
					<th>Max Allocation</th>
					<th>Current Allocation</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php

				//$currentAllocation = $this->dashboard_model->get_unit_class_list();

				if ( isset( $maids ) && $maids != false ) {
					$srl_no = 0;
					//print_r($maids);
					foreach ( $maids as $maids ) {
						$srl_no++;
						$m_id = $maids->maid_id;

						$currentAllocation = $this->dashboard_model->current_allocation( $m_id );
						if ( $currentAllocation == '' ) {
							$currentAllocation = 0;
						}
						?>
				<tr>
					<td>
						<?php echo $srl_no;?>
					</td>
					<td>
						<?php echo $maids->maid_name;?>
					</td>
					<td>
						<?php 
			if(strtolower($maids->staff_availability) == "available")
				echo $maids->staff_availability;
			else
				echo '<span style="color:#AAAAAA;">'.$maids->staff_availability.'</span>';
		?>
					</td>
					<td>
						<?php echo $maids->type;?>
					</td>
					<td>
						<?php echo $maids->maid_contact;?>
					</td>
					<td>
						<?php echo $maids->maid_address;?>
					</td>
					<td>
						<?php echo $maids->section;?>
					</td>
					<td>
						<?php if($currentAllocation){ echo "Assigned";} else { echo "Un Assigned";}?>
					</td>
					<td>
						<?php echo $maids->max_alo_unit;?>
					</td>
					<td>
						<?php 
			
			if($currentAllocation == 0)
				echo '<span style="color:#AAAAAA;">'.$currentAllocation.'</span>';
			else
				echo '<span style="color:#f94f4c; font-weight:800;">'.$currentAllocation.'</span>';
		?>
					</td>
					<td class="ba">
						<div class="btn-group">
							<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li><a onclick="soft_delete(<?php echo $maids->maid_id;?>)" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a>
								</li>
								<li><a href="<?php echo base_url() ?>dashboard/edit_maid?maid_id=<?php echo $maids->maid_id;?>" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a>
								</li>
							</ul>
						</div>
					</td>
				</tr>
				<?php }}?>
			</tbody>
		</table>
	</div>
</div>


<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add Maid </h4>
			</div>
			<?php
			$form = array(
				'class' => '',
				'id' => 'form',
				'method' => 'post'
			);
			echo form_open_multipart( 'dashboard/add_maid', $form );
			?>


			<div class="modal-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input type="text" autocomplete="off" class="form-control" name="maid_name" required="required" placeholder="Staff Name *">
							<label></label>
							<span class="help-block">Staff Name *</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" name="type" required>
								<option value="">Select Type</option>
								<?php $maids=$this->dashboard_model->get_all_staff_type();
                  if(isset($maids) && $maids)
                  {
                    foreach($maids as $maid)
                    {
                    ?>
								<option value="<?php echo $maid->housekeeping_staff_type_name ?>">
									<?php echo $maid->housekeeping_staff_type_name ?>
								</option>
								<?php
								}
								}
								?>
							</select>
							<label></label>
							<span class="help-block">Select Type *</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input type="text" autocomplete="off" class="form-control" name="max_alo_unit" placeholder="Max allocation count *">
							<label></label>
							<span class="help-block">Max allocation count *</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input type="text" autocomplete="off" class="form-control" name="maid_contact" required="required" maxlength="10" onKeyPress="return onlyNos(event, this);" placeholder="Staff Phone No *">
							<label></label>
							<span class="help-block">Staff Phone No *</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input type="text" autocomplete="off" class="form-control" name="maid_sec" required="required" placeholder="Staff Section *">
							<label></label>
							<span class="help-block">Staff Section *</span> </div>
					</div>
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" name="status" required>
								<option value="" selected="selected" disabled="disabled">Select Availability</option>
								<option value="Available">Available</option>
								<option value="Not available"> Not Available</option>
							</select>
							<label></label>
							<span class="help-block">Select Availability *</span> </div>
					</div>
					<div class="col-md-12">
						<div class="form-group form-md-line-input">
							<textarea cols="" rows="" autocomplete="off" class="form-control" name="maid_address" placeholder="Staff Address *"></textarea>
							<label></label>
							<span class="help-block">Staff Address *</span> </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn submit">Submit</button>
				<button type="submit" class="btn default">Reset</button>
			</div>
			<?php form_close(); ?>
		</div>
	</div>
</div>



<script type="text/javascript">
	$( document ).ready( function () {
		$( '#chkbx_tdy' ).prop( 'checked', true );
	} );

	function check_sub() {
		document.getElementById( 'form_date' ).submit();
	}

	function soft_delete( id ) {
		swal( {
			title: "Are you sure?",
			text: "All the releted Informations and data will be deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {
			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/delete_maid?maid_id=" + id,
				data: {
					maid_id: id
				},
				success: function ( data ) {
					swal( {
							title: data.data,
							text: "",
							type: "success"
						},
						function () {
							location.reload();
						} );
				}
			} );
		} );
	}

	function show_modal() {
		document.getElementById( 'responsive' ).style.display = "block";
	}
</script>