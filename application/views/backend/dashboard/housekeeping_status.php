<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
<?php endif;?>
<div class="portlet light borderd">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-edit"></i> Housekeeping Status</div>
		<div class="actions">
			<a href="<?php echo base_url();?>dashboard/add_profit_center" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add House keeping status </a>
		</div>
	</div>
	<div class="portlet-body">
		
		<div id="table1">
			<table class="table table-striped table-bordered table-hover" id="sample_1">
				<thead>
					<tr>
						<th scope="col">Id</th>
						<th scope="col">Status Type</th>
						<th scope="col">Status Name</th>
						<th scope="col"> Primary Color</th>
						<th scope="col">Secondary Color</th>
						<!--<th scope="col">Booking Status Slug</th>-->

					</tr>
				</thead>
				<tbody>
					<?php if(isset($data) && $data){                       
                            foreach($data as $dst){
                               
                                ?>
					<tr id="row_<?php echo $dst->status_id;?>">
						<td>
							<?php echo $dst->status_id;?>
						</td>
						<td>
							<?php echo $dst->type;?>
						</td>
						<td>
							<?php echo $dst->status_name;?>
						</td>
						<input type="hidden" id="hid" value="<?php echo $dst->status_id; ?>">
						<td><input type="color" class="btn btn-color" onchange="change_Primary_color(this.value,'<?php echo $dst->status_id; ?>')" name="" value='<?php echo $dst->color_primary;?>'>
						</td>
						<td><input type="color" class="btn btn-color" onchange="change_Secondary_color(this.value,'<?php echo $dst->status_id; ?>')" name="" value='<?php echo $dst->color_secondary;?>' >
						</td>

						<!--<td><?php //echo $dst->booking_status_slug;?></td>-->

					</tr>
					<?php }} ?>

				</tbody>
			</table>
		</div>
	</div>
</div>
<script>
	function soft_delete( id ) {
		swal( {
			title: "Are you sure?",
			text: "All the releted transactions and data will be deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {





			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/delete_pc?pc_id=" + id,
				data: {},
				success: function ( data ) {
					//alert("Checked-In Successfully");
					//location.reload();
					swal( {
							title: data.data,
							text: "",
							type: "success"
						},
						function () {

							location.reload();

						} );
				}
			} );



		} );
	}



	/*
function check_sub(){
  document.getElementById('form_date').submit();
}	*/



	function change_Primary_color( code, id ) {

		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>unit_class_controller/change_Primary_color",
			data: {
				code: code,
				id: id
			},
			success: function ( data ) {
				swal( {
					title: data.data,
					text: "",
					type: "success"
				} );

			}
		} );

	}

	function change_Secondary_color( code, id ) {

		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>unit_class_controller/change_Secondary_color",
			data: {
				code: code,
				id: id
			},
			success: function ( data ) {
				swal( {
					title: data.data,
					text: "",
					type: "success"
				} );

			}
		} );

	}
</script>