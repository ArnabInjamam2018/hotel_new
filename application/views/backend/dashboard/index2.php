<script src="<?php echo base_url();?>assets/global/plugins/daypilot/js/daypilot/daypilot-all.min.js" type="text/javascript"></script>

<?php
/*if(isset($bookings_date) && $bookings){
    $av=0; $co=0; $ci=0; $ad=0; $con=0; $th=0;

    foreach($bookings as $booking){
    }
}*/

?>
<div id="home">
<input type="hidden" id="txd" value="0">
  <div class="page-toolbar">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue-madison">
          <div class="visual"> <i class="fa fa-users" style="color: #242424;"></i> </div>
          <div class="details">
            <div class="number">
              <?php $guest=0; ?>
              <?php 
			  date_default_timezone_set('Asia/Kolkata');
			//  print_r($bookings);
			  if(isset($bookings) && $bookings):
                                        $i=0;
                                    
                                        $deposit=0;
        
                                    foreach($bookings as $booking):
									 
                                    if($booking->user_id == $this->session->userdata('user_id') && $booking->booking_status_id==5):
        
        
                                            $rooms = $this->dashboard_model->get_room_number($booking->room_id);
											//print_r($rooms);exit;
                                            
                                            if(isset($rooms) && $rooms):
                                                foreach($rooms as $room):
        
        
        
                                                    if($room->hotel_id==$this->session->userdata('user_hotel')):
        
                                                        $date1=date_create($booking->cust_from_date);
                                                        $date2=date_create($booking->cust_end_date);
                                                        $current_date= new DateTime('now');
                                                        $diff=date_diff($current_date, $date1);
        
                                                        if($date1 <= $current_date && $date2 >=$current_date):
                                                        
                                                        $guest = $guest + $booking->no_of_guest;
        
                                                        ?>
              <?php $i++;?>
              <?php endif; ?>
              <?php endif; ?>
              <?php endforeach; ?>
              <?php endif; ?>
              <?php endif; ?>
              <?php endforeach; ?>
              <?php endif;?>
              <span data-counter="counterup" data-value="<?php echo $guest; ?>">0</span>
              </div>
            <div class="desc"> Guests At Presnt </div>
          </div>
          <a class="more" href="<?php echo base_url() ?>dashboard/all_reports"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red-intense">
          <div class="visual"> <i class="fa fa-inr" style="margin-left: -15px; color: #242424;"></i> </div>
          <div class="details">
            <div class="number">
              <?php $revenue=0; ?>
              <?php 
                                    $revenue=0;
                                    foreach($unique_room_bookings as $row){
										//print_r($unique_room_bookings);exit;
                                       
										if($row->t_amount!=''){
                                        
										echo 'INR <span data-counter="counterup" data-value="'.number_format(intval($row->t_amount)).'">0</span>';
                                        }
                                        else{
                                            echo 0;
                                        }
                                    }
                                    
                                        ?>
            </div>
            <div class="desc"> Today's Revenue </div>
          </div>
          <a class="more" href="<?php echo base_url() ?>dashboard/all_f_reports"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green-haze">
          <div class="visual"> <i class="fa fa-book" style="color: #242424;"></i> </div>
          <div class="details">
            <div class="number">
              <?php $i=0; ?>
              <?php if(isset($bookings) && $bookings):
                                        $i=0;
                                        $deposit=0;
                                    date_default_timezone_set('Asia/Kolkata');
        
                                        foreach($bookings as $booking):
                                    if($booking->user_id==$this->session->userdata('user_id') && $booking->booking_status_id !="7"):
        
        
                                            $rooms=$this->dashboard_model->get_room_number($booking->room_id);
                                            if(isset($rooms) && $rooms):
                                            foreach($rooms as $room):
        
        
        
                                                if($room->hotel_id==$this->session->userdata('user_hotel')):
        
                                                    //$date1=date_create($booking->cust_from_date);
                                                    $date1=date("Y-m-d",strtotime($booking->cust_from_date));
                                                    $date2=date("Y-m-d",strtotime($booking->cust_end_date));
                                                    $current_date= date("Y-m-d");
                                                    //$diff=date_diff($current_date, $date1);
        
                                                    if($date1 >$current_date):
                                                        
        
        
                                                    ?>
              <?php $i++;?>
              <?php endif; ?>
              <?php endif; ?>
              <?php endforeach; ?>
              <?php endif;?>
              <?php endif; ?>
              <?php endforeach; ?>
              <?php endif;?>               
              <span data-counter="counterup" data-value="<?php echo $i; ?>">0</span>
              </div>
            <div class="desc"> New Bookings </div>
          </div>
          <a class="more" href="<?php echo base_url() ?>dashboard/all_bookings"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple-plum">
          <div class="visual"> <i class="fa fa-building" style="color: #242424;"></i> </div>
          <div class="details">
            <div class="number">
              <?php $k=0;
			  if(isset($bookings) && $bookings):
                                        $k=0;
                                        $deposit=0;
								
                                        foreach($bookings as $booking):
        
        
                                            $rooms=$this->dashboard_model->get_room_number($booking->room_id);
                                            if(isset($rooms) && $rooms):
                                            foreach($rooms as $room):
        
        
        
                                                if($room->hotel_id==$this->session->userdata('user_hotel') && $booking->booking_status_id !='7'):
        
                                                    $date1=date_create($booking->cust_from_date);
                                                    $date2=date_create($booking->cust_end_date);
                                                    $current_date= new DateTime('now');
        
        
                                                    if($date1 <=$current_date && $date2>=$current_date):
        
        
        
        
                                                        ?>
              <?php $k++;?>
              <?php endif; ?>
              <?php endif; ?>
              <?php endforeach; ?>
              <?php endif;?>
              <?php endforeach; ?>
              <?php endif ;?>
              <?php
                                    $j=0;
                                    if(isset($all_rooms) && $all_rooms):
                                    foreach($all_rooms as $r):
        
                                    $j++;
        
                                    endforeach;
                                    endif;
        
                                    ?>
              <?php
                                    //echo $j;
                                    //echo $i;
                                    if($j!=0) {
										echo '<span data-counter="counterup" data-value="'.round((($j - $k) * 100 / $j)).'">0</span> %';
                                    }
                                    else{
        
                                        echo "100 %";
                                    }
                                    ?>
            </div>
            <div class="desc"> Room Vaccancy today </div>
          </div>
          <a class="more" href="<?php echo base_url() ?>dashboard/all_rooms"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div id="booking_calendar">
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-green"> <i class="fa fa-table font-green"></i> <span class="caption-subject bold uppercase" style="letter-spacing: 1px;"> Reservation Management</span> </div>
          </div>
          <div class="portlet-body form">
            <form role="form">
              <div class="form-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="clearfix" style="margin-bottom:35px;">
                      <div class="btn-group" data-toggle="buttons">
                        <input type="hidden" id="hidden_field1" value="">
                        <label class="btn green dark-stripe">
                          <input id="5" type="checkbox" onchange="calendar()" class="toggle">
                          Detailed View</label>
                        <label class="btn default blue-stripe" style="margin-left:10px;">
                          <input id="7" type="checkbox" onchange="calendar2()" class="toggle">
                          Detailed View 2 </label>
                        <label class="btn default yellow-stripe" style="margin-left:10px;">
                          <input id="6" type="checkbox" onchange="snapshot()" class="toggle">
                          Snapshot View </label>
                      </div>
                      <button class="btn btn-warning pull-right" id="showfilter" type="button" style="padding:0;"><span class="tooltips"   data-style="default" data-container="body" data-original-title="Booking Filter" style="padding: 7px 14px; display:block;"><i class="fa fa-filter"></i></span></button>
                    </div>
                  </div>
                  <script type="text/javascript">
                       function calendar(){
                         window.location="<?php echo base_url(); ?>dashboard/add_booking_calendar";
                       }
                       function calendar2(){
                         window.location="<?php echo base_url(); ?>dashboard/calendar_load_2";
                       }
                       function snapshot(){
                         window.location="<?php echo base_url(); ?>dashboard/snapshot_load";
                       }
					   $('#showfilter').click(function(){
						   if($('#bookfilter').css('opacity') == 1)
						   {
							$('#bookfilter').css('height','0');
							$('#bookfilter').css('opacity','0');
							$('#bookfilter').css('overflow','hidden');
							$("#scl").val('500');
						   }
						   else
						   {
							$('#bookfilter').css('height','190px');
							$('#bookfilter').css('opacity','1');
							$('#bookfilter').css('overflow','visible');
							$("#scl").val('660');
						   }
						});
                   </script>
                  <div id="bookfilter" style="height:0; opacity:0; overflow:hidden;" class="col-md-12">
                  	<div class="row">
                        <div class="col-md-6 col-sm-6">
                          <div class="portlet light bordered">
                            <div class="row">
							
							 <div class="col-md-4">
                                <div class="form-group">
                                  <label>Unit Type :</label>
                                  <?php $units=$this->dashboard_model->all_units(); ?>
                                  <select  id="filter121" class="form-control input-sm bs-select">
                                    <?php if(isset($units) && $units): ?>
                                    <option value="all">All</option>
                                    <?php foreach($units as $unit): ?>
                                    <option value="<?php echo $unit->id; ?>"><?php echo $unit->unit_name; ?></option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                  </select>
                                </div>
                              </div>
							<div class="col-md-4">
                                <div class="form-group">
                                  <label>Unit Class:</label>
                                  <select id="filter3" class="form-control input-sm bs-select">
                                    <option value="all" >All</option>
                                    <?php 
                                    $unit_class= $this->dashboard_model->get_unit_class_list();
                                    foreach($unit_class as $uc)
                                    {?>
                                    <option value="<?php echo $uc->hotel_unit_class_name;?>"><?php echo $uc->hotel_unit_class_name; ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Unit Category:</label>
                                  <?php $units=$this->dashboard_model->all_units(); ?>
                                  <select  id="filter1" class="form-control input-sm bs-select">
                                    <?php if(isset($units) && $units): ?>
                                    <option value="all">All</option>
                                    <?php foreach($units as $unit): ?>
                                    <option value="<?php echo $unit->id; ?>"><?php echo $unit->unit_type; ?></option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>No Of Beds:</label>
                                  <select id="filter4" class="form-control input-sm bs-select">
                                    <option value="all">All</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                  </select>
                                </div>
                              </div>
                             
                              
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Start date:</label>
                                  <div class="input-group">
                                    <input class="form-control input-md" id="start" type="text">
                                    <span class="input-group-btn"> <a class="btn green btn-md" onclick="picker.show(); return false;"><i class="fa fa-calendar-o"></i></a> </span> </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Time range:</label>
                                  <select id="timerange" class="form-control input-sm bs-select">
                                    <option value="week">Week</option>
                                    <option value="2weeks">2 Weeks</option>
                                    <option value="month" selected>Month</option>
                                    <option value="2months">2 Months</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                          <div class="portlet light bordered">
                            <h5 style="font-size:14px; font-weight:400;">Price Range</h5>
                            <div class="range" style="padding: 5px 0 13px;">
                              <input id="range_3" type="text" name="range_1" value=""/>
                            </div>
                            <button type="button" onclick="price_range()" Class="btn pink btn-sm">Search</button>
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-inline" style="margin-bottom:35px;">
                        <div class="form-group form-md-line-input">
                            <input id="filter_room_no" type="text" class="form-control" placeholder="Search Room"></input>
                            <label></label>
                            <span class="help-block">Search Room</span>
                        </div>
                        <div class="btn-group pull-right">
                          <label for="autocellwidth" class="auto_cl btn grey-cascade" id="auto_cl_id">
                            <input type="checkbox" id="autocellwidth" class="toggle" style="display:none;"><span id="disp">Expanded View</span></label>
                        </div>
                    </div>
                  </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function() {
                        //dp.cellWidth = 100;
                        //dp.cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                        //dp.update(); 						
                    });
                    var picker = new DayPilot.DatePicker({
                        target: 'start',
                        pattern: 'M/d/yyyy',
                        date: DayPilot.Date.today().addDays(-3),
                        onTimeRangeSelected: function(args) {
                            loadTimeline(args.start);
                            loadEvents();
                        }
                    });
					
                    $("#timerange").change(function() {
                        switch (this.value) {
                            case "week":
                                dp.days = 7;
                                break;
                            case "2weeks":
                                dp.days = 14;
                                break;
                            case "month":
                                dp.days = dp.startDate.daysInMonth();
                                break;
                            case "2months":
                                dp.days = dp.startDate.daysInMonth() + dp.startDate.addMonths(1).daysInMonth();
                                break;
                        }
                        loadTimeline(DayPilot.Date.today());
                        loadEvents();
                    });

                    $("#autocellwidth").click(function() {
                        dp.cellWidth = 100;  // reset for "Fixed" mode
                        dp.cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                        document.getElementById('auto_cl_id').style.backgroundColor = $(this).is(":checked") ? "#00CC99" : "#A5A5A5";
                        var a = $(this).is(":checked") ? "Compact View" : "Expanded View";
					
						$('#disp').text(a);
						dp.update();

                    });
                </script>
                <div style="position:relative;">
                  <div class="bok">Booking</div>
                   <div id="dp"> </div>
                </div>
				<input type="hidden" id="scl" value="500">
                <script>
                var dp = new DayPilot.Scheduler("dp");
                var clean;
				dp.dynamicEventRendering = "Disabled";
				dp.dynamicLoading = "true";
                dp.rowMaxHeight = 10000;
                dp.allowEventOverlap = false;
                dp.borderColor = "red";               
                dp.days = dp.startDate.daysInMonth();
				
                loadTimeline(DayPilot.Date.today().addDays(-3));				
				/*dp.init();
				loadResources();
				loadEvents();*/
				
                dp.eventDeleteHandling = "Update";
				
                dp.onBeforeCellRender = function(args) {
                  
                    if (args.cell.start < DayPilot.Date.today())
                    {
                        args.cell.backColor = "#F9F9F9";
                    }
                    else
                    {
                        var dayOfWeek = args.cell.start.getDayOfWeek();
                        if (dayOfWeek === 6 || dayOfWeek === 0) {
                            args.cell.backColor = "#f2ffee";
                        }
                    }

                    if (args.cell.start <= DayPilot.Date.today() && DayPilot.Date.today() < args.cell.end)
                    {
                        args.cell.backColor = "#DFF4F2";                      
                    }
                    if (args.cell.resource === "unit") {

                        args.cell.backColor = "#EEEEEE";                   

                        args.cell.allowed = false;

                    }
                };
				
				Date.prototype.addHours = function(h){
					this.setHours(this.getHours()+h);
					return this;
				}

                dp.onBeforeTimeHeaderRender = function(args) {
                    var d = new Date();
                    var dayOfWeek = args.header.start.getDayOfWeek();
                    var dayOfMonth = args.header.start.getDayOfWeek();
                    if(args.header.start <= DayPilot.Date.today() && DayPilot.Date.today() < args.header.end){
                        args.header.fontColor  = "#ffffff";
                        args.header.backColor  = "#33D0E1";
						//console.log(DayPilot.Date.today().value.toString("M/d/yyyy HH:mm"));
						//console.log(new Date().addHours(4).toString("M/d/yyyy HH:mm"));
						
                    }
                    else if (dayOfWeek === 6 || dayOfWeek === 0) {

                        args.header.backColor  = "#E2EFDA";
                    }
                    else{
                        args.header.backColor  = "#EDEDED";
                    }

                };

                dp.timeHeaders = [
                    { groupBy: "Month", format: "MMMM yyyy" },
                    { groupBy: "Day", format: "d" },
                    { groupBy: "Day", format: "ddd" },	
                ];

                dp.eventHeight = 50;
                //dp.bubble = new DayPilot.Bubble({});
				//dp.bubble.showAfter(2000);
 
				dp.bubble = new DayPilot.Bubble({
					cssOnly: true,
					onLoad: function(args) {
						args.async = true;  
						setTimeout(function() {
							args.loaded();
						}, 2000);
					}
				});
	
                dp.rowHeaderColumns = [
                    {title: "Engine", width: 100},
                ];
                    
                dp.onBeforeResHeaderRender = function(args) {
                   var beds = function(count) {
                        return count + " bed" + (count > 1 ? "s" : "");
                    };
                };

                // http://api.daypilot.org/daypilot-scheduler-oneventmoved/
                dp.onEventMoved = function (args) {
                    var modal = new DayPilot.Modal();
                    modal.closed = function() {
                        dp.clearSelection();
                        // reload all events
                        var data = this.result;
                        //if (data && data.result === "OK") {
                            loadEvents();
                        //}
                    };					
                    if ( args.newEnd < DayPilot.Date.today() ) {
                        swal({
                                title: "Past Date",
                                text: "Booking Events can not be modified in past dates",
                                type: "error",
								confirmButtonColor: "#607d8b"
                            },
                            function(){
								loadEvents();
                            });
                        return false;

                    } else {
                        modal.showUrl("<?php echo base_url();?>bookings/hotel_booking_move?id="+args.e.id()+"&newStart="+args.newStart.toString()+"&newEnd="+args.newEnd.toString()+"&newResource="+args.newResource+"");
						//alert('Samrat');
						return false;
                    }                   
                };
                
                dp.onEventResized = function (args) {
                    var modal = new DayPilot.Modal();
                    modal.closed = function() {
                        dp.clearSelection();

                        // reload all events
                        var data = this.result;
                        //if (data && data.result === "OK") {
                            loadEvents();
                        //}
                    };
                    if ( !1 ) {
                        swal({
                                title: "Past Date",
                                text: "Booking Events can not be modified in past dates",
                                type: "error",
								confirmButtonColor: "#F27474"								
                            },
                            function(){
                                $( "#booking_calendar" ).load( "<?php echo base_url() ?>dashboard/calendar_load" );
								
                            });
                        return false;
                    }
                    else
                    {
                        modal.showUrl("<?php echo base_url();?>bookings/hotel_booking_resize?id="+args.e.id()+"&newStart="+args.newStart.toString()+"&newEnd="+args.newEnd.toString()+"");
                    }
                };

                dp.onEventDeleted = function(args) {
                    $.post("<?php echo base_url();?>bookings/booking_delete",
                        {
                            id: args.e.id()
                        },
                        function() {
                            dp.message("Deleted.");
                        });
                };
				
                dp.onTimeRangeSelected = function (args) {
					
                    var modal = new DayPilot.Modal();
                    modal.closed = function() {
                        dp.clearSelection();
                        // reload all events
                        var data = this.result;
                        if (data && data.result === "OK") {
                            loadEvents();
                        }
                    };
                  
                    if ( args.start >= DayPilot.Date.today() && DayPilot.Date.today() < args.end )
                    {   //alert(args.resource);
				        
						if(args.resource == 'unit'){
							swal({
								title: "Click on an empty cell to take a booking.",
								text: "You have clicked on an unit type divider.",
								type: "info",
								showCancelButton: true,
								confirmButtonColor: "#33D0E1",
								confirmButtonText: "Take Group Booking!",
								closeOnConfirm: true
							}, function () {
								window.location = "<?php echo base_url()?>dashboard/add_group_booking";
							});	
							dp.clearSelection();	
						} else {
							$.ajax({
								type:"GET",
								url: "<?php echo base_url()?>bookings/clean_check?resource="+args.resource,
								data:{resource_id:'asd'},
								success:function(data)
								{
									if(data.say=="no") {
										if(args.start == DayPilot.Date.today() && DayPilot.Date.today() < args.end ){
											swal({
												title: "This unit is not clean!",
												text: "Do you want to take a new booking?",
												type: "info",
												showCancelButton: true,
												confirmButtonColor: "#00CC66",
												confirmButtonText: "Take Booking!",
												closeOnConfirm: true
											}, function () {
												
												modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
											});
								/*	swal({
											  title: "Are you sure?",
											  text: "",
											  type: "success",
											  showCancelButton: true,
											  confirmButtonColor: "#DD6B55",
											  confirmButtonText: "Version 1",
											  cancelButtonText: "Version 2",
											  closeOnConfirm: false,
											  closeOnCancel: false
											},
											function(isConfirm){
											  if (isConfirm) {
												 
											swal("", "Pop new booking ver1 ", "success");
												modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
											}else{
												swal("Pop new booking ver2", "Cancelled)", "success");
												modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking_ver2?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
											}
											});	*/		
											
											
										} else {
											
											modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
										}
										  dp.clearSelection();
									}else if(data.say=="yes") {
                                      /* var ranges = dp.range.all();
									  
									   $.each( ranges, function( key, value ) {
													  alert( key + ": " + value );
													});*/
										/*swal({
											  title: "Are you sure?",
											  text: "",
											  type: "success",
											  showCancelButton: true,
											  confirmButtonColor: "#DD6B55",
											  confirmButtonText: "Version 1",
											  cancelButtonText: "Version 2",
											  closeOnConfirm: false,
											  closeOnCancel: false
											},
											function(isConfirm){
											  if (isConfirm) {
												 
											swal("", "Pop new booking ver1 ", "success");
												modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
											}else{
												swal("Pop new booking ver2", "Cancelled)", "success");
												modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking_ver2?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
											}
											});		*/
									modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
									}
								}
							});
                        }
						
					}
                    else
                    {
                        swal({
                                title: "Past Date!",
                                text: "Bookings Can't be taken in past dates",
                                type: "error",
								confirmButtonColor: "#F27474"
                            },
                            function(){
								dp.clearSelection();
                            });
                        return false;
                    }
                };

                				
              
                dp.onRowClick=function(args){
                    //alert(args.resource.id);
                    var modal = new DayPilot.Modal();
                    modal.closed = function() {
                        // reload all events
                        var data = this.result;
                        if (data && data.result === "OK") {
                            loadEvents();
                        }
                    };
                    modal.showUrl("<?php echo base_url();?>bookings/pop_new_room?id=" + args.resource.id);
                }
	
                dp.onBeforeEventRender = function(args) {
                    var start = new DayPilot.Date(args.e.start);
                    var end = new DayPilot.Date(args.e.end);
                    var today = new DayPilot.Date().getDatePart();                   
                    args.e.html = args.e.cust_name;
					
                    switch (args.e.status) {
                        case "7":                           
                            args.e.barColor = args.e.bar_color_code;
                            args.e.toolTip = args.e.booking_status;
                            args.e.backColor = args.e.body_color_code;
                            // }
                            break;
                        case "1":                          
                            args.e.barColor = args.e.bar_color_code;
                            if(args.e.booking_status_secondary!='') {
                                args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
                            }
                            else {
                                args.e.toolTip = args.e.booking_status;
                            }
                            args.e.backColor = args.e.body_color_code;
                            // }
                            break;
                        case "2":                            
                            args.e.barColor = args.e.bar_color_code;
                            if(args.e.booking_status_secondary!='') {
                                args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
                            }
                            else {
                                args.e.toolTip = args.e.booking_status;
                            }
                            args.e.backColor = args.e.body_color_code;
                            // }
                            break;
                        case "3":
                            args.e.barColor = args.e.bar_color_code;
                            args.e.toolTip = args.e.booking_status;
                            args.e.backColor = args.e.body_color_code;
                            break;
                        case "5":
                            args.e.barColor = args.e.bar_color_code;
                            if(args.e.booking_status_secondary!='') {
                                args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
                            }
                            else {
                                args.e.toolTip = args.e.booking_status;
                            }
                            args.e.backColor = args.e.body_color_code;
                            break;
                        case "8":
                            args.e.barColor = args.e.bar_color_code;
                            args.e.toolTip = args.e.booking_status;
                            args.e.backColor = args.e.body_color_code;
                            break;
                        case "4":                          
                            args.e.barColor = args.e.bar_color_code;
                            args.e.toolTip = args.e.booking_status;
                            args.e.backColor = args.e.body_color_code;
                            //args.e.children ='dqwd';
                            //}
                            break;
                        case '6': // checked out
                            args.e.barColor = args.e.bar_color_code;
                            args.e.toolTip = args.e.booking_status;
                            args.e.backColor = args.e.body_color_code;
                            break;
                    }
                    args.e.html = args.e.html + "<br /><span style='color:grey'>" + args.e.toolTip + "</span>";
                    var paid = args.e.paid;
                    var paidColor = args.e.paid_color;
					
                    args.e.areas = [
                        { left: 4, bottom: 8, right: 4, height: 2, html: "<div style='background-color:" + paidColor + "; height: 100%; width:" + paid + "'></div>", v: "Visible" }
                    ];
                };
				
				
				// Start _sb				
				
				
				/*dp.links.list = [
				  {
					from: "2199", 
					to: "2203", 
					type:"FinishToStart", 
					color: "#79DDB7"
				  }
				];*/
				
				/*dp.dynamicLoading = true;
				dp.onScroll = function(args) {
					args.async = true;					
					var start = args.viewport.start;
					var end = args.viewport.end;
					
					$.post("<?php echo base_url();?>bookings/hotel_backend_events", 
					  {
						start: start.toString(),
						end: end.toString()
					  },
					  function(data) {
						args.events = data;
						args.loaded();
						//console.log(start.toString()+' \ '+end.toString());
					  }
					);	
						//console.log('onScroll');
				};*/
				
				dp.onEventClick = function(args) {
					if (args.ctrl) {
						//console.log('if (args.ctrl)');
						dp.multiselect.add(args.e);   // add to selection 
						args.preventDefault();        // cancel the default action
						//alert('Samrat');
					}
					else if (args.shift) {
						dp.multiselect.remove(args.e);   // Remove from selection 
						args.preventDefault();        // cancel the default action
						//console.log('else if (args.shift)');
					}
                };
				
				dp.eventDoubleClickHandling = "Enabled";
				dp.onEventDoubleClick = function(args) {
					//alert("Event with id " + args.e.id() + " was double-clicked");
						var modal = new DayPilot.Modal();
						modal.showUrl("<?php echo base_url();?>bookings/hotel_edit_booking?id=" + args.e.id());
						//console.log('else if(!args.ctrl && !args.shift)');
						modal.closed = function(data) {
                        if (this.result == "OK") {
                            loadEvents();
							//dp.events.update(args.e);
                        }
						//console.log(args.e.id());
						loadEvents();
						dp.multiselect.add(args.e);
						//dp.multiselect.remove(args.e);
						};
				};
				
				dp.autoRefreshInterval = 30; 
				dp.autoRefreshMaxCount = 100;
				dp.autoRefreshEnabled = false;

				dp.onAutoRefresh = function(args) { 
					/*dp.events.load(
					  "<?php echo base_url();?>bookings/hotel_backend_events",
					  function success(args) {
						dp.message("Events loaded");
					  },
					  function error(args) {
						dp.message("Loading failed.");
					  }
					);*/
					loadEvents();
					//console.log('update');
				};
				
				dp.contextMenu = new DayPilot.Menu({items: [
				  {text:"Edit", onclick: function() { 
					// POP NEW BOOKING
					//console.log(this.source.id);
						var modal = new DayPilot.Modal();
						modal.showUrl("<?php echo base_url();?>bookings/hotel_edit_booking?id=" + this.source.id());
						console.log('else if(!args.ctrl && !args.shift)');
						modal.closed = function(data) {
                        if (this.result == "OK") {
                            loadEvents();
							//dp.events.update(args.e);
                        }
						  
						//dp.multiselect.clear();	 // Remove from selection
						loadEvents();
						//dp.clearSelection();
						//dp.events.update(args.e.id);
						};
				  
				  } },
				  {text:"-"},
				 
				  //{text:"-"},
				  {text:"Select", onclick: function() { dp.multiselect.add(this.source); } },
				  {text:"Deselect", onclick: function() { dp.multiselect.remove(this.source); } },
				]});
				
				dp.eventClickHandling = "Select";
				dp.allowMultiSelect = true;
				
				//Enable free-hand rectangle selecting
				dp.multiSelectRectangle = "Free";
				
				//Real-time event handler
				dp.onRectangleEventSelecting = function(args) {
				  var msg = args.events.map(function(item) { return item.text(); }).join(" ");
				  $("#msg").html(msg);
				};
				
				//Append the events to the existing selection:
				dp.onRectangleEventSelect = function(args) {
				  args.append = true;
				};
				
				dp.crosshairType = "Full"; // Row & Column Highlighting on hover
				
				var myDate = new Date();
				var d = Math.abs(myDate.getHours() - 11);
				var dateOffset = (d*60*60*1000); 	
				
				var dte = new Date(myDate.setTime(myDate.getTime() - dateOffset));
				dp.separators = [{color:"#33D0E1", location: dte}];
				
				dp.eventMovingStartEndEnabled = true; 
				dp.eventMovingStartEndFormat = "dd MMMM, yyyy";
				
				// End _sb
				
				
				function loadEvents() {
                    var start = dp.visibleStart();
                    var end = dp.visibleEnd();
                          var st= start.toString();
                          var ed= end.toString();					
					//console.log(start+' | '+end);
					dp.events.load("<?php echo base_url();?>bookings/hotel_backend_events?st="+st+" & ed="+ed);
                }
				dp.init();
				dp.treeEnabled = true;
				//dp.rows.load("<?php echo base_url();?>bookings/hotel_backend_rooms_tree");
				//dp.events.load("<?php echo base_url();?>bookings/hotel_backend_events?st="+dp.startDate.addDays(-3)+" & ed="+dp.startDate.addDays(30));
				
				loadResources();
				loadEvents();
				
                function loadTimeline(date) {
                    dp.scale = "Manual";
                    dp.timeline = [];
                    var start = date.getDatePart().addHours(0);
                    for (var i = 0; i < dp.days; i++) {
                        dp.timeline.push({start: start.addDays(i), end: start.addDays(i+1)});
                    }					
                    dp.update();
					dp.cellWidth = 100;
                    dp.cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";					
                }                
				
                
                function loadResources() {
					dp.resources=[];
                    /*$.post("<?php echo base_url();?>bookings/hotel_backend_rooms_tree",
                        { capacity: $("#filter").val() },
                        function(data) {                                   
                            dp.resources = data;
                            dp.update();
                        });*/
					dp.rows.load("<?php echo base_url();?>bookings/hotel_backend_rooms_tree");	
                }
				
                $(document).ready(function() {
                //  $( "#booking_calendar").load( "<?php echo base_url() ?>dashboard/calendar_load" );
				
				$("#filter1").change(function() {
                        //alert("HERE");
                        $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_unitCategory",
                            { unit_id: $("#filter1").val() },
                            function(data) {
                                dp.resources = data;
                                dp.update();
									$('#filter2').prop('selectedIndex', 0);
                                    $('#filter3').prop('selectedIndex', 0);
                                    $('#filter121').prop('selectedIndex', 0);
                                    $('#filter4').prop('selectedIndex', 0);
                            });
                    });
				
                    $("#filter121").change(function() {
                        //alert("HERE");
                        $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_unit_tree",
                            { unit_id: $("#filter121").val() },
                            function(data) {
                                dp.resources = data;
                                dp.update();
                                $('#filter1').prop('selectedIndex',0);
                                $('#filter2').prop('selectedIndex',0);
                                $('#filter3').prop('selectedIndex',0);
                                $('#filter4').prop('selectedIndex',0);
                            });
                    });

                    $("#filter2").change(function() {

                        $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_unittype_tree",
                            { unit_type: $("#filter2").val() },
                            function(data) {
                                dp.resources = data;
                                dp.update();
								$('#filter1').prop('selectedIndex',0);
                                $('#filter121').prop('selectedIndex',0);
                                $('#filter3').prop('selectedIndex',0);
                                $('#filter4').prop('selectedIndex',0);
                            });
                    });
                    $("#filter3").change(function() {
                        $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_unitclass_tree",
                            { unit_class: $("#filter3").val() },
                            function(data) {
                                dp.resources = data;
                                dp.update();
								$('#filter1').prop('selectedIndex',0);
                                $('#filter2').prop('selectedIndex',0);
                                $('#filter121').prop('selectedIndex',0);
                                $('#filter4').prop('selectedIndex',0);
                            });
                    });
                    $("#filter4").change(function() {
                        $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_roombed_tree",
                            { no_bed: $("#filter4").val() },
                            function(data) {
                                dp.resources = data;
                                dp.update();
								$('#filter1').prop('selectedIndex',0);
                                $('#filter2').prop('selectedIndex',0);
                                $('#filter3').prop('selectedIndex',0);
                                $('#filter121').prop('selectedIndex',0);
                            });
                    });
                    $("#filter_room_no").keyup(function() {
                        var no=$("#filter_room_no").val();
                        if(no !="") {
                            $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_roomno_tree",
                                {room_no: $("#filter_room_no").val()},
                                function (data) {
                                    dp.resources = data;
                                    dp.update();
									$('#filter1').prop('selectedIndex',0);
                                    $('#filter2').prop('selectedIndex', 0);
                                    $('#filter3').prop('selectedIndex', 0);
                                    $('#filter121').prop('selectedIndex', 0);
                                    $('#filter4').prop('selectedIndex', 0);
                                });
                        }else{
                            //normal distribution --> start
							//alert("HERE");
                            /*$.post("<?php echo base_url();?>bookings/hotel_backend_rooms_tree",
                                { capacity: $("#filter").val() },
                                function(data) {
                                    // alert(JSON.stringify(data));
                                    /*   dp.resources = [
                                     { name: data.unit_name, id: "Tools", expanded: true, children:[
                                     { name : "Tool 1", id : "Tool1" },
                                     { name : "Tool 2", id : "Tool2" }
                                     ]
                                     },
                                     ];*/
                                    /*dp.resources = data;
                                    dp.update();
                                });*/
							dp.rows.load("<?php echo base_url();?>bookings/hotel_backend_rooms_tree");
                            //normal distribution -->end
                        }  });
                });
            </script> 
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-sm-6">
      <div class="portlet light ">
        <div class="portlet-title">
		
          <div class="caption"> <i class="icon-share font-blue-steel hide"></i> <span class="caption-subject font-blue-steel bold uppercase">Recent Bookings</span> </div>
        </div>
        <div class="portlet-body">
          <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
            <ul class="feeds">
            <?php               
			 if(isset($recent_bookings) && $recent_bookings):
			 
			 foreach($recent_bookings as $rb):
			?>
              <li>
                <div class="col1">
                  <div class="cont">
                    <div class="cont-col1">
					<?php 
					  $s_id=$rb->booking_status_id;
				     $color=$this->unit_class_model->status_color($s_id);
					
					?>
					
                      <div style="background-color:<?php echo $color->body_color_code; ?>;" class="label label-sm label-info"> <i style="color:<?php echo $color->bar_color_code; ?>;" class="fa fa-check"></i> </div>
                    </div>
                    <div class="cont-col2">
                      <div class="desc">
                   <?php 
                        $room_number=$this->dashboard_model->get_room_number($rb->room_id);
							//echo "<pre>";
							//print_r($room_number);
						echo $hotel_name->hotel_name; ?>: 
						<strong><?php echo $rb->cust_name; ?></strong> | ID: <?php echo $rb->booking_id; ?> 
                        <a href="<?php echo base_url();?>dashboard/booking_edit?b_id=<?php echo $rb->booking_id; ?>" class="btn btn-warning btn-xs"> Room No:
                        <?php 
                        if(isset($room_number)){
                        foreach($room_number as $r){ 
                        echo $r->room_no; 
		} }?>
                        <?php if($rb->group_id!=0){?><i class="fa fa-users" aria-hidden="true" style="font-size: 10px;"></i>
						<?php }?></a> 
</div> 				
                    </div>
                  </div>
                </div>
                <div class="col2">
                  <div class="date"> <?php echo $rb->stay_days ?> days
                    <?php
						$status=$this->dashboard_model->get_status($rb->booking_status_id);
						foreach($status as $stat){
						echo $stat->booking_status;
						}
					?>
                  </div>
                </div>
              </li>
              <?php endforeach;
              //endforeach;
                    endif;                                  
              ?>
            </ul>
          </div>
          <div class="scroller-footer">
            <div class="btn-arrow-link pull-right"> <a href="<?php echo base_url();?>dashboard/all_bookings">See All Bookings</a> <i class="icon-arrow-right"></i> </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-sm-6">
      <div class="portlet light tasks-widget">
        <div class="portlet-title">
          <div class="caption"> 
          <i class="icon-share font-green-haze hide"></i>
          <span class="caption-subject font-green-haze bold uppercase">Tasks</span> 
          <span class="caption-helper">tasks summary...</span> </div>
          <div class="actions">
            <div class="btn-group">
              <button class="btn green-haze btn-circle btn-sm"  data-toggle="modal" href="#responsive"> Add Task </button>
              <!-- /.modal -->
              <div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <?php
						$form = array(
							'class'             => 'form-body',
							'id'                => 'form',
							'method'            => 'post',
						);
						echo form_open_multipart('dashboard/add_task',$form);
					?>
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h4 class="modal-title">Assign a Task</h4>
                    </div>
                    <div class="modal-body">
                      <div class="scroller" style="height:200px" data-always-visible="1" data-rail-visible1="1">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group form-md-line-input">
                              <input type="text" autocomplete="off" name="title" class="form-control" id="title" placeholder="Title *" required="required">
                              <label></label><span class="help-block">Title *</span>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group form-md-line-input">
                                <select name="asign_from" id="booking_rent" class="form-control bs-select" required="required">
                                  <option value="" disabled selected>Assign From</option>
                                  <option value="<?php echo $this->session->userdata('user_id'); ?>"> <?php echo $this->session->userdata('user_name'); ?></option>
                                </select>
                                <label></label><span class="help-block">Assign From</span>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group form-md-line-input">
                                <select name="asign_to" id="booking_rent" class="form-control bs-select" required="required">
                                  <option value="" disabled selected>Assign To</option>
                                    <?php
										if(isset($tasks_assigee) && $tasks_assigee):
										foreach($tasks_assigee as $tasks_assigee):
									?>
                                  <option value="<?php echo $tasks_assigee->admin_id; ?>"> <?php echo $tasks_assigee->admin_first_name .' '. $tasks_assigee->admin_middle_name .' '. $tasks_assigee->admin_last_name; ?></option>
                                    <?php 
										endforeach; 
										endif;
									?>
                                </select>
                                <label></label><span class="help-block">Assign To</span>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group form-md-line-input">
                              <textarea name="task_desc" class="form-control" row="3" required="required" placeholder="Task Description *"></textarea>
                              <label></label><span class="help-block">Task Description *</span>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <select name="task_priority" id="task_priority" class="form-control bs-select" required="required">
                                  <option value="" disabled selected>Priority</option>
                                  <option value="1">High</option>
                                  <option value="2">Medium</option>
                                  <option value="3">Low</option>
                                </select>
                                <label></label><span class="help-block">Priority</span>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group form-md-line-input">
                                <input type="text" autocomplete="off" name="due_date" class="form-control date-picker" id="due_date" required="required">
                                <label></label><span class="help-block">Due Date</span>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" data-dismiss="modal" class="btn default">Close</button>
                      <button type="submit" class="btn green">Assign Task</button>
                    </div>
                    <?php form_close(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="portlet-body">
          <div class="task-content" id="task_manage">
            <div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="1"> 
              <!-- START TASK LIST -->
              <ul class="task-list">
                <?php
					if(isset($tasks_pending) && $tasks_pending):
					foreach($tasks_pending as $tasks_pending):

						$priority = $tasks_pending->priority;
						$from = $tasks_pending->from_id;
						$to = $tasks_pending->to_id;

						$from_details = $this->dashboard_model->get_task_user($from);
						$to_details = $this->dashboard_model->get_task_user($to);
						if ($priority == '1')                                           
                            {
                                $color = "#f3565d";
                                $priority = 'High';
                            }
                            else if ($priority == '2') 
                            {
                                $color = "#428bca";
                                $priority = 'Medium';
                            }
                            else
                            {
                                $color = "#45b6af";
                                $priority = 'Low';
                            }
				?>
                <li>
                  <div class="task-checkbox">
                    <?php if ($priority == '1') { ?>
                    <span class="label label-sm" style="background-color:<?php echo $color; ?>"><?php echo $priority; ?></span>
                    <?php } else if ($priority == '2') { ?>
                    <span class="label label-sm" style="background-color:<?php echo $color; ?>"><?php echo $priority; ?></span>
                    <?php  } else { ?>
                    <span class="label label-sm" style="background-color:<?php echo $color; ?>"><?php echo $priority; ?></span>
                    <?php  } ?>
                  </div>
                  <div class="task-title"> <span class="task-title-sp">
                  	<?php if ($priority == '1') { ?>
                  	<a href="<?php echo base_url(); ?>dashboard/task_details?task_id=<?php echo $tasks_pending->id ;?>&task_asignee=<?php echo $tasks_pending->to_id; ?>" style="color:<?php echo $color; ?>; font-weight: 500;"><?php echo  $tasks_pending->title; ?></a>  
                    <?php } else if ($priority == '2') { ?> 
                    <a href="<?php echo base_url(); ?>dashboard/task_details?task_id=<?php echo $tasks_pending->id ;?>&task_asignee=<?php echo $tasks_pending->to_id; ?>" style="color:<?php echo $color; ?>; font-weight: 500;"><?php echo  $tasks_pending->title; ?></a>  
                    <?php  } else { ?> 
                    <a href="<?php echo base_url(); ?>dashboard/task_details?task_id=<?php echo $tasks_pending->id ;?>&task_asignee=<?php echo $tasks_pending->to_id; ?>" style="color:<?php echo $color; ?>; font-weight: 500;"><?php echo  $tasks_pending->title; ?></a>    
                    <?php  } ?>            
                    <strong><?php							
					    if(isset($from_details) && $from_details){
							foreach ($from_details as $from_details){
								echo '('. $from_details->admin_first_name .' '. $from_details->admin_middle_name .' '. $from_details->admin_last_name;
							}
						}
						echo " <i class='fa fa-angle-double-right bounce'></i> ";
						if(isset($to_details) && $to_details) {
							foreach ($to_details as $to_details) {
								echo $to_details->admin_first_name .' '. $to_details->admin_middle_name .' '. $to_details->admin_last_name .')';
							}
						}
					?></strong>
                    <?php echo $tasks_pending->task; ?>
                    <i class="fa fa-exclamation-triangle" style="color:#c23f44;"></i> <span class="task-bell"><?php echo date('d/m/Y', strtotime($tasks_pending->due_date)); ?> </span> </span></div>
                  <div class="task-config">
                    <div class="task-config-btn btn-group"> 
                    <a href="<?php base_url()?>dashboard/complete_task?task_id=<?php echo $tasks_pending->id; ?>" class="btn green btn-sm" style="padding: 4px 3px 4px;"> <i class="fa fa-check"></i></a>
                    </div>
                  </div>
                </li>
                <?php 
					endforeach; 
					endif;
				?>
              </ul>
              <!-- END START TASK LIST --> 
            </div>
          </div>
          <div class="task-footer">
            <div class="btn-arrow-link pull-right"> <a href="<?php base_url()?>dashboard/all_tasks">See All Tasks</a> <i class="icon-arrow-right"></i> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-sm-6">
      <div class="portlet light tasks-widget">
        <div class="portlet-title">
          <div class="caption"> <span class="caption-subject font-blue-steel bold uppercase">Hotel Charts</span> </div>
        </div>
        <div class="portlet-body">
          <div class="task-content">
            <div class="input-group">
              <input class="form-control date-picker"  type="text" placeholder="Select Date" id="datepicker_date">
              <span class="input-group-btn">
              <button type="button" class="btn blue" onclick="pie_date()"> Go</button>
              </span> </div>
            <div id="chartdiv"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-sm-6">
      <div class="portlet light tasks-widget">
        <div class="portlet-title">
          <div class="caption"> <span class="caption-subject font-green-haze bold uppercase">Room Status Chart</span> </div>
        </div>
        <div class="portlet-body">
          <div class="task-content">
            <ul id="chart_table" class="list-group">
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="portlet light tasks-widget">
        <div class="portlet-title">
          <div class="caption"> <span class="caption-subject font-green-haze bold uppercase">Recent Booking & income Cont</span> </div>
        </div>
        <div class="portlet-body">
          <div class="task-content">
            <div id="chartdiv_one" style="width:100%; height:400px;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php 
			$color=$this->unit_class_model->booking_status_type();
			$flg=0;
			if(isset($color) && $color){
					foreach($color as $col){
						$name=$col->booking_status;
						$co=$col->bar_color_code;
						$flg++;
					?>
              <input type="hidden" id="hrow_<?php echo $flg; ?>"  value=<?php echo $co;?>>
             <?php }} ?>
<?php 
$Temporary_hold=$this->unit_class_model->status_color(1);
$advance=$this->unit_class_model->status_color(2);
$pending=$this->unit_class_model->status_color(3);
$confirmed=$this->unit_class_model->status_color(4);
$checkedin=$this->unit_class_model->status_color(5);
$checkedout=$this->unit_class_model->status_color(6);
$cancelled=$this->unit_class_model->status_color(7);
$incomplete=$this->unit_class_model->status_color(8);
$due=$this->unit_class_model->status_color(9);


 ?>
<input type="hidden" id="th" value=<?php echo $Temporary_hold->body_color_code; ?>>
<input type="hidden" id="adv" value=<?php echo $advance->body_color_code; ?>>
<input type="hidden" id="pen" value=<?php echo $pending->body_color_code; ?>>
<input type="hidden" id="conf" value=<?php echo $confirmed->body_color_code; ?>>
<input type="hidden" id="chkin" value=<?php echo $checkedin->body_color_code; ?>>
<input type="hidden" id="thkout" value=<?php echo $checkedout->body_color_code; ?>>
<input type="hidden" id="can" value=<?php echo $cancelled->body_color_code; ?>>
<input type="hidden" id="inc" value=<?php echo $incomplete->body_color_code; ?>>
<input type="hidden" id="due" value=<?php echo $due->body_color_code; ?>>


<script type="text/javascript">
var th_color=$('#th').val();
		var adv_color=$('#adv').val();
		var pen_color=$('#pen').val();
		var conf_color=$('#conf').val();
		var chkin_color=$('#chkin').val();
		var thkout_color=$('#thkout').val();
		var can_color=$('#can').val();
		var inc_color=$('#inc').val();
		var due_color=$('#due').val();
function update_task(val){
    var result = confirm("are you want to delete this task?");
    var linkurl = '<?php echo base_url() ?>dashboard/update_task/'+val;
    if (result) {
        alert(linkurl);
    }
    
}

$(function () {
	$('#datepicker2').datepicker({
		locale: 'ru'
	});
});

    var chart;
    var chart2;
    var av;
    var co;
    var ci;
    var ad;
    var con;
    var th;
    var chartData;

    function pie_date() {
        var date = document.getElementById('datepicker_date').value;
        if(date!=""){



        // PIE CHART
        chart = new AmCharts.AmPieChart();
		
		

        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>dashboard/all_bookings_date",
                datatype:'json',
                data:{date:date},
                success:function(data)
                {document.getElementById("chart_table").innerHTML='<li class="list-group-item note">Available<span class="badge badge-success badge-roundless">'+data.av+'</span></li>'+
				' <li  id="row1" class="list-group-item note" style="background-color:'+thkout_color+';">Checked Out<span class="badge badge-warning badge-roundless">'+data.cho+'</span></li>'+
				' <li  class="list-group-item note" style="background-color:'+chkin_color+';">Checked In<span class="badge badge-info badge-roundless">'+data.chi+'</span></li>'+
				' <li class="list-group-item note"style="background-color:'+adv_color+';" >Advance<span class="badge badge-danger badge-roundless">'+data.advnce+'</span></li>'+
				' <li class="list-group-item note" style="background-color:'+conf_color+';">Confirmed<span class="badge badge-success badge-roundless">'+data.con+'</span></li> '+
				'<li class="list-group-item note" style="background-color:'+th_color+';">Temporary Hold<span class="badge badge-danger badge-roundless">'+data.thld+'</span></li>';





                    //load chart data dynamically

                    //  av=data.av;

                    chartData = [
                       {
                            "country": "",
                            "visits": data.av

                        },
                        {
                            "country": "",
                            "visits": data.cho
                        },
                        {
                            "country": "",
                            "visits": data.chi
                        },
                        {
                            "country": "",
                            "visits": data.advnce
                        },
                        {
                            "country": "",
                            "visits": data.con
                        },
                        {
                            "country": "",
                            "visits": data.thld
                        },

                    ];

                    //alert(chartData);


                    // title of the chart
                    chart.dataProvider = chartData;
                    chart.titleField = "country";
                    chart.valueField = "visits";
                    chart.sequencedAnimation = true;
                    chart.startEffect = "elastic";
                    chart.innerRadius = "30%";
                    chart.startDuration = 2;
                    chart.labelRadius = 15;
                    chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                    // the following two lines makes the chart 3D
                    chart.depth3D = 10;
                    chart.angle = 15;

                    // WRITE
                    chart.write("chartdiv");

                }
            });
    }}

    $( document ).ready(function() {
		
 //alert(thkout_color);
        chart = new AmCharts.AmPieChart();
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>dashboard/all_bookings_date_today",
                datatype:'json',
                data:{date:"abc"},
                success:function(data)
                { document.getElementById("chart_table").innerHTML='<li id="row0" class="list-group-item note" >Available<span class="badge badge-success badge-roundless">'+data.av+'</span></li>'+
				'<li id="row1" class="list-group-item note"  style="background-color:'+thkout_color+';">Checked Out<span class="badge badge-warning badge-roundless">'+data.cho+'</span></li>'+
				'<li id="row3"class="list-group-item note"  style="background-color:'+chkin_color+';">Checked In<span class="badge badge-info badge-roundless">'+data.chi+'</span></li>'+
				'<li id="row4"class="list-group-item note"  style="background-color:'+adv_color+';">Advance<span class="badge badge-danger badge-roundless">'+data.advnce+'</span></li>'+
				'<li id="row5"class="list-group-item note"  style="background-color:'+conf_color+';">Confirmed<span class="badge badge-success badge-roundless">'+data.con+'</span></li>'+
				'<li id="row6" class="list-group-item note"  style="background-color:'+th_color+';">Temporary Hold<span class="badge badge-danger badge-roundless">'+data.thld+'</span></li>';
				var a=1;				
				var b=$('#hrow_'+a).val();
				//alert(b);
				document.getElementById("row"+a).style.backgroundColor = b;
                    chartData = [
                        {
                            "country": "",
                            "visits": data.av

                        },
                        {
                            "country": "",
                            "visits": data.cho
                        },
                        {
                            "country": "",
                            "visits": data.chi
                        },
                        {
                            "country": "",
                            "visits": data.advnce
                        },
                        {
                            "country": "",
                            "visits": data.con
                        },
                        {
                            "country": "",
                            "visits": data.thld
                        },

                    ];
				
                    //alert(chartData);
                    // title of the chart
                    chart.dataProvider = chartData;
                    chart.titleField = "country";
                    chart.valueField = "visits";
                    chart.sequencedAnimation = true;
                    chart.startEffect = "elastic";
                    chart.innerRadius = "30%";
                    chart.startDuration = 2;
                    chart.labelRadius = 15;
                    chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                    // the following two lines makes the chart 3D
                    chart.depth3D = 10;
                    chart.angle = 15;

                    // WRITE
                    chart.write("chartdiv");

                }
            });
    });
	
</script> 
<script>
    var chart2;
    var chartData;
    $( document ).ready(function() {
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>dashboard/ten_days_revenue_bookings",
                datatype:'json',
                data:{date:"abc"},
                success:function(data1)
                {
  
        chart2 = new AmCharts.AmSerialChart();

        chart2.dataProvider = data1; //chartData;
        chart2.categoryField = "year";
        chart2.startDuration = 1;

        chart2.handDrawn = true;
        chart2.handDrawnScatter = 3;

        // AXES
        // category
        var categoryAxis = chart2.categoryAxis;
        categoryAxis.gridPosition = "start";

        // value
        var valueAxis = new AmCharts.ValueAxis();
        valueAxis.axisAlpha = 0;
        chart2.addValueAxis(valueAxis);

        // GRAPHS
        // column graph
        var graph1 = new AmCharts.AmGraph();
        graph1.type = "column";
        graph1.title = "Income";
        graph1.lineColor = "#E27979";
        graph1.valueField = "income";
        graph1.lineAlpha = 1;
        graph1.fillAlphas = 1;
        graph1.dashLengthField = "dashLengthColumn";
        graph1.alphaField = "alpha";
        graph1.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]] Thousands</b> [[additional]]</span>";
        chart2.addGraph(graph1);

        // line
        var graph2 = new AmCharts.AmGraph();
        graph2.type = "line";
        graph2.title = "New Bookings";
        graph2.lineColor = "#18B2C5";
        graph2.valueField = "expenses";
        graph2.lineThickness = 3;
        graph2.bullet = "round";
        graph2.bulletBorderThickness = 3;
        graph2.bulletBorderColor = "#18B2C5";
        graph2.bulletBorderAlpha = 1;
        graph2.bulletColor = "#ffffff";
        graph2.dashLengthField = "dashLengthLine";
        graph2.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        chart2.addGraph(graph2);

        // LEGEND
        var legend = new AmCharts.AmLegend();
        legend.useGraphSettings = true;
        chart2.addLegend(legend);

        // WRITE
        chart2.write("chartdiv_one");
                }
            });


    },10000);
	
$(window).scroll(function(){
  var sck = $("#scl").val();	
  var sticky = $('#dp');
   var scr = $(window).scrollTop();
   //alert(sck);
   //alert(scr);
  if (scr >= sck){ 
  //alert("here");
      sticky.addClass('fixed');
  } else  {
	  sticky.removeClass('fixed'); 
  }
});
$(window).ready(function(){
	$( ".scheduler_default_main > div:nth-child(3) > div:nth-child(1)" ).addClass( "sdv");
	$( ".scheduler_default_main .sdv" ).wrap( "<div class='aa'></div>" );
});

var myEvent = window.attachEvent || window.addEventListener;
var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload';
myEvent(chkevent, function (e) {
var booking_id = document.getElementById('txd').value;
if( booking_id > 0 ){
	 $.ajax({
	  url: "<?php echo base_url()?>bookings/cancel_booking",
	  type: "POST",
	  data:{booking_id:booking_id},
	  success: function(data){
		//console.log(data);
	  }        
	  });				
}		 
 //console.log(document.getElementById('txd').value);
});
</script>  