<?php

    ini_set("display_errors", 1);

    // Reports for either E_ERROR | E_WARNING | E_NOTICE  | Any Error
    error_reporting(E_ALL);
	
	ini_set('memory_limit', '64M');

?>

<script src="<?php echo base_url();?>assets/global/plugins/daypilot/js/daypilot/daypilot-all.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/amcharts/plugins/export/export.css" type="text/css" media="all"/>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/pie.js" type="text/javascript"></script>
<div id="home">
	<input type="hidden" id="txd" value="0">

	<ul class="nav nav-tabs">
		<li class="active"> <a href="#bookingCalander" data-toggle="tab" aria-expanded="true"> <i class="fa fa-calendar" aria-hidden="true"></i> BOOKING CALENDAR </a> </li>
		<li class="" id="dash_bo"> <a href="<?php echo base_url();?>dashboard#quickInfo"> <i class="fa fa-tachometer" aria-hidden="true"></i> Quick Info </a> </li>
		<li class="" id="dash_bo"> <a href="<?php echo base_url();?>dashboard"> <i class="fa fa-tachometer" aria-hidden="true"></i> Detailed Dashboard </a> </li>
	</ul>
	<div class="tab-content" style="padding-top:15px;">
		<div class="tab-pane active" id="bookingCalander">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-7 boking-menu">
							<button class="btn pink pull-right" id="showfilter" type="button" style="padding:0;"><span class="tooltips"   data-style="default" data-container="body" data-original-title="Booking Filter" style="padding: 4px 10px; display:block;"><i class="fa fa-filter"></i></span></button>
							<div class="btn-group pull-right" style="margin:0 15px 0 0;"> <a class="btn pink btn-sm" href="javascript:;" data-toggle="dropdown" aria-expanded="true"> <i class="fa fa-desktop" aria-hidden="true"></i> Reservation Views <i class="fa fa-angle-down"></i> </a>
								<ul class="dropdown-menu">
									<li> <a href="javascript:;" onclick="calendar()"> <i class="fa fa-calendar" aria-hidden="true"></i> Calendar View </a> </li>

									<li> <a href="javascript:;" onclick="roomview()"> <i class="fa fa-th" aria-hidden="true"></i> Room View </a> </li>
								</ul>
							</div>
								<label for="autocellwidth" class="auto_cl btn grey-cascade btn-sm pull-right" id="auto_cl_id" style="margin:0 15px;">
                        <input type="checkbox" id="autocellwidth" class="toggle" style="display:none;">
                        <span id="disp">Expanded View</span></label>
							<div class="form-group pull-right" style="padding:0;">
								<input id="filter_room_no" type="text" class="form-control input-sm" placeholder="Search Room">
								</input>
							</div>
						</div>
						<script type="text/javascript">
							function take_booking() {

								//alert('booking area');

							}



							$( '#showfilter' ).click( function () {
								if ( $( '#bookfilter' ).css( 'opacity' ) == 1 ) {
									$( '#bookfilter' ).css( 'height', '0' );
									$( '#bookfilter' ).css( 'opacity', '0' );
									$( '#bookfilter' ).css( 'overflow', 'hidden' );
									$( "#scl" ).val( '280' );
								} else {
									$( '#bookfilter' ).css( 'height', '190px' );
									$( '#bookfilter' ).css( 'opacity', '1' );
									$( '#bookfilter' ).css( 'overflow', 'visible' );
									$( "#scl" ).val( '470' );
								}
							} );
						</script>
						<div id="bookfilter" style="height:0; opacity:0; overflow:hidden;" class="col-md-12">
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="portlet light bordered">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label>Unit Type :</label>
													<?php $units=$this->dashboard_model->all_units(); ?>
													<select id="filter121" class="form-control input-sm bs-select">
														<?php if(isset($units) && $units): ?>
														<option value="all">All</option>
														<?php foreach($units as $unit): ?>
														<option value="<?php echo $unit->id; ?>">
															<?php echo $unit->unit_name; ?>
														</option>
														<?php endforeach; ?>
														<?php endif; ?>
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Unit Class:</label>
													<select id="filter3" class="form-control input-sm bs-select">
														<option value="all">All</option>
														<?php 
                                    $unit_class= $this->dashboard_model->get_unit_class_list();
                                    foreach($unit_class as $uc)
                                    {?>
														<option value="<?php echo $uc->hotel_unit_class_name;?>">
															<?php echo $uc->hotel_unit_class_name; ?>
														</option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Unit Category:</label>
													<?php $units=$this->dashboard_model->all_units(); ?>
													<select id="filter1" class="form-control input-sm bs-select">
														<?php if(isset($units) && $units): ?>
														<option value="all">All</option>
														<?php foreach($units as $unit): ?>
														<option value="<?php echo $unit->id; ?>">
															<?php echo $unit->unit_type; ?>
														</option>
														<?php endforeach; ?>
														<?php endif; ?>
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>No Of Beds:</label>
													<select id="filter4" class="form-control input-sm bs-select">
														<option value="all">All</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
														<option value="10">10</option>
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Start date:</label>
													<div class="input-group">
														<input class="form-control input-md" id="start" type="text">
														<span class="input-group-btn"> <a class="btn green btn-md" onclick="picker.show(); return false;"><i class="fa fa-calendar-o"></i></a> </span> </div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Time range:</label>
													<select id="timerange" class="form-control input-sm bs-select">
														<option value="week">Week</option>
														<option value="2weeks">2 Weeks</option>
														<option value="month" selected>Month</option>
														<option value="2months">2 Months</option>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							<!--	<div class="col-md-6 col-sm-6">
									<div class="portlet light bordered">
										<h5 style="font-size:14px; font-weight:400;">Price Range</h5>
										<div class="range" style="padding: 5px 0 13px;">
											<input id="range_3" type="text" name="range_1" value=""/>
										</div>
										<button type="button" onclick="price_range()" Class="btn pink btn-sm">Search</button>
									</div>
								</div>-->
							</div>
						</div>
					</div>
					<script type="text/javascript">
						$( document ).ready( function () {
							//dp.cellWidth = 100;
							//dp.cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
							//dp.update(); 						
						} );
						var picker = new DayPilot.DatePicker( {
							target: 'start',
							pattern: 'M/d/yyyy',
							date: DayPilot.Date.today().addDays( -3 ),
							onTimeRangeSelected: function ( args ) {
								loadTimeline( args.start );


								loadEvents();

							}
						} );

						$( "#timerange" ).change( function () {
							switch ( this.value ) {
								case "week":
									dp.days = 7;
									break;
								case "2weeks":
									dp.days = 14;
									break;
								case "month":
									dp.days = dp.startDate.daysInMonth();
									break;
								case "2months":
									dp.days = dp.startDate.daysInMonth() + dp.startDate.addMonths( 1 ).daysInMonth();
									break;
							}
							loadTimeline( DayPilot.Date.today() );
							loadEvents();
						} );


						$( "#autocellwidth" ).click( function () {
							if ( window.view == 1 ) {
								//alert('display1');
								dp.cellWidth = 100; // reset for "Fixed" mode
								dp.cellWidthSpec = $( this ).is( ":checked" ) ? "Auto" : "Fixed";
								document.getElementById( 'auto_cl_id' ).style.backgroundColor = $( this ).is( ":checked" ) ? "#00CC99" : "#A5A5A5";
								var a = $( this ).is( ":checked" ) ? "Compact View" : "Expanded View";

								$( '#disp' ).text( a );
								dp.update();
							} else {
								//	alert('disp2');
								dp.cellWidth = 100; // reset for "Fixed" mode
								dp.cellWidthSpec = $( this ).is( ":checked" ) ? "Auto" : "Fixed";
								document.getElementById( 'auto_cl_id' ).style.backgroundColor = $( this ).is( ":checked" ) ? "#00CC99" : "#A5A5A5";
								var a = $( this ).is( ":checked" ) ? "Compact View" : "Expanded View";

								$( '#disp' ).text( a );
								dp.update();
								if ( a == "Compact View" ) {
									window.roomview_exp1 = 1;
									roomview_exp();

								} else {
									window.roomview_exp1 = 0;
									roomview();
								}
							}

						} );
					</script>


					<div id="b_booking">
						<div style="position:relative;">
							<div class="bok">Booking</div>
							<div id="dp"> </div>
						</div>

						<input type="hidden" id="scl" value="280">
						<script>
							var dp = new DayPilot.Scheduler( "dp" );
							var clean;
							dp.dynamicEventRendering = "Disabled";
							dp.dynamicLoading = "true";
							dp.rowMaxHeight = 10000;
							dp.allowEventOverlap = false;
							dp.borderColor = "red";
							dp.days = dp.startDate.daysInMonth();

							loadTimeline( DayPilot.Date.today().addDays( -3 ) );
							/*dp.init();
							loadResources();
							loadEvents();*/

							dp.eventDeleteHandling = "Update";

							dp.onBeforeCellRender = function ( args ) {

								if ( args.cell.start < DayPilot.Date.today() ) {
									args.cell.backColor = "#F9F9F9";
								} else {
									var dayOfWeek = args.cell.start.getDayOfWeek();
									if ( dayOfWeek === 6 || dayOfWeek === 0 ) {
										args.cell.backColor = "#f2ffee";
									}
								}

								if ( args.cell.start <= DayPilot.Date.today() && DayPilot.Date.today() < args.cell.end ) {
									args.cell.backColor = "#DFF4F2";
								}
								if ( args.cell.resource === "unit" ) {

									args.cell.backColor = "#EEEEEE";

									args.cell.allowed = false;

								}
							};

							Date.prototype.addHours = function ( h ) {
								this.setHours( this.getHours() + h );
								return this;
							}

							dp.onBeforeTimeHeaderRender = function ( args ) {
								var d = new Date();
								var dayOfWeek = args.header.start.getDayOfWeek();
								var dayOfMonth = args.header.start.getDayOfWeek();
								if ( args.header.start <= DayPilot.Date.today() && DayPilot.Date.today() < args.header.end ) {
									args.header.fontColor = "#ffffff";
									args.header.backColor = "rgba(0, 188, 212, 0.45)";
									//console.log(DayPilot.Date.today().value.toString("M/d/yyyy HH:mm"));
									//console.log(new Date().addHours(4).toString("M/d/yyyy HH:mm"));

								} else if ( dayOfWeek === 6 || dayOfWeek === 0 ) {

									args.header.backColor = "#E2EFDA";
								} else {
									args.header.backColor = "#EDEDED";
								}

							};

							dp.timeHeaders = [ {
								groupBy: "Month",
								format: "MMMM yyyy"
							}, {
								groupBy: "Day",
								format: "d"
							}, {
								groupBy: "Day",
								format: "ddd"
							}, ];

							dp.eventHeight = 50;
							//dp.bubble = new DayPilot.Bubble({});
							//dp.bubble.showAfter(2000);

							dp.bubble = new DayPilot.Bubble( {
								cssOnly: true,
								onLoad: function ( args ) {
									args.async = true;
									setTimeout( function () {
										args.loaded();
									}, 2000 );
								}
							} );

							dp.rowHeaderColumns = [ {
								title: "Engine",
								width: 100
							}, ];

							dp.onBeforeResHeaderRender = function ( args ) {
								var beds = function ( count ) {
									return count + " bed" + ( count > 1 ? "s" : "" );
								};
							};

							// http://api.daypilot.org/daypilot-scheduler-oneventmoved/
							dp.onEventMoved = function ( args ) {
								var modal = new DayPilot.Modal();
								modal.closed = function () {
									dp.clearSelection();
									// reload all events
									var data = this.result;
									//if (data && data.result === "OK") {
									loadEvents();
									//}
								};
								if ( args.newEnd < DayPilot.Date.today() ) {
									swal( {
											title: "Past Date",
											text: "Reservations can not be modified in past dates",
											type: "error",
											confirmButtonColor: "#607d8b"
										},
										function () {
											loadEvents();
										} );
									return false;

								} else {
									modal.showUrl( "<?php echo base_url();?>bookings/hotel_booking_move?id=" + args.e.id() + "&newStart=" + args.newStart.toString() + "&newEnd=" + args.newEnd.toString() + "&newResource=" + args.newResource + "" );
									//alert('Samrat');
									return false;
								}
							};

							dp.onEventResized = function ( args ) {
								var modal = new DayPilot.Modal();
								modal.closed = function () {
									dp.clearSelection();

									// reload all events
									var data = this.result;
									//if (data && data.result === "OK") {
									loadEvents();
									//}
								};
								if ( !1 ) {
									swal( {
											title: "Past Date",
											text: "Reservations can not be modified in past dates",
											type: "error",
											confirmButtonColor: "#F27474"
										},
										function () {
											$( "#booking_calendar" ).load( "<?php echo base_url() ?>dashboard/calendar_load" );

										} );
									return false;
								} else {
									modal.showUrl( "<?php echo base_url();?>bookings/hotel_booking_resize?id=" + args.e.id() + "&newStart=" + args.newStart.toString() + "&newEnd=" + args.newEnd.toString() + "" );
								}
							};

							dp.onEventDeleted = function ( args ) {
								alert( "HERE" );
								$.post( "<?php echo base_url();?>bookings/booking_delete", {
										id: args.e.id()
									},
									function () {
										dp.message( "Deleted." );
									} );
							};

							dp.onTimeRangeSelected = function ( args ) {

								var modal = new DayPilot.Modal();
								modal.closed = function () {
									dp.clearSelection();
									// reload all events
									var data = this.result;
									if ( data && data.result === "OK" ) {
										loadEvents();
									}
								};

								if ( args.start >= DayPilot.Date.today() && DayPilot.Date.today() < args.end ) { //alert(args.resource);

									if ( args.resource == 'unit' ) {
										swal( {
											title: "Click on an empty cell to take a Reservation.",
											text: "You have clicked on an unit/room type divider.",
											type: "info",
											showCancelButton: true,
											confirmButtonColor: "#33D0E1",
											confirmButtonText: "Take Group Reservation!",
											closeOnConfirm: true
										}, function () {
											window.location = "<?php echo base_url()?>dashboard/add_group_booking";
										} );
										dp.clearSelection();
									} else { // if clicked on a room
										$.ajax( {
											type: "GET",
											url: "<?php echo base_url()?>bookings/clean_check?resource=" + args.resource,
											data: {
												resource_id: 'asd'
											},
											success: function ( data ) {
												console.log(args.resource+' '+args.start+' '+args.end);
													if ( data.say == "no" ) {
														if ( args.start == DayPilot.Date.today() && DayPilot.Date.today() < args.end ) {
															swal( {
																title: "This unit is not clean!",
																text: "Do you want to take a new Reservation?",
																type: "info",
																showCancelButton: true,
																confirmButtonColor: "#00CC66",
																confirmButtonText: "Take Booking!",
																closeOnConfirm: true
															}, function () {
																modal.showUrl( "<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource );
															} );
														} else {
															modal.showUrl( "<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource );
														}
														dp.clearSelection();
													} else if ( data.say == "yes" ) {
														modal.showUrl( "<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource );
													}
											} // End ajax success
										} ); // End ajax
									}

								} else {
									swal( {
											title: "Past Date!",
											text: "Reservations Can't be taken in past dates",
											type: "error",
											confirmButtonColor: "#F27474"
										},
										function () {
											dp.clearSelection();
										} );
									return false;
								}
							};



							dp.onRowClick = function ( args ) {
								//alert(args.resource.id);
								var modal = new DayPilot.Modal();
								modal.closed = function () {
									// reload all events
									var data = this.result;
									if ( data && data.result === "OK" ) {
										loadEvents();
									}
								};
								modal.showUrl( "<?php echo base_url();?>bookings/pop_new_room?id=" + args.resource.id );
							}

							dp.onBeforeEventRender = function ( args ) {
								var start = new DayPilot.Date( args.e.start );
								var end = new DayPilot.Date( args.e.end );
								var today = new DayPilot.Date().getDatePart();
								args.e.html = args.e.cust_name;

								switch ( args.e.status ) {
									case "7":
										args.e.barColor = args.e.bar_color_code;
										args.e.toolTip = args.e.booking_status;
										args.e.backColor = args.e.body_color_code;
										// }
										break;
									case "1":
										args.e.barColor = args.e.bar_color_code;
										if ( args.e.booking_status_secondary != '' ) {
											args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
										} else {
											args.e.toolTip = args.e.booking_status;
										}
										args.e.backColor = args.e.body_color_code;
										// }
										break;
									case "2":
										args.e.barColor = args.e.bar_color_code;
										if ( args.e.booking_status_secondary != '' ) {
											args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
										} else {
											args.e.toolTip = args.e.booking_status;
										}
										args.e.backColor = args.e.body_color_code;
										// }
										break;
									case "3":
										args.e.barColor = args.e.bar_color_code;
										args.e.toolTip = args.e.booking_status;
										args.e.backColor = args.e.body_color_code;
										break;
									case "5":
										args.e.barColor = args.e.bar_color_code;
										if ( args.e.booking_status_secondary != '' ) {
											args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
										} else {
											args.e.toolTip = args.e.booking_status;
										}
										args.e.backColor = args.e.body_color_code;
										break;
									case "8":
										args.e.barColor = args.e.bar_color_code;
										args.e.toolTip = args.e.booking_status;
										args.e.backColor = args.e.body_color_code;
										break;
									case "4":
										args.e.barColor = args.e.bar_color_code;
										args.e.toolTip = args.e.booking_status;
										args.e.backColor = args.e.body_color_code;
										//args.e.children ='dqwd';
										//}
										break;
									case '6': // checked out
										args.e.barColor = args.e.bar_color_code;
										args.e.toolTip = args.e.booking_status;
										args.e.backColor = args.e.body_color_code;
										break;
								}
								args.e.html = args.e.html + "<br /><span style='color:grey'>" + args.e.toolTip + "</span>";
								var paid = args.e.paid;
								var paidColor = args.e.paid_color;

								args.e.areas = [ {
									left: 4,
									bottom: 8,
									right: 4,
									height: 2,
									html: "<div style='background-color:" + paidColor + "; height: 100%; width:" + paid + "'></div>",
									v: "Visible"
								} ];
							};


							// Start _sb				


							/*dp.links.list = [
							  {
							    from: "2199", 
							    to: "2203", 
							    type:"FinishToStart", 
							    color: "#79DDB7"
							  }
							];*/

							/*dp.dynamicLoading = true;
							dp.onScroll = function(args) {
							    args.async = true;					
							    var start = args.viewport.start;
							    var end = args.viewport.end;
							    
							    $.post("<?php echo base_url();?>bookings/hotel_backend_events", 
							      {
							        start: start.toString(),
							        end: end.toString()
							      },
							      function(data) {
							        args.events = data;
							        args.loaded();
							        //console.log(start.toString()+' \ '+end.toString());
							      }
							    );	
							        //console.log('onScroll');
							};*/

							dp.onEventClick = function ( args ) {
								if ( args.ctrl ) {
									//console.log('if (args.ctrl)');
									dp.multiselect.add( args.e ); // add to selection 
									args.preventDefault(); // cancel the default action
									//alert('Samrat');
								} else if ( args.shift ) {
									dp.multiselect.remove( args.e ); // Remove from selection 
									args.preventDefault(); // cancel the default action
									//console.log('else if (args.shift)');
								}
							};

							dp.eventDoubleClickHandling = "Enabled";
							dp.onEventDoubleClick = function ( args ) {
								//alert("Event with id " + args.e.id() + " was double-clicked");
								var modal = new DayPilot.Modal();
								modal.showUrl( "<?php echo base_url();?>bookings/hotel_edit_booking?id=" + args.e.id() );
								//console.log('else if(!args.ctrl && !args.shift)');
								modal.closed = function ( data ) {
									if ( this.result == "OK" ) {
										loadEvents();
										//dp.events.update(args.e);
									}
									//console.log(args.e.id());
									loadEvents();
									dp.multiselect.add( args.e );
									//dp.multiselect.remove(args.e);
								};
							};

							dp.autoRefreshInterval = 30;
							dp.autoRefreshMaxCount = 480;
							dp.autoRefreshEnabled = true;

							dp.onAutoRefresh = function ( args ) {
								//alert(window.view);					
								if ( window.view == 1 ) {
									loadEvents();
								} else {
									console.log( 'roomview auto call' );
									var ffg = $('#filter_room_no').val();
									if ( ffg == '' && window.roomview_exp1 == 0 ) {
										roomview();
									} else if ( ffg == '' && window.roomview_exp1 == 1 ) {
										roomview_exp();
									}
								}

							}; // _sb 29May17

							dp.contextMenu = new DayPilot.Menu( {
								items: [ {
										text: "Edit",
										onclick: function () {
											// POP Edit BOOKING
											console.log( this.source.id );
											var modal = new DayPilot.Modal();
											modal.showUrl( "<?php echo base_url();?>bookings/hotel_edit_booking?id=" + this.source.id() );
											//console.log( 'else if(!args.ctrl && !args.shift)' );
											modal.closed = function ( data ) {
												if ( this.result == "OK" ) {
													loadEvents();
													//dp.events.update(args.e);
												}

												dp.multiselect.clear();	 // Remove from selection
												loadEvents();
												dp.clearSelection();
												//dp.events.update(args.e.id);
											};

										}
									}, {
										text: "-"
									},

									//{text:"-"},
									{
										text: "Select",
										onclick: function () {
											dp.multiselect.add( this.source );
										}
									}, {
										text: "Deselect",
										onclick: function () {
											dp.multiselect.remove( this.source );
										}
									}, {
										text: "Take Another Booking",
										onclick: function () {
											//console.log( this.source.id );
											$.ajax( {
												type: "GET",
												url: "<?php echo base_url()?>bookings/getBkDet?id=" + this.source.id(),
												data: {	resource_id: 'infi' },
												success: function (dataBk) {

													let dataBkj = dataBk;
													
													let d = new Date();	
													//console.log(d);													
													let startab = (d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate()); // Sys date
													let d1 = d.setDate(d.getDate() + 1);
													d1 = new Date(d1);
													//console.log(d1);
													let endab = (d1.getFullYear() + "-" + (d1.getMonth()+1) + "-" + d1.getDate()); // Sys Date + 1
													
													let roomab = dataBkj.roomab;
													let startabC = new Date(dataBkj.startab);
													let endabC = new Date(dataBkj.endab);
													endabC = (endabC.getFullYear() + "-" + (endabC.getMonth()+1) + "-" + endabC.getDate());
													console.log(dataBkj.endab+' - '+endabC);
													
													let canTake = dataBkj.canTake;
													console.log(typeof(dataBkj));
													console.log(dataBkj);
													console.log(dataBkj.canTake + ' yes');
													console.log(startab +' '+ endabC);
													console.log(startab == endabC);


													if(dataBkj.canTake == 'yes'){ // Existing Reservation is checked out
													if(startab == endabC) {	
													$.ajax({
													type: "GET",
													url: "<?php echo base_url()?>bookings/clean_check?resource=" + roomab,
													data: {	resource_id: 'asd' },
													success: function (data) {
														var modal = new DayPilot.Modal();
															if ( data.say == "no" ) {
																	swal( {
																		title: "This unit is not clean!",
																		text: "Do you want to take a new Reservation?",
																		type: "info",
																		showCancelButton: true,
																		confirmButtonColor: "#00CC66",
																		confirmButtonText: "Take Booking!",
																		closeOnConfirm: true
																	}, 
																	function () {
																		modal.showUrl( "<?php echo base_url();?>bookings/hotel_new_booking?start=" + startab + "&end=" + endab + "&resource=" + roomab );
																	});
																dp.clearSelection();
															} else if ( data.say == "yes" ) {
																modal.showUrl( "<?php echo base_url();?>bookings/hotel_new_booking?start=" + startab + "&end=" + endab + "&resource=" + roomab );
															}
													} // End ajax success
													}); // End ajax 
													
													} else {
														toastr.error('Reservations can not be taken in past days!');
													}													
													} else {
														toastr.error('The existing reservation must be checked out first!', 'Room Occupied!');
													}
													
													
												} // End ajax success
											}); // End ajax
											
											
											
												
											
											/*$.ajax( {
												type: "GET",
												url: "<?php echo base_url()?>bookings/clean_check?resource=" + this.source.resource,
												data: {	resource_id: 'asd' },
												success: function (data) {
													console.log(args.resource+' '+args.start+' '+args.end);
														if ( data.say == "no" ) {
															if ( args.start == DayPilot.Date.today() && DayPilot.Date.today() < args.end ) {
																swal( {
																	title: "This unit is not clean!",
																	text: "Do you want to take a new Reservation?",
																	type: "info",
																	showCancelButton: true,
																	confirmButtonColor: "#00CC66",
																	confirmButtonText: "Take Booking!",
																	closeOnConfirm: true
																}, function () {
																	modal.showUrl( "<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource );
																} );
															} else {
																modal.showUrl( "<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource );
															}
															dp.clearSelection();
														} else if ( data.say == "yes" ) {
															modal.showUrl( "<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource );
														}
												} // End ajax success
											}); // End ajax  */
										}
									},
								]
							} );

							dp.eventClickHandling = "Select";
							dp.allowMultiSelect = true;

							//Enable free-hand rectangle selecting
							dp.multiSelectRectangle = "Free";

							//Real-time event handler
							dp.onRectangleEventSelecting = function ( args ) {
								var msg = args.events.map( function ( item ) {
									return item.text();
								} ).join( " " );
								$( "#msg" ).html( msg );
							};

							//Append the events to the existing selection:
							dp.onRectangleEventSelect = function ( args ) {
								args.append = true;
							};

							dp.crosshairType = "Full"; // Row & Column Highlighting on hover

							// Current Time Line
							var myDate = new Date();
							var d = Math.abs( myDate.getHours() - 16 );
							var dateOffset = ( d * 60 * 60 * 1000 );

							var dte = new Date( myDate.setTime( myDate.getTime() - dateOffset ) );
							dp.separators = [ {
								color: "#33D0E1",
								location: dte
							} ];

							// To enable the start & end date while event moving _sb
							dp.eventMovingStartEndEnabled = true;
							dp.eventMovingStartEndFormat = "dd MMMM, yyyy";

							// End _sb


							function loadEvents() {
								var start = dp.visibleStart();
								d = new Date(start);
								d.setDate(d.getDate() - 5);
								start = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate() + "T00:00:00";
								console.log(typeof(start)+' '+d);
								var end = dp.visibleEnd();
								var st = start.toString();
								var ed = end.toString();
								//console.log(start+' | '+end);
								console.log(start+' | '+end+' /loadEvents' );
								dp.events.load( "<?php echo base_url();?>bookings/hotel_backend_events?st=" + st + " & ed=" + ed );

							} // _sb 29May17





							dp.init();
							dp.treeEnabled = true;
							var a = 'default';
							loadResources( a );
							dp.treeEnabled = true;
							loadEvents();

							function loadTimeline( date ) {
								dp.scale = "Manual";
								dp.timeline = [];
								var start = date.getDatePart().addHours( 0 );
								for ( var i = 0; i < dp.days; i++ ) {
									dp.timeline.push( {
										start: start.addDays( i ),
										end: start.addDays( i + 1 )
									} );
								}
								dp.update();
								dp.cellWidth = 100;
								dp.cellWidthSpec = $( this ).is( ":checked" ) ? "Auto" : "Fixed";
							}


							function loadResources( a ) {
								dp.resources = [];
								/*$.post("<?php echo base_url();?>bookings/hotel_backend_rooms_tree",
								    { capacity: $("#filter").val() },
								    function(data) {                                   
								        dp.resources = data;
								        dp.update();
								    });*/

								//alert(a);
								if ( a == 'default' ) {
									dp.rows.load( "<?php echo base_url();?>bookings/hotel_backend_rooms_tree" );
									
								} else if ( a == 'hotel_backend_rooms_snp' ) {
									dp.rows.load( "<?php echo base_url();?>bookings/hotel_backend_rooms_snp" );
								} else if ( a == 'hotel_backend_rooms_tree_2' ) {

									dp.rows.load( "<?php echo base_url();?>bookings/hotel_backend_rooms_tree_2" );
								}
								
								console.log('loadResources -> hotel_backend_rooms_tree '+a);
								//console.log("<?php echo base_url();?>bookings/hotel_backend_rooms_tree");
									
									$.ajax({

									url: "<?php echo base_url(); ?>bookings/hotel_backend_rooms_tree",
									type: "GET",
									
									success: function ( data ) {

										console.log(data);
									},

									})
							}









							$( document ).ready( function () {
								//  $( "#booking_calendar").load( "<?php echo base_url() ?>dashboard/calendar_load" );

								$( '#generate_inv' ).hide();
								$( '#dwn_link2' ).hide();
								$( "#filter1" ).change( function () {
									//alert("HERE");
									$.post( "<?php echo base_url();?>bookings/hotel_backend_onchange_unitCategory", {
											unit_id: $( "#filter1" ).val()
										},
										function ( data ) {
											dp.resources = data;
											dp.update();
											$( '#filter2' ).prop( 'selectedIndex', 0 );
											$( '#filter3' ).prop( 'selectedIndex', 0 );
											$( '#filter121' ).prop( 'selectedIndex', 0 );
											$( '#filter4' ).prop( 'selectedIndex', 0 );
										} );
								} );

								$( "#filter121" ).change( function () {
									//alert("HERE");
									$.post( "<?php echo base_url();?>bookings/hotel_backend_onchange_unit_tree", {
											unit_id: $( "#filter121" ).val()
										},
										function ( data ) {
											dp.resources = data;
											dp.update();
											$( '#filter1' ).prop( 'selectedIndex', 0 );
											$( '#filter2' ).prop( 'selectedIndex', 0 );
											$( '#filter3' ).prop( 'selectedIndex', 0 );
											$( '#filter4' ).prop( 'selectedIndex', 0 );
										} );
								} );

								$( "#filter2" ).change( function () {

									$.post( "<?php echo base_url();?>bookings/hotel_backend_onchange_unittype_tree", {
											unit_type: $( "#filter2" ).val()
										},
										function ( data ) {
											dp.resources = data;
											dp.update();
											$( '#filter1' ).prop( 'selectedIndex', 0 );
											$( '#filter121' ).prop( 'selectedIndex', 0 );
											$( '#filter3' ).prop( 'selectedIndex', 0 );
											$( '#filter4' ).prop( 'selectedIndex', 0 );
										} );
								} );
								$( "#filter3" ).change( function () {
									$.post( "<?php echo base_url();?>bookings/hotel_backend_onchange_unitclass_tree", {
											unit_class: $( "#filter3" ).val()
										},
										function ( data ) {
											dp.resources = data;
											dp.update();
											$( '#filter1' ).prop( 'selectedIndex', 0 );
											$( '#filter2' ).prop( 'selectedIndex', 0 );
											$( '#filter121' ).prop( 'selectedIndex', 0 );
											$( '#filter4' ).prop( 'selectedIndex', 0 );
										} );
								} );
								$( "#filter4" ).change( function () {
									$.post( "<?php echo base_url();?>bookings/hotel_backend_onchange_roombed_tree", {
											no_bed: $( "#filter4" ).val()
										},
										function ( data ) {
											dp.resources = data;
											dp.update();
											$( '#filter1' ).prop( 'selectedIndex', 0 );
											$( '#filter2' ).prop( 'selectedIndex', 0 );
											$( '#filter3' ).prop( 'selectedIndex', 0 );
											$( '#filter121' ).prop( 'selectedIndex', 0 );
										} );
								} );
								$( "#filter_room_no" ).keyup( function () {
									var no = $( "#filter_room_no" ).val();

									if ( window.view == 1 ) {

										if ( no != "" ) {
											$.post( "<?php echo base_url();?>bookings/hotel_backend_onchange_roomno_tree", {
													room_no: $( "#filter_room_no" ).val()
												},
												function ( data ) {
													dp.resources = data;
													dp.update();
													$( '#filter1' ).prop( 'selectedIndex', 0 );
													$( '#filter2' ).prop( 'selectedIndex', 0 );
													$( '#filter3' ).prop( 'selectedIndex', 0 );
													$( '#filter121' ).prop( 'selectedIndex', 0 );
													$( '#filter4' ).prop( 'selectedIndex', 0 );
												} );
										} else {
											//normal distribution --> start
											//alert("HERE");
											/*$.post("<?php echo base_url();?>bookings/hotel_backend_rooms_tree",
											    { capacity: $("#filter").val() },
											    function(data) {
											        // alert(JSON.stringify(data));
											        /*   dp.resources = [
											         { name: data.unit_name, id: "Tools", expanded: true, children:[
											         { name : "Tool 1", id : "Tool1" },
											         { name : "Tool 2", id : "Tool2" }
											         ]
											         },
											         ];*/
											/*dp.resources = data;
                                    dp.update();
                                });*/
											dp.rows.load( "<?php echo base_url();?>bookings/hotel_backend_rooms_tree" );
											//normal distribution -->end
										}

									} else if ( window.view == 0 ) {
										//alert('hello');
										//alert();
										$.ajax( {
											type: "POST",
											url: "<?php echo base_url()?>unit_class_controller/roomview_dashboard",
											data: {
												id: no
											},
											success: function ( data ) {
												//$('#b_booking1').show();
												window.view = 0;
												$( '#b_booking' ).html( data );


											}
										} );



									}
								} );
							} );
						</script>
					</div>

				</div>
			</div>
		</div>

		
	</div>
</div>
<?php
$color = $this->unit_class_model->booking_status_type();
$flg = 0;
if ( isset( $color ) && $color ) {
	foreach ( $color as $col ) {
		$name = $col->booking_status;
		$co = $col->bar_color_code;
		$flg++;
		?>
		<input type="hidden" id="hrow_<?php echo $flg; ?>" value=<?php echo $co;?>>
		<?php }} ?>
		<?php 
$Temporary_hold=$this->unit_class_model->status_color(1);
$advance=$this->unit_class_model->status_color(2);
$pending=$this->unit_class_model->status_color(3);
$confirmed=$this->unit_class_model->status_color(4);
$checkedin=$this->unit_class_model->status_color(5);
$checkedout=$this->unit_class_model->status_color(6);
$cancelled=$this->unit_class_model->status_color(7);
$incomplete=$this->unit_class_model->status_color(8);
$due=$this->unit_class_model->status_color(9);

if(isset($Temporary_hold)){
 ?> 		  <input type="hidden" id="th" value="<?php echo $Temporary_hold->body_color_code; ?>"/>		  <input type="hidden" id="adv" value="<?php echo $advance->body_color_code; ?>"/>		  <input type="hidden" id="pen" value="<?php echo $pending->body_color_code; ?>"/>		  <input type="hidden" id="conf" value="<?php echo $confirmed->body_color_code; ?>"/>		  <input type="hidden" id="chkin" value="<?php echo $checkedin->body_color_code; ?>"/>		  <input type="hidden" id="thkout" value="<?php echo $checkedout->body_color_code; ?>"/>		  <input type="hidden" id="can" value="<?php echo $cancelled->body_color_code; ?>"/>		  <input type="hidden" id="inc" value="<?php echo $incomplete->body_color_code; ?>"/>		  <input type="hidden" id="due" value="<?php echo $due->body_color_code; ?>"/>		  <?php } ?>	
		<script type="text/javascript">
			var th_color = $( '#th' ).val();
			var adv_color = $( '#adv' ).val();
			var pen_color = $( '#pen' ).val();
			var conf_color = $( '#conf' ).val();
			var chkin_color = $( '#chkin' ).val();
			var thkout_color = $( '#thkout' ).val();
			var can_color = $( '#can' ).val();
			var inc_color = $( '#inc' ).val();
			var due_color = $( '#due' ).val();

			function update_task( val ) {
				var result = confirm( "are you want to delete this task?" );
				var linkurl = '<?php echo base_url() ?>dashboard/update_task/' + val;
				if ( result ) {
					//alert(linkurl);
				}

			}

			$( function () {
				$( '#datepicker2' ).datepicker( {
					locale: 'ru'
				} );
			} );

			var chart;
			var chart2;
			var av;
			var co;
			var ci;
			var ad;
			var con;
			var th;
			var chartData;

			function pie_date() {
				var date = document.getElementById( 'datepicker_date' ).value;
				if ( date != "" ) {



					// PIE CHART
					chart = new AmCharts.AmPieChart();



					jQuery.ajax( {
						type: "POST",
						url: "<?php echo base_url(); ?>dashboard/all_bookings_date",
						datatype: 'json',
						data: {
							date: date
						},
						success: function ( data ) {
							document.getElementById( "chart_table" ).innerHTML = '<li class="list-group-item note">Available<span class="badge badge-success badge-roundless">' + data.av + '</span></li>' +
								' <li  id="row1.1" class="list-group-item note" style="background-color:' + thkout_color + ';">Checked Out<span class="badge badge-warning badge-roundless">' + data.cho + '</span></li>' +
								' <li  class="list-group-item note" style="background-color:' + chkin_color + ';">Checked In<span class="badge badge-info badge-roundless">' + data.chi + '</span></li>' +
								' <li class="list-group-item note"style="background-color:' + adv_color + ';" >Advance<span class="badge badge-danger badge-roundless">' + data.advnce + '</span></li>' +
								' <li class="list-group-item note" style="background-color:' + conf_color + ';">Confirmed<span class="badge badge-success badge-roundless">' + data.con + '</span></li> ' +
								'<li class="list-group-item note" style="background-color:' + th_color + ';">Temporary Hold<span class="badge badge-danger badge-roundless">' + data.thld + '</span></li>';





							//load chart data dynamically

							//  av=data.av;

							chartData = [ {
									"country": "",
									"visits": data.av

								}, {
									"country": "",
									"visits": data.cho
								}, {
									"country": "",
									"visits": data.chi
								}, {
									"country": "",
									"visits": data.advnce
								}, {
									"country": "",
									"visits": data.con
								}, {
									"country": "",
									"visits": data.thld
								},

							];

							//alert(chartData);


							// title of the chart
							chart.dataProvider = chartData;
							chart.titleField = "country";
							chart.valueField = "visits";
							chart.sequencedAnimation = true;
							chart.startEffect = "elastic";
							chart.innerRadius = "30%";
							chart.startDuration = 2;
							chart.labelRadius = 15;
							chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
							// the following two lines makes the chart 3D
							chart.depth3D = 10;
							chart.angle = 15;

							// WRITE
							chart.write( "chartdiv" );

						}
					} );
				}
			}

			$( document ).ready( function () {
				window.roomview_exp1 = 0;
				window.view = 1;
				//alert(thkout_color);

				setTimeout( function () {

					chart = new AmCharts.AmPieChart();
					jQuery.ajax( {
						type: "POST",
						url: "<?php echo base_url(); ?>dashboard/all_bookings_date_today",
						datatype: 'json',
						data: {
							date: "abc"
						},
						success: function ( data ) {
							document.getElementById( "chart_table" ).innerHTML = '<li id="row0" class="list-group-item note" >Available<span class="badge badge-success badge-roundless">' + data.av + '</span></li>' +
								'<li id="row1.1" class="list-group-item note"  style="background-color:' + thkout_color + ';">Checked Out<span class="badge badge-warning badge-roundless">' + data.cho + '</span></li>' +
								'<li id="row3"class="list-group-item note"  style="background-color:' + chkin_color + ';">Checked In<span class="badge badge-info badge-roundless">' + data.chi + '</span></li>' +
								'<li id="row4"class="list-group-item note"  style="background-color:' + adv_color + ';">Advance<span class="badge badge-danger badge-roundless">' + data.advnce + '</span></li>' +
								'<li id="row5"class="list-group-item note"  style="background-color:' + conf_color + ';">Confirmed<span class="badge badge-success badge-roundless">' + data.con + '</span></li>' +
								'<li id="row6" class="list-group-item note"  style="background-color:' + th_color + ';">Temporary Hold<span class="badge badge-danger badge-roundless">' + data.thld + '</span></li>';
							var a = 1;
							var b = $( '#hrow_' + a ).val();
							//alert(b);
							//document.getElementById("row"+a).style.backgroundColor = b; // commented on 25/05/2017
							chartData = [ {
									"country": "",
									"visits": data.av

								}, {
									"country": "",
									"visits": data.cho
								}, {
									"country": "",
									"visits": data.chi
								}, {
									"country": "",
									"visits": data.advnce
								}, {
									"country": "",
									"visits": data.con
								}, {
									"country": "",
									"visits": data.thld
								},

							];

							//alert(chartData);
							// title of the chart
							chart.dataProvider = chartData;
							chart.titleField = "country";
							chart.valueField = "visits";
							chart.sequencedAnimation = true;
							chart.startEffect = "elastic";
							chart.innerRadius = "30%";
							chart.startDuration = 2;
							chart.labelRadius = 15;
							chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
							// the following two lines makes the chart 3D
							chart.depth3D = 10;
							chart.angle = 15;

							// WRITE
							chart.write( "chartdiv" );

						}
					} );

				}, 20000 );

			} );
		</script>
		<script>
			var chart2;
			var chartData;

			$( document ).ready( function () {

				setTimeout( function () {

					jQuery.ajax( {
						type: "POST",
						url: "<?php echo base_url(); ?>dashboard/ten_days_revenue_bookings",
						datatype: 'json',
						data: {
							date: "abc"
						},
						success: function ( data1 ) {
							//var myJsonString = JSON.stringify(data1);
							//alert(myJsonString);
							//console.log(data1);
							//alert(data1);
							chart2 = new AmCharts.AmSerialChart();

							chart2.dataProvider = data1; //chartData;
							chart2.categoryField = "year";
							chart2.startDuration = 1;

							chart2.handDrawn = true;
							chart2.handDrawnScatter = 3;

							// AXES
							// category
							var categoryAxis = chart2.categoryAxis;
							categoryAxis.gridPosition = "start";

							// value
							var valueAxis = new AmCharts.ValueAxis();
							valueAxis.axisAlpha = 0;
							chart2.addValueAxis( valueAxis );

							// GRAPHS
							// column graph
							var graph1 = new AmCharts.AmGraph();
							graph1.type = "column";
							graph1.title = "Income";
							graph1.lineColor = "#E27979";
							graph1.valueField = "income";
							graph1.lineAlpha = 1;
							graph1.fillAlphas = 1;
							graph1.dashLengthField = "dashLengthColumn";
							graph1.alphaField = "alpha";
							graph1.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]] Thousands</b> [[additional]]</span>";
							chart2.addGraph( graph1 );

							// line
							var graph2 = new AmCharts.AmGraph();
							graph2.type = "line";
							graph2.title = "New Bookings";
							graph2.lineColor = "#18B2C5";
							graph2.valueField = "expenses";
							graph2.lineThickness = 3;
							graph2.bullet = "round";
							graph2.bulletBorderThickness = 3;
							graph2.bulletBorderColor = "#18B2C5";
							graph2.bulletBorderAlpha = 1;
							graph2.bulletColor = "#ffffff";
							graph2.dashLengthField = "dashLengthLine";
							graph2.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
							chart2.addGraph( graph2 );

							// LEGEND
							var legend = new AmCharts.AmLegend();
							legend.useGraphSettings = true;
							chart2.addLegend( legend );

							// WRITE
							chart2.write( "chartdiv_one" );
						}
					} );


					$.ajax( {

						type: "POST",
						url: "<?php echo base_url(); ?>dashboard/current_day_revenue_bookings",
						//  datatype:'json',
						data: {
							t_date: "today"
						},
						success: function ( data1 ) {
							//console.log(data1);
							//var myJsonString = JSON.stringify(data1);
							//alert(myJsonString);
							$( '#portlet_tran3' ).html( data1 );
							$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
							App.initSlimScroll( ".scroller" );

						},
					} );

				}, 5000 );



				$.ajax( {

					type: "POST",
					url: "<?php echo base_url(); ?>setting/current_day_notifications",
					//  datatype:'json',
					data: {
						n_date: "today"
					},
					success: function ( data1 ) {
						//alert(data1);
						//console.log(data1);
						$( '#portlet_noti2' ).html( data1 );
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );

					},
				} );




			}, 80000 );

			$( window ).scroll( function () {
				var sck = $( "#scl" ).val();
				var sticky = $( '#dp' );
				var scr = $( window ).scrollTop();
				if ( scr >= sck ) {
					sticky.addClass( 'fixed' );
				} else {
					sticky.removeClass( 'fixed' );
				}
			} );
			$( window ).ready( function () {
				$( ".scheduler_default_main > div:nth-child(3) > div:nth-child(1)" ).addClass( "sdv" );
				$( ".scheduler_default_main .sdv" ).wrap( "<div class='aa'></div>" );
			} );

			var myEvent = window.attachEvent || window.addEventListener;
			var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload';
			myEvent( chkevent, function ( e ) {
				var booking_id = document.getElementById( 'txd' ).value;
				if ( booking_id > 0 ) {
					$.ajax( {
						url: "<?php echo base_url()?>bookings/cancel_booking",
						type: "POST",
						data: {
							booking_id: booking_id
						},
						success: function ( data ) {
							//console.log(data);
						}
					} );
				}
				//console.log(document.getElementById('txd').value);
			} );
		</script>
		<script>
			var b_stat = 11;
			var b_source = 100;
			var status_text = 'all booking';
			var b_source_text = 'Frontdesk';
			var itemType = '';
			var date_status;
			$( '#port_yes' ).on( 'click', function () {

				//	alert('port_yes');
				$( '#portlet_today' ).hide();
				$( '#portlet_tomorrow' ).hide();


				itemType = 'Yesterday';
				$.ajax( {

					url: "<?php echo base_url(); ?>setting/get_arrival",
					type: "POST",
					data: {
						b_source: b_source,
						b_stat: b_stat,
						status_text: status_text,
						date_status: itemType
					},
					success: function ( data ) {

						// alert(data);
						//console.log('port_yes' +data);
						//alert('b_source' +b_source);
						//alert('b_stat' +b_stat);
						//alert(status_text);
						$( 'span.source-type' ).html( status_text );
						if ( b_source == 100 ) {
							$( 'span.source-text' ).html( 'All Source' );
						} else {
							$( 'span.source-text' ).html( b_source_text );
						}
						//

						$( '#portlet_yesterday' ).html( data );
						$( '#portlet_yesterday' ).show();
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );
					},

				} )


				//$('#portlet_today').hide();
			} );

			$( '#port_today' ).on( 'click', function () {

				//alert('port_today');
				itemType = 'Today';
				$( '#portlet_tomorrow' ).hide();
				$( '#portlet_yesterday' ).hide();

				//	 $('div#portlet_today').remove();
				$.ajax( {

					url: "<?php echo base_url(); ?>setting/get_arrival",
					type: "POST",
					data: {
						b_source: b_source,
						b_stat: b_stat,
						status_text: status_text,
						date_status: itemType
					},
					success: function ( data ) {
						//console.log('port_today' +data);
						//alert('b_stat' +b_stat);
						//alert(status_text);
						$( 'span.source-type' ).html( status_text );
						if ( b_source == 100 ) {
							$( 'span.source-text' ).html( 'All Source' );
						} else {
							$( 'span.source-text' ).html( b_source_text );
						}
						//

						$( '#portlet_today' ).html( data );
						$( '#portlet_today' ).show();
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );
					},

				} )


				//$('#portlet_today').hide();
			} );

			$( '#port_tomrw' ).on( 'click', function () {

				//alert('port_tomrw');
				itemType = 'Tommorow';
				$( '#portlet_yesterday' ).hide();
				$( '#portlet_today' ).hide();

				$.ajax( {

					url: "<?php echo base_url(); ?>setting/get_arrival",
					type: "POST",
					data: {
						b_source: b_source,
						b_stat: b_stat,
						status_text: status_text,
						date_status: itemType
					},
					success: function ( data ) {
						//console.log('port_tomrw' +data);
						//alert('b_stat' +b_stat);
						//alert(status_text);
						$( 'span.source-type' ).html( status_text );
						if ( b_source == 100 ) {
							$( 'span.source-text' ).html( 'All Source' );
						} else {
							$( 'span.source-text' ).html( b_source_text );
						}
						//

						$( '#portlet_tomorrow' ).html( data );
						$( '#portlet_tomorrow' ).show();
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );

					},

				} )


				//$('#portlet_today').hide();port_tomrw
			} );


			$( 'ul.all-books li' ).click( function ( e ) {
				//alert($(this).find("a").text());
				itemType = $( 'ul.port-dates' ).find( 'li.active' ).data( 'itemtype' );
				//	alert('itemType' +itemType);


				var b_stat_text = $( this ).find( "a" ).text().trim();
				if ( b_stat_text == 'Arrivals/ Checkins' ) {

					b_stat = 5;
					status_text = 'arrival';
				}

				if ( b_stat_text == 'Checkedout Bookings' ) {

					b_stat = 6;
					status_text = 'checkout';
				}

				if ( b_stat_text == 'Departures/ Checkout' ) {

					b_stat = 5;
					status_text = 'Departures';

				}
				if ( b_stat_text == 'All Bookings' ) {

					b_stat = 11;
					status_text = 'all booking';
				}
				if ( b_stat_text == 'No Show Bookings' ) {

					b_stat = 3;
					status_text = 'No Show Bookings';
				}

				/* alert('b_stat' +b_stat);
				 alert('b_source' +b_source);
				 alert('b_stat' +b_stat); 
				alert('status_text' +status_text);
				*/
				$.ajax( {

					url: "<?php echo base_url(); ?>setting/get_arrival",
					type: "POST",
					data: {
						b_source: b_source,
						b_stat: b_stat,
						status_text: status_text,
						date_status: itemType
					},
					success: function ( data ) {
						console.log( data );
						//	alert(data);
						//alert(status_text);
						$( 'span.source-type' ).html( status_text );
						if ( b_source == 100 ) {
							$( 'span.source-text' ).html( 'All Source' );
						} else {
							$( 'span.source-text' ).html( b_source_text );
						}

						//
						//$('#portlet_today').html(data);
						if ( itemType == 'Today' ) {
							$( '#portlet_yesterday' ).hide();
							$( '#portlet_tomorrow' ).hide();
							$( '#portlet_today' ).html( data );

						}
						if ( itemType == 'Yesterday' ) {
							console.log( 'Yesterday' + data );
							$( '#portlet_today' ).hide();
							$( '#portlet_tomorrow' ).hide();
							$( '#portlet_yesterday' ).html( data );


						}
						if ( itemType == 'Tommorow' ) {
							$( '#portlet_today' ).hide();
							$( '#portlet_yesterday' ).hide();
							$( '#portlet_tomorrow' ).html( data );

						}
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );

					},

				} )


			} );



			$( 'ul.all-source li' ).click( function ( e ) {
				itemType = $( 'ul.port-dates' ).find( 'li.active' ).data( 'itemtype' );
				//alert(itemType);
				//alert($(this).find("a").text());
				b_source_text = $( this ).find( "a" ).text().trim();

				if ( b_source_text == 'Frontdesk' ) {

					b_source = 1;
				}

				if ( b_source_text == 'Hotel Office' ) {

					b_source = 2;
				}
				if ( b_source_text == 'Hotel Website' ) {

					b_source = 3;
				}
				if ( b_source_text == 'OTA/ GDS/ Channel' ) {

					b_source = 4;
				}
				if ( b_source_text == 'Agent/ Broker' ) {

					b_source = 5;
				}
				if ( b_source_text == 'All Sources' ) {

					b_source = 100;
				}
				// alert('b_source' +b_source);
				// alert('b_stat' +b_stat);
				// alert('status_text' +status_text);
				$.ajax( {

					url: "<?php echo base_url(); ?>setting/get_arrival",
					type: "POST",
					data: {
						b_source: b_source,
						b_stat: b_stat,
						status_text: status_text,
						date_status: itemType
					},
					success: function ( data ) {

						//	alert(data);
						$( 'span.source-type' ).html( status_text );
						$( 'span.source-text' ).html( b_source_text );

						if ( itemType == 'Today' ) {
							$( '#portlet_yesterday' ).hide();
							$( '#portlet_tomorrow' ).hide();
							$( '#portlet_today' ).html( data );

						}
						if ( itemType == 'Yesterday' ) {
							console.log( data );
							$( '#portlet_today' ).hide();
							$( '#portlet_tomorrow' ).hide();
							$( '#portlet_yesterday' ).html( data );

						}
						if ( itemType == 'Tommorow' ) {
							$( '#portlet_today' ).hide();
							$( '#portlet_yesterday' ).hide();
							$( '#portlet_tomorrow' ).html( data );

						}
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );

					},

				} )
			} );

			// start of transactions...

			var trandate = 'today';
			var t_stat = 'all';
			var t_status_text = 'all';
			var m_source = 'all_modes';
			var m_source_text = 'All Modes';
			var t_stat_text = 'All Type';
			//var tran_mode 
			$( '#yester_tran' ).on( 'click', function () {



				trandate = 'Yesterday';
				$( '#portlet_tran3' ).hide();
				$( '#portlet_tran1' ).hide();
				$.ajax( {

					url: "<?php echo base_url(); ?>setting/get_day_revenue_bookings",
					type: "POST",
					data: {
						t_stat: t_stat,
						trandate: trandate,
						tran_mode: m_source
					},
					success: function ( data ) {


						if ( trandate == 'Yesterday' ) {
							//	$('#portlet_today').hide();
							$( '#portlet_tran2' ).html( data );
							$( '#portlet_tran2' ).show();

						}

						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );

					},

				} )
			} );

			$( '#today_tran' ).on( 'click', function () {

				trandate = 'today';

				$( '#portlet_tran2' ).hide();
				$( '#portlet_tran1' ).hide();
				$.ajax( {

					url: "<?php echo base_url(); ?>setting/get_day_revenue_bookings",
					type: "POST",
					data: {
						t_stat: t_stat,
						trandate: trandate,
						tran_mode: m_source
					},
					success: function ( data ) {


						if ( trandate == 'today' ) {
							//	$('#portlet_today').hide();
							$( '#portlet_tran3' ).html( data );
							$( '#portlet_tran3' ).show();

						}

						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );

					},

				} )
			} );

			$( '#seven_days_tran' ).on( 'click', function () {

				trandate = 'seven_days';

				$( '#portlet_tran2' ).hide();
				$( '#portlet_tran3' ).hide();
				$.ajax( {

					url: "<?php echo base_url(); ?>setting/get_day_revenue_bookings",
					type: "POST",
					async: false,
					data: {
						t_stat: t_stat,
						trandate: trandate,
						tran_mode: m_source
					},
					success: function ( data ) {

						//alert(data);
						if ( trandate == 'seven_days' ) {
							//	$('#portlet_today').hide();
							$( '#portlet_tran1' ).html( data );
							$( '#portlet_tran1' ).show();

						}

						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );

					},

				} );

			} );

			$( '#tran_range' ).on( 'click', function () {

				$( '#portlet_tran1' ).hide();
				$( '#portlet_tran2' ).hide();
				$( '#portlet_tran3' ).hide();
				$( 'div#portlet_tran_range1' ).addClass( 'active' );
				$( '#portlet_tran_range1' ).show();
				$( '#portlet_tran_range2' ).hide();

			} );


			$( '#tran_buttton' ).on( 'click', function () {
				$( '#portlet_tran1' ).hide();
				$( '#portlet_tran2' ).hide();
				$( '#portlet_tran3' ).hide();
				$( '#portlet_tran_range1' ).hide();
				$( '#portlet_tran_range2' ).show();

				var end_date_tran = $( '#end_date_tran' ).val();
				var start_date_tran = $( '#start_date_tran' ).val();

				///alert('guest_date' +end_date_guest + 'start_date_guest' +start_date_guest);
				//alert('g_type' +g_type);
				//alert('g_source' +g_source);

				$.ajax( {

					type: "POST",
					url: "<?php echo base_url();?>setting/get_revenue_specific_date",
					data: {
						start_date_tran: start_date_tran,
						end_date_tran: end_date_tran,
						g_type: g_type,
						g_source: g_source
					},
					success: function ( data ) {

						//	alert(data);

						$( '#portlet_tran_range2' ).html( data );
						$( '#portlet_tran_range2' ).show();
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );
					},
				} );


			} );



			//$('ul.tran-type li').click(function(e) 
			function tran_type( t_stats, t_stat_texts ) {
				//alert($(this).find("a").text());
				//  itemType = $('ul.port-dates').find('li.active').data('itemtype');
				//alert('itemType' +itemType);

				t_stat = t_stats;
				t_stat_text = t_stat_texts;
				//var  t_stat_text = $(this).find("a").text().trim();
				//var t_stat_value = $(this).find("a").val().trim();


				$.ajax( {

					url: "<?php echo base_url(); ?>setting/get_day_revenue_bookings",
					type: "POST",
					data: {
						t_stat: t_stat,
						t_status_text: t_status_text,
						trandate: trandate,
						tran_mode: m_source
					},
					success: function ( data ) {
						//	alert(data);
						//alert(status_text);
						$( 'span.tran-type-text' ).html( t_stat_text );
						/*	if(b_source == 100){
								$('span.source-text').html('All Source');
							}else{
								$('span.source-text').html(b_source_text);
							}
						*/
						//
						//$('#portlet_today').html(data);
						if ( trandate == 'today' ) {
							$( '#portlet_tran1' ).hide();
							$( '#portlet_tran2' ).hide();
							$( '#portlet_tran3' ).html( data );


						}
						if ( trandate == 'Yesterday' ) {
							$( '#portlet_tran1' ).hide();
							$( '#portlet_tran3' ).hide();
							$( '#portlet_tran2' ).html( data );


						}
						if ( trandate == 'seven_days' ) {
							$( '#portlet_tran2' ).hide();
							$( '#portlet_tran3' ).hide();
							$( '#portlet_tran1' ).html( data );

						}
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );

					},

				} )


			}

			// all modes for transaction table...

			$( 'ul.all-modes li' ).click( function ( e ) {
				// itemType = $('ul.port-dates').find('li.active').data('itemtype');
				//alert(itemType);
				//alert($(this).find("a").text());
				m_source_text = $( this ).find( "a" ).text().trim();



				if ( m_source_text == 'All Modes' ) {

					m_source = 'all_modes';
				}

				if ( m_source_text == 'Cash' ) {

					m_source = 'cash';
				}
				if ( m_source_text == 'Cr/ Dr Card' ) {

					m_source = 'cards';
				}
				if ( m_source_text == 'Cheque/ Darft' ) {

					m_source = 'cheque';
				}
				if ( m_source_text == 'Ewallet' ) {

					m_source = 'ewallet';
				}

				// alert('b_source' +b_source);
				// alert('b_stat' +b_stat);
				// alert('status_text' +status_text);

				$.ajax( {

					url: "<?php echo base_url(); ?>setting/get_day_revenue_bookings",
					type: "POST",
					data: {
						t_stat: t_stat,
						t_status_text: t_status_text,
						trandate: trandate,
						tran_mode: m_source
					},
					success: function ( data ) {

						//	alert(data);
						$( 'span.tran-type-text' ).html( t_stat_text );
						$( 'span.tran-mode-text' ).html( m_source_text );


						if ( trandate == 'today' ) {
							$( '#portlet_tran1' ).hide();
							$( '#portlet_tran2' ).hide();
							$( '#portlet_tran3' ).html( data );

						}
						if ( trandate == 'Yesterday' ) {
							$( '#portlet_tran1' ).hide();
							$( '#portlet_tran3' ).hide();
							$( '#portlet_tran2' ).html( data );

						}
						if ( trandate == 'seven_days' ) {
							$( '#portlet_tran2' ).hide();
							$( '#portlet_tran3' ).hide();
							$( '#portlet_tran1' ).html( data );

						}
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );

					},

				} )
			} );

			var guest_date = 'today';
			var g_type = 'all_types';
			var g_source = 'all_source';
			var g_source_text = 'All source';
			var g_type_text = 'All types';

			$( '#guest_yest' ).on( 'click', function () {

				guest_date = 'yesterday';
				$( '#portlet_guest1' ).hide();
				$( '#portlet_guest3' ).hide();
				$( '#portlet_guestrange1' ).hide();
				$( '#portlet_guestrange2' ).hide();
				/* alert('guest_date' +guest_date);
	 alert('g_type' +g_type);
	alert('g_source' +g_source);*/

				$.ajax( {

					type: "POST",
					url: "<?php echo base_url();?>setting/get_guest_in_house",
					data: {
						guest_date: guest_date,
						g_type: g_type,
						g_source: g_source
					},
					success: function ( data ) {

						//	alert(data);
						$( '#portlet_guest2' ).html( data );
						$( '#portlet_guest2' ).show();
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );
					},
				} );

			} );




			$( '#guest_specific_range' ).on( 'click', function () {
				$( '#portlet_guest1' ).hide();
				$( '#portlet_guest2' ).hide();
				$( '#portlet_guest3' ).hide();
				$( 'div#portlet_guestrange1' ).addClass( 'active' );
				$( '#portlet_guestrange1' ).show();
				$( '#portlet_guestrange2' ).hide();
				/* alert('guest_date' +guest_date);
		 alert('g_type' +g_type);
		alert('g_source' +g_source);*/
			} );


			$( '#g_buttton' ).on( 'click', function () {
				$( '#portlet_guest1' ).hide();
				$( '#portlet_guest2' ).hide();
				$( '#portlet_guest3' ).hide();
				$( '#portlet_guestrange1' ).hide();
				$( '#portlet_guestrange2' ).show();

				var end_date_guest = $( '#end_date_guest' ).val();
				var start_date_guest = $( '#start_date_guest' ).val();

				///alert('guest_date' +end_date_guest + 'start_date_guest' +start_date_guest);
				//alert('g_type' +g_type);
				//alert('g_source' +g_source);

				$.ajax( {

					type: "POST",
					url: "<?php echo base_url();?>setting/get_guest_specific_date",
					data: {
						start_date_guest: start_date_guest,
						end_date_guest: end_date_guest,
						g_type: g_type,
						g_source: g_source
					},
					success: function ( data ) {

						//	alert(data);

						$( '#portlet_guestrange2' ).html( data );
						$( '#portlet_guestrange2' ).show();
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );
					},
				} );


			} );


			$( '#guest_today' ).on( 'click', function () {

				guest_date = 'today';
				/* alert('guest_date' +guest_date);
	 alert('g_type' +g_type);
	alert('g_source' +g_source);*/
				$( '#portlet_guest1' ).hide();
				$( '#portlet_guest2' ).hide();
				$( '#portlet_guestrange1' ).hide();
				$( '#portlet_guestrange2' ).hide();

				$.ajax( {

					type: "POST",
					url: "<?php echo base_url();?>setting/get_guest_in_house",
					data: {
						guest_date: guest_date,
						g_type: g_type,
						g_source: g_source
					},
					success: function ( data ) {

						//	alert(data);

						$( '#portlet_guest3' ).html( data );
						$( '#portlet_guest3' ).show();
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );
					},
				} );

			} );

			$( '#guest_seven_days' ).on( 'click', function () {

				guest_date = 'seven_days';
				/*	 alert('guest_date' +guest_date);
				 alert('g_type' +g_type);
				alert('g_source' +g_source);*/
				$( '#portlet_guest3' ).hide();
				$( '#portlet_guest2' ).hide();
				$( '#portlet_guestrange1' ).hide();
				$( '#portlet_guestrange2' ).hide();

				$.ajax( {

					type: "POST",
					url: "<?php echo base_url();?>setting/get_guest_in_house",
					data: {
						guest_date: guest_date,
						g_type: g_type,
						g_source: g_source
					},
					success: function ( data ) {

						//	alert(data);
						$( '#portlet_guest1' ).html( data );
						$( '#portlet_guest1' ).show();
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );
					},
				} );

			} );


			$( 'ul.guest-source li' ).click( function ( e ) {
				guest_date = $( 'ul.guest-dates' ).find( 'li.active' ).data( 'itemtype' );
				//alert(itemType);
				//alert($(this).find("a").text());
				g_source_text = $( this ).find( "a" ).text().trim();

				if ( g_source_text == 'Frontdesk' ) {

					g_source = 1;
				}

				if ( g_source_text == 'Hotel Office' ) {

					g_source = 2;
				}
				if ( g_source_text == 'Hotel Website' ) {

					g_source = 3;
				}
				if ( g_source_text == 'OTA/ GDS/ Channel' ) {

					g_source = 4;
				}
				if ( g_source_text == 'Agent/ Broker' ) {

					g_source = 5;
				}
				if ( g_source_text == 'All Source' ) {

					g_source = 'all_source';
				}
				//	alert('g_source' +g_source);
				//alert('guest_date' +guest_date);
				// alert('g_type' +g_type);
				$.ajax( {

					url: "<?php echo base_url(); ?>setting/get_guest_in_house",
					type: "POST",
					data: {
						guest_date: guest_date,
						g_type: g_type,
						g_source: g_source
					},
					success: function ( data ) {

						//alert(data);
						//console.log(data);
						//alert(data);
						$( 'span.guest-type-text' ).html( g_type_text );
						$( 'span.guest-source-text' ).html( g_source_text );

						if ( guest_date == 'today' ) {
							$( '#portlet_guest1' ).hide();
							$( '#portlet_guest2' ).hide();
							$( '#portlet_guest3' ).html( data );

						}
						if ( guest_date == 'yesterday' ) {
							$( '#portlet_guest1' ).hide();
							$( '#portlet_guest3' ).hide();
							$( '#portlet_guest2' ).html( data );

						}
						if ( guest_date == 'seven_days' ) {
							$( '#portlet_guest3' ).hide();
							$( '#portlet_guest2' ).hide();
							$( '#portlet_guest1' ).html( data );

						}
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );

					},

				} )
			} );



			$( 'ul.guest-list li' ).click( function ( e ) {
				guest_date = $( 'ul.guest-dates' ).find( 'li.active' ).data( 'itemtype' );

				g_type_text = $( this ).find( "a" ).text().trim();

				alert( 'g_type_text' + g_type_text );

				if ( g_type_text == 'All Types' ) {

					g_type = 'all_types';
				}

				if ( g_type_text == 'Family' ) {

					g_type = 'Family';
				}
				if ( g_type_text == 'VIP' ) {

					g_type = 'VIP';
				}
				if ( g_type_text == 'Military' ) {

					g_type = 'Military';
				}
				if ( g_type_text == 'Student' ) {

					g_type = 'Student';
				}


				//	alert('g_source' +g_source);
				// alert('guest_date' +guest_date);
				alert( 'g_type' + g_type );

				$.ajax( {

					url: "<?php echo base_url(); ?>setting/get_guest_in_house",
					type: "POST",
					data: {
						guest_date: guest_date,
						g_type: g_type,
						g_source: g_source
					},
					success: function ( data ) {

						//alert(data);
						console.log( data );
						//alert(data);
						$( 'span.guest-type-text' ).html( g_type_text );
						$( 'span.guest-source-text' ).html( g_source_text );

						if ( guest_date == 'today' ) {
							$( '#portlet_guest1' ).hide();
							$( '#portlet_guest2' ).hide();
							$( '#portlet_guest3' ).html( data );

						}
						if ( guest_date == 'yesterday' ) {
							$( '#portlet_guest1' ).hide();
							$( '#portlet_guest3' ).hide();
							$( '#portlet_guest2' ).html( data );

						}
						if ( guest_date == 'seven_days' ) {
							$( '#portlet_guest3' ).hide();
							$( '#portlet_guest2' ).hide();
							$( '#portlet_guest1' ).html( data );

						}
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );

					},

				} )

			} );


			// notification start..
			var noti_text = 'all';
			var noti_tab = '';
			$( '#yest_noti' ).on( 'click', function () {

				//noti_tab = $('ul.notice_tab').find('li.active').data('itemtype');
				noti_tab = 'all';
				//	alert('noti_tab' +noti_tab);
				$( '#portlet_noti2' ).hide();
				$.ajax( {

					type: "POST",
					url: "<?php echo base_url(); ?>setting/current_day_notifications",
					//  datatype:'json',
					data: {
						n_date: noti_tab
					},
					success: function ( data1 ) {
						//alert(data1);
						//console.log(data1);
						$( '#portlet_noti1' ).html( data1 );
						$( '#portlet_noti1' ).show();
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );

					},
				} );



			} );


			$( '#today_noti' ).on( 'click', function () {

				noti_tab = 'today';
				//	alert('noti_tab' +noti_tab);
				$( '#portlet_noti1' ).hide();
				$.ajax( {

					type: "POST",
					url: "<?php echo base_url(); ?>setting/current_day_notifications",
					//  datatype:'json',
					data: {
						n_date: noti_tab
					},
					success: function ( data1 ) {
						//alert(data1);
						//console.log(data1);
						$( '#portlet_noti2' ).html( data1 );
						$( '#portlet_noti2' ).show();
						$.getScript( '<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' );
						App.initSlimScroll( ".scroller" );

					},
				} );



			} );
		</script>
		<script>
			function roomview() {

				$.ajax( {
					type: "GET",
					url: "<?php echo base_url()?>unit_class_controller/roomview_dashboard",
					data: {
						resource_id: 'asd'
					},
					success: function ( data ) {
						//$('#b_booking1').show();
						window.view = 0;
						$( '#showfilter' ).hide();
						$( '#b_booking' ).html( data );


					}
				} );

			}

			function roomview_exp() {

				$.ajax({
					type: "GET",
					url: "<?php echo base_url()?>unit_class_controller/roomview_dashboard_exp",
					data: {
						resource_id: 'asd'
					},
					success: function ( data ) {
						//$('#b_booking1').show();
						$( '#showfilter' ).hide();
						window.view = 0;
						$( '#b_booking' ).html( data );

					}
				});
			}

			function pop_edit_bo( id ) {
				// POP EDIT BOOKING BOOKING VIEW

				var modal = new DayPilot.Modal();
				modal.showUrl( "<?php echo base_url();?>bookings/hotel_edit_booking?id=" + id );
				console.log( 'else if(!args.ctrl && !args.shift)' );
				modal.closed = function ( data ) {
					if ( this.result == "OK" ) {
						loadEvents();
						//dp.events.update(args.e);
					}

					//dp.multiselect.clear();	 // Remove from selection
					loadEvents();
					//dp.clearSelection();
					//dp.events.update(args.e.id);
				};

			}

			function pop_new_bo( resource ) {
				var a = new Date();
				b = new Date( a );

				var y = b.getFullYear();
				var d = b.getDate();
				var m = b.getMonth() + 1


				var start = y + '-' + m + '-' + d;
				//alert( start );

				a.setDate( a.getDate() + 1 );
				a = new Date( a );
				var y1 = a.getFullYear();
				var d1 = a.getDate();
				var m1 = a.getMonth() + 1

				var end = y1 + '-' + m1 + '-' + d1;
				//alert( end );
				var start = start + 'T00:00:00';
				var end = end + 'T00:00:00';
				//$resourc='158';
				//alert(resource);
				swal( {
					title: "This unit is not clean!",
					text: "Do you want to take a new booking?",
					type: "info",
					showCancelButton: true,
					confirmButtonColor: "#00CC66",
					confirmButtonText: "Take Booking!",
					closeOnConfirm: true
				}, function () {
					var modal = new DayPilot.Modal();
					modal.showUrl( "<?php echo base_url();?>bookings/hotel_new_booking?start=" + start + "&end=" + end + "&resource=" + resource );
				} );
			}

			function calendar() {
				//$('#b_booking').html();
				//  dp.init();
				// $('#b_booking1').hide();
				//$('#b_booking').show();

				$( '#showfilter' ).show();
				window.view = 1;

				var data = '<div style="position:relative;"><div class="bok">Booking</div><div id="dp"></div></div>';
				$( '#b_booking' ).html( data );
				dp.init();
				var a = 'default';
				loadResources( a );


			}
		</script>
		<script>
			var click_bk_id = '';
			var click_grp_id = '';
			$( 'body' ).on( 'click','a.click_print', function () {

				//alert('hello');
				click_bk_id = $( this ).parent().find( '.dash_booking_id' ).val();
				alert( 'click_print' + click_bk_id );
				$.ajax( {

					type: "POST",
					url: "<?php echo base_url(); ?>dashboard/generate_inv",
					data: {
						booking_id: click_bk_id
					},
					success: function ( data ) {

						alert( 'data' + data );
						if ( data == "exists" ) {

							$( '#generate_inv' ).hide();
							//  $('#dwn_link2').attr('disabled',true);
							$( '#dwn_link2' ).show();
						} else {

							$( '#generate_inv' ).show();
							//$('#dwn_link2').attr('disabled',false);
							$( '#dwn_link2' ).hide();

						}

					},


				} )
				$( "#responsive_print" ).modal();
			} );
			
			
			
			
			

			$( 'body' ).on( 'click','a.go_to_details', function () {

				click_bk_id = $( this ).parent().find( '.dash_booking_id' ).val();
				click_grp_id = $( this ).parent().find( '.dash_grp_id' ).val();
				// alert('click_grp_id' +click_grp_id);
				if ( click_grp_id == '0' ) {

					window.open( '<?php echo base_url(); ?>dashboard/booking_edit?b_id=' + click_bk_id, '_blank' );

				} else {

					// alert('click_grp_id_under_grp' +click_grp_id);
					$( "#responsive_details" ).modal();
				}
				//alert('hhh');
			} );


			function download_pdf3() {


				//alert('booking_id' +click_bk_id);
				var booking_id = click_bk_id;
				//$.post("<?php echo base_url();?>bookings/pdf_generate");
				$( "#dwn_link2" ).attr( "href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id );

				return false;
			}

			function folio_generate() {

				var booking_id = click_bk_id;
				//	alert(click_bk_id);
				$.ajax( {
					type: "POST",
					url: "<?php echo base_url(); ?>bookings/folio_generate",
					dataType: 'json',
					data: {
						booking_id: booking_id
					},
					success: function ( data ) {


						// location.reload();
						$( '#generate_inv' ).hide();
						$( '#dwn_link2' ).show();

					}
				} );
			}

			function go_to_details_single() {

				var booking_id = click_bk_id;
				//alert(click_bk_id);
				window.open( '<?php echo base_url(); ?>dashboard/booking_edit?b_id=' + click_bk_id, '_blank' );
			}

			function go_to_details_group() {

				var booking_id = click_bk_id;
				//alert(click_bk_id);

				window.open( '<?php echo base_url(); ?>dashboard/booking_edit?b_id=' + click_bk_id + '&group_id=' + click_grp_id, '_blank' );
			}
		</script>