<div class="portlet box blue">
	<div class="portlet-title">

	</div>
	<div class="portlet-body form">
		<?php   
	$form = array(
		'id'				=> 'form',
		'method'			=> 'post'
	);
	echo form_open_multipart('enquiry/getEnquiry',$form);
  ?>
		<div class="form-body">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select" name="unit_type" required>
							<option value="">Unit Type</option>
							<?php $unit_type=$this->dashboard_model->getUnitType();
				foreach($unit_type as $ut){
			?>
							<option value="<?php echo $ut->id?>~<?php echo $ut->default_occupancy?>~<?php echo $ut->max_occupancy?>">
								<?php echo $ut->unit_name ?>
							</option>
							<?php	}?>
						</select>
						<label></label>
						<span class="help-block">Unit type *</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input type="text" autocomplete="off" class="form-control date-picker" id="start_date" name="start_date" required="required" placeholder="Start Date *">
						<label></label>
						<span class="help-block">Start Date *</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input type="text" autocomplete="off" class="form-control date-picker" id="end_date" name="end_date" required="required" placeholder="End Date *">
						<label></label>
						<span class="help-block">end Date *</span> </div>
				</div>
				<div class="col-md-3">
					<div class="form-group form-md-line-input">
						<input type="text" autocomplete="off" class="form-control" id="number_of_room" name="number_of_room" required="required" placeholder="Number of Room *">
						<label></label>
						<span class="help-block">Number of Room *</span> </div>
				</div>
			</div>
		</div>
		<div class="form-actions right">
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn submit">Submit</button>
					<button type="reset" class="btn default">Reset</button>
				</div>
			</div>
		</div>

		<?php echo form_close(); ?>
	</div>
</div>