<link href="<?php echo base_url(); ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	* {
		box-sizing: border-box;
		padding: 0;
		margin: 0;
	}
	
	body {
		font-family: Verdana, Geneva, sans-serif;
	}
	
	.list-unstyled {
		list-style: none;
	}
	
	tr {
		display: table-row;
		vertical-align: inherit;
		border-color: inherit;
	}
	
	table {
		border-spacing: 0;
		border-collapse: collapse;
		background-color: transparent;
		border-color: grey;
		display: table;
		width: 100%;
		max-width: 100%;
		margin-bottom: 20px;
		font-family: Verdana, Geneva, sans-serif;
		font-size: 11px;
		line-height: 1.42857143;
		color: #555555;
	}
	
	.td-pad th,
	.td-pad td {
		padding: 2px;
	}
	.fon-big, .fon-big table, .fon-big span{
		font-size: 16px !important;
		color: #000000 !important;
		font-family: Arial, Helvetica, sans-serif !important;
	}
	.fon-big tr, .fon-big tbody{
		background: none !important;
	}
	.fon-big hr{
		background-color: #000000 !important;
	}
</style>

<?php 
	/*echo '<pre> adhoc incoice';
	print_r($Ahb_data);
	print_r($Ahb_line_data);
	exit;*/


if($printer->printer_settings=='normal'){
	$print='';
}else{
	$print='fon-big';
} ?>
<div style="padding:10px 35px;" class="<?php echo $print ?> ">

<?php 

	ini_set('memory_limit', '64M');

//print_r($line_items);

$glo=0;

$glo1=0;

$stat=0;

$chk=$this->bookings_model->get_invoice_settings();



if(isset($chk->booking_source_inv_applicable)){

    $booking_source = $chk->booking_source_inv_applicable;

} else {

	$booking_source ="";

}

if(isset($chk->nature_visit_inv_applicable)){

    $nature_visit = $chk->nature_visit_inv_applicable;

} else {

	$nature_visit ="";

}

if(isset($chk->booking_note_inv_applicable)){

    $booking_note = $chk->booking_note_inv_applicable;

} else {

	$booking_note ="";

}

if(isset($chk->company_details_inv_applicable)){

    $company_details = $chk->company_details_inv_applicable;

} else {

	$company_details ="";

}

if(isset($chk->service_tax_applicable)){

    $service_tax = $chk->service_tax_applicable;

} else {

	$service_tax =0;

}

if(isset($chk->service_charg_applicable)){

    $service_charg = $chk->service_charg_applicable;

} else {

	$service_charg = 0;

}

if(isset($chk->luxury_tax_applicable)){

    $luxury_tax = $chk->luxury_tax_applicable;

} else {

	$luxury_tax =0;

}



if(isset($chk->invoice_pref) && isset($chk->invoice_suf)){	

	$suf=$chk->invoice_suf;

    $pref=$chk->invoice_pref;

} else {

	$suf="";

    $pref="";	

}



if(isset($chk->unit_no)){

    $unit_no = $chk->unit_no;

} else {

	$unit_no = 0;

}

if(isset($chk->unit_cat)){

    $unit_cat = $chk->unit_cat;

} else {

	$unit_cat = 0;

}

if(isset($chk->invoice_setting_food_paln)){

    $fd_plan = $chk->invoice_setting_food_paln;

} else {

	$fd_plan =0;

}



if(isset($chk->invoice_setting_service)){

    $service = $chk->invoice_setting_service;

} else {

	$service =0;

}



if(isset($chk->invoice_setting_extra_charge)){

    $extra_charge = $chk->invoice_setting_extra_charge;

} else {

	$extra_charge =0;

}



if(isset($chk->invoice_setting_laundry)){

    $laundry = $chk->invoice_setting_laundry;

} else {

	$laundry =0;

}

	if ( isset( $chk->logo ) ) {

		$logo = $chk->logo;

	} else {

		$logo = 0;

	}
	
	
	if ( isset( $chk->pos_app ) ) {

		$pos_settings = $chk->pos_app;

	} else {

		$pos_settings = 0;

	}
	
	if ( isset( $chk->adjust_app ) ) {

		$adjust_app = $chk->adjust_app;

	} else {

		$adjust_app = 0;

	}
	
	
	if ( isset( $chk->disc_app ) ) {

		$disc_app = $chk->disc_app;

	} else {

		$disc_app = 0;

	}
	
	
		if ( isset( $chk->fd_app ) ) {

		$fd_app = $chk->fd_app;

	} else {

		$fd_app = 0;

	}
	
	if ( isset( $chk->pos_seg ) ) {

		$pos_seg = $chk->pos_seg;

	} else {

		$pos_seg = 0;

	}
	
		if ( isset( $chk->font_size ) ) {

		$font_sizeH = $chk->font_size;

	} else {

		$font_sizeH = 14;

	}
	
	if ( isset( $chk->hotel_color ) ) {

		$colorH = $chk->hotel_color;

	} else {

		$colorH = '#000000';

	}
	
		if ( isset( $chk->declaration ) ) {

		$declaration = $chk->declaration;

	} else {

		$declaration = '';

	}
	
		if ( isset( $chk->invoice_no ) ) {

		$invoice_no = $chk->invoice_no;

	} else {

		$invoice_no = 0;

	}
	
	if ( isset( $chk->show_bk_type ) ) {

		$show_bk_type = $chk->show_bk_type;

	} else {

		$show_bk_type = 'yes';

	}
	

foreach ($Ahb_data as $key3 ) { ?>

	<table width="100%">

		<tr>

			<?php if($logo==1) {?>
			<td align="left" valign="middle"><img src="<?php echo base_url(); ?>upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>"/>
			</td>
			<?php }?>

			<td align="right" valign="middle">
			    <strong style="font-size:<?php echo $font_sizeH; ?>px; color:<?php echo $colorH; ?>;">
					<?php echo $hotel_name->hotel_name?>
				</strong>
			</td>

		</tr>

		<tr>

			<td colspan="2" width="100%">
				<hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;">
			</td>

		</tr>

	</table>

	<table width="100%">

		<?php 

			if(isset($tax) && $tax){

			foreach($tax as $tax_details){			

		?>

		<tr>

			<td width="70%" align="left" valign="top">
				<?php
					$paid = 0;
				?>

				<span style="color:#1C4E4F; font-weight:bold; font-size:12px; line-height:20px; display:block;">TAX INVOICE (AdHoc Billing)</span>
				<?php
				
				if($invoice_no==1){
                    echo $pref.'/'.$key3->ahb_id.'-'.$key3->invoice_no.$suf."<br/>"; 
                } else {
                    echo "<strong>".$pref.'/'."</strong><span>"." - ".$key3->ahb_id.'-'.$key3->invoice_no."  ".$suf."</span>"."<br/>";
                }
            ?>
				<?php 
			
			date_default_timezone_set("Asia/Kolkata");

			echo "DATE:&nbsp;",date("g:i a \-\n l jS F Y") ?>

				<?php

					if(isset($key3->guest_id) && $key3->guest_id && $key3->guest_id != 0) {

					$guest_det = $this->dashboard_model->get_guest_details( $key3->guest_id );
					
					foreach ( $guest_det as $gst ) {
						if ( ( $key3->bill_to_com == 1 ) && ( $gst->g_type == 'Corporate' ) && ( $gst->corporate_id != NULL ) && ( $gst->corporate_id != 0 ) )
						// Check if Bill to Company is Possible
						{
						
						$corporate_details = $this->dashboard_model->fetch_c_id1( $gst->corporate_id );
				?>

				<ul class="list-unstyled">

					<li>
						<strong style="color:#6B67A1;">

							<?php

							if ( $corporate_details->legal_name )

								echo $corporate_details->legal_name;

							else

								echo $corporate_details->name;

							?>

						</strong>
					</li>

					<li>

						<?php 

                    $fg = 10;

                    if(isset($corporate_details->address) && ($corporate_details->address != '' && $corporate_details->address != ' ')){

                                            echo ($corporate_details->address);

                                            $fg = 0	;

                                            //echo ', ';

                                        }

                    if(isset($corporate_details->city) && ($corporate_details->city != '' && $corporate_details->city != ' ')){

                                            if($fg == 0){

                                                echo ', ';

                                                

                                            }

                                                

                                            echo $corporate_details->city;

                                            echo '<br>';

                                            $fg = 1;

                                        }

                    else if($fg == 0)

                                            $fg = 5;

                    if(isset($corporate_details->state) && ($corporate_details->state != '' && $corporate_details->state != ' ')){

                                            if($fg == 5)

                                                echo '<br>';

                                            echo $corporate_details->state;

                                            $fg = 2;

                                            //echo ', ';

                                        }

                    if(isset($corporate_details->pin_code) && $corporate_details->pin_code!= NULL){

                                            if($fg == 2 || $fg == 5){

                                                echo ', ';

                                            }									

                                            echo 'PIN - '.$corporate_details->pin_code;

                                            $fg = 3;

                                        }

                    if(isset($corporate_details->country) && $corporate_details->country != '' && $corporate_details->country != ' '){

                                            if($fg == 3)

                                                echo ', ';

                                            echo $corporate_details->country;

                                            /*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';

                                            $fg = 4;*/

                                        }

                    if($fg == 10){

                                            echo 'No Info';

                                        }

                ?>

					</li>

					<li>
						<?php echo '<strong>Phone: </strong>'.$corporate_details->g_contact_no; ?> </li>

					<li>
						<?php echo '<strong>Email: </strong>'.$corporate_details->corp_email; ?> </li>

					<li>
						<?php 
							
						echo '<strong>GSTIN: </strong> ';
						
						if(isset($corporate_details->gstin)){
							
							echo $corporate_details->gstin;
						} else {
							echo '<span style="color:#888; font-style: italic;">no info</span>';
						}

						?>
					</li>

					<li>&nbsp; &nbsp;</li>

					<li>

						<?php 

					echo '<strong style="font-size:15px; color:#6B67A1; text-transform: capitalize;">Guest Name: </strong>';
					
								if($gst->g_gender == 'male' || $gst->g_gender == 'Male'){
									 $sex='<label style="color:#0476F6"><i style"color:#0476F6;" class="fa fa-male" aria-hidden="true"></i></label>';
								}else if($gst->g_gender == 'female' || $gst->g_gender == 'Female'){
									$sex='<label style="color:#FF2A6A"><i style"color:#FF2A6A;" class="fa fa-female" aria-hidden="true"></i></label>';
								}else{
									$sex='';
								}
							
								if($gst->g_gender == 'Male' || $gst->g_gender == 'male')
									$salu = 'Mr. ';
								else if($gst->g_gender == 'Female' || $gst->g_gender == 'male'){
									if($gst->g_married == 'married')
										$salu = 'Mrs. ';
									else
										$salu = 'Ms. ';
								}
								else
									$salu = '';

							echo $salu.' '.$gst->g_name;

					if($gst->c_des)

						echo ' - '.$gst->c_des;

												

							?></li>

				</ul>

				<?php  } 

	 else{

	?>

				<ul class="list-unstyled">

					<li>
						<strong style="color:#6B67A1; font-size:15px; text-transform: capitalize;"> 

							<?php 

					if($gst->g_gender == 'male' || $gst->g_gender == 'Male'){
									 $sex='<label style="color:#0476F6"><i style"color:#0476F6;" class="fa fa-male" aria-hidden="true"></i></label>';
								}else if($gst->g_gender == 'female' || $gst->g_gender == 'Female'){
									$sex='<label style="color:#FF2A6A"><i style"color:#FF2A6A;" class="fa fa-female" aria-hidden="true"></i></label>';
								}else{
									$sex='';
								}
							
								if($gst->g_gender == 'Male' || $gst->g_gender == 'male')
									$salu = 'Mr. ';
								else if($gst->g_gender == 'Female' || $gst->g_gender == 'male'){
									if($gst->g_married == 'married')
										$salu = 'Mrs. ';
									else
										$salu = 'Ms. ';
								}
								else
									$salu = '';

							echo $salu.' '.$gst->g_name;		

							?>

						</strong></li>

					<li>

						<?php 

									

									// Logic for Guest Address

									$fg = 10;

									if(isset($gst->g_address) && ($gst->g_address != '' && $gst->g_address != ' ')){

										echo ($gst->g_address);

										$fg = 0	;

										//echo ', ';

									}

									if(isset($gst->g_city) && ($gst->g_city != '' && $gst->g_city != ' ')){

										if($fg == 0){

											echo ', ';
										}
										echo $gst->g_city;

										echo '<br>';

										$fg = 1;
									}

									else if($fg == 0)

										$fg = 5;

									if(isset($gst->g_state) && ($gst->g_state != '' && $gst->g_state != ' ')){

										if($fg == 5)

											echo '<br>';

										echo $gst->g_state;

										$fg = 2;

										//echo ', ';

									}

									if(isset($gst->g_pincode) && $gst->g_pincode!= NULL){

										if($fg == 2 || $fg == 5){

											echo ', ';

										}									

										echo 'PIN - '.$gst->g_pincode;

										$fg = 3;

									}

									if(isset($gst->g_country) && $gst->g_country != '' && $gst->g_country != ' '){

										if($fg == 3)

											echo ', ';

										echo $gst->g_country;

										/*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';

										$fg = 4;*/

									}

									if($fg == 10){

										echo 'No Info';

									}

								?>

					</li>

					<li><strong>Phone: </strong>

						<?php if($gst->g_contact_no) {echo $gst->g_contact_no;} else { echo '<span style="color:#AAAAAA;">No info</span>';} ?>

					</li>

					<li><strong>Email: </strong>

						<?php if($gst->g_email) {echo $gst->g_email;} else { echo '<span style="color:#AAAAAA;">No info</span>';} ?>

					</li>

					<li>
						<?php 
						
							echo '<strong>GSTIN: </strong> ';
							
							if(isset($gst->gstin)){
								echo $gst->gstin;
							} else {
								echo '<span style="color:#888; font-style: italic;">not applicable/ no info</span>';
							}			
						
						?>
					</li>

				</ul>

				<?php
					}
				}
					
				} else { ?>
					
						</br>
						<strong style="color:#6B67A1; font-size:15px; text-transform: capitalize;"> 

							<?php 
							
								echo $key3->guest_name;		

							?>

						</strong>
					</li>
				<?php
					}
				?>
				</br>
				<ul class="list-unstyled">

					<li><strong>Bill Date: </strong>
						<?php echo $key3->bill_date; ?> | <strong>Consumption Date: </strong> <?php echo $key3->cons_date; ?> | <strong>Biller: </strong> <?php echo ''; ?>
					</li>

					<li><strong>Service Type: </strong>
						<?php echo $key3->service_type; ?> | <strong>Guest Type: </strong> <?php echo $key3->guest_type; ?> | <strong>Status: </strong> <?php echo $key3->status; ?> 
					</li>
					<?php if($key3->bill_note != null || $key3->bill_note != '') { ?>
					<li><strong>Bill Note: </strong>
						<?php echo $key3->bill_note; ?>
					</li>
					<?php } ?>
				</ul>
			</td>

			<td width="30%" align="left" valign="top">
				
				<?php if($company_details == '1'){ ?> GST Reg No.:

				<?php  echo $tax_details->service_tax_no;?>

				<br/> CIN No:

				<?php  echo $tax_details->cin_no; } else { ?>

				<?php } ?>
				<div class="well">
					<strong>
						<?php echo  $hotel_name->hotel_name; ?>
					</strong><br/>

					<?php echo  $hotel_contact['hotel_street1']; ?>
					<?php echo  $hotel_contact['hotel_street2'].', '; ?><br>

					<?php echo  $hotel_contact['hotel_district']; ?> -
					<?php echo  $hotel_contact['hotel_pincode']; ?>
					<?php echo  $hotel_contact['hotel_state']; ?> -
					<?php echo  $hotel_contact['hotel_country']; ?><br/>

					<strong>Contact No:</strong>
					<?php echo  '+91 '.$hotel_contact['land_phone_no'].'/ '.$hotel_contact['hotel_frontdesk_mobile']; ?><br/>

					<strong>Email:</strong>
					<?php echo  $hotel_contact['hotel_frontdesk_email']; ?><br/>
					
					<strong>Website:</strong>
					<?php echo  $hotel_contact['website']; ?><br/>

				</div>
			</td>

		</tr>

		<?php }}?>

	</table>

	<?php } ?>

	<span style="font-size:13px; font-weight:700; color:#1C4E4F; display:block; padding:5px 5px;">Product/ Service Item Details</span>

	<hr style="background: #1C4E4F; border: none; height: 1px;">

	<table class="td-pad" width="100%">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th width="4%" align="center" valign="middle"> # </th>
				<th width="20%" align="left" valign="middle"> Product/ Service Name </th>
				<th width="8%" align="center" valign="middle"> HSN/ SAC </th>
				<th align="center" valign="middle"> Qty </th>
				<th align="center" valign="middle"> Rate </th>
				<th align="center" valign="middle"> Total </th>
				<th align="center" valign="middle"> SGST </th>
				<th align="center" valign="middle"> CGST </th>
				<th align="center" valign="middle"> IGST </th>
				<th align="center" valign="middle"> Srv Chrg </th>
				<th align="center" valign="middle"> Disc </th>
				<th align="center" valign="middle"> Adj </th>
				<th width="7% align="right" valign="middle"> Sum Total </th>

			</tr>

		</thead>

		<tbody style="background: #FAFAFA">

			<?php 

	$group=$this->dashboard_model->get_bookingLineItem_group_id($_GET['group_id']); // Geting The Unit Type

		//	echo '<pre>';

		//	print_r($details); //exit;

		$count_r = 0;
		$totalAm = 0;
		$totQty = 0;
		$totRate = 0;
		$totTot = 0;
		$totSgst = 0;
		$totCgst = 0;
		$totIgst = 0;
		$totSchrg = 0;
		$totDisc = 0;
		$totAdj = 0;
		$total=0; 

	if($Ahb_line_data){ 
	
	foreach ($Ahb_line_data as $index => $key ) { 
	
		
	?>



			<tr>

				<td align="center" valign="middle">
					<?php echo $key->ahl_id; ?>
				</td>

				<td align="left" valign="middle">

				<?php // Room Details

					echo $key->name;
				?>

				</td>
				
				<td align="center" valign="middle">

					<?php 

						echo $key->hsn_sac;

					?>

				</td>
				
				<!--Room No-->

				<td align="center" valign="middle">

					<?php 

						echo $key->qty;
						$totQty = $totQty + $key->qty;
					?>

				</td>
				
				

				<td align="center" valign="middle">

					<?php 

						echo $key->rate;
						$totRate = $totRate + $key->rate;

					?>

				</td>

				<td align="center" valign="middle">

					<?php 

						echo $key->qty*$key->rate; 
						$totTot = $totTot + $key->qty*$key->rate;
					?>

				</td>
				
				<td align="center" valign="middle">

					<?php 
						
						echo $key->sgst;
						$totSgst = $totSgst + $key->sgst;
					?>

				</td>

				<td align="center" valign="middle">
					<?php 

						echo $key->cgst;
						$totCgst = $totCgst + $key->cgst;

					?>
				</td>
				
				<td align="center" valign="middle">
					<?php 

						echo $key->igst;
						$totIgst = $totIgst + $key->igst;

					?>
				</td>
				
				<td align="center" valign="middle">
					<?php 

						echo $key->service_charge;
						$totSchrg = $totSchrg + $key->service_charge; 

					?>
				</td>
				
				<td align="center" valign="middle">
					<?php 

						echo $key->disc;
						$totDisc = $totDisc + $key->disc;

					?>
				</td>
				
				<td align="center" valign="middle">
					<?php 
						echo $key->adjustment;
						$totAdj = $totAdj + $key->adjustment;
					?>
				</td>
				
				<td align="center" valign="middle">
					<?php 

					//total room rent

						echo $key->total;
						$totalAm = $totalAm + $key->total;

					?>
				</td>
				
			</tr>
			
			<?php 
			$total = $total + $key->price+ $key->service; } } 
			?>
			
			<tr>		
				
				<td></td>
				<td></td>
				<td></td>
				
				<td align="center" valign="middle" style="font-weight:800">
					<?php 
						echo number_format($totQty,2);
					?>
				</td>
				
				<td align="center" valign="middle" style="font-weight:800">
					<?php 
						echo number_format($totRate,2);
					?>
				</td>
				
				<td align="center" valign="middle" style="font-weight:800">
					<?php 
						echo number_format($totTot,2);
					?>
				</td>
				
				<td align="center" valign="middle" style="font-weight:800">
					<?php 
						echo number_format($totSgst,2);
					?>
				</td>
				
				<td align="center" valign="middle" style="font-weight:800">
					<?php 
						echo number_format($totCgst,2);
					?>
				</td>
				
				<td align="center" valign="middle" style="font-weight:800">
					<?php 
						echo number_format($totIgst,2);
					?>
				</td>
				
				<td align="center" valign="middle" style="font-weight:800">
					<?php 
						echo number_format($totSchrg,2);
					?>
				</td>
				
				<td align="center" valign="middle" style="font-weight:800">
					<?php 
						echo number_format($totDisc,2);
					?>
				</td>
				
				<td align="center" valign="middle" style="font-weight:800">
					<?php 
						echo number_format($totAdj,2);
					?>
				</td>
				
				
				<td align="center" valign="middle" style="font-weight:800">
					<?php 
						echo number_format($totalAm,2);
					?>
				</td>
			</tr>
	</tbody>
	<hr style="background: #EDEDED; border: none; height: 1px;">
	</table>
	
	<?php



	foreach ( $details2 as $groups ) {

		# code...



		$charge_string = $groups->charges_id;



		$charge_total_price = $groups->charges_cost;

	}

	if($transaction){ ?>

	<span style="font-size:13px; font-weight:700; color:#D75C58; padding:5px 5px; display:block;"> Payment Details </span>

	<hr style="background: #D75C58; border: none; height: 1px;">

	<table width="100%" class="td-pad">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th width="5%" align="left"> # </th>

				<th width="5%" align="left"> Status </th>

				<th width="20%"> Transaction Date </th>

				<th width="10%"> Profit Center </th>

				<th width="15%"> Payment Mode </th>

				<th width="30%" align="center"> Transaction Details </th>

				<th width="10%" align="right" style="padding-right:8px;"> Amount </th>

			</tr>

		</thead>



		<tbody style="background: #fafafa">

			<?php   

			  $i = 0;
			  $paid=0;

			  foreach ($transaction as $keyt ) {
				  
				  if($keyt->t_status == 'Done' || $keyt->t_status == 'done')
					$paid=$paid+$keyt->t_amount;

				  $i++;

			  ?>

			<tr>

				<td>
					<?php echo $keyt->t_id;?>
				</td>

				<td align="center" valign="middle">
					<?php 

						if($keyt->t_status == 'Cancel')
							echo '<span style="color:red;">Declined</span>';

						else if($keyt->t_status == 'Pending')
							echo '<span style="color:orange;">Processing</span>';

						else
							echo '<span style="color:green;">Recieved</span>';

					?>
				</td>

				<td align="center" valign="middle">
					<?php echo date("g:ia dS M Y",strtotime($keyt->t_date)); ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $keyt->p_center; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $keyt->t_payment_mode; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $keyt->t_bank_name; ?>
				</td>

				<td align="right" valign="middle">
					<?php echo $keyt->t_amount; ?>
				</td>

			</tr>

			<?php  } ?>

			<tr>

				<td colspan="7">
					<hr style="background: #EEEEEE; border: none; height: 1px; ">
				</td>

			</tr>

			<tr>

				<td colspan="6" style="text-align:right; padding-right:16%;"><strong>Total Paid Amount:</strong>
				</td>

				<td align="right" valign="middle">
					<strong>

						<?php 

							if(isset($paid) && $paid && $paid != 0) {

								echo $paid;

							} else {

								$paid=0;

								echo "Yet to pay";

							}

						?>

					</strong>
				</td>

			</tr>

		</tbody>

		<?php  } ?>

	</table>

	<table class="td-pad td-fon-size" width="100%">


		<tr>

			<td align="right" valign="middle"><strong>Total Amount:</strong>
			</td>

			<td align="right" valign="middle">
				<strong>

					<?php 
						
						echo '<i class="fa fa-inr" style="font-size:12px; font-weight:normal;"></i> '.number_format($totTot + $totCgst + $totSgst + $totIgst + $totSchrg, 2);	

					?>

				</strong>
			</td>

		</tr>

		<tr>

			<td align="right" valign="middle">Discount:</td>

			<td align="right" valign="middle">
				<?php echo $totDisc;  ?>
			</td>

		</tr>

		<tr>

			<td align="right" valign="middle">Adjustment amount:</td>

			<td align="right" valign="middle">
				<?php 
					echo $totAdj; 
				?>
			</td>

		</tr>

		<tr>

			<td align="right"><strong style="font-size:13px; font-weight:600; color:#8876A8;">Total Payable Amount (Grand Total):</strong></td>

			<td align="right" valign="middle" style="font-size:13px; font-weight:700; color:#33A076;"><strong>
				<?php 

				echo '<i class="fa fa-inr" style="font-size:12px; font-weight:normal;"></i> '.number_format($totalAm, 2); ?>
			</strong></td>

		</tr>
		
		<tr>
			<td></td>
			<td align="right">
				<em style="text-transform: capitalize;"><?php 
				$net_payWords = $this->dashboard_model->getIndianCurrency($totalAm);
				if(isset($net_payWords)){
					echo ' </br>'.$net_payWords.' only';
				}
				 ?> </em>
			</td>
		</tr>
		
		<tr>

			<td align="right">Total Paid Amount:</td>

			<td align="right" valign="middle">
				<?php //$payAmount = $grand_total_amount; 

			 if(isset($paid) && $paid) {

				 echo number_format($paid, 2);

			 } else {

				  $paid=0;

				  echo 'none';

			 }

			  ?>
			</td>

		</tr>

		<tr>

			<td align="right" valign="middle"><span style="font-size:13px; font-weight:700; color:#8876A8;">Total Due:</span>
			</td>

			<td align="right" valign="middle">
				<?php 

					$td = $totalAm-$paid;

					$paid_c = number_format($td, 2);

					if($paid_c != 0){ 

				?>

				<span style="font-size:13px; font-weight:700; color:#F65656;"><i class="fa fa-inr" style="font-size:12px; font-weight:normal;"></i> <?php echo number_format($td,2);?></span>

				<?php

				} else {
				
				?>

				<span style="font-size:13px; font-weight:700; color:#7FBA67;">Full Paid</span>

				<?php } ?>
			</td>

		</tr>

	</table>

	<table width="100%">

		
		<tr>

			<td valign="bottom">

				<span style="font-size:9px; font-weight:400; color:#939292; display:block;">996311 - ACCOMODATION IN HOTEL/INN/GUEST HOUSE/ CLUB OR CAMP SITE ETC.SERVICE</span>
				</br>
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;">996411 - TRANSPORT</span>
				</br>
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;">996411 - PROGRAM,BUSINESS AUXILIARY SERVICE, SPA, DECORATION</span>
				</br>
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;">9963 - RESTAURANT SERVICE</span>
			</td>

			<td align="center" width="35%">______________________________<br/> Authorized Signature </td>

		</tr>

		<tr>

			<td width="65%">
            <?php if($chk->declaration!='') {?>
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;"><?php echo $chk->declaration;?></span>
            <?php }?>
            </td>    
			<td align="center" width="35%">
				<?php   // Showing the Hotel Name from Session

					$hotel_n = $this->dashboard_model->get_hotel_name($this->session->userdata('user_hotel'));

					if(isset($hotel_n->hotel_name) && $hotel_n->hotel_name)

						echo 'For, '.$hotel_n->hotel_name;

				?>
			</td>

		</tr>

	</table>

	<hr style="background: #EEEEEE; border: none; height: 1px; margin-top:10px; width:100%;">

	<table width="100%" class="td-pad">
		<tr>
			<td align="center"><?php if($chk->ft_text!=""){ echo $chk->ft_text; }?></td>
		</tr>
		<tr>
			<td align="center">This is a Computer Generated Invoice & should be treated as signed by an authorized signatory</td>
		</tr>
	</table>
	<hr style="background: #00C5CD; border: none; height: 1px; margin-top:10px; width:100%;">
	<span style="font-size:9px; font-weight:400; color:#939292; display:block; text-align: center;"> Powered by Royalpms. Thanks for your stay & kindly visit <?php echo $hotel_n->hotel_name; ?> again! </span>

</div>

<script>

	window.print();

</script>