<style>

	@page {
	   margin-top: 1cm !important;
	   margin-bottom: 1cm !important;
	}
	
	.header {
		width: 100%;
		text-align: center;
		position: relative;
		font-size: 10px;POS Details

	}
	.footer {
		width: 100%;
		text-align: center;
		position: fixed;
		font-size: 10px;
	}
	.header {
		top: 0px;
	}
	.footer {
		bottom: 0px;
	}
	.pagenum:before {
		content: counter(page);
	}

</style>

<link href="<?php echo base_url();?>assets/layouts/layout/css/invoice-style.css" rel="stylesheet" type="text/css" />
<?php if($printer->printer_settings=='normal'){
	$print='';
	
} else {
	$print='fon-big';
} 

ini_set('memory_limit', '64M');
$m=0;

?>

<div class="header">
    <span>Invoice - Single</span>
</div>
<div class="footer">
    Page <span class="pagenum"></span>
</div>

<div style="padding:10px 35px;" class="<?php echo $print ?> ">

	<?php

	//echo "<pre>";

	//print_r($chagr_tax['rr_tax']);

	if ( isset( $chagr_tax ) && $chagr_tax ) {

		foreach ( $chagr_tax[ 'rr_tax' ] as $key => $ch ) {

			$rm_ch[ $key ] = $ch;

		}

		foreach ( $chagr_tax[ 'mp_tax' ] as $key => $mpch ) {

			$mp_ch[ $key ] = $mpch;

		}

		foreach ( $chagr_tax[ 'all_price' ] as $key => $Allch ) {

			$all_ch[ $key ] = $Allch;

		}

	} else {

		$rm_c = 0;

		$mp_ch = 0;

		$all_ch = 0;



	}

	//print_r($all_ch);

	// print_r($chagr_tax);

	if ( isset( $adjustment ) && $adjustment->amount != 0 ) {

		$adjustment = $adjustment->amount;

	} else {

		$adjustment = 0;

	}


	$chk = $this->bookings_model->get_invoice_settings();
	/*echo "<pre>";
	print_r($chk);
	exit;
	*/

	if ( isset( $chk->booking_source_inv_applicable ) ) {

		$booking_source = $chk->booking_source_inv_applicable;

	} else {

		$booking_source = "";

	}


	if(isset($chk->hotel_sub_text)){

	$hotel_sub_text = $chk->hotel_sub_text;

}else{

	$hotel_sub_text = 0;
}

	if ( isset( $chk->nature_visit_inv_applicable ) ) {

		$nature_visit = $chk->nature_visit_inv_applicable;

	} else {

		$nature_visit = "";

	}

	if ( isset( $chk->booking_note_inv_applicable ) ) {

		$booking_note = $chk->booking_note_inv_applicable;

	} else {

		$booking_note = "";

	}

	if ( isset( $chk->company_details_inv_applicable ) ) {

		$company_details = $chk->company_details_inv_applicable;

	} else {

		$company_details = "";

	}

	if ( isset( $chk->service_tax_applicable ) ) {

		$service_tax = $chk->service_tax_applicable;

	} else {

		$service_tax = 0;

	}

	if ( isset( $chk->service_charg_applicable ) ) {

		$service_charg = $chk->service_charg_applicable;

	} else {

		$service_charg = 0;

	}

	if ( isset( $chk->authorized_signatory_applicable ) ) {

		$authority_sign = $chk->authorized_signatory_applicable;

	} else {

		$authority_sign = 0;

	}

	if ( isset( $chk->unit_no ) ) {

		$unit_no = $chk->unit_no;

	} else {

		$unit_no = 0;

	}

	if ( isset( $chk->unit_cat ) ) {

		$unit_cat = $chk->unit_cat;

	} else {

		$unit_cat = 0;

	}

	if ( isset( $chk->luxury_tax_applicable ) ) {

		$luxury_tax = $chk->luxury_tax_applicable;

	} else {

		$luxury_tax = 0;

	}

	if ( isset( $chk->invoice_setting_food_paln ) ) {

		$fd_plan = $chk->invoice_setting_food_paln;

	} else {

		$fd_plan = 0;

	}



	if ( isset( $chk->invoice_setting_service ) ) {

		$service = $chk->invoice_setting_service;

	} else {

		$service = 0;

	}



	if ( isset( $chk->invoice_setting_extra_charge ) ) {

		$extra_charge = $chk->invoice_setting_extra_charge;

	} else {

		$extra_charge = 0;

	}



	if ( isset( $chk->invoice_setting_laundry ) ) {

		$laundry = $chk->invoice_setting_laundry;

	} else {

		$laundry = 0;

	}



	if ( isset( $chk->invoice_pref ) && isset( $chk->invoice_suf ) ) {

		$suf = $chk->invoice_suf;

		$pref = $chk->invoice_pref;

	} else {

		$suf = "";

		$pref = "";

	}

	if ( isset( $chk->logo ) ) {

		$logo = $chk->logo;

	} else {

		$logo = 0;

	}
	
		if ( isset( $chk->pos_app ) ) {

		$pos_settings = $chk->pos_app;

	} else {

		$pos_settings = 0;

	}
	
		if ( isset( $chk->adjust_app ) ) {

		$adjust_app = $chk->adjust_app;

	} else {

		$adjust_app = 0;

	}
	
		if ( isset( $chk->disc_app ) ) {

		$disc_app = $chk->disc_app;

	} else {

		$disc_app = 0;

	}
	
		if ( isset( $chk->fd_app ) ) {

		$fd_app = $chk->fd_app;

	} else {

		$fd_app = 0;

	}
	
	if ( isset( $chk->pos_seg ) ) {

		$pos_seg = $chk->pos_seg;

	} else {

		$pos_seg = 0;

	}
	
	if ( isset( $chk->font_size ) ) {

		$font_sizeH = $chk->font_size;

	} else {

		$font_sizeH = 14;

	}
	
	if ( isset( $chk->hotel_color ) ) {

		$colorH = $chk->hotel_color;

	} else {

		$colorH = '#000000';

	}
	
		if ( isset( $chk->declaration ) ) {

		$declaration = $chk->declaration;

	} else {

		$declaration = '';

	}
	
	if ( isset( $chk->invoice_no ) ) {

		$invoice_no = $chk->invoice_no;

	} else {

		$invoice_no = 0;

	}
	
	if (isset( $chk->show_bk_type )) {

		$show_bk_type = $chk->show_bk_type;

	} else {

		$show_bk_type = 'yes';

	}
	if (isset( $chk->booking_number_generate )) {

		//echo $chk->booking_number_generate;
		$booking_number_inv = $chk->booking_number_generate;

	} else {

		$booking_number_inv = 0;

	}
	
	if(isset($chk->font_size_gp)){

	$font_size_gp = $chk->font_size_gp;

}else{

	$font_size_gp = 0;
}


	if(isset($splitBill->splitBill)){

$splitBill = $splitBill->splitBill;

}else{

	$splitBill = 0;
}

	$flag = 0;

	//echo "<pre>";

	//print_r($chk);

	?>
	

	<table width="100%">

		<tr>
		<?php if($logo!=0){?>
			<td align="left" valign="middle"><img src="upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>"/>

			</td>
		<?php }?>
			<td align="right" valign="middle">
				<strong style="font-size:<?php echo $font_sizeH; ?>px !important; color:<?php echo $colorH; ?>;">
					<?php echo $hotel_name->hotel_name;?>
				</strong>
				<br>
				<?php
				if($hotel_sub_text==1){
				?>
				<strong style="font-size:<?php echo $font_size_gp; ?>px !important; color:<?php echo $colorH; ?>;">
					<?php echo $hotel_name->hotel_gpname;?>
				</strong>
<?php
				
				}
?>			
</td>
		</tr>

		<tr>
			<td colspan="2">

				<hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;">

			</td>
		</tr>

	</table>
	<table width="100%">

		<tr>

			<?php 

if(isset($tax) && $tax ){

	//echo "<pre>";

	//print_r($tax);

	//exit;

    foreach($tax as $tax_details){

foreach($booking_details as $bookings){ 

 

    $bd_id=$bookings->booking_id;
    
    $invoice=$this->dashboard_model->get_invoice($bd_id);
    $bk_number=$this->dashboard_model->get_booking_number($bd_id,'sb');
	$inv_number=$this->dashboard_model->get_invoice_number($bd_id,'sb');
   
?>

			<td width="70%" align="left" valign="top">
				<span style="color:#3D4B76; font-weight:bold; font-size:12px; line-height:20px; display:block;">TAX INVOICE 
					<?php 
					
						if($show_bk_type == 'yes'){
							if($bookings->group_id == 0)
								echo ' (Single Unit Reservation)';
							else
								echo ' <span style="color:#F8681A;">(Single under Group)</span>'; 
						}
						
					?>
				</span>

				<?php 
				   
				
					if($booking_number_inv == 0) {
						
						if($invoice && isset($invoice))	{
							
							if($invoice_no==1) {
								    echo $pref.$invoice->invoice_no.$invoice->id.$suf;
								
							} else {
							    
							echo "<strong>".$pref."</strong><span>"." / ".$inv_number->invoice_id."  ".$suf."</span>";
							}
						} 
					} else {
					
						echo "<strong>".$pref."</strong><span>"." / ".$inv_number->invoice_id."  ".$suf."</span>";
				
					}
				?>

				<br/>

				<?php

				date_default_timezone_set( "Asia/Kolkata" );

				echo "DATE:&nbsp;", date( "g:i a \-\n l jS F Y" )

				?>

				<?php

				foreach ( $total_payment as $row3 ) {

					$total_amount = $row3->t_amount;

					?>

				<?php }

           // ?end

          

          ?>

				

				<?php 

		// _sb dt25_10_16 - Only 2 Foreach

		$flag = 0;

		foreach($booking_details as $row){ 

		  $guest_det=$this->dashboard_model->get_guest_details($row->guest_id);

		  foreach($guest_det as $gst)

		  {

    

			 if(($row->bill_to_com == '1') && ($gst->g_type == 'Corporate') && ($gst->corporate_id != NULL) && ($gst->corporate_id != 0)) // Check if Bill to Company is Possible

			  {

				   $corporate_details=$this->dashboard_model->fetch_c_id1($gst->corporate_id); 

				   ?>



				<ul class="list-unstyled">

					<li>

						<strong style="color:#6B67A1; font-size:15px;">

							<?php

							if ( $corporate_details->legal_name )

								echo $corporate_details->legal_name;

							else

								echo $corporate_details->name;

							?>

						</strong>

					</li>

					<li>

						<?php 

						$fg = 10;

						if(isset($corporate_details->address) && ($corporate_details->address != '' && $corporate_details->address != ' ')){

                                        echo ($corporate_details->address);

                                        $fg = 0	;

                                        //echo ', ';

                                    }

						if(isset($corporate_details->city) && ($corporate_details->city != '' && $corporate_details->city != ' ')){

                                        if($fg == 0){

                                            echo ', ';

                                            

                                        }

                                            

                                        echo $corporate_details->city;

                                        echo '<br>';

                                        $fg = 1;

                                    }

						else if($fg == 0)
                                $fg = 5;

						if(isset($corporate_details->state) && ($corporate_details->state != '' && $corporate_details->state != ' ')){

                                        if($fg == 5)

                                            echo '<br>';

                                        echo $corporate_details->state;

                                        $fg = 2;

                                        //echo ', ';

                                    }

						if(isset($corporate_details->pin_code) && $corporate_details->pin_code!= NULL){

                                        if($fg == 2 || $fg == 5){

                                            echo ', ';

                                        }									

                                        echo 'PIN - '.$corporate_details->pin_code;

                                        $fg = 3;

                                    }

						if(isset($corporate_details->country) && $corporate_details->country != '' && $corporate_details->country != ' '){

                                        if($fg == 3)

                                            echo ', ';

                                        echo $corporate_details->country;

                                        /*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';

                                        $fg = 4;*/

                                    }

						if($fg == 10){

                                        echo 'No Info';

                                    }

						?>



					</li>

					<li>

						<?php echo '<strong>Phone: </strong>'.$corporate_details->g_contact_no; ?> </li>

					<li>

						<?php echo '<strong>Email: </strong>'.$corporate_details->corp_email; ?> </li>

					<li>

						<?php echo '<strong>GSTIN: </strong>' ?>
						<?php 
							if(isset($corporate_details->gstin) && $corporate_details->gstin != "" && $corporate_details->gstin != null)
								echo $corporate_details->gstin; 
							else
								echo '<span style="color:#888; font-style: italic;">not applicable/ no info</span>';
						?> 

					</li>

					<li style="font-size:13px;">

						<?php echo '<strong>Guest Name: </strong>';
						
								if($gst->g_gender == 'male' || $gst->g_gender == 'Male'){
									 $sex='<label style="color:#0476F6"><i style"color:#0476F6;" class="fa fa-male" aria-hidden="true"></i></label>';
								}else if($gst->g_gender == 'female' || $gst->g_gender == 'Female'){
									$sex='<label style="color:#FF2A6A"><i style"color:#FF2A6A;" class="fa fa-female" aria-hidden="true"></i></label>';
								}else{
									$sex='';
								}
							
								if($gst->g_gender == 'Male' || $gst->g_gender == 'male')
									$salu = 'Mr. ';
								else if($gst->g_gender == 'Female' || $gst->g_gender == 'male'){
									if($gst->g_married == 'married')
										$salu = 'Mrs. ';
									else
										$salu = 'Ms. ';
								}
								else
									$salu = '';

							echo $salu.' '.$gst->g_name;

							if($gst->c_des)

								echo ' - '.$gst->c_des;

						?> 
					</li>

					</ul>

					<?php

					} // End IF 
					else {

					?>

					<ul class="list-unstyled">

					<li>

						<strong style="font-size:15px; color:#6B67A1; text-transform: capitalize;">

							<?php 
								if($gst->g_gender == 'male' || $gst->g_gender == 'Male'){
									 $sex='<label style="color:#0476F6"><i style"color:#0476F6;" class="fa fa-male" aria-hidden="true"></i></label>';
								}else if($gst->g_gender == 'female' || $gst->g_gender == 'Female'){
									$sex='<label style="color:#FF2A6A"><i style"color:#FF2A6A;" class="fa fa-female" aria-hidden="true"></i></label>';
								}else{
									$sex='';
								}
							
								if($gst->g_gender == 'Male' || $gst->g_gender == 'male')
									$salu = 'Mr. ';
								else if($gst->g_gender == 'Female' || $gst->g_gender == 'male'){
									if($gst->g_married == 'married')
										$salu = 'Mrs. ';
									else
										$salu = 'Ms. ';
								}
								else
									$salu = '';

							echo $salu.' '.$gst->g_name;
							
							?> 
							
						</strong>

					</li>

					<li>

						<?php 

						$fg = 10;

						if(isset($gst->g_address) && ($gst->g_address != '' && $gst->g_address != ' ')){

												echo ($gst->g_address);

												$fg = 0	;

												//echo ', ';

											}

						if(isset($gst->g_city) && ($gst->g_city != '' && $gst->g_city != ' ')){

												if($fg == 0){

													echo ', ';

													

												}

													

												echo $gst->g_city;

												echo '<br>';

												$fg = 1;

											}

						else if($fg == 0)

												$fg = 5;

						if(isset($gst->g_state) && ($gst->g_state != '' && $gst->g_state != ' ')){

												if($fg == 5)

													echo '<br>';

												echo $gst->g_state;

												$fg = 2;

												//echo ', ';

											}

						if(isset($gst->g_pincode) && $gst->g_pincode!= NULL){

												if($fg == 2 || $fg == 5){

													echo ', ';

												}									

												echo 'PIN - '.$gst->g_pincode;

												$fg = 3;

											}

						if(isset($gst->g_country) && $gst->g_country != '' && $gst->g_country != ' '){

												if($fg == 3)

													echo ', ';

												echo $gst->g_country;

												/*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';

												$fg = 4;*/

											}

						if($fg == 10){

												echo 'No Info';

											}

					?>

					</li>

					<li><strong>Phone: </strong>

						<?php echo $gst->g_contact_no; ?> </li>

					<li><strong>Email: </strong>
						<?php echo $gst->g_email; ?> 
					</li>

					<li>

						<?php echo '<strong>GSTIN: </strong>' ?>
						<?php 
							if(isset($gst->gstin) && $gst->gstin != "" && $gst->gstin != null)
								echo $gst->gstin; 
							else
								echo '<span style="color:#888; font-style: italic;">not applicable/ no info</span>';
						?> 
					</li>

				</ul>

				<?php } // End Else

      } // End Foreach

	} // End Foreach

    ?>

				<ul class="list-unstyled">

					<li style="height:5px;">&nbsp;</li>

					<?php

					if ( $booking_source == '1' ) {

						foreach ( $payment_details as $row2 ) {

							?>

							<li><strong>Booking Source: </strong>

							<?php $bk_source=$this->dashboard_model->getsourceId($row2->booking_source); 

							

							if($bk_source){

								echo $bk_source->booking_source_name;

							} else {

								echo $row2->booking_source;

							} 
						} 
					} ?>

					<?php

					if ( $nature_visit == '1' ) {

						foreach ( $payment_details as $row2 ) {

							?>

					<?php if(isset($row2->nature_visit) && $row2->nature_visit != ""){?>

					<strong> | Nature of Visit: </strong>

						<?php  echo $row2->nature_visit.' '; ?>

					</li>

					<?php } ?>

					<?php } } 
					
					if(isset($row2->booking_status_id) && $row2->booking_status_id != "") {
													
													$status_data = '';
													$status = $row2->booking_status_id;
													
                                                    if($status==4){
                                                        $status_data="Confirmed";
                                                    }else if($status==1){
                                                        $status_data="Confirmed";
                                                    }else if($status==2){
                                                        $status_data="Advance";
                                                    }else if($status==3){
                                                        $status_data="Pending";
                                                    }else if($status==5){
                                                        $status_data="Checked-In";
                                                    }else if($status==6){
                                                        $status_data="Checked-Out";
                                                    }else if($status==8){
                                                        $status_data="Incomplete";
                                                    }
                                                    else if($status==7){
                                                        $status_data="Cancelled";
                                                    }
                                                    else{
                                                        $status_data="undefined";
                                                    }
													
													$status_secondary = $row2->booking_status_id_secondary;
                                                    if($status_secondary==3){
                                                        $status_data2=" (Pending/ Checkin due)";
                                                    }
                                                    else if($status_secondary==9){
                                                        $status_data2=" (Payment Due)";
                                                    }
                                                    else{
                                                        $status_data2="";
                                                    }
													
				
						echo '<strong>Reservation Status: </strong> <span style="font-size: 13px">'.$status_data.'</span>'.$status_data2;
						
					}
					
					?>

				</ul>

			</td>

			<td width="30%" align="left" valign="top">

				<?php if($company_details == '1'){ ?> GST Reg No.:

				<?php  echo $tax_details->service_tax_no;?>

				<br/> CIN No.:

				<?php  echo $tax_details->cin_no; } else { ?> &nbsp;

				<?php } ?>

				<div class="well">

					<strong>

						<?php echo  $hotel_name->hotel_name; ?>

					</strong><br/>

					<?php echo  $hotel_contact['hotel_street1']; ?>

					<?php echo  $hotel_contact['hotel_street2'].', '; ?><br>

					<?php echo  $hotel_contact['hotel_district']; ?> -

					<?php echo  $hotel_contact['hotel_pincode']; ?>

					<?php echo  $hotel_contact['hotel_state']; ?> -

					<?php echo  $hotel_contact['hotel_country']; ?><br/>

					<strong>Contact No:</strong>
					<?php echo  '+91 '.$hotel_contact['land_phone_no'].'/ '.$hotel_contact['hotel_frontdesk_mobile']; ?><br/>

					<strong>Email:</strong>
					<?php echo  $hotel_contact['hotel_frontdesk_email']; ?><br/>
					
					<strong>Website:</strong>
					<?php echo  $hotel_contact['hotel_website']; ?><br/>

				</div>

			</td>

			<?php }}}?>

		</tr>

	</table>


	</br>
	<span style="font-size:13px; font-weight:700; color:#009595; display:block; padding:5px 5px;">Booking Details</span>

	<hr style="background: #009595; border: none; height: 1px;">

	<table width="100%" class="td-pad">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th align="left"> # </th>

				<th width="30%" align="left"> Product / Service Name </th>

				<th width="8%" align="left" valign="middle"> HSN/ SAC </th>
				
				<th> Qty </th>

				<th> Unit Price </th>

				<th> Modifier </th>

				<th> Amount </th>

				<th> Total Tax </th>

				<th align="right"> Total </th>

			</tr>

		</thead>

		<tbody style="background: #fafafa">

			<?php

			$total_for_one_booking = 0;

			$sc = 0;

			$st = 0;

			$lc = 0;

			$discount = 0;

			foreach ( $total_payment as $row3 ) {

				$total_amount = $row3->t_amount;

				$room_rent_sum_total = 0;

				if ( isset( $payment_details ) ) {

					?>

			<?php foreach($payment_details as $row2)

                    { 

                     

                    ?>

			<tr>

				<?php if($group_id =="0"){	

				$ab=$row2->booking_id; 

	$d=0;

	$co=0;

	$disc1= $this->unit_class_model->get_discount_details_single($ab);

	if(isset($disc1) && $disc1){

		foreach($disc1 as $ds){

			$co++;

			$d+=$ds->discount_amount;

		}

	}else{

		$d=0;

	}

	?>
				<td style="text-align:left">

					<?php 
					   if($booking_number_inv == 0){
					    echo $row2->booking_id; 
						}else{ 
						echo $bk_number->c_booking_id;
						}
					 ?>

				</td>

				<td style="text-align:left">

				<?php // Unit Details

                $units=$this->dashboard_model->get_unit_exact($row2->unit_id);

                foreach ($units as $key) {

                            # code...

                                $unit_name=$units->unit_name;

                }

                // Logic for checking Invoice Settings, wheather to show Unit Details

				if($unit_cat == '1'){

					echo $unit_name;

				}

				else{

					if($unit_no != '1')

						echo 'Unit 1';

				}

				if($unit_no == '1'){

					echo " (Room No - ".$row2->room_no.") ";

				}

				?>

					<br/>

					CheckIn Date:

					<?php echo date('d M , Y',strtotime($row2->cust_from_date_actual)); ?> at

					<?php echo date('h:i A',strtotime($row2->confirmed_checkin_time));?> <br/>

					Checkout Date:

					<?php echo date('d M , Y',strtotime($row2->cust_end_date_actual)); ?> at

					<?php echo date('h:i A',strtotime($row2->checkout_time));
					
					echo "<br />[".$row2->no_of_adult."(A) + ".$row2->no_of_child."(C)]";

					if($booking_note == '1'){ echo $row2->comment; }?>

				</td>
				
				<td align="center" valign="middle">

					<?php 

						echo '996311';

					?>

				</td>

				<td style="text-align:center">
					<?php 
						echo $row2->stay_days;
						
						if($row2->stay_days > 1)
							echo " Days";
						else if($row2->stay_days == 1)
							echo " Day";
					?>
				</td>

				<td style="text-align:center">
					<?php

						if ( $row2->rent_mode_type == "add" ) {

							$fv = ( $chagr_tax[ 'all_price' ][ 'room_rent' ] / $row2->stay_days ) - $row2->mod_room_rent;

						} else {

							$fv = ( $chagr_tax[ 'all_price' ][ 'room_rent' ] / $row2->stay_days ) + $row2->mod_room_rent;

						}

						echo number_format( $fv, 2, '.', '' );

					?>

				</td>

				<td style="text-align:center">

					<?php if($row2->rent_mode_type == "add"){echo '(+) ';} else { echo '(-) ';} echo number_format($row2->mod_room_rent, 2, '.','');

					if($row2->stay_days>1 && $row2->mod_room_rent!=0)

					echo "<br> (".number_format($row2->mod_room_rent*$row2->stay_days, 2, '.','').")";; ?>

				</td>

				<?php } ?>

				<td style="text-align:center">

					<?php   
					
						if($group_id =="0"){

									echo $price= number_format($all_ch['room_rent'], 2, '.',''); 

						} else {

							$price=0;

						}   
				  
					  if(isset($chagr_tax['rr_tax']) && $chagr_tax['rr_tax'] ){
						  $tot_rtax = $chagr_tax['rr_tax']['total'];
					  } else {
						  $tot_rtax=0;
					  }
					  
					  if(isset($chagr_tax['mp_tax']) && $chagr_tax['mp_tax']){
						  $tot_mptax = $chagr_tax['mp_tax']['total'];
					  } else {
						  $tot_mptax=0; 
					  }

						if($row2->room_rent_tax_details == 'No Tax') {


						}

					if($row2->room_rent_tax_details == 'No Tax')

							$lc=0;

							
							$discount=$row2->discount;

				?>
				</td>

				<td style="text-align:center">
					<?php 

						if($all_ch['exrr_chrg']==0 && isset($chagr_tax['rr_tax']['total'])){

							echo number_format($chagr_tax['rr_tax']['total'],2,'.','');

							$rm_tax=$chagr_tax['rr_tax']['total'];

						}else{

							$rm_tax=0;
							echo '-';

						}

					?>
				</td>

				<td style="text-align:right">
					<?php  
					
						if($group_id =="0"){

							$room_rent_sum_total= $all_ch['room_rent'];

							echo number_format($room_rent_sum_total+$rm_tax, 2, '.','');

						}

					?>
				</td>

			</tr>



			<?php 

				$ex_per = 0;

				if($all_ch['exrr_chrg']!=0){

				$ex_per=$all_ch['exrr_chrg'];

				} else {

					$ex_per=0;

				}

				$total_ex = 0;

				if($ex_per && $all_ch['exrr_total_tax']!=''){

					$total_ex=$all_ch['exrr_total_tax']+$ex_per;

				}else{

					$total_ex=$ex_per;

				}

				if($ex_per  > 0){
			?>


			<tr>

				<td align="left">
					<?php 

						if($group_id == '0')

						{echo "#";} 

					?>
				</td>

				<td align="left">
					<?php 

						if($ex_per>0)

							echo 'Extra person Room Rent '; 

						if(isset($row2->ex_ad_no)){

							echo "<br />[".$row2->ex_ad_no."(A) + ".$row2->ex_ch_no."(C)]";

						}

					?>
				</td>
				
				<td align="center" valign="middle">

					<?php 

						echo '996311';

					?>

				</td>

				<td align="center">
					<?php 

						echo $row2->stay_days;
						
						if($row2->stay_days > 1)
							echo " Days";
						else if($row2->stay_days == 1)
							echo " Day";

					?>
				</td>

				<td align="center">
					<?php 

						if(isset($row2->ex_per_uprice))

							echo  number_format($row2->ex_per_uprice,2,'.','');

					?>
				</td>

				<td align="center">

					n/a

				</td>



				<td align="center">

					<?php if($ex_per)echo number_format($ex_per,2,'.','');?>

				</td>



				<td align="center">

					-

				</td>

				<td align="right">
					<?php 
						echo number_format($ex_per, 2, '.','');
					?>
				</td>

			</tr>

			<tr>

				<td align="left">
					<?php 

						if($group_id == '0')

						{echo "#";} 

					?>
				</td>



				<td align="left">
					<?php 

						if($ex_per>0)
							echo '<strong>Total Room Rent </strong>'; 

						if(isset($row2->ex_ad_no)){

						}

					?>
				</td>
				
				<td align="center" valign="middle">

					<?php 

						echo '-';

					?>

				</td>

				<td align="center">
					<?php 

						echo $row2->stay_days;
						
						if($row2->stay_days > 1)
							echo " Days";
						else if($row2->stay_days == 1)
							echo " Day";

					?>
				</td>

				<td align="center">
					<?php 

						if(isset($row2->ex_per_uprice))
							echo  number_format($row2->ex_per_uprice+$fv,2,'.','');

					?>
				</td>

				<td align="center">

					n/a

				</td>

				<td align="center">
					<?php if($ex_per)echo number_format($ex_per+$all_ch['room_rent'],2,'.','');?>
				</td>

				<td align="center">
					<?php 

						 if($group_id =="0"){echo number_format($tot_rtax, 2, '.','');} ; 

					?>
				</td>

				<td align="right">
					<?php 

						echo number_format($all_ch['room_rent']+$tot_rtax+$ex_per, 2, '.','');
						$tot_rm_ch=$all_ch['room_rent']+$tot_rtax+$ex_per;

					?>
				</td>

			</tr>

			<?php } 
			
			if($fd_plan == '1') {
			
			?>

			<tr>

				<td align="left">

					<?php 

						if($group_id == '0' )

						{echo "#";} 

					?>

				</td>

				<td align="left">
					<?php 

						$data=$this->rate_plan_model->fetch_food_plan_name($row2->food_plans);

						if($data)
							echo 'Meal Plan - <strong>'.$data->name.'</strong>'; 

						if(isset($data->name)) 
							echo "<br />[".$row2->no_of_adult."(A) + ".$row2->no_of_child."(C)]"; 

					?>
				</td>
				
				<td align="center" valign="middle">

					<?php 

						echo '9963';

					?>

				</td>

				<td align="center">
					<?php 
						echo $row2->stay_days;
						
						if($row2->stay_days > 1)
							echo " Days";
						else if($row2->stay_days == 1)
							echo " Day";
					?>
				</td>

				<td style="text-align:center">
					<?php

						if ( $all_ch[ 'meal_plan_chrg' ] > 0 ) {

							$total_fd_price = $all_ch[ 'meal_plan_chrg' ] + $tot_mptax;

						}

						if ( $row2->food_plan_mod_type == "add" ) {

							$fv1 = ( $all_ch[ 'meal_plan_chrg' ] / $row2->stay_days ) - $row2->food_plan_mod_rent;

						} else {

							$fv1 = ( $all_ch[ 'meal_plan_chrg' ] / $row2->stay_days ) + $row2->food_plan_mod_rent;

						}

						echo number_format( $fv1, 2, '.', '' );

					?>
				</td>

				<td style="text-align:center">
					<?php 

						if($row2->food_plan_mod_type == ""){

							
						}

						else if($row2->food_plan_mod_type == "add"){

							echo '(+) ';

						} else { 

							echo '(-) ';

						} 
						echo number_format($row2->food_plan_mod_rent, 2, '.',''); 

						if($row2->stay_days>1)
							echo "<br> (".number_format($row2->food_plan_mod_rent*$row2->stay_days, 2, '.','').")"; 

					?>
				</td>

				<td align="center">
					<?php 

						if($all_ch['meal_plan_chrg']!=0){

						  echo number_format($all_ch['meal_plan_chrg'], 2, '.','');

						}
						else
						  echo '0';

				    ?>
				</td>



				<td align="center">
					<?php 

						if($all_ch['exmp_chrg']==0){

							echo number_format($tot_mptax,2,'.','');

							$ex_mp=$tot_mptax;

						} else {
							$ex_mp=0;
							echo '-';
						}	

					?>
				</td>

				<td align="right">
					<?php 
						if($row2->food_plan_price!=0){

						  $food_price =$row2->food_plan_price+$ex_mp;
						  echo number_format($food_price, 2, '.','');

						}
						else
							echo '0.00';
				    ?>
				</td>
				
			</tr>

			<?php 
			}
			
				$total_ex_m=0;
				$ex_per_m=$all_ch['exmp_chrg'];
				
				if($ex_per_m>0) {
				
			?>

			<tr>

				<td align="left">

					<?php if($group_id == '0' )

						{echo "#";} ?>

				</td>

				<td align="left">
					<?php 

						echo 'Extra person Meal Charge';

						if(isset($row2->ex_ad_no)) 

							echo "<br />[".$row2->ex_ad_no."(A) + ".$row2->ex_ch_no."(C)]";		

					?>
				</td>
				
				<td align="center" valign="middle">

					<?php 

						echo '9963';

					?>

				</td>

				<td align="center">
					<?php 
						echo $row2->stay_days;
						
						if($row2->stay_days > 1)
							echo " Days";
						else if($row2->stay_days == 1)
							echo " Day";
					?>
				</td>

				<td align="center">

					<?php if(isset($row2->ex_per_u_mprice))echo number_format($row2->ex_per_u_mprice,2,'.','')?>

				</td>

				<td align="center">n/a</td>



				<td align="center">

					<?php if($ex_per_m)echo number_format($ex_per_m,2,'.','');?>

				</td>

				<td align="center">

					-

				</td>

				<td align="right">

					<?php 

						if($ex_per_m)

						$total_ex_m=$all_ch['exmp_total_tax']+$ex_per_m;

						echo number_format($ex_per_m, 2, '.','');

					?>

			</td>

			</tr>

			<tr>

				<td align="left">
					<?php 
					if($group_id == '0' )

					{echo "#";} ?>
				</td>

				<td align="left">
					<?php 
						echo '<strong>Total Meal Charge </strong>';

						if(isset($row2->ex_ad_no)) 

							//echo "<br />[".$row2->ex_ad_no."(A) + ".$row2->ex_ch_no."(C)]";		

					?>
				</td>
				
				<td align="center" valign="middle">

					<?php 

						echo '-';

					?>

				</td>
				
				<td align="center">
					<?php 

						echo $row2->stay_days;
						
						if($row2->stay_days > 1)
							echo " Days";
						else if($row2->stay_days == 1)
							echo " Day";

					?>
				</td>

				<td align="center">
					<?php if(isset($row2->ex_per_u_mprice))echo number_format($row2->ex_per_u_mprice+$fv1,2,'.','')?>
				</td>

				<td align="center">n/a</td>



				<td align="center">

					<?php if($ex_per_m)echo number_format($ex_per_m+$all_ch['meal_plan_chrg'],2,'.','');?>

				</td>

				<td align="center">

					<?php $food_tax5=0;

		   

						if($tot_mptax!=0){

						$food_tax5 =$tot_mptax;

						  echo number_format($food_tax5, 2, '.','');

					

						}

						else

							echo '0.00';?>



				</td>

				<td align="right">

					<?php 

						

						if($ex_per_m)

						$total_ex_m=$all_ch['exmp_total_tax']+$food_tax5;

						echo number_format($ex_per_m+ $food_price+ $food_tax5, 2, '.','');

						

					?>

				</td>

			</tr>


			<?php }?>

		</tbody>

	</table>

	<hr style="background: #EDEDED; border: none; height: 1px;">



	<?php



	$service_string = $row2->service_id;

	$service_total_price = $row2->service_price;

	$service_sum = 0;

	if ( $service_string != "" ) {

		?>

	<span style="font-size:13px; font-weight:700; color:#6A4178; display:block; padding:5px 5px;">Extra Services</span>

	<hr style="background: #6A4178; border: none; height: 1px;">

	<table width="100%" class="td-pad">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th align="left"> No </th>

				<th width="15%" align="left"> Service Name </th>

				<th> Qty </th>

				<th> Unit Price </th>

				<th> Amount </th>

				<th> Total Tax </th>

				<th align="right"> Total </th>

			</tr>

		</thead>

		<tbody style="background: #fafafa">



			<?php



			$counter = 0;

			$temp = 0;

			$service_array = explode( ",", $service_string );



			// print_r($service_array); 

			//exit;



			for ( $i = 1; $i < sizeof( $service_array ); $i++ ) {



				$services = $this->dashboard_model->get_service_details1( $service_array[ $i ] );

				//	echo "****";

				//print_r($services); 

				$occurance_strng = substr_count( $row2->service_id, $service_array[ $i ] );

				//	echo "####";

				//	print_r($occurance_strng);

				//exit;

				if ( $occurance_strng > 1 ) {

					$counter++;

					if ( $counter == 1 ) {

						?>

			<tr>

				<td style="text-align:left">

					<?php echo $counter; ?>

				</td>

				<td align="left">

					<?php echo $services->s_name; ?>

				</td>

				<td style="text-align:center">

					<?php echo $occurance_strng; ?>

				</td>

				<td style="text-align:center">

					<?php echo number_format($services->s_price, 2, '.',''); ?>

				</td>

				<td style="text-align:center">

					<?php $service_total= ($services->s_price * $occurance_strng); echo number_format($service_total, 2, '.',''); ?>

				</td>

				<td style="text-align:center">

					<?php echo $service_tax= ($services->s_tax ); ?>

				</td>

				<td style="text-align:right">

					<?php $service_grand_total=$service_total+$service_tax; echo number_format($service_grand_total, 2, '.','');?>

				</td>

			</tr>

			<?php

			}

			} else {

				$counter++;


				?>

			<tr>

				<td style="text-align:left">

					<?php echo $counter; ?>

				</td>

				<td align="left">

					<?php echo $services->s_name; ?>

				</td>

				<td style="text-align:center">

					<?php echo $occurance_strng; ?>

				</td>

				<td style="text-align:center">

					<?php echo number_format($services->s_price, 2, '.',''); ?>

				</td>

				<td style="text-align:center">

					<?php $service_total= ($services->s_price * $occurance_strng); echo number_format($service_total, 2, '.','');?>

				</td>

				<td style="text-align:center">

					<?php echo $service_tax= ($services->s_tax ); ?>

				</td>

				<td style="text-align:right">

					<?php $service_grand_total=$service_total+$service_tax; echo number_format($service_grand_total, 2, '.','');?>

				</td>

			</tr>



			<?php 

                        }

                        $service_sum=$service_sum+$service_grand_total; 

                    }

					?>

		</tbody>

	</table>

	<hr style="background: #EDEDED; border: none; height: 1px;">



	<?php

	}
 

	$charge_string = $row2->booking_extra_charge_id;

	$charge_total_price = $row2->booking_extra_charge_amount;


	$charge_sum = 0;
$master=$this->unit_class_model->get_laundry_master($bid,'0');
	if ( $charge_string != "" ||  (isset($master) && $master) ) {

		?>

	<span style="font-size:13px; font-weight:700; color:#366B9B; display:block; padding:5px 5px;">Extra Charge Details</span>

	<hr style="background: #366B9B; border: none; height: 1px;">

	<table width="100%" class="td-pad">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th align="left"> # </th>
				<th align="center" width="10%"> Date </th>
				<th align="left" width="20%"> Service Name </th>
				<th align="left"> HSN/ SAC </th>
				<th align="center"> Qty </th>
				<th align="center"> Unit Price </th>
				<th align="center"> Amount </th>
				<th align="center"> SGST </th>				
				<th align="center"> CGST </th>				
				<th align="center"> IGST </th>				
				<th align="center"> UTGST </th>			
				<th align="center"> Total Tax </th>				
				<th align="center"> Disc </th>
				<th align="right"> Total </th>

			</tr>

		</thead>

		<tbody style="background: #fafafa">



			<?php



			$charge_array = explode( ",", $charge_string );


			for ( $i = 1; $i < sizeof( $charge_array ); $i++ ) {

				$charges = $this->dashboard_model->get_charge_details( $charge_array[ $i ] );



				?>

			<tr>

				<td align="left">

					<?php echo $charges->crg_id; ?>

				</td>

				<td align="center">

					<?php echo date("g:ia dS M Y",strtotime($charges->aded_on)); ?>

				</td>

				<td align="left">

					<?php echo $charges->crg_description; ?>

				</td>
				
				<td align="left">

					<?php echo $charges->hsn_sac; ?>

				</td>

				<td align="center">

					<?php echo $charges->crg_quantity; ?>

				</td>

				<td align="center">

					<?php echo $charges->crg_unit_price; ?>

				</td>

				<td align="center">

					<?php echo $charge_total = ($charges->crg_unit_price * $charges->crg_quantity); ?>

				</td>

				<td align="center">

					<?php echo $charges->cgst*$charge_total/100; ?>

				</td>
				
				<td align="center">

					<?php echo $charges->sgst*$charge_total/100; ?>

				</td>
				
				<td align="center">

					<?php echo $charges->igst*$charge_total/100; ?>

				</td>
				
				<td align="center">

					<?php echo $charges->utgst*$charge_total/100; ?>

				</td>
				
				<td align="center">

					<?php echo $charges->crg_tax*$charge_total/100; ?>

				</td>
				
				<td align="center">

					<?php echo '0.00';?>

				</td>
				<td align="right">

					<?php echo $charge_grand_total=$charges->crg_total; ?>

				</td>

			</tr>

			<?php $charge_sum=$charge_sum+$charge_grand_total; 

	  }

	   
			
			$totalLaundry=0;
			$discountLaundry=0;
			$i=0;
			
			  if(isset($master) && $master){
				 foreach($master as $mas){
						$i++;
			$totalLaundry=$totalLaundry+$mas->grand_total;
			$discountLaundry=$discountLaundry+$mas->discount;
			
			  $line_item_info=$this->unit_class_model->get_line_item_info($mas->laundry_id);
			  
			?>
<tr>
<td style="text-align:left"><?php echo $mas->laundry_id ?></td>
			     <td align="center"><?php echo date("g:ia dS M Y",strtotime($mas->dateAdded));  ?></td>
			     <td style="text-align:left"><?php echo  $line_item_info->qty; ?></td>
			      <td align="center"><?php echo $mas->total_item ?></td>
				  <td align="center"><?php echo 'n/a'; ?></td>
			       <td align="center"><?php echo $mas->grand_total+$mas->discount ?></td>
			          <td align="center"><?php echo '0.00%'; ?></td>
					  <td align="center"><?php echo $mas->discount ?></td>
			         
			            <td align="right"><?php echo $mas->grand_total; ?></td>

</tr>
					<?php 
					$charge_sum=$charge_sum+$mas->grand_total;
					
					}} ?>

		</tbody>

	</table>

	<hr style="background: #EDEDED; border: none; height: 1px;">



	<?php } 

	  }?>

	<?php 



       if($group_id =="0"){ $total_for_one_booking= $total_for_one_booking+$row2->room_rent_sum_total+$service_sum+$charge_sum;}else{$total_for_one_booking= $total_for_one_booking+$service_sum+$charge_sum;}  }} ?>

	<table width="100%">

		<tr>

			<td>&nbsp;</td>

		</tr>

	</table>

	<table width="100%" class="td-pad td-fon-size">

		<tbody>

			<?php 

			$tot_amt=0;

					//$food_price=0;

			if($group_id =="0"){ 

			?>

			<tr>
				
				<?php
					if(isset($ex_per) && $ex_per != ""){ 
					
					} else {
						echo '<td width="25%" style="text-align:right; font-weight:bold;"></td>';
						echo '<td width="25%" style="text-align:right; font-weight:bold;"></td>';
					}
				?>
				
				<td width="25%" style="text-align:right; font-weight:bold;">Room Rent</td>

				<td width="25%" style="text-align:right">

				<?php 
				
				echo $price;                 

                ?>

				</td>

			<?php 

                  //$row->food_plan_tax;

                if(isset($ex_per) && $ex_per != ""){ ?>


					<td width="25%" style="text-align:right; "> Extra person Room Rent </td>

					<td width="25%" style="text-align:right">
						<?php
							echo number_format( ( float )$ex_per, 2, '.', '' );
						?>
					</td>
			</tr>

			<?php

				} else {
					$ex_per = 0;
				}


			if(isset($rm_ch['r_id'])){

			unset($rm_ch['r_id']);
			unset($rm_ch['planName']);
			unset($rm_ch['total']);
			echo '<tr>';
			foreach($rm_ch as $key =>$v){
				
			  if(isset($price) && $price > 0)
				$taxRRper = number_format(($v/($price+$ex_per)*100), 2, '.', '');
			  else
				$taxRRper = 0;
			
			  if($taxRRper != 1.25 && $taxRRper != 2.50 && $taxRRper != 6.00 && $taxRRper != 9.00 && $taxRRper != 12.00 && $taxRRper != 14.00 && $taxRRper != 18.00 && $taxRRper != 28.00){
				  $taxRRper = ''; 
			  } else {
				  $taxRRper = ' @'.$taxRRper.'% ';
			  }

			  echo '
				<td width="25%" style="text-align:right; ">RR '.$key.$taxRRper.' </td>
				<td width="25%" style="text-align:right">'.number_format((float)($v), 2, '.', '').'</td>';

			}
			echo '</tr>';
		}

		if(isset($food_price) && $food_price !=""){
            $food_price = number_format((float)$food_price, 2, '.', '');
        } else {
            $food_price = "0.00";
		}

		if(isset($food_tax) && $food_tax !=""){

                     $food_tax = number_format((float)$food_tax, 2, '.', '');

                }

                else{

                     $food_tax = "0.00";

                }	


	 if($fd_plan == '1'){ ?>

			<tr>
				
				<?php
					if(isset($ex_per_m) && $ex_per_m != ""){ 
					
					} else {
						echo '<td width="25%" style="text-align:right; font-weight:bold;"></td>';
						echo '<td width="25%" style="text-align:right; font-weight:bold;"></td>';
					}
				?>
				
				<td width="25%" style="text-align:right; font-weight:bold;">Meal Plan</td>

				<td width="25%" style="text-align:right">
					<?php 
						if(isset($food_price) && $food_price !=""){
							 echo $food_price = number_format((float)$all_ch['meal_plan_chrg'], 2, '.', '');
						} else {
							 echo $food_price = "0.00";
						}
				    ?>
				</td>

			</tr>



			<?php 

                  //$row->food_plan_tax;

                if(isset($ex_per_m) && $ex_per_m !=""){

					?>



			<tr>

				<td style="text-align:right; ">Extra person Meal Plan</td>

				<td style="text-align:right">

					<?php

					echo number_format( ( float )$ex_per_m, 2, '.', '' );

					?>

				</td>

			</tr>

			<?php

			} else {
				$ex_per_m = 0;
			}

			
	 if(isset($mp_ch['r_id'])){

		unset($mp_ch['r_id']);
		unset($mp_ch['planName']);
		unset($mp_ch['total']);
		echo '<tr>';
		foreach($mp_ch as $key =>$v){
			
		if(isset($all_ch['meal_plan_chrg']) && $all_ch['meal_plan_chrg'] > 0)
			$taxMPper = number_format(($v/($all_ch['meal_plan_chrg']+$ex_per_m)*100),2);
		else
			$taxMPper = 0;
		
		if($taxMPper != 1.25 && $taxMPper != 2.50 && $taxMPper != 6.00 && $taxMPper != 9.00 && $taxMPper != 12.00 && $taxMPper != 14.00 && $taxMPper != 18.00 && $taxMPper != 28.00){
		  $taxMPper = ''; 
		} else {
		  $taxMPper = ' @'.$taxMPper.'% '; 
		}
		

		echo '<td width="25%" style="text-align:right; ">MP '.$key.$taxMPper.'</td>
		<td width="25%" style="text-align:right">'.number_format((float)($v), 2, '.', '').'</td>'; 
	  
	 }
		echo '</tr>';
	 }

  
    if(isset($ex_per) && $ex_per != ""){

           
			}

        if(isset($ex_per_m) && $ex_per_m !=""){

				
			}

	}
	
	?>
			<tr>
				<td></td><td></td>	
				<td width="25%" style="text-align:right; "><strong>Total Room Charges:</strong>

				</td>

				<td width="25%" style="text-align:right">

					<?php



					if ( isset( $tot_mptax ) && $tot_mptax > 0 ) {

						$tot_amt = $price + $tot_rtax;



						//  echo number_format((float)($room_rent_sum_total+$food_price+$food_tax1+$total_ex_m+$total_ex), 2, '.', '');

						echo '<strong>' . number_format( ( float )( $room_rent_sum_total + $food_price + $tot_rtax + $tot_mptax + $ex_per_m + $ex_per ), 2, '.', '' ) . '</strong>';

					} else {

						echo '<strong>' . number_format( ( float )( $room_rent_sum_total + $tot_rtax + $ex_per ), 2, '.', '' ) . '</strong>';

					}

					?>

				</td>
			</tr>

			<?php } ?>



			<?php if($service == '1' && $service_sum > 0){ ?>

			<tr>
				<td width="25%" style="text-align:right; ">Service Amount:</td>

				<td width="25%" style="text-align:right">

					<?php echo number_format((float)$service_sum, 2, '.', '');

                  

                ?>

				</td>

			

			<?php } else {
				echo '<tr><td></td><td></td>';
			} ?>



			<?php if($extra_charge == '1' && $charge_sum > 0){ ?>

				<td width="25%" style="text-align:right; ">Charge Amount:</td>

				<td width="25%" style="text-align:right">

					<?php echo number_format($charge_sum, 2); ?>

				</td>

			</tr>

			<?php } else { echo '</tr>'; }?>

			<tr>
				<td></td><td></td>	
				<td width="25%" style="text-align:right; "><span style="font-size:13px; font-weight:600;">Reservation Total:</span>

				</td>

				<td width="25%" style="text-align:right">

					<span style="font-size:13px; font-weight:600;">

			<?php 
					if(isset($food_tax)){
					//echo 'INR '.number_format(($g_tot=$tot_amt+$food_price+$food_tax1+$charge_sum+$service_sum+$total_ex_m+$total_ex), 2);
						echo 'INR '.number_format(($g_tot = $room_rent_sum_total + $food_price+$tot_rtax+$tot_mptax+$ex_per_m+$ex_per+ $charge_sum +$service_sum) - $d, 2);

					} else {

						echo 'INR '.number_format(($g_tot=$tot_amt+$charge_sum+$service_sum)-$d, 2);

					}

            ?>

					</span>

				</td>

			</tr>

		</tbody>

	</table>



	<?php  if(isset($all_adjustment)) {?>

	<span style="font-size:13px; font-weight:700; color:#D75C58; padding:5px 5px; display:block;"> Adjustment Details </span>
	
	<hr style="background: #D75C58; border: none; height: 1px;">

	<table width="100%" class="td-pad">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th width="5%" align="left"> # </th>

				<th width="20%" align="left"> Date </th>

				<th width="25%"> Reason </th>

				<th width="15%"> Amount</th>

				<th width="35%"> Note </th>



			</tr>

		</thead>

		<tbody style="background: #fafafa">

			<?php $i=1;

     $m=0;

		//print_r($all_adjustment);

           foreach($all_adjustment as $adjst){

			  $m++; 

			 ?>

			<tr>

				<td style="text-align:left">

					<?php  echo $i; ?>

				</td>

				<td style="text-align:left">

					<?php echo date("g:ia dS M Y",strtotime($adjst->addedOn)); ?>

				</td>

				<td style="text-align:center">

					<?php echo $adjst->reason ;?>

				</td>

				<td style="text-align:center">

					<?php echo $adjst->amount; ?>

				</td>

				<td style="text-align:center">

					<?php echo $adjst->note; ?>

				</td>

			</tr>



			<tr>

				<td colspan="7">

					<hr style="background: #EEEEEE; border: none; height: 1px; ">

				</td>

			</tr>

			<?php $i++; }?>

		</tbody>

	</table>

	<?php }?>

	<?php
	$sum_sgst = 0; $sum_igst = 0; $sum_cgst = 0;
	$total_bill_amount = 0;

	$total_tax_amount = 0;
    $sum=0;
	$sum_pos = 0;
		if($splitBill==0){
	if ($pos && isset( $pos ) ) {

		?>

	<span style="font-size:13px; font-weight:700; color:#5AA650; padding:5px 5px; display:block;"> POS Details </span>

	<hr style="background: #5AA650; border: none; height: 1px;">

	<table width="100%" class="td-pad">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th width="5%" align="center" valign="middle"> Ref No </th>

				<th width="10%" align="center" valign="middle"> Date </th>
				<?php if($fd_app==1) { // Invoice Settings ?> 
					<th width="30%" align="left" valign="middle"> Items Details </th>
				<?php }?>
				<th  align="center" valign="middle"> Amount </th>

				<th  align="center" valign="middle"> CGST </th>
				<th  align="center" valign="middle"> SGST </th>
				<th  align="center" valign="middle"> IGST </th>

				<th  align="center" valign="middle"> Discount </th>

				<th  align="center" valign="middle"> Total </th>

				<th  align="right" valign="middle"> Due </th>

			</tr>

		</thead>

		<tbody style="background: #fafafa">

			<?php $sum=0; $sum1=0; $pos_taxt = 0; 
			
			foreach ($pos as $key) {
			 
			    	if($chk->paid_pos_item_show =='1')	{	

			 if($key->total_due){
						
						$pos_tax = 0;
                         $pos_dis =$key->discount ; 
					
						$pos_sgst = $key->food_sgst + $key->liquor_sgst; 
						$pos_tax = $pos_tax + $pos_sgst;
						
						$pos_cgst = $key->food_cgst + $key->liquor_cgst; 
						$pos_tax = $pos_tax + $pos_cgst;
						
						$pos_igst = $key->food_igst + $key->liquor_igst; 
						$pos_tax = $pos_tax + $pos_igst;
						
						$pos_taxt = $pos_tax + $pos_taxt;
						$sum_sgst=$sum_sgst+$pos_sgst;
						$sum_cgst=$sum_cgst+$pos_cgst;
						$sum_igst=$sum_igst+$pos_igst;

             ?>

			<tr>

				<td align="center" valign="middle">

					<?php echo $key->invoice_number; ?>

				</td>

				<td align="center" valign="middle">

					<?php echo date("g:ia dS M Y",strtotime($key->date)); ?>

				</td>
				
				<?php if($fd_app==1) {?>
				<td align="left" valign="middle" class="hidden-480">
					<?php 
                            $items=$this->dashboard_model->all_pos_items($key->pos_id);
								
                            foreach ($items as $item ) {
                                echo $item->item_name." (".intval($item->item_quantity)."), ";
                            }
                    ?>
				</td>
				<?php }?>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php 

					//	$u_amt = $key->total_amount-$pos_tax+$d; 
				//		$u_amt = $key->total_amount; 

						echo number_format($u_amt, 2, '.','');

					?>

				</td>

				<td align="center" valign="middle" class="hidden-480">

					<?php echo $pos_sgst; ?>

				</td>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php echo $pos_cgst; ?>

				</td>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php echo $pos_igst; ?>

				</td>

				<td align="center" valign="middle" class="hidden-480">

					<?php //echo $d; 
				echo	$key->discount
					
					?>

				</td>

				<td align="center" valign="middle">

					<?php echo $key->total_amount; ?>

				</td>

				<td align="right" valign="middle">

					<?php echo $key->total_due; ?>

				</td>

			</tr>
			

			<?php $sum_pos=$sum_pos+$key->total_due;

                   $sum1=$sum1+$key->total_amount; 

					$total_bill_amount = $total_bill_amount + $key->bill_amount ;

					

					$total_tax_amount = $total_tax_amount + $key->tax ;

			 }
			    	}else{
			    	    
			    	    
			    	    			$pos_tax = 0;
                       
						$pos_sgst = $key->food_sgst + $key->liquor_sgst; 
						$pos_tax = $pos_tax + $pos_sgst;
						
						$pos_cgst = $key->food_cgst + $key->liquor_cgst; 
						$pos_tax = $pos_tax + $pos_cgst;
						
						$pos_igst = $key->food_igst + $key->liquor_igst; 
						$pos_tax = $pos_tax + $pos_igst;
						
						$pos_taxt = $pos_tax + $pos_taxt;
						$sum_sgst=$sum_sgst+$pos_sgst;
						$sum_cgst=$sum_cgst+$pos_cgst;
						$sum_igst=$sum_igst+$pos_igst;

             ?>

			<tr>

				<td align="center" valign="middle">

					<?php echo $key->invoice_number; ?>

				</td>

				<td align="center" valign="middle">

					<?php echo date("g:ia dS M Y",strtotime($key->date)); ?>

				</td>
				
				<?php if($fd_app==1) {?>
				<td align="left" valign="middle" class="hidden-480">
					<?php 
                            $items=$this->dashboard_model->all_pos_items($key->pos_id);
								
                            foreach ($items as $item ) {
                                echo $item->item_name." (".intval($item->item_quantity)."), ";
                            }
                    ?>
				</td>
				<?php }?>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php 

						$u_amt = $key->total_amount-$pos_tax+$d; 

						echo number_format($u_amt, 2, '.','');

					?>

				</td>

				<td align="center" valign="middle" class="hidden-480">

					<?php echo $pos_sgst; ?>

				</td>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php echo $pos_cgst; ?>

				</td>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php echo $pos_igst; ?>

				</td>

				<td align="center" valign="middle" class="hidden-480">

					<?php echo $d; ?>

				</td>

				<td align="center" valign="middle">

					<?php echo $key->total_amount; ?>

				</td>

				<td align="right" valign="middle">

					<?php echo $key->total_due; ?>

				</td>

			</tr>
			

			<?php $sum_pos=$sum_pos+$key->total_due;

                   $sum1=$sum1+$key->total_amount; 

					$total_bill_amount = $total_bill_amount + $key->bill_amount ;

					

					$total_tax_amount = $total_tax_amount + $key->tax ;
			    	    
			    	}

				   } ?>
	 </tbody>

	</table>
	<table class="td-pad">
	    <tbody>

			<tr>

				<td colspan="2">

					<hr style="background: #EEEEEE; border: none; height: 1px; ">

				</td>

			</tr>

			<tr>

				<td  style="text-align:right; width:75%">Total POS Bill Amount:</td>

				<td  style="text-align:right">

					<?php 

                                

                           echo number_format(($total_bill_amount - $pos_taxt), 2, '.','');

                              

                        

                        ?>

				</td>

			</tr>
			
			<?php if($pos_seg==1) {?>
			<tr>
			<td align="right" valign="middle" width="76%">POS SGST:</td>
			<td align="right" valign="middle">

				<?php echo number_format($sum_sgst,2); ?>
				
			</td>
			</tr>
			<tr>
			<td align="right" valign="middle" width="76%">POS CGST:</td>
			<td align="right" valign="middle">

				<?php echo number_format($sum_cgst,2); ?>
				
			</td>
			</tr>
			<tr>
				<td align="right" valign="middle" width="76%">POS IGST:</td>
				<td align="right" valign="middle">

					<?php echo number_format($sum_igst,2); ?>
					
				</td>
			</tr>
			<?php } ?>

			<tr>

				<td  style="text-align:right; ">Total POS Tax:</td>

				<td  style="text-align:right">

					<?php 

                            echo number_format($pos_taxt, 2, '.','');

                    ?>

				</td>

			</tr>
        
			<tr>

				<td  style="text-align:right; "><strong>Total POS Amount:</strong>

				</td>

				<td  style="text-align:right">

					<strong>

						<?php 

                                

                          // $sum_pos=$sum;

                            echo number_format($sum_pos, 2, '.','');  

                        

                        ?>

					</strong>

				</td>

			</tr>

		</tbody>

	</table>

	<hr style="background: #EEEEEE; border: none; height: 1px;">



		<?php }} ?>

	<?php 

  if($transaction_details){  ?>

	<span style="font-size:13px; font-weight:700; color:#D75C58; padding:5px 5px; display:block;"> Payment Details </span>

	<hr style="background: #D75C58; border: none; height: 1px;">

	<table width="100%" class="td-pad">

		<thead>

			<tr style="background: #EDEDED; color: #1b1b1b">

				<th width="5%" align="left"> # </th>

				<th width="5%" align="left"> Status </th>
				
				<th width="15%" align="center">Bk Status </th>

				<th width="20%"> Transaction Date </th>

				<th width="10%"> Profit Center </th>

				<th width="15%"> Payment Mode </th>

				<th width="15%" align="center"> Transaction Details </th>

				<th width="15%" align="right" style="padding-right:8px;"> Amount </th>

			</tr>

		</thead>

		<tbody style="background: #fafafa">

			<?php

			$i = 1;

			$sum = 0;

			foreach ( $transaction_details as $row1 ) {

				?>

			<tr>

				<td style="text-align:left">

					<?php echo $i; ?>

				</td>

				<td style="text-align:left">

					<?php

					if ( $row1->t_status == 'Cancel' )

						echo '<span style="color:red;">Declined</span>';

					else if ( $row1->t_status == 'Pending' )

						echo '<span style="color:orange;">Processing</span>';

					else

						echo '<span style="color:green;">Recieved</span>';

					?>

				</td>
				<td style="text-align:center">
					<?php 
							
							if(isset($row1->t_booking_status) && $row1->t_booking_status!=NULL){
							
							if($row1->t_booking_status == 1){
								echo "Temporary Hold";
							}
							if($row1->t_booking_status == 2){
								echo "Advance";
							}
							if($row1->t_booking_status == 3){
								echo "Pending";
							}
							if($row1->t_booking_status == 4){
								echo "Confirmed";
							}
							if($row1->t_booking_status == 5){
								echo "Checked In";
							}
							if($row1->t_booking_status == 6){
								echo "Checked Out";
							}
							if($row1->t_booking_status == 7){
								echo "Cancelled";
							}
							if($row1->t_booking_status == 8){
								echo "Incomplete";
							}
							}
					
					?>
				</td>

				<td style="text-align:center">

					<?php echo date("g:ia dS M Y",strtotime($row1->t_date)); ?>

				</td>

				<td style="text-align:center">

					<?php echo $row1->p_center; ?>

				</td>

				<td style="text-align:center">

					<?php echo $row1->t_payment_mode; ?>

				</td>

				<td style="text-align:center">

					<?php echo $row1->t_bank_name ;?>

				</td>

				<td style="text-align:right">

					<?php echo $row1->t_amount ;?>

				</td>

			</tr>

			<?php $i++; 

			if($row1->t_status == 'Done'){

			$sum=$sum + $row1->t_amount;

            }} ?>

			<tr>

				<td colspan="7">

					<hr style="background: #EEEEEE; border: none; height: 1px; ">

				</td>

			</tr>

			<tr>

				<td colspan="7" style="text-align:right; padding-right:17%;"><strong>Total Payment Completed:</strong>

				</td>

				<td align="right">

					<strong>

						<?php 

						if(isset($sum) && $sum) {

						echo 'INR '.$sum;

						} 

						else {

						$paid=0;

						echo "Yet to pay";

						}

						?>

					</strong>

				</td>

			</tr>

		</tbody>

	</table>





	<?php

	}

	?>



	<hr style="background: #EEEEEE; border: none; height: 1px;">



	<table class="td-pad td-fon-size" width="100%">


		<?php if($charge_sum != 0 && 1 == 2){ //not showing as shown earlier ?>
		<tr>

			<td align="right" width="76%" valign="middle">Ex Chrage Amount:</td>

			<td align="right" valign="middle">

				<?php 

				echo number_format(($charge_sum), 2, '.','');

				?>

			</td>

		</tr>
<?php 
		}
	if($splitBill==0){	
if ($pos && isset( $pos ) ) {
if($pos_settings == 1) {?>
		<tr>

			<td align="right" valign="middle" width="76%">POS Amount:</td>

			<td align="right" valign="middle">

				<?php  

					if(isset($sum)){

						//$sum=0;

					}else{

						$sum=0;

					}

					if(isset($m)){

						//$sum=0;

					}else{

						$m=0;

					}

				  $total_bill_amt=$total_bill_amount+$total_tax_amount;

				  echo number_format(($total_bill_amt), 2, '.',''); 

			  ?>

			</td>

		</tr>
<?php }
}} 

if($disc_app==1 && $d != 0) {
?>


		<tr>
			<td align="right" valign="middle" width="76%">Discount:</td>
			<td align="right" valign="middle">

				<?php echo number_format($d,2); ?>
				<?php echo '('.$co.')'; ?>
			</td>
		</tr>
<?php } 

	if($adjust_app==1 && $m != 0) {?>
		<tr>

			<td align="right" valign="middle" width="76%">Adjustment Amount:</td>

			<td align="right" valign="middle">

				<?php echo number_format($adjustment,2); ?>

				<?php echo '('.$m.')'; ?></td>

		</tr>
<?php } 

	

?>

		<tr>

			<td align="right" valign="middle"><strong style="font-size:13px; font-weight:600; color:#8876A8;">Total Payable Amount (Grand Total):</strong>

			</td>

			<td align="right" valign="middle" style="font-size:13px; font-weight:700; color:#33A076;">

				<strong>

					<?php 
					
					
					
						echo 'INR '.number_format(($net_pay=($g_tot+$sum_pos+$adjustment)-$d),2);
						
					?>

				</strong>

			</td>

		</tr>
		<tr>
			<td></td>
			<td align="right">
				<em style="text-transform: capitalize;"><?php 
				$net_payWords = $this->dashboard_model->getIndianCurrency($net_pay);
				if(isset($net_payWords)){
					echo ' </br>'.$net_payWords.' only';
				}
				 ?> </em>
			</td>
		</tr>
		
		<tr>

			<td align="right" valign="middle" width="76%">Total Paid amount:</td>

			<td align="right" valign="middle">

				<?php 
				if($sum != 0)
					echo number_format($sum,2); 
				else
					echo 'none';
				?>

			</td>

		</tr>

		<tr>

			<td align="right" valign="middle"><span style="font-size:13px; font-weight:600; color:#8876A8;">Total Due:</span>

			</td>

			<td align="right" valign="middle">

				<?php 

					$td = $net_pay-$sum;

					$paid_c = number_format($td, 2);

					if($paid_c != 0){ 
				?>

				<span style="font-size:13px; font-weight:700; color:#F65656;">INR <?php echo $paid_c;?></span>

				<?php

				} else {

				?>

				<span style="font-size:13px; font-weight:700; color:#7FBA67;">Full Paid</span>

				<?php }?>

			</td>

		</tr>

	</table>

	<?php 
		if($authority_sign == '1'){ 
	?>

	<table width="100%">

		<tr>

			<td style="padding:20px 0 0;" colspan="2"></td>

		</tr>

		<tr>
			<td valign="bottom">
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;">996311 - ACCOMODATION IN HOTEL/INN/GUEST HOUSE/ CLUB OR CAMP SITE ETC.SERVICE</span>
				</br>
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;">996411 - TRANSPORT</span>
				</br>
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;">996411 - PROGRAM,BUSINESS AUXILIARY SERVICE, SPA, DECORATION</span>
				</br>
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;">9963 - RESTAURANT SERVICE</span>
			</td>

			<td align="center" width="35%">______________________________<br/> Authorized Signature </td>
		</tr>

		<tr>
			<td width="65%">
				<?php if($chk->declaration!='') {?>
				<span style="font-size:9px; font-weight:400; color:#939292; display:block;"><?php echo $chk->declaration;?></span>
                <?php }?>
			</td>

			<td align="center" width="35%">
				<?php   // Showing the Hotel Name from Session

					$hotel_n = $this->dashboard_model->get_hotel_name($this->session->userdata('user_hotel'));

					if(isset($hotel_n->hotel_name) && $hotel_n->hotel_name)

						echo 'For, '.$hotel_n->hotel_name;

				?>
			</td>
		</tr>
	</table>

	<?php } ?>
	<hr style="background: #EEEEEE; border: none; height: 1px; margin-top:10px; width:100%;">

	<table width="100%" class="td-pad">
		<tr>
			<td align="center" style="font-size:11px;"><?php if($chk->ft_text!=""){ echo $chk->ft_text; }?></td>
		</tr>
		<tr>
			<td align="center">This is a Computer Generated Invoice & should be treated as signed by an authorized signatory</td>
		</tr>
	</table>
	<hr style="background: #00C5CD; border: none; height: 1px; margin-top:10px; width:100%;">
	<span style="font-size:9px; font-weight:400; color:#939292; display:block; text-align: center;"> Powered by Royalpms. Thanks for your stay & kindly visit <?php echo $hotel_name->hotel_name; ?> again! </span>
	
</div>