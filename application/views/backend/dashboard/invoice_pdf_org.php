<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Verdana, Geneva, sans-serif;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td {
	font-size: 12px;
}
.td-fon-size td {
	font-size: 14px;
}
</style>
<div style="padding:15px 35px;">
  <?php
 
/*$chars = '0123456789';$r='';
for($i=0;$i<7;$i++)
{
    $r.=$chars[rand(0,strlen($chars)-1)];
}
$randomstring = $r."  ";*/
$chk=$this->bookings_model->get_invoice_settings();
//echo "<pre>";
//print_r($chk);
if(isset($chk->booking_source_inv_applicable)){
    $booking_source = $chk->booking_source_inv_applicable;
} else {
	$booking_source ="";
}
if(isset($chk->nature_visit_inv_applicable)){
$nature_visit = $chk->nature_visit_inv_applicable;
} else {
	$nature_visit ="";
}
if(isset($chk->booking_note_inv_applicable)){
$booking_note = $chk->booking_note_inv_applicable;
} else {
	$booking_note ="";
}
if(isset($chk->company_details_inv_applicable)){
$company_details = $chk->company_details_inv_applicable;
} else {
	$company_details ="";
}
if(isset($chk->service_tax_applicable)){
    $service_tax = $chk->service_tax_applicable;
} else {
	$service_tax =0;
}
if(isset($chk->service_charg_applicable)){
    $service_charg = $chk->service_charg_applicable;
} else {
	$service_charg = 0;
}
if(isset($chk->authorized_signatory_applicable)){
    $authority_sign = $chk->authorized_signatory_applicable	;
} else {
	$authority_sign = 0;
}
if(isset($chk->unit_no)){
    $unit_no = $chk->unit_no;
} else {
	$unit_no = 0;
}
if(isset($chk->unit_cat)){
    $unit_cat = $chk->unit_cat;
} else {
	$unit_cat = 0;
}
if(isset($chk->luxury_tax_applicable)){
    $luxury_tax = $chk->luxury_tax_applicable;
} else {
	$luxury_tax =0;
}
if(isset($chk->invoice_setting_food_paln)){
    $fd_plan = $chk->invoice_setting_food_paln;
} else {
	$fd_plan =0;
}

if(isset($chk->invoice_setting_service)){
    $service = $chk->invoice_setting_service;
} else {
	$service =0;
}

if(isset($chk->invoice_setting_extra_charge)){
    $extra_charge = $chk->invoice_setting_extra_charge;
} else {
	$extra_charge =0;
}

if(isset($chk->invoice_setting_laundry)){
    $laundry = $chk->invoice_setting_laundry;
} else {
	$laundry =0;
}

if(isset($chk->invoice_pref) && isset($chk->invoice_suf)){
	$suf=$chk->invoice_suf;
    $pref=$chk->invoice_pref;
} else {
	$suf="";
   $pref="";	
}

$flag=0;
//print_r($chk);
?>
  <table width="100%">
    <tr>
      <td align="left" valign="middle"><img src="upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" /></td>
      <td align="right" valign="middle"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?></td>
    </tr>
    <tr>
      <td colspan="2"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr>
      <td colspan="2"><table width="100%">
          <tr>
            <?php 
    if(isset($tax)){
        foreach($tax as $tax_details){
    foreach($booking_details as $bookings){ 
     
        $bd_id=$bookings->booking_id;
        $invoice=$this->dashboard_model->get_invoice($bd_id);
    
    ?>
            <td width="80%" align="left" valign="top" style="font-size:12px;"><?php if($company_details == '1'){ ?>
              Services Tax Reg No.:
              <?php  echo $tax_details->service_tax_no;?>
              <br />
              Vat Reg No.:
              <?php  echo $tax_details->vat_reg_no;?>
              <br />
              Luxury Tax No.:
              <?php  echo $tax_details->luxury_tax_reg_no;?>
              <br />
              CIN No.:
              <?php  echo $tax_details->cin_no; } else { ?>
              &nbsp;
              <?php } ?></td>
            <td width="20%" align="left" valign="top" style="font-size:12px;">
			<span style="color:#009595; font-weight:bold; font-size:18px; line-height:20px; display:block; padding-bottom:5px;">INVOICE 
			<?php 
				if($bookings->group_id == 0)
					echo ' (Single Booking)';
				else
					echo ' <span style="color:#F8681A;">(Single under Group)</span>';
			?>
			</span>
            <?php 
            if($invoice && isset($invoice))
            {
                echo $pref.$invoice->invoice_no.$invoice->id.$suf;
            }?>
              <br />
              <?php
			  	date_default_timezone_set("Asia/Kolkata");
			  	 echo "DATE:&nbsp;",date("g:ia dS M Y") ?>                 
             </td>
            <?php }}}?>
          </tr>
          <tr>
            <td colspan="2" width="100%">&nbsp;</td>
          </tr>
          <tr>
           <td><?php 
	// _sb dt25_10_16 - Only 2 Foreach
	$flag = 0;
    foreach($booking_details as $row){ 
      $guest_det=$this->dashboard_model->get_guest_details($row->guest_id);
	  foreach($guest_det as $gst)
      {
		
		 if(($row->bill_to_com == '1') && ($gst->g_type == 'Corporate') && ($gst->corporate_id != NULL) && ($gst->corporate_id != 0)) // Check if Bill to Company is Possible
		  {
			   $corporate_details=$this->dashboard_model->fetch_c_id1($gst->corporate_id); 
			   ?>
	   
		  <ul class="list-unstyled" style="font-size:12px;">
            <li> 
				<strong style="color:#6B67A1; text-size:12px;"> 
				<?php
					if($corporate_details->legal_name)
						echo $corporate_details->legal_name;
					else
						echo $corporate_details->name;
				?> 
				</strong>
			</li>        
            <li>
				<?php 
                    $fg = 10;
                    if(isset($corporate_details->address) && ($corporate_details->address != '' && $corporate_details->address != ' ')){
                                            echo ($corporate_details->address);
                                            $fg = 0	;
                                            //echo ', ';
                                        }
                    if(isset($corporate_details->city) && ($corporate_details->city != '' && $corporate_details->city != ' ')){
                                            if($fg == 0){
                                                echo ', ';
                                                
                                            }
                                                
                                            echo $corporate_details->city;
                                            echo '<br>';
                                            $fg = 1;
                                        }
                    else if($fg == 0)
                                            $fg = 5;
                    if(isset($corporate_details->state) && ($corporate_details->state != '' && $corporate_details->state != ' ')){
                                            if($fg == 5)
                                                echo '<br>';
                                            echo $corporate_details->state;
                                            $fg = 2;
                                            //echo ', ';
                                        }
                    if(isset($corporate_details->pin_code) && $corporate_details->pin_code!= NULL){
                                            if($fg == 2 || $fg == 5){
                                                echo ', ';
                                            }									
                                            echo 'PIN - '.$corporate_details->pin_code;
                                            $fg = 3;
                                        }
                    if(isset($corporate_details->country) && $corporate_details->country != '' && $corporate_details->country != ' '){
                                            if($fg == 3)
                                                echo ', ';
                                            echo $corporate_details->country;
                                            /*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';
                                            $fg = 4;*/
                                        }
                    if($fg == 10){
                                            echo 'No Info';
                                        }
                ?>
				 
			</li>
            <li> <?php echo '<strong>Phone: </strong>'.$corporate_details->g_contact_no; ?> </li>
            <li> <?php echo '<strong>Email: </strong>'.$corporate_details->corp_email; ?> 
			</li>
			<li> <?php echo '<br><strong style="color:#06B4F5">Guest Name: </strong>';
			echo $gst->g_name;
			if($gst->c_des)
				echo ' - '.$gst->c_des;
			?> </li>			
          </ul>
			<?php
			}  // End IF 
			
			else{
			?>
			
			<ul class="list-unstyled" style="font-size:12px;">
                <li>
					<strong style="color:#6B67A1; text-size:12px;"> 
					<?php echo $gst->g_name; ?> </strong>
				</li>
                <!--<li> <?php echo $gst->corporate_id; ?>  </li>-->                
				<li>
                  <?php 
                    $fg = 10;
                    if(isset($gst->g_address) && ($gst->g_address != '' && $gst->g_address != ' ')){
                                            echo ($gst->g_address);
                                            $fg = 0	;
                                            //echo ', ';
                                        }
                    if(isset($gst->g_city) && ($gst->g_city != '' && $gst->g_city != ' ')){
                                            if($fg == 0){
                                                echo ', ';
                                                
                                            }
                                                
                                            echo $gst->g_city;
                                            echo '<br>';
                                            $fg = 1;
                                        }
                    else if($fg == 0)
                                            $fg = 5;
                    if(isset($gst->g_state) && ($gst->g_state != '' && $gst->g_state != ' ')){
                                            if($fg == 5)
                                                echo '<br>';
                                            echo $gst->g_state;
                                            $fg = 2;
                                            //echo ', ';
                                        }
                    if(isset($gst->g_pincode) && $gst->g_pincode!= NULL){
                                            if($fg == 2 || $fg == 5){
                                                echo ', ';
                                            }									
                                            echo 'PIN - '.$gst->g_pincode;
                                            $fg = 3;
                                        }
                    if(isset($gst->g_country) && $gst->g_country != '' && $gst->g_country != ' '){
                                            if($fg == 3)
                                                echo ', ';
                                            echo $gst->g_country;
                                            /*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';
                                            $fg = 4;*/
                                        }
                    if($fg == 10){
                                            echo 'No Info';
                                        }
                ?>
                </li>
                <!--<li><?php echo $gst->g_place; ?> </li>
				<li><?php echo $gst->g_city; ?> </li>
				<li> <?php if($gst->g_pincode) echo "PIN:".$gst->g_pincode; ?> </li>
				<li><?php echo $gst->g_state." - ".$gst->g_country; ?> </li>-->
                <li><strong>Phone: </strong><?php echo $gst->g_contact_no; ?> </li>
                <li><strong>Email: </strong><?php echo $gst->g_email; ?> </li>
            </ul>
		  <?php } // End Else
		  } // End Foreach
	}
		  ?>
              <ul class="list-unstyled" style="font-size:12px;">
                <li>&nbsp;</li>
                <?php
           if($booking_source == '1'){		   
           foreach($payment_details as $row2){ ?>
                <li><strong>Booking Source: </strong>
                  <?php  echo $row2->booking_source; ?>
                </li>
	  <?php } }?>
                <?php
           if($nature_visit == '1'){		   
           foreach($payment_details as $row2){ ?>
                <?php if(isset($row2->nature_visit) && $row2->nature_visit != ""){?>
                <li><strong>Nature of Visit: </strong>
                  <?php  echo $row2->nature_visit; ?>
                </li>
                <?php } ?>
                <?php } }?>
              </ul></td>
            <td align="left" valign="top"><?php
            foreach($total_payment as $row3){
                $total_amount = $row3->t_amount;
                ?>
              <?php }
			   // ?end
			  
			  ?>
              <div class="well"> <strong><?php echo  $hotel_name->hotel_name; ?></strong><br/>
                <?php echo  $hotel_contact['hotel_street1']; ?> <?php echo  $hotel_contact['hotel_street2'].', '; ?><br>
                <?php echo  $hotel_contact['hotel_district']; ?> - <?php echo  $hotel_contact['hotel_pincode']; ?> <?php echo  $hotel_contact['hotel_state']; ?> - <?php echo  $hotel_contact['hotel_country']; ?><br/>
                <strong>Phone:</strong> <?php echo  $hotel_contact['hotel_frontdesk_mobile']; ?><br/>
                <strong>Email:</strong> <?php echo  $hotel_contact['hotel_frontdesk_email']; ?><br/>
                <br />
              </div></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <table width="100%">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <span style="font-size:16px; font-weight:700; color:#009595; display:block; padding:10px 5px;">Booking Details</span>
  <hr style="background: #009595; border: none; height: 1px;">
  <table width="100%" class="td-pad">
    <thead>
      <tr style="background: #EDEDED; color: #1b1b1b">
        <th align="left"> No </th>
        <th width="35%" align="left"> Product / Service Name </th>
        <th> Quantity </th>
        <th> Unit Price </th>
        <th> Modifier </th>
        <th> Amount </th>
        <th> Total Tax </th>
        <th align="right"> Total </th>
      </tr>
    </thead>
    <tbody style="background: #fafafa">
      <?php
           $total_for_one_booking=0;
           $sc=0;
           $st=0;
           $lc=0;
           $discount=0;
        foreach($total_payment as $row3){
            $total_amount = $row3->t_amount;
            $room_rent_sum_total=0;
            if(isset($payment_details)){
            ?>
      <?php foreach($payment_details as $row2)
                    { 
                     
                    ?>
      <tr>
        <?php if($group_id =="0"){	
    ?>
	<?php $ab=$row2->booking_id; 
	$d=0;
	$co=0;
	$disc1= $this->unit_class_model->get_discount_details_single($ab);
	if(isset($disc1) && $disc1){
		foreach($disc1 as $ds){
			$co++;
			$d+=$ds->discount_amount;
	}}
	?>
	
        <td style="text-align:left"><?php echo $row2->booking_id; ?></td>
        <td style="text-align:left"><?php // Unit Details
                        $units=$this->dashboard_model->get_unit_exact($row2->unit_id);

                        foreach ($units as $key) {
                            # code...
                                $unit_name=$units->unit_name;
                        }
                // Logic for checking Invoice Settings, wheather to show Unit Details
				if($unit_cat == '1'){
					echo $unit_name;
					
				}
				else{
					if($unit_no != '1')
						echo 'Unit 1';
				}
					
				if($unit_no == '1'){
					echo " (Unit No - ".$row2->room_no.") ";
					
				}
				?>
          <br />
          <strong>CheckIn Date:</strong> <?php echo date('d M , Y',strtotime($row2->cust_from_date)); ?> at <?php echo date('h:i A',strtotime($row2->confirmed_checkin_time));?> <br />
          <strong>Checkout Date:</strong> <?php echo date('d M , Y',strtotime($row2->cust_end_date)); ?> at <?php echo date('h:i A',strtotime($row2->confirmed_checkout_time));?><br />
          <?php  if($booking_note == '1'){ echo $row2->comment; }?></td>
        <td style="text-align:center"><?php echo $row2->stay_days." (Days)"; ?></td>
        <td style="text-align:center"><?php //echo number_format(($row2->room_rent_total_amount/$row2->stay_days), 2, '.',''); 
		  ?>
          <?php
			if($row2->rent_mode_type == "add"){
				$fv = ($row2->room_rent_total_amount/$row2->stay_days) - $row2->mod_room_rent; 
			} else {
				$fv = ($row2->room_rent_total_amount/$row2->stay_days) + $row2->mod_room_rent;
			}
		    echo number_format($fv, 2, '.',''); 
		  ?></td>
        <td style="text-align:center"><?php if($row2->rent_mode_type == "add"){echo '(+)';} else { echo '(-)';} echo number_format($row2->mod_room_rent, 2, '.',''); ?></td>
        <?php }?>
        <td style="text-align:center"><?php   if($group_id =="0"){echo $price= $row2->room_rent_total_amount; } 
          else
          {
            $price=0;
          }
                $sc=$row2->service_charge;
                $st=$row2->service_tax;
                
                if($row2->room_rent_tax_details == 'No Tax')
                {
                    $sc=0;
                    $st=0;
                }
                
                
                
                
                
       $qry=$this->bookings_model->room_tax($this->session->userdata('user_hotel'));
       foreach($qry as $qr)
       {
        $lux_slab1=$qr->luxury_tax_slab1;
        $lux_slab2=$qr->luxury_tax_slab2;
        $lux_slab_range=$qr->luxury_tax_slab2_range_to;
       }
        $luxury_tax_per=0;
        /* if(($row2->room_rent_total_amount/$row2->stay_days) < $lux_slab_range)
        {
            $luxury_tax_per=$lux_slab1;
        }
        else
        {
            $luxury_tax_per=$lux_slab2;
        } _sb*/
        
        $luxury_tax_per = $row2->luxury_tax;
        
                $lc=$luxury_tax_per;
                
        if($row2->room_rent_tax_details == 'No Tax')
                $lc=0;
                
                
                $discount=$row2->discount;
          ?></td>
        <td style="text-align:center"><?php if($group_id =="0"){echo $row2->room_rent_tax_amount;} ?></td>
        <td style="text-align:right"><?php  if($group_id =="0"){
            $room_rent_sum_total= $row2->room_rent_total_amount + $row2->room_rent_tax_amount;
			echo number_format($room_rent_sum_total, 2, '.','');
            }
              ?></td>
      </tr>
      <tr >
        <td align="left"><?php if($group_id == '0' )
          {echo "#";} ?></td>
        <td align="left">
			<?php 
				$data = $this->bookings_model->fetch_food_plan($row2->food_plans);
				if($data)
					echo '<strong>Meal Plan: </strong>'.$data->fp_name; 
			?>
		</td>
        <td align="center"><?php if(isset($data->fp_name)) echo ($row2->no_of_adult+$row2->no_of_child)."<br />[".$row2->no_of_adult."(A) + ".$row2->no_of_child."(C)]"; ?></td>
        <td align="center">
			<?php 
				  $data=$this->bookings_model->fetch_food_plan($row2->food_plans);
				  if(isset($data)) {
					  echo $data->fp_price_adult."(A)<br />".$data->fp_price_children."(C)";
					  }
						echo " ";
			?>
		</td>
		<td align="center">n/a</td>
        
        <td align="center">
			<?php 
				if($row2->food_plan_price!=0) 
						echo $food_price=$row2->food_plan_price; 
				else
						echo '0';
			?>
		</td>
        <td align="center"><?php 
		  $food_tax5=0;
		  $data=$this->bookings_model->fetch_food_plan($row2->food_plans);
		  if($data){ 
			if($data->fp_tax_percentage!=0){
			  $food_tax5 =($data->fp_tax_percentage/100)*$row2->food_plan_price;
			  echo number_format($food_tax5, 2, '.','');
			}
			else
				echo '0';
		  } ?>
          <?php
			/*$food_tax=0;		  
			$food_tax1=0;
			$food_tax2=0;
			$scP=0;		  
		  $data=$this->bookings_model->fetch_food_plan($row2->food_plans);
		  if($data){ 
		  if($data->fp_tax_percentage > 0){
			 $scP= 23 - $data->fp_tax_percentage;	
			 $food_tax1=($data->fp_tax_percentage/100)*$row2->food_plan_price;
			 $food_tax2=($scP/100)*$row2->food_plan_price;
			 $food_tax = $food_tax1 + $food_tax2;			  
			 echo number_format($food_tax);
		  } }*/?></td>
        <td align="right">
			<?php 
				if($row2->food_plan_price!=0){ 
				  $ft = $food_tax5 + $row2->food_plan_price; 
				  echo number_format($ft, 2, '.','');
				}
				else
					echo '0';
		   ?>
	    </td>
      </tr>
      
      </tbody>
  </table>
  <hr style="background: #EDEDED; border: none; height: 1px;">
  <table width="100%">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  
      
      <?php

                  $service_string=$row2->service_id;
                    $service_total_price=$row2->service_price;
                    $service_sum=0;
                    if($service_string!=""){
                ?>
   <span style="font-size:16px; font-weight:700; color:#6A4178; display:block; padding:10px 5px;">Extra Services</span>
  <hr style="background: #6A4178; border: none; height: 1px;">
  <table width="100%" class="td-pad">
    <thead>
      <tr style="background: #EDEDED; color: #1b1b1b">
        <th align="left"> No </th>
        <th width="35%" align="left"> Service Name </th>
        <th> Quantity </th>
        <th> Unit Price </th>
        <th> Amount </th>
        <th> Total Tax </th>
        <th align="right"> Total </th>
      </tr>
    </thead>
    <tbody style="background: #fafafa">
  
      <?php

                $counter = 0;
                $temp = 0;
                $service_array=explode(",", $service_string);

                //print_r($service_array); 

                for ($i=1; $i < sizeof($service_array) ; $i++) { 

                    $services=$this->dashboard_model->get_service_details1($service_array[$i]);

                    $occurance_strng = substr_count($row2->service_id,$service_array[$i]);
					
                    if ($occurance_strng > 1) {
                        $counter ++;
                        if ($counter == 1) {
                            ?>
      <tr>
        <td style="text-align:left"><?php echo $counter; ?></td>
        <td align="left"><?php echo $services->s_name; ?></td>
        <td style="text-align:center"><?php echo $occurance_strng; ?></td>
        <td style="text-align:center"><?php echo number_format($services->s_price, 2, '.',''); ?></td>
        <td style="text-align:center"><?php $service_total= ($services->s_price * $occurance_strng); echo number_format($service_total, 2, '.',''); ?></td>
        <td style="text-align:center"><?php echo $service_tax= ($services->s_tax ); ?></td>
        <td style="text-align:right"><?php $service_grand_total=$service_total+$service_tax; echo number_format($service_grand_total, 2, '.','');?></td>
      </tr>
      <?php
                        }
                    }
                    else{
					$counter ++;	
                   ?>
      <tr>
        <td style="text-align:left"><?php echo $counter; ?></td>
        <td align="left"><?php echo $services->s_name; ?></td>
        <td style="text-align:center"><?php echo $occurance_strng; ?></td>
        <td style="text-align:center"><?php echo number_format($services->s_price, 2, '.',''); ?></td>
        <td style="text-align:center"><?php $service_total= ($services->s_price * $occurance_strng); echo number_format($service_total, 2, '.','');?></td>
        <td style="text-align:center"><?php echo $service_tax= ($services->s_tax ); ?></td>
        <td style="text-align:right"><?php $service_grand_total=$service_total+$service_tax; echo number_format($service_grand_total, 2, '.','');?></td>
      </tr>
      
      <?php 
                        }
                        $service_sum=$service_sum+$service_grand_total; 
                    }
					?>
       </tbody>
  </table> 
  <hr style="background: #EDEDED; border: none; height: 1px;">
  <table width="100%">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>             
                    <?php
                } 
                ?>
        
  
  
       
      <?php

                  $charge_string=$row2->booking_extra_charge_id;
                    $charge_total_price=$row2->booking_extra_charge_amount;


                        $charge_sum=0;
            if($charge_string!=""){
                ?>
      <span style="font-size:16px; font-weight:700; color:#366B9B; display:block; padding:10px 5px;">Extra Charges</span>
  <hr style="background: #366B9B; border: none; height: 1px;">
  <table width="100%" class="td-pad">
    <thead>
      <tr style="background: #EDEDED; color: #1b1b1b">
        <th align="left"> No </th>
        <th align="center" width="10%"> Date </th>
        <th width="35%" align="left"> Service Name </th>
        <th> Quantity </th>
        <th> Unit Price </th>
        <th> Amount </th>
        <th> Total Tax </th>
        <th align="right"> Total </th>
      </tr>
    </thead>
    <tbody style="background: #fafafa">
      
      <?php

                $charge_array=explode(",", $charge_string);

                //print_r($service_array); 

                for ($i=1; $i < sizeof($charge_array) ; $i++) { 
                    $charges=$this->dashboard_model->get_charge_details($charge_array[$i]);

           ?>
      <tr>
        <td style="text-align:left"><?php echo $charges->crg_id; ?></td>
        <td align="center"><?php echo date("g:ia dS M Y",strtotime($charges->aded_on)); ?></td>
        <td width="33%" align="left"><?php echo $charges->crg_description; ?></td>
        <td style="text-align:center"><?php echo $charges->crg_quantity; ?></td>
        <td style="text-align:center"><?php echo $charges->crg_unit_price; ?></td>
        <td style="text-align:center"><?php echo $charge_total= ($charges->crg_unit_price * $charges->crg_quantity); ?></td>
        <td style="text-align:center"><?php echo $charge_tax= ($charges->crg_tax ); 
                    echo "%";?></td>
        <td style="text-align:right"><?php echo $charge_grand_total=$charges->crg_total; ?></td>
      </tr>
      <?php $charge_sum=$charge_sum+$charge_grand_total; 
	  }
	   ?>
      </tbody>
  </table>
  <hr style="background: #EDEDED; border: none; height: 1px;">
  <table width="100%">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
      <?php } 
	  }?>
      <?php 

       if($group_id =="0"){ $total_for_one_booking= $total_for_one_booking+$row2->room_rent_sum_total+$service_sum+$charge_sum;}else{$total_for_one_booking= $total_for_one_booking+$service_sum+$charge_sum;}  }} ?>
    
  <table width="100%" class="td-pad td-fon-size">
    <tbody>
      <?php 
        $tot_amt=0;
                //$food_price=0;
        if($group_id =="0"){ 
                
        ?>
      <tr>
        <td width="80%" style="text-align:right; ">Room Price:</td>
        <td  style="text-align:right" ><?php echo $price;
                  
                ?></td>
      </tr>
      <?php $foo1=$price * $st/100; 
		if($service_tax == '1'){ ?>
      <tr>
        <td style="text-align:right; ">Service Tax:</td>
        <td  style="text-align:right" ><?php
                  echo $foo1=number_format((float)($price * $st/100), 2, '.', '');
                ?></td>
      </tr>
      <?php }?>
      <?php $foo2 = $price * $sc/100;
        if($service_charg == '1'){ ?>
      <tr>
        <td style="text-align:right; ">Service Charge:</td>
        <td  style="text-align:right" ><?php $foo2 = $price * $sc/100;
                  echo number_format((float)$foo2, 2, '.', '');
                ?></td>
      </tr>
      <?php } ?>
      <?php $foo3 = $price * $lc/100; 
        if($luxury_tax == '1'){ ?>
      <tr>
        <td style="text-align:right; ">Luxury Tax:</td>
        <td  style="text-align:right" ><?php $foo3 = $price * $lc/100;
                  echo number_format((float)$foo3, 2, '.', '');
                ?></td>
      </tr>
      <?php } ?>
	  
     <?php 
	  if(isset($food_price) && $food_price !=""){
                  $food_price = number_format((float)$food_price, 2, '.', '');
            }
            else{
                  $food_price = "0.00";
			}
			
		if(isset($food_tax) && $food_tax !=""){
                     $food_tax = number_format((float)$food_tax, 2, '.', '');
                }
                else{
                     $food_tax = "0.00";
                }	
			
			
	 
	 if($fd_plan == '1'){ ?>
	  <tr>
        <td style="text-align:right; ">Food Plan Price:</td>
        <td  style="text-align:right" ><?php 
            if(isset($food_price) && $food_price !=""){
                 echo $food_price = number_format((float)$food_price, 2, '.', '');
            //echo $food_price;
            }
            else{
                        echo $food_price = "0.00";
                    }
               ?></td>
      </tr>
      <tr>
        <td style="text-align:right; ">Food Plan Tax:</td>
        <td  style="text-align:right" ><?php 
            //$food_tax=0;
                if(isset($food_tax) && $food_tax !=""){
                    echo $food_tax = number_format((float)$food_tax, 2, '.', '');
                }
                else{
                    echo $food_tax = "0.00";
                }
               ?></td>
      </tr>
	 <?php }?>
      <tr>
        <td style="text-align:right; ">Total Amount:</td>
        <td  style="text-align:right" ><?php
        
          if(isset($food_tax)){
                $tot_amt=$price+$foo1+$foo2+$foo3;
          echo number_format((float)($room_rent_sum_total+$food_price+$food_tax), 2, '.', '');
          }
            else{
                 echo number_format((float)($room_rent_sum_total), 2, '.', '');
            }
                ?></td>
      </tr>
      <?php } ?>
       <?php if($service == '1'){ ?>
	  <tr>
        <td style="text-align:right; ">Service Amount:</td>
        <td  style="text-align:right" ><?php echo number_format((float)$service_sum, 2, '.', '');
                  
                ?></td>
      </tr>
	   <?php }?>
	   <?php if($extra_charge == '1'){ ?>
	  <tr>
        <td style="text-align:right; ">Charge Amount:</td>
        <td  style="text-align:right" ><?php echo number_format($charge_sum, 2);
                  
                ?></td>
      </tr>
	   <?php }?>
	   
      <tr>
        <td style="text-align:right; "><span style="font-size:18px; font-weight:700; color:#8876A8;">Booking Grand  Total:</span></td>
        <td  style="text-align:right" ><span style="font-size:18px; font-weight:700; color:#F65656;">
            <?php  if(isset($food_tax)){
              echo number_format(($g_tot=$tot_amt+$food_price+$food_tax+$charge_sum+$service_sum), 2);
          }
            else{
                echo number_format(($g_tot=$tot_amt+$charge_sum+$service_sum), 2);
            }
                ?></span>
          </td>
      </tr>
    </tbody>
  </table>
  <table width="100%">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <?php
$total_bill_amount = 0 ;
$total_tax_amount = 0 ;
 $sum_pos=0; if($pos && isset($pos)){  ?>
  <span style="font-size:16px; font-weight:700; color:#5AA650; padding:10px 5px; display:block;"> POS Details </span>
  <hr style="background: #5AA650; border: none; height: 1px;">
  <table width="100%" class="td-pad">
    <thead>
      <tr style="background: #EDEDED; color: #1b1b1b">
        <th width="5%" align="center" valign="middle"> No </th>
        <th width="10%" align="center" valign="middle"> Date </th>
        <th width="30%" align="left" valign="middle"> Food Items </th>
        <th width="10%" align="center" valign="middle"> Amount </th>
        <th width="10%" align="center" valign="middle"> Tax </th>
        <th width="10%" align="center" valign="middle"> Discount </th>
        <th width="15%" align="center" valign="middle"> Total </th>
        <th width="10%" align="right" valign="middle"> Due </th>
      </tr>
    </thead>
    <tbody style="background: #fafafa">
      <?php $sum=0; $sum1=0; foreach ($pos as $key) {
                       
                        # code...
                     
                     ?>
      <tr>
        <td align="center" valign="middle"><?php echo $key->pos_id; ?></td>
        <td align="center" valign="middle"><?php echo date("g:ia dS M Y",strtotime($key->date)); ?></td>
        <td align="left" valign="middle" class="hidden-480"><?php 
                                $items=$this->dashboard_model->all_pos_items($key->pos_id);
                                foreach ($items as $item ) {
                                    echo "<span style='display:block;'>".$item->item_name." (".$item->item_quantity.") "."</span>";
                                }
                            ?></td>
        <td align="center" valign="middle" class="hidden-480"><?php 
		$u_amt=$key->total_amount-$key->tax+$d; 
		 echo number_format($u_amt, 2, '.','');
		?></td>
        <td align="center" valign="middle" class="hidden-480"><?php echo $key->tax; ?></td>
        <td align="center" valign="middle" class="hidden-480"><?php echo $d; ?></td>
        <td align="center" valign="middle"><?php echo $key->total_amount; ?></td>
        <td align="right" valign="middle"><?php echo $key->total_due; ?></td>
      </tr>
      <?php $sum=$sum+$key->total_due;
                   $sum1=$sum1+$key->total_amount; 
					$total_bill_amount = $total_bill_amount + $key->bill_amount ;
					
					$total_tax_amount = $total_tax_amount + $key->tax ;
					
				   } ?>
      <tr>
        <td colspan="8" ><hr style="background: #EEEEEE; border: none; height: 1px; "></td>
      </tr>
      <tr>
        <td colspan="7" style="text-align:right; "><strong>Total Bill Amount:</strong></td>
        <td  style="text-align:right" ><strong>
          <?php 
                                
                           echo number_format($total_bill_amount, 2, '.','');
                              
                        
                        ?>
          </strong></td>
      </tr>
      <tr>
        <td colspan="7" style="text-align:right; "><strong>Total Tax:</strong></td>
        <td  style="text-align:right" ><strong>
          <?php 
                                
                            echo number_format($total_tax_amount, 2, '.','');
						  
                              
                        
                        ?>
          </strong></td>
      </tr>
      <tr>
        <td colspan="7" style="text-align:right; "><strong>Total POS Amount:</strong></td>
        <td  style="text-align:right" ><strong>
          <?php 
                                
                           $sum_pos=$sum;
                            echo number_format($sum_pos, 2, '.','');  
                        
                        ?>
          </strong></td>
      </tr>
    </tbody>
  </table>
  <hr style="background: #EEEEEE; border: none; height: 1px;">
  <table width="100%">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <?php } ?>
  <?php if($transaction_details){  ?>
  <span style="font-size:16px; font-weight:700; color:#D75C58; padding:10px 5px; display:block;"> Payment Details </span>
  <hr style="background: #D75C58; border: none; height: 1px;">
  <table width="100%" class="td-pad">
    <thead>
      <tr style="background: #EDEDED; color: #1b1b1b">
        <th width="5%" align="left"> # </th>
        <th width="5%" align="left"> Status </th>
        <th width="20%"> Transaction Date </th>
        <th width="10%"> Profit Center </th>
        <th width="15%"> Payment Mode </th>
        <th width="30%" align="center"> Transaction Details </th>
        <th width="10%" align="right" style="padding-right:8px;"> Amount </th>
      </tr>
    </thead>
    <tbody style="background: #fafafa">
      <?php
            $i = 1; $sum =0;
            foreach($transaction_details as $row1){
			 ?>
      <tr>
        <td style="text-align:left"><?php echo $i; ?></td>
        <td style="text-align:left">
			<?php
				if($row1->t_status == 'Cancel')
							echo '<span style="color:red;">Declined</span>';
				else if($row1->t_status == 'Pending')
							echo '<span style="color:orange;">Processing</span>';
				else
							echo '<span style="color:green;">Recieved</span>';
			?>
		</td>
        <td style="text-align:center"><?php echo date("g:ia dS M Y",strtotime($row1->t_date)); ?></td>
        <td style="text-align:center"><?php echo $row1->p_center; ?></td>
        <td style="text-align:center"><?php echo $row1->t_payment_mode; ?></td>
        <td style="text-align:center"><?php echo $row1->t_bank_name ;?></td>
        <td style="text-align:right"><?php echo $row1->t_amount ;?></td>
      </tr>
      <?php $i++; 
	  if($row1->t_status == 'Done'){
			$sum=$sum + $row1->t_amount;
            }} ?>
       <tr>
        <td colspan="6" ><hr style="background: #EEEEEE; border: none; height: 1px; "></td>
      </tr>
      <tr>
        <td colspan="5" style="text-align:right; "><strong>Total Payment Completed:</strong></td>
        <td align="right" valign="middle"><strong>
          <?php 
if(isset($sum) && $sum) {
echo 'INR '.$sum;
} 
else {
$paid=0;
echo "Yet to pay";
}
?>
          </strong></td>
      </tr>
    </tbody>
  </table>
  <hr style="background: #EEEEEE; border: none; height: 1px;">
  <table width="100%">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table class="td-pad td-fon-size" width="100%">
    <tr>
      <td align="right" width="75%" valign="middle">Booking amount:</td>
      <td align="right" valign="middle">
        <?php  if(isset($food_tax)){
				  echo number_format(($g_tot=$tot_amt+$food_price+$food_tax+$charge_sum+$service_sum), 2);
			  }
				else{
					echo number_format(($g_tot=$tot_amt+$charge_sum+$service_sum), 2);
				}
                    ?>
        </td>
    </tr>
    <tr>
      <td align="right" valign="middle">Ex Chrage Amount:</td>
      <td align="right" valign="middle"><?php 
echo number_format(($charge_sum), 2, '.','');
?></td>
    </tr>
    <tr>
      <td align="right" valign="middle">POS Amount:</td>
      <td align="right" valign="middle">
        <?php  
			  $total_bill_amt=$total_bill_amount+$total_tax_amount;
			  echo number_format($total_bill_amt, 2, '.',''); 
			  ?>
       </td>
    </tr>
    <tr>
      <td align="right" valign="middle">POS tax Amount:</td>
      <td align="right" valign="middle"><?php echo number_format($total_tax_amount, 2, '.','');?></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Total Paid amount:</td>
      <td align="right" valign="middle"><?php echo number_format($sum,2); ?></td>
    </tr>
    <tr>
      <td align="right" valign="middle"><strong>Total Amount:</strong></td>
      <td align="right" valign="middle"><strong><?php echo  number_format(($g_tot+$sum_pos),2); ?></strong></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Discount:</td>
      <td align="right" valign="middle"><?php echo number_format($d,2); ?>(<?php echo $co; ?>)</td>
    </tr>
    <tr>
      <td align="right" valign="middle">Total Payable Amount:</td>
      <td align="right" valign="middle"><?php echo number_format(($net_pay=($g_tot-$sum_pos)-$d),2); ?></td>
    </tr>
    <tr>
      <td align="right" valign="middle"><span style="font-size:18px; font-weight:700; color:#8876A8;">Total Due:</span></td>
      <td align="right" valign="middle">
      	<?php 
$td = $net_pay-$sum;
$paid_c = number_format($td, 2);
if($paid_c != 0){ ?>
                <span style="font-size:18px; font-weight:700; color:#F65656;">INR <?php echo $paid_c;?></span>
                <?php	
}
else{ ?>
                <span style="font-size:18px; font-weight:700; color:#7FBA67;">Full Paid</span>
                <?php }?>
      </td>
    </tr>
  </table>
  
  <?php
 }
	 if($authority_sign == '1'){ 
 ?>
  <table width="100%">
    <tr>
      <td style="padding:100px 0 0;" colspan="2"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="center" width="35%">______________________________<br />
        Authorized Signature </td>
    </tr>
    <tr>
      <td width="65%">&nbsp;</td>
      <td align="center" width="35%"><?php   // Showing the Hotel Name from Session
                
        $hotel_n = $this->dashboard_model->get_hotel_name($this->session->userdata('user_hotel'));
        if(isset($hotel_n->hotel_name) && $hotel_n->hotel_name)
            echo 'For, '.$hotel_n->hotel_name;
    ?></td>
    </tr>
  </table>
  <?php }?>
  <table width="100%">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <hr style="background: #00C5CD; border: none; height: 1px; margin-bottom:10px; width:100%;">
  <span style="font-size:9px; font-weight:400; color:#939292; display:block;"> Powered by HOTEL OBJECTS, from Theinfinitytree Techno Solutions PVT. LTD.  Thank you for your stay! </span> </div>
