<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/dashboard/assets/admin/pages/css/invoice.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo base_url();?>assets/dashboard/assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 14px;
	line-height: 1.42857143;
	color: #555555;
}
.well {
	border: 0;
	-webkit-box-shadow: none !important;
	-moz-box-shadow: none !important;
	box-shadow: none !important;
	min-height: 20px;
	margin-bottom: 20px;
	border-radius: 4px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
}
.td-pad th, .td-pad td {
	padding: 10px;
}
</style>
<?php
$chars = '0123456789';$r='';
for($i=0;$i<7;$i++)
{
    $r.=$chars[rand(0,strlen($chars)-1)];
}
$randomstring = $r."  ";

?>
<div style="padding:15px 35px;">
  <table width="100%">
    <tr>
      <td colspan="2" align="center"><img src="upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo" /></td>
    </tr>
    <tr>
      <td colspan="2"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr>
      <td colspan="2">
      	<table>
          <tr>
              <td width="63%" style="font-size:12px;"> Services Tax Reg No.:  AAHCA9006GSD001<br />
            Vat Reg No.: 19240920098
<br />
             Luxury Tax No.: 00450010500103<br />
            CIN No.: U55101WB2009PTC134693
 </td>
            <td width="37%" style="font-size:12px;"><span style="color:#00C5CD; font-weight:bold;">INVOICE</span><br />
              <?php echo "INVC","$randomstring" ?><br />
              <?php echo "DATE:&nbsp;",date("d M Y") ?></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td width="85%"><?php foreach($booking_details as $row){ ?>
        <div class="col-xs-4">
          <h3>Bill To:</h3>
          <ul class="list-unstyled">
            <li> <b>Name:</b> <?php echo $row->cust_name ?> </li>
            <li> <b>Address:</b> <?php echo $row->cust_address ?> </li>
            <li> <b>Mobile no:</b> <?php echo $row->cust_contact_no ?> </li>
          </ul>
        </div>
        <?php } ?>
        </td>
      <td width="15%">
	  <?php
            foreach($total_payment as $row3){
                $total_amount = $row3->t_amount;
                ?>
        <?php } ?>
        <div class="well"> <strong><?php echo  $hotel_name->hotel_name; ?></strong><br/>
          <?php echo  $hotel_contact['hotel_street1']; ?><br/>
          <?php echo  $hotel_contact['hotel_district']; ?>, <?php echo  $hotel_contact['hotel_state']; ?> , <?php echo  $hotel_contact['hotel_pincode']; ?><br/>
          <abbr title="Phone"><strong>Phone:</strong></abbr> <?php echo  $hotel_contact['hotel_frontdesk_mobile']; ?><br/>
          <abbr title="Phone"><strong>Mail:</strong></abbr> <?php echo  $hotel_contact['hotel_frontdesk_email']; ?> </div>
          </td>
    </tr>
    
    <!-- Booking Details -->
    <tr>
      <td colspan="2">
      <table width="100%" class="td-pad">
          <thead>
            <tr style="background: #00C5CD; color: white">
              <th width="5%" align="left"> No </th>
              <th width="33%"> Product / Service Name </th>
              <!--<th class="hidden-480">
                         Amount
                    </th>-->
              <th width="10%"> Quantity </th>
              <th width="10%"> Unit Price </th>
              <th width="10%"> Amount </th>
              
              <th width="12%"> Total Tax </th>
              <th width="10%" align="right"> Total </th>
            </tr>
          </thead>
          <tbody style="background: #F2F2F2">
            <?php
               $total_for_one_booking=0;
            foreach($total_payment as $row3){
                $total_amount = $row3->t_amount;
                ?>
            <?php foreach($payment_details as $row2)
                        { ?>
            <tr>
              <td style="text-align:left"><?php echo $row2->booking_id; ?></td>
              <td style="text-align:center"><div>
                  <?php 
                            $units=$this->dashboard_model->get_unit_exact($row2->unit_id);

                            foreach ($units as $key) {
                                # code...
                                    $unit_name=$units->unit_name;
                            }

                        echo $unit_name." (".$row2->room_no.") "; ?>
                </div>
                <div> <strong>CheckIn Date:</strong> <?php echo date('d M , Y',strtotime($row2->cust_from_date)); ?> at <?php echo date('h:i A',strtotime($row2->confirmed_checkin_time));?> <strong>Checkout Date:</strong> <?php echo date('d M , Y',strtotime($row2->cust_end_date)); ?> at <?php echo date('h:i A',strtotime($row2->confirmed_checkout_time));?> </div>
                <div> <?php echo $row2->comment; ?> </div></td>
              <td style="text-align:center"><?php echo $row2->stay_days." (Days)"; ?></td>
              <td style="text-align:center"><?php echo ($row2->room_rent_total_amount/$row2->stay_days); ?></td>
              <td style="text-align:center"><?php   if($group_id =="0"){echo $price= $row2->room_rent_total_amount; }else{
               echo  $price=0;
              }
                    $sc=$row2->service_charge;
                    $st=$row2->service_tax;
                    $lc=$row2->luxury_tax;
                    $discount=$row2->discount;
              ?></td>
               
              <td style="text-align:center"><?php echo $row2->room_rent_tax_amount; ?>
               
              </td>
              <td style="text-align:right"><?php  if($group_id =="0"){
                echo $room_rent_sum_total= $row2->room_rent_sum_total;
                }else{
                  echo $room_rent_sum_total=0;
                  } 
                  ?></td>
            </tr>

            
            <!-- service start-->
            
            <?php

                      $service_string=$row2->service_id;
                        $service_total_price=$row2->service_price;
                        $service_sum=0;
                        if($service_string!=""){
                    ?>
            <tr>
              <td colspan="7" ><hr style="background: #00C5CD; border: none; height: 1px; "></td>
            </tr>
            <?php

                    $counter = 0;
                    $temp = 0;
                    $service_array=explode(",", $service_string);

                    //print_r($service_array); 

                    for ($i=1; $i < sizeof($service_array) ; $i++) { 

                        $services=$this->dashboard_model->get_service_details($service_array[$i]);

                        $occurance_strng = substr_count($row2->service_id,$service_array[$i]);

                        if ($occurance_strng > 1) {
                            $counter ++;
                            if ($counter == 1) {
                                ?>
            <tr>
              <td style="text-align:left"><?php echo $services->s_id; ?></td>
              <td style="text-align:center"><?php echo $services->s_name; ?></td>
              <td style="text-align:center"><?php echo $occurance_strng; ?></td>
              <td style="text-align:center"><?php echo $services->s_price; ?></td>
              <td style="text-align:center"><?php echo $service_total= ($services->s_price * $occurance_strng); ?></td>
             
              <td style="text-align:center"><?php echo $service_tax= ($services->s_tax ); ?></td>
              <td style="text-align:right"><?php echo $service_grand_total=$service_total+$service_tax; ?></td>
            </tr>
            <?php
                            }
                        }
                        else{
                       ?>
            <tr>
              <td style="text-align:left"><?php echo $services->s_id; ?></td>
              <td style="text-align:center"><?php echo $services->s_name; ?></td>
              <td style="text-align:center"><?php echo $occurance_strng; ?></td>
              <td style="text-align:center"><?php echo $services->s_price; ?></td>
              <td style="text-align:center"><?php echo $service_total= ($services->s_price * $occurance_strng); ?></td>
              
              <td style="text-align:center"><?php echo $service_tax= ($services->s_tax ); ?></td>
              <td style="text-align:right"><?php echo $service_grand_total=$service_total+$service_tax; ?></td>
            </tr>
            <?php
                            }
                            $service_sum=$service_sum+$service_grand_total; 
                        }
                    } 
                    ?>
            
            <!-- charge calculation -->
            
            <?php

                      $charge_string=$row2->booking_extra_charge_id;
                        $charge_total_price=$row2->booking_extra_charge_amount;


                            $charge_sum=0;
                if($charge_string!=""){
                    ?>
            <tr>
              <td colspan="7" ><hr style="background: #00C5CD; border: none; height: 1px; "></td>
            </tr>
            <?php

                    $charge_array=explode(",", $charge_string);

                    //print_r($service_array); 

                    for ($i=1; $i < sizeof($charge_array) ; $i++) { 
                        # code...

                        $charges=$this->dashboard_model->get_charge_details($charge_array[$i]);


                         

                   
                        # code...
                    

               ?>
            <tr>
              <td style="text-align:left"><?php echo $charges->crg_id; ?></td>
              <td width="33%" style="text-align:center"><?php echo $charges->crg_description; ?></td>
              <td style="text-align:center"><?php echo $charges->crg_quantity; ?></td>
              <td style="text-align:center"><?php echo $charges->crg_unit_price; ?></td>
              <td style="text-align:center"><?php echo $charge_total= ($charges->crg_unit_price * $charges->crg_quantity); ?></td>
              <td style="text-align:center"><?php echo $charge_tax= ($charges->crg_tax ); 
                        echo "%";?></td>
              <td style="text-align:right"><?php echo $charge_grand_total=$charges->crg_total; ?></td>
            </tr>
            <?php $charge_sum=$charge_sum+$charge_grand_total; }} ?>
            <?php 

           if($group_id =="0"){ $total_for_one_booking= $total_for_one_booking+$row2->room_rent_sum_total+$service_sum+$charge_sum;}else{$total_for_one_booking= $total_for_one_booking+$service_sum+$charge_sum;}  }} ?>
            <tr>
              <td colspan="7" ><hr style="background: #00C5CD; border: none; height: 1px; "></td>
            </tr>
             <tr>
              <td colspan="6" style="text-align:right; ">Room Price:</td>
              <td  style="text-align:right" ><?php echo $price;
                      
                    ?></td>
            </tr>
               <tr>
              <td colspan="6" style="text-align:right; ">Service Tax:</td>
              <td  style="text-align:right" ><?php echo $st."%";
                      
                    ?></td>
            </tr>
               <tr>
              <td colspan="6" style="text-align:right; ">Service Charge:</td>
              <td  style="text-align:right" ><?php echo $sc."%";
                      
                    ?></td>
            </tr>
               <tr>
              <td colspan="6" style="text-align:right; ">Luxury Tax:</td>
              <td  style="text-align:right" ><?php echo $lc."%";
                      
                    ?></td>
            </tr>
               <tr>
              <td colspan="6" style="text-align:right; ">Total Amount:</td>
              <td  style="text-align:right" ><?php echo $room_rent_sum_total;
                      
                    ?></td>
            </tr>
             <tr>
              <td colspan="6" style="text-align:right; ">Service Amount:</td>
              <td  style="text-align:right" ><?php echo $service_sum;
                      
                    ?></td>
            </tr>
             <tr>
              <td colspan="6" style="text-align:right; ">Charge Amount:</td>
              <td  style="text-align:right" ><?php echo $charge_sum;
                      
                    ?></td>
            </tr>
            <tr>
              <td colspan="6" style="text-align:right; "><strong>Grand Total:</strong></td>
              <td  style="text-align:right" ><strong><?php echo $total_for_one_booking;
                      
                    ?></strong></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    
    <!-- Service -->
    
    <tr>
      <td colspan="2" width="100%">&nbsp;</td>
    </tr>
    
    <!--pos details-->
    <?php $sum_pos=0; if($pos && isset($pos)){  ?>
    <tr>
      <td colspan="2">
      <table width="100%" class="td-pad">
          <thead>
            <tr style="background: #00C5CD; color: white">
              <th width="5%" align="center" valign="middle"> No </th>
              <th width="19%" align="center" valign="middle"> Date </th>
              <th width="30%" align="center" valign="middle"> Food Items </th>
              <th width="10%" align="center" valign="middle"> Tax </th>
              <th width="10%" align="center" valign="middle"> Discount </th>
              <th width="16%" align="center" valign="middle"> Total </th>
              <th width="10%" align="right" valign="middle"> Due </th>
            </tr>
          </thead>
          <tbody style="background: #F2F2F2">
            <?php $sum=0; $sum1=0; foreach ($pos as $key) {
                           
						    # code...
                         
						 ?>
            <tr>
              <td align="center" valign="middle"><?php echo $key->pos_id; ?></td>
              <td align="center" valign="middle"><?php echo $key->date; ?></td>
              <td align="center" valign="middle" class="hidden-480"><?php 
                                    $items=$this->dashboard_model->all_pos_items($key->pos_id);
                                    foreach ($items as $item ) {
										
                                        # code...
                                        echo $item->item_name."(".$item->item_quantity.") ";
                                        echo "</br>";
                                    }
                                ?></td>
              <td align="center" valign="middle" class="hidden-480"><?php echo $key->tax; ?></td>
              <td align="center" valign="middle" class="hidden-480"><?php echo $key->discount; ?></td>
              <td align="center" valign="middle"><?php echo $key->total_amount; ?></td>
              <td align="right" valign="middle"><?php echo $key->total_due; ?></td>
            </tr>
            <?php $sum=$sum+$key->total_due;
                       $sum1=$sum1+$key->total_amount;  } ?>
            <tr>
              <td colspan="7" ><hr style="background: #00C5CD; border: none; height: 1px; "></td>
            </tr>
            <tr>
              <td colspan="6" style="text-align:right; "><strong>Total Due amount:</strong></td>
              <td  style="text-align:right" ><strong>
                <?php 
									
							   echo $sum_pos=$sum;
								  
							
                            ?>
                </strong></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <?php } ?>
    <tr>
      <td colspan="2" width="100%">&nbsp;</td>
    </tr>
    
    <!-- Transaction -->
    <tr>
      <td colspan="2"><?php if($transaction_details){  ?>
        <table width="100%"  class="td-pad">
          <thead>
            <tr  style="background: #00C5CD; color: white">
              <th width="5%" align="left"> # </th>
              <th width="30%"> Transaction Date </th>
              <!--<th class="hidden-480">
                         Amount
                    </th>-->
              <th width="25%"> Payment Mode </th>
              <th width="30%"> Bank name </th>
              <th width="10%" align="right" style="padding-right:8px;"> Amount </th>
            </tr>
          </thead>
          <tbody style="background: #F2F2F2">
            <?php
                $i = 1; $sum =0;
                foreach($transaction_details as $row1){  ?>
            <tr>
              <td style="text-align:left"><?php echo $i; ?></td>
              <td style="text-align:center"><?php echo date("Y-m-d",strtotime($row1->t_date))." (".date("H:i:s",strtotime($row1->t_date)).")"; ?></td>
              <td style="text-align:center"><?php echo $row1->t_payment_mode; ?></td>
              <td style="text-align:center"><?php echo $row1->t_bank_name ;?></td>
              <td style="text-align:right"><?php echo $row1->t_amount ;?></td>
            </tr>
            <?php $i++; $sum=$sum + $row1->t_amount;
                } ?>
            <tr>
              <td colspan="4" style="padding-right:8px; text-align:right;"><strong>Total amount:</strong></td>
              <td style="text-align:right"><strong><?php echo $sum; ?></strong></td>
            </tr>
             <tr>
              <td colspan="4" style="padding-right:8px; text-align:right;"><strong>Discount:</strong></td>
              <td style="text-align:right"><strong><?php echo $discount; ?></strong></td>
            </tr>
             <tr>
              <td colspan="4" style="padding-right:8px; text-align:right;"><strong>Total Payble:</strong></td>
              <td style="text-align:right"><strong><?php echo $sum-$discount; ?></strong></td>
            </tr>
            <tr>
              <td colspan="5" ><hr style="background: #00C5CD; border: none; height: 1px; "></td>
            </tr>
            <tr>
              <td colspan="4" style="padding-right:8px; text-align:right;"><strong>Total Due:</strong></td>
              <td style="text-align:right"><strong><?php echo ($total_for_one_booking+$sum_pos) -$sum-$discount; ?></strong></td>
            </tr>
          </tbody>
        </table>
        <?php } ?></td>
    </tr>
    <tr>
      <td colspan="2" width="100%" height="100px">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="padding:25px 0 0;">
      <table>
          <tr>
            <td width="65%">&nbsp;</td>
            <td align="center" width="35%">______________________________<br />
              Authorized Signature </td>
          </tr>
        </table>
        </td>
    </tr>
  </table>
</div>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script> 
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip --> 
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<script src="<?php echo base_url();?>assets/dashboard/assets/global/scripts/metronic.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/dashboard/assets/admin/layout/scripts/layout.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/dashboard/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/dashboard/assets/admin/layout/scripts/demo.js" type="text/javascript"></script> 
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
    });
</script>
