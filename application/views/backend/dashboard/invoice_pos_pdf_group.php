<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td{
	font-size:12px;
}
.fon-big, .fon-big table, .fon-big span{
		font-size: 16px !important;
		color: #000000 !important;
		font-family: Arial, Helvetica, sans-serif !important;
	}
	.fon-big tr, .fon-big tbody{
		background: none !important;
	}
	.fon-big hr{
		background-color: #000000 !important;
	}
</style>

<?php if($printer->printer_settings=='normal'){
	$print='';
}else{
	$print='fon-big';
} ?>
<div style="padding:10px 35px;" class="invoice <?php echo $print ?> ">

	<table width="100%" border="0" cellspacing="0" cellpadding="0">

		<tr>

			<td align="left" valign="middle"><img src="upload/hotel/<?php echo $hotel->hotel_logo_images_thumb;?>" alt="logo" class="logo-default"/>
			</td>

			<td align="right" valign="middle">
				<?php echo "<strong><font size='14'>".$hotel->hotel_name.'</font></strong>'?>
			</td>

		</tr>

		<tr>

			<td width="100%" colspan="2">&nbsp;

				<hr style="background: #00C5CD; border: none; height: 1px; margin-bottom:10px;">

			</td>

		</tr>

	</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">

		<?php 

	if(isset($tax)){

	foreach($tax as $tax_details){

	?>

		<tr>

			<td width="70%" valign="top" style="font-size:12px;"> GST Reg No.:
				<?php  echo $tax_details->service_tax_no;?><br/> CIN No.:
				<?php  echo $tax_details->cin_no;?>
				<ul class="list-unstyled">

					<li>
						<strong>
							<?php echo $bookings->g_name; ?>
						</strong>
					</li>
					<li><strong>GSTIN No:</strong>
						
					</li>

					<li>
						<?php echo $bookings->g_address; ?> </li>

					<li>
						<?php echo $bookings->g_city; ?> </li>

					<li> PIN:
						<?php echo $bookings->g_pincode; ?> </li>

					<li>
						<?php echo $bookings->g_state; ?> -
						<?php echo $bookings->g_country; ?> </li>

					<li><strong>Phone:</strong>
						<?php echo $bookings->g_contact_no; ?>
					</li>

					<li><strong>Email:</strong>
						<?php echo $bookings->g_email; ?>
					</li>

				</ul>

			</td>

			<td width="30%" valign="top" style="font-size:12px;"><span style="color:#00C5CD; font-weight:bold;"> POS INVOICE</span><br/>

				<?php echo "DATE:&nbsp;",date("d M Y") ?>
				<strong>
					<?php echo $hotel->hotel_name; ?>
				</strong><br/>

				<?php echo $hotel->hotel_street1; ?><br/>

				<?php echo $hotel->hotel_street2; ?><br/>

				<?php echo $hotel->hotel_district." - ".$hotel->hotel_pincode; ?><br/>

				<?php echo $hotel->hotel_state?> -
				<?php echo $hotel->hotel_country; ?><br/>

				<strong>Phone:</strong>
				<?php echo $hotel->hotel_owner_mobile; ?><br/>

				<strong>Mail:</strong>
				<?php echo $hotel->hotel_owner_email; ?>

			</td>

		</tr>



		<?php }}?>

	</table>

	<table>

		<tr>
			<td>&nbsp;</td>
		</tr>

	</table>

	<table class="td-pad">

		<thead>

			<tr style="background: #e5e5e5; color: #1b1b1b">

				<th width="8%" align="center" valign="middle"> # </th>

				<th width="15%" align="center" valign="middle"> Date </th>

				<th width="20%" align="center" valign="middle" class="hidden-480"> Food Items </th>

				<th align="center" valign="middle" class="hidden-480"> SGST </th>

				<th align="center" valign="middle" class="hidden-480"> CGST </th>

				<th align="center" valign="middle" class="hidden-480"> IGST </th>

				<th align="center" valign="middle" class="hidden-480"> S Chrg </th>

				<th align="center" valign="middle" class="hidden-480"> Tax </th>

				<th align="center" valign="middle" class="hidden-480"> Disc </th>

				<th align="center" valign="middle"> Total </th>

				<th align="right" valign="middle"> Due </th>

			</tr>

		</thead>

		<tbody style="background: #F2F2F2">

			<?php 

	  $sum=0; 

	  $sum1=0; 

	  $tx=0;
	  $pos_taxt = 0;

	  foreach ($pos as $key) {
		  
						$pos_tax = 0;
						
						$pos_sgst = $key->food_sgst + $key->liquor_sgst; 
						$pos_tax = $pos_tax + $pos_sgst;
						
						$pos_cgst = $key->food_cgst + $key->liquor_cgst; 
						$pos_tax = $pos_tax + $pos_cgst;
						
						$pos_igst = $key->food_igst + $key->liquor_igst; 
						$pos_tax = $pos_tax + $pos_igst;
						
						$pos_taxt = $pos_tax + $pos_taxt;
						$sum_sgst=$sum_sgst+$pos_sgst;
						$sum_cgst=$sum_cgst+$pos_cgst;
						$sum_igst=$sum_igst+$pos_igst;
						

						$u_amt=$key->total_amount-$pos_tax+$d; 

    ?>

			<tr>

				<td width="10%" align="center" valign="middle">
					<?php echo $key->invoice_number; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $key->date; ?>
				</td>

				<td width="30%" align="center" valign="middle" class="hidden-480">

					<?php 

                $items=$this->dashboard_model->all_pos_items($key->pos_id);

                foreach ($items as $item ) {

                   

                    echo $item->item_name." - ".$item->item_quantity." "."- Rs. ".$item->unit_price."";

                    echo "<br>";

                }

            ?>

				</td>

				<td align="center" valign="middle" class="hidden-480">
					
					<?php  // sgst

					 

					 echo number_format($pos_sgst, 2, '.','');

					?>

				</td>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php 

					 

					 echo number_format($pos_cgst, 2, '.','');

					?>

				</td>
				
				<td align="center" valign="middle" class="hidden-480">

					<?php 

					 echo number_format($pos_igst, 2, '.','');

					?>

				</td>

				<td align="center" valign="middle" class="hidden-480">
					<?php echo $key->fvat; ?>
				</td>

				<td align="center" valign="middle" class="hidden-480">
					<?php echo $pos_tax; ?>
				</td>

				<td align="center" valign="middle" class="hidden-480">
					<?php echo $key->discount; ?>
				</td>

				<td align="center" valign="middle">
					<?php echo $key->total_amount; ?>
				</td>

				<td align="right" valign="middle">
					<?php echo $key->total_due; ?>
				</td>

			</tr>

			<?php $sum=$sum+$key->total_due;

    $sum1=$sum1+$key->total_amount;

	$tx=$tx+$key->tax;

	} ?>

			<tr>
				<td colspan="12" style="padding:0;">
					<hr style="background: #00C5CD; border: none; height: 1px;">
				</td>
			</tr>

			<tr>

				<td colspan="10" align="right" valign="middle">

					<strong>Discount:</strong>

				</td>

				<td colspan="1" align="right" valign="middle">

					<?php echo $key->discount; ?>

				</td>

			</tr>

			<tr>

				<td colspan="10" align="right" valign="middle">

					<strong>Tax:</strong><br/>

				</td>

				<td colspan="1" align="right" valign="middle">

					<?php echo $pos_taxt; ?>

				</td>

			</tr>

			<tr>

				<td colspan="10" align="right" valign="middle">

					<strong>Grand Total:</strong>

				</td>

				<td colspan="1" align="right" valign="middle">

					<?php echo $sum1; ?>

				</td>

			</tr>

			<tr>

				<td colspan="10" align="right" valign="middle">

					<strong>Total Due:</strong>

				</td>

				<td colspan="1" align="right" valign="middle">

					<?php echo $sum; ?>

				</td>

			</tr>

		</tbody>

	</table>

	<table>

		<tr>
			<td>&nbsp;</td>
		</tr>

	</table>

	<table>

		<tr>

			<td style="padding:100px 0 0;">

				<table>

					<tr>

						<td width="65%">&nbsp;</td>

						<td align="center" width="35%">______________________________<br/> Authorized Signature

						</td>

					</tr>

				</table>

			</td>

		</tr>

	</table>

</div>