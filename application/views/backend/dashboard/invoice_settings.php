
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-file-archive-o"></i>Invoice Settings </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
          <div class="col-md-10">
            
            
        </div>
        
        <div class="col-md-2">
          <div class="btn-group pull-right">
            <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
            <ul class="dropdown-menu pull-right">
              <li> <a href="javascript:window.print();"> Print </a> </li>
              <li> <a href="" target="_blank"> Save as PDF </a> </li>
              <!--<li> <a href="javascript:;"> Export to Excel </a> </li>-->
            </ul>
          </div>
        </div>
          </div>
        </div>
		<table class="table table-striped table-bordered table-hover" id="sample_1">
          <thead>
            <tr> 
              <th> # </th>
			  <th> Hotel Id </th>
			 <!-- <th>	Group Bookings Prefix </th>
              <th>Single Bookings Prefix</th>-->
              <th>Booking Source Invoice  </th>
              <th>	Booking Note Invoice </th>
              <th>Nature Visit Invoice </th>
              <th>Company Details Invoice  </th>
              <th>Service Tax  </th>
              <th>Luxury Tax  </th>
              <th>Update Date </th>
			  <th>Action </th>
            </tr>
          </thead>
          <tbody>
           
          <tr>
            <?php
						if(isset($invoice_settings) && $invoice_settings){
						$srl_no=0;
						//print_r($invoice_settings);
						//exit;
						foreach($invoice_settings as $settings){
						$srl_no++;
					  
					
						 
    			  ?>
    				<td>43344<?php echo $srl_no;?></td>
    				<td><?php echo $settings->hotel_id;?></td>
    				<!--<td><?php echo $settings->gb_prefix;?></td>
    				<td><?php echo $settings->sb_prefix;?></td>-->
    				<td><?php if($settings->booking_source_inv_applicable==1)echo "Applicable"; else echo "Not Applicable";?></td>
    				<td><?php if($settings->nature_visit_inv_applicable==1)echo "Applicable"; else echo "Not Applicable";?></td>
    				<td><?php if($settings->booking_note_inv_applicable==1)echo "Applicable"; else echo "Not Applicable";?></td>
    				<td><?php if($settings->company_details_inv_applicable==1)echo "Applicable"; else echo "Not Applicable";?></td>
    				<td><?php if($settings->service_tax_applicable==1)echo "Applicable"; else echo "Not Applicable";?></td>
    				<td><?php if($settings->luxury_tax_applicable==1)echo "Applicable"; else echo "Not Applicable"; ?></td>
    				<td><?php echo $settings->update_date	;?></td>
					
					<td>
                                	<table width="100%">
                                    <tr>
                                   
									  <td>
                                    <a href="<?php echo base_url() ?>dashboard/edit_invoice_settings?id=<?php echo $settings->id;?>" class="btn blue" data-toggle="modal" > <i class="fa fa-edit"></i></a>
                                    </td>
									</tr>
                                    </table>
                                </td>
		      </tr>
				 <?php }}?>
          </tbody>
        </table>
		
      </div>
    </div>
  </div>
</div>

<script>
function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_stock_invent?a_id="+id,
                data:{a_id:id},
                success:function(data)
                {
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });
        });
    }
</script>		 