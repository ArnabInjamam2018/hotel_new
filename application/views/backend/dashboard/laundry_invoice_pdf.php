<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Verdana, Geneva, sans-serif;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td {
	font-size: 12px;
}
.td-fon-size td {
	font-size: 14px;
}
</style>

<div style="padding:15px 35px;">
<?php
 
$chars = '0123456789';$r='';
for($i=0;$i<7;$i++)
{
    $r.=$chars[rand(0,strlen($chars)-1)];
}
$randomstring = $r."  ";
$chk=$this->bookings_model->get_invoice_settings();

if(isset($chk->booking_source_inv_applicable)){
    $booking_source = $chk->booking_source_inv_applicable;
} else {
	$booking_source ="";
}
if(isset($chk->nature_visit_inv_applicable)){
$nature_visit = $chk->nature_visit_inv_applicable;
} else {
	$nature_visit ="";
}
if(isset($chk->booking_note_inv_applicable)){
$booking_note = $chk->booking_note_inv_applicable;
} else {
	$booking_note ="";
}
if(isset($chk->company_details_inv_applicable)){
$company_details = $chk->company_details_inv_applicable;
} else {
	$company_details ="";
}
if(isset($chk->service_tax_applicable)){
    $service_tax = $chk->service_tax_applicable;
} else {
	$service_tax =0;
}
if(isset($chk->service_charg_applicable)){
    $service_charg = $chk->service_charg_applicable;
} else {
	$service_charg = 0;
}
if(isset($chk->authorized_signatory_applicable)){
    $authority_sign = $chk->authorized_signatory_applicable	;
} else {
	$authority_sign = 0;
}
if(isset($chk->unit_no)){
    $unit_no = $chk->unit_no;
} else {
	$unit_no = 0;
}
if(isset($chk->unit_cat)){
    $unit_cat = $chk->unit_cat;
} else {
	$unit_cat = 0;
}
if(isset($chk->luxury_tax_applicable)){
    $luxury_tax = $chk->luxury_tax_applicable;
} else {
	$luxury_tax =0;
}
if(isset($chk->invoice_setting_food_paln)){
    $fd_plan = $chk->invoice_setting_food_paln;
} else {
	$fd_plan =0;
}

if(isset($chk->invoice_setting_service)){
    $service = $chk->invoice_setting_service;
} else {
	$service =0;
}

if(isset($chk->invoice_setting_extra_charge)){
    $extra_charge = $chk->invoice_setting_extra_charge;
} else {
	$extra_charge =0;
}

if(isset($chk->invoice_setting_laundry)){
    $laundry = $chk->invoice_setting_laundry;
} else {
	$laundry =0;
}

if(isset($chk->invoice_pref) && isset($chk->invoice_suf)){
	$suf=$chk->invoice_suf;
    $pref=$chk->invoice_pref;
} else {
	$suf="";
   $pref="";	
}


//print_r($chk);
?>
<table width="100%">
  <tr>
    <td align="left" valign="middle"><img src="upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" /></td>
    <td align="right" valign="middle"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?></td>
  </tr>
  <tr>
    <td colspan="2"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
  </tr>
</table>
<table width="100%">
  <tr>
    <?php 
if(isset($tax) && $tax ){
	//echo "<pre>";
	//print_r($tax);
	//exit;
    foreach($tax as $tax_details){
		if(isset($booking_details) && $booking_details){
foreach($booking_details as $bookings){ 
 
    $bd_id=$bookings->booking_id;
    $invoice=$this->dashboard_model->get_invoice($bd_id);

?>
    <td width="80%" align="left" valign="top"><?php if($company_details == '1'){ ?>
      Services Tax Reg No.:
      <?php  echo $tax_details->service_tax_no;?>
      <br />
      Vat Reg No.:
      <?php  echo $tax_details->vat_reg_no;?>
      <br />
      Luxury Tax No.:
      <?php  echo $tax_details->luxury_tax_reg_no;?>
      <br />
      CIN No.:
      <?php  echo $tax_details->cin_no; } else { ?>
      &nbsp;
      <?php } ?></td>
    <td width="20%" align="left" valign="top"><span style="color:#3D4B76; font-weight:bold; font-size:12px; line-height:20px; display:block; padding-bottom:5px;">INVOICE
      <?php 
                    if($bookings->group_id == 0)
                        echo ' (Single Booking)';
                    else
                        echo ' <span style="color:#F8681A;">(Single under Group)</span>';
                ?>
      </span>
      <?php 
            if($invoice && isset($invoice))
            {
                echo $pref.$invoice->invoice_no.$invoice->id.$suf;
            }?>
      <?php
            date_default_timezone_set("Asia/Kolkata");
             echo "DATE:&nbsp;",date("g:ia dS M Y") ?></td>
    <?php }}
	else{
		
	?>
    <td width="80%" align="left" valign="top"><?php if($company_details == '1'){ ?>
      Services Tax Reg No.:
      <?php  echo $tax_details->service_tax_no;?>
      <br />
      Vat Reg No.:
      <?php  echo $tax_details->vat_reg_no;?>
      <br />
      Luxury Tax No.:
      <?php  echo $tax_details->luxury_tax_reg_no;?>
      <br />
      CIN No.:
      <?php  echo $tax_details->cin_no; } else { ?>
      &nbsp;
      <?php } ?>
      <br />
      Name:
      <?php //echo "<pre>";print_r($ad_hoc); 
echo $ad_hoc->laundry_guest_name;?>
      <br/>
      Type:<?php echo $ad_hoc->laundry_guest_type;?></td>
    <td align="left" valign="top"><span style="color:#3D4B76; font-weight:bold; font-size:12px; line-height:20px; display:block; padding-bottom:5px;">INVOICE <span style="color:#F8681A;">(Ad Hoc)</span> </span>
      <?php
            date_default_timezone_set("Asia/Kolkata");
             echo "DATE:&nbsp;",date("g:ia dS M Y") ?>
      <div class="well"> <strong><?php echo  $hotel_name->hotel_name; ?></strong><br/>
        <?php echo  $hotel_contact['hotel_street1']; ?> <?php echo  $hotel_contact['hotel_street2'].', '; ?><br>
        <?php echo  $hotel_contact['hotel_district']; ?> - <?php echo  $hotel_contact['hotel_pincode']; ?> <?php echo  $hotel_contact['hotel_state']; ?> - <?php echo  $hotel_contact['hotel_country']; ?><br/>
        <strong>Phone:</strong> <?php echo  $hotel_contact['hotel_frontdesk_mobile']; ?><br/>
        <strong>Email:</strong> <?php echo  $hotel_contact['hotel_frontdesk_email']; ?><br/>
      </div></td>
    <?php }}}?>
  </tr>
  <tr>
    <td colspan="2" width="100%"></td>
  </tr>
  <tr>
    <td colspan="2"><?php 
// _sb dt25_10_16 - Only 2 Foreach
$flag = 0;
if(isset($booking_details) && $booking_details){
foreach($booking_details as $row){ 
  $guest_det=$this->dashboard_model->get_guest_details($row->guest_id);
  foreach($guest_det as $gst)
  {
    
     if(($row->bill_to_com == '1') && ($gst->g_type == 'Corporate') && ($gst->corporate_id != NULL) && ($gst->corporate_id != 0)) // Check if Bill to Company is Possible
      {
           $corporate_details=$this->dashboard_model->fetch_c_id1($gst->corporate_id); 
           ?>
      <ul class="list-unstyled" >
        <li> <strong style="color:#6B67A1;">
          <?php
                if($corporate_details->legal_name)
                    echo $corporate_details->legal_name;
                else
                    echo $corporate_details->name;
            ?>
          </strong> </li>
        <li>
          <?php 
                $fg = 10;
                if(isset($corporate_details->address) && ($corporate_details->address != '' && $corporate_details->address != ' ')){
                                        echo ($corporate_details->address);
                                        $fg = 0	;
                                        //echo ', ';
                                    }
                if(isset($corporate_details->city) && ($corporate_details->city != '' && $corporate_details->city != ' ')){
                                        if($fg == 0){
                                            echo ', ';
                                            
                                        }
                                            
                                        echo $corporate_details->city;
                                        echo '<br>';
                                        $fg = 1;
                                    }
                else if($fg == 0)
                                        $fg = 5;
                if(isset($corporate_details->state) && ($corporate_details->state != '' && $corporate_details->state != ' ')){
                                        if($fg == 5)
                                            echo '<br>';
                                        echo $corporate_details->state;
                                        $fg = 2;
                                        //echo ', ';
                                    }
                if(isset($corporate_details->pin_code) && $corporate_details->pin_code!= NULL){
                                        if($fg == 2 || $fg == 5){
                                            echo ', ';
                                        }									
                                        echo 'PIN - '.$corporate_details->pin_code;
                                        $fg = 3;
                                    }
                if(isset($corporate_details->country) && $corporate_details->country != '' && $corporate_details->country != ' '){
                                        if($fg == 3)
                                            echo ', ';
                                        echo $corporate_details->country;
                                        /*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';
                                        $fg = 4;*/
                                    }
                if($fg == 10){
                                        echo 'No Info';
                                    }
            ?>
        </li>
        <li> <?php echo '<strong>Phone: </strong>'.$corporate_details->g_contact_no; ?> </li>
        <li> <?php echo '<strong>Email: </strong>'.$corporate_details->corp_email; ?> </li>
        <li> <?php echo '<br><strong style="color:#06B4F5">Guest Name: </strong>';
        echo $gst->g_name;
        if($gst->c_des)
            echo ' - '.$gst->c_des;
        ?> </li>
      </ul>
      <?php
        }  // End IF 
        
        else{
        ?>
      <ul class="list-unstyled">
        <li> <strong style="color:#6B67A1;"> <?php echo $gst->g_name; ?> </strong> </li>
        <!--<li> <?php echo $gst->corporate_id; ?>  </li>-->
        <li>
          <?php 
                $fg = 10;
                if(isset($gst->g_address) && ($gst->g_address != '' && $gst->g_address != ' ')){
                                        echo ($gst->g_address);
                                        $fg = 0	;
                                        //echo ', ';
                                    }
                if(isset($gst->g_city) && ($gst->g_city != '' && $gst->g_city != ' ')){
                                        if($fg == 0){
                                            echo ', ';
                                            
                                        }
                                            
                                        echo $gst->g_city;
                                        echo '<br>';
                                        $fg = 1;
                                    }
                else if($fg == 0)
                                        $fg = 5;
                if(isset($gst->g_state) && ($gst->g_state != '' && $gst->g_state != ' ')){
                                        if($fg == 5)
                                            echo '<br>';
                                        echo $gst->g_state;
                                        $fg = 2;
                                        //echo ', ';
                                    }
                if(isset($gst->g_pincode) && $gst->g_pincode!= NULL){
                                        if($fg == 2 || $fg == 5){
                                            echo ', ';
                                        }									
                                        echo 'PIN - '.$gst->g_pincode;
                                        $fg = 3;
                                    }
                if(isset($gst->g_country) && $gst->g_country != '' && $gst->g_country != ' '){
                                        if($fg == 3)
                                            echo ', ';
                                        echo $gst->g_country;
                                        /*if($gst->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';
                                        $fg = 4;*/
                                    }
                if($fg == 10){
                                        echo 'No Info';
                                    }
            ?>
        </li>
        <!--<li><?php echo $gst->g_place; ?> </li>
            <li><?php echo $gst->g_city; ?> </li>
            <li> <?php if($gst->g_pincode) echo "PIN:".$gst->g_pincode; ?> </li>
            <li><?php echo $gst->g_state." - ".$gst->g_country; ?> </li>-->
        <li><strong>Phone: </strong><?php echo $gst->g_contact_no; ?> </li>
        <li><strong>Email: </strong><?php echo $gst->g_email; ?> </li>
      </ul>
      <?php } // End Else
      } // End Foreach
}}
      ?>
      <ul class="list-unstyled">
        <li style="height:5px;">&nbsp;</li>
        <?php
			if(isset($booking_details) && $booking_details){
       if($booking_source == '1'){		   
     ?>
        <li><strong>Booking Source: </strong>
          <?php if(isset($row2->booking_source) && $row2->booking_source){ $bk_source=$this->dashboard_model->getsourceId($row2->booking_source); 
			  }
			  
                    if(isset($bk_source) && $bk_source){
             echo $bk_source->booking_source_name;
              }else{
                  //echo $row2->booking_source;
                  echo "N/A";
			}?>
        </li>
        <?php  }?>
        <?php
       if($nature_visit == '1'){		   
     ?>
        <?php if(isset($row2->nature_visit) && $row2->nature_visit != ""){?>
        <li><strong>Nature of Visit: </strong>
          <?php  echo $row2->nature_visit; ?>
        </li>
        <?php } }}?>
      </ul></td>
  </tr>
</table>
<table width="100%">
    <tr>
      <td>&nbsp;</td>
    </tr>
</table>
  <input type="hidden" name="htax" id="htax" value="<?php echo $laundry_details->tax; ?>">
  <input type="hidden" name="hdate" id="hdate" value="<?php echo $laundry_details->receiv_date; ?>">
  <table width="100%" class="td-pad">
    <thead>
      <tr style="background: #e5e5e5; color: #1b1b1b">
        <th> Sl No </th>
        <th> Cloth Type </th>
        <th> Fabric </th>
        <th> Condition </th>
        <th> Service </th>
        <th> QTY </th>
        <th> Color </th>
        <th> Brand </th>
        <th> Price </th>
        <th> Tax </th>
        <th> Total </th>
      </tr>
    </thead>
    <tbody style="background: #F2F2F2">
      <?php 
	   $sl_no=0;
	   if(isset($laundry_details) && $laundry_details)
	   {
			$line_itm=$this->unit_class_model->get_line_itm($laundry_details->laundry_id);
			//echo "<pre>";
			//print_r($line_itm); //exit;
			//print_r($laundry_details); //exit;
			foreach($line_itm as $l_t){
					 $sl_no++;
		  ?>
      <tr align='center'>
        <td><?php echo  $sl_no;?></td>
        <td><?php $c_id=$l_t->cloth_type_id;
		$c_name=$this->unit_class_model->get_cloth_type_by_id($c_id);
		echo $c_name-> laundry_ct_name; 
		?></td>
        <td><?php $f_id=$l_t->fabric_type_id ;
		$f_name=$this->unit_class_model->get_fabric_type_by_id($f_id);
		echo $f_name->fabric_type; 
		?></td>
        <td><?php echo $l_t->item_condition  ;?></td>
        <td><?php $s_id=$l_t->laundry_service_type_id  ;
		$s_name=$this->unit_class_model->get_service_type_by_id($s_id);
		echo $s_name->laundry_type_name; 
		
		?></td>
        <td><?php echo $l_t->laundry_item_qty  ;?></td>
        <td><?php $color_id=$l_t->laundry_item_color  ;
		$color=$this->unit_class_model->get_color_by_id($color_id);
		if(isset($color)){
		echo $color->color_name;
		}?></td>
        <td><?php if($l_t->laundry_item_brand) echo $l_t->laundry_item_brand  ;?></td>
        <td><?php echo $l_t->item_price  ;?></td>
        <td><?php echo  $laundry_details->tax;?></td>
        <td><?php echo $l_t->item_price*$l_t->laundry_item_qty;  ?></td>
      </tr>
      <?php }}?>
    </tbody>
  </table>
    <?php $san=json_decode($laundry_details->response);
	//echo "<pre>";
	//print_r($obj);
	
	/*$san =array();
			 if(isset($obj) && $obj!=null){
			for($i=0; $i < count($obj); $i++){
				
				if(isset($obj[$i]) && $obj[$i]!=null){
				foreach($obj[$i] as $key => $val){
					$san[$key] =$val;
				}
			
		}*///print_r($san);
		//foreach ($san as $key => $value) {
		//echo '<p>'.$key.'='.$value.'</p>';
		//}
			//}
			 //}
	
	?>
    <table width="100%" class="td-pad td-fon-size">
      <tbody>
        <tr>
          <td width="76%" style="text-align:right;">Total Qty:</td>
          <td  style="text-align:right" ><?php echo $laundry_details->total_item; ?></td>
        </tr>
        <?php  foreach ($san as $key => $value) {?>
        <tr>
          <td width="76%" style="text-align:right; font-weight:bold;"><?php echo $key; ?></td>
          <td  style="text-align:right" ><?php echo $value;?></td>
        </tr>
        <?php }?>
        <tr>
          <td width="76%" style="text-align:right; ">Discount:</td>
          <td  style="text-align:right" ><?php echo $laundry_details->discount; ?></td>
        </tr>
        <tr>
          <td width="76%" style="text-align:right;">Total Amount Paid:</td>
          <td  style="text-align:right" ><?php echo $laundry_details->total_amount;  ?></td>
        </tr>
        <tr>
          <td width="76%" style="text-align:right; ">Due:</td>
          <td  style="text-align:right" ><?php echo $laundry_details->payment_due;  ?></td>
        </tr>
        <tr>
          <td width="76%" style="text-align:right;">Grand Total: </td>
          <td  style="text-align:right" ><?php echo $laundry_details->grand_total;  ?></td>
        </tr>
      </tbody>
    </table>
    
    <!--<p>Total Qty: <?php //echo $laundry_details->total_item; ?></p>
	   <p>Discount: <?php //echo $laundry_details->discount; ?></p>	   
	   <p id="t_amount">Total Amount Paid: <?php //echo $laundry_details->total_amount;  ?></p>
	   <p>Due: <?php //echo $laundry_details->payment_due; ?></p>
	   <p>Grand Total: <?php //echo $laundry_details->grand_total;  ?></p>--> 
  <table width="100%">
    <tr>
      <td style="padding:100px 0 0;" colspan="2"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="center" width="35%">______________________________<br />
        Authorized Signature </td>
    </tr>
    <tr>
      <td width="65%">&nbsp;</td>
      <td align="center" width="35%"><?php   // Showing the Hotel Name from Session
                
        $hotel_n = $this->dashboard_model->get_hotel_name($this->session->userdata('user_hotel'));
        if(isset($hotel_n->hotel_name) && $hotel_n->hotel_name)
            echo 'For, '.$hotel_n->hotel_name;
    ?></td>
    </tr>
    <tr>
      <td style="padding:10px 0 0;" colspan="2"></td>
    </tr>
  </table>
  <hr style="background: #00C5CD; border: none; height: 1px; margin-bottom:10px; width:100%;">
  <span style="font-size:9px; font-weight:400; color:#939292; display:block;"> Powered by  Royalpms.  Thank you for your stay! </span>
</div>
