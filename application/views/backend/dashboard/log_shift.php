<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light borderd">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Shift Logs </div>
    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body">
    
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr> 
          <!--<th scope="col">
                    Select
                </th>-->
          <th scope="col"> Unique Id </th>
          <th scope="col"> Date </th>
          <th scope="col"> Shift Name </th>
          <th scope="col"> Admin Name </th>
          <th scope="col"> Admin Start Time-End Time </th>
          <th scope="col"> New Bookings </th>
          <th scope="col"> Check Ins </th>
          <th scope="col"> Check Out </th>
          <th scope="col"> Opening Balance </th>
          <th scope="col"> Closing Balance </th>
          <th scope="col"> Total Transaction </th>
          
          <!--<th scope="col">
                    Broker Address
                </th>
                <th scope="col">
                    Broker Contact
                </th>
                <th scope="col">
                    Broker Email
                </th>
                <th scope="col">
                    Broker Website
                </th>

                <th scope="col">
                    Broker Pan Id
                </th>
                <th scope="col">
                    Broker Bank Account
                </th>
                <th scope="col">
                    Broker Bank IFSC Code
                </th>
                <th scope="col">
                    Broker Photo
                </th>-->
          <th scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php
            $shifts=$this->dashboard_model->all_logs();

            if(isset($shifts) && $shifts):
                $i=1;
                foreach($shifts as $shift):
                    $class = ($i%2==0) ? "active" : "success";


                    ?>
        <tr> 
          <!-- <td width="50">
                             <div class="md-checkbox pull-left">
                                 <input type="checkbox" id="checkbox1" class="md-check">
                                 <label for="checkbox1">
                                     <span></span>
                                     <span class="check"></span>
                                     <span class="box"></span>
                                 </label>
                             </div>
                         </td>-->
          
          <td><?php echo $shift->log_id ?></td>
          <td><?php echo $shift->date ?></td>
          <td><?php
                            $s_details=$this->dashboard_model->get_shift_details($shift->shift_id);
                            foreach($s_details as $s){
                                echo $s->shift_name;
                            }
                            ?></td>
          <td><?php
                            $details=$this->dashboard_model->get_user_details($shift->user_id);
                            foreach($details as $detail) {
                                echo $detail->admin_first_name." ".$detail->admin_last_name;
                                }?></td>
          <td><?php echo "(".$shift->start_time." - ".$shift->end_time.")" ?></td>
          <td><?php
                            if($shift->end_time!="00:00:00") {
                                $new_book = 0;
                                $checkin = 0;
                                $checkout = 0;
                                $bookings = $this->dashboard_model->get_shift_log($shift->date, $shift->start_time,$shift->end_time);
                                if($bookings){
                                foreach ($bookings as $booking) {


                                    $new_book = $new_book + 1;
                                    if($booking->booking_status_id==5){
                                        $checkin++;
                                    }elseif($booking->booking_status_id==6){

                                        $checkout++;

                                    }

                                }}

                                $datetime_start=$shift->date." ".$shift->start_time;
                                $datetime_end=$shift->date." ".$shift->end_time;

                                //echo $datetime_start;
                                //echo $datetime_end;
                                $income=0;
                                $transactions=$this->dashboard_model->all_transactions_by_date($datetime_start,$datetime_end);
                                if($transactions ){
                                    foreach($transactions as $ta){
                                        $income=$income+$ta->t_amount;
                                    }
                                }

                            }else{

                                $new_book = 0;
                                $checkin = 0;
                                $checkout = 0;
                                $bookings = $this->dashboard_model->get_shift_log_endless($shift->date, $shift->start_time);
                                if($bookings){
                                foreach ($bookings as $booking) {


                                    $new_book = $new_book + 1;
                                    if($booking->booking_status_id==5){
                                        $checkin++;
                                    }elseif($booking->booking_status_id==6){

                                        $checkout++;

                                    }

                                }}

                                $datetime_start=$shift->date." ".$shift->start_time;
                                $datetime_end=$shift->date." ".$shift->end_time;

                                $income=0;
                                $transactions=$this->dashboard_model->all_transactions_by_date_endless($datetime_start,$datetime_end);
                                if($transactions ){
                                    foreach($transactions as $ta){
                                        $income=$income+$ta->t_amount;
                                    }
                                }

                            }

                            ?>
            <?php echo $new_book; ?></td>
          <td><?php echo $checkin; ?></td>
          <td><?php echo $checkout; ?></td>
          <td><?php
                            $opening_ta=$this->dashboard_model->opening_balance_by_date($datetime_start);
                            $balance=0;
                            if($opening_ta){
                                foreach($opening_ta as $ta){

                                    $balance=$balance+$ta->t_amount;
                                }
                            }
                            echo $balance;
                            ?></td>
          <td><?php
                            if($shift->end_time!="00:00:00") {
                                $opening_ta = $this->dashboard_model->opening_balance_by_date($datetime_end);

                            $balance=0;
                            if($opening_ta){
                                foreach($opening_ta as $ta){

                                    $balance=$balance+$ta->t_amount;
                                }
                            }
                            echo $balance;
                            }else{
                                echo "N/A";
                            }
                            ?></td>
          <td><?php echo $income; ?></td>
          <td class="ba">
          	<div class="btn-group no-bgbut">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="<?php echo base_url() ?>dashboard/delete_shift?shift_id=<?php echo $shift->shift_id;?>" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a> </li>
                <li><a href="<?php echo base_url();?>dashboard/edit_shift?shift_id=<?php echo $shift->shift_id; ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a> </li>
              </ul>
            </div>
          </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
