<table id="assignment" class="table table-striped table-bordered  table-advance  table-hover table-responsive">
<?php  $rooms=$this->dashboard_model->all_rooms(); ?>
    <tbody>
    <?php
    foreach($rooms as $room){
        ?>

        <tr id="room<?php echo $room->room_id; ?>">
            <td style="display: none"><?php echo $room->room_id; ?></td>

            <?php  $rmm=$this->dashboard_model->room_maid_match($room->room_id);
            if($rmm){
                foreach($rmm as $assign){
                    ?>
                    <td style="display: none;">
                        <?php echo $assign->maid_id; ?>
                    </td>

                    <td class="highlight" id="div1" ondrop="drop(event)" ondragover="allowDrop(event)" >
                        <div style=" width:100%" id="drag<?php echo $assign->maid_id; ?>" ondragstart="drag(event)" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" ><?php echo $assign->maid_name ?></div>
                    </td>
                <?php } }else{

                ?>

                <td class="highlight" id="div1" ondrop="drop(event)" ondragover="allowDrop(event)" >

                    <div style="display: none"id="" value="null">
                </td>

            <?php
            }?>

        </tr>
    <?php } ?>

    </tbody>
</table>