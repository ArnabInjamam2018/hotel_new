<div class="portlet box blue">
<?php 
//echo "<pre>";

foreach($bd as $bd){
//print_r($bd);
$val = $this->dashboard_model->get_unit_exact($bd->room_id);
$fd = $this->dashboard_model->get_meal_plan_by_id($bd->food_plans);
}
//print_r($fd->name);
?>
  <div class="portlet-title"> </div>
  <div class="portlet-body form">
  	<div class="form-body">
    <table width="100%">
      <tr>
        <td width="30%" align="left" valign="top"><br>
        	<strong>Name : </strong><?php echo $bd->cust_name;?>
        </td>
        <td width="40%" align="left" valign="top">&nbsp;</td>
        <td width="30%" align="left" valign="top"><br><strong><?php echo $hotel_name->hotel_name;?></strong><br>
          <strong>DATE: </strong><?php echo $bd->booking_taking_date;?> </td>
      </tr>
    </table>
    <span style="font-size:13px; font-weight:700; color:#009595; display:block; padding:5px 5px;">Booking Details</span>
    <hr style="background: #009595; border: none; height: 1px;">
    <table width="100%" class="table table-bordered table-hover">
    <thead>
      <tr style="background: #EDEDED; color: #1b1b1b">
        <th style="text-align:center"> Product / Service Name </th>
        <th style="text-align:center"> No of Day(s) </th>
		<th style="text-align:center"> No of Room(s) </th>
        <th style="text-align:center"> Unit Price </th>
        <th style="text-align:center"> Modifier </th>
        <th style="text-align:center"> Amount </th>
        <th style="text-align:center"> Total Tax </th>
        <th style="text-align:right"> Total </th>
      </tr>
    </thead>
    <tbody style="background: #fafafa">
    	<tr>
        <td align="center" valign="middle"><?php echo $val->unit_name;?><br>
		<strong>From Date : </strong><?php echo $bd->cust_from_date_actual;?><br><strong>To Date : </strong><?php echo $bd->cust_end_date_actual;?>
		</td>
        <td align="center" valign="middle"><?php echo $bd->stay_days;?> </td>
		<td align="center" valign="middle"><?php echo $bd->number_of_room;?></td>
        <td align="center" valign="middle"><?php echo $bd->unit_room_rent;?></td>
        <td align="center" valign="middle"><?php if($bd->rent_mode_type == 'add'){echo '(+)';} else { echo '(-)'; } echo $bd->mod_room_rent;?></td>
        <td align="center" valign="middle"><?php echo $bd->room_rent_total_amount * $bd->number_of_room;?></td>
        <td align="center" valign="middle"><?php echo $bd->room_rent_tax_amount * $bd->number_of_room;?></td>
        <td align="right" valign="middle"><?php echo ($bd->room_rent_total_amount + $bd->room_rent_tax_amount) * $bd->number_of_room;?></td>
        </tr>
    	<tr>
        <td align="center" valign="middle"><strong>Meal Plan</strong><br>
		<?php echo $fd->name;?>
		</td>
        <td align="center" valign="middle"><?php echo $bd->stay_days;?></td>
		<td align="center" valign="middle"><?php echo $bd->number_of_room;?></td>
        <td align="center" valign="middle"><?php echo $bd->food_plan_uprice;?></td>
        <td align="center" valign="middle"><?php if($bd->food_plan_mod_type == 'add'){echo '(+)';} else { echo '(-)'; } echo $bd->food_plan_mod_rent;?></td>
        <td align="center" valign="middle"><?php echo $bd->food_plan_price * $bd->number_of_room;?></td>
        <td align="center" valign="middle"><?php echo $bd->food_plan_tax * $bd->number_of_room;?></td>
        <td align="right" valign="middle"><?php echo ($bd->food_plan_price + $bd->food_plan_tax) * $bd->number_of_room;?></td>
        </tr>		
        <tr>
        	<td align="right" valign="middle" colspan="8">&nbsp;</td>
        </tr>
        <tr>
      	<td align="right" valign="middle" colspan="7">Extra Person Room Rent: </td>
        <td align="right" valign="middle"><?php echo $bd->ex_per_price * $bd->number_of_room;?></td>
        </tr>
        <tr>
      	<td align="right" valign="middle" colspan="7">Extra Person Room Rent tax: </td>
        <td align="right" valign="middle"><?php echo $bd->ex_per_tax * $bd->number_of_room;?></td>
        </tr>
        <tr>
      	<td align="right" valign="middle" colspan="7">Extra Person meal Plan Price: </td>
        <td align="right" valign="middle"><?php echo $bd->ex_per_m_price * $bd->number_of_room;?></td>
        </tr>
        <tr>
      	<td align="right" valign="middle" colspan="7">Extra Person meal Plan Tax: </td>
        <td align="right" valign="middle"><?php echo $bd->ex_per_m_tax * $bd->number_of_room;?></td>
        </tr>		
        <tr>
      	<td align="right" valign="middle" colspan="7"><strong>Booking Grand Total:</strong> </td>
        <td align="right" valign="middle">INR <?php echo $bd->room_rent_sum_total * $bd->number_of_room;?></td>
        </tr>
      </tbody>
  </table>
  </div>
  <div class="form-actions right">
  	<a href="<?php echo base_url();?>enquiry/send_mail/<?php echo $bid;?>" class="btn submit">Send Enquiry</a>
  </div>
  </div>
</div>
