<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
<?php endif;?>
<div class="portlet light borderd">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-edit"></i>
			<?php if(isset($stat) && $stat ){echo $stat;}else{echo "List of Data";}?>
		</div>
		<div class="actions">
			<a class="btn btn-circle green btn-outline btn-sm" data-toggle="modal" href="#responsive"> <i class="fa fa-plus"></i>Add Marketing Personel </a>
		</div>
	</div>
	<div class="portlet-body">
			<div id="table1">
				<table class="table table-striped table-bordered table-hover" id="sample_1">
					<thead>
						<tr>
							<th scope="col"> Id </th>
							<th scope="col"> Marketing Personnel Name </th>
							<th scope="col">Contact No</th>
							<th scope="col">Email Id</th>
							<th scope="col"> Profit center</th>
							<th scope="col">Designation</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php if(isset($data) && $data):

					  $i=1;
					  foreach($data as $gst):
						  $class = ($i%2==0) ? "active" : "success";
						  $id=$gst->id;
						  ?>
						<tr id="row_<?php echo $gst->id;?>">
							<td align="center">
								<?php echo $gst->id; ?>
							</td>
							<td align="left">
								<?php echo $gst->name; ?>
							</td>
							<td align="left">
								<?php echo $gst->contact_no; ?>
							</td>
							<td align="left">
								<?php echo $gst->email; ?>
							</td>
							<td align="left">
								<?php echo $gst->profit_center; ?>
							</td>
							<td align="left">
								<?php echo $gst->designation; ?>
							</td>
							<td align="center" class="ba">
								<div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
									<ul class="dropdown-menu pull-right" role="menu">
										<li><a onclick="soft_delete('<?php echo $id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a>
										</li>
										<li><a onclick="edit_marketing_personel('<?php echo $gst->id; ?>')" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a>
										</li>
									</ul>
								</div>

							</td>
						</tr>
						<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
	</div>

	<!-- END SAMPLE TABLE PORTLET-->
</div>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add Marketing Personel</h4>
			</div>
			<?php

			$form = array(
				'class' => 'form-body',
				'id' => 'form',
				'method' => 'post'
			);

			echo form_open_multipart( 'Unit_class_controller/add_marketing_personel', $form );

			?>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<input type="text" class="form-control" name="name" id="name" required placeholder="Marketing Personel Name">
							<label></label>
							<span class="help-block">Marketing Personel Name</span> </div>
					</div>
					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<input type="text" class="form-control" name="contact_no" id="contact_no" required placeholder="Contact No">
							<label></label>
							<span class="help-block">Contact No</span> </div>
					</div>
					<div class="col-md-6">
						<div class="form-group form-md-line-input ">
							<input type="text" class="form-control" name="email_id" id="email_id" required placeholder="Email Id">
							<label></label>
							<span class="help-block">Email Id</span> </div>
					</div>
					<div class="col-md-6">
						<div class="form-group form-md-line-input ">
							<select class="form-control bs-select" id="profit_center" name="profit_center" required>
								<?php $pc=$this->dashboard_model->all_pc(); 
						//print_r($pc);
						if(isset($pc) && $pc){
							foreach($pc as $profit){	
						?>
								<option value="<?php echo $profit->profit_center_location; ?>">
									<?php echo $profit->profit_center_location; ?>
								</option>
								<?php }} ?>
							</select>
							<label></label>
							<span class="help-block">Profit Center</span> </div>
					</div>
					<div class="col-md-12">
						<div class="form-group form-md-line-input ">
							<textarea class="form-control" row="3" name="designation" placeholder="designation" id="designation"></textarea>
							<label></label>
							<span class="help-block">designation:</span> </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn default">Close</button>
				<button type="submit" class="btn green">Save</button>
			</div>
			<?php echo form_close(); ?> </div>
	</div>
</div>
<div id="edit_booking_nature_visit_modal" class="modal fade" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add Unit Type</h4>
			</div>
			<?php

			$form = array(
				'class' => 'form-body',
				'id' => 'form',
				'method' => 'post'
			);

			echo form_open_multipart( 'Unit_class_controller/update_marketing_personel', $form );

			?>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<input type="text" class="form-control" name="name1" id="name1" required placeholder="Marketing Personel Name">
							<label></label>
							<span class="help-block">Marketing Personel Name</span> </div>
					</div>
					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<input type="text" class="form-control" name="contact_no1" id="contact_no1" required placeholder="Contact No">
							<label></label>
							<span class="help-block">Contact No</span> </div>
					</div>
					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<input type="email" class="form-control" name="email1" id="email1" required placeholder="Email Id">
							<label></label>
							<span class="help-block">Email Id</span> </div>
					</div>
					<input type="hidden" name="hid" id="hid">
					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<select class="form-control bs-select" id="profit_center1" name="profit_center1" required>
								<?php $pc=$this->dashboard_model->all_pc(); 
						//print_r($pc);
						if(isset($pc) && $pc){
							foreach($pc as $profit){	
						?>
								<option value="<?php echo $profit->profit_center_location; ?>">
									<?php echo $profit->profit_center_location; ?>
								</option>
								<?php }} ?>
							</select>
							<label></label>
							<span class="help-block">Profit Center</span> </div>
					</div>
					<div class="col-md-12">
						<div class="form-group form-md-line-input">
							<textarea class="form-control" row="3" name="designation1" placeholder="designation" id="designation1"></textarea>
							<label></label>
							<span class="help-block">designation:</span> </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn default">Close</button>
				<button type="submit" class="btn green">Save</button>
			</div>
			<?php echo form_close(); ?> </div>
	</div>
</div>
<script>
	function soft_delete( id ) {
		swal( {
			title: "Are you sure?",
			text: "All the releted transactions and data will be deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {
			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>unit_class_controller/delete_marketing_personnel?f_id=" + id,
				data: {
					id: id
				},
				success: function ( data ) {

					swal( {
							title: data.data,
							text: "",
							type: "success"
						},
						function () {

							//document.getElementById("demostat"+id).innerHTML = data.data;
							$( '#row_' + id ).remove();

						} );
				}
			} );



		} );
	}







	function edit_marketing_personel( id ) {
		//alert(id);

		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>unit_class_controller/edit_marketing_personel",
			data: {
				id: id
			},
			success: function ( data ) {
				//alert(data.id);
				$( '#hid' ).val( data.id );

				$( '#name1' ).val( data.name );
				$( '#contact_no1' ).val( data.contact_no );
				$( '#email1' ).val( data.email );
				$( '#profit_center1' ).val( data.profit_center );
				$( '#designation1' ).val( data.designation );

				// alert(data.name);

				$( '#edit_booking_nature_visit_modal' ).modal( 'toggle' );

			}
		} );
	}
</script>