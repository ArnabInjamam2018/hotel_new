<table id="logo-name">
	<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel')); ?>
	<tr>
		<td align="left" valign="middle"><img src="<?php echo base_url();?>upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/>
		</td>
		<td align="right" valign="middle">
			<?php echo "<strong style='font-size:18px;'>".$hotel_name->hotel_name.'</font></strong>'?>
		</td>
	</tr>

	<tr>
		<td width="100%" colspan="2">
			<hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;">
		</td>
	</tr>
	<tr>
		<td colspan="2"><strong>Date:</strong>
			<?php echo date('D-M-Y'); ?>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2">&nbsp;</td>
	</tr>
</table>
<div class="portlet light borderd">
	<div class="portlet-title">
		<div class="caption"> <i class="glyphicon glyphicon-bed"></i>Monthly Summary Report </div>
		<div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
	</div>
	<div class="portlet-body">
		<table class="table table-striped table-bordered table-hover " id="sample_2">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Month</th>
					<th scope="col">Day</th>
					<th scope="col">Room Count</th>
					<th scope="col">Occupancy %</th>
					<th scope="col">Room Rent</th>
					<th scope="col">Meal Rent</th>
					<th scope="col">POS</th>
					<th scope="col">Adjustment Amount</th>
					<th scope="col">Disc Amount</th>
					<th scope="col">Laundry Amount</th>
					<th scope="col">Room Inclusion</th>
					<th scope="col">Avg.Daily Rate (Incl.Inclusion)</th>
					<th scope="col">Total Taxes</th>
					<th scope="col">Gross total</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<?php
					$mp = 0.00;
					$a_date = date( "Y-01-01" );
					$maxDays = date( 't' );
					$sqlTotal = 0;
					$sum2 = 0;
					$sum3 = 0;
					$tax = 0;
					$gross = 0;
					$lAmt = 0;
					$start_date = date( "Y-01-01" );
					$end_date = date( 'Y-01-t' );



					$sql = $this->dashboard_model->roomcount( $start_date, $end_date );
					if ( isset( $sql ) && $sql ) {
						$sqlTotal = $sql->id;
					} else {
						$sqlTotal = 0;
					}
					$avgdailyrent = $this->dashboard_model->getBookingLineItem_byDate1( $start_date, $end_date );
					$pos = $this->dashboard_model->getPosAmt( $start_date, $end_date );
					if ( isset( $pos ) && $pos ) {
						$posAmt = $pos->tot_Amt;
					} else {
						$posAmt = 0;
					}
					$AdjstAmt = $this->dashboard_model->getadjAmt( $start_date, $end_date );
					if ( isset( $AdjstAmt ) && $AdjstAmt ) {
						$Adjst = $AdjstAmt->total;
					} else {
						$Adjst = 0;
					}


					$disc = $this->dashboard_model->getDiscAmt( $start_date, $end_date );
					if ( isset( $disc ) && $disc ) {
						$disAmt = $disc->total;
					} else {
						$disAmt = 0;
					}


					$extra = $this->dashboard_model->getExtraChargeAmt( $start_date, $end_date );
					if ( isset( $extra ) && $extra ) {
						$exAmt = $extra->total;
					} else {
						$exAmt = 0;
					}
					$laundry = $this->dashboard_model->getLaundry_byDate1( $start_date, $end_date );

					if ( isset( $laundry ) && $laundry ) {
						$lAmt = $laundry->total;
					} else {
						$lAmt = 0;
					}


					$sum3 = $exAmt + $posAmt + $Adjst + $lAmt;
					if ( isset( $avgdailyrent ) && $avgdailyrent ) {
						foreach ( $avgdailyrent as $av ) {

							$sum2 = $sum2 + $av->roomRent;
							$mp = $mp + $av->mealPlan;
							//	$sum3=$sum3+$av->extra_service_total_amount;
							$tax = $tax + $av->rrTax + $av->mpTax;

						}
						$gross = $sum2 + $mp + $tax + $sum3 - $disAmt;
					}


					?>
					<td align="center">
						<?php  echo 1;?>
					</td>
					<td align="center">
						<a href="<?php echo base_url();?>reports/monthly_summ_r2?m=<?php echo 1?>">
							<?php  echo date("F", strtotime($a_date)); ?> </a>
						<?php echo " ".date("Y", strtotime($a_date))?>
					</td>
					<td align="center">
						<?php echo date("t", strtotime($a_date)); ?>
					</td>
					<td align="center">
						<?php echo $totalrooms;?>
					</td>
					<td align="center">
						<?php
						//Occupancy
						echo round( ( $sqlTotal * 100 ) / ( $maxDays * $totalrooms ), 2 ) . '%';
						?>
					</td>
					<td align="center">
						<?php 
					//Room rent total
							if(isset($sum2) || $sum2>0){ echo $sum2;} else{ echo $sum2=0.00;};?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $mp ) && $mp > 0 )echo $mp;
						?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $posAmt ) && $posAmt > 0 ) {
							echo $posAmt;
						} else {
							echo 0;
						}
						?>
					</td>

					<td align="center">
						<?php if(isset($Adjst)){ echo $Adjst;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($disAmt) && $disAmt>0){ echo $disAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($lAmt) && $lAmt>0){ echo $lAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($sum3) && $sum3>0){ echo $sum3;}else{ echo 0;} ?>
					</td>

					<?php 
					
					if($sqlTotal==0){$sqlTotal=1;}
					$avg=$sum2/$sqlTotal;

					?>
					<td align="center">
						<?php if($avg>0) echo round($avg,2);?>
					</td>
					<td align="center">
						<?php
						//Total Tax
						if ( isset( $tax ) && $tax ) {
							echo $tax;
						} else {
							echo $tax = 0.00;
						};
						?>
					</td>
					<td align="center">
						<?php if($sum2>0 || $tax>0){echo $gross;}else{  echo "0.00";}?>
					</td>
				</tr>

				<tr>
					<?php   $mp=0.00;
					$a_date = date("Y-02-01");
                    $maxDays=date('t');
					$sqlTotal=0;
					$sum2=0;
					$sum3=0;
					$tax=0;
					$gross=0;
					$start_date=$a_date;
					$end_date=date('Y-02-t');
                 
                        $sql= $this->dashboard_model->roomcount($start_date,$end_date);
						if(isset($sql) && $sql){
							$sqlTotal=$sql->id;
						}else{
							$sqlTotal=0;
						}
						$avgdailyrent=$this->dashboard_model->getBookingLineItem_byDate1($start_date,$end_date);
						$pos=$this->dashboard_model->getPosAmt($start_date,$end_date);
						if(isset($pos) && $pos){
						$posAmt=$pos->tot_Amt;
						}else{
							$posAmt=0;
						}
						$AdjstAmt=$this->dashboard_model->getadjAmt($start_date,$end_date);
						if(isset($AdjstAmt) && $AdjstAmt){
						$Adjst=$AdjstAmt->total;
						}else{
							$Adjst=0;
						}
						
						
						$disc=$this->dashboard_model->getDiscAmt($start_date,$end_date);
						if(isset($disc) && $disc){
						$disAmt=$disc->total;
						}else{
							$disAmt=0;
						}
						
						
						$extra=$this->dashboard_model->getExtraChargeAmt($start_date,$end_date);
						if(isset($extra) && $extra){
						$exAmt=$extra->total;
						}else{
							$exAmt=0;
						}
						$laundry=$this->dashboard_model->getLaundry_byDate1($start_date,$end_date);
						
						if(isset($laundry) && $laundry){
						$lAmt=$laundry->total;
						}else{
							$lAmt=0;
						}
					   // $sqlTotal=$sql->id;
						$sum3=$exAmt+$posAmt+$Adjst+$lAmt;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									 
							     $sum2=$sum2+$av->roomRent; 
							     $mp=$mp+$av->mealPlan; 
								//	$sum3=$sum3+$av->extra_service_total_amount;
									$tax=$tax+$av->rrTax+$av->mpTax;
								
							}
							$gross=$sum2+$mp+$tax+$sum3-$disAmt;
							}
							
					
						?>
					<td align="center">
						<?php  echo 2; ?>
					</td>
					<td align="center">
						<a href="<?php echo base_url();?>reports/monthly_summ_r2?m=<?php echo 2?>">
							<?php  echo date("F", strtotime($a_date)); ?> </a>
						<?php echo " ".date("Y", strtotime($a_date))?>
					</td>
					<td align="center">
						<?php echo date("t", strtotime($a_date));?>
					</td>
					<td align="center">
						<?php //Room Count
										echo $totalrooms;?>
					</td>
					<td align="center">
						<?php
						//Occupancy
						echo round( ( $sqlTotal * 100 ) / ( $maxDays * $totalrooms ), 2 ) . '%';
						?>
					</td>
					<td align="center">
						<?php 
					//Room rent total
							if(isset($sum2) || $sum2>0){ echo $sum2;} else{ echo $sum2=0.00;};?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $mp ) && $mp > 0 )echo $mp;
						?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $posAmt ) && $posAmt > 0 ) {
							echo $posAmt;
						} else {
							echo 0;
						}
						?>
					</td>

					<td align="center">
						<?php if(isset($Adjst)){ echo $Adjst;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($disAmt) && $disAmt>0){ echo $disAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($lAmt) && $lAmt>0){ echo $lAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($sum3) && $sum3>0){ echo $sum3;}else{ echo 0;} ?>
					</td>

					<?php 
					
					if($sqlTotal==0){$sqlTotal=1;}
					$avg=$sum2/$sqlTotal;

					?>
					<td align="center">
						<?php if($avg>0) echo round($avg,2);?>
					</td>
					<td align="center">
						<?php
						//Total Tax
						if ( isset( $tax ) && $tax ) {
							echo $tax;
						} else {
							echo $tax = 0.00;
						};
						?>
					</td>
					<td align="center">
						<?php if($sum2>0 || $tax>0){echo $gross;}else{  echo "0.00";}?>
					</td>
				</tr>

				<tr>
					<?php   $mp=0.00;
				$a_date = date("Y-03-01");
                    $maxDays=date('t');
					$sqlTotal=0;
					$sum2=0;
					$sum3=0;
					$tax=0;
					$gross=0;
					$start_date=$a_date;
					$end_date=date('Y-03-t');
                  $sql= $this->dashboard_model->roomcount($start_date,$end_date);
					if(isset($sql) && $sql){
							$sqlTotal=$sql->id;
						}else{
							$sqlTotal=0;
						}
						$avgdailyrent=$this->dashboard_model->getBookingLineItem_byDate1($start_date,$end_date);
						$pos=$this->dashboard_model->getPosAmt($start_date,$end_date);
						if(isset($pos) && $pos){
						$posAmt=$pos->tot_Amt;
						}else{
							$posAmt=0;
						}
						$AdjstAmt=$this->dashboard_model->getadjAmt($start_date,$end_date);
						if(isset($AdjstAmt) && $AdjstAmt){
						$Adjst=$AdjstAmt->total;
						}else{
							$Adjst=0;
						}
						
						
						$disc=$this->dashboard_model->getDiscAmt($start_date,$end_date);
						if(isset($disc) && $disc){
						$disAmt=$disc->total;
						}else{
							$disAmt=0;
						}
						
						
						$extra=$this->dashboard_model->getExtraChargeAmt($start_date,$end_date);
						if(isset($extra) && $extra){
						$exAmt=$extra->total;
						}else{
							$exAmt=0;
						}
						
						$laundry=$this->dashboard_model->getLaundry_byDate1($start_date,$end_date);
						
						if(isset($laundry) && $laundry){
						$lAmt=$laundry->total;
						}else{
							$lAmt=0;
						}
					   // $sqlTotal=$sql->id;
						$sum3=$exAmt+$posAmt+$Adjst+$lAmt;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									 
							     $sum2=$sum2+$av->roomRent; 
							     $mp=$mp+$av->mealPlan; 
								//	$sum3=$sum3+$av->extra_service_total_amount;
									$tax=$tax+$av->rrTax+$av->mpTax;
								
							}
							$gross=$sum2+$mp+$tax+$sum3-$disAmt;
							}
							
					
						?>
					<td align="center">
						<?php  echo 3; ?>
					</td>
					<td align="center">
						<a href="<?php echo base_url();?>reports/monthly_summ_r2?m=<?php echo 3?>">
							<?php  echo date("F", strtotime($a_date)); ?> </a>
						<?php echo " ".date("Y", strtotime($a_date))?>
					</td>
					<td align="center">
						<?php echo date("t", strtotime($a_date));?>
					</td>
					<td align="center">
						<?php //Room Count
										echo $totalrooms;?>
					</td>
					<td align="center">
						<?php
						//Occupancy
						echo round( ( $sqlTotal * 100 ) / ( $maxDays * $totalrooms ), 2 ) . '%';
						?>
					</td>
					<td align="center">
						<?php 
					//Room rent total
							if(isset($sum2) || $sum2>0){ echo $sum2;} else{ echo $sum2=0.00;};?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $mp ) && $mp > 0 )echo $mp;
						?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $posAmt ) && $posAmt > 0 ) {
							echo $posAmt;
						} else {
							echo 0;
						}
						?>
					</td>

					<td align="center">
						<?php if(isset($Adjst)){ echo $Adjst;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($disAmt) && $disAmt>0){ echo $disAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($lAmt) && $lAmt>0){ echo $lAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($sum3) && $sum3>0){ echo $sum3;}else{ echo 0;} ?>
					</td>

					<?php 
					
					if($sqlTotal==0){$sqlTotal=1;}
					$avg=$sum2/$sqlTotal;

					?>
					<td align="center">
						<?php if($avg>0) echo round($avg,2);?>
					</td>
					<td align="center">
						<?php
						//Total Tax
						if ( isset( $tax ) && $tax ) {
							echo $tax;
						} else {
							echo $tax = 0.00;
						};
						?>
					</td>
					<td align="center">
						<?php if($sum2>0 || $tax>0){echo $gross;}else{  echo "0.00";}?>
					</td>
				</tr>

				<!--error-->
				<tr>
					<?php   $mp=0.00;
				   $a_date = date("Y-04-01");
                    $maxDays=date('t');
					$sqlTotal=0;
					$sum2=0;
					$sum3=0;
					$tax=0;
					$gross=0;
					$start_date=$a_date;
					$end_date=date('Y-04-t');
                 
                        $sql= $this->dashboard_model->roomcount($start_date,$end_date);
						if(isset($sql) && $sql){
							$sqlTotal=$sql->id;
						}else{
							$sqlTotal=0;
						}
						$avgdailyrent=$this->dashboard_model->getBookingLineItem_byDate1($start_date,$end_date);
						$pos=$this->dashboard_model->getPosAmt($start_date,$end_date);
						if(isset($pos) && $pos){
						$posAmt=$pos->tot_Amt;
						}else{
							$posAmt=0;
						}
						$AdjstAmt=$this->dashboard_model->getadjAmt($start_date,$end_date);
						if(isset($AdjstAmt) && $AdjstAmt){
						$Adjst=$AdjstAmt->total;
						}else{
							$Adjst=0;
						}
						
						
						$disc=$this->dashboard_model->getDiscAmt($start_date,$end_date);
						if(isset($disc) && $disc){
						$disAmt=$disc->total;
						}else{
							$disAmt=0;
						}
						
						
						$extra=$this->dashboard_model->getExtraChargeAmt($start_date,$end_date);
						if(isset($extra) && $extra){
						$exAmt=$extra->total;
						}else{
							$exAmt=0;
						}
					   $laundry=$this->dashboard_model->getLaundry_byDate1($start_date,$end_date);
						
						if(isset($laundry) && $laundry){
						$lAmt=$laundry->total;
						}else{
							$lAmt=0;
						}
						$sum3=$exAmt+$posAmt+$Adjst+$lAmt;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									 
							     $sum2=$sum2+$av->roomRent; 
							     $mp=$mp+$av->mealPlan; 
								$tax=$tax+$av->rrTax+$av->mpTax;
								
							}
							$gross=$sum2+$mp+$tax+$sum3-$disAmt;
							}
							
					
						?>
					<td align="center">
						<?php  echo 4; ?>
					</td>
					<td align="center">
						<a href="<?php echo base_url();?>reports/monthly_summ_r2?m=<?php echo 4;?>">
							<?php  echo date("F", strtotime($a_date)); ?> </a>
						<?php echo " ".date("Y", strtotime($a_date))?>
					</td>
					<td align="center">
						<?php echo date("t", strtotime($a_date));?>
					</td>
					<td align="center">
						<?php echo $totalrooms;?>
					</td>
					<td align="center">
						<?php
						echo round( ( $sqlTotal * 100 ) / ( $maxDays * $totalrooms ), 2 ) . '%';
						?>
					</td>
					<td align="center">
						<?php 
							if(isset($sum2) || $sum2>0){ echo $sum2;} else{ echo $sum2=0.00;};?>
					</td>
					<td align="center">
						<?php
						if ( isset( $mp ) && $mp > 0 )echo $mp;
						?>
					</td>
					<td align="center">
						<?php
						if ( isset( $posAmt ) && $posAmt > 0 ) {
							echo $posAmt;
						} else {
							echo 0;
						}
						?>
					</td>

					<td align="center">
						<?php if(isset($Adjst)){ echo $Adjst;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($disAmt) && $disAmt>0){ echo $disAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($lAmt) && $lAmt>0){ echo $lAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($sum3) && $sum3>0){ echo $sum3;}else{ echo 0;} ?>
					</td>

					<?php 
					
					if($sqlTotal==0){$sqlTotal=1;}
					$avg=$sum2/$sqlTotal;

					?>
					<td align="center">
						<?php if($avg>0) echo round($avg,2);?>
					</td>
					<td align="center">
						<?php
						//Total Tax
						if ( isset( $tax ) && $tax ) {
							echo $tax;
						} else {
							echo $tax = 0.00;
						};
						?>
					</td>
					<td align="center">
						<?php if($sum2>0 || $tax>0){echo $gross;}else{  echo "0.00";}?>
					</td>
				</tr>

				<tr>
					<?php
					$mp = 0.00;
					$a_date = date( "Y-05-01" );
					$maxDays = date( 't' );
					$sqlTotal = 0;
					$sum2 = 0;
					$sum3 = 0;
					$tax = 0;
					$gross = 0;
					$posAmt = 0;
					$start_date = date( 'Y-05-01' );
					$end_date = date( 'Y-05-t' );
					/*  for($i=1; $i<=$maxDays; $i++){
                        if($i<10){
                          $val= $y.'-'.$m.'-'.'0'.$i;  
                        }else{
                          $val= $y.'-'.$m.'-'.$i; 
                        } 
					}*/
					$sql = $this->dashboard_model->roomcount( $start_date, $end_date );
					if ( isset( $sql ) && $sql ) {
						$sqlTotal = $sql->id;
					} else {
						$sqlTotal = 0;
					}
					$avgdailyrent = $this->dashboard_model->getBookingLineItem_byDate1( $start_date, $end_date );
					$pos = $this->dashboard_model->getPosAmt( $start_date, $end_date );
					if ( isset( $pos ) && $pos ) {
						$posAmt = $pos->tot_Amt;
					} else {
						$posAmt = 0;
					}
					$AdjstAmt = $this->dashboard_model->getadjAmt( $start_date, $end_date );
					if ( isset( $AdjstAmt ) && $AdjstAmt ) {
						$Adjst = $AdjstAmt->total;
					} else {
						$Adjst = 0;
					}


					$disc = $this->dashboard_model->getDiscAmt( $start_date, $end_date );
					if ( isset( $disc ) && $disc ) {
						$disAmt = $disc->total;
					} else {
						$disAmt = 0;
					}


					$extra = $this->dashboard_model->getExtraChargeAmt( $start_date, $end_date );
					if ( isset( $extra ) && $extra ) {
						$exAmt = $extra->total;
					} else {
						$exAmt = 0;
					}
					$laundry = $this->dashboard_model->getLaundry_byDate1( $start_date, $end_date );

					if ( isset( $laundry ) && $laundry ) {
						$lAmt = $laundry->total;
					} else {
						$lAmt = 0;
					}

					$sum3 = $exAmt + $posAmt + $Adjst + $lAmt;
					if ( isset( $avgdailyrent ) && $avgdailyrent ) {
						foreach ( $avgdailyrent as $av ) {

							$sum2 = $sum2 + $av->roomRent;
							$mp = $mp + $av->mealPlan;
							//	$sum3=$sum3+$av->extra_service_total_amount;
							$tax = $tax + $av->rrTax + $av->mpTax;

						}
						$gross = $sum2 + $mp + $tax + $sum3 - $disAmt;
					}
					?>
					<td align="center">
						<?php  echo 5; ?>
					</td>
					<td align="center">
						<a href="<?php echo base_url();?>reports/monthly_summ_r2?m=<?php echo 5?>">
							<?php  echo date("F", strtotime($a_date)); ?> </a>
						<?php echo " ".date("Y", strtotime($a_date))?>
					</td>
					<td align="center">
						<?php echo date("t", strtotime($a_date));?>
					</td>
					<td align="center">
						<?php //Room Count
										echo $totalrooms;?>
					</td>
					<td align="center">
						<?php
						//Occupancy
						echo round( ( $sqlTotal * 100 ) / ( $maxDays * $totalrooms ), 2 ) . '%';
						?>
					</td>
					<td align="center">
						<?php 
					//Room rent total
							if(isset($sum2) || $sum2>0){ echo $sum2;} else{ echo $sum2=0.00;};?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $mp ) && $mp > 0 )echo $mp;
						?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $posAmt ) && $posAmt > 0 ) {
							echo $posAmt;
						} else {
							echo 0;
						}
						?>
					</td>

					<td align="center">
						<?php if(isset($Adjst)){ echo $Adjst;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($disAmt) && $disAmt>0){ echo $disAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($lAmt) && $lAmt>0){ echo $lAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($sum3) && $sum3>0){ echo $sum3;}else{ echo 0;} ?>
					</td>

					<?php 
					
					if($sqlTotal==0){$sqlTotal=1;}
					$avg=$sum2/$sqlTotal;

					?>
					<td align="center">
						<?php if($avg>0) echo round($avg,2);?>
					</td>
					<td align="center">
						<?php
						//Total Tax
						if ( isset( $tax ) && $tax ) {
							echo $tax;
						} else {
							echo $tax = 0.00;
						};
						?>
					</td>
					<td align="center">
						<?php if($sum2>0 || $tax>0){echo $gross;}else{  echo "0.00";}?>
					</td>
				</tr>

				<tr>
					<?php   $mp=0.00;
				 $a_date = date("Y-06-01");
                    $maxDays=date('t');
					$sqlTotal=0;
					$sum2=0;
					$sum3=0;
					$tax=0;
					$gross=0;
					$start_date= $a_date;
					$end_date=date('Y-06-t');
                  /*  for($i=1; $i<=$maxDays; $i++){
                        if($i<10){
                          $val= $y.'-'.$m.'-'.'0'.$i;  
                        }else{
                          $val= $y.'-'.$m.'-'.$i; 
                        } 
					}*/
                        $sql= $this->dashboard_model->roomcount($start_date,$end_date);
						if(isset($sql) && $sql){
							$sqlTotal=$sql->id;
						}else{
							$sqlTotal=0;
						}
						$avgdailyrent=$this->dashboard_model->getBookingLineItem_byDate1($start_date,$end_date);
						$pos=$this->dashboard_model->getPosAmt($start_date,$end_date);
						if(isset($pos) && $pos){
						$posAmt=$pos->tot_Amt;
						}else{
							$posAmt=0;
						}
						$AdjstAmt=$this->dashboard_model->getadjAmt($start_date,$end_date);
						if(isset($AdjstAmt) && $AdjstAmt){
						$Adjst=$AdjstAmt->total;
						}else{
							$Adjst=0;
						}
						
						
						$disc=$this->dashboard_model->getDiscAmt($start_date,$end_date);
						if(isset($disc) && $disc){
						$disAmt=$disc->total;
						}else{
							$disAmt=0;
						}
						
						
						$extra=$this->dashboard_model->getExtraChargeAmt($start_date,$end_date);
						if(isset($extra) && $extra){
						$exAmt=$extra->total;
						}else{
							$exAmt=0;
						}
					  
					  $laundry=$this->dashboard_model->getLaundry_byDate1($start_date,$end_date);
						
						if(isset($laundry) && $laundry){
						$lAmt=$laundry->total;
						}else{
							$lAmt=0;
						}
						$sum3=$exAmt+$posAmt+$Adjst+$lAmt;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									 
							     $sum2=$sum2+$av->roomRent; 
							     $mp=$mp+$av->mealPlan; 
								//	$sum3=$sum3+$av->extra_service_total_amount;
									$tax=$tax+$av->rrTax+$av->mpTax;
								
							}
							$gross=$sum2+$mp+$tax+$sum3-$disAmt;
							}
					
						?>
					<td align="center">
						<?php  echo 6; ?>
					</td>
					<td align="center">
						<a href="<?php echo base_url();?>reports/monthly_summ_r2?m=<?php echo 6?>">
							<?php  echo date("F", strtotime($a_date)); ?> </a>
						<?php echo " ".date("Y", strtotime($a_date))?>
					</td>
					<td align="center">
						<?php echo date("t", strtotime($a_date));?>
					</td>
					<td align="center">
						<?php //Room Count
										echo $totalrooms;?>
					</td>
					<td align="center">
						<?php
						//Occupancy
						echo round( ( $sqlTotal * 100 ) / ( $maxDays * $totalrooms ), 2 ) . '%';
						?>
					</td>
					<td align="center">
						<?php 
					//Room rent total
							if(isset($sum2) || $sum2>0){ echo $sum2;} else{ echo $sum2=0.00;};?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $mp ) && $mp > 0 )echo $mp;
						?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $posAmt ) && $posAmt > 0 ) {
							echo $posAmt;
						} else {
							echo 0;
						}
						?>
					</td>

					<td align="center">
						<?php if(isset($Adjst)){ echo $Adjst;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($disAmt) && $disAmt>0){ echo $disAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($lAmt) && $lAmt>0){ echo $lAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($sum3) && $sum3>0){ echo $sum3;}else{ echo 0;} ?>
					</td>

					<?php 
					
					if($sqlTotal==0){$sqlTotal=1;}
					$avg=$sum2/$sqlTotal;

					?>
					<td align="center">
						<?php if($avg>0) echo round($avg,2);?>
					</td>
					<td align="center">
						<?php
						//Total Tax
						if ( isset( $tax ) && $tax ) {
							echo $tax;
						} else {
							echo $tax = 0.00;
						};
						?>
					</td>
					<td align="center">
						<?php if($sum2>0 || $tax>0){echo $gross;}else{  echo "0.00";}?>
					</td>
				</tr>

				<tr>
					<?php   $mp=0.00;
				    $a_date = date("Y-07-01");
                    $maxDays=date('t');
					$sqlTotal=0;
					$sum2=0;
					$sum3=0;
					$tax=0;
					$gross=0;
					$start_date=$a_date;
					$end_date=date('Y-07-t');
                  /*  for($i=1; $i<=$maxDays; $i++){
                        if($i<10){
                          $val= $y.'-'.$m.'-'.'0'.$i;  
                        }else{
                          $val= $y.'-'.$m.'-'.$i; 
                        } 
					}*/
                        $sql= $this->dashboard_model->roomcount($start_date,$end_date);
						if(isset($sql) && $sql){
							$sqlTotal=$sql->id;
						}else{
							$sqlTotal=0;
						}
						$avgdailyrent=$this->dashboard_model->getBookingLineItem_byDate1($start_date,$end_date);
						$pos=$this->dashboard_model->getPosAmt($start_date,$end_date);
						if(isset($pos) && $pos){
						$posAmt=$pos->tot_Amt;
						}else{
							$posAmt=0;
						}
						$AdjstAmt=$this->dashboard_model->getadjAmt($start_date,$end_date);
						if(isset($AdjstAmt) && $AdjstAmt){
						$Adjst=$AdjstAmt->total;
						}else{
							$Adjst=0;
						}
						
						
						$disc=$this->dashboard_model->getDiscAmt($start_date,$end_date);
						if(isset($disc) && $disc){
						$disAmt=$disc->total;
						}else{
							$disAmt=0;
						}
						
						
						$extra=$this->dashboard_model->getExtraChargeAmt($start_date,$end_date);
						if(isset($extra) && $extra){
						$exAmt=$extra->total;
						}else{
							$exAmt=0;
						}
					   
					   $laundry=$this->dashboard_model->getLaundry_byDate1($start_date,$end_date);
						
						if(isset($laundry) && $laundry){
						$lAmt=$laundry->total;
						}else{
							$lAmt=0;
						}
						$sum3=$exAmt+$posAmt+$Adjst+$lAmt;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									 
							     $sum2=$sum2+$av->roomRent; 
							     $mp=$mp+$av->mealPlan; 
								//	$sum3=$sum3+$av->extra_service_total_amount;
									$tax=$tax+$av->rrTax+$av->mpTax;
								
							}
							$gross=$sum2+$mp+$tax+$sum3-$disAmt;
							}
					
						?>
					<td align="center">
						<?php  echo 7; ?>
					</td>
					<td align="center">
						<a href="<?php echo base_url();?>reports/monthly_summ_r2?m=<?php echo 7?>">
							<?php  echo date("F", strtotime($a_date)); ?> </a>
						<?php echo " ".date("Y", strtotime($a_date))?>
					</td>
					<td align="center">
						<?php echo date("t", strtotime($a_date));?>
					</td>
					<td align="center">
						<?php //Room Count
										echo $totalrooms;?>
					</td>
					<td align="center">
						<?php
						//Occupancy
						echo round( ( $sqlTotal * 100 ) / ( $maxDays * $totalrooms ), 2 ) . '%';
						?>
					</td>
					<td align="center">
						<?php 
					//Room rent total
							if(isset($sum2) || $sum2>0){ echo $sum2;} else{ echo $sum2=0.00;};?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $mp ) && $mp > 0 )echo $mp;
						?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $posAmt ) && $posAmt > 0 ) {
							echo $posAmt;
						} else {
							echo 0;
						}
						?>
					</td>

					<td align="center">
						<?php if(isset($Adjst)){ echo $Adjst;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($disAmt) && $disAmt>0){ echo $disAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($lAmt) && $lAmt>0){ echo $lAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($sum3) && $sum3>0){ echo $sum3;}else{ echo 0;} ?>
					</td>

					<?php 
					
					if($sqlTotal==0){$sqlTotal=1;}
					$avg=$sum2/$sqlTotal;

					?>
					<td align="center">
						<?php if($avg>0) echo round($avg,2);?>
					</td>
					<td align="center">
						<?php
						//Total Tax
						if ( isset( $tax ) && $tax ) {
							echo $tax;
						} else {
							echo $tax = 0.00;
						};
						?>
					</td>
					<td align="center">
						<?php if($sum2>0 || $tax>0){echo $gross;}else{  echo "0.00";}?>
					</td>
				</tr>

				<tr>
					<?php   $mp=0.00;
				 $a_date = date("Y-08-01");
                    $maxDays=date('t');
					$sqlTotal=0;
					$sum2=0;
					$sum3=0;
					$tax=0;
					$gross=0;
					$start_date= $a_date;
					$end_date=date('Y-08-t');
                  /*  for($i=1; $i<=$maxDays; $i++){
                        if($i<10){
                          $val= $y.'-'.$m.'-'.'0'.$i;  
                        }else{
                          $val= $y.'-'.$m.'-'.$i; 
                        } 
					}*/
                        $sql= $this->dashboard_model->roomcount($start_date,$end_date);
						if(isset($sql) && $sql){
							$sqlTotal=$sql->id;
						}else{
							$sqlTotal=0;
						}
						$avgdailyrent=$this->dashboard_model->getBookingLineItem_byDate1($start_date,$end_date);
						$pos=$this->dashboard_model->getPosAmt($start_date,$end_date);
						if(isset($pos) && $pos){
						$posAmt=$pos->tot_Amt;
						}else{
							$posAmt=0;
						}
						$AdjstAmt=$this->dashboard_model->getadjAmt($start_date,$end_date);
						if(isset($AdjstAmt) && $AdjstAmt){
						$Adjst=$AdjstAmt->total;
						}else{
							$Adjst=0;
						}
						
						
						$disc=$this->dashboard_model->getDiscAmt($start_date,$end_date);
						if(isset($disc) && $disc){
						$disAmt=$disc->total;
						}else{
							$disAmt=0;
						}
						
						
						$extra=$this->dashboard_model->getExtraChargeAmt($start_date,$end_date);
						if(isset($extra) && $extra){
						$exAmt=$extra->total;
						}else{
							$exAmt=0;
						}
						$laundry=$this->dashboard_model->getLaundry_byDate1($start_date,$end_date);
						
						if(isset($laundry) && $laundry){
						$lAmt=$laundry->total;
						}else{
							$lAmt=0;
						}
					   
						$sum3=$exAmt+$posAmt+$Adjst+$lAmt;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									 
							     $sum2=$sum2+$av->roomRent; 
							     $mp=$mp+$av->mealPlan; 
								//	$sum3=$sum3+$av->extra_service_total_amount;
									$tax=$tax+$av->rrTax+$av->mpTax;
								
							}
							$gross=$sum2+$mp+$tax+$sum3-$disAmt;
							}
					
						?>
					<td align="center">
						<?php  echo 8; ?>
					</td>
					<td align="center">
						<a href="<?php echo base_url();?>reports/monthly_summ_r2?m=<?php echo 8?>">
							<?php  echo date("F", strtotime($a_date)); ?> </a>
						<?php echo " ".date("Y", strtotime($a_date))?>
					</td>
					<td align="center">
						<?php echo date("t", strtotime($a_date));?>
					</td>
					<td align="center">
						<?php //Room Count
										echo $totalrooms;?>
					</td>
					<td align="center">
						<?php
						//Occupancy
						echo round( ( $sqlTotal * 100 ) / ( $maxDays * $totalrooms ), 2 ) . '%';
						?>
					</td>
					<td align="center">
						<?php 
					//Room rent total
							if(isset($sum2) || $sum2>0){ echo $sum2;} else{ echo $sum2=0.00;};?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $mp ) && $mp > 0 )echo $mp;
						?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $posAmt ) && $posAmt > 0 ) {
							echo $posAmt;
						} else {
							echo 0;
						}
						?>
					</td>

					<td align="center">
						<?php if(isset($Adjst)){ echo $Adjst;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($disAmt) && $disAmt>0){ echo $disAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($lAmt) && $lAmt>0){ echo $lAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($sum3) && $sum3>0){ echo $sum3;}else{ echo 0;} ?>
					</td>

					<?php 
					
					if($sqlTotal==0){$sqlTotal=1;}
					$avg=$sum2/$sqlTotal;

					?>
					<td align="center">
						<?php if($avg>0) echo round($avg,2);?>
					</td>
					<td align="center">
						<?php
						//Total Tax
						if ( isset( $tax ) && $tax ) {
							echo $tax;
						} else {
							echo $tax = 0.00;
						};
						?>
					</td>
					<td align="center">
						<?php if($sum2>0 || $tax>0){echo $gross;}else{  echo "0.00";}?>
					</td>
				</tr>

				<tr>
					<?php   $mp=0.00;
				 $a_date = date("Y-09-01");
                    $maxDays=date('t');
					$sqlTotal=0;
					$sum2=0;
					$sum3=0;
					$tax=0;
					$gross=0;
					$start_date=$a_date;
					$end_date=date('Y-09-t');
                  /*  for($i=1; $i<=$maxDays; $i++){
                        if($i<10){
                          $val= $y.'-'.$m.'-'.'0'.$i;  
                        }else{
                          $val= $y.'-'.$m.'-'.$i; 
                        } 
					}*/
                        $sql= $this->dashboard_model->roomcount($start_date,$end_date);
						if(isset($sql) && $sql){
							$sqlTotal=$sql->id;
						}else{
							$sqlTotal=0;
						}
						$avgdailyrent=$this->dashboard_model->getBookingLineItem_byDate1($start_date,$end_date);
						$pos=$this->dashboard_model->getPosAmt($start_date,$end_date);
						if(isset($pos) && $pos){
						$posAmt=$pos->tot_Amt;
						}else{
							$posAmt=0;
						}
						$AdjstAmt=$this->dashboard_model->getadjAmt($start_date,$end_date);
						if(isset($AdjstAmt) && $AdjstAmt){
						$Adjst=$AdjstAmt->total;
						}else{
							$Adjst=0;
						}
						
						
						$disc=$this->dashboard_model->getDiscAmt($start_date,$end_date);
						if(isset($disc) && $disc){
						$disAmt=$disc->total;
						}else{
							$disAmt=0;
						}
						
						
						$extra=$this->dashboard_model->getExtraChargeAmt($start_date,$end_date);
						if(isset($extra) && $extra){
						$exAmt=$extra->total;
						}else{
							$exAmt=0;
						}
						$laundry=$this->dashboard_model->getLaundry_byDate1($start_date,$end_date);
						
						if(isset($laundry) && $laundry){
						$lAmt=$laundry->total;
						}else{
							$lAmt=0;
						}
					    
						$sum3=$exAmt+$posAmt+$Adjst+$lAmt;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									 
							     $sum2=$sum2+$av->roomRent; 
							     $mp=$mp+$av->mealPlan; 
								//	$sum3=$sum3+$av->extra_service_total_amount;
									$tax=$tax+$av->rrTax+$av->mpTax;
								
							}
							$gross=$sum2+$mp+$tax+$sum3-$disAmt;
							}
					
						?>
					<td align="center">
						<?php  echo 9; ?>
					</td>
					<td align="center">
						<a href="<?php echo base_url();?>reports/monthly_summ_r2?m=<?php echo 9?>">
							<?php  echo date("F", strtotime($a_date)); ?> </a>
						<?php echo " ".date("Y", strtotime($a_date))?>
					</td>
					<td align="center">
						<?php echo date("t", strtotime($a_date));?>
					</td>
					<td align="center">
						<?php //Room Count
										echo $totalrooms;?>
					</td>
					<td align="center">
						<?php
						//Occupancy
						echo round( ( $sqlTotal * 100 ) / ( $maxDays * $totalrooms ), 2 ) . '%';
						?>
					</td>
					<td align="center">
						<?php 
					//Room rent total
							if(isset($sum2) || $sum2>0){ echo $sum2;} else{ echo $sum2=0.00;};?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $mp ) && $mp > 0 )echo $mp;
						?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $posAmt ) && $posAmt > 0 ) {
							echo $posAmt;
						} else {
							echo 0;
						}
						?>
					</td>

					<td align="center">
						<?php if(isset($Adjst)){ echo $Adjst;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($disAmt) && $disAmt>0){ echo $disAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($lAmt) && $lAmt>0){ echo $lAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($sum3) && $sum3>0){ echo $sum3;}else{ echo 0;} ?>
					</td>

					<?php 
					
					if($sqlTotal==0){$sqlTotal=1;}
					$avg=$sum2/$sqlTotal;

					?>
					<td align="center">
						<?php if($avg>0) echo round($avg,2);?>
					</td>
					<td align="center">
						<?php
						//Total Tax
						if ( isset( $tax ) && $tax ) {
							echo $tax;
						} else {
							echo $tax = 0.00;
						};
						?>
					</td>
					<td align="center">
						<?php if($sum2>0 || $tax>0){echo $gross;}else{  echo "0.00";}?>
					</td>
				</tr>

				<tr>
					<?php   $mp=0.00;
				 $a_date = date("Y-10-01");
                    $maxDays=date('t');
					$sqlTotal=0;
					$sum2=0;
					$sum3=0;
					$tax=0;
					$gross=0;
					$start_date=$a_date;
					$end_date=date('Y-10-t');
                  /*  for($i=1; $i<=$maxDays; $i++){
                        if($i<10){
                          $val= $y.'-'.$m.'-'.'0'.$i;  
                        }else{
                          $val= $y.'-'.$m.'-'.$i; 
                        } 
					}*/   $sql= $this->dashboard_model->roomcount($start_date,$end_date);
                        if(isset($sql) && $sql){
							 $sqlTotal=$sql->id;
						}else{
							$sqlTotal=0;
						}
						$avgdailyrent=$this->dashboard_model->getBookingLineItem_byDate1($start_date,$end_date);
						$pos=$this->dashboard_model->getPosAmt($start_date,$end_date);
						if(isset($pos) && $pos){
						$posAmt=$pos->tot_Amt;
						}else{
							$posAmt=0;
						}
						$AdjstAmt=$this->dashboard_model->getadjAmt($start_date,$end_date);
						if(isset($AdjstAmt) && $AdjstAmt){
						$Adjst=$AdjstAmt->total;
						}else{
							$Adjst=0;
						}
						
						
						$disc=$this->dashboard_model->getDiscAmt($start_date,$end_date);
						if(isset($disc) && $disc){
						$disAmt=$disc->total;
						}else{
							$disAmt=0;
						}
						
						
						$extra=$this->dashboard_model->getExtraChargeAmt($start_date,$end_date);
						if(isset($extra) && $extra){
						$exAmt=$extra->total;
						}else{
							$exAmt=0;
						}
						
						$laundry=$this->dashboard_model->getLaundry_byDate1($start_date,$end_date);
						
						if(isset($laundry) && $laundry){
						$lAmt=$laundry->total;
						}else{
							$lAmt=0;
						}
					    
						$sum3=$exAmt+$posAmt+$Adjst+$lAmt;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									 
							     $sum2=$sum2+$av->roomRent; 
							     $mp=$mp+$av->mealPlan; 
								//	$sum3=$sum3+$av->extra_service_total_amount;
									$tax=$tax+$av->rrTax+$av->mpTax;
								
							}
							$gross=$sum2+$mp+$tax+$sum3-$disAmt;
							}
					
						?>
					<td align="center">
						<?php  echo 10; ?>
					</td>
					<td align="center">
						<a href="<?php echo base_url();?>reports/monthly_summ_r2?m=<?php echo 10?>">
							<?php  echo date("F", strtotime($a_date)); ?> </a>
						<?php echo " ".date("Y", strtotime($a_date))?>
					</td>
					<td align="center">
						<?php echo date("t", strtotime($a_date));?>
					</td>
					<td align="center">
						<?php //Room Count
										echo $totalrooms;?>
					</td>
					<td align="center">
						<?php
						//Occupancy
						echo round( ( $sqlTotal * 100 ) / ( $maxDays * $totalrooms ), 2 ) . '%';
						?>
					</td>
					<td align="center">
						<?php 
					//Room rent total
							if(isset($sum2) || $sum2>0){ echo $sum2;} else{ echo $sum2=0.00;};?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $mp ) && $mp > 0 )echo $mp;
						?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $posAmt ) && $posAmt > 0 ) {
							echo $posAmt;
						} else {
							echo 0;
						}
						?>
					</td>

					<td align="center">
						<?php if(isset($Adjst)){ echo $Adjst;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($disAmt) && $disAmt>0){ echo $disAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($lAmt) && $lAmt>0){ echo $lAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($sum3) && $sum3>0){ echo $sum3;}else{ echo 0;} ?>
					</td>

					<?php 
					
					if($sqlTotal==0){$sqlTotal=1;}
					$avg=$sum2/$sqlTotal;

					?>
					<td align="center">
						<?php if($avg>0) echo round($avg,2);?>
					</td>
					<td align="center">
						<?php
						//Total Tax
						if ( isset( $tax ) && $tax ) {
							echo $tax;
						} else {
							echo $tax = 0.00;
						};
						?>
					</td>
					<td align="center">
						<?php if($sum2>0 || $tax>0){echo $gross;}else{  echo "0.00";}?>
					</td>
				</tr>

				<tr>
					<?php   $mp=0.00;
				 $a_date = date("Y-11-01");
                    $maxDays=date('t');
					$sqlTotal=0;
					$sum2=0;
					$sum3=0;
					$tax=0;
					$gross=0;
					$start_date= $a_date;
					$end_date=date('Y-11-t');
                  /*  for($i=1; $i<=$maxDays; $i++){
                        if($i<10){
                          $val= $y.'-'.$m.'-'.'0'.$i;  
                        }else{
                          $val= $y.'-'.$m.'-'.$i; 
                        } 
					}*/
                        $sql= $this->dashboard_model->roomcount($start_date,$end_date);
						if(isset($sql) && $sql){
							$sqlTotal=$sql->id;
						}else{
							$sqlTotal=0;
						}
						$avgdailyrent=$this->dashboard_model->getBookingLineItem_byDate1($start_date,$end_date);
						$pos=$this->dashboard_model->getPosAmt($start_date,$end_date);
						if(isset($pos) && $pos){
						$posAmt=$pos->tot_Amt;
						}else{
							$posAmt=0;
						}
						$AdjstAmt=$this->dashboard_model->getadjAmt($start_date,$end_date);
						if(isset($AdjstAmt) && $AdjstAmt){
						$Adjst=$AdjstAmt->total;
						}else{
							$Adjst=0;
						}
						
						
						$disc=$this->dashboard_model->getDiscAmt($start_date,$end_date);
						if(isset($disc) && $disc){
						$disAmt=$disc->total;
						}else{
							$disAmt=0;
						}
						
						
						$extra=$this->dashboard_model->getExtraChargeAmt($start_date,$end_date);
						if(isset($extra) && $extra){
						$exAmt=$extra->total;
						}else{
							$exAmt=0;
						}
					    
						$laundry=$this->dashboard_model->getLaundry_byDate1($start_date,$end_date);
						
						if(isset($laundry) && $laundry){
						$lAmt=$laundry->total;
						}else{
							$lAmt=0;
						}
						$sum3=$exAmt+$posAmt+$Adjst+$lAmt;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									 
							     $sum2=$sum2+$av->roomRent; 
							     $mp=$mp+$av->mealPlan; 
								//	$sum3=$sum3+$av->extra_service_total_amount;
									$tax=$tax+$av->rrTax+$av->mpTax;
								
							}
							$gross=$sum2+$mp+$tax+$sum3-$disAmt;
							}
						?>
					<td align="center">
						<?php  echo 11; ?>
					</td>
					<td align="center">
						<a href="<?php echo base_url();?>reports/monthly_summ_r2?m=<?php echo 11?>">
							<?php  echo date("F", strtotime($a_date)); ?> </a>
						<?php echo " ".date("Y", strtotime($a_date))?>
					</td>
					<td align="center">
						<?php echo date("t", strtotime($a_date));?>
					</td>
					<td align="center">
						<?php //Room Count
										echo $totalrooms;?>
					</td>
					<td align="center">
						<?php
						//Occupancy
						echo round( ( $sqlTotal * 100 ) / ( $maxDays * $totalrooms ), 2 ) . '%';
						?>
					</td>
					<td align="center">
						<?php 
					//Room rent total
							if(isset($sum2) || $sum2>0){ echo $sum2;} else{ echo $sum2=0.00;};?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $mp ) && $mp > 0 )echo $mp;
						?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $posAmt ) && $posAmt > 0 ) {
							echo $posAmt;
						} else {
							echo 0;
						}
						?>
					</td>

					<td align="center">
						<?php if(isset($Adjst)){ echo $Adjst;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($disAmt) && $disAmt>0){ echo $disAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($lAmt) && $lAmt>0){ echo $lAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($sum3) && $sum3>0){ echo $sum3;}else{ echo 0;} ?>
					</td>

					<?php 
					
					if($sqlTotal==0){$sqlTotal=1;}
					$avg=$sum2/$sqlTotal;

					?>
					<td align="center">
						<?php if($avg>0) echo round($avg,2);?>
					</td>
					<td align="center">
						<?php
						//Total Tax
						if ( isset( $tax ) && $tax ) {
							echo $tax;
						} else {
							echo $tax = 0.00;
						};
						?>
					</td>
					<td align="center">
						<?php if($sum2>0 || $tax>0){echo $gross;}else{  echo "0.00";}?>
					</td>
				</tr>

				<tr>
					<?php  

			//echo 	$beg = date('W', strtotime(date("Y-12-01")));
			
			  $mp=0.00;
				$a_date = date("Y-12-01");
                    $maxDays=date('t');
					$sqlTotal=0;
					$sum2=0;
					$sum3=0;
					$tax=0;
					$gross=0;
					$start_date=$a_date;
					$end_date=date('Y-12-t');
                  
                        $sql= $this->dashboard_model->roomcount($start_date,$end_date);
						if(isset($sql) && $sql){
							$sqlTotal=$sql->id;
						}else{
							$sqlTotal=0;
						}
						$avgdailyrent=$this->dashboard_model->getBookingLineItem_byDate1($start_date,$end_date);
						$pos=$this->dashboard_model->getPosAmt($start_date,$end_date);
						if(isset($pos) && $pos){
						$posAmt=$pos->tot_Amt;
						}else{
							$posAmt=0;
						}
						$AdjstAmt=$this->dashboard_model->getadjAmt($start_date,$end_date);
						if(isset($AdjstAmt) && $AdjstAmt){
						$Adjst=$AdjstAmt->total;
						}else{
							$Adjst=0;
						}
						
						
						$disc=$this->dashboard_model->getDiscAmt($start_date,$end_date);
						if(isset($disc) && $disc){
						$disAmt=$disc->total;
						}else{
							$disAmt=0;
						}
						
						
						$extra=$this->dashboard_model->getExtraChargeAmt($start_date,$end_date);
						if(isset($extra) && $extra){
						$exAmt=$extra->total;
						}else{
							$exAmt=0;
						}
						$laundry=$this->dashboard_model->getLaundry_byDate1($start_date,$end_date);
						if(isset($laundry) && $laundry){
						$lAmt=$laundry->total;
						}else{
							$lAmt=0;
						}
					   
						$sum3=$exAmt+$posAmt+$Adjst+$lAmt;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									 
							     $sum2=$sum2+$av->roomRent; 
							     $mp=$mp+$av->mealPlan; 
								//	$sum3=$sum3+$av->extra_service_total_amount;
									$tax=$tax+$av->rrTax+$av->mpTax;
								
							}
							$gross=$sum2+$mp+$tax+$sum3-$disAmt;
							}
					
						?>
					<td align="center">
						<?php  echo 12; ?>
					</td>
					<td align="center">
						<a href="<?php echo base_url();?>reports/monthly_summ_r2?m=<?php echo 12?>">
							<?php  echo date("F", strtotime($a_date)); ?> </a>
						<?php echo " ".date("Y", strtotime($a_date))?>
					</td>
					<td align="center">
						<?php echo date("t", strtotime($a_date));?>
					</td>
					<td align="center">
						<?php //Room Count
										echo $totalrooms;?>
					</td>
					<td align="center">
						<?php
						//Occupancy
						echo round( ( $sqlTotal * 100 ) / ( $maxDays * $totalrooms ), 2 ) . '%';
						?>
					</td>
					<td align="center">
						<?php 
					//Room rent total
							if(isset($sum2) || $sum2>0){ echo $sum2;} else{ echo $sum2=0.00;};?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $mp ) && $mp > 0 )echo $mp;
						?>
					</td>
					<td align="center">
						<?php
						//Room Total service tax
						if ( isset( $posAmt ) && $posAmt > 0 ) {
							echo $posAmt;
						} else {
							echo 0;
						}
						?>
					</td>

					<td align="center">
						<?php if(isset($Adjst)){ echo $Adjst;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($disAmt) && $disAmt>0){ echo $disAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($lAmt) && $lAmt>0){ echo $lAmt;}else{ echo 0;}?>
					</td>
					<td align="center">
						<?php if(isset($sum3) && $sum3>0){ echo $sum3;}else{ echo 0;} ?>
					</td>

					<?php 
					
					if($sqlTotal==0){$sqlTotal=1;}
					$avg=$sum2/$sqlTotal;

					?>
					<td align="center">
						<?php if($avg>0) echo round($avg,2);?>
					</td>
					<td align="center">
						<?php
						//Total Tax
						if ( isset( $tax ) && $tax ) {
							echo $tax;
						} else {
							echo $tax = 0.00;
						};
						?>
					</td>
					<td align="center">
						<?php if($sum2>0 || $tax>0){echo $gross;}else{  echo "0.00";}?>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>