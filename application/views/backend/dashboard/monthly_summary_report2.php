<table id="logo-name" >
	<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel')); ?>
    <tr>
      <td align="left" valign="middle"><img src="<?php echo base_url();?>upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/></td>
		<td align="right" valign="middle"><?php echo "<strong style='font-size:18px;'>".$hotel_name->hotel_name.'</font></strong>'?></td>
        </tr>		
    
    <tr>
      <td width="100%" colspan="2"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td colspan="2"><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%" colspan="2">&nbsp;</td>
    </tr>
</table>
<div class="row">
  <div class="col-md-12 "> 
   
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="glyphicon glyphicon-bed"></i>Monthly Summary Report </div>
        <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            <div class="col-md-12" align="right">
			 
					<a type="button" href="<?php echo base_url();?>reports/monthly_summary_report" class="btn submit">Back</a>
     
   
			
			
            <!--  <div class="btn-group pull-right">
                <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu pull-right">
                  <li> <a href="javascript:window.print();"> Print </a> </li>
                  <li> <a href="javascript:;"> Save as PDF </a> </li>
                  <li> <a href="javascript:;"> Export to Excel </a> </li>
                </ul>
              </div>-->
            </div>
          </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="sample_2">
          <thead>
            <tr>
               <th scope="col">Day</th>
              <th scope="col">Room Count</th>
              <th scope="col">Occupancy %</th>
              <th scope="col">Room Rent</th>
              <th scope="col">Meal Rent</th>
              <th scope="col">POS</th>
              <th scope="col">Disc Amount</th>
              <th scope="col">Adjustment Amount</th>
              <th scope="col">Laundry Amount</th>
              <th scope="col">Room Inclusion</th>
              <th scope="col">Avg.Daily Rate (Incl.Inclusion)</th>
              <th scope="col">Total Taxes</th>
              <th scope="col">Gross total</th>
            </tr>
          </thead>
          <tbody>
            <?php
				 $sum3=0;
                 /*   $y= date("Y");
                   /* $m= date("m"); */
                  /*  $maxDays=date('t');
					*/
					
                   
						$sum2=0;
						$sum_new=0;
						$gross_new=0;
						$tax1=0;
						$sum3=0;	
						$tot_mp=0;
						$mp=0;
						$tax=0;
						$i=0;
						$disAmt=0;
						$lAmt=0;
						$totalGross=0;
					
				if(isset($bookingDate) && $bookingDate)
				{    
						foreach($bookingDate as $bk){
						   // print_r($bk);
						$i++;		
					 $start= $bk->date; 
        				$sql= $this->dashboard_model->roomcount($start,$start);
					//	print_r($sql); 
						if(isset($sql) && $sql){
							$sqlTotal=$sql->id;
						}else{
							$sqlTotal=0;
						}
						$avgdailyrent=$this->dashboard_model->getBookingLineItem_byDate1($start,$start);
					
					if(isset($avgdailyrent) && $avgdailyrent){
						
						foreach($avgdailyrent as $avg){
							$pos=$this->dashboard_model->getPosAmt($start,$start);
						
						if(isset($pos) && $pos){
						$posAmt=$pos->tot_Amt;
						}else{
							$posAmt=0;
						}
						$AdjstAmt=$this->dashboard_model->getadjAmt($start,$start);
						if(isset($AdjstAmt) ){
						$Adjst=$AdjstAmt->total;
						}else{
							$Adjst=0;
						}
						
						
						$disc=$this->dashboard_model->getDiscAmt($start,$start);
						
						if(isset($disc) && $disc){
						$disAmt=$disc->total;
						}else{
							$disAmt=0;
						}
						
						
						$extra=$this->dashboard_model->getExtraChargeAmt($start,$start);
						if(isset($extra) && $extra){
						$exAmt=$extra->total;
						}else{
							$exAmt=0;
						}
						
						$laundry=$this->dashboard_model->getLaundry_byDate1($start,$start);
						
						if(isset($laundry) && $laundry){
						$lAmt=$laundry->total;
						}else{
							$lAmt=0;
						}
					  
						$sum3=$Adjst+$exAmt+$posAmt+$lAmt;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									 
							     $sum2=$av->roomRent; 
							     $mp=$av->mealPlan; 
								//	$sum3=$sum3+$av->extra_service_total_amount;
									$tax=$av->rrTax+$av->mpTax;
								
							}
							$gross=$sum2+$mp+$tax+$sum3-$disAmt;
							}

							}	} ?>
            <tr>
              <td align="center"><?php  echo date("jS F Y",strtotime($start)); ?></td>
             
              <td align="center"><?php echo $sqlTotal;?></td>
              <td align="center"><?php
					echo round(($sqlTotal*100)/($totalrooms),2).'%';?></td>
            <td align="center"><?php 	if(isset($sum2) || $sum2>0){ echo $sum2;} else{ echo $sum2=0.00;};?></td>
            <td align="center"><?php	if(isset($mp) && $mp>0){	echo $mp;}else{ echo "0";}?></td>
			<td align="center"><?php if(isset($posAmt) && $posAmt>0)	{echo $posAmt;}else{ echo 0;}?></td>
			<td align="center"><?php if(isset($disAmt) && $disAmt>0){ echo $disAmt;}else{ echo 0;}?></td>
			<td align="center"><?php if(isset($Adjst) ){ echo $Adjst;}else{ echo 0;}?></td>			
<td align="center"><?php if(isset($lAmt) && $lAmt>0){ echo $lAmt;}else{ echo 0;}?></td>			
			<td align="center"><?php if(isset($sum3) && $sum3>0){ echo $sum3;}else{ echo 0;} ?></td>		
					
					<?php 
					
					if($sqlTotal==0){$sqlTotal=1;}
					$avg=$sum2/$sqlTotal;

					?>
              <td align="center"><?php if($avg>0) echo round($avg,2);?></td>
              <td align="center"><?php
					//Total Tax
					if(isset($tax) && $tax){ echo $tax;} else{ echo $tax=0.00;};	?></td>
              <td align="center"><?php if($sum2>0 || $tax>0){echo $gross;}else{  echo "0.00";}?></td>
            </tr>
			
				<?php  }
			
				
				}
						?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT--> 