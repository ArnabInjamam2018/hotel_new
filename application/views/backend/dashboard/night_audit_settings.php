<!-- 17.11.2015-->
<?php
/*echo "<pre>";
print_r($rates);
echo "</pre>";
exit();
*/
?>
<?php if($this->session->flashdata('err_msg')):?>

<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<!-- 17.11.2015-->
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase">Audit Setting</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
    $form = array(
        'class' 			=> '',
        'id'				=> 'form',
        'method'			=> 'post',
            
    );
    echo form_open_multipart('night_Audit_controller/night_audit_settings',$form);
    
    ?>
    <div class="form-body">
      <div class="row">
        <?php 
$data=$this->unit_class_model->get_audit_settings();
//echo "<pre>";
//print_r($data);
if(isset($data) && $data){
			foreach($data as $as){
		  $time=$as->night_audit_due_time;
		  $email=$as->Email_to;
		  $status=$as->Checkin_Checkout_status;
		  $toggle=$as->toggle;
}
}
		  ?>
        <div class="col-md-6">
          <div class="form-group form-md-line-input form-horizontal">
          	<div class="row">
            <label class="col-md-10 control-label" for="form_control_1">Disable Checkin & Checkout if night audit is not performed?</label>
            <div class="col-md-2">
              <div class="md-radio-inline">
                <div class="switch">
                  <?php
                                 //$tax=$as->tax_applied	;
                                 
                                 ?>
                  <input type="checkbox" <?php if(isset($status) && $status){ echo " checked ";} ?>   id="chk6"  class="md-check cmn-toggle cmn-toggle-round " name="chk6">
                  <label for="chk6"></label>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group form-md-line-input form-horizontal">
          	<div class="row">
            <label class="col-md-8 control-label" for="form_control_1">Toggle?</label>
            <div class="col-md-4">
              <div class="md-radio-inline">
                <div class="switch">
                  <?php
    // $tax_meal=$as->tax_applied_mealplan	;
     ?>
                  <input type="checkbox" <?php if(isset($toggle) && $toggle){ echo " checked ";}?>   id="chk7"  class="md-check cmn-toggle cmn-toggle-round " name="chk7">
                  <label for="chk7"></label>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
        <!-- <div class="col-md-3">
                <div class="form-group form-md-line-input">
                  <input type="Time" class="form-control time-picker" name="time" required="required" autocomplete="off" value="<?php if(isset($time) && $time){ echo $time;}?> " id="time" placeholder="Audit Time *">
                  <label></label>
                  <span class="help-block">Audit Time *</span>
                </div>
            </div>-->
        
        <div class="col-md-2">
          <div class="form-group form-md-line-input">
            <input onblur="start_time()" autocomplete="off" type="time" class="form-control " id="starttime" name="starttime" value="<?php if(isset($time) && $time){ echo $time;}?> " placeholder="Start Time *">
            <label></label>
            <span class="help-block">Time *</span> </div>
        </div>
        <div class="col-md-2">
          <div class="form-group form-md-line-input" id="unitClass">
            <input autocomplete="off" type="email" value="<?php if(isset($email) && $email){ echo $email;}?> " class="form-control"  id="Email_to" name="Email_to" placeholder="Email To*">
            <label></label>
            <span class="help-block">Email_to</span> </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn green" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <input type="hidden" name="hid">
    <?php form_close(); ?>
  </div>
</div>
<!--  modal addition ..--> 

<!-- end of modal---> 

<script>

	
 function check_due(val){
	 //alert(val);
	 var due=$("#payments_due").val(val).val();
	 var pay_amt=$('#pay_amount').val();
	 var ch_pay_amt=$('#chng_pay_amt').val();
	 
	 	//alert(ch_pay_amt);
	if(val==due){
		$("#paments_status").attr('value','0').change();
	}
 }
           $(document).ready(function() {
                        cellWidth = 100;
                        cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                       
                    });

            $("#autocellwidth").click(function() {
                      cellWidth = 100;  // reset for "Fixed" mode
                        cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                      /*  document.getElementById('auto_cl_id').style.backgroundColor = $(this).is(":checked") ? "#00CC99" : "#A5A5A5";
                        var a = $(this).is(":checked") ? "Pay Later" : "Pay Now";
						//alert(a);
						if(a=="Pay Later"){
							
							document.getElementById('pay').style.display = 'none';
	                        document.getElementById('pay1').style.display = 'block';
							
						}else{
							document.getElementById('pay').style.display = 'block';
	                        document.getElementById('pay1').style.display = 'block';
						}
						var dis=$('#disp').text(a);
					
                    */
					document.getElementById('auto_cl_id').style.backgroundColor = $(this).is(":checked") ? "#297CAC" : "#39B9A1";
                        var a = $(this).is(":checked") ? "Pay Later" : "Pay Now";
						if(a == 'Pay Later')
						{	
							document.getElementById('pay').style.display = 'block';
	                        document.getElementById('pay1').style.display = 'block';
						}
						else
						{	
							
						    document.getElementById('pay').style.display = 'none';
	                        document.getElementById('pay1').style.display = 'block';
						}
						
						$('#disp').text(a);

					});
					
					

   
   
 
	
   var sum=0; 
  var flag=0;
  var flag1=0;

  
   $("#additems").click(function(){
var season=$('#season2').val();
var start=$('#start_date_p').val();
var end=$('#end_date_p').val();
     
	if(season!=null && season!=''){
$.ajax({
		   type: "POST",
		   url: "<?php echo base_url();?>rate_plan/add_season_mapping",
		    data:{season:season,start:start,end:end},
		   success: function(data){
			   
			 //  alert(data);
		/*$('#items tr:first').after('<tr id="row_'+flag+'">'+
	'<td class="hidden-480"><input name="cloth_type[]" id="cloth_type'+flag+'" type="text" value="'+data.name+'" class="form-control input-sm" ></td>'+
	'<td class="hidden-480"><input name="fabric[]" id="fabric'+flag+'" type="text" value="'+data.fabric+'" class="form-control input-sm" ></td>'+	
	'<td class="hidden-480"><input name="size[]" id="fabric'+flag+'" type="text" value="'+size+'" class="form-control input-sm" ></td>'+	
	'<td class="hidden-480"><input name="condition[]" id="condition'+flag+'" type="text" value="'+condition+'" class="form-control input-sm" ></td>');
			 */  
				
		   }//,async: false
		});

	
	
	  $('#season').val('');
	$('#start_date').val('');
	$('#end_date').val('');
	
	
	
	location.reload();
	}else{
		alert('Enter value');
	}
	 

});

	
	function check_date_stat(){
	
	var a=$('#contract_start_date').val();
	 a=new Date(a);
	   var b=$('#contract_end_date').val();
	   b=new Date(b);
	   if(a>=b && b!='' ){
		   alert('End Date sould getter tan Start Date ');
		   $('#contract_end_date').val('');
	   }
	  
	  var c=$('#contract_end_date'+(flag-1)).val();
	 // alert(c);
	    c=new Date(c);
		if(c>a){
			
			alert('Please enter valid date');
			$('#contract_start_date').val('');
			$('#contract_end_date').val('');
		}
		
	  
	   
}
	
	

	
	
	
function show_modal(){
	$('#basic1').modal('show');
}	
  

function set_vendor(a,b,c,d){
	//alert(d)
	$("#select_l_itm").val(a);
	$("#address").val(b);
	$("#ph_no").val(c);
	$("#vendor_id").val(d);
	
	$('#basic1').modal('toggle');
	
}	
	
	
</script> 
<script>
  
  $('#end_date_p').on('click',function(){
	  
	  var seasons=$('#season2').val();
var starts=$('#start_date_p').val();

if(seasons ==null || seasons =='' || starts ==null || starts ==''){
	
	//alert(seasons);
	//alert()
	swal("Enter season and start date first!", "", "error")
}
else{
	
	//alert(seasons);
	//alert(starts);
	//$this->rate_plan_model->check_season_by_date($season,$start,$end);
	$.ajax({
		   type: "POST",
		   url: "<?php echo base_url();?>rate_plan/check_season_by_date",
		    data:{seasons:seasons,starts:starts},
		   success: function(data){
			   
			  // alert(data);
			   if(data =='already defined'){
				   
				   swal("Already defined this date range under the same season!", "", "error")
				   $('#season2').val('');
				   $('#start_date_p').val('');
				   $('#end_date_p').val('');
				   $('#season2').focus();
			   }
		 
				
		   },
		});
}
	  
  });


</script> 
<script>


 


 function check_date(){
	
	var a=$('#bill_date').val();
	 a=new Date(a);
	   var b=$('#bill_due_date').val();
	   b=new Date(b);
	   if(a>=b && b!='' ){
		  // alert('End Date sould getter tan Start Date ');
		   swal("Due Date sould getter than Bill Date!", "", "error")
		   $('#bill_due_date').val('');
	   }
		  
	
}


function update_default(v){
	
	 $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>rate_plan/default_season",
    data:{plan:v},
    success: function(data){
			
		if(data.data== "y")
		{
			 swal("Duplicate season name!", "", "error")
			 $('#season').val('');
		}
    }
    });
}












function check_duplicate(val){
	 $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>dashboard/duplicate_season",
    data:{data:val},
    success: function(data){
			
		if(data.data== "y")
		{
			 swal("Duplicate season name!", "", "error")
			 $('#season').val('');
		}
    }
    });
}
$(document).ready(function() {

$('.date-picker').datepicker({
                orientation: "left",
				startDate: today,
                autoclose: true,
				todayHighlight: true
            })
			/*.on('changeDate', function(ev){
				var selected1 = $("#sd").val();
				var selected2 = $("#ed").val();
				//alert("HERE");
				$.ajax({				
					url: '<?php echo base_url(); ?>dashboard/get_available_room2',
					type: "POST",
					data: {date1:selected1 , date2:selected2},
					success: function(data){
					   //alert(data);
					   $("#hd").val(data);
					},
				});
        });*/
});




</script> 
<script>
var psc = 0;

function all_rate_plans(){
	//alert("dgdg");
var populateStatus=$('#populate').val();
//alert(populateStatus);	
var pwd = $('#rate_pwd').val();
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
} 

today = dd+'-'+mm+'-'+yyyy;


if(pwd == today){
//if(1){
	if(populateStatus=='0'){
	//alert("dgdg");
	<?php $functionName1="add_rate_plan";?>
	alert('<?php echo $functionName1;?>');
	swal('Ressing all the Rate Plan Data to 0.00! This may take some time.');
	$('#rate_pwd').val('');
	$('#responsive').modal('toggle');
	 alert('<?php echo base_url()?>rate_plan/<?php echo $functionName1;?>');
	 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>rate_plan/<?php echo $functionName1;?>",
                
                success:function(data)
                {
               //    alert(data);
				  // console.log(data);
                     swal({
                            title: data.data,
                            text: "Data populated successfully!",
                            type: "success"
                        },
                        function(){
							//$('#row_'+id).remove();

                        });
                        
                }
            });
}else{
	//alert("d1gd2g");
	<?php $functionName2="edit_rate_plan";?>
	//alert('<?php echo $functionName2;?>');
	swal('Ressing all the Rate Plan Data to 0.00! This may take some time.');
	$('#rate_pwd').val('');
	$('#responsive').modal('toggle');
	 alert('<?php echo base_url()?>rate_plan/<?php echo $functionName2;?>');
	 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>rate_plan/<?php echo $functionName2;?>",
                
                success:function(data)
                {
                   alert(data);
				   console.log(data);
                     swal({
                            title: data.data,
                            text: "Data populated successfully!",
                            type: "success"
                        },
                        function(){
							//$('#row_'+id).remove();

                        });
                        
                }
            });
}
	

			
}//end of if..
else{
	psc++;
	//swal('You entered a wrong password, 5 attempts left');
	if(psc <= 5){
		swal("wrong!", "You entered a wrong password, "+ (5-psc) +" attempts left", "error");
	}
	
	$('#responsive').modal('toggle');
	
	
	
}

}
</script>