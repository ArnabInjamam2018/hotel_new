   <div class="portlet light borderd">
        <div class="portlet-title">
          <div class="caption"> <i class="fa fa-file-archive-o"></i>Note & Preference report</div>
          <!--
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
                --> 
        </div>
        <div class="portlet-body">
          <div class="table-toolbar">
            <div class="row">
              <div class="col-md-7">
                  <?php

                            $form = array(
                                'class'       => 'form-inline',
                                'id'        => 'form_date',
                                'method'      => 'post'
                            );

                            echo form_open_multipart('dashboard/note_preference_report_datefilter',$form);

                            ?>
                      <div class="form-group">
                        <input type="text" autocomplete="off"  required="required" id="t_dt_frm" name="t_dt_frm" value="<?php if(isset($start_date)){echo $start_date; } ?>"  class="form-control date-picker" placeholder="Start Date">
                      </div>
                      <div class="form-group">
                        <input type="text" autocomplete="off"   required="required" name="t_dt_to" class="form-control date-picker" value="<?php if(isset($start_date)){echo $end_date; } ?>"  placeholder="End Date">
                      </div>
					   
                    <button  class="btn btn-default" onclick="check_sub()" type="submit" >Search</button>
                 
                  <?php form_close(); ?>
              </div>              
            </div>
          </div>
          <table class="table table-striped table-hover table-bordered" id="sample_1">
            <thead>
              <tr> 
                <th> # </th>
				<th> Guest </th>
				<th> Type </th>
				<th> Booking ID </th>
                <th> Check In </th>
                <th> Check Out </th>
                <th> Status </th>
              <!--  <th> Housekeeping Status </th>-->
                <th>Arival Details </th>
                <th> Deperture Details </th>
                <th>Booking Note</th>
				  <th>Booking Preference</th>
                <th> Total Bill Amount</th>
              </tr>
            </thead>
            <tbody>
              <?php
						if(isset($bookings) && $bookings){
						$srl_no=0;
						//echo "<pre>";
					//print_r($bookings);
						//exit;
						
						foreach($bookings as $report){
							$srl_no++;
						
					 $status_id=$report->booking_status_id;
			
						$status=$this->dashboard_model->get_status_by_id($status_id);
						if(isset($status)){
							foreach($status as $st){
								
							
			  ?>
             <tr>
             
				<td><?php echo $srl_no;?></td>
				<td><?php echo $report->cust_name;?></td>	
				<td>
					<?php
						if(!$report->group_id==0){
							echo "Group";
						}
						else{
							echo "Single";
						}
					?>
				</td>
				<td><?php  if($report->booking_id_actual != "" && $report->group_id==0){
					echo $report->booking_id_actual;} 
					elseif($report->group_id!==0){echo "HM0".$report->hotel_id."GRP00".$report->group_id;}?>
					</td>
				<td><?php if($report->cust_from_date_actual){echo $report->cust_from_date_actual;}else{?>
					<span style="color:#AAAAAA;">No info</span>
					
				<?php }?></td>
				<td><?php if($report->cust_end_date_actual){echo $report->cust_end_date_actual;}else{?>
					<span style="color:#AAAAAA;">No info</span>
					
				<?php }?></td>
				<td><?php
				
					
					if($st->booking_status){echo $st->booking_status;}else{?>
					<span style="color:#AAAAAA;">No info</span>
					
				<?php }
					
					?>
				</td>
				
				<td><?php
				if($report->coming_from!=""){
				echo "(".$report->coming_from.")"." - ".$report->arrival_mode." - ".$report->arrival_details;
				}else{?>
					<span style="color:#AAAAAA;">No info</span>
					
				<?php }
				
				
				?></td>
				
				<td><?php
					if($report->next_destination!=""){
					echo "(".$report->next_destination.")"." - ".$report->dept_mode." - ".$report->departure_details;
					}else{?>
					<span style="color:#AAAAAA;">No info</span>
					
				<?php }
					
					
					?></td>
				<td><?php if($report->comment){ echo $report->comment;}else{?>
					<span style="color:#AAAAAA;">No info</span>
					
				<?php }?></td>	
				<td><?php if($report->preference){echo $report->preference;}else{?>
					<span style="color:#AAAAAA;">No info</span>
					
				<?php }?></td>	
				<td><?php echo $report->room_rent_sum_total;?></td>			
			  </tr>
						<?php }}}}?>
            </tbody>
          </table>
		 
		</div>
		</div>  
<script type="text/javascript">

$(document).ready(function(){
  $('#chkbx_tdy').prop('checked', true);
});

function check_sub(){
  document.getElementById('form_date').submit();
}
</script>