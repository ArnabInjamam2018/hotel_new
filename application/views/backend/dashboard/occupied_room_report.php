<div class="portlet light borderd">
  <div class="portlet-title">
    <div class="caption"> <i class="glyphicon glyphicon-bed"></i>Daily Occupancy Report (Checkedin) </div>
    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-8">
          <?php

                        $form = array(
                            'class'       => 'form-inline',
                            'id'        => 'form_date',
                            'method'      => 'post'
                        );

                        echo form_open_multipart('reports/occupied_room_report',$form);

                        ?>
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" id="t_dt_frm" name="t_dt_frm" value="<?php if(isset($start_date) && $start_date){ echo date("d/m/Y", strtotime($start_date));}?>"   class="form-control date-picker" placeholder="Start Date">
          </div>
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" name="t_dt_to" value="<?php if(isset($end_date) && $end_date){ echo date("d/m/Y", strtotime($end_date));}?>"  class="form-control date-picker" placeholder="End Date">
          </div>
          <button class="btn btn-default" onclick="check_sub()" type="submit">Search</button>
          <?php form_close(); ?>
        </div>
      </div>
    </div>
    <table class="table table-striped table-hover table-bordered" id="sample_3">
      <thead>
        <tr>
   
          <th scope="col">#</th>
		  <th scope="col">Room No</th>
          <th scope="col">Booking Id</th>
		  <th scope="col">Guest Name</th>
		  <th scope="col">Arrival Date</th>
          <th scope="col">Expt Departure Date</th>
          <th scope="col">Meal Plan</th>
		  <th scope="col">No of Adult</th>
		  <th scope="col">No of Child</th>
          <th scope="col">Nature of visit</th>
          <th scope="col">Guest Contact No</th>
      
       
        </tr>
      </thead>
      <tbody>
<?php
$tot_rr=0;
$tot_child=0;
$x=1;
if(isset($occu_rooms) && $occu_rooms!=''){
	
	/*echo "<pre>";
	print_r($occu_rooms);
	//exit;*/
	foreach($occu_rooms as $occu){
		$food=$occu->meal_plan_name;
		
	echo "<tr>";
	echo "<td>".$x."</td>";
	echo "<td>".$occu->room_no."</td>";
	echo "<td>".$occu->booking_id."</td>";
	echo "<td>".$occu->cust_name."</td>";
	echo "<td>".date('d-m-Y',strtotime($occu->cust_from_date_actual))."</td>";
	echo "<td>".date('d-m-Y',strtotime($occu->cust_end_date_actual))."</td>";
	
	echo "<td>".$food."</td>";
	echo "<td>".$occu->no_of_adult."</td>";
	echo "<td>".$occu->no_of_child."</td>";
	echo "<td>".$occu->nature_visit."</td>";
	echo "<td>".$occu->cust_contact_no."</td>";
	echo "</tr>";
	$x++;
	$tot_rr=$tot_rr+$occu->no_of_adult;
	$tot_child=$tot_child+$occu->no_of_child;
	}
	
	
	
}
//echo "<pre>";
//print_r($occu_rooms);

?>
      </tbody>
      <tfoot>
        <tr>
        	<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
          <td style="font-weight:700; color:#666666; text-align:center;" colspan='2'> TOTAL NO OF GUESTS </td>
          <td><?php if(isset($tot_rr) && $tot_rr)  echo $tot_rr; else echo "0";?></td>
          <td><?php if(isset($tot_child) && $tot_child)  echo $tot_child; else echo "0";?></td>
          <td></td>
		<td></td>

        </tr>
      </tfoot>
    </table>
  </div>
</div>
<script>

$(document).ready(function(){
  $('#chkbx_tdy').prop('checked', true);
});

function check_sub(){
  document.getElementById('form_date').submit();
}
</script> 
