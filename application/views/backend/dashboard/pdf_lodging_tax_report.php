<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td{
	font-size:12px;
}
</style>
<div style="padding:15px 35px;">
<table>
		<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));?>
    <tr>
      	<td align="left"> <img src="upload/hotel/<?php if(isset($hotel_name->hotel_logo_images_thumb))echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/></td>
        <td colspan="2" align="center"><strong><font size='13'>Lodging Tax Report</font></strong></td>
		<td align="right"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?></td>
    </tr>
    <tr>
      <td width="100%" colspan="4"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td colspan="3">
	<?php if(isset($start_date) && isset($end_date) && $start_date && $end_date ){
		echo "<strong>From: </strong>".$start_date."  <strong>To: </strong>".$end_date;
	}
		?>
	</td><td align="right"><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%" colspan="4">&nbsp;</td>
    </tr>
</table>
		
             <table class="table table-striped table-hover table-bordered" id="sample_1">
            <thead>
              <tr> 
                <th> # </th>
				<th> Guest Name </th>
                <th> Room Type </th>
                <th> Room No </th>
                <th> Check In </th>
                <th> Check Out </th>
                <th>Night </th>
                <th> Room Rent(Excluding Taxs) </th>
				<th> Occupancy Tax </th>
				<th>discount </th>
				<th> Other Tax </th>
              </tr>
            </thead>
            <tbody>
             
             <tr>
              <?php
						if(isset($bookings)){
						$srl_no=0;
						//echo "<pre>";
						//print_r($bookings);
						//exit;
						foreach($bookings as $report){
						$srl_no++;
					$room_id=$report->room_id;
					$room=$this->dashboard_model->get_room_by_id($room_id);
						if(!empty($room)){
					 foreach($room as $room){
						 
			  ?>
				<td><?php echo $srl_no;?></td>
				<td><?php echo $report->cust_name;?></td>	
				<td><?php echo $room->unit_name ;?></td>
				<td><?php echo $room->room_no;?></td>
				<td><?php echo $report->cust_from_date_actual;?></td>
				<td><?php echo $report->cust_end_date_actual;?></td>
				<td>
					<?php
					$from=$report->cust_from_date_actual;
					$to=$report->cust_end_date_actual;
					echo $nights = (strtotime($to) - strtotime($from)) / 86400;
				?></td>
				<td><?php echo $report->room_rent_total_amount;?></td>
				
				<td><?php
						
					 $occupancy_tax=$report->service_tax+$report->luxury_tax+$report->service_charge;
				echo $tax=$report->room_rent_total_amount*( $occupancy_tax/100);
					?>
					</td>
				<td><?php echo $report->discount; ?></td>	
					<td><?php echo $report->extra_service_total_amount;?></td>	
						
			  </tr>
		<?php }}}}?>
            </tbody>
          </table>
          

</div>