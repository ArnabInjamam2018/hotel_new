<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td{
	font-size:9px;
}
</style>

<div style="padding:15px 35px;">
<table>
		<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));?>
    <tr>
      <td align="center"><img src="upload/hotel/<?php
			 
			if(isset($hotel_name->hotel_logo_images_thumb))echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/></td>
		<tr>
		<td align="center"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?></td>
        </tr>		
    </tr>
    <tr>
      <td width="100%"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%">&nbsp;</td>
    </tr>
</table>
<table class="td-pad" width="100%">
  <thead>
    <tr style="background: #e5e5e5; color: #1b1b1b">
			  <th scope="col">Day</th>
              <th scope="col">Room Count</th>
              <th scope="col">Occupancy %</th>
              <th scope="col">Room Rent</th>
              <th scope="col">Room Inclusion</th>
              <th scope="col">Avg.Daily Rate (Incl.Inclusion)</th>
              <th scope="col">Total Taxes</th>
              <th scope="col">Gross total</th>
    </tr>
    </tr>
  </thead>
 <tbody style="background: #F2F2F2">
            
              <tr>
              <?php
				//Day
				/*echo $number = cal_days_in_month(CAL_GREGORIAN, 02, 2016);
                    $y= date("Y");
                    $m= date("m"); 
                    $maxDays=date('t');*/
					$sqlTotal=0;
					$sum2=0;
					$sum3=0;
					$tax=0;
					$gross=0;
                    for($i=1; $i<=$maxDays; $i++){
                        if($i<10){
                          $val= $y.'-'.$m.'-'.'0'.$i;  
                        }else{
                          $val= $y.'-'.$m.'-'.$i; 
                        } 
						
                        $sql= $this->dashboard_model->roomcount($val);
                       
						$avgdailyrent=$this->dashboard_model->avgdailyrent($val);
					     $sqlTotal +=$sql;
						
							
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$sum2=$sum2+$av->room_rent_total_amount;//exit();
								}
							}
							
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$sum3=$sum3+$av->service_price;//exit();
								}
							}
							
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$tax=$tax+$av->room_rent_tax_amount;//exit();
								}
							}
							
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$gross=$gross+$av->room_rent_tax_amount+$av->room_rent_total_amount+$av->service_price;//exit();
								}
							}
							
							
							
							
							
					}
						
						
						?>
              <td align="center"><?php echo $maxDays;?></td>
              <td align="center"><?php //Room Count
										echo $sqlTotal;?></td>
              <td align="center"><?php
					//Occupancy
										echo round(($sqlTotal*100)/($maxDays*$totalrooms),2).'%';?></td>
              <td align="center"><?php 
					//Room rent total
							echo $sum2;?></td>
              <td align="center"><?php
					//Room Total service tax
					echo $sum3;?></td>
              <td align="center"><?php echo round(($avg=$sum2/$sqlTotal),2);?></td>
              <td align="center"><?php
					//Total Tax
						echo $tax;?></td>
              <td align="center"><?php echo $gross;?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="glyphicon glyphicon-bed"></i>List of All Rooms </div>
        <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            <div class="col-md-12">
            <!--  <div class="btn-group pull-right">
                <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                <ul class="dropdown-menu pull-right">
                  <li> <a href="javascript:window.print();"> Print </a> </li>
                  <li> <a href="javascript:;"> Save as PDF </a> </li>
                  <li> <a href="javascript:;"> Export to Excel </a> </li>
                </ul>
              </div>-->
            </div>
          </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="sample_editable_2">
          <thead>
            <tr>
              <th scope="col">Day</th>
              <th scope="col">Room Count</th>
              <th scope="col">Occupancy %</th>
              <th scope="col">Room Rent</th>
              <th scope="col">Room Inclusion</th>
              <th scope="col">Avg.Daily Rate (Incl.Inclusion)</th>
              <th scope="col">Total Taxes</th>
              <th scope="col">Gross total</th>
            </tr>
          </thead>
          <tbody>
            <?php
				//Day
                    /*$y= date("Y");
                    $m= date("m"); 
                    $maxDays=date('t');*/
					
                    for($i=1; $i<=$maxDays; $i++){
                        if($i<10){
                          $val= $y.'-'.$m.'-'.'0'.$i;  
                        }else{
                          $val= $y.'-'.$m.'-'.$i; 
                        } 
                        $sql= $this->dashboard_model->roomcount($val);
                        $avgdailyrent=$this->dashboard_model->avgdailyrent($val);
						
						//$sum=0;
						//if(isset($avgdailyrent) && $avgdailyrent){
						//foreach($avgdailyrent as $avg){
							//echo $sum=$sum+$avg->room_rent_total_amount;
						
                ?>
            <tr>
              <td align="center"><?php //Day
										echo $i; ?></td>
              <td align="center"><?php //Room Count
										echo $sql;?></td>
              <td align="center"><?php //Occupancy  
						echo round((($sql/$totalrooms)*100), 2).'%'; ?></td>
              <td align="center"><?php 
				//Room Rent
								$sum=0; 
								if(isset($avgdailyrent) && $avgdailyrent){		
									foreach($avgdailyrent as $avg){		
							    $sum=$sum+$avg->room_rent_total_amount;
							}
					}	
					if($sum == "0"){
						echo "0";
					}else{
						echo round($sum,2);
					}
				?></td>
              <td align="center"><?php 
					//Room Inclusion
							$sum2=0;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$sum2=$sum2+$av->service_price;//exit();
								}
							}
							if($sum2 == "0" || $sum2 == ""){
								echo "0";
							}
							else{
								echo $sum2;
							}?></td>
              <td align="center"><?php 
						//Avg.Daily Rate (Incl.Inclusion)
							if($sql=="0"){
								echo $avg="0";
							}else{
								echo round(($avg=$sum/$sql),2);
							}
				?></td>
              <td align="center"><?php 
					//Room Tax
							$tax=0;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$tax=$tax+$av->room_rent_tax_amount;//exit();
								}
							}
							if($tax == "0"){
								echo "0";
							}
							else{
								echo $tax;
							}?></td>
              <td align="center"><?php 
					//Gross Amount
							$gross=0;
							if(isset($avgdailyrent) && $avgdailyrent){		
								foreach($avgdailyrent as $av){		
									$gross=$gross+$av->room_rent_tax_amount+$av->room_rent_total_amount+$av->service_price;//exit();
								}
							}
							if($gross == "0"){
								echo "0";
							}
							else{
								echo $gross;
							}?></td>
            </tr>
            <?php  }?>
   </tbody>
</table>
</div>
