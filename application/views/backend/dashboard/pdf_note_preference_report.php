<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td{
	font-size:12px;
}
</style>
<div style="padding:15px 35px;">
<table>
		<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));?>
    <tr>
      	<td align="left"> <img src="upload/hotel/<?php if(isset($hotel_name->hotel_logo_images_thumb))echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/></td>
        <td colspan="2" align="center"><strong><font size='13'>Note & Preference Report</font></strong></td>
		<td align="right"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?></td>
    </tr>
    <tr>
      <td width="100%" colspan="4"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td colspan="3">
	<?php if(isset($start_date) && isset($end_date) && $start_date && $end_date ){
		echo "<strong>From: </strong>".$start_date."  <strong>To: </strong>".$end_date;
	}
		?>
	</td><td align="right"><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%" colspan="4">&nbsp;</td>
    </tr>
</table>
   <table class="td-pad" width="100%">
            <thead>
              <tr> 
                <th> # </th>
				<th> Guest </th>
				<th> Type </th>
				<th> Booking ID </th>
                <th> Check In </th>
                <th> Check Out </th>
                <th> Status </th>
              <!--  <th> Housekeeping Status </th>-->
                <th>Arival Details </th>
                <th> Deperture Details </th>
                <th>Booking Note</th>
				  <th>Booking Preference</th>
                <th> Total Bill Amount</th>
              </tr>
            </thead>
            <tbody>
			
			<?php
				if(isset($bookings) && $bookings){
						$srl_no=0;
						//echo "<pre>";
					//print_r($bookings);
						//exit;
						
						foreach($bookings as $report){
							$srl_no++;
						
					 $status_id=$report->booking_status_id;
			
						$status=$this->dashboard_model->get_status_by_id($status_id);
						if(isset($status)){
							foreach($status as $st){
								
							
			  ?>
             <tr>
             
				<td><?php echo $srl_no;?></td>
				<td><?php echo $report->cust_name;?></td>	
				<td>
					<?php
						if(!$report->group_id==0){
							echo "Group";
						}
						else{
							echo "Single";
						}
					?>
				</td>
				<td><?php  if($report->booking_id_actual != "" && $report->group_id==0){
					echo $report->booking_id_actual;} 
					elseif($report->group_id!==0){echo "HM0".$report->hotel_id."GRP00".$report->group_id;}?>
					</td>
				<td><?php echo $report->cust_from_date_actual;?></td>
				<td><?php echo $report->cust_end_date_actual;?></td>
				<td><?php
				
					
					echo $st->booking_status;
					
					?>
				</td>
				
				<td><?php
				if($report->coming_from!=""){
				echo "(".$report->coming_from.")"." - ".$report->arrival_mode." - ".$report->arrival_details;}?></td>
				
				<td><?php
					if($report->next_destination!=""){
					echo "(".$report->next_destination.")"." - ".$report->dept_mode." - ".$report->departure_details;}?></td>
				<td><?php echo $report->comment;?></td>	
				<td><?php echo $report->preference;?></td>	
				<td><?php echo $report->room_rent_sum_total;?></td>			
			  </tr>
						<?php }}}}?>
            </tbody>
          </table>
		  </div>