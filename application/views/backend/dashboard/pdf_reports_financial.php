<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td{
	font-size:12px;
}
</style>
<div style="padding:15px 35px;">
<table>
		<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));?>
    <tr>
      	<td align="left"> <img src="upload/hotel/<?php if(isset($hotel_name->hotel_logo_images_thumb))echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/></td>
        <td colspan="2" align="center"><strong><font size='13'>Daily Financial Report</font></strong></td>
		<td align="right"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?></td>
    </tr>
    <tr>
      <td width="100%" colspan="4"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td colspan="3">
	<?php if(isset($start_date) && isset($end_date) && $start_date && $end_date ){
		echo "<strong>From: </strong>".$start_date."  <strong>To: </strong>".$end_date;
	}
		?>
	</td><td align="right"><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%" colspan="4">&nbsp;</td>
    </tr>
</table>
<table class="td-pad">
    <thead>	 
    <tr style="background: #e5e5e5; color: #1b1b1b">
        <th align="center">
            #
        </th>
        <th align="center">
             Transaction Date 
        </th>
        <th align="center">
        	 Details 
        </th>
        <th align="center">
            Transaction Id
        </th>
		<th align="center">
            Room
        </th>
        <th align="center">
            Transaction Type
        </th>
        <th align="center">
            Transaction From
        </th>
        <th align="center">
            Transaction To
        </th>

        <th align="center">
            Payment Mode
        </th>
      <th align="center">
             Expense 
      </th>
		<th align="center">
             Income
        </th>
    </tr>
    </thead>
    <tbody style="background: #F2F2F2">
    <?php 			$daily_sum=0;
                    $broker_sum=0;
					$vendor_amount=0;
					$government_amount=0;
					$expense=0;
					$income=0;
					$vendor_pay=0;
					$gov_pay=0;
					$broker_amount=0;
					$channel_sum=0;
					$g_fund_sum=0;
					$misc_sum=0;
					$rest_sum=0;
					$reten_sum=0;
					$comp_sum=0;
					$tax_sum=0;
					$ven_sum=0;
					$channel_pay=0;
					$misc_pay_sum=0;
					$sal_pay=0;
					$sal_amount=0;
					$misc_amount=0;
                    ?>
    
    <?php if(isset($transactions) && $transactions):
        $daily_sum=0;
        $i=1;
        $deposit=0;
        $srl_no=0;
        foreach($transactions as $transaction):
            $srl_no++;


            $class = ($i%2==0) ? "active" : "success";

            $transaction_id='HM00TA00'.$transaction->t_id;
			 $booking_id=$transaction->t_booking_id;
							if(isset($booking_id) && $booking_id){
							$query=$this->dashboard_model->cust_name($booking_id);
							foreach($query as $bk_details){
								$rm_id=$bk_details->room_id;
								$rmD=$this->dashboard_model->get_room($rm_id);
								$room_no=$rmD->room_no;
							}}
            ?>

             <tr > 
                <!--<td><input type="checkbox" name="delete[]" value="<?php //echo $transaction->t_id;?>" class="form-control" onclick="this.form.submit();"/></td>-->
                <td align="center" style="width: 27px;"><?php echo $srl_no; ?></td>
                <td align="center"><?php echo $transaction->t_date; ?></td>
                <td align="center"><?php
				if($transaction->t_booking_id!=0){
					echo 'HM0'.$this->session->userdata('user_hotel').'00'.$transaction->t_booking_id;
				}
				elseif($transaction->details_id!=0 ){
					echo 'HM0'.$this->session->userdata('user_hotel').'00'.$transaction->details_id;
				}
				elseif($transaction->t_group_id!=0 ){
					echo 'HM0'.$this->session->userdata('user_hotel').'00GRPc'.$transaction->t_group_id;
				}
				
				
				?></td>
                <td align="center"><?php echo $transaction_id;?></td>
				<td align="center"><?php echo $room_no; ?></td>
                <?php foreach($types as $type): ?>
                <?php if($type->hotel_transaction_type_id==$transaction->transaction_releted_t_id): ?>
               
               <td align="center"><?php echo $type->hotel_transaction_type_name; ?></td>
                <?php endif; ?>
                <?php endforeach; ?>
                <?php foreach($entity as $ent): //print_r($ent);?>
                <?php if($ent->hotel_entity_type_id==$transaction->transaction_from_id): ?>
               <td align="center"><?php echo $ent->entity_name; ?></td>
                <?php endif; ?>
                <?php endforeach; ?>
                <?php foreach($entity as $ent): ?>
                <?php if($ent->hotel_entity_type_id==$transaction->transaction_to_id): ?>
               <td align="center"><?php echo $ent->entity_name; ?></td>
                <?php endif; ?>
                <?php endforeach; ?>
                <?php //print_r($transaction);?>
               <td align="center"><?php echo $transaction->t_payment_mode;?></td>
                <td align="center"><?php foreach($entity as $ent){
								
					
								if($ent->hotel_entity_type_id==$transaction->transaction_to_id){
									 $to_vendor=$ent->entity_name;
									
									if($to_vendor=="Vendor"){
										$vendor_amount=$vendor_amount+$transaction->t_amount;
										echo $vendor_pay=$transaction->t_amount;
									}if($to_vendor=="Broker"){
										$broker_amount=$broker_amount+$transaction->t_amount;
										echo $broker_pay=$transaction->t_amount;
									}
									if($to_vendor=="Government"){
										$government_amount=$government_amount+$transaction->t_amount;
										echo $gov_pay=$transaction->t_amount;
									}
									if($to_vendor=="Channel"){
									$channel_amount=$channel_amount+$transaction->t_amount;
									echo $channel_pay=$transaction->t_amount;
									}
									if($to_vendor=="Employee"){
									$sal_amount=$sal_amount+$transaction->t_amount;
									echo $sal_pay=$transaction->t_amount;
									
									}
									if($to_vendor=="Debtor"){
									$misc_amount=$misc_amount+$transaction->t_amount;
									echo $misc_pay=$transaction->t_amount;
									}
									if($to_vendor!="Government" && $to_vendor!="Vendor" && $to_vendor!="Broker" && $to_vendor!="Channel" && $to_vendor!="Employee" && $to_vendor!="Debtor"){
										echo 0;
									}
									
								}
							}
						
							?>
               <td align="center"><?php foreach($entity as $ent){
							
								if($ent->hotel_entity_type_id==$transaction->transaction_to_id){
								  $to_vendor=$ent->entity_name;
										
									if($to_vendor=="Vendor"){
										$vendor_amount=$vendor_amount+$transaction->t_amount;
										$vendor_pay=$transaction->t_amount;
									}
									if($to_vendor=="Government"){
										$government_amount=$government_amount+$transaction->t_amount;
										$gov_pay=$transaction->t_amount;
									}
									if($to_vendor=="Hotel"){
									$h_amount=$government_amount+$transaction->t_amount;
									echo $h_pay=$transaction->t_amount;
									}
									
									if($to_vendor!="Government" && $to_vendor!="Vendor" && $to_vendor!="Broker" && $to_vendor!="Channel" && $to_vendor!="Debtor" && $to_vendor!="Hotel" && $to_vendor!="Employee"){
										echo $income=$transaction->t_amount;
									}
									
									if($to_vendor!="Hotel"){
										echo 0;
										}
									
				}}?></td>
      </tr>
            <?php

                           if($transaction->transaction_releted_t_id==1 ){
								$i++;
								//$daily_sum=0;								
								$daily_sum=$daily_sum+$transaction->t_amount;							
                            }
                            else if($transaction->transaction_releted_t_id==3){
								//$broker_sum=0;
								$broker_sum=$broker_sum+$transaction->t_amount;
                            }
							else if($transaction->transaction_releted_t_id==10){
								//$g_fund_sum=0;
								$channel_sum=$channel_sum+$transaction->t_amount;
							}
							else if($transaction->transaction_releted_t_id==2){
								//$g_fund_sum=0;
								$g_fund_sum=$g_fund_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==13){
								//$misc_sum=0;
								$misc_sum=$misc_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==14){
								
								//$rest_sum=0;
								$rest_sum=$rest_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==6){
							
								//$reten_sum=0;
							 $reten_sum=$reten_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==11){
							
								//$comp_sum=0;
								$comp_sum=$comp_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==4){
								
								//$tax_sum=0;
								$tax_sum=$tax_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==5){
								
								//$ven_sum=0;
								$ven_sum=$ven_sum+$transaction->t_amount;
						
							}
							else if($transaction->transaction_releted_t_id==12){
								
								//$misc_pay_sum=0;
								$misc_pay_sum=$misc_pay_sum+$transaction->t_amount;
						
							}

						//exit;	
                            ?>
              <?php endforeach; ?>
              <?php endif;?>
              <?php 
						
			  ?>

       
    
    </tbody>

</table>
<table>
<tr>
      <td width="100%">&nbsp;</td>
  </tr>
</table>
<?php 
		if(isset($to_vendor)){		
				
				?>
          <table class="td-pad">
            <thead>	 
            <tr style="background: #e5e5e5; color: #1b1b1b">
                <th scope="col">Type</th>
                <th scope="col">Description</th>
                <th scope="col">Income</th>
                <th scope="col">Expenditure</th>
              </tr>
            </thead>
            <tbody style="background: #F2F2F2">
              <?php
				
					
						 $transaction_type=$this->dashboard_model->transaction_type();
						$i=count($transaction_type);
						
						if(isset($transaction_type) && $transaction_type){
							//$sl_no=0;
							
							//print_r($transaction_type);
						//exit;
				foreach($transaction_type as $t_type){
								//$sl_no++;
								
								
						//$sum=0;
						//if(isset($avgdailyrent) && $avgdailyrent){
						//foreach($avgdailyrent as $avg){
							//echo $sum=$sum+$avg->room_rent_total_amount;
						
                ?>
              <tr>
					<td align="center"><?php
							
									echo $t_type->hotel_transaction_type_name;
							
					?></td>
					<td align="center"><?php echo $t_type->hotel_transaction_type_description; ?></td>
					<td align="center"><?php 
					
						//if($t_type->hotel_transaction_type_cat=="Income"){
							
					   if($t_type->hotel_transaction_type_name=="Booking payment"){
							echo  number_format($daily_sum);
						}
						else if($t_type->hotel_transaction_type_name=="Misc Income"){
							echo number_format($misc_sum);
						}
						else if($t_type->hotel_transaction_type_name=="Retension money"){
							echo  number_format($reten_sum);
						}
						
							else{echo "0";}
						
						//}
				
					
				?></td>
                <td align="center"><?php 
				
					//if($t_type->hotel_transaction_type_cat=="Expense"){
						if($t_type->hotel_transaction_type_name=="Broker payment"){
							echo number_format($broker_amount);
						}
						else if($t_type->hotel_transaction_type_name=="Channel payment"){
						echo number_format($channel_sum);
						}
						else if($t_type->hotel_transaction_type_name=="Guest refund"){
							echo number_format($g_fund_sum);
						}
						else if($t_type->hotel_transaction_type_name=="Vendor payment")
					{
					    	echo number_format($ven_sum);
						
					}
					else if($t_type->hotel_transaction_type_name=="Tax paymemnt"){
						echo number_format($tax_sum);
						//echo $government_amount;
						
					}
					else if($t_type->hotel_transaction_type_name=="Misc Payment"){
						echo number_format($misc_pay_sum);
						
					}
					else if($t_type->hotel_transaction_type_name=="Salary payment"){
						echo number_format($sal_amount);
						
					}
					else{
						echo "0";
					}
						
					//}
					
					
				
					
				
				
					?></td>
              </tr>
              <?php }}?>
            </tbody>
          </table>
          <?php } ?>
<table>
	<tr>
    	<td>&nbsp;</td>
    </tr>
</table>

<table>
 <tr>
      <td colspan="2"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
		<tr>
            <td align="center"> <strong>Total Amount income : </strong><?php  
			$total_income=$daily_sum+$rest_sum+$reten_sum+$misc_sum;
			echo number_format($total_income);
		?></td>
        </tr>
		<tr>
            <td align="center"><strong> Total Amount expanse : </strong> <?php  
			$total_exp=0;
			$total_exp=$total_exp+$broker_amount+$channel_pay+$ven_sum+$tax_sum+$misc_pay_sum+$sal_amount;
			echo number_format($total_exp);
		?></td>
            </tr>
			 <tr>
      <td colspan="2"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
  </table>
  </div>