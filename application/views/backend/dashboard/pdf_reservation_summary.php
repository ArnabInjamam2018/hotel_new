<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td{
	font-size:9px;
}

</style>
<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));?>
<div style="padding:15px 35px;">
<table>
		
    <tr>
	<?php if(isset($hotel_name->hotel_logo_images_thumb)) { ?>
      <td align="left"><img src="upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/></td>
    <?php } ?>
		<td colspan="3" align="right"><?php echo "<strong><font size='12'>".$hotel_name->hotel_name.'</font></strong>'?></td>
        	
    </tr>
    <tr>
      <td width="100%" colspan="4"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td colspan="4"><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%" colspan="4">&nbsp;</td>
    </tr>
</table>

<table >

    <tr>
              <td scope="col" width="5%">#</td>
              <td scope="col" width="5%">Booking Id</td>
              <!--<td scope="col" width="5%">Folio</td>-->
              <td scope="col" width="5%">Guest Name</td>
              <!--<td scope="col" width="5%">State</td>
			  <td scope="col" width="5%">Phone</td>
			  <td scope="col" width="5%">Email</td>-->
			  <td scope="col" width="5%">Room Type</td>
              <td scope="col" width="5%">Room</td>
              <td scope="col" width="5%">PAX</td>
			  <td scope="col" width="5%">Status</td>
			  <td scope="col" width="5%">Check In</td>
              <td scope="col" width="5%">Check Out</td>	
		  
			  <td scope="col" width="5%">Paid </td>
			  <td scope="col" width="5%">Pending Balance</td>
            
    </tr>

<?php //exit();?>  

            
              <?php 
			    //echo "<pre>";
			    if(isset($summary) && $summary != ""){
				    $srl_no=0;
					$ex_sum = 0;
					foreach($summary as $sum){
					//echo $sum->g_email;	
					  //print_r($sum->room_no);
					  //exit;					
					$srl_no++;
					$booking_id=$sum->booking_id;
					if(isset($booking_id) && $booking_id != ""){
						//echo $booking_id;
						//exit;
						$transaction = $this->bookings_model->get_total_payment($booking_id);
						$paid_amount=0; 
                    if(isset($transaction) && $transaction != ""){						
					    foreach ($transaction as $transactions)
						{
							$paid_amount = $paid_amount + $transactions->t_amount;
						}	
                    }
						$from_days=$sum->cust_from_date_actual;
						$end=$sum->cust_end_date_actual;
						if(isset($from_days) && isset($end)){
						 $days = (strtotime($end) - strtotime($from_days)) / (60 * 60 * 24);
						} else {
							$days = 0;
						}
						//print $days;
						//echo $maxdiff;
					    //exit;
						   
						$pending=$sum->room_rent_sum_total-$paid_amount;
						
						
					}
					
			  ?>
			  	<tr>
				<td align="center" ><?php echo $srl_no ;?></td>
			    <td align="center" ><?php if(isset($sum->booking_id_actual)){ echo $sum->booking_id_actual; }?></td>
                <!--<td align="center" >N/A</td>-->
                <td align="center" ><?php if(isset($sum->cust_name)){ echo $sum->cust_name; }?></td>
                <!--<td align="center" ><?php //if(isset($sum->g_state)){ echo $sum->g_state;}?></td> 
                <td align="center" ><?php //if(isset($sum->g_contact_no)){ echo $sum->g_contact_no;}?></td>
                <td align="center" ><?php //if(isset($sum->g_email)){ echo $sum->g_email;}?></td>-->
				<td align="center" ><?php if(isset($sum->unit_name)){ echo $sum->unit_name;}?></td>
				<td align="center" ><?php if(isset($sum->room_no)){ echo $sum->room_no;}?></td>
				<td align="center"><?php if(isset($sum->no_of_guest) && $sum->no_of_guest !=""){ echo $sum->no_of_guest;}?></td>
				<td align="center"><?php echo $sum->booking_status;?></td>
				<td align="center"><?php echo $sum->cust_from_date_actual;?></td>
                <td align="center"><?php echo $sum->cust_end_date_actual;?></td>
                				
				<td align="center" ><?php echo $paid_amount;?></td>
                <td align="center" >
				<?php if(isset($pending) && $pending > 0){
						echo $pending;
					} else {
						echo "0";
					};
				?>
				</td>
			  
			</tr>
            <?php } }?>

</table>
</div>
