 <style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td{
	font-size:12px;
}
</style>
<div style="padding:15px 35px;">     
  <table>
		<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));?>
    <tr>
      	<td align="left"> <img src="upload/hotel/<?php if(isset($hotel_name->hotel_logo_images_thumb))echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/></td>
        <td colspan="2" align="center"><strong><font size='13'>Transfer Asset</font></strong></td>
		<td align="right"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?></td>
    </tr>
    <tr>
      <td width="100%" colspan="4"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td>
	<?php if(isset($start_date) && isset($end_date) && $start_date && $end_date ){
		echo "<strong>From: </strong>".$start_date."  <strong>To: </strong>".$end_date;
	}
		?>
	</td>
	<td align="left"><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%" colspan="4">&nbsp;</td>
    </tr>
</table>

        <table class="table table-striped table-bordered table-hover" id="sample_1">
          <thead>
            <tr> 
              <!-- <th scope="col">
                                Select
                            </th>-->
              <th scope="col">Serial No </th>
              <th scope="col">Upload GRN/ Transfer Voucher image </th>
              <th scope="col">Date </th>
             
              <th scope="col">item</th>
              <th scope="col">From Warehouse </th>
              <th scope="col">To Warehouse</th>
              <th scope="col">Qty</th>
              <th scope="col">Note</th>
             
			  
            </tr>
          </thead>
          <tbody>
		  <?php 
			$srl_no=0;
		  if(isset($transfer_asset) && $transfer_asset ){
				//print_r($transfer_asset);//exit;
			foreach($transfer_asset as $asset){
			$srl_no++;
		  ?>
		  
		  <tr style="background: #F2F2F2">
           <td align="center"><?php echo $srl_no; ?></td>
           <td align="center"><img  width="60px" height="60px" src="<?php echo base_url();?>upload/asset/<?php if( $asset->grn_image== '') { echo "no_images.png"; } else { echo $asset->grn_image; }?>" alt=""/>
		   <img  width="60px" height="60px" src="<?php echo base_url();?>upload/asset/<?php if( $asset->transfr_vou_image== '') { echo "no_images.png"; } else { echo $asset->transfr_vou_image; }?>" alt=""/>
		   </td>
           <td align="center"><?php echo $asset->date;?></td>
          
           <td align="center"><?php  if(isset($asset->item_id)){
			    $item_name=$this->dashboard_model->get_stock_item($asset->item_id);
			echo $item_name->a_name;
			   
		   }?></td>
           <td align="center"><?php 
		   $warehouse_id=$asset->from_warehouse_id;
		   $wharehouse=$this->dashboard_model->get_warehouse($warehouse_id);
			echo $wharehouse->name;
		   ?></td>
           <td align="center"><?php $id=$asset->to_warehouse_id;
		    $wharehouse=$this->dashboard_model->get_warehouse($id);
			echo $wharehouse->name;
		   ?></td>
           <td align="center"><?php echo $asset->qty;?></td>
           <td align="center"><?php echo $asset->Note;?></td>
           
           </tr>
		  <?php }}?>
          </tbody>
        </table>
      </div>