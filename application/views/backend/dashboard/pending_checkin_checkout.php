<div class="colr">
  <ul >
    <li> <span class="brdr " style="background:#f1eda5;"></span> Due Within 4 days</li>
    &nbsp;&nbsp;
    <li> <span class="brdr " style="background:#ffb9de;"></span> Due Past</li>
    &nbsp;&nbsp;
    <li> <span class="brdr " style="background:#87f6ff;"></span> Due Today</li>
    <li> <span class="brdr " style="background:#83ffbd;"></span> Due Later</li>
  </ul>
</div>
<div class="portlet light bordered">
<div class="portlet-title">
  <div class="caption"> <i class="fa fa-edit"></i>List of All Pending Bookings </div>
  <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
</div>
<div class="portlet-body">
  <div class="table-toolbar">
    <div class="row">
      <div class="col-md-4">
        <div class="btn-group">
          <label for="autocellwidth" class="auto_cl btn" id="auto_cl_id" style="background-color:#61FF00;">
            <input type="checkbox" id="autocellwidth" class="toggle" style="display:none;">
            <span id="disp">Checkin</span> </label>
        </div>
        <div class="btn-group">
          <input type="hidden" id="curdate" value="<?php echo date('Y-m-d');?>"  />
          <label  class="auto_cl btn grey-cascade" onclick="today_dis()" id="show_today"> <span >Todays Booking</span> </label>
        </div>
        <div class="btn-group">
          <label  class="auto_cl btn grey-cascade" onclick="up_dis()" id="show_up"> <span >Expected</span> </label>
        </div>
        <div class="btn-group">
          <label  class="auto_cl btn grey-cascade" onclick="due_dis()" id="show_due"> <span >Due</span> </label>
        </div>
      </div>
      <div class="col-md-5">
        <?php

                            $form = array(
                                'class'       => 'form-inline',
                                'id'        => 'form_date',
                                'method'      => 'post'
                            );

                            echo form_open_multipart('dashboard/get_pending_booking_report_by_date',$form);

                            ?>
        <div class="form-group">
          <input type="text" autocomplete="off" required="required" value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
        </div>
        <div class="form-group">
          <input type="text" autocomplete="off" required="required" value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
        </div>
        <button class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search"></i></button>
        <?php form_close(); ?>
      </div>
      <script type="text/javascript">
            function check_sub(){
               document.getElementById('form_date').submit();
            }
        </script> 
    </div>
  </div>
  <table class="table table-striped table-bordered table-scrollable" id="sample_1">
    <thead>
      <tr>
        <th>Booking Id </th>
        <th>Booking Status </th>
        <th>Customer Name </th>
        <th>Checkin date </th>
        <th>Pending For</th>
        <th>Total Booking Amount</th>
      </tr>
    </thead>
    <tbody>
	
      <?php
//$bookings=$this->bookings_model->all_bookings();
	  if(isset($bookings) && $bookings):
                        //echo "<pre>";
						//print_r($bookings);exit;
                       $i=1;
                       $color='';
                        $deposit=0;
                        $item_no=0;
                        foreach($bookings as $booking):
                            $bills=$this->bookings_model->all_folio($booking->booking_id);
                            
                                     $times=$this->dashboard_model->checkout_time();
                                    foreach($times as $time) {
                                    if($time->hotel_check_in_time_fr=='PM') {
                                        if($time->hotel_check_in_time_hr == 12) {
                                          $modifier = ($time->hotel_check_in_time_hr);
                                        } else {
                                          $modifier = ($time->hotel_check_in_time_hr + 12);	
                                        }
                                    }
                                    else{
                                        $modifier = ($time->hotel_check_in_time_hr);
                                    }
                                    $hotel_check_in_time_mm = $time->hotel_check_in_time_mm;
                                        }
                                    
                                    
                                    
                                    $cust_from_date=$booking->cust_from_date;
                                    $exp=explode(' ',$cust_from_date);
                                    $cust_to=new DateTime($booking->cust_end_date);
                                    $cust_from=new DateTime($exp[0]." ".$modifier.":".$hotel_check_in_time_mm.":00");
                                    $cur= new DateTime();
                                    
                                    $dDiff = $cust_from->diff($cur);
                                    $days_s= $dDiff->format('%r%a');
                                    $hours_s = $dDiff->format('%r%h');
                                    $min_s = $dDiff->format('%r%i');
                                    
                                  /*  $dDiff = $cust_to->diff($cur);
                                    $days_e= $dDiff->format('%r%a');
                                    $hours_e = $dDiff->format('%r%h');
                                    $min_e = $dDiff->format('%r%m');*/
                                    
                    if(
                    (((($booking->booking_status_id == 4) || ($booking->booking_status_id == 1) || ($booking->booking_status_id == 2)) /*&& ($days_s >= 0) && ($hours_s >= 0)*/))
                    /*||	
                    (($booking->booking_status_id == 5)&&($days_e > 0))*/
                      )
                        {		
                            if(isset($bills)){
                        
                        foreach ($bills as $item) {
                        # code...

                          $item_no++;

                            } }
                            
                            $disp="";
                            
                            
                            if(($days_s > 0))
        {
            $disp = $disp.' Due';	
        }
        else if(($days_s == 0) && (($hours_s >= 0) || ($min_s >= 0)))
        {
            $disp = $disp.' Due';	
        }
        else if(($days_s <= 0) && (($hours_s < 0) || ($min_s < 0)))
        {
            $disp = $disp.' Remaining';
        }
                            
                            
                            if(($days_s == 0))
                            {
                                if(($hours_s < 2))
                                {
                                    $color='#83ffbd';
                                }
                                else
                                {
                                    $color='#f1eda5';
                                }
                            }
                            if(($days_s > 0) && ($days_s < 6))
                            {
                                $color='#f1eda5';
                            }
                            if(($days_s >= 6))
                            {
                                $color='#ffb9de';
                            }
            ?>
      <?php
            
                            $rooms=$this->dashboard_model->get_room_number($booking->room_id);
                            if(isset($rooms) && $rooms):
                            foreach($rooms as $room):
                            
                            
                                if($room->hotel_id==$this->session->userdata('user_hotel')):

								if(isset($transactions) && $transactions){
                                    foreach($transactions as $transaction):

                                        if($transaction->t_booking_id=='HM0'.$this->session->userdata('user_hotel').'00'.$booking->booking_id):

                                            $deposit=$deposit+$transaction->t_amount;
                                        endif;
                                    endforeach;

                                }

                                    /*Calculation start */
                                   
                                    $room_number=$room->room_no;
                                    $room_cost=$room->room_rent;
                                    /* Find Duration */
                                    $date1=date_create($booking->cust_from_date);
                                    $date2=date_create($booking->cust_end_date);
                                    $diff=date_diff($date1, $date2);
                                    $cust_duration= $diff->format("%a");
                                    
                                    

                                    /*Cost Calculations*/
                                    $total_cost=$cust_duration*$room_cost;
                                    $total_cost=$total_cost+$total_cost*0.15;
                                    $food_tax=0;
                                    if($booking->food_plans!="")
                                    {
                                    $data=$this->bookings_model->fetch_food_plan($booking->food_plans);
                                    if($data)
                                    $food_tax=($data->fp_tax_percentage/100)*$booking->food_plan_price;
                                    }
                  $due=($booking->room_rent_sum_total+$booking->service_price+$booking->food_plan_price+$food_tax+$booking->booking_extra_charge_amount)-$booking->cust_payment_initial-$deposit;

                                    $status_id=$booking->booking_status_id;

                                    $status=$this->dashboard_model->get_status_by_id($status_id);
                                    
                                    if(isset($status)){
                                        //print_r($status);
                                        foreach($status as $st){
                                        
                                    /*Calculation End */


                                    $class = ($i%2==0) ? "active" : "success";

                                    $booking_id='HM0'.$this->session->userdata('user_hotel').'00'.$booking->booking_id;
                                    $book_id=$booking->booking_id;
                                    ?>
      <tr style="background-color:<?php echo  $color; ?>;">
        <td><?php if($booking->group_id !="0"){
                echo '<i class="fgroup"></i>';
                } 
                else{
                 echo '<i class="fa fa-user"></i>';
                } 	
                ?>
          <a 

              <?php if($booking->group_id !="0"){
                echo 'style="color:green; " ';
             } 
             ?>
              href="<?php echo base_url();?>dashboard/booking_edit?b_id=<?php echo $booking->booking_id ?>"><?php echo $booking_id?></a>
		</td>
		
        <td>
			<span class="label" style="background-color:<?php echo $st->bar_color_code ?>; color:<?php echo $st->body_color_code ?>;">
				<?php  echo $st->booking_status?>
            </span>
		</td>
		
        <td>
			<?php 

              $guests=$this->dashboard_model->get_guest_details($booking->guest_id);
                if(isset($guests) && $guests){
                 foreach ($guests as $key ) {
               if($key->g_name!=""){
                echo $key->g_name;
               }
               else{
                   echo "na";
               }
                }}
            ?>
		</td>
        <?php 
                    date_default_timezone_set('Asia/Kolkata');
                    $sum=0;
                if($booking->booking_extra_charge_id != "")
                {
                    $e_chr= explode(',',$booking->booking_extra_charge_id);
                    for($k=0;$k<count($e_chr);$k++)
                    {
                        $ecs=$this->dashboard_model->get_charge_details($e_chr[$k]);
                        if(isset($ecs) && $ecs)
                        {
                            
                        $sum=$sum+$ecs->crg_tax;
                        
                        }
                    }
                    
                } //echo number_format($sum,2,'.',',');
                ?>
        <?php  $tax_ammunt=$booking->room_rent_total_amount* ($booking->luxury_tax/100);?>
        <?php $service_tax_amount=$booking->room_rent_total_amount* ($booking->service_tax/100);?>
        <?php  $extra_ch_amount=$booking->room_rent_total_amount* ($booking->service_charge/100);?>
        <?php  $f=0; if(isset($fd_vat->fvat)) {  $f=$fd_vat->fvat; } /*echo $f;*/?>
        <?php  $l=0; if(isset($fd_vat->lvat)) {  $l=$fd_vat->lvat; } /*echo $l;*/ ?>
        <?php 
                $fd_vat=$this->dashboard_model->get_fd_tax_by_id($booking->booking_id);
                    if(isset($fd_vat)){
                     $total=$booking->room_rent_total_amount+$fd_vat->fvat+$fd_vat->lvat+$tax_ammunt+$service_tax_amount+$extra_ch_amount+$sum;
                    }
                else{
                     $total=$booking->room_rent_total_amount+$tax_ammunt+$service_tax_amount+$extra_ch_amount+$sum;
                }
                 ?>
        <td>
			<?php 
				$ex_arry = explode(' ',$booking->cust_from_date); 
				echo date("l jS F Y",strtotime($ex_arry[0]));
			?>
		</td>
		
        <td><?php echo abs($days_s)." Day ".abs($hours_s)." hr ".abs($min_s)." Min ".$disp;?></td>
		<?php
		/*$info_lineItem=$this->dashboard_model->get_bookingLineItem($booking->booking_id);
			if(isset($info_lineItem)){
			$rr_tot=$info_lineItem->rr_tot;
			$rr_tot_tax=$info_lineItem->rr_tot_tax;
			$exrr=$info_lineItem->exrr;
			$exrr_tax=$info_lineItem->exrr_tax;
			$t=$rr_tot+$rr_tot_tax+$exrr+$exrr_tax;
			}else{
				$t=0;
			}
*/
if(isset($booking->rm_total)){
	$rm_total=$booking->rm_total;
}
else{
	$rm_total=0;
}
if(isset($booking->rr_tot_tax)){
$rr_tot_tax=$booking->rr_tot_tax;
}else{
	$rr_tot_tax=0;
}
if(isset($booking->exrr)){
$exrr=$booking->exrr;
}else{
	$exrr=0;
}
if(isset($booking->exrr_tax)){
	$exrr_tax=$booking->exrr_tax;	
}else{
	$exrr_tax=0;
}

if(isset($booking->mp_tot)){
$mp_tot=$booking->mp_tot;
}else{
	$mp_tot=0;
}
if(isset($booking->mp_tax)){
$mp_tax=$booking->mp_tax;
}else{
	$mp_tax=0;
}
if(isset($booking->exmp)){
$exmp=$booking->exmp;
}else{
	$exmp=0;
}
if(isset($booking->exmp)){
$exmp_tax=$booking->exmp_tax;
}else{
	$exmp_tax=0;
}
$total_a=$rm_total+$rr_tot_tax+$exrr+$exrr_tax+$mp_tot+$mp_tax+$exmp+$exmp_tax;
		?>
        <td><?php echo '<i class="fa fa-inr" aria-hidden="true"></i> '.number_format($total_a,2,'.',',');?></td>
      </tr>
    <input type="hidden" id="item_no" value="<?php echo $item_no;?>">
      </input>
    
    <?php $i++;}}?>
    <?php endif; ?>
    <?php endforeach;?>
    <?php endif; ?>
    <?php }endforeach; ?>
    <?php endif;?>
      </tbody>
    
  </table>
  <table class="table table-striped table-bordered table-scrollable" id="sample_2">
    <thead>
      <tr>
        <th>Booking Id </th>
        <th>Booking Status </th>
        <th>Customer Name </th>
        <th>Checkout date </th>
        <th>Pending For</th>
        <th>Booking Amount</th>
      </tr>
    </thead>
    <tbody>
      <?php if(isset($bookings) && $bookings):
                        //print_r($bookings);
                       $i=1;
                        $deposit=0;
                        $item_no=0;
                        foreach($bookings as $booking):
                            $bills=$this->bookings_model->all_folio($booking->booking_id);
                                /*	$cust_from=date_create($booking->cust_from_date);
                                    $cust_to=date_create($booking->cust_end_date);
                                    $cur=date_create(date('Y-m-d H:i:s'));*/
                                    
                                     $times=$this->dashboard_model->checkout_time();
                                    foreach($times as $time) {
                                    if($time->hotel_check_out_time_fr=='PM') {
                                        if($time->hotel_check_out_time_hr == 12) {
                                          $modifier = ($time->hotel_check_out_time_hr);
                                        } else {
                                          $modifier = ($time->hotel_check_out_time_hr + 12);	
                                        }
                                    }
                                    else{
                                        $modifier = ($time->hotel_check_out_time_hr);
                                    }
                                    $hotel_check_out_time_mm = $time->hotel_check_out_time_mm;
                                        }
                                    
                                    
                                    
                                    $cust_end_date=$booking->cust_end_date;
                                    $exp=explode(' ',$cust_end_date);
                                    $cust_from=new DateTime($booking->cust_from_date);
                                    $cust_to=new DateTime($exp[0]." ".$modifier.":".$hotel_check_out_time_mm.":00");
                                    $cur= new DateTime();
                                    
                                
                                  /*  $dDiff = $cust_from->diff($cur);
                                    $days_s= $dDiff->format('%r%a');*/
                                    
                                    $dDiff = $cust_to->diff($cur);
                                    $days_e= $dDiff->format('%r%a');
                                    $hours_e = $dDiff->format('%r%h');
                                    $min_e = $dDiff->format('%r%i');
                         
                         
                         $disp="";
                         
        if(($days_e > 0))
        {
            $disp = $disp.' Due';	
        }
        else if($days_e < 0){
            $disp = ' Remaining';
        }	
        else if(($days_e == 0) && (($hours_e >= 0) |  ($min_e >= 0)))
        {
            $disp = $disp.' Due';	
        }
        else if(($days_e == 0) && (($hours_e < 0) || ($min_e < 0)))
        {
            $disp = $disp.' Remaining';
        }
                         
                         
                                    
                    if(($booking->booking_status_id == 5)/*&&($days_e >= 0) && ($hours_e>=0)*/)
                        {		
                        if(isset($bills)){
                        
                        foreach ($bills as $item) {
                        # code...

                          $item_no++;

                            } }
                            
                            if(($days_e == 0))
                            {
                                if(($hours_e < 2))
                                {
                                    $color='#83ffbd';
                                }
                                else
                                {
                                    $color='#f1eda5';
                                }
                            }
                            if(($days_e > 0) && ($days_e < 6))
                            {
                                $color='#f1eda5';
                            }
                            if(($days_e >= 6))
                            {
                                $color='#ffb9de';
                            }
                            
            ?>
      <?php
            
                            $rooms=$this->dashboard_model->get_room_number($booking->room_id);
                            if(isset($rooms) && $rooms):
                            foreach($rooms as $room):
                            
                            
                                if($room->hotel_id==$this->session->userdata('user_hotel')):

								if(isset($transactions) && $transactions){
                                    foreach($transactions as $transaction):

                                        if($transaction->t_booking_id=='HM0'.$this->session->userdata('user_hotel').'00'.$booking->booking_id):

                                            $deposit=$deposit+$transaction->t_amount;
                                        endif;
                                    endforeach;
								}

                                

                                    /*Calculation start */
                                   
                                    $room_number=$room->room_no;
                                    $room_cost=$room->room_rent;
                                    /* Find Duration */
                                    $date1=date_create($booking->cust_from_date);
                                    $date2=date_create($booking->cust_end_date);
                                    $diff=date_diff($date1, $date2);
                                    $cust_duration= $diff->format("%a");
                                    
                                    

                                    /*Cost Calculations*/
                                    $total_cost=$cust_duration*$room_cost;
                                    $total_cost=$total_cost+$total_cost*0.15;
                                    $food_tax=0;
                                    if($booking->food_plans!="")
                                    {
                                    $data=$this->bookings_model->fetch_food_plan($booking->food_plans);
                                    if($data)
                                    $food_tax=($data->fp_tax_percentage/100)*$booking->food_plan_price;
                                    }
                  $due=($booking->room_rent_sum_total+$booking->service_price+$booking->food_plan_price+$food_tax+$booking->booking_extra_charge_amount)-$booking->cust_payment_initial-$deposit;

                                    $status_id=$booking->booking_status_id;

                                    $status=$this->dashboard_model->get_status_by_id($status_id);
                                    
                                    if(isset($status)){
                                        //print_r($status);
                                        foreach($status as $st){
                                        
                                    /*Calculation End */


                                    $class = ($i%2==0) ? "active" : "success";

                                    $booking_id='HM0'.$this->session->userdata('user_hotel').'00'.$booking->booking_id;
                                    $book_id=$booking->booking_id;
                                    ?>
      <tr style="background-color:<?php echo $color;?>;">
        <td>
			<?php if($booking->group_id !="0"){
                echo '<i class="fa fa-group"></i>';
                } 
                else{
                 echo '<i class="fa fa-user"></i>';
                } 	
                ?>
          <a 

              <?php if($booking->group_id !="0"){
                echo 'style="color:green; " ';
             } 
			 ?>
              href="<?php echo base_url();?>dashboard/booking_edit?b_id=<?php echo $booking->booking_id ?>"><?php echo $booking_id?></a>
		</td>
		
        <td><span class="label" style="background-color:<?php echo $st->bar_color_code ?>; color:<?php echo $st->body_color_code ?>;">
          <?php  echo $st->booking_status?>
          </span>
		</td>
		
        <td>
			<?php 

              $guests=$this->dashboard_model->get_guest_details($booking->guest_id);
                if(isset($guests) && $guests){
                 foreach ($guests as $key ) {
               if($key->g_name!=""){
                echo $key->g_name;
               }
               else{
                   echo "na";
               }
                }}
            ?>
		</td>
        <?php 
                    $sum=0;
                if($booking->booking_extra_charge_id != "")
                {
                    $e_chr= explode(',',$booking->booking_extra_charge_id);
                    for($k=0;$k<count($e_chr);$k++)
                    {
                        $ecs=$this->dashboard_model->get_charge_details($e_chr[$k]);
                        if(isset($ecs) && $ecs)
                        {
                            
                        $sum=$sum+$ecs->crg_tax;
                        
                        }
                    }
                    
                }//echo number_format($sum,2,'.',',');
                ?>
        <?php  $tax_ammunt=$booking->room_rent_total_amount* ($booking->luxury_tax/100);?>
        <?php $service_tax_amount=$booking->room_rent_total_amount* ($booking->service_tax/100);?>
        <?php  $extra_ch_amount=$booking->room_rent_total_amount* ($booking->service_charge/100);?>
        <?php  $f=0; if(isset($fd_vat->fvat)) {  $f=$fd_vat->fvat; } /*echo $f;*/?>
        <?php  $l=0; if(isset($fd_vat->lvat)) {  $l=$fd_vat->lvat; } /*echo $l;*/ ?>
        <?php 
                $fd_vat=$this->dashboard_model->get_fd_tax_by_id($booking->booking_id);
                    if(isset($fd_vat)){
                     $total=$booking->room_rent_total_amount+$fd_vat->fvat+$fd_vat->lvat+$tax_ammunt+$service_tax_amount+$extra_ch_amount+$sum;
                    }
                else{
                     $total=$booking->room_rent_total_amount+$tax_ammunt+$service_tax_amount+$extra_ch_amount+$sum;
                }
                 ?>
        <td>
			<?php 
				$ex_arry = explode(' ',$booking->cust_end_date); 
				echo date("l jS F Y",strtotime($ex_arry[0]));
			?>
		</td>
		
        <td><?php echo abs($days_e)." Day ".abs($hours_e)." Hr ".abs($min_e)." Min ".$disp; ?></td>
        <?php /*$info_lineItem=$this->dashboard_model->get_bookingLineItem($booking->booking_id);
        if(isset($info_lineItem)){
        $rr_tot=$info_lineItem['rr_tot'];
        $rr_tot_tax=$info_lineItem['rr_tot_tax'];
        $exrr=$info_lineItem['exrr'];
        $exrr_tax=$info_lineItem['exrr_tax'];
        $t=$rr_tot+$rr_tot_tax+$exrr+$exrr_tax;
        }else{
            $t=0;
        }*/
		if(isset($booking->rm_total)){
	$rm_total=$booking->rm_total;
}
else{
	$rm_total=0;
}
if(isset($booking->rr_tot_tax)){
$rr_tot_tax=$booking->rr_tot_tax;
}else{
	$rr_tot_tax=0;
}
if(isset($booking->exrr)){
$exrr=$booking->exrr;
}else{
	$exrr=0;
}
if(isset($booking->exrr_tax)){
	$exrr_tax=$booking->exrr_tax;	
}else{
	$exrr_tax=0;
}

if(isset($booking->mp_tot)){
$mp_tot=$booking->mp_tot;
}else{
	$mp_tot=0;
}
if(isset($booking->mp_tax)){
$mp_tax=$booking->mp_tax;
}else{
	$mp_tax=0;
}
if(isset($booking->exmp)){
$exmp=$booking->exmp;
}else{
	$exmp=0;
}
if(isset($booking->exmp)){
$exmp_tax=$booking->exmp_tax;
}else{
	$exmp_tax=0;
}
$total_a=$rm_total+$rr_tot_tax+$exrr+$exrr_tax+$mp_tot+$mp_tax+$exmp+$exmp_tax;
        ?>
        <td><?php echo '<i class="fa fa-inr" aria-hidden="true"></i> '.number_format($total_a,2,'.',',');?></td>
      </tr>
    <input type="hidden" id="item_no" value="<?php echo $item_no;?>">
      </input>
    
    <?php $i++;}}?>
    <?php endif; ?>
    <?php endforeach;?>
    <?php endif; ?>
    <?php }endforeach; ?>
    <?php endif;?>
      </tbody>
    
  </table>
</div>
</div>
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            //alert(id);

          



            $.ajax({
                
                url: "<?php echo base_url()?>dashboard/soft_delete_booking?id="+id,
				type:"POST",
                data:{id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });

        });
    }
	
function download_pdf(booking_id) {
	
	
	
	alert(booking_id);
	$("#dwn_link").attr("href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id);
	var f = $("#f");
            $.post(f.attr("action"), f.serialize(), function (result) {
            
                close(eval(result));
            });
	 //return false;
	
}
	
	
</script> 
<script>
	$(window).load(function() {
	//$('#sample_1').hide();
	($('#sample_2').DataTable()).destroy();
	$('#sample_2').hide();
	
	});
				$("#autocellwidth").click(function() {
                        document.getElementById('auto_cl_id').style.backgroundColor = $(this).is(":checked") ? "#9ED2CB" : "#61FF00";
                        var a = $(this).is(":checked") ? "Checkout" : "Checkin";
						if(a == 'Checkout')
						{	
							($('#sample_1').DataTable()).destroy();
							$('#sample_1').hide();
							$('#sample_2').dataTable();
							
							$('#sample_2').show();	
							$('#show_today').css('background-color','#A5A5A5');
							$('#show_due').css('background-color','#A5A5A5');
							$('#show_up').css('background-color','#A5A5A5');
						}
						else
						{	
							($('#sample_2').DataTable()).destroy();
							$('#sample_2').hide();
							$('#sample_1').dataTable();
							$('#sample_1').show();
							$('#show_today').css('background-color','#A5A5A5');
							$('#show_due').css('background-color','#A5A5A5');
							$('#show_up').css('background-color','#A5A5A5');
						}
						$('#disp').text(a);

                    });
					</script> 
<script>
					function today_dis()
					{
							$('#show_due').css('background-color','#A5A5A5');
							$('#show_up').css('background-color','#A5A5A5');
						
						if((($('#sample_1').DataTable()).search() == 'Remaining')  || (($('#sample_1').DataTable()).search() == 'Due')|| (($('#sample_2').DataTable()).search() == 'Due') || (($('#sample_2').DataTable()).search() == 'Remaining'))
						{
							($('#sample_1').DataTable()).destroy();
							($('#sample_2').DataTable()).destroy();
						}
						
				if((($('#sample_1').DataTable()).search() == '') && (($('#sample_2').DataTable()).search() == '') )
						{
							$('#show_today').css('background-color','#00CC99');

							
						 var b = $('#disp').text().trim();
						 //alert(a);
								if(b == 'Checkout')
								{
									//alert('blank');
									($('#sample_1').DataTable()).destroy();
									$('#sample_1').hide();
									
									$('#sample_2').show();
									($('#sample_2').DataTable()).search($('#curdate').val()).draw();
									
								
								}
								else
								{
									if((($('#sample_1').DataTable()).search() == 'Due')  || (($('#sample_1').DataTable()).search() == 'Remaining')|| (($('#sample_2').DataTable()).search() == 'Due') || (($('#sample_2').DataTable()).search() == 'Remaining'))
						{
							($('#sample_1').DataTable()).destroy();
							($('#sample_2').DataTable()).destroy();
						}
									
									($('#sample_2').DataTable()).destroy();
									$('#sample_2').hide();
									
									$('#sample_1').show();
									($('#sample_1').DataTable()).search($('#curdate').val()).draw();
									
														
									}
						}
				else
						{
							$('#show_today').css('background-color','#829596');
						
						 var b = $('#disp').text().trim();
						 //alert(b);
								if(b == 'Checkout')
									{
										
										
									($('#sample_1').DataTable()).destroy();
									$('#sample_1').hide();
									
									
									($('#sample_2').DataTable()).search("").draw();
									($('#sample_2').DataTable()).search("");
									$('#sample_2').show();
									
								}
								else
								{
										($('#sample_2').DataTable()).destroy();
										$('#sample_2').hide();
										
										($('#sample_1').DataTable()).search("").draw();
										($('#sample_1').DataTable()).search("");
										$('#sample_1').show();
								}	
						}
						
					}
					
</script> 
<script>
function up_dis()
					{
						
							$('#show_today').css('background-color','#A5A5A5');
							$('#show_due').css('background-color','#A5A5A5');
						
						
						if((($('#sample_1').DataTable()).search() == $('#curdate').val())  || (($('#sample_1').DataTable()).search() == 'Due')|| (($('#sample_2').DataTable()).search() == 'Due') || (($('#sample_2').DataTable()).search() == $('#curdate').val()))
						{
							
							($('#sample_1').DataTable()).destroy();
							($('#sample_2').DataTable()).destroy();
						}
						
				if((($('#sample_1').DataTable()).search() == '') && (($('#sample_2').DataTable()).search() == '') )
						{
							$('#show_up').css('background-color','#00CC99');
							
						 var b = $('#disp').text().trim();
						 //alert(a);
								if(b == 'Checkout')
								{
									//alert('blank');
									($('#sample_1').DataTable()).destroy();
									$('#sample_1').hide();
									
									$('#sample_2').show();
									($('#sample_2').DataTable()).search('Remaining').draw();
									
								
								}
								else
								{
									
									($('#sample_2').DataTable()).destroy();
									$('#sample_2').hide();
									
									$('#sample_1').show();
									($('#sample_1').DataTable()).search('Remaining').draw();
									
														
									}
						}
				else
						{
							
							
							
							
							
							
							$('#show_up').css('background-color','#829596');
						
						 var b = $('#disp').text().trim();
						 //alert(b);
								if(b == 'Checkout')
									{
										
										
									($('#sample_1').DataTable()).destroy();
									$('#sample_1').hide();
									
									
									($('#sample_2').DataTable()).search("").draw();
									($('#sample_2').DataTable()).search("");
									$('#sample_2').show();
									
								}
								else
								{
										($('#sample_2').DataTable()).destroy();
										$('#sample_2').hide();
										
										($('#sample_1').DataTable()).search("").draw();
										($('#sample_1').DataTable()).search("");
										$('#sample_1').show();
								}	
						}
						
					}
</script> 
<script>
function due_dis()
					{
						
							$('#show_today').css('background-color','#A5A5A5');
							$('#show_up').css('background-color','#A5A5A5');
						
						
	
						if((($('#sample_1').DataTable()).search() == $('#curdate').val())  || (($('#sample_1').DataTable()).search() == 'Remaining')|| (($('#sample_2').DataTable()).search() == 'Remaining') || (($('#sample_2').DataTable()).search() == $('#curdate').val()))
						{
							//alert(($('#sample_1').DataTable()).search());
							($('#sample_1').DataTable()).destroy();
							($('#sample_2').DataTable()).destroy();
						}
	
				if((($('#sample_1').DataTable()).search() == '') && (($('#sample_2').DataTable()).search() == '') )
						{
							$('#show_due').css('background-color','#00CC99');
							
						 var b = $('#disp').text().trim();
						 //alert(a);
								if(b == 'Checkout')
								{
									//alert('blank');
									($('#sample_1').DataTable()).destroy();
									$('#sample_1').hide();
									
									$('#sample_2').show();
									($('#sample_2').DataTable()).search('Due').draw();
									
								
								}
								else
								{
									
									($('#sample_2').DataTable()).destroy();
									$('#sample_2').hide();
									
									$('#sample_1').show();
									($('#sample_1').DataTable()).search('Due').draw();
									
														
									}
						}
				else
						{
							
							
							
							$('#show_due').css('background-color','#829596');
						
						 var b = $('#disp').text().trim();
						 //alert(b);
								if(b == 'Checkout')
									{
									
										
									($('#sample_1').DataTable()).destroy();
									$('#sample_1').hide();
									
									
									($('#sample_2').DataTable()).search("").draw();
									($('#sample_2').DataTable()).search("");
									$('#sample_2').show();
									
								}
								else
								{
									
										($('#sample_2').DataTable()).destroy();
										$('#sample_2').hide();
										
										($('#sample_1').DataTable()).search("").draw();
										($('#sample_1').DataTable()).search("");
										$('#sample_1').show();
								}	
						}
						
					}
</script>
<div id="output"></div>
<script>
$( "#get_output" ).click(function() {
	
	var a = ($('#sample_1').DataTable()).search();
	var b = ($('#sample_2').DataTable()).search();
	//alert(a);
	//alert(b);
	var c = $('#disp').text().trim();
		
								if(c == 'Checkin')
									{
			//alert(c);							
	($('#sample_1').DataTable()).destroy();
	($('#sample_1').DataTable({paging: false,})).search(a).draw();
	$('body').css('visibility', 'hidden');
	$('#sample_1').css('visibility','visible');
	$('table tr td a').removeAttr('href');
	window.print();
	$('body').css('visibility', 'visible');
	($('#sample_1').DataTable()).destroy();
	($('#sample_2').DataTable()).destroy();
	($('#sample_1').DataTable({pagelength:10,})).search(a).draw();
									}
								else
									{
	($('#sample_2').DataTable()).destroy();
	($('#sample_2').DataTable({paging: false,})).search(b).draw();
	$('body').css('visibility', 'hidden');
	$('#sample_2').css('visibility','visible');
	$('table tr td a').removeAttr('href');
	window.print();
	$('body').css('visibility', 'visible');
	($('#sample_1').DataTable()).destroy();
	($('#sample_2').DataTable()).destroy();
	($('#sample_2').DataTable({pagelength:10,})).search(b).draw();
									}
		
});
</script> 
<!-- END CONTENT --> 
