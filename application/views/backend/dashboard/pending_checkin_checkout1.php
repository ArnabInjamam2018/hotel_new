<div class="colr">
  <ul >
    <li> <span class="brdr " style="background:#f1eda5;"></span> Due Within 4 days</li>
    &nbsp;&nbsp;
    <li> <span class="brdr " style="background:#ffb9de;"></span> Due Past</li>
    &nbsp;&nbsp;
    <li> <span class="brdr " style="background:#87f6ff;"></span> Due Today</li>
    <li> <span class="brdr " style="background:#83ffbd;"></span> Due Later</li>
  </ul>
</div>
<div class="portlet box blue">
<div class="portlet-title">
  <div class="caption"> <i class="fa fa-edit"></i>List of All Pending Bookings </div>
  <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
</div>
<div class="portlet-body">
  <div class="table-toolbar">
    <div class="row">
      <div class="col-md-4">
        <div class="btn-group">
          <label for="autocellwidth" class="auto_cl btn grey-cascade" id="auto_cl_id" style="background-color:#39B9A1;">
            <input type="checkbox" id="autocellwidth" class="toggle" style="display:none;">
            <span id="disp">Checkin</span> </label>
        </div>
        <div class="btn-group">
          <input type="hidden" id="curdate" value="<?php echo date('Y-m-d');?>"  />
          <label  class="auto_cl btn grey-cascade" onclick="today_dis()" id="show_today"> <span >Todays Booking</span> </label>
        </div>
        <div class="btn-group">
          <label  class="auto_cl btn grey-cascade" onclick="up_dis()" id="show_up"> <span >Expected</span> </label>
        </div>
        <div class="btn-group">
          <label  class="auto_cl btn grey-cascade" onclick="due_dis()" id="show_due"> <span >Due</span> </label>
        </div>
      </div>
      <div class="col-md-5">
        <?php

                            $form = array(
                                'class'       => 'form-inline',
                                'id'        => 'form_date',
                                'method'      => 'post'
                            );

                            echo form_open_multipart('dashboard/get_pending_booking_report_by_date',$form);

                            ?>
        <div class="form-group">
          <input type="text" autocomplete="off" required="required" value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
        </div>
        <div class="form-group">
          <input type="text" autocomplete="off" required="required" value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
        </div>
        <button class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search"></i></button>
        <?php form_close(); ?>
      </div>
      <script type="text/javascript">
            function check_sub(){
               document.getElementById('form_date').submit();
            }
        </script> 
    </div>
  </div>
  <table class="table table-striped table-hover table-bordered table-scrollable" id="sample_1">
    <thead>
      <tr>
        <th>Booking Id </th>
        <th>Booking Status </th>
        <th>Customer Name </th>
        <th>Checkin date </th>
        <th>Pendig Dyas</th>
        <th>Total Booking Amount</th>
      </tr>
    </thead>
    <tbody>
	
      <?php 
	  $bookings=$this->bookings_model->all_bookings();
	 // print_r($bookings);
	 if(isset($bookings) && $bookings){
                        //print_r($bookings);
                       $i=1;
                       $color='';
                        $deposit=0;
                        $item_no=0;
                        foreach($bookings as $booking){?>
                            
      <tr style="background-color:<?php echo  $color; ?>;">
        <td><?php if($booking->group_id !="0"){
                echo '<i class="fa fa-group"></i>';
                } 
                else{
                 echo '<i class="fa fa-user"></i>';
                } 	
                ?>
          <a 

              <?php if($booking->group_id !="0"){
                echo 'style="color:green; " ';
             }              
			?>
              href="<?php echo base_url();?>dashboard/booking_edit?b_id=<?php echo $booking->booking_id ?>"><?php echo $booking->booking_id?></a></td>
        <td><span class="label" style="background-color:<?php echo $st->bar_color_code ?>; color:<?php echo $st->body_color_code ?>;">
          <?php  echo $st->booking_status?>
          </span></td>
        <td><?php 
/*
              $guests=$this->dashboard_model->get_guest_details($booking->guest_id);
                if(isset($guests) && $guests){
                 foreach ($guests as $key ) {
               if($key->g_name!=""){
                echo $key->g_name;
               }
               else{
                   echo "na";
               }
                }}
            ?></td>
        <?php 
                    date_default_timezone_set('Asia/Kolkata');
                    $sum=0;
                if($booking->booking_extra_charge_id != "")
                {
                    $e_chr= explode(',',$booking->booking_extra_charge_id);
                    for($k=0;$k<count($e_chr);$k++)
                    {
                        $ecs=$this->dashboard_model->get_charge_details($e_chr[$k]);
                        if(isset($ecs) && $ecs)
                        {
                            
                        $sum=$sum+$ecs->crg_tax;
                        
                        }
                    }
                    
                }*///echo number_format($sum,2,'.',',');
                ?>
        <?php // $tax_ammunt=$booking->room_rent_total_amount* ($booking->luxury_tax/100);?>
        <?php //$service_tax_amount=$booking->room_rent_total_amount* ($booking->service_tax/100);?>
        <?php  //$extra_ch_amount=$booking->room_rent_total_amount* ($booking->service_charge/100);?>
        <?php  //$f=0; if(isset($fd_vat->fvat)) {  $f=$fd_vat->fvat; } /*echo $f;*/?>
        <?php  //$l=0; if(isset($fd_vat->lvat)) {  $l=$fd_vat->lvat; } /*echo $l;*/ ?>
        <?php 
                /*$fd_vat=$this->dashboard_model->get_fd_tax_by_id($booking->booking_id);
                    if(isset($fd_vat)){
                     $total=$booking->room_rent_total_amount+$fd_vat->fvat+$fd_vat->lvat+$tax_ammunt+$service_tax_amount+$extra_ch_amount+$sum;
                    }
                else{
                     $total=$booking->room_rent_total_amount+$tax_ammunt+$service_tax_amount+$extra_ch_amount+$sum;
                }*/
                 ?>
        <td><?php //$ex_arry = explode(' ',$booking->cust_from_date); echo $ex_arry[0];?></td>
        <td><?php //echo abs($days_s)." Day ".abs($hours_s)." hr ".abs($min_s)." Min ".$disp;?></td>
		<?php
		/*$info_lineItem=$this->dashboard_model->get_bookingLineItem($booking->booking_id);
			if(isset($info_lineItem)){
			$rr_tot=$info_lineItem->rr_tot;
			$rr_tot_tax=$info_lineItem->rr_tot_tax;
			$exrr=$info_lineItem->exrr;
			$exrr_tax=$info_lineItem->exrr_tax;
			$t=$rr_tot+$rr_tot_tax+$exrr+$exrr_tax;
			}else{
				$t=0;
			}
*/
		?>
        <td><?php echo '<i class="fa fa-inr" aria-hidden="true"></i>'.number_format($t,2,'.',',');?></td>
      </tr>
    <input type="hidden" id="item_no" value="<?php echo $item_no;?>">
      </input>
    
	 <?php $i++;}}?>
    
      </tbody>
    
  </table>