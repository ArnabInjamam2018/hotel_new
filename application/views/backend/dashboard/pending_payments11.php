<div class="colr">
  <ul >
    <li>
      <span class="brdr " style="background:#f1eda5;"></span>
      Due Within 4 days</li>
    &nbsp;&nbsp;
    <li>
      <span class="brdr " style="background:#ffb9de;"></span>
      Due Past</li>
    &nbsp;&nbsp;
    <li>
      <span class="brdr " style="background:#87f6ff;"></span>
      Due Today</li>
    <li>
      <span class="brdr " style="background:#83ffbd;"></span>
      Due Later</li>
  </ul>
</div>
<script >
function update_security_details()
		{	
			var booking_id = $("#b_id").text();
			var booking_type =  $("#b_type").text();
			var due_date = $("#due_date").val();
			var card_number =$("#card_number").val();
			var cvv =$("#cvv").val();
			var exp_date =$("#exp_date").val();
			var ac_no =$("#ac_no").val();
			var ifsc =$("#ifsc").val();
			var c_name =$("#c_name").val();
			var c_number =$("#c_number").val();
			//alert (booking_id+booking_type+due_date+card_number+cvv+exp_date+ac_no+ifsc+c_name+c_number);
			//window.location.replace("<?php echo base_url()?>"+"dashboard/update_credit_security_details?booking_id="+booking_id+"&booking_type="+booking_type+"&cvv="+cvv+"&due_date="+due_date+"&card_number="+card_number+"&exp_date="+exp_date+"&ac_no"+ac_no+"&ifsc"+ifsc+"&c_name"+c_name+"&c_number"+c_number);
		if(due_date != "")
		{
		$.ajax({
			type:"POST",
			url: "<?php echo base_url()?>dashboard/update_credit_security_details",
			data:{booking_id:booking_id,booking_type:booking_type,due_date:due_date,card_number:card_number,cvv:cvv,exp_date:exp_date,ac_no:ac_no,ifsc:ifsc,c_name:c_name,c_number:c_number,},
			success:function(data)
			{
				if (data != "")
				{
					$('#responsive').modal('toggle');
					swal(
		data,
		'Your Data Hasbeen Successfully Updated',
			''
		);
				location.reload();
				
				}

			}
		});
			
		}
			else
			{
				$("#due_date").focus();
				alert("Due Date Cannot Be left Blank");
			}
			
		}
</script>

<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true"  style="display:none;" >
<div class="modal-dialog">
   <div class="modal-content">
      <div class="modal-header">
      	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Security Details <br />
<label style="margin-right:5px;"><b>Booking Id : </b><span id="b_id"></span></label>
		<label style="margin-right:5px;"><b>Type : </b><span id="b_type"></span></label>
		<label style="margin-right:5px;"><b>Due : </b><span id="b_due"></span></label></h4>		
      </div>	  
      <div class="modal-body">
        <div class="row" >
            <div class="col-md-6">
          <div class="form-group form-md-line-input">
            <input  type="text" class="form-control date-picker" id="due_date" name="due_date" placeholder="Due Date"/>
            <label></label>
            <span class="help-block">Due Date</span>
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group form-md-line-input">                            
            <input type="text"  class="form-control" id="card_number" name="card_number" placeholder="Card Number"/>
            <label></label>
            <span class="help-block">Card Number</span>
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group form-md-line-input">                            
            <input type="text" class="form-control" id="cvv"  name="cvv" placeholder="CVV"/>
            <label></label>
            <span class="help-block">CVV</span>
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group form-md-line-input">
            <input type="text" name="exp_date" id="exp_date" class="form-control  date-picker" placeholder="Expiry Date"/>
            <label></label>
            <span class="help-block">Expiry Date</span>
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group form-md-line-input">                            
            <input type="text" id="ac_no" class="form-control" name="ac_no" placeholder="Banck A/C No"/>
            <label></label>
            <span class="help-block">Banck A/C No</span>
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group form-md-line-input">
            <input name="ifsc" class="form-control"  id="ifsc" placeholder="IFSC Code"/>                            
            <label></label>
            <span class="help-block">IFSC Code</span>
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group form-md-line-input">                            
            <input type="text" class="form-control"  id="c_name" name="c_name" placeholder="Company Name"/>
            <label></label>
            <span class="help-block">Company Name</span>
          </div> 
          </div>
          <div class="col-md-6">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control"  id="c_number" name="c_number" placeholder="Company Number"/>
            <label></label>
            <span class="help-block">Company Number</span>
          </div>
          
        </div>

        
    </div>
      </div>
	  
      <div class="modal-footer">
	  <button type="button" class="btn btn-warning"  onclick="update_security_details()">Update</button>
	  <a id="pay" ><button type="button" class="btn btn-success"  >Pay</button></a>
        <button type="button" class="btn btn-Primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Bookings </div>
    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body">
 
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr>
          <th> Booking Id </th>
          <!--<th> Booking Type </th>-->
          <th> Booking Status </th>
          <th>Due Date</th>
          <th> Amount Payable</th>
          <th> Paid Amount</th>
          <th>Due Amount</th>
         <!--- <th> Admin </th>--->
          <th scope="col" class="action"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($bookings) && $bookings){
                   //echo "<pre>";
				   //print_r($bookings);exit;
					$i=1;
                    $deposit=0;
                    $item_no=0;
                    foreach($bookings as $booking){
						
						$rm_total=$booking->rm_total;
						if(isset($rm_total) && $rm_total){
							$rm_total;
						}else{
							$rm_total=0;
						}
						
						$rr_tot_tax=$booking->rr_tot_tax;
						if(isset($rr_tot_tax) && $rr_tot_tax){
							$rr_tot_tax;
						}else{
							$rr_tot_tax=0;
						}
						
						$exrr=$booking->exrr;
						if(isset($exrr) && $exrr){
							$exrr;
						}else{
							$exrr=0;
						}
						
						$exrr_tax=$booking->exrr_tax;
						if(isset($exrr_tax) && $exrr_tax){
							$exrr_tax;
						}else{
							$exrr_tax=0;
						}
						
						$mp_tot=$booking->mp_tot;
						if(isset($mp_tot) && $mp_tot){
							$mp_tot;
						}else{
							$mp_tot=0;
						}
						
						$mp_tax=$booking->mp_tax;
						if(isset($mp_tax) && $mp_tax){
							$mp_tax;
						}else{
							$mp_tax=0;
						}
						
						$exmp=$booking->exmp;
						if(isset($exmp) && $exmp){
							$exmp;
						}else{
							$exmp=0;
						}
						
						$exmp_tax=$booking->exmp_tax;
						if(isset($exmp_tax) && $exmp_tax){
							$exmp_tax;
						}else{
							$exmp_tax=0;
						}
                    $total_amo=$rm_total+$rr_tot_tax+$exrr+$exrr_tax+$mp_tot+$mp_tax+$exmp+$exmp_tax;
                    /*if(($booking->group_id == 0)&&($booking->booking_id_actual != ""))
                    {
                        $cr_flag=$this->dashboard_model->get_checkout_on_credit($booking->booking_id,'sb');
                        
                        
                        if($cr_flag != false)
                        {
                            
                        $due_date="";	
                        $total_payable=0;
                        foreach($cr_flag as $cr) 
                        {
                        $total_payable=$cr->total_amt;
                        $due_date= $cr->due_date;
                        }
                        
                        
                        $tr_data=$this->dashboard_model->get_total_payment($booking->booking_id);
                        if($tr_data)
                        {
                            foreach ($tr_data as $tr)
                            {
                            $paid_amt = ($tr->t_amount);
                            }
                        }
                        else
                        {
                            $paid_amt = 0;
                        }	
                            
                        
                                                        
                            
                        $bills=$this->bookings_model->all_folio($booking->booking_id);
                        
                        if(isset($bills)){
                    
                    foreach ($bills as $item) {
                    # code...

                      $item_no++;

                        } }
        ?>
     
      
      <?php
        
                        $rooms=$this->dashboard_model->get_room_number($booking->room_id);
                        if(isset($rooms) && $rooms):
                        foreach($rooms as $room):
                        
                        
                            if($room->hotel_id==$this->session->userdata('user_hotel')):


                             /*   foreach($transactions as $transaction):

                                    if($transaction->t_booking_id=='HM0'.$this->session->userdata('user_hotel').'00'.$booking->booking_id):

                                        $deposit=$deposit+$transaction->t_amount;
                                    endif;
                                endforeach;*/



                            
                               
                               /*   $room_number=$room->room_no;
                                $room_cost=$room->room_rent;
                           
                              $date1=date_create($booking->cust_from_date);
                                $date2=date_create($booking->cust_end_date);
                                $diff=date_diff($date1, $date2);
                                $cust_duration= $diff->format("%a");*/

                                /*Cost Calculations*/
                                /*$total_cost=$cust_duration*$room_cost;
                                $total_cost=$total_cost+$total_cost*0.15;
                                $food_tax=0;
                                if($booking->food_plans!="")
                                {
                                $data=$this->bookings_model->fetch_food_plan($booking->food_plans);
                                if($data)
                                $food_tax=($data->fp_tax_percentage/100)*$booking->food_plan_price;
                                }
                $due=($booking->room_rent_sum_total+$booking->service_price+$booking->food_plan_price+$food_tax+$booking->booking_extra_charge_amount)-$booking->cust_payment_initial-$deposit;

                                $status_id=$booking->booking_status_id;

                                $status=$this->dashboard_model->get_status_by_id($status_id);
                                
                                if(isset($status)){
									//echo "<pre>";
                                    //print_r($status);
                                    foreach($status as $st){
                                    
                                /*Calculation End */


                               /* $class = ($i%2==0) ? "active" : "success";

                                $booking_id='HM0'.$this->session->userdata('user_hotel').'00'.$booking->booking_id;
                                $book_id=$booking->booking_id;
                                
                                
                                if (($total_payable-$paid_amt) > 0)
                                {
                                $date1=date_create($due_date);
                                $date2=date_create(date('Y-m-d'));
                                $dDiff = $date2->diff($date1);
                                 $diff = $dDiff->format('%R').$dDiff->days;
                                 if	(($diff > 0) && ($diff < 5))
                                 {
                                     $color="#f1eda5";
                                 }
                                 else if(($diff < 0))
                                 {
                                     $color= "#ffb9de";
                                 }
                                 else if(($diff == 0))
                                 {
                                     $color= "#87f6ff";
                                 }
                                 else
                                 {
                                    $color= "#83ffbd"; 
                                 }*/
								 $status_id=$booking->booking_status_id;

                                $status=$this->dashboard_model->get_status_by_id($status_id);
								//echo "<pre>";
								//print_r($status);exit;
								foreach($status as $stat){
								$colorBODY=$stat->body_color_code;
								$colorBAR=$stat->bar_color_code;
								$Bstatus=$stat->booking_status;
								}
								$tr_data=$this->dashboard_model->get_total_payment_1($booking->booking_id);
								//echo "<pre>";
								//print_r($tr_data);exit;
                                ?>
								
        <tr style="background-color:<?php //echo $color; ?>;">
     <?php if($booking->group_id !="0") {?>
        <td> <!-- Booking ID -->
			  
			  <a href="<?php echo base_url();?>dashboard/booking_edit?b_id=<?php echo $booking->booking_id ?>"><?php echo $booking->booking_id_actual;?></a>
		</td>
      
        <td><span class="label" style="background-color:<?php echo $colorBODY; ?>; color:<?php echo $colorBAR; ?>">
          <?php  echo $Bstatus;?>
          </span>
          
          </td>
 <td><?php //$due_dt=0; foreach($cr_flag as $cr) {$due_dt=$cr->due_date;}echo $due_dt;//max($due,0); ?></td>
       
        <td><?php echo $total_amo; ?></td>
        <td><?php echo number_format((float)$tr_data->t_amount,2,'.',',') ; 
          ?></td>
         <td> <?php //echo number_format((float)($due_amt=$total_payable-$paid_amt),2,'.','') ;?></td>
      
        <td><a  onclick="show_security_details(<?php //echo $booking->booking_id;?>,'<?php// echo $type;?>',<?php //echo $due_amt; ?>)"  class="btn blue btn-xs" ><i class="fa fa-eye"></i></a>
        </td><?php } ?>
      </tr>
								
       <input type="hidden" id="item_no" value="<?php //echo //$item_no;?>"> </input>
                                <?php //$i++;
								}
								}
								?>
  
      
      <?php 
      $groups = $this->dashboard_model->get_group_pending_payments();
      if(isset($groups) && $groups)
      {
          foreach($groups as $gp)
        {
          
          
      $amountPaid = $this->dashboard_model->get_amountPaidByGroupID($gp->booking_id);
      if(isset($amountPaid) && $amountPaid) 
              {
                $paid=$amountPaid->tm;
              }
              else
              {
                $paid=0;
              }

      $sb_row=$this->dashboard_model->get_sbID_detail($gp->booking_id);
      if(isset($sb_row) && $sb_row){
      $bookingStatusID = $sb_row->booking_status_id;
      }
      else
      {
        $bookingStatusID=0;  
      }
       $status=$this->dashboard_model->get_status_by_id($bookingStatusID);
                                
                                if(isset($status)){
                                    foreach($status as $st)
                                    {
                                        $bar_color=$st->bar_color_code;
                                        $body_color=$st->body_color_code;
                                        $status_name=$st->booking_status;
                                    }
                                }
                                else
                                {
                                    $bar_color="";
                                    $body_color="";
                                    $status_name="";
                                }
                        
          $due_date=$gp->due_date;
          
          $g_amount_payable=$gp->total_amt;
          $group_paid=$paid;
          $group_due=$g_amount_payable-$group_paid;
          
          
          if (($group_due) > 0)
                                {
                                $date1=date_create($due_date);
                                $date2=date_create(date('Y-m-d'));
                                $dDiff = $date2->diff($date1);
                                 $diff= $dDiff->format('%R').$dDiff->days;
                                 if	(($diff > 0) && ($diff < 5))
                                 {
                                     $color="#f1eda5";
                                 }
                                 else if(($diff < 0))
                                 {
                                     $color= "#ffb9de";
                                 }
                                 else if(($diff == 0))
                                 {
                                     $color= "#87f6ff";
                                 }
                                 else
                                 {
                                    $color= "#83ffbd"; 
                                 }
          ?>
      
      
	  <tr style="background-color:<?php echo $color; ?>;">
		  <td><a href="<?php echo base_url();?>dashboard/booking_edit_group?group_id=<?php echo $gp->booking_id ?>">
				<?php echo $booking_id='HM0'.$this->session->userdata('user_hotel').'GRP00'.$gp->booking_id."    "; ?></a><i class="fa fa-group"></i>
		  </td>
		  <!--<td><?php  foreach($groups as $cr)$type=$cr->booking_type; if($type == 'sb'){echo "<span class=' label label-primary'>Single Booking</span>";} else { echo "<span label class='label label-primary'>Group booking</span>";}?></td>--->
		  <td><span class="label" style="background-color:<?php echo $bar_color; ?>; color:<?php echo $body_color; ?>;">
			  <?php  echo $status_name;?>
			  </span>  
		  </td>
		  <td><?php echo $due_date; ?></td>
		  <td><?php echo number_format((float)$g_amount_payable,2,'.',','); ?></td>
		  <td><?php echo number_format((float)$group_paid,2,'.',','); ?></td>
		  <td><?php echo number_format((float)$group_due,2,'.',','); ?></td>
		  <td><a  onclick="show_security_details(<?php echo $gp->booking_id;?>,'<?php echo $type;?>',<?php echo $group_due; ?>)"  class="btn blue btn-xs" ><i class="fa fa-eye"></i></a>
		  </td>
      </tr>
      
      
      
    <?php   
        }
      }
      }
      
       ?>

      
        </tbody>
      
    </table>
  </div>
</div>

<script type="text/javascript" src="<?php echo base_url();?>assets/common/js/table_sort_style.js">
</script>

<script>
function show_security_details(b_id,b_type,due)
{
		
	$.ajax({
                
                url: "<?php echo base_url()?>dashboard/get_checkout_on_credit",
				type:"POST",
                data:{id:b_id,type:b_type,},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                   /* swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });*/
						var str=data.split('_');
						
						//``alert(data);
						$('#b_due').text(parseFloat(due));
						$('#b_id').text(b_id);
						$('#b_type').text(b_type);
						$('#due_date').val(str[1]);
						$('#card_number').val(str[2]);
						$('#cvv').val(str[3]);
						$('#exp_date').val(str[4]);
						$('#ac_no').val(str[5]);
						$('#ifsc').val(str[6]);
						$('#c_name').val(str[7]);
						$('#c_number').val(str[8]);
						$("#responsive").modal();
						
						if(b_type == 'sb')
						{
						$("#pay").attr('href',"<?php echo base_url();?>dashboard/booking_edit?b_id="+b_id+"&fl=1");
						}
						else
						{
						$("#pay").attr('href',"<?php echo base_url();?>dashboard/booking_edit_group?group_id="+b_id+"&fl=1");
						}
                }
            });
			}
			
			
			


		</script>


<!-- END CONTENT  --> 