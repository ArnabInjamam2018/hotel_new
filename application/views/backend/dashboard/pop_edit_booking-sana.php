<?php 
	     foreach ($rooms as $room) {  

	    	$bills=$this->bookings_model->all_folio($room->booking_id);
	    	$item_no=0;
	    	foreach ($bills as $item) {
	    		# code...

	    		$item_no++;

	    	} ?> 
	    	<input type="hidden" id="item_no" value="<?php echo $item_no ?>"></input>
	    	<?php

	    	 $poses=$this->dashboard_model->all_pos_booking($room->booking_id);
                    $pos_amount=0;
                    if($poses){
                    foreach ($poses as $key ) {

                      # code...
                      $pos_amount=$pos_amount+$key->total_due;
                    }}
                  

				//Cost calculation
            $room_id=$room->room_id;

            $discount=$room->discount;

            

				$total_cost=$room->room_rent_total_amount;
				$total_tax=$room->room_rent_tax_amount;
				$booking_sum_total=$room->room_rent_sum_total;
				  $group_id = $room->group_id;
				if($group_id =="0"){
				$grand_total=$room->room_rent_sum_total;
				//if($grand_total==0){
					$grand_total=$total_cost;
				//}
				}else{
					$grand_total=0;
				}
				//End Cost Calculation
	            $room_no = $room->room_no;
	 			$food_plan_id=$room->food_plans;
	 			$food_fetch_details = $this->bookings_model->fetch_food_plan($food_plan_id);
	 			if (!empty($food_fetch_details )) {
	 				$food_plan_name = $food_fetch_details->fp_name;
	 			}
	 			else{
	 				$food_plan_name = 'N/A';
	 			}
	 			

	 			
	 			if(!empty($room->food_plan_price)){
					
					$food_tax=0;
					if($room->food_plans!="")
					{
					$data=$this->bookings_model->fetch_food_plan($room->food_plans);
					if($data){
					 //$food_tax=($data->fp_tax_percentage/100)*$room->food_plan_price;
					 $scP= 23 - $data->fp_tax_percentage;	
					 $food_tax1=($data->fp_tax_percentage/100)*$room->food_plan_price;
					 $food_tax2=($scP/100)*$room->food_plan_price;
					 $food_tax = $food_tax1 + $food_tax2;				
					}
					}
	 				$food_amount = $room->food_plan_price+$food_tax;
	 			}
	 			else{
	 				$food_amount ='N/A';
	 			}


				$booking_id = $room->booking_id;
				$cust_name = $room->cust_name;
				$checkin_time=$room->checkin_time;
				$checkout_time=$room->checkout_time;
				$cust_address = $room->cust_address;
				$cust_contact_no = $room->cust_contact_no;
				$cust_from_date = $room->cust_from_date;

                $service_amount=$room->service_price;
                if($service_amount ==0){
                    $service_amount="N/A";
                }
				else{
                    $service_amount=$service_amount;
                    $grand_total=$grand_total+$service_amount;
                }

                 $charge_amount=$room->booking_extra_charge_amount;
                if($charge_amount ==0){
                    $charge_amount="N/A";
                }else{
                    $charge_amount=$charge_amount;
                    $grand_total=$grand_total+$charge_amount;
                }

                $grand_total=$grand_total+$pos_amount+$food_amount+$room->room_rent_tax_amount;


            $times=$this->dashboard_model->checkin_time();
            foreach($times as $time) {
                if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
                    $modifier = ($time->hotel_check_in_time_hr + 12);
                }
                else{
                    $modifier = ($time->hotel_check_in_time_hr);
                }
            }

            foreach($times as $time) {
                if($time->hotel_check_out_time_fr=='PM' && $time->hotel_check_out_time_hr !="12") {
                    $modifier2 = ($time->hotel_check_out_time_hr + 12);
                }
                else{
                    $modifier2 = ($time->hotel_check_out_time_hr);
                }
            }
        if($group_id ==0){

            $cust_end_date1 =date("Y-m-d H:i:s",strtotime($room->cust_end_date));
            $cust_from_date1 =date("Y-m-d H:i:s",strtotime($room->cust_from_date));

        }else{

        	//$cust_end_date1 =date("Y-m-d H:i:s",strtotime($room->cust_end_date_actual));
            //$cust_from_date1 =date("Y-m-d H:i:s",strtotime($room->cust_from_date_actual));
			$cust_end_date1 =date("Y-m-d H:i:s",strtotime($room->cust_end_date));
            $cust_from_date1 =date("Y-m-d H:i:s",strtotime($room->cust_from_date));

        }

        if($group_id ==0){
            $cust_end_date =date("Y-m-d H:i:s",strtotime($room->cust_end_date)+60*60*$modifier2);
            $cust_from_date =date("Y-m-d H:i:s",strtotime($room->cust_from_date)+60*60*$modifier);
        }else{

        	 $cust_end_date=$cust_from_date1;
        	 $cust_end_date=$cust_end_date1;

        }
			$custEndDate = date("Y-m-d",strtotime($room->cust_end_date_actual));
            $custFromDate = date("Y-m-d",strtotime($room->cust_from_date_actual));


				//$cust_end_date = $room->cust_end_date;
				$diff = abs(strtotime($cust_from_date) - strtotime($cust_end_date));
				$years = floor($diff / (365*60*60*24));
                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
				//$d1 = strtotime($cust_from_date);
				//$dt1 = date_create($d1,'Y-m-d H:i:s');
				//$d2 = strtotime($cust_end_date);
				//$dt2 = date_create($d2,'Y-m-d H:i:s');
				//$diff=date_diff($cust_from_date,$cust_end_date);
				//$cust_booking_status = $room->cust_booking_status;
				//$cust_payment_initial = $room->cust_payment_initial;
				$base_room_rent = $room->base_room_rent;
				$rent_mode_type = $room->rent_mode_type;
				$mod_room_rent = $room->mod_room_rent;
				$booking_status_id = $room->booking_status_id;
			$booking_status_id_secondary = $room->booking_status_id_secondary;
				$booking_checkout_time = $room->checkout_time;
				$no_of_guest=$room->no_of_guest;
                 $no_of_adult=$room->no_of_adult;
                $no_of_child=$room->no_of_child;
				$booking_source=$room->booking_source;
				if($room->broker_id !=0){
                    $broks=$this->dashboard_model->get_broker_details($room->broker_id);
                    foreach($broks as $b) {
                        $broker_name = $b->b_name;
                    }
				}
				else{
					$broker_name="N/A";
				}
            if($room->channel_id !=0){

                $chns=$this->dashboard_model->get_channel_details($room->channel_id);
                /*print_r($chns);
                exit();*/
                foreach($chns as $c) {
                    $channel_name = $chns->channel_name;
                }
            }
            else{
                $channel_name="N/A";
            }
				if($rent_mode_type == 'p')
				{
					$total_amount = ($base_room_rent + $mod_room_rent)*$days;
				}
				else
				{
					$total_amount = ($base_room_rent - $mod_room_rent)*$days;
				}
				//print_r($room);
				//exit();
	         }
		$paid_amount=0;	 
		foreach ($transaction as $transaction)
		{
			$paid_amount = $paid_amount=0	+ $transaction->t_amount;
		}
		$due_amount = $grand_total - $paid_amount;
		//$due_amount = $grand_total;
		
		if($booking_status_id == 6)
		{
			$flag =1;
		}

		if($group_id !=0){
			$amountPaid = $this->dashboard_model->get_amountPaidByGroupID($group_id);
			if(isset($amountPaid) && $amountPaid) {
				$paidgrp=$amountPaid->tm;
			} else {
				$paidgrp=0;
			}
			$due_amount_grp = $grand_total - $paidgrp;
			//$due_amount = $grand_total - $paidgrp;
		}else{
			$paidgrp="na";
			$due_amount_grp="na";
		}
		
		if($group_id !=0){
			$amountPaid = $this->dashboard_model->get_amountPaidByGroupID($group_id);
			$ad = $this->dashboard_model->getAD($group_id);
			if(isset($amountPaid) && $amountPaid) {
				$paidgrp=$amountPaid->tm;
			} else {
				$paidgrp=0;
			}
			$amount = $this->dashboard_model->getAmountByGroupID($group_id);
			if(isset($amount) && $amount) {
				$gp_total=$amount->tm;
			} else {
				$gp_total=0;
			}			
			
			$dueAmountGp = $gp_total - $paidgrp;
			
		} else {
			$paidgrp="na";
			$dueAmountGp="na";
		}		

		
		
?>
<!DOCTYPE html>
<html lang="en">
<head>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">


        window.onload = function() {
            $("#master").attr("disabled", "disabled").off('click');
        };


	$(document).ready(function(){
		$("#checkout_credit").hide();
		 $("#checkout_credit").hide();
		 $("#tab5").hide();
		 $("#tab2").hide();
		 
		 $("#tab3").hide();
		 $("#tab6").hide();
		  var amount = $("#due_amount").val();
		  if(due_amount>0){$("#checkout_credit").show();}
		  var amount_grp = $("#due_amount_grp").val();
		  
		  var date_checkin = $("#checkindate").val();
		  var date_checkout = $("#checkoutdate").val();
		  
		$("#another-booking").hide();
		 $("#cancel_booking_reataintion").hide();
		  $("#edit-btn-grp").hide();
		 
		 var status =$("#statusid").val();
		  var group =$("#groupid").val();
		 //alert(status);
		 if(group !=0){
		 	 $("#edit-btn-grp").show();
		 }
		  if(status == 1)
		 {
			  //$("#edit-btn").hide();
			 $("#add_transaction").hide();
			 $("#invoice_billing").hide();
			 $("#checkout").hide();
		 }
		  if(status == 2)
		 {
			var d1 = new Date();
            var d2 = new Date(date_checkin);
             var d3 = new Date(date_checkout);
			if((d1.getTime() < d2.getTime()) || (d1.getTime()>d3.getTime())){
				$("#checkin-btn").hide();
			}
			$("#checkout").hide();
			// $("#cancel_booking_reataintion").show();
			//$("#cancel-booking").show();
		 }
		 if(status == 4)
		 {
			var d1 = new Date();
            var d2 = new Date(date_checkin);
             var d3 = new Date(date_checkout);
			if((d1.getTime() < d2.getTime()) || (d1.getTime()>d3.getTime()) ){
				$("#checkin-btn").hide();
			}
			$("#checkout").hide();
			$("#cancel-booking").hide();
             if(d1.getTime()>d2.getTime()){  //changed by Ashes d3 to d2
                // $("#cancel_booking_reataintion").show();
				 if($("#tot_transact").val() == 0){//changed by souvik
				  
			 $("#cancel-booking").show();
			  }
			  else
			  {
				$("#cancel_booking_reataintion").show();  
			  }

             }
             //$("#checkin-btn").show();
			 //$("#cancel_booking_reataintion").show();
			 
		 }
		  if(status == 5)
		 {  
			$("#checkin-btn").hide();
			$("#checkout_credit").show();
			 $("#take_new_bk").hide();

			if(amount_grp !="na"){

				if(amount_grp <=0){

					$("#checkout").show();

				}else{
						$("#checkout").hide();
				}

			}else{
			if(parseInt(amount)<=0)
			{
			
				$("#checkout").show();
			 
			}
			else{
				$("#checkout").hide();
			}}


			$("#cancel-booking").hide();
		 }
		 if(status == 6)
		 {  
			//$("#edit-btn").hide();
			 $("#add_transaction").hide();
			 $("#checkin-btn").hide();
			 $("#checkout").hide();
			 $("#cancel-booking").hide();
			
             var d1 = new Date();
             var month = d1.getMonth() + 1; //months from 1-12
             var day = d1.getDate();
             var year = d1.getFullYear();
             var hours = d1.getHours(); //returns 0-23
             var minutes = d1.getMinutes(); //returns 0-59
             var seconds = d1.getSeconds(); //returns 0-59

             var newdate = year + "/" + month + "/" + day+" "+hours+":"+minutes;

            // var newdate1 = year + "/" + month + "/" + day;

             var d2 = new Date(date_checkout);
             var month2 = d2.getMonth() + 1; //months from 1-12
             var day2 = d2.getDate();
             var year2 = d2.getFullYear();
             var hours2 = d2.getHours(); //returns 0-23
             var minutes2 = d2.getMinutes(); //returns 0-59
             var seconds2 = d2.getSeconds(); //returns 0-59

             var newdate2 = year2 + "/" + month2 + "/" + day2+" "+hours2+":"+minutes2;

//alert(newdate+"  "+newdate2);

             if(newdate<newdate2) {
                 $("#another-booking").show();
             }
		 }
		 if(status == 7)
		 {  
			$("#cancel-booking").hide();
			//$("#edit-btn").hide();
			 $("#add_transaction").hide();
			 $("#checkin-btn").hide();
			 $("#checkout").hide();
			 $("#cancel-booking").hide();
			 $("#another-booking").show();
			 $("#invoice_billing").hide();
		 }
		 if(status == 8)
		 {  
			$("#cancel-booking").show();
			$("#edit-btn").show();
			 $("#add_transaction").hide();
			 $("#checkin-btn").hide();
			 $("#checkout").hide();
			 //$("#cancel-booking").hide();
			 $("#another-booking").hide();
			 $("#invoice_billing").hide();
		 }
		 if(status == 3)
		 {  
			//$("#edit-btn").hide();
			 $("#add_transaction").hide();
			 $("#invoice_billing").hide();
			 $("#checkout").hide();
		 }

	$.ajax({
	   url: "<?php echo base_url();?>dashboard/check_booking_status",
	   success: function(msg){
		   
		   if(msg==0){
			   swal({
                            title: "No Data Found",
                            text: "",
                            type: "warning"
                        },
                        function(){
							
                        });
		   }
		   
	   }
	});
		
		 
				
	});
	</script>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/sweetdate/sweetalert.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/dashboard/assets/global/plugins/select2/select2.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo base_url();?>assets/dashboard/assets/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/global/css/plugins-md.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/dashboard/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/assets/dashboard/assets/admin/layout/css/themes/light2.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/dashboard/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<style>
	html, body{
		height:100%;
	}
</style>
</head>
<body class="page-md page-header-fixed page-quick-sidebar-over-content">
<div style="position:relative; height: 100%;">
<script>
setTimeout(function() 
{ 
    document.getElementById("loader").style.display = "none"; 
	document.getElementById("body").style.display = "block"; 
}, 1500);


</script>
<div id="loader" style="top:55%; left:60%">  
  <div class='loader'>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='window'></div>
    <div class='door'></div>
    <div class='hotel-sign'> <span>H</span> <span>O</span> <span>T</span> <span>E</span> <span>L</span> </div>
  </div>
</div>
<div class="page-container" id="body" style="display:none;">
  <input type="hidden" id="statusid" value="<?php echo $booking_status_id ?>">
  <input type="hidden" id="groupid" value="<?php echo $group_id ?>">
  <input type="hidden" id="checkindate" value="<?php echo $cust_from_date ?>">
  <input type="hidden" id="checkoutdate" value="<?php echo $cust_end_date ?>">
  <input type="hidden" id="checkindate1" value="<?php echo $cust_from_date1 ?>">
  <input type="hidden" id="checkoutdate1" value="<?php echo $cust_end_date1 ?>">
  <input type="hidden" id="checkInDate11" value="<?php echo $custFromDate ?>">
  <input type="hidden" id="checkOutDate11" value="<?php echo $custEndDate ?>">  
  <div class="portlet box grey-cascade" style="margin-bottom:0px;"> 
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-pin font-white"></i> Reservation Details
        </div>
        <div class="tools" style="display: inline-block; float: right; padding: 12px 0 8px;">
            <a href="javascript:close();" style="color:#ffffff;"> <i class="fa fa-times"> </i></a>
        </div>
    </div>           
    <div class="portlet-body form">
        <form action="bookings/hotel_backend_create" id="f1" method="POST" style="margin-bottom:0;">
          <div class="form-body">
            <table class="table table-hover pop-edittable" style="background:#F1F3F2; border:1px solid #ddd;">
              <tr>
                <td width="50%"><span>Booking Id:</span> <?php echo 'HM0'.$this->session->userdata('user_hotel').'00'.$booking_id ?></td>
                <td width="50%"><span>Booking Status:</span> <?php
                                                    $status = $booking_status_id;
                                                    if($status==4){
                                                        $status_data="Confirmed";
                                                    }else if($status==1){
                                                        $status_data="Confirmed";
                                                    }else if($status==2){
                                                        $status_data="Advance";
                                                    }else if($status==3){
                                                        $status_data="Pending";
                                                    }else if($status==5){
                                                        $status_data="Checked-In";
                                                    }else if($status==6){
                                                        $status_data="Checked-Out";
                                                    }else if($status==8){
                                                        $status_data="Incomplete";
                                                    }
                                                    else if($status==7){
                                                        $status_data="Cancelled";
                                                    }
                                                    else{
                                                        $status_data="undefined";
                                                    }


                                                    echo $status_data; ?></td>
              </tr>
              <tr>
                <td width="50%"><span>Booking Secondary Status:</span> <?php
                                                    $status_secondary = $booking_status_id_secondary;
                                                    if($status_secondary==3){
                                                        $status_data2="Pending";
                                                    }
                                                    else if($status_secondary==9){
                                                        $status_data2="Payment Due";
                                                    }
                                                    else{
                                                        $status_data2="N/A";
                                                    }


                                                    echo $status_data2; ?></td>                         
                <td width="50%"><span>Guest Name:</span> <?php echo $cust_name; ?></td>
              </tr>
              <tr>
                <td colspan="2"><span>Guest Address:</span> <?php echo $cust_address; ?></td>
              </tr>
              <tr>
                <td><span>Guest Number:</span> <?php echo $cust_contact_no; ?></td>
              
                <td><span>Check in date:</span> <?php 
                                                $date_s =  date_create($cust_from_date);
                                                
                                                echo date_format($date_s,"d.M.Y") ; ?></td>
              </tr>
              <tr>
                <td><span>Check in time:</span> <?php 
                                                
                                                
                                                echo $checkin_time ; ?></td>                          
                <td><span>Check Out date:</span> <?php 
                                                $date_e =  date_create($cust_end_date);
                                                echo date_format($date_e,"d.M.Y") ; ?></td>
              </tr>
              <tr>
                <td><span>Check Out time:</span> <?php 
                                                echo $checkout_time ;  ?></td>                          
                <td><span>Room No:</span> <?php echo $room_no; ?></td>
              </tr>
              <tr>
                <td><span>Number of Guests:</span> <i class="fa " style="font-size:12px;font-weight:normal;"></i>&nbsp;<?php echo $no_of_adult+$no_of_child; ?> (Adult: <?php echo $no_of_adult; ?> + Child: <?php echo $no_of_child; ?> )</td>                          
                <td><span>Booking Source:</span> <i class="fa " style="font-size:12px;font-weight:normal;"></i>&nbsp;<?php echo $booking_source; ?></td>
              </tr>
              <tr>
                <td><span>Broker name:</span> <i class="fa " style="font-size:12px;font-weight:normal;"></i>&nbsp;<?php echo $broker_name; ?></td>                          
                <td><span>Channel name:</span> <i class="fa " style="font-size:12px;font-weight:normal;"></i>&nbsp;<?php echo $channel_name; ?></td>
              </tr>
              <tr>
                <td><span>Total Booking Amount:</span> <i class="fa fa-inr" style="font-size:12px;font-weight:normal;"></i>&nbsp;<?php echo $total_cost; ?></td>                          
                <td><span>Total Tax:</span> <i class="fa fa-inr" style="font-size:12px; font-weight:normal;"></i>&nbsp;<?php echo $total_tax; ?></td>
              </tr>
			  
              <tr>
                <td><span>Extra Service Amount:</span> <i class="fa fa-inr" style="font-size:12px; font-weight:normal;"></i>&nbsp;<?php echo $service_amount; ?></td>                          
                <td><span>Extra Charge Amount:</span> <i class="fa fa-inr" style="font-size:12px; font-weight:normal;"></i>&nbsp;<?php echo $charge_amount; ?></td>
              </tr>

              <tr>
                <td><span>Extra POS Amount:</span> <i class="fa fa-inr" style="font-size:12px; font-weight:normal;"></i>&nbsp;<?php echo $pos_amount; ?></td> 
                  <td><span>Discount:</span> <i class="fa fa-inr" style="font-size:12px; font-weight:normal;"></i>&nbsp;<?php echo $discount; ?></td>                         
            
              </tr>
              <tr>
			  <tr>
                  <td><span>Food Plan Selected:</span> &nbsp; <?php echo $food_plan_name; ?></td>
                <td colspan="2"><span>Food Plan Price:</span> <i class="fa fa-inr" style="font-size:12px;font-weight:normal;"></i>&nbsp;<?php echo number_format((float)$food_amount, 2, '.', '');?></td>
              </tr>
                  <td><span>Total Amount Payable:</span> <i class="fa fa-inr" style="font-size:12px; font-weight:normal;"></i>&nbsp;<?php $gt =$grand_total-$discount; echo number_format((float)$gt, 2, '.', ''); ?></td>
                <td colspan="2"><span>Pending Amount:</span> <i class="fa fa-inr" style="font-size:12px;font-weight:normal;"></i>&nbsp;<?php $da = $due_amount-$discount; echo number_format((float)$da, 2, '.', ''); ?><input id="tot_transact" type="text" style="display:none" value="<?php echo $paid_amount;?>" /></td>
              </tr>
              
            </table>
            <?php 
				$cur_dt =  date('Y-m-d',strtotime($cust_from_date)); 
				$d =   date('Y-m-d');
            ?>
            <?php if($cur_dt == $d ) { ?>
            <?php if(($booking_status_id=='2' || $booking_status_id=='3' || $booking_status_id=='4')) {  ?>
            <?php } ?>
            <?php } ?>
            <div id="checkin">
                  <button class="btn btn-block btn-oii" id="checkin-btn" type="button" onClick="checkIn()" data-toggle="dropdown">
                  <span style="width:30px;float:left;"><i class="fa fa-bed" style="font-size:16px;"></i></span>
                  Check In</button>
                  <button class="btn btn-block purple" id="edit-btn" onClick="go_edit('<?php echo $booking_id; ?>')" type="button"  data-toggle="dropdown">
                  <span style="width:30px;float:left;"> <i class="fa fa-pencil" style="font-size:16px;"></i> </span>
                  Details/ Edit </button>
                  <button class="btn btn-block grrru" id="edit-btn-grp" onClick="go_edit_group('<?php echo $booking_id; ?>','<?php echo $group_id; ?>')" type="button"  data-toggle="dropdown">
                  <span style="width:30px;float:left;"> <i class="fa fa-group" style="font-size:16px;"></i> </span>
                  Group Details/ Edit </button>
				  <button class="btn btn-block grrru" id="take_new_bk" onClick="take_new_booking()" type="button"  data-toggle="dropdown">
                  <span style="width:30px;float:left;"> <i class="fa fa-group" style="font-size:16px;"></i> </span>
                  Take New Booking </button>
                  <?php if($group_id =="0"){ ?>
                  <button id="add_transaction" class="btn btn-block green" type="button" data-toggle="dropdown" >
                  <span style="width:30px;float:left;"> <i class="fa fa-plus-square" style="font-size:16px;"></i></span>
                  Add Transaction </button>
                  <?php } ?>
                  <div id="tab2" style="display:none; border:1px solid #26a69a; margin:-15px 0 15px; padding-top:10px;">
                    <div class="form-body">
                        <div class="row" >
                          <div class="form-group col-xs-6">
                            <label>Booking Id<span class="required">*</span></label>
                            <input type="text" class="form-control" id="" name="card_number" value="<?php echo 'HM0'.$this->session->userdata('user_hotel').'00'.$booking_id ?>" 
                                                    disabled="disabled"/>
                          </div>
                          <div class="form-group col-xs-6">
                            <label>Total Amount Paybale<span class="required">*</span></label>
                            <input type="text" required class="form-control" id="" name="card_number" value="<?php echo $grand_total-$discount; ?>" 
                                                    disabled="disabled"/>
                          </div>
                          <div class="form-group col-xs-6">
                            <label>Due Amount<span class="required">*</span></label>
                            <input type="text" class="form-control" id="due_amount"  name="card_number" value="<?php echo $due_amount-$discount; ?>" 
                                                    disabled="disabled"/>
                            <input  class="form-control" id="due_amount_grp"   type="hidden" value="<?php echo $due_amount_grp; ?>" 
                                                    disabled="disabled"/> 
                          </div>
                          <div class="form-group col-xs-6">
                            <label>Profit Center <span class="required"> * </span> </label>
                            <select name="p_center" id="profit_center" class="form-control" >
                               <?php $pc=$this->dashboard_model->all_pc();?>
						  <?php
						  $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
						  ?>
						  <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
                           <?php $pc=$this->dashboard_model->all_pc1();
							foreach($pc as $prfit_center){
							?>
						  
						  <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
							<?php }?>
                              </select>
                          </div>
                          <div class="form-group col-xs-6">
                            <label>Amount <span class="required"> * </span> </label>
                            <input type="text" id="add_amount" class="form-control" name="card_number" placeholder="Give Amount" 
                              onblur="check_amount();"  required="required"/>
                            <input type="hidden" id="booking_id" value="<?php  echo $booking_id; ?>">
                            <input type="hidden" id="booking_status_id" value="<?php  echo $booking_status_id; ?>">
                          </div>
                          <div class="form-group col-xs-6">
                            <label>Payment Mode <span class="required"> * </span> </label>
                            <select name="country" class="form-control" placeholder=" Booking Type" id="pay_mode" onChange="swap_bank()">
                                <option value="" disabled selected >----- Select Payment Mode -----</option>
                                <!-----<option value="Cash">Cash</option>
                                <option value="Debit">Debit Card</option>
                                <option value="Credit">Credit Card</option>
								 <option value="Bill To Company">Bill To Company</option>--->
								 <?php $mop = $this->dashboard_model->get_payment_mode_list();
			  if($mop != false)
			  {
				  foreach($mop as $mp)
				  {
				?>
				<option value="<?php echo $mp->p_mode_name; ?>" ><?php echo $mp->p_mode_des; ?></option>
			  <?php }
			  } ?>
                              </select>
                          </div>
                          <div class="form-group col-xs-6" id="bank" style="display: none;">
                            <label>Bank Name <span class="required"> * </span> </label>
                            <input type="text" class="form-control" placeholder="Bank name" id="bankname" >
                          </div>                                                                
                        </div>
                    </div>
                    <div class="form-actions right">
                          <button onClick=" return ajax_hotel_submit();" class="btn btn-primary btn-sm" id="add_submit">Submit</button>
                      </div>
                  </div>
                  <button id="invoice_billing" class="btn btn-block yellow" type="button" data-toggle="dropdown">
                  <span style="width:30px;float:left;"> <i class="fa fa-inr" style="font-size:16px;"></i> </span>
                  Invoice & Billing </button>
                  <div id="tab3" style="display:none; border:1px solid #c49f47; margin:-15px 0 15px; padding-top:10px;">
                        <div class="form-body">
                            <div class="row">
                                <div class="form-group col-xs-12 pop-edittable">
                                    <span>Name:</span> <?php echo $cust_name; ?><br>
                                    <span>Mobile no:</span> <?php echo $cust_contact_no;?><br>
                                    <span>Address:</span> <?php echo $cust_address ?>
                                </div>
                                <div class="col-xs-12">
                                    <table class="table table-bordered table-hover" style="background:#F1F3F2; border:1px solid #ddd;">
                                    <thead>
                                      <tr>
                                        <th style="text-align:center !important;">#</th>
                                        <th style="text-align:center !important;">Transaction Id </th>
                                        <th style="text-align:center !important;">Payment Mode </th>
                                        <th style="text-align:center !important;">Bank name </th>
                                        <th style="text-align:right !important;">Amount </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $count =  1;
                                        if($view_transaction){
                                        foreach($view_transaction as $row){ ?>
                                        <tr>
                                          <td align="center"><?php echo $count;?></td>
                                          <td align="center"><?php echo $row->t_id; ?></td>
                                          <td align="center"><?php echo $row->t_payment_mode; ?></td>
                                          <td align="center"><?php echo $row->t_bank_name; ?></td>
                                          <td align="right"><?php echo $row->t_amount; ?></td>
                                        </tr>
                                            <?php 
                                        $count++;
                                        }}?>
                                        <tr>
                                            <td colspan="4" align="right">Total amount Paid:</td>
                                            <td align="right"><?php echo $paid_amount; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="right">Total Amount Remaining:</td>
                                            <td align="right"><?php echo ($grand_total - $paid_amount  ); ?></td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>                                            
                            </div>
                        </div>
                        <div class="form-actions right">
                            <a class="btn btn-primary btn-sm" id="dwn_link" onClick="download_pdf();" style="margin-top:10px; float:right;"> Download <i class="fa fa-download"></i> </a> 
                        </div>
                        <div id="form_payment_error"> </div>
                  </div>
                  <button class="btn btn-block blue-hoki" id="checkout" onClick="return checkOut(<?php echo $total_cost-$discount; ?>,<?php echo $paid_amount; ?>,<?php echo $due_amount-$discount; ?>);" type="button"  href="#responsive">
                  <span style="width:30px;float:left;"><span class="glyphicon glyphicon-unchecked" style="font-size:16px;"></span> </span>
                  Checkout </button>
				  <?php
		
				  if ((($booking_status_id == 5)&&($due_amount!=0))||($dueAmountGp != 0))
						{?>
				  <button class="btn btn-block blue-hoki" id="checkout_credit" onClick="checkOut_show();" type="button"  href="#responsive">
					<span style="width:30px;float:left;"><span class="glyphicon glyphicon-unchecked" style="font-size:16px;"></span> </span>
                  Checkout On Credit </button>
						<?php }
	
						?>
                  <div id="tab22" style="display:none; border:1px solid #26a69a; margin:-15px 0 15px; padding-top:10px;">
                    <div class="form-body">
                        <div class="row" >
                          <div class="form-group col-xs-6">
                            <label>Due Date<span class="required">*</span></label>
                            <input type="text" class="form-control date-picker" id="due_date" name="due_date"  
                             />
                          </div>
						  
                          <div class="form-group col-xs-6">
                            <label>Card Number<span class="required"></span></label>
                            <input type="text"  class="form-control" id="card_number" name="card_number" 
                                                   />
                          </div>
						  
                          <div class="form-group col-xs-6">
                            <label>CVV<span class="required"></span></label>
                            <input type="text" class="form-control" id="cvv"  name="cvv"  
                             />
                     
                          </div>
						  
                          <div class="form-group col-xs-6">
                            <label>Expiry Date<span class="required">  </span> </label>
                            <input type="text" name="exp_date" id="exp_date" class="form-control date-picker" >
                          
                          </div>
						  
                          <div class="form-group col-xs-6">
                            <label>Banck A/C no <span class="required"> </span> </label>
                            <input type="text" id="ac_no" class="form-control" name="ac_no"  />
                       
                          </div>
						  
                          <div class="form-group col-xs-6">
                            <label>IFSC Code <span class="required">  </span> </label>
                            <input name="ifsc" class="form-control"  id="ifsc" />
                          
                          </div>
						  
                          <div class="form-group col-xs-6" >
                            <label>Company Name <span class="required"> </span> </label>
                            <input type="text" class="form-control"  id="c_name" name="c_name"/>
                          </div> 
						  
						  <div class="form-group col-xs-6"  >
                            <label>Company Number <span class="required">  </span> </label>
                            <input type="text" class="form-control"  id="c_number" name="c_number" />
                          </div>
						  
                        </div>
						
						<button class="btn btn-block blue-hoki" id="checkout_on_credit" onClick="return checkOut_on_credit(<?php if ($group_id != 0){echo $gp_total;} else {echo $grand_total-$discount;} ?>,<?php echo $paid_amount; ?>,<?php if ($dueAmountGp != 0){echo $dueAmountGp;} else{echo $due_amount-$discount; }?>);" type="button"  href="#responsive">
                  Checkout</button>
						
                    </div>

                    <!--<div class="form-actions right">
                          <button onClick=" return ajax_hotel_submit();" class="btn btn-primary btn-sm" id="add_submit">Submit</button>
                      </div>--->
                  </div>
                  
                  <div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true"  style="display:none;">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="note note-success"> <h3>The booking has been successfully checked out</h3> </div>
                              </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <a onClick="send_mail()" class="btn blue">Send Mail</a>
                          <a class="btn green hidden-print" id="dwn_link2" onClick="download_pdf3();"> Download Pdf <i class="fa fa-download"></i> </a>
                          <a onClick="reload_checkout()" data-dismiss="modal" class="btn default">Close</a>
                        </div>
                      </div>
                    </div>
                  </div> 
				  <!--<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true"  style="display:none;">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="note note-success"> <h3>The booking has been successfully checked out</h3> </div>
                              </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <a onClick="send_mail()" class="btn blue">Send Mail</a>
                          <a class="btn green hidden-print" id="dwn_link2" onClick="download_pdf3();"> Download Pdf <i class="fa fa-download"></i> </a>
                          <a onClick="reload_checkout()" data-dismiss="modal" class="btn default">Close</a>
                        </div>
                      </div>
                    </div>
                  </div>-->
                  
                  <div class="tab-pane" id="tab6"  style="display:none;">
                    <h1 style="color: #755252 !important;"><span class="glyphicon glyphicon-check"></span>" Checkout</h1>
                    <div class="form-group">
                      <label class="control-label col-md-3">Total Amount <span class="required"> * </span> </label>
                      <div class="col-md-4">
                        <input type="text" class="form-control" name="card_number" placeholder="Total Amount"/>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3">Pay Amount <span class="required"> * </span> </label>
                      <div class="col-md-4">
                        <input type="text" class="form-control" name="card_number" placeholder="Pay Amount"/>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3">Pending Amount <span class="required"> * </span> </label>
                      <div class="col-md-4">
                        <input type="text" class="form-control" name="card_number" placeholder="Pending Amount"/>
                      </div>
                    </div>
                    
                    <!--action--> 
                    `
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9"> <a href="javascript:;" class="btn green "
                                                                style="width:80px;margin-left:-10px;"> Pay </a> 
                          
                          <!--<a href="javascript:;" class="btn green button-submit">
                                                                Submit <i class="m-icon-swapright m-icon-white"></i>
                                                                </a>--> 
                        </div>
                      </div>
                    </div>
                    
                    <!--end of action--> 
                  </div>
                  <button class="btn btn-block btn-danger" id="cancel-booking" type="button" data-toggle="dropdown" onClick="return cancel();">
                  <span style="width:30px;float:left;"> <i class="fa fa-times" style="font-size:16px;"> </i> </span>
                  Cancel Booking </button>
                  <button class="btn btn-block btn-primary" id="another-booking"  type="button" data-toggle="modal" onClick="return another_booking();">
                  <span style="width:30px;float:left;"><i class="fa fa-user-plus" style="font-size:16px;"></i> </span>
                  Take Another Booking </button>
                  
                  <div id="Add_Booking" class="modal fade" tabindex="-1" aria-hidden="true"  style="display:none;">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title">Responsive & Scrollable</h4>
                        </div>
                        <div class="modal-body">
                          <?php

                                $form = array(
                                    'class' 			=> 'form-body',
                                    'id'				=> 'form',
                                    'method'			=> 'post'
                                );

                                echo form_open_multipart('bookings/hotel_backend_create_basic',$form);

                                ?>
                          <div class="scroller" style="height:300px" width="700px" data-always-visible="1" data-rail-visible1="1"> 
                            <!--pop new booking test-->
                            
                            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                              <input type="text" autocomplete="off" class="form-control" name="cust_name" required>
                              <label>Event Name <span class="required">*</span></label>
                              <span class="help-block">Enter Event Name...</span> </div>
                            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                              <input type="text" autocomplete="off" required name="start_dt" class="form-control date-picker "  id="c_valid_from" >
                              <label>Event From <span class="required">*</span></label>
                              <label for="form_control_3"></label>
                              <span class="help-block">Enter Valid Date... </span> </div>
                            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
                              <input type="text" autocomplete="off" required name="end_dt" class="form-control date-picker" id="c_valid_upto" >
                              <label>Event Up to <span class="required">*</span></label>
                              <label for="form_control_2"></label>
                              <span class="help-block">Enter Valid Up to Date... </span> </div>
                            <input type="hidden" name="room_id" value="<?php echo $room_id; ?>">
                            <br>
                            <div class="clearfix"></div>
                          </div>
                          <br>
                          <div class="form-actions noborder">
                            <button type="submit" class="btn blue" >Submit</button>
                            <button type="button" class="btn default">Reset</button>
                          </div>
                          <?php form_close(); ?>
                          
                          <!--pop new booking test--> 
                        </div>
                      </div>
                      <!--<div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn default">Close</button>
                                <button type="button" class="btn green">Save changes</button>
                            </div>--> 
                    </div>
                  </div>
                  
                  <button id="cancel_booking_reataintion" class="btn btn-block btn-danger" type="button" data-toggle="dropdown">
                  <span style="width:30px;float:left;"> <i class="fa fa-times" style="font-size:16px;"></i></span>
                  Cancel Booking With Retaintion </button>
                  <div id="tab12" style="display:none; border:1px solid #f1353d; margin:-15px 0 15px; padding-top:10px;">
                        <div class="form-body" >
                            <div class="row">
                              <div class="form-group col-xs-6">
                                <label>Total Amount Due<span class="required">*</span></label>
                                <input type="text" class="form-control" id="" name="" value="<?php echo $due_amount ?>" disabled="disabled"/>
                                
                              </div>
                              <div class="form-group col-xs-6">
                                <label>Do you want to take Retaintion ?<span class="required">*</span></label>
                                <div class="radio-list" style="margin:7px 0">
                                    <label class="radio-inline">
                                      <input type="radio" name="optionsRadios" id="optionsRadios4" value="option1" onClick="yes()" >
                                      Yes </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="optionsRadios" id="optionsRadios5" value="option2" onClick="no()" checked>
                                      No </label>
                                  </div>
                              </div>                                  
                              <div id="block" style="display:none;">
                                <div class="form-group col-xs-6">
                                  <label>Total Amount Paid<span class="required">*</span></label>
                                  <input type="text" class="form-control" id="ret_paid" name="" value="<?php echo $paid_amount ?>" disabled="disabled"/>
                                </div>
                                <div class="form-group col-xs-6">
                                  <label>Refund Amount<span class="required">*</span></label>
                                  <input type="text" class="form-control" id="ret_refund" onBlur="get_retension()"  name="" value=""/>
                                </div>
                                <script type="text/javascript">
                                    function get_retension(){
                                        var due= document.getElementById("ret_paid").value;
                                        var refund=document.getElementById("ret_refund").value;
                                        document.getElementById("ret_amount").value=parseInt(due)-parseInt(refund);
                                    }
                                </script>
                                <div class="form-group col-xs-6">
                                  <label>Enter Retintion Money<span class="required">*</span></label>
                                  <input type="text" class="form-control" id="ret_amount"  name="" value=""/>
                                </div>
                                <div class="form-group col-xs-6">
                                  <label>Payment Mode <span class="required"> * </span> </label>
                                  <select id="ret_payment_mode" class="form-control" placeholder=" Booking Type" onChange="ret_bank2(this.value)">
                                      <option value="">----- Select Payment Mode -----</option>
                                      <option value="Cash">Cash</option>
                                      <option value="Debit">Debit Card</option>
                                      <option value="Credit">Credit Card</option>
									   <option value="Bill To Company">Bill To Company</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-6" id="bank_rent_abc" style="display: none;">
                                  <label>Bank Name </label>
                                  <input type="text" class="form-control" placeholder="Bank name" id="ret_bank" >
                                </div>
                              </div>    
                            </div>              
                        </div>
                        <div class="form-actions right">
                            <div id="normal_cancel">
                              <button onClick=" cancel()" class="btn btn-primary btn-sm" id="cancel_normal">Cancel Booking</button>
                            </div>
                            <div id="block1" style="display:none;">
                                <button onClick="cancel_retention() " class="btn btn-primary" id="">Cancel Booking With Retention</button>
                            </div>
                        </div> 
                  </div>
            </div>
          </div>
        </form>
    </div>
  </div>
</div> 
</div>
<script type="text/javascript">







	        function close() {
                //window.parent.location.reload();
		        //window.parent.$( "#booking_calendar").load( "<?php echo base_url() ?>dashboard/calendar_load" );             
				window.parent.DayPilot.ModalStatic.close(this, "OK");
	        }
	        $("#f").submit(function () {
	            var f = $("#f");
	            $.post(f.attr("action"), f.serialize(), function (result) {
	                close(eval(result));
	            });
	            return false;
	        });


	        $(document).ready(function () {

	            $("#name").focus();

	            $("#add_transaction").click(function(){

	            	$("#tab2").toggle();
					$("#tab3").hide();
	            });

	    $("#invoice_billing").click(function(){

	  	$("#tab3").toggle();
		$("#tab2").hide();

	  });

	  $("#cancel_booking_reataintion").click(function(){

	  	$("#tab12").toggle();

	  });

	    $("#split_booking").click(function(){

	  	$("#tab15").toggle();

	  });
});//end of document.ready

	        </script> 
<script>
 
	$(document).ready(function(){
	$("#add_transaction").click(function() {
    $('html, body').animate({
        scrollTop: $("#tab2").offset().top
    }, 2000);
    });
 	$("#invoice_billing").click(function() {
    $('html, body').animate({
        scrollTop: $("#tab3").offset().top
    }, 2000);
    });
});
</script>
<script type="text/javascript">
		function ajax_hotel_submit()
		{
			    //document.getElementById('add_submit').style.display= 'none';
				$('#add_submit').prop('disabled', true);
				//alert($("#add_amount").val());
				var booking_id = $("#booking_id").val();
				var booking_status_id = $("#booking_status_id").val();
				var p_center = $("#profit_center").val();
				var add_amount = $("#add_amount").val();
				var pay_mode   = $("#pay_mode").val();
				var bankname   = $("#bankname").val();
				var payment_status = $("#payment_status").val();
				var from_date = $("#checkindate").val();
				var to_date = $("#checkoutdate").val();
				//alert(to_date); return false;
				if($("#add_amount").val() == null || $("#add_amount").val() ==0 )
				{
					//alert('Error');
					$('#add_submit').prop('disabled', false);
					swal({
                                    title: "Error",
                                    text: "An Error Occurd",
                                    type: "warning"
                                },
                                function(){
                                    //location.reload();
                                });
					return false;
				}
				else{

				jQuery.ajax(
				{
					type: "POST",
					url: "<?php echo base_url(); ?>bookings/add_booking_transaction",
					dataType: 'json',
					data: {t_booking_id:booking_id,t_booking_status_id:booking_status_id,p_center: p_center,t_amount:add_amount,t_payment_mode:pay_mode,t_bank_name:bankname,t_status:payment_status,start_date:from_date,end_date:to_date },
					success: function(data){
						//alert('Add Successfull');
						swal({
							title: "Add Successfull",
							text: "Your Payment Is Taken",
							type: "success"
						},
						function(){			
						$("#booking_id").val(' ');
						$("#booking_status_id").val(' ');
						$("#add_amount").val(' ');
						$("#pay_mode").val(' ');
						$("#bankname").val(' ');
						$("#payment_status").val(' ');
						$('#tab2').hide();
						//location.reload();         
                        });
					
					}
				});
				}
	    	return false;
		}
	</script> 
<script>

function download_pdf2()
{
	$.ajax(
		{
			type: "POST",
			url: "<?php echo base_url();?>Dashboard/",
			data:
			{
				term:a
			}
		}
	).done(
		function(data)
		{
			//console.log(data);
			//$("#d").html(data);
		}
	);

	$("#bank_name").hide();

	  $("#pay_mode").change(function(){

	    if($("#pay_mode").val()=='Debit' || $("#pay_mode").val()=='Credit')
	    {
			$("#bank_name").show();
	    }

	    else
	    {
	    	$("#bank_name").hide();
	    }
	  });

}


function download_pdf() {
	var booking_id = $('#booking_id').val();
	var item_no = $('#item_no').val();
	if(item_no=="0"){

		alert("No invoice has been generated. Generate in booking details page");

	}else{
	
	$("#dwn_link").attr("href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id);
	var f = $("#f");
            $.post(f.attr("action"), f.serialize(), function (result) {
            
                close(eval(result));
            });
	 return false;
	}
}
function download_pdf3() {

    var booking_id = $('#booking_id').val();
    //$.post("<?php echo base_url();?>bookings/pdf_generate");
    $("#dwn_link2").attr("href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id);
    var f = $("#f");
    $.post(f.attr("action"), f.serialize(), function (result) {
        close(eval(result));
    });
    return false;
}


function checkIn()
{
	var booking_id =  $("#booking_id").val();
	$.ajax({
			type:"POST",
			url: "<?php echo base_url()?>dashboard/checkin_submission",
			data:{booking_id:booking_id},
			success:function(data)
			{
				//alert("Checked-In Successfully");
				//location.reload();
				swal({
                                    title: "Checked-In Successfully",
                                    text: "You Are Checked-In Successfully",
                                    type: "success"
                                },
                                function(){
                                    location.reload();
                                });
			}
		});
}

function checkOut(total, paid,due)
{

//alert(due);


//conformation start
swal({   title: "Are you sure?",   text: "You will be checked out and checkout date and time will be changed accordingly!",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, Checkout Me!",   closeOnConfirm: true }, function(){  


//$(".btn red btn-lg frth").attr("data-toggle", "modal");



	var booking_id =  $("#booking_id").val();
    var date_checkout = $("#checkoutdate1").val();
	var dateCheckout = $("#checkOutDate11").val();
	if((parseInt(due))<=0)
	{
		$.ajax({
			type:"POST",
			url: "<?php echo base_url()?>dashboard/checkout_submission",
			data:{booking_id:booking_id,date_checkout:date_checkout,dateCheckout:dateCheckout},
			success:function(data)
			{
				//alert("Checked Out");
				swal({
                                    title: "Checked Out",
                                    text: "You Are Checked Out ",
       
                             type: "success"
                                },
                                function(){
                                	$("#responsive").modal();

                                   // location.reload();
                                });



			}
		});
	}
	else
	{
		$('#tab2').show();
	}







 });
//conformation end






}
function send_mail(){
    var booking_id =  $("#booking_id").val();
    var date_checkin = $("#checkindate").val();
    var date_checkout = $("#checkoutdate").val();

    $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>bookings/send_mail",
        data: {booking_id: booking_id},
        success: function (data) {
          //  alert(data);
            swal({
                    title: data,
                    text: "Email Sent Sucessfully",
                    type: "success"
                },
                function(){
                    location.reload();
                });
        }
    });


}
function cancel()
{ var booking_id =  $("#booking_id").val();
    var date_checkin = $("#checkindate").val();
    var date_checkout = $("#checkoutdate").val();
    var d1 = new Date();
    var d2 = new Date(date_checkin);
    var d3 = new Date(date_checkout);



        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>dashboard/cancel_submission",
            data: {booking_id: booking_id},
            success: function (data) {
                swal({
                        title: "Booking Cancelled",
                        text: "Booking is cancelled",
                        type: "warning"
                    },
                    function(){

                        window.parent.$( "#booking_calendar").load( "<?php echo base_url() ?>dashboard/calendar_load" );
                        parent.DayPilot.ModalStatic.close();
                        //location.reload();
                    });
            }
        });


}

function cancel_retention()
{ var booking_id =  $("#booking_id").val();
    var date_checkin = $("#checkindate").val();
    var date_checkout = $("#checkoutdate").val();
    var amount=$("#ret_amount").val();
    var mode=$("#ret_payment_mode").val();
    var bank=$("#ret_bank").val();
    var d1 = new Date();
    var d2 = new Date(date_checkin);
    var d3 = new Date(date_checkout);



    $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>dashboard/cancel_submission_retention",
        data: {booking_id: booking_id, amount:amount,mode:mode,bank:bank},
        success: function (data) {
            swal({
                    title: "Retention Money Taken",
                    text: "Retention  money is taken and a transaction is added",
                    type: "success"
                },
                function(){
                    window.parent.$( "#booking_calendar").load( "<?php echo base_url() ?>dashboard/calendar_load" );
                    parent.DayPilot.ModalStatic.close();
                    //location.reload();
                });
        }
    });


}

function another_booking()
{
   // alert("dasd");
   // var booking_id =  $("#booking_id").val();

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    }

    if(mm<10) {
        mm='0'+mm
    }

    today = yyyy+'-'+mm+'-'+dd;
    //alert(today);


    $.ajax({
        type:"POST",
        url: "<?php echo base_url()?>bookings/hotel_backend_create_basic",
        data:{room_id:<?php echo $room_id ?>,start_dt:today,end_dt:today, cust_name: 'New Guest'},
        success:function(data)
        {
            alert('Booking Event Generated');
            //location.reload();
			swal({
                                    title: "Booking Event Generated",
                                    text: "A blank booking event is created",
                                    type: "success"
                                },
                                function(){
                                    location.reload();
                                });
        }
    });


}

/*function another_booking()
{
    dp.onEventClick = function(args) {
        var modal = new DayPilot.Modal();
        modal.closed = function() {
            // reload all events
            var data = this.result;
            if (data && data.result === "OK") {
                loadEvents();
            }
        };
        modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking?id=" + args.e.id());
    };

}*/
function swap_bank(){
    var mode= $('#pay_mode').val();

    if(mode=="cards"){
        document.getElementById("bank").style.display="block";
    }
    else{
        document.getElementById("bank").style.display="none";
    }
}

function check_amount()
{
	var amount = $("#add_amount").val();
	//alert(amount);
	var due_amount  = $("#due_amount").val();
	//alert(due_amount);
	if(parseInt(amount) > parseInt(due_amount))
	{
		//alert("The amount should be less than the due amount Rs: "+due_amount);
		swal({
                                    title: "Amount Should Be Smaller",
                                    text: "(The amount should be less than the due amount Rs: +due_amount)",
                                    type: "warning"
                                },
                                function(){
                                    //location.reload();
                                });
		$("#add_amount").val("");
		return false;
	}
	else{
		return true;
	}
}

function go_edit(id){

    //window.parent.location.href( "<?php echo base_url() ?>dashboard/edit_booking?b_id="+id );
	parent.parent.window.location.replace("<?php echo base_url() ?>dashboard/booking_edit?b_id="+id );

    //parent.DayPilot.ModalStatic.close(result);

}

function take_new_booking(){

    //window.parent.location.href( "<?php echo base_url() ?>dashboard/edit_booking?b_id="+id );
	//parent.parent.window.location.replace("<?php echo base_url() ?>dashboard/pop_new_booking" );

  // parent.DayPilot.ModalStatic.close(result);




}
function go_edit_group(id,grp){

    //window.parent.location.href( "<?php echo base_url() ?>dashboard/edit_booking?b_id="+id );
	parent.parent.window.location.replace("<?php echo base_url() ?>dashboard/booking_edit_group?b_id="+id+"&group_id="+grp );

    //parent.DayPilot.ModalStatic.close(result);




}

function reload_checkout(){
	parent.DayPilot.ModalStatic.close();
	//location.reload();
}

function yes()
{
	document.getElementById('block').style.display= 'block';
    document.getElementById('cancel_normal').style.display= 'none';
	document.getElementById('block1').style.display= 'block';
}
function no()
{
	document.getElementById('cancel_normal').style.display= 'block';
	document.getElementById('block').style.display= 'none';
	document.getElementById('block1').style.display= 'none';
}
/*$(this).modal({
    backdrop: 'static',
    keyboard: false
})*/
</script> 
<script >
	function ret_bank2(value){
		//alert(value);
		if(value=="Cash"){

			document.getElementById("bank_rent_abc").style.display="none";

		}else{
			document.getElementById("bank_rent_abc").style.display="block";
		}
	}
</script> 
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
<script src="<?php echo base_url();?>assets/dashboard/charts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/charts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/charts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/clock/flipclock.min.js" type="text/javascript"/></script>
<script src="<?php echo base_url();?>assets/dashboard/sweetdate/sweetalert.min.js" type="text/javascript"/></script>

<script>
    function price_range(){
        var range=document.getElementById("range_1").value;

        $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_roomprice_tree",
            { range: range },
            function(data) {
                dp.resources = data;
                dp.update();
            });
       // alert(range);

    }
	
	
	
	function checkOut_on_credit(total,paid,due)
{ 
	var booking_id =  $("#booking_id").val();
    var date_checkout = $("#checkoutdate1").val();
    var dateCheckout = $("#checkOutDate11").val();
	//alert(due);
	//return false;
    var due_date = $("#due_date").val();
    var card_number =$("#card_number").val();
    var cvv =$("#cvv").val();
    var exp_date =$("#exp_date").val();
    var ac_no =$("#ac_no").val();
    var ifsc =$("#ifsc").val();
    var c_name =$("#c_name").val();
    var c_number =$("#c_number").val();
	
	var id =<?php echo $group_id ?>;	
			if (id != 0)
			{
				var b_id= id;
				var b_type = 'gb';
			}
			else
			{
				var b_id=booking_id;
				var b_type = 'sb';
			}
			
			//alert (b_type);
			//return false;
	
	if(/*(booking_id != "")&&(date_checkout != "")&& (dateCheckout != "")&&*/(due_date != "")/*&&(card_number != "")&&(cvv != "")&&(exp_date != "")&&(ac_no != "")&&(ifsc != "")&&(c_name != "")&&(c_number != "")*/)
	{
		          
	if((parseInt(due)) > 0)
	{

		$.ajax({
			type:"POST",
			url: "<?php echo base_url()?>dashboard/checkout_submission",
			data:{booking_id:booking_id,date_checkout:date_checkout,dateCheckout:dateCheckout,cr_fl:'1',},
			success:function(data)
			{
			
		$.ajax({
			type:"POST",
			url: "<?php echo base_url()?>dashboard/checkout_submission_on_credit",
			data:{booking_id:b_id,date_checkout:date_checkout,dateCheckout:dateCheckout,due_date : due_date,card_number : card_number,cvv : cvv,exp_date : exp_date,ac_no : ac_no,ifsc : ifsc,c_name :c_name,c_number :c_number,b_type:b_type,total:total,},
			success:function(data)
			{
				
				swal({
                                    title: "Checked Out On Credit",
                                    text: "Creditor ID : "+data,
       
                             type: "success"
                                },
                                function(){
									
                                	$("#responsive").modal();

                                });
								



			}
		});
		}
		});
	}
	else
	{
		//$('#tab00').show();
		alert("Already Paid");
	}


	}
	else
	{
		alert("Due Date Cannot Be Empty");
	}




 }
	
	</script>
	<script>
function checkOut_show()
{   
if($('#tab22').css('display') != 'none')
	{
	$('#tab22').hide();
	}
else
	{
	$('#tab22').show();
	$("html, body").delay(100).animate({scrollTop: $('#checkout_on_credit').offset().top }, 700);
	}
}
	</script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script>
	$('#due_date').datepicker({ dateFormat: 'yy-mm-dd', minDate: new Date() });
	$('#exp_date').datepicker({ dateFormat: 'yy-mm-dd', minDate: new Date() });
</script>
<?php if($group_id !=0){ if ($gp_total-$ad->ad > $paidgrp){ ?>
	<script type="text/javascript">
	    //console.log("HERE");
		$(document).ready(function(){
			$("#checkout").hide();
		});
	</script>
<?php } }?>
</body>
<!-- END BODY -->
</html>