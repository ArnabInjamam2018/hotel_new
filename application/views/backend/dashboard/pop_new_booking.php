<?php 

date_default_timezone_set('Asia/Kolkata');

$total_rent=0;
foreach ($rooms as $room) {
	$room_id= $room->room_id;

    $room_no = $room->room_no;
    $room_uid=$room->unit_id;
	$UnitDetails=$this->dashboard_model->getUnit_details($room_uid);
    $room_bed=$UnitDetails->default_occupancy;
	$room_occupancy=$UnitDetails->max_occupancy;
  
}


if(isset($taxes) && $taxes!=''){
	foreach ($taxes as $tax) {
		$hotel_service_tax=$tax->hotel_service_tax;
		$hotel_luxury_tax=$tax->hotel_luxury_tax;
		
	}
}
else{
	
	$hotel_service_tax=0;
	$hotel_luxury_tax=0;
}

date_default_timezone_set('Asia/Kolkata');

$start_dt = date("d-m-Y", strtotime($start));
$dateSt = strtotime("+1 day", strtotime($start_dt));
$dateSt = date("d-m-Y", $dateSt);

if(isset($times) && $times){
	
	foreach($times as $time) {
			if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
				$tym = ($time->hotel_check_in_time_hr + 12) . ":" . $time->hotel_check_in_time_mm;
			}
			else {
				$tym = ($time->hotel_check_in_time_hr) . ":" . $time->hotel_check_in_time_mm;
			}
		}
} else {
	$tym =0;
}

?>
	<script>
		
		console.info(<?php echo date("d-m-Y").' == '.$dateSt;?>);
	</script>
<?php



if($start_dt==date("d-m-Y") && ($tym < date("H:i:s"))) { 
    $checkin_time=date("H:i");
	
} else {
    foreach($times as $time) {
        if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
            $checkin_time = ($time->hotel_check_in_time_hr + 12) . ":" . $time->hotel_check_in_time_mm;
        }
        else{
            $checkin_time = ($time->hotel_check_in_time_hr) . ":" . $time->hotel_check_in_time_mm;
        }
    }
}

if(isset($times) && $times){
	foreach($times as $time) {
		if($time->hotel_check_out_time_fr=='PM' && $time->hotel_check_out_time_hr !="12") {
			$checkout_time = ($time->hotel_check_out_time_hr + 12) . ":" . $time->hotel_check_out_time_mm;
		}
		else{
			$checkout_time = ($time->hotel_check_out_time_hr) . ":" . $time->hotel_check_out_time_mm;
		}
	}
}

$chkD = 0;
if(date("d-m-Y") == $dateSt && $checkout_time > date("H:i:s")){
	$chkD = 1;
	$start_dt1 = date("d-m-Y");
	$checkin_time=date("H:i:s");
} // _sb 29-06-17
else
	$start_dt1 = 'no';

$end_dt = date("d-m-Y", strtotime($end));
$start_d = new DateTime($start_dt);
$end_d =  new DateTime($end_dt);
$dStart = new DateTime($start_dt);
$dEnd  = new DateTime($end_dt);
$dDiff = $dStart->diff($dEnd);
   
   $datediff = $dDiff->days;

   $diff= floor($datediff/(60*60*24));

$start_time = date("H:i:s", strtotime($start));


$end_time = date("H:i:s", strtotime($end));

$event_name="No events today";
$event_color_bg="white";
$event_color_text="#b4bcc8";


?>
<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://fonts.googleapis.com/css?family=Exo+2:400,700|Monda:400,700|Open+Sans:400,700|Roboto:400,700" rel="stylesheet">
<link href="<?php echo base_url();?>assets/layouts/layout/css/font.css" rel="stylesheet" type="text/css" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url();?>assets/global/plugins/daypilot/media/layout.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url();?>assets/pages/css/bootstrap-wizard.css" rel="stylesheet" type="text/css" />
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo base_url();?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url();?>assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?php echo base_url();?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/themes/light2.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/typeahead1/jquery.typeahead.css">
<!-- END THEME LAYOUT STYLES -->
<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script>

   

    </script>
<script>
$(document).ready(function(){
	$('#loading-image').hide();
	$('#loading-image6').hide();
    var current_width = $(window).width();
    if(current_width < 568)
      $('html').addClass("pop-edit");
});
$(window).resize(function(){
    var current_width = $(window).width();
    if(current_width < 568)
      $('html').addClass("pop-edit");

    if(current_width > 568)
      $('html').removeClass("pop-edit");

  });
</script>
<style>
html, body {
	height: 100%;
}
</style>
</head>
<body class="page-md page-header-fixed page-quick-sidebar-over-content">
<div style="position:relative; height: 100%;"> 
  <script>
    setTimeout(function() 
    { 
        document.getElementById("loader").style.display = "none"; 
        document.getElementById("body").style.display = "block"; 
    }, 1500);
    
    
    </script>
  <div id="loader" style="top:45%; left:45%">
    <div class='loader'>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='window'></div>
      <div class='door'></div>
      <div class='hotel-sign'> <span>H</span> <span>O</span> <span>T</span> <span>E</span> <span>L</span> </div>
    </div>
  </div>
  <div class="page-container" id="body" style="display:none; margin:0;">
    <div class="portlet box grey-cascade" style="margin-bottom:0px;">
      <div class="portlet-title">
        <div class="caption"><i class="fa fa-address-card" aria-hidden="true"></i> New Reservation </div>
        <div class="tools" style="display: inline-block; float: right; padding: 12px 0 8px;"> <a onclick="cancel_booking();" style="color:#ffffff;"> <!--href="javascript:close();"--><i class="fa fa-times"> </i></a> </div>
        <div id="short_info" style="font-size:12px; text-align:center; width:100%; padding:11px 0;"></div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
          <div class="clearfix long">
            <ul class="nav wizard-nav-list">
              <li class="wizard-nav-item" id="istli"><a class="wizard-nav-link active" ><span class="glyphicon glyphicon-chevron-right"></span> Guest Details</a></li>
              <li class="wizard-nav-item" id="2ndli"><a class="wizard-nav-link"><span class="glyphicon glyphicon-chevron-right"></span> Reservation Details</a></li>
              <li class="wizard-nav-item" id="3rdli"><a class="wizard-nav-link"><span class="glyphicon glyphicon-chevron-right"></span> Broker / OTA</a></li>
              <!--<li class="wizard-nav-item" id="4thli"><a class="wizard-nav-link"><span class="glyphicon glyphicon-chevron-right"></span> Summary</a></li>-->
              <li class="wizard-nav-item" id="5thli"><a class="wizard-nav-link red"><span class="glyphicon glyphicon-chevron-right"></span> Final Submit</a></li>
            </ul>
            <div class="new-bookarea">
              
             <div id="tab_child"  style="display:none; background: lightcyan; padding:10px;">
                <form action="" class="form-horizontal" id="formchild" method="POST">
                  <div class="form-body" style="display:none;">
                    <input type="hidden" id="id_guest" name="id_guest" value="" />
                    <input type="hidden" id="room_id" name="room_id" value="<?php echo $resource; ?>" />
                    <div class="form-group">
                      <label class="control-label col-md-3">Master Booking Id child: <span class="required"> * </span> </label>
                      <div class="row">
                        <div class="col-xs-2"> <b style="margin-left: 45%; margin-top: 550px; color: #a9a9a9"> HM0<?php echo $this->session->userdata('user_hotel') ?>00</b> </div>
                        <div class="col-xs-6">
                          <input type="text" required onkeyup="master_id_hint(this.value)" onkeypress="return onlyNos(event, this); " class="form-control" name="master_id" id="master_id" value="" placeholder="Master Booking Id"/>
                        </div>
                        <div class="col-xs-3" id="master_id_validate"> </div>
                        <input type="hidden" id="master_id_validate_token">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3">Guest Name child: <span class="required"> * </span> </label>
                      <div class="col-md-4">
                        <input type="text" id="g_name_child" required onkeypress="return onlyLtrs(event, this);" class="form-control" name="cust_name" id="cust_name" placeholder="Guest Name" value="" />
                      </div>
                    </div>
                    <div class="row" style="display:none;">
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label class="control-label col-md-3"> Address child: <span class="required"> * </span> </label>
                          <div class="col-md-4">
                            <input id="g_address_child" type="text" required class="form-control" name="cust_address_child" 
                                      id="cust_address_child" placeholder="Guest Address" value=""/>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label class="control-label col-md-3">Mobile Number child: <span class="required"> * </span> </label>
                          <div class="col-md-4">
                            <input id="g_number_child" type="text" required maxlength="10" onkeypress="return onlyNos(event,this);" class="form-control" name="cust_contact_no_child" id="cust_contact_no_child"placeholder="Guest Number" value=""/>
													
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="line" style="float:left; width:100%;">
                      <div class="row">
                        <div class="col-xs-6">
                          <div class="form-group ">
                            <label class="control-label col-md-3">Check in date child: <span class="required"> * </span> </label>
                            <div class="col-md-4">
                              <input type="text" required  class="form-control" id="startDate" name="start_dt" value="<?php echo $start_dt; ?>"  />
                              
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-6">
                          <div class="form-group">
                            <label class="control-label col-md-3">Check in Time yo yo: <span class="required"> * </span> </label>
                            <div class="col-md-4">
                              <input type="text" id="checkin_id_child" required class="form-control"  name="start_time_child" value="<?php ?>" />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                </form>
              </div>
              
              
			  <div id="tab12" style="display:block; min-height: 494px; background:#F1F3F2; border:1px solid #e5e5e5; position:relative;">
			  
						 <div id="loading-image6" class="pop-lo">
		  					<img src="<?php echo base_url();  ?>assets/ajax-loader.gif" width="94" height="94">		  
		  			  </div>
			  

                <form action="" id="form1st" method="POST">
                  <div class="form-body" style="padding-bottom:67px;">
                    <input type="hidden" id="id_guest2" name="id_guest" value="" />
                    <input type="hidden" id="room_id" name="room_id" value="<?php echo $resource; ?>" />
                    <input type="hidden" id="room_noh" name="room_noh" value="<?php echo $room_no; ?>" />
					
                    <div class="row">
					<div style="margin-bottom:10px; font-size:14px; color:#008F6C; text-align:center;"> <strong style="color:#2B3643">Room Number:</strong> <?php echo $room_no; ?> </div>
                      <div class="col-xs-12">
                          <div class="form-group">
                           <!--<label>Guest Name: <span class="required"> * </span> </label>-->
                            <div class="typeahead__container ">
                                <div class="typeahead__field">            
                                    <span class="typeahead__query">
                                        <input class="js-typeahead-user_v1" name="g_name[query]" id="g_name" type="search" placeholder="search guest here..." autocomplete="off">
                                    </span>
									
                                   
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-xs-3">
                        <div class="form-group">
                          <label>Profit Center <span class="required"> * </span> </label>
                          <select name="p_center" class="form-control input-sm bs-select" id="" required >
                            <?php $pc=$this->dashboard_model->all_pc();?>
                            <?php
                              $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;} else {$defPro="Select";}
                              ?>
                            <option value="<?php echo $defPro; ?>"selected><?php echo $defPro; ?></option>
                            <?php $pc=$this->dashboard_model->all_pc1();
                                foreach($pc as $prfit_center){
                                ?>
                            <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                            <?php }?>
                          </select>
                        </div>
                      </div>
					   <div class="col-xs-3">
                        <div class="form-group">
                          <label> Guest Name: <span class="required"> * </span> </label>
                          <input type="text" required onkeypress="return onlyLtrs(event, this);" class="form-control input-sm" name="cust_name" id="cust_name" placeholder="Guest Name" value="" />
                        </div>
                      </div>
                       <div class="col-xs-3">
                        <div class="form-group">
                          <label> Pin Code: </label>
                          <input type="text" class="form-control input-sm" name="cust_address" id="cust_address" placeholder="Guest Pincode" onblur="fetch_all_address_hotel(this.value,'#g_country','#g_state')" value="" />
                          <input type="text"  name="cust_full_address" id="cust_full_address" style="display:none;"/>
                        </div>
                      </div>
                      
					  <div class="col-xs-3">
					
						<div class="form-group">
						<label> Country: <span class="required"> * </span> </label>
						<select class="form-control input-sm bs-select" id="g_country" name="g_country" required>
							<option value="India" disabled="disabled">Select Country Name </option>
						<?php $allCountry = $this->setting_model->getAllCountry();
							
							/*echo '<pre>';
							print_r($allCountry);
							echo '</pre>';
							exit;*/
						
							if(isset($allCountry) && $allCountry){
								$i = 0;
								$prevReg = '';
								$defC = 'India';
								$selectC = '';
								$defC = strtoupper($defC);
								
								foreach($allCountry as $country){
									
										if($country->Region != $prevReg){
											echo '<optgroup label="'.$country->Region.'">';
										}
									
										if(trim($defC) == trim(strtoupper($country->Country))){
											$selectC = 'selected="selected"';
										} else {
											$selectC = '';
										}
									
										if(isset($country->Country)){
						?>
											<option value="<?php echo trim(strtoupper($country->Country)); ?>" <?php echo $selectC; ?>>
												<?php echo $country->Country; ?>
											</option>
						<?php
										}

										$prevReg = $country->Region;
								}
							}
						?>
						</select>
						</div>
					</div>
					  
					  
                      <div class="col-xs-3" id="g_state_div1">
                        <div class="form-group">
						
                          <label> State: <span class="required"> * </span> </label>
                         <!-- <input type="text" required class="form-control input-sm" name="g_state" id="g_state" placeholder="State" value="" />-->
						 <select class="form-control input-sm bs-select" id="g_state" name="g_state" required >
							<option value="India" disabled="disabled">Select State Name </option>
						 
									<?php 
				
				$allState = $this->setting_model->getAllState();
				
				//print_r($allState);
				//exit;
					
					if(isset($allState) && $allState){
						$i = 0;
						$prevTyp = '';
						$defS = 'West Bengal';
						$selectS = '';
						$defS = strtoupper($defS);
						
						foreach($allState as $state){
							
							if($state->Type != $prevTyp)
								echo '<optgroup label="'.$state->Type.'">';
							
							if(trim($defS) == trim(strtoupper($state->Name))){
								$selectS = 'selected="selected"';
							} else {
								$selectS = '';
							}
							
							if(isset($state->Name)){
								?>
							<option value="<?php echo trim(strtoupper($state->Name)); ?>" <?php echo $selectS; ?>>
								<?php echo $state->Name; ?>
							</option>
							<?php
							}

							$prevTyp = $state->Type;
							}
							}
							?>
							</select>
                        </div>
                      </div>
					  
					  
					  <div class="col-md-3" id="other_st" style="display:none;">
                <div class="form-group" >
				<label>Enter State: <span class="required"> * </span> </label>
			<input type="text" class="form-control input-sm" place="put your country's state" id="g_state1" name="g_state1">
				</div>
				</div>
                      <div class="col-xs-3">
                        <div class="form-group">
                          <label>Mobile Number: <span class="required"> * </span> </label>
                          <input type="text" required maxlength="30" onblur="onlyNum(this.value);" class="form-control input-sm" name="cust_contact_no" id="cust_contact_no" placeholder="Guest Number" value=""/> 
						
					   </div>
                      </div>
					  	<script>
						function onlyNum(num){
						 
							  var filter = /^[0-9-+/, ]+$/;

						if (filter.test(num)) {

						return true;
						}else {
						swal("Please enter number properly!", "", "warning");
						$("#cust_contact_no").val("");
						$("#cust_contact_no").focus();
						}

					}
		</script>
                      <div class="col-xs-3">
                        <div class="form-group">
                          <label>Email Id: </label>
                          <input type="text" class="form-control input-sm" name="cust_mail" id="cust_mail" placeholder="Email Id" value="" />
                        </div>
                      </div>
                      <div class="col-xs-3">
                        <div class="form-group">
                          <label>Nature of visit: <span class="required"> * </span></label>
                          <?php $visit=$this->dashboard_model->getNatureVisit1(); ?>
                          <select name="nature_visit" id="nature_visit" class="form-control input-sm bs-select" placeholder="Booking Type" required>
                            <?php $defNature=$this->dashboard_model->default_nature_visit(); if(isset($defNature)&& $defNature){$defname=$defNature->booking_nature_visit_name;}else{$defname="Select";}  ?>
                            <option value="<?php echo $defname; ?>"selected><?php echo $defname;?></option>
                            <?php foreach ($visit as $key ) {
                                        # code...
                                      ?>
                            <option value="<?php echo $key->booking_nature_visit_name; ?>"><?php echo $key->booking_nature_visit_name; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-3">
                        <div class="form-group ">
                          <label>Check in date: <span class="required"> * </span> </label>
                          <div class="input-group date ">
                            <input type="text" required id="start_dt"  class="form-control input-sm " name="start_dt" value="<?php echo $start_dt; ?>" onblur="change_stay_span(this.value)" placeholder="dd-mm-yyyy" readonly>
                            <span class="input-group-btn">
                         <button class="btn default btn-sm" type="button" style="padding: 5px 10px 4px;" onclick="make_date_readable('start_dt')"><i class="fa fa-edit"></i></button>
						 <input type="hidden"  class="form-control" id="startDate1" name="start_dt1" value="<?php echo $start_dt1; ?>"  />
                            </span> </div>
                        </div>
                      </div>
                      <div class="col-xs-3">
                        <div class="form-group">
                          <label>Check in Time: <span class="required"> * </span> </label>
                          <!--<input type="text" id="checkin_id" required  onclick="check_booking(this.value)" class="form-control timepicker timepicker-24 input-sm" name="start_time" onchange="checkintime()"  />-->
                          <input type="text" id="checkin_id" required class="form-control input-sm" name="start_time" value="<?php echo $checkin_time;?>"/>
						  <input type="hidden" id="def_checkin_time" value="<?php echo $checkin_time;?>"/>
                        </div>
                      </div>
                      <div class="col-xs-3">
                        <div class="form-group  ">
                          <label>Check Out date: <span class="required"> * </span> </label>
                          <div class="input-group date ">
                            <input  required="required" type="text" class="form-control input-sm" id="end_dt" name="end_dt" value="<?php echo $end_dt; ?>" readonly  placeholder="dd-mm-yyyy"/>
                            <span class="input-group-btn">
                            <button class="btn default btn-sm" type="button" style="padding: 5px 10px 4px;" onclick="make_date_readable('end_dt')"><i class="fa fa-edit"></i></button>
                            </span> </div>
                        </div>
                      </div>
                      <div class="col-xs-3">
                        <div class="form-group ">
                          <label>Check Out Time: <span class="required"> * </span> </label>
                          <input type="text" required class="form-control timepicker timepicker-24 input-sm" name="end_time" id="end_time" onchange="checkouttime()" value="<?php echo $checkout_time;?>"/>
						  <input type="hidden" id="def_checkout_time" value="<?php echo $checkout_time;?>"/>
                        </div>
                      </div>
                      <div class="col-xs-3">
                        <div class="form-group">
                          <label>Coming From:</label>
                          <input type="text" class="form-control input-sm" name="coming_from" placeholder="Coming From"/>
                        </div>
                      </div>
                      <div class="col-xs-3">
                        <div class="form-group">
                          <label>Marketing Personnel:</label>
                          <?php $marketing=$this->unit_class_model->all_marketing_personel();   ?>
                          <select name="marketing_personnel" id="marketing_personnel" class="form-control input-sm bs-select" placeholder="Marketing Personnel" required>
                            <option value="0">Select MP </option>
                            <?php foreach ($marketing as $key ) {
                                        # code...
                                      ?>
                            <option value="<?php echo $key->name; ?>"><?php echo $key->name; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-3">
                        <div class="form-group">
                          <label>Next Destination:<span class="required"> </span> </label>
                          <input type="text"  class="form-control input-sm" name="next_destination" placeholder="Next Destination"/>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-actions right" style="position:absolute; bottom:0; width: 100%;">
                  	<!--<a href="javascript:void(0);" onclick="prev();" class="btn btn-default"> Previous </a> -->
                  	<input name="Submit1" id="submit1" type="submit" class="btn submit" value="Continue" onclick="updateRates()" />                   
                    <a href="javascript:close();" class="btn pink pull-left">Cancel </a> 
                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Send message</button>--> 
                  </div>
                </form>
              </div>
			  
              <div class="frm2 form" id="tab2" style="display:none; min-height: 494px; background:#F1F3F2; border:1px solid #e5e5e5; padding:10px; position:relative;">
               	<div id="loading-image" class="pop-lo">
		  			<img src="<?php echo base_url();  ?>assets/ajax-loader.gif" width="94" height="94">		  
		  		</div>
                <form action="" id="form2nd" method="POST">
                  <div class="form-body" >
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <div class="radio-list row re-modi re-modiss"> 
                          	<div class="col-xs-3">                             
                              <label for="radio6" class="radio-inline curen"> 
                              	<input type="radio" id="radio6"  name="booking_type" class="curen" value="current" onclick="check_cur_date(); get_season();" >
                              	<i class="fa fa-clock-o" aria-hidden="true"></i> <span>Current (Checkin)</span>
                              </label>
                            </div>
                            <div class="col-xs-3">  
                              <label for="radio7" class="radio-inline adven active">
                              	<input type="radio" id="radio7" name="booking_type" onclick="get_season()" class="adven" value="advance" >
                              	<i class="fa fa-calendar" aria-hidden="true"></i> <span>Advance </span>
                              </label>           
                            </div>
                            <div class="col-xs-3">                     
                              <label for="radio8" class="radio-inline temp">
                              	<input type="radio" id="radio8" name="booking_type"  class="temp" value="temporaly" onclick="get_season()" >
                              	<i class="fa fa-lock" aria-hidden="true"></i> <span>Temporary Hold </span>
                              </label>
                            </div>
                          </div>
                        </div>
						<input type="hidden" id="Bkstatus" />
						<script>
							$("#radio6").on('click', function(){
								
								var status=$("#radio6").val();
								$("#Bkstatus").val(status);
							});
							$("#radio7").on('click', function(){
								
								var status=$("#radio7").val();
								$("#Bkstatus").val(status);
							});
							$("#radio8").on('click', function(){
								
								var status=$("#radio8").val();
								$("#Bkstatus").val(status);
							});
						</script>
                      </div>
                      <div class="col-xs-3">
                        <div class="form-group">
                          <label>Booking Source:</label>
                          <select name="booking_source"  id="booking_source" class="form-control input-sm bs-select"  >
                            <?php $defBookingSource=$this->unit_class_model->default_booking_source(); 
                                
                                if(isset($defBookingSource) && $defBookingSource){$defname=$defBookingSource->booking_source_name ;}else{$defname="Select";}
                                ?>
                           
                            <?php 
                              $dta=$this->unit_class_model->get_booking_source1();
                              if($dta)
                              {
                                  foreach($dta as $d) 
                              {
                                  ?>
                            <option value="<?php echo $d->bk_source_type_id;?>"><?php echo $d->booking_source_name;?></option>
                           
                            <?php } }?>
                          </select>
                          <input type="hidden" id="season_id" />
                          <input type="hidden" id="u_type_id" />
                          <input type="hidden" id="o_id" />
                        </div>
                      </div>
                      <input id="brokerchannel" value="" type="hidden"/>
					  <div class="col-xs-3">
                        <div class="form-group">
                          <label>Charge Type:</label>
                          <select name="booking_rent" id="booking_rent" class="form-control input-sm bs-select" required>
                            <option value="" disabled>Select Charge Type</option>
                            <option selected value="b_room_charge" >Base Room Rent</option>
                            <option value="w_room_charge" >Non AC Room Rent</option>
                            <option value="s_room_charge" >Seasonal Room Rent</option>
                          </select>
                        </div>
                      </div>
					  
                      <?php $meal_plan = $this->bookings_model->get_meal_plan(); ?>
                      <div class="col-xs-3">
                        <div class="form-group" id="mp">
                          <label>Meal Plan</label>
                          <select name="plan_id" id="plan_id" onchange="get_rmCh(taxCal)" class="form-control input-sm bs-select" required >
                            <option value="" disabled>Select meal plan *</option>
                            <?php 
                            if(isset($meal_plan) && $meal_plan){	
                             foreach ($meal_plan as $meal_plan) {
                            ?>
								<option  <?php if($meal_plan->name == 'No Plan') echo 'selected'; ?> value="<?php echo $meal_plan->hotel_meal_plan_cat_id; ?>"><?php echo $meal_plan->name; ?></option>
                            <?php
                            }}
                            ?>
                          </select>
                        </div>
                      </div>				  
                      <div class="col-xs-3">
                        <div class="form-group">
                          <label>No. of Adult: <span class="required"> * </span> </label>
                          <input onblur="check_occupancy();get_season()" id="no_of_adult" type="number" min="0" max="15" required  class="form-control input-sm" name="adult" value=""  />
                        </div>
                      </div>
                      <div class="col-xs-3">
                        <div class="form-group">
                          <label>No. of Child:<span class="required"> * </span> </label>
                          <input onblur="check_occupancy();get_season()" id="no_of_child" type="number" min="0" max="15" required class="form-control input-sm" name="child" value="" />
                        </div>
                      </div>
                      <input type="hidden" id="occupancy" value="<?php echo $room_occupancy; ?>"/>
                      <input type="hidden" id="room_bed" value="<?php echo $room_bed; ?>"/>
                      
                      <div id="extra_charge" style="display: none;">
                        <div class="col-xs-3" >
                          <div class="form-group">
                            <label>Extra Charge Type:</label>
                            <div class="radio-list re-ac">
                              <label class="radio-inline add1">
                                <input class="addition3" type="radio" name="charge_mode_type" id="charge_mode_type" value="adult" onclick="set_extra_charge(this.value)" style="display:none;">
                                <i class="fa fa-male"></i> Adult</label>
                              <label class="radio-inline sub1">
                                <input class="addition4" type="radio" name="charge_mode_type" id="charge_mode_type" value="child" onclick="set_extra_charge(this.value)" style="display:none;">
                                <i class="fa fa-child"></i> Child</label>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-3" id="extra_charge_modifier" style="display: none;">
                          <div class="form-group">
                            <label>Extra Charge:</label>
                            <input onblur="charge_modifier(this.value)" readonly type="text" class="form-control input-sm" id="extra_charge_amount" name="extra_charge_amount"   value="0"/>
                          </div>
                        </div>
                      </div>
                      <div id="rrMod">
                        <div class="col-xs-3">
                          <div class="form-group">
                            <label>Rent modifier<span class="required"></span></label>
                            <div class="radio-list re-modi">
                              <label id="lbl" class="checkbox-inline perc" style="padding:0">
                                <input class="addition33" type="checkbox" id="perc" name="perc" value="percent" style="display:none;">
                                <i class="fa fa-percent"></i></label>
                              <label class="radio-inline add" style="padding:0">
                                <input class="addition1" type="radio" name="rent_mode_type" id="rent_mode_type" value="add" style="display:none;">
                                <i class="fa fa-plus-square"></i></label>
                              <label class="radio-inline sub" style="padding:0; margin-left:7px;">
                                <input class="addition2" type="radio" name="rent_mode_type" id="rent_mode_type" value="substract" style="display:none;">
                                <i class="fa fa-minus-square"></i></label>
                              <input type="hidden" id="perc1" name="perc1" value="0">
                              <input type="hidden" id="modf_rr" name="modf_rr" value="0">
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-3" style="display:none;" id="dp">
                          <div class="form-group" id="spn">
                            <label>Modifier Amount <span class="required"> * </span></label>
                            <input type="number"  class="form-control input-sm" min="0" id="mod_room_rent" name="mod_room_rent" value="" onblur="amount_check(this.value)" />
                            <span class="error-val" style="display:none;">Max value 100</span> </div>
                        </div>
                      </div>
                      <!-- End rrMod Div -->
					   <!-- Ajax loader -->
                      
                      <div class="col-xs-3">
                        <div class="form-group">
                          <label>Meal modifier<span class="required"></span></label>
                          <div class="radio-list re-modi">
                            <label id="lbl1" class="checkbox-inline perc" style="padding:0">
                              <input class="addition34" type="checkbox" id="mperc" name="mperc" value="percent" style="display:none;">
                              <i class="fa fa-percent"></i></label>
                            <label class="radio-inline add11" style="padding:0">
                              <input class="addition11" type="radio" name="m_rent_mode_type" id="m_rent_mode_type" value="add" style="display:none;">
                              <i class="fa fa-plus-square"></i></label>
                            <label class="radio-inline sub11" style="padding:0; margin-left:7px;">
                              <input class="addition22" type="radio" name="m_rent_mode_type" id="m_rent_mode_type" value="substract" style="display:none;">
                              <i class="fa fa-minus-square"></i></label>
                            <input type="hidden" id="m_perc1" name="m_perc1" value="0">
                            <input type="hidden" id="modf_mp" name="modf_mp" value="0">
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-3" style="display:none;" id="dp1">
                        <div class="form-group" id="spn">
                          <label>Meal Mod Amt <span class="required"> * </span></label>
                          <input type="number"  class="form-control input-sm" min="0" id="mod_meal_rent" name="mod_meal_rent" value="" onblur="m_amount_check(this.value)" />
                          <span class="error-val" style="display:none;">Max value is 100</span> </div>
                      </div>
                      <div id="charges" class="clearfix" style="margin-bottom: 15px;">
                        <div class="col-xs-12" style="font-size:12px;">
                          <div class="row">
                            <div class="col-xs-4">
                              <label> Room Rent (RR): </label>
                              <label><i class="fa fa-inr" aria-hidden="true"></i> <span id="target"></span></label>
                            </div>
							 <div class="col-xs-4">
                              <label> ExBed RR: </label>
                              <label><i class="fa fa-inr" aria-hidden="true"></i> <span id="target_ex"></span></label>
                            </div>
                            <div class="col-xs-4">
                              <label> RR Tax: </label>
                              <label><i class="fa fa-inr" aria-hidden="true"></i> <span id="target_tr"></span></label>
							  <input type="hidden" value="0" id="rr_total_tax">
							  <input type="hidden" value="0" id="mp_total_tax">
                            </div>
                            <input type="hidden" id="dumb_rate" value="0"/>
                   			<input type="hidden" class="form-control input-sm" id="base_room_rent1" name="base_room_rent1"  value="0"/>
							<input type="hidden" class="form-control input-sm" id="chk_mod" value="0"/>
                            <input type="hidden" class="form-control input-sm" id="room_id" name="room_id"  value="<?php echo $room_id; ?>"/>
                            <input type="hidden" class="form-control input-sm" id="diff" name="stay_span"  value=""/>
                            <div style="display:none" class="col-xs-4">
                              <label> Extra Person RR Tax: </label>
                              <label><i class="fa fa-inr" aria-hidden="true"></i> <span id="target_extx"></span></label>
                            </div>
                            <div class="col-xs-4">
                              <label> Meal Plan (MP): </label>
                              <label><i class="fa fa-inr" aria-hidden="true"></i> <span id="target_mp"></span></label>
                            </div>
							<div class="col-xs-4">
                              <label> ExBed MP: </label>
                              <label><i class="fa fa-inr" aria-hidden="true"></i> <span id="target_exmp"></span></label>
                            </div>
                            <div class="col-xs-4">
                              <label> MP Tax: </label>
                              <label><i class="fa fa-inr" aria-hidden="true"></i> <span id="target_tm"></span></label>
                            </div> 
                            <div style="display:none" class="col-xs-4">
                              <label> ExBed MP Tax: </label>
                              <label><i class="fa fa-inr" aria-hidden="true"></i> <span id="target_exmptx"></span></label>
                            </div>
						  
						<div class="col-xs-12" style="border-top: 1px solid #E2E2E2; padding: 5px 0 4px;margin-top: 5px;">
					      
							<div class="col-xs-4" style="color:#607D8B;">
                              <label style="font-weight:600;"> Total RR: </label>
                              <label style="font-weight:600;"><i class="fa fa-inr" aria-hidden="true"></i> <span id="target_trrT"></span></label>
                            </div> 
							<div class="col-xs-4" style="color:#607D8B;">
                              <label style="font-weight:600;"> Total MP: </label>
                              <label style="font-weight:600;"><i class="fa fa-inr" aria-hidden="true"></i> <span id="target_tmpT"></span></label>
                            </div> 
							<div class="col-xs-4" style="color:#607D8B;">
                              <label style="font-weight:600;"> Total Reservation Charge: </label>
                              <label style="font-weight:600;"><i class="fa fa-inr" aria-hidden="true"></i> <span id="target_tT"></span></label>
                            </div> 
                          
                        </div> 
						</div>
                        </div>
                          
                      </div>
                      <!-- End of #charges div --> 
                      
                      <!-- Meal Modifier was here -->
                      
                      <input type="hidden" class="form-control input-sm" id="base_room_rent" name="base_room_rent"  value="0"/>
					  <input type="hidden" class="form-control input-sm" id="unit_room_rent" name="unit_room_rent"  value="0"/>
                      <input type="hidden" class="form-control input-sm" id="ex_adult_r" name="ex_adult_r"  value="0"/>
                      <input type="hidden" class="form-control input-sm" id="ex_child_r" name="ex_child_r"  value="0"/>
                      <input type="hidden" class="form-control input-sm" id="ex_u_price" name="ex_u_price"  value="0"/>
                      <input type="hidden" class="form-control input-sm" id="ex_u_mprice" name="ex_u_mprice"  value="0"/>
                      <input type="hidden" class="form-control input-sm" id="ex_adult_m" name="ex_adult_m"  value="0"/>
                      <input type="hidden" class="form-control input-sm" id="ex_child_m" name="ex_child_m"  value="0"/>
                      <input type="hidden" id="price" name="plan_price" class="form-control input-sm" value="0"/>
                      <input type="hidden" id="price2" name="plan_tprice" class="form-control input-sm" value="0"/>
                      <input type="hidden" id="price1" name="plan_price1" class="form-control input-sm" value="0"/>
                      <div class="col-xs-12">
                        <div class="form-group">
                          <label>Comment for booking</label>
                          <textarea class="form-control" rows="3" name="comment"></textarea>
                        </div>
                      </div>
                    </div>
                    
					 
                  </div>
                  <div class="form-actions right" style="position:absolute; bottom:0; width: 100%;">
                    <input type="hidden" name="booking_1st" id="booking_1st" value="" />
					<input type="hidden" name="booking_2nd" id="booking_2nd" value="" />
                    <a href="javascript:void(0);" onclick="prev1();" class="btn btn-default">Previous </a>
                    <input name="Submit2" id="submit2" onclick="check_sum3('sum2')"  type="submit" class="btn submit popovers" value="Continue" data-container="body" data-trigger="hover" data-placement="top" data-content="Please check all the entered data before proceeding" data-original-title="Check Carefully" aria-describedby="popover692586"/>
                    <a href="javascript:close();" class="btn pink pull-left">Cancel </a> 
                  </div>
                </form>
              </div>
			  
			  

              <div class="frm3 form" id="tab3" style="display:none; min-height: 514px; background:#F1F3F2; border:1px solid #e5e5e5; position:relative;">
                <form action="" id="form3rd" method="POST">
                  <div class="form-body" style="padding-bottom:86px;">
                    <div class="row">
                      <div class="col-xs-6">
                        <div class="form-group" id="broker_name">
                          <label>Agent/OTA<span class="required"> * </span> </label>
                         <?php 
                           ?> 
							<select name="broker_id" id="broker_id" class="form-control" onchange="get_commision()" >
						 
						 
                          </select>
                        </div>
						
                      </div>
					 
                      <div class="col-xs-6">
                        <div class="form-group" id="broker_commision">
                          <label>Commision <span class="required"> * </span> </label>
                          <input value="" type="text" class="form-control" name="broker_commission" id="broker_commission" placeholder="Commision" />
                        </div>
                      </div>
					  <div class="col-xs-6">
                <div class="form-group">
                    <select name="commission_applicable" id="commission_applicable" class="form-control" onchange="commision_select(this.value)" >                        
                         <option value="0" selected="selected" name="applicable_for">Commision applicable for</option>
                         <option value="1">Room Rent</option>
                         <option value="2">Room Rent + Food</option>
                         <option value="3">Total Bill </option>
                    </select>                	
                     
                </div>
            </div>
			<input type="hidden" name="commision_tot" id="commision_tot">
			<div class="col-xs-6">
			<span>Total Commision:</span><span id="tcom"></span>
			</div><input type="hidden" name="tcomV" id="tcomV">
			<input type="hidden" name="applyedAmount" id="applyedAmount">
                    </div>
                  </div>
				  <input type="hidden" name="set_agent_ota" id="set_agent_ota">
                  <div class="form-actions right" style="position:absolute; bottom:0; width: 100%;">
                    <input type="hidden" name="booking_1st12" id="booking_1st12" value="" />
                    <a href="javascript:void(0);" onclick="prev12();" class="btn btn-default">Previous </a>
                    <input name="Submit3" id="submit3" type="submit" class="btn submit" value="Continue" />
                    <a href="javascript:close();" class="btn pink pull-left"> Cancel </a> 
                  </div>
                </form>
              </div>
              <div class="frm4 form" id="tab4" style="display:none; min-height: 494px; background:#F1F3F2; border:1px solid #e5e5e5; position:relative;">
                <form action="" id="form4th" method="POST">
                  <div class="form-body" style="padding-bottom:67px;">
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
							<!-- newly added unit id for kit assign..-->
							<input type="hidden" class="form-control input-sm" id="room_uid_kit" name="room_uid_kit"  value="<?php echo $room_uid; ?>"/>
							
							<!--- end -->
                          
                          <select name="booking_tax" id="booking_tax" required class="form-control input-sm bs-select" onchange="noTax(this.value)" ><!--onchange="check_sum3(this.value)">-->
                            <option value="" disabled selected>Tax Type</option>
                            <option value="notax" >No Tax</option>
                            <option id="luxury" value="tax"  selected>Tax</option>
                          </select>
						  <input type="hidden"  name="tax_track" id="tax_track" value="1">
                          <input type="hidden" id="booking_tax_type" name="booking_tax_type" onchange="check_sum3(this.value)" value="No Tax"/>
						  <input type="hidden" id="booking_tax_types" name="booking_tax_types"  value=""/>
                        <input type="hidden" id="booking_rm_no" name="booking_rm_no" value="<?php echo $room_no; ?>"/>
                        </div>
                      </div>
                      <div class="clearfix" style="margin-bottom:15px;">
                      <div class="col-xs-6">
                        <div class="row">                         
                          <div class="col-xs-6">
                            <label>Total Rent:</label>
                          </div>
                          <div class="col-xs-6">
                            <label  style="color:#008F6C;" id="target2" > </label>
                          </div>
                     	</div>
                      </div>
                      <div class="col-xs-6">
                        <div class="row">
                          <div class="col-xs-6">
                            <label>Meal Price:</label>
                          </div>
                          <div class="col-xs-6">
                            <label style="color:#008F6C;" id="m_target2" > </label>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="row">
                          <div class="col-xs-6">
                            <label>Extra Person Room Rent:</label>
                          </div>
                          <div class="col-xs-6">
                            <label style="color:#008F6C;" id="ex_target2" > </label>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="row">
                          <div class="col-xs-6">
                            <label>Extra Person Meal Price:</label>
                          </div>
                          <div class="col-xs-6">
                            <label style="color:#008F6C;" id="m_targetEx" > </label>
                          </div>
                        </div>
                      </div>
					  <div class="col-xs-6">
                        <div class="row">
                          <div class="col-xs-12">
                            <h5 style="font-size: 12px; color: #a75050;">Tax Details <strong>(Reservation)</strong></h5>
                          </div>
                          <div id="tax_details_area_bk">
                            
                          </div>
                          <div class="col-xs-6">
                            <label> Room Rent Tax: </label>
                              <label><i class="fa fa-inr" aria-hidden="true"></i> <span id="target_tr"></span></label>
                          </div>
                          <div class="col-xs-6">
                            <label id="target5" style="display:block; color:#DC166A;"></label>
                          </div>
                          <div style="display:none" class="col-xs-6">
                            <label> Extra Room Rent Tax: </label>
                              <label><i class="fa fa-inr" aria-hidden="true"></i> <span id="ext_target_tr1"></span></label>
                          </div>
                          <div style="display:none" class="col-xs-6">
                            <label id="ext_target_tr" style="display:block; color:#DC166A;"></label>
                          </div>
                        </div>
                      </div>
					  </hr>
					  <div class="col-xs-6">
                        <div class="row">
                          <div class="col-xs-12">
                            <h5 style="font-size: 12px; color: #a75050;">Tax Details <strong>(Meal)</strong></h5>
                          </div>
                          <div id="tax_details_area_meal">
                            
                          </div>
                          <div class="col-xs-6">
                            <label> Meal Tax :</label>
                          </div>
                          <div class="col-xs-6">
                            <label id="total_m_tax" style="display:block; color:#DC166A;"></label>
                          </div>
                          <div style="display:none" class="col-xs-6">
                            <label> Extra Meal Tax :</label>
                          </div>
                          <div style="display:none" class="col-xs-6">
                            <label id="ext_total_m_tax" style="display:block; color:#DC166A;"></label>
                          </div> 
                        </div>
                      </div>
                      <div class="col-xs-12">
                        <div style="border-top: 1px solid #3d822e; padding: 5px 0 4px;margin-top: 5px;">
                        	<div class="row">                         	
							  <div class="col-xs-6">
								<label style="font-weight:bold;">Sum Total: INR </label>
							  </div>
							  <div class="col-xs-6">
								<label style="font-weight:bold;" id="target3"></label>
							  </div>
							</div>
                        </div>
                      </div>
                      </div>
                      <input type="hidden" id="tax" name="tax"/>
                      <input type="hidden" id="total" name="total"/>
                      <input type="hidden" id="ex_per_price" name="ex_per_price"/>
                      <input type="hidden" id="ex_per_tax" name="ex_per_tax"/>
                      <input type="hidden" id="ex_per_m_price" name="ex_per_m_price"/>
                      <input type="hidden" id="ex_per_m_tax" name="ex_per_m_tax"/>
                      <input type="hidden" id="total_m_tax1" name="plan_m_tax" class="form-control input-sm"/>
					  <input type="hidden" id="extra_ch_no" name="extra_ch_no" value="" />
                      <input type="hidden" id="extra_ad_no" name="extra_ad_no" value="" />
                      <div class="col-xs-12 form-group">
                        <a onclick= "bookingChargeLineItem();" class="btn submit lst" id="hide-btn" style="margin-top:0px;float: right;margin-right: 10px display:block;"> Submit </a> </div>
                      <div class="col-xs-3" style="display:none;" id="paymm">
                        <div class="form-group">
                          <label>Payment Mode</label>
                          <select name="t_payment_mode" id="t_payment_mode" class="form-control input-sm bs-select" placeholder="Booking Type" onchange="payment_mode_change();" onclick="paymentamount_show">
                            <option value="Select The Payment Mode" disabled selected>Select The Payment Mode</option>
                           
                            <?php $mop = $this->dashboard_model->get_payment_mode_list();
                  if($mop != false)
                  {
                      foreach($mop as $mp)
                      {
                    ?>
                            <option value="<?php echo $mp->p_mode_name; ?>" ><?php echo $mp->p_mode_des; ?></option>
                            <?php }
                  } ?>
                            <!--<option value="Bill To Company">Bill To Company</option>-->
                          </select>
                        </div>
                      </div>
                      <div id="payment_amnt" style="display:none;" >
                        <div class="form-group col-xs-3">
                          <label>Profit Center <span class="required"> * </span> </label>
                          <select name="p_center" class="form-control input-sm bs-select" id="" required>
                            <?php $pc=$this->dashboard_model->all_pc();?>
                            <?php
                              $defProfit=$this->unit_class_model->profit_center_default(); if(isset($defProfit) && $defProfit){ $defPro=$defProfit->profit_center_location;}else{$defPro="Select";}
                              ?>
                            <option value="<?php echo $defPro;  ?>"selected><?php echo $defPro; ?></option>
                            <?php $pc=$this->dashboard_model->all_pc1();
                                foreach($pc as $prfit_center){
                                ?>
                            <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
                            <?php }?>
                          </select>
                        </div>
                        <div class="form-group col-xs-3">
                          <label>Payment Amount <span class="required"> * </span> </label>
                          <input type="text" id="payment_input" onkeypress="return onlyNos(event, this);" required value=""  class="form-control hlf" name="t_amount" placeholder= "Payment Amount " autocomplete="off" />
                          <span class="help-block"> </span> </div>
                      </div>
                      <div id="bank">
                        <div class="form-group col-xs-3">
                          <label>Bank Name <span class="required"> * </span> </label>
                          <input type="text"  required="required" onkeypress="return onlyLtrs();" class="form-control" name="t_bank_name" placeholder="Bank Name" />
                          <span class="help-block"> </span> </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-actions" style="position:absolute; bottom:0; width: 100%;">
                    <input type="hidden" name="booking_3rd" id="booking_3rd" value="" />
                    <input name="Submit4" id="submit4" type="submit" class="btn submit" value="Submit"  onclick="show_hiden()" style="display:none;"/>
                  </div>
                </form>
                <div style="position: absolute; bottom: 18px; right: 12px;">
                  
				  <form action="<?php echo base_url();?>bookings/popup_close" class="form-horizontal" id="f" method="POST">
                    <div class="new-btn" id="print_tab">
                      <input type="hidden" name="booking_3rd" id="booking_3rd" value="" />
                      <div id="hidden-div" style="display:none;"> 
					  
								   <?php $invoice = 1;
										if($invoice == 1) {
									?>
									<a onclick="return folio_generate();" class="btn purplene pull-right" id="add_submit1">Generate Invoice</a>
									<?php } ?>	
								  
					  <a class="btn green" id="dwn_link"  href=""  onclick="download_pdf(); "> Download <i class="glyphicon glyphicon-download"></i> </a>
					  <a onclick="openview();" class="btn yellow"> View <i class="fa fa-eye"></i></a>
					  <a onclick= "openview2();" class="btn submit lst" > Submit </a> </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" name="" id="response">
<input type="hidden" name="" id="BookingResponse">
<input type="hidden" name="" id="mealResponse">
<!--[if lt IE 9]>
<script src="<?php echo base_url();?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]--> 
<!-- BEGIN CORE PLUGINS --> 
<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN PAGE LEVEL PLUGINS --> 
<script src="<?php echo base_url();?>assets/global/plugins/moment.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/typeahead1/jquery.typeahead.js"></script>
<!-- END PAGE LEVEL PLUGINS --> 
<!-- BEGIN THEME GLOBAL SCRIPTS --> 
<script src="<?php echo base_url();?>assets/global/scripts/app.min.js" type="text/javascript"></script> 
<!-- END THEME GLOBAL SCRIPTS --> 
<!-- BEGIN PAGE LEVEL SCRIPTS --> 
<script src="<?php echo base_url();?>assets/pages/scripts/components-select2.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/pages/scripts/ui-sweetalert.min.js" type="text/javascript"></script> 
<!-- END PAGE LEVEL SCRIPTS --> 
<!-- BEGIN THEME LAYOUT SCRIPTS --> 
<script src="<?php echo base_url();?>assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script> 
<!-- END THEME LAYOUT SCRIPTS --> 

<script type="text/javascript">

function checkintime(){
		
		let stDate = $("#start_dt").val();
		let endDate = $("#end_dt").val();
		let chkintime=$('#checkin_id').val();
		let def_chkintime=$('#def_checkin_time').val();
		let ctime=chkintime.split(/:/);
		let chk_time=ctime[0]*3600+ctime[1]*60;
		let dctime=def_chkintime.split(/:/);
		let chkd_time=dctime[0]*3600+dctime[1]*60;

		//console.log(chk_time+' chkintime '+(chk_time - chkd_time)/3600);
		if(chk_time < chkd_time){
			//swal("check checkin time");
			//$('#checkin_id').val(def_chkintime);
			return(chk_time - chkd_time);
		}
}

function checkouttime(){
	
		let chkouttime=$('#end_time').val();
		let def_chkouttime=$('#def_checkout_time').val();
		let ctime=chkouttime.split(/:/);
		let chk_time=ctime[0]*3600+ctime[1]*60;
		let dctime=def_chkouttime.split(/:/);
		let chkd_time=dctime[0]*3600+dctime[1]*60;
		
		if ( chk_time > chkd_time){
			swal("check checkout time");
			//$('#end_time').val(def_chkouttime);
		}
}	
$("label.sub1").addClass("active");
window.expType = 'c';
  var bk_id = $('#booking_3rd').val();
    function close(result) {
        if (parent && parent.DayPilot && parent.DayPilot.ModalStatic) {
            parent.DayPilot.ModalStatic.close(result);
        }
    }

    
    $(document).ready(function() {
		
		
		var guestevent = 0;
		var r_no = '';
		 var myguest_name ='';
		 var myguest_address = '';
		 var myguest_mail = '';
		 var myguest_mobile = '';
		 var old_guest_details  ='';
		 var guestId ='';
		 var guest_check_in_date = '';
		 var guest_check_out_date = '';
		 var guest_checkin_time = '';
		 var guest_checkout_time = '';
		 var end = '';
		 var std ='';
        //$('#tab11').hide();
       // $('#tab_child').hide();
        $('#tab12').show();
        $('#tab2').hide();
        $('#tab3').hide();
        $('#tab4').hide();
        //$('#broker_name').hide();
        //$('#broker_commision').hide();

		 
		 var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);		
		$(".date-picker" ).datepicker({
    
		orientation: "left",
		startDate: today,
		autoclose: true,
		todayHighlight: true
		//dateFormat : 'dd-mm-yyyy'
		
   
		});
		 
		 
		 
        $('#bank').hide();
        $('#istli').addClass("active");


        $("#form1st").validate({
			
			
              submitHandler: function() {
				
				chkD = <?php echo $chkD;?>;
				std = $('#start_dt').val();
				end = $('#end_dt').val();
				var rmid = $('#room_id').val();
				var bking_1st = $('#booking_1st').val();
				myguest_name = $('#cust_name').val();
				myguest_address = $('#cust_address').val();
				myguest_mail = $('#cust_mail').val();
				myguest_mobile = $('#cust_contact_no').val();
				guest_checkin_time = $('#checkin_id').val();
				guest_checkout_time = $('#end_time').val();
				guestId = $('#id_guest2').val();
				
				//console.log('$("#form1st").validate => '+std+', '+end+', '+rmid);
				
				if(bking_1st == null || bking_1st == ''){
					
				$.ajax({
			
				type:"POST",
                url:"<?php echo base_url(); ?>setting/check_avail_room_final",
                datatype:'json',
				async:false,
                data:{std:std,end:end,rmid:rmid},
                success:function(data){ 
			 
					//alert(data);
					console.log("====>"+data);
					//alert("booking_id "+data.booking_id);
					
					if(data){
						
						 $("#start_dt").css('color', 'red');
						 $("#start_dt").css('border', '1px solid red');
						 $("#end_dt").css('color', 'red');
						 $("#end_dt").css('border', '1px solid red');
						 //console.log('insNB' +data);
						// alert('Already exists');
						 swal('Already Booking Exists Between this daterange'+ std +'to ' +end);					
						 return false;
						
					}
					else{
						
								//r_no=<?php echo $room_no; ?>;
								r_no=$("#room_noh").val();
								
								//console.log('else of $("#form1st").validate '+r_no);

								$.post("<?php echo base_url();?>bookings/hotel_backend_create",
								$("#form1st").serialize(),
								function(data){

									//console.log('stDate'+$('#start_dt').val());
									old_guest_details = {
										guest_name : $('#cust_name').val(),
										guest_mob :$('#cust_contact_no').val(),
										guest_address :$('#cust_address').val(),
										guest_mail :$('#cust_mail').val(),
										guest_start_dt:std,
										guest_end_dt:$('#end_dt').val(),
										guest_checkin_time:$('#checkin_id').val(),
										guest_checkout_time:$('#end_time').val()
									};
									
									
									$('#booking_1st').val(data.bookings_id);
									$('#id_guest2').val(data.guest_id);
									$('#tab12').hide();
									$('#tab2').show();
									$('#istli').removeClass("active");
									$('#2ndli').addClass("active");
									document.getElementById('short_info').innerHTML="<b>Booking Id:</b> HM0<?php echo $this->session->userdata('user_hotel') ?>00"+data.bookings_id+" | <b>Guest Name:</b> "+data.guest_name.substring(0, 15)+" | <b>Room No:</b> "+r_no;
									window.parent.document.getElementById('txd').value=$('#booking_1st').val();
									
									//console.log('guestDetails=>' +old_guest_details["guest_name"]);
							   });
							return false;
						
					}
					
                },
			
		});// end of ajax
				
	    }// end of booking id checking..

		 else{
			 
									
						if((myguest_name != old_guest_details["guest_name"] || myguest_address != old_guest_details["guest_address"] || myguest_mobile != old_guest_details["guest_mob"] || myguest_mail != old_guest_details["guest_mail"]) && (std == old_guest_details["guest_start_dt"] && end == old_guest_details["guest_end_dt"] && guest_checkin_time == old_guest_details["guest_checkin_time"] && guest_checkout_time == old_guest_details["guest_checkout_time"] )){
										
										//console.log('guest_start_dt=>' +old_guest_details["guest_start_dt"]);
										//console.log('guest_end_dt=>' +old_guest_details["guest_end_dt"]);

										$.ajax({
											type:"POST",
											url:"<?php echo base_url(); ?>setting/update_guest_details_booking",
											async:false,
											data:{myguest_name:myguest_name,myguest_address:myguest_address,myguest_mobile:myguest_mobile,myguest_mail:myguest_mail,bookingid:$('#booking_1st').val(),guestId:guestId},
											
											success:function(data){ 

													////console.log('daaa' +data);
													if(data.succes_result == "success"){
													
													//toastr["success"]("successfully guest details updated");
													$('#tab12').hide();
													$('#tab2').show();
													$('#istli').removeClass("active");
													$('#2ndli').addClass("active");
													document.getElementById('short_info').innerHTML="<b>Booking Id:</b> HM0<?php echo $this->session->userdata('user_hotel') ?>00"+$('#booking_1st').val()+" | <b>Guest Name:</b> "+myguest_name.substring(0, 15)+" | <b>Room No:</b> "+r_no;
													window.parent.document.getElementById('txd').value=$('#booking_1st').val();
													
													old_guest_details = {
										
														guest_name : $('#cust_name').val(),
														guest_mob :$('#cust_contact_no').val(),
														guest_address :$('#cust_address').val(),
														guest_mail :$('#cust_mail').val(),
														guest_start_dt:$('#start_dt').val(),
														guest_end_dt:$('#end_dt').val(),
														guest_checkin_time:$('#checkin_id').val(),
														guest_checkout_time:$('#end_time').val()
										
													};
													return false;
												}
												else{
													//toastr["warning"]('Not updated guest table');
													return false;
												}
											}// end of success
										});				
						
									}// chek if guest details same...
									
									else if((std != old_guest_details["guest_start_dt"] || end != old_guest_details["guest_end_dt"] || guest_checkin_time != old_guest_details["guest_checkin_time"] || guest_checkout_time != old_guest_details["guest_checkout_time"]) && (myguest_name == old_guest_details["guest_name"] && myguest_address == old_guest_details["guest_address"] && myguest_mobile == old_guest_details["guest_mob"] && myguest_mail == old_guest_details["guest_mail"])){
										
										
											
											$.ajax({
			
													type:"POST",
													url:"<?php echo base_url(); ?>setting/check_avail_room",
													datatype:'json',
													async:false,
													data:{std:std,end:end,rmid:rmid,singleid:$('#booking_1st').val()},
													success:function(data){ 
												 
														//alert(data);
														console.log(data);
														//alert("booking_id "+data.booking_id);
														
														if(data){
															
															 $("#start_dt").css('color', 'red');
															 $("#start_dt").css('border', '1px solid red');
															 $("#end_dt").css('color', 'red');
															 $("#end_dt").css('border', '1px solid red');
															 //console.log('insNB' +data);
															// alert('Already exists');
															 swal('Already Booking Exists Between this daterange'+ std +'to ' +end);					
															 return false;
															
														}
														else{
															
																  r_no=<?php echo $room_no; ?>;
																	$('#loading-image6').show();
											$.ajax({
											
											type:"POST",
											url:"<?php echo base_url(); ?>setting/update_guest_details_booking2",
											async:false,
											data:{myguest_name:myguest_name,myguest_address:myguest_address,myguest_mobile:myguest_mobile,myguest_mail:myguest_mail,bookingid:$('#booking_1st').val(),guestId:guestId,std:std,end:end,guest_checkout_time:guest_checkout_time,guest_checkin_time:guest_checkin_time},
											
											
											success:function(data){ 
										 
												//	alert('middle conditions');
												//	 alert(data);
													 //console.log('daaa' +data);
													if(data.succes_result == "success"){
													//toastr["success"]('successfully booking date & time updated');
													//$('#loading-image6').show();
													setTimeout(function(){
														
														$('#tab12').hide();
														$('#tab2').show();
														$('#istli').removeClass("active");
														$('#2ndli').addClass("active");
														document.getElementById('short_info').innerHTML="<b>Booking Id:</b> HM0<?php echo $this->session->userdata('user_hotel') ?>00"+$('#booking_1st').val()+" | <b>Guest Name:</b> "+myguest_name.substring(0, 15)+" | <b>Room No:</b> "+r_no;
														window.parent.document.getElementById('txd').value=$('#booking_1st').val();
													
														old_guest_details = {
										
														guest_name : $('#cust_name').val(),
														guest_mob :$('#cust_contact_no').val(),
														guest_address :$('#cust_address').val(),
														guest_mail :$('#cust_mail').val(),
														guest_start_dt:$('#start_dt').val(),
														guest_end_dt:$('#end_dt').val(),
														guest_checkin_time:$('#checkin_id').val(),
														guest_checkout_time:$('#end_time').val()
										
														
													};
													$('#loading-image6').hide();
													
													},500);
													
													return false;
													
												}
												else{
													
													$('#loading-image').hide();
													//toastr["warning"]('Not updated guest table');
													return false;
												}
											}// end of success
										});
																return false;
															
														}
														
													},
												
											});// end of ajax
										
										
										
									}//end of else if for only date time update...
									
									else if((std != old_guest_details["guest_start_dt"] || end != old_guest_details["guest_end_dt"] || guest_checkin_time != old_guest_details["guest_checkin_time"] || guest_checkout_time != old_guest_details["guest_checkout_time"]) && (myguest_name != old_guest_details["guest_name"] || myguest_address != old_guest_details["guest_address"] || myguest_mobile != old_guest_details["guest_mob"] || myguest_mail != old_guest_details["guest_mail"])){
										
									//	swal('Inside combine condition');
										
										
										$.ajax({
			
													type:"POST",
													url:"<?php echo base_url(); ?>setting/check_avail_room",
													datatype:'json',
													async:false,
													data:{std:std,end:end,rmid:rmid,singleid:$('#booking_1st').val()},
													success:function(data){ 
												 
														//alert(data);
														console.log(data);
														//alert("booking_id "+data.booking_id);
														
														if(data){
															
															 $("#start_dt").css('color', 'red');
															 $("#start_dt").css('border', '1px solid red');
															 $("#end_dt").css('color', 'red');
															 $("#end_dt").css('border', '1px solid red');
															 //console.log('insNB' +data);
															// alert('Already exists');
															 swal('Already Booking Exists Between this daterange'+ std +'to ' +end);					
															 return false;
															
														}
														else{
															
																  r_no=<?php echo $room_no; ?>;
																	$('#loading-image6').show();
											$.ajax({
											
											type:"POST",
											url:"<?php echo base_url(); ?>setting/update_guest_details_booking3",
											//datatype:'json',
											async:false,
											data:{myguest_name:myguest_name,myguest_address:myguest_address,myguest_mobile:myguest_mobile,myguest_mail:myguest_mail,bookingid:$('#booking_1st').val(),guestId:guestId,std:std,end:end,guest_checkout_time:guest_checkout_time,guest_checkin_time:guest_checkin_time},
											
											
											success:function(data){ 
										 
												//	alert('middle conditions');
												//	 alert(data);
													 //console.log('daaa' +data);
													if(data.succes_result == "success"){
													swal('successfully guest details and booking date & time updated');
													//$('#loading-image6').show();
													setTimeout(function(){
														
														$('#tab12').hide();
														$('#tab2').show();
														$('#istli').removeClass("active");
														$('#2ndli').addClass("active");
														document.getElementById('short_info').innerHTML="<b>Booking Id:</b> HM0<?php echo $this->session->userdata('user_hotel') ?>00"+$('#booking_1st').val()+" | <b>Guest Name:</b> "+myguest_name.substring(0, 15)+" | <b>Room No:</b> "+r_no;
														window.parent.document.getElementById('txd').value=$('#booking_1st').val();
													
														old_guest_details = {
										
														guest_name : $('#cust_name').val(),
														guest_mob :$('#cust_contact_no').val(),
														guest_address :$('#cust_address').val(),
														guest_mail :$('#cust_mail').val(),
														guest_start_dt:$('#start_dt').val(),
														guest_end_dt:$('#end_dt').val(),
														guest_checkin_time:$('#checkin_id').val(),
														guest_checkout_time:$('#end_time').val()
										
														
													};
													$('#loading-image6').hide();
													
													},500);
													
													return false;
													
												}
												else{
													
													$('#loading-image').hide();
													swal('Not updated guest table');
													return false;
												}
											}// end of success
										});
																return false;
															
														}
														
													},
												
											});// end of ajax
										return false;
										
									}// end of combine else if...
													
									else{
									//	alert(' No update guest');
										
										$.ajax({
											
											type:"POST",
											url:"<?php echo base_url(); ?>setting/update_guest_details_booking",
											datatype:'json',
											async:false,
											data:{myguest_name:myguest_name,myguest_address:myguest_address,myguest_mobile:myguest_mobile,myguest_mail:myguest_mail,bookingid:$('#booking_1st').val(),guestId:guestId},
											success:function(data){ 
										 
													if(data.succes_result == "success"){
													//swal('No Changes Made in Bookings');
													$('#tab12').hide();
													$('#tab2').show();
													$('#istli').removeClass("active");
													$('#2ndli').addClass("active");
													document.getElementById('short_info').innerHTML="<b>Booking Id:</b> HM0<?php echo $this->session->userdata('user_hotel') ?>00"+$('#booking_1st').val()+" | <b>Guest Name:</b> "+myguest_name.substring(0, 15)+" | <b>Room No:</b> "+r_no;
													window.parent.document.getElementById('txd').value=$('#booking_1st').val();
													
													old_guest_details = {
										
														guest_name : $('#cust_name').val(),
														guest_mob :$('#cust_contact_no').val(),
														guest_address :$('#cust_address').val(),
														guest_mail :$('#cust_mail').val(),
														guest_start_dt:$('#start_dt').val(),
														guest_end_dt:$('#end_dt').val(),
														guest_checkin_time:$('#checkin_id').val(),
														guest_checkout_time:$('#end_time').val()
										
													};
													return false;
													
												}
												else{
													
													
													swal('Not updated guest table');
													return false;
												}
											}// end of success
										});
										
									}
									
				 
			 } 
			 

            } //don't let the page refresh on submit.

			
			
			
        });

       
        $("#form2nd").validate({
			//if(){
				//alert("HERE");
			//}
            submitHandler: function() {

                $.post("<?php echo base_url();?>bookings/hotel_backend_create2",
                    $("#form2nd").serialize(),
                    function(data){
						//alert(data.booking_source);
						
						$('#set_agent_ota').val(data.booking_source);
                        var booking_source = data.booking_source;
                        var booking_source_type = data.booking_source_name;
						
						<?php
						//$booking_source_ta_stat=$this->dashboard_model->get_booking_source_ta_stat();
						?>
						//var booking_source_ta_stat=0;
                        if(booking_source_type == 'Agent')
                        {
                            $('#booking_1st12').val(data.bookings_id);
                            $('#brokerchannel').val('0');
                            $('#tab2').hide();
                            $('#tab3').show();
                            $('#print_tab').hide();
                            $('#2ndli').removeClass("active");
							//$('#broker_id').hode();
							//document.getElementById('broker_id').style.display='none';
                            $('#3rdli').addClass("active");
                            return_broker();
                        }
                        else if(booking_source_type == 'OTA'){
                            $('#booking_1st12').val(data.bookings_id);
                            $('#brokerchannel').val('1');
                            $('#tab2').hide();
                            $('#tab3').show();
                            $('#print_tab').hide();
                            $('#2ndli').removeClass("active");
                            $('#3rdli').addClass("active");
                            return_channel();

                        }
                        else
                        {
                            $('#booking_3rd').val(data.bookings_id);
                            $('#tab2').hide();
                            $('#tab4').show();
                            $('#2ndli').removeClass("active");
                            $('#5thli').addClass("active");
                        }

                    });
                return false; //don't let the page refresh on submit.

            }
        });

        $("#form3rd").validate({
            submitHandler: function() {

                var flag=document.getElementById("brokerchannel").value;
                var commis=$('#tcom').text;
                if(flag=='0'){
                    var url='hotel_backend_create_broker';
                }
                else if(flag=='1'){
                    var url='hotel_backend_create_channel';
                }

                var bookings_id = $('#booking_1st12').val();

                $.post("<?php echo base_url();?>bookings/"+url+"?booking_id_broker="+bookings_id,
                    $("#form3rd").serialize(),
                    function(data){
                        $('#booking_3rd').val(data.bookings_id);
                        $('#tab3').hide();
                        $('#tab4').show();
                        $('#3rdli').removeClass("active");
                        $('#5thli').addClass("active");

                    });
                return false;


            }
        });
        $("#form4th").validate({
			//var a=$('#tax_track').val();
			//alert(a);
            submitHandler: function() {
               $.post("<?php echo base_url();?>bookings/hotel_backend_create4",
                    $("#form4th").serialize(),
                    function(data){
                        $('#booking_3rd').val(data.bookings_id);
                        $("#submit4").prop("disabled", true);
                        $('#print_tab').show();
						 window.parent.document.getElementById('txd').value='0';

                    });
                return false; //don't let the page refresh on submit.

            }
        });

    });
	
    function returning_form() {

        $('#tab1').hide();
        $('#tab11').show();

    }
    function child_form() {

        $('#tab1').hide();
        //$('#tab_child').show();

    }
    function showbroker() {

        $('#broker_name').show();
        return_broker();
        $('#broker_commision').show();

    }

    function hidebroker() {

        $('#broker_name').hide();
        $('#broker_commision').hide();

    }
    function new_form()
    {
        $('#tab1').hide();
        $('#tab12').show();
    }

    $("#f").submit(function () {
        var f = $("#f");
        $.post(f.attr("action"), f.serialize(), function (result) {
            close(eval(result));
        });
        return false;
    });

	
    function prev(){
        $('#tab12').hide();
        $('#tab11').show();
    }	

    function prev1(){
        $('#tab2').hide();
        $('#tab12').show();
    }	
	
	
	
    $(document).ready(function () {
        $("#name").focus();
		window.popNewCountSub = 0;
    });

</script> 
<script>
$(".curen").on("click", function(){
	
	if(myflag == '2'){
		$("label.curen").removeClass("active");
		$("label.adven").removeClass("active");
		$("label.temp").removeClass("active");
	} else {
        $("label.curen").addClass("active");
		$("label.adven").removeClass("active");
		$("label.temp").removeClass("active");
	}
});
$(".adven").on("click", function(){
        $("label.adven").addClass("active");
		$("label.temp").removeClass("active");
		$("label.curen").removeClass("active");
});
$(".temp").on("click", function(){
        $("label.temp").addClass("active");
		$("label.curen").removeClass("active");
		$("label.adven").removeClass("active");
});

$(".addition1").on("click", function(){
	    var md = 0;
        document.getElementById("chk_mod").value = md;
        $("label.add").addClass("active");
		$("label.sub").removeClass("active");
		$("#dp").css('display', 'block');

});
$(".addition2").on("click", function(){
	    var md = 0;
        document.getElementById("chk_mod").value = md;
		$("label.sub").addClass("active");
		$("label.add").removeClass("active");
		$("#dp").css('display', 'block');
		
});

$(".addition11").on("click", function(){
	    var md = 0;
        document.getElementById("chk_mod").value = md;
        $("label.add11").addClass("active");
		$("label.sub11").removeClass("active");
		$("#dp1").css('display', 'block');

});
$(".addition22").on("click", function(){
	    var md = 0;
        document.getElementById("chk_mod").value = md;
		$("label.sub11").addClass("active");
		$("label.add11").removeClass("active");
		$("#dp1").css('display', 'block');
		
});





$(".addition3").on("click", function(){

        $("label.add1").addClass("active");
		$("label.sub1").removeClass("active");
		window.expType = 'a';
});

$(".addition4").on("click", function(){

		$("label.sub1").addClass("active");
		$("label.add1").removeClass("active");
		window.expType = 'c';

});

$(".addition33").on("click", function(){

		let rr = $("#base_room_rent1").val();
		let mod = $("#mod_room_rent").val();
		if($('#perc'). prop("checked") == true){
		    $("#lbl").addClass("active");
			$("#perc1").val(1);
			$("#modf_rr").val(mod * rr / 100);
				
		} else {
			$("#lbl").removeClass("active");
			$("#perc1").val(0);
			$("#modf_rr").val(mod);
		}
		

});

$(".addition34").on("click", function(){

		if($('#mperc'). prop("checked") == true){
		    $("#lbl1").addClass("active");
			$("#m_perc1").val('1');				
		}else {
			$("#lbl1").removeClass("active");
			$("#m_perc1").val('0');
		}

});




    function submit_form()
    {

        var f = $("#f");
        $.post(f.attr("action"), f.serialize(), function (result) {
            close(eval(result));
        });
        return false;

    }
   
	function download_pdf() {
        var booking_id = '3524';
  
		  jQuery.ajax({
				  type: "POST",
				  url: "<?php echo base_url(); ?>bookings/folio_generate",
				  dataType: 'json',
				  data: {booking_id:booking_id},
				  success: function(data){
			 
			// alert(data);
					swal({
			title: "Generated Invoice ID - "+ data.invoice_id,
			text: "Your invoice is successfully generated",
			type: "success"
		   },
		   function(){
			// location.reload();
			$('#add_submit').hide();
			$('#dwn_link').attr("disabled",false);
		   });
				  }
		   }); // End Ajax
				$("#dwn_link").attr("href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id);
				var f = $("#f");
				$.post(f.attr("action"), f.serialize(), function (result) {
					close(eval(result));
				});
				return false;
    }

    function payment_mode_change()
    {
        var p = $('#t_payment_mode').val();
        //alert(p);
        if(p=='cash')
        {
            $('#bank').hide();
			
        }
        else
        {
            $('#bank').show();
        }
		
		document.getElementById('payment_amnt').style.display= 'block';
    }

    function openview() {
		
        var booking_id = $('#booking_3rd').val();
		//var booking_id = '3524';
        window.open("<?php echo base_url();?>bookings/invoice_generate?booking_id=" + booking_id);
        //$('#f').submit();
        var f = $("#f");
        $.post(f.attr("action"), f.serialize(), function (result) {
            close(eval(result));
        });
        return false;
    } 

    function openview1(booking_id) {
	//	alert("hello"); return false;
        var total = $('#total').val();
        var booking_id = $('#booking_3rd').val();
		var ex_ad_no = $('#extra_ad_no').val();
		var ex_ch_no = $('#extra_ch_no').val();
		var tax_track = $('#tax_track').val();
		var set_agent_ota = $('#set_agent_ota').val();
		var broker_id = $('#broker_id').val();
		var broker_commission = $('#broker_commission').val();
		var tcomV = $('#tcomV').val();
		var applyedAmount = $('#applyedAmount').val();
		var commission_applicable = $('#commission_applicable').val();
		var status = $("#Bkstatus").val();
		//alert("status "+status); return false;
		
		$.ajax({
				
				type:"POST",
				url: "<?php echo base_url()?>bookings/hotel_backend_create5",
				data:{extra_ad_no:ex_ad_no,extra_ch_no:ex_ch_no,total:total,bookings_id:booking_id,tax_track:tax_track,set_agent_ota:set_agent_ota,broker_id:broker_id,broker_commission:broker_commission,tcomV:tcomV,commission_applicable:commission_applicable,applyedAmount:applyedAmount,status:status},
				success:function(data){
				
					//console.log('bookingId' +data.bookings_id);
					$('#booking_3rd').val(data.bookings_id);
				}
		}); // End Ajax

		window.parent.document.getElementById('txd').value='0';
        var f = $("#f");
		send_Mail_user(booking_id);
        $.post(f.attr("action"), f.serialize(), function (result) {
            close(eval(result));
        });
		
         return false;
    }
	
	function send_Mail_user(booking_id){
		//console.log('mail function ');
		var cust_mail=$('#cust_mail').val();
		
		
		
		
		$.ajax({
									
									type:"POST",
									url: "<?php echo base_url()?>bookings/send_Mail_user",
									data:{mail_id:cust_mail,booking_id:booking_id},
									success:function(data){
									
										//console.log('mail function success');
										//alert("hello");
										
									}
							}); 
		
		
		
		 
	}
	
    function openview2() {
        window.parent.document.getElementById('txd').value='0';
        var f = $("#f");
        $.post(f.attr("action"), f.serialize(), function (result) {

            close(eval(result));
        });
        return false;
    }
	
	function cancel_booking(){
		
		window.parent.document.getElementById('txd').value='0';
		var booking_id = $('#booking_1st').val();
		
		if( booking_id > 0 ){
			
			 $.ajax({
			  url: "<?php echo base_url()?>bookings/cancel_booking",
			  type: "POST",
			  data:{booking_id:booking_id},
			  success: function(data){
				//console.log(data);
			  }        
			  });				
		}
		
		var f = $("#f");
		
        $.post(f.attr("action"), f.serialize(), function (result) {
            close(eval(result));
        });
		
        return false;
	} // End cancel_booking
	
</script> 
<script>
    function onlyNos(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        catch (err) {
           // alert(err.Description);
        }
    }
    /* 11.17.2015*/
    function onlyLtrs(e, t)
    {

        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }
            if ( charCode > 32 && (charCode < 64 &&  charCode < 90)) {
                return false;
            }
            return true;
        }
        catch (err) {
         //   alert(err.Description);
        }
    }

    function return_broker()
    {
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_broker",
                datatype:'json',
                success:function(data)
                {
                    var resultHtml = '';
                    resultHtml+='<option value="">Select Broker</option>'
                    $.each(data, function(key,value){
						//alert(value.b_id+','+value.b_name);
                        resultHtml+='<option value="'+ value.b_id +'">'+ value.b_name +'</option>';
						
                    });
					//alert(resultHtml);
                    $('#broker_id').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
        return false;


    }

    function return_channel()
    {
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_channel",
                datatype:'json',
                success:function(data)
                {
                    var resultHtml = '';
                    resultHtml+='<option value="">Select Channel</option>'
                    $.each(data, function(key,value){

                        resultHtml+='<option value="'+ value.channel_id +'">'+ value.channel_name +'</option>';

                    });
                    $('#broker_id').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
        return false;


    }


    function return_guest_search()
    {
        var guest_name = $('#cust_search').val();
        //alert(guest_name);

        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_search",
                datatype:'json',
                data:{guest:guest_name},
                success:function(data)
                {

                    var resultHtml = '';
                    resultHtml+='<div class="return_guestinn"><table class="table table-striped table-bordered table-hover"><thead><tr><th>Name</th><th>Address</th><th width="100">Mobile No</th></tr></thead><tbody>';
                    $.each(data, function(key,value){
                        resultHtml+='<tr  class="crsr collapsed" data-toggle="collapse" data-target="#toggleDemo'+value.g_id+'">';
                        resultHtml+='<td>'+ value.g_name +'</td>';
                        resultHtml+='<td>'+ value.g_address +'</td>';
                        resultHtml+='<td>'+ value.g_contact_no +'</td>';
                        resultHtml+='</tr><tr>';
                        resultHtml+='<td colspan="3"  cellpadding="0" cellspacing="0" style="padding:0;"><div id="toggleDemo'+value.g_id+'" class="mrgn" style="padding:10px;">';
                        resultHtml += '<div class="clearfix"><div class="col-xs-8 cl-in"><span style="font-size:12px; margin-bottom:6px;"> No of Visits: '+value.visits+' </span>';

                        resultHtml+='<input type="hidden" id="g_id_hide"  value="'+value.g_id+'" ></input>';
                        resultHtml+='<span style="font-size:12px;">Total Transections: <i class="fa fa-inr" style="font-size: 11px;"></i> '+value.amount_spent+'</span><span style="font-size:12px;">Last visit: On '+value.cust_from_date+' at '+value.room_no+'</span><button class="btn blue btn-xs" style="margin-top:5px;" name="g_id" id="g_id"" value="'+value.g_id+'" onclick="get_guest('+value.g_id+')" >Continue</button></div><div class="col-xs-4 sm">';
                        if(value.g_photo_thumb!='') {
                            resultHtml += '<div class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/' + value.g_photo_thumb + '" > ' +
                            '</div>';
                        }
                        else{
                            resultHtml += '<div class="pic pull-right" style="width:100px; height:92px; border:1px solid #DDDDDD;">' +
                            '<img src="<?php echo base_url() ?>upload/guest/no_images.png" > ' +
                            '</div>';
                        }
                        //resultHtml+='<td><input type="radio" name="g_id" id="'+value.g_id+'" value="'+value.g_id+'" onclick="get_guest(this.value)" /></td>';
                        resultHtml+='</div></div></td></tr>';
                    });
                    resultHtml+='</tbody></table></div>';
                    $('#return_guest').html(resultHtml);
                    //alert(data);
                    // console.log(data);
                    //$('#dtls').html(data);
                }
            });
        return false;


    }
	var myflag = '1';
    function check_cur_date()
    {
        var start_dt = '<?php echo date('Y-m-d',strtotime($start_dt)); ?>';
        var current_dt = '<?php echo date('Y-m-d'); ?>';
        if(start_dt != current_dt)
        {
            $("input:radio").attr("checked", false);
            swal('Checkin can only be taken from Current Date');
			myflag = '2';
        }
    }
	function check_temp_date()
    {
        var start_dt = '<?php echo date('Y-m-d',strtotime($start_dt)); ?>';
        var current_dt = '<?php echo date('Y-m-d'); ?>';
        if(start_dt != current_dt)
        {
            $("input:radio").attr("checked", false);
            swal('Temporary Booking can only be taken from Current Date');
        }
    }

    function check_advance_date()
    {
        var start_dt = '<?php echo date('Y-m-d',strtotime($start_dt)); ?>';
        var current_dt = '<?php echo date('Y-m-d'); ?>';
        if(start_dt == current_dt)
        {
            $("input:radio").attr("checked", false);
            swal('Advance Booking can not be taken from Current Date');
        }
    }

    function get_guest(id)
    {
        //var g_id = $('#g_id_hide').val();
        var g_id=id;
        //alert(g_id);
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/return_guest_get",
                datatype:'json',
                data:{guest_id:g_id},
                success:function(data)
                {  //alert(id);
                    $('#tab11').hide();
                    $('#tab12').show();
                    $('#id_guest2').val(id);
                    $('#cust_name').val(data.g_name);
                    $('#cust_address').val(data.g_address);
                    $('#cust_contact_no').val(data.g_contact_no);
					$('#cust_mail').val(data.g_email);
                }
            });
        return false;
    }

    function get_commision()
    { var flag=document.getElementById("brokerchannel").value;
        //alert(flag);
        var b_id = $('#broker_id').val();
        var booking_id = $('#booking_1st12').val();

            if(flag==0){
                var url='get_commision';
            }
        else if(flag==1){
            var url='get_commision2';
        }
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/"+url+"",
                datatype:'json',
                data:{b_id:b_id,booking_id:booking_id},
                success:function(data)
                { //alert(data);	
                   var room_rent = parseInt(data.base_room_rent);
                    var commision = parseInt(data.broker_commission);

                    //var tot_commision = (room_rent * (commision/100));
					$('#commision_tot').val(commision);
					//$('#roomrent_tot').val(room_rent);
					commision=commision+'%';
                    $('#broker_commission').val(commision);
                }
            });


        return false;
    }

</script> 
<script>
    $("#returning").hover(function() {
        //alert("nnn");
        $("#returning").css("background-color", "");
    });
</script> 
<script>



function check_sum2(value2){
		
		var days = diffDays();
		var diff=days;
		var d=parseInt(diff);
		
		document.getElementById("base_room_rent").value=value2;
		document.getElementById("dumb_rate").value = value2;
		document.getElementById("target").innerHTML=value2;
		document.getElementById("target2").innerHTML=value2*d;
		document.getElementById("base_room_rent1").value=value2;
}
	
	
    function amount_check(value){

		// _sb
		var days = diffDays();
		var ttx;
        tax=0;	
		let rr = $("#base_room_rent1").val();
		let mod = $("#mod_room_rent").val();
		if($('#perc'). prop("checked") == true){
		   	$("#modf_rr").val(mod * rr / 100);
				
		} else {
			$("#modf_rr").val(mod);
		}
			 
		taxCal();
		//alert(value);
		var chk = document.getElementById("chk_mod").value;
		var md = value;
		var radio_val = document.getElementsByName('rent_mode_type');
		var base_rent = document.getElementById("base_room_rent1").value;
		
        if ((value >= 0) && (value != '')){
			if ( chk != value ){		
				////console.log('Inside Second if, '+chk+' != '+value);
				var diff=document.getElementById("diff").value;
				var days = diffDays();
				var d=days;
		
			 if($('#perc'). prop("checked") == true){
				if(value > 100 ){				
					$("#spn .error-val").css('display', 'block');
					return false;
				} else {
					var md = 0;
					document.getElementById("chk_mod").value = md;				
					value = parseFloat((base_rent*value)/100);
					ttx=(value*tax/100);
					var ttx1 = ttx*days;
					var ttx2 = ttx1.toFixed(2);
					$('#target_tr').text(ttx2);
					$("#spn span").css('display', 'none');
				}			
			 }else {
				var md = 0;
				document.getElementById("chk_mod").value = md;			
				value = value;
				$("#spn span").css('display', 'none');
			 }		

			for (var i = 0, length = radio_val.length; i < length; i++) {
			
				if (radio_val[i].checked) {
					
					var radio_selected = radio_val[i].value; 
					//alert(radio_selected);

					if (radio_selected == "add") 
					{
						//alert('Add Function Call' + base_rent);
						var sum_val = (parseFloat(base_rent) + parseFloat(value))*days;
						document.getElementById("base_room_rent1").value = base_rent;
						document.getElementById("base_room_rent").value = sum_val;
						document.getElementById("dumb_rate").value = sum_val;
						ttx=(sum_val*tax/100);
						document.getElementById("target").innerHTML = document.getElementById("base_room_rent").value+" ("+(base_rent*days)+")";
						document.getElementById("target2").innerHTML=parseFloat(document.getElementById("base_room_rent").value)*d;
						document.cookie="rate="+document.getElementById("base_room_rent").value;	
						taxCal();
					}
					else
					{
						var sum_val = (parseFloat(base_rent) - parseFloat(value))*days;
						document.getElementById("base_room_rent1").value = base_rent;
						document.getElementById("base_room_rent").value = sum_val;
						document.getElementById("dumb_rate").value = sum_val;
						ttx=(sum_val*tax/100);
						document.getElementById("target").innerHTML = document.getElementById("base_room_rent").value+" ("+(base_rent*days)+")";
						document.getElementById("target2").innerHTML=parseFloat(document.getElementById("base_room_rent").value)*d;
						
						document.cookie="rate="+document.getElementById("base_room_rent").value;	
						taxCal();	
						
					}
					break;
					
				} // End IF
			
			} // End For
			} //End Outer If
		} // End First If 
		else{
			var baseroom = base_rent*days;
			var baseroom1 = baseroom.toFixed(2);
			//alert('baseroom1' +baseroom1);
			$('#target').text(baseroom1);
			taxCal();
			//$('#target_tr').text(base_rent*days*0.23);
		}
      
		document.getElementById("chk_mod").value = md;
    } // End function amount_check
	
	
	function m_amount_check(value){ 
	
        tax=0;			
		let mod = $("#mod_meal_rent").val();
		var chk = document.getElementById("chk_mod").value;
		var md = value;
		var radio_val = document.getElementsByName('m_rent_mode_type');
		let meal_rent = $("#price1").val();
		var days = diffDays();
		
		if($('#mperc'). prop("checked") == true){
		   $("#modf_mp").val(mod * meal_rent / 100);
			
		} else {
			
			$("#modf_mp").val(mod);
				
		}
        if ((value >= 0) && (value != '')){
			if ( chk != value ){		
				var diff=document.getElementById("diff").value;
				var d = days;
		
				if($('#mperc'). prop("checked") == true){

					if(value > 100 ){				
						$("#spn .error-val").css('display', 'block');
						return false;
					} else {
						var md = 0;
						document.getElementById("chk_mod").value = md;				
						value = parseFloat((meal_rent*value)/100);
						ttx=(value*tax/100);
						
						$("#spn span").css('display', 'none');
					}			
			 }else {
				var md = 0;
				document.getElementById("chk_mod").value = md;			
				value = value;
				$("#spn span").css('display', 'none');
			 }		

			for (var i = 0, length = radio_val.length; i < length; i++) {
			
				if (radio_val[i].checked) {
					
					var radio_selected = radio_val[i].value; 
					
					if (radio_selected == "add") 
					{
						var sum_val = (parseFloat(meal_rent) + parseFloat(value))*days;
						
						document.getElementById("price1").value = meal_rent;
						document.getElementById("price").value = sum_val;
						document.getElementById("dumb_rate").value = sum_val;
						ttx=(sum_val*tax/100);
						document.getElementById("target_mp").innerHTML = document.getElementById("price").value+" ("+(meal_rent*days)+")";
						
						taxCal();
					}
					else
					{
						//alert('Substract Function Call' + base_rent);
						var sum_val = (parseFloat(meal_rent) - parseFloat(value))*days;
						document.getElementById("price1").value = meal_rent;
						document.getElementById("price").value = sum_val;
						document.getElementById("dumb_rate").value = sum_val;
						ttx=(sum_val*tax/100);
						document.getElementById("target_mp").innerHTML = document.getElementById("price").value+" ("+(meal_rent*days)+")";
						
						taxCal();
						
					}
					break;
					
				} // End IF
			
			} // End For
			} //End Outer If
		} // End First If 
		else{
			
			var mealp = meal_rent*days;
			var mealp1 = mealp.toFixed(2);
			$('#target_mp').text(mealp1);
			taxCal();
		}
        
		document.getElementById("chk_mod").value = md;
    }
	
	//last step calculations
function check_sum3(tax){
		
		var early;
		var totRR = parseFloat($('#target').text());		
		var totExRR = parseFloat($('#target_ex').text());
		var totMP = parseFloat($('#target_mp').text());
		var totExMP = parseFloat($('#target_exmp').text());
		var room_tax= parseFloat($('#target_tr').text());	
		var meal_tax= parseFloat($('#target_tm').text());
		var ext_room_rent= parseFloat($('#target_extx').text());	
		var ext_meal= parseFloat($('#target_exmptx').text());		
		var sum_total=parseFloat(room_tax)+parseFloat(meal_tax)+parseFloat(totRR)+parseFloat(totMP);

		if(isNaN(totExRR)){
			totExRR = 0;
		}
		if(isNaN(totExMP)){
			totExMP = 0;
		}
		var tot = totRR+totExRR+totMP+totExMP;
		//console.log(totRR);
		/*var roomRent1 = document.getElementById("base_room_rent").value;
		var dbSlab1 = document.getElementById("dbSlab1").value;
		var dbSlab2 = document.getElementById("dbSlab2").value;
		var dbSlab3 = document.getElementById("dbSlab3").value;
		var dbSlab = document.getElementById("dbSlab").value;
		var ltax;*/
				
		/*if((parseFloat(roomRent1) >= parseInt(dbSlab1)) && (parseInt(roomRent1) <= parseInt(dbSlab2))){
			ltax = document.getElementById("dblt1").value;
			document.getElementById("luxuryt").value=ltax;
		}
		else if ((parseInt(roomRent1) >= parseInt(dbSlab3)))
		{
			ltax = document.getElementById("dblt2").value;
			document.getElementById("luxuryt").value=ltax;
		}
		else
		{
			ltax=0;
			document.getElementById("luxuryt").value=ltax;
		}*/
		
        /*if(tax =="sum"){
            var early = tax;
           	var stax1 = document.getElementById('stax').value;
			var ltax1 = document.getElementById('ltax').value;
			document.getElementById('servicet').value = stax1;
			document.getElementById('servicec').value = '0';
            tax = parseFloat(document.getElementById('servicet').value)+parseFloat(ltax);			
        }
		else if(tax == "sum2"){
        	var early = tax;
			var stax1 = document.getElementById('stax').value;
			var ltax1 = document.getElementById('ltax').value;
            var totalFixedTax =23;
            var scharge1 = parseFloat(totalFixedTax) -(parseFloat(stax1) + parseFloat(ltax));			
			scharge1=scharge1.toFixed(2);
			document.getElementById('servicet').value=stax1;
			document.getElementById('servicec').value=scharge1;			
			tax=parseFloat(document.getElementById('servicet').value)+parseFloat(document.getElementById('servicec').value)+parseFloat(ltax);
        } 
		else {
			
		}*/
		
		var days = diffDays();
		var diff=days;
		var base_rate=document.getElementById("base_room_rent").value;
		var meal_price=document.getElementById("price").value;
		var ex_price=document.getElementById("extra_charge_amount").value;
		var ex_m_price=$("#target_exmptx").text();
		
		if(isNaN(ex_m_price)){
			ex_m_price = 0.00;
		}
		if(isNaN(ex_price)){
			ex_price = 0.00;
		}

		var total_rate = parseFloat(base_rate)+( parseFloat(base_rate)*(parseFloat(tax)/100))+parseFloat(meal_price)+( parseFloat(meal_price)*(parseFloat(tax)/100));
		var total_tax = (parseFloat(base_rate)*(parseFloat(tax)/100))*parseFloat(diff);
		document.getElementById("total").value=total_rate.toFixed(2);
		
		total_rate="<i class='fa fa-inr'></i> "+parseFloat(total_rate*parseFloat(diff)).toFixed(2);
		
        if(early=="sum"){
           
        }else if(early == "sum2"){

        	
            document.getElementById("booking_tax_type").value = 'Tax';

        } 
		else{
			
			document.getElementById("booking_tax_type").value = 'No Tax';
		
    	}
			if(tax!=0){
			$('#target2').text(totRR.toFixed(2));
			//alert(room_tax);
			$('#ext_target_tr').text(ext_room_rent);
			$('#ext_total_m_tax').text(ext_meal);
			
			$('#target5').text(room_tax);
			$('#m_target2').text(totMP.toFixed(2));
			$('#m_targetEx').text(totExMP.toFixed(2));
			$('#total_m_tax').text(meal_tax);	
			document.getElementById("tax").value = parseFloat(total_tax).toFixed(2);
			$('#ex_target2').text(totExRR.toFixed(2));
			$('#ex_tax').text((totExRR*tax/100).toFixed(2));
			$('#exM_tax').text((totExMP*tax/100).toFixed(2));
			//$('#target3').text((tot*(tax+100)/100).toFixed(2));	
			$('#total').val((tot*(tax+100)/100).toFixed(2));	
			$('#total_m_tax1').val((totMP*tax/100).toFixed(2));	
			$('#ex_per_price').val(totExRR.toFixed(2));
			$('#ex_per_tax').val((totExRR*tax/100).toFixed(2));
			$('#ex_per_m_price').val(totExMP.toFixed(2));
			$('#ex_per_m_tax').val((totExMP*tax/100).toFixed(2));
			//$('#base_room_rent').val(totRR.toFixed(2));
			$('#tax').val((totRR*tax/100).toFixed(2));

			/*var r1=$('#target2').text();
			var m1=$('#m_target2').text();
			var er=$('#ex_target2').text();
			var em=$('#m_targetEx').text();
			var rrt=$('#target5').text();
			var mpt=$('#total_m_tax').text();
			var e_room_tax= parseFloat($('#target_extx').text());	
			var e_meal_tax= parseFloat($('#target_exmptx').text());
			r1=parseFloat(r1);
			m1=parseFloat(m1);
			er=parseFloat(er);
			em=parseFloat(em);
			rrt=parseFloat(rrt);
			mpt=parseFloat(mpt);
			$('#target3').text((r1+m1+er+em+rrt+mpt+e_room_tax+e_meal_tax).toFixed(2));*/
			$('#booking_tax_types').val('Tax');
	}
	
} // end function check_sum3
 
	
	
</script> 
<script type="text/javascript">
   function show_hiden()
{
	
	document.getElementById('hidden-div').style.display= 'block';
	document.getElementById('hide-btn').style.display= 'none';
}
   function shows_hiden()
{
	
	document.getElementById('paymm').style.display= 'block';
}

$(function() {
    $('#payment_input').keypress(function() {
        var v = document.getElementById('payment_input').value;
		v = parseInt(v);
		if (v == ''){
			document.getElementById('submit4').style.display = "none";
		}
		else{
			document.getElementById('submit4').style.display = "block";
		}
    });
	
	
	
	$('#payment_input').blur(function() {
        var v = document.getElementById('payment_input').value;
		v = parseInt(v);
		if (v == ''){
			document.getElementById('submit4').style.display = "none";
		}
		else{
			document.getElementById('submit4').style.display = "block";
		}
    });
	
});

    function master_id_hint(s){

        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>bookings/master_id_validate",
                datatype:'json',
                data:{booking_id:s},
                success:function(data)
                {
                    document.getElementById("master_id_validate").innerHTML=data.valid;
                    document.getElementById("master_id_validate_token").value=data.token;
                    document.getElementById("checkout_date_child").value=data.end_dt;
                    document.getElementById("checkout_date_child_confirmed").value=data.end_dt;
                    document.getElementById("checkout_time_child").value=data.end_time;
                    document.getElementById("g_name_child").value=data.g_name;
                    document.getElementById("g_address_child").value=data.g_address;
                    document.getElementById("g_number_child").value=data.g_number;
                }
            });

    }

    function check_date(date){
        var date_master=document.getElementById("checkout_date_child_confirmed").value;
        if(date_master<date){
            document.getElementById("checkout_date_child").value=date_master;
            swal({
                    title: "Checkout Date Greater",
                    text: "Child checkout date must be less than or equal master checkout date",
                    type: "warning"
                },
                function(){
                    //location.reload();
                });
            return false;

        }
    }


</script> 
<script type="text/javascript">
function check_occupancy2() { /* code here */

    var adult = $("#no_of_adult").val();

    var child = $("#no_of_child").val();

    var occupancy = $("#occupancy").val();

    var extra= (parseInt(adult)+parseInt(child)) - parseInt(occupancy); 

    if(parseInt(occupancy) < (parseInt(adult)+parseInt(child)) ){
      swal("occupancy limit exceeds. MAX value: "+occupancy);
     document.getElementById("no_of_adult").value="";
     document.getElementById("no_of_child").value="";
     
    

    }

    
    var adult = $("#no_of_adult").val();

    var child = $("#no_of_child").val();

    var occupancy = $("#room_bed").val();

    var extra = (parseInt(adult)+parseInt(child)) - parseInt(occupancy); 

    if(parseInt(occupancy) < (parseInt(adult)+parseInt(child)) ){
      swal("Bed limit exceeds. MAX value: "+occupancy);
	  $("#no_of_adult").val('');
			$("#no_of_child").val('');
			$("#o_id").val('');
      document.getElementById("extra_charge").style.display="block";
     
    }else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))==1){
			$("#o_id").val(1);
		}else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))==2){
			$("#o_id").val(2);
		}else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))==3){
			$("#o_id").val(3);
		}else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))>3){
			$("#o_id").val(4);
		}

   }

   function check_occupancy() {

		var flag = 0;
		var adult = $("#no_of_adult").val();
		var child = $("#no_of_child").val();
        var maxo = parseInt($("#occupancy").val());
		var occupancy = $("#room_bed").val();
		var extra= (parseInt(adult)+parseInt(child)) - parseInt(occupancy); 
		
		
   if((parseInt(adult)+parseInt(child)) > 0){
		if(parseInt(maxo) < (parseInt(adult)+parseInt(child))){
			swal("Max limit reached! MAX value: "+maxo);
			$("#no_of_adult").val('');
			$("#no_of_child").val('');
			$("#o_id").val('');
			$("#extra_charge_amount").val('');
			document.getElementById("extra_charge").style.display="none";
			$("#charges").hide();
			flag = 0;
		}
		else if(parseInt(occupancy) < (parseInt(adult)+parseInt(child))){
			swal("Bed limit reached! MAX value: "+occupancy);
			document.getElementById("extra_charge").style.display="block";
			flag = 0;	
			$("#charges").show();
			$("#o_id").val(occupancy);
		}	
		else{
			$("#extra_charge_amount").val('');
			document.getElementById("extra_charge").style.display="none";
			flag = 1;
			$("#charges").show();
			
		}
		
		
		if(flag === 1){
				
			if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))==1){
			$("#o_id").val(1);
			}else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))==2){
				$("#o_id").val(2);
			}else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))==3){
				$("#o_id").val(3);
			}else if(parseInt(occupancy) >= (parseInt(adult)+parseInt(child)) && (parseInt(adult)+parseInt(child))>3){
				$("#o_id").val(4);
			}
			else{
				$("#charges").hide();
			}
		}
		
		}
		else{
			$("#charges").hide();
			$("#o_id").val('');
			document.getElementById("extra_charge").style.display="none";
			$("#extra_charge_amount").val('');
		}
		
	 }

	 function set_extra_charge(val){
		//tax=23;
		
		
		
		$('#loading-image').show();
		var diff=document.getElementById("diff").value;
		var days = diffDays();
		var d=days;

			var ex_cno=0;
			var ex_ano=0;
		 if(val=='child'){
			 ex_cno=paxe;
			 ex_ano=0;
			window.expType = 'c';
			 var ex_ch=$('#ex_child_r').val();
			 var ex_ch_mp=$('#ex_child_m').val();
			  $('#ex_u_mprice').val(ex_ch_mp);
			 $('#ex_u_price').val(ex_ch);
			var ex_mp= $('#target_exmp').text();
			 var ex_ch=parseFloat(ex_ch);
			 
			 $('#extra_charge_amount').val(ex_ch*paxe*d);
			 $('#extra_ch_no').val(parseInt(ex_cno));
			 $('#extra_ad_no').val(0);
			 $('#target_ex').text(ex_ch*paxe*d);
			 //mtx=parseInt(ex_ch)*paxe*tax/100*d;
			  mptx=parseInt(ex_ch_mp)*paxe*d;
			
			$('#target_exmp').text(mptx);			 
		 }
		 if(val=='adult'){
			  ex_ano=paxe;
			  ex_cno=0;
			  
			 window.expType = 'a';
			 var ex_ad_mp=$('#ex_adult_m').val();
			 $('#ex_u_mprice').val(ex_ad_mp);
			var ex_ad=$('#ex_adult_r').val();
			 $('#ex_u_price').val(ex_ad);
			  $('#extra_ad_no').val(parseInt(ex_ano));
			  $('#extra_ch_no').val(0);
			 $('#extra_charge_amount').val(ex_ad*paxe*d);
			 $('#target_ex').text(ex_ad*paxe*d);
			 mtx=parseInt(ex_ad)*paxe*tax/100*d;
			 mptx=parseInt(ex_ad_mp)*paxe*d;
			//$('#target_extx').text(mtx);
			$('#target_exmp').text(mptx);
			//$('#target_exmptx').text(mptx*tax/100);
			
		}
		
		$('#loading-image').hide();
		
		
		taxCal();
	}

function charge_modifier(value){
        window.tax=23;
	 
}
	 
</script> 
<script>
function make_date_readable(a)
{
	var b= $('#'+a+'').val();
	//alert (b);
	$('#'+a+'').removeAttr('readonly');
	//$('#'+a+'').addClass(' date date-picker');
}

function fetch_all_address(pincode,g_country,g_state,g_city){
	var pin_code = document.getElementById('cust_address').value;
//	alert(pin_code);
    if(pin_code != ""){
		jQuery.ajax(
		{
			type: "POST",
			url: "<?php echo base_url(); ?>bookings/fetch_address",
			dataType: 'json',
			data: {pincode: pin_code},
			success: function(data){
				var full_address = (data.city+","+data.state+","+data.country);
				//alert (full_address);
				$('#cust_full_address').val(full_address);
				//alert ($('#cust_full_address').val());
			}
		});
	}
}
function fetch_all_address_hotel(pincode,g_country,g_state)
{
	//var pin_code = document.getElementById(pincode).value;
	//alert(pincode+g_country+g_state);
	

	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address2",
		dataType: 'json',
		data: {pincode: pincode},
		success: function(data){
			//alert(data);
		//	//console.log('state_g' +state_g);
		if(data.state!=''){
			var st = data.state;
			var state_g = st.toUpperCase();
			//alert('state_g' +state_g);
				//console.log('state_g' +state_g);
		//	$('#g_state').val(state_g).change();
			var cnt = data.country;
			var country_g = cnt.toUpperCase();
			//$('#g_country').val(country_g);
			
			$(g_country).val(country_g).change();
			$(g_state).val(state_g).change();
		}
			
			//var full_address = (data.city+","+data.state+","+data.country);
			//$('#cust_full_address').val(full_address);
		}

	});
}
function prev11(){
        $('#tab11').hide();
        $('#tab1').show();
    }
	
function prev12(){
        $('#tab3').hide();
        $('#tab2').show();
}
</script> 
<script>
function change_stay_span(a)
{
			var cid = a.split("-");
			var cod = $('#end_dt').val().split("-");
			
			var check_in_date = new Date(cid[2],cid[1],cid[0]);
            var check_out_date = new Date(cod[2],cod[1],cod[0]);
		    var stay_days = (check_out_date.getTime()- check_in_date.getTime())/(1000*3600*24);
			//alert(stay_days);
			 $('#diff').val(stay_days);
	
}


function check_booking(val){
	var start_dt=$('#start_dt').val();
	var end_dt=$('#end_dt').val();
	var chkout_time=$('#end_time').val();
		
			jQuery.ajax(
		{
			type: "POST",
			url: "<?php echo base_url(); ?>bookings/check_booking",
			dataType: 'json',
			data: {start_dt:start_dt,end_dt:end_dt,chkin_time:val,chkout_time:chkout_time},
			success: function(data){
				if(data.data==1){
					swal("Booking already Exists!");
				}else{
					
				}
				
			}
		});
}

function get_season(){
	//alert('f');
	var start_dt=$('#start_dt').val();
	var end_dt=$('#end_dt').val();
	
	jQuery.ajax(
		{
			type: "POST",
			url: "<?php echo base_url(); ?>rate_plan/get_season",
			data: {start_dt:start_dt,end_dt:end_dt},
			success: function(data){
				//alert(data);
				$('#season_id').val(data);
			}
		});

		
var room_id=$('#room_id').val();
	jQuery.ajax(
		{
			type: "POST",
			url: "<?php echo base_url(); ?>rate_plan/get_unit_type_id",
			data: {room_id:room_id},
			success: function(data){
				$('#u_type_id').val(data);
			}
		});		
		
		}

function get_meal_plan(){
	var bc = parseInt($("#booking_source").val());
	var adult = parseInt($("#no_of_adult").val());
	var child = parseInt($("#no_of_child").val());
	if(!isNaN(child) && !isNaN(adult) && !isNaN(bc) && (adult+child)>0){
		
		$("#charges").show();

	}
	else{

		$("#charges").hide();

	}		
}

$( "#no_of_adult, #no_of_child" ).change(function() {
	var rmCh = $('#booking_rent').val();
	
	check_occupancy();
	if(rmCh != undefined || rmCh != null){
		get_meal_plan();
		get_rmCh(taxCal);
	}	
});

$( "#booking_source, #booking_rent" ).change(function() {
  var mplan = $('#plan_id').val();
    
  if((mplan != undefined) && (mplan != '')){
	  get_meal_plan();
	  get_rmCh(taxCal);
	  
  }
});

$( "#booking_rent" ).change(function() {
  var mplan = $('#plan_id').val();
    
  if((mplan != undefined) && (mplan != '')){
	  get_meal_plan();
	  get_rmCh(taxCal);
	
  }
});

function popLast(){
	var totRent = parseFloat($('#target').val());
	//console.log('totRent = '+totRent);	
	
	parseFloat($('#target2').val(totRent));
	
} // End function popLast

let toDate = (dateStr) => {
    const [day, month, year] = dateStr.split("-");
    return new Date(year, month - 1, day);
}; // End toDate

let diffDays = () => {
	
	let stDate = $("#start_dt").val();
	let endDate = $("#end_dt").val();
	let date1 = toDate(stDate);
	let date2 = toDate(endDate);		
	let timeDiff = Math.abs(date2.getTime() - date1.getTime());
	let Days = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
	
	if(Days < 1) {Days = 1;}
	if(checkintime() < 0){
		Days = Days;
	}
	return Days;
	
}; // End diffDays

let updateRates = () => {
	
	$("#diff").val(diffDays());
	let rr = $("#target").text();
	let mp = $("#target_mp").text();

	if(rr > 0 || mp > 0){
		//console.log('Updating values...');
		get_rmCh(taxCal);
	}
}

function get_rmCh(callback){
	get_season();
	$("#mod_room_rent").val('');
	var rmCh = $('#booking_rent').val();
	var mplan = $('#plan_id').val();
	var bc = parseInt($("#booking_source").val());
	var s_id = parseInt($("#season_id").val());
	var u_id = parseInt($("#u_type_id").val());
	var o_id = parseInt($("#o_id").val());
	// _sb
	var days = diffDays();
	
	//swal('Type - '+rmCh+' | Season - '+s_id+' | Unit Type - '+u_id+' | Occupancy - '+o_id+' | Source - '+bc+' | Meal - '+mplan);
	var adult = parseInt($("#no_of_adult").val());
	var child = parseInt($("#no_of_child").val());
	var defo = parseInt($("#room_bed").val());
	var maxo = parseInt($("#occupancy").val());
	var pax = adult + child;
	window.paxe = 0;
	var rr;	var mr;	var ar;	var cr;
	
	//alert(defo+' - '+maxo);
	if(pax>defo){
		paxe = pax - defo;
	}
	
	console.log('Calling Rate Plan:: Type = '+rmCh+', MPlan = '+mplan+', Source = '+bc+', Season = '+s_id+', Unit Type = '+u_id+', Occupancy = '+o_id+', days = '+days);
	
	if(!isNaN(child) && !isNaN(adult) && (pax > 0) && (rmCh!="") && days > 0){
		
		$('#loading-image').show();	
				
		jQuery.ajax({
			
				type: "POST",
				url: "<?php echo base_url(); ?>rate_plan/get_rmCh",
				data: {
					chType:rmCh,
					mplan:mplan,
					source:bc,
					season:s_id,
					unitType:u_id,
					pax:o_id
				},
				success: function(data){
					
					console.log('success: get_rmCh' +data.room_charge);
					
					$("#base_room_rent").val(data.room_charge);
					$("#base_room_rent1").val(data.room_charge);
					$("#unit_room_rent").val(data.room_charge);
					$("#price").val(data.meal_charge);
					$("#price1").val(data.meal_charge);
					$("#ex_child_r").val(data.room_charge_ec);
					$("#ex_adult_r").val(data.room_charge_ea);	
					$("#ex_adult_m").val(data.meal_charge_ea);	
					$("#ex_child_m").val(data.meal_charge_ec);	
					rr = $("#base_room_rent").val();
					mr = $("#price").val();
					ar = $("#ex_adult_r").val();
					arm = $("#ex_adult_m").val();
					cr = $("#ex_child_r").val();
					crm = $("#ex_child_m").val();
					var dyr = days*rr;
					var dyr1 = dyr.toFixed(2);
					$("#target").text(dyr1);		
					var mpd = days*mr;
					var mpd1 = mpd.toFixed(2);
					$("#target_mp").text(mpd1);
					var mrd = days*mr;
					var mrd1 = mrd.toFixed(2);
					$("#price2").val(mrd1);
					var tt=parseFloat(data.room_charge);			 
					var tm=parseFloat(data.meal_charge);
					

					if(paxe>0){
						if(expType === 'c'){
							var paxer = (paxe*cr*days).toFixed(2);
							$("#target_ex").text(paxer);
							var paxem = (paxe*crm*days).toFixed(2);
							$("#target_exmp").text(paxem);
						}
						else if(expType === 'a'){
							var paxer = (paxe*ar*days).toFixed(2);
							$("#target_ex").text(paxer);
							var paxem = (paxe*arm*days).toFixed(2);
							$("#target_exmp").text(paxem);
						}
					}
					else{
						$("#target_ex").text('0');
						$("#target_extx").text('0');
						$("#target_exmp").text('0');
						$("#target_exmptx").text('0');
						$("#target_tm").text('0');
					}
					
					if(callback && typeof(callback) === 'function') {
						callback();
					} else
						swal('Invalid Callback supplied');
					
				}, // end ajax success
				
				complete:function(){
					
					if($("#target").text() == 0) {
						
						$("#no_of_adult").val('');
						$("#no_of_child").val('');
						
					}
					
					if(paxe !== 0 && $("#target_ex").text() == 0) {
						
						$("#no_of_adult").val('');
						$("#no_of_child").val('');
						
					}
					
					$('#loading-image').hide();
				},
		}); // end ajax
	} else {
		//swal('Please enter the PAX (No of Adult & Child) Properly!');
	}		
	
}	// END function get_rmCh

	var taxMarge = new Array();
	var obj = {};
	var flgR=0;
	var flgM=0;
	
function taxCal(){
		
	let roomRent = parseFloat($('#target').text());
	let ex_roomRent;
	let ex_mealCharge;
	let total_meal;
	let mealCharge;
	ex_roomRent = parseFloat($('#target_ex').text());
	if(ex_roomRent == ''){ex_roomRent=0;}
	mealCharge = parseFloat($('#target_mp').text());
	ex_mealCharge = parseFloat($('#target_exmp').text());
	if(ex_mealCharge == ''){ex_roomRent=0;}
	let total_roomRent=parseFloat(roomRent)+parseFloat(ex_roomRent);
	total_meal=parseFloat(mealCharge)+parseFloat(ex_mealCharge);
	
	/*if(roomRent >= 0)  // Logic For seperate tax calculation for extra person
		get_tax('Booking',roomRent,0);	 
	
	if(mealCharge >= 0)
		get_tax('Meal Plan',mealCharge,0);
	
	if(ex_roomRent >= 0)
		get_tax('Booking',ex_roomRent,1);
	
	if(ex_mealCharge >= 0)
		get_tax('Meal Plan',ex_mealCharge,1);
	
	if(total_roomRent >= 0)
		get_tax('Booking',total_roomRent,2);
	
	if(total_meal >= 0)
		get_tax('Meal Plan',total_meal,2);*/ 
	
	if(roomRent >= 0)
		get_tax('Booking',roomRent,0);	 
	
	if(mealCharge >= 0)
		get_tax('Meal Plan',mealCharge,0);
	
	if(ex_roomRent >= 0)
		get_tax('Booking',roomRent+ex_roomRent,0);
	
	if(ex_mealCharge >= 0)
		get_tax('Meal Plan',mealCharge+ex_mealCharge,0);
	
	if(total_roomRent >= 0)
		get_tax('Booking',total_roomRent,2);
	
	if(total_meal >= 0)
		get_tax('Meal Plan',total_meal,2);
	
}

var mp = [];
var rr = [];
var hhh=1;
var ppptax = [];
var abc=0;

function get_tax(type1,amt,ex){
	
	var g_state = $('#g_state').val();
	if(g_state == null || g_state ==''){
		
		g_state = $('#g_state1').val();
	}
//	alert('g_state' +g_state);
	var target;
	var tttt=0;
	target=amt;
	var startDate=$('#startDate').val();
	
	// _sb
	var tdays = diffDays();
	
	//var tdays = parseInt($("#diff").val());
	var price=parseFloat(amt);
	if(tdays){tdays;}else{tdays=1;}
	price=price/tdays;
	
	let extraMP = parseFloat($('#target_exmp').text())
	
	$('#total_m_tax').text(0);
	
	//$('#loading-image').show();

	//console.log('Calling tax plan - Price = '+price+' Type = '+type1+' Ex P = '+ex);
	$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>dashboard/showtaxplan1",
	async: false,
    data:{price:price,args:'a',type1:type1,dates:startDate,g_state:g_state},	
    success: function(data){
		if(data)
		{
			//alert(' total is = '+data);//return false;
			var data1=data;
			var tax_response=data;
			var tax_rule_id=data.r_id;
			var tax_total=data.total;
			var k=[];
			$.each(data, function(key, value) {
		
				if(key !='total' && key!='r_id' && key!='planName'){
					//k[key]=value;
					//alert(key+'='+value);
					var va=parseFloat(tdays)*parseFloat(value);
					

					if((type1=='Meal Plan' && ex==1)  || (type1=='Booking' && ex==1) ||(type1=='Meal Plan' && ex==0)  || (type1=='Booking' && ex==0)){
						
					if(type1 == 'Booking'){
						if(!(key in rr)){
							rr[key]= va;
						} else {
							rr[key]= rr[key] + va;
						}
					} else {
						if(!(key in mp)){
							mp[key]= va;
						} else {
							mp[key]= mp[key] + va;
						}						
					}	
					}
				
				}
			  
			});
			
			
				for (var key2 in rr){
					if (typeof rr[key2] !== 'function') {
					
					}
				}	
				for (var key1 in mp){
					if (typeof mp[key1] !== 'function') {
						
					}
				}
	
	//console.log('Tax: '+type1+'  '+tax_total);
	
	if(type1 == 'Meal Plan'){	
		if(ex == 1){
			tax_total=parseFloat(tdays)*parseFloat(tax_total);
			let total = amt+tax_total;
			//console.log('Total Meal Plan '+total);
			$('#target_tmpT').text(total.toFixed(2));
		} else if(ex == 0) {
			tax_total=parseFloat(tdays)*parseFloat(tax_total);
			$('#target_tm').text(tax_total.toFixed(2));
			let total = amt+tax_total;
			$('#target_tmpT').text(total.toFixed(2));
			//console.log(parseFloat(total.toFixed(2)) + ' ' + parseFloat($('#target_trrT').text()));
			let totalT = parseFloat(total.toFixed(2)) + parseFloat($('#target_trrT').text());
			$('#target_tT').text(totalT.toFixed(2));
			$('#target3').text(totalT.toFixed(2));
		}
		$('#total_m_tax').text(tax_total.toFixed(2));
	} else {
		if(ex == 0){
			tax_total=parseFloat(tdays)*parseFloat(tax_total);
			$('#target_tr').text(tax_total.toFixed(2));
			let total = amt+tax_total;
			$('#target_trrT').text(total.toFixed(2));
		}
		else if(ex == 1)
		{
			var aa=$('#target_ex').text();
			if(aa>0){
				tax_total=parseFloat(tdays)*parseFloat(tax_total);
				$('#target_extx').text(tax_total.toFixed(2));
				let total = amt+tax_total;
			}else{
				tax_total=0;
				$('#target_extx').text('0');
				let total = amt+tax_total;	
			}
			$('#target_trrT').text(total.toFixed(2));
			//console.log(parseFloat(total.toFixed(2)) + ' ' + parseFloat($('#target_tmpT').text()));
			let totalT = parseFloat(total.toFixed(2)) + parseFloat($('#target_tmpT').text());
			$('#target_tT').text(totalT.toFixed(2));
			$('#target3').text(totalT.toFixed(2));
		}
	}
 
	}
		var stringData = JSON.stringify(data);
		//alert(type1+' - '+stringData);
		let roomRent = parseFloat($('#target').text());
		let mealCharge = parseFloat($('#target_mp').text());
		if((ex==2 || roomRent) && (ex==2 || mealCharge)){
			
				if(type1=='Booking'){
					$('#response').val(stringData);									
					$('#BookingResponse').val(stringData);
					$('#rr_total_tax').val(tax_total*tdays);
					
				}
				
				if(type1=='Meal Plan'){
					//alert('meal'+stringData);
					$('#mealResponse').val(stringData);
					$('#mp_total_tax').val(tax_total*tdays);
			
				}
				
				if(type1=='Booking' || type1=='Meal Plan'){
				delete data['total'];
				delete data['r_id'];
				delete data['planName'];
			
		}
		
			if(flgR == 0 &&  type1 == 'Booking'){
			$("#tax_details_area_bk").text('');
			$.each(data, function(key, value) {
				
				$("#tax_details_area_bk").append('<div class="col-xs-6"><label id="target4" style="display:block;">'+key+': '+'</label></div><div class="col-xs-6"><label class="room0" style="color:#DC166A;">'+value*tdays+'</label></div>');
				abc=parseFloat(abc)+value;
				ppptax[key]=value*tdays;
			});
			//flgR = flgR + 1;
			}
			
			if(flgM ==0 &&  type1=='Meal Plan'){
			$("#tax_details_area_meal").text('');
			$.each(data, function(key, value) {
				$("#tax_details_area_meal").append('<div class="col-xs-6"><label id="target4" style="display:block;">'+key+': '+'</label></div><div class="col-xs-6"><label class="meal0" style="color:#DC166A;">'+value*tdays+'</label></div>');
				abc=parseFloat(abc)+value;
				ppptax[key]=value*tdays;
			});
			//flgM=flgM+1;			
			}
		
		}
		
		hhh=hhh+1;
		
		
	}, // End Ajax Success
				/*complete:function(){
					$('#loading-image').hide();
				},*/
	
    }); // End Ajax
	            /*for (var key2 in rr){
					if (typeof rr[key2] !== 'function') {
						  
					}
				}	
				for (var key1 in mp){
					if (typeof mp[key1] !== 'function') {
						
					}
				}*/
} // function get_tax

function formatDate(dt) {
	
	var at = dt.split("-");
    var month = at[0];
    var day = at[1];
    var year = at[2];
    return [day, month, year].join('-');
}

function dateformat_increase(date,co) {
	//alert('this is date'+date);
	////console.log('line Item Date: '+new Date(date));
	let date1 = date;
	date1.setDate(date1.getDate() + co);
	//console.log('line Item Date: '+date);
    var d = new Date(date1),
        month = '' + (d.getMonth()+1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
//day_sta=parseInt(a);
/*day=parseInt(day);
day=day+parseInt(co);*/   // Who did this??!
day=day.toString();
if (day.length < 2) day = '0' + day;
    return [ year,  month,day ].join('-');
}

function dateformat_decrease(date,co) {
	date.setDate(date.getDate() - co);
	return date;
}

function parseDate(str) {
    var mdy = str.split('-');
    return new Date(mdy[2], mdy[0]-1, mdy[1]);
}


function parseDate1(str) {
    var mdy = str.split('-');
    return new Date(mdy[0]-1, mdy[2],  mdy[1]);
}




function bookingChargeLineItem(){
 
  document.getElementById('hide-btn').style.display= 'none';
	
  window.popNewCountSub++;
	
  if(window.popNewCountSub === 1){

  var i=0;
  var ajxLnS;
  var callOpnV1;
  var tax=$('#tax_track').val();
  var barray=[];
  var marray=[];
  var startDate_increase=$('#start_dt').val();	
  var for_a;
  var days = diffDays();
  var date11_last;
  if(days<1){
	days=1;
  }
	var adlt = $('#no_of_adult').val();
	var kid =  $('#no_of_child').val();
	var room_rent=parseFloat($('#target').text());
	var extra_parsen_rom_rent=parseFloat($('#target_ex').text());
	var meal_plan=parseFloat($('#target_mp').text());
	var extra_meal_plan=parseFloat($('#target_exmp').text());
	
	if(tax!='0'){
		var room_rent_tax=parseFloat($('#rr_total_tax').val());
		var extra_parsen_rom_rent_tax=parseFloat($('#target_extx').text());
		var meal_plan_tax=parseFloat($('#mp_total_tax').val());
		var extra_meal_plan_tax=parseFloat($('#target_exmptx').text());
		var BookingResponse=$('#BookingResponse').val();
		var mealResponse=$('#mealResponse').val();
	}else{
		var room_rent_tax=0;
		var extra_parsen_rom_rent_tax=0;
		var meal_plan_tax=0;
		var extra_meal_plan_tax=0;	
		var BookingResponse=0;
		var mealResponse=0;
	}
	
	var booking_id=parseFloat($('#booking_1st').val());
	
	room_rent=room_rent/days;	
	room_rent_tax=room_rent_tax/days;	
	extra_parsen_rom_rent=extra_parsen_rom_rent/days;
	extra_parsen_rom_rent_tax=extra_parsen_rom_rent_tax/days;	
	meal_plan=meal_plan/days;	
	meal_plan_tax=meal_plan_tax/days;
	extra_meal_plan=extra_meal_plan/days;	
	extra_meal_plan_tax=extra_meal_plan_tax/days;
	callOpnV1 = 0;
	
	
	
	for(i=0;i<days;i++){

		for_a=formatDate(startDate_increase);
		for_a=parseDate(for_a);
		//console.log(for_a);
		if(checkintime() < 0){
			  for_a=dateformat_decrease(for_a,1);
			  //console.log(for_a);
		}
		
		date11=dateformat_increase(for_a,i); // Wrong Code taking 2017-06-31 ?!
		
		console.log('Ajax Call(bookingChargeLineItem_insert) - date: '+date11);
		if(date11 != date11_last){
			
			//alert('ita '+i);

			//console.log('Ajax Call(bookingChargeLineItem_insert) - date: '+date11+' id: '+booking_id+' RR: '+room_rent+' RRTax: '+room_rent_tax+' EPRR: '+extra_parsen_rom_rent+' EPRRTax: '+extra_parsen_rom_rent_tax+' '+meal_plan+' '+meal_plan_tax+' '+extra_meal_plan+' '+extra_meal_plan_tax+' Bk Resp: '+BookingResponse+' Ml Resp: '+mealResponse +' adult==' +adlt);
		
			let linesub = jQuery.ajax(
			{
				type: "POST",
				url: "<?php echo base_url(); ?>bookings/bookingChargeLineItem_insert",
				async: false,
				data: {
					date11:date11,
					booking_id:booking_id,
					room_rent:room_rent,
					room_rent_tax:room_rent_tax,
					extra_parsen_rom_rent:extra_parsen_rom_rent,
					extra_parsen_rom_rent_tax:extra_parsen_rom_rent_tax,
					meal_plan:meal_plan,
					adult:adlt,
					kid:kid,
					meal_plan_tax:meal_plan_tax,
					extra_meal_plan:extra_meal_plan,
					extra_meal_plan_tax:extra_meal_plan_tax,
					BookingResponse:BookingResponse,
					mealResponse:mealResponse
				}
			}); // end ajax
			
			
			let linesub2 = linesub.done((data) => {
				ajxLnS = data.status;
				if(ajxLnS === 1){
					callOpnV1 ++;
				}	
			});
			
		}
			
		date11_last = date11;
			
	} // End For Loop
		
	if(callOpnV1 == days){
		$("#lineid").val('Success');
		$("#linestatus").val('Success');
		//console.log('Calling openview1');		
		openview1(booking_id);
		
		
	} else {
		swal('Please close the page & retake the booking');
		$("#lineid").val('Failure');
	}

  }
} // End function bookingChargeLineItem
 
</script>

<script> //typehead 
var aa;
var mtax;
var rm;
var rrt;
var mpt;
var ex_rr;
var ex_mp;

function noTax(v){

	var r1=$('#target2').text();
	var m1=$('#m_target2').text();
	var er=$('#ex_target2').text();
	var em=$('#m_targetEx').text();
	r1=parseFloat(r1);
	m1=parseFloat(m1);
	er=parseFloat(er);
	em=parseFloat(em);
	rrt=parseFloat(rrt);

	mpt=parseFloat($('#target5').text());
	
	if(v=="notax"){
		aa = $("#tax_details_area").html();
		rrt=parseFloat($('#total_m_tax').text());
		rm=parseFloat($('#target5').text());
		mtax=parseFloat($('#total_m_tax').text());
		ex_rr=parseFloat($('#ext_target_tr').text());
		ex_mp=parseFloat($('#ext_total_m_tax').text());
		var f=$('#room0').text();
		$('.room0').text(0);
		$('.meal0').text(0);
		$('#tax_track').val(0);
		$('#total_m_tax').text(0);
		$('#target5').text(0);
		$('#ext_target_tr').text(0);
		$('#ext_total_m_tax').text(0);
		$('#target3').text((r1+m1+er+em).toFixed(2));
		$('#booking_tax_types').val('No Tax');
	}
	else{
		
		 $("#tax_details_area").html(aa);
		 $('#ext_target_tr').text(ex_rr);
		 $('#ext_total_m_tax').text(ex_mp.toFixed(2));
		 $('#total_m_tax').text(mtax.toFixed(2));
		 $('#target5').text(rm);
		 $('#tax_track').val(1);
		 $('#target3').text((r1+m1+er+em+rm+mtax+ex_rr+ex_mp).toFixed(2));
		 $('#booking_tax_types').val('Tax');
	}
}

$.typeahead({
    input: '.js-typeahead-user_v1',
    minLength: 1,
    order: "asc",
    dynamic: true,
    delay: 200,
    backdrop: {
        "background-color": "#fff"
    },
   
    source: {
       project: {
            display: "name",
            ajax: [{
                type: "GET",
                url: "<?php echo base_url();?>dashboard/test_typehead",
                data: {
                    q: "{{query}}"
                }
            }, "data.name"],
            template: '<div class="clearfix">' +
                '<div class="project-img">' +
                    '<img class="img-responsive" src="{{image}}">' +
                '</div>' +
                '<div class="project-information">' +
                    '<span class="pro-name" style="font-size:15px;"> {{name}}</span><span><i class="fa fa-phone"></i> <strong>Contact no:</strong> {{ph_no}} </span><span><i class="fa fa-envelope"></i> <strong>Email:</strong> {{email}}</span></span>' +
                '</span>' +
				'</div>' +
            '</div>'
        }
    },
    callback: {
        onClick: function (node, a, item, event) {		 
			$("#cust_address").val(item.pincode);
			$("#g_address_child").val(item.address);
			$("#cust_contact_no").val(item.ph_no);
			$("#cust_mail").val(item.email);
			$("#cust_name").val(item.name);
			$('#id_guest2').val(item.id);
			var st = item.state;
			var state_g = st.toUpperCase();
			$('#g_state').val(state_g).change();
			var cnt = item.country;
			var country_g = cnt.toUpperCase();
			//alert(country_g +' '+state_g);
			$('#g_country').val(country_g).change();
		 
        },
        onSendRequest: function (node, query) {
            //console.log('request is sent')
			
        },
        onReceiveRequest: function (node, query) {
            
			if(query!=''){
				$("#cust_name").val('');
				$("#cust_address").val('');
			$("#g_address_child").val('');
			$("#cust_contact_no").val('');
			$("#cust_mail").val('');
			$('#id_guest2').val('');
			}
        },
         onCancel: function (node, query) {
            
			if(query!=''){
				$("#cust_name").val('');
				$("#cust_address").val('');
			$("#g_address_child").val('');
			$("#cust_contact_no").val('');
			$("#cust_mail").val('');
			$('#id_guest2').val('');
			}
        }		
    },
    debug: true
});

	
function check_email(){
	 var a=$('#email').val();
	 var v1 = a.indexOf("@");
	 //alert(v1);
	 var v2 = a.indexOf(".");
	 //alert(v2);
	 var v3=a.length;
	 if((v1<v2)&&(v2-v1)<12 && (v3-v2)<=5){
	 }
	 else{
		 $('#email').val("");
	 }
}
function commision_select(status){
	
	var commision=parseFloat($('#commision_tot').val());
	var rent=parseFloat($('#target').text());
	//alert(commision);

	var meal=parseFloat($('#target_mp').text());
	
	var a1=parseFloat($('#target_tr').text());
	if(a1==null){
		a1=0;
	}
	//alert(a1);
	
	var a2=parseFloat($('#target_ex').text());
	if(a2==null){
		a2=0;
	}
	//alert(a2);
	var a3=parseFloat($('#target_extx').text());
	if(a3==null){
		a3=0;
	}
	//alert(a3);
	var a4=parseFloat($('#target_tm').text());
	if(a4==null){
		a4=0;
	}
	//alert(a4);
	var a5=parseFloat($('#target_exmp').text());
	if(a5==null){
		a5=0;
	}
	//alert(a5);
	var a6=parseFloat($('#target_exmptx').text());
	if(a6==null){
		a6=0;
	}
	var tcom;
	if(status=='1'){tcom=rent;}
	if(status=='2'){tcom=rent+meal;}
	if(status=='3'){tcom=rent+meal+a1+a2+a3+a4+a5+a6;}
	var applyedAmount=tcom;
	tcom=tcom*(commision/100);
	$('#tcom').text(tcom);
	$('#tcomV').val(tcom);
	$('#applyedAmount').val(applyedAmount);
}
</script>
<script>
$('#g_country').on('change',function(){
	
	
	var contyr = $(this).val();
	//alert('contyr' +contyr);
	if(contyr == "INDIA"){
		
	//	alert('ind');
		$('#g_state_div1').show();
		$('#other_st').hide();
		$('#g_state1').val('');
	}
	else{
		
	//	alert('hind');
		$('#other_st').show();
		$('#g_state').val('');
		$('#g_state_div1').hide();
		//<input type="text" class="form-control" place="put your country state" id="g_state" name="g_state">
	//	$('#g_state_div').pre('<input type="text" class="form-control"place="put your country state" id="g_state" name="g_state">');
	}
	
});

$(document).keypress(
    function(event){
     if (event.which == '13') {
        event.preventDefault();
      }
});

</script>
</body>
</html>