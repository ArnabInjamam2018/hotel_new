<link href="https://fonts.googleapis.com/css?family=Exo+2:400,700|Monda:400,700|Open+Sans:400,700|Roboto:400,700" rel="stylesheet">
<link href="<?php echo base_url();?>assets/layouts/layout/css/font.css" rel="stylesheet" type="text/css" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo base_url();?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url();?>assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?php echo base_url();?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/themes/light2.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->

<script>
    //$('body').width( $(window).width(20) );
</script>

<?php $room=$this->dashboard_model->get_room_number_exact($room_id);?>
<?php 
 $room1=$this->rate_plan_model->get_unit_type_id($room_id);
 $room2=$this->rate_plan_model->get_rmCh(1,1,1,$room1->unit_id,1);

/*if($room->room_rent_seasonal==0){
    $room_rent_seasonal=$room->room_rent;
}else{$room_rent_seasonal=$room->room_rent_seasonal;}



if($room->room_rent_weekend==0){

    $room_rent_weekend=$room->room_rent;


}else{$room_rent_weekend=$room->room_rent_weekend;}*/
?>
<?php
    $clean=$room->clean_id;
	 $bgColor=$this->dashboard_model->get_color_by_houskeeping_status_id($clean);
	
	  if(isset($bgColor) && $bgColor) 
	  {
		  $color=$bgColor->color_primary;
		  $status=$bgColor->status_name;
	  }
    else
	{
		$color="#f00";
	}
    ?>
 <body style="background:none;">   
<div class="page-container">
<div class="portlet box" style="border-color:<?php echo $color; ?>; border-width:0 1px 1px; border-style:none solid solid; background-color:<?php echo $color; ?>; margin-bottom:0px;">
  <div class="portlet-title">
    <div class="caption"> Room Details</div>
    <div class="tools" style="display: inline-block; float: right; padding: 12px 0 8px;"> <a href="javascript:close();" style="color:#ffffff;"> <i class="fa fa-times"> </i></a> </div>
  </div>
  <div class="portlet-body clearfix">
    <div class="thumbnail pull-left" style="width:40%;"> <img src="<?php echo base_url();?>upload/thumb/<?php if($room->room_image_thumb == '') { echo "no_images.png"; } else { echo $room->room_image_thumb; } ?>" style="width:auto; height:40%;">
      <div class="caption text-center">        
          <h4 ><strong style="color: #2B3643;">Unit Id:</strong> <a onClick="go_edit('<?php echo $room_id; ?>')" style="color: #008F6C;"> <?php echo $room->room_no ;?></a></h4>
          <p><strong style="color: #2B3643;">Last Check In Date:</strong> <?php  $last_checkin_date=$this->dashboard_model->room_last_checkin_date($room->room_id);
                                 if(isset($last_checkin_date) && $last_checkin_date): ?>
          <?php foreach($last_checkin_date as $element){$date=date("d-M-Y",strtotime($element->cust_from_date));} ?>
          <span style="color: #008F6C;"><?php echo $date; ?></span>
          <?php endif; ?>
          <?php  $last_checkin_date=$this->dashboard_model->room_last_checkin_date($room->room_id);
                                if(!$last_checkin_date): ?>
          <span ><?php echo "N/A"; ?></span>
          <?php endif; ?></p>
          <p style="margin-bottom:0;"><strong style="color: #2B3643;">Hosekeeping Status:</strong> <a style="color: #008F6C;" onClick="redirect_hk()"href="javascript:void(0);">
			<?php 
				if(isset($status)) { echo $status;} 
				else echo '<span style="color:#EF4343;">Undefined!';
			?></a>
		  </p>
      </div>
    </div>
    <div class="ho-des pull-right" style="width:58%;">
        <ul class="list-group">
            <li class="list-group-item" style="color:black"> <?php echo ucwords($room2->meal_plan)." | ".$room2->rate_plan_type_name." | ".$room2->source_type_name." | ".$room2->occupancy_type."(Occupancy)"; ?></li>
            <li class="list-group-item"><strong>Base Room Rent:</strong> <i class="fa fa-inr"></i> <?php echo $room2->b_room_charge ?></li>
            <li class="list-group-item"><strong>Weekend Room Rent:</strong> <i class="fa fa-inr"></i> <?php echo $room2->w_room_charge ?></li>
            <li class="list-group-item"><strong>Seasonal Room Rent:</strong> <i class="fa fa-inr"></i> <?php echo $room2->s_room_charge ?></li>
      		<?php $unit=$this->dashboard_model->get_unit_exact($room->unit_id); ?>
            <li class="list-group-item"><strong>Floor:</strong> <?php echo $room->floor_no ?></li>
            <li class="list-group-item"><strong>Unit Type:</strong> <?php echo $unit->unit_name ?></li>
            <li class="list-group-item"><strong>Unit Class:</strong> <?php echo $unit->unit_class ?></li>
            <li class="list-group-item"><strong>Added to OTA(s):</strong> NO</li>
        </ul>  
        <?php $features=$this->dashboard_model->room_feature_all($room_id); ?>
            <div class="btn-group-sm" style="padding-top:12px;">
              <?php if(isset($features) && $features){
                                            $feature_array=array(
                                                0 =>0,
                                                1 =>0,
                                                2 =>0,
                                                3=>0,
                                                4=>0,
                                                5=>0,
                                                6=>0,
                                                7=>0,
                                                8=>0,
                                                9=>0,
                                                10=>0,
                                                11=>0,
                                                12=>0,
            
                                            );
											$count=count($features);
                                            $i=1;
                                            for($i=1;$i<12;$i++){
                                            foreach($features as $feature){
            
                                                if($feature->room_feature_id ==$i){
            
                                                    $feature_array[$i]=1;
            
            
                                                }
            
            
                                                 } }}
            
            
            
                                            ?>
              <?php if(isset($feature_array) && $feature_array){ ?>
              <?php if($feature_array[1] ==1){?>
              <button type="button" class="btn green-meadow btn-xs">AC</button>
              <?php  }else{ ?>
              <button type="button" class="btn grey-cascade btn-xs">AC</button>
              <?php } ?>
              <?php if($feature_array[2] ==1){?>
              <button type="button" class="btn green-meadow btn-xs">TV</button>
              <?php  }else{ ?>
              <button type="button" class="btn grey-cascade btn-xs">TV</button>
              <?php } ?>
              <?php if($feature_array[6] ==1){?>
              <button type="button" class="btn green-meadow btn-xs">GY</button>
              <?php  }else{ ?>
              <button type="button" class="btn grey-cascade btn-xs">GY</button>
              <?php } ?>
              <?php if($feature_array[3] ==1){?>
              <button type="button" class="btn green-meadow btn-xs">EB</button>
              <?php  }else{ ?>
              <button type="button" class="btn grey-cascade btn-xs">EB</button>
              <?php } ?>
              <?php if($feature_array[4] ==1){?>
              <button type="button" class="btn green-meadow btn-xs">BL</button>
              <?php  }else{ ?>
              <button type="button" class="btn grey-cascade btn-xs">BL</button>
              <?php } ?>
              <?php if($feature_array[5] ==1){?>
              <button type="button" class="btn green-meadow btn-xs">PV</button>
              <?php  }else{ ?>
              <button type="button" class="btn grey-cascade btn-xs">PV</button>
              <?php } ?>
              <?php if($feature_array[7] ==1){?>
              <button type="button" class="btn green-meadow btn-xs">RH</button>
              <?php  }else{ ?>
              <button type="button" class="btn grey-cascade btn-xs">RH</button>
              <?php } ?>
              <?php if($feature_array[8] ==1){?>
              <button type="button" class="btn green-meadow btn-xs">HS</button>
              <?php  }else{ ?>
              <button type="button" class="btn grey-cascade btn-xs">HS</button>
              <?php } ?>
			  
			  <?php if($feature_array[9] ==1){?>
              <button type="button" class="btn green-meadow btn-xs">WI-FI</button>
              <?php  }else{ ?>
              <button type="button" class="btn grey-cascade btn-xs">WI-FI</button>
              <?php } ?>
			  
			  <?php if($feature_array[10] ==1){?>
              <button type="button" class="btn green-meadow btn-xs">Bath Tub</button>
              <?php  }else{ ?>
              <button type="button" class="btn grey-cascade btn-xs">Bath Tub</button>
              <?php } ?>
			  
			  <?php if($feature_array[11] ==1){?>
              <button type="button" class="btn green-meadow btn-xs">24 SRV</button>
              <?php  }else{ ?>
              <button type="button" class="btn grey-cascade btn-xs">24 SRV</button>
              <?php } ?>
			  
			  <?php if($feature_array[11] ==1){?>
              <button type="button" class="btn green-meadow btn-xs">Fridge</button>
              <?php  }else{ ?>
              <button type="button" class="btn grey-cascade btn-xs">Fridge</button>
              <?php } ?>
			  
			  
			  
              <?php }else{?>
              <button type="button" class="btn grey-cascade">AC</button>
              <button type="button" class="btn grey-cascade">TV</button>
              <button type="button" class="btn grey-cascade">GY</button>
              <button type="button" class="btn grey-cascade">EB</button>
              <button type="button" class="btn grey-cascade">Bl</button>
              <button type="button" class="btn grey-cascade">PV</button>
              <button type="button" class="btn grey-cascade">RH</button>
              <button type="button" class="btn grey-cascade">HS</button>
			  
              <button type="button" class="btn grey-cascade">WI-FI</button>
              <button type="button" class="btn grey-cascade">Bath Tub</button>
              <button type="button" class="btn grey-cascade">24 SRV</button>
              <button type="button" class="btn grey-cascade">Fridge</button>
              <?php
            
            
                                        } ?>
            </div>        
    </div> 
  </div>
</div>
</div>
<script>
function redirect(room_id){
	//alert(room_id);
	var url = '<?php echo base_url();?>';
	  parent.DayPilot.ModalStatic.close();
	  window.top.location.href= url+"dashboard/edit_room?room_id="+room_id;
}
function close(result) {
        if (parent && parent.DayPilot && parent.DayPilot.ModalStatic) {
            parent.DayPilot.ModalStatic.close(result);
        }
    }
function redirect_hk(){
	//alert(room_id);
	var url = '<?php echo base_url();?>';
	  parent.DayPilot.ModalStatic.close();
	  window.top.location.href= url+"dashboard/housekeeping";
}
function go_edit(id){
   
	parent.parent.window.location.replace("<?php echo base_url() ?>dashboard/edit_room?room_id="+id );
    
}
</script>
</body>