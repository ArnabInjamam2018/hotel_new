<!-- BEGIN PAGE CONTENT-->
<div class="profile">
    <!--BEGIN TABS-->
    <div class="tabbable-line tabbable-full-width">
      <ul class="nav nav-tabs">
        <li class="active"> <a href="#tab_1_1" data-toggle="tab"> Profile <?php //$browser = get_browser();?> </a> </li>
		 <li> <a href="#tab_1_6" data-toggle="tab"> Account </a> </li>       
      </ul>
	  
	  <?php if(isset($admin_name) && $admin_name){
				foreach($admin_name as $info){
					$first_name=$info->admin_first_name;
					$middle_name=$info->admin_middle_name;
					$last_name=$info->admin_last_name;
					$id=$info->admin_id;
					$admin_image=$info->admin_image;
					$city=$info->admin_city;
					$country=$info->admin_country;
					$dob=$info->admin_dob;
					$admin_id=$info->admin_id;
					$admin_img=$info->admin_image;
					$role=$info->dept_role;
					$admin_phone=$info->admin_phone1;
					$about_me=$info->about_me;
					$admin_email=$info->admin_email;					
					$dept=$info->dept;
					$class=$info->class;
					
				
					
					?>
					
					<input type="hidden" name="hid" id="hid" value="<?php echo $id;?>">
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1_1">
          <div class="row">
            <div class="col-md-2">
              <ul class="list-unstyled profile-nav">
                <li> 
                	<form action="#" role="form">
                        <div class="form-group">
                            <div class="fileinput fileinput-new" data-provides="fileinput" style="display:block;">
                                <div class="fileinput-new thumbnail" style="width: 100%; height: 150px;">
                                   <?php if(isset($admin_img)){?>
										<img src="<?php echo base_url()?>upload/admin/<?php echo $admin_img?>" alt="" /> 
								 
								   <?php }else{?>
								   
								    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
								   <?php }?>
								   </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Edit </span>
                                        <span class="fileinput-exists"> Edit </span>										
                                        <input type="file" onchange="change_profilePic(this.value,<?php echo $admin_id; ?>)" id="file" name="..."> </span> 										
                               </div>
                            </div>
                        </div>
                    </form>
                </li>
                <!--<li> <a href="javascript:;"> Projects </a> </li>
                <li> <a href="javascript:;"> Messages <span> 3 </span> </a> </li>
                <li> <a href="javascript:;"> Friends </a> </li>
                <li> <a href="javascript:;"> Settings </a> </li>--->
              </ul>
            </div>
			
			<script>
			
			$(document).ready(function(){
    $("#fileid").on('click',function(){
     var path=$('#fileid').val();
     var id=$('#hid').val();
	alert(path);alert(id);
		$.ajax({
                type:"POST",
                url: "<?php echo base_url();?>unit_class_controller/change_profile_pic",
                data:{path:path,id:id,dg_image:dg_image},
                success:function(data)
                {
					
                    swal({
                            
							title: data,
                            text: "",
                            type: "success"
                        });
                        
                }
            });
			
			//alert('3');
    });
});
				
			</script>
			
            <div class="col-md-10">
              <div class="row">
                <div class="col-md-8 profile-info">
			<h1><?php echo $first_name.''.$middle_name.' '.$last_name;
			}}?></h1>
                  <p> <?php echo $about_me;?> </p>
                  <p> <a href="javascript:;"> <?php echo $admin_email ?></a> </p>
                  <ul class="list-inline">
				  <?php $country_name= $this->unit_class_model->country_by_id($country);
					if(isset($country_name)){					
						$c=$country_name->country_name;
					}else{
						$c='';
					}
					
				  ?>
                    <li> <i class="fa fa-map-marker"></i> <?php echo $c;?> </li>
                    <li> <i class="fa fa-calendar"></i> <?php echo $dob; ?></li>
                    <li> <i class="fa fa-briefcase"></i> <?php echo $role; ?> </li>
                    <li> <i class="fa fa-star"></i> <?php echo $dept; ?> </li>
                    <li> <i class="fa fa-heart"></i> <?php echo $class ?> </li>
                  </ul>
                </div>
                <!--end col-md-8-->
                <div class="col-md-4">
                  <div class="portlet sale-summary  light">
                    <div class="portlet-title">
                      <div class="caption"> Monthly Summary </div>
                      <div class="tools"> <a class="reload" href="javascript:;"> </a> </div>
                    </div>
                    <div class="portlet-body">
                      <ul class="list-unstyled">
                        <li> <span class="sale-info"> Total Transaction <i class="fa fa-img-up"></i> </span> <span class="sale-num"> <?php $total_transaction=$this->dashboard_model->total_transaction();echo $total_transaction->total ?></span> </li>
                        <li> <span class="sale-info"> Single Bookings <i class="fa fa-img-down"></i> </span> <span class="sale-num">  <?php $singleTotal=$this->dashboard_model->single_booking_count(); echo $singleTotal; ?></span> </li>
                        <li> <span class="sale-info"> group Bookings </span> <span class="sale-num"><?php $groupTotal=$this->dashboard_model->group_booking_count();echo $groupTotal?> </span> </li>
                        <li> <span class="sale-info"> Total Logins</span> <span class="sale-num"> <?php $groupTotal=$this->dashboard_model->total_login();echo $groupTotal->total ?>  </span> </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <!--end col-md-4--> 
              </div>
              <!--end row-->
              <div class="tabbable-line tabbable-custom-profile">
                <ul class="nav nav-tabs">
                  <li class="active"> <a href="#tab_1_11" data-toggle="tab"> Recent Collection  </a> </li>
                  <li> <a href="#tab_1_12" data-toggle="tab"> Transaction Summary </a> </li>
                  <!--<li> <a href="#tab_1_13" data-toggle="tab"> Booking Summary </a> </li>-->
                  <li> <a href="#tab_1_14" data-toggle="tab"> Booking Summary </a> </li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1_11">
                    <div class="portlet-body">
                      <table class="table table-striped table-bordered table-advance table-hover">
                        <thead>
                          <tr>
                            <th> <i class="fa fa-briefcase"></i> Hotel </th>
                            <th> <i class="fa fa-briefcase"></i> Date</th>
                            
                            <th class="hidden-xs">  Transaction Type </th>
                            <th class="hidden-xs">  Income/expense </th>
                            <th> <i class="fa fa-bookmark"></i> Amount </th>
                            <!--<th> </th>-->
                          </tr>
                        </thead>
                        <tbody>
						<?php  if(isset($transactions) && $transactions){
							foreach($transactions as $transaction){
								
								$dec = 'none';
					if($transaction->t_status == 'Cancel')
						$dec = 'line-through';
					$d = date("d",strtotime($transaction->t_date)).date("m",strtotime($transaction->t_date)).date("y",strtotime($transaction->t_date));
                    $transaction_id='TA0'.$transaction->hotel_id.'/'.$d.'/'.$transaction->t_id;
					
					$hotelID=$this->session->userdata('user_hotel');
					$userID=$this->session->userdata('user_id');
					$cmpName=$this->bookings_model->get_cmp_name($hotelID);
								
							?>
                          <tr>
                            <td><a href="javascript:;"> <?php echo $cmpName->hotel_name; ?></a></td>
                            <td><a href="javascript:;"> <?php echo $transaction->t_date; ?></a></td>
                            
                            
                            <td class="hidden-xs"><?php echo  $transaction->transaction_type;?>  </td>
							<?php 
							$typ=$this->bookings_model->get_transactionType_by_id($transaction->transaction_type_id);?>
							<?php if(isset($typ) && $typ){
								$cat=$typ->hotel_transaction_type_cat;
								
							}else{
								$cat='NA';
								//$amo=0;
							}?>
							<td class="hidden-xs"><?php echo  $cat;?>  </td>
                            <td> <?php echo $transaction->t_amount; ?> <span class="label label-success label-sm"> Paid </span></td>
                            <!--<td><a class="btn default btn-xs green-stripe" href="javascript:;"> View </a></td>-->
                          </tr>
						<?php }}?>
                          
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!--tab-pane-->
				  
				  <!--TRANSACTION SUMMARY 10 Days -->
				   <div class="tab-pane" id="tab_1_12">
                    <div class="portlet-body">
                      <table class="table table-striped table-bordered table-advance table-hover">
                        <thead>
                          <tr>
                            <th> <i class="fa fa-briefcase"></i> Hotel </th>
                            <th> <i class="fa fa-briefcase"></i> Date</th>                            
                            <th> <i class="fa fa-bookmark"></i> Amount </th>
                            <!--<th> </th>-->
                          </tr>
                        </thead>
                        <tbody>
						<?php  if(isset($transactions_date) && $transactions_date){
							foreach($transactions_date as $transaction){
								
								$dec = 'none';
					if($transaction->t_status == 'Cancel')
						$dec = 'line-through';
					$d = date("d",strtotime($transaction->t_date)).date("m",strtotime($transaction->t_date)).date("y",strtotime($transaction->t_date));
                    $transaction_id='TA0'.$transaction->hotel_id.'/'.$d.'/'.$transaction->t_id;
					
					$hotelID=$this->session->userdata('user_hotel');
					$userID=$this->session->userdata('user_id');
					$cmpName=$this->bookings_model->get_cmp_name($hotelID);
								
							?>
                          <tr>
                            <td><a href="javascript:;"> <?php echo $cmpName->hotel_name; ?></a></td>
                            <td><a href="javascript:;"> <?php echo date("l jS F Y",strtotime($transaction->t_date)); ?></a></td>
                            <!--<td class="hidden-xs"><?php //echo  $transaction->transaction_type;?>  </td>-->
                            <td> <?php echo 'INR '.$transaction->t_amount; ?> <span class="label label-success label-sm"> Collected </span></td>
                            <!--<td><a class="btn default btn-xs green-stripe" href="javascript:;"> View </a></td>-->
                          </tr>
						<?php }}?>
                          
                        </tbody>
                      </table>
                    </div>
                  </div>
				  
				  <!-- END TRANSACTION SUMMARY -->
				 
				   <!--START BOOKING SUMMARY   -->
				 <!--<div class="tab-pane" id="tab_1_13">
                 	<div class="portlet-body">
                      <table class="table table-striped table-bordered table-advance table-hover">
                        <thead>
                          <tr>
                            
                            <th> <i class="fa fa-briefcase"></i> Booking id</th>
                            <th> <i class="fa fa-briefcase"></i> Booking Date</th>
                            <th> <i class="fa fa-briefcase"></i> Booking Type</th>
							
                           
                          </tr>
                        </thead>
                        <tbody>
						<?php //print_r($booking);
						if(isset($booking) && $booking){$i=1;
						foreach($booking as $bookings){
							
					
					$hotelID=$this->session->userdata('user_hotel');
					$userID=$this->session->userdata('user_id');
					$cmpName=$this->bookings_model->get_cmp_name($hotelID);
								
							?>
                          <tr>
                           
                            <td><a href="javascript:;"> <?php //echo $bookings->booking_id; ?></a></td>
                            <td><a href="javascript:;"> <?php //echo $bookings->addON; ?></a></td>
                            <td><a href="javascript:;"> <?php //if($i>5){echo "GROUP BOOKING";}else{echo "SINGLE BOOKING";} $i++; ?></a></td>
                            
                            
                          </tr>
						<?php }}?>
                          
                        </tbody>
                      </table>
                    </div>
                 </div>-->
				  <!--END  BOOKING SUMMARY  -->
				  
                  <div class="tab-pane" id="tab_1_14">
                      <div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
                        <ul class="feeds">
						
						<?php //print_r($booking);
						if(isset($booking) && $booking){$j=1;
						foreach($booking as $bookings){
							
					
					$hotelID=$this->session->userdata('user_hotel');
					$userID=$this->session->userdata('user_id');
					$cmpName=$this->bookings_model->get_cmp_name($hotelID);
								if($j>5){
									$cl="fa fa-users -o";
								}
								else{
									$cl="fa fa-user -o";
								}
							?>
                          
                           
                             
                          
                         
                            
                            
                         
						
                           
                            
                          <li>
                            <div class="col1">
                              <div class="cont">
                                <div class="cont-col1">
                                  <div class="label label-success"> <i class='<?php echo $cl; ?>'></i> </div>
                                </div><?php $hotelID=$this->session->userdata('user_hotel');
					$userID=$this->session->userdata('user_id');
					$cmp=$this->bookings_model->get_cmp_name($hotelID);
				//	echo '<pre>';
				//	print_r($bookings);
					?>
					
                                <div class="cont-col2">
                                  <div class="desc"> <?php echo $bookings->bid; ?>&nbsp;&nbsp;<span class="label label-danger label-sm"> <?php echo $cmp->hotel_name; ?> <i class="fa fa-share"></i> </span> </div>
                                </div>
								<div class="cont-col2">
                                  <div class="desc"> <?php echo $bookings->customerName; ?> </div>
                                </div>
								<div class="cont-col2">
                                  <div class="desc"> <?php echo $bookings->addON; ?> </div>
                                </div>
								<div class="cont-col2">
                                  <div class="desc">  <?php if($j>5){echo "GROUP BOOKING";}else{echo "SINGLE BOOKING";} $j++; ?> </div>
                                </div>
                              </div>
                            </div>
                            
                          </li>
						<?php }}?>
                          
                          
                        </ul>
                      </div>
                  </div>
                  <!--tab-pane--> 
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--tab_1_2-->
        <div class="tab-pane" id="tab_1_3">
          <div class="row profile-account">
            <div class="col-md-3">
              <ul class="ver-inline-menu tabbable margin-bottom-10">
                <li class="active"> <a data-toggle="tab" href="#tab_1-1"> <i class="fa fa-cog"></i> Personal info </a> <span class="after"> </span> </li>
                <li> <a data-toggle="tab" href="#tab_2-2"> <i class="fa fa-picture-o"></i> Change Avatar </a> </li>
                <li> <a data-toggle="tab" href="#tab_3-3"> <i class="fa fa-lock"></i> Change Password </a> </li>
                <li> <a data-toggle="tab" href="#tab_4-4"> <i class="fa fa-eye"></i> Privacity Settings </a> </li>
              </ul>
            </div>
            <div class="col-md-9">
              <div class="tab-content">
                <div id="tab_1-1" class="tab-pane active">
                  <form role="form" action="#">
				  <div class="form-group">
                      <label class="control-label">Admin Role</label>
                      <input type="text" placeholder="John" value="<?php echo $role; ?>" class="form-control"/>
                    </div>
					
                    <div class="form-group">
                      <label class="control-label">First Name</label>
                      <input type="text" placeholder="John"  class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Last Name</label>
                      <input type="text" placeholder="Doe" class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Mobile Number</label>
                      <input type="text" placeholder="+1 646 580 DEMO (6284)" class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Interests</label>
                      <input type="text" placeholder="Design, Web etc." class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Occupation</label>
                      <input type="text" placeholder="Web Developer" class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">About</label>
                      <textarea class="form-control" rows="3" placeholder="We are the best hotels in the world!!!"></textarea>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Website Url</label>
                      <input type="text" placeholder="http://www.mywebsite.com" class="form-control"/>
                    </div>
                    <div class="margiv-top-10"> <a href="javascript:;" class="btn green"> Save Changes </a> <a href="javascript:;" class="btn default"> Cancel </a> </div>
                  </form>
                </div>
                <div id="tab_2-2" class="tab-pane">
                  <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. </p>
                  <form action="#" role="form">
                    <div class="form-group">
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/> </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                        <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="...">
                          </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                      <div class="clearfix margin-top-10"> <span class="label label-danger"> NOTE! </span> <span> Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span> </div>
                    </div>
                    <div class="margin-top-10"> <a href="javascript:;" class="btn green"> Submit </a> <a href="javascript:;" class="btn default"> Cancel </a> </div>
					
                  </form>
                </div>
                
                <div id="tab_4-4" class="tab-pane">
                  <form action="#">
                    <table class="table table-bordered table-striped">
                      <tr>
                        <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
                        <td><label class="uniform-inline">
                            <input type="radio" name="optionsRadios1" value="option1"/>
                            Yes </label>
                          <label class="uniform-inline">
                            <input type="radio" name="optionsRadios1" value="option2" checked/>
                            No </label></td>
                      </tr>
                      <tr>
                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                        <td><label class="uniform-inline">
                            <input type="checkbox" value=""/>
                            Yes </label></td>
                      </tr>
                      <tr>
                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                        <td><label class="uniform-inline">
                            <input type="checkbox" value=""/>
                            Yes </label></td>
                      </tr>
                      <tr>
                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                        <td><label class="uniform-inline">
                            <input type="checkbox" value=""/>
                            Yes </label></td>
                      </tr>
                    </table>
                    <!--end profile-settings-->
                    <div class="margin-top-10"> <a href="javascript:;" class="btn green"> Save Changes </a> <a href="javascript:;" class="btn default"> Cancel </a> </div>
                  </form>
                </div>
              </div>
            </div>
            <!--end col-md-9--> 
          </div>
        </div>
        <!--end tab-pane-->
        <div class="tab-pane" id="tab_1_4">
          <div class="row">
            <div class="col-md-12">
              <div class="add-portfolio"> <span> 502 Items sold this week </span> <a href="javascript:;" class="btn icn-only green"> Add a new Project <i class="m-icon-swapright m-icon-white"></i> </a> </div>
            </div>
          </div>
          <!--end add-portfolio-->
          <div class="row portfolio-block">
            <div class="col-md-5">
              <div class="portfolio-text"> <img src="<?php echo base_url();?>assets/dashboard/assets/admin/pages/media/profile/logo_metronic.jpg" alt=""/>
                <div class="portfolio-text-info">
                  <h4>Metronic - Responsive Template</h4>
                  <p> Lorem ipsum dolor sit consectetuer adipiscing elit. </p>
                </div>
              </div>
            </div>
            <div class="col-md-5 portfolio-stat">
              <div class="portfolio-info"> Today Sold <span> 187 </span> </div>
              <div class="portfolio-info"> Total Sold <span> 1789 </span> </div>
              <div class="portfolio-info"> Earns <span> $37.240 </span> </div>
            </div>
            <div class="col-md-2">
              <div class="portfolio-btn"> <a href="javascript:;" class="btn bigicn-only"> <span> Manage </span> </a> </div>
            </div>
          </div>
          <!--end row-->
          <div class="row portfolio-block">
            <div class="col-md-5 col-sm-12 portfolio-text"> <img src="<?php echo base_url();?>assets/dashboard/assets/admin/pages/media/profile/logo_azteca.jpg" alt=""/>
              <div class="portfolio-text-info">
                <h4>Metronic - Responsive Template</h4>
                <p> Lorem ipsum dolor sit consectetuer adipiscing elit. </p>
              </div>
            </div>
            <div class="col-md-5 portfolio-stat">
              <div class="portfolio-info"> Today Sold <span> 24 </span> </div>
              <div class="portfolio-info"> Total Sold <span> 660 </span> </div>
              <div class="portfolio-info"> Earns <span> $7.060 </span> </div>
            </div>
            <div class="col-md-2 col-sm-12 portfolio-btn"> <a href="javascript:;" class="btn bigicn-only"> <span> Manage </span> </a> </div>
          </div>
          <!--end row-->
          <div class="row portfolio-block">
            <div class="col-md-5 portfolio-text"> <img src="<?php echo base_url();?>assets/dashboard/assets/admin/pages/media/profile/logo_conquer.jpg" alt=""/>
              <div class="portfolio-text-info">
                <h4>Metronic - Responsive Template</h4>
                <p> Lorem ipsum dolor sit consectetuer adipiscing elit. </p>
              </div>
            </div>
            <div class="col-md-5 portfolio-stat">
              <div class="portfolio-info"> Today Sold <span> 24 </span> </div>
              <div class="portfolio-info"> Total Sold <span> 975 </span> </div>
              <div class="portfolio-info"> Earns <span> $21.700 </span> </div>
            </div>
            <div class="col-md-2 portfolio-btn"> <a href="javascript:;" class="btn bigicn-only"> <span> Manage </span> </a> </div>
          </div>
          <!--end row--> 
        </div>
		 <div class="tab-pane" id="tab_1_5">
          <div class="row profile-account">
            <div class="col-md-3">
              <ul class="ver-inline-menu tabbable margin-bottom-10">
                <li class="active"> <a data-toggle="tab" href="#stat_1-1"> <i class="fa fa-cog"></i> Sales Summary </a> <span class="after"> </span> </li>
                <li> <a data-toggle="tab" href="#stat_2-2"> <i class="fa fa-picture-o"></i> Collection Summary </a> </li>
                <li> <a data-toggle="tab" href="#stat_3-3"> <i class="fa fa-lock"></i> Other Actions Log </a> </li>
               
              </ul>
            </div>
            <div class="col-md-9">
              <div class="tab-content">
                <div id="stat_1-1" class="tab-pane active">
                  <form role="form" action="#">
                    <div class="form-group">
                      <label class="control-label">First Name</label>
                      <input type="text" placeholder="John" class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Last Name</label>
                      <input type="text" placeholder="Doe" class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Mobile Number</label>
                      <input type="text" placeholder="+1 646 580 DEMO (6284)" class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Interests</label>
                      <input type="text" placeholder="Design, Web etc." class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Occupation</label>
                      <input type="text" placeholder="Web Developer" class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">About</label>
                      <textarea class="form-control" rows="3" placeholder="We are the best hotels in the world!!!"></textarea> 
                    </div>
                    <div class="form-group">
                      <label class="control-label">Website Url</label>
                      <input type="text" placeholder="http://www.mywebsite.com" class="form-control"/>
                    </div>
                    <div class="margiv-top-10"> <a href="javascript:;" class="btn green"> Save Changes </a> <a href="javascript:;" class="btn default"> Cancel </a> </div>
                  </form>
                </div>
                <div id="stat_2-2" class="tab-pane">
                  <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. </p>
                 <form role="form" action="#">
                    <div class="form-group">
                      <label class="control-label">First Name</label>
                      <input type="text" placeholder="John" class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Last Name</label>
                      <input type="text" placeholder="Doe" class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Mobile Number</label>
                      <input type="text" placeholder="+1 646 580 DEMO (6284)" class="form-control"/>
                    </div>
                    
                    <div class="margiv-top-10"> <a href="javascript:;" class="btn green"> Save Changes </a> <a href="javascript:;" class="btn default"> Cancel </a> </div>
                  </form>
                </div>
                
                <div id="tab_4-4" class="tab-pane">
                  <form action="#">
                    <table class="table table-bordered table-striped">
                      <tr>
                        <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
                        <td><label class="uniform-inline">
                            <input type="radio" name="optionsRadios1" value="option1"/>
                            Yes </label>
                          <label class="uniform-inline">
                            <input type="radio" name="optionsRadios1" value="option2" checked/>
                            No </label></td>
                      </tr>
                      <tr>
                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                        <td><label class="uniform-inline">
                            <input type="checkbox" value=""/>
                            Yes </label></td>
                      </tr>
                      <tr>
                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                        <td><label class="uniform-inline">
                            <input type="checkbox" value=""/>
                            Yes </label></td>
                      </tr>
                      <tr>
                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                        <td><label class="uniform-inline">
                            <input type="checkbox" value=""/>
                            Yes </label></td>
                      </tr>
                    </table>
                    <!--end profile-settings-->
                    <div class="margin-top-10"> <a href="javascript:;" class="btn green"> Save Changes </a> <a href="javascript:;" class="btn default"> Cancel </a> </div>
                  </form>
                </div>
              </div>
            </div>
            <!--end col-md-9--> 
          </div>
        </div>
        <!--end tab-pane-->
        <div class="tab-pane" id="tab_1_6">
          <div class="row">
            <div class="col-md-3">
              <ul class="ver-inline-menu tabbable margin-bottom-10">
                <!--<li class="active"> <a data-toggle="tab" href="#tab_1"> <i class="fa fa-briefcase"></i> General Questions </a> <span class="after"> </span> </li>-->
                
				<li class="active"> <a data-toggle="tab" href="#tab_4"> <i class="fa fa-cog"></i> Personal info </a> <span class="after"> </span> </li>
               
                <li> <a data-toggle="tab" href="#tab_6"> <i class="fa fa-lock"></i> Change Password </a> </li>
                <!--<li> <a data-toggle="tab" href="#tab_7"> <i class="fa fa-eye"></i> Privacity Settings </a> </li>
				
				<li> <a data-toggle="tab" href="#tab_2"> <i class="fa fa-group"></i> Membership </a> </li>
                <li> <a data-toggle="tab" href="#tab_3"> <i class="fa fa-leaf"></i> Terms Of Service </a> </li>
                <li> <a data-toggle="tab" href="#tab_1"> <i class="fa fa-info-circle"></i> License Terms </a> </li>
                <li> <a data-toggle="tab" href="#tab_2"> <i class="fa fa-tint"></i> Payment Rules </a> </li>	-->			
				
                <!--<li> <a data-toggle="tab" href="#tab_3"> <i class="fa fa-plus"></i> Other Questions </a> </li>-->
              </ul>
            </div>
            <div class="col-md-9">
              <div class="tab-content">
                <div id="tab_1" class="tab-pane ">
                  <div id="accordion1" class="panel-group">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1"> 1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a> </h4>
                      </div>
                      <div id="accordion1_1" class="panel-collapse collapse in">
                        <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_2"> 2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a> </h4>
                      </div>
                      <div id="accordion1_2" class="panel-collapse collapse">
                        <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                      </div>
                    </div>
                    <div class="panel panel-success">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_3"> 3. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor ? </a> </h4>
                      </div>
                      <div id="accordion1_3" class="panel-collapse collapse">
                        <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                      </div>
                    </div>
                    <div class="panel panel-warning">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_4"> 4. Wolf moon officia aute, non cupidatat skateboard dolor brunch ? </a> </h4>
                      </div>
                      <div id="accordion1_4" class="panel-collapse collapse">
                        <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                      </div>
                    </div>
                    <div class="panel panel-danger">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_5"> 5. Leggings occaecat craft beer farm-to-table, raw denim aesthetic ? </a> </h4>
                      </div>
                      <div id="accordion1_5" class="panel-collapse collapse">
                        <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_6"> 6. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth ? </a> </h4>
                      </div>
                      <div id="accordion1_6" class="panel-collapse collapse">
                        <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_7"> 7. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft ? </a> </h4>
                      </div>
                      <div id="accordion1_7" class="panel-collapse collapse">
                        <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="tab_2" class="tab-pane" >
                  <div id="accordion2" class="panel-group">
                    <div class="panel panel-warning">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_1"> 1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a> </h4>
                      </div>
                      <div id="accordion2_1" class="panel-collapse collapse in">
                        <div class="panel-body">
                          <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </p>
                          <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-danger">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_2"> 2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a> </h4>
                      </div>
                      <div id="accordion2_2" class="panel-collapse collapse">
                        <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                      </div>
                    </div>
                    <div class="panel panel-success">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_3"> 3. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor ? </a> </h4>
                      </div>
                      <div id="accordion2_3" class="panel-collapse collapse">
                        <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_4"> 4. Wolf moon officia aute, non cupidatat skateboard dolor brunch ? </a> </h4>
                      </div>
                      <div id="accordion2_4" class="panel-collapse collapse">
                        <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_5"> 5. Leggings occaecat craft beer farm-to-table, raw denim aesthetic ? </a> </h4>
                      </div>
                      <div id="accordion2_5" class="panel-collapse collapse">
                        <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_6"> 6. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth ? </a> </h4>
                      </div>
                      <div id="accordion2_6" class="panel-collapse collapse">
                        <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_7"> 7. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft ? </a> </h4>
                      </div>
                      <div id="accordion2_7" class="panel-collapse collapse">
                        <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="tab_3" class="tab-pane">
                  <div id="accordion3" class="panel-group">
                    <div class="panel panel-danger">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_1"> 1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a> </h4>
                      </div>
                      <div id="accordion3_1" class="panel-collapse collapse in">
                        <div class="panel-body">
                          <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. </p>
                          <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. </p>
                          <p> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-success">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_2"> 2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a> </h4>
                      </div>
                      <div id="accordion3_2" class="panel-collapse collapse">
                        <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_3"> 3. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor ? </a> </h4>
                      </div>
                      <div id="accordion3_3" class="panel-collapse collapse">
                        <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_4"> 4. Wolf moon officia aute, non cupidatat skateboard dolor brunch ? </a> </h4>
                      </div>
                      <div id="accordion3_4" class="panel-collapse collapse">
                        <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_5"> 5. Leggings occaecat craft beer farm-to-table, raw denim aesthetic ? </a> </h4>
                      </div>
                      <div id="accordion3_5" class="panel-collapse collapse">
                        <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_6"> 6. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth ? </a> </h4>
                      </div>
                      <div id="accordion3_6" class="panel-collapse collapse">
                        <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_7"> 7. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft ? </a> </h4>
                      </div>
                      <div id="accordion3_7" class="panel-collapse collapse">
                        <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                      </div>
                    </div>
                  </div>
                </div>
				
				
				 <div id="tab_4" class="tab-pane active">
                  <form role="form" action="#">
				  <div class="form-group">
                      <label class="control-label">Admin Role</label>
                      <input type="text" placeholder="John" value="<?php echo $role; ?>" class="form-control" readonly/>
                    </div>
					
                    <div class="form-group">
                      <label class="control-label">First Name</label>
                      <input type="text" placeholder="John" id="account_first_name" value="<?php echo $first_name;?>" class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Last Name</label>
                      <input type="text" placeholder="Doe" id="account_last_name" class="form-control" value="<?php echo $last_name ?>" />
                    </div>
                    <div class="form-group">
                      <label class="control-label">Mobile Number</label>
                      <input type="text" placeholder="+1 646 580 DEMO (6284)" id="account_mobile" class="form-control" value="<?php echo $admin_phone ?>" />
                    </div>
                    <div class="form-group">
                      <label class="control-label">Interests</label>
                      <input type="text" placeholder="Design, Web etc." class="form-control"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Occupation</label>
                      <input type="text" placeholder="Web Developer" id="account_occupation" class="form-control" value="<?php echo $dept ?>"/>
                    </div>
					<div class="form-group">
                      <label class="control-label">DOB</label>
                      <input type="text" placeholder="Web Developer" id="account_dob" class="form-control  date-picker" value="<?php echo $dob ?>"/>
                    </div>
					
					<div class="form-group">
                      <label class="control-label">Country</label>
                     
					  <select id="account_country" class="form-control bs-select">
					 <?php $country1=$this->unit_class_model->fetch_country();
					 foreach($country1 as $counries){
					 ?>
                     <option  value="<?php echo $counries->id?>" <?php if($country==$counries->id) echo "selected";?>><?php echo $counries->country_name;?></option>                            
					 <?php }?>
                           </select>
                    </div>
					<div class="form-group">
                      <label class="control-label">Class</label>
                      <input type="text" placeholder="Class" id="account_class" class="form-control" value="<?php echo $class ?>"/>
                    </div>
                    <div class="form-group">
                      <label class="control-label">About</label>
                      <textarea class="form-control" rows="3" placeholder="We are the best hotels in the world!!!" id="account_about"  ><?php echo $about_me ?></textarea>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Website Url</label>
                      <input type="text" placeholder="http://www.mywebsite.com"  class="form-control" id="account_website" value="<?php echo $admin_email ?>" />	
                    </div>
					<input type="hidden" id="admin_hid" value="<?php echo $id;?>">
                    <div class="margiv-top-10"> <a href="javascript:;" class="btn green"> <span onclick="save_profile()">Save Changes</span> </a> <a href="javascript:;" class="btn default"> Cancel </a> </div>
                  </form>
                </div>
                <!--<div id="tab_5" class="tab-pane">
                  <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. </p>
                  <form action="#" role="form">
                    <div class="form-group">
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/> </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                        <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
                          <input type="file" name="...">
                          </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                      </div>
                      <div class="clearfix margin-top-10"> <span class="label label-danger"> NOTE! </span> <span> Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span> </div>
                    </div>
                    <div class="margin-top-10"> <a href="javascript:;" class="btn green"> Submit </a> <a href="javascript:;" class="btn default"> Cancel </a> </div>
					
                  </form>
                </div>-->
                <div id="tab_6" class="tab-pane pass-change">
				<?php //print_r($_SESSION); ?>
                    <?php
					$form = array(
						'class' 			=> 'form-horizontal',
						'id'				=> 'form',
						'method'			=> 'post',								
					);
					echo form_open_multipart('bookings/change_password',$form);
                    ?>
                    <div class="form-group" id="pos">
					  <span></span>
                    </div>					
                    <div class="form-group" id="ckp">
                      <label class="col-md-3 control-label">Current Password</label>
                      <div class="col-md-4">
                      	<div class="input-group">
                          <input type="password" class="form-control" required name="old_password" id="old_password" onblur="checkUser(this.value)"/>
                          <span class="input-group-addon"><i class="fa fa-check" aria-hidden="true" style="display:none;" id="chk"></i> <i class="fa fa-close" aria-hidden="true" style="display:none;" id="cle"></i></span>
                      	</div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3 control-label">New Password Hello</label>
                      <div class="col-md-4">
                      	<div class="input-group">
                      		<input type="password" class="form-control" required name="new_password" id="new_password"/>
                      	</div>
                      </div>
                    </div>
                    <div class="form-group" id="pds">
                      <label class="col-md-3 control-label">Re-type New Password</label>
                      <div class="col-md-4">
                      	<div class="input-group">
                          <input type="password" class="form-control" required name="confirm_password" id="confirm_password" onblur="confirmPassword(this.value)"/>
                          <span class="input-group-addon"><i class="fa fa-check" aria-hidden="true" style="display:none;" id="chk1"></i> <i class="fa fa-close" aria-hidden="true" style="display:none;" id="cle1"></i></span>
                      	</div>
                      </div>
                    </div>
                    <div class="margin-top-10"> 
					<button type="button" class="btn blue" disabled id="btnS" onclick="change_password()">Submit</button>  
					</div>
                  </form>
                </div>
                <div id="tab_7" class="tab-pane">
                  <form action="#">
                    <table class="table table-bordered table-striped">
                      <tr>
                        <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
                        <td><label class="uniform-inline">
                            <input type="radio" name="optionsRadios1" value="option1"/>
                            Yes </label>
                          <label class="uniform-inline">
                            <input type="radio" name="optionsRadios1" value="option2" checked/>
                            No </label></td>
                      </tr>
                      <tr>
                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                        <td><label class="uniform-inline">
                            <input type="checkbox" value=""/>
                            Yes </label></td>
                      </tr>
                      <tr>
                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                        <td><label class="uniform-inline">
                            <input type="checkbox" value=""/>
                            Yes </label></td>
                      </tr>
                      <tr>
                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                        <td><label class="uniform-inline">
                            <input type="checkbox" value=""/>
                            Yes </label></td>
                      </tr>
                    </table>
                    <!--end profile-settings-->
                    <div class="margin-top-10"> <a href="javascript:;" class="btn green"> Save Changes </a> <a href="javascript:;" class="btn default"> Cancel </a> </div>
                  </form>
                </div>
              </div>
            </div>
            <!--end col-md-9--> 
          </div>
        </div>
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
              </div>
            </div>
          </div>
        </div>
        <!--end tab-pane--> 
      </div>
    </div>
    <!--END TABS--> 
</div>
<!-- END PAGE CONTENT--> 
<script>
$(document).ready(function() {
//$('#textboxname').datepicker();
$('#account_dob').datepicker('setDate', 'today');});

$( document ).ready(function(){
	toastr.optionsOverride = 'positionclass = "toast-bottom-right"';
		toastr.options.positionClass = 'toast-bottom-right';
			
	})
	
function change_profilePic(id,admin_id){
	//alert(admin_id);
	//alert('hello world');
	 var input = document.getElementById("file");
  file = input.files[0];
  if(file != undefined){
    formData= new FormData();
    if(!!file.type.match(/image.*/)){
      formData.append("image", file);
      $.ajax({
        url: "<?php echo base_url()?>unit_class_controller/profile_pic_change/"+admin_id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
			//alert(data);
           // alert('success');
		   toastr.success('Update Successfully! - '+data, 'success!');
			
        }
      });
    }else{
      alert('Not a valid image!');
    }
  }else{
    alert('Input something!');
  }
		
			
		
	
}


function checkUser(val) {
	 //console.log('checkUser(val)');
	 if (val != ""){
     $.ajax({
      url: "<?php echo base_url()?>bookings/checkUser",
      type: "POST",
      data:'val='+val,
      success: function(data){
        console.log(data);
        if(data == 0){
              $("#old_password").val("");
			  console.log('Not Found');
			  //$("#ckp span").text("Invalid password.");
			  $("#ckp span").addClass("error-text");
			  $("#ckp span").removeClass("valid-text");
			  $("#chk").css('display', 'none');
			  $("#cle").css('display', 'block');
			  $('#old_password').css('background-color', '#FFFFFF');
			} else {
				
			  //$("#ckp span").text( 'Valid password.' );	
			  $("#ckp span").addClass("valid-text");
			  $("#ckp span").removeClass("error-text");
			  $("#cle").css('display', 'none');
			  $("#chk").css('display', 'block');
			  $('#old_password').css('background-color', '#27D0A1');
			}
      }        
      });
  } 
  else {
	swal("Please enter your current password");  
  }
  
}
function confirmPassword(val) {
   	var pwd = $("#new_password").val();
	//alert(pwd);
    if(val != pwd) {
	  $("#confirm_password").val("");
	  $("#pds span").text("Password & confirm password must be same.");
	  $("#btnS").prop('disabled', true);
	  $("#pds span").addClass("error-text");
	  $("#pds span").removeClass("valid-text");	  
    } else {
	  $("#btnS").prop('disabled', false);	
	  $("#pds span").text( 'Click on submit button to change password.' );	
	  $("#pds span").addClass("valid-text");
	  $("#pds span").removeClass("error-text");	  
	}
}

function change_password() {
	//alert("here");
	var npwd = $("#new_password").val();
	var opwd = $("#old_password").val();
	if (opwd == ""){
     swal("Please enter your current password.");
	 return false;
	} else { 
     $.ajax({
      url: "<?php echo base_url()?>bookings/change_password",
      type: "POST",
      data:{npwd:npwd , opwd:opwd},
      success: function(data){
        //console.log(data);
        if(data == 0){
			  $("#pos span").text("Database Error. Try again later!");
			  $("#old_password").val("");
			  $("#new_password").val("");
			  $("#confirm_password").val("");
			  $("#pds span").text( '' );
			  $("#ckp span").text( '' );
			  $("#btnS").prop('disabled', true);			  
			} else {
			  $("#pos span").text( 'Password updated Successfully.' );
			  $("#old_password").val("");
			  $("#new_password").val("");
			  $("#confirm_password").val("");
			  $("#pds span").text( '' );
			  $("#ckp span").text( '' );
              $("#btnS").prop('disabled', true);
			  $("#cle1").css('display', 'none');
			  $("#chk1").css('display', 'block');	
			  $('#old_password').css('background-color', '#FFFFFF');			  
			}
      }        
      });
 }	  
}
</script>
<script>

function save_profile(){
	//alert("hello");
	
	var first_name=$('#account_first_name').val();
	var last_name=$('#account_last_name').val();
	var mobile=$('#account_mobile').val();
	var occupation=$('#account_occupation').val();
	var about=$('#account_about').val();
	var website=$('#account_website').val();
	var account_class=$('#account_class').val();
	var id=$('#admin_hid').val();
	var country=$('#account_country').val();
	var dob=$('#account_dob').val();
	
	
$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/update_admin_profile",
                data:{account_class:account_class,id:id,first_name:first_name,last_name:last_name,mobile:mobile,occupation:occupation,about:about,website:website,country:country,dob:dob},
                success:function(data)
                {
                    //alert(data);
                    //location.reload();
                   swal({
                            title: "success",
                            text: "",
                            type: "success"
                        }
                 );
				 location.reload();
                }
            });
			

}
</script>