<!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
<?php endif;?>
<!-- 19.07.2016-->
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase">Quick Room Add</span> </div>
	</div>
	<div class="portlet-body form">
		<?php

		$form = array(
			'class' => '',
			'id' => 'form',
			'method' => 'post',
		);



		echo form_open_multipart( 'unit_class_controller/add_quick_room', $form );

		?>
		<div class="form-body">
			<div class="row">
				<div class="col-md-12" style="padding-top:30px;">
					<div class="">
						<table class="table table-striped table-bordered table-hover table-scrollable" id="items">
							<thead>
								<tr>
									<th width="18%">Image</th>
									<th width="10%">Room NO</th>
									<th>Room Name </th>
									<th width="15%">Unit category</th>
									<th width="15%">Unit Type</th>
									<th>Unit Class</th>
									<th width="10%">Bed Capacity</th>
									<th width="6%">Floor Id</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="form-group">
										<div class="fileinput fileinput-new" data-provides="fileinput">
											<span id="image" class="btn green btn-file btn-sm"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
											<input type="file" class="form-control" id="image" name="image[]" maxlength="10">
											</span> <span class="fileinput-filename"> </span> <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
										</div>
									</td>
									<td class="form-group">
										<input id="room_no" name="room_no[]" type="text" value="" placeholder="Room No" onchange="check_room_no(this.value)" class="form-control input-sm">
									</td>
									<td class="form-group">
										<input id="room_name" name="room_name[]" type="text" value="" class="form-control input-sm" placeholder="Room Name">
									</td>
									<td class="form-group">
										<select name="unit_category[]" id="unit_category" class="form-control input-sm bs-select" onchange="get_unit_name(this.value)">
											<option value="" disabled="" selected="">Unit Category</option>
											<?php foreach($category as $uName){ ?>
											<option value="<?php echo $uName->name;?>">
												<?php echo $uName->name;?>
											</option>
											<?php } ?>
										</select>
									</td>
									<td class="form-group" id="unit_type1">
										<select class="form-control bs-select" id="unit_type" name="unit_name" required="required" onchange="get_unit_class1(this.value)" disabled>
											<option value="" disabled="" selected="">Unit Type</option>
										</select>
									</td>
									<td class="form-group" id="unitClass">
										<input type="text" autocomplete="off" class="form-control input-sm" readonly id="unit_class" name="unit_class[]" placeholder="Unit Class *">
									</td>
									<td class="form-group">
										<select name="bed_capacity[]" id="bed_capacity" class="form-control input-sm bs-select" placeholder="Beg Capacity">
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
									</td>

									<td class="form-group">
										<select name="floor_id[]" id="floor_id" class="form-control input-sm bs-select" placeholder="Floor Id">
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
									</td>
									<td> <a class="btn green btn-xs" id="additems"><i class="fa fa-plus" aria-hidden="true"></i></a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="form-actions right">
			<button type="submit" class="btn submit">Submit</button>
			<!-- 18.11.2015  -- onclick="return check_mobile();" -->
			<button type="reset" class="btn default">Reset</button>
		</div>
		<input type="hidden" name="hid">
		<?php form_close(); ?>
		<!-- END CONTENT -->

	</div>
</div>
<script>
	function removeRow( id ) {

		swal( {
			title: "Are you sure?",
			text: "Do you want to remove the room!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {
			swal( "Deleted!", "Yes delete the row.", "success" );
			$( '#row_' + id ).remove();
		} );
	}

	var flag = 0;
	$( document ).on( 'blur', '#form_control_11', function () {
		$( '#form_control_11' ).addClass( 'focus' );
	} );
	$( document ).on( 'blur', '#form_control_12', function () {
		$( '#form_control_12' ).addClass( 'focus' );
	} );

	var ff = flag;
	var check = $( '#contract_start_date' + ff ).val();
	var a = $( '#contract_end_date' ).val()
		//if(check<=a){


	$( "#additems" ).click( function () {

		var x = 0;
		var image = $( '#image' ).val();
		var room_no = $( '#room_no' ).val();
		var room_name = $( '#room_name' ).val();
		var unit_type = $( '#unit_type' ).val();
		var unit_category = $( '#unit_category' ).val();
		var unit_class = $( '#unit_class' ).val();
		var bed_capacity = $( '#bed_capacity' ).val();
		var base_rent = $( '#base_rent' ).val();
		var max_occupancy = $( '#max_occupancy' ).val();

		var floor_id = $( '#floor_id' ).val();

		//alert(image);

		if ( room_no != '' && unit_type != '' && unit_category != '' ) {

			//$('#ids').val( it+','+ i_name) ;
			$( '#items tr:first' ).after( '<tr id="row_' + flag + '">' +
				'<td><input name="image1[]" id="contract_start_date' + flag + '"  type="file" value="' + image + '" class="form-control input-sm" readonly></td>' +
				'<td><input name="room_no1[]" id="contract_start_date' + flag + '" type="text" value="' + room_no + '" class="form-control input-sm" readonly></td>' +
				'<td><input name="room_name1[]" id="contract_start_date' + flag + '" type="text" value="' + room_name + '" class="form-control input-sm" readonly></td>' +
				'<td><input name="unit_category1[]" id="contract_start_date' + flag + '" type="text" value="' + unit_category + '" class="form-control input-sm" readonly></td>' +
				'<td><input name="unit_type1[]" id="contract_start_date' + flag + '" type="text" value="' + unit_type + '" class="form-control input-sm" readonly></td>' +
				'<td><input name="unit_class1[]" id="contract_start_date' + flag + '" type="text" value="' + unit_class + '" class="form-control input-sm" readonly></td>' +
				'<td><input name="bed_capacity1[]" id="contract_start_date' + flag + '" type="text" value="' + bed_capacity + '" class="form-control input-sm" readonly></td>' +
				//'<td><input name="base_rent1[]" id="contract_start_date'+flag+'" type="text" value="'+base_rent+'" class="form-control input-sm" readonly></td>'+
				'<td><input name="floor_id1[]" id="contract_start_date' + flag + '" type="text" value="' + floor_id + '" class="form-control input-sm" readonly></td>' +
				//'<td><input name="max_occupancy1[]" id="contract_start_date'+flag+'" type="text" value="'+max_occupancy+'" class="form-control input-sm" readonly></td>'+

				'<td><a href="javascript:void(0);" class="btn red btn-sm" onclick="removeRow(' + flag + ')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>' );
			x = x + 1;
			flag++;
			$( '#room_no' ).val( '' );
			$( '#room_name' ).val( '' );
			$( '#unit_type' ).val( '' );
			$( '#unit_class' ).val( '' );
			$( '#bed_capacity' ).val( '' );
			$( '#max_occupancy' ).val( '' );
			$( '#unit_category' ).val( '' );
			$( '#base_rent' ).val( '' );

			swal( "Success! Please click submit to add room", "Room is Set to create! ", "success" )

		} else {
			swal( 'enter value properly' );
		}
	} );

	function check_date() {
		var a = $( '#contract_start_date' ).val();
		a = new Date( a );
		var b = $( '#contract_end_date' ).val();
		b = new Date( b );
		if ( a >= b && b != '' ) {
			alert( 'B stop' );
			$( '#contract_end_date' ).val( '' );
		}

		var c;
		if ( flag > 0 ) {
			c = $( '#contract_start_date' ).val();
			c = new Date( c );
			if ( a.getTime() >= c.getTime() || b.getTime() >= c.getTime() ) {
				alert( 'C stop' );
				$( '#contract_start_date' ).val( '' );
			}
		}

	}

	function get_unit_class( val ) {
		//alert(val);
		$.ajax( {
			type: "POST",
			url: "<?php echo base_url();?>dashboard/get_unit_class",
			data: "val=" + val,
			success: function ( msg ) {
				$( "#reg_type1" ).html( msg );
			}
		} );
	}
</script>
<script>
	function check_room_no( no ) {
		$.ajax( {
			type: "POST",
			url: "<?php echo base_url();?>unit_class_controller/check_room_no",
			data: {
				no: no
			},
			success: function ( data ) {
				if ( data.ans == 'Not Available' ) {
					//alert("wesfewsfw");
					swal( "warning", "Room is not available!", "error" );
					$( '#room_no' ).val( '' );
				}

			}
		} );
	}

	function get_unit_name( val ) {
		console.log( val );
		$.ajax( {
			type: "POST",
			url: "<?php echo base_url();?>dashboard/get_unit_name",
			data: {
				val: val
			},
			success: function ( msg ) {
				console.log( 'msg - ' + msg );
				$( "#unit_type1" ).html( msg );
				$.getScript( '<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js' );
			}
		} );
	}

	function get_unit_class1( val ) {
		//alert(val);
		$.ajax( {
			type: "POST",
			url: "<?php echo base_url();?>dashboard/get_unit_class",
			data: "val=" + val,
			success: function ( msg ) {
				//alert(msg);
				$( "#unitClass" ).html( msg );
			}
		} );
	}

	function showType( val ) {
		//alert(val);
		$( "#featureType_" + val ).toggle();
	}
	$( document ).ready( function () {
		$( '.showhidetarget' ).hide();
	} );

	function getVal( id, val ) {
		//alert(id);
		//alert(val);
		if ( val == '1' ) {
			document.getElementById( "samt_" + id ).style.display = "block";
			document.getElementById( "uamt_" + id ).value = "1";
		} else {
			document.getElementById( "uamt_" + id ).value = "0";
			document.getElementById( "amt_" + id ).value = "";
			document.getElementById( "samt_" + id ).style.display = "none";

		}
	}

	function get_additional_value( val ) {
		if ( val == '1' ) {
			document.getElementById( "smt" ).style.display = "block";
		} else {
			document.getElementById( "smt" ).style.display = "none";

		}
	}
</script>