<!-- 17.11.2015-->
<?php
/*echo "<pre>";
print_r($rates);
echo "</pre>";
exit();
*/
?>
<style>
.red {
	background-color: red;
}
</style>
<?php if($this->session->flashdata('err_msg')):?>
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<!-- 17.11.2015-->
<div class="portlet box blue">
<div class="portlet-title">
  <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase">Rate Plan Setting</span> </div>
</div>
<div class="portlet-body">
  <div class="table-toolbar">
    <div class="row">
      <div class="col-md-6"> 
        <!--  <button  class="btn red" onclick="all_rate_plans()" > Populate Data </button>-->
		<?php
		$populate_track = $this->rate_plan_model->rate_plan_populate_tracking($this->session->userdata('user_hotel'));
		
		if($populate_track->populate_status == '0' || $populate_track->populate_status == '' || $populate_track->populate_status == null){
			
			$rate_plan_status = $populate_track->populate_status;
			$rate_plan_status_name = "Populate Data";
			$populate_class =  "btn red";
		}
		else{
			$rate_plan_status = $populate_track->populate_status;
			$rate_plan_status_name = "Reset Data";
			$populate_class =  "btn blue";
		}
		
		?>
        <button  class="<?php echo $populate_class;  ?>" id="populate" value="<?php echo $rate_plan_status;  ?>"> <?php echo $rate_plan_status_name; ?></button>
			
        </button>
      </div>
    </div>
  </div>
  <?php
    $form = array(
        'class' 			=> '',
        'id'				=> 'form',
        'method'			=> 'post',
            
    );
    //echo form_open_multipart('rate_plan/rate_plan_setting_view',$form);
    
    ?>
  <div class="row">
    <?php $data=$this->rate_plan_model->get_plan_setting_by_h_id();
			foreach($data as $as){
		 // $meal1=$as->tax_applied_mealplan;
		  ?>
    <div class="form-horizontal">
      <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <label class="col-md-8 control-label" for="form_control_1">Tax Applied?</label>
          <div class="col-md-4">
            <div class="md-radio-inline">
              <div class="switch">
                <?php
                                 $tax=$as->tax_applied	;
                                 
                                 ?>
                <input type="checkbox" <?php if($tax){echo "checked";} ?>   id="chk6" onchange="applied_tax()" class="md-check cmn-toggle cmn-toggle-round " name="access_report">
                <label for="chk6"></label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <label class="col-md-8 control-label" for="form_control_1">Tax Applied on meal Plan?</label>
          <div class="col-md-4">
            <div class="md-radio-inline">
              <div class="switch">
                <?php
     $tax_meal=$as->tax_applied_mealplan	;
     ?>
                <input type="checkbox" <?php if($tax_meal){echo "checked";}  ?>   id="chk7" onchange="applied_tax_mealplan()" class="md-check cmn-toggle cmn-toggle-round " name="access_report">
                <label for="chk7"></label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <label class="col-md-8 control-label" for="form_control_1">Use Default occupancy rate </label>
          <div class="col-md-4">
            <div class="md-radio-inline">
              <div class="switch">
                <?php
     $occupancy=$as->default_occupancy	;
     ?>
                <input type="checkbox" <?php if($occupancy){echo "checked";} ?>   id="chk8" onchange="default_occupancy()" class="md-check cmn-toggle cmn-toggle-round " name="access_report">
                <label for="chk8"></label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group form-md-line-input">
        <input autocomplete="off" type="number" class="form-control" value="<?php echo $as->total_room_rent_tax ?>" onblur="update_room_rent_tax(this.value)" id="room_rent_tax" name="room_rent_tax" placeholder="Total Room Rent Tax *">
        <label></label>
        <span class="help-block">Total Room Rent Tax *</span> </div>
    </div>
    <div class="col-md-4">
      <div class="form-group form-md-line-input" id="unitClass">
        <input autocomplete="off" type="text" onkeypress=" return onlyNos(event, this);" class="form-control" value="<?php echo $as->total_meal_plan_tax;?>" <?php if($tax_meal=='0'){ echo "disabled";} ?> onblur="update_meal_plan_tax(this.value)" id="meal_plan_tax1" name="meal_plan_tax" placeholder="Total Meal Plan Tax *">
        <label></label>
        <span class="help-block">Total Meal Plan Tax</span> </div>
    </div>
    <div class="col-md-4">
      <div class="form-group form-md-line-input" id="unitClass">
        <select  class="form-control bs-select" onchange="update_default(this.value)"  id="default" name="default" required >
          <option value="" >Select Default</option>
          <?php $season_name=$this->rate_plan_model->all_rate_plan_type();
							foreach($season_name as $season){
								$a=$season->name;
								$id=$season->rate_plan_type_id;

						?>
          <option value="<?php echo $a; ?>" <?php if($as->season_default == $a) {echo "selected";} ?>> <?php echo $a; ?></option>
          <?php }?>
        </select>
      </div>
    </div>
    <?php }?>
    <div class="col-md-12">
      <h4 style="margin-top:20px;"> Season Mapping </h4>
      <table class="table table-striped table-hover table-bordered " id="items">
        <thead>
          <tr> 
            <!--<th width="10%">Image</th>-->
            <th >Season </th>
            <th >Start date </th>
            <th >End Date</th>
            <th align="center" width="3%">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $season_mapping=$this->rate_plan_model->all_season_mapping(); 
                foreach($season_mapping as $mapping){

                	$season_id=$mapping->season_mapping_id;
                	$name=$mapping->season_name;
                	$start=$mapping->start_date;
                	$end=$mapping->end_date;
					$s_id=$mapping->season_mapping_id;



                ?>
          <tr id="tr_<?php echo $s_id; ?>">
            <td class="form-group"><select class="form-control bs-select"  id="season1" name="season[]" required >
                <option value=""  disabled="disabled" >Default nature visit</option>
                <?php $season_name=$this->rate_plan_model->all_rate_plan_type();
							foreach($season_name as $season){
								$a=$season->name;
								$id=$season->rate_plan_type_id;

						?>
                <option value="<?php echo $a; ?>" <?php if($a==$name){echo "selected";} ?>><?php echo $a; ?></option>
                <?php }?>
              </select></td>
            <td class="form-group"><input  id="contract_start_date1" name="start_date[]" type="text" value="<?php echo $start; ?>"  class="form-control date-picker"   placeholder="Date *"  ></td>
            <td class="form-group"><input  id="contract_end_date1" name="end_date[]" type="text" value="<?php echo $end; ?>"   class="form-control date-picker"   placeholder="end_date *" ></td>
            
            <!--<a class="btn blue btn-sm"  id="additems"><i class="fa fa-plus" aria-hidden="true"></i></a>
					 <a class="btn green btn-sm"  id="additems"><i class="fa fa-pencil" aria-hidden="true"></i></a> 
				  </td>-->
            
            <td><a href="javascript:void(0);" class="btn red btn-xs" onclick="removeRow('<?php echo $s_id ?>')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
          </tr>
          <?php }?>
          <tr id="">
            <td class="form-group"><select class="form-control bs-select"  id="season2" name="season1" required >
                <option value=""   selected >Select</option>
                <?php $season_name=$this->rate_plan_model->all_rate_plan_type();
							foreach($season_name as $season){
								$a=$season->name;
								$id=$season->rate_plan_type_id;

						?>
                <option value="<?php echo $a; ?>"><?php echo $a; ?></option>
                <?php }?>
              </select></td>
            <td class="form-group"><input  id="start_date_p" name="start_date_p" type="text"  class="form-control date-picker"   placeholder="Date *"  ></td>
            <td class="form-group"><input  id="end_date_p" name="end_date_p" type="text"    class="form-control date-picker"   placeholder="end_date *"  ></td>
            <td><a class="btn blue btn-xs"  id="additems"><i class="fa fa-plus" aria-hidden="true"></i></a> 
              <!-- <a class="btn green btn-sm"  id="additems"><i class="fa fa-pencil" aria-hidden="true"></i></a> --></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
<!--  modal addition ..-->

<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Rate plan Settings</h4>
      </div>
      <!--    <input type="hidden"  name="taxid" id="taxid"  value=''>-->
      
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="rate_pwd" id="rate_pwd" required placeholder="Enter password">
              <label></label>
              <span class="help-block">Enter password..</span> </div>
          </div>
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn default">Close</button>
            <button type="button" class="btn green" onclick="all_rate_plans()">Submit</button>
          </div>
          <?php
		 
		//$tax_rule =  $this->dashboard_model->get_all_tax_rule();
		//print_r($tax_rule);
		//foreach($tax_rule as $tax){
		 ?>
          <label></label>
          <label></label>
          <label></label>
          <?php
		//}
		 ?>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- end of modal---> 
<script>

$(document).ready(function(){
	//alert('populate');
	$('#populate').on('click',function(){
		//alert("populate");
		$('#responsive').modal('toggle');
	});
});
</script> 
<script>
function default_occupancy(){
	var a=1;
	//alert('hello');
	$.ajax({
		   type: "POST",
		   url: "<?php echo base_url();?>rate_plan/default_occupancy",
		    data:{a:a},
		   success: function(data){
			   
			   
		   }//,async: false
		});
}

function applied_tax(){
	var a=1;
	//alert('hello');
	$.ajax({
		   type: "POST",
		   url: "<?php echo base_url();?>rate_plan/toggle_applied_tax",
		    data:{a:a},
		   success: function(data){
			   
			   //alert(data);
		   }//,async: false
		});
}

function applied_tax_mealplan(){
	var a=1;
	//alert('hello');
	$.ajax({
		   type: "POST",
		   url: "<?php echo base_url();?>rate_plan/toggle_applied_tax_mealplan",
		    data:{a:a},
		   success: function(data){
			    //alert(data.meal);
			   if(data.meal=='0'){
				  
				   $('#meal_plan_tax1').val(0);
				   $('#meal_plan_tax1').attr("disabled", "disabled"); 
			   }
			   else{
				   $('#meal_plan_tax1').attr("disabled", false); 
			   }
			   
		   }//,async: false
		});
}

function update_room_rent_tax(room_rent){
	if(room_rent<100 && room_rent>0 ){
	$.ajax({
		   type: "POST",
		   url: "<?php echo base_url();?>rate_plan/update_room_rent_tax",
		    data:{room_rent:room_rent},
		   success: function(data){
		   }//,async: false
		});
	}else{
		alert("Invalid Entry");
		$('#room_rent_tax').val('');
	}
	
}


function update_meal_plan_tax(meal){
	if(meal<100 && meal>0 ){
	$.ajax({
		   type: "POST",
		   url: "<?php echo base_url();?>rate_plan/update_meal_plan_tax",
		    data:{meal:meal},
		   success: function(data){
		   }//,async: false
		});
	}
	else{
		alert("Invalid Entry");
		$('#meal_plan_tax').val('');
	}
}


function removeRow(id){
 	alert(id);
 	//alert(total);
	
	
	

	swal({ 
	title: "Are you sure?",   text: "Do you want to remove the row!",
	type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false
	},
	function(){ 
	swal("Deleted!", "Yes delete the row.", "success");
	//REMOVE FROM DATABASE
	
	$.ajax({
		   type: "POST",
		   url: "<?php echo base_url();?>rate_plan/delete_season_mapping_by_id",
		    data:{id:id},
		   success: function(data){
		   }//,async: false
		});
	
	//END
	
	$('#tr_'+id).remove();
	});
	
	
	
	
	}
	

</script> 
<script>

	
 function check_due(val){
	 //alert(val);
	 var due=$("#payments_due").val(val).val();
	 var pay_amt=$('#pay_amount').val();
	 var ch_pay_amt=$('#chng_pay_amt').val();
	 
	 	//alert(ch_pay_amt);
	if(val==due){
		$("#paments_status").attr('value','0').change();
	}
 }
           $(document).ready(function() {
                        cellWidth = 100;
                        cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                       
                    });

            $("#autocellwidth").click(function() {
                      cellWidth = 100;  // reset for "Fixed" mode
                        cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                      /*  document.getElementById('auto_cl_id').style.backgroundColor = $(this).is(":checked") ? "#00CC99" : "#A5A5A5";
                        var a = $(this).is(":checked") ? "Pay Later" : "Pay Now";
						//alert(a);
						if(a=="Pay Later"){
							
							document.getElementById('pay').style.display = 'none';
	                        document.getElementById('pay1').style.display = 'block';
							
						}else{
							document.getElementById('pay').style.display = 'block';
	                        document.getElementById('pay1').style.display = 'block';
						}
						var dis=$('#disp').text(a);
					
                    */
					document.getElementById('auto_cl_id').style.backgroundColor = $(this).is(":checked") ? "#297CAC" : "#39B9A1";
                        var a = $(this).is(":checked") ? "Pay Later" : "Pay Now";
						if(a == 'Pay Later')
						{	
							document.getElementById('pay').style.display = 'block';
	                        document.getElementById('pay1').style.display = 'block';
						}
						else
						{	
							
						    document.getElementById('pay').style.display = 'none';
	                        document.getElementById('pay1').style.display = 'block';
						}
						
						$('#disp').text(a);

					});
					
					

   
   
 
	
   var sum=0; 
  var flag=0;
  var flag1=0;

  
   $("#additems").click(function(){
var season=$('#season2').val();
var start=$('#start_date_p').val();
var end=$('#end_date_p').val();
     
	if(season!=null && season!=''){
$.ajax({
		   type: "POST",
		   url: "<?php echo base_url();?>rate_plan/add_season_mapping",
		    data:{season:season,start:start,end:end},
		   success: function(data){
			   
			 //  alert(data);
		/*$('#items tr:first').after('<tr id="row_'+flag+'">'+
	'<td class="hidden-480"><input name="cloth_type[]" id="cloth_type'+flag+'" type="text" value="'+data.name+'" class="form-control input-sm" ></td>'+
	'<td class="hidden-480"><input name="fabric[]" id="fabric'+flag+'" type="text" value="'+data.fabric+'" class="form-control input-sm" ></td>'+	
	'<td class="hidden-480"><input name="size[]" id="fabric'+flag+'" type="text" value="'+size+'" class="form-control input-sm" ></td>'+	
	'<td class="hidden-480"><input name="condition[]" id="condition'+flag+'" type="text" value="'+condition+'" class="form-control input-sm" ></td>');
			 */  
				
		   }//,async: false
		});

	
	
	  $('#season').val('');
	$('#start_date').val('');
	$('#end_date').val('');
	
	
	
	location.reload();
	}else{
		alert('Enter value');
	}
	 

});

	
	function check_date_stat(){
	
	var a=$('#contract_start_date').val();
	 a=new Date(a);
	   var b=$('#contract_end_date').val();
	   b=new Date(b);
	   if(a>=b && b!='' ){
		   alert('End Date sould getter tan Start Date ');
		   $('#contract_end_date').val('');
	   }
	  
	  var c=$('#contract_end_date'+(flag-1)).val();
	 // alert(c);
	    c=new Date(c);
		if(c>a){
			
			alert('Please enter valid date');
			$('#contract_start_date').val('');
			$('#contract_end_date').val('');
		}
		
	  
	   
}
	
	

	
	
	
function show_modal(){
	$('#basic1').modal('show');
}	
  

function set_vendor(a,b,c,d){
	//alert(d)
	$("#select_l_itm").val(a);
	$("#address").val(b);
	$("#ph_no").val(c);
	$("#vendor_id").val(d);
	
	$('#basic1').modal('toggle');
	
}	
	
	
</script> 
<script>
  
  $('#end_date_p').on('click',function(){
	  
	  var seasons=$('#season2').val();
var starts=$('#start_date_p').val();

if(seasons ==null || seasons =='' || starts ==null || starts ==''){
	
	//alert(seasons);
	//alert()
	swal("Enter season and start date first!", "", "error")
}
else{
	
	//alert(seasons);
	//alert(starts);
	//$this->rate_plan_model->check_season_by_date($season,$start,$end);
	$.ajax({
		   type: "POST",
		   url: "<?php echo base_url();?>rate_plan/check_season_by_date",
		    data:{seasons:seasons,starts:starts},
		   success: function(data){
			   
			  // alert(data);
			   if(data =='already defined'){
				   
				   swal("Already defined this date range under the same season!", "", "error")
				   $('#season2').val('');
				   $('#start_date_p').val('');
				   $('#end_date_p').val('');
				   $('#season2').focus();
			   }
		 
				
		   },
		});
}
	  
  });


</script> 
<script>


 


 function check_date(){
	
	var a=$('#bill_date').val();
	 a=new Date(a);
	   var b=$('#bill_due_date').val();
	   b=new Date(b);
	   if(a>=b && b!='' ){
		  // alert('End Date sould getter tan Start Date ');
		   swal("Due Date sould getter than Bill Date!", "", "error")
		   $('#bill_due_date').val('');
	   }
		  
	
}


function update_default(v){
	
	 $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>rate_plan/default_season",
    data:{plan:v},
    success: function(data){
			
		if(data.data== "y")
		{
			 swal("Duplicate season name!", "", "error")
			 $('#season').val('');
		}
    }
    });
}












function check_duplicate(val){
	 $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>dashboard/duplicate_season",
    data:{data:val},
    success: function(data){
			
		if(data.data== "y")
		{
			 swal("Duplicate season name!", "", "error")
			 $('#season').val('');
		}
    }
    });
}
$(document).ready(function() {

$('.date-picker').datepicker({
                orientation: "left",
				startDate: "date",
                autoclose: true,
				todayHighlight: true
            })
			/*.on('changeDate', function(ev){
				var selected1 = $("#sd").val();
				var selected2 = $("#ed").val();
				//alert("HERE");
				$.ajax({				
					url: '<?php echo base_url(); ?>dashboard/get_available_room2',
					type: "POST",
					data: {date1:selected1 , date2:selected2},
					success: function(data){
					   //alert(data);
					   $("#hd").val(data);
					},
				});
        });*/
});




</script> 
<script>
var psc = 0;

function all_rate_plans(){
	//alert("dgdg");
var populateStatus=$('#populate').val();
//alert(populateStatus);	
var pwd = $('#rate_pwd').val();
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();



today = dd+'-'+mm+'-'+yyyy;


if(pwd == "infitree"){
//if(1){
	if(populateStatus=='0'){
	//alert("dgdg");
	<?php $functionName1="add_rate_plan";?>
	alert('<?php echo $functionName1;?>');
	swal('Ressing all the Rate Plan Data to 0.00! This may take some time.');
	$('#rate_pwd').val('');
	$('#responsive').modal('toggle');
	 alert('<?php echo base_url()?>rate_plan/<?php echo $functionName1;?>');
	 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>rate_plan/<?php echo $functionName1;?>",
                
                success:function(data)
                {
               //    alert(data);
				  // console.log(data);
                     swal({
                            title: data.data,
                            text: "Data populated successfully!",
                            type: "success"
                        },
                        function(){
							//$('#row_'+id).remove();

                        });
                        
                }
            });
}else{
	//alert("d1gd2g");
	<?php $functionName2="edit_rate_plan";?>
	//alert('<?php echo $functionName2;?>');
	swal('Ressing all the Rate Plan Data to 0.00! This may take some time.');
	$('#rate_pwd').val('');
	$('#responsive').modal('toggle');
	 alert('<?php echo base_url()?>rate_plan/<?php echo $functionName2;?>');
	 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>rate_plan/<?php echo $functionName2;?>",
                
                success:function(data)
                {
                   alert(data);
				   console.log(data);
                     swal({
                            title: data.data,
                            text: "Data populated successfully!",
                            type: "success"
                        },
                        function(){
							//$('#row_'+id).remove();

                        });
                        
                }
            });
}
	

			
}//end of if..
else{
	psc++;
	//swal('You entered a wrong password, 5 attempts left');
	if(psc <= 5){
		swal("wrong!", "You entered a wrong password, "+ (5-psc) +" attempts left", "error");
	}
	
	$('#responsive').modal('toggle');
	
	
	
}

}
</script>