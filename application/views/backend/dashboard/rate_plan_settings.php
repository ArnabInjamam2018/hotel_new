<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light borderd">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i> All Rates Plan
      <?php if(isset($stat) && $stat){echo $stat;}?>
    </div>
    <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body">
    <div id="table1">
    
    <table class="table table-striped table-hover table-bordered mass-book short" id="sample_3">
      <thead>
        <tr style="color:#F8681A;">
          <th rowspan="2" width="1%" style="background-position: right 67px; padding: 58px 10px 10px; background-color:#eeeeee !important; color:#333333; !important"> # </th>
          <th rowspan="2" style="background-position: right 67px; padding: 58px 10px 10px; background-color:#eeeeee !important; color:#333333; !important"> Unit Type </th>
          <th rowspan="2" scope="col" style="background-position: right 67px; padding: 58px 10px 10px; background-color:#eeeeee !important; color:#333333; !important">Meal Plan</th>
          <th rowspan="2" scope="col" style="background-position: right 67px; padding: 58px 10px 10px; border-right: 1px solid #dddddd; background-color:#eeeeee !important; color:#333333; !important">Occopancy</th>                
          
          <th scope="col" colspan="2" style="text-align:center; border-right: 1px solid #dddddd; background-color:#eeeeee !important; color:#26c281 !important; font-size:21px;">Base</th>
          <th scope="col" colspan="2" style="text-align:center; border-right: 1px solid #dddddd; background-color:#eeeeee !important; color:#e43a45 !important; font-size:21px;">Weekend</th>
          <th scope="col" colspan="2" style="text-align:center; background-color:#eeeeee !important; color:#337ab7 !important; font-size:21px;">Seasonal</th>
        </tr>
        <tr style="background-color: #e7ecf1;color: #333;">
            <th style="color:#26c281 !important; text-align:center;">Room<br/> Rent</th>
            <th style="color:#26c281 !important; text-align:center;">Meal <br/>Plan</th>
            <th style="color:#e43a45 !important; text-align:center;">Room <br/>Rent</th>
            <th style="color:#e43a45 !important; text-align:center;">Meal <br/>Plan</th>
            <th style="color:#337ab7 !important; text-align:center;">Room <br/>Rent</th>
            <th style="color:#337ab7 !important; text-align:center;">Meal <br/>Plan</th>
         </tr>
      </thead>
      <tbody>				
        <?php   $sl_no=0;
                $datas = $this->rate_plan_model->get_all_rate_plan();
				//echo "<pre>";
			  // print_r($datas); exit;
                //echo (sizeof($data));
                $put = '';
                $pmp = '';
            if(isset($rates) && $rates){	
                //print_r($rates);
              foreach($rates as $data){
                    $sl_no++;
           ?>
        <tr>
          <td><?php echo $sl_no;?></td>
          <td>
              <?php 
                        if($put != $data->unit_type_name)
                            echo '<span style="font-size:12px; font-weight:bold">'.$data->unit_type_name.'</span>';
						else
                            echo '<span style="font-size:12px;">'.$data->unit_type_name.'</span>';
                        $put = $data->unit_type_name;
              ?>
          </td>
          <td>
                <?php 
                        if($pmp != $data->meal_plan){
                            echo '<span style="font-size:13px; font-weight:bold">'.$data->meal_plan.'</span>';
                        }
						else{
                            echo '<span style="font-size:12px;">'.$data->meal_plan.'</span>';
                        $pmp = $data->meal_plan;
						}
                ?>
          </td>
          <td style="border-right: 1px solid #dddddd;">
                <?php echo '<span style="font-size:12px">'.$data->occupancy_type.'</span>';
                        
                ?>
                
          </td>
          
          <td> <input type="text" id="br_c<?php echo $data->rate_plan_id;?>" value="<?php if($data->b_r_c!=-1) echo $data->b_r_c ;?>"  class="form-control input-sm"  onblur="update_bplan('<?php echo $data->rate_plan_id;?>','<?php echo $data->unit_type_id;?>','<?php echo $data->meal_plan_id;?>','<?php echo $data->occupancy_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);" style="text-align:center; padding-right: 20px;"></td>
          
          <td><input type="text" id="bm_c<?php echo $data->rate_plan_id;?>" value="<?php if($data->b_m_c!=-1) echo $data->b_m_c ;?>"  <?php if($data->meal_plan_id==1) echo "disabled";?> class="form-control input-sm"  onblur="update_bmplan('<?php echo $data->rate_plan_id;?>','<?php echo $data->unit_type_id;?>','<?php echo $data->meal_plan_id;?>','<?php echo $data->occupancy_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);" style="text-align:center; padding-right: 20px;"> </td>
          
          <td> <input type="text" id="wr_c<?php echo $data->rate_plan_id;?>" value="<?php if($data->w_r_c!=-1) echo $data->w_r_c ;?>"   class="form-control input-sm"  onblur="update_wrplan('<?php echo $data->rate_plan_id;?>','<?php echo $data->unit_type_id;?>','<?php echo $data->meal_plan_id;?>','<?php echo $data->occupancy_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);"  style="text-align:center; padding-right: 20px;"> </td>
          
          <td><input type="text" id="wm_c<?php echo $data->rate_plan_id;?>" value="<?php if($data->w_m_c!=-1) echo $data->w_m_c ;?>"   <?php if($data->meal_plan_id==1) echo "disabled";?> class="form-control input-sm"  onblur="update_wmplan('<?php echo $data->rate_plan_id;?>','<?php echo $data->unit_type_id;?>','<?php echo $data->meal_plan_id;?>','<?php echo $data->occupancy_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);" style="text-align:center; padding-right: 20px;"> </td>
          
          <td> <input type="text" id="sr_c<?php echo $data->rate_plan_id;?>" value="<?php if($data->s_r_c!=-1) echo $data->s_r_c ;?>"   class="form-control input-sm"  onblur="update_srplan('<?php echo $data->rate_plan_id;?>','<?php echo $data->unit_type_id;?>','<?php echo $data->meal_plan_id;?>','<?php echo $data->occupancy_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);" style="text-align:center; padding-right: 20px;"> </td>
          
          <td><input type="text" id="sm_c<?php echo $data->rate_plan_id;?>" value="<?php if($data->s_m_c!=-1) echo $data->s_m_c ;?> "  <?php if($data->meal_plan_id==1) echo "disabled";?> class="form-control input-sm" onblur="update_smplan('<?php echo $data->rate_plan_id;?>','<?php echo $data->unit_type_id;?>','<?php echo $data->meal_plan_id;?>','<?php echo $data->occupancy_id;?>')" maxlength="10" onkeypress=" return onlyNos(event, this);" style="text-align:center; padding-right: 20px;"></td>
          
        </tr>
            <?php }}?>
      </tbody>
    </table>
  </div>	
  </div>  
</div>
<div id="myModal" class="modal fade in" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
         <div class="modal-body">
         <h4 style="color:#C41E3A;"><i class="fa fa-warning"></i> <strong>System Warning</strong></h4>
        <p>Editing the rates from this page can alter all the rate plans, please proceed only if you are sure & authorized.</p>
      </div>
      <div class="modal-footer">
        <a class="btn greenLight" href="<?php echo base_url()?>dashboard" style="margin-right:10px;">Disagree</a>
        <a class="btn pink" data-dismiss="modal">Agree</a>
      </div>
    </div>
  </div>
</div>
<!-- end edit modal --> 
<script>
    
function update_wmplan(r_id,u_id,m_id,o_id){
	
	 
	var wm_c=$("#wm_c"+r_id).val();
	var wm_ct=$("#wm_ct"+r_id).val();
	//var wr_ctax=10;
	var wm_ctax=10;
	var wm_cttx=wm_c*wm_ctax/100;
	
	$("#wm_ct"+r_id).val(wm_cttx);
	
	
	 $.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>rate_plan/update_r_wmplan",
                data:{wm_c:wm_c,wm_ct:wm_cttx,u_id:u_id,m_id:m_id,o_id:o_id},
                success:function(data)
                {
                   
                   
                }
            });
}
</script> 
<script> 


function show_rp(id){
//	p_id=id;
	//alert(p_id);
}

function get_rate(id){
	//alert(p_id);
	//alert(id);
	if(p_id==''){
		swal("Please Select Season!", "", "warning") 
		return false;
	}
	$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>rate_plan/get_rate_plan",
                data:{p_id:p_id,s_id:id},
                success:function(msg)
                {
				  
				$('#table1').html(msg);
				   $('#table_12').dataTable( {
						"pageLength": 50
    } );
	
		 
                }
            });
}


</script> 
<script> 
var i=1; 
var j=1; 

function update_bplan(r_id,u_id,m_id,o_id){
		
		//return false;
			if(i==1){
				swal({
  title: "Are you sure?",
  text: "You will not be able to edit fields!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Yes",
  cancelButtonText: "No",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {
	  i++;
    swal("", "Here you can edit fields ", "success");
	var br_c=parseFloat($("#br_c"+r_id).val());
	var br_ct=parseFloat($("#br_ct"+r_id).val());
	
	if(isNaN(br_c)){
		return false;
	}
	var br_ctax=10;
	var br_cttx=br_c*br_ctax/100;
	 
	$("#br_ct"+r_id).val(br_cttx);
	
	 $.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>rate_plan/update_r_bplan",
                data:{br_c:br_c,br_ct:br_cttx,u_id:u_id,m_id:m_id,o_id:o_id},
                success:function(data)
                {
                   
                }
            });
  } else {
    swal("Cancelled", "Cancelled)", "error");
	$("#br_c"+r_id).val('');
	$("#br_ct"+r_id).val('');
    $("#bm_c"+r_id).val('');
	$("#bm_ct"+r_id).val('');
  }
});
			}else{
				var br_c=parseFloat($("#br_c"+r_id).val());
	var br_ct=parseFloat($("#br_ct"+r_id).val());
	//alert(br_c); 
	if(isNaN(br_c)){
		return false;
	}
	var br_ctax=10;
	var br_cttx=br_c*br_ctax/100;
	 
	$("#br_ct"+r_id).val(br_cttx);
	
	 $.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>rate_plan/update_r_bplan",
                data:{br_c:br_c,br_ct:br_cttx,u_id:u_id,m_id:m_id,o_id:o_id},
                success:function(data)
                {
                   
                }
            });
				
			}
	
}

function update_bmplan(r_id,u_id,m_id,o_id){
	if(i==1 && j==1){
	swal({
  title: "Are you sure?",
  text: "You will not be able to edit fields!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Yes",
  cancelButtonText: "No",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {
	  j++;
	  i++;
    swal("", "Here you can edit fields ", "success");
	
	
	
	var bm_c=parseFloat($("#bm_c"+r_id).val());
	//var bm_ct=parseFloat($("#bm_ct"+r_id).val());
	if(isNaN(bm_c)){
		return false;
	}
	var bm_ctax=10;
	//alert(bm_c);
	//return false;
	var bm_cttx=bm_c*bm_ctax/100;
	
	$("#bm_ct"+r_id).val(bm_cttx);
	 $.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>rate_plan/update_r_bmplan",
                data:{bm_c1:bm_c,bm_ct1:bm_cttx,u_id:u_id,m_id:m_id,o_id:o_id},
			    success:function(data)
                {
                  //alert(data);
                }
            });
  

}else{
	swal("Cancelled", "Cancelled)", "error");
}
});			
	
} else{
	var bm_c=parseFloat($("#bm_c"+r_id).val());
	//var bm_ct=parseFloat($("#bm_ct"+r_id).val());
	var bm_ctax=10;
	//alert(bm_c);
	//return false;
	var bm_cttx=bm_c*bm_ctax/100;
	
	$("#bm_ct"+r_id).val(bm_cttx);
	 $.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>rate_plan/update_r_bmplan",
                data:{bm_c1:bm_c,bm_ct1:bm_cttx,u_id:u_id,m_id:m_id,o_id:o_id},
			    success:function(data)
                {
                  //alert(data);
                }
            });
	
}

}


function update_wrplan(r_id,u_id,m_id,o_id){
	var wr_c=$("#wr_c"+r_id).val();
	if(isNaN(wr_c)){
		return false;
	}
	//var wr_ct=$("#wr_ct"+r_id).val();
	var wr_ctax=10;
	var wr_cttx=wr_c*wr_ctax/100;
	
	$("#wr_ct"+r_id).val(wr_cttx);
	//alert(wr_c);
	
	
	 $.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>rate_plan/update_r_wrplan",
                data:{wr_c:wr_c,wr_ct:wr_cttx,u_id:u_id,m_id:m_id,o_id:o_id},
                success:function(data1)
                {
                 // alert(data1); 
                   
                }
            });
  

			
	
}



function update_srplan(r_id,u_id,m_id,o_id){
	
	var sr_c=parseFloat($("#sr_c"+r_id).val());
	var sr_ct=parseFloat($("#sr_ct"+r_id).val());
	//var sm_c=parseFloat($("#sm_c"+r_id).val());
	//var sm_ct=parseFloat($("#sm_ct"+r_id).val());
//	alert(sr_c);
//	alert(sr_ct);
//	alert(sm_c);
//	alert(sm_ct);
     if(isNaN(sr_c)){
		return false;
	}
    var sr_ctax=10;
	//var sm_ctax=10;
	var sr_cttx=sr_c*sr_ctax/100;
	//var sm_cttx=sm_c*sm_ctax/100;
	$("#sr_ct"+r_id).val(sr_cttx);
	//$("#sm_ct"+r_id).val(sm_cttx);
	
	 $.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>rate_plan/update_r_srplan",
                data:{sr_c:sr_c,sr_ct:sr_cttx,u_id:u_id,m_id:m_id,o_id:o_id},
                success:function(data)
                {
                   
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){
					//$('#row_'+id).remove();

                        });
                }
            });
}
function update_smplan(r_id,u_id,m_id,o_id){
	
	//var sr_c=parseFloat($("#sr_c"+r_id).val());
//	var sr_ct=parseFloat($("#sr_ct"+r_id).val());
	var sm_c=parseFloat($("#sm_c"+r_id).val());
	var sm_ct=parseFloat($("#sm_ct"+r_id).val());
//	alert(sr_c);
//	alert(sr_ct);
//	alert(sm_c);
//	alert(sm_ct);
   // var sr_ctax=10;
   if(isNaN(sm_c)){
		return false;
	}
	var sm_ctax=10;
	//var sr_cttx=sr_c*sr_ctax/100;
	var sm_cttx=sm_c*sm_ctax/100;
	//$("#sr_ct"+r_id).val(sr_cttx);
	$("#sm_ct"+r_id).val(sm_cttx);
	
	 $.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>rate_plan/update_r_smplan",
                data:{sm_c:sm_c,sm_ct:sm_cttx,u_id:u_id,m_id:m_id,o_id:o_id},
                success:function(data)
                {
                   
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){
					//$('#row_'+id).remove();

                        });
                }
            });
}
</script>
 <script type="text/javascript">
    $(window).load(function(){
        $('#myModal').modal('show');
    });
</script>
<style>
.modal {
    display:block;
	padding-right:0 !important;
	background:rgba(51,51,51,0.2);	
}	
</style>