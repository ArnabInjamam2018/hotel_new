<!-- BEGIN PAGE CONTENT-->
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
<!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>

<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="form-group">
  <div class="col-md-12 control-label">
    <div class="alert alert-success alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
  </div>
</div>
<?php endif;?>
<!-- 17.11.2015-->
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"><i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Release Item</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                            $form = array(
                                'class' 			=> '',
                                'id'				=> 'form',
                                'method'			=> 'post',								
                            );
							
							

                            echo form_open_multipart('dashboard/release_item',$form);

                            ?>
    <div class="form-body">
      <div class="row">
        <div class="modal fade" id="basic1" tabindex="-1" role="basic" aria-hidden="true" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Lost Items</h4>
              </div>
              <div class="modal-body">
			  
				<div class="portlet-body">
                                    
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs ">
                                            <li class="active">
                                                <a href="#tab_15_1" onclick="fetch_item1('Electronics')" data-toggle="tab" aria-expanded="true"> Electronics Items </a>
                                            </li>
                                            <li class="">
                                                <a href="#tab_15_2" onclick="fetch_item1('Jewelery')" data-toggle="tab" aria-expanded="false"> Jewelery </a>
                                            </li>
                                            <li class="">
                                                <a href="#tab_15_3" onclick="fetch_item1('Id Card')" data-toggle="tab" aria-expanded="false"> Id Card </a>
                                            </li>
											<li class="">
                                                <a href="#tab_15_4" onclick="fetch_item1('Bag')" data-toggle="tab" aria-expanded="false"> Bag </a>
                                            </li>
											<li class="">
                                                <a href="#tab_15_5" onclick="fetch_item1('Others')" data-toggle="tab" aria-expanded="false"> Others </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane" id="tab_15_1">
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_15_2">
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_15_3">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
			  
			  
			  
			  
                <?php 
                                	if(isset($found_item)){
                                		foreach ($found_item as $value) {
												# code...
											//echo "<pre>";
											//print_r($value);
										}
                                	}
                                ?>
                <!--<a class="btn pink" data-toggle="modal" href="" onclick="fetch_item1('Electronics')"> Electronics Items</a><a class="btn pink" data-toggle="modal" href="" onclick="fetch_item1('Jewelery')"> Jewelery</a> <a class="btn pink" data-toggle="modal" href="" onclick="fetch_item1('Id Card')"> Id Card</a> <a class="btn pink" data-toggle="modal" href="" onclick="fetch_item1('Bag')"> Bag</a> <a class="btn pink" data-toggle="modal" href="" onclick="fetch_item1('Others')"> Other</a> -->
				
				</div>
              <div id="mdl2"> </div>
              <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                
              </div>
            </div>
            <!-- /.modal-content --> 
          </div>
          <!-- /.modal-dialog --> 
        </div>
        <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Items</h4>
              </div>
              <div class="modal-body">
                <?php 
                                	if(isset($found_item)){
                                		foreach ($found_item as $value) {
												# code...
											//echo "<pre>";
											//print_r($value);
										}
                                	}
									//echo '<pre>';
									//print_r($found_item);
                                ?>
                <a class="btn pink" data-toggle="modal" href="" onclick="fetch_item('Electronics')"> Electronics Items</a> <a class="btn pink" data-toggle="modal" href="" onclick="fetch_item('Jewelery')"> Jewelery</a> <a class="btn pink" data-toggle="modal" href="" onclick="fetch_item('Id Card')"> Id Card</a> <a class="btn pink" data-toggle="modal" href="" onclick="fetch_item('Bag')"> Bag</a> <a class="btn pink" data-toggle="modal" href="" onclick="fetch_item('Others')"> Other</a> </div>
              <div id="mdl1"> </div>
              <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                
              </div>
            </div>
            <!-- /.modal-content --> 
          </div>
          <!-- /.modal-dialog --> 
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="hidden" id="id" name="id" value="" >
          <input autocomplete="off" type="text" class="form-control date-picker" id="form_control_11" name="date" placeholder="Select Date *">
          <label></label>
          <span class="help-block">Select Date *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control " id="select_l_itm" name="item" data-toggle="modal" href="#basic1" placeholder="Select Lost item *">
          <input autocomplete="off" type="hidden" class="form-control " id="select_l_itm_id" name="select_l_itm_id">
          <label></label>
          <span class="help-block">Select Lost item *</span>
        </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control " id="select_f_itm" name="item" data-toggle="modal" href="#basic" placeholder="Select Found item *">
          <input autocomplete="off" type="hidden" class="form-control " id="select_f_itm_id" name="select_f_itm_id">
          <label></label>
          <span class="help-block">Select Found item *</span>           
        </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="release_by" placeholder="Release By">
          <label></label>
          <span class="help-block">Release By</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" type="text" class="form-control date-picker" id="form_control_12" name="release_date" placeholder="Release Date">
          <label></label>
          <span class="help-block">Release Date</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control timepicker timepicker-24" id="form_control_1" name="release_time" placeholder="Release Time *">
          <label></label>
          <span class="help-block">Release Time *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="claim_by" placeholder="Claim By Name">
          <label></label>
          <span class="help-block">Claim By Name</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input autocomplete="off" type="text" class="form-control" id="mobile" name="g_contact_no"  required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" placeholder="Mobile No *">
          <label></label>
          <span class="help-block">Mobile No *</span> </div>
        </div>
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="booking_id" placeholder="Booking Id">
          <label></label>
          <span class="help-block">Booking Id</span> </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn submit" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>
<script>

$( document ).ready(function() {
    fetch_item1('Electronics');
});
	function fetch_item(value){
		
		       // alert("ggg");
		 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/fetch_item",
                data:{item: value},
                success:function(data1)
                {	
						//alert(data1);
						
						$('#mdl1').html(data1);
					//alert(data);
                }
            });

	}

function fetch_item1(value){		
		        //alert(value);
		 $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/fetch_lost_items",
                data:{item: value},
                success:function(data1)
                {	
						//alert(data1);
						$('#mdl2').html(data1);
					//alert(data.itm);
                }
            });

	}
   
    
	function getVal(val){
		var val_arr=val.split("-");
		//alert(val_arr[0]);
		//alert(val_arr[1]);
		document.getElementById("select_f_itm").value=val_arr[0];
		document.getElementById("select_f_itm_id").value=val_arr[1];
		
		$('#basic').modal('hide');
	}
	function getValue(val){
		document.getElementById("id").value=val;
	}
	function getVal1(val){
		var val_arr=val.split("-");
		//alert(val_arr[0]);
		//alert(val_arr[1]);
		document.getElementById("select_l_itm").value=val_arr[0];
		document.getElementById("select_l_itm_id").value=val_arr[1];

		document.getElementById("id").value=val;
		$('#basic1').modal('hide');
	}
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});
</script> 
