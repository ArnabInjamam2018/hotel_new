<div class="portlet light borderd">
        <div class="portlet-title">
          <div class="caption"> <i class="fa fa-file-archive-o"></i>Resevation(ADR) Reports </div>
          <!--
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
                --> 
        </div>
        <div class="portlet-body">
          <div class="table-toolbar">
            <div class="row">
              <div class="col-md-7">
                  <?php

                            $form = array(
                                'class'       => 'form-inline',
                                'id'        => 'form_date',
                                'method'      => 'post'
                            );

                            echo form_open_multipart('reports/get_r_report_by_date',$form);

                            ?>
                      <div class="form-group">
                        <input type="text" autocomplete="off" required="required" value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date">
                      </div>
                      <div class="form-group">
                        <input type="text" autocomplete="off" required="required" value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" class="form-control date-picker" placeholder="End Date">
                      </div>
					   
                    <button class="btn btn-default" onclick="check_sub()" type="submit">Search</button>
                  <?php form_close(); ?>
              </div>              
            </div>
          </div>
          <table class="table table-striped table-hover table-bordered" id="sample_1">
            <thead>
              <tr> 
                <th> # </th>
				<th> Guest Name </th>
                <th> Room Type </th>
                <th> Room No </th>
                <th> Check In </th>
                <th> Check Out </th>
                <th>Night </th>
                <th> Room Rent(Excluding Taxs) </th>
                <th> ADR </th>
              </tr>
            </thead>
            <tbody>
             
             
              <?php
						if(isset($reports) && $reports){
							//print_r($reports);
						$srl_no=0;
						foreach($reports as $report){
						$srl_no++;
					$room_id=$report->room_id;
					$room=$this->dashboard_model->get_room_by_id($room_id);
						if(!empty($room)){
					 foreach($room as $rooms){
						 
			  ?>
			  <tr>
				<td><?php echo $srl_no;?></td>
				<td><?php echo $report->cust_name;?></td>	
				<td><?php echo $rooms->unit_name ;?></td>
				<td><?php echo $rooms->room_no;?></td>
				<td><?php echo $report->cust_from_date_actual;?></td>
				<td><?php echo $report->cust_end_date_actual;?></td>
				<td>
					<?php
					$from=$report->cust_from_date_actual;
					$to=$report->cust_end_date_actual;
					echo $nights = (strtotime($to) - strtotime($from)) / 86400;
				?></td>
				<td><?php echo $report->room_rent_total_amount;?></td>
				<td><?php 
					if($nights==0){
						echo $adr=$report->room_rent_total_amount;
					}
					else{
						echo $adr=$report->room_rent_total_amount/$nights;
					}
					?>
					</td>
						
						
			  </tr>
					 <?php }}}}?>
            </tbody>
          </table>
		 
		
	</div>										
	</div>										

</script>
<script>
$(document).ready(function(){
  $('#chkbx_tdy').prop('checked', true);
});

function check_sub(){
  document.getElementById('form_date').submit();
}
</script>