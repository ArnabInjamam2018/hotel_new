<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 14px;
	line-height: 1.42857143;
	color: #555555;
}
.well {
	border: 0;
	-webkit-box-shadow: none !important;
	-moz-box-shadow: none !important;
	box-shadow: none !important;
	min-height: 20px;
	margin-bottom: 20px;
	border-radius: 4px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad {
	font-size: 10px;
}
 @media print {
  body * {
    visibility: hidden;
  }
  #sample_editable_1, #sample_editable_1 * {
    visibility: visible;
  }
  #sample_editable_1 {
    position: absolute;
    left: 0;
    top: 0;
  }
}
</style>

<div style="padding:10px;">
<table>
		<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));?>
    <tr>
      <td align="center"><img src="upload/hotel/<?php
			 
			if(isset($hotel_name->hotel_logo_images_thumb))echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/></td>
		</tr>
		<tr>
		<td colspan="3" align="right"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?></td>
        </tr>		
    
    <tr>
      <td width="100%"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%">&nbsp;</td>
    </tr>
</table>
<table class="td-pad" width="100%">
  <thead>
  
    <tr style="background: #00C5CD; color: white">
      <th> Sl.no </th>
      <th> Guest Name </th>
      <th> Room Type </th>
      <th> Room Number </th>
      <th> Checked In </th>
      <th> Checked Out </th>
      <th> Night </th>
      <th> Room Rent(Excluding Tax) </th>
      <th> ADR </th>
    </tr>
  </thead>
  <tbody style="background: #F2F2F2">
    <?php
						if(isset($reports) && $reports):
							//print_r($reports);
						$srl_no=0;
						foreach($reports as $report):
						$srl_no++;
					$room_id=$report->room_id;
					$room=$this->dashboard_model->get_room_by_id($room_id);
						if(!empty($room)):
					 foreach($room as $rooms):
						 
			  ?>
     <tr align="center" >
				<td><?php echo $srl_no;?></td>
				<td><?php echo $report->cust_name;?></td>	
				<td><?php echo $rooms->unit_name ;?></td>
				<td><?php echo $rooms->room_no;?></td>
				<td><?php echo $report->cust_from_date_actual;?></td>
				<td><?php echo $report->cust_end_date_actual;?></td>
				<td>
					<?php
					$from=$report->cust_from_date_actual;
					$to=$report->cust_end_date_actual;
					echo $nights = (strtotime($to) - strtotime($from)) / 86400;
				?></td>
				<td><?php echo $report->room_rent_total_amount;?></td>
				<td><?php 
					if($nights==0){
						echo $adr=$report->room_rent_total_amount;
					}
					else{
						echo $adr=$report->room_rent_total_amount/$nights;
					}
					?>
					</td>
						
						
			  </tr>
    <?php //$i++;?>
	<?php endforeach; ?>
    <?php endif; ?>
    
    <?php endforeach; ?>
    <?php endif;?>
  </tbody>
</table>
</div>
