<div class="row">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet light borderd">
			<div class="portlet-title">
				<div class="caption"> <i class="glyphicon glyphicon-bed"></i>Reservation Summary </div>

				<div class="actions">
					<a href="javascript:;" class="collapse">
								</a>
				
					<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
				
					<a href="javascript:;" class="reload">
								</a>
				
					<a href="javascript:;" class="remove">
								</a>
				
				</div>

			</div>

			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-8">
							<?php

							$form = array(
								'class' => 'form-inline',
								'id' => 'form_date',
								'method' => 'post'
							);

							echo form_open_multipart( 'dashboard/get_r_summary_by_date', $form );

							?>
							<div class="form-group">
								<input type="text" autocomplete="off" required="required" id="t_dt_frm" value="<?php if(isset($start_date)){ echo $start_date;}?>" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date">
							</div>
							<div class="form-group">
								<input type="text" autocomplete="off" required="required" name="t_dt_to" value="<?php if(isset($end_date)){ echo $end_date;}?>" class="form-control date-picker" placeholder="End Date">
							</div>
							<button class="btn btn-default" onclick="check_sub()" type="submit">Search</button>

							<?php form_close(); ?>
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover table-bordered" id="sample_1">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Booking Id</th>
							<th scope="col">Folio</th>
							<th scope="col">Guest Name</th>
							<th scope="col">State</th>
							<th scope="col">Phone</th>
							<th scope="col">Email Id</th>
							<th scope="col">Room Type</th>
							<th scope="col">Room</th>
							<th scope="col">PAX</th>
							<th scope="col">Status</th>
							<th scope="col">NightAudit</th>
							<th scope="col">Check In</th>
							<th scope="col">Check Out</th>
							<th scope="col">Days</th>
							<th scope="col">Room Rent</th>
							<th scope="col">Room Rent Tax</th>
							<th scope="col">Extra Service</th>
							<th scope="col">Extra Charges</th>
							<th scope="col">Extra Tax</th>
							<th scope="col">POS Bill</th>
							<th scope="col">Paid </th>
							<th scope="col">Pending Balance</th>
						</tr>
					</thead>

					<tbody>


						<?php if($summary && isset($summary)){
				    $srl_no=0;
					foreach($summary as $sum){
					//echo $sum->g_email;	
					 $srl_no++;
					$booking_id=$sum->booking_id;
					if(isset($booking_id) && $booking_id){
						//echo $booking_id;
						$transaction = $this->bookings_model->get_total_payment($booking_id);
						$paid_amount=0;  
					foreach ($transaction as $transaction)
						{
							$paid_amount = $paid_amount=0 + $transaction->t_amount;
						}
						

						$from_days=$sum->cust_from_date_actual;
						$end=$sum->cust_end_date_actual;
						$days = (strtotime($end) - strtotime($from_days)) / (60 * 60 * 24);
						//print $days;
						//echo $maxdiff;
					
						   
						$pending=$sum->room_rent_sum_total-$paid_amount;
						
						
						}
					
			  ?>
						<tr>
							<td align="center">
								<?php echo $srl_no ;?>
							</td>
							<td align="center">
								<?php echo $sum->booking_id_actual;?>
							</td>
							<td align="center">N/A</td>
							<td align="center">
								<?php echo $sum->cust_name;?>
							</td>
							<td align="center">
								<?php echo $sum->g_state;?>
							</td>
							<td align="center">
								<?php echo $sum->g_contact_no;?>
							</td>
							<td align="center">
								<?php echo $sum->g_email;?>
							</td>
							<td align="center">
								<?php echo $sum->unit_name;?>
							</td>
							<td align="center">
								<?php echo $sum->room_no;?>
							</td>
							<td align="center">
								<?php echo $sum->no_of_guest;?>
							</td>
							<td align="center">
								<?php echo $sum->booking_status;?>
							</td>
							<td align="center">N/A</td>
							<td align="center">
								<?php echo $sum->cust_from_date_actual;?>
							</td>
							<td align="center">
								<?php echo $sum->cust_end_date_actual;?>
							</td>
							<td align="center">
								<?php echo $days;?>
							</td>
							<td align="center">
								<?php echo $sum->room_rent_total_amount;?>
							</td>
							<td align="center">
								<?php echo $sum->room_rent_tax_amount;?>
							</td>
							<td align="center">
								<?php echo $sum->service_price;?>
							</td>
							<td align="center">
								<?php echo $sum->extra_service_total_amount?>
							</td>
							<td align="center">
								<?php $ex_sum=$sum->service_price+$sum->extra_service_total_amount;
				echo $ex_sum;
				?>

							</td>
							<td align="center">asf</td>
							<td align="center">
								<?php echo $paid_amount;?>
							</td>
							<td align="center">
								<?php if($pending>0){
							echo $pending;
						}
						else{
							echo "0";
						};?>
							</td>

						</tr>

						<?php }}?>
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>
<script>
	$( document ).ready( function () {
		$( '#chkbx_tdy' ).prop( 'checked', true );
	} );

	function check_sub() {
		document.getElementById( 'form_date' ).submit();
	}
</script>