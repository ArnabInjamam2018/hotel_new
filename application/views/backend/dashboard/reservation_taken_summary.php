<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/amcharts/plugins/export/export.css" type="text/css" media="all" />
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/serial.js" type="text/javascript"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all"/>

    <div class="portlet light borderd">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-th-list"></i> <strong> Daily Entry Reservation Summary</strong> </div>
        <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            
            <div class="col-md-8">
              <?php

	                            $form = array(
	                                'class'     => 'form-inline ftop',
	                                'id'        => 'form_date',
	                                'method'    => 'post'
	                            );

	                            echo form_open_multipart('dashboard/get_booking_report_by_date',$form);

	                            ?>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
              </div>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
              </div>
			  
			  <div class="form-group">
              <button onclick="get_val();" class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search" aria-hidden="true"></i></button>
			  </div>
			 
                  
              <?php form_close(); ?>
            </div>
            <script type="text/javascript">
				function check_sub(){
				   document.getElementById('form_date').submit();
				}
			</script>
            
          </div>
        </div>
		
			      
	<p class="font-green-sharp">*This report is Considering Dates the avgAmt was taken on to count avgAmt. <br> Please use the table below to navigate or filter the results. You can download the table as excel and pdf.</p>
	
	<ul class="nav nav-tabs">
                                            <li class="active">												
                                                <a href="#reservationData" data-toggle="tab" aria-expanded="true"> <i class="fa fa-calendar" aria-hidden="true"></i> Data </a>
                                            </li>
											
                                            <li class="">												
                                                <a href="#graph" onclick="generateChartData()" data-toggle="tab" aria-expanded="false">  <i class="fa fa-tachometer" aria-hidden="true"></i> Chart </a>
                                            </li>
    </ul>
	
	<div class="tab-content">
	
	<div class="tab-pane active" id="reservationData">
	
	<div id="table1">	
	
       <table class="table table-striped table-bordered table-hover" id="sample_1">
        <!--<table class="table table-striped">-->
          <thead>
            <tr>
              
			  <!--<th> Select </th>-->
              
              
              <th width="5%"> # </th>
              <th width="10%"> Date </th>
              <th width="15%"> Reservations Taken </th>
              <th width="10%"> Total Booking Amt </th>
              <th width="10%"> Avg Room Night </th>
              <!--<th width="10%"> Inclutions </th>
              <th width="5%"> Disc</th>
              <th width="5%"> Paid</th>-->
              <th width="10%"> Distribution by Status </th>
              <th width="10%"> Distribution by Source </th>
              <th width="10%"> Distribution by Nature of Vist </th>
              <th width="10%"> Distribution by Unit Type </th>
              
             
            </tr>
          </thead>
          <tbody>
            <?php 
			
			if(isset($bookingDate) && $bookingDate){
					$j=0;
					$p=0;
					$p1=0;
					$check = '';
			
			foreach($bookingDate as $book){ 
				
					
					$j++;
					if($p == $book->count || $j == 1)
						$p1 = '-';
					else if($p > $book->count)
						$p1 = '<i style="color:#FF5B5B;" class="fa fa-arrow-down" aria-hidden="true"></i>';
					else
						$p1 = '<i style="color:#30AE72;" class="fa fa-arrow-up" aria-hidden="true"></i>';
					
			
					$p = $book->count;	
			?>
         
									
          <tr>
            <td>  <?php 
					echo $j;
				  ?>
			 </td>
			<td>  <?php 
					echo '<span style="font-size:14px;">'.date("l jS F Y",strtotime($book->booking_taking_date)).'</span>';
					
				  ?>
			 </td>
			<td> 
				
				<?php
					$countR = $this->dashboard_model->roomCount1();
					
					echo '<span style="font-size:18px; font-weight:600;">'.$book->count.'</span> <span style="color:#AAAAAA;">Unit</span>';	
					echo ' | <span style="">'.number_format($book->count/$countR->count*100, 2).'</span> <span style="color:#AAAAAA;">%</span>';
					echo ' | <span style="">'.$p1.'</span>';
					
					$bookingGrp = $this->dashboard_model->getBookingGrp_byDate($book->booking_taking_date);
					
					if(isset($bookingGrp) && $bookingGrp){
						foreach($bookingGrp as $bookingGrp){
							$grp = $bookingGrp->grp;					
						}
					}
					else
						$grp = 0;
					
					
					echo '<br/> (<span style="color:#3D4B76;">'.($book->count - $grp);
					echo ' </span> / <span style="color:#F8681A;">'.$grp.'</span>)';
					
					echo '<span style="font-size:15px; font-weight:600;"> | '.$book->days.'</span> <span style="color:#AAAAAA;">days</span>';
					
				  ?>
			</td>
			 
			
			<td>
				
				
												<?php 
					$bookingAmt = $this->dashboard_model->getLineItem_byTakingDate($book->booking_taking_date);
												//print_r($bookingNature);
											if(isset($bookingAmt) && $bookingAmt){
												foreach($bookingAmt as $bookingAmt){ 
												
													
												?>
                                                    
                                                        
                                                        <span style="font-size:12px"><?php echo $bookingAmt->amt;?></span> 
														
                                                    
												<?php }}
											else
												echo 'no info';	
												?>
                                                    
                     
			</td>
			
			
			<td>
				<span style="font-size:12px"><?php echo number_format($bookingAmt->amt/$book->days,2);?></span>
			</td>
			
			<!--<td></td>
			<td></td>
			<td></td>-->
			<td>
				<ul class="nav nav-pills nav-stacked">
				<?php 
					$bookingStatus = $this->dashboard_model->getBookingStatus_byDate($book->booking_taking_date);
												
							if(isset($bookingStatus) && $bookingStatus){
								foreach($bookingStatus as $bookingStatus){ 							
				?>
										<li>
                                           <a href="javascript:;">
                                            <span class="badge" style="background-color:<?php echo $bookingStatus->color;?>;"> <?php echo $bookingStatus->count;?></span> <span style="font-size:12px"><?php echo $bookingStatus->status;?></span> 
										   </a>
                                        </li>
				<?php 			}
							}
							else
								echo 'no info';
				?>
				</ul>
			</td>
			 
			<td>
				
				<ul class="nav nav-pills nav-stacked">
												<?php 
												$bookingSource = $this->dashboard_model->getBookingSource_byDate($book->booking_taking_date);
												
											if(isset($bookingSource) && $bookingSource){
												foreach($bookingSource as $source1){ 
												
													
												?>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <span class="badge badge-warning"> <?php echo $source1->count;?> </span> <span style="font-size:12px"><?php echo $source1->booking_source_name;?></span> </a>
                                                    </li>
												<?php }}
											else
												echo 'no info';
												?>
                                                    
                                                </ul>
			  
			</td>
			
			<td>
				<?php ?>
				<ul class="nav nav-pills nav-stacked">
												<?php 
												$bookingNature = $this->dashboard_model->getBookingNature_byDate($book->booking_taking_date);
												//print_r($bookingNature);
											if(isset($bookingNature) && $bookingNature){
												foreach($bookingNature as $nature){ 
												
													
												?>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <span class="badge badge-info"> <?php echo $nature->count;?> </span> <span style="font-size:12px"><?php echo $nature->nature_visit;?></span> </a>
                                                    </li>
												<?php }}
											else
												echo 'no info';	
												?>
                                                   
                      </ul>                          
			  
			</td>
			
			<td>
				<ul class="nav nav-pills nav-stacked">
				<?php
					$bookingUT = $this->dashboard_model->getBookingUnitType_byDate($book->booking_taking_date);
													//print_r($bookingNature);
												if(isset($bookingUT) && $bookingUT){
													foreach($bookingUT as $bookingUT){ 
												?>
													<li>
                                                        <a href="javascript:;">
                                                            <span class="badge badge-danger"> <?php echo $bookingUT->count;?> </span> <span style="font-size:12px"><?php echo $bookingUT->unit_name;?> </span></a>
                                                    </li>
												<?php		
													}
												}
				
					
				?>
				</ul>
			</td>
			
			
            
           
          </tr>
		   <input type="hidden" id="item_no" value="<?php //echo $item_no;?>"> </input>
		   
			
          
          <?php  
			}}
		  ?>
         
            </tbody>
          
        </table>
      
    </div>
    
	</div>
	
	<div class="tab-pane" id="graph">
		<div id="chartdiv" style="width: 100%; height: 500px;"></div>
                                              
	</div>
	
    </div>
    </div>
    </div>

<script>
		$( document ).ready(function () {			   
			window.resTknchartCick = 0;			  
			window.resTknchartData = [];			  
        });
 
           let generateChartData = () => {

					let ajaxjson = $.ajax({
						url: "<?php echo base_url()?>reports/resTakenChart",
						type:"POST",
						data:{id:""}
					});

					if(window.resTknchartCick === 0){ // To check if the chart is already generated
						
						let x = ajaxjson.done((ajaxdata) => {
							pushData(ajaxdata);
							if(_.isEqual(window.resTknchartData, ajaxdata)){
								console.log('same array, '+_.isEqual(window.resTknchartData, ajaxdata)); // To check if the data has changed

							}
							window.resTknchartData = ajaxdata;
						})
						
						x.done((chartdata) => {
							generateChart(chartdata); // Calling to generate chart with the array of objects created inside foreach
							console.log('Generated Chart...');
						}).done(() => {
							toastr["success"]("Chart generated Successfully");  
							window.resTknchartCick = 1;
						}).fail((xhr) => {
							toastr["error"]("Failed to get data from ajax");
							console.log('error on ajax call', xhr);
						}).fail((xhr) => {
							toastr["error"]("Failed to transform json");
							console.log('error on array transformation', xhr);
						}).fail((xhr) => {
							toastr["error"]("Failed to generate chart");
							console.log('error on array transformation', xhr);
						});  // @niloy please use promise medhods for sync flow
					}
           };
		   
		   let pushData = (data) => {
			   
			   let chartData = data.map((x) => { // Looping through json array passed from ajax

				    let newDate = new Date(x.date);
					newDate.setDate(newDate.getDate()); // Converting date form model to js date format
					
					if(checkDate(newDate) === true){  // Checking if the date is valid
						return {
										   date: newDate,
										   count: x.count,
										   days: x.days,
										   bookingAmt: parseFloat(x.bookingAmt).toFixed(2),
										   avgAmt: parseFloat(x.avgAmt).toFixed(2)
							   }; // creating the objects for chart
					}
			   });
			   console.dir(chartData);
		   }; // End function pushData
		   
		   let chart = new AmCharts.AmSerialChart();
		   
		   let generateChart = (chartData) => {
			    // SERIAL CHART
               let chart = new AmCharts.AmSerialChart();

               chart.dataProvider = chartData;
               chart.categoryField = "date";

               // listen for "dataUpdated" event (fired when chart is inited) and call zoomChart method when it happens
               chart.addListener("dataUpdated", zoomChart);

               chart.synchronizeGrid = true; // this makes all axes grid to be at the same intervals

               // AXES
               // category
               let categoryAxis = chart.categoryAxis;
               categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
               categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to DD
               categoryAxis.minorGridEnabled = true;
               categoryAxis.axisColor = "#DADADA";
               categoryAxis.twoLineMode = true;
               categoryAxis.dateFormats = [{
                    period: 'fff',
                    format: 'JJ:NN:SS'
                }, {
                    period: 'ss',
                    format: 'JJ:NN:SS'
                }, {
                    period: 'mm',
                    format: 'JJ:NN'
                }, {
                    period: 'hh',
                    format: 'JJ:NN'
                }, {
                    period: 'DD',
                    format: 'DD'
                }, {
                    period: 'WW',
                    format: 'DD'
                }, {
                    period: 'MM',
                    format: 'MMM'
                }, {
                    period: 'YYYY',
                    format: 'YYYY'
                }];

               // first value axis (on the left)
               let valueAxis1 = new AmCharts.ValueAxis();
               valueAxis1.axisColor = "#FF6600";
               valueAxis1.axisThickness = 2;
               chart.addValueAxis(valueAxis1);

               // second value axis (on the right)
               let valueAxis2 = new AmCharts.ValueAxis();
               valueAxis2.position = "right"; // this line makes the axis to appear on the right
               valueAxis2.axisColor = "#FCD202";
               valueAxis2.gridAlpha = 0;
               valueAxis2.axisThickness = 2;
               chart.addValueAxis(valueAxis2);

               // third value axis (on the left, detached)
               let valueAxis3 = new AmCharts.ValueAxis();
               valueAxis3.offset = 50; // this line makes the axis to appear detached from plot area
               valueAxis3.gridAlpha = 0;
               valueAxis3.axisColor = "#B0DE09";
               valueAxis3.axisThickness = 2;
               chart.addValueAxis(valueAxis3);
			   
			   // third value axis (on the left, detached)
               let valueAxis4 = new AmCharts.ValueAxis();
			   valueAxis4.position = "right"; // this line makes the axis to appear on the right
               valueAxis4.offset = 50; // this line makes the axis to appear detached from plot area
               valueAxis4.gridAlpha = 0;
               valueAxis4.axisColor = "#3598DC";
               valueAxis4.axisThickness = 2;
               chart.addValueAxis(valueAxis4);

               // GRAPHS
               // first graph
               let graph1 = new AmCharts.AmGraph();
               graph1.valueAxis = valueAxis1; // we have to indicate which value axis should be used
               graph1.title = "Booking Count";
               graph1.valueField = "count";
               graph1.bullet = "round";
               graph1.hideBulletsCount = 30;
               graph1.bulletBorderThickness = 1;
               chart.addGraph(graph1);

               // second graph
               let graph2 = new AmCharts.AmGraph();
               graph2.valueAxis = valueAxis2; // we have to indicate which value axis should be used
               graph2.title = "Days Booked";
               graph2.valueField = "days";
               graph2.bullet = "square";
               graph2.hideBulletsCount = 30;
               graph2.bulletBorderThickness = 1;
               chart.addGraph(graph2);

               // third graph
               let graph3 = new AmCharts.AmGraph();
               graph3.valueAxis = valueAxis3; // we have to indicate which value axis should be used
               graph3.valueField = "bookingAmt";
               graph3.title = "Total Booking Amount";
               graph3.bullet = "triangleUp";
               graph3.hideBulletsCount = 30;
               graph3.bulletBorderThickness = 1;
               chart.addGraph(graph3);
			   
			   // Fourth graph
               let graph4 = new AmCharts.AmGraph();
               graph4.valueAxis = valueAxis4; // we have to indicate which value axis should be used
               graph4.valueField = "avgAmt";
               graph4.title = "Average Room Night";
               graph4.bullet = "triangleUp";
               graph4.hideBulletsCount = 30;
               graph4.bulletBorderThickness = 1;
               chart.addGraph(graph4);

               // CURSOR
               let chartCursor = new AmCharts.ChartCursor();
               chartCursor.cursorAlpha = 0.1;
               chartCursor.fullWidth = true;
               chartCursor.valueLineBalloonEnabled = true;
               chart.addChartCursor(chartCursor);

               // SCROLLBAR
               let chartScrollbar = new AmCharts.ChartScrollbar();
               chart.addChartScrollbar(chartScrollbar);

               // LEGEND
               let legend = new AmCharts.AmLegend();
               legend.marginLeft = 110;
               legend.useGraphSettings = true;
               chart.addLegend(legend);
			   
			   /*let exports = new AmCharts.export();
			   exports.enabled = true;*/
			
               // WRITE CHART
               chart.write("chartdiv");
		   };
		
		   let checkDate = (date) => {
			   if ( Object.prototype.toString.call(date) === "[object Date]" ) {
				  // it is a date
				  if (isNaN(date.getTime())) return false; // date is not valid
				  else return true; // date is valid
				}
				else return false; // not a date
		   } // Fucntion if the object passed is a date object
  
  		   // this method is called when chart is first inited as we listen for "dataUpdated" event
           function zoomChart() {
               // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
               chart.zoomToIndexes(10, 20);
           }

</script> 

<!-- END CONTENT (c) The Infinity Tree --> 




