<style>
 @media print and (orientation: landscape) {
  body * {
    visibility: hidden;
  }
  #sample_editable_1, #sample_editable_1 * {
    visibility: visible;
  }
  #sample_editable_1 {
    position: absolute;
    left: 0;
    top: 0;
  }
}
</style>

<div class="row">
  <div class="col-md-12 "> 
    <!-- BEGIN SAMPLE FORM PORTLET-->

    <!-- BEGIN SAMPLE TABLE PORTLET-->
	<table id="logo-name" >
	<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel')); ?>
    <tr>
      <td align="left" valign="middle"><img src="<?php echo base_url();?>upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/></td>
		<td align="right" valign="middle"><?php echo "<strong style='font-size:18px;'>".$hotel_name->hotel_name.'</font></strong>'?></td>
        </tr>		
    
    <tr>
      <td width="100%" colspan="2"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td colspan="2"><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%" colspan="2">&nbsp;</td>
    </tr>
</table>
	
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="glyphicon glyphicon-bed"></i>List of All Rooms </div>
        <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            
          </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="sample_4">
          <thead>
            <tr>
              <th scope="col">Year</th>              <th scope="col">Month</th>              <th scope="col">Total Room Inventory</th>              <th scope="col">Occupancy</th>
              <th scope="col">RevPar (Incl. Inclusions)</th>
              <th scope="col">RevPAR (Excl. Inclusions)</th>
              <th scope="col">Total Room Rent</th>
              <th scope="col">Total Room Inclusions</th>
              <th scope="col">Avg.Daily Rate (Incl.Inclusion)</th>
              <th scope="col">Total Taxes</th>
              <th scope="col">Gross Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
                <?php						$countR = $this->dashboard_model->roomCount1();										if(isset($revPar) && $revPar) {												foreach($revPar as $revPar){
					
				?>
              <td align="center">
                <?php 				
					echo $revPar->year;
				?>
              </td>			  			  <td align="center">                <?php 									echo $revPar->month;				?>              </td>			  			  <td align="center">                <?php 					echo $revPar->rmcnt;				?>              </td>			  			  <td align="center">                <?php 					echo number_format($revPar->occu,2).' %';				?>              </td>
              <td align="center">
				<?php 																			echo number_format(($revPar->bkrev + $revPar->totTx + $revPar->excAmt + $revPar->posTot + $revPar->excTax)/$revPar->rmcnt,2);				?>
              </td>
              <td align="center">
                <?php 																			echo number_format(($revPar->bkrev + $revPar->totTx)/$revPar->rmcnt,2);				?>
                </td>
              <td align="center">				<?php 									echo number_format($revPar->totRR,2);
				?>			  </td>
              <td align="center">				<?php 					echo number_format(($revPar->excAmt + $revPar->posTot - $revPar->posTax),2); 				?>			  </td>
              <td align="center">
                <?php  
											
									?>
                </td>
              <td align="center">
                <?php 
					echo number_format($revPar->totTx + $revPar->excTax + $revPar->posTax,2);		
									?>
                </td>
              <td align="center">
                <?php 
									?>
                </td>
            </tr>						<?php 								}					}						?>
            </a>
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<!-- END PAGE CONTENT--> 