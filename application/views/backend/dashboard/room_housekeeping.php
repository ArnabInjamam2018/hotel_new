<div class="portlet light bordered">
<div class="portlet-title">
  <div class="caption"> <i class="fa fa-file-archive-o"></i>Detailed Room Status</div>
</div>
<div class="portlet-body">
  <div class="table-toolbar">
    <div class="row">
      <div class="col-md-10">
        <form  action="<?php echo base_url() ?>dashboard/change_room_status_bulk" method="post" class="form-horizontal" >
          <div class="form-group">
            <label class="col-md-3 control-label">Housekeping Status</label>
            <div class="col-md-4">
              <select onchange="status_change()" name="room_status" class="form-control input-md-4">
                <?php 
          $hks=$this->dashboard_model->get_all_housekeeping_status();
          foreach($hks as $h) 
          {?>
                <option value="<?php echo $h->status_id;?>" class="so"><?php echo $h->status_name;?></option>
                <?php } ?>
              </select>
            </div>
            <?php foreach($logs as $maids){ ?>
          <label style = "display:none;"> <input type="checkbox" value="<?php echo $maids->room_id; ?>" id="get_<?php echo $maids->room_id; ?>" name="get_<?php echo $maids->room_id; ?>"> <?php echo $maids->room_no; ?></label>
            <?php } ?>
            <input type="submit" Value="Change Status" class="btn green">
            <a onclick="get_print();" class="btn blue"><i class="fa fa-print"></i>&nbsp;Print List</a>

          </div>
        </form>
      </div>
      
    </div>
  </div>

  <table class="table table-striped table-bordered table-newhover" id="sample_1">
    <thead>
      <tr>
        <th>
        <!---<div class="checkbox-list"><label>
            <input type="checkbox" name="check_all" id="checkAll" value="1">Check all</label>
            </div>---></th>
        <th>Room No </th>
        <th>Room Type</th>
        <th>Housekeeping Status</th>
        <th>Availablity</th>
        <th>Auditing Status</th>
        <th>Assigned Staff Nmae </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <?php
                if(isset($logs) && $logs){
                $srl_no=0;
                //print_r($maids);
                $date1=date("Y-m-d",strtotime(date("Y-m-d")));
                $array= $this->dashboard_model->get_booking_details_by_date($date1);
                /*print_r($array);
                exit;*/
                foreach($logs as $maids){
                $srl_no++;
              $out=0;
              if($array!=false)
              {
         foreach($array as $ar){if (($ar->room_id==$maids->room_id) && ($ar->confirmed_checkin_time!="00:00:00"))$out++;};
              }
        /*print_r($array);
         exit;*/
         
          ?>
        <td>
            <div class="md-checkbox">
                <input type="checkbox" id="chk_<?php echo $maids->room_id;?>" class="md-check" name="chk_<?php echo $maids->room_id;?>" value="<?php echo $maids->room_id;?>" onclick="activate_room(<?php echo $maids->room_id;?>)">
                <label for="chk_<?php echo $maids->room_id;?>">
                    <span></span>
                    <span class="check"></span>
                    <span class="box"></span>
                </label>
            </div>
           <!--<div class="checkbox-list">
           <label>
              <input type="checkbox" class="chk" id="chk_<?php echo $maids->room_id;?>" name="chk_<?php echo $maids->room_id;?>" value="<?php echo $maids->room_id;?>" onclick="activate_room(<?php echo $maids->room_id;?>)" />
            </label>
          </div>-->
       	</td>
      
        <td><?php $rm_no=$this->dashboard_model->get_room_no($maids->room_id);
					if(isset($rm_no) && $rm_no){
					echo $rm_no->room_no;
					}
		?></td>
        <td><?php $u_name=$this->dashboard_model->get_unit_type_by_id($maids->unit_id);
                if(isset($u_name) && $u_name){echo  $u_name->unit_name; }
				else echo 'id - '.$maids->unit_id;
        ?></td>
        
        <td <?php
                $clr=$this->dashboard_model->get_all_housekeeping_status_by_id($maids->clean_id);
                if($clr and ($clr!= false))
                {
                echo "style='color:#ffffff; text-align:center; background-color:".$clr->color_primary."'";
                }
                else
                {
                echo "style='color:#ffffff; text-align:center; background-color:f1f1f1f1'";
                }
                ?>>
        <?php $st=$this->dashboard_model->get_all_housekeeping_status_by_id($maids->clean_id);
        if(isset($st) && $st){
        echo $st->status_name."";}?></td>
        <?php if($out>0){ echo"<td class='label-danger' style='color:#ffffff; text-align:center;'>Booked</td>";} else {echo "<td class='label-success' style='color:#ffffff;text-align:center;'>".$maids->room_availability."</td>";} $out=0;?>
        <td><?php echo $maids->room_audit_status;?></td>
        <td  style="color:#ffffff; text-align:center;">
        <?php  
          $i=0;
          $staff=$this->dashboard_model->get_all_staff_type();
          if(isset($staff) && $staff)
          {
              $count=0;
            foreach($staff as $sf)
            {
                
          
          $rmm=$this->dashboard_model->get_housekeeping_pending_asso_staff($maids->room_id,$sf->housekeeping_staff_type_name,'room');
                     if($rmm)
                        {
                                        
                                    foreach($rmm as $assign)
                                    {
                                        
                                          ?>
        <span class="label-success " style="padding:5px; color:#ffffff; text-align:center; "><b><?php echo $this->dashboard_model->get_maid_by_id($assign->staff_id)->maid_name." (".$sf->housekeeping_staff_type_name.")";?></b></span>
        <?php
                                    }
                            $count++;	
                        }
                $i=$i+2;	
            }
                if($count==0)
                echo "<span class='label-warning' style='color:#ffffff; text-align:center; padding:5px;'>No Staff Assigned !!</span>";
          }?>
          </td>
      </tr>
      <?php }}?>
    </tbody>
  </table>
</div>
</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script>

$(function(){
    $("#checkAll").click(function () {
		
        if($(this).is(':checked')){
           $(".checker > span").addClass ( 'checked' );
		   $('.chk').attr('checked', this.checked);
        } else {
            $(".checker > span").removeClass ( 'checked' );
			$('.chk').prop('checked', false);
		}
    });
 
    $(".chk").click(function(){
        //alert($(".chk").length );
        //alert($(".chk:checked").length );
  
        if($(".chk").length == $(".chk:checked").length) {
            $("#checkAll").attr("checked", "checked");
			$("#uniform-checkAll > span").addClass ( 'checked' );
        } else {
            $("#checkAll").removeAttr("checked");
			$("#uniform-checkAll > span").removeClass ( 'checked' );
        }
 
    });
});
	
</script>
<script>
function activate_room(a)
{
	//alert(a);
	var stat = $('#chk_'+a).is(":checked") ?  'in':'out' ;
	//alert (stat);
	
	if(stat == 'in')
	{
	$('#get_'+a).attr('checked','checked');
	}
	else
	{
	$('#get_'+a).removeAttr("checked");
	}
	
	
} 
</script>
<script>
function get_print()
{
	//alert("hello");
	($('#sample_editable_1').DataTable()).destroy();
	$('#sample_editable_1').DataTable({paging: false,});
	$('body').css('visibility', 'hidden');
	$('#sample_editable_1').css('visibility','visible');
	window.print();
	$('body').css('visibility', 'visible');
	($('#sample_editable_1').DataTable()).destroy();
	$('#sample_editable_1').DataTable({pagelength:10,});
	//location.reload();
}
</script>
