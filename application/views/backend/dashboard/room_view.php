<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Room View</span> </div>
  </div>
  <div class="portlet-body form">
    <div class="form-body"> 
      <div class="room-cat"> 
	  <?php if(isset($unit) && $unit){
		  foreach($unit as $u){
		  ?> 
		  <h5><b><?php echo $u->unit_name; ?></b></h5>    
          <div class="row">
		  <?php  
		  $flg=0;
		  if(isset($rooms) && $rooms){
		  foreach($rooms as $room){
			  if($u->unit_name!=$flg){
			  ?>
      	  
		  <?php }?>
            <div class="col-md-2">
                <div class="room-v bookd" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      <?php echo $room->room_no ;?>
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span>
                             <i class="fa fa-group" aria-hidden="true" style="color:#138e9d;"></i>
                             <dd>Kartik Reddy</dd> <small>(HM06004709)</small>
                         </span>
                         <span>
                             Checked in (D) 
                             <i class="fa fa-minus-circle" style="color:#E27979;"></i>  
                             <i class="fa fa-plus" style="color:#0000B3;"></i> 
                             <small style="float:right;">FLR: 4</small>                         
                         </span>
                     </div>
                </div>
            </div>
		  <?php }} ?>
		  </div>
			
		  <?php } }?>
      </div>
      
      
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn submit">Submit</button>
      <button  type="reset" class="btn default">Reset</button>
    </div
  ></div>
</div>