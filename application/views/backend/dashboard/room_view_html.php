<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Room View</span> </div>
  </div>
  <div class="portlet-body form">
    <div class="form-body"> 
      <div class="room-cat"> 
      	  <h5>AC Non Standard Room</h5>    
          <div class="row">
            <div class="col-md-2">
                <div class="room-v bookd" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span>
                             <i class="fa fa-group" aria-hidden="true" style="color:#138e9d;"></i>
                             <dd>Kartik Reddy</dd> <small>(HM06004709)</small>
                         </span>
                         <span>
                             Checked in (D) 
                             <i class="fa fa-minus-circle" style="color:#E27979;"></i>  
                             <i class="fa fa-plus" style="color:#0000B3;"></i> 
                             <small style="float:right;">FLR: 4</small>                         
                         </span>
                     </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="room-v" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span class="emt">sdvfdsv</span>
                     </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="room-v bookd" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span>
                             <i class="fa fa-group" aria-hidden="true" style="color:#138e9d;"></i>
                             <dd>Kartik Reddy</dd> <small>(HM06004709)</small>
                         </span>
                         <span>
                             Checked in (D) 
                             <i class="fa fa-minus-circle" style="color:#E27979;"></i>  
                             <i class="fa fa-plus" style="color:#0000B3;"></i> 
                             <small style="float:right;">FLR: 4</small>                         
                         </span>
                     </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="room-v" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span class="emt">sdvfdsv</span>
                     </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="room-v bookd" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span>
                             <i class="fa fa-group" aria-hidden="true" style="color:#138e9d;"></i>
                             <dd>Kartik Reddy</dd> <small>(HM06004709)</small>
                         </span>
                         <span>
                             Checked in (D) 
                             <i class="fa fa-minus-circle" style="color:#E27979;"></i>  
                             <i class="fa fa-plus" style="color:#0000B3;"></i> 
                             <small style="float:right;">FLR: 4</small>                         
                         </span>
                     </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="room-v" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span class="emt">sdvfdsv</span>
                     </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="room-v bookd" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span>
                             <i class="fa fa-group" aria-hidden="true" style="color:#138e9d;"></i>
                             <dd>Kartik Reddy</dd> <small>(HM06004709)</small>
                         </span>
                         <span>
                             Checked in (D) 
                             <i class="fa fa-minus-circle" style="color:#E27979;"></i>  
                             <i class="fa fa-plus" style="color:#0000B3;"></i> 
                             <small style="float:right;">FLR: 4</small>                         
                         </span>
                     </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="room-v" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span class="emt">sdvfdsv</span>
                     </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="room-v bookd" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span>
                             <i class="fa fa-group" aria-hidden="true" style="color:#138e9d;"></i>
                             <dd>Kartik Reddy</dd> <small>(HM06004709)</small>
                         </span>
                         <span>
                             Checked in (D) 
                             <i class="fa fa-minus-circle" style="color:#E27979;"></i>  
                             <i class="fa fa-plus" style="color:#0000B3;"></i> 
                             <small style="float:right;">FLR: 4</small>                         
                         </span>
                     </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="room-v" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span class="emt">sdvfdsv</span>
                     </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="room-v bookd" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span>
                             <i class="fa fa-group" aria-hidden="true" style="color:#138e9d;"></i>
                             <dd>Kartik Reddy</dd> <small>(HM06004709)</small>
                         </span>
                         <span>
                             Checked in (D) 
                             <i class="fa fa-minus-circle" style="color:#E27979;"></i>  
                             <i class="fa fa-plus" style="color:#0000B3;"></i> 
                             <small style="float:right;">FLR: 4</small>                         
                         </span>
                     </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="room-v" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span class="emt">sdvfdsv</span>
                     </div>
                </div>
            </div>
          </div>
      </div>
      <div class="room-cat"> 
      	  <h5>Non AC Standard Room</h5>    
          <div class="row">
            <div class="col-md-2">
                <div class="room-v bookd" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span>
                             <i class="fa fa-group" aria-hidden="true" style="color:#138e9d;"></i>
                             <dd>Kartik Reddy</dd> <small>(HM06004709)</small>
                         </span>
                         <span>
                             Checked in (D) 
                             <i class="fa fa-minus-circle" style="color:#E27979;"></i>  
                             <i class="fa fa-plus" style="color:#0000B3;"></i> 
                             <small style="float:right;">FLR: 4</small>                         
                         </span>
                     </div>
                </div>
            </div>
          </div>
      </div>
      <div class="room-cat"> 
      	  <h5>Non AC Standard Room</h5>    
          <div class="row">
            <div class="col-md-2">
                <div class="room-v bookd" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span>
                             <i class="fa fa-group" aria-hidden="true" style="color:#138e9d;"></i>
                             <dd>Kartik Reddy</dd> <small>(HM06004709)</small>
                         </span>
                         <span>
                             Checked in (D) 
                             <i class="fa fa-minus-circle" style="color:#E27979;"></i>  
                             <i class="fa fa-plus" style="color:#0000B3;"></i> 
                             <small style="float:right;">FLR: 4</small>                         
                         </span>
                     </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="room-v" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span class="emt">sdvfdsv</span>
                     </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="room-v" style="border-right: 7px solid #e1b80d ;">
                    <div class="room-vstatus" style="background:#9B383A;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          <small class="rava active">AV</small>
                      </span>
                    </div>
                    <div style="color:#e1b80d" class="rroom-noo">
                      103
                      <span>Room</span>
                    </div>
                    <div class="rbed">
                      <span>3 </span>
                      <span>(4)</span>
                    </div>
                    <div class="room-v-cl">
                         <span class="emt">sdvfdsv</span>
                     </div>
                </div>
            </div>
          </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn submit">Submit</button>
      <button  type="reset" class="btn default">Reset</button>
    </div
  ></div>
</div>