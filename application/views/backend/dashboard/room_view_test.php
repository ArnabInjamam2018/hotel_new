<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Room View TEst</span> </div>
  </div>
  <div class="portlet-body">
    <div class="form-body"> 
      <div class="room-cat"> 
		  <?php  
		  $flg=0;
		  $typeOld = '';
		  if(isset($rooms) && $rooms){
			  foreach($rooms as $room){
				  
				  if((int)$room->group_id > 0 || (int)$room->booking_id > 0){
						$avl = '<small class="rava">BK';
						$bkd = 'bookd';
						
						if((int)$room->group_id > 0){
							$grp = '<i class="fa fa-group" aria-hidden="true" style="color:#138e9d;"></i>';
						}
						else
							$grp = '';
						
						if($room->exc_amt > 0)
							  $exc = '<i class="fa fa-plus" aria-hidden="true" style="color:#0000B3;"></i>';
						else
							  $exc = '';
						  
						if($room->pos_amt > 0)
							  $pos = '<i style="color:#FFAE04; " class="fa fa-cutlery" aria-hidden="true"></i>';
						else
							  $pos = '';
						
				  }
				  else{
								$avl = '<small class="rava active">AV';
								$bkd = '';
								$grp = '';
								$exc = '';
								$pos = '';
				  }
				  
				 if($typeOld != $room->unit_name){
					if($flg == 1){
				  ?>
						</div>
					<?php } ?>
				 </br>
				
				<h5><b><?php echo $room->unit_name;?></b> <?php echo $room->unit_class;?> | <?php echo 'Default Occupancy '.$room->default_occupancy;?>/ <?php echo 'Max Occupancy '.$room->max_occupancy;?></h5>
				
				
				<div class="row">
				 <?php }
					$flg = 1;
				 ?>
				  
				 <div class="col-md-2">
                  <div class="room-v <?php echo $bkd;?>" style="border-right: 7px solid <?php echo $room->color_primary;?>; background: <?php echo $room->pBdCC;?>;">
                    <div class="room-vstatus" style="background:<?php echo $room->pBaCC;?>;"></div>
                    <div class="rot-av">
                      <span class="rota">Ota</span>
                      <span>
                          <small class="rac active">AC</small>
                          
						  <?php 
							echo $avl;
						  ?></small>
						  
                      </span>
                    </div>					 
                    <div style="color:<?php echo $room->color_primary;?>" class="rroom-noo">
                      <?php echo $room->room_no;?>
                      <span><?php echo $room->unit_type;?></span>                      
                    </div>
                    <div class="rbed">					  
                   	  <?php echo $grp;?>
                      <?php echo $exc;?>
                      <i class="fa fa-minus-circle" style="color:#E27979;"></i>
                      <?php echo $pos;?>               
                    </div>
                    <div class="room-v-cl">
                         <span>                             
                             <dd><?php echo $room->g_name;?></dd> 
                             <small>Flr: <?php echo $room->floor_no;?></small>
                             <small><?php 
								if($room->booking_id > 0) 
									echo 'BK0'.$room->booking_id; 
								else 
									echo '';?></small>
                         </span>
                         <span>
                             <?php echo $room->pSts;?> 
                             <?php echo '<small style="color:'.$room->sBaCC.';">'.$room->sSts.'</small>';?>
                         </span>
                     </div>
					</div>
				</div>
				  
				  <?php
				  
				  $typeOld = $room->unit_name;
				  
				 ?>
			  <?php 
			  } 
		  }
		  ?>
      </div>
      
      
    </div>
    
	</div>
</div>