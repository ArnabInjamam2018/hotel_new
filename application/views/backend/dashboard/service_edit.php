
<script type="text/javascript">
	var click_count= 0;

    $(document).ready(function(){
        $("#service_tax").hide();
        $("#service_tax1").hide();
        $("#service_tax2").hide();
        $("#service_tax3").hide();
        $("#upload_logo").hide();
        $("#upload_text").hide();
    });

    function hideShowLogo()
    {
        //alert(document.getElementById('st_logo'));
        if($('#st_logo').val() == 'Yes')
        {
            $('#upload_logo').show();
            $('#upload_text').hide();
            $("#text").removeAttr("required");
            $("#logo").attr("required","true");
        }
        else if ($('#st_logo').val() == 'No')
        {
            $('#upload_logo').hide();
            $('#upload_text').show();
            $("#logo").removeAttr("required");
            $("#text").attr("required","true");
        }
        else
        {
            $('#upload_logo').hide();
            $('#upload_text').hide();
            $("#logo").removeAttr("required");
            $("#text").removeAttr("required");
        }
    }
    function hideShow()
    {
        if($('#tax').val() == 'Yes')
        {
            $("#service_tax").show();
            $("#service_tax1").show();
            $("#service_tax2").show();
            $("#service_tax3").show();
        }
        else
        {
            $("#service_tax").hide();
            $("#service_tax1").hide();
            $("#service_tax2").hide();
            $("#service_tax3").hide();
        }
    }

</script>
<script>

    function fetch_all_address_hotel(pincode,g_country,g_state,g_city)
    {
        var pin_code = document.getElementById(pincode).value;
        //alert(pincode + "/" + g_country + "/" + g_state + "/" + g_city+"=" + pin_code);


        jQuery.ajax(
            {
                type: "POST",
                url: "<?php echo base_url(); ?>bookings/fetch_address",
                dataType: 'json',
                data: {pincode: pin_code},
                success: function(data){
                    //alert(data.country);
                    $(g_country).val(data.country);
                    $(g_state).val(data.state);
                    $(g_city).val(data.city);
                }

            });
    }




</script>
<script type="text/javascript">
    function submit_form()
    {
        document.getElementById('submit_form').submit();
    }
</script>
<!-- BEGIN PAGE CONTENT-->
<?php 
 if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue" id="form_wizard_1">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> EDIT SERVICE - <span class="step-title"> Step 1 of 4 </span> </div>
    <div class="tools hidden-xs"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body form">
    <form action="<?php echo base_url();?>dashboard/edit_service" enctype="multipart/form-data" id="submit_form" method="POST">
      <div class="form-wizard">
        <div class="form-body">
          <ul class="nav nav-pills nav-justified steps">
            <li> <a href="#tab1" data-toggle="tab" class="step"> <span class="number"> 1 </span> <span class="desc"> <i class="fa fa-check"></i> Service Information </span> </a> </li>
            <li> <a href="#tab2" data-toggle="tab" class="step"> <span class="number"> 2 </span> <span class="desc"> <i class="fa fa-check"></i> Eligibility criteria 1 </span> </a> </li>
            <li> <a href="#tab3" data-toggle="tab" class="step active"> <span class="number"> 3 </span> <span class="desc"> <i class="fa fa-check"></i> Price and Tax </span> </a> </li>
          </ul>
          <div id="bar" class="progress progress-striped" role="progressbar">
            <div class="progress-bar progress-bar-success"> </div>
          </div>
          <div class="tab-content">
            <div class="alert alert-danger display-none">
              <button class="close" data-dismiss="alert"></button>
              You have some form errors. Please check below. </div>
            <div class="alert alert-success display-none">
              <button class="close" data-dismiss="alert"></button>
              Your form validation is successful! </div>
            <div class="tab-pane active" id="tab1">
              <div class="clearfix">
                <div class="col-md-4">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="s_name"  value="<?php if(isset($services->s_name)) { echo $services->s_name; } ?>" placeholder="Service Name *"/>
                    <label></label>
                    <span class="help-block">Service Name *</span> </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group form-md-line-input">
                    <select class="form-control bs-select"  name="s_category" >
                      <option value="" disabled="disabled" selected="selected">Service Category</option>
                      <option value="food & drink" <?php if($services->s_category=="food & drink"){echo "selected";} ?>>Food and Drink</option>
                      <option value="travel & tour" <?php if($services->s_category=="travel & tour"){echo "selected";} ?>>Travel and Tour</option>
                      <option value="room service" <?php if($services->s_category=="room service"){echo "selected";} ?>>Room Service</option>
                      <option value="transport" <?php if($services->s_category=="transport"){echo "selected";} ?>>Transport</option>
                      <option value="honeymoon" <?php if($services->s_category=="honeymoon"){echo "selected";} ?>>Honeymoon</option>
                      <option value="mixed" <?php if($services->s_category=="mixed"){echo "selected";} ?>>Mixed</option>
                    </select>
                    <label></label>
                    <span class="help-block">Service Category</span> </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group form-md-line-input">
                    <input value="<?php echo $services->s_description; ?>" type="text" class="form-control" name="s_description" placeholder="Service Description"/>
                    <label></label>
                    <span class="help-block">Service Description</span> </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group form-md-line-input">
                    <select class="form-control bs-select"  name="s_no_member" >
                      <option value="" disabled="disabled" selected="selected">Service Applied For *</option>
                      <option value="1" <?php if($services->s_no_member=="1"){echo "selected";} ?> >One Member</option>
                      <option value="2" <?php if($services->s_no_member=="2"){echo "selected";} ?> >All Member</option>
                      <option value="3" <?php if($services->s_no_member=="3"){echo "selected";} ?> >Custoem Number of Member</option>
                    </select>
                    <label></label>
                    <span class="help-block">Service Applied For *</span> </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="s_no_member_custom" placeholder="Custom Member"/>
                    <label></label>
                    <span class="help-block">Custom Member</span> </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group form-md-line-input">
                    <select class="form-control bs-select"  name="s_periodicity" >
                      <option value="" disabled="disabled" selected="selected">Service Periodicity *</option>
                      <option value="1" <?php if($services->s_periodicity=="1"){echo "selected";} ?>>Once per half day</option>
                      <option value="2" <?php if($services->s_periodicity=="2"){echo "selected";} ?>>Once per day</option>
                      <option value="3" <?php if($services->s_periodicity=="3"){echo "selected";} ?>>Once Per visit</option>
                      <option value="4" <?php if($services->s_periodicity=="4"){echo "selected";} ?>>Once every month</option>
                      <option value="5" <?php if($services->s_periodicity=="5"){echo "selected";} ?>>Once every year</option>
                    </select>
                    <label></label>
                    <span class="help-block">Service Periodicity *</span> </div>
                </div>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group form-md-line-input uploadss">
                        <label>Service Image</label>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/> </div>
                          <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                          <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
                            <input type="file" name="s_image" />
                            </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group form-md-line-input uploadss">
                        <label>Service Icon</label>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/> </div>
                          <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                          <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
                            <input type="file" name="s_icon" />
                            </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab2">
              <div class="row" id="add_service_step2">
                <div class="col-md-12 form-horizontal">
                  <div class="form-group form-md-line-input">
                    <label class="col-md-4 control-label">Apply To All Guest</label>
                    <div class="col-md-8">
                      <div class="md-radio-inline">
                        <div class="md-radio">
                          <input type="radio" id="radio6" value="1" name="rent_mode_type" class="md-radiobtn" onclick="banner_noshow()">
                          <label for="radio6"> <span></span> <span class="check"></span> <span class="box"></span> Yes</label>
                        </div>
                        <div class="md-radio">
                          <input type="radio" id="radio7" value="0" name="rent_mode_type" class="md-radiobtn" checked onclick="banner_show()">
                          <label for="radio7"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group form-md-line-input">
                    <input  class="form-control" value="<?php echo  $services->s_rules?>" type="text" id="rule_list" name="rule_list_val" placeholder="Rules Entered">
                    <label></label>
                    <span class="help-block">Rules Entered *</span> </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group form-md-line-input">
                    <select id="Master_cat" class="form-control bs-select" data-hide-disabled="true" title="Master Category" data-live-search="true" onchange="master_catagory(this.value)">
                      <option  value="" disabled selected> Master Category</option>
                      <option  value="Genaral Category" > General Category</option>
                      <option  value="Guest"> Guest</option>
                      <option  value="Booking"> Booking </option>
                      <option  value="Unit"> Unit </option>
                      <option  value="Event"> Event</option>
                    </select>
                    <label></label>
                    <span class="help-block">Master Category</span> </div>
                </div>
                <div class="col-md-3" id="gn" style="display:none;">
                  <div class="form-group form-md-line-input">
                    <select id="gen_cat"  class="form-control bs-select"  data-hide-disabled="true" title="Genaral Category" data-live-search="true" onchange="open_oparetor()">
                      <option  value="" disabled selected> Genaral Category</option>
                      <option  value="Total Bill" > Total Bill</option>
                      <option  value="date('Y-m-d')"> System Date</option>
                    </select>
                    <label></label>
                    <span class="help-block">Genaral Category</span> </div>
                </div>
                <div class="col-md-3" id="guest" style="display:none;">
                  <div class="form-group form-md-line-input">
                    <select name="s_guest" id="guest_cat"  class="form-control bs-select"   data-hide-disabled="true" title="Guest" data-live-search="true" onchange="open_oparetor()">
                      <option  value="" disabled selected> Guest</option>
                      <option  value="g_name" > Guest name</option>
                      <option  value="g_gender"> Guest gender</option>
                      <option  value="g_age" > Guest age</option>
                      <option  value="g_type"> Guest Type</option>
                      <option  value="g_state" > Guest State</option>
                      <option  value="g_vip=1"> VIP?</option>
                      <option  value="g_preffered=1?" > Preferred?</option>
                      <option  value="g_no_times"> No of times visited</option>
                      <option  value="g_total_spending" > Total spending</option>
                      <option  value="g_no_times=1"> New Guest?</option>
                      <option  value="Global category"> Global category</option>
                    </select>
                    <label></label>
                    <span class="help-block">Guest</span> </div>
                </div>
                <div class="col-md-3" id="booking" style="display:none;">
                  <div class="form-group form-md-line-input">
                    <select name="s_booking" id="booking_cat"  class="form-control bs-select"  data-hide-disabled="true" title="Booking" data-live-search="true" onchange="open_oparetor()">
                      <option  value="" disabled selected> Booking</option>
                      <option  value="cust_from_date" > Booking start date</option>
                      <option  value="cust_end_date"> Booking end date</option>
                      <option  value="no_of_guest" > No of Guest</option>
                      <option  value="no_of_guest >1"> Family?</option>
                      <option  value="no_of_guest =1" > Individual?</option>
                      <option  value="nature_visit"> Nature of visit</option>
                      <option  value="booking_source" > Booking source</option>
                      <option  value="stay_days"> No of stay days</option>
                      <option  value="room_rent_total_amount" > Total Rent</option>
                      <option  value="Global category"> Global category</option>
                      <option  value="Weekend"> Weekend</option>
                    </select>
                    <label></label>
                    <span class="help-block">Booking</span> </div>
                </div>
                <div class="col-md-3" id="unit" style="display:none">
                  <div class="form-group form-md-line-input">
                    <select name="s_unit" id="unit_cat"  class="form-control bs-select" data-hide-disabled="true" title="Unit" data-live-search="true" onchange="open_oparetor()">
                      <option  value="" disabled selected> Unit</option>
                      <option  value="room_no" > Room/ Unit no</option>
                      <option  value="room_bed"> No of Bed</option>
                      <option  value="floor_no" > Floor</option>
                      <option  value="Room category"> Room category</option>
                      <option  value="Global category" > Global category</option>
                    </select>
                    <label></label>
                    <span class="help-block">Unit</span> </div>
                </div>
                <div class="col-md-3" id="event" style="display:none">
                  <div class="form-group form-md-line-input">
                    <select name="s_event" id="event_cat"  class="form-control bs-select" data-hide-disabled="true" title="Event" data-live-search="true" onchange="open_oparetor()">
                      <option  value="" disabled selected> Event</option>
                      <option  value="e_name" > Event Name</option>
                      <option  value="e_from <=DATE() OR DATE() >=e_upto "> is Event?</option>
                    </select>
                    <label></label>
                    <span class="help-block">Event</span> </div>
                </div>
                <div class="col-md-3" id="oparetor" style="display:none;" >
                  <div class="form-group form-md-line-input" >
                    <select name="s_operator" id="operator_cat"  class="form-control bs-select"   data-hide-disabled="true" title="Oparetor" data-live-search="true" onchange= "open_value()">
                      <option  value="" disabled selected> oparetor</option>
                      <option  value="=" > =</option>
                      <option  value=">" > > </option>
                      <option  value="<" > < </option>
                      <option  value="IN "> IN </option>
                      <option  value="LIKE"> LIKE </option>
                      <option  value="NOT EQUAL TO"> NOT EQUAL TO </option>
                      <option  value="NOT IN"> NOT IN </option>
                      <option  value="IS NULL"> IS NULL </option>
                      <option  value="IS NOT NULL"> IS NOT NULL </option>
                      <option  value="NOT LIKE"> NOT LIKE </option>
                    </select>
                    <label></label>
                    <span class="help-block">Oparetor</span> </div>
                </div>
                <div class="col-md-3" id="value_one" style="display:none;" >
                  <div class="form-group form-md-line-input" >
                    <input type="text" id="rule_value"   class="form-control" name="s_value" placeholder="value" onkeypress="open_tgl_btn()" />
                    <label></label>
                    <span class="help-block">value</span> </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group form-md-line-input">
                    <button class="btn blue" onclick="append_div();reset_div();"> Add Another Rule </button>
                    <button class="btn blue" onclick="append_group()"> Start Grouping</button>
                    <div id="and-or"  style="display:none; margin:0 10px;">
                      <div class="form-group form-md-radios">
                        <div class="md-radio-inline">
                          <div class="md-radio" style="margin-right: 12px;">
                            <input type="radio" id="radio8" name="rent_mode_type" class="md-radiobtn" value="AND" onclick="concat1(this.value)">
                            <label for="radio8"> <span></span> <span class="check"></span> <span class="box"></span> AND</label>
                          </div>
                          <div class="md-radio">
                            <input type="radio" id="radio9" name="rent_mode_type" class="md-radiobtn" value="OR" onclick="concat1(this.value)">
                            <label for="radio9"> <span></span> <span class="check"></span> <span class="box"></span> OR</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <button id="end_grouping" class="btn blue" onclick="append_group2()" style="display:none;"> End Grouping</button>
                  </div>
                </div>
              </div>
            </div>
            <script>
                        var x = 1;

                        setInterval(function(){
                            var data= document.getElementById('rule_list').value;
                            if(data.indexOf("(") > -1){

                                document.getElementById('end_grouping').style.display="inline-block";

                            }
                            else{
                                document.getElementById('end_grouping').style.display="none";
                            }

                        },1000);

                        function concat1(value){
                            document.getElementById('rule_list').value+= '  '+ value +' ';
                            document.getElementById('and-or').style.display="none";

                        }
                       function append_group(){
                           document.getElementById('rule_list').value+= ' ( ';
                       }
                        function append_group2(){
                            document.getElementById('rule_list').value+= ' ) ';
                        }

                        function append_div(){

                            document.getElementById('and-or').style.display="inline-block";
							var booking_cat = document.getElementById('booking_cat').value;
                            var unit_cat = document.getElementById('unit_cat').value;
                            var guest_cat = document.getElementById('guest_cat').value;
                            var gen_cat = document.getElementById('gen_cat').value;
                            var event_cat = document.getElementById('event_cat').value;
							var operator_cat = document.getElementById('operator_cat').value;
							var rule_value = document.getElementById('rule_value').value;
                           // var and_or = document.getElementById('and_or').value;

                            //alert(and_or);


                            var total_rule=booking_cat+' '+unit_cat+' '+guest_cat+' '+gen_cat+' '+event_cat+' '+operator_cat+' '+"'"+rule_value+"'";
                           // alert(total_rule);
                            document.getElementById('rule_list').value+= ' '+ total_rule +' ';

                            /*
                            $('#booking_cat').val( $('#booking_cat').prop('defaultSelected') );
                            $('#unit_cat').val( $('#unit_cat').prop('defaultSelected') );
                            $('#guest_cat').val( $('#guest_cat').prop('defaultSelected') );
                            $('#operator_cat').val( $('#operator_cat').prop('defaultSelected') );
                            */                           

                           // $('#booking_cat').prop('selectedIndex',0);
                            

                           // $("#tab2").load("<?php echo base_url() ?>assets/html_static/rule_add.html");

                         /*   var elements = document.getElementsByTagName('booking_cat');
                            for (var i = 0; i < elements.length; i++)
                            {
                                elements[i].selectedIndex = 0;
                            }
                           // document.getElementById('booking_cat').selectedIndex=0;
                           document.getElementById('unit_cat').value="";
                           document.getElementById('guest_cat').value="";
                            document.getElementById('gen_cat').value="";
                             document.getElementById('event_cat').value="";
                            document.getElementById('operator_cat').value="";
                             document.getElementById('rule_value').value="";*/

                            /*
							
							if (master_cat == "Genaral Category"){
								var gen_val = document.getElementById('gen_cat').value;
                                document.getElementById('rule_list').innerHTML += '<br> Rule ' + x +': &nbsp; <input class="form-control" type="text" value='+ gen_val +' '+ operator_cat +' '+ rule_value +'>'; 
								//alert(gen_val);
							}
							else if (master_cat == "Guest"){
								var guest_val = document.getElementById('guest_cat').value;
                                document.getElementById('rule_list').innerHTML += '<br> Rule ' + x +': &nbsp; <input class="form-control" type="text" value='+ guest_val +' '+ operator_cat +' '+ rule_value +'>'; 
								//alert(guest_val);
							}
							else if (master_cat == "Booking"){
								var booking_val = document.getElementById('booking_cat').value;
                                document.getElementById('rule_list').innerHTML += '<br> Rule ' + x +': &nbsp; <input class="form-control" type="text" value='+ booking_val +' '+ operator_cat +' '+ rule_value +'>'; 
								//alert(booking_val);
							}
							else if (master_cat == "Unit"){
								var unit_val = document.getElementById('unit_cat').value;
                                document.getElementById('rule_list').innerHTML += '<br> Rule ' + x +': &nbsp; <input class="form-control" type="text" value='+ unit_val +' '+ operator_cat +' '+ rule_value +'>'; 
								//alert(unit_val);
							}
							else{
								var event_val = document.getElementById('event_cat').value;
                                document.getElementById('rule_list').innerHTML += '<br> Rule ' + x +': &nbsp; <input class="form-control" type="text" value='+ event_val +' '+ operator_cat +' '+ rule_value +'>'; 
								//alert(event_val);
							}*/
                            
                            x = x+1;
						}
					
					</script>
            <div class="tab-pane clearfix" id="tab3">
              <div class="row">
                <input type="hidden" name="s_rules" id="s_rules">
                <div class="col-md-4">
                  <div class="form-group form-md-line-input">
                    <input value="<?php echo  $services->s_price ?>"  type="type" autocomplete="off" class="form-control" id="form_control_1" name="s_price" required="required" onkeypress="return onlyNos(event, this);" placeholder="Base Price *">
                    <label></label>
                    <span class="help-block">Base Price *</span> </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group form-md-line-input">
                    <input value="<?php echo  $services->s_price_weekend?>"  type="type" autocomplete="off" class="form-control" id="form_control_1" name="s_price_weekend" required="required" onkeypress="return onlyNos(event, this);" placeholder="Weekend Price *">
                    <label></label>
                    <span class="help-block">Weekend Price *</span> </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group form-md-line-input">
                    <input value="<?php echo  $services->s_price_holiday ?>"  type="type" autocomplete="off" class="form-control" id="form_control_1" name="s_price_holiday" required="required" onkeypress="return onlyNos(event, this);" placeholder="Holiday Price *">
                    <label></label>
                    <span class="help-block">Holiday Price *</span> </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group form-md-line-input">
                    <input value="<?php echo  $services->s_price_special ?>"  type="type" autocomplete="off" class="form-control" id="form_control_1" name="s_price_special" required="required" onkeypress="return onlyNos(event, this);" placeholder="Special Price *">
                    <label></label>
                    <span class="help-block">Special Price *</span> </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group form-md-line-input">
                    <select id="tax" class="form-control bs-select" onchange="return hideShow();" name="s_tax_applied" >
                      <option>Tax Applied?</option>
                      <option <?php if($services->s_tax_applied=="y"){echo "selected";} ?> value="y">Yes</option>
                      <option <?php if($services->s_tax_applied=="n"){echo "selected";} ?>  value="n">No</option>
                    </select>
                    <label></label>
                    <span class="help-block">Tax Applied? *</span> </div>
                </div>
                <div class="col-md-4" id="s_tax" style="display: none;">
                  <div class="form-group form-md-line-input">
                    <input value="<?php echo $services->s_tax?>" type="text" class="form-control" name="s_tax" />
                    <label></label>
                    <span class="help-block"> Tax %</span></div>
                </div>
                <div class="col-md-4">
                  <div class="form-group form-md-line-input">
                    <select class="form-control bs-select" id="discount" onchange="return hideShow2();" name="s_discount_applied">
                      <option>Discount Applied?</option>
                      <option <?php if($services->s_discount_applied=="y"){echo "selected";} ?>  value="y">Yes</option>
                      <option <?php if($services->s_discount_applied=="n"){echo "selected";} ?>  value="n">No</option>
                    </select>
                    <label></label>
                    <span class="help-block">Discount Applied?</span> </div>
                </div>
                <div class="col-md-4" id="service_discount" style="display: none;">
                  <div class="form-group form-md-line-input">
                    <input value="<?php echo $services->s_discount ?>" type="text" class="form-control" name="s_discount" />
                    <input type="hidden" name="services_id" value="<?php echo $services->s_id ?>" >
                    <label class="control-label col-md-4"></label>
                    <span class="help-block">Discount %</span></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="form-actions right"><a href="javascript:;" class="btn default button-previous"> <i class="m-icon-swapleft"></i> Back </a> <a href="javascript:;" class="btn blue button-next" onclick="check();"> Continue <i class="m-icon-swapright m-icon-white"></i> </a> <a href="javascript:;" class="btn green button-submit" onclick="submit_form()"> Submit <i class="m-icon-swapright m-icon-white"></i> </a> </div>
    </form>
  </div>
</div>
<script type="text/javascript">
                    function save_rule(){
                        //alert($("#submit_form").serialize());

                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>dashboard/add_rule",
                            data: $("#submit_form").serialize(),
                            success: function(msg) {
                                var s_rules=document.getElementById("s_rules").value;
                                if(s_rules!='') {
                                    document.getElementById("s_rules").value = document.getElementById("s_rules").value + "," + msg;
                                }
                                else{
                                    document.getElementById("s_rules").value = msg;
                                }
                               // alert( 'Rule Saved Successfully');
							   swal({
                                    title: "Rule Saved Successfully",
                                    text: "Your Rule Saved Successfully ",
                                    type: "success"
                                },
                                function(){
                                    document.getElementById("save_rule_btn").disabled = true;
                                    document.getElementById("another_rule_btn").style.display = 'block';
                                });
                               // document.getElementById("save_rule_btn").disabled = true;
                                //document.getElementById("another_rule_btn").style.display = 'block';
                            }
                        });
                    }
                    function add_row()
                    {
                        document.getElementById("rule_entry").style.display = 'none';
                        $('#tab2').load('<?php echo base_url() ?>assets/html_static/rule_entry.html');

                    }
                    function hideShow()
                    {
                        if($('#tax').val() == 'y')
                        {
                            $("#s_tax").show();

                        }
                        else
                        {
                            $("#s_tax").hide();

                        }
                    }
                    function hideShow2()
                    {
                        if($('#discount').val() == 'y')
                        {
                            $("#service_discount").show();

                        }
                        else
                        {
                            $("#service_discount").hide();

                        }
                    }
					
					 function open_gn()
						{
							
							document.getElementById('gn').style.display= 'block';
							document.getElementById('guest').style.display= 'none';
							document.getElementById('booking').style.display= 'none';
							document.getElementById('unit').style.display= 'none';
							document.getElementById('event').style.display= 'none';
							
						}
						function open_guest()
						{
							
							document.getElementById('guest').style.display= 'block';
							document.getElementById('gn').style.display= 'none';
							document.getElementById('booking').style.display= 'none';
							document.getElementById('unit').style.display= 'none';
							document.getElementById('event').style.display= 'none';
							
						}
						function open_book()
						{
							
							document.getElementById('booking').style.display= 'block';
							document.getElementById('gn').style.display= 'none';
							document.getElementById('guest').style.display= 'none';
							document.getElementById('unit').style.display= 'none';
							document.getElementById('event').style.display= 'none';
							
						}
						function open_unit()
						{
							document.getElementById('unit').style.display= 'block';
							document.getElementById('booking').style.display= 'none';
							document.getElementById('gn').style.display= 'none';
							document.getElementById('guest').style.display= 'none';
							document.getElementById('event').style.display= 'none';
							
						}
						function open_event()
						{
							document.getElementById('event').style.display= 'block';
							document.getElementById('unit').style.display= 'none';
							document.getElementById('booking').style.display= 'none';
							document.getElementById('gn').style.display= 'none';
							document.getElementById('guest').style.display= 'none';
							
						}
						function open_oparetor()
						{
							document.getElementById('oparetor').style.display= 'block';
							
							
						}
						function open_value()
						{
							document.getElementById('value_one').style.display= 'block';
							
							
						}
						function open_tgl_btn()
						{
							document.getElementById('tgl_btn').style.display= 'block';
							
							
						}
						 
						 
                </script> 
<script>
					function master_catagory(val_mc)
					{
						
						val_mc = val_mc.toString();
						
					
						
						if(val_mc == "Genaral Category"){
							document.getElementById('gn').style.display = "block";
							document.getElementById('guest').style.display = "none";
							document.getElementById('booking').style.display = "none";
							document.getElementById('unit').style.display = "none";
							document.getElementById('event').style.display = "none";
						}
						
						else if(val_mc == "Guest"){
							document.getElementById('guest').style.display = "block";
							document.getElementById('gn').style.display = "none";
							document.getElementById('booking').style.display = "none";
							document.getElementById('unit').style.display = "none";
							document.getElementById('event').style.display = "none";
						}
						
						else if(val_mc == "Booking"){
							document.getElementById('booking').style.display = "block";
							document.getElementById('gn').style.display = "none";
							document.getElementById('guest').style.display = "none";
							document.getElementById('unit').style.display = "none";
							document.getElementById('event').style.display = "none";
						}
						else if(val_mc == "Unit"){
							document.getElementById('unit').style.display = "block";
							document.getElementById('booking').style.display = "none";
							document.getElementById('gn').style.display = "none";
							document.getElementById('guest').style.display = "none";
							document.getElementById('event').style.display = "none";
						}
						else{
							document.getElementById('event').style.display = "block";
							document.getElementById('unit').style.display = "none";
							document.getElementById('booking').style.display = "none";
							document.getElementById('gn').style.display = "none";
							document.getElementById('guest').style.display = "none";
						}
						
						/*
						$('#add_another').on('click', function(){
						   $('#add_service_step2').append( $('#add_service_step2') ); // append -> object
						});
						*/						
					}

                   function reset_div(){
                       var x = document.getElementById("operator_cat").selectedIndex;
                       var y = document.getElementById("operator_cat").options;
                      //y[x]=0;

                       document.getElementById("operator_cat").selectedIndex = -1;
                       document.getElementById("unit_cat").selectedIndex = -1;
                       document.getElementById("booking_cat").selectedIndex = -1;
                       document.getElementById("guest_cat").selectedIndex = -1;
                       document.getElementById("gen_cat").selectedIndex = -1;
                   }
					
					
				</script> 
