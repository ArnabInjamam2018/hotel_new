<div style="border: 1px solid #F24B4B; border-top: 0;" class="portlet box blue">
  <div class="portlet-title" style="background-color:#F24B4B;">
    <div class="caption"> <i class="fa-spin fa fa-cog" aria-hidden="true" syle="font-size:20px;"></i> <span class="caption-subject bold uppercase"> Settings</span> <span class="pull-right" style="font-size:12px;"> &nbsp please be careful & sure</span></div>
  </div>
  <div class="portlet-body form">
    <div class="form-body">
	  <!-- Row#1 -->
      <div class="row" style="padding-bottom:20px;">
	  
      	<div class="col-md-3">
        	<div class="link-block">
                <h4>Hotel Master</h4>
                <div class="link-blockinn">
                    <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="You Can maintain all details of your Hotel" data-original-title="View Hotel" href="<?php echo base_url();?>setting/all_hotel_m"> <i class="fa fa-eye"></i> View Hotels </a></br>
                    <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="You Can maintain all details of your Hotel" data-original-title="View Hotel" href="<?php echo base_url();?>setting/unit_type"> <i class="fa fa-eye"></i> Unit Type </a></br>
                    <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="You Can maintain all details of your Hotel" data-original-title="View Hotel" href="<?php echo base_url();?>setting/all_unit_class"> <i class="fa fa-eye"></i> Unit class </a></br>
                    <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="You Can maintain all details of your Hotel" data-original-title="View Hotel" href="<?php echo base_url();?>setting/all_unit_type_category"> <i class="fa fa-eye"></i> Unit Category </a>
                </div>
            </div>
        </div>

        <div class="col-md-3">
        	<div class="link-block">
                <h4>General Settings</h4>
                <div class="link-blockinn">
                	<a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Set your invoice, As you want to show" data-original-title="Invoice Setting" " href="<?php echo base_url();?>setting/invoice_settings"> <i class="fa fa-toggle-on" aria-hidden="true"></i> Invoice Settings </a>
					
					</br>
					<a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Set your invoice, As you want to show" data-original-title="Invoice Setting" " href="<?php echo base_url();?>setting/auto_invoice_settings"> <i class="fa fa-toggle-on" aria-hidden="true"></i> Auto Invoice Generate </a>
					</br>
                	<a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Set your invoice, As you want to show" data-original-title="POS Setting" " href="<?php echo base_url();?>setting/hotel_to_yummpos_setting"> <i class="fa fa-toggle-on" aria-hidden="true"></i> POS Settings </a>	
                	
                </div>
            </div>
        </div>
        
        <div class="col-md-3">
        	<div class="link-block">
                <h4>Rate Plan Setup</h4>
                <div class="link-blockinn">
                	<a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Customize rate for every single room category" data-original-title="Rate plan Master" " href="<?php echo base_url();?>rate_plan/rate_plan_settings"> <i class="fa fa-sliders" aria-hidden="true"></i> Rate plan Master </a>
                	<a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Setting of Rate Plan" data-original-title="Rate Plan Settings" " href="<?php echo base_url();?>rate_plan/rate_plan_setting_view"> <i class="fa fa-toggle-on" aria-hidden="true"></i> Rate Plan Settings </a>
                    <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Details of Rate Plan" data-original-title="Rate plan Details" " href="<?php echo base_url();?>rate_plan/all_rate_plan"> <i class="fa fa-sliders" aria-hidden="true"></i> Rate plan Details </a> 
                </div>
            </div>
        </div>
        
        <div class="col-md-3">
        	<div class="link-block">
                <h4>Tax Plan Setup</h4>
                <div class="link-blockinn">
                	<a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Hotel tax Type" data-original-title="Type" " href="<?php echo base_url();?>setting/tax_type_settings"> <i class="fa fa-sliders" aria-hidden="true"></i> Tax Type </a></br>
         			<a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Tax Category Details" data-original-title="Category" " href="<?php echo base_url();?>setting/all_tax_category"> <i class="fa fa-sliders" aria-hidden="true"></i> Tax Category  </a></br>
         			<a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Set all kind Tax" data-original-title="Tax Rule" " href="<?php echo base_url();?>setting/all_tax_rule"> <i class="fa fa-sliders" aria-hidden="true"></i> Tax Rule </a>  
                </div>
            </div>
        </div>
	  </div>
    
	  <!-- Row#2 -->
	  <div class="row" style="padding-bottom:20px;">
	  
	    <div class="col-md-3">
        	<div class="link-block">
                <h4>System Settings</h4>
                <div class="link-blockinn">
                	<a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="purpose of Visit" data-original-title="Nature of Visit" " href="<?php echo base_url();?>setting/booking_nature_visit"> <i class="fa fa-briefcase" aria-hidden="true"></i> Nature of Visit </a></br>
					
                     <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="From where get booking" data-original-title="Source" " href="<?php echo base_url();?>setting/hotel_booking_source"> <i class="fa fa-soundcloud" aria-hidden="true"></i> Booking Source </a></br>
					 
                     <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Profit Center" data-original-title="Profit Center" " href="<?php echo base_url();?>setting/all_profit_center" class="nav-link nav-toggle"> <i class="fa fa-first-order" aria-hidden="true"></i>  Profit Center <span class="arrow "> </a></br>
											
                     <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Area Code" data-original-title="Hotel Area Code" " href="<?php echo base_url();?>setting/hotel_area_code"> <i class="fa fa-map-o" aria-hidden="true"></i> Hotel Area Code </a></br>
					 
                     <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Status of all booking" data-original-title="Booking Status" " href="<?php echo base_url();?>setting/booking_status_type"> <i class="fa fa-tags" aria-hidden="true"></i> Booking Status </a></br>
					 
                     <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Housekeeeping Status of Hotel" data-original-title="Housekeeeping Status" " href="<?php echo base_url();?>setting/housekeeping_status"> <i class="fa fa-tags" aria-hidden="true"></i> Housekeeeping Status </a>
					 
                     <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Who take the booking" data-original-title="Marketing Personnel" " href="<?php echo base_url();?>setting/marketing_personnel"> <i class="glyphicon glyphicon-user" aria-hidden="true"></i> Marketing Personnel </a>
					 
                     <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Extra Charge Details Setting" data-original-title="Extra Charges" " href="<?php echo base_url();?>setting/all_charges"> <i class="fa fa-th-list" aria-hidden="true"></i> Extra Charges </a> 
                </div>
            </div>
        </div>        
			

		<div class="col-md-3">		
        	<div class="link-block">
                <h4>Cash Drawer</h4>
                <div class="link-blockinn">
                    <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="You check all kind of Cash Report" data-original-title="All Cashdrawer Log" " href="<?php echo base_url();?>cashdrawer/all_cashdrawer_log"> <i class="fa fa-eye"></i> All Cashdrawer Log  </a>
                </div>
				<div class="link-blockinn">
                    <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Setting of Cashdrawer" data-original-title="Cashdrawer Setting" " href="<?php echo base_url();?>cashdrawer/all_cashdrawer"> <i class="fa fa-eye"></i> All Cashdrawer </a>
                </div>
            </div>
        </div>
		
		<div class="col-md-3">
        	<div class="link-block">
                <h4>Night Audit</h4>
                <div class="link-blockinn">
                    <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Hello world" data-original-title="Night Audit"  onclick="nightAudit(<?php //echo $this->session->userdata('user_hotel'); ?>)"> <i class="fa fa-eye"></i>Night Audit  </a>
                </div>
				
				<div class="link-blockinn">
                    <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Night Audit report Setting" data-original-title="Audit Setting" " href="<?php echo base_url();?>night_Audit_controller/night_audit_settings"> <i class="fa fa-eye"></i>Night Audit Settings  </a>
                </div>
				<div class="link-blockinn">
                    <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Night Audit List Report" data-original-title="Night Audit List" " href="<?php echo base_url();?>night_Audit_controller/all_night_audit"> <i class="fa fa-eye"></i>Night Audit List  </a>
                </div>
				
            </div>
        </div>
      
	    <div class="col-md-3">
        	<div class="link-block">
                <h4>Misc Setting</h4>
                <div class="link-blockinn">
                    <a class="btn btn-xs popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="Warehouse Setting" data-original-title="Warehouse" " href="<?php echo base_url();?>setting/warehouse_default"> <i class="fa fa-eye"></i> warehouse  </a>
                </div>
            </div>
        </div>
		
	  
	  </div>
  </div>
  </div>
  </div>
  <!-- END CONTENT --> 
<script>
function nightAudit(id){
	
	//alert(id);
	
	swal({
  title: "NOTE!",
  text: "Write a Note:",
  type: "input",
  showCancelButton: true,
  closeOnConfirm: false,
  animation: "slide-from-top",
  inputPlaceholder: "Write something"
},


function(inputValue){
  
  if (inputValue) {
	jQuery.ajax(
                        {
                            type: "POST",
                            url: "<?php echo base_url(); ?>unit_class_controller/nightAudit",
                            dataType: 'json',
                            data: {id:id,note:inputValue},
                            success: function(data){
									//alert(data)
								if(data.status){
								swal("Night Audit!", "Processing Night Audit!", "success");
								}
							}
							});
  }
							
							if (inputValue === "") {
    swal.showInputError("You need to write something!");
    return false
  }
							
 
});

	
}
</script>