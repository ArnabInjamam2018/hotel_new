<script src="<?php echo base_url();?>assets/global/plugins/daypilot/js/daypilot/daypilot-all.min.js" type="text/javascript"></script>
<div id="home">
  <div class="row">
    <div class="col-md-12">
      <div id="booking_calendar">
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-green"> <i class="icon-pin font-green"></i> <span class="caption-subject bold uppercase"> Reservation Management</span> </div>
          </div>
          <div class="portlet-body form">
            <form role="form">
              <div class="form-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="clearfix" style="margin-bottom:35px;">
                      <div class="btn-group" data-toggle="buttons">
                        <input type="hidden" id="hidden_field1" value="">
                        <label class="btn default green-stripe">
                          <input id="5" type="checkbox" onchange="calendar()" class="toggle">
                          Detailed View </label>
                        </a>
                        <label class="btn default blue-stripe" style="margin-left:10px;">
                          <input id="7" type="checkbox" onchange="calendar2()" class="toggle">
                          Detailed View 2 </label>
                        <label class="btn yellow dark-stripe" style="margin-left:10px;">
                          <input id="6" type="checkbox" onchange="snapshot()" class="toggle">
                          Snapshot View </label>
                      </div>
                    </div>
                  </div>
                  <script type="text/javascript">
					   function calendar(){
						 window.location="<?php echo base_url(); ?>dashboard/add_booking_calendar";
					   }
					   function calendar2(){
						 window.location="<?php echo base_url(); ?>dashboard/calendar_load_2";
					   }
					   function snapshot(){
						 window.location="<?php echo base_url(); ?>dashboard/snapshot_load";
					   }
				   </script>
                  <div class="col-md-12">
                    <div class="form-inline" style="margin-bottom:35px;">
                      <div class="form-group form-md-line-input">
                        <input id="filter_room_no" type="text" class="form-control" placeholder="Search Room">
                        </input>
                        <label></label>
                        <span class="help-block">Search Room</span> </div>
                      <div class="btn-group pull-right">
                        <label for="autocellwidth" class="auto_cl btn grey-cascade" id="auto_cl_id">
                          <input type="checkbox" id="autocellwidth" class="toggle" style="display:none;">
                          <span id="disp">Expanded View</span></label>
                      </div>
                    </div>
                  </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function() {
                        //dp.cellWidth = 60;
                        //dp.cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                        //dp.update();
                        //dp.cell.enabled = false;
                        /*if (start.getTime() < today.getTime())
                         { dp.Cell.Enabled = false; e.Day.IsSelectable = false; }*/
                        //alert(start.getTime());
                    });
                    var picker = new DayPilot.DatePicker({
                        target: 'start',
                        pattern: 'M/d/yyyy',
                        date: new DayPilot.Date().firstDayOfMonth(),
                        onTimeRangeSelected: function(args) {
                            //dp.startDate = args.start;
                            loadTimeline(args.start);
                            loadEvents();
                        }
                    });
					
                    $("#timerange").change(function() {
                        switch (this.value) {
                            case "week":
                                dp.days = 7;
                                break;
                            case "2weeks":
                                dp.days = 14;
                                break;
                            case "month":
                                dp.days = dp.startDate.daysInMonth();
                                break;
                            case "2months":
                                dp.days = dp.startDate.daysInMonth() + dp.startDate.addMonths(1).daysInMonth();
                                break;
                        }
                        loadTimeline(DayPilot.Date.today());
                        loadEvents();
                    });

                    $("#autocellwidth").click(function() {
                        dp.cellWidth = 100;  // reset for "Fixed" mode
                        dp.cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
                        document.getElementById('auto_cl_id').style.backgroundColor = $(this).is(":checked") ? "#00CC99" : "#A5A5A5";
                        var a = $(this).is(":checked") ? "Compact View" : "Expanded View";
					
						$('#disp').text(a);
						dp.update();

                    });
                </script>
                <div style="position:relative;">
                  <div class="bok">Booking</div>
                  <div id="dp"> </div>
                </div>
                <script>            
					var dp = new DayPilot.Scheduler("dp");
					dp.rowMaxHeight = 10000;
                	dp.allowEventOverlap = false;
                	dp.borderColor = "red";
					//dp.scale = "Day";
					//dp.startDate = new DayPilot.Date().firstDayOfMonth();
					dp.days = dp.startDate.daysInMonth();
					//loadTimeline(DayPilot.Date.today(-3));
					loadTimeline(DayPilot.Date.today().addDays(-3));

					dp.eventDeleteHandling = "Update";
					dp.onBeforeCellRender = function(args) {
                  
						if (args.cell.start < DayPilot.Date.today())
						{
							args.cell.backColor = "#F9F9F9";
						   //args.cell.backColor = "black";
						}
						else
						{
							var dayOfWeek = args.cell.start.getDayOfWeek();
							if (dayOfWeek === 6 || dayOfWeek === 0) {
								args.cell.backColor = "#f2ffee";
							}
						}
	
						if (args.cell.start <= DayPilot.Date.today() && DayPilot.Date.today() < args.cell.end)
						{
							args.cell.backColor = "#DFF4F2";
						   // alert(args.cell.start+"   "+args.cell.end);
							//args.cell.backColor = "black";
						}
						if (args.cell.resource === "unit") {
	
							args.cell.backColor = "#EEEEEE";
						   // args.cell.html += " (loaded dynamically)";
							//args.cell.left.enabled = false;
							//args.cell.right.enabled = false;
						   // args.right.html = "You can't create an event here";
	
							args.cell.allowed = false;
	
						}
					};
					dp.onBeforeTimeHeaderRender = function(args) {
						var d = new Date();
						var dayOfWeek = args.header.start.getDayOfWeek();
						var dayOfMonth = args.header.start.getDayOfWeek();
						//if (args.header.start == DayPilot.Date.today()) {
						if(args.header.start <= DayPilot.Date.today() && DayPilot.Date.today() < args.header.end){
							args.header.fontColor  = "#ffffff";
							args.header.backColor  = "#33D0E1";
							//args.header.fontSize = "35pt";
	
						}
						else if (dayOfWeek === 6 || dayOfWeek === 0) {
	
							args.header.backColor  = "#E2EFDA";
						}
						else{
							args.header.backColor  = "#EDEDED";
						}
	
					};

					dp.timeHeaders = [
						{ groupBy: "Month", format: "MMMM yyyy" },
						{ groupBy: "Day", format: "d" },
						{ groupBy: "Day", format: "ddd" },	
					];

					dp.eventHeight = 50;
					dp.bubble = new DayPilot.Bubble({});

					dp.rowHeaderColumns = [
						{title: "Room", width: 100}
						//{title: "Capacity", width: 80},
						//{title: "Status", width: 80}
					];

					dp.onBeforeResHeaderRender = function(args) {
						var beds = function(count) {
							return "";//count + " bed" + (count > 1 ? "s" : "");
						};

					};

					// http://api.daypilot.org/daypilot-scheduler-oneventmoved/
                dp.onEventMoved = function (args) {
                    var modal = new DayPilot.Modal();
                    modal.closed = function() {
                        dp.clearSelection();
                        // reload all events
                        var data = this.result;
                        //if (data && data.result === "OK") {
                            loadEvents();
                        //}
                    };					
                    if ( args.newEnd < DayPilot.Date.today() ) {
                        swal({
                                title: "Past Date",
                                text: "Booking Events can not be modified in past dates",
                                type: "error",
								confirmButtonColor: "#F27474"
                            },
                            function(){
                                //$( "#booking_calendar" ).load( "<?php echo base_url() ?>dashboard/calendar_load" );
								loadEvents();
                            });
                        return false;

                    } else {
                        modal.showUrl("<?php echo base_url();?>bookings/hotel_booking_move?id="+args.e.id()+"&newStart="+args.newStart.toString()+"&newEnd="+args.newEnd.toString()+"&newResource="+args.newResource+"");
						return false;
                    }
                    /*$.post("<?php echo base_url();?>bookings/booking_backend_move",
                        {
                            id: args.e.id(),
                            newStart: args.newStart.toString(),
                            newEnd: args.newEnd.toString(),
                            newResource: args.newResource
                        },
                        function(data) {
                           // dp.message(data.message);
                           location.reload();
                        });*/
                };

                // http://api.daypilot.org/daypilot-scheduler-oneventresized/
                dp.onEventResized = function (args) {
                    var modal = new DayPilot.Modal();
                    modal.closed = function() {
                        dp.clearSelection();

                        // reload all events
                        var data = this.result;
                        //if (data && data.result === "OK") {
                            loadEvents();
                        //}
                    };
                    if ( !1 ) {
                        swal({
                                title: "Past Date",
                                text: "Booking Events can not be modified in past dates",
                                type: "error",
								confirmButtonColor: "#F27474"								
                            },
                            function(){
                                $( "#booking_calendar" ).load( "<?php echo base_url() ?>dashboard/calendar_load" );
								
                            });
                        return false;
                    }
                    else
                    {
                        modal.showUrl("<?php echo base_url();?>bookings/hotel_booking_resize?id="+args.e.id()+"&newStart="+args.newStart.toString()+"&newEnd="+args.newEnd.toString()+"");
                    }
                };

                dp.onEventDeleted = function(args) {
                    $.post("<?php echo base_url();?>bookings/booking_delete",
                        {
                            id: args.e.id()
                        },
                        function() {
                            dp.message("Deleted.");
                        });
                };
                dp.onTimeRangeSelected = function (args) {
                    //var name = prompt("New event name:", "Event");
                    //if (!name) return;
                    var modal = new DayPilot.Modal();
                    modal.closed = function() {
                        dp.clearSelection();
                        // reload all events
                        var data = this.result;
                        if (data && data.result === "OK") {
                            loadEvents();
                        }
                    };
                  
                    if ( args.start >= DayPilot.Date.today() && DayPilot.Date.today() < args.end    )
                    {   //alert(args.resource);
				        if(args.resource == 'unit'){
							swal({
								title: "Click on an empty cell to take a booking.",
								text: "You have clicked on an unit type divider.",
								type: "info",
								showCancelButton: true,
								confirmButtonColor: "#33D0E1",
								confirmButtonText: "Take Group Booking!",
								closeOnConfirm: true
							}, function () {
								window.location = "<?php echo base_url()?>dashboard/add_group_booking";
							});	
							dp.clearSelection();	
						} else {
							$.ajax({
								type:"GET",
								url: "<?php echo base_url()?>bookings/clean_check?resource="+args.resource,
								data:{resource_id:'asd'},
								success:function(data)
								{
									if(data.say=="no") {
										if(args.start == DayPilot.Date.today() && DayPilot.Date.today() < args.end ){
											swal({
												title: "This unit is not clean!",
												text: "Do you want to take a booking?",
												type: "info",
												showCancelButton: true,
												confirmButtonColor: "#00CC66",
												confirmButtonText: "Take Booking!",
												closeOnConfirm: true
											}, function () {
												modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
											});
											
										} else {
											modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
										}
										  dp.clearSelection();
									}else if(data.say=="yes") {

										modal.showUrl("<?php echo base_url();?>bookings/hotel_new_booking?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
									}
								}
							});
                        }
					}
                    else
                    {
                        swal({
                                title: "Past Date!",
                                text: "Bookings Can't be taken in past dates",
                                type: "error",
								confirmButtonColor: "#F27474"
                            },
                            function(){
								//alert("HERE");
                                //$( "#booking_calendar" ).load( "<?php echo base_url() ?>dashboard/calendar_load" );
								dp.clearSelection();
                            });
                        return false;
                    }
                };

                dp.onEventClick = function(args) {
                    var modal = new DayPilot.Modal();
					modal.showUrl("<?php echo base_url();?>bookings/hotel_edit_booking?id=" + args.e.id());
                    modal.closed = function(data) {
						//alert("HERE");
                        // reload all events
                        //var data = this.result;
						//alert(JSON.stringify(data));
                        if (this.result == "OK") {
                            loadEvents();
							//dp.events.update(args.e);
							
                        }
						loadEvents();
						//dp.clearSelection();
						//dp.events.update(args.e.id);
                    };
                };				
              
                dp.onRowClick=function(args){
                    //alert(args.resource.id);
                    var modal = new DayPilot.Modal();
                    modal.closed = function() {
                        // reload all events
                        var data = this.result;
                        if (data && data.result === "OK") {
                            loadEvents();
                        }
                    };
                    modal.showUrl("<?php echo base_url();?>bookings/pop_new_room?id=" + args.resource.id);
                }

					dp.onBeforeEventRender = function(args) {
						var start = new DayPilot.Date(args.e.start);
						var end = new DayPilot.Date(args.e.end);

						var today = new DayPilot.Date().getDatePart();

						//args.e.html = args.e.text + " (" + start.toString("M/d/yyyy") + " - " + end.toString("M/d/yyyy") + ")";
						//args.e.html = args.e.cust_name;
						//args.e.html = args.e.html + "<br /><span style=''>" + args.e.cust_name + "</span>";
						switch (args.e.status) {
							case "7":
								/*var in2days = today.addDays(1);

								 if (start.getTime() < in2days.getTime()) {
								 args.e.barColor = args.e.bar_color_code;
								 args.e.toolTip = args.e.booking_status;
								 args.e.backColor = args.e.body_color_code;
								 }
								 else {*/
								args.e.barColor = args.e.body_color_code;
								args.e.toolTip = args.e.booking_status;
								args.e.backColor = args.e.bar_color_code;
								// }
								break;
							case "1":
								/*var in2days = today.addDays(1);

								 if (start.getTime() < in2days.getTime()) {
								 args.e.barColor = args.e.bar_color_code;
								 args.e.toolTip = args.e.booking_status;
								 args.e.backColor = args.e.body_color_code;
								 }
								 else {*/
								args.e.barColor = args.e.body_color_code;
								if(args.e.booking_status_secondary!='') {
									args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
								}
								else {
									args.e.toolTip = args.e.booking_status;
								}
								args.e.backColor = args.e.bar_color_code;
								// }
								break;
							case "2":
								/*var in2days = today.addDays(1);

								 if (start.getTime() < in2days.getTime()) {
								 args.e.barColor = args.e.bar_color_code;
								 args.e.toolTip = args.e.booking_status;
								 args.e.backColor = args.e.body_color_code;
								 }
								 else {*/
								args.e.barColor = args.e.body_color_code;
								if(args.e.booking_status_secondary!='') {
									args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
								}
								else {
									args.e.toolTip = args.e.booking_status;
								}
								args.e.backColor = args.e.bar_color_code;
								// }
								break;
							case "3":

								args.e.barColor = args.e.body_color_code;
								args.e.toolTip = args.e.booking_status;
								args.e.backColor = args.e.bar_color_code;

								break;
							case "5":
								args.e.barColor = args.e.body_color_code;
								if(args.e.booking_status_secondary!='') {
									args.e.toolTip = args.e.booking_status + " (" + args.e.booking_status_secondary + ")";
								}
								else {
									args.e.toolTip = args.e.booking_status;
								}
								args.e.backColor = args.e.bar_color_code;

								break;
							case "8":
								args.e.barColor = args.e.body_color_code;
								args.e.toolTip = args.e.booking_status;
								args.e.backColor = args.e.bar_color_code;

								break;
							case "4":
								/* var arrivalDeadline = today.addHours(18);

								 if (start.getTime() < today.getTime() || (start.getTime() === today.getTime() && now.getTime() > arrivalDeadline.getTime())) { // must arrive before 6 pm
								 args.e.barColor = args.e.bar_color_code;
								 args.e.toolTip = args.e.booking_status;
								 args.e.backColor = args.e.body_color_code;
								 }
								 else {*/
								args.e.barColor = args.e.body_color_code;
								args.e.toolTip = args.e.booking_status;
								args.e.backColor = args.e.bar_color_code;
								//args.e.children ='dqwd';
								//}
								break;
							/*case 'Arrived': // arrived
							 var checkoutDeadline = today.addHours(10);

							 if (end.getTime() < today.getTime() || (end.getTime() === today.getTime() && now.getTime() > checkoutDeadline.getTime())) { // must checkout before 10 am
							 args.e.barColor = "#f41616";  // red
							 args.e.toolTip = "Late checkout";
							 }
							 else
							 {
							 args.e.barColor = "#1691f4";  // blue
							 args.e.toolTip = "Arrived";
							 }
							 break;*/
							case '6': // checked out
								args.e.barColor = args.e.body_color_code;
								args.e.toolTip = args.e.booking_status;
								args.e.backColor = args.e.bar_color_code;
								break;
							/*default:
							 args.e.toolTip = "Unexpected state";
							 break; */
						}

						args.e.html =""; //args.e.html + "<br /><span style='color:gray'>" + args.e.toolTip + "</span>";

						var paid = args.e.paid;
						var paidColor = args.e.paid_color;

						args.e.areas = [
							//{ bottom: 10, right: 4, html: "<div style='color:" + paidColor + "; font-size: 8pt;'>Paid: " + paid + "</div>", v: "Visible"},
							{ left: 4, bottom: 8, right: 4, height: 2, html: "<div style='background-color:" + paidColor + "; height: 100%; width:" + paid + "'></div>", v: "Visible" }
						];

					};


					dp.init();

					loadResources();
					loadEvents();

					function loadTimeline(date) {
						dp.scale = "Manual";
						dp.timeline = [];
						var start = date.getDatePart().addHours(0);
						for (var i = 0; i < dp.days; i++) {
							dp.timeline.push({start: start.addDays(i), end: start.addDays(i+1)});
						}
						dp.cellWidth = 50;
                        dp.cellWidthSpec = $(this).is(":checked") ? "Auto" : "Fixed";
						dp.update();
					}

					function loadEvents() {
						var start = dp.visibleStart();
						var end = dp.visibleEnd();

						/*$.post("<?php echo base_url();?>bookings/hotel_backend_events",
							{
								start: start.toString(),
								end: end.toString()
							},
							function(data) {
								dp.events.list = data;
								dp.update();
							}
						);*/
								var st= start.toString();
								var ed= end.toString();					
								//console.log(start+' | '+end);
								dp.events.load("<?php echo base_url();?>bookings/hotel_backend_events?st="+st+" & ed="+ed);
						
					}
					function loadResources() {
						$.post("<?php echo base_url();?>bookings/hotel_backend_rooms_snp",
							{ capacity: $("#filter").val() },
							function(data) {
							   // var d=data.substr(0,10);
								dp.resources = data;
								dp.rowHeaderWidthAutoFit = false;
								dp.update();
							});
					}


				</script> 
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
