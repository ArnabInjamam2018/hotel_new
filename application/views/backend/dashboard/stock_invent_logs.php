<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-file-archive-o"></i>Stock & Inventory / Operational Logs </div>
	</div>
	<div class="portlet-body">
		<div class="table-toolbar">
			<div class="row">
				<?php

				$form = array(
					'class' => 'form-inline',
					'id' => 'form_date',
					'method' => 'post'
				);

				echo form_open_multipart( 'dashboard/stock_invent_log', $form );

				?>
				<div class="col-md-10">
					<div class="form-group">
						<input type="text" autocomplete="off" required="required" id="t_dt_frm" value="<?php if(isset($start_date)){ echo $start_date;}?>" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date">
					</div>
					<div class="form-group">
						<input type="text" autocomplete="off" required="required" value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" class="form-control date-picker" placeholder="End Date">
					</div>
					<button class="btn btn-default" onclick="check_sub()" type="submit">Search</button>
				</div>

				<?php form_close(); ?>
			</div>
		</div>
		<ul class="nav nav-tabs">
			<li class="active">
				<a style="font-size:15px;" href="#StockInventory-Logs" data-toggle="tab" aria-expanded="true"> Stock & Inventory Logs </a>
			</li>
			<li class="">
				<a style="font-size:15px;" href="#Operational-Log" data-toggle="tab" aria-expanded="false"> Operational Log </a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="StockInventory-Logs">
				<table class="table table-striped table-bordered table-hover" id="sample_1">
					<thead>
						<tr>
							<th> # </th>
							<th> Asset Image </th>
							<th>Item </th>
							<th>Type & Category </th>
							<!--<th>Asset Category</th>-->
							<!--<th>Make</th>-->
							<!--<th>Description</th>-->
							<th> Item Details </th>
							<!--<th>Qty </th>-->
							<th style="padding: 0 15px 0 0;">
								<table class="table table-hover table-bordered" style="margin:0;">
									<thead>
										<tr>
											<th style="text-align:right" width="64%"> Warehouse Name </th>
											<th align="left" width="36%">Qty</th>
										</tr>
									</thead>
								</table>
							</th>
							<!--<th>Low Stock </th>-->
							<th style="width:20%;">Other Info </th>
						</tr>
					</thead>
					<tbody>


						<?php
						if ( isset( $logs ) && $logs ) {
							$srl_no = 0;
							foreach ( $logs as $log ) {
								$srl_no++;
								?>
						<tr>
							<td>
								<?php echo $srl_no;?>
							</td>

							<td width="5%"><a class="single_2" href="<?php echo base_url();?>upload/asset/<?php if( $log->a_image== '') { echo " no_images.png "; } else { echo $log->a_image; }?>"><img src="<?php echo base_url();?>upload/asset/<?php if( $log->a_image== '') { echo "no_images.png"; } else { echo $log->a_image; }?>" alt="" style="width:100%;"/></a>
							</td>
							<td>
								<?php // Item
                                echo '<strong>'.$log->a_name.'</strong>';
                                if($log->make != '')
                                    echo '<span style="color:#007F7F;"> ('.$log->make.')</span>';
                            ?>
							</td>
							<td>
								<?php // Type & Category
                                echo $log->item_type.' &nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;';
                                echo '<span class="label-flat" style="background-color:#E6E6FA; color:#000000;">'.$log->a_category.'</span>';
                            ?>
							</td>
							<!--<td><?php echo $log->a_category;?></td>-->

							<!--<td><?php echo $log->make;?></td>-->
							<!--<td><?php echo $log->item_des;?></td>-->
							<td>
								<?php // Item Details
                                    if($log->u_price != '')
                                        echo '<strong>Unit price:</strong> INR '.$log->u_price.'<br>';
                                    if($log->qty != ''){
                                      if($log->qty<$log->l_stock)
                                        echo '<Span class="label glow" style="background-color:#E52828; color:#ffffff;";>Low Qty in stock:  </span> '.$log->qty.' '.$log->unit.'<br>';
                                      else
                                        echo '<strong>Qty in stock: </strong>'.$log->qty.' '.$log->unit.'<br>';
                                    }	
                                    if($log->l_stock != '')
                                        echo '<strong>Low stock qty: </strong>'.$log->l_stock.' '.$log->unit;
                                ?>
							</td>
							<!--<td><?php echo $log->qty;?></td>-->
							<td>
								<table class="table table-bordered" style="margin:0;">
									<?php 
                                    $wharehouse=$this->dashboard_model->get_warehouse_details();
                                    if(isset($wharehouse) && $wharehouse){
                                        $stock_id=$this->dashboard_model->get_stock_id($log->a_name);
                                        if($stock_id!=false){
                                        foreach($wharehouse as $wh){
                                            
                                            if($stock_id->hotel_stock_inventory_id == $wh->item_id){
                                                if($wh->qty != 0){
                                ?>

									<tr>
										<td align="right" width="60%" style="padding:0 8px">
											<?php // Warehouse name
                                        
                                        //echo $wh->warehouse_id;
                                        $wharehouse=$this->dashboard_model->get_warehouse( $wh->warehouse_id);
                                        if(isset($wharehouse) && $wharehouse){
                                            echo '<span style="color:#000C49">'.$wharehouse->name.': </span>';
                                        }
                                        ?>
										</td>
										<td align="left" width="40%" style="padding:0 8px">
											<?php 
                                            echo $wh->qty.' <span style="color:#AAAAAA;">'.$log->unit.'</span>';
                                        ?>
										</td>

									</tr>

									<?php
									}
									}
									}
									}
									}
									//print_r($wharehouse);
									?>
								</table>
								
							</td>
							<!--<td><?php echo $log->l_stock;?></td>-->
							<td>
								<?php // Item Details
                                echo "<strong>Importance: </strong>";
                                    if(isset($log->importance)){
                                        
                                            if($log->importance==1) {
                                                echo '<span class="label-flat" style="background-color:#3E3274; color:#ffffff;">High</span>';
                                                }
                                            else if($log->importance==2){
                                                echo '<span class="label-flat" style="background-color:#F6990A; color:#ffffff;">Medium</span>';
                                            }
                                            else if($log->importance==3){
                                                echo '<span class="label-flat" style="background-color:#52C6C9; color:#ffffff;">Low</span>';
                                            }
                                        
                                    }
                                    else
                                        echo '<span class="label-flat" style="background-color:#D2BDB3; color:#ffffff;">No Importance</span>';
                                    echo '<br>';
                                        
                                    
                                    if($log->note != '')
                                        echo "<strong>Note: </strong>".$log->note;
                                    if($log->item_des != '')
                                        echo "<br><strong>Desc:</strong> ".$log->item_des;
                                ?>
							</td>
						</tr>
						<?php }}?>

					</tbody>
				</table>
			</div>
			<div class="tab-pane" id="Operational-Log">
				<table class="table table-striped table-bordered table-hover" id="sample_2">
					<thead>
						<tr>
							<th> # </th>
							<th width="8%"> Date </th>
							<th>Item Name </th>
							<th>Type & Category </th>
							<th>Opertaion</th>
							<th>Warehouse </th>
							<th>Qty </th>
							<th>Updated Stock</th>
							<th width="18%">Other Details</th>

						</tr>
					</thead>
					<tbody>
						<?php
						if ( isset( $new_log ) && $new_log ) {
							$srl_no1 = 0;
							
							//exit;
							foreach ( $new_log as $new_logs ) {
								$srl_no1++;



								?>
						<tr>
							<td>
								<?php echo $srl_no1;?>
							</td>
							<td width="10%">
								<?php // Date
                                    echo date("g:ia \-\n l jS F Y",strtotime($new_logs->date))
                                ?>
							</td>
							<td>
								<?php // Item Name
                                    if(isset($new_logs->item)){
                                    $id=$new_logs->item;
                                    $stock_name=$this->dashboard_model->get_stock_name($id);
                                    if(isset($stock_name)){
                                        echo '<strong>'.$stock_name->a_name.'</strong>';
                                    }
                                    
                                    }
                            ?>
							</td>
							<td>
								<?php // Type & Category
                                    echo $new_logs->a_type.' &nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;';
                                    echo '<span class="label-flat" style="background-color:#E6E6FA; color:#000000;">'.$new_logs->a_category.'</span>';
                                ?>
							</td>


							<td align="left" valign="middle">
								<?php 
                                    if($new_logs->operation=='add') {echo "<span class='label' style='text-transform:uppercase; background-color:#1C9A71;'> ".$new_logs->operation." </span>"; }
                                    else if($new_logs->operation=='sub') 
                                    {
                                        echo "<span class='label label-sm label-danger' style='text-transform:uppercase;'> ".$new_logs->operation." </span>"; 
                                    }
                                    else if($new_logs->operation=='Transfer')
                                    { 
                                        echo "<span class='label' style='text-transform:uppercase; background-color:#8871B5;'> ".$new_logs->operation." </span>"; 
                                    }
                                    else
                                    { 
                                        echo "<span class='label' style='text-transform:uppercase; background-color:#F2C54B;'> ".$new_logs->operation." </span>"; 
                                    }
                                ?>
							</td>
							<td>
								<?php  
                                    if(isset($new_logs->warehouse) && $new_logs->warehouse){
                                        $id1=$new_logs->warehouse;
                                        $wharehouse1=$this->dashboard_model->get_warehouse($id1);
                                     if(isset($wharehouse1) && $wharehouse1 ){
                                        if($new_logs->operation=='add')
                                            echo '<i style="color:#1C9A71;" class="fa fa-plus" aria-hidden="true"></i> ';
                                        else if($new_logs->operation=='sub')
                                            echo '<i style="color:#F3565D;" class="fa fa-minus" aria-hidden="true"></i> ';
                                        else if($new_logs->operation=='Transfer')
                                        { 
                                            echo '<i style="color:#8871B5;" class="fa fa-exchange" aria-hidden="true"></i> '; 
                                        }
                                        else
                                            echo '<i style="color:#F2C54B;" class="fa fa-minus" aria-hidden="true"></i> ';
                                        echo $wharehouse1->name;
                                    }
                                    }
                                ?>
							</td>
							<td>
								<?php 
                                echo $new_logs->qty.' <span style="color:#AAAAAA;">'.$log->unit.'</span>';
                            ?>
							</td>
							<td>
								<?php 
                                echo $new_logs->closing_qty.' <span style="color:#AAAAAA;">'.$log->unit.'</span>';
                            ?>
							</td>
							<td width="15%">
								<?php 
                                $f = 0;
                                if($new_logs->note != ''){
                                    echo '<strong>Note: </Strong>'.$new_logs->note.'<br>';
                                    $f = 1;
                                }
                                if($new_logs->link_purchase){
                                    echo '<strong>Purchase ID: </strong>'.$new_logs->link_purchase.'<br>';
                                    $f = 1;
                                }
                                if($new_logs->vendor){
									$venodr=$this->dashboard_model->get_vendor_name($new_logs->vendor);
									
                                    echo '<strong>Vendor: </strong>'.$venodr->hotel_vendor_name.'<br>';
                                    $f = 1;
                                }
                                if($new_logs->price!='0'){						
                                    echo '<strong>Price: </strong>INR '.number_format($new_logs->price,2);
                                    $f = 1;
                                }							
                                if($f == 0)
                                    echo '<span style="color:#AAAAAA;">No information</span>';
                            ?>
							</td>
						</tr>
						<?php }}?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- END -->