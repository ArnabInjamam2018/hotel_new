<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>Hotel Stock Inventory Category </div>
    <div class="actions">
    	<a class="btn btn-circle green btn-outline btn-sm"  data-toggle="modal" href="#responsive"> <i class="fa fa-plus"></i>Add New  </a>
    </div>
  </div>
  <div class="portlet-body">
    
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr>
          <th scope="col"> Category Name </th>
          <th scope="col"> Date Added </th>
          <th scope="col" width="400"> Unit Description </th>
          <th scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($data) && $data):

                         // $i=1;
                          foreach($data as $gst):
                             // $class = ($i%2==0) ? "active" : "success";
                              //$g_id=$gst->id;
                              ?>
        <tr>
          <td align="left"><?php echo $gst->name; ?></td>
          <td align="left"><?php echo $gst->change_date;?></td>
          <td align="left"><?php echo $gst->description; ?></td>
          <td align="center" class="ba">
          	<div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $gst->id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a> </li>
                <li><a onclick="edit_stock('<?php echo $gst->id; ?>')" data-toggle="modal" class="btn green btn-xs"> <i class="fa fa-edit"></i></a> </li>
              </ul>
            </div>
          </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<div id="responsive" class="modal fade" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Stock Inventory Category</h4>
      </div>
      <?php

	  $form = array(
		  'class' 			=> '',
		  'id'				=> 'form',
		  'method'			=> 'post'
	  );
	
	  echo form_open_multipart('Unit_class_controller/add_Stock_inventory_category',$form);
	
	  ?>
      <div class="modal-body">
        <div class="form-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="name" id="name" placeholder="Category Name " required="required">
                <label></label>
                <span class="help-block">Category Name *</span>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <textarea class="form-control" placeholder="Description " row="3" name="desc" id="desc"></textarea>
                <label></label>
                <span class="help-block">Description *</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
      <?php echo form_close(); ?> </div>
  </div>
</div>
<div id="edit_stock_inventory" class="modal fade" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Stock Inventory Category</h4>
      </div>
      <?php

	  $form = array(
		  'class' 			=> '',
		  'id'				=> 'form',
		  'method'			=> 'post'
	  );
	
	  echo form_open_multipart('Unit_class_controller/edit_stock_inventory_category',$form);
	
	  ?>
      <input type="hidden" name="hid1" id="hid1">
      <div class="modal-body">
        <div class="form-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="name1" id="name1" placeholder="Category Name " required="required">
                <label></label>
                <span class="help-block">Category Name *</span>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <textarea class="form-control" placeholder="Description " row="3" name="desc1" id="desc1"></textarea>
                <label></label>
                <span class="help-block">Description *</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
      <?php echo form_close(); ?> </div>
  </div>
</div>
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>Unit_class_controller/delete_stock_inventory_category?f_id="+id,
                data:{f_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
	
	
	function edit_stock(id){
		//alert(id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/edit_stock",
                data:{id:id},
                success:function(data)
                {
                    //alert(data.id);
					
                   $('#hid1').val(data.id);
				   $('#name1').val(data.name);
				   $('#desc1').val(data.description);
				  // $('#unit_class1').val(data.change_date);
				  
				   
				   
                    $('#edit_stock_inventory').modal('toggle');
					
                }
            });
	}
</script> 
