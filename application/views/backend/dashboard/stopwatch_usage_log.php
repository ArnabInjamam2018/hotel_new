<!-- BEGIN PAGE CONTENT-->
<style>
.ds .required {
	color: #e02222;
	font-size: 12px;
	padding-left: 2px;
}
.ds .form-group.form-md-line-input .form-control ~ label {
	width: 94%;
	left: 19px;
}
.ds .form-group.form-md-line-input {
	margin-bottom: 15px;
}
.ds .lt {
	line-height: 38px;
}
.ds .tld {
	margin-bottom: 10px !important;
	padding-top: 10px;
}
.tld_in {
	background-color: #f8f8f8;
	width: 100%;
	float: left;
	padding-top: 7px;
}
.tld_in:hover {
	background-color: #f1f1f1;
}
.ds .dropzone .dz-default.dz-message {
	width: 100%;
	height: 50px;
	margin-left: 0px;
	margin-top: 0px;
	top: 0;
	left: 0;
	font-size: 50%;
}
.ds .dropzone {
	min-height: 130px;
}
.form-group.form-md-line-input.form-md-floating-label.col-md-6.full {
	width: 100%;
}
.form-group.form-md-line-input.form-md-floating-label.col-md-6.add {
	width: 100%;
}
.form-group.form-md-line-input.form-md-floating-label.col-md-6.rght {
	width: 50%;
	margin-top: -85px;/* float: right; */
}
.form-group.form-md-line-input.form-md-floating-label.col-md-6.dwn {
	width: 51%;
	float: right;
}
.fclbl {
	top: 0 !important;
	font-size: 13px !important;
}


/* Stopwatch Styles */

.button {
  color: #bbb;
  text-decoration: none;
}

.controls{
  margin-top: -270px
}

.button:first-child {
    margin-left: 0;
}

.button:last-child {
    margin-right: 0;
}

.button:hover {
  color: black;
  text-decoration: none;
}
.button:active {
  color: black;
  text-decoration: none;
}

.stopwatch {
  font-size: 2vw;
  height: 40%;
  line-height: 10px;
  text-align: center;
}

.results {
  border-color: lime;
  list-style: none;
  transform: translateX(-50%);
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/dashboard/Colorpicker/farbtastic.css">
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/Colorpicker/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/Colorpicker/farbtastic.js" type="text/javascript"></script>
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
<div class="row ds">
  <div class="col-md-12">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green"> <i class="icon-pin font-green"></i> <span class="caption-subject bold uppercase"> Add stopwatch Usage Log</span> </div>
      </div>
      <div class="portlet-body form">
        <?php

          $form = array(
              'class' 			=> 'form-body',
              'id'				=> 'frm',
              'method'			=> 'post',								
          );
          echo form_open_multipart('dashboard/add_stopwatch_usage_log',$form);

        ?>
        <div class="form-body"> 
          <!-- 17.11.2015-->
          <?php if($this->session->flashdata('err_msg')):?>
          <div class="form-group">
            <div class="col-md-12 control-label">
              <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
            </div>
          </div>
          <?php endif;?>
          <?php if($this->session->flashdata('succ_msg')):?>
          <div class="form-group">
            <div class="col-md-12 control-label">
              <div class="alert alert-success alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
            </div>
          </div>
          <?php endif;?>
          <!-- 17.11.2015-->
          <div class="row">
            <?php if(isset($datas) && $datas){
                                  			
            ?>
            <div class="form-group form-md-line-input col-md-6">
              <select class="form-control"  name="gen_name" required="required" >
                <option value="">---Select Generator---</option>
                <?php foreach($datas as $d){ ?>
                <option value="<?php echo $d->gen_name?>"><?php echo $d->gen_name?></option>
                <?php }}?>
              </select>
              <label>Generator List <span class="required">*</span></label>
              <span class="help-block">Select Generator...</span> 
            </div>

            <div class="form-group form-md-line-input col-md-6">
              <input autocomplete="off" type="text" class="form-control date-picker" value="<?php echo date('d/m/Y'); ?>" id="datepicker" name="startdate">
              <label id="lbl">Start Date<span class="required">*</span></label>
              <span class="help-block">Please select the date in which the machine started ...</span> 
            </div>

            <div class="form-group form-md-line-input col-md-6"> 
              <div class="stopwatch" id="stpwtch"></div>
              <ul class="results"></ul>
              <nav class="controls form-control">
                <a href="#" class="button" onClick="stopwatch.start();">Start</a>
                <!--<a href="#stopwtch" class="button" onClick="stopwatch.lap();">Lap</a>-->
                <a href="#" class="button" onClick="stopwatch.stop();">Pause</a>
                <!--<a href="#" class="button" onClick="stopwatch.restart();">Restart</a>-->
                <a href="#" class="button" onClick="stopwatch.reset_stpwtch();">End</a>
              </nav>
            </div>

            <div class="form-group form-md-line-input col-md-6" >
              <input onblur="start_time()" autocomplete="off" type="text" value="<?php echo date('h:i'); ?>" class="form-control timepicker timepicker-24" id="starttime" name="starttime" >
              <label>Start Time<span class="required">*</span></label>
              <span class="help-block">Please select the stop time in which the machine started ...</span> 
            </div>

            <div class="form-group form-md-line-input form-md-floating-label col-md-6" >
              <input  autocomplete="off" type="text" class="form-control" id="o_fuel" name="o_fuel" required="required" >
              <label> Opening Fuel Reading<span class="required">*</span></label>
            </div>

            <div class="form-group form-md-line-input col-md-6">
              <input autocomplete="off" type="text" class="form-control date-picker" value="<?php echo date('d/m/Y'); ?>" id="stopdatepicker" name="stopdate" >
              <label id="lbl1">Stop Date<span class="required">*</span></label>
              <span class="help-block">Please select the End date in which the machine stopped ...</span>
            </div>

            <div class="form-group form-md-line-input col-md-6" >
              <input autocomplete="off" type="text" class="form-control timepicker timepicker-24" id="stptime" name="stoptime" >
              <label>Stop Time<span class="required">*</span></label>
              <span class="help-block">Please select the stop time in which the machine stopped ...</span> 
            </div>

            <div class="form-group form-md-line-input form-md-floating-label col-md-6"  >
              <input  onblur="diffuel()" autocomplete="off" type="text" class="form-control" id="c_fuel" name="c_fuel" required="required" >
              <label> Closing Fuel Reading<span class="required">*</span></label>
            </div>

            <div class="form-group form-md-line-input col-md-6">
              <input autocomplete="off" type="text" class="form-control timepicker timepicker-24" id="pausefrom" name="pausefrom" >
              <label>Pause From:<span class="required">*</span></label>
              <span class="help-block"></span>
            </div>

            <div class="form-group form-md-line-input col-md-6">
              <input autocomplete="off" type="text" class="form-control timepicker timepicker-24" id="pauseto" name="pauseto" >
              <label>To:<span class="required">*</span></label>
              <span class="help-block"></span> 
            </div>

            <div class="form-group form-md-line-input col-md-6">
              <select class="form-control"  name="admin" required="required" >
                <option value="">---Select Admin---</option>
                <?php if(isset($usertypes) && $usertypes){
                                        			foreach($usertypes as $users){
                                        		?>
                <option value="<?php echo $users->admin_first_name.' '.$users->admin_last_name?>"><?php echo $users->admin_first_name.' '.$users->admin_last_name?></option>
                <?php }}?>
              </select>
              <label>Admin <span class="required">*</span></label>
              <span class="help-block">Select Admin...</span> 
            </div>
            
            <!-- 1st one --> 
            <!-- </div> -->
            
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="totalhours" required="required">
              
              <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
              <label> Total Hours<span class="required">*</span></label>
            </div>

            <div  class="form-group form-md-line-input form-md-floating-label col-md-6">
              <input autocomplete="off" type="text" class="form-control" id="fuel_consumption" name="fuelconsumption" required="required">
              
              <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
              <label> Fuel Consumption<span class="required">*</span></label>
            </div>
            <!-- <div  class="form-group form-md-line-input form-md-floating-label col-md-6">
                                    	<input autocomplete="off" type="text" class="form-control" id="o_fuel" name="o_fuel" required="required">
										
                                        <input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">
                                        <label> Opening Fuel Reading<span class="required">*</span></label>
                                       
                                       
                                    </div> --> 
            
            <!--<div class="form-group form-md-line-input form-md-floating-label col-md-4">
										<input type="text" class="form-control" id="form_control_1" >
										<label>Coming From <span class="required">*</span></label>
										<span class="help-block">Enter Coming From...</span>
									</div>
									<div class="form-group form-md-line-input form-md-floating-label col-md-6">
									
										<input type="text" class="form-control" id="form_control_1" >
										<label>Going To <span class="required">*</span></label>
										<span class="help-block">Enter Going To...</span>
									</div>-->
            
            <div class="form-group form-md-line-input form-md-floating-label col-md-6">
              <input  autocomplete="off" type="text" class="form-control" id="differfuel" name="differfuel">
              
              <!--<input  type="date" class="form-control" id="form_control_1" name="g_dob" required="required">-->
              <label> Difference in Fuel Reading<span class="required">*</span></label>
            </div>
            
            <!--<div class="form-group form-md-line-input form-md-floating-label col-md-4">
									
										<input type="text" class="form-control" id="form_control_1" >
										<label id="form_control_1">Others <span class="required">*</span></label>
										<span class="help-block">Enter Others Option...</span>
									</div>--> 
            <!-- <div class="form-group form-md-line-input form-md-floating-label col-md-6 dwn">
									<p><strong>Upload Photo</strong><span class="required">*</span></p>
			<!--<div action="http://localhost/hotel//assets/dashboard/assets/global/plugins/dropzone/upload.php" class="dropzone" id="my-dropzone">
					</div>--> <!-- 17.11.2015 --> 
            <!--<?php echo form_upload('image_photo');?>--> 
            <!-- 17.11.2015 --> 
          </div>
          
        </div>
        <div class="form-actions right">
          <button type="submit" class="btn blue" >Submit</button>
          <!-- 18.11.2015  -- onclick="return check_mobile();" -->
          <button  type="reset" class="btn default">Reset</button>
        </div>      
      <?php form_close(); ?>
      </div>
      <!-- END CONTENT --> 
    </div>
  </div>
</div>
<script>
   function check_Cylinders(value){
   		
       if(value =="Cylinders"){

           document.getElementById("c_choice").style.display="block";
       }else{
           document.getElementById("c_choice").style.display="none";
       }
      	if(value =="Rotation"){

           document.getElementById("r_choice").style.display="block";
       }else{
           document.getElementById("r_choice").style.display="none";
       }
       if(value =="Design"){

           document.getElementById("d_choice").style.display="block";
       }else{
           document.getElementById("d_choice").style.display="none";
       }
   }
   function diffuel()
   {

    var o=document.getElementById('o_fuel').value;

    var c=document.getElementById('c_fuel').value;
    //alert(c);
    var result=parseFloat(o) - parseFloat(c);
    document.getElementById("fuel_consumption").focus();
    document.getElementById('fuel_consumption').value=result;
    document.getElementById("differfuel").focus();
    document.getElementById('differfuel').value = result;
   }
  
  $(document).on('blur', '#datepicker', function () {
  	$('#lbl').addClass('fclbl');
  });
  $(document).on('blur', '#stopdatepicker', function () {
  	$('#lbl1').addClass('fclbl');
  	
  });


  $(document).on('click', '#frm', function () {
  	//alert('here');
  });



  </script> 

  <script type="text/javascript">
    
    class Stopwatch
    {
      constructor(display, results) {
          this.running = false;
          this.display = display;
          this.results = results;
          this.laps = [];
          this.reset();
          this.print(this.times);
      }
      
      reset_stpwtch(){
        var txt = document.getElementById('stpwtch').innerHTML;
        txt = String(txt);
        txt = txt.trim();
        var txt_minuite = txt.substr(0, 2);
        
        var get_tim =  document.getElementById('starttime').value;
        var get_tim_minuite = get_tim.substr(get_tim.length - 2);

        var get_tim_hour = get_tim.substr(0, 2);

        get_tim_hour = parseInt(get_tim_hour);
        get_tim_minuite = parseInt(get_tim_minuite);
        txt_minuite = parseInt(txt_minuite);

        var nw_minit = txt_minuite + get_tim_minuite;

        if (nw_minit > 60) 
        {
          var nw_hr = get_tim_hour + Math.ceil(nw_minit/60);
          var nw_time = nw_hr +":"+nw_minit;
        }
        else{
          var nw_time = get_tim_hour +":"+nw_minit;
        }

        this.reset();
        this.print(this.times);
      }

      reset() {
          this.times = [ 0, 0, 0];
      }
      
      start() {
          if (!this.time) this.time = performance.now();
          if (!this.running) {
              this.running = true;
              requestAnimationFrame(this.step.bind(this));
          }

          var currentdate = new Date();
          var curr_hr = currentdate.getHours();
          var curr_mnt = currentdate.getMinutes();
          var nw_tim = curr_hr + ":" + curr_mnt;
          //alert(nw_tim);
          document.getElementById('pauseto').value = nw_tim;
      }
      
      lap() {
          let times = this.times;
          if (this.running) {
              this.reset();
          }
          let li = document.createElement('li');
          li.innerText = this.format(times);
          this.results.appendChild(li);
      }
      
      stop() {
          this.running = false;
          this.time = null;
          var currentdate = new Date();
          var curr_hr = currentdate.getHours();
          var curr_mnt = currentdate.getMinutes();
          var nw_tim = curr_hr + ":" + curr_mnt;
          document.getElementById('pausefrom').value = nw_tim;
      }

      restart() {
          if (!this.time) this.time = performance.now();
          if (!this.running) {
              this.running = true;
              requestAnimationFrame(this.step.bind(this));
          }
          this.reset();
      }
      
      clear() {
          clearChildren(this.results);
      }
      
      step(timestamp) {
          if (!this.running) return;
          this.calculate(timestamp);
          this.time = timestamp;
          this.print();
          requestAnimationFrame(this.step.bind(this));
      }
      
      calculate(timestamp) {
          var diff = timestamp - this.time;
          // Hundredths of a second are 100 ms
          this.times[2] += diff / 10;
          // Seconds are 100 hundredths of a second
          if (this.times[2] >= 100) {
              this.times[1] += 1;
              this.times[2] -= 100;
          }
          // Minutes are 60 seconds
          if (this.times[1] >= 60) {
              this.times[0] += 1;
              this.times[1] -= 60;
          }
      }
      
      print() {
          this.display.innerText = this.format(this.times);
      }
      
      format(times) {
        return `\
        ${pad0(times[0], 2)}:\
        ${pad0(times[1], 2)}:\
        ${pad0(Math.floor(times[2]), 2)}`;
      }
    }

    function pad0(value, count) {
        var result = value.toString();
        for (; result.length < count; --count)
            result = '0' + result;
        return result;
    }

    function clearChildren(node) {
        while (node.lastChild)
            node.removeChild(node.lastChild);
    }

  let stopwatch = new Stopwatch(
    document.querySelector('.stopwatch'),
    document.querySelector('.results'));
  </script>
