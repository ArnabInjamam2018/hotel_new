<!-- BEGIN PAGE CONTENT-->
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Substract Stock/ Inventory</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/update1_stock_invent',$form);

                        ?>
    <div class="form-body"> 
      <!-- 17.11.2015-->
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <!-- 17.11.2015-->
      
      <div class="row">
        <?php if(isset($assets)){
          //print_r($assets);
          foreach($assets as $asset){
             
       }}?>
       <input type="hidden" name="stock_inventory_id" value="<?php echo $asset->hotel_stock_inventory_id ;?>">
       <input type="hidden" id="stock_inventory_id" name="stock_id" value="<?php echo $asset->hotel_stock_inventory_id ;?>">
        <input type="hidden" id="qty1" name="qty1" value="">
        
        <input type="hidden" id="opening_qty" name="opening_qty" value="<?php echo $asset->opening_qty ;?>">
        <input type="hidden"  name="unit" value="<?php echo $asset->unit ;?>">
        <input type="hidden" id="closing_qty" name="closing_qty" value="<?php echo $asset->closing_qty ;?>">
        <input type="hidden" id="l_stock" name="l_stock" value="">
        <input type="hidden" id="stock_qty" name="stock_qty" value="">
        
        
        <div class="col-md-4">
        <div class="form-group form-md-line-input" >
          <input type="text" autocomplete="off" id="mydate" name="date" class="form-control" value="<?php echo date("Y/m/d");?>" readOnly placeholder="Date *">
          <label></label>
          <span class="help-block">Date *</span> </div>
        </div>
     <!--   <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <select class="form-control bs-select" id="a_type" name="a_type" required="required" >
            <option value="" selected="selected" disabled="disabled">Select Type</option>
            <?php 
                if(isset($type)){
                    //print_r($type);
                    //exit;
                foreach($type as $types){
                        
        ?>
            <option value="<?php //echo $types->asset_type_name;?>"><?php //echo $types->asset_type_name;?></option>
            <?php }}?>
          </select>
          <label></label>
          <span class="help-block">Type *</span> </div>
        </div>-->
        <div class="col-md-4">
        <div class="form-group form-md-line-input">
          <select class="form-control bs-select"  id="a_category" name="a_category" required="required" onchange="get_item_name()" >
            <option value="">Select Category</option>
            <?php 
                if(isset($category)){
                    //print_r($category);
                    //exit;
                foreach($category as $categorys){
                        
        ?>
            <option value="<?php echo $categorys->name; ?>"><?php echo $categorys->name; ?></option>
            
            <?php 
                
                }}		
                ?>
          </select>
          <label></label>
          <span class="help-block">Select Category *</span> </div>
        </div>
        <div class="col-md-4" >
            <div class="form-group form-md-line-input" id="hd">          
                <select class="form-control bs-select" id="a_name" name="a_name" required="required" >
                  <option value="">Select Item</option>
                  <?php if(isset($assets)){
                    foreach($assets as $asset){
                    
                ?>
                  <option value="<?php echo $asset->a_name;?>">
                  <?php  echo $asset->a_name;?>
                  </option>
                  <?php } }?>
                </select>
                <label></label>
                <span class="help-block">Select Item *</span> </div>
        </div>
          
          <div class="col-md-4" id="wh">
          <div class="form-group form-md-line-input">
          <select class="form-control bs-select"  name="warehouse" required="required" onchange="get_qty(this.value)" >
          	<option value="" selected="selected" disabled="disabled">Select Warehouse</option>
             <?php if(isset($warehouse)){
            //print_r($warehouse);
            foreach($warehouse as $wh){
          ?>
            <option value="<?php echo $wh->id;?>" <?php if($asset->warehouse==$wh->id) echo "selected";?>><?php echo $wh->name;?></option>
             <?php }}?>
            
          </select>
          <label></label>
          <span class="help-block">Select Warehouse *</span> </div>	
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="qty" name="qty"  required="required" onblur=" check_qty(this.value)" placeholder="QTY *">
              <label></label>
              <span class="help-block">QTY *</span> </div>
         </div>
		<div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="rsn" name="Reason"  required="required"  placeholder="Reason *">
              <label></label>
              <span class="help-block">Reason *</span> </div>
         </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" class="btn submit" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
    <!-- END CONTENT --> 
  </div>
</div>
<script>

function get_qty(value){
	//alert(value);
	 var item_id= document.getElementById("a_name").value;
	 //alert(item_id);
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/get_qty",
	   data: {data:value,item_id:item_id},
	   success: function(msg){
		//  alert(msg);
		  $("#stock_qty").val(msg);
		   
	   }
	});
}

function check_qty(value){
		
	var stock_qty=$("#stock_qty").val();
	var l_stock=$("#l_stock").val();
	var qty=value;
//	alert(stock_qty);
//	alert(l_stock);
	//exit;
	
	if(parseInt(value) > parseInt(stock_qty)){
		
		
		
		
		swal({
                            title: "Enter a lower Qty",
                            text: "The vale entered is higher than the current stock "+parseInt(stock_qty),
                            type: "warning"
                        },
                        function(){
							 document.getElementById("qty").value = "";
                           // location.reload();

                        });

	}
	else if(parseInt(value)<= parseInt(l_stock)){
			swal({
                            title: "Are you sure, This will move the item to low stock?",
                            text: "Current Low Stock Qty is "+parseInt(l_stock),
                            type: "warning"
                        },
                        function(){
							 //document.getElementById("qty").value = "";
                           // location.reload();

                        });
			
		}
}


function get_item_name(){
	//alert("sfbs");
	// var val= document.getElementById("a_type").value;
	 var val1= document.getElementById("a_category").value;
	//alert(val);
	//alert(val1);
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/get_item_name",
	   data: {data1:val1},
	   success: function(msg){
		   //alert(msg);
		   $("#hd").html(msg);
	   }
	});
}




function get_item_price(val){
	//alert(val);
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/get_item_price",
	   data: {data:val},
	   success: function(msg){
		  //alert(msg);
		  //
		  var value=msg.split("**");
		  var  value2=value[1];
		  var  value3=value[0];
		  //alert(value3);
           //return false;
		  
		  var val=value2.split("_");
         // alert(val[0]);
         // alert(val[1]);
         // alert(val[2]);
         // alert(val[3]);
         // alert(val[4]);
         //alert(val[5]);
		    $("#qty1").val(val[2]);
			 $("#opening_qty").val(val[3]);
		    $("#closing_qty").val(val[4]);
		    $("#l_stock").val(val[5]);
		   // $("#u_price").val(val[0]);
			$("#wh").html("<div class='form-group form-md-line-input'><select class='form-control'  name='warehouse' required='required' onchange='get_qty(this.value)' >"+value3+"</select> <label> </label><span class='help-block'>Select Warehouse *</span></div>");
	   }
	});
}




var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
$(document).ready(function() {
$('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
				endDate: today,
                autoclose: true
            });
});
</script> 
