<style>
.task_name{
	cursor:pointer;
	
}
</style>
<script>
$( document ).ready(function() {
	var ht = $("#scnd_div").height();
	//var ht = document.getElementById('scnd_div').height();
	//alert(ht);
    
	if(ht=="333"){
		//document.getElementById('scnd_div').style.overflow='auto';
	}
	
	var task_ido;
	var cnt = 0;
	
	$('.task_name').on('click',function(){

		$(this).each(function( index ) {
			
			var task_asignee = $(this).parent().find('.task_asignee').val();
			var task_id = $(this).parent().find('.task_id').val();
			//console.log('The task id:' +task_id);
			//console.log('The task asignee:' +task_asignee);
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>dashboard/other_task_details",
				data: {
					task_asignee: task_asignee,
					task_id: task_id
					},
				success: function(data){
					$('#scnd_div').html(data);
					console.log(task_ido+' '+cnt);
					//console.log(document.getElementById(task_id));
					document.getElementById(task_id).style.color = '#07635a';
					$('#'+task_id).css('background', '#f8fbd8');
					if(cnt > 1){
						document.getElementById(task_ido).style.color = 'black';
						$('#'+task_ido).css('background', '#F9F9F9');
					} else {
						$('#'+<?php echo $get_task->id ?>).css('background', '#F9F9F9');
					}
					task_ido = task_id;
				}
				
			});	
		});
		cnt++;
	});
});
</script>

<div class="row">
    <div class="col-md-6">
    	<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    Other Tasks 
                </div>           
            </div>
			
			 <div class="portlet-body">
				<table class="table table-striped table-hover table-bordered table-scrollable">
					<?php
					foreach ($get_other_tasks as $get_other_tasks) 
					{
						$cmps = '#32C5D2';
						$get_status = $get_other_tasks->status;
						if ($get_status == "0") {
							$task_status_desc = "<span style='color:red;'>Pending</span>";
							
						}
						else{
							$task_status_desc = "<span style='color:green;'>Completed</span>";
							
						}
						
						$tot = $get_other_tasks->to_id;
						$to_detailst = $this->dashboard_model->get_task_user($tot);
						foreach ($to_detailst as $to_detailst) {
                                $toname = $to_detailst->admin_first_name .' '. $to_detailst->admin_middle_name .' '. $to_detailst->admin_last_name ;
                        }

						?>
						<tr>
							<td style="font-size:14px;width:10px;">
							  <div style="height:20px;overflow:hidden;" class="task_div">
							  <input type="hidden" name="task_id" class="task_id" value="<?php echo $get_other_tasks->id ;?>">
							  <input type="hidden" name="task_asignee" class="task_asignee" value="<?php echo $get_other_tasks->to_id; ?>">
							  <span style="<?php if($get_other_tasks->id == $get_task->id) echo 'background:#F8FBD8';?>" id="<?php echo $get_other_tasks->id; ?>" class="task_name"><?php echo $get_other_tasks->title.' - <i class="fa fa-calendar" aria-hidden="true"></i> '.date("jS F Y", strtotime($get_other_tasks->due_date)).' | <i class="fa fa-user" aria-hidden="true"></i> '.$toname; ?></span>
							  <span class="task_name pull-right"><?php echo $task_status_desc; ?></span>
								
							  </div>
							</td>
						</tr>
						<?php
					}
					?>
				</table>
            </div>
			
			
			
			
		</div>
    </div>
	<?php 
				//print_r($get_task);
                $from = $get_task->from_id;
                $to = $get_task->to_id;
    
                $from_details = $this->dashboard_model->get_task_user($from);
                $to_details = $this->dashboard_model->get_task_user($to);
    
                $get_status = $get_task->status;
                if ($get_status == "0") {
                    $task_status_desc = "Pending";
					$cmps = '#EA5B5B';
                }
                else{
                    $task_status_desc = "Completed";
					$cmps = '#4DA14D';
                }
                $get_prority = $get_task->priority;
                if ($get_prority == '1')                                           
                {
                    $color = "#06050B";
					$background = "#ebeaf1";
                }
                else if ($get_prority == '2') 
                {
                    $color = "#180f01";
					$background = "#fef4e6";
                }
                else
                {
                    $color = "#081314";
					$background = "#edf9f9";
                }
            ?>
	
    <div id="scnd_div" class="col-md-6">
	<?php if($get_task->title) { ?>
    	<div class="portlet box green">
		    <div class="portlet-title" style="background-color:<?php echo $cmps ?>;">
                <div class="caption">
                	<?php echo $get_task->title; ?>
                </div>
            </div>
            
            <div class="portlet-body">
               <ul class="list-group" style="margin-bottom:0;"> 
                    <li class="list-group-item" style="background:<?php echo $background; ?>">
                        <label style="font-weight:bold">Assignee:</label>
                        <label style="color:<?php echo $color; ?>;">
                        <?php
                            foreach ($from_details as $from_details) 
                            {
                                echo $from_details->admin_first_name .' '. $from_details->admin_middle_name .' '. $from_details->admin_last_name;
                            }
                        ?>
                        </label>
                    </li>
                    <li class="list-group-item" style="background:<?php echo $background; ?>">
                        <label style="font-weight:bold">Asigned to:</label>
                        <label style="color:<?php echo $color; ?>;">
                        <?php
                            foreach ($to_details as $to_details) 
                            {
                                echo $to_details->admin_first_name .' '. $to_details->admin_middle_name .' '. $to_details->admin_last_name ;
                            }
                        ?>
                        </label>
                    </li>
                    <li class="list-group-item" style="background:<?php echo $background; ?>">
                        <label style="font-weight:bold">Task Details:</label>
                        <label style="color:<?php echo $color; ?>;">
                            <?php echo $get_task->task; ?>
                        </label>
                    </li>
                    <li class="list-group-item" style="background:<?php echo $background; ?>">
                        <label style="font-weight:bold">Task Status:</label>
                        <label style="color:<?php echo $color; ?>;">
							<?php echo $task_status_desc; ?>
                        </label>
                    </li>
                    <li class="list-group-item" style="background:<?php echo $background; ?>">
                        <label style="font-weight:bold">Task Due Date:</label>
                        <label style="color:<?php echo $color; ?>;">
							<?php echo date("l jS F Y", strtotime($get_task->due_date)); ?>
                        </label>
                    </li>
                    <li class="list-group-item" style="background:<?php echo $background; ?>">
                        <label style="font-weight:bold">Task Given on:</label>
                        <label style="color:<?php echo $color; ?>;">
                            <?php echo date("g:i A \- l jS F Y", strtotime($get_task->added_date)); ?>
                        </label>
                    </li>  
					<li class="list-group-item" style="background:<?php echo $background; ?>">
						<?php if($task_status_desc == 'Pending') { ?>
                        <div class="task-config-btn btn-group"> 
							<a href="<?php echo base_url(); ?>dashboard/complete_task?task_id=<?php echo $get_task->id; ?>" class="btn green btn-sm" style="padding: 4px 3px 4px;"> Mark this task Complete <i class="fa fa-check"></i></a> 
						</div>
						<?php } else if ($task_status_desc == 'Completed') { ?>
						<div class="task-config-btn btn-group"> 
							<a href="<?php echo base_url(); ?>dashboard/reopen_task?task_id=<?php echo $get_task->id; ?>" class="btn green btn-sm" style="padding: 4px 3px 4px;"> Reopen this task <i class="fa fa-check"></i></a> 
						</div>
						<?php } ?>
                    </li>  
            	</ul>    
            </div>
         </div>
	<?php } ?>
    </div>
	
</div>

