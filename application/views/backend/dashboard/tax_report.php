<?php 
//echo '<pre>';
//print_r($line_item);?>

<div class="portlet light borderd">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-file-archive-o"></i>Tax Reports </div>
    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body" style="position:relative;">
  	<div id="loading" style="display:none;">
          <div style="position:absolute; z-index:3; top:20%; left:50%;">
              <div class='loader'>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>
                <div class='window'></div>				
                <div class='door'></div>
              </div>
              <div class='hotel-sign'> <span>H</span> <span>O</span> <span>T</span> <span>E</span> <span>L</span> </div> 
          </div>
      	   <div id="loading-out" style="position:absolute; width:100%; height:100%; background:rgba(0,0,0,0.1); z-index:2; top:0; left:0;"></div>
      </div>
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-7">
          <?php

                            $form = array(
							
                                'class'     => 'form-inline',
                                'id'        => 'form_date',
                                'method'    => 'post'
                            );

                           echo form_open_multipart('dashboard/tax_report',$form);

                            ?>
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" id="t_dt_frm" value="<?php if(isset($start_date)){ echo $start_date;}?>" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date">
          </div>
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" id="t_dt_to" class="form-control date-picker" placeholder="End Date">
          </div>
          <!--  <button class="btn btn-default" onclick="check_sub()" type="submit">Search</button>-->
          <button class="btn btn-default" onclick="tax_filter1()" type="button">Search</button>
          <?php form_close(); ?>
        </div>
      </div>
    </div>
    <div class="tabbable-line tabbable-full-width">
      <ul class="nav nav-tabs">
        <li id="mr" class="tabV" onclick="get_mr()"> <a> Meal Plan & Room Rent Tax</a> </li>
        <li id="rr" class="tabV"> <a onclick="get_rmTax()"> Room Rent Tax </a> </li>
        <li id="mp" class="tabV"> <a onclick="get_mpTax()"> Meal Plan Tax </a> </li>
      </ul>      
      <div id="table1" style="margin-top: -20px;">
        <table class="table table-striped table-hover table-bordered" id="sample_4">
          <thead>
            <tr>
              <th> # </th>
              <th>Reservation ID </th>
              <th> Guest Name </th>
              <th> Room No</th>
              <th> Booking Date</th>
              <th> Room Rent</th>
              <?php
				$f=0;
				$cm=0;
				$as = array();
				$sa = array();
				$taxType=$this->dashboard_model->get_distinct_tax_type();
			//echo "<pre>";
			//print_r($taxType);
				if(isset($taxType) && $taxType){
				
				foreach($taxType as $tax){
					
				array_push($as,$tax->tax_name);	
				?>
              <th><?php echo 	$tax->tax_name ?></th>
              <?php } 
				}?>
              <th class="">POS Tax</th>
              <th class="">Extra Tax</th>
              <th class="">Total Tax</th>
            </tr>
          </thead>
          <tbody>
            <?php 
			$sl=0;
			$mf=0;
			$co=0;
			$pos_tax1=0;
			$pos_tax=0;	
			$pos_tot_amount1=0;	
			$ch_tax=0.00;
			$s_tax=0.00;
$charge_details=0;			
				if(isset($line_item) && $line_item){
			//$id=$item->booking_id;
		
					foreach($line_item as  $item){
					//if(isset($item->booking_extra_charge_id) && $item->booking_extra_charge_id>0){
                                            $charge_id_array=explode(",",$item->booking_extra_charge_id);
											$ch_tax=0.00;
                                           for($i=1;$i<sizeof($charge_id_array);$i++) {
                    
                                        $charge_details=$this->dashboard_model->get_charge_details($charge_id_array[$i]);
									 if(isset($charge_details->crg_quantity) && $charge_details->crg_quantity){
									 $ch_qty=$charge_details->crg_quantity;
									 }else{
										 $ch_qty=0;
									 }
									 if(isset($charge_details->crg_unit_price) && $charge_details->crg_unit_price){
										$ch_price=$charge_details->crg_unit_price;
									 }else{
										 $ch_price=0;
									 }
										if(isset($charge_details->crg_tax) && $charge_details->crg_tax){
											$cr=$charge_details->crg_tax;
										}else{
											$cr=0;
										}
										$ch_amt=$ch_price*($cr/100)*$ch_qty;
										$ch_tax =$ch_tax+$ch_amt;
									//echo " ch tax ".$ch_tax." id ".$item->booking_id;
								}
					
					//}	
					if($item->booking_id_actual!='') { 
					
						$id=$item->booking_id;
						$extra_ser=$this->dashboard_model->get_service_details2($id);
						if(isset($extra_ser) && $extra_ser){
					foreach($extra_ser as $service_tax){
						//$s_tax+=$service_tax->tax;
						$sp=$service_tax->s_price;
						 $qty=$service_tax->qty;
						$stax_amt=$sp*($service_tax->tax/100)*$qty;
						$s_tax=$stax_amt;
				
						}
					
					}else { $s_tax=0; }
					}
					else{  $id=$item->booking_id;
						  $extra_ser=$this->dashboard_model->get_service_details3($id);	
						  if(isset($extra_ser) && $extra_ser){
					
					foreach($extra_ser as $service_tax){
						$sp=$service_tax->s_price;
						 $qty=$service_tax->qty;
						$stax_amt=$sp*($service_tax->tax/100)*$qty;
						$s_tax=$stax_amt;
						//echo $stax_amt."rtyhtf";
						}
					
					}else { $s_tax=0; }
					}
					
				$extra_tax_amt=$ch_tax +$s_tax;
					?>
          <td><?php echo ++$sl;?></td>
            <td><?php if($item->group_id == 0){ echo '<i style="color:#023358;" class="fa fa-user"></i> BK0'.$this->session->userdata('user_hotel').'0'.$item->booking_id;} else { echo '<i style="color:#F8681A;" class="fa fa-group"></i>-><i style="color:#023358;" class="fa fa-user"></i> BK0'.$this->session->userdata('user_hotel').'0'.$item->booking_id.' ('.$item->group_id.')';} ?></td>
            <td><?php echo $item->cust_name ?></td>
            <td><?php echo $item->room_no ?></td>
            <td><?php  
			echo date("dS-M-Y",strtotime($item->cust_from_date));?>
				<div style="color:#1F897F;">-to-</div>
				<?php 
				
				echo date("dS-M-Y",strtotime($item->cust_end_date));?>
			
		</td>
				
		<td>
		<?php echo $item->rm_total;?>
		</td>
            <?php   
			  if($item->group_id==0){
				   $poses=$this->dashboard_model->all_pos_booking($item->booking_id);
					
            
                if($poses){
                foreach ($poses as $key ) {
				$pos_tax=$key->tax;
                $pos_tot_amount1=$key->total_amount;
				}}	
				  $type1='sb';
				 $bid=$item->booking_id;
			  }
			  else{
				 $type1='gb';
				   $bid=$item->group_id; 
				   $poses_gb=$this->dashboard_model->all_pos_booking_group($item->group_id);
				   $pos_amount1=0;
            
                if($poses_gb){
                foreach ($poses_gb as $key1 ) {
				$pos_tax1=$key1->tax;
				 $pos_tot_amount1=$key1->total_amount;
                }}
			  }
			
		$taxData=$this->bookings_model->line_charge_item_tax($type1,$bid);
			
		 if(isset($taxData) && $taxData){
			 //unset($taxData['mp_tax']['total']);
			unset($taxData['mp_tax']['r_id']);
			unset($taxData['mp_tax']['planName']);
			//unset($taxData['mp_tax']['Service Charge']);
			
			unset($taxData['rr_tax']['r_id']);
			unset($taxData['rr_tax']['planName']);
			unset($taxData['rr_tax']['total']);
			$taxData['booking_id']=$bid;
			$a1=$taxData['rr_tax'];
			$a2=$taxData['mp_tax'];//print_r($taxData);
			$keys = array_fill_keys(array_keys($a1 + $a2), 0);
		
			$sum_array=array();
			//................NB
			$sa = array_merge($keys,$a1);
			$sb = array_merge($keys,$a2);
			
			
		

			$sc = array();
			foreach (array_keys($sa + $sb) as $key) {
				$sc[$key] = $sa[$key] + $sb[$key];
			}
	
			if($taxData['mp_tax'])
			{ 
				$f=count($taxData['mp_tax']);
			}
			else {//echo ;
			}
			
				foreach($as as $key => $val1){
					
				if (array_key_exists($val1, $sc)) { ?>
            <td><?php echo $sc[$val1];?></td>
            <?php } else { ?>
            <td>0.00</td>
            <?php }
				}
				} else { 
                if(isset($taxType) && $taxType){
				
				foreach($taxType as $tax){
				?>
            <td>0.00</td>
            <?php } 
				}?>
            <?php }	?>
            <td><?php if($item->group_id!='' && $item->group_id!=0)  { echo $pos_tax1;}else { echo $pos_tax;} ?></td>
            <td><?php
				//print_r($charge_details);
			echo number_format($extra_tax_amt,2,'.',',');
			  
			  ?></td>
            <td><?php echo $item->rr_tot_tax+$item->mp_tax+$pos_tax+$pos_tax1+$extra_tax_amt;  ?></td>
          </tr>
          <?php 
				}}
			?>
            </tr>
          
            </tbody>
          
        </table>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var type='both_tax';
function get_mr(){
	
	window.location.replace("<?php echo base_url()?>dashboard/tax_report");
		$('#mr').addClass("active");
}
function loaderOn() {
    $("#loading").show();
}

function loaderOff() {
    $("#loading").hide();
}

function tax_filter1(){
	var start_date=$("#t_dt_frm").val();
	var end_date=$("#t_dt_to").val();
	//alert(type);
	//alert(end_date);
	loaderOn();
	$.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>dashboard/tax_filter",
                data:{start_date:start_date,end_date:end_date,type:type},
                success:function(data)
                { 
                   ///alert(data);
				   $('#table1').html(data);	
				$.getScript('<?php echo base_url();?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
				   $.getScript('<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons-ex.min.js');
				  $.getScript('<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js');
				  
                loaderOff();
				}
            });
}

function get_rmTax(){
	type='rm_tax';
	
		$("#t_dt_frm").val('');
	$("#t_dt_to").val('');
	loaderOn();
	$('.tabV').removeClass("active");
		$('#rr').addClass("active");
	$.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>dashboard/rr_tax_report",
                data:{},
                success:function(data)
                { 
                   ///alert(data);
				   $('#table1').html(data);	
				$.getScript('<?php echo base_url();?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
				   $.getScript('<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons-ex.min.js');
				  $.getScript('<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js');
				  
                loaderOff();
				}
            });
}
function get_mpTax(){
	type='mp_tax';
	$("#t_dt_frm").val('');
	$("#t_dt_to").val('');
	loaderOn();
	$('.tabV').removeClass("active");
		$('#mp').addClass("active");
	$.ajax({
                type:"POST",
                 url: "<?php echo base_url()?>dashboard/mp_tax_report",
                data:{},
                success:function(data)
                {
                   ///alert(data);
					  $('#table1').html(data);	

				  $.getScript('<?php echo base_url();?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
				  $.getScript('<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons-ex.min.js');
				  $.getScript('<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js');
				   
                loaderOff();
				}
            });
}

$(document).ready(function(){
  $('#chkbx_tdy').prop('checked', true);
   $("#loading").hide();
 	$('#mr').addClass("active");
});

function check_sub(){
  document.getElementById('form_date').submit();
}
</script>