<style type="text/css">
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
}
body {
	font-family: Corbel;
}
.list-unstyled {
	list-style: none;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
	background-color: transparent;
	border-color: grey;
	display: table;
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555555;
}
.td-pad th, .td-pad td {
	padding: 5px;
}
.td-pad th, .td-pad td{
	font-size:12px;
}
</style>
<div style="padding:15px 35px;">     
       <table>
		<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));?>
    <tr>
      	<td align="left"> <img src="upload/hotel/<?php if(isset($hotel_name->hotel_logo_images_thumb))echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/></td>
        <td colspan="2" align="center"><strong><font size='13'>Tax Report</font></strong></td>
		<td align="right"><?php echo "<strong><font size='14'>".$hotel_name->hotel_name.'</font></strong>'?></td>
    </tr>
    <tr>
      <td width="100%" colspan="4"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td colspan="3">
	<?php if(isset($start_date) && isset($end_date) && $start_date && $end_date ){
		echo "<strong>From: </strong>".$start_date."  <strong>To: </strong>".$end_date;
	}
		?>
	</td><td align="right"><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%" colspan="4">&nbsp;</td>
    </tr>
</table>
          <table class="td-pad" width="100%">
            <thead>
              <tr style="background: #00C5CD; color: white"> 
                <th> # </th>
				<th>Booking ID </th>
				<th> Guest Name </th>
                <th> Check In </th>
				 <th> Check Out </th>
                <th> Room Charge</th>
                <th>Other Charge</th>
			    <th> Luxury Tax</th>
				<th> Service Tax</th>
				<th> Service Charge</th>
			    <th>Vat-Food </th>
                <th> Vat-Liq </th>
                <th>Total</th>
				<th>Pending</th>
                <th> Status</th>
              </tr>
            </thead>
            <tbody>
              <?php
						if($bookings && isset($bookings)){
						$srl_no=0;
						
						//echo "<pre>";
					//print_r($bookings);
						//exit;
						
						foreach($bookings as $report){
							$srl_no++;
							//echo "<pre>";
							//print_r($report);
							//exit;
						$status_id=$report->booking_status_id;
						$booking_id=$report->booking_id;
						$status=$this->dashboard_model->get_status_by_id($status_id);
						$fd_vat=$this->dashboard_model->get_fd_tax_by_id($booking_id);
						
					//if(!empty($fd_vat)){
					//foreach($fd_vat as $vat){
						//echo $vat->lvat;
					//}//
					//}
					//print_r($fd_vat);
					 
							if(!empty($status)){
					 foreach($status as $st){
						
			  ?>
             <tr style="background: #F2F2F2">
             
				<td align="center"><?php echo $srl_no;?></td>
				<td align="center"><?php echo $report->booking_id_actual;?></td>	
				<td align="center">
					 <?php echo $report->cust_name;?>    
				</td>
				<td align="center"><?php echo $report->cust_from_date_actual;?></td>
				<td align="center"><?php echo $report->cust_end_date_actual;?></td>
				<td align="center"><?php echo $report->room_rent_total_amount;?></td>
				<td align="center"><?php echo $report->extra_service_total_amount;?></td>
				<td align="center"><?php echo $tax_ammunt=$report->room_rent_total_amount* ($report->luxury_tax/100);?></td>
				<td align="center"><?php echo $service_tax_amount=$report->room_rent_total_amount* ($report->service_tax/100);?></td>
				<td align="center"><?php echo $extra_ch_amount=$report->room_rent_total_amount* ($report->service_charge/100);?></td>
				<td align="center"><?php  if(isset($fd_vat->fvat)) { echo $f=$fd_vat->fvat; } ?></td>
				<td align="center"><?php  if(isset($fd_vat->lvat)) { echo $l=$fd_vat->lvat; } ?></td>
				<td align="center"><?php 
				
					if(isset($fd_vat)){
					echo $total=$report->room_rent_sum_total+$fd_vat->fvat+$fd_vat->lvat;
					}
				else{
						echo $total=$report->room_rent_sum_total;
				}
				 ?></td>
				<td align="center"><?php echo $report->total_due_amount;?></td>
				
					<td align="center">
					              <span class="label" style="background-color:<?php echo $st->bar_color_code;?>; color:<?php echo $st->body_color_code ?>;"> <?php echo $st->booking_status; ?></span>
				
				</td>
			  </tr>
					 <?php }
					}}}?>
            </tbody>
          </table>
		 
		</div>
											
		  
