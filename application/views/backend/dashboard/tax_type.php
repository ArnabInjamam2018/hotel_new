<?php if($this->session->flashdata('err_msg')):?>
	<div class="alert alert-danger alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
	<div class="alert alert-success alert-dismissible text-center" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>

<div class="portlet light borderd">
  <div class="portlet-title">
	<div class="caption"> <strong><i class="fa fa-edit"></i></strong> List of All Tax Elements </div>
	<div class="actions"> 
		<a href="<?php echo base_url();?>dashboard/tax_type_add" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i>Add New </a>
	</div>
	</div>
	<div class="portlet-body">	  
	  <table class="table table-striped table-bordered table-hover" id="sample_1">
		<thead>
		  <tr>
			<th scope="col">Sl </th>
			<th scope="col">Tax Element </th>
			<th scope="col">Tax Category </th>
			<th scope="col">Tax Type </th>
			<th scope="col">Tax Description </th>
			<th scope="col"> Action </th>
		  </tr>
		</thead>
		<tbody>
		
		
		  <?php
					  $sl =0;
					  foreach($tax_details as $gst){
						$sl++;  
						  $g_id=$gst->htl_id;
						  ?>
		  <tr id="row_<?php echo $gst->tax_id;?>">
			<td><?php echo $sl;?></td>
			<td><?php echo $gst->tax_name; ?></td>
			<td><?php echo $gst->tax_category; ?></td>
			
			<?php
			//echo $this->dashboard_model->tax_type_join($gst->tax_type_tag_id);
			$tax_data = $this->dashboard_model->tax_type_join($gst->tax_type_tag_id);
			//$tax_type = $tax_data['tax_type'];
			//die();
			?>
			<td><?php if(isset($tax_data) && $tax_data ) echo $tax_data->tax_name; ?></td>
			<td><?php echo $gst->tax_desc; ?></td>
			
			<!--<td align="center"><?php //echo $gst->unit_type; ?></td>
			<td align="center"><?php //echo $gst->unit_class;?></td>
			<td align="center"><?php //echo $gst->unit_desc;
								
								 ?></td>-->
			<!--<td align="center">  
			<a href="<?php //echo base_url() ?>dashboard/edit_unit_type/<?php //echo $gst->id;?>" class="btn blue"><i class="fa fa-edit"></i></a>            
			<a onclick="soft_delete('<?php //echo $g_id;?>')" data-toggle="modal"  class="btn red" ><i class="fa fa-trash"></i></a></td>-->

			
			<td align="center" class="ba">
            	<div class="btn-group">
                  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
				  <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $gst->tax_id;?>')" data-toggle="modal"  class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
               <li><a onclick="edit_extra_charge('<?php echo $gst->tax_id;?>')" class="btn green btn-xs" data-toggle="modal"><i class="fa fa-edit"></i></a></li>
              </ul>
                </div>	
			</td>				
		  </tr>
		  <?php
					  }
					  
					  ?>
		</tbody>
	  </table>
	</div>
</div>

<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Tax Settings</h4>
       </div>
       <input type="hidden"  name="taxid" id="taxid"  value=''>

      <div class="modal-body">
        <div class="row">
		   <div class="col-md-4">
            <div class="form-group form-md-line-input">
             <input type="text" class="form-control" name="t_name" id="t_name" required placeholder="Tax Name">

			 
			
              <label></label>
              <span class="help-block">Tax Name</span> </div>
          </div>
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
             <!-- <input autocomplete="off" type="text" class="form-control" id="tax_cate" name="tax_cate" placeholder="Tax Category ..."  >-->
			  <select  class="form-control"  id="tax_cate" name="tax_cate" required >
               
						<?php 
						/*$tag1 = $this->dashboard_model->tax_category_list();
						//print_r($tag1);
						
						//if(isset($tag)){
							foreach($tag1 as $tag){?>
								<option value="<?php echo $tag->tax_category?>"><?php echo $tag->tax_category; ?></option>
							<?php
							//}	
							}*/ 
							
						?>	
						
						<option value="Direct">Direct</option>
						<option value="Indirect">Indirect</option>
						<option value="Non-statutory">Non-statutory</option>
						
					
              </select>
			
              <label></label>
              <span class="help-block">Tax Category *</span> </div>
          </div>
		  <div class="col-md-4">
            <div class="form-group form-md-line-input" id="tax_type1">
              <select class="form-control"  id="tax_type" name="tax_type" required >
               
               <?php 
						$tag1 = $this->dashboard_model->tax_type_list();
						//if(isset($tag)){
							foreach($tag1 as $tag){?>
								<option value="<?php echo $tag->tax_type_id ?>"><?php echo $tag->tax_name; ?></option>
							<?php
							//}	
						}
					  ?>
              </select>
              <label></label>
              <span class="help-block">Tax Type *</span> </div>
          </div>
		  <div class="col-md-12">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="tax_desc" name="tax_desc" placeholder="Tax Description..."  maxlength="50" >
              <label></label>
              <span class="help-block">Enter tax Description *</span> </div>
          </div>
      
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="button" class="btn green" onclick="update_tax()">Save</button>
      </div>
       </div>
  </div>
</div>

<script>

 
 
 function soft_delete(tax_id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_tax_type",
                data:{taxname:tax_id},
                success:function(data)
                {
					//alert(data);
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                           $('#row_'+id).remove();
						   //location.reload();

                        });
                }
            });



        });
    }
 
	</script>
	<script>
	
	function update_tax(){
		//alert('hello');
		var id=$('#taxid').val();
		var name=$('#t_name').val();
		var tax_cate=$('#tax_cate').val();
		var tax_desc=$('#tax_desc').val();
		var tax_types=$('#tax_type').val();
		//alert(id + '' +name + '' +tax_cate + '' +tax_types);
			$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/update_tax_type",
                data:{taxid:id,taxname:name,tax_categ:tax_cate,tax_descrp:tax_desc,tax_type_id:tax_types},
                success:function(data)
                {
					//alert(data);
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
	
                    $('#responsive').modal('hide');
					
                }
            });
		
	}
</script>

<script>
function edit_extra_charge(tax_id){
		//alert(tax_id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/edit_tax_plan",
                data:{"taxid":tax_id},
                success:function(data)
                {
                   // alert(data.unit_price_editable);
				// $('#name').append( '<option value="'+data.charge_name+'">'+data.charge_name+'</option>' );
				//alert(data);
				console.log(data);
				
				   $('#t_name').val(data.tax_name);
				   $('#taxid').val(data.tax_id);
				   $('#tax_cate').val(data.tax_category);
				   $('#tax_type').val(data.tax_type_tag_id);
				   $('#tax_desc').val(data.tax_desc);
				   
				   
                    $('#responsive').modal('toggle');
					
                }
            });
	}
</script>
<script>
$(document).ready(function(){
	
	//alert('hello');
	$('#tax_cate').on('change',function(){
		
		var category = $('#tax_cate').val();
		//alert(category);
		$.ajax({
			
			type:"POST",
			url:"<?php echo base_url()?>dashboard/tax_ajax",
			data:{t_cate:category},
			success:function(data){
				
				//console.log(data);
				//alert(data);
				$('#tax_type1').html(data);
				
			},
			
		});
	});
	

});

</script>


