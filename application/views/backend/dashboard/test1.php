<div class="row">
  <div class="form-body"> 
    <!-- 17.11.2015-->
    <?php if($this->session->flashdata('err_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-danger alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
    <?php if($this->session->flashdata('succ_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-success alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
  </div>
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-edit"></i>List of All Food Plans </div>
        <div class="tools"> 
          <a href="javascript:;" class="reload"></a> </div>
        </div>
        <div class="portlet-body">
          <div class="table-toolbar">
            <div class="row">
              <div class="col-md-6">
                <!--<div class="btn-group">
                  <button class="btn green"  data-toggle="modal" href="#responsive"> Add New <i class="fa fa-plus"></i> </button>
                </div>-->
              </div>
              <div class="col-md-6">
                  <div class="btn-group pull-right" style="margin-left:10px;">
                    <button class="btn dropdown-toggle" data-toggle="dropdown">Filter Column <i class="fa fa-angle-down"></i> </button>
                    <ul class="dropdown-menu pull-right checkbox-list">
                      <li> <label><input class="show_hide_column" checked data-columnindex="0" type="checkbox"> Plan Name</label> </li>
					  <li> <label><input class="show_hide_column" checked data-columnindex="1" type="checkbox"> Description</label> </li>
					  <li> <label><input class="show_hide_column" checked data-columnindex="2" type="checkbox"> Plan Category </label></li>
					  <li> <label><input class="show_hide_column" checked data-columnindex="3" type="checkbox"> Plan Class </label></li>
					  <li> <label><input class="show_hide_column" checked data-columnindex="4" type="checkbox"> Price For Adult </label></li>
					  <li> <label><input class="show_hide_column" checked data-columnindex="5" type="checkbox"> Price For Child</label></li>
					  <li> <label><input class="show_hide_column" checked data-columnindex="6" type="checkbox"> Tax Applied</label></li>
					  <li> <label><input class="show_hide_column" checked data-columnindex="7" type="checkbox"> Discount Applied</label></li>
					  <li> <label><input class="show_hide_column" checked data-columnindex="8" type="checkbox">Tax percentage</label></li>
                      <li> <label><input class="show_hide_column" checked data-columnindex="9" type="checkbox"> Action </label></li>
                    </ul>
                  </div>

                  <div class="btn-group pull-right">
                    <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i> </button>
                    <ul class="dropdown-menu pull-right">
                      <li> <a href="javascript:;"> Print </a> </li>
                      <li> <a href="javascript:;" id="pdf"> Save as PDF </a> </li>
                      <li> <a href="javascript:;" id="excel"> Export to Excel </a> </li>
                    </ul>
                  </div>
            	</div> 
            </div>
          </div>
          <table class="table table-striped table-hover table-scrollable" id="dataTable1">
            <thead>
              <tr>
                <th scope="col">Plan Name </th>
				<th scope="col">Description </th>
				<th scope="col">Plan Category </th>
				<th scope="col">Plan Class </th>
				<th scope="col">Price For Adult </th>
				<th scope="col">Price For Child </th>
				<th scope="col">Tax Applied </th>
				<th scope="col">Discount Applied</th>
				<th scope="col">Tax percentage </th>
                <!--<th scope="col"> Unit Type </th>
                <th scope="col"> Unit Class </th>
                <th scope="col" width="400"> Unit Description </th>-->
                <th scope="col"> Action </th>
              </tr>
            </thead>
            <tbody>
              <?php if(isset($unit) && $unit):

                          $i=1;
                          foreach($unit as $gst):
                              $class = ($i%2==0) ? "active" : "success";
                              $g_id=$gst->id;
                              ?>
              <tr id="row_<?php echo $gst->id;?>">
                <td><?php echo $gst->fp_name; ?></td>
				<td><?php echo $gst->fp_description; ?></td>
				<td><?php echo $gst->fp_category; ?></td>
				<td><?php echo $gst->fp_class; ?></td>
				<td><?php echo $gst->fp_price_adult; ?></td>
				<td><?php echo $gst->fp_price_children; ?></td>
				<td><?php echo $gst->fp_tax_applied; ?></td>
				<td><?php echo $gst->fp_discount_applied; ?></td>
				<td><?php echo $gst->fp_tax_percentage; ?></td>
                <!--<td align="center"><?php //echo $gst->unit_type; ?></td>
                <td align="center"><?php //echo $gst->unit_class;?></td>
                <td align="center"><?php //echo $gst->unit_desc;
  									
  									 ?></td>-->
                <!--<td align="center">  
                <a href="<?php //echo base_url() ?>dashboard/edit_unit_type/<?php //echo $gst->id;?>" class="btn blue"><i class="fa fa-edit"></i></a>            
                <a onclick="soft_delete('<?php //echo $g_id;?>')" data-toggle="modal"  class="btn red" ><i class="fa fa-trash"></i></a></td>-->

				
				<td align="center">
                <table>
                	<tr>
                    	<td>
							<a href="<?php echo base_url() ?>dashboard/view_food_plan?id=<?php echo $gst->id;?>"  class="btn green" ><i class="fa fa-eye"></i></a>
                		</td>
                        <td>				
                			<a href="<?php echo base_url() ?>dashboard/edit_food_plan?id=<?php echo $gst->id;?>" class="btn blue"><i class="fa fa-edit"></i></a>
                		</td>
                        <td>            
                 			<a onclick="soft_delete('<?php echo $gst->id;?>')" data-toggle="modal"  class="btn red" ><i class="fa fa-trash"></i></a>
                 		</td>
                 	</tr>
                </table>				
				</td>				
              </tr>
              <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
    </div>
    
    <!-- END SAMPLE TABLE PORTLET--> 
  </div>
</div>
<!-- /.modal -->
<script src="<?php echo base_url();?>assets/common/js/table_sort_style.js" type="text/javascript"></script>
<script type="text/javascript">
 function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_fp?fp_id="+id,
                data:{m_id:id},
                success:function(data) {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){
                            //location.reload();
							$('#row_'+id).remove();
                        });	
                }
            });
        });
    }
</script>

