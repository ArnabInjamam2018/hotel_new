<div>

	<button type="button" class="btn submit" onclick="func()">Offset Test</button>

</div>

<?php 

	
	
	date_default_timezone_set("Asia/Kolkata");
		
        $times=$this->dashboard_model->checkin_time();
		
        foreach($times as $time) {
            if($time->hotel_check_in_time_fr=='PM' && $time->hotel_check_in_time_hr !="12") {
                $modifier = ($time->hotel_check_in_time_hr + 12) + $time->hotel_check_in_time_mm/60;
            }
            else{
                $modifier = ($time->hotel_check_in_time_hr) + $time->hotel_check_in_time_mm/60;
            }
        }

        $times2=$this->dashboard_model->checkout_time();
		
        foreach($times2 as $time2) {
            if($time2->hotel_check_out_time_fr=='PM' && $time2->hotel_check_out_time_hr !="12") {
                $modifier2 = ($time2->hotel_check_out_time_hr + 12) + $time->hotel_check_out_time_mm/60;
            }
            else{
                $modifier2 = ($time2->hotel_check_out_time_hr) + $time->hotel_check_out_time_mm/60;
            }
        }

        
        $start_time = '02:45:00';
        $end_time = '10:45:00';
        $start_hour = round($start_time);
        $end_hour = round($end_time);
        $final_start_hour = $start_hour - $modifier;
        $final_end_hour = $end_hour - $modifier;
        $final_start_time = $final_start_hour.":00:00";
        $final_end_time = $final_end_hour.":00:00";
        $checkin_time = date("H:i:s", strtotime($start_time));
        $checkout_time = date("H:i:s", strtotime($end_time));
		$start_dt = '2017-06-29';
        $end_dt = '2017-06-29';
        $cust_from_date = date('Y-m-d',strtotime($start_dt))."T".date("H:i:s", strtotime($final_start_time));
        $cust_end_date = date('Y-m-d',strtotime($end_dt))."T".date("H:i:s", strtotime($final_end_time));
        $from_date = date('Y-m-d',strtotime($start_dt))."T".date("H:i:s", strtotime($start_time));
        $from_date_final=date('Y-m-d H:i:s', strtotime($from_date) - 60 * 60 * $modifier);
        $end_date = date('Y-m-d',strtotime($end_dt))."T".date("H:i:s", strtotime($end_time));
        $end_date_final=date('Y-m-d H:i:s', strtotime($end_date) - 60 * 60 * $modifier);
        $booking_status_id = 8;
		
		echo '</br>';
		echo '---</br>';
		echo 'Dates: '.$start_dt.' '.$start_time.' & '.$end_dt.' '.$end_time;
		echo '</br>';
		echo '</br>';
		echo 'start_hour -> '.$start_hour;
		echo '</br>';
		echo 'end_hour -> '.$end_hour;
		echo '</br>';
		echo 'final_start_hour -> '.$final_start_hour;
		echo '</br>';
		echo 'final_end_hour -> '.$final_end_hour;
		echo '</br>';
		echo 'final_start_time -> '.$final_start_time;
		echo '</br>';
		echo 'final_end_time -> '.$final_end_time;
		echo '</br>';
		echo 'checkin_time -> '.$checkin_time;
		echo '</br>';
		echo 'checkout_time -> '.$checkout_time;
		echo '</br>';
		echo 'cust_from_date -> '.$cust_from_date;
		echo '</br>';
		echo 'cust_end_date -> '.$cust_end_date;
		echo '</br>';
		echo 'from_date -> '.$from_date;
		echo '</br>';
		echo 'end_date -> '.$end_date;
		echo '</br>';
		echo 'from_date_final -> '.$from_date_final;
		echo '</br>';
		echo 'end_date_final -> '.$end_date_final;
		echo '</br>';
		echo '</br>';
		echo '</br>';
		echo '<span class="label" style="font-size:15px; background-color:#00698c; color:#bfffff">cust from date -> '.$from_date_final.'</span>';
		echo '</br>';
		echo '</br>';
		echo '<span class="label" style="font-size:15px; background-color:#00698c; color:#bfffff">cust end date -> '.$end_date_final.'</span>';

?>

<script>
	
	let unitavaillofflag = 0;
	
	let func = () => {
		unitavaillofflag += 1;
		console.log(unitavaillofflag);
	}
	
	$(window).on('beforeunload', function(){
		// your logic here
		//alert('test - '+unitavaillofflag);		
	});
	
	/*function warning(unitavaillofflag)
    {
	  if(parseInt(unitavaillofflag) >= 1 ){
		  
		  alert("Clicked - "+unitavaillofflag);
	  }
	   else{
		   
		    alert("Not Clicked - "+unitavaillofflag);
	   }
	   
    }
    
	window.onbeforeunload = warning(unitavaillofflag);
	
	/* Method 2 */
	/*if (window.performance) {
	  console.info("window.performance work's fine on this browser");
	}
	
	if (performance.navigation.type == 1) {
		console.info( "This page is reloaded"+unitavaillofflag);
	} else {
		console.info( "This page is not reloaded");
	}*/
	
	// Method 3
	
	$(window).load(function () {
	 //if IsRefresh cookie exists
	 var IsRefresh = getCookie("IsRefresh");
	 //alert(IsRefresh);
	 
	 if (IsRefresh != null && IsRefresh != "") {
		//cookie exists then you refreshed this page(F5, reload button or right click and reload)
		//SOME CODE
		DeleteCookie("IsRefresh");
	 }
	 else {
		//cookie doesnt exists then you landed on this page
		//SOME CODE
		setCookie("IsRefresh", unitavaillofflag, 1);
	 }
	})
	
	    function setCookie(c_name, value, exdays) {
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + exdays);
            var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
            document.cookie = c_name + "=" + c_value;
        }

    function getCookie(c_name) {
        var i, x, y, ARRcookies = document.cookie.split(";");
        for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
            x = x.replace(/^\s+|\s+$/g, "");
            if (x == c_name) {
                return unescape(y);
            }
        }
    }

    function DeleteCookie(name) {
            document.cookie = name + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
        }
		
	

</script>