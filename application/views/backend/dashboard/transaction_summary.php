
    <div class="portlet light borderd">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-th-list"></i> <strong> (ADR) Average Daily Rate Report </strong> </div>
        <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            
            <div class="col-md-8">
              <?php

	                            $form = array(
	                                'class'     => 'form-inline ftop',
	                                'id'        => 'form_date',
	                                'method'    => 'post'
	                            );

	                            echo form_open_multipart('dashboard/get_booking_report_by_date',$form);

	                            ?>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
              </div>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
              </div>
			  
			  <div class="form-group">
              <button onclick="get_val();" class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search" aria-hidden="true"></i></button>
			  </div>
			 
                  
              <?php form_close(); ?>
            </div>
            <script type="text/javascript">
				function check_sub(){
				   document.getElementById('form_date').submit();
				}
			</script>
            
          </div>
        </div>
		
	<p class="font-green-sharp">*This report is Considering the Stay Dates to count bookings</p>		      
				   
	<div id="table1">		
       <table class="table table-striped table-bordered table-hover" id="sample_1">
        <!--<table class="table table-striped">-->
          <thead>
            <tr>
              
			  <!--<th> Select </th>-->
              
              
              <th width="3%"> # </th>
              <th width="15%"> Date </th>
              <th width="15%"> Units Booked </th>
              <th width="10%"> ADR (RR) </th>
			  <th width="10%"> ADR (RR + Tax) </th>
              <th width="10%"> ADR (RR + MP) </th>
              <th width="10%"> ADR (RR + MP + Tax) </th>
              
             
            </tr>
          </thead>
          <tbody>
            <?php 
			
			if(isset($bookingDate) && $bookingDate){
					$j=0;
					$p=0;
					$p1=0;
					$check = '';
					
					$today = new DateTime('now');
					
			//print_r($bookingSource);
			foreach($bookingDate as $book){ 
									
					$j++;
					$stl = '';
					$stl2 = '';
					
					if(strtotime($book->date) == strtotime(date("Y-m-d"))){
						
						$stl = 'color:#04CC58; font-weight:600;';
						$stl2 = 'background-color:#F4FFEC;';
						//echo date("Y-m-d").' - '.$book->date;
					}		
			
			?>
         
									
          <tr style="<?php echo $stl2;?>">
		  
            <td>  
				  <?php 
					echo $j;
				  ?>
			 </td>
			
			 <td>  
				  <?php 
					echo '<span style="font-size:14px;'.$stl.'">'.date("l jS F Y",strtotime($book->date)).'</span>';				
				  ?>
			 </td>
			 
			 <td> 
				
				<?php 

					$bookingGrp = $this->dashboard_model->getBookingGrpWithin_byDate($book->date);
				
					
					if(isset($bookingGrp) && $bookingGrp){
						foreach($bookingGrp as $bookingGrp){
							$grp = $bookingGrp->grp;					
						}
					}
					else
						$grp = 0;
					
					if($p == $bookingGrp->count || $j == 1)
						$p1 = '-';
					else if($p > $bookingGrp->count)
						$p1 = '<i style="color:#FF5B5B;" class="fa fa-arrow-down" aria-hidden="true"></i>';
					else
						$p1 = '<i style="color:#30AE72;" class="fa fa-arrow-up" aria-hidden="true"></i>';
					
			
					$p = $bookingGrp->count;
					
					
					echo '<a style="font-size:17px; font-weight:600;">'.$bookingGrp->count.'</a> <span style="color:#AAAAAA;">Unit</span>';
					echo ' | <span style="">'.number_format($bookingGrp->count/60*100, 2).'</span> <span style="color:#AAAAAA;">%</span>';
					echo ' | <span style="">'.$p1.'</span>';
					
					
					//echo '<br/>Date: '.$book->date;
					echo '<br/> (<span style="color:#3D4B76;">'.($bookingGrp->count - $grp);
					echo ' </span> / <span style="color:#F8681A;">'.($grp).'</span>)';
					
				  ?>
			 </td>
			 
			<td>
				
				
									<?php 
													$adr1 = 0;
													$adr2 = 0;
													$adr3 = 0;
													$adr4 = 0;
										$BookingAmt = $this->dashboard_model->getBookingAmt_byDate($book->date);
												
										if(isset($BookingAmt) && $BookingAmt){
											foreach($BookingAmt as $BookingAmt){
												
													if($p != 0){
														
														$adr1 = ($BookingAmt->room_rent)/$p;
														$adr2 = ($BookingAmt->room_rent + $BookingAmt->room_tax)/$p;
														$adr3 = ($BookingAmt->room_rent + $BookingAmt->meal)/$p;
														$adr4 = ($BookingAmt->room_rent + $BookingAmt->room_tax + $BookingAmt->meal + $BookingAmt->meal_tax)/$p;
													}
												
													echo number_format($adr1, 2);
													//echo '<br/> '.$book->date;
											}
										}
									?>
              
			  
			</td>
			
			<td>
				
				
									<?php 
											echo number_format($adr2, 2);
									?>
                                            
			  
			</td>
			
			<td>
				
				<?php
					echo number_format($adr3, 2);
				?>
				
			</td>
			
			<td>
				<?php
					echo number_format($adr4, 2);
				?>
			
			</td>         
 
		
          </tr>
		   <input type="hidden" id="item_no" value="<?php //echo $item_no;?>"> </input>
		   
			
          
          <?php  
			}}
		  ?>
         
            </tbody>
          
        </table>
      
    </div>
    </div>
    </div>

<script>

   function fetch_data(val){
	   alert()
	   $.ajax({
                
                url: "<?php echo base_url()?>dashboard/get_no_tax_booking",
				success:function(data)
                { 
                   $('#table1').html(data);
				   $('#dataTable2').dataTable( {
						"pageLength": 10 
					} );
                 }
            });
   }
   
					
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            //alert(id);
		$.ajax({
                
                url: "<?php echo base_url()?>dashboard/soft_delete_booking?id="+id,
				type:"POST",
                data:{id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });

        });
    }
	
function download_pdf(booking_id) {
	
	
	
	//alert(booking_id);
	$("#dwn_link").attr("href", "<?php echo base_url();?>bookings/pdf_generate?booking_id=" + booking_id);
	var f = $("#f");
            $.post(f.attr("action"), f.serialize(), function (result) {
            
                close(eval(result));
            });
	 //return false;
	
}
	
	
</script> 

<script>

$( document ).ready(function() {
	$("#select2_sample_modal_5").select2({
		
	/*	data: [
		 <?php $admins = $this->dashboard_model->fetch_admin();
					foreach($admins as $adm){?>
    {
      id: '<?php echo $adm->admin_id; ?>',
      text: '<?php echo $adm->admin_first_name." ".$adm->admin_last_name; ?>'
    },
	<?php } ?>
	]*/
		 
          /* tags: [ <?php $admins = $this->dashboard_model->fetch_admin();
					foreach($admins as $adm)
					{echo '"'.$adm->admin_id.'",';}?>]*/
        });
});

function get_val()
{
	$("#admin").val($("#select2_sample_modal_5").val());
	//alert($("#admin").val());
}
</script>
<!-- END CONTENT --> 
