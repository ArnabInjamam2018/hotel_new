<table id="logo-name" >
	<?php $hotel_name= $this->dashboard_model->get_hotel($this->session->userdata('user_hotel')); ?>
    <tr>
      <td align="left" valign="middle"><img src="<?php echo base_url();?>upload/hotel/<?php echo $hotel_name->hotel_logo_images_thumb;?>" alt="logo"/></td>
		<td align="right" valign="middle"><?php echo "<strong style='font-size:18px;'>".$hotel_name->hotel_name.'</font></strong>'?></td>
        </tr>		
    
    <tr>
      <td width="100%" colspan="2"><hr style="background: #00C5CD; border: none; height: 1px; margin:10px 0;"></td>
    </tr>
    <tr><td colspan="2"><strong>Date:</strong> <?php echo date('D-M-Y'); ?></td></tr>
    <tr>
      <td width="100%" colspan="2">&nbsp;</td>
    </tr>
</table>
<div class="row">
  <div class="col-md-12 "> 
    
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption"> <i class="glyphicon glyphicon-bed"></i>Monthly Transaction Summary Report </div>
        <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body datatab">
        <div class="table-toolbar">
          <div class="row">
            
          </div>
        </div>
        <table class="table table-striped table-bordered table-hover " id="sample_2">
          <thead>
            <tr>
              <th align="center">#</th>
              <th align="center">Month</th>
              <th align="center">Day</th>
              <th align="center">count</th>
			  <th align="center">Type</th>
			  <th align="center">Payment Mode</th>
			 <!-- <th align="center">Profit Center</th>-->
              <th align="center">Income</th>
              <th align="center">Expanse</th>
              
            </tr>
          </thead>
          <tbody>
            <tr>
              <?php
			  
					$a_date = date("Y-01-01");
                    $maxDays=date('t');
					$income=0;
					$start_date=date("Y-01-01");
					$end_date=date('Y-01-t');
			     
                
                       
						$dailytransaction=$this->dashboard_model->transaction_report($start_date,$end_date);
						
							if(isset($dailytransaction) && $dailytransaction){	
				 		 $bk=0;
				  $bkpay=0;
			      $grefund='';
				  $grefundpay='';
				  $exp=0;
				  $broker='';
				  $brokerpay=0;
				  $tax=0;
				  $taxpay=0;
				  $vendor=0;
				  $vendorpay=0;
				  $cancel=0;
				  $cancelpay=0;
				  $sal=0;
				  $salpay=0;
				  $chanel=0;
				  $chanelpay=0;
				  $comp=0;
				  $comppay=0;
				  $misc=0;
				  $miscpay=0;
				  $misc_in=0;
				  $misc_inpay=0;
				  $restr=0;
				  $restrpay=0;
				  $Mservice=0;
				  $Mservicepay=0;
				  $Guest='';
				  $Guestpay=0;
				  $lap=0;
				  $lappay=0;
				  $spp=0;
				  $spppay=0;
				  $contra=0;
				  $contrapay=0;
				  $charity=0;
				  $charitypay=0;	
				  $count=0;				  
				  $b=0;				  
				  $g=0;	
				  $bro=0;	
				  $t=0;				  
				  $v=0;	
					$c=0;
					$ch=0;
					$s=0;
					$co=0;
					$m=0;
					$mi=0;
					$r=0;
					$ms=0;
					$gp=0;
					$l=0;
					$sp=0;
					$con=0;
					$cr=0;
					$ca=0;
					$cas=0;
					$car=0;
					$dfr=0;
					$ew=0;
					$fu=0;
					$cq=0;
					$Pc=0;
							
							
				  foreach($dailytransaction as $tranc){		
								
								$pCenter=$tranc->p_center;
								if($pCenter==$tranc->p_center){
									$Pc=$Pc+$tranc->p_c;
								$pCenter." ".$Pc;
								}		



									
							     $count=$count+$tranc->count; 
								 
								 $status=$tranc->t_payment_mode;
							     $type=$tranc->hotel_transaction_type_name; 
								 $Inc_Exp=$tranc->hotel_transaction_type_cat; 
							   
							    if($type=='Booking payment'){
								   $bk=$type;
								   $bkpay=$bkpay+$tranc->total;
								  $b=$b+$tranc->count;
							   }else if($type=='Guest refund'){
								   $grefund=$type;
								   $grefundpay=$grefundpay+$tranc->total;
								   $g+=$tranc->count; 
							   }else if($type=='Broker payment'){
								   $broker=$type;
								   $brokerpay=$brokerpay+$tranc->total;
								   $bro+=$tranc->count; 
							   }else if($type=='Tax paymemnt'){
								   $tax=$type;
								   $taxpay=$taxpay+$tranc->total;
								    $t+=$tranc->count;
							   }else if($type=='Vendor payment'){
								   $vendor=$type;
								   $vendorpay=$vendorpay+$tranc->total;
								    $v+=$tranc->count;
							   }else if($type=='Cancellation Fees'){
								   $cancel=$type;
								   $cancelpay=$cancelpay+$tranc->total;
								    $c+=$tranc->count;
							   }else if($type=='Salary payment'){
								   $sal=$type;
								   $salpay=$salpay+$tranc->total;
								    $s+=$tranc->count;
							   }else if($type=='Channel payment'){
								   $chanel=$type;
								   $chanelpay=$chanelpay+$tranc->total;
								    $ch+=$tranc->count;
							   }else if($type=='Compliance Payment'){
								   $comp=$type;
								   $comppay=$comppay+$tranc->total;
								    $co+=$tranc->count;
							   }else if($type=='Misc Payment'){
								   $misc=$type;
								   $miscpay=$miscpay+$tranc->total;
								    $m+=$tranc->count;
							   }else if($type=='Misc Income'){
								   $misc_in=$type;
								   $misc_inpay=$misc_inpay+$tranc->total;
								    $mi+=$tranc->count;
							   }else if($type=='Resturant payment'){
								   $restr=$type;
								   $restrpay=$restrpay+$tranc->total;
								    $r+=$tranc->count;
							   }else if($type=='Misc service payment'){
								   $Mservice=$type;
								   $Mservicepay=$Mservicepay+$tranc->total;
								    $ms+=$tranc->count;
							   }else if($type=='Guest Payment'){
								   $Guest=$type;
								   $Guestpay=$Guestpay+$tranc->total;
								    $gp+=$tranc->count;
							   }else if($type=='Local Authority Payment'){
								   $lap=$type;
								   $lappay=$lappay+$tranc->total;
								    $l+=$tranc->count;
							   }else if($type=='Service Provider Payment'){
								   $spp=$type;
								   $spppay=$spppay+$tranc->total;
								    $sp+=$tranc->count;
							   }else if($type=='Contractor Payment'){
								   $contra=$type;
								   $contrapay=$contrapay+$tranc->total;
								    $con+=$tranc->count;
							   }else if($type=='Charity'){
								   $charity=$type;
								   $charitypay=$charitypay+$tranc->total;
								    $cr+=$tranc->count;
							   }
								if($Inc_Exp=='Income'){
								$income=$income+$tranc->total;
								}else{
									$exp=$exp+$tranc->total;
								}
								
				  if($status=='cash' || $status=='Cash'){
								$cash=$status;
								$cas++;
							}
							if($status=='cards'){
								$card=$status;
								$car++;
							}		
						if($status=='fund'){
								$fund=$status;
								$fu++;
							}
							if($status=='cheque'){
								$cheque=$status;
								$cq++;
							}
							if($status=='draft'){
								$draft=$status;
								$dfr++;
							}	
							if($status=='ewallet'){
								$ewallet=$status;
								$ew++;
							}	
							
							}
							//$gross=$rmAmt+$rrtax+$mealPlan+$mpTax;
							}
							
					
						?>
              <td align="center"><?php  echo 1;?></td>
              <td align="center"> <a href="<?php echo base_url();?>reports/daily_transaction_summary?m=<?php echo 1?>"> <?php  echo date("F", strtotime($a_date)); ?> </a> <?php echo " ".date("Y", strtotime($a_date))?></td>
              <td align="center"><?php echo date("t", strtotime($a_date)); ?></td>
              <td align="center"><?php //Room Count
										echo $count;?></td>
              <td align="center"><?php
					//Occupancy    
									if(isset($bkpay) && $bkpay){
									echo $bk." :".$bkpay."(".$b. ")"."</br>";
									}
									if(isset($grefundpay) && $grefundpay){
									echo $grefund." :".$grefundpay."(" .$g. ")"."</br>";
									}
									if(isset($brokerpay) && $brokerpay){
									echo $broker." :".$brokerpay."(" .$bro. ")"."</br>";
									}
									if(isset($taxpay) && $taxpay){
									echo $tax." :".$taxpay."(" .$t. ")"."</br>";
									}
									if(isset($vendorpay) && $vendorpay){
									echo $vendor." :".$vendorpay."(" .$v. ")"."</br>";
									}
									if(isset($cancelpay) && $cancelpay){
									echo $cancel." :".$cancelpay."(" .$c. ")"."</br>";
									}
									if(isset($salpay) && $salpay){
									echo $sal." :".$salpay."(" .$s. ")"."</br>";
									}
									if(isset($chanelpay) && $chanelpay){
									echo $chanel." :".$chanelpay."(" .$ch. ")"."</br>";
									}
									if(isset($comppay) && $comppay){
									echo $comp." :".$comppay."(" .$co. ")"."</br>";
									}
									if(isset($miscpay) && $miscpay){
									echo $misc." :".$miscpay."(" .$m. ")"."</br>";
									}
									if(isset($misc_inpay) && $misc_inpay){
									echo $misc_in." :".$misc_inpay."(" .$mi. ")"."</br>";
									}
									if(isset($restrpay) && $restrpay){
									echo $restr." :".$restrpay."(" .$r. ")"."</br>";
									}
									if(isset($Mservicepay) && $Mservicepay){
									echo $Mservice." :".$Mservicepay."(" .$ms. ")"."</br>";
									}
									if(isset($Guestpay) && $Guestpay){
									echo $Guest." :".$Guestpay."(" .$gp. ")"."</br>";
									}
									if(isset($lappay) && $lappay){
									echo $lap." :".$lappay."(" .$l. ")"."</br>";
									}
									if(isset($spppay) && $spppay){
									echo $spp." :".$spppay."(" .$sp. ")"."</br>";
									}
									if(isset($contrapay) && $contrapay){
									echo $contra." :".$contrapay."(" .$con++. ")"."</br>";
									}if(isset($charitypay) && $charitypay){
									echo $charit." :".$charitypay."(" .$ct. ")"."</br>";
									}
									?></td>
									<td align="center"><?php 
						
					if(isset($cash)) { echo $cash.": "."( ".$cas." )"."</br>";
					}
					if(isset($card) && $card){
						echo $card.": "."( ".$car." )"."</br>";
					}
					if(isset($fund) && $fund){
						echo $fund.": "."( ".$fu." )"."</br>";
					}
					if(isset($cheque) && $cheque){
						echo $cheque.": "."( ".$cq." )"."</br>";
					}
					if(isset($draft) && $draft){
						echo $draft.": "."( ".$dfr." )"."</br>";
					}
					if(isset($ewallet) && $ewallet){
						echo $ewallet.": "."( ".$ew." )"."</br>";
					}
						?></td>
						
					
              <td align="center"><?php 
					if(isset($income) && $income>0) {echo $income;}else{
						echo "0";
					}
						?></td> 
						 
						
              <td align="center"><?php
					if(isset($exp) && $exp>0) {echo $exp;}else{
						echo "0";
					}
			?></td>
				
				

            </tr>
			
			 <tr>
              <?php   
					$a_date = date("Y-02-01");
                    $maxDays=date('t');
					$income=0;
					$start_date=date("Y-02-01");
					$end_date=date('Y-02-t');
			     
					
                
                       
						$dailytransaction=$this->dashboard_model->transaction_report($start_date,$end_date);
						
							if(isset($dailytransaction) && $dailytransaction){	
				  $bk=0;
				  $bkpay=0;
			      $grefund='';
				  $grefundpay='';
				  $exp=0;
				  $broker='';
				  $brokerpay=0;
				  $tax=0;
				  $taxpay=0;
				  $vendor=0;
				  $vendorpay=0;
				  $cancel=0;
				  $cancelpay=0;
				  $sal=0;
				  $salpay=0;
				  $chanel=0;
				  $chanelpay=0;
				  $comp=0;
				  $comppay=0;
				  $misc=0;
				  $miscpay=0;
				  $misc_in=0;
				  $misc_inpay=0;
				  $restr=0;
				  $restrpay=0;
				  $Mservice=0;
				  $Mservicepay=0;
				  $Guest='';
				  $Guestpay=0;
				  $lap=0;
				  $lappay=0;
				  $spp=0;
				  $spppay=0;
				  $contra=0;
				  $contrapay=0;
				  $charity=0;
				  $charitypay=0;	
				  $count=0;				  
				  $b=0;				  
				  $g=0;	
				  $bro=0;	
				  $t=0;				  
				  $v=0;	
					$c=0;
					$ch=0;
					$s=0;
					$co=0;
					$m=0;
					$mi=0;
					$r=0;
					$ms=0;
					$gp=0;
					$l=0;
					$sp=0;
					$con=0;
					$cr=0;
					$ca=0;
					$cas=0;
					$car=0;
					$dfr=0;
					$ew=0;
					$fu=0;
					$cq=0;		
							
								
				  foreach($dailytransaction as $tranc){		
									 
							     $count=$count+$tranc->count; 
								 
								 $status=$tranc->t_payment_mode;
							     $type=$tranc->hotel_transaction_type_name; 
								 $Inc_Exp=$tranc->hotel_transaction_type_cat; 
							   
							    if($type=='Booking payment'){
								   $bk=$type;
								   $bkpay=$bkpay+$tranc->total;
								  $b=$b+$tranc->count;
							   }else if($type=='Guest refund'){
								   $grefund=$type;
								   $grefundpay=$grefundpay+$tranc->total;
								   $g+=$tranc->count; 
							   }else if($type=='Broker payment'){
								   $broker=$type;
								   $brokerpay=$brokerpay+$tranc->total;
								   $bro+=$tranc->count; 
							   }else if($type=='Tax paymemnt'){
								   $tax=$type;
								   $taxpay=$taxpay+$tranc->total;
								    $t+=$tranc->count;
							   }else if($type=='Vendor payment'){
								   $vendor=$type;
								   $vendorpay=$vendorpay+$tranc->total;
								    $v+=$tranc->count;
							   }else if($type=='Cancellation Fees'){
								   $cancel=$type;
								   $cancelpay=$cancelpay+$tranc->total;
								    $c+=$tranc->count;
							   }else if($type=='Salary payment'){
								   $sal=$type;
								   $salpay=$salpay+$tranc->total;
								    $s+=$tranc->count;
							   }else if($type=='Channel payment'){
								   $chanel=$type;
								   $chanelpay=$chanelpay+$tranc->total;
								    $ch+=$tranc->count;
							   }else if($type=='Compliance Payment'){
								   $comp=$type;
								   $comppay=$comppay+$tranc->total;
								    $co+=$tranc->count;
							   }else if($type=='Misc Payment'){
								   $misc=$type;
								   $miscpay=$miscpay+$tranc->total;
								    $m+=$tranc->count;
							   }else if($type=='Misc Income'){
								   $misc_in=$type;
								   $misc_inpay=$misc_inpay+$tranc->total;
								    $mi+=$tranc->count;
							   }else if($type=='Resturant payment'){
								   $restr=$type;
								   $restrpay=$restrpay+$tranc->total;
								    $r+=$tranc->count;
							   }else if($type=='Misc service payment'){
								   $Mservice=$type;
								   $Mservicepay=$Mservicepay+$tranc->total;
								    $ms+=$tranc->count;
							   }else if($type=='Guest Payment'){
								   $Guest=$type;
								   $Guestpay=$Guestpay+$tranc->total;
								    $gp+=$tranc->count;
							   }else if($type=='Local Authority Payment'){
								   $lap=$type;
								   $lappay=$lappay+$tranc->total;
								    $l+=$tranc->count;
							   }else if($type=='Service Provider Payment'){
								   $spp=$type;
								   $spppay=$spppay+$tranc->total;
								    $sp+=$tranc->count;
							   }else if($type=='Contractor Payment'){
								   $contra=$type;
								   $contrapay=$contrapay+$tranc->total;
								    $con+=$tranc->count;
							   }else if($type=='Charity'){
								   $charity=$type;
								   $charitypay=$charitypay+$tranc->total;
								    $cr+=$tranc->count;
							   }
								if($Inc_Exp=='Income'){
								$income=$income+$tranc->total;
								}else{
									$exp=$exp+$tranc->total;
								}
								
				  if($status=='cash' || $status=='Cash'){
								$cash=$status;
								$cas++;
							}
							if($status=='cards'){
								$card=$status;
								$car++;
							}		
						if($status=='fund'){
								$fund=$status;
								$fu++;
							}
							if($status=='cheque'){
								$cheque=$status;
								$cq++;
							}
							if($status=='draft'){
								$draft=$status;
								$dfr++;
							}	
							if($status=='ewallet'){
								$ewallet=$status;
								$ew++;
							}	
							
							}
							//$gross=$rmAmt+$rrtax+$mealPlan+$mpTax;
							}
							
					
						?>
              <td align="center"><?php  echo 2;?></td>
              <td align="center"> <a href="<?php echo base_url();?>reports/daily_transaction_summary?m=<?php echo 2?>"> <?php  echo date("F", strtotime($a_date)); ?> </a> <?php echo " ".date("Y", strtotime($a_date))?></td>
              <td align="center"><?php echo date("t", strtotime($a_date)); ?></td>
              <td align="center"><?php //Room Count
										echo $count;?></td>
              <td align="center"><?php
					//Occupancy    
									if(isset($bkpay) && $bkpay){
									echo $bk." :".$bkpay."(".$b. ")"."</br>";
									}
									if(isset($grefundpay) && $grefundpay){
									echo $grefund." :".$grefundpay."(" .$g. ")"."</br>";
									}
									if(isset($brokerpay) && $brokerpay){
									echo $broker." :".$brokerpay."(" .$bro. ")"."</br>";
									}
									if(isset($taxpay) && $taxpay){
									echo $tax." :".$taxpay."(" .$t. ")"."</br>";
									}
									if(isset($vendorpay) && $vendorpay){
									echo $vendor." :".$vendorpay."(" .$v. ")"."</br>";
									}
									if(isset($cancelpay) && $cancelpay){
									echo $cancel." :".$cancelpay."(" .$c. ")"."</br>";
									}
									if(isset($salpay) && $salpay){
									echo $sal." :".$salpay."(" .$s. ")"."</br>";
									}
									if(isset($chanelpay) && $chanelpay){
									echo $chanel." :".$chanelpay."(" .$ch. ")"."</br>";
									}
									if(isset($comppay) && $comppay){
									echo $comp." :".$comppay."(" .$co. ")"."</br>";
									}
									if(isset($miscpay) && $miscpay){
									echo $misc." :".$miscpay."(" .$m. ")"."</br>";
									}
									if(isset($misc_inpay) && $misc_inpay){
									echo $misc_in." :".$misc_inpay."(" .$mi. ")"."</br>";
									}
									if(isset($restrpay) && $restrpay){
									echo $restr." :".$restrpay."(" .$r. ")"."</br>";
									}
									if(isset($Mservicepay) && $Mservicepay){
									echo $Mservice." :".$Mservicepay."(" .$ms. ")"."</br>";
									}
									if(isset($Guestpay) && $Guestpay){
									echo $Guest." :".$Guestpay."(" .$gp. ")"."</br>";
									}
									if(isset($lappay) && $lappay){
									echo $lap." :".$lappay."(" .$l. ")"."</br>";
									}
									if(isset($spppay) && $spppay){
									echo $spp." :".$spppay."(" .$sp. ")"."</br>";
									}
									if(isset($contrapay) && $contrapay){
									echo $contra." :".$contrapay."(" .$con++. ")"."</br>";
									}if(isset($charitypay) && $charitypay){
									echo $charit." :".$charitypay."(" .$ct. ")"."</br>";
									}
									?></td>
									<td align="center"><?php 
						
					if(isset($cash)) { echo $cash.": "."( ".$cas." )"."</br>";
					}
					if(isset($card) && $card){
						echo $card.": "."( ".$car." )"."</br>";
					}
					if(isset($fund) && $fund){
						echo $fund.": "."( ".$fu." )"."</br>";
					}
					if(isset($cheque) && $cheque){
						echo $cheque.": "."( ".$cq." )"."</br>";
					}
					if(isset($draft) && $draft){
						echo $draft.": "."( ".$dfr." )"."</br>";
					}
					if(isset($ewallet) && $ewallet){
						echo $ewallet.": "."( ".$ew." )"."</br>";
					}
						?></td>
              <td align="center"><?php 
					if(isset($income) && $income>0) {echo $income;}else{
						echo "0";
					}
						?></td> 
						
              <td align="center"><?php
					if(isset($exp) && $exp>0) {echo $exp;}else{
						echo "0";
					}
			?></td>
				
            </tr>
			
			<tr>
              <?php  
				$a_date = date("Y-03-01");
                 $income=0;
					$start_date=date("Y-03-01");
					$end_date=date('Y-03-t');
			     
					
                
                       
						$dailytransaction=$this->dashboard_model->transaction_report($start_date,$end_date);
						
							if(isset($dailytransaction) && $dailytransaction){	
				  $bk=0;
				  $bkpay=0;
			      $grefund='';
				  $grefundpay='';
				  $exp=0;
				  $broker='';
				  $brokerpay=0;
				  $tax=0;
				  $taxpay=0;
				  $vendor=0;
				  $vendorpay=0;
				  $cancel=0;
				  $cancelpay=0;
				  $sal=0;
				  $salpay=0;
				  $chanel=0;
				  $chanelpay=0;
				  $comp=0;
				  $comppay=0;
				  $misc=0;
				  $miscpay=0;
				  $misc_in=0;
				  $misc_inpay=0;
				  $restr=0;
				  $restrpay=0;
				  $Mservice=0;
				  $Mservicepay=0;
				  $Guest='';
				  $Guestpay=0;
				  $lap=0;
				  $lappay=0;
				  $spp=0;
				  $spppay=0;
				  $contra=0;
				  $contrapay=0;
				  $charity=0;
				  $charitypay=0;	
				  $count=0;				  
				  $b=0;				  
				  $g=0;	
				  $bro=0;	
				  $t=0;				  
				  $v=0;	
					$c=0;
					$ch=0;
					$s=0;
					$co=0;
					$m=0;
					$mi=0;
					$r=0;
					$ms=0;
					$gp=0;
					$l=0;
					$sp=0;
					$con=0;
					$cr=0;
					$ca=0;
					$cas=0;
					$car=0;
					$dfr=0;
					$ew=0;
					$fu=0;
					$cq=0;		
							
								
				  foreach($dailytransaction as $tranc){		
									 
							     $count=$count+$tranc->count; 
								 
								 $status=$tranc->t_payment_mode;
							     $type=$tranc->hotel_transaction_type_name; 
								 $Inc_Exp=$tranc->hotel_transaction_type_cat; 
							   
							    if($type=='Booking payment'){
								   $bk=$type;
								   $bkpay=$bkpay+$tranc->total;
								  $b=$b+$tranc->count;
							   }else if($type=='Guest refund'){
								   $grefund=$type;
								   $grefundpay=$grefundpay+$tranc->total;
								   $g+=$tranc->count; 
							   }else if($type=='Broker payment'){
								   $broker=$type;
								   $brokerpay=$brokerpay+$tranc->total;
								   $bro+=$tranc->count; 
							   }else if($type=='Tax paymemnt'){
								   $tax=$type;
								   $taxpay=$taxpay+$tranc->total;
								    $t+=$tranc->count;
							   }else if($type=='Vendor payment'){
								   $vendor=$type;
								   $vendorpay=$vendorpay+$tranc->total;
								    $v+=$tranc->count;
							   }else if($type=='Cancellation Fees'){
								   $cancel=$type;
								   $cancelpay=$cancelpay+$tranc->total;
								    $c+=$tranc->count;
							   }else if($type=='Salary payment'){
								   $sal=$type;
								   $salpay=$salpay+$tranc->total;
								    $s+=$tranc->count;
							   }else if($type=='Channel payment'){
								   $chanel=$type;
								   $chanelpay=$chanelpay+$tranc->total;
								    $ch+=$tranc->count;
							   }else if($type=='Compliance Payment'){
								   $comp=$type;
								   $comppay=$comppay+$tranc->total;
								    $co+=$tranc->count;
							   }else if($type=='Misc Payment'){
								   $misc=$type;
								   $miscpay=$miscpay+$tranc->total;
								    $m+=$tranc->count;
							   }else if($type=='Misc Income'){
								   $misc_in=$type;
								   $misc_inpay=$misc_inpay+$tranc->total;
								    $mi+=$tranc->count;
							   }else if($type=='Resturant payment'){
								   $restr=$type;
								   $restrpay=$restrpay+$tranc->total;
								    $r+=$tranc->count;
							   }else if($type=='Misc service payment'){
								   $Mservice=$type;
								   $Mservicepay=$Mservicepay+$tranc->total;
								    $ms+=$tranc->count;
							   }else if($type=='Guest Payment'){
								   $Guest=$type;
								   $Guestpay=$Guestpay+$tranc->total;
								    $gp+=$tranc->count;
							   }else if($type=='Local Authority Payment'){
								   $lap=$type;
								   $lappay=$lappay+$tranc->total;
								    $l+=$tranc->count;
							   }else if($type=='Service Provider Payment'){
								   $spp=$type;
								   $spppay=$spppay+$tranc->total;
								    $sp+=$tranc->count;
							   }else if($type=='Contractor Payment'){
								   $contra=$type;
								   $contrapay=$contrapay+$tranc->total;
								    $con+=$tranc->count;
							   }else if($type=='Charity'){
								   $charity=$type;
								   $charitypay=$charitypay+$tranc->total;
								    $cr+=$tranc->count;
							   }
								if($Inc_Exp=='Income'){
								$income=$income+$tranc->total;
								}else{
									$exp=$exp+$tranc->total;
								}
								
				  if($status=='cash' || $status=='Cash'){
								$cash=$status;
								$cas++;
							}
							if($status=='cards'){
								$card=$status;
								$car++;
							}		
						if($status=='fund'){
								$fund=$status;
								$fu++;
							}
							if($status=='cheque'){
								$cheque=$status;
								$cq++;
							}
							if($status=='draft'){
								$draft=$status;
								$dfr++;
							}	
							if($status=='ewallet'){
								$ewallet=$status;
								$ew++;
							}	
							
							}
							//$gross=$rmAmt+$rrtax+$mealPlan+$mpTax;
							}
							
					
						?>
              <td align="center"><?php  echo 3;?></td>
              <td align="center"> <a href="<?php echo base_url();?>reports/daily_transaction_summary?m=<?php echo 3?>"> <?php  echo date("F", strtotime($a_date)); ?> </a> <?php echo " ".date("Y", strtotime($a_date))?></td>
              <td align="center"><?php echo date("t", strtotime($a_date)); ?></td>
              <td align="center"><?php //Room Count
										echo $count;?></td>
              <td align="center"><?php
					//Occupancy    
									if(isset($bkpay) && $bkpay){
									echo $bk." :".$bkpay."(".$b. ")"."</br>";
									}
									if(isset($grefundpay) && $grefundpay){
									echo $grefund." :".$grefundpay."(" .$g. ")"."</br>";
									}
									if(isset($brokerpay) && $brokerpay){
									echo $broker." :".$brokerpay."(" .$bro. ")"."</br>";
									}
									if(isset($taxpay) && $taxpay){
									echo $tax." :".$taxpay."(" .$t. ")"."</br>";
									}
									if(isset($vendorpay) && $vendorpay){
									echo $vendor." :".$vendorpay."(" .$v. ")"."</br>";
									}
									if(isset($cancelpay) && $cancelpay){
									echo $cancel." :".$cancelpay."(" .$c. ")"."</br>";
									}
									if(isset($salpay) && $salpay){
									echo $sal." :".$salpay."(" .$s. ")"."</br>";
									}
									if(isset($chanelpay) && $chanelpay){
									echo $chanel." :".$chanelpay."(" .$ch. ")"."</br>";
									}
									if(isset($comppay) && $comppay){
									echo $comp." :".$comppay."(" .$co. ")"."</br>";
									}
									if(isset($miscpay) && $miscpay){
									echo $misc." :".$miscpay."(" .$m. ")"."</br>";
									}
									if(isset($misc_inpay) && $misc_inpay){
									echo $misc_in." :".$misc_inpay."(" .$mi. ")"."</br>";
									}
									if(isset($restrpay) && $restrpay){
									echo $restr." :".$restrpay."(" .$r. ")"."</br>";
									}
									if(isset($Mservicepay) && $Mservicepay){
									echo $Mservice." :".$Mservicepay."(" .$ms. ")"."</br>";
									}
									if(isset($Guestpay) && $Guestpay){
									echo $Guest." :".$Guestpay."(" .$gp. ")"."</br>";
									}
									if(isset($lappay) && $lappay){
									echo $lap." :".$lappay."(" .$l. ")"."</br>";
									}
									if(isset($spppay) && $spppay){
									echo $spp." :".$spppay."(" .$sp. ")"."</br>";
									}
									if(isset($contrapay) && $contrapay){
									echo $contra." :".$contrapay."(" .$con++. ")"."</br>";
									}if(isset($charitypay) && $charitypay){
									echo $charit." :".$charitypay."(" .$ct. ")"."</br>";
									}
									?></td>
									<td align="center"><?php 
						
					if(isset($cash)) { echo $cash.": "."( ".$cas." )"."</br>";
					}
					if(isset($card) && $card){
						echo $card.": "."( ".$car." )"."</br>";
					}
					if(isset($fund) && $fund){
						echo $fund.": "."( ".$fu." )"."</br>";
					}
					if(isset($cheque) && $cheque){
						echo $cheque.": "."( ".$cq." )"."</br>";
					}
					if(isset($draft) && $draft){
						echo $draft.": "."( ".$dfr." )"."</br>";
					}
					if(isset($ewallet) && $ewallet){
						echo $ewallet.": "."( ".$ew." )"."</br>";
					}
						?></td>
              <td align="center"><?php 
					if(isset($income) && $income>0) {echo $income;}else{
						echo "0";
					}
						?></td> 
						
              <td align="center"><?php
					if(isset($exp) && $exp>0) {echo $exp;}else{
						echo "0";
					}
			?></td>
				
            </tr>
			
			<tr>
              <?php   
				    $a_date = date("Y-04-01");
                    $maxDays=date('t');
					$income=0;
					$start_date=date("Y-04-01");
					$end_date=date('Y-04-t');
			     $dailytransaction=$this->dashboard_model->transaction_report($start_date,$end_date);
						
							if(isset($dailytransaction) && $dailytransaction){	
				  $bk=0;
				  $bkpay=0;
			      $grefund='';
				  $grefundpay='';
				  $exp=0;
				  $broker='';
				  $brokerpay=0;
				  $tax=0;
				  $taxpay=0;
				  $vendor=0;
				  $vendorpay=0;
				  $cancel=0;
				  $cancelpay=0;
				  $sal=0;
				  $salpay=0;
				  $chanel=0;
				  $chanelpay=0;
				  $comp=0;
				  $comppay=0;
				  $misc=0;
				  $miscpay=0;
				  $misc_in=0;
				  $misc_inpay=0;
				  $restr=0;
				  $restrpay=0;
				  $Mservice=0;
				  $Mservicepay=0;
				  $Guest='';
				  $Guestpay=0;
				  $lap=0;
				  $lappay=0;
				  $spp=0;
				  $spppay=0;
				  $contra=0;
				  $contrapay=0;
				  $charity=0;
				  $charitypay=0;	
				  $count=0;				  
				  $b=0;				  
				  $g=0;	
				  $bro=0;	
				  $t=0;				  
				  $v=0;	
					$c=0;
					$ch=0;
					$s=0;
					$co=0;
					$m=0;
					$mi=0;
					$r=0;
					$ms=0;
					$gp=0;
					$l=0;
					$sp=0;
					$con=0;
					$cr=0;
					$ca=0;
					$cas=0;
					$car=0;
					$dfr=0;
					$ew=0;
					$fu=0;
					$cq=0;		
							
								
				  foreach($dailytransaction as $tranc){		
									 
							     $count=$count+$tranc->count; 
								 
								 $status=$tranc->t_payment_mode;
							     $type=$tranc->hotel_transaction_type_name; 
								 $Inc_Exp=$tranc->hotel_transaction_type_cat; 
							   
							    if($type=='Booking payment'){
								   $bk=$type;
								   $bkpay=$bkpay+$tranc->total;
								  $b=$b+$tranc->count;
							   }else if($type=='Guest refund'){
								   $grefund=$type;
								   $grefundpay=$grefundpay+$tranc->total;
								   $g+=$tranc->count; 
							   }else if($type=='Broker payment'){
								   $broker=$type;
								   $brokerpay=$brokerpay+$tranc->total;
								   $bro+=$tranc->count; 
							   }else if($type=='Tax paymemnt'){
								   $tax=$type;
								   $taxpay=$taxpay+$tranc->total;
								    $t+=$tranc->count;
							   }else if($type=='Vendor payment'){
								   $vendor=$type;
								   $vendorpay=$vendorpay+$tranc->total;
								    $v+=$tranc->count;
							   }else if($type=='Cancellation Fees'){
								   $cancel=$type;
								   $cancelpay=$cancelpay+$tranc->total;
								    $c+=$tranc->count;
							   }else if($type=='Salary payment'){
								   $sal=$type;
								   $salpay=$salpay+$tranc->total;
								    $s+=$tranc->count;
							   }else if($type=='Channel payment'){
								   $chanel=$type;
								   $chanelpay=$chanelpay+$tranc->total;
								    $ch+=$tranc->count;
							   }else if($type=='Compliance Payment'){
								   $comp=$type;
								   $comppay=$comppay+$tranc->total;
								    $co+=$tranc->count;
							   }else if($type=='Misc Payment'){
								   $misc=$type;
								   $miscpay=$miscpay+$tranc->total;
								    $m+=$tranc->count;
							   }else if($type=='Misc Income'){
								   $misc_in=$type;
								   $misc_inpay=$misc_inpay+$tranc->total;
								    $mi+=$tranc->count;
							   }else if($type=='Resturant payment'){
								   $restr=$type;
								   $restrpay=$restrpay+$tranc->total;
								    $r+=$tranc->count;
							   }else if($type=='Misc service payment'){
								   $Mservice=$type;
								   $Mservicepay=$Mservicepay+$tranc->total;
								    $ms+=$tranc->count;
							   }else if($type=='Guest Payment'){
								   $Guest=$type;
								   $Guestpay=$Guestpay+$tranc->total;
								    $gp+=$tranc->count;
							   }else if($type=='Local Authority Payment'){
								   $lap=$type;
								   $lappay=$lappay+$tranc->total;
								    $l+=$tranc->count;
							   }else if($type=='Service Provider Payment'){
								   $spp=$type;
								   $spppay=$spppay+$tranc->total;
								    $sp+=$tranc->count;
							   }else if($type=='Contractor Payment'){
								   $contra=$type;
								   $contrapay=$contrapay+$tranc->total;
								    $con+=$tranc->count;
							   }else if($type=='Charity'){
								   $charity=$type;
								   $charitypay=$charitypay+$tranc->total;
								    $cr+=$tranc->count;
							   }
								if($Inc_Exp=='Income'){
								$income=$income+$tranc->total;
								}else{
									$exp=$exp+$tranc->total;
								}
								
				  if($status=='cash' || $status=='Cash'){
								$cash=$status;
								$cas++;
							}
							if($status=='cards'){
								$card=$status;
								$car++;
							}		
						if($status=='fund'){
								$fund=$status;
								$fu++;
							}
							if($status=='cheque'){
								$cheque=$status;
								$cq++;
							}
							if($status=='draft'){
								$draft=$status;
								$dfr++;
							}	
							if($status=='ewallet'){
								$ewallet=$status;
								$ew++;
							}	
							
							}
							//$gross=$rmAmt+$rrtax+$mealPlan+$mpTax;
							}
							
					
						?>
              <td align="center"><?php  echo 4;?></td>
              <td align="center"> <a href="<?php echo base_url();?>reports/daily_transaction_summary?m=<?php echo 4?>"> <?php  echo date("F", strtotime($a_date)); ?> </a> <?php echo " ".date("Y", strtotime($a_date))?></td>
              <td align="center"><?php echo date("t", strtotime($a_date)); ?></td>
              <td align="center"><?php //Room Count
										echo $count;?></td>
              <td align="center"><?php
					//Occupancy    
									if(isset($bkpay) && $bkpay){
									echo $bk." :".$bkpay."(".$b. ")"."</br>";
									}
									if(isset($grefundpay) && $grefundpay){
									echo $grefund." :".$grefundpay."(" .$g. ")"."</br>";
									}
									if(isset($brokerpay) && $brokerpay){
									echo $broker." :".$brokerpay."(" .$bro. ")"."</br>";
									}
									if(isset($taxpay) && $taxpay){
									echo $tax." :".$taxpay."(" .$t. ")"."</br>";
									}
									if(isset($vendorpay) && $vendorpay){
									echo $vendor." :".$vendorpay."(" .$v. ")"."</br>";
									}
									if(isset($cancelpay) && $cancelpay){
									echo $cancel." :".$cancelpay."(" .$c. ")"."</br>";
									}
									if(isset($salpay) && $salpay){
									echo $sal." :".$salpay."(" .$s. ")"."</br>";
									}
									if(isset($chanelpay) && $chanelpay){
									echo $chanel." :".$chanelpay."(" .$ch. ")"."</br>";
									}
									if(isset($comppay) && $comppay){
									echo $comp." :".$comppay."(" .$co. ")"."</br>";
									}
									if(isset($miscpay) && $miscpay){
									echo $misc." :".$miscpay."(" .$m. ")"."</br>";
									}
									if(isset($misc_inpay) && $misc_inpay){
									echo $misc_in." :".$misc_inpay."(" .$mi. ")"."</br>";
									}
									if(isset($restrpay) && $restrpay){
									echo $restr." :".$restrpay."(" .$r. ")"."</br>";
									}
									if(isset($Mservicepay) && $Mservicepay){
									echo $Mservice." :".$Mservicepay."(" .$ms. ")"."</br>";
									}
									if(isset($Guestpay) && $Guestpay){
									echo $Guest." :".$Guestpay."(" .$gp. ")"."</br>";
									}
									if(isset($lappay) && $lappay){
									echo $lap." :".$lappay."(" .$l. ")"."</br>";
									}
									if(isset($spppay) && $spppay){
									echo $spp." :".$spppay."(" .$sp. ")"."</br>";
									}
									if(isset($contrapay) && $contrapay){
									echo $contra." :".$contrapay."(" .$con++. ")"."</br>";
									}if(isset($charitypay) && $charitypay){
									echo $charit." :".$charitypay."(" .$ct. ")"."</br>";
									}
									?></td>
									<td align="center"><?php 
						
					if(isset($cash)) { echo $cash.": "."( ".$cas." )"."</br>";
					}
					if(isset($card) && $card){
						echo $card.": "."( ".$car." )"."</br>";
					}
					if(isset($fund) && $fund){
						echo $fund.": "."( ".$fu." )"."</br>";
					}
					if(isset($cheque) && $cheque){
						echo $cheque.": "."( ".$cq." )"."</br>";
					}
					if(isset($draft) && $draft){
						echo $draft.": "."( ".$dfr." )"."</br>";
					}
					if(isset($ewallet) && $ewallet){
						echo $ewallet.": "."( ".$ew." )"."</br>";
					}
						?></td>
              <td align="center"><?php 
					if(isset($income) && $income>0) {echo $income;}else{
						echo "0";
					}
						?></td> 
						
              <td align="center"><?php
					if(isset($exp) && $exp>0) {echo $exp;}else{
						echo "0";
					}
			?></td>
				
            </tr>
			
			<tr>
              <?php
			   $a_date = date("Y-05-01");
                    $maxDays=date('t');
					$income=0;
					$start_date=date("Y-05-01");
					$end_date=date('Y-05-t');
			     
					
                
                       
						$dailytransaction=$this->dashboard_model->transaction_report($start_date,$end_date);
						
							if(isset($dailytransaction) && $dailytransaction){	
				  $bk=0;
				  $bkpay=0;
			      $grefund='';
				  $grefundpay='';
				  $exp=0;
				  $broker='';
				  $brokerpay=0;
				  $tax=0;
				  $taxpay=0;
				  $vendor=0;
				  $vendorpay=0;
				  $cancel=0;
				  $cancelpay=0;
				  $sal=0;
				  $salpay=0;
				  $chanel=0;
				  $chanelpay=0;
				  $comp=0;
				  $comppay=0;
				  $misc=0;
				  $miscpay=0;
				  $misc_in=0;
				  $misc_inpay=0;
				  $restr=0;
				  $restrpay=0;
				  $Mservice=0;
				  $Mservicepay=0;
				  $Guest='';
				  $Guestpay=0;
				  $lap=0;
				  $lappay=0;
				  $spp=0;
				  $spppay=0;
				  $contra=0;
				  $contrapay=0;
				  $charity=0;
				  $charitypay=0;	
				  $count=0;				  
				  $b=0;				  
				  $g=0;	
				  $bro=0;	
				  $t=0;				  
				  $v=0;	
					$c=0;
					$ch=0;
					$s=0;
					$co=0;
					$m=0;
					$mi=0;
					$r=0;
					$ms=0;
					$gp=0;
					$l=0;
					$sp=0;
					$con=0;
					$cr=0;
					$ca=0;
					$cas=0;
					$car=0;
					$dfr=0;
					$ew=0;
					$fu=0;
					$cq=0;		
							
								
				  foreach($dailytransaction as $tranc){		
									 
							     $count=$count+$tranc->count; 
								 
								 $status=$tranc->t_payment_mode;
							     $type=$tranc->hotel_transaction_type_name; 
								 $Inc_Exp=$tranc->hotel_transaction_type_cat; 
							   
							    if($type=='Booking payment'){
								   $bk=$type;
								   $bkpay=$bkpay+$tranc->total;
								  $b=$b+$tranc->count;
							   }else if($type=='Guest refund'){
								   $grefund=$type;
								   $grefundpay=$grefundpay+$tranc->total;
								   $g+=$tranc->count; 
							   }else if($type=='Broker payment'){
								   $broker=$type;
								   $brokerpay=$brokerpay+$tranc->total;
								   $bro+=$tranc->count; 
							   }else if($type=='Tax paymemnt'){
								   $tax=$type;
								   $taxpay=$taxpay+$tranc->total;
								    $t+=$tranc->count;
							   }else if($type=='Vendor payment'){
								   $vendor=$type;
								   $vendorpay=$vendorpay+$tranc->total;
								    $v+=$tranc->count;
							   }else if($type=='Cancellation Fees'){
								   $cancel=$type;
								   $cancelpay=$cancelpay+$tranc->total;
								    $c+=$tranc->count;
							   }else if($type=='Salary payment'){
								   $sal=$type;
								   $salpay=$salpay+$tranc->total;
								    $s+=$tranc->count;
							   }else if($type=='Channel payment'){
								   $chanel=$type;
								   $chanelpay=$chanelpay+$tranc->total;
								    $ch+=$tranc->count;
							   }else if($type=='Compliance Payment'){
								   $comp=$type;
								   $comppay=$comppay+$tranc->total;
								    $co+=$tranc->count;
							   }else if($type=='Misc Payment'){
								   $misc=$type;
								   $miscpay=$miscpay+$tranc->total;
								    $m+=$tranc->count;
							   }else if($type=='Misc Income'){
								   $misc_in=$type;
								   $misc_inpay=$misc_inpay+$tranc->total;
								    $mi+=$tranc->count;
							   }else if($type=='Resturant payment'){
								   $restr=$type;
								   $restrpay=$restrpay+$tranc->total;
								    $r+=$tranc->count;
							   }else if($type=='Misc service payment'){
								   $Mservice=$type;
								   $Mservicepay=$Mservicepay+$tranc->total;
								    $ms+=$tranc->count;
							   }else if($type=='Guest Payment'){
								   $Guest=$type;
								   $Guestpay=$Guestpay+$tranc->total;
								    $gp+=$tranc->count;
							   }else if($type=='Local Authority Payment'){
								   $lap=$type;
								   $lappay=$lappay+$tranc->total;
								    $l+=$tranc->count;
							   }else if($type=='Service Provider Payment'){
								   $spp=$type;
								   $spppay=$spppay+$tranc->total;
								    $sp+=$tranc->count;
							   }else if($type=='Contractor Payment'){
								   $contra=$type;
								   $contrapay=$contrapay+$tranc->total;
								    $con+=$tranc->count;
							   }else if($type=='Charity'){
								   $charity=$type;
								   $charitypay=$charitypay+$tranc->total;
								    $cr+=$tranc->count;
							   }
								if($Inc_Exp=='Income'){
								$income=$income+$tranc->total;
								}else{
									$exp=$exp+$tranc->total;
								}
								
				  if($status=='cash' || $status=='Cash'){
								$cash=$status;
								$cas++;
							}
							if($status=='cards'){
								$card=$status;
								$car++;
							}		
						if($status=='fund'){
								$fund=$status;
								$fu++;
							}
							if($status=='cheque'){
								$cheque=$status;
								$cq++;
							}
							if($status=='draft'){
								$draft=$status;
								$dfr++;
							}	
							if($status=='ewallet'){
								$ewallet=$status;
								$ew++;
							}	
							
							}
							//$gross=$rmAmt+$rrtax+$mealPlan+$mpTax;
							}
							
					
						?>
              <td align="center"><?php  echo 5;?></td>
              <td align="center"> <a href="<?php echo base_url();?>reports/daily_transaction_summary?m=<?php echo 5?>"> <?php  echo date("F", strtotime($a_date)); ?> </a> <?php echo " ".date("Y", strtotime($a_date))?></td>
              <td align="center"><?php echo date("t", strtotime($a_date)); ?></td>
              <td align="center"><?php //Room Count
										echo $count;?></td>
              <td align="center"><?php
					//Occupancy    
									if(isset($bkpay) && $bkpay){
									echo $bk." :".$bkpay."(".$b. ")"."</br>";
									}
									if(isset($grefundpay) && $grefundpay){
									echo $grefund." :".$grefundpay."(" .$g. ")"."</br>";
									}
									if(isset($brokerpay) && $brokerpay){
									echo $broker." :".$brokerpay."(" .$bro. ")"."</br>";
									}
									if(isset($taxpay) && $taxpay){
									echo $tax." :".$taxpay."(" .$t. ")"."</br>";
									}
									if(isset($vendorpay) && $vendorpay){
									echo $vendor." :".$vendorpay."(" .$v. ")"."</br>";
									}
									if(isset($cancelpay) && $cancelpay){
									echo $cancel." :".$cancelpay."(" .$c. ")"."</br>";
									}
									if(isset($salpay) && $salpay){
									echo $sal." :".$salpay."(" .$s. ")"."</br>";
									}
									if(isset($chanelpay) && $chanelpay){
									echo $chanel." :".$chanelpay."(" .$ch. ")"."</br>";
									}
									if(isset($comppay) && $comppay){
									echo $comp." :".$comppay."(" .$co. ")"."</br>";
									}
									if(isset($miscpay) && $miscpay){
									echo $misc." :".$miscpay."(" .$m. ")"."</br>";
									}
									if(isset($misc_inpay) && $misc_inpay){
									echo $misc_in." :".$misc_inpay."(" .$mi. ")"."</br>";
									}
									if(isset($restrpay) && $restrpay){
									echo $restr." :".$restrpay."(" .$r. ")"."</br>";
									}
									if(isset($Mservicepay) && $Mservicepay){
									echo $Mservice." :".$Mservicepay."(" .$ms. ")"."</br>";
									}
									if(isset($Guestpay) && $Guestpay){
									echo $Guest." :".$Guestpay."(" .$gp. ")"."</br>";
									}
									if(isset($lappay) && $lappay){
									echo $lap." :".$lappay."(" .$l. ")"."</br>";
									}
									if(isset($spppay) && $spppay){
									echo $spp." :".$spppay."(" .$sp. ")"."</br>";
									}
									if(isset($contrapay) && $contrapay){
									echo $contra." :".$contrapay."(" .$con++. ")"."</br>";
									}if(isset($charitypay) && $charitypay){
									echo $charit." :".$charitypay."(" .$ct. ")"."</br>";
									}
									?></td>
									<td align="center"><?php 
						
					if(isset($cash)) { echo $cash.": "."( ".$cas." )"."</br>";
					}
					if(isset($card) && $card){
						echo $card.": "."( ".$car." )"."</br>";
					}
					if(isset($fund) && $fund){
						echo $fund.": "."( ".$fu." )"."</br>";
					}
					if(isset($cheque) && $cheque){
						echo $cheque.": "."( ".$cq." )"."</br>";
					}
					if(isset($draft) && $draft){
						echo $draft.": "."( ".$dfr." )"."</br>";
					}
					if(isset($ewallet) && $ewallet){
						echo $ewallet.": "."( ".$ew." )"."</br>";
					}
						?></td>
              <td align="center"><?php 
					if(isset($income) && $income>0) {echo $income;}else{
						echo "0";
					}
						?></td> 
						
              <td align="center"><?php
					if(isset($exp) && $exp>0) {echo $exp;}else{
						echo "0";
					}
			?></td>
				
            </tr>
			
			<tr>
              <?php    $a_date = date("Y-06-01");
                    $maxDays=date('t');
					$income=0;
					$start_date=date("Y-06-01");
					$end_date=date('Y-06-t');
			     
					
                
                       
						$dailytransaction=$this->dashboard_model->transaction_report($start_date,$end_date);
						
							if(isset($dailytransaction) && $dailytransaction){	
				  $bk=0;
				  $bkpay=0;
			      $grefund='';
				  $grefundpay='';
				  $exp=0;
				  $broker='';
				  $brokerpay=0;
				  $tax=0;
				  $taxpay=0;
				  $vendor=0;
				  $vendorpay=0;
				  $cancel=0;
				  $cancelpay=0;
				  $sal=0;
				  $salpay=0;
				  $chanel=0;
				  $chanelpay=0;
				  $comp=0;
				  $comppay=0;
				  $misc=0;
				  $miscpay=0;
				  $misc_in=0;
				  $misc_inpay=0;
				  $restr=0;
				  $restrpay=0;
				  $Mservice=0;
				  $Mservicepay=0;
				  $Guest='';
				  $Guestpay=0;
				  $lap=0;
				  $lappay=0;
				  $spp=0;
				  $spppay=0;
				  $contra=0;
				  $contrapay=0;
				  $charity=0;
				  $charitypay=0;	
				  $count=0;				  
				  $b=0;				  
				  $g=0;	
				  $bro=0;	
				  $t=0;				  
				  $v=0;	
					$c=0;
					$ch=0;
					$s=0;
					$co=0;
					$m=0;
					$mi=0;
					$r=0;
					$ms=0;
					$gp=0;
					$l=0;
					$sp=0;
					$con=0;
					$cr=0;
					$ca=0;
					$cas=0;
					$car=0;
					$dfr=0;
					$ew=0;
					$fu=0;
					$cq=0;		
							
								
				  foreach($dailytransaction as $tranc){		
									 
							     $count=$count+$tranc->count; 
								 
								 $status=$tranc->t_payment_mode;
							     $type=$tranc->hotel_transaction_type_name; 
								 $Inc_Exp=$tranc->hotel_transaction_type_cat; 
							   
							    if($type=='Booking payment'){
								   $bk=$type;
								   $bkpay=$bkpay+$tranc->total;
								  $b=$b+$tranc->count;
							   }else if($type=='Guest refund'){
								   $grefund=$type;
								   $grefundpay=$grefundpay+$tranc->total;
								   $g+=$tranc->count; 
							   }else if($type=='Broker payment'){
								   $broker=$type;
								   $brokerpay=$brokerpay+$tranc->total;
								   $bro+=$tranc->count; 
							   }else if($type=='Tax paymemnt'){
								   $tax=$type;
								   $taxpay=$taxpay+$tranc->total;
								    $t+=$tranc->count;
							   }else if($type=='Vendor payment'){
								   $vendor=$type;
								   $vendorpay=$vendorpay+$tranc->total;
								    $v+=$tranc->count;
							   }else if($type=='Cancellation Fees'){
								   $cancel=$type;
								   $cancelpay=$cancelpay+$tranc->total;
								    $c+=$tranc->count;
							   }else if($type=='Salary payment'){
								   $sal=$type;
								   $salpay=$salpay+$tranc->total;
								    $s+=$tranc->count;
							   }else if($type=='Channel payment'){
								   $chanel=$type;
								   $chanelpay=$chanelpay+$tranc->total;
								    $ch+=$tranc->count;
							   }else if($type=='Compliance Payment'){
								   $comp=$type;
								   $comppay=$comppay+$tranc->total;
								    $co+=$tranc->count;
							   }else if($type=='Misc Payment'){
								   $misc=$type;
								   $miscpay=$miscpay+$tranc->total;
								    $m+=$tranc->count;
							   }else if($type=='Misc Income'){
								   $misc_in=$type;
								   $misc_inpay=$misc_inpay+$tranc->total;
								    $mi+=$tranc->count;
							   }else if($type=='Resturant payment'){
								   $restr=$type;
								   $restrpay=$restrpay+$tranc->total;
								    $r+=$tranc->count;
							   }else if($type=='Misc service payment'){
								   $Mservice=$type;
								   $Mservicepay=$Mservicepay+$tranc->total;
								    $ms+=$tranc->count;
							   }else if($type=='Guest Payment'){
								   $Guest=$type;
								   $Guestpay=$Guestpay+$tranc->total;
								    $gp+=$tranc->count;
							   }else if($type=='Local Authority Payment'){
								   $lap=$type;
								   $lappay=$lappay+$tranc->total;
								    $l+=$tranc->count;
							   }else if($type=='Service Provider Payment'){
								   $spp=$type;
								   $spppay=$spppay+$tranc->total;
								    $sp+=$tranc->count;
							   }else if($type=='Contractor Payment'){
								   $contra=$type;
								   $contrapay=$contrapay+$tranc->total;
								    $con+=$tranc->count;
							   }else if($type=='Charity'){
								   $charity=$type;
								   $charitypay=$charitypay+$tranc->total;
								    $cr+=$tranc->count;
							   }
								
								if($Inc_Exp=='Income'){
								$income=$income+$tranc->total;
								}else{
									$exp=$exp+$tranc->total;
								}
								
				  if($status=='cash' || $status=='Cash'){
								$cash=$status;
								$cas++;
							}
							if($status=='cards'){
								$card=$status;
								$car++;
							}		
						if($status=='fund'){
								$fund=$status;
								$fu++;
							}
							if($status=='cheque'){
								$cheque=$status;
								$cq++;
							}
							if($status=='draft'){
								$draft=$status;
								$dfr++;
							}	
							if($status=='ewallet'){
								$ewallet=$status;
								$ew++;
							}	
							
							}
							//$gross=$rmAmt+$rrtax+$mealPlan+$mpTax;
							}
							
					
						?>
              <td align="center"><?php  echo 6;?></td>
              <td align="center"> <a href="<?php echo base_url();?>reports/daily_transaction_summary?m=<?php echo 6?>"> <?php  echo date("F", strtotime($a_date)); ?> </a> <?php echo " ".date("Y", strtotime($a_date))?></td>
              <td align="center"><?php echo date("t", strtotime($a_date)); ?></td>
              <td align="center"><?php //Room Count
										echo $count;?></td>
              <td align="center"><?php
					//Occupancy    
									if(isset($bkpay) && $bkpay){
									echo $bk." :".$bkpay."(".$b. ")"."</br>";
									}
									if(isset($grefundpay) && $grefundpay){
									echo $grefund." :".$grefundpay."(" .$g. ")"."</br>";
									}
									if(isset($brokerpay) && $brokerpay){
									echo $broker." :".$brokerpay."(" .$bro. ")"."</br>";
									}
									if(isset($taxpay) && $taxpay){
									echo $tax." :".$taxpay."(" .$t. ")"."</br>";
									}
									if(isset($vendorpay) && $vendorpay){
									echo $vendor." :".$vendorpay."(" .$v. ")"."</br>";
									}
									if(isset($cancelpay) && $cancelpay){
									echo $cancel." :".$cancelpay."(" .$c. ")"."</br>";
									}
									if(isset($salpay) && $salpay){
									echo $sal." :".$salpay."(" .$s. ")"."</br>";
									}
									if(isset($chanelpay) && $chanelpay){
									echo $chanel." :".$chanelpay."(" .$ch. ")"."</br>";
									}
									if(isset($comppay) && $comppay){
									echo $comp." :".$comppay."(" .$co. ")"."</br>";
									}
									if(isset($miscpay) && $miscpay){
									echo $misc." :".$miscpay."(" .$m. ")"."</br>";
									}
									if(isset($misc_inpay) && $misc_inpay){
									echo $misc_in." :".$misc_inpay."(" .$mi. ")"."</br>";
									}
									if(isset($restrpay) && $restrpay){
									echo $restr." :".$restrpay."(" .$r. ")"."</br>";
									}
									if(isset($Mservicepay) && $Mservicepay){
									echo $Mservice." :".$Mservicepay."(" .$ms. ")"."</br>";
									}
									if(isset($Guestpay) && $Guestpay){
									echo $Guest." :".$Guestpay."(" .$gp. ")"."</br>";
									}
									if(isset($lappay) && $lappay){
									echo $lap." :".$lappay."(" .$l. ")"."</br>";
									}
									if(isset($spppay) && $spppay){
									echo $spp." :".$spppay."(" .$sp. ")"."</br>";
									}
									if(isset($contrapay) && $contrapay){
									echo $contra." :".$contrapay."(" .$con++. ")"."</br>";
									}if(isset($charitypay) && $charitypay){
									echo $charit." :".$charitypay."(" .$ct. ")"."</br>";
									}
									?></td>
									<td align="center"><?php 
						
					if(isset($cash)) { echo $cash.": "."( ".$cas." )"."</br>";
					}
					if(isset($card) && $card){
						echo $card.": "."( ".$car." )"."</br>";
					}
					if(isset($fund) && $fund){
						echo $fund.": "."( ".$fu." )"."</br>";
					}
					if(isset($cheque) && $cheque){
						echo $cheque.": "."( ".$cq." )"."</br>";
					}
					if(isset($draft) && $draft){
						echo $draft.": "."( ".$dfr." )"."</br>";
					}
					if(isset($ewallet) && $ewallet){
						echo $ewallet.": "."( ".$ew." )"."</br>";
					}
						?></td>
              <td align="center"><?php 
					if(isset($income) && $income>0) {echo $income;}else{
						echo "0";
					}
						?></td> 
						
              <td align="center"><?php
					if(isset($exp) && $exp>0) {echo $exp;}else{
						echo "0";
					}
			?></td>
				
            </tr>
			
			<tr>
              <?php   $a_date = date("Y-07-01");
                    $maxDays=date('t');
					$income=0;
					$start_date=date("Y-07-01");
					$end_date=date('Y-07-t');
			     
					
                
                       
						$dailytransaction=$this->dashboard_model->transaction_report($start_date,$end_date);
						
							if(isset($dailytransaction) && $dailytransaction){	
				  $bk=0;
				  $bkpay=0;
			      $grefund='';
				  $grefundpay='';
				  $exp=0;
				  $broker='';
				  $brokerpay=0;
				  $tax=0;
				  $taxpay=0;
				  $vendor=0;
				  $vendorpay=0;
				  $cancel=0;
				  $cancelpay=0;
				  $sal=0;
				  $salpay=0;
				  $chanel=0;
				  $chanelpay=0;
				  $comp=0;
				  $comppay=0;
				  $misc=0;
				  $miscpay=0;
				  $misc_in=0;
				  $misc_inpay=0;
				  $restr=0;
				  $restrpay=0;
				  $Mservice=0;
				  $Mservicepay=0;
				  $Guest='';
				  $Guestpay=0;
				  $lap=0;
				  $lappay=0;
				  $spp=0;
				  $spppay=0;
				  $contra=0;
				  $contrapay=0;
				  $charity=0;
				  $charitypay=0;	
				  $count=0;				  
				  $b=0;				  
				  $g=0;	
				  $bro=0;	
				  $t=0;				  
				  $v=0;	
					$c=0;
					$ch=0;
					$s=0;
					$co=0;
					$m=0;
					$mi=0;
					$r=0;
					$ms=0;
					$gp=0;
					$l=0;
					$sp=0;
					$con=0;
					$cr=0;
					$ca=0;
					$cas=0;
					$car=0;
					$dfr=0;
					$ew=0;
					$fu=0;
					$cq=0;		
							
								
				  foreach($dailytransaction as $tranc){		
									 
							     $count=$count+$tranc->count; 
								 
								 $status=$tranc->t_payment_mode;
							     $type=$tranc->hotel_transaction_type_name; 
								 $Inc_Exp=$tranc->hotel_transaction_type_cat; 
							   
							    if($type=='Booking payment'){
								   $bk=$type;
								   $bkpay=$bkpay+$tranc->total;
								  $b=$b+$tranc->count;
							   }else if($type=='Guest refund'){
								   $grefund=$type;
								   $grefundpay=$grefundpay+$tranc->total;
								   $g+=$tranc->count; 
							   }else if($type=='Broker payment'){
								   $broker=$type;
								   $brokerpay=$brokerpay+$tranc->total;
								   $bro+=$tranc->count; 
							   }else if($type=='Tax paymemnt'){
								   $tax=$type;
								   $taxpay=$taxpay+$tranc->total;
								    $t+=$tranc->count;
							   }else if($type=='Vendor payment'){
								   $vendor=$type;
								   $vendorpay=$vendorpay+$tranc->total;
								    $v+=$tranc->count;
							   }else if($type=='Cancellation Fees'){
								   $cancel=$type;
								   $cancelpay=$cancelpay+$tranc->total;
								    $c+=$tranc->count;
							   }else if($type=='Salary payment'){
								   $sal=$type;
								   $salpay=$salpay+$tranc->total;
								    $s+=$tranc->count;
							   }else if($type=='Channel payment'){
								   $chanel=$type;
								   $chanelpay=$chanelpay+$tranc->total;
								    $ch+=$tranc->count;
							   }else if($type=='Compliance Payment'){
								   $comp=$type;
								   $comppay=$comppay+$tranc->total;
								    $co+=$tranc->count;
							   }else if($type=='Misc Payment'){
								   $misc=$type;
								   $miscpay=$miscpay+$tranc->total;
								    $m+=$tranc->count;
							   }else if($type=='Misc Income'){
								   $misc_in=$type;
								   $misc_inpay=$misc_inpay+$tranc->total;
								    $mi+=$tranc->count;
							   }else if($type=='Resturant payment'){
								   $restr=$type;
								   $restrpay=$restrpay+$tranc->total;
								    $r+=$tranc->count;
							   }else if($type=='Misc service payment'){
								   $Mservice=$type;
								   $Mservicepay=$Mservicepay+$tranc->total;
								    $ms+=$tranc->count;
							   }else if($type=='Guest Payment'){
								   $Guest=$type;
								   $Guestpay=$Guestpay+$tranc->total;
								    $gp+=$tranc->count;
							   }else if($type=='Local Authority Payment'){
								   $lap=$type;
								   $lappay=$lappay+$tranc->total;
								    $l+=$tranc->count;
							   }else if($type=='Service Provider Payment'){
								   $spp=$type;
								   $spppay=$spppay+$tranc->total;
								    $sp+=$tranc->count;
							   }else if($type=='Contractor Payment'){
								   $contra=$type;
								   $contrapay=$contrapay+$tranc->total;
								    $con+=$tranc->count;
							   }else if($type=='Charity'){
								   $charity=$type;
								   $charitypay=$charitypay+$tranc->total;
								    $cr+=$tranc->count;
							   }
								if($Inc_Exp=='Income'){
								$income=$income+$tranc->total;
								}else{
									$exp=$exp+$tranc->total;
								}
								
				  if($status=='cash' || $status=='Cash'){
								$cash=$status;
								$cas++;
							}
							if($status=='cards'){
								$card=$status;
								$car++;
							}		
						if($status=='fund'){
								$fund=$status;
								$fu++;
							}
							if($status=='cheque'){
								$cheque=$status;
								$cq++;
							}
							if($status=='draft'){
								$draft=$status;
								$dfr++;
							}	
							if($status=='ewallet'){
								$ewallet=$status;
								$ew++;
							}	
							
							}
							//$gross=$rmAmt+$rrtax+$mealPlan+$mpTax;
							}
							
					
						?>
              <td align="center"><?php  echo 7;?></td>
              <td align="center"> <a href="<?php echo base_url();?>reports/daily_transaction_summary?m=<?php echo 7?>"> <?php  echo date("F", strtotime($a_date)); ?> </a> <?php echo " ".date("Y", strtotime($a_date))?></td>
              <td align="center"><?php echo date("t", strtotime($a_date)); ?></td>
              <td align="center"><?php //Room Count
										echo $count;?></td>
              <td align="center"><?php
					//Occupancy    
									if(isset($bkpay) && $bkpay){
									echo $bk." :".$bkpay."(".$b. ")"."</br>";
									}
									if(isset($grefundpay) && $grefundpay){
									echo $grefund." :".$grefundpay."(" .$g. ")"."</br>";
									}
									if(isset($brokerpay) && $brokerpay){
									echo $broker." :".$brokerpay."(" .$bro. ")"."</br>";
									}
									if(isset($taxpay) && $taxpay){
									echo $tax." :".$taxpay."(" .$t. ")"."</br>";
									}
									if(isset($vendorpay) && $vendorpay){
									echo $vendor." :".$vendorpay."(" .$v. ")"."</br>";
									}
									if(isset($cancelpay) && $cancelpay){
									echo $cancel." :".$cancelpay."(" .$c. ")"."</br>";
									}
									if(isset($salpay) && $salpay){
									echo $sal." :".$salpay."(" .$s. ")"."</br>";
									}
									if(isset($chanelpay) && $chanelpay){
									echo $chanel." :".$chanelpay."(" .$ch. ")"."</br>";
									}
									if(isset($comppay) && $comppay){
									echo $comp." :".$comppay."(" .$co. ")"."</br>";
									}
									if(isset($miscpay) && $miscpay){
									echo $misc." :".$miscpay."(" .$m. ")"."</br>";
									}
									if(isset($misc_inpay) && $misc_inpay){
									echo $misc_in." :".$misc_inpay."(" .$mi. ")"."</br>";
									}
									if(isset($restrpay) && $restrpay){
									echo $restr." :".$restrpay."(" .$r. ")"."</br>";
									}
									if(isset($Mservicepay) && $Mservicepay){
									echo $Mservice." :".$Mservicepay."(" .$ms. ")"."</br>";
									}
									if(isset($Guestpay) && $Guestpay){
									echo $Guest." :".$Guestpay."(" .$gp. ")"."</br>";
									}
									if(isset($lappay) && $lappay){
									echo $lap." :".$lappay."(" .$l. ")"."</br>";
									}
									if(isset($spppay) && $spppay){
									echo $spp." :".$spppay."(" .$sp. ")"."</br>";
									}
									if(isset($contrapay) && $contrapay){
									echo $contra." :".$contrapay."(" .$con++. ")"."</br>";
									}if(isset($charitypay) && $charitypay){
									echo $charit." :".$charitypay."(" .$ct. ")"."</br>";
									}
									?></td>
									<td align="center"><?php 
						
					if(isset($cash)) { echo $cash.": "."( ".$cas." )"."</br>";
					}
					if(isset($card) && $card){
						echo $card.": "."( ".$car." )"."</br>";
					}
					if(isset($fund) && $fund){
						echo $fund.": "."( ".$fu." )"."</br>";
					}
					if(isset($cheque) && $cheque){
						echo $cheque.": "."( ".$cq." )"."</br>";
					}
					if(isset($draft) && $draft){
						echo $draft.": "."( ".$dfr." )"."</br>";
					}
					if(isset($ewallet) && $ewallet){
						echo $ewallet.": "."( ".$ew." )"."</br>";
					}
						?></td>
              <td align="center"><?php 
					if(isset($income) && $income>0) {echo $income;}else{
						echo "0";
					}
						?></td> 
						
              <td align="center"><?php
					if(isset($exp) && $exp>0) {echo $exp;}else{
						echo "0";
					}
			?></td>
				
            </tr>
			
			<tr>
              <?php   $a_date = date("Y-08-01"); 
                    $maxDays=date('t');
					$income=0;
					$start_date=date("Y-08-01");
					$end_date=date('Y-08-t');
			     
					
                
                       
						$dailytransaction=$this->dashboard_model->transaction_report($start_date,$end_date);
						
							if(isset($dailytransaction) && $dailytransaction){	
				  $bk=0;
				  $bkpay=0;
			      $grefund='';
				  $grefundpay='';
				  $exp=0;
				  $broker='';
				  $brokerpay=0;
				  $tax=0;
				  $taxpay=0;
				  $vendor=0;
				  $vendorpay=0;
				  $cancel=0;
				  $cancelpay=0;
				  $sal=0;
				  $salpay=0;
				  $chanel=0;
				  $chanelpay=0;
				  $comp=0;
				  $comppay=0;
				  $misc=0;
				  $miscpay=0;
				  $misc_in=0;
				  $misc_inpay=0;
				  $restr=0;
				  $restrpay=0;
				  $Mservice=0;
				  $Mservicepay=0;
				  $Guest='';
				  $Guestpay=0;
				  $lap=0;
				  $lappay=0;
				  $spp=0;
				  $spppay=0;
				  $contra=0;
				  $contrapay=0;
				  $charity=0;
				  $charitypay=0;	
				  $count=0;				  
				  $b=0;				  
				  $g=0;	
				  $bro=0;	
				  $t=0;				  
				  $v=0;	
					$c=0;
					$ch=0;
					$s=0;
					$co=0;
					$m=0;
					$mi=0;
					$r=0;
					$ms=0;
					$gp=0;
					$l=0;
					$sp=0;
					$con=0;
					$cr=0;
					$ca=0;
					$cas=0;
					$car=0;
					$dfr=0;
					$ew=0;
					$fu=0;
					$cq=0;		
							
								
				  foreach($dailytransaction as $tranc){		
									 
							     $count=$count+$tranc->count; 
								 
								 $status=$tranc->t_payment_mode;
							     $type=$tranc->hotel_transaction_type_name; 
								 $Inc_Exp=$tranc->hotel_transaction_type_cat; 
							   
							   if($type=='Booking payment'){
								   $bk=$type;
								   $bkpay=$bkpay+$tranc->total;
								  $b=$b+$tranc->count;
							   }else if($type=='Guest refund'){
								   $grefund=$type;
								   $grefundpay=$grefundpay+$tranc->total;
								   $g+=$tranc->count; 
							   }else if($type=='Broker payment'){
								   $broker=$type;
								   $brokerpay=$brokerpay+$tranc->total;
								   $bro+=$tranc->count; 
							   }else if($type=='Tax paymemnt'){
								   $tax=$type;
								   $taxpay=$taxpay+$tranc->total;
								    $t+=$tranc->count;
							   }else if($type=='Vendor payment'){
								   $vendor=$type;
								   $vendorpay=$vendorpay+$tranc->total;
								    $v+=$tranc->count;
							   }else if($type=='Cancellation Fees'){
								   $cancel=$type;
								   $cancelpay=$cancelpay+$tranc->total;
								    $c+=$tranc->count;
							   }else if($type=='Salary payment'){
								   $sal=$type;
								   $salpay=$salpay+$tranc->total;
								    $s+=$tranc->count;
							   }else if($type=='Channel payment'){
								   $chanel=$type;
								   $chanelpay=$chanelpay+$tranc->total;
								    $ch+=$tranc->count;
							   }else if($type=='Compliance Payment'){
								   $comp=$type;
								   $comppay=$comppay+$tranc->total;
								    $co+=$tranc->count;
							   }else if($type=='Misc Payment'){
								   $misc=$type;
								   $miscpay=$miscpay+$tranc->total;
								    $m+=$tranc->count;
							   }else if($type=='Misc Income'){
								   $misc_in=$type;
								   $misc_inpay=$misc_inpay+$tranc->total;
								    $mi+=$tranc->count;
							   }else if($type=='Resturant payment'){
								   $restr=$type;
								   $restrpay=$restrpay+$tranc->total;
								    $r+=$tranc->count;
							   }else if($type=='Misc service payment'){
								   $Mservice=$type;
								   $Mservicepay=$Mservicepay+$tranc->total;
								    $ms+=$tranc->count;
							   }else if($type=='Guest Payment'){
								   $Guest=$type;
								   $Guestpay=$Guestpay+$tranc->total;
								    $gp+=$tranc->count;
							   }else if($type=='Local Authority Payment'){
								   $lap=$type;
								   $lappay=$lappay+$tranc->total;
								    $l+=$tranc->count;
							   }else if($type=='Service Provider Payment'){
								   $spp=$type;
								   $spppay=$spppay+$tranc->total;
								    $sp+=$tranc->count;
							   }else if($type=='Contractor Payment'){
								   $contra=$type;
								   $contrapay=$contrapay+$tranc->total;
								    $con+=$tranc->count;
							   }else if($type=='Charity'){
								   $charity=$type;
								   $charitypay=$charitypay+$tranc->total;
								    $cr+=$tranc->count;
							   }
								if($Inc_Exp=='Income'){
								$income=$income+$tranc->total;
								}else{
									$exp=$exp+$tranc->total;
								}
								
				  if($status=='cash' || $status=='Cash'){
								$cash=$status;
								$cas++;
							}
							if($status=='cards'){
								$card=$status;
								$car++;
							}		
						if($status=='fund'){
								$fund=$status;
								$fu++;
							}
							if($status=='cheque'){
								$cheque=$status;
								$cq++;
							}
							if($status=='draft'){
								$draft=$status;
								$dfr++;
							}	
							if($status=='ewallet'){
								$ewallet=$status;
								$ew++;
							}	
							
							}
							//$gross=$rmAmt+$rrtax+$mealPlan+$mpTax;
							}
							
					
						?>
              <td align="center"><?php  echo 8;?></td>
              <td align="center"> <a href="<?php echo base_url();?>reports/daily_transaction_summary?m=<?php echo 8?>"> <?php  echo date("F", strtotime($a_date)); ?> </a> <?php echo " ".date("Y", strtotime($a_date))?></td>
              <td align="center"><?php echo date("t", strtotime($a_date)); ?></td>
              <td align="center"><?php //Room Count
										echo $count;?></td>
              <td align="center"><?php
					//Occupancy    
									if(isset($bkpay) && $bkpay){
									echo $bk." :".$bkpay."(".$b. ")"."</br>";
									}
									if(isset($grefundpay) && $grefundpay){
									echo $grefund." :".$grefundpay."(" .$g. ")"."</br>";
									}
									if(isset($brokerpay) && $brokerpay){
									echo $broker." :".$brokerpay."(" .$bro. ")"."</br>";
									}
									if(isset($taxpay) && $taxpay){
									echo $tax." :".$taxpay."(" .$t. ")"."</br>";
									}
									if(isset($vendorpay) && $vendorpay){
									echo $vendor." :".$vendorpay."(" .$v. ")"."</br>";
									}
									if(isset($cancelpay) && $cancelpay){
									echo $cancel." :".$cancelpay."(" .$c. ")"."</br>";
									}
									if(isset($salpay) && $salpay){
									echo $sal." :".$salpay."(" .$s. ")"."</br>";
									}
									if(isset($chanelpay) && $chanelpay){
									echo $chanel." :".$chanelpay."(" .$ch. ")"."</br>";
									}
									if(isset($comppay) && $comppay){
									echo $comp." :".$comppay."(" .$co. ")"."</br>";
									}
									if(isset($miscpay) && $miscpay){
									echo $misc." :".$miscpay."(" .$m. ")"."</br>";
									}
									if(isset($misc_inpay) && $misc_inpay){
									echo $misc_in." :".$misc_inpay."(" .$mi. ")"."</br>";
									}
									if(isset($restrpay) && $restrpay){
									echo $restr." :".$restrpay."(" .$r. ")"."</br>";
									}
									if(isset($Mservicepay) && $Mservicepay){
									echo $Mservice." :".$Mservicepay."(" .$ms. ")"."</br>";
									}
									if(isset($Guestpay) && $Guestpay){
									echo $Guest." :".$Guestpay."(" .$gp. ")"."</br>";
									}
									if(isset($lappay) && $lappay){
									echo $lap." :".$lappay."(" .$l. ")"."</br>";
									}
									if(isset($spppay) && $spppay){
									echo $spp." :".$spppay."(" .$sp. ")"."</br>";
									}
									if(isset($contrapay) && $contrapay){
									echo $contra." :".$contrapay."(" .$con++. ")"."</br>";
									}if(isset($charitypay) && $charitypay){
									echo $charit." :".$charitypay."(" .$ct. ")"."</br>";
									}
									?></td>
									<td align="center"><?php 
						
					if(isset($cash)) { echo $cash.": "."( ".$cas." )"."</br>";
					}
					if(isset($card) && $card){
						echo $card.": "."( ".$car." )"."</br>";
					}
					if(isset($fund) && $fund){
						echo $fund.": "."( ".$fu." )"."</br>";
					}
					if(isset($cheque) && $cheque){
						echo $cheque.": "."( ".$cq." )"."</br>";
					}
					if(isset($draft) && $draft){
						echo $draft.": "."( ".$dfr." )"."</br>";
					}
					if(isset($ewallet) && $ewallet){
						echo $ewallet.": "."( ".$ew." )"."</br>";
					}
						?></td>
              <td align="center"><?php 
					if(isset($income) && $income>0) {echo $income;}else{
						echo "0";
					}
						?></td> 
						
              <td align="center"><?php
					if(isset($exp) && $exp>0) {echo $exp;}else{
						echo "0";
					}
			?></td>
				
            </tr>
			
			<tr>
              <?php   $a_date = date("Y-09-01");
                    $maxDays=date('t');
					$income=0;
					$start_date=date("Y-09-01");
					$end_date=date('Y-09-t');
			     
					
                
                       
						$dailytransaction=$this->dashboard_model->transaction_report($start_date,$end_date);
						
							if(isset($dailytransaction) && $dailytransaction){	
				  $bk=0;
				  $bkpay=0;
			      $grefund='';
				  $grefundpay='';
				  $exp=0;
				  $broker='';
				  $brokerpay=0;
				  $tax=0;
				  $taxpay=0;
				  $vendor=0;
				  $vendorpay=0;
				  $cancel=0;
				  $cancelpay=0;
				  $sal=0;
				  $salpay=0;
				  $chanel=0;
				  $chanelpay=0;
				  $comp=0;
				  $comppay=0;
				  $misc=0;
				  $miscpay=0;
				  $misc_in=0;
				  $misc_inpay=0;
				  $restr=0;
				  $restrpay=0;
				  $Mservice=0;
				  $Mservicepay=0;
				  $Guest='';
				  $Guestpay=0;
				  $lap=0;
				  $lappay=0;
				  $spp=0;
				  $spppay=0;
				  $contra=0;
				  $contrapay=0;
				  $charity=0;
				  $charitypay=0;	
				  $count=0;				  
				  $b=0;				  
				  $g=0;	
				  $bro=0;	
				  $t=0;				  
				  $v=0;	
					$c=0;
					$ch=0;
					$s=0;
					$co=0;
					$m=0;
					$mi=0;
					$r=0;
					$ms=0;
					$gp=0;
					$l=0;
					$sp=0;
					$con=0;
					$cr=0;
					$ca=0;
					$cas=0;
					$car=0;
					$dfr=0;
					$ew=0;
					$fu=0;
					$cq=0;		
							
								
				  foreach($dailytransaction as $tranc){		
									 
							     $count=$count+$tranc->count; 
								 
								 $status=$tranc->t_payment_mode;
							     $type=$tranc->hotel_transaction_type_name; 
								 $Inc_Exp=$tranc->hotel_transaction_type_cat; 
							   
							   if($type=='Booking payment'){
								   $bk=$type;
								   $bkpay=$bkpay+$tranc->total;
								  $b=$b+$tranc->count;
							   }else if($type=='Guest refund'){
								   $grefund=$type;
								   $grefundpay=$grefundpay+$tranc->total;
								   $g+=$tranc->count; 
							   }else if($type=='Broker payment'){
								   $broker=$type;
								   $brokerpay=$brokerpay+$tranc->total;
								   $bro+=$tranc->count; 
							   }else if($type=='Tax paymemnt'){
								   $tax=$type;
								   $taxpay=$taxpay+$tranc->total;
								    $t+=$tranc->count;
							   }else if($type=='Vendor payment'){
								   $vendor=$type;
								   $vendorpay=$vendorpay+$tranc->total;
								    $v+=$tranc->count;
							   }else if($type=='Cancellation Fees'){
								   $cancel=$type;
								   $cancelpay=$cancelpay+$tranc->total;
								    $c+=$tranc->count;
							   }else if($type=='Salary payment'){
								   $sal=$type;
								   $salpay=$salpay+$tranc->total;
								    $s+=$tranc->count;
							   }else if($type=='Channel payment'){
								   $chanel=$type;
								   $chanelpay=$chanelpay+$tranc->total;
								    $ch+=$tranc->count;
							   }else if($type=='Compliance Payment'){
								   $comp=$type;
								   $comppay=$comppay+$tranc->total;
								    $co+=$tranc->count;
							   }else if($type=='Misc Payment'){
								   $misc=$type;
								   $miscpay=$miscpay+$tranc->total;
								    $m+=$tranc->count;
							   }else if($type=='Misc Income'){
								   $misc_in=$type;
								   $misc_inpay=$misc_inpay+$tranc->total;
								    $mi+=$tranc->count;
							   }else if($type=='Resturant payment'){
								   $restr=$type;
								   $restrpay=$restrpay+$tranc->total;
								    $r+=$tranc->count;
							   }else if($type=='Misc service payment'){
								   $Mservice=$type;
								   $Mservicepay=$Mservicepay+$tranc->total;
								    $ms+=$tranc->count;
							   }else if($type=='Guest Payment'){
								   $Guest=$type;
								   $Guestpay=$Guestpay+$tranc->total;
								    $gp+=$tranc->count;
							   }else if($type=='Local Authority Payment'){
								   $lap=$type;
								   $lappay=$lappay+$tranc->total;
								    $l+=$tranc->count;
							   }else if($type=='Service Provider Payment'){
								   $spp=$type;
								   $spppay=$spppay+$tranc->total;
								    $sp+=$tranc->count;
							   }else if($type=='Contractor Payment'){
								   $contra=$type;
								   $contrapay=$contrapay+$tranc->total;
								    $con+=$tranc->count;
							   }else if($type=='Charity'){
								   $charity=$type;
								   $charitypay=$charitypay+$tranc->total;
								    $cr+=$tranc->count;
							   }
								if($Inc_Exp=='Income'){
								$income=$income+$tranc->total;
								}else{
									$exp=$exp+$tranc->total;
								}
								
				  if($status=='cash' || $status=='Cash'){
								$cash=$status;
								$cas++;
							}
							if($status=='cards'){
								$card=$status;
								$car++;
							}		
						if($status=='fund'){
								$fund=$status;
								$fu++;
							}
							if($status=='cheque'){
								$cheque=$status;
								$cq++;
							}
							if($status=='draft'){
								$draft=$status;
								$dfr++;
							}	
							if($status=='ewallet'){
								$ewallet=$status;
								$ew++;
							}	
							
							}
							//$gross=$rmAmt+$rrtax+$mealPlan+$mpTax;
							}
							
					
						?>
              <td align="center"><?php  echo 9;?></td>
              <td align="center"> <a href="<?php echo base_url();?>reports/daily_transaction_summary?m=<?php echo 9?>"> <?php  echo date("F", strtotime($a_date)); ?> </a> <?php echo " ".date("Y", strtotime($a_date))?></td>
              <td align="center"><?php echo date("t", strtotime($a_date)); ?></td>
              <td align="center"><?php //Room Count
										echo $count;?></td>
              <td align="center"><?php
					//Occupancy    
									if(isset($bkpay) && $bkpay){
									echo $bk." :".$bkpay."(".$b. ")"."</br>";
									}
									if(isset($grefundpay) && $grefundpay){
									echo $grefund." :".$grefundpay."(" .$g. ")"."</br>";
									}
									if(isset($brokerpay) && $brokerpay){
									echo $broker." :".$brokerpay."(" .$bro. ")"."</br>";
									}
									if(isset($taxpay) && $taxpay){
									echo $tax." :".$taxpay."(" .$t. ")"."</br>";
									}
									if(isset($vendorpay) && $vendorpay){
									echo $vendor." :".$vendorpay."(" .$v. ")"."</br>";
									}
									if(isset($cancelpay) && $cancelpay){
									echo $cancel." :".$cancelpay."(" .$c. ")"."</br>";
									}
									if(isset($salpay) && $salpay){
									echo $sal." :".$salpay."(" .$s. ")"."</br>";
									}
									if(isset($chanelpay) && $chanelpay){
									echo $chanel." :".$chanelpay."(" .$ch. ")"."</br>";
									}
									if(isset($comppay) && $comppay){
									echo $comp." :".$comppay."(" .$co. ")"."</br>";
									}
									if(isset($miscpay) && $miscpay){
									echo $misc." :".$miscpay."(" .$m. ")"."</br>";
									}
									if(isset($misc_inpay) && $misc_inpay){
									echo $misc_in." :".$misc_inpay."(" .$mi. ")"."</br>";
									}
									if(isset($restrpay) && $restrpay){
									echo $restr." :".$restrpay."(" .$r. ")"."</br>";
									}
									if(isset($Mservicepay) && $Mservicepay){
									echo $Mservice." :".$Mservicepay."(" .$ms. ")"."</br>";
									}
									if(isset($Guestpay) && $Guestpay){
									echo $Guest." :".$Guestpay."(" .$gp. ")"."</br>";
									}
									if(isset($lappay) && $lappay){
									echo $lap." :".$lappay."(" .$l. ")"."</br>";
									}
									if(isset($spppay) && $spppay){
									echo $spp." :".$spppay."(" .$sp. ")"."</br>";
									}
									if(isset($contrapay) && $contrapay){
									echo $contra." :".$contrapay."(" .$con++. ")"."</br>";
									}if(isset($charitypay) && $charitypay){
									echo $charit." :".$charitypay."(" .$ct. ")"."</br>";
									}
									?></td>
									<td align="center"><?php 
						
					if(isset($cash)) { echo $cash.": "."( ".$cas." )"."</br>";
					}
					if(isset($card) && $card){
						echo $card.": "."( ".$car." )"."</br>";
					}
					if(isset($fund) && $fund){
						echo $fund.": "."( ".$fu." )"."</br>";
					}
					if(isset($cheque) && $cheque){
						echo $cheque.": "."( ".$cq." )"."</br>";
					}
					if(isset($draft) && $draft){
						echo $draft.": "."( ".$dfr." )"."</br>";
					}
					if(isset($ewallet) && $ewallet){
						echo $ewallet.": "."( ".$ew." )"."</br>";
					}
						?></td>
              <td align="center"><?php 
					if(isset($income) && $income>0) {echo $income;}else{
						echo "0";
					}
						?></td> 
						
              <td align="center"><?php
					if(isset($exp) && $exp>0) {echo $exp;}else{
						echo "0";
					}
			?></td>
				
            </tr>
			
			<tr>
              <?php   $a_date = date("Y-10-01");
                    $maxDays=date('t');
					$income=0;
					$start_date=date("Y-10-01");
					$end_date=date('Y-10-t');
			     
					
                
                       
						$dailytransaction=$this->dashboard_model->transaction_report($start_date,$end_date);
						
							if(isset($dailytransaction) && $dailytransaction){	
				  $bk=0;
				  $bkpay=0;
			      $grefund='';
				  $grefundpay='';
				  $exp=0;
				  $broker='';
				  $brokerpay=0;
				  $tax=0;
				  $taxpay=0;
				  $vendor=0;
				  $vendorpay=0;
				  $cancel=0;
				  $cancelpay=0;
				  $sal=0;
				  $salpay=0;
				  $chanel=0;
				  $chanelpay=0;
				  $comp=0;
				  $comppay=0;
				  $misc=0;
				  $miscpay=0;
				  $misc_in=0;
				  $misc_inpay=0;
				  $restr=0;
				  $restrpay=0;
				  $Mservice=0;
				  $Mservicepay=0;
				  $Guest='';
				  $Guestpay=0;
				  $lap=0;
				  $lappay=0;
				  $spp=0;
				  $spppay=0;
				  $contra=0;
				  $contrapay=0;
				  $charity=0;
				  $charitypay=0;	
				  $count=0;				  
				  $b=0;				  
				  $g=0;	
				  $bro=0;	
				  $t=0;				  
				  $v=0;	
					$c=0;
					$ch=0;
					$s=0;
					$co=0;
					$m=0;
					$mi=0;
					$r=0;
					$ms=0;
					$gp=0;
					$l=0;
					$sp=0;
					$con=0;
					$cr=0;
					$ca=0;
					$cas=0;
					$car=0;
					$dfr=0;
					$ew=0;
					$fu=0;
					$cq=0;		
							
								
				  foreach($dailytransaction as $tranc){		
									 
							     $count=$count+$tranc->count; 
								 
								 $status=$tranc->t_payment_mode;
							     $type=$tranc->hotel_transaction_type_name; 
								 $Inc_Exp=$tranc->hotel_transaction_type_cat; 
							   
							    if($type=='Booking payment'){
								   $bk=$type;
								   $bkpay=$bkpay+$tranc->total;
								  $b=$b+$tranc->count;
							   }else if($type=='Guest refund'){
								   $grefund=$type;
								   $grefundpay=$grefundpay+$tranc->total;
								   $g+=$tranc->count; 
							   }else if($type=='Broker payment'){
								   $broker=$type;
								   $brokerpay=$brokerpay+$tranc->total;
								   $bro+=$tranc->count; 
							   }else if($type=='Tax paymemnt'){
								   $tax=$type;
								   $taxpay=$taxpay+$tranc->total;
								    $t+=$tranc->count;
							   }else if($type=='Vendor payment'){
								   $vendor=$type;
								   $vendorpay=$vendorpay+$tranc->total;
								    $v+=$tranc->count;
							   }else if($type=='Cancellation Fees'){
								   $cancel=$type;
								   $cancelpay=$cancelpay+$tranc->total;
								    $c+=$tranc->count;
							   }else if($type=='Salary payment'){
								   $sal=$type;
								   $salpay=$salpay+$tranc->total;
								    $s+=$tranc->count;
							   }else if($type=='Channel payment'){
								   $chanel=$type;
								   $chanelpay=$chanelpay+$tranc->total;
								    $ch+=$tranc->count;
							   }else if($type=='Compliance Payment'){
								   $comp=$type;
								   $comppay=$comppay+$tranc->total;
								    $co+=$tranc->count;
							   }else if($type=='Misc Payment'){
								   $misc=$type;
								   $miscpay=$miscpay+$tranc->total;
								    $m+=$tranc->count;
							   }else if($type=='Misc Income'){
								   $misc_in=$type;
								   $misc_inpay=$misc_inpay+$tranc->total;
								    $mi+=$tranc->count;
							   }else if($type=='Resturant payment'){
								   $restr=$type;
								   $restrpay=$restrpay+$tranc->total;
								    $r+=$tranc->count;
							   }else if($type=='Misc service payment'){
								   $Mservice=$type;
								   $Mservicepay=$Mservicepay+$tranc->total;
								    $ms+=$tranc->count;
							   }else if($type=='Guest Payment'){
								   $Guest=$type;
								   $Guestpay=$Guestpay+$tranc->total;
								    $gp+=$tranc->count;
							   }else if($type=='Local Authority Payment'){
								   $lap=$type;
								   $lappay=$lappay+$tranc->total;
								    $l+=$tranc->count;
							   }else if($type=='Service Provider Payment'){
								   $spp=$type;
								   $spppay=$spppay+$tranc->total;
								    $sp+=$tranc->count;
							   }else if($type=='Contractor Payment'){
								   $contra=$type;
								   $contrapay=$contrapay+$tranc->total;
								    $con+=$tranc->count;
							   }else if($type=='Charity'){
								   $charity=$type;
								   $charitypay=$charitypay+$tranc->total;
								    $cr+=$tranc->count;
							   }
								if($Inc_Exp=='Income'){
								$income=$income+$tranc->total;
								}else{
									$exp=$exp+$tranc->total;
								}
								
				  if($status=='cash' || $status=='Cash'){
								$cash=$status;
								$cas++;
							}
							if($status=='cards'){
								$card=$status;
								$car++;
							}		
						if($status=='fund'){
								$fund=$status;
								$fu++;
							}
							if($status=='cheque'){
								$cheque=$status;
								$cq++;
							}
							if($status=='draft'){
								$draft=$status;
								$dfr++;
							}	
							if($status=='ewallet'){
								$ewallet=$status;
								$ew++;
							}	
							
							}
							//$gross=$rmAmt+$rrtax+$mealPlan+$mpTax;
							}
							
					
						?>
              <td align="center"><?php  echo 10;?></td>
              <td align="center"> <a href="<?php echo base_url();?>reports/daily_transaction_summary?m=<?php echo 10?>"> <?php  echo date("F", strtotime($a_date)); ?> </a> <?php echo " ".date("Y", strtotime($a_date))?></td>
              <td align="center"><?php echo date("t", strtotime($a_date)); ?></td>
              <td align="center"><?php //Room Count
										echo $count;?></td>
              <td align="center"><?php
					//Occupancy    
									if(isset($bkpay) && $bkpay){
									echo $bk." :".$bkpay."(".$b. ")"."</br>";
									}
									if(isset($grefundpay) && $grefundpay){
									echo $grefund." :".$grefundpay."(" .$g. ")"."</br>";
									}
									if(isset($brokerpay) && $brokerpay){
									echo $broker." :".$brokerpay."(" .$bro. ")"."</br>";
									}
									if(isset($taxpay) && $taxpay){
									echo $tax." :".$taxpay."(" .$t. ")"."</br>";
									}
									if(isset($vendorpay) && $vendorpay){
									echo $vendor." :".$vendorpay."(" .$v. ")"."</br>";
									}
									if(isset($cancelpay) && $cancelpay){
									echo $cancel." :".$cancelpay."(" .$c. ")"."</br>";
									}
									if(isset($salpay) && $salpay){
									echo $sal." :".$salpay."(" .$s. ")"."</br>";
									}
									if(isset($chanelpay) && $chanelpay){
									echo $chanel." :".$chanelpay."(" .$ch. ")"."</br>";
									}
									if(isset($comppay) && $comppay){
									echo $comp." :".$comppay."(" .$co. ")"."</br>";
									}
									if(isset($miscpay) && $miscpay){
									echo $misc." :".$miscpay."(" .$m. ")"."</br>";
									}
									if(isset($misc_inpay) && $misc_inpay){
									echo $misc_in." :".$misc_inpay."(" .$mi. ")"."</br>";
									}
									if(isset($restrpay) && $restrpay){
									echo $restr." :".$restrpay."(" .$r. ")"."</br>";
									}
									if(isset($Mservicepay) && $Mservicepay){
									echo $Mservice." :".$Mservicepay."(" .$ms. ")"."</br>";
									}
									if(isset($Guestpay) && $Guestpay){
									echo $Guest." :".$Guestpay."(" .$gp. ")"."</br>";
									}
									if(isset($lappay) && $lappay){
									echo $lap." :".$lappay."(" .$l. ")"."</br>";
									}
									if(isset($spppay) && $spppay){
									echo $spp." :".$spppay."(" .$sp. ")"."</br>";
									}
									if(isset($contrapay) && $contrapay){
									echo $contra." :".$contrapay."(" .$con++. ")"."</br>";
									}if(isset($charitypay) && $charitypay){
									echo $charit." :".$charitypay."(" .$ct. ")"."</br>";
									}
									?></td>
									<td align="center"><?php 
						
					if(isset($cash)) { echo $cash.": "."( ".$cas." )"."</br>";
					}
					if(isset($card) && $card){
						echo $card.": "."( ".$car." )"."</br>";
					}
					if(isset($fund) && $fund){
						echo $fund.": "."( ".$fu." )"."</br>";
					}
					if(isset($cheque) && $cheque){
						echo $cheque.": "."( ".$cq." )"."</br>";
					}
					if(isset($draft) && $draft){
						echo $draft.": "."( ".$dfr." )"."</br>";
					}
					if(isset($ewallet) && $ewallet){
						echo $ewallet.": "."( ".$ew." )"."</br>";
					}
						?></td>
              <td align="center"><?php 
					if(isset($income) && $income>0) {echo $income;}else{
						echo "0";
					}
						?></td> 
						
              <td align="center"><?php
					if(isset($exp) && $exp>0) {echo $exp;}else{
						echo "0";
					}
			?></td>
				
            </tr>
			
			<tr>
              <?php  

			//echo 	$beg = date('W', strtotime(date("Y-12-01")));
			
			 $a_date = date("Y-11-01");
                    $maxDays=date('t');
					$income=0;
					$start_date=date("Y-11-01");
					$end_date=date('Y-11-t');
			     
					
                
                       
						$dailytransaction=$this->dashboard_model->transaction_report($start_date,$end_date);
						
							if(isset($dailytransaction) && $dailytransaction){	
				  $bk=0;
				  $bkpay=0;
			      $grefund='';
				  $grefundpay='';
				  $exp=0;
				  $broker='';
				  $brokerpay=0;
				  $tax=0;
				  $taxpay=0;
				  $vendor=0;
				  $vendorpay=0;
				  $cancel=0;
				  $cancelpay=0;
				  $sal=0;
				  $salpay=0;
				  $chanel=0;
				  $chanelpay=0;
				  $comp=0;
				  $comppay=0;
				  $misc=0;
				  $miscpay=0;
				  $misc_in=0;
				  $misc_inpay=0;
				  $restr=0;
				  $restrpay=0;
				  $Mservice=0;
				  $Mservicepay=0;
				  $Guest='';
				  $Guestpay=0;
				  $lap=0;
				  $lappay=0;
				  $spp=0;
				  $spppay=0;
				  $contra=0;
				  $contrapay=0;
				  $charity=0;
				  $charitypay=0;	
				  $count=0;				  
				  $b=0;				  
				  $g=0;	
				  $bro=0;	
				  $t=0;				  
				  $v=0;	
					$c=0;
					$ch=0;
					$s=0;
					$co=0;
					$m=0;
					$mi=0;
					$r=0;
					$ms=0;
					$gp=0;
					$l=0;
					$sp=0;
					$con=0;
					$cr=0;
					$ca=0;
					$cas=0;
					$car=0;
					$dfr=0;
					$ew=0;
					$fu=0;
					$cq=0;		
							
								
				  foreach($dailytransaction as $tranc){		
									 
							     $count=$count+$tranc->count; 
								 
								 $status=$tranc->t_payment_mode;
							     $type=$tranc->hotel_transaction_type_name; 
								 $Inc_Exp=$tranc->hotel_transaction_type_cat; 
							   
							    if($type=='Booking payment'){
								   $bk=$type;
								   $bkpay=$bkpay+$tranc->total;
								  $b=$b+$tranc->count;
							   }else if($type=='Guest refund'){
								   $grefund=$type;
								   $grefundpay=$grefundpay+$tranc->total;
								   $g+=$tranc->count; 
							   }else if($type=='Broker payment'){
								   $broker=$type;
								   $brokerpay=$brokerpay+$tranc->total;
								   $bro+=$tranc->count; 
							   }else if($type=='Tax paymemnt'){
								   $tax=$type;
								   $taxpay=$taxpay+$tranc->total;
								    $t+=$tranc->count;
							   }else if($type=='Vendor payment'){
								   $vendor=$type;
								   $vendorpay=$vendorpay+$tranc->total;
								    $v+=$tranc->count;
							   }else if($type=='Cancellation Fees'){
								   $cancel=$type;
								   $cancelpay=$cancelpay+$tranc->total;
								    $c+=$tranc->count;
							   }else if($type=='Salary payment'){
								   $sal=$type;
								   $salpay=$salpay+$tranc->total;
								    $s+=$tranc->count;
							   }else if($type=='Channel payment'){
								   $chanel=$type;
								   $chanelpay=$chanelpay+$tranc->total;
								    $ch+=$tranc->count;
							   }else if($type=='Compliance Payment'){
								   $comp=$type;
								   $comppay=$comppay+$tranc->total;
								    $co+=$tranc->count;
							   }else if($type=='Misc Payment'){
								   $misc=$type;
								   $miscpay=$miscpay+$tranc->total;
								    $m+=$tranc->count;
							   }else if($type=='Misc Income'){
								   $misc_in=$type;
								   $misc_inpay=$misc_inpay+$tranc->total;
								    $mi+=$tranc->count;
							   }else if($type=='Resturant payment'){
								   $restr=$type;
								   $restrpay=$restrpay+$tranc->total;
								    $r+=$tranc->count;
							   }else if($type=='Misc service payment'){
								   $Mservice=$type;
								   $Mservicepay=$Mservicepay+$tranc->total;
								    $ms+=$tranc->count;
							   }else if($type=='Guest Payment'){
								   $Guest=$type;
								   $Guestpay=$Guestpay+$tranc->total;
								    $gp+=$tranc->count;
							   }else if($type=='Local Authority Payment'){
								   $lap=$type;
								   $lappay=$lappay+$tranc->total;
								    $l+=$tranc->count;
							   }else if($type=='Service Provider Payment'){
								   $spp=$type;
								   $spppay=$spppay+$tranc->total;
								    $sp+=$tranc->count;
							   }else if($type=='Contractor Payment'){
								   $contra=$type;
								   $contrapay=$contrapay+$tranc->total;
								    $con+=$tranc->count;
							   }else if($type=='Charity'){
								   $charity=$type;
								   $charitypay=$charitypay+$tranc->total;
								    $cr+=$tranc->count;
							   }
								if($Inc_Exp=='Income'){
								$income=$income+$tranc->total;
								}else{
									$exp=$exp+$tranc->total;
								}
								
				  if($status=='cash' || $status=='Cash'){
								$cash=$status;
								$cas++;
							}
							if($status=='cards'){
								$card=$status;
								$car++;
							}		
						if($status=='fund'){
								$fund=$status;
								$fu++;
							}
							if($status=='cheque'){
								$cheque=$status;
								$cq++;
							}
							if($status=='draft'){
								$draft=$status;
								$dfr++;
							}	
							if($status=='ewallet'){
								$ewallet=$status;
								$ew++;
							}	
							
							}
							//$gross=$rmAmt+$rrtax+$mealPlan+$mpTax;
							}
							
					
						?>
              <td align="center"><?php  echo 11;?></td>
              <td align="center"> <a href="<?php echo base_url();?>reports/daily_transaction_summary?m=<?php echo 11?>"> <?php  echo date("F", strtotime($a_date)); ?> </a> <?php echo " ".date("Y", strtotime($a_date))?></td>
              <td align="center"><?php echo date("t", strtotime($a_date)); ?></td>
              <td align="center"><?php //Room Count
										echo $count;?></td>
              <td align="center"><?php
					//Occupancy    
									if(isset($bkpay) && $bkpay){
									echo $bk." :".$bkpay."(".$b. ")"."</br>";
									}
									if(isset($grefundpay) && $grefundpay){
									echo $grefund." :".$grefundpay."(" .$g. ")"."</br>";
									}
									if(isset($brokerpay) && $brokerpay){
									echo $broker." :".$brokerpay."(" .$bro. ")"."</br>";
									}
									if(isset($taxpay) && $taxpay){
									echo $tax." :".$taxpay."(" .$t. ")"."</br>";
									}
									if(isset($vendorpay) && $vendorpay){
									echo $vendor." :".$vendorpay."(" .$v. ")"."</br>";
									}
									if(isset($cancelpay) && $cancelpay){
									echo $cancel." :".$cancelpay."(" .$c. ")"."</br>";
									}
									if(isset($salpay) && $salpay){
									echo $sal." :".$salpay."(" .$s. ")"."</br>";
									}
									if(isset($chanelpay) && $chanelpay){
									echo $chanel." :".$chanelpay."(" .$ch. ")"."</br>";
									}
									if(isset($comppay) && $comppay){
									echo $comp." :".$comppay."(" .$co. ")"."</br>";
									}
									if(isset($miscpay) && $miscpay){
									echo $misc." :".$miscpay."(" .$m. ")"."</br>";
									}
									if(isset($misc_inpay) && $misc_inpay){
									echo $misc_in." :".$misc_inpay."(" .$mi. ")"."</br>";
									}
									if(isset($restrpay) && $restrpay){
									echo $restr." :".$restrpay."(" .$r. ")"."</br>";
									}
									if(isset($Mservicepay) && $Mservicepay){
									echo $Mservice." :".$Mservicepay."(" .$ms. ")"."</br>";
									}
									if(isset($Guestpay) && $Guestpay){
									echo $Guest." :".$Guestpay."(" .$gp. ")"."</br>";
									}
									if(isset($lappay) && $lappay){
									echo $lap." :".$lappay."(" .$l. ")"."</br>";
									}
									if(isset($spppay) && $spppay){
									echo $spp." :".$spppay."(" .$sp. ")"."</br>";
									}
									if(isset($contrapay) && $contrapay){
									echo $contra." :".$contrapay."(" .$con++. ")"."</br>";
									}if(isset($charitypay) && $charitypay){
									echo $charit." :".$charitypay."(" .$ct. ")"."</br>";
									}
									?></td>
									<td align="center"><?php 
						
					if(isset($cash)) { echo $cash.": "."( ".$cas." )"."</br>";
					}
					if(isset($card) && $card){
						echo $card.": "."( ".$car." )"."</br>";
					}
					if(isset($fund) && $fund){
						echo $fund.": "."( ".$fu." )"."</br>";
					}
					if(isset($cheque) && $cheque){
						echo $cheque.": "."( ".$cq." )"."</br>";
					}
					if(isset($draft) && $draft){
						echo $draft.": "."( ".$dfr." )"."</br>";
					}
					if(isset($ewallet) && $ewallet){
						echo $ewallet.": "."( ".$ew." )"."</br>";
					}
						?></td>
              <td align="center"><?php 
					if(isset($income) && $income>0) {echo $income;}else{
						echo "0";
					}
						?></td> 
						
              <td align="center"><?php
					if(isset($exp) && $exp>0) {echo $exp;}else{
						echo "0";
					}
			?></td>
				
            </tr>
			
						 <tr>
              <?php   
					$a_date = date("Y-12-01");
                    $maxDays=date('t');
					$income=0;
					$start_date=date("Y-12-01");
					$end_date=date('Y-12-t');
			     
					
                
                       
						$dailytransaction=$this->dashboard_model->transaction_report($start_date,$end_date);
						
							if(isset($dailytransaction) && $dailytransaction){	
				  $bk=0;
				  $bkpay=0;
			      $grefund='';
				  $grefundpay='';
				  $exp=0;
				  $broker='';
				  $brokerpay=0;
				  $tax=0;
				  $taxpay=0;
				  $vendor=0;
				  $vendorpay=0;
				  $cancel=0;
				  $cancelpay=0;
				  $sal=0;
				  $salpay=0;
				  $chanel=0;
				  $chanelpay=0;
				  $comp=0;
				  $comppay=0;
				  $misc=0;
				  $miscpay=0;
				  $misc_in=0;
				  $misc_inpay=0;
				  $restr=0;
				  $restrpay=0;
				  $Mservice=0;
				  $Mservicepay=0;
				  $Guest='';
				  $Guestpay=0;
				  $lap=0;
				  $lappay=0;
				  $spp=0;
				  $spppay=0;
				  $contra=0;
				  $contrapay=0;
				  $charity=0;
				  $charitypay=0;	
				  $count=0;				  
				  $b=0;				  
				  $g=0;	
				  $bro=0;	
				  $t=0;				  
				  $v=0;	
					$c=0;
					$ch=0;
					$s=0;
					$co=0;
					$m=0;
					$mi=0;
					$r=0;
					$ms=0;
					$gp=0;
					$l=0;
					$sp=0;
					$con=0;
					$cr=0;
					$ca=0;
					$cas=0;
					$car=0;
					$dfr=0;
					$ew=0;
					$fu=0;
					$cq=0;		
							
								
				  foreach($dailytransaction as $tranc){		
									 
							     $count=$count+$tranc->count; 
								 
								 $status=$tranc->t_payment_mode;
							     $type=$tranc->hotel_transaction_type_name; 
								 $Inc_Exp=$tranc->hotel_transaction_type_cat; 
							   
							    if($type=='Booking payment'){
								   $bk=$type;
								   $bkpay=$bkpay+$tranc->total;
								  $b=$b+$tranc->count;
							   }else if($type=='Guest refund'){
								   $grefund=$type;
								   $grefundpay=$grefundpay+$tranc->total;
								   $g+=$tranc->count; 
							   }else if($type=='Broker payment'){
								   $broker=$type;
								   $brokerpay=$brokerpay+$tranc->total;
								   $bro+=$tranc->count; 
							   }else if($type=='Tax paymemnt'){
								   $tax=$type;
								   $taxpay=$taxpay+$tranc->total;
								    $t+=$tranc->count;
							   }else if($type=='Vendor payment'){
								   $vendor=$type;
								   $vendorpay=$vendorpay+$tranc->total;
								    $v+=$tranc->count;
							   }else if($type=='Cancellation Fees'){
								   $cancel=$type;
								   $cancelpay=$cancelpay+$tranc->total;
								    $c+=$tranc->count;
							   }else if($type=='Salary payment'){
								   $sal=$type;
								   $salpay=$salpay+$tranc->total;
								    $s+=$tranc->count;
							   }else if($type=='Channel payment'){
								   $chanel=$type;
								   $chanelpay=$chanelpay+$tranc->total;
								    $ch+=$tranc->count;
							   }else if($type=='Compliance Payment'){
								   $comp=$type;
								   $comppay=$comppay+$tranc->total;
								    $co+=$tranc->count;
							   }else if($type=='Misc Payment'){
								   $misc=$type;
								   $miscpay=$miscpay+$tranc->total;
								    $m+=$tranc->count;
							   }else if($type=='Misc Income'){
								   $misc_in=$type;
								   $misc_inpay=$misc_inpay+$tranc->total;
								    $mi+=$tranc->count;
							   }else if($type=='Resturant payment'){
								   $restr=$type;
								   $restrpay=$restrpay+$tranc->total;
								    $r+=$tranc->count;
							   }else if($type=='Misc service payment'){
								   $Mservice=$type;
								   $Mservicepay=$Mservicepay+$tranc->total;
								    $ms+=$tranc->count;
							   }else if($type=='Guest Payment'){
								   $Guest=$type;
								   $Guestpay=$Guestpay+$tranc->total;
								    $gp+=$tranc->count;
							   }else if($type=='Local Authority Payment'){
								   $lap=$type;
								   $lappay=$lappay+$tranc->total;
								    $l+=$tranc->count;
							   }else if($type=='Service Provider Payment'){
								   $spp=$type;
								   $spppay=$spppay+$tranc->total;
								    $sp+=$tranc->count;
							   }else if($type=='Contractor Payment'){
								   $contra=$type;
								   $contrapay=$contrapay+$tranc->total;
								    $con+=$tranc->count;
							   }else if($type=='Charity'){
								   $charity=$type;
								   $charitypay=$charitypay+$tranc->total;
								    $cr+=$tranc->count;
							   }
								if($Inc_Exp=='Income'){
								$income=$income+$tranc->total;
								}else{
									$exp=$exp+$tranc->total;
								}
								
				  if($status=='cash' || $status=='Cash'){
								$cash=$status;
								$mod=$this->dashboard_model->mode_count($date,$status);
								//echo "cash".$mod->md;
								$cas=$mod->md;
							}
							else if($status=='cards'){
								$card=$status;
								$mod=$this->dashboard_model->mode_count($date,$card);
								$car=$mod->md;
								
							}		
						else if($status=='fund'){
								$fund=$status;
								$mod=$this->dashboard_model->mode_count($date,$fund);
								$fu=$mod->md;
								
							}
							else if($status=='cheque'){
								$cheque=$status;
								$mod=$this->dashboard_model->mode_count($date,$cheque);
								$cq=$mod->md;
								
							}
							else if($status=='draft'){
								$draft=$status;
								$mod=$this->dashboard_model->mode_count($date,$draft);
								$dfr=$mod->md;
								
							}	
							else if($status=='ewallet'){
								$ewallet=$status;
								$mod=$this->dashboard_model->mode_count($date,$ewallet);
								$ew=$mod->md;
								
							}else{
								$cas=0;
								$car=0;
								$dfr=0;
								$ew=0;
								$fu=0;
								$cq=0;
								$Pc=0;
							}		
							
							}
							//$gross=$rmAmt+$rrtax+$mealPlan+$mpTax;
							}
							
					
						?>
              <td align="center"><?php  echo 12;?></td>
              <td align="center"> <a href="<?php echo base_url();?>reports/daily_transaction_summary?m=<?php echo 12?>"> <?php  echo date("F", strtotime($a_date)); ?> </a> <?php echo " ".date("Y", strtotime($a_date))?></td>
              <td align="center"><?php echo date("t", strtotime($a_date)); ?></td>
              <td align="center"><?php //Room Count
										echo $count;?></td>
              <td align="center"><?php
					//Occupancy    
									if(isset($bkpay) && $bkpay){
									echo $bk." :".$bkpay."(".$b. ")"."</br>";
									}
									if(isset($grefundpay) && $grefundpay){
									echo $grefund." :".$grefundpay."(" .$g. ")"."</br>";
									}
									if(isset($brokerpay) && $brokerpay){
									echo $broker." :".$brokerpay."(" .$bro. ")"."</br>";
									}
									if(isset($taxpay) && $taxpay){
									echo $tax." :".$taxpay."(" .$t. ")"."</br>";
									}
									if(isset($vendorpay) && $vendorpay){
									echo $vendor." :".$vendorpay."(" .$v. ")"."</br>";
									}
									if(isset($cancelpay) && $cancelpay){
									echo $cancel." :".$cancelpay."(" .$c. ")"."</br>";
									}
									if(isset($salpay) && $salpay){
									echo $sal." :".$salpay."(" .$s. ")"."</br>";
									}
									if(isset($chanelpay) && $chanelpay){
									echo $chanel." :".$chanelpay."(" .$ch. ")"."</br>";
									}
									if(isset($comppay) && $comppay){
									echo $comp." :".$comppay."(" .$co. ")"."</br>";
									}
									if(isset($miscpay) && $miscpay){
									echo $misc." :".$miscpay."(" .$m. ")"."</br>";
									}
									if(isset($misc_inpay) && $misc_inpay){
									echo $misc_in." :".$misc_inpay."(" .$mi. ")"."</br>";
									}
									if(isset($restrpay) && $restrpay){
									echo $restr." :".$restrpay."(" .$r. ")"."</br>";
									}
									if(isset($Mservicepay) && $Mservicepay){
									echo $Mservice." :".$Mservicepay."(" .$ms. ")"."</br>";
									}
									if(isset($Guestpay) && $Guestpay){
									echo $Guest." :".$Guestpay."(" .$gp. ")"."</br>";
									}
									if(isset($lappay) && $lappay){
									echo $lap." :".$lappay."(" .$l. ")"."</br>";
									}
									if(isset($spppay) && $spppay){
									echo $spp." :".$spppay."(" .$sp. ")"."</br>";
									}
									if(isset($contrapay) && $contrapay){
									echo $contra." :".$contrapay."(" .$con++. ")"."</br>";
									}if(isset($charitypay) && $charitypay){
									echo $charit." :".$charitypay."(" .$ct. ")"."</br>";
									}
									?></td>
									<td align="center"><?php 
						
					if(isset($cash)) { echo $cash.": "."( ".$cas." )"."</br>";
					}
					if(isset($card) && $card){
						echo $card.": "."( ".$car." )"."</br>";
					}
					if(isset($fund) && $fund){
						echo $fund.": "."( ".$fu." )"."</br>";
					}
					if(isset($cheque) && $cheque){
						echo $cheque.": "."( ".$cq." )"."</br>";
					}
					if(isset($draft) && $draft){
						echo $draft.": "."( ".$dfr." )"."</br>";
					}
					if(isset($ewallet) && $ewallet){
						echo $ewallet.": "."( ".$ew." )"."</br>";
					}
						?></td>
              <td align="center"><?php 
					if(isset($income) && $income>0) {echo $income;}else{
						echo "0";
					}
						?></td> 
						
              <td align="center"><?php
					if(isset($exp) && $exp>0) {echo $exp;}else{
						echo "0";
					}
			?></td>
				
            </tr>
			
          </tbody>
        </table>
      </div>
    </div>

  </div>
</div>
<!-- END PAGE CONTENT--> 

