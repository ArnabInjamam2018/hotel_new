<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<?php

                            $form = array(
                                'class' 			=> 'form-body',
                                'id'				=> 'form',
                                'method'			=> 'post',								
                            );
							
							

                            echo form_open_multipart('dashboard/transfer_asset',$form);

                            ?>


<div class="modal fade" id="basic1" tabindex="-1" role="basic" aria-hidden="true" >
  <div class="modal-dialog ">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Transfer Asset</h4>
      </div>
      <div class="modal-body">
        <div class="row">
		
          <div class="col-md-4">
            <div id="wh"class="form-group form-md-line-input">
              <select class="form-control bs-select" onchange="get_item_name(this.value)" name="from_warehouse" id="from_warehouse" required >
                <option value="" disabled selected>From Warehouse</option>
                <?php if(isset($warehouse)){
                    //print_r($warehouse);
                    foreach($warehouse as $wh){
                  ?>
                <option value="<?php echo $wh->id;?>"><?php echo $wh->name;?></option>
                <?php }}?>
              </select>
              <label></label>
              <span class="help-block">From Warehouse *</span> </div>
          </div>
			<div class="col-md-4">
          <div class="form-group form-md-line-input" id="hd">
              
                
                  <select class="form-control bs-select" id="a_name" name="item_id" required >
                    <option value="">Select Item</option>
                  </select>
                  <label></label>
                  <span class="help-block">Select Item *</span> 
              
          </div>
          </div>
          <input type="hidden" name="qty1" id="qty1" value="" class="form-control">
          <div class="col-md-4">
          <div id="wh1"class="form-group form-md-line-input">
            <select class="form-control bs-select"   name="to_warehouse" id="to_warehouse" required onchange="chekDuplicateWh(this.value)">
              <option value="" disabled selected>To Warehouse</option>
              <?php if(isset($warehouse)){
				//print_r($warehouse);
				foreach($warehouse as $wh){
			  ?>
              <option value="<?php echo $wh->id;?>"><?php echo $wh->name;?></option>
              <?php }}?>
            </select>
            <label></label>
            <span class="help-block">To Warehouse *</span> </div>
          </div>
          <div class="col-md-4">
              <div id="wh1"class="form-group form-md-line-input">
                <input autocomplete="off" type="text" class="form-control" name="qty" id="qty" onkeyup="check_qty(this.value)" required="required"  placeholder="QTY *">
                <label></label>
                <span class="help-block">QTY *</span> </div>
          </div>
          <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select  class="form-control bs-select"  name="admin_id" required>
              <option value="" selected="selected" disabled="disabled">Select Admin Name</option>
              <?php if(isset($admin)&& $admin){

											foreach ($admin as $value) {
												# code...
										?>
              <option value="<?php echo $value->admin_id; ?>"><?php echo $value->admin_first_name." ".$value->admin_middle_name." ".$value->admin_last_name?></option>
              <?php
										}}
									?>
            </select>
            <label></label>
            <span class="help-block">Approve By *</span> </div>
          </div>
          <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <textarea autocomplete="off" type="text" class="form-control"  name="note1" placeholder="Note"></textarea>
                <label></label>
                <span class="help-block">Note *</span> </div>
          </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-6">
            <div class="fileinput fileinput-new" data-provides="fileinput">
              <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
              <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select GRN Image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('grn_image');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
            </div>
          </div>
          <div class="form-group form-md-line-input form-md-floating-label col-md-6">
            <div class="fileinput fileinput-new" data-provides="fileinput">
              <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/> </div>
              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
              <div> <span class="btn default btn-file"> <span class="fileinput-new"> Transfer Voucher image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('transfr_vou_image');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn blue" >Save changes</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<?php echo form_close();?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> Asset Transfers <i class="fa fa-edit"></i>
      <?php if(isset($stat) && $stat){echo $stat;}?>
    </div>
    <div class="actions">
    	<a href="javascript:void(0);" class="btn btn-circle green btn-outline btn-sm" onclick="show()"> <i class="fa fa-plus"></i>Transfer Asset </a>
    </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <?php

	                           
								if(isset($start_date) && isset($end_date))
								{
								$s_date=$start_date;
								$e_date=$end_date;
								}
								else
								{
								$s_date="";
								$e_date="";
								}
	                            ?>
        <div class="col-md-7 form-inline">
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
          </div>
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" value="<?php if(isset($end_date)){ echo $end_date;}?>" id="t_dt_to" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
          </div>
          <button class="btn btn-default" type="button" onclick="date_filter()"><i class="fa fa-search" aria-hidden="true"></i></button>
        </div>        
      </div>
    </div>
    <div id="table1">
      <table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr> 
            <!-- <th scope="col">
                                Select
                            </th>-->
            <th scope="col">#</th>
            <th scope="col">GRN</th>
            <th scope="col">Transfer Voucher</th>
            <th scope="col">Date </th>
            <th scope="col">Item Name</th>
            <th scope="col">From Warehouse </th>
            <th scope="col">To Warehouse</th>
            <th scope="col">Qty</th>
            <th scope="col">Approve By </th>
			<th scope="col">Note</th>
            <th scope="col"> Action </th>
          </tr>
        </thead>
        <tbody>
          <?php 
			$srl_no=0;
		  if(isset($transfer_asset) && $transfer_asset ){
				//print_r($transfer_asset);//exit;
			foreach($transfer_asset as $asset){
			$srl_no++;
		  ?>
          <tr>
            <td><?php echo $srl_no; ?></td>
            <td width="5%"><a class="single_2" href="<?php echo base_url();?>upload/asset/<?php if( $asset->grn_image== '') { echo "no_images.png"; } else { echo $asset->grn_image; }?>"><img src="<?php echo base_url();?>upload/asset/<?php if( $asset->grn_image== '') { echo "no_images.png"; } else { echo $asset->grn_image; }?>" alt="" style="width:100%;"/></a></td>
            <td width="5%" align="center"><a class="single_2" href="<?php echo base_url();?>upload/asset/<?php if( $asset->transfr_vou_image== '') { echo "no_images.png"; } else { echo $asset->transfr_vou_image; }?>"><img src="<?php echo base_url();?>upload/asset/<?php if( $asset->transfr_vou_image== '') { echo "no_images.png"; } else { echo $asset->transfr_vou_image; }?>" alt="" style="width:55%;"/></a></td>
            <td><?php echo $asset->date;?></td>
            <td><?php  if(isset($asset->item_id)){
			    $item_name=$this->dashboard_model->get_stock_item($asset->item_id);
					if(isset($item_name)){
			echo $item_name->a_name;
					}  
		   }?></td>
            <td><?php 
		   $warehouse_id=$asset->from_warehouse_id;
		   $wharehouse=$this->dashboard_model->get_warehouse($warehouse_id);
			echo $wharehouse->name;
		   ?></td>
            <td><?php $id=$asset->to_warehouse_id;
		    $wharehouse=$this->dashboard_model->get_warehouse($id);
			echo $wharehouse->name;
		   ?></td>
            <td><?php echo $asset->qty;?></td>
            
            <td><?php $admin_id=$asset->approv_admin_id;
					$admin_name=$this->dashboard_model->get_admin($admin_id);
					if(isset($admin_name) && $admin_name){
					foreach($admin_name as $name){
					echo $name->admin_first_name.$name->admin_middle_name.$name->admin_last_name;
					}}
		   ?></td>
		   <td><?php echo $asset->Note;?></td>
            <td><a onclick="soft_delete('<?php echo $asset->id;?>')" data-toggle="modal"  class="btn red btn-xs" ><i class="fa fa-trash"></i></a></td>
          </tr>
          <?php }}?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script>

function chekDuplicateWh(){
	var to_wh=$("#to_warehouse").val();
	var from_wh=$("#from_warehouse").val();
	if(parseInt(to_wh)==parseInt(from_wh)){
		$('#basic1').modal('hide');
			swal({
                            title: "From Warehouse & To Warehouse Must be Diffrent",
                            text: "",
                            type: "warning"
                        },
                        function(){
							
							// document.getElementById("qty").value = "";
                           // location.reload();
							$('#basic1').modal('show');
							$("#to_warehouse").val('');
							$("#from_warehouse").val('');
							$("#a_name").val('');
                        });
	}
}


function date_filter(){
	var start_date=$("#t_dt_frm").val();
	var end_date=$("#t_dt_to").val();

	//alert(start_date);
	//alert(end_date);
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/transfer_asset_date",
	   data: {fdate:start_date,edate:end_date},
	   success: function(msg){
		   
		   if(msg==0){
			   swal({
                            title: "No Data Found",
                            text: "",
                            type: "warning"
                        },
                        function(){
							
							// document.getElementById("qty").value = "";
                           // location.reload();
							
                        });
		   }
		   
		   
		 else{  
		  $('#table1').html(msg);
				   $('#dataTable2').dataTable( {
						"pageLength": 10 
    } );
	
		 }
	   }
	});
	
	
	
	}
   function get_item_name(val){
	//alert(val);
	//alert(val1);
	
	/// var val= document.getElementById("a_type").value;
	// var val1= document.getElementById("a_category").value;
	//alert(val);
	//alert(val1);
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/get_item_name_by_wh_id",
	   data: {data:val},
	   success: function(msg){
		  // alert(msg);
		   $("#hd").html(msg);
	   }
	});
}

function get_item_qty(val){
	//alert(val);
	var wh_id=$("#from_warehouse").val();
	//alert(wh_id);
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/get_qty",
	   data: {data:wh_id,item_id:val},
	   success: function(msg){
		   //alert(msg);
		    $("#qty1").val(msg);
	   }
	});
	
	
	
	
}

	function check_qty(val){
		//alert(val);
		var qty=$("#qty1").val();
		//alert(qty);
		if(parseInt(val)>parseInt(qty)){
			$('#basic1').modal('hide');
			swal({
                            title: "Enter Minimum Qty",
                            text: "",
                            type: "warning"
                        },
                        function(){
							
							// document.getElementById("qty").value = "";
                           // location.reload();
							$('#basic1').modal('show');
							$('#qty').val('');
                        });
		}
	}

	function show(){
		
			$('#basic1').modal('show');
			
	}
	function show1(){
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/data_transferasset",
                data:{id:id},
                success:function(data)
                {
			
				   $('#asdf123').val('sdvsdv');
				   $('#asdf123').val('sdvsdv');
				   $('#asdf123').val('sdvsdv');
				   $('#asdf123').val('sdvsdv');
				   $('#asdf123').val('sdvsdv');
				   $('#asdf123').val('sdvsdv');
                }
            });
			
		
		
		
			$('#basic2').modal('show');
			
	}
	
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          



            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_transfer_asset?id="+id,
                data:{},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });



        });
    }
	
	
	function check(id){
		var form=$('#from_warehouse').val();
		var to=$('#to_warehouse').val();
		//alert(form);
		//alert(to);
		
		
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/get_warehouse_stock",
                data:{id:form},
                success:function(data)
                {
                   // alert(data.qty);
					//'item_id'=>$query->item_id
					//'warehouse_id'=>$query->warehouse_id
                   $('#hid1').val(data.qty);
					
					
                }
            });
			
			if(form == to ){
			alert('please change');
			$('#from_warehouse').val('0');
			$('#to_warehouse').val('0');
		}
		
	}
	
	
	function check1(id){
		var form=$('#from_warehouse').val();
		var to=$('#to_warehouse').val();
		//alert(form);
		//alert(to);
		if(form == to ){
			alert('please change');
			$('#from_warehouse').val('0');
			$('#to_warehouse').val('0');
		}
		
	}
	
	
function check_sub(){
  document.getElementById('form_date').submit();
}
function check_sub1(){
  document.getElementById('form').submit();
}
	
	
</script> 
