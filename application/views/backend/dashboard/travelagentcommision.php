<div class="portlet light borderd">
  <div class="portlet-title">
    <div class="caption"> <i class="glyphicon glyphicon-bed"></i>Travel Agent Report </div>
    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-5">
          <?php

                        $form = array(
                            'class'       => 'form-inline',
                            'id'        => 'form_date',
                            'method'      => 'post'
                        );

                        echo form_open_multipart('reports/travelagentcommission',$form);

                        ?>
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" id="t_dt_frm" value="<?php if(isset($start_date)){ echo $start_date;}?>"  name="t_dt_frm" class="form-control date-picker" placeholder="Start Date">
          </div>
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" name="t_dt_to" value="<?php if(isset($end_date)){ echo $end_date;}?>"  class="form-control date-picker" placeholder="End Date">
          </div>
          <button class="btn btn-default" onclick="check_sub()" type="submit">Search</button>
          <?php echo form_close(); ?> </div>
        <div class="col-md-2 pull-right">
        	<select class="bs-select form-control" data-style="btn-default" onchange="get_group(this.value)">
                <option value="3">All</option>
                <option value="0">Single</option>
                <option value="1">Group</option>
            </select>
        	<!--<div class="btn-group">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"> Dropdown
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu" role="menu">
				<li>
                        <a href="javascript:;" onclick="get_group('3')">All</a>
                    </li>
                    
                    <li>
                        <a href="javascript:;" onclick="get_group('0')">Single</a>
                    </li>
					<li>
                        <a href="javascript:;" onclick="get_group('1')">Group</a>
                    </li>
                    
                </ul>
            </div>-->
          <!--<button class="btn btn-success" onclick="get_group('1')">Group</button>
          <button class="btn btn-success" onclick="get_group('0')">Single</button>
          <button class="btn btn-success" onclick="get_group('3')">All</button>-->
        </div>
      </div>
    </div>
    <table class="table table-striped table-hover table-bordered" id="sample_3">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Type</th>
          <th scope="col">Type Id</th>
          <th scope="col">Booking Id</th>
          <th scope="col">Group Id</th>
          <th scope="col">Date</th>
          <th scope="col">Booking Status</th>
          <th scope="col">Applable On</th>
          <th scope="col">Applied Amount</th>
          <th scope="col">Commision Amount</th>
        </tr>
      </thead>
      <tbody>
        <?php $i=0;$totalGroup=0;$totalAppliedAmo=0;$totalcomm=0;
		
		
		//echo "<pre>";
		//print_r($travelAgent);
				if(isset($travelAgent) && $travelAgent){
				foreach($travelAgent as $travel){
			$i++;
	if($travel->groupID){
	$totalGroup++;
}if($travel->applyedAmount){
	$totalAppliedAmo+=$travel->applyedAmount;
}
if($travel->amount){
	$totalcomm+=$travel->amount;
}
if($travel->taType=='agent'){
$que=$this->bookings_model->get_broker_details($travel->taID);

if(isset($que) && $que){
foreach($que as $q){
$name='<span style="color:#008822; font-weight:bold;">'.$q->b_name.'</span>';
}
}}else{
$que=$this->bookings_model->get_channel_details($travel->taID);

if(isset($que) && $que){
foreach($que as $q){
$name='<span style="color:#0277BD; font-weight:bold;">'.$q->channel_name.'</span>';
}
}
}
?>
        <tr>
          <td><?php echo $i; ?></td>
          <td><?php echo $travel->taType; ?></td>
          <td><?php echo $name ;?></td>
          <td><?php echo $travel->bookingID;?></td>
          <td><?php echo $travel->groupID;?></td>
          <td><?php echo $travel->addedDate;?></td>
          <td><?php

$stat=$this->dashboard_model->get_status($travel->booking_status_id);
if(isset($stat) && $stat){foreach($stat as $sta){  $St=$sta->booking_status;}}
 echo $St;?></td>
          <td><?php 
				if($travel->commAppOn=='rr'){
				$coma="Room Rent";
				}else if($travel->commAppOn=='rrf'){
                        $coma="Room Rent + Food";
					}
				else if($travel->commAppOn=='tb'){
                        $coma="Total Bill";
					}else{
$coma='';
}
			
echo $coma; ?></td>
          <td><?php echo $travel->applyedAmount?></td>
          <td><?php echo $travel->amount?></td>
        </tr>
        <?php  }}?>
      </tbody>
      <tfoot>
        <tr>
          <td></td>
          <td></td>
          <td style="font-weight:700; color:#666666; text-align:center;"> TOTAL </td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:left;" value="<?php if(isset($stat ) && $stat )  echo number_format($i,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:left;" value="<?php if(isset($stat ) && $stat )  echo number_format($totalGroup,2,".",","); else echo "0";?>" /></td>
          <td></td>
          <td></td>
          <td></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:left;" value="<?php if(isset($stat ) && $stat )  echo number_format($totalAppliedAmo,2,".",","); else echo "0";?>" /></td>
          <td><input  class="s-inp" style="font-weight:700; width:100%; color:#2FC3A1; text-align:left;" value="<?php if(isset($stat ) && $stat )  echo number_format($totalcomm,2,".",","); else echo "0";?>" /></td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>
<script>

$(document).ready(function(){
  $('#chkbx_tdy').prop('checked', true);
});

function check_sub(){
  document.getElementById('form_date').submit();
}

function get_group(type){
	
	$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>reports/travelagentcommission_type",
                data:{type:type},
                success:function(data)
                {
                   ///alert(data);
					  $('#sample_3').html(data);	
				  $.getScript('<?php echo base_url();?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
				  $.getScript('<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons-ex.min.js');
				  $.getScript('<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js');
				   
                loaderOff();
				}
            });
	
}
</script> 
