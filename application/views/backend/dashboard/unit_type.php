<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <strong> <i class="fa fa-edit" ></i> </strong> List of All Unit Type's </div>
    <div class="actions"> 
    	<a class="btn btn-circle green btn-outline btn-sm"  data-toggle="modal" href="#responsive" > <i class="fa fa-plus"></i> Add New</a>
    </div>
  </div>
  <div class="portlet-body">
    
	
	<p class="font-green-sharp" style="font-weight:600;">*Example - AC Standard Room, Premium Cottage, Sapphire, Diamond, Executive Suite etc.</p> <p class="font-green-sharp">Please use the table below to navigate or filter the results. You can download the table as excel and pdf.</p>
	
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr>
		  <th scope="col" width="5%"> sl </td>
          <th scope="col" width="10%"> Unit Type </th>
          <th scope="col" width="10%"> Unit Category </th>
          <th scope="col" width="10%"> Unit Class </th>
		  <th scope="col" width="5%"> Unit Count </th>
		  <th scope="col" width="5%"> Kit Name </th>
           <th scope="col" width="10%"> Default Occu</th>
          <th scope="col" width="8%"> Max Occu</th>
          <th scope="col" width="22%"> Note </th>
          <th scope="col" width="5%"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($unit) && $unit):

                      $i=1;
					  $sl = 0;
                      foreach($unit as $gst):
                          $class = ($i%2==0) ? "active" : "success";
                          $g_id=$gst->id;
						  $sl++;
                          ?>
        <tr id="row_<?php echo $gst->id;?>">
          <td align="left"><?php echo $sl; ?></td>
          <td align="left"><?php echo $gst->unit_name; ?></td>
          <td align="left"><?php echo $gst->unit_type; ?><input type="hidden" class="unit_id" name="unit_id" value="<?php echo $gst->id; ?>"></td>
          <td align="left"><?php echo $gst->unit_class;?></td>
		  <td align="left">
			<?php 
				if($this->dashboard_model->count_room($gst->id) > 0)
					echo $this->dashboard_model->count_room($gst->id);
				else
					echo '<span style="color:#AAAAAA;">'.$this->dashboard_model->count_room($gst->id).'</span>';
			?>
		  </td>
		  <?php
		  
		  
		  ?>
          <td align="left">
			<?php 
					/*if($this->dashboard_model->get_default_kit_name($gst->kit) == 0)
						echo 'undefined';
					else*/
						echo $this->dashboard_model->get_default_kit_name($gst->kit);

			?></td>
          <td align="left"><?php echo $gst->default_occupancy;?></td>
          <td align="left"><?php echo $gst->max_occupancy;?></td>
		  <td align="left"><?php echo $gst->unit_desc;?></td>
          <td align="center" class="ba"><div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li> <a onclick="soft_delete('<?php echo $g_id;?>')" data-toggle="modal"  class="btn red btn-xs"><i class="fa fa-trash"></i></a> </li>
                <li>
                  <a onclick="edit_unit_type('<?php echo $gst->id; ?>')" data-toggle="modal" class="btn green btn-xs"> <i class="fa fa-edit"></i></a>
                </li>
              </ul>
            </div></td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>

<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>dashboard/delete_unit_type?f_id="+id,
                data:{f_id:id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
					if(data.data == 'This Unit type all ready has rooms'){
						
						//alert('warn');
						swal('This Unit type all ready has rooms',"Warning");
						
					}else{
					
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                           // location.reload();
							$('#row_'+id).remove();
                        });
					}
                }
            });



        });
    }
	
	
	function edit_unit_type(id){
		//alert(id);
		
		$.ajax({
                type:"POST",
                url: "<?php echo base_url()?>unit_class_controller/edit_unit_type",
                data:{id:id},
                success:function(data)
                {
                //  alert(data.unit_type);
                   $('#hid1').val(data.id);
				   $('#unit_type1').val(data.unit_type).change();
				   $('#unit_name1').val(data.unit_name);
				   $('#unit_class1').val(data.unit_class).change();
				    $('#kit1').val(data.kit).change();
				   $('#desc1').val(data.unit_desc);
				   $('#default_occ1').val(data.default_occupancy);
				   $('#max_occ1').val(data.max_occupancy);
				    $('#editmodal').modal('toggle');
					
                }
            });
	}
</script> 
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('dashboard/add_unit_type',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add Unit Type</h4>
      </div>
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
          		<div class="col-md-6">
              <div class="form-group form-md-line-input">
                <select class="form-control bs-select"   name="unit_type" id="unit_type" required >
                  <option value="" disabled="" selected="">Unit Category</option>
                  <?php foreach($unitTypeName as $uName){ ?>
                  <option value="<?php echo $uName->name."/".$uName->id;?>"><?php echo $uName->name;?></option>
                  <?php } ?>
                </select>
                <label></label>
                <span class="help-block">Unit Category</span> 
              </div>
              </div>
              <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <select class="form-control bs-select" name="unit_class" id="unit_class" required >
                  <option value="" disabled="disabled" selected="selected">Unit Class</option>
                  <?php foreach($unit_class as $uc)
						{?>
                  <option value="<?php echo $uc->hotel_unit_class_name."/".$uc->hotel_unit_class_id;?>"><?php echo $uc->hotel_unit_class_name; ?></option>
                  <?php } ?>
                </select>
                <label></label>
                <span class="help-block">Unit class</span> </div>
              </div>
			  
			  <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="unit_name" id="unit_name" required="required" placeholder="Unit type">
                <label></label>
                <span class="help-block">Unit Type</span> </div>
              </div>
			  
			   <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <select class="form-control bs-select" name="kit" id="kit">
                  <option value="" disabled="disabled" selected="selected">Select Default Kit</option>
                  <?php foreach($kit as $k)
						{?>
                  <option value="<?php echo $k->hotel_define_kit_id;?>"><?php echo $k->kit_name; ?></option>
                  <?php } ?>
                </select>
                <label></label>
                <span class="help-block">Kit</span> </div>
              </div>
			  
			    <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="default_occ" id="default_occ" required="required" placeholder="Default Occupancy">
                <label></label>
                <span class="help-block">Default Occupancy</span> </div>
              </div>
			  <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="max_occ" id="max_occ" required="required" placeholder="Max Occupancy">
                <label></label>
                <span class="help-block">Max Occupancy</span> </div>
              </div>
			  
			  
              <div class="col-md-12">
              <div class="form-group form-md-line-input">
                <textarea autocomplete="off" type="text" class="form-control"  name="desc" id="desc" placeholder="Description"></textarea>
                <label></label>
                <span class="help-block">Description</span> </div>
              </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>
<div id="editmodal" class="modal fade" tabindex="-1" aria-hidden="true">
  <?php

  $form = array(
      'class' 			=> 'form-body',
      'id'				=> 'form',
      'method'			=> 'post'
  );

  echo form_open_multipart('dashboard/edit_unit_type',$form);

  ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Unit Type</h4>
      </div>
      <input type="hidden" name="hid1" id="hid1">
      <div class="modal-body">
        <div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
          <div class="row">
          	<div class="col-md-6">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select"   name="unit_type1" id="unit_type1" required >
                <option value="" disabled="disabled" selected="selected">Unit Type</option>
                <?php foreach($unitTypeName as $uName){ ?>
                <option value="<?php echo $uName->name;?>"><?php echo $uName->name;?></option>
                <?php } ?>
              </select>
              <label></label>
              <span class="help-block">Unit Category *</span> </div>
            </div>
            <div class="col-md-6">
            <div class="form-group form-md-line-input">
              <input type="text" class="form-control" name="unit_name1" value="<?php if(isset($uName->name) && $uName->name){echo $uName->name;} ?>" placeholder="Unit Name *" id="unit_name1" required="required">
              <label></label>
              <span class="help-block">Unit type Name *</span> </div>
            </div>
            <div class="col-md-6">
            <div class="form-group form-md-line-input">
              <select class="form-control bs-select"  name="unit_class1" id="unit_class1" required >
                <option value="" disabled="" selected="">--Unit Class--</option>
                <?php foreach($unit_class as $uc)
					{?>
                <option value="<?php echo $uc->hotel_unit_class_name;?>"><?php echo $uc->hotel_unit_class_name; ?></option>
                <?php } ?>
              </select>
              <span class="help-block">Unit Class</span> </div>
            </div>
			<div class="col-md-6">
              <div class="form-group form-md-line-input">
                <select class="form-control bs-select" name="kit1" id="kit1">
                  
                  <?php foreach($kit as $k)
						{?>
                  <option value="<?php echo $k->hotel_define_kit_id;?>"><?php echo $k->kit_name; ?></option>
                  <?php } ?>
                </select>
                <label></label>
                <span class="help-block">Kit</span> </div>
              </div>
            <div class="col-md-12">
            <div class="form-group form-md-line-input">
              <textarea autocomplete="off" type="text" class="form-control" placeholder="Description"  name="desc1" id="desc1" ></textarea>
              <span class="help-block">Description</span> </div>
            </div>
			 
			<div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="default_occ1" id="default_occ1" required="required" placeholder="Default Occupancy">
                <label></label>
                <span class="help-block">Default Occupancy</span> </div>
              </div>
			  <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <input type="text" class="form-control" name="max_occ1" id="max_occ1" required="required" placeholder="Max Occupancy">
                <label></label>
                <span class="help-block">Max Occupancy</span> </div>
              </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
        <button type="submit" class="btn green">Save</button>
      </div>
    </div>
  </div>
  <?php form_close(); ?>
</div>