<!-- BEGIN PAGE CONTENT-->

<?php if($this->session->flashdata('err_msg')):?>
      <div class="alert alert-danger alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
      <div class="alert alert-success alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<script>




function get_dg_detail()
{
	var gen_name = $('#gen_name').val();
	var nm_txt = $("#gen_name option:selected").text();
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>dashboard/get_gen_detail_by_id",
		data: {dgset_id: gen_name},
		success: function(data){
			var myString= data ;
			var arr = myString.split('-');
			/*alert (arr[0]);
			alert (arr[1]);*/
			var consm = arr[0];
			consm = parseInt(consm);
			consm = consm.toFixed(2);
			$('#fuel_consumption_rate').val(consm);
			$('#fuel_type').val(arr[1]);
		    $('#nm_txt_val').val(nm_txt);
			//alert ($('#fuel_type').val());
		}

	});
}


</script>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add DG set usage log</span> </div>
  </div>
  <div class="portlet-body form">
    <?php

                        $form = array(
                            'class' 			=> '',
                            'id'				=> 'form',
                            'method'			=> 'post',								
                        );
                        
                        

                        echo form_open_multipart('dashboard/add_usage_log',$form);

                        ?>
    <div class="form-body"> 
      <div class="row">
      	<div class="col-md-4">
            <div class="form-group form-md-line-input">            
            <input type="hidden"  id="fuel_consumption_rate" name="fuel_consumption_rate"/>
            <input type="hidden"  id="nm_txt_val" name="nm_txt_val"/>
            <input type="hidden"  id="fuel_type" name="fuel_type"/>        
              <select class="form-control bs-select"  name="gen_name" id="gen_name"  onchange="get_dg_detail()" required="required">
                <option value="" disabled="disabled" selected="selected">Select Dg Set</option>
                <?php if(isset($datas) && $datas){
                                                    foreach($datas as $data){?>
                <option value="<?php echo $data ->dgset_id?>"><?php echo $data ->gen_name?></option>
                <?php }}?>
              </select>
              <label></label>
              <span class="help-block">Select Dg Set *</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control" id="datepicker" name="startdate" placeholder="Start Date *">
              <label></label>
              <span class="help-block">Start Date *</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input autocomplete="off" type="text" class="form-control date-picker" id="stopdatepicker" name="stopdate" placeholder="Stop Date *">
              <label></label>
              <span class="help-block">Stop Date *</span>
          	</div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input onblur="start_time()" autocomplete="off" type="time" class="form-control " id="starttime" name="starttime" placeholder="Start Time *">
              <label></label>
              <span class="help-block">Start Time *</span>
          	</div>
        </div>   
        <div class="col-md-4">       
        	<div class="form-group form-md-line-input" >
              <input onblur="cal_time()" autocomplete="off" type="time" class="form-control " id="stime" name="stoptime" placeholder="Stop time *">
              <label></label>
              <span class="help-block">Stop time *</span>
        	</div>
        </div>
        <div class="col-md-4">       
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control" id="o_fuel" name="o_fuel" onkeyup="chk_if_num('o_fuel')" required="required" placeholder="Opening Fuel Reading *">
              <label></label>
              <span class="help-block">Opening Fuel Reading *</span>
            </div>
        </div>
        <div class="col-md-4">   
            <div class="form-group form-md-line-input">
              <input  onblur="diffuel()" autocomplete="off" type="text" class="form-control" id="c_fuel" name="c_fuel" onkeyup="chk_if_num('c_fuel')" required="required" placeholder="Closing Fuel Reading *">
              <label></label>
              <span class="help-block">Closing Fuel Reading *</span>
            </div>
        </div>
        <div class="col-md-4">   
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control" id="differfuel" name="differfuel" onkeyup="chk_if_num('differfuel')"  required="required" placeholder="Difference in Fuel Reading *">
              <label></label>
              <span class="help-block">Difference in Fuel Reading *</span>
            </div>
        </div>
        
        <div class="col-md-4"> 
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control" id="tot_hours" name="totalhours" onkeyup="chk_if_num('tot_hours')"required="required" placeholder="Total Hours *" readonly required >
              <label></label>
              <span class="help-block">Total Hours *</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input  autocomplete="off" type="text" class="form-control" id="fuel_consumption" name="fuelconsumption" onkeyup="chk_if_num('fuel_consumption')" required="required" placeholder="Fuel Consumption" readonly required>
              <label></label>
              <span class="help-block">Fuel Consumption *</span>
            </div>
        </div>
		  
      </div>
    </div>
	
    <div class="form-actions right">
      <button type="submit" class="btn submit" onclick = "check_input()" >Submit</button>
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php form_close(); ?>
  </div>
  <!-- END CONTENT --> 
</div>
<script>
   $(function () {
                $('#datepicker').datepicker({
					todayHighlight: true,
					autoclose : true,
				endDate: '+0d'
				});
				 $('#stopdatepicker').datepicker({
					todayHighlight: true,
					autoclose : true,
					endDate: '+0d'
					 });
					 
					 var currentTime = new Date();
        var hours = currentTime.getHours();
        var minutes = currentTime.getMinutes();
        var seconds = currentTime.getSeconds();
		var cur_time_dis = hours+":"+minutes;
					 document.getElementById("starttime").defaultValue = cur_time_dis;
					 document.getElementById("stime").defaultValue = cur_time_dis;
					
   });
   
   
   
   
   function diffuel()
   {

    var o=document.getElementById('o_fuel').value;

    var c=document.getElementById('c_fuel').value;
   
    var result=parseFloat(o)-parseFloat(c);
	if(result>0)
	{
	document.getElementById('differfuel').focus();
    document.getElementById('differfuel').value=result;
	}
	else
	{
		
		swal({  
		 title: "Alert",   
		 text: "Please check closing fuel readings", 
		 type: "error", 
		 }, function(){ 
		document.getElementById('c_fuel').focus();
				$('#c_fuel').val("0");
				$('#fuel_consumption').val("");
		 });
		
	}
   }

	function cal_time()
		{
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			 var curdate=(mm+"/"+dd+"/"+yyyy);
			 
			 
		
			var rate = $('#fuel_consumption_rate').val();
			rate = parseFloat(rate);
			var date1 = new Date($('#datepicker').val()+" "+$('#starttime').val());
			var date2 = new Date($('#stopdatepicker').val()+" "+$('#stime').val());
			var dt_dif = ((date2-date1)/3600000);
			dt_dif = dt_dif.toFixed(2);
			
			if(parseInt(rate)>0)
			{
				
					if(dt_dif > 0)
						{
								
							document.getElementById('tot_hours').focus();
							$('#tot_hours').val(dt_dif);
						
							document.getElementById('fuel_consumption').focus();
							$('#fuel_consumption').val(dt_dif * rate);
						
						}
					else
						{

							
							swal({  
		 title: "Alert",   
		 text: "End Date / Time Cannot Be Less than / equal to Start Date / Time Cannot", 
		 type: "error", 
		 }, function(){ 
		$('#datepicker').focus();
		//$('#datepicker').val("");
		$('#tot_hours').val("");					
		 });
							
						}
			}
			else
			{
				
				swal({  
		 title: "Alert",   
		 text: "Please select DG Set name ..", 
		 type: "error", 
		 }, function(){ 
		$('#gen_name').focus();
							
		 });
				
				
			}
			
			
		}

		
		
		function chk_if_num( a )
		{
				if(isNaN($('#'+a+'').val())==true)
				{
					alert ("please enter a valid "+a);
					$('#'+a+'').val("");
					
				}			
		}

		
</script> 
<script>

</script>
