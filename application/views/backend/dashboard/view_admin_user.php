<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption"> <i class="glyphicon glyphicon-bed"></i> All Admin Users </div>
    <div class="actions">
    	<a href="<?php echo base_url();?>setting/add_admin_user" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-plus"></i> Add New </a>
    </div>
  </div>
  <div class="portlet-body">
    <!--<div class="table-toolbar">
      <div class="row">
        <div class="col-md-6">
          <div class="btn-group">  </div>
        </div>        
      </div>
    </div>-->
    <table class="table table-striped table-bordered table-hover" id="sample_1">
      <thead>
        <tr>
		  <th width="5%" align="center" valign="middle" scope="col"> Sl. No. </th>
          <th width="15%" align="center" valign="middle" scope="col"> Name </th>
          <th width="10%" align="center" valign="middle" scope="col"> Username </th>
          <th width="10%" align="center" valign="middle" scope="col"> Profit Center </th>          
          <th width="10%" align="center" valign="middle" scope="col"> User Type </th>
          <th width="10%" align="center" valign="middle" scope="col"> Admin Role </th>
		  <th width="10%" align="center" valign="middle" scope="col"> Status </th>
          <th width="10%" align="center" valign="middle" scope="col"> Action </th>
        </tr>
      </thead>
      <tbody>
        <?php 
		    if(isset($ad) && $ad){
			$i=1;
			foreach($ad as $ad){
			$getUserType=$this->setting_model->getUserType($ad->user_type_id);
			$getAdminRole=$this->setting_model->getAdminRole($ad->admin_role_id);			
		?>
        <tr>
		  <td align="left" valign="middle"><?php echo $i;?></td>
          <td align="left" valign="middle"><?php echo $ad->admin_first_name." ".$ad->admin_last_name;?></td>
          <td align="left" valign="middle"><?php echo $ad->user_name;?></td>
          <td align="left" valign="middle"><?php echo $ad->profit_center; ?></td>
          <td align="left" valign="middle"><?php echo $getUserType->user_type_name;  ?></td>
          <td align="left" valign="middle"><?php if(isset($ad->admin_role_id) && $ad->admin_role_id >0 ) { 		  echo $getAdminRole->admin_roll_name;		 		  }  ?></td>
          <td align="left" valign="middle">
		  <?php 
		    if($ad->user_status == 'A'){
				echo '<span class="label" style="background-color:#6F9F67; color:#ffffff;">Active</span>';
			} else {
				echo '<span class="label" style="background-color:#E52828; color:#ffffff;">Pending</span>';
			}
		  ?>
		  </td>
          <td align="center" valign="middle" class="ba">
          	<div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li> <a onclick="soft_delete('<?php echo $ad->admin_id; ?>','<?php echo $ad->login_id; ?>')" data-toggle="modal" class="btn red btn-xs"> <i class="fa fa-trash"></i></a> </li>
                <li> <a href="<?php echo base_url() ?>setting/edit_admin_user?id=<?php echo $ad->admin_id; ?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a> </li>
              </ul>
            </div>
          </td>
        </tr>
        <?php $i++;?>
		<?php } }?>
      </tbody>
    </table>
  </div>
</div>
<!-- END PAGE CONTENT--> 
<script>
    function soft_delete(admin_id,login_id){
        swal({   title: "Are you sure?",   text: "All the details of this user will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>setting/delete_admin_user",
                data:{admin_id:admin_id,login_id:login_id},
                success:function(data)
                {
                    //alert("Checked-In Successfully");
                    //location.reload();
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                            location.reload();

                        });
                }
            });
        });
    }
</script>