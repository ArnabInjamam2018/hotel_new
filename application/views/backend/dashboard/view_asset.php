<!-- BEGIN PAGE CONTENT-->

<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <span class="caption-subject bold uppercase"><i class="fa fa-plus-square"></i>&nbsp; Add Asset</span> </div>
  </div>
  <div class="portlet-body form">
    <?php if($this->session->flashdata('err_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-danger alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
    <?php if($this->session->flashdata('succ_msg')):?>
    <div class="form-group">
      <div class="col-md-12 control-label">
        <div class="alert alert-success alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
      </div>
    </div>
    <?php endif;?>
    <?php
                  $form = array(
                      'class'       => '',
                      'id'        => 'form',
                      'method'      => 'post',
      
                  ); 
                  echo form_open_multipart('dashboard/view_asset',$form);
                  ?>
    <div class="form-body">
      <div class="row">
        <?php if(isset($assets)){
			//print_r($assets);
		}
		?>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="a_type" id="unit_type" class="form-control bs-select" required readonly>
              <option ><?php echo $assets->a_type;?></option>
            </select>
            <label></label>
            <span class="help-block">Asset Type *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" value="<?php echo $assets->a_name;?>" readonly class="form-control" id="ct_name" name="a_name" required="required" placeholder="Asset Name *">
            <label></label>
            <span class="help-block">Asset Name *</span> </div>
        </div>
        <div class="col-md-4 form-horizontal" style="padding-top: 24px;">
          <div class="form-group form-md-line-input">
            <label class="col-md-6 control-label"> Asset First Hand<span class="required" id="b_contact_name">*</span></label>
            <div class="col-md-6">
              <div class="md-radio-inline">
                <div class="md-radio">
                  <input type="radio" id="radio51" class="md-check" name="a_first_hand" value="yes" <?php if($assets->a_first_hand=="yes"){echo "checked";}?> disabled='disabled'>
                  <label for="radio51"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
                </div>
                <div class="md-radio" >
                  <input type="radio" id="radio50" class="md-check" name="a_first_hand" value="no" <?php if($assets->a_first_hand=="no"){echo "checked";}?> disabled='disabled'>
                  <label for="radio50"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" required="required" value="<?php echo $assets->a_bought_date;?>"  name="a_bought_date" class="form-control date-picker "  id="c_valid_from" readonly placeholder="Asset Bought On *">
            <label></label>
            <span class="help-block">Asset Bought On *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" readonly autocomplete="off" class="form-control" id="ct_name" value="<?php echo $assets->a_description;?>" name="a_description" required="required" placeholder="Asset Description *">
            <label></label>
            <span class="help-block">Asset Description *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" readonly class="form-control" id="ct_name" value="<?php echo $assets->a_reg_number;?>" name="a_reg_number" required="required" placeholder="Asset Registration Number *">
            <label></label>
            <span class="help-block">Asset Registration Number *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" readonly class="form-control" id="ct_name" value="<?php echo $assets->a_purchased_from;?>" name="a_purchased_from" required="required" placeholder="Asset Purchased From *">
            <label></label>
            <span class="help-block">Asset Purchased From *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off"  readonly onkeypress="return onlyNos(event,this)" class="form-control"value="<?php echo $assets->a_seller_contact_no;?>" name="a_seller_contact_no" required="required" maxlength="10" placeholder="Asset Seller Contact Number *">
            <label></label>
            <span class="help-block">Asset Seller Contact Number *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" readonly onkeypress="return onlyNos(event,this)" class="form-control" value="<?php echo $assets->a_service_contact_no;?>" name="a_service_contact_no" required="required" maxlength="10" placeholder="Asset Service Contact Number *">
            <label></label>
            <span class="help-block">Asset Service Contact Number *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" readonly class="form-control" id="ct_name" name="a_incharge" value="<?php echo $assets->a_incharge;?>" required="required" placeholder="Asset In Charge *">
            <label></label>
            <span class="help-block">Asset In Charge *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" readonly class="form-control" id="ct_name" name="a_cost" value="<?php echo $assets->a_cost;?>" required="required" placeholder="Asset Cost *">
            <label></label>
            <span class="help-block">Asset Cost *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" readonly class="form-control" id="ct_name" name="a_annual_depreciation" value="<?php echo $assets->a_annual_depreciation;?>" required="required" placeholder="Asset Annual Depreciation *">
            <label></label>
            <span class="help-block">Asset Annual Depreciation *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off"  readonly class="form-control date-picker" id="ct_name2" name="a_decomission_date" value="<?php echo $assets->a_decomission_date;?>" required="required" placeholder="Asset AMC Decomission Date *">
            <label></label>
            <span class="help-block">Asset AMC Decomission Date *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select"  name="a_category" required disabled>
              <option value="Inventory/Stock" <?php if($assets->a_category=="Inventory/Stock"){echo "select=selected";}?>>Inventory/Stock</option>
            </select>
            <label></label>
            <span class="help-block">Category *</span> </div>
        </div>
        <div class="col-md-4 form-horizontal" style="padding-top: 24px;">
          <div class="form-group form-md-line-input">
            <label class="col-md-6 control-label"> Asset AMC<span class="required"  id="b_contact_name">*</span></label>
            <div class="col-md-6">
              <div class="md-radio-inline">
                <div class="md-radio">
                  <input type="radio" disabled id="radio52" class="md-check" onclick="amc_open()" name="a_amc" value="yes" <?php if($assets->a_amc=="yes"){ echo "checked";}?> >
                  <label for="radio52"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
                </div>
                <div class="md-radio">
                  <input type="radio"  disabled  id="radio53" class="md-check" onclick="amc_close()" name="a_amc" value="no" <?php if($assets->a_amc=="no"){ echo "checked";}?>>
                  <label for="radio53"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="AMC" style="display:none">
        	<div class="col-md-4">
              <div class="form-group form-md-line-input">
                <input type="text" autocomplete="off" readonly class="form-control" id="ct_name" name="a_amc_agency_name" placeholder="Asset AMC Agency Name">
                <label></label>
                <span class="help-block">Asset AMC Agency Name</span> </div>
          </div>
          <div class="col-md-4">
          <div class="form-group form-md-line-input form-md-floating-label col-md-4">
            <input type="text" autocomplete="off" readonly onkeypress="return onlyNos(event,this)" class="form-control" id="ct_name" name="a_amc_reg_contact_no" maxlength="10" placeholder="Asset AMC Contact Number">
            <label></label>
            <span class="help-block">Asset AMC Contact Number</span> </div>
          </div>
          <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" readonly class="form-control date-picker" id="ct_name1" name="a_amc_renewal_date">
            <label>Asset AMC Renewal Date</label>
            <span class="help-block">Asset AMC Renewal Date</span> </div>
          </div>
          <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" autocomplete="off" readonly class="form-control" id="ct_name" name="a_amc_charge" placeholder="Asset AMC Renewal Charge">
            <label>Asset AMC Renewal Charge</label>
            <span class="help-block">Asset AMC Renewal Charge</span> </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="row">
          	<div class="col-md-4">
            <div class="form-group form-md-line-input uploadss">
              <label>Upload Asset Image</label>
              <div class="fileinput fileinput-new" data-provides="fileinput" readonly>
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>upload/asset/originals/asset_image/<?php echo $assets->a_asset_image;?>" alt=""/> </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('a_asset_image');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
              </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input uploadss">
              <label>Upload Asset Procurement Bill 1</label>
              <div class="fileinput fileinput-new" data-provides="fileinput" readonly>
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>upload/asset/originals/Proc_bill_1/<?php echo $assets->a_proc_bill_1_imag;?>" alt=""> </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('a_proc_bill_1_imag');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
              </div>
              <?php //echo form_upload('a_proc_bill_1_imag');?>
              <!--<input type="file"  name="a_proc_bill_1_imag" />--> 
            </div>
            </div>
            <div class="col-md-4">
            <div class="form-group form-md-line-input uploadss">
              <label>Upload Asset Procurement Bill 2</label>
              <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                  <?php if($assets->a_proc_bill_2_imag!=""){
                   echo '<img src="'.base_url().'upload/asset/originals/Proc_bill_2/'.$assets->a_proc_bill_2_imag.'" alt="">';
                 
				}				 else{
					echo '<img src="<?php echo base_url();?>assets/global/img/no-img.png" alt=""/>';
				  }
				?>
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                <div> <span class="btn default btn-file"> <span class="fileinput-new" disabled> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('a_proc_bill_2_imag');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
              </div>
              <?php //echo form_upload('a_proc_bill_2_imag');?>
              <!--<input type="file"  name="a_proc_bill_2_imag" />--> 
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <div class="form-actions right"> <a href="javascript:window.history.go(-1);" class="btn green">Back</a> <a href="<?php echo base_url() ?>dashboard/Edit_asset?a_id=<?php echo $assets->a_id;?>" 
              class="btn blue" data-toggle="modal"><i class="fa fa-edit"></i>Edit</a> </div>
    </div>
    <?php form_close(); ?>
  </div>
</div>

<!-- END CONTENT --> 
<script>
function amc_open(){
  document.getElementById('AMC').style.display='block';
}
function amc_close(){
  document.getElementById('AMC').style.display='none';
}
$(document).on('blur', '#c_valid_from', function () {
	$('#c_valid_from').addClass('focus');
});
$(document).on('blur', '#ct_name2', function () {
	$('#ct_name2').addClass('focus');
});
$(document).on('blur', '#ct_name1', function () {
	$('#ct_name1').addClass('focus');
	
});
</script> 
