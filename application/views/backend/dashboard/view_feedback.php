<!-- BEGIN PAGE CONTENT-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/dashboard/js/star-rating.css"/>
<script>
$(document).ready(function() {
$('input.typeahead-devs').typeahead({
name: 'gName',
//local:['Sunday']
remote : '<?php echo base_url(); ?>dashboard/get_guest_name/%QUERY'
});
});		  

</script>
<div class="portlet box blue">
	<div class="portlet-title">
    	<div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase">Add Feedback</span> </div>
    </div>
    <div class="portlet-body form">
    <?php
            $form = array(
                'class' 			=> '',
                'id'				=> 'form',
                'method'			=> 'post',
            );
            echo form_open_multipart('dashboard/edit_feedback',$form);
            ?>
    <div class="form-body">
      <?php if($this->session->flashdata('err_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata('succ_msg')):?>
      <div class="form-group">
        <div class="col-md-12 control-label">
          <div class="alert alert-success alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
        </div>
      </div>
      <?php endif;?>
      <div class="row">
        <?php
                        if(isset($fdback)){
                            
                            foreach($fdback as $fd){
                            
                        ?>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" value="<?php echo  $hotel_name->hotel_name; ?>" readonly="readonly" class="form-control input-sm" placeholder="Hotel Name">
              <label></label>
              <span class="help-block">Hotel Name</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <input type="text" value="<?php 
                      if(isset($_GET['fdback_id'])){
                          //echo $_GET['bID'];
                          
                          if($fd->booking_id==0){
                          echo "N/A";
                      }	
                      else{
                          echo $fd->booking_id;
                          
                      }
                      } 
                    ?>" readonly="readonly" class="form-control input-sm"  placeholder="Booking Id">
              <label></label>
              <span class="help-block">Booking Id</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <?php 
                        if(isset($_GET['bID'])){
                            echo '<input type="text" name="gName" class="form-control input-sm" placeholder="Please Enter Name" value="' .$guest->g_name.' " readonly="readonly" placeholder="Guest Name">';
                            
                            
                        } else {
                            echo '<input type="text" name="gName" class="form-control input-sm" placeholder="Please Enter Name" value="'.$fd->guest_name.'" readonly="readonly" placeholder="Guest Name">';
                        }
                    ?>
              <label></label>
              <span class="help-block">Guest Name</span>
            </div>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Ease of booking</label>
          <input type="hidden" name="f_id" value="<?php echo $fd->id ?>">
          <input id="input-2c2" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w1" value="<?php echo $fd->ease_booking; ?>" required="required" readonly>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Reception</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w2" value="<?php echo $fd->reception; ?>" readonly>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Staff</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w3" value="<?php echo $fd->staff; ?>" readonly>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Cleanliness</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w4" value="<?php echo $fd->cleanliness; ?>" readonly>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Ambience</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w5" value="<?php echo $fd->ambience; ?>" readonly>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Sleep Quality</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w6" value="<?php echo $fd->sleep_quality; ?>" readonly>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Room Quality</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w7" value="<?php echo $fd->room_quality; ?>" readonly>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Food Quality</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w8" value="<?php echo $fd->food_quality; ?>" readonly>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Environment Quality</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w9" value="<?php echo $fd->env_quality; ?>" readonly>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Service</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w10" value="<?php echo $fd->service; ?>" readonly>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Package</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w11" value="<?php echo $fd->package; ?>" readonly>
        </div>
        <div class="form-group form-md-line-input form-md-floating-label col-md-4">
          <label style="font-size: 13px; color:#999;">Extra Amenities</label>
          <input id="input-2c" class="rating form-control input-sm" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" name="w12" value="<?php echo $fd->extra; ?>" readonly>
        </div>
        <div class="form-group form-md-radios form-md-line-input col-md-4">
          <label>Will you come back again?</label>
          <div class="md-radio-inline">
            <div class="md-radio">
              <input type="radio" id="radio1" name="or1" value="1" class="md-radiobtn" disabled <?php if($fd->come_back == '1') { echo 'checked="checked"';}?> readonly>
              <label for="radio1"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio2" name="or1" class="md-radiobtn" value="2" disabled <?php if($fd->come_back == '2') { echo 'checked="checked"';}?>readonly>
              <label for="radio2"> <span></span> <span class="check"></span> <span class="box"></span> Maybe </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio3" name="or1" class="md-radiobtn" value="0" disabled <?php if($fd->come_back == '0') { echo 'checked="checked"';}?>readonly>
              <label for="radio3"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
            </div>
          </div>
        </div>
        <div class="form-group form-md-radios form-md-line-input col-md-4">
          <label>Will you refer to a friend?</label>
          <div class="md-radio-inline">
            <div class="md-radio">
              <input type="radio" id="radio4" name="or2" value="1" class="md-radiobtn" disabled <?php if($fd->refer_friend == '1') { echo 'checked="checked"';}?> readonly>
              <label for="radio4"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio5" name="or2" class="md-radiobtn" value="2" disabled <?php if($fd->refer_friend == '2') { echo 'checked="checked"';}?>readonly>
              <label for="radio5"> <span></span> <span class="check"></span> <span class="box"></span> Maybe </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio6" name="or2" class="md-radiobtn" value="0" disabled <?php if($fd->refer_friend == '0') { echo 'checked="checked"';}?>readonly>
              <label for="radio6"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
            </div>
          </div>
        </div>
        <div class="form-group form-md-radios form-md-line-input col-md-4">
          <label>Was the price reasonable?</label>
          <div class="md-radio-inline">
            <div class="md-radio">
              <input type="radio" id="radio7" name="or3" value="1" class="md-radiobtn" disabled <?php if($fd->reasonable_cost== '1') { echo 'checked="checked"';}?> readonly>
              <label for="radio7"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio8" name="or3" class="md-radiobtn" value="2" disabled <?php if($fd->reasonable_cost== '2') { echo 'checked="checked"';}?>readonly>
              <label for="radio8"> <span></span> <span class="check"></span> <span class="box"></span> Maybe </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio9" name="or3" class="md-radiobtn" value="0" disabled <?php if($fd->reasonable_cost== '0') { echo 'checked="checked"';}?>readonly>
              <label for="radio9"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
            </div>
          </div>
        </div>
        <div class="form-group form-md-radios form-md-line-input col-md-4">
          <label>Do you have a suggestion?</label>
          <div class="md-radio-inline">
            <div class="md-radio">
              <input type="radio" id="radio10" name="or4" value="1" class="md-radiobtn" disabled <?php if($fd->suggestion== '1') { echo 'checked="checked"';}?> readonly>
              <label for="radio10"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio11" name="or4" class="md-radiobtn" value="0" disabled <?php if($fd->suggestion== '0') { echo 'checked="checked"';}?>readonly>
              <label for="radio11"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
            </div>
          </div>
        </div>
        <div class="form-group form-md-radios form-md-line-input col-md-4">
          <label>Add to Social Media</label>
          <div class="md-radio-inline">
            <div class="md-radio">
              <input type="radio" id="radio12" name="or5" value="1" class="md-radiobtn" disabled <?php if($fd->add_social_media== '1') { echo 'checked="checked"';}?> readonly>
              <label for="radio12"> <span></span> <span class="check"></span> <span class="box"></span> Yes </label>
            </div>
            <div class="md-radio">
              <input type="radio" id="radio13" name="or5" class="md-radiobtn" value="0" disabled <?php if($fd->add_social_media== '0') { echo 'checked="checked"';}?>readonly>
              <label for="radio13"> <span></span> <span class="check"></span> <span class="box"></span> No </label>
            </div>
          </div>
        </div>
        <div class="col-md-12">
            <div class="form-group form-md-line-input">
              <textarea autocomplete="off" row="3" type="text" class="form-control input-sm" id="form_control_1" name="comment" readonly placeholder="Comment"><?php echo $fd->comment;?></textarea>              
              <label></label>
              <span class="help-block">Comment</span>
            </div>
        </div>
      </div>
      <?php } }?>
    </div>
    <div class="form-actions right"> <a href="javascript:window.history.go(-1);" class="btn green">Back</a> <a href="<?php echo base_url() ?>dashboard/fetch_gst_by_id?fdback_id=<?php echo $fd->id;?>" 
          class="btn blue" data-toggle="modal"><i class="fa fa-edit"></i>Edit</a> </div>
    <?php form_close(); ?>
    <!-- END CONTENT --> 
    </div>
</div>
