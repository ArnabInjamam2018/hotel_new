<div class="row">
  <div class="col-md-12">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase"> Food Plan Details </span> </div>
      </div>
      <?php //print_r($service); ?>
      <div class="portlet-body form">
        <div class="form-body form-horizontal form-row-sepe">
          <div class="form-body">
            <div class="row">
              <div class="col-md-3 text-center">
                <?php if(isset($fp->fp_image) && $fp->fp_image != ""){
				 echo '<img src="'.base_url().'upload/food_plan/'.$fp->fp_image.'" style="border:1px solid #dddddd; padding:5px;" class="img-responsive"/>';
			 } else {
                 echo '<img src="'.base_url().'upload/guest/no_images_food.png" style="border:1px solid #dddddd; padding:5px;"/>';
			 } ?>                
              </div>
              <div class="col-md-9" id="b-card">
              	<h4><strong><?php echo $fp->fp_name; ?></strong></h4>
                <div class="form-group">
                	<div class="col-md-6">
                      <label><strong style="color: #2B3643;">Category:</strong></label>&nbsp;
                      <label style="color:#008F6C"><?php echo $fp->fp_category; ?></label>
                	</div>
                	<div class="col-md-6">
                      <label><strong style="color: #2B3643;">Class:</strong></label>&nbsp;
                      <label style="color:#008F6C"><?php echo $fp->fp_class; ?></label>
                	</div>
                </div>
                <div class="form-group">                	
                	<div class="col-md-6">
                      <label><strong style="color: #2B3643;">Price for adult:</strong></label>&nbsp;
                      <label style="color:#008F6C"><?php echo $fp->fp_price_adult; ?></label>
                	</div>
                	<div class="col-md-6">
                      <label><strong style="color: #2B3643;">Price for Child:</strong></label>&nbsp;
                      <label style="color:#008F6C"><?php echo $fp->fp_price_children; ?></label>
                    </div>
               </div>
               <div class="form-group">
                	<div class="col-md-6">
                      <label><strong style="color: #2B3643;">Tax applied:</strong></label>&nbsp;
                      <label style="color:#008F6C"><?php echo $fp->fp_tax_applied; ?></label>
                   	</div>
					<?php if(isset($fp->fp_tax_applied) && $fp->fp_tax_applied == 'Yes'){ ?>
                    <div class="col-md-6">
                      <label><strong style="color: #2B3643;">Tax Percentage:</strong></label>&nbsp;
                      <label style="color:#008F6C"><?php echo $fp->fp_tax_percentage; ?></label>
                    </div>
                    <?php } ?>
               </div>
               <div class="form-group">
                    <div class="col-md-6">
                      <label><strong style="color: #2B3643;">Discount applied:</strong></label>&nbsp;
                      <label style="color:#008F6C"><?php echo $fp->fp_discount_applied; ?></label>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-12">
                      <label><strong style="color: #2B3643;">Description:</strong></label>&nbsp;
                      <label style="color:#008F6C"><?php echo $fp->fp_description; ?></label>
                	</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-haze"> <span class="caption-subject bold uppercase">Individual Services:</span> </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body form-horizontal form-row-sepe">
          <table class="table table-striped table-hover table-bordered mass-book" >
            <thead>
              <tr>
                <th width="5%"> # </th>
                <th width="35%"> Service Name </th>
                <th width="15%"> Service Periodicity </th>
                <th width="25%"> Individual Service Cost </th>
              </tr>
            </thead>
            <tbody>
              <?php $x = 1; if(isset($service) && $service !="") { foreach($service as $service){ ?>
              <tr>
                <td class="form-group"><?php echo $x; ?></td>
                <td class="form-group"><?php echo $service->s_name; ?></td>
                <td class="form-group"><?php echo $service->qty; ?></td>
                <td class="form-group"><?php echo $service->s_unit_cost; ?></td>
              </tr>
              <?php $x++; } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="portlet">
      <div class="portlet-body form">
        <div class="form-actions right"> <a href="<?php echo base_url() ?>dashboard/all_food_plans" class="btn blue">All Plan</a> <a href="<?php echo base_url() ?>dashboard/edit_food_plan?id=<?php echo $_GET['id'];?>" class="btn green">Edit</a> </div>
      </div>
    </div>
  </div>
</div>
