<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
<?php endif;?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-edit"></i>Fuel Log </div>
		<div class="actions">
			<a href="<?php echo base_url();?>dashboard/add_sub_fuel_log" class="btn btn-circle green btn-outline btn-sm"> <i class="fa fa-flask"></i>Add/ Sub Fuel </a>
		</div>
	</div>
	<div class="portlet-body">

		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#portlet_tab2" data-toggle="tab" aria-expanded="false"> Log of Fuel Inventory </a>
			</li>
			<li class="">
				<a href="#portlet_tab3" data-toggle="tab" aria-expanded="true"> Fuel List </a>
			</li>

		</ul>
		<div class="tab-content">

			<div class="tab-pane active" id="portlet_tab2">

				<table class="table table-striped table-bordered table-hover" id="sample_1">
					<thead>
						<tr>
							<!-- <th scope="col">
                            Select
                        </th>-->
							<!--<th scope="col"> Purchase ID </th>-->
							<th scope="col"> # </th>
							<th scope="col"> Date </th>
							<th scope="col"> Fuel Name </th>
							<th scope="col"> Operation</th>
							<th scope="col"> Quantity </th>
							<th scope="col"> Price </th>
							<th scope="col"> Vendor </th>
							<th scope="col"> Operational Notes</th>
							<th scope="col"> Action </th>
						</tr>
					</thead>
					<tbody>
						<?php
						if ( isset( $f_log ) && $f_log ):

							$i = 1;
						$j = 0;
						foreach ( $f_log as $log ):
							$j++;
						$class = ( $i % 2 == 0 ) ? "active" : "success";
						$id = $log->id;
						//echo $log->fuel_name;

						?>
						<tr>
							<td align="left" valign="middle" width="5%">
								<?php
								echo $j;
								?>
							</td>
							<td align="left" valign="middle" width="10%">
								<?php // Date Time
                    echo date("g:ia \-\n l jS F Y",strtotime($log->timestamp));
                ?>
							</td>
							<td align="left" valign="middle" width="10%">
								<?php
								$id = $log->fuel_name;
								$f_name = $this->dashboard_model->fetch_fuel_name( $id );
								if ( isset( $f_name ) && $f_name ) {
									echo '<strong>' . $f_name->fuel_name . '</strong>';
								} else {
									echo '<strong>' . $id . '</strong>';
								}
								?>
								<td align="left" valign="middle" width="5%">
									<?php // Operation
									if($log->in_out_type=='add') {echo "<span class='label label-sm label-success' style='background-color:#1C9A71; text-transform:uppercase;'> ".$log->in_out_type." </span>"; }
									elseif($log->in_out_type=='Cons') {echo "<span class='label label-sm label-warning' style='text-transform:uppercase;'> ".$log->in_out_type." </span>"; }
									else { echo "<span class='label label-sm label-danger' style='text-transform:uppercase;'> ".$log->in_out_type." </span>";}?>
								</td>
							</td>
							<td align="left" valign="middle" width="10%">
								<?php // Quantity
                    echo number_format($log->quantity,2);
								?>
							</td>
							<td align="left" valign="middle" width="10%">
								<?php
								echo 'INR ' . number_format( $log->price, 2 );
								?>
							</td>
							<td align="left" valign="middle" width="12%">
								<?php
								if ( $log->seller == '' && $log->seller == NULL ) {
									echo '<span style="color:#AAAAAA;">No info</span>';
								} else {
									echo $log->seller;
								}
								?>
							</td>
							<td align="left" valign="middle" width="30%">
								<?php
								if ( $log->note == '' && $log->note == NULL ) {
									echo '<span style="color:#AAAAAA;">No info</span>';
								} else {
									echo $log->note;
								}
								?>
							</td>

							<td align="center" valign="middle" width="5%">
								<a onclick="soft_delete('<?php echo $log->id;?>','<?php echo $log->usage_log_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
						<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>

			</div>
			<div class="tab-pane" id="portlet_tab3">
				<table class="table table-striped table-bordered table-hover" id="sample_2">
					<thead>
						<tr>
							<th>Fuel Type</th>
							<th>Current Stock</th>
							<th>Reorder Level</th>
							<th width="35%">Description</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($master_log as $mas) {
					?>
						<tr>
							<td>
								<span style="font-size:15px; font-weight:600;">
									<?php echo $mas->fuel_name;?>
								</span>
							</td>
							<td>
								<?php echo number_format($mas->cur_stock,2).' <span style="color:#AAAAAA;">'.$mas->unit.'</span>';?>
							</td>
							<td>
								<?php
								if ( isset( $mas->reorder_level ) )
									echo number_format( $mas->reorder_level, 2 ) . ' <span style="color:#AAAAAA;">' . $mas->unit . '</span>';
								?>
							</td>
							<td>
								<?php
								if ( isset( $mas->fuel_desc ) )
									echo $mas->fuel_desc;
								?>
							</td>
							<td align="center" valign="middle" class="ba" width="5%">
								<div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
									<ul class="dropdown-menu pull-right" role="menu">
										<?php if($this->dashboard_model->get_fuel_usage_count($mas->fuel_name)==0) {?>
										<li><a onclick="edit_fuel('<?php echo $mas->id; ?>')" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i></a>
										</li>
										<li><a onclick="soft_delete_fuel('<?php echo $mas->fuel_name?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a>
										</li>
										<?php }
							else{?>
										<li><a onclick="soft_message('<?php echo $mas->fuel_name?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a>
										</li>
										<?php }?>
									</ul>
								</div>
							</td>

						</tr>
						<?php }
			?>
					</tbody>
				</table>

			</div>
		</div>





	</div>
</div>


<div id="fuel" class="modal fade" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit Fuel</h4>
			</div>
			<div class="modal-body">
				<div class="scroller" style="height:280px" data-always-visible="1" data-rail-visible1="1">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group form-md-line-input">
								<input autocomplete="off" type="text" class="form-control" id="fuel_name" name="fuel_name" onblur="chk_type(value)" required placeholder="Fuel Type *">
								<label></label>
								<span class="help-block">Fuel Type *</span>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group form-md-line-input">
								<input autocomplete="off" type="text" class="form-control" id="fuel_desc" name="fuel_desc" required placeholder="Fuel Desscription *">
								<label></label>
								<span class="help-block">Fuel Desscription *</span>
							</div>
						</div>
						<input autocomplete="off" type="hidden" class="form-control" id="fuel_id" name="fuel_id" required placeholder="Fuel Desscription *">
						<div class="col-md-4">
							<div class="form-group form-md-line-input">
								<input autocomplete="off" type="text" class="form-control" id="unit" name="unit" required placeholder="Fuel Unit *">
								<label></label>
								<span class="help-block">Fuel Unit *</span>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group form-md-line-input">
								<input autocomplete="off" type="text" class="form-control" id="reorder_level" name="reorder_level" required placeholder="Minimum Stock to be Maintained *">
								<label></label>
								<span class="help-block">Minimum Stock to be Maintained *</span>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group form-md-line-input" id="qty">
								<input autocomplete="off" type="text" id="quantity" class="form-control" name="quantity" required="required" placeholder="Fuel Quantity *">
								<label></label>
								<span class="help-block">Fuel Quantity *</span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions right">
					<button type="submit" class="btn blue" onClick="update()">Submit</button>
					<button type="reset" class="btn default">Reset</button>
				</div>
				<!--- col-sm-12 -->

			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn default">Close</button>
		<button type="submit" class="btn green">Save</button>
	</div>
</div>



<!-- END CONTENT -->
<script>
	function update() {
		var f_id = $( '#fuel_id' ).val();
		var f_name = $( '#fuel_name' ).val();
		var f_des = $( '#fuel_desc' ).val();
		var unit = $( '#unit' ).val();
		var record_lvl = $( '#reorder_level' ).val();
		var qty = $( '#quantity' ).val();

		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>dashboard/update_fuel/",
			data: {
				f_id: f_id,
				f_name: f_name,
				f_des: f_des,
				unit: unit,
				record_lvl: record_lvl,
				qty: qty
			},
			success: function ( data ) {
				if ( data.data == 1 ) {
					$( '#fuel' ).modal( 'hide' );
					swal( {
							title: "Updated Successfully",
							text: "",
							type: "success"
						},
						function () {

							location.reload();

						} );
				}

			}
		} );

	}


	function edit_fuel( val ) {
		//alert(val); return false;
		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>dashboard/fetch_fuel/",
			data: {
				f_id: val
			},
			success: function ( data ) {
				$( '#fuel_id' ).val( data.id );
				$( '#fuel_name' ).val( data.fuel_name );
				$( '#fuel_desc' ).val( data.fuel_desc );
				$( '#unit' ).val( data.unit );
				$( '#reorder_level' ).val( data.reorder_level );
				$( '#quantity' ).val( data.cur_stock );
				$( '#fuel' ).modal( 'show' );
			}
		} );
	}



	function soft_delete_fuel( nm ) {
		swal( {
			title: "Are you sure?",
			text: "This Fuel Entity Will Be Permanently Deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {

			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/delete_fuel/" + nm,
				//data:{booking_id:id},
				success: function ( data ) {
					swal( {
							title: data,
							text: "",
							type: "success"
						},
						function () {

							location.reload();

						} );
				}
			} );



		} );
	}

	function soft_message( nm ) {
		swal( {
			title: " This Fuel Entity (" + nm + ") Cannot Be Deleted",
			text: "Please Delete all the log related to this fuel at first",
			type: "warning",
		}, function () {} );
	}
</script>

<script>
	function soft_delete( id, usage_log_id ) {

		//alert(usage_log_id);
		//return false;

		if ( usage_log_id == '' ) {
			swal( {
				title: "Are you sure?",
				text: "All the releted transactions and data will be deleted",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			}, function () {





				$.ajax( {
					type: "POST",
					url: "<?php echo base_url()?>dashboard/delete_fuel_log?id=" + id,
					data: {
						a_id: id
					},
					success: function ( data ) {
						//alert("Checked-In Successfully");
						//location.reload();
						swal( {
								title: data.data,
								text: "",
								type: "success"
							},
							function () {

								location.reload();

							} );
					}
				} );



			} );
		} else {

			swal( {
					title: "Can not Delete",
					text: "Please delete 'CONS' records from Usage Log",
					type: "warning"
				},
				function () {

					//location.reload();



				} );
			return false;


		}

	}
</script>