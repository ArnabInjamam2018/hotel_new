<script>
    function fetch_all_address_hotel(pincode,g_country,g_state,g_city)
    {
        var pin_code = document.getElementById(pincode).value;
        jQuery.ajax(
            {
                type: "POST",
                url: "<?php echo base_url(); ?>bookings/fetch_address",
                dataType: 'json',
                data: {pincode: pin_code},
                success: function(data){
                    //alert(data.country);
                    $(g_country).val(data.country);
                    $(g_state).val(data.state);
                    $(g_city).val(data.city);
                }

            });
    }
</script>
<script type="text/javascript">
    function submit_form()
    {
        document.getElementById('submit_form').submit();
    }
</script>
<!-- BEGIN PAGE CONTENT-->
<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> ADD SERVICE - <span class="step-title"> Step 1 of 4 </span> </div>
    <div class="tools hidden-xs"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body">
    <form action="<?php echo base_url();?>dashboard/add_service" class=""  enctype="multipart/form-data" id="submit_form" method="POST">
      <div class="form-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group form-md-line-input">
              <?php $warehouse=$this->unit_class_model->all_warehouse(); ?>
              <select class="form-control bs-select" onchange="set_default(this.value)"  name="s_category" required>
                <option value="" >Select</option>
                <?php foreach($warehouse as $w_house){?>
                <option value=<?php echo $w_house->id; if($w_house->default_warehouse=='1'){echo " selected";}?> ><?php echo $w_house->name.$w_house->default_warehouse; ?></option>
                <?php }?>
              </select>
              <label></label>
              <span class="help-block">Set Default *</span> </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<script>
function set_default(v){
	
	//alert("warehouse default"+v+name);
	
	jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>setting/set_default",
                datatype:'json',
                data:{id:v},
                success:function(data)
                {  
				swal("Good job!", "Warehouse Set as Default!", "success")
				
					
                }
            });
}
</script> 
<!-- END PAGE CONTENT-->