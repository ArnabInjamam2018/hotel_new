<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('err_msg');?>
	</strong>
</div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong>
		<?php echo $this->session->flashdata('succ_msg');?>
	</strong>
</div>
<?php endif;?>
<div class="portlet light borderd">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-edit"></i>
			<?php if(isset($pos) && $pos){?>
		</div>
		<div class="actions">
			<a class="btn btn-circle green btn-outline btn-sm" data-toggle="modal" href="#edit_profit"> <i class="fa fa-plus"></i>Edit YummPoss Setting</a>
		</div>
		<?php
			}else{
				?>
				<a class="btn btn-circle green btn-outline btn-sm" data-toggle="modal" href="#responsive"> <i class="fa fa-plus"></i>Add YummPoss Setting</a>
				<?php
			}
			?>
	</div>
	<div class="portlet-body">
		
		<div id="table1">
			<table class="table table-striped table-bordered table-hover" id="sample_1">
				<thead>
					<tr>
						<th scope="col">Hotel Name </th>
						<th scope="col">YummPoss Link </th>
						<th scope="col"> Status </th>
						<th scope="col"> Api Key </th>
						<th scope="col"> Salt Key </th>
						
					</tr>
				</thead>
				<tbody>
					<?php if(isset($pos) && $pos):
                    //print_r($pc);//exit;    
                        $i=1;
                        $hotel_details1 = $this->dashboard_model->get_hotel($pos->hotel_id);
                      
                            $class = ($i%2==0) ? "active" : "success";
                            //$r_id=$items->l_id;
                            ?>
					<tr id="row_<?php echo $pos->s_id;?>">
						<td>
							<?php echo $hotel_details1->hotel_name;?>
          
						</td>
						<td>
							<?php echo $pos->yumm_link;?>
						</td>
						<td> <input type="hidden" id="hid" value="<?php echo $pos->yumm_status;?>">
							<button <?php 
                echo "disabled"; ?> class="btn green btn-xs"  value="<?php echo $pos->s_id;?>"> <span id="demostat<?php echo $pos->yumm_status;?>">
              <?php if($pos->yumm_status == 1){ echo  "Active";}
else{
	
echo "Inactive";
}	?>
              </span> </button>
						</td>
						<td>
							<?php echo $pos->api_key;?>
						</td>
						<td>
							<?php echo $pos->salt_key;?>
						</td>
					
					</tr>
					
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add Profit Center</h4>
			</div>
			<?php

			$form = array(
				'class' => 'form-body',
				'id' => 'form',
				'method' => 'post'
			);

			echo form_open_multipart( 'setting/hotel_to_yummpos_setting/add', $form );

			?>
			<div class="modal-body">
				<div class="row">
				<div class="col-md-4">
				<?php $hotel_details = $this->dashboard_model->get_hotel($this->session->userdata('user_hotel'));
					//print_r($hotel_details);

				?>
						<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control" id="hotel_name" name="hotel_name" value="<?php  echo $hotel_details->hotel_name; ?>">
							<input autocomplete="off" type="hidden" class="form-control" id="hotel_id" name="hotel_id" value ="<?php echo $this->session->userdata('user_hotel'); ?>"placeholder="Enter Hotel Name  ..."  >
							<label></label>
							<span class="help-block">Hotel Name *</span> </div>
					</div>
					
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="yumm_link" name="yumm_link" placeholder="Enter YummPoss link  ..."  >
							<label></label>
							<span class="help-block">YummPoss Link *</span> </div>
					</div>
					
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<select  class="form-control bs-select" id="yumm_status" name="yumm_status" required>
								<option value="" selected="selected" disabled="disabled">yumm status</option>
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
							<label></label>
							<span class="help-block">Default yumm status *</span> </div>
					</div>
					<div class="col-md-12">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="api_key" name="api_key" placeholder="Enter API KEY  ..."  >
							<label></label>
							<span class="help-block">API KEY...</span> </div>
					</div>
					<div class="col-md-12">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="salt_key" name="salt_key" placeholder="Enter salt key Name  ..."  >
							<label></label>
							<span class="help-block">SALT KEY...</span> </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn default">Close</button>
				<button type="submit" class="btn green">Save</button>
			</div>
			<?php echo form_close(); ?> </div>
	</div>
</div>
<!--edit modal -->

<div id="edit_profit" class="modal fade" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit Profit Center</h4>
			</div>
			<?php

			$form = array(
				'class' => 'form-body',
				'id' => 'form',
				'method' => 'post'
			);

			echo form_open_multipart( 'setting/hotel_to_yummpos_setting/edit', $form );

			?>
						<div class="modal-body">
				<div class="row">
				<div class="col-md-4">
				<?php

				if(isset($pos) && $pos){
					
				$hotel_details = $this->dashboard_model->get_hotel($pos->hotel_id);
					//print_r($hotel_details);

				?>
						<div class="form-group form-md-line-input">
						<input autocomplete="off" type="text" class="form-control" id="hotel_name1" name="hotel_name1" value="<?php  echo $hotel_details->hotel_name; ?>">
							<input autocomplete="off" type="hidden" class="form-control" id="hotel_id1" name="hotel_id1" value ="<?php echo $pos->hotel_id; ?>"placeholder="Enter Hotel Name  ..."  >
							<label></label>
							<span class="help-block">Hotel Name *</span> </div>
					</div>
					
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="yumm_link1" name="yumm_link1"  value ="<?php echo $pos->yumm_link;?>" placeholder="Enter YummPoss link  ..."  >
							<label></label>
							<span class="help-block">YummPoss Link *</span> </div>
					</div>
					
					<div class="col-md-4">
						<div class="form-group form-md-line-input">
							<select  class="form-control bs-select" id="yumm_status1" name="yumm_status1" required>
								<option value=""  disabled="disabled">yumm status</option>
								<?php  
								if($pos->yumm_status == 1){
								?>
								<option value="1" selected>Yes</option>
								<option value="0">No</option>
								<?php
								}else{
								?>
								<option value="1" >Yes</option>
								<option value="0" selected>No</option>
								<?php
								}
								?>
							</select>
							<label></label>
							<span class="help-block">Default yumm status *</span> </div>
					</div>
					<div class="col-md-12">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="api_key1" name="api_key1" placeholder="Enter API KEY  ..."  value = "<?php echo $pos->	api_key ?>">
							<label></label>
							<span class="help-block">API KEY...</span> </div>
					</div>
					<div class="col-md-12">
						<div class="form-group form-md-line-input">
							<input autocomplete="off" type="text" class="form-control" id="salt_key1" name="salt_key1" placeholder="Enter salt key Name  ..."   value = "<?php echo $pos->salt_key ?>">
							<label></label>
							<span class="help-block">SALT KEY...</span> </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn default">Close</button>
				<button type="submit" class="btn green">Update</button>
			</div>
			<?php 
				}
			echo form_close(); ?> </div>
	</div>
</div>

<!-- end edit modal --> 
<script>
	function soft_delete( id ) {
		swal( {
			title: "Are you sure?",
			text: "All the releted transactions and data will be deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {





			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/delete_pc?pc_id=" + id,
				data: {},
				success: function ( data ) {
					//alert("Checked-In Successfully");
					//location.reload();
					swal( {
							title: data.data,
							text: "",
							type: "success"
						},
						function () {
							$( '#row_' + id ).remove();

						} );
				}
			} );



		} );
	}


	function status( id ) {
		var hid = $( '#hid' ).val();
		//alert(id);


		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>unit_class_controller/profit_center_status",
			data: {
				id: id,
				status: hid
			},
			success: function ( data ) {
				// alert(data);
				//location.reload();
				document.getElementById( "demostat" + id ).innerHTML = data.data;
				//alert(id);
				$( '#row_' + id ).remove();


			}
		} );

	}


	function check_sub() {
		document.getElementById( 'form_date' ).submit();
	}


	function profit_center() {
		var a = document.getElementById( "demo" ).innerHTML;
		if ( a == "Active Profit Center" ) {
			//$("#status123").removeClass();
			// $("#status123").css('background-color',"blue");
			document.getElementById( "demo" ).innerHTML = "Inactive Profit Center";

		} else {
			//$("#status123").removeClass();
			//$("#status123").css('background-color',"green");
			document.getElementById( "demo" ).innerHTML = "Active Profit Center";
		}


		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>unit_class_controller/profit_center_filter",
			data: {
				status: a
			},
			success: function ( data ) {
				//alert($data);
				$( '#table1' ).html( data );
				$.getScript( '<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons-ex.min.js' );
				$.getScript( '<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js' );

			}
		} );


	}

	function check( value ) {


		if ( value == 1 ) {
			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/check_profit_center_default",
				data: {
					a: value
				},
				success: function ( data ) {
					//alert(data);
					//alert("Checked-In Successfully");
					//location.reload();

					$( '#responsive' ).modal( 'hide' );
					swal( {
							title: "Are you sure?",
							text: data + "!",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Fix as Default!",
							cancelButtonText: "No, cancel plz!",
							closeOnConfirm: true,
							closeOnCancel: true
						},
						function ( isConfirm ) {
							if ( isConfirm ) {
								$( "#default" ).val( '1' );
								$( '#responsive' ).modal( 'show' );
							} else {
								$( "#default" ).val( '0' );
								$( '#responsive' ).modal( 'show' );
							}
						} );




				}
			} );
		}


		//});

	}


	function check1( value ) {


		if ( value == 1 ) {
			$.ajax( {
				type: "POST",
				url: "<?php echo base_url()?>dashboard/check_profit_center_default",
				data: {
					a: value
				},
				success: function ( data ) {
					//alert(data);
					//alert("Checked-In Successfully");
					//location.reload();

					$( '#edit_profit' ).modal( 'hide' );
					swal( {
							title: "Are you sure?",
							text: data + "!",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Fix as Default!",
							cancelButtonText: "No, cancel plz!",
							closeOnConfirm: true,
							closeOnCancel: true
						},
						function ( isConfirm ) {
							if ( isConfirm ) {
								$( "#default1" ).val( '1' );
								$( '#edit_profit' ).modal( 'show' );
							} else {
								$( "#default1" ).val( '0' );
								$( '#edit_profit' ).modal( 'show' );
							}
						} );




				}
			} );
		}


		//});

	}

	function edit_profit( id ) {
		//alert(id);

		$.ajax( {
			type: "POST",
			url: "<?php echo base_url()?>unit_class_controller/get_edit_profit_center",
			data: {
				id: id
			},
			success: function ( data ) {
				//alert(data.id);
				$( '#hid2' ).val( data.id );
				$( '#hid1' ).val( data.profit_center_location );

				$( '#name1' ).val( data.profit_center_location );
				$( '#balance1' ).val( data.profit_center_balance );
				$( '#des1' ).val( data.profit_center_des );
				$( '#default1' ).val( data.profit_center_default );

				//$('#desc1').val(data.status);
				// $('#unit_class1').val(data.change_date);



				$( '#edit_profit' ).modal( 'toggle' );

			}
		} );
	}
</script>