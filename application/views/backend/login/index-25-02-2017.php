<!DOCTYPE html>
<html >
<head>
<meta charset="UTF-8">
<title>HotelObjects</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>

<link href='https://fonts.googleapis.com/css?family=Exo+2:400,600,700,800,500' rel='stylesheet' type='text/css'>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo base_url();?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url();?>assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="<?php echo base_url();?>assets/favicon.ico" />
<!-- END HEAD -->
</head>

<body class="login">
<div class="logo">
 <img src="<?php echo base_url();?>assets/layouts/layout/img/hotel-object1.png" style="width:23%;">
</div>
<!-- BEGIN LOGIN -->
<div class="content" style="background-color: #F7F7F7; color: #555555; box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.2); border-radius:10px;" > 
  <!-- BEGIN LOGIN FORM -->
  <?php

                            $form = array(
                                'class' 			=> 'login-form',
                                'id'				=> 'form',
                                'method'			=> 'post'
                            );

                            echo form_open_multipart('login/user_login',$form);

                            ?>
  <h3 class="form-title uppercase">Login to your account</h3>
  <?php if(isset($msg)&& $msg):
		?>
  <div class="alert alert-danger ">
    <button class="close" data-close="alert"></button>
    <span>
    <?php if(isset($msg) && $msg){
			echo "$msg"; }?>
    </span> </div>
  <?php endif; ?>
  <div class="form-group"> 
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label visible-ie8 visible-ie9">Username</label>
    <div class="input-icon"> <i class="fa fa-user"></i>
      <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="user_name"/>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label visible-ie8 visible-ie9">Password</label>
    <div class="input-icon"> <i class="fa fa-lock"></i>
      <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="user_password"/>
    </div>
  </div>
  <div class="form-actions">  	
    <button type="submit" class="btn btn-success uppercase">Login</button>
    <label class="rememberme check mt-checkbox mt-checkbox-outline">
        <input type="checkbox" name="remember" value="1" />Remember
        <span></span>
    </label>
    <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
  </div>
  
  <?php echo form_close(); ?> 
  <!-- END LOGIN FORM --> 
  <!-- BEGIN FORGOT PASSWORD FORM -->
  <form class="forget-form" action="index.html" method="post">
    <h3>Forget Password ?</h3>
    <p> Enter your e-mail address below to reset your password. </p>
    <div class="form-group">
      <div class="input-icon"> <i class="fa fa-envelope"></i>
        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
      </div>
    </div>
    <div class="form-actions">
      <button type="button" id="back-btn" class="btn"> <i class="m-icon-swapleft"></i> Back </button>
      <button type="submit" class="btn blue pull-right"> Submit <i class="m-icon-swapright m-icon-white"></i> </button>
    </div>
  </form>
  
</div>
<!-- END FORGOT PASSWORD FORM -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright" style="color:#fff;"> 2016 &copy; <a href="http://www.theinfinitytree.com/" title="The Infinity Tree" style="color:#fff;">The Infinity Tree</a> </div>

<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url();?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/login.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS --> 
<script>
jQuery(document).ready(function() { 
       // init background slide images
       $.backstretch([
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/1.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/2i.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/3.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/4.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/5.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/6.jpg",		
        ], {
          fade: 1500,
          duration: 5000
    });
});
</script> 
<!-- END JAVASCRIPTS -->

</body>
</html>