<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->

<!--[if !IE]><!-->

<html lang="en" class="no-js">

<!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

<meta charset="utf-8"/>

<title>Royalpms | <?php if(isset($heading) && $heading) echo $heading; else echo "Dashboard";?></title>

<meta content="width=device-width, initial-scale=1" name="viewport"/>

<!--<link rel="manifest" href="<?php echo base_url();?>assets/manifest.json" />-->
<link rel="shortcut icon" href="<?php echo base_url();?>assets/favicon.ico" type="image/x-icon" />

<!--<link href="https://fonts.googleapis.com/css?family=Exo+2:400,700|Monda:400,700|Open+Sans:400,700|Roboto:400,700" rel="stylesheet">-->

<link href="<?php echo base_url();?>assets/layouts/layout/css/font.css" rel="stylesheet" type="text/css" />

<!-- BEGIN GLOBAL MANDATORY STYLES -->

<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<!--<link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />-->

<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

<!--<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />-->

<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME GLOBAL STYLES -->

<!--<link href="<?php echo base_url();?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />

<link href="<?php echo base_url();?>assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />-->

<link href="<?php echo base_url();?>assets/global/plugins/layout.css" rel="stylesheet" type="text/css" />

<!-- END THEME GLOBAL STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->

<link href="<?php echo base_url();?>assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />

<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME LAYOUT STYLES -->

<!-- END THEME LAYOUT STYLES -->

<link rel="shortcut icon" href="<?php echo base_url();?>assets/favicon.ico" />

<!-- END HEAD -->

</head>



<body class="login">

<div class="col-lg-5 left-login">

<div class="logo">

 <img src="<?php echo base_url();?>assets/layouts/layout/img/logo2.jpg" style="width:23%;">

</div>

<!-- BEGIN LOGIN -->

<div class="content"> 

  <!-- BEGIN LOGIN FORM -->

  <?php

	$form = array(

		'class' 			=> 'login-form',

		'id'				=> 'form',

		'method'			=> 'post'

	);



	echo form_open_multipart('login/user_login',$form);



	?>

  <h3 class="form-title uppercase">Login to your account</h3>

  <?php if(isset($msg)&& $msg):

		?>

  <div class="alert alert-danger ">

    <a class="close" data-close="alert"></a>

    <span>

    <?php if(isset($msg) && $msg){

			echo "$msg"; }?>

    </span> 

  </div>

  <?php endif; ?>

  <div class="form-group form-md-line-input">

    <div class="input-icon"> 

      <!-- <i class="fa fa-user"></i> -->

      <input class="form-control" type="text" autocomplete="off" autofocus placeholder="Username" name="user_name">

      <label></label>

      <span class="help-block">Username</span>

    </div>

  </div>

  <div class="form-group form-md-line-input">

    <div class="input-icon"> 

      <!-- <i class="fa fa-lock"></i> -->

      <input class="form-control password" type="password" autocomplete="off" autofocus placeholder="Password" name="user_password">

      <a href="javascript:void(0);" class="pass-view" id="showHide"><i class="fa fa-eye-slash"></i></a>

      <label></label>

      <span class="help-block">Password</span>

    </div>

  </div>

  

  <div class="form-actions">  	

    <button type="submit" class="btn btn-success uppercase">Login</button>

    <label class="rememberme check mt-checkbox mt-checkbox-outline">

        <input type="checkbox" name="remember" value="1" />Remember

        <span></span>

    </label>

    <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>

  </div>  

  <?php echo form_close(); ?> 

  <!-- END LOGIN FORM --> 

  <!-- BEGIN FORGOT PASSWORD FORM -->

  <form class="forget-form" action="index.html" method="post">

    <h3>Forget Password ?</h3>

    <p> Enter your e-mail address below to reset your password. </p>

    <div class="form-group form-md-line-input">

      <div class="input-icon"> 

        <!-- <i class="fa fa-envelope"></i> -->

        <input class="form-control" type="text" autocomplete="off" placeholder="Email" name="email"/>

      </div>

    </div>

    <div class="form-actions">

      <button type="button" id="back-btn" class="btn"> <i class="m-icon-swapleft"></i> Back </button>

      <button type="submit" class="btn blue pull-right"> Submit <i class="m-icon-swapright m-icon-white"></i> </button>

    </div>

  </form>

</div>

<!-- END FORGOT PASSWORD FORM -->

<!-- BEGIN COPYRIGHT -->

<div class="copyright" style="color:#fff;"> 

	<?php echo date('Y'); ?> &copy; All Rights Reserved @ <a href="http://royalpms.com" title="BookMania" target="_blank" style="color:#fff;">RoyalPMS</a> 

</div>

</div>


<div class="col-lg-7 right-login">
  <img src="<?php echo base_url();?>assets/layouts/layout/img/hotel-login.jpg">
</div>

<!--[if lt IE 9]>

<script src="../assets/global/plugins/respond.min.js"></script>

<script src="../assets/global/plugins/excanvas.min.js"></script> 

<script src="../assets/global/plugins/ie8.fix.min.js"></script> 

<![endif]-->

<!-- BEGIN CORE PLUGINS -->

<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>

<!--<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>-->

<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript">

</script>

<script src="<?php echo base_url();?>assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->

<script src="<?php echo base_url();?>assets/global/scripts/app.min.js" type="text/javascript"></script>

<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/pages/scripts/login.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS --> 

<script>

jQuery(document).ready(function() { 

       // init background slide images

  /*     $.backstretch([

        "<?php //echo base_url();?>assets/global/plugins/backstretch/images/1.jpg",

        "<?php //echo base_url();?>assets/global/plugins/backstretch/images/2i.jpg",

        "<?php //echo base_url();?>assets/global/plugins/backstretch/images/3.jpg",

        "<?php //echo base_url();?>assets/global/plugins/backstretch/images/4.jpg",

        "<?php //echo base_url();?>assets/global/plugins/backstretch/images/5.jpg",

        "<?php //echo base_url();?>assets/global/plugins/backstretch/images/6.jpg",		

        ], {

          fade: 1500,

          duration: 5000

    });*/

});

</script> 

<script>

$(document).ready(function() {

  $("#showHide").click(function() {

    if ($(".password").attr("type") == "password") {

      $(".password").attr("type", "text");	

    } else {

      $(".password").attr("type", "password");

    }

  });

  $('.pass-view').click(function() {

		$('.pass-view .fa').toggleClass('fa-eye');

	});

});

</script>

<!-- END JAVASCRIPTS -->



</body>

</html>