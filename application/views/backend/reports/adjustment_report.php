
    <div class="portlet light borderd">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-balance-scale" aria-hidden="true"></i> Adjustment Report </div>
        <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            
            <div class="col-md-8">
              <?php

	                            $form = array(
	                                'class'     => 'form-inline ftop',
	                                'id'        => 'form_date',
	                                'method'    => 'post'
	                            );

	                            echo form_open_multipart('dashboard/get_booking_report_by_date',$form);

	                            ?>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
              </div>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
              </div>
			  
			  <div class="form-group">
              <button onclick="get_val();" class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search" aria-hidden="true"></i></button>
			  </div>
			 
                  
              <?php form_close(); ?>
            </div>
            <script type="text/javascript">
				function check_sub(){
				   document.getElementById('form_date').submit();
				}
			</script>
            
          </div>
        </div>
		
	<p class="font-green-sharp">Please use the table below to navigate or filter the results. You can download the table as excel and pdf.</p>	
	<ul class="nav nav-tabs">
                                            <li class="active">												
                                                <a href="#adjustmentData" data-toggle="tab" aria-expanded="true"> <i class="fa fa-calendar" aria-hidden="true"></i> Data </a>
                                            </li>
											
                                            <li class="">												
                                                <a href="#graph" data-toggle="tab" aria-expanded="false">  <i class="fa fa-tachometer" aria-hidden="true"></i> Graph </a>
                                            </li>
                                          
    </ul>
       
	   <div class="tab-content">
										
      <div class="tab-pane active" id="adjustmentData">
        <div id="table1">		
       <table class="table table-striped table-bordered table-hover" id="sample_3">

          <thead>
		  
            <tr>
              <th width="3%"> # </th>
			  <th width="8%"> Added On </th>
              <th width="5%"> Booking ID </th>
			  <th width="5%"> Source </th>
              <th width="8%"> Guest </th>
              <th width="8%"> Added By </th>
			  <th width="5%"> Dr/ Cr </th>
			  <th width="5%"> Amount </th>
			  <th width="5%"> Total Booking Amount </th> 
              <th width="10%"> Reason </th>
			  <th width="10%"> Adjustment Note </th>             
            </tr>
			
          </thead>
		  
          <tbody>
            <?php 
			
			if(isset($adjustment) && $adjustment){
					
					$j=0;
					$p=0;
					$p1=0;
					$check = '';
					
					$today = new DateTime('now');
					

			foreach($adjustment as $book){ 
									
					$j++;
					$stl = '';
					$stl2 = '';
					
					if(strtotime($book->addOn) == strtotime(date("Y-m-d"))){
						
						$stl = 'color:#04CC58; font-weight:600;';
						$stl2 = 'background-color:#F4FFEC;';
						
					}	
					
					if(isset($book->bar_color_code) && $book->bar_color_code){
						$stCol = $book->bar_color_code; 
						$stbCol = $book->body_color_code;
					}
					else{
						$stCol = '';
						$stbCol = '';
					}
			
			?>
         
									
          <tr style="<?php echo $stl2;?>">
		  
            <td>  
				  <?php 
					echo $j;
				  ?>
			 </td>
			 
			 <td>  
				  <?php 
					
					echo date("g:i A \-\n l jS F Y", strtotime($book->addedOn));
					
				  ?>
			 </td>
			 
			 <td>  
				  <?php 
					if($book->booking_id && isset($book->booking_id) && $book->group_id == NULL)
						echo '<span  class="label-flat" style="font-size:14px; color:'.$stCol.'; background:'.$stbCol.';">BK0'.$book->hotel_id.'/0'.$book->booking_id.'</span>';	
					else
						echo '<span class="label-flat" style="font-size:14px; color:'.$stCol.'; background:'.$stbCol.';">GBK0'.$book->hotel_id.'/0'.$book->group_id.'</span>';	
				  ?>
			 </td>
			
			 <td>  
				  <?php 
					echo $book->booking_source_name;					
				  ?>
			 </td>
			 
			 <td>  
				  <?php 
					echo $book->g_name;					
				  ?>
			 </td>
			
			<td>  
				  <?php 
					
					echo $book->admin_name;
					
				  ?>
			</td>	
			 
			<td>
									<?php 
										
										if($book->dr_cr == 'Dr'){
											$drcls = "fa fa-arrow-right";
											$cls = "purple";
										}
										else{
											$drcls = "fa fa-arrow-left";
											$cls = "red";
										}
										?>
										<label class="label" style="background-color: <?php echo $cls;?>"> <span class="hidden-xs"> <?php echo '<i class="'.$drcls.'" style="font-size:12px;"></i> '.$book->dr_cr; ?> </span> </label>
									
			</td>
			
			<td>
									<?php
										if($book->amount < 0){
											$amcol = '#F44040';
										}
										else
											$amcol = '#41417D';
									?>
									<label> <?php echo '<i class="fa fa-inr" style="font-size:12px; color:'.$amcol.';"></i> '.$book->amount; ?> </label>
			</td>
			 
			 
			<td>
									<?php 
										echo $book->bookingTotal;
									?>
			</td>
			
			<td>
									<?php 
										echo $book->reason;
									?>
			</td>
			
			<td>
						<?php
							echo $book->note;
						?>
			</td>
			
			
			
			
			
          </tr>
		   <input type="hidden" id="item_no" value="<?php //echo $item_no;?>"> </input>
		   
			
          
          <?php  
			}}
		  ?>
         
            </tbody>
          
        </table>
      
    </div>
                                              
	  </div>
	  
	  <div class="tab-pane" id="graph">
       
			Need Graph here
                                              
	  </div>
												  
	
	  </div>
	</div>
    </div>

<!-- END CONTENT --> 