<div style="border: 1px solid #607D8B; border-top: 0;" class="portlet box blue">
  <div class="portlet-title" style="background-color:#607D8B;">
    <div class="caption"> <i syle="font-size:18px;" class="fa fa-bar-chart" aria-hidden="true"></i> <span class="caption-subject bold uppercase">Chart Implementation Examples</span> <span class="pull-right" style="font-size:12px;"> &nbsp for reference purpose</span></div>
  </div>
  <div class="portlet-body form">
 
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
       <script src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/pie.js" type="text/javascript"></script>
	   <script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/amcharts.js"></script>
       <script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/serial.js"></script>
        <link rel="stylesheet" href="http://192.168.1.3/hotelobjects_dev/assets/amcharts/style.css" type="text/css">

 
 <script>



           $( document ).ready(function () {
               // generate some random data first
               generateChartData();
           });
		   

           // generate some random data, quite different range
           function generateChartData() {
               
			   let id = '';
			   
			   $.ajax({
					url: "<?php echo base_url()?>reports/chartList4",
					type:"POST",
					data:{id:id},
					success:function(data)
					{
						console.log(data);
						generateChart(data);
						//pushData(data);
						alert('Ajax Success');
					}
				});
           }
		   
		   function pushData(data){
		   //function pushData(){
			   
			    var chart;
				var chartData = [];
			   
			   alert('pushData');
			   let cnt = 0;
			   var firstDate = new Date();
               firstDate.setDate(firstDate.getDate() - 50);
			   
			   $.each(data, function(key, value){
						   //console.log(value.income);
						
						   var newDate = new Date(firstDate);
						   newDate.setDate(newDate.getDate() + cnt);
						   //var newDate = value.date;

						  
						//   console.log('Date: '+newDate+' income '+visits+' expense '+hits+' incCnt '+views+' expCnt '+bookings);

						   chartData.push({
							  
							  // views: views,
							  
							   nature_visits:nature_visit,
							   no_of_adults:no_of_adult
						   });
						   
						cnt = cnt +1;
					}
			   );
			   
			   generateChart(chartData)
			   
		   }
		   
		   let chart = new AmCharts.AmSerialChart();
		   
		   function generateChart(chartData){
			    // SERIAL CHART
				
				//var chartData = <?php echo $chartDatajson; ?>;
				
                chart = new AmCharts.AmPieChart();
                chart.dataProvider = chartData;
                chart.titleField = "Booking_Source";
                chart.valueField = "no_of_adults";
                chart.gradientRatio = [0, 0, 0 ,-0.2, -0.4];
                chart.gradientType = "radial";

                // LEGEND
                legend = new AmCharts.AmLegend();
                legend.align = "center";
                legend.markerType = "circle";
                chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                chart.addLegend(legend);

                // WRITE
                chart.write("chartdiv");

               // AXES
               // category
           
		   }

  
  
  		   // this method is called when chart is first inited as we listen for "dataUpdated" event
           function zoomChart() {
               // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
               chart.zoomToIndexes(10, 20);
           }
		   
	
        </script>
        
      
       <h1>1.  Pie Chart With Legend</h1>
      <div id="chartdiv" style="width: 100%; height: 400px;"></div>
        <table align="center" cellspacing="20">
            <tr>
                <td>
                    <input type="radio" checked="true" name="group" id="rb1" onclick="setLabelPosition()">labels outside
                    <input type="radio" name="group" id="rb2" onclick="setLabelPosition()">labels inside</td>
                <td>
                    <input type="radio" name="group2" id="rb3" onclick="set3D()">3D
                    <input type="radio" checked="true" name="group2" id="rb4" onclick="set3D()">2D</td>
                <td>Legend switch type:
                    <input type="radio" checked="true" name="group3" id="rb5"
                    onclick="setSwitch()">x
                    <input type="radio" name="group3" id="rb6" onclick="setSwitch()">v</td>
            </tr>
        </table>
      
    
  </div>
    
    
    
    
  <!-- END CONTENT --> 
</div>
