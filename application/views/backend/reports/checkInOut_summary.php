
    <div class="portlet light borderd">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-th-list"></i> Checkin & Checkout Summary  </div>
        <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            
            <div class="col-md-8">
              <?php

	                            $form = array(
	                                'class'     => 'form-inline ftop',
	                                'id'        => 'form_date',
	                                'method'    => 'post'
	                            );

	                            echo form_open_multipart('dashboard/get_booking_report_by_date',$form);

	                            ?>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
              </div>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
              </div>
			  
			  <div class="form-group">
              <button onclick="get_val();" class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search" aria-hidden="true"></i></button>
			  </div>
			 
                  
              <?php form_close(); ?>
            </div>
            <script type="text/javascript">
				function check_sub(){
				   document.getElementById('form_date').submit();
				}
			</script>
            
          </div>
        </div>
		
	<p class="font-green-sharp">Please use the table below to navigate or filter the results. You can download the table as excel and pdf.</p>	
	<ul class="nav nav-tabs">
                                            <li class="active">												
                                                <a href="#Checkouts" data-toggle="tab" aria-expanded="true"> <i class="fa fa-calendar" aria-hidden="true"></i> Checkouts </a>
                                            </li>
											
                                            <li class="">												
                                                <a href="#Checkins" data-toggle="tab" aria-expanded="false">  <i class="fa fa-tachometer" aria-hidden="true"></i> Checkins </a>
                                            </li>
                                            <!--<li class="active">
                                                <a href="#tab_15_3" data-toggle="tab" aria-expanded="true"> Section 3 </a>
                                            </li>-->
    </ul>
                                        <div class="tab-content">
										
      <div class="tab-pane active" id="Checkouts">
        <div id="table1">		
       <table class="table table-striped table-bordered table-hover" id="sample_3">
        <!--<table class="table table-striped">-->
          <thead>
            <tr>
              
			  <!--<th> Select </th>-->
              
              
              <th width="3%"> # </th>
              <th width="12%"> Date </th>
              <th width="10%"> Rooms Checked out </th>
              <th width="5%"> Groups Checked out </th>
			  <th width="5%"> Guests Checked out </th>            
			  <th width="5%"> Expected Checked outs </th>            
			  <th width="5%"> Checkout pending </th>            
			  <th width="5%"> Checkin pending </th>            
              
             
            </tr>
          </thead>
          <tbody>
            <?php 
			
			if(isset($checkout) && $checkout){
					//echo "<pre>";
					//print_r($bookingDate);
					//exit;
					$j=0;
					$p=0;
					$p1=0;
					$check = '';
					
					$today = new DateTime('now');
					
			//print_r($bookingSource);
			foreach($checkout as $book){ 
									
					$j++;
					$stl = '';
					$stl2 = '';
					
					if(strtotime($book->date) == strtotime(date("Y-m-d"))){
						
						$stl = 'color:#04CC58; font-weight:600;';
						$stl2 = 'background-color:#F4FFEC;';
						//echo date("Y-m-d").' - '.$book->date;
					}		
			
			?>
         
									
          <tr style="<?php echo $stl2;?>">
		  
            <td>  
				  <?php 
					echo $j;
				  ?>
			 </td>
			
			 <td>  
				  <?php 
					echo '<span style="font-size:14px;'.$stl.'">'.date("l jS F Y",strtotime($book->date)).'</span>';				
				  ?>
			 </td>
			 
			 
			 
			<td>
									<?php 
											echo $book->roomsCheckout;
											//echo '<br/> '.$book->date;
									?>
			</td>
			
			<td>
									<?php 
											echo $book->groupsCheckout;
									?>
			</td>
			
			<td>
									<?php
										echo $book->guestCheckout;
									?>
			</td>
			
			<td>
						<?php
							$exp = $this->reports_model->getCheckoutExp($book->date);
							if(isset($exp) && $exp){									
							
								foreach($exp as $exp){ 
										
									if($exp->count != NULL && isset($exp->count))
										echo $exp->count;
									else
										echo 0;
								

						?>
			</td>
			
			<td>
									<?php
										if($exp->countCheckin != NULL && isset($exp->countCheckin)){
											if($exp->countCheckin > 0)
												echo '<span style="color:red;">'.$exp->countCheckin.'<span>';
											else
												echo $exp->countCheckin;
										}
										else
											echo 0;								
									?>
			</td>
			
			<td>
									<?php
										$diff = $exp->count - $exp->countPending;
										
										if($exp->countPending != NULL && isset($exp->countPending)){
											if($diff > 0)
												echo '<span style="color:red;">'.$exp->countPending.'<span>';
											else
												echo $exp->countPending;
										}
										else
											echo 0;	
										
									?>
									
			</td>
			
          </tr>
		   <input type="hidden" id="item_no" value="<?php //echo $item_no;?>"> </input>
		   
			
          
          <?php  
			}}}}
		  ?>
         
            </tbody>
          
        </table>
      
    </div>
                                              
	  </div>
	  
	  <div class="tab-pane" id="Checkins">
        <div id="table2">		
       <table class="table table-striped table-bordered table-hover" id="sample_2">
        <!--<table class="table table-striped">-->
          <thead>
            <tr>
              
			  <!--<th> Select </th>-->
              
              
              <th width="3%"> # </th>
              <th width="10%"> Date </th>
              <th width="5%"> Rooms Checked in </th>
              <th width="5%"> Groups Checked in </th>
			  <th width="5%"> Guests Checkedin </th>            
			  <th width="5%"> Expected Checked ins </th>            
			  <th width="5%"> Checkins/ Checkouts </th>            
			  <th width="5%"> Checkin pending </th>            
              
             
            </tr>
          </thead>
          <tbody>
            <?php 
			
			if(isset($checkin) && $checkin){
					//echo "<pre>";
					//print_r($bookingDate);
					//exit;
					$j=0;
					$p=0;
					$p1=0;
					$check = '';
					
					$today = new DateTime('now');
					
			//print_r($bookingSource);
			foreach($checkin as $checkin){ 
									
					$j++;
					$stl = '';
					$stl2 = '';
					
					if(strtotime($checkin->date) == strtotime(date("Y-m-d"))){
						
						$stl = 'color:#04CC58; font-weight:600;';
						$stl2 = 'background-color:#F4FFEC;';
						//echo date("Y-m-d").' - '.$checkin->date;
					}		
			
			?>
         
									
          <tr style="<?php echo $stl2;?>">
		  
            <td>  
				  <?php 
					echo $j;
				  ?>
			 </td>
			
			 <td>  
				  <?php 
					echo '<span style="font-size:14px;'.$stl.'">'.date("l jS F Y",strtotime($checkin->date)).'</span>';				
				  ?>
			 </td>
			 
			 
			 
			<td>
									<?php 
											echo $checkin->roomsCheckin;
											//echo '<br/> '.$checkin->date;
									?>
			</td>
			
			<td>
									<?php 
											echo $checkin->groupsCheckin;
									?>
			</td>
			
			<td>
									<?php
										echo $checkin->guestCheckin;
									?>
			</td>
			
			<td>
						<?php
							$expc = $this->reports_model->getCheckinExp($checkin->date);
							if(isset($expc) && $expc){									
							
								foreach($expc as $expc){ 
										
									if($expc->count != NULL && isset($expc->count))
										echo $expc->count;
									else
										echo 0;
								

						?>
			</td>
			
			<td>
									<?php
										$tot = $expc->countCheckin + $expc->countCheckout;
										
										if($tot != NULL && isset($tot)){
												echo $expc->countCheckin.' + '.$expc->countCheckout;
										}
										else
											echo 0;									
									?>
			</td>
			
			<td>
									<?php
										
										if($expc->countNonCheckin != NULL && isset($expc->countNonCheckin)){
											if($expc->countNonCheckin > 0)
												echo '<span style="color:red;">'.$expc->countNonCheckin.'<span>';
											else
												echo $expc->countNonCheckin;
										}
										else
											echo 0;
									?>
									
			</td>
			
          </tr>
		   <input type="hidden" id="item_no" value="<?php //echo $item_no;?>"> </input>
		   
			
          
          <?php  
			}}}}
		  ?>
         
            </tbody>
          
        </table>
      
    </div>
                                              
	  </div>
												  
	
	  </div>
	</div>
    </div>

<!-- END CONTENT --> 