
<div class="portlet light borderd">
  <div class="portlet-title">
    <div class="caption"><strong> <i class="fa fa-inr" aria-hidden="true"></i></strong>  Daily Transactional Summary  </div>
    <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-8">
          <?php

	                            $form = array(
	                                'class'     => 'form-inline ftop',
	                                'id'        => 'form_date',
	                                'method'    => 'post'
	                            );

	                            echo form_open_multipart('dashboard/get_booking_report_by_date',$form);

	                            ?>
          <div class="form-group">
            <input type="text" autocomplete="off"  value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
          </div>
          <div class="form-group">
            <input type="text" autocomplete="off"  value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
          </div>
          <div class="form-group">
            <button class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
          <?php form_close(); ?>
        </div>
        <script type="text/javascript">
				function check_sub(){
				   document.getElementById('form_date').submit();
				}
			</script> 
      </div>
    </div>
    
    <!--<p class="font-green-sharp">Please use the table below to navigate or filter the results. You can print, copy, download the table as excel and pdf.</p>-->
    
    <ul class="nav nav-tabs">
      <li class="active"> <a href="#Daily-re" data-toggle="tab" aria-expanded="true"> <i class="fa fa-tachometer" aria-hidden="true"></i> Daily </a> </li>
      <li class=""> <a href="#Monthly-re" data-toggle="tab" aria-expanded="false"> <i class="fa fa-calendar" aria-hidden="true"></i> Monthly</a> </li>
    </ul>
    <div class="tab-content">
    	<div class="tab-pane active" id="Daily-re">
            <table class="table table-striped table-bordered table-hover" id="sample_3">
              <!--<table class="table table-striped">-->
              <thead>
                <tr>
                  <th width="3%"> # </th>
                  <th width="12%"> Date </th>
                  <th width="5%"> Dist by Status </th>
                  <th width="10%"> Dist By Type </th>
                  <th width="5%"> Dist by ProfitCenter </th>
                  <th width="5%"> Dist by Mode </th>
                  <th width="5%"> Income </th>
                  <th width="5%"> Expense </th>
                </tr>
              </thead>
              <tbody>
                <?php 
                
                if(isset($transactions) && $transactions){
                        //echo "<pre>";
                        //print_r($bookingDate);
                        //exit;
                        $j=0;
                        $p=0;
                        $p1=0;
                        $check = '';
                        
                        $today = new DateTime('now');
                        
                //print_r($bookingSource);
                    foreach($transactions as $transactions){ 
                                        
                        $j++;
                        $stl = '';
                        $stl2 = '';
                        
                        if(strtotime($transactions->date) == strtotime(date("Y-m-d"))){
                            
                            $stl = 'color:#04CC58; font-weight:600;';
                            $stl2 = 'background-color:#F4FFEC;';
                            //echo date("Y-m-d").' - '.$book->date;
                        }	
    
                        if(1==1){
                            $stl1 = '';
                        }
                        else
                            $stl1 = '';
                
                ?>
                <tr style="<?php echo $stl2;?>">
                  <td><?php 
                        echo $j;
                      ?></td>
                  <td><?php 
                        echo '<span style="font-size:14px;'.$stl.'">'.date("D jS F Y",strtotime($transactions->date)).'</span>';
                      ?></td>
                  <td>
                      <?php 
                        $type = $this->reports_model->getTransacByDateGrpSts($transactions->date);
            
                                if(isset($type) && $type){
                                    foreach($type as $type){
                                        $col2 = '';
                                        $col1 = '';
                                        $sign = '';
                                        if($type->income == 0)
                                            $col2 = '#AAAAAA';
                                        else{
                                            if($type->sts == "Done" || $type->sts == "done" || $type->sts == " Done" || $type->sts == "Done ")
                                            $col2 = '#5DC378';
                                            else if($type->sts == "Pending")
                                                $col2 = '#FB9C3C';
                                            else
                                                $col2 = '#FF0033';
                                        }
                                        
                                        if($type->expense == 0)
                                            $col1 = '#AAAAAA';
                                        else{
                                            if($type->sts == "Done" || $type->sts == "done" || $type->sts == " Done" || $type->sts == "Done ")
                                            $col1 = '#5DC378';
                                            else if($type->sts == "Pending")
                                                $col1 = '#FB9C3C';
                                            else
                                                $col1 = '#FF0033';
                                            $sign = ' - ';
                                        }
                    ?>  
                       <div class="tr-de">                     
                           <span><?php echo $type->sts ?></span>
                           <?php echo '<a href="javascript:;" style="color:'.$col2.'; font-weight:600;"><small class="badge" style="background-color:#127561;">'.$type->cinc.'</small>'.'Rs.'.$type->income.'</a>';?>                       
                          <?php echo '<a href="javascript:;" style="color:'.$col1.'; font-weight:600;"><small class="badge" style="background-color:#A40C0C;"> '.$type->cexp.'</small>'.$sign.'Rs.'.$type->expense.'</a>';?>
                      </div>
                      <?php 			}
                                }
                                else
                                    echo 'no info';
                    ?>
                  </td>
                  <td>
                      <?php 
                        $type = $this->reports_model->getTransacByDateGrpType($transactions->date);
                                                    
                                if(isset($type) && $type){
                                    foreach($type as $type){
                                        $col2 = '';
                                        $col1 = '';
                                        $sign = '';
                                        if($type->income == 0)
                                            $col2 = '#AAAAAA';
                                        if($type->expense == 0)
                                            $col1 = '#AAAAAA';
                                        else 
                                            $sign = ' - ';
                    ?>
                    	<div class="tr-de">  
							<span><?php echo $type->type ?></span>
                            <?php echo '<a href="javascript:;" style="color:'.$col2.'; font-weight:600;"><small class="badge" style="background-color:#127561;"> '.$type->cinc.'</small>'.'Rs.'.$type->income.'</a>';?>
                            <?php echo '<a href="javascript:;" style="color:'.$col1.'; font-weight:600;"><small class="badge" style="background-color:#A40C0C;">'.$type->cexp.'</small>'.$sign.'Rs.'.$type->expense.'</a>';?> 
                        </div>
                      <?php 			}
                                }
                                else
                                    echo 'no info';
                    ?>
                  </td>
                  <td>
                      <?php 
                        $type = $this->reports_model->getTransacByDateGrpPC($transactions->date);
                                                    
                                if(isset($type) && $type){
                                    foreach($type as $type){
                                        $col2 = '';
                                        $col1 = '';
                                        $sign = '';
                                        if($type->income == 0)
                                            $col2 = '#AAAAAA';
                                        if($type->expense == 0)
                                            $col1 = '#AAAAAA';
                                        else 
                                            $sign = ' - ';
                    ?> 
                    	<div class="tr-de">   
                      	  <span><?php echo $type->pc ?></span>                         
						  <?php echo '<a href="javascript:;" style="color:'.$col2.'; font-weight:600;"><small class="badge" style="background-color:#127561;">'.$type->cinc.'</small>'.'Rs.'.$type->income.'</a>';?> 
                          <?php echo '<a href="javascript:;" style="color:'.$col1.'; font-weight:600;"><small class="badge" style="background-color:#A40C0C;">'. $type->cexp.'</small>'.$sign.'Rs.'.$type->expense.'</a>';?>
                        </div>
                      <?php 			}
                                }
                                else
                                    echo 'no info';
                    ?>
                  </td>
                  <td>
                      <?php 
                        $type = $this->reports_model->getTransacByDateGrpMode($transactions->date);
                                                    
                                if(isset($type) && $type){
                                    foreach($type as $type){
                                        $col2 = '';
                                        $col1 = '';
                                        $sign = '';
                                        if($type->income == 0)
                                            $col2 = '#AAAAAA';
                                        if($type->expense == 0)
                                            $col1 = '#AAAAAA';
                                        else 
                                            $sign = ' - ';
                    ?>
                    	<div class="tr-de">  
                      	  <span><?php echo $type->mode ?></span>                      
                          <?php echo '<a href="javascript:;" style="color:'.$col2.'; font-weight:600;"><small class="badge" style="background-color:#127561;"> '.$type->cinc.'</small>'.'Rs.'.$type->income.'</a>';?>
                          <?php echo '<a href="javascript:;" style="color:'.$col1.'; font-weight:600;"><small class="badge" style="background-color:#A40C0C;">'.$type->cexp.'</small>'.$sign.'Rs.'.$type->expense.'</a>';?>
                         </div>
                      <?php 			}
                                }
                                else
                                    echo 'no info';
                    ?>
                  </td>
                  <td><?php 
                        echo '<h1 style="font-size:20px; font-weight:400; color:#127561;"> <i class="fa fa-inr" aria-hidden="true"></i> '.$transactions->income.'</h1>';						
                    ?></td>
                  <td><?php 
                        echo '<h1 style="font-size:20px; font-weight:400; color:#A40C0C;"> <i class="fa fa-inr" aria-hidden="true"></i> '.$transactions->expense.'</h1>';				
                    ?></td>
                </tr>
              <input type="hidden" id="item_no" value="<?php //echo $item_no;?>">
                </input>
              
              <?php  
                }}
              ?>
                </tbody>
              
            </table>
        </div>
        <div class="tab-pane" id="Monthly-re">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
              <!--<table class="table table-striped">-->
              <thead>
                <tr>
                  <th width="3%"> # </th>
                  <th width="12%"> Date </th>
                  <th width="5%"> Dist by Status </th>
                  <th width="10%"> Dist By Type </th>
                  <th width="5%"> Dist by ProfitCenter </th>
                  <th width="5%"> Dist by Mode </th>
                  <th width="5%"> Income </th>
                  <th width="5%"> Expense </th>
                </tr>
              </thead>
              <tbody>
                <?php 
                
                if(isset($transactionsMon) && $transactionsMon){
                        //echo "<pre>";
                        //print_r($bookingDate);
                        //exit;
                        $j=0;
                        $p=0;
                        $p1=0;
                        $check = '';
                        
                        $today = new DateTime('now');
                        
                //print_r($bookingSource);
                    foreach($transactionsMon as $transactions){ 
                                        
                        $j++;
                        $stl = '';
                        $stl2 = '';
                        
                        if(strtotime($transactions->month) == strtotime(date("M"))){
                            
                            $stl = 'color:#04CC58; font-weight:600;';
                            $stl2 = 'background-color:#F4FFEC;';
                            //echo date("Y-m-d").' - '.$book->date;
                        }	
    
                        if(1==1){
                            $stl1 = '';
                        }
                        else
                            $stl1 = '';
                
                ?>
                <tr style="<?php echo $stl2;?>">
                  <td><?php 
                        echo $j;
                      ?></td>
                  <td><?php 
                        echo '<span style="font-size:14px;'.$stl.'">'.$transactions->month.' '.$transactions->year.'</span>';
                        //echo '</br>'.$transactions->cinc.' - '.$transactions->cexp;
                      ?></td>
                  <td>
                      <?php 
                        $type = $this->reports_model->getTransacByDateGrpStsMon($transactions->month,$transactions->year);
            
                                if(isset($type) && $type){
                                    foreach($type as $type){
                                        $col2 = '';
                                        $col1 = '';
                                        $sign = '';
                                        if($type->income == 0)
                                            $col2 = '#AAAAAA';
                                        else{
                                            if($type->sts == "Done" || $type->sts == "done" || $type->sts == " Done" || $type->sts == "Done ")
                                            $col2 = '#5DC378';
                                            else if($type->sts == "Pending")
                                                $col2 = '#FB9C3C';
                                            else
                                                $col2 = '#FF0033';
                                        }
                                        
                                        if($type->expense == 0)
                                            $col1 = '#AAAAAA';
                                        else{
                                            if($type->sts == "Done" || $type->sts == "done" || $type->sts == " Done" || $type->sts == "Done ")
                                            $col1 = '#5DC378';
                                            else if($type->sts == "Pending")
                                                $col1 = '#FB9C3C';
                                            else
                                                $col1 = '#FF0033';
                                            $sign = ' - ';
                                        }
                    ?>
                      <div class="tr-de">                     
                          <span><?php echo $type->sts ?></span>
                          <?php echo '<a href="javascript:;" style="color:'.$col2.'; font-weight:600;"><small class="badge" style="background-color:#127561;">'.$type->cinc.'</small>'.'Rs.'.$type->income.'</a>';?>
                          <?php echo '<a href="javascript:;" style="color:'.$col1.'; font-weight:600;"><small class="badge" style="background-color:#A40C0C;">'.$type->cexp.'</small>'.$sign.'Rs.'.$type->expense.'</a>';?>
                      </div>
                      <?php 			}
                                }
                                else
                                    echo 'no info';
                    ?>
                  </td>
                  <td>
                      <?php 
                        $type = $this->reports_model->getTransacByDateGrpTypeMon($transactions->month,$transactions->year);
                                                    
                                if(isset($type) && $type){
                                    foreach($type as $type){
                                        $col2 = '';
                                        $col1 = '';
                                        $sign = '';
                                        if($type->income == 0)
                                            $col2 = '#AAAAAA';
                                        if($type->expense == 0)
                                            $col1 = '#AAAAAA';
                                        else 
                                            $sign = ' - ';
                    ?>
                      <div class="tr-de">                     
                          <span><?php echo $type->type ?></span>
						  <?php echo '<a href="javascript:;" style="color:'.$col2.'; font-weight:600;"><small class="badge" style="background-color:#127561;">'.$type->cinc.'</small>'.'Rs.'.$type->income.'</a>';?>
                          <?php echo '<a href="javascript:;" style="color:'.$col1.'; font-weight:600;"><small class="badge" style="background-color:#A40C0C;">'.$type->cexp.'</small>'.$sign.'Rs.'.$type->expense.'</a>';?>
                      </div>
                      <?php 			}
                                }
                                else
                                    echo 'no info';
                    ?>
                  </td>
                  <td>
                      <?php 
                        $type = $this->reports_model->getTransacByDateGrpPCMon($transactions->month,$transactions->year);
                                                    
                                if(isset($type) && $type){
                                    foreach($type as $type){
                                        $col2 = '';
                                        $col1 = '';
                                        $sign = '';
                                        if($type->income == 0)
                                            $col2 = '#AAAAAA';
                                        if($type->expense == 0)
                                            $col1 = '#AAAAAA';
                                        else 
                                            $sign = ' - ';
                    ?>
                    	<div class="tr-de">                     
                          <span><?php echo $type->pc ?></span>                      
						  <?php echo '<a href="javascript:;" style="color:'.$col2.'; font-weight:600;"><small class="badge" style="background-color:#127561;">'.$type->cinc.'</small>'.'Rs.'.$type->income.'</a>';?> 
                          <?php echo '<a href="javascript:;" style="color:'.$col1.'; font-weight:600;"><small class="badge" style="background-color:#A40C0C;">'.$type->cexp.'</small>'.$sign.'Rs.'.$type->expense.'</a>';?>
                        </div>
                      <?php 			}
                                }
                                else
                                    echo 'no info';
                    ?>
                  </td>
                  <td>
                      <?php 
                        $type = $this->reports_model->getTransacByDateGrpModeMon($transactions->month,$transactions->year);
                                                    
                                if(isset($type) && $type){
                                    foreach($type as $type){
                                        $col2 = '';
                                        $col1 = '';
                                        $sign = '';
                                        if($type->income == 0)
                                            $col2 = '#AAAAAA';
                                        if($type->expense == 0)
                                            $col1 = '#AAAAAA';
                                        else 
                                            $sign = ' - ';
                    ?>
                    	<div class="tr-de">                     
                          <span><?php echo $type->mode ?></span>  
						  <?php echo '<a href="javascript:;" style="color:'.$col2.'; font-weight:600;"><small class="badge" style="background-color:#127561;">'.$type->cinc.'</small>'.'Rs.'.$type->income.'</a>';?>
                          <?php echo '<a href="javascript:;" style="color:'.$col1.'; font-weight:600;"><small class="badge" style="background-color:#A40C0C;">'.$type->cexp.'</small>'.$sign.'Rs.'.$type->expense.'</a>';?>
                      </div>
                      <?php 			}
                                }
                                else
                                    echo 'no info';
                    ?>
                  </td>
                  <td><?php 
                        echo '<h1 style="font-size:20px; font-weight:400; color:#127561;"> <i class="fa fa-inr" aria-hidden="true"></i> '.$transactions->income.'</h1>';						
                    ?></td>
                  <td><?php 
                        echo '<h1 style="font-size:20px; font-weight:400; color:#A40C0C;"> <i class="fa fa-inr" aria-hidden="true"></i> '.$transactions->expense.'</h1>';				
                    ?></td>
                </tr>
              <input type="hidden" id="item_no" value="<?php //echo $item_no;?>">
                </input>
              
              <?php  
                }}
              ?>
                </tbody>
              
            </table>
        </div>
    </div>
  </div>
</div>

<!-- END CONTENT --> 