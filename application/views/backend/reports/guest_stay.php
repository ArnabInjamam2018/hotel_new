
    <div class="portlet light borderd">
      <div class="portlet-title">
        <div class="caption"> <i class="fa fa-th-list"></i> <strong> Guest Stay Reprot </strong> </div>
        <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            
            <div class="col-md-8">
              <?php

	                            $form = array(
	                                'class'     => 'form-inline ftop',
	                                'id'        => 'form_date',
	                                'method'    => 'post'
	                            );

	                            echo form_open_multipart('dashboard/get_booking_report_by_date',$form);

	                            ?>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
              </div>
              <div class="form-group">
                <input type="text" autocomplete="off"  value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
              </div>
			  
			  <div class="form-group">
              <button onclick="get_val();" class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search" aria-hidden="true"></i></button>
			  </div>
			 
                  
              <?php form_close(); ?>
            </div>
            <script type="text/javascript">
				function check_sub(){
				   document.getElementById('form_date').submit();
				}
			</script>
            
          </div>
        </div>
		
	<p class="font-green-sharp">Please use the table below to navigate or filter the results. You can download the table as excel and pdf.</p>	
	<ul class="nav nav-tabs">
                                            <li class="active">												
                                                <a href="#Checkouts" data-toggle="tab" aria-expanded="true"> <i class="fa fa-calendar" aria-hidden="true"></i> All Guest Stay </a>
                                            </li>
											
                                            <li class="">												
                                                <a href="#Checkins" data-toggle="tab" aria-expanded="false">  <i class="fa fa-tachometer" aria-hidden="true"></i> Guest in House </a>
                                            </li>
                                            <!--<li class="active">
                                                <a href="#tab_15_3" data-toggle="tab" aria-expanded="true"> Section 3 </a>
                                            </li>-->
    </ul>
                                        <div class="tab-content">
										
      <div class="tab-pane active" id="Checkouts">
        <div id="table1">		
       <table class="table table-striped table-bordered table-hover" id="sample_3">
        <!--<table class="table table-striped">-->
          <thead>
            <tr>
              
			  <!--<th> Select </th>-->
              
              
              <th width="3%"> # </th>
              <th width="5%"> Booking ID </th>
              <th width="8%"> Guest </th>
              <th width="8%"> Contact Details </th>
			  <th width="5%"> Purpose </th>
			  <th width="5%"> Status </th>
              <th width="10%"> Added </th>
			  <th width="5%"> Checkin </th>            
			  <th width="5%"> Checkout </th>  
			  <th width="5%"> Charge </th>            
			  <th width="5%"> ID Proof </th>          
              
             
            </tr>
          </thead>
          <tbody>
            <?php 
			
			if(isset($guest) && $guest){
					//echo "<pre>";
					//print_r($bookingDate);
					//exit;
					$j=0;
					$p=0;
					$p1=0;
					$check = '';
					
					$today = new DateTime('now');
					
			//print_r($bookingSource);
			foreach($guest as $book){ 
									
					$j++;
					$stl = '';
					$stl2 = '';
					
					if(strtotime($book->addOn) == strtotime(date("Y-m-d"))){
						
						$stl = 'color:#04CC58; font-weight:600;';
						$stl2 = 'background-color:#F4FFEC;';
						
					}	
					
					if(isset($book->bar_color_code) && $book->bar_color_code){
						$stCol = $book->bar_color_code; 
						$stbCol = $book->body_color_code;
					}
					else{
						$stCol = '';
						$stbCol = '';
					}
						

					if($book->addEvent == 'checkin'){
						$addEvent = 'During Checkin';
					}
					else if($book->addEvent == 'after checkin'){
						$addEvent = 'After Checkin';
					}
					else if($book->addEvent == 'pending'){
						$addEvent = 'Before Checkin';
					}
					else
						$addEvent = '';
					
					if($book->g_gender == 'male' || $book->g_gender == 'Male'){
						 $sex='<label style="color:#0476F6"><i style"color:#0476F6;" class="fa fa-male" aria-hidden="true"></i></label>';
					}else if($book->g_gender == 'female' || $book->g_gender == 'Female'){
						$sex='<label style="color:#FF2A6A"><i style"color:#FF2A6A;" class="fa fa-female" aria-hidden="true"></i></label>';
					}else{
						$sex='';
					}
					
					if($book->g_gender == 'Male' || $book->g_gender == 'male')
						$salu = 'Mr. ';
					else if($book->g_gender == 'Female' || $book->g_gender == 'male'){
						if($book->g_married == 'married')
							$salu = 'Mrs. ';
						else
							$salu = 'Ms. ';
					}
					else
						$salu = '';
			
			?>
         
									
          <tr style="<?php echo $stl2;?>">
		  
            <td>  
				  <?php 
					echo $j;
				  ?>
			 </td>
			 
			 <td>  
				  <?php 
					echo '<span class="label-flat" style="color:'.$stCol.'; background:'.$stbCol.';">BK0'.$book->hotel_id.'/0'.$book->booking_id.'</span>';				
				  ?>
			 </td>
			
			 <td>  
				  <?php 
					echo $salu.' '.$book->g_name.' '.$sex.' </br>'.$book->g_occupation;					
				  ?>
			 </td>
			 
			 <td>  
				  <?php 
					
					echo '<i style"color:#AAAAAA;" class="fa fa-home" aria-hidden="true"></i>  &nbsp;';
							$fg = 10;
						if(isset($book->g_address) && ($book->g_address != '' && $book->g_address != ' ')){
								echo ($book->g_address);
								$fg = 0	;
								
						}
						if(isset($book->g_city) && ($book->g_city != '' && $book->g_city != ' ')){
								if($fg == 0){
									echo ', ';
									
								}
									
								echo $book->g_city;
								echo '<br>';
								$fg = 1;
						}
						else if($fg == 0)
								$fg = 5;
						if(isset($book->g_state) && ($book->g_state != '' && $book->g_state != ' ')){
								if($fg == 5)
									echo '<br>';
								echo $book->g_state;
								$fg = 2;
								
							}
						if(isset($book->g_pincode) && $book->g_pincode!= NULL){
								if($fg == 2 || $fg == 5){
									echo ', ';
								}									
								echo 'Postal Code - '.$book->g_pincode;
								$fg = 3;
							}
						if(isset($book->g_country) && $book->g_country != '' && $book->g_country != ' '){
								if($fg == 3 || $fg == 2)
									echo ', ';
								echo $book->g_country;
								if($book->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';
								$fg = 4;
							}
						if($fg == 10){
								echo '<span style="color:#AAAAAA;">No info</span>';
							}
							
						echo '</br>'.$book->g_contact_no.' - '.$book->g_email;
					
				  ?>
			</td>	
			 
			<td>
									<?php 
										echo $book->stayPurpose;
									?>
			</td>
			
			<td>
									<?php
										echo $book->status;						
									?>
			</td>
			 
			 
			<td>
									<?php 
							echo $addEvent.'- '.$book->addOn;
									?>
			</td>
			
			<td>
						<?php
							echo $book->check_in_date;
						?>
			</td>
			
			<td>
						<?php
							echo $book->check_out_date;	
						?>
			</td>
			
			<td>
						<?php
							echo $book->chargeAmt;	
						?>
			</td>
			
			<td>
									<?php
										echo $book->g_id_proof;	
									?>	
			</td>
			
          </tr>
		   <input type="hidden" id="item_no" value="<?php //echo $item_no;?>"> </input>
		   
			
          
          <?php  
			}}
		  ?>
         
            </tbody>
          
        </table>
      
    </div>
                                              
	  </div>
	  
	  <div class="tab-pane" id="Checkins">
        <div id="table2">		
       <table class="table table-striped table-bordered table-hover" id="sample_2">
        <!--<table class="table table-striped">-->
          <thead>
            <tr>
              
			  <!--<th> Select </th>-->
              
              
              <th width="3%"> # </th>
              <th width="10%"> Date </th>
              <th width="5%"> Rooms Checked in </th>
              <th width="5%"> Groups Checked in </th>
			  <th width="5%"> Guests Checkedin </th>            
			  <th width="5%"> Expected Checked ins </th>            
			  <th width="5%"> Checkins/ Checkouts </th>            
			  <th width="5%"> Checkin pending </th>            
              
             
            </tr>
          </thead>
          <tbody>
            <?php 
			
			if(isset($checkin) && $checkin){
					//echo "<pre>";
					//print_r($bookingDate);
					//exit;
					$j=0;
					$p=0;
					$p1=0;
					$check = '';
					
					$today = new DateTime('now');
					
			//print_r($bookingSource);
			foreach($checkin as $checkin){ 
									
					$j++;
					$stl = '';
					$stl2 = '';
					
					if(strtotime($checkin->date) == strtotime(date("Y-m-d"))){
						
						$stl = 'color:#04CC58; font-weight:600;';
						$stl2 = 'background-color:#F4FFEC;';
						//echo date("Y-m-d").' - '.$checkin->date;
					}		
			
			?>
         
									
          <tr style="<?php echo $stl2;?>">
		  
            <td>  
				  <?php 
					echo $j;
				  ?>
			 </td>
			
			 <td>  
				  <?php 
					echo '<span style="font-size:14px;'.$stl.'">'.date("l jS F Y",strtotime($checkin->date)).'</span>';				
				  ?>
			 </td>
			 
			 
			 
			<td>
									<?php 
											echo $checkin->roomsCheckin;
											//echo '<br/> '.$checkin->date;
									?>
			</td>
			
			<td>
									<?php 
											echo $checkin->groupsCheckin;
									?>
			</td>
			
			<td>
									<?php
										echo $checkin->guestCheckin;
									?>
			</td>
			
			<td>
						<?php
							$expc = $this->reports_model->getCheckinExp($checkin->date);
							if(isset($expc) && $expc){									
							
								foreach($expc as $expc){ 
										
									if($expc->count != NULL && isset($expc->count))
										echo $expc->count;
									else
										echo 0;
								

						?>
			</td>
			
			<td>
									<?php
										$tot = $expc->countCheckin + $expc->countCheckout;
										
										if($tot != NULL && isset($tot)){
												echo $expc->countCheckin.' + '.$expc->countCheckout;
										}
										else
											echo 0;									
									?>
			</td>
			
			<td>
									<?php
										
										if($expc->countNonCheckin != NULL && isset($expc->countNonCheckin)){
											if($expc->countNonCheckin > 0)
												echo '<span style="color:red;">'.$expc->countNonCheckin.'<span>';
											else
												echo $expc->countNonCheckin;
										}
										else
											echo 0;
									?>
									
			</td>
			
          </tr>
		   <input type="hidden" id="item_no" value="<?php //echo $item_no;?>"> </input>
		   
			
          
          <?php  
			}}}}
		  ?>
         
            </tbody>
          
        </table>
      
    </div>
                                              
	  </div>
												  
	
	  </div>
	</div>
    </div>

<!-- END CONTENT --> 