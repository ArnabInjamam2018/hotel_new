<div class="portlet light borderd">
  <div class="portlet-title">
    <div class="caption"> <i class="glyphicon glyphicon-bed"></i>Invoice Report </div>
    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-8">
          <?php

                        $form = array(
                            'class'       => 'form-inline',
                            'id'        => 'form_date',
                            'method'      => 'post'
                        );

                        echo form_open_multipart('reports/invoice_report',$form);

                        ?>
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" id="t_dt_frm" value="<?php if(isset($start_date)){ echo $start_date;}?>"  name="t_dt_frm" class="form-control date-picker" placeholder="Start Date">
          </div>
          <div class="form-group">
            <input type="text" autocomplete="off" required="required" name="t_dt_to" value="<?php if(isset($end_date)){ echo $end_date;}?>"  class="form-control date-picker" placeholder="End Date">
          </div>
          <button class="btn btn-default" onclick="check_sub()" type="submit">Search</button>
          <?php form_close(); ?>
        </div>
      </div>
    </div>
    <table class="table table-striped table-hover table-bordered" id="sample_3">
      <thead>
        <tr>
   
          <th scope="col">#</th>
		  <th scope="col">Invoice No</th>
          <th scope="col">Booking Id</th>
		  <th scope="col">Guest Name</th>
		  <th scope="col">Checkin Date</th>
		  <th scope="col">CheckOUT Date</th>
		  <th scope="col">Genaration Date</th>
         
        </tr>
      </thead>
      <tbody>
<?php

$x=1;
if(isset($invoice) && $invoice!=''){
	
	/*echo "<pre>";
	print_r($invoice);
	exit;*/
	foreach($invoice as $inv){
		
		$inv_no=$inv->invoice_no."". $inv->id;
		$bk_id="";
		$cust_name="";
		$chkin="";
		$chkout="";
		if(isset($inv->invoice_type) && $inv->invoice_type){
			if($inv->invoice_type == "booking"){
				$bk_id="single_".$inv->related_id;
				$c_name=$this->reports_model->get_guest($inv->cust_name);
				if(isset($c_name) && $c_name){
				foreach($c_name as $c_n){
					$cust_name=$c_n->g_name;
				}
				}
				$s_bk=$this->reports_model->get_bk_date_single($inv->related_id);
				if(isset($s_bk) && $s_bk){
				foreach($s_bk as $s_b){
					$chkin=date('Y-m-d',strtotime($s_b->cust_from_date_actual));
					$chkout=date('Y-m-d',strtotime($s_b->cust_end_date_actual));
				}
				}
			}
			if($inv->invoice_type == "grp_booking"){
				$bk_id="Grp_".$inv->related_id;
				$cust_name=$inv->cust_name;
				
				$g_bk=$this->reports_model->get_bk_date_grp($inv->related_id);
				if(isset($g_bk) && $g_bk){
				foreach($g_bk as $g_b){
					$chkin=date('Y-m-d',strtotime($g_b->start_date));
					$chkout=date('Y-m-d',strtotime($g_b->end_date));
				}	
				}
			}
		
		}
		
		
	echo "<tr>";
	echo "<td>".$x."</td>";
	echo "<td>".$inv_no."</td>";
	echo "<td>".$bk_id."</td>";
	echo "<td>".$cust_name."</td>";
	echo "<td>".$chkin."</td>";
	echo "<td>".$chkout."</td>";
	echo "<td>".date('Y-m-d',strtotime($inv->date_generated))."</td>";
	echo "</tr>";
	$x++;
	
	}
	
	
	
}
//echo "<pre>";
//print_r($occu_rooms);

?>
      </tbody>
      
    </table>
  </div>
</div>
<script>

$(document).ready(function(){
  $('#chkbx_tdy').prop('checked', true);
});

function check_sub(){
  document.getElementById('form_date').submit();
}
</script> 
