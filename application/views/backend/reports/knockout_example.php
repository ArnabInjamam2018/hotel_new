<div class="portlet light bordered">
<div class="portlet-title">
    <div class="caption"> <i class="fa fa-industry"></i> <span class="caption-subject bold uppercase"> Knockout Example</span> </div>
</div>
<div class="portlet-body form">
<h4 style="color:#EC6A08;">Knockout Test - seat reservations</h4>
<h3> Your seat reservations (<span data-bind="text: seats().length"></span> Seats) *Max is 5</h3>
<br></br>
<button class="btn pink" data-bind="click: addSeat, enable: seats().length < 5">Reserve another seat</button>
<br></br>

<div class="portlet-body form">
<div class="form-body form-horizontal form-row-sepe">
<table class="table table-striped table-hover table-bordered">
    <thead>
        <tr>
            <th>Passenger name</th>
            <th>Meal</th>
            <th>Price</th>
            <th width="8%">Action</th>
        </tr>
    </thead>
    
    <tbody data-bind="foreach: seats">
        <td><input class="form-control input-sm" data-bind="value: name" /></td>
        <td><select data-bind="options: $root.availableMeals, value: meal, optionsText: 'mealName'"></select></td>
        <td data-bind="text: formattedPrice"></td>
        <td><a class="btn red" href="#" data-bind="click: $root.removeSeat">Remove</a></td>
    </tbody>
</table>
</div>
</div>
<br>
<h3 data-bind="visible: totalSurcharge() > 0" style="color:#3D2058; font-weight:bold;">
    Total surcharge: INR <span data-bind="text: totalSurcharge().toFixed(2)"></span>
</h3>

</div>
</div>


<script src="<?php echo base_url();?>assets/global/plugins/knockout-min.js" type="text/javascript"></script>

<script>

// Knockout.js Class to represent a row in the table in View
function SeatReservation(name, initialMeal) {
    var self = this;
    self.name = name;
    self.meal = ko.observable(initialMeal);
    self.formattedPrice = ko.computed(function() {
        var price = self.meal().price;
        return price ? "INR " + price.toFixed(2) : "None";        
    });
}

// Overall viewmodel for this screen, along with initial state
function ReservationsViewModel() {
    var self = this;

    // Non-editable catalog data - would come from the server DB
    self.availableMeals = [
        { mealName: "Standard (Maggi)", price: 55 },
        { mealName: "Premium (Pomfret Fish)", price: 34.95 },
        { mealName: "Ultimate (whole Chicken)", price: 290 }
    ];    

    // Editable data
    self.seats = ko.observableArray([
        new SeatReservation("Steve Rogers", self.availableMeals[1]),
        new SeatReservation("Bert Simsons", self.availableMeals[0]),
        new SeatReservation("Jhon Wik", self.availableMeals[2])
    ]);
    
    self.addSeat = function() {
        self.seats.push(new SeatReservation("James Gordon", self.availableMeals[0]));
    }
    
    self.removeSeat = function(seat) { self.seats.remove(seat) }
    
    self.totalSurcharge = ko.computed(function() {
       var total = 0;
       for (var i = 0; i < self.seats().length; i++)
           total += self.seats()[i].meal().price;
       return total;
    });
}

ko.applyBindings(new ReservationsViewModel());

</script>