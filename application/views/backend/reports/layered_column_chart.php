<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
       <script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/amcharts.js"></script>

        <link rel="stylesheet" href="http://192.168.1.3/hotelobjects_dev/assets/amcharts/style.css" type="text/css">

       <script src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/pie.js" type="text/javascript"></script>
	   <script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/amcharts.js"></script>
       <script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/serial.js"></script>
        <link rel="stylesheet" href="http://192.168.1.3/hotelobjects_dev/assets/amcharts/style.css" type="text/css">

<div style="border: 1px solid #607D8B; border-top: 0;" class="portlet box blue">
  <div class="portlet-title" style="background-color:#607D8B;">
    <div class="caption"> <i syle="font-size:18px;" class="fa fa-bar-chart" aria-hidden="true"></i> <span class="caption-subject bold uppercase">Chart Implementation Examples</span> <span class="pull-right" style="font-size:12px;"> &nbsp for reference purpose</span></div>
  </div>
  
  <div class="portlet-body form">
 <?php 
 //echo "<pre>";
 //print_r($chartdata);

$chartDatajson= json_encode($chartdata);
//echo $chartDatajson;
//die;
 ?>
     
		
 <script>
 
		   $( document ).ready(function () {
               // generate some random data first
               generateChartData();
           });
		   
		   function generateChartData() {
               
			   let id = '';
			   
			   $.ajax({
					url: "<?php echo base_url()?>reports/chartdata",
					type:"POST",
					data:{id:id},
					success:function(data)
					{
						console.log(data);
						generateChart(data);
						//pushData(data);
						alert('Ajax Success');
					}
				});
           }
		   
            var legend;

            var cData = [
                {
                    "country": "Czech Republic",
                    "litres": 156.9
                },
                {
                    "country": "Ireland",
                    "litres": 131.1
                },
                {
                    "country": "Germany",
                    "litres": 115.8
                },
                {
                    "country": "Australia",
                    "litres": 109.9
                },
                {
                    "country": "Austria",
                    "litres": 108.3
                },
                {
                    "country": "UK",
                    "litres": 65
                },
                {
                    "country": "Belgium",
                    "litres": 50
                }
            ];
			
			function pushData(data){
		    //function pushData(){
			   
			   var chartData = [];
			   
			   alert('pushData');
			   let cnt = 0;
			   var firstDate = new Date();
               firstDate.setDate(firstDate.getDate() - 50);
			   
			   $.each(data, function(key, value){
						   //console.log(value.income);
						
						   var nature = value.nature_visit;
						   var count = parseInt(value.No_Of_Guest_Sum);
						  
						   console.log('nature: '+nature+' count '+count);

						   chartData.push({
							  
							  // views: views,
							  
							   nature_visits:nature,
							   no_of_adults:count
						   });
						   
						cnt = cnt +1;
					}
			   );
			   
				generateChart(chartData); 
		   } // End Function
		   
		   
			let chart = new AmCharts.AmSerialChart();
			
            function generateChart(chartData) {
                // PIE CHART
                let chart = new AmCharts.AmSerialChart();
				
				//alert(JSON.stringify(chartData));
				
                chart.dataProvider = chartData;
                chart.categoryField = "type";
                chart.startDuration = 1;
				
				// GRAPH
				var graph = new AmCharts.AmGraph();
                graph.valueField = "amt";
                graph.balloonText = "[[category]]: <b>[[value]]</b>";
                graph.type = "column";
                graph.lineAlpha = 0;
                graph.fillAlphas = 0.8;
                chart.addGraph(graph);

                // CURSOR
                var chartCursor = new AmCharts.ChartCursor();
                chartCursor.cursorAlpha = 0;
                chartCursor.zoomable = false;
                chartCursor.categoryBalloonEnabled = false;
                chart.addChartCursor(chartCursor);

                chart.creditsPosition = "top-right";

                // WRITE
                chart.write("chartdiv");
            }
			
            // changes label position (labelRadius)
            function setLabelPosition() {
                if (document.getElementById("rb1").checked) {
                    chart.labelRadius = 30;
                    chart.labelText = "[[title]]: [[value]]";
                } else {
                    chart.labelRadius = -30;
                    chart.labelText = "[[percents]]%";
                }
                chart.validateNow();
            }


            // makes chart 2D/3D
            function set3D() {
                if (document.getElementById("rb3").checked) {
                    chart.depth3D = 10;
                    chart.angle = 10;
                } else {
                    chart.depth3D = 0;
                    chart.angle = 0;
                }
                chart.validateNow();
            }

            // changes switch of the legend (x or v)
            function setSwitch() {
                if (document.getElementById("rb5").checked) {
                    legend.switchType = "x";
                } else {
                    legend.switchType = "v";
                }
                legend.validateNow();
            }
        </script>
        
      
       <h1>Pie Chart With Legend </h1>
      <div id="chartdiv" style="width: 100%; height: 400px;"></div>

  </div>
    
    
    
    
  <!-- END CONTENT --> 
</div>
