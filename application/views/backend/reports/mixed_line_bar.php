<!-- Styles -->
<style>
	body { background-color: #3f3e3b; color: #fff; }
#chartdiv {
	width	: 100%;
	height	: 500px;
}
										
</style>

<!-- Resources -->
<!--<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/amcharts/plugins/export/export.css" type="text/css" media="all" />
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/serial.js" type="text/javascript"></script>
<!-- Chart code -->
<script>
		
		 $( document ).ready(function () {
               // generate some random data first
               generateChartData();
           });

			function generateChartData() {
               
			   let id = '';
			   
			   $.ajax({
					url: "<?php echo base_url()?>reports/bkCntTakenbyDateOnly",
					type:"POST",
					data:{id:id},
					success:function(data)
					{
						console.log(data);
						generateChart(data);
					}
				});
           }

	function generateChart(chartData){
		var chart = AmCharts.makeChart("chartdiv", {
			"type": "serial",
			"marginRight": 40,
			"marginLeft": 40,
			"autoMarginOffset": 20,
			"mouseWheelZoomEnabled":true,
			"dataDateFormat": "YYYY-MM-DD",
			"valueAxes": [{
				"id": "v1",
				"axisAlpha": 0,
				"position": "left",
				"ignoreAxisWidth":true
			}],
			"balloon": {
				"borderThickness": 1,
				"shadowAlpha": 0
			},
			"graphs": [{
				"id": "g1",
				"balloon":{
				  "drop":true,
				  "adjustBorderColor":false,
				  "color":"#ffffff"
				},
				"bullet": "round",
				"bulletBorderAlpha": 1,
				"bulletColor": "#FFFFFF",
				"bulletSize": 5,
				"connect": false,
				"hideBulletsCount": 50,
				"lineThickness": 2,
				"title": "red line",
				"useLineColorForBulletBorder": true,
				"valueField": "value",
				"balloonText": "<span style='font-size:18px;'>[[value]]</span>"
			}],
			"chartScrollbar": {
				"graph": "g1",
				"oppositeAxis":false,
				"offset":30,
				"scrollbarHeight": 80,
				"backgroundAlpha": 0,
				"selectedBackgroundAlpha": 0.1,
				"selectedBackgroundColor": "#888888",
				"graphFillAlpha": 0,
				"graphLineAlpha": 0.5,
				"selectedGraphFillAlpha": 0,
				"selectedGraphLineAlpha": 1,
				"autoGridCount":true,
				"color":"#AAAAAA"
			},
			"chartCursor": {
				"pan": true,
				"valueLineEnabled": true,
				"valueLineBalloonEnabled": true,
				"cursorAlpha":1,
				"cursorColor":"#258cbb",
				"limitToGraph":"g1",
				"valueLineAlpha":0.2,
				"valueZoomable":true
			},
			"valueScrollbar":{
			  "oppositeAxis":false,
			  "offset":50,
			  "scrollbarHeight":10
			},
			"categoryField": "date",
			"categoryAxis": {
				"parseDates": true,
				"dashLength": 1,
				"minorGridEnabled": true
			},
			"export": {
				"enabled": true
			},
			"dataProvider": chartData
			
			});
	
	}

chart.addListener("rendered", zoomChart);

zoomChart();

function zoomChart() {
    chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
}
</script>

<!-- HTML -->
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"> <i class="fa "></i> <span class="caption-subject bold uppercase"> Mixed Line Bar</span> </div>
	</div>
	<div class="portlet-body form">
		<div id="chartdiv"></div>		
	</div>
</div>