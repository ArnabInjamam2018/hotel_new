<div style="border: 1px solid #607D8B; border-top: 0;" class="portlet box blue">
  <div class="portlet-title" style="background-color:#607D8B;">
    <div class="caption"> <i syle="font-size:18px;" class="fa fa-bar-chart" aria-hidden="true"></i> <span class="caption-subject bold uppercase">Chart Implementation Examples</span> <span class="pull-right" style="font-size:12px;"> &nbsp for reference purpose</span></div>
  </div>
  </div>
  <div class="portlet-body">
  <div class="row">
 <?php 
 //echo "<pre>";
//print_r($chart_reports);

//$chartDatajson= json_encode($chart_reports);
//echo $chartDatajson;
//die;
 ?>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/amcharts/plugins/export/export.css" type="text/css" media="all" />
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/serial.js" type="text/javascript"></script>
	   <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
	   <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
	   <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />


<script>



           $( document ).ready(function () {
               // generate some random data first
               generateChartData();
           });
		   

           // generate some random data, quite different range
           function generateChartData() {
               
			   let id = '';
			   
			   $.ajax({
					url: "<?php echo base_url()?>reports/getTransacDate",
					type:"POST",
					data:{id:id},
					success:function(data)
					{
						//console.log(data);
						pushData(data);
						alert('Ajax Success');
					}
				});
           }
		   
		   function pushData(data){
		   //function pushData(){
			   
			    var chart;
				var chartData = [];
			   
			   //alert('pushData');
			   let cnt = 0;
			   let i = 0;
			   var firstDate = new Date();
               firstDate.setDate(firstDate.getDate() - 50);
			   
			   $.each(data, function(key, value){
						   //console.log(value.income);
						
						   //var newDate = new Date(firstDate);
						   //newDate.setDate(newDate.getDate() + i + 5);
						   var newDate = new Date(value.date); // date form model
						   newDate.setDate(newDate.getDate());
						   //var newDate = value.date;
						   chk = checkDate(newDate);
						   
						   if(chk === true){
							   if(value.income < 0 || value.expense < 0 || value.cinc < 0 || value.cexp < 0)
								   cnt = cnt + 1;
							
							   var visits = parseInt(value.income);
							   var hits = parseInt(value.expense);
							   var views = parseInt(value.cinc);
							   var bookings = parseInt(value.cexp);
							   
							   console.log(i+'<-ita '+chk+' Date: '+newDate+' income '+visits+' expense '+hits+' incCnt '+views+' expCnt '+bookings);

							   chartData.push({
								   date: newDate,
								   visits: visits,
								   hits: hits,
								   views: views,
								   bookings: bookings
							   });
						   
						   }
						   else { 
								
								console.log('OMG!!'+cnt); 
						   }
						   
						   i = i + 1;
					}
			   );
			   
			   generateChart(chartData)
			   
		   }
		   
		   let chart = new AmCharts.AmSerialChart();
		   
		   function generateChart(chartData){
			    // SERIAL CHART
               let chart = new AmCharts.AmSerialChart();

               chart.dataProvider = chartData;
               chart.categoryField = "date";

               // listen for "dataUpdated" event (fired when chart is inited) and call zoomChart method when it happens
               chart.addListener("dataUpdated", zoomChart);

               chart.synchronizeGrid = true; // this makes all axes grid to be at the same intervals

               // AXES
               // category
               var categoryAxis = chart.categoryAxis;
               categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
               categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to DD
               categoryAxis.minorGridEnabled = true;
               categoryAxis.axisColor = "#DADADA";
               categoryAxis.twoLineMode = true;
               categoryAxis.dateFormats = [{
                    period: 'fff',
                    format: 'JJ:NN:SS'
                }, {
                    period: 'ss',
                    format: 'JJ:NN:SS'
                }, {
                    period: 'mm',
                    format: 'JJ:NN'
                }, {
                    period: 'hh',
                    format: 'JJ:NN'
                }, {
                    period: 'DD',
                    format: 'DD'
                }, {
                    period: 'WW',
                    format: 'DD'
                }, {
                    period: 'MM',
                    format: 'MMM'
                }, {
                    period: 'YYYY',
                    format: 'YYYY'
                }];

               // first value axis (on the left)
               var valueAxis1 = new AmCharts.ValueAxis();
               valueAxis1.axisColor = "#FF6600";
               valueAxis1.axisThickness = 2;
               chart.addValueAxis(valueAxis1);

               // second value axis (on the right)
               var valueAxis2 = new AmCharts.ValueAxis();
               valueAxis2.position = "right"; // this line makes the axis to appear on the right
               valueAxis2.axisColor = "#FCD202";
               valueAxis2.gridAlpha = 0;
               valueAxis2.axisThickness = 2;
               chart.addValueAxis(valueAxis2);

               // third value axis (on the left, detached)
               var valueAxis3 = new AmCharts.ValueAxis();
               valueAxis3.offset = 50; // this line makes the axis to appear detached from plot area
               valueAxis3.gridAlpha = 0;
               valueAxis3.axisColor = "#B0DE09";
               valueAxis3.axisThickness = 2;
               chart.addValueAxis(valueAxis3);
			   
			   // third value axis (on the left, detached)
               var valueAxis4 = new AmCharts.ValueAxis();
			   valueAxis4.position = "right"; // this line makes the axis to appear on the right
               valueAxis4.offset = 50; // this line makes the axis to appear detached from plot area
               valueAxis4.gridAlpha = 0;
               valueAxis4.axisColor = "#3598DC";
               valueAxis4.axisThickness = 2;
               chart.addValueAxis(valueAxis4);

               // GRAPHS
               // first graph
               var graph1 = new AmCharts.AmGraph();
               graph1.valueAxis = valueAxis1; // we have to indicate which value axis should be used
               graph1.title = "Income";
               graph1.valueField = "visits";
               graph1.bullet = "round";
               graph1.hideBulletsCount = 30;
               graph1.bulletBorderThickness = 1;
               chart.addGraph(graph1);

               // second graph
               var graph2 = new AmCharts.AmGraph();
               graph2.valueAxis = valueAxis2; // we have to indicate which value axis should be used
               graph2.title = "Expense";
               graph2.valueField = "hits";
               graph2.bullet = "square";
               graph2.hideBulletsCount = 30;
               graph2.bulletBorderThickness = 1;
               chart.addGraph(graph2);

               // third graph
               var graph3 = new AmCharts.AmGraph();
               graph3.valueAxis = valueAxis3; // we have to indicate which value axis should be used
               graph3.valueField = "views";
               graph3.title = "Income Count";
               graph3.bullet = "triangleUp";
               graph3.hideBulletsCount = 30;
               graph3.bulletBorderThickness = 1;
               chart.addGraph(graph3);
			   
			   // Fourth graph
               var graph4 = new AmCharts.AmGraph();
               graph4.valueAxis = valueAxis4; // we have to indicate which value axis should be used
               graph4.valueField = "bookings";
               graph4.title = "Expense Count";
               graph4.bullet = "triangleUp";
               graph4.hideBulletsCount = 30;
               graph4.bulletBorderThickness = 1;
               chart.addGraph(graph4);

               // CURSOR
               var chartCursor = new AmCharts.ChartCursor();
               chartCursor.cursorAlpha = 0.1;
               chartCursor.fullWidth = true;
               chartCursor.valueLineBalloonEnabled = true;
               chart.addChartCursor(chartCursor);

               // SCROLLBAR
               var chartScrollbar = new AmCharts.ChartScrollbar();
               chart.addChartScrollbar(chartScrollbar);

               // LEGEND
               var legend = new AmCharts.AmLegend();
               legend.marginLeft = 110;
               legend.useGraphSettings = true;
               chart.addLegend(legend);
			   
			   /*var exports = new AmCharts.export();
			   exports.enabled = true;*/
			
               // WRITE
               chart.write("chartdiv");
		   }
		
		   function checkDate(date){
			   
			   if ( Object.prototype.toString.call(date) === "[object Date]" ) {
				  // it is a date
				  if ( isNaN( date.getTime() ) ) {  // date.valueOf() could also work
					// date is not valid
					return false;
				  }
				  else {
					// date is valid
					return true;
				  }
				}
				else {
				  // not a date
				  return false;
			   }

		   }
  
  
  		   // this method is called when chart is first inited as we listen for "dataUpdated" event
           function zoomChart() {
               // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
               chart.zoomToIndexes(10, 20);
           }
		   
	
        </script>
        
      
       <h1 style="padding-left:22px;">  Multiple Value Axes</h1>
	   <div class="col-md-12">
		<div id="chartdiv" style="width: 100%; height: 500px;"></div>
	   </div>
    
  </div>
    
    
    
    
  <!-- END CONTENT --> 
</div>
