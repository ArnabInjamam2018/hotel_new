<link rel="stylesheet" href="http://192.168.1.3/hotelobjects_dev/assets/amcharts/style.css" type="text/css">
<script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/amcharts.js"></script>
<script src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/pie.js" type="text/javascript"></script>
<script type="text/javascript" src="http://192.168.1.3/hotelobjects_dev/assets/amcharts/serial.js"></script> 

<div style="border: 1px solid #607D8B; border-top: 0;" class="portlet box blue">
	<div class="portlet-title" style="background-color:#607D8B;">
		<div class="caption"> <i syle="font-size:18px;" class="fa fa-bar-chart" aria-hidden="true"></i> <span class="caption-subject bold uppercase">Pie Chart With Legend</span>
		</div>
	</div>

	<div class="portlet-body form">
		<?php 
 //echo "<pre>";
 //print_r($pie_data);

$chartDatajson= json_encode($pie_data);
//echo $chartDatajson;
//die;
 ?>
		<script>
			$( document ).ready( function () {
				// generate some random data first
				generateChartData();
			} );

			function generateChartData() {

				let id = '';

				$.ajax( {
					url: "<?php echo base_url()?>reports/chartList4",
					type: "POST",
					data: {
						id: id
					},
					success: function ( data ) {
						console.log( data );
						generateChart( data );
						//pushData(data);
						alert( 'Ajax Success' );
					}
				} );
			}

			var legend;

			var cData = [ {
				"country": "Czech Republic",
				"litres": 156.9
			}, {
				"country": "Ireland",
				"litres": 131.1
			}, {
				"country": "Germany",
				"litres": 115.8
			}, {
				"country": "Australia",
				"litres": 109.9
			}, {
				"country": "Austria",
				"litres": 108.3
			}, {
				"country": "UK",
				"litres": 65
			}, {
				"country": "Belgium",
				"litres": 50
			} ];

			function pushData( data ) {
				//function pushData(){

				var chartData = [];

				alert( 'pushData' );
				let cnt = 0;
				var firstDate = new Date();
				firstDate.setDate( firstDate.getDate() - 50 );

				$.each( data, function ( key, value ) {
					//console.log(value.income);

					var nature = value.nature_visit;
					var count = parseInt( value.No_Of_Guest_Sum );

					console.log( 'nature: ' + nature + ' count ' + count );

					chartData.push( {

						// views: views,

						nature_visits: nature,
						no_of_adults: count
					} );

					cnt = cnt + 1;
				} );

				generateChart( chartData );
			} // End Function


			let chart = new AmCharts.AmPieChart();

			function generateChart( chartData ) {
				// PIE CHART
				let chart = new AmCharts.AmPieChart();

				//alert(JSON.stringify(chartData));

				chart.dataProvider = chartData;
				chart.titleField = "nature_visit";
				chart.valueField = "No_Of_Guest_Sum";
				chart.gradientRatio = [ 0, 0, 0, -0.2, -0.4 ];
				chart.gradientType = "radial";

				// LEGEND
				legend = new AmCharts.AmLegend();
				legend.align = "center";
				legend.markerType = "circle";
				chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
				chart.addLegend( legend );

				// WRITE
				chart.write( "chartdiv" );
			}

			// changes label position (labelRadius)
			function setLabelPosition() {
				if ( document.getElementById( "rb1" ).checked ) {
					chart.labelRadius = 30;
					chart.labelText = "[[title]]: [[value]]";
				} else {
					chart.labelRadius = -30;
					chart.labelText = "[[percents]]%";
				}
				chart.validateNow();
			}


			// makes chart 2D/3D
			function set3D() {
				if ( document.getElementById( "rb3" ).checked ) {
					chart.depth3D = 10;
					chart.angle = 10;
				} else {
					chart.depth3D = 0;
					chart.angle = 0;
				}
				chart.validateNow();
			}

			// changes switch of the legend (x or v)
			function setSwitch() {
				if ( document.getElementById( "rb5" ).checked ) {
					legend.switchType = "x";
				} else {
					legend.switchType = "v";
				}
				legend.validateNow();
			}
		</script>
		<div id="chartdiv" style="width: 100%; height: 800px; margin: 0;"></div>
		<table align="center" cellspacing="20">
			<tr>
				<td>
					<input type="radio" checked="true" name="group" id="rb1" onclick="setLabelPosition()">labels outside
					<input type="radio" name="group" id="rb2" onclick="setLabelPosition()">labels inside</td>
				<td>
					<input type="radio" name="group2" id="rb3" onclick="set3D()">3D
					<input type="radio" checked="true" name="group2" id="rb4" onclick="set3D()">2D</td>
				<td>Legend switch type:
					<input type="radio" checked="true" name="group3" id="rb5" onclick="setSwitch()">x
					<input type="radio" name="group3" id="rb6" onclick="setSwitch()">v</td>
			</tr>
		</table>
	</div>
</div>