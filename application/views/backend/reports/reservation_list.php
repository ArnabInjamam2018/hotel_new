<div class="portlet light borderd">
	<div class="portlet-title">
		<div class="caption"> <strong> <i class="fa fa-bed" aria-hidden="true"></i> </strong> Reservation List </div>
		
	</div>
	<div class="portlet-body">
		<div class="table-toolbar">
			<div class="row">
				<div class="col-md-10">
					<?php

					$form = array(
						'class' => 'form-inline ftop',
						'id' => 'form_date',
						'method' => 'post'
					);

					echo form_open_multipart( 'dashboard/get_booking_report_by_date', $form );

					?>
					<div class="form-group">
						<input type="text" autocomplete="off" value="<?php if(isset($start_date)){ echo $start_date;}?>" id="t_dt_frm" name="t_dt_frm" class="form-control date-picker" placeholder="Start Date"/>
					</div>
					<div class="form-group">
						<input type="text" autocomplete="off" value="<?php if(isset($end_date)){ echo $end_date;}?>" name="t_dt_to" name="t_dt_to" class="form-control date-picker" placeholder="End Date"/>
					</div>
					<div class="form-group">

						<select id="select2_sample_modal_5" class="form-control js-example-basic-multiple" multiple="multiple" style="min-width:100px; max-width:265px;">
							<?php $admins = $this->dashboard_model->fetch_admin();
							foreach($admins as $adm){?>
								<option value="<?php echo $adm->admin_id; ?>">
									<?php echo $adm->admin_first_name." ".$adm->admin_last_name; ?>
								</option>
							<?php } ?>
						</select>
						<input name="admins" id="admin" type="text" style="display:none;"/>
					</div>
					<div class="form-group">
						<button onclick="get_val();" class="btn btn-default" type="submit" onclick="check_sub()"><i class="fa fa-search" aria-hidden="true"></i></button>
					</div>


					<?php form_close(); ?>
					<div class="form-group">
						<div class="btn-group"> <a class="btn green " href="javascript:;" data-toggle="dropdown"> <i class="fa fa-bed"></i> Filter <i class="fa fa-angle-down"></i> </a>
							<ul class="dropdown-menu pull-right all-books">
								<li> <a href="javascript:;"> <i class="fa fa-pencil"></i> All Bookings </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-pencil"></i> Arrivals/ Checkins </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-trash-o"></i> Departures/ Checkout </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-trash-o"></i> Checkedout Bookings </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-ban"></i> No Show Bookings</a> </li>
								<li class="divider"> </li>
								<li> <a href="javascript:;"> <i class="fa fa-table"></i> Booking Calendar </a> </li>
							</ul>
						</div>
						<div class="btn-group"> <a class="btn green" href="javascript:;" data-toggle="dropdown"> <i class="fa fa-bookmark" aria-hidden="true"></i> Source <i class="fa fa-angle-down"></i> </a>
							<ul class="dropdown-menu pull-right all-source">
								<li> <a href="javascript:;"> <i class="fa fa-pencil"></i> All Sources </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-pencil"></i> Frontdesk </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-trash-o"></i> Hotel Office </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-trash-o"></i> Hotel Website </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-ban"></i> OTA/ GDS/ Channel </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-ban"></i> Agent/ Broker </a> </li>
								<li class="divider"> </li>
								<li> <a href="javascript:;"> Reservation Summary </a> </li>
							</ul>
						</div>
						<div class="btn-group"> <a class="btn green" href="javascript:;" data-toggle="dropdown"> <i class="fa fa-bookmark" aria-hidden="true"></i> Nature <i class="fa fa-angle-down"></i> </a>
							<ul class="dropdown-menu pull-right all-source">
								<li> <a href="javascript:;"> <i class="fa fa-pencil"></i> All Sources </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-pencil"></i> Frontdesk </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-trash-o"></i> Hotel Office </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-trash-o"></i> Hotel Website </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-ban"></i> OTA/ GDS/ Channel </a> </li>
								<li> <a href="javascript:;"> <i class="fa fa-ban"></i> Agent/ Broker </a> </li>
								<li class="divider"> </li>
								<li> <a href="javascript:;"> Reservation Summary </a> </li>
							</ul>
						</div>
					</div>
				</div>
				<script type="text/javascript">
					function check_sub() {
						document.getElementById( 'form_date' ).submit();
					}
				</script>

				<div class="col-md-2 pull-right">
					<div class="form-group form-md-checkboxes">
						<div class="md-checkbox-inline text-right">
							<div class="md-checkbox">
								<input type="checkbox" id="chkbx_tdy" onclick="fetch_data()" class="md-check">
								<label for="chkbx_tdy">
									<span></span>
									<span class="check"></span>
									<span class="box"></span> No Tax </label>
							</div><div class="md-checkbox">
								<input type="checkbox" id="chkbx_tdy" onclick="fetch_data()" class="md-check">
								<label for="chkbx_tdy">
									<span></span>
									<span class="check"></span>
									<span class="box"></span> No Show </label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<p class="font-green-sharp">*This report is Considering the stay dates for generating data. <br> Please use the table below to navigate or filter the results. You can download the table as excel and pdf.</p>
		<div id="table1">
			<table class="table table-striped table-bordered table-hover" id="sample_4">
				<!--<table class="table table-striped">-->
				<thead>
					<tr>

						<!--<th> Select </th>-->
						<th width="3%"> # </th>
						<th width="8%"> Booking ID </th>
						<th> Status </th>
						<th width="8%"> Guest Name </th>
						<th class="none"> Guest Address </th>
						<th class="none"> Guest Contact </th>
						<th align="center"> Room No </th>
						<th> Source </th>
						<th> Nature</th>
						<th align="center"> PAX</th>
						<th width="10%"> Check In </th>
						<th width="30%"> Check Out </th>
						<th class="col"> Food Plan</th>
						<th width="5%" align="center" class="none"> Room Rent </th>
						<th width="5%" align="center" class="none"> RR Tax </th>
						<th width="5%" class="none"> Meal Amt </th>
						<th width="5%" class="none"> Meal Tax </th>
						<th width="5%" align="center" class="none"> POS Amt </th>
						<th width="5%" align="center" class="none"> Ex Charge </th>
						<th width="5%" align="center" class="none"> Ex Serv </th>
						<th width="5%" align="center" class="none"> Laundry </th>
						<th width="5%" align="center" class="none"> Discount </th>
						<th width="5%" align="center" class="none"> Adjustment </th>
						<th class="col" width="5%" align="center" > Total Amt</th>
						<th class="col" width="5%" align="center" > Paid Amt</th>
						<th width="5%" align="center" class="col"> Amt Due </th>
						<th class="none"> Mktng Exe </th>
						<th class="none"> Admin </th>
						
					</tr>
				</thead>
				<tbody>
					<?php if(isset($reservation) && $reservation){
					
						$i=0;
                        $deposit=0;
						$item_no=0;
						$msg="";
						$totalRoomRent = 0;
						$totalRoomRentTax = 0;
						$totalMealPlan = 0;
						$totalMealPlanTax = 0;
						$totalPosAmt = 0;
						$bkidLast = 0;
						$totalExAmt = 0;
						$totalDiscAmt = 0;
						
						
                        foreach($reservation as $booking){
						$dataEr = 0;
						$totalRow = 0;
						$i++;						$paidAmt = 0;
						
						if($booking->bkid != $bkidLast){
						
						if($booking->type == 'group')
						{
							$col = '#FFEA73';
						}
						else
							$col = '#EFF5F9';
		?>

					<tr style="background-color:<?php echo $col;?>;">

						<td>
							<?php

							echo $i;

							?>
						</td>

						<td>
							<?php
							if ( $booking->type == 'single' )
								echo '<span class="label" style="color:' . $booking->bar_color_code . '; background-color:#fff">BK0' . $this->session->userdata( 'user_hotel' ) . '-0' . $booking->bkid . '</span>';
							else
								echo 'GBK0' . $this->session->userdata( 'user_hotel' ) . '-0' . $booking->bkid;
							
							$bkidLast = $booking->bkid;
							?>
						</td>

						<td>
							<?php
								echo $booking->booking_status;
							?>
						</td>

						<td>
							<?php
								if($booking->g_gender == 'male' || $booking->g_gender == 'Male'){
									 $sex='<label style="color:#0476F6"><i style"color:#0476F6;" class="fa fa-male" aria-hidden="true"></i></label>';
								}else if($booking->g_gender == 'female' || $booking->g_gender == 'Female'){
									$sex='<label style="color:#FF2A6A"><i style"color:#FF2A6A;" class="fa fa-female" aria-hidden="true"></i></label>';
								}else{
									$sex='';
								}
							
								if($booking->g_gender == 'Male' || $booking->g_gender == 'male')
									$salu = 'Mr. ';
								else if($booking->g_gender == 'Female' || $booking->g_gender == 'male'){
									if($booking->g_married == 'married')
										$salu = 'Mrs. ';
									else
										$salu = 'Ms. ';
								}
								else
									$salu = '';
								
								echo $salu.$booking->g_name.' '.$sex.' </br>'.$booking->g_occupation;
							?>
						</td>
						
						<td>
							<?php
								if ( $booking->type == 'single' ){
									echo '<i style"color:#AAAAAA;" class="fa fa-home" aria-hidden="true"></i>  &nbsp;';
										$fg = 10;
									if(isset($booking->g_address) && ($booking->g_address != '' && $booking->g_address != ' ')){
											echo ($booking->g_address);
											$fg = 0	;
											
									}
									if(isset($booking->g_city) && ($booking->g_city != '' && $booking->g_city != ' ')){
											if($fg == 0){
												echo ', ';
												
											}
												
											echo $booking->g_city;
											echo '<br>';
											$fg = 1;
									}
									else if($fg == 0)
											$fg = 5;
									if(isset($booking->g_state) && ($booking->g_state != '' && $booking->g_state != ' ')){
											if($fg == 5)
												echo '<br>';
											echo $booking->g_state;
											$fg = 2;
											
										}
									if(isset($booking->g_pincode) && $booking->g_pincode!= NULL){
											if($fg == 2 || $fg == 5){
												echo ', ';
											}									
											echo 'Postal Code - '.$booking->g_pincode;
											$fg = 3;
										}
									if(isset($booking->g_country) && $booking->g_country != '' && $booking->g_country != ' '){
											if($fg == 3 || $fg == 2)
												echo ', ';
											echo $booking->g_country;
											if($booking->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';
											$fg = 4;
									}
									if($fg == 10){
											echo '<span style="color:#AAAAAA;">No info</span>';
									}
								}	
							?>
						</td>
						
						<td>
							<?php
								echo $booking->g_contact_no.'</br>'.$booking->g_email;
							?>
						</td>
						
						<td>
							<?php
								echo $booking->room_no;
								if($booking->room_no != $booking->room_name)
									echo ' </br>'.$booking->room_name;
							?>
						</td>
						
						<td>
							<?php
								echo $booking->source;
							?>
						</td>
						
						<td>
							<?php
								echo $booking->nature;
							?>
						</td>
						
						<td>
							<?php
								echo $booking->pax;
							?>
						</td>
						
						<td>
							<?php
								echo $booking->start;
							?>
						</td>
						
						<td>
							<?php
								echo $booking->end;
							?>
						</td>

						<td></td>
						
						<td>
							
							<?php
								if ( $booking->type == 'single' ){
									if(!is_null($booking->room_rent)){
										echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
										echo $booking->room_rent+$booking->exrr;
										$totalRoomRent = $totalRoomRent + $booking->room_rent + $booking->exrr;
										$totalRow = $totalRow + $booking->room_rent+$booking->exrr;
									}
									else{
										echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
										$dataEr = 1;
									}
								} else {
									if(!is_null($booking->room_rent_g)){
										echo '<span style="font-weight:600"><i class="fa fa-inr" aria-hidden="true"></i> ';
										echo $booking->room_rent_g+$booking->exrr_g.'</span>';
										$totalRow = $totalRow + $booking->room_rent_g+$booking->exrr_g;
									}
									else{
										echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
										$dataEr = 1;
									}
								}
							?>
						</td>
						
						<td>
							<?php
								if ( $booking->type == 'single' ){
									if($dataEr != 1){
										echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
										echo $booking->rr_tot_tax+$booking->exrr_tax;
										$totalRoomRentTax = $totalRoomRentTax + $booking->rr_tot_tax+$booking->exrr_tax;
										$totalRow = $totalRow + $booking->rr_tot_tax+$booking->exrr_tax;
									}
									else {
										echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
										$dataEr = 1;
									}
								} else {
									if($dataEr != 1){
										echo '<span style="font-weight:600"><i class="fa fa-inr" aria-hidden="true"></i> ';
										echo $booking->rr_tot_tax_g+$booking->exrr_tax_g.'</span>';
										$totalRow = $totalRow + $booking->rr_tot_tax_g+$booking->exrr_tax_g;
									}
									else {
										echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
										$dataEr = 1;
									}
								}
							?>
						</td>
						<td>
							<?php
								if ( $booking->type == 'single' ) {
									if($dataEr != 1){
										echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
										echo $booking->mp_tot+$booking->exmp;
										$totalMealPlan = $totalMealPlan + $booking->mp_tot+$booking->exmp;
										$totalRow = $totalRow + $booking->mp_tot+$booking->exmp;
									}
									else {
										echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
										$dataEr = 1;
									}
								} else {
									if($dataEr != 1){
										echo '<span style="font-weight:600"><i class="fa fa-inr" aria-hidden="true"></i> ';
										echo $booking->mp_tot_g+$booking->exmp_g.'</span>';
										$totalRow = $totalRow + $booking->mp_tot_g+$booking->exmp_g;
									}
									else {
										echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
										$dataEr = 1;
									}
								}
							?>
						</td>
						<td>
							<?php
								if ( $booking->type == 'single' ) {
									if($dataEr != 1){
										echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
										echo $booking->mp_tax+$booking->exmp_tax;
										$totalMealPlanTax = $totalMealPlanTax + $booking->mp_tax+$booking->exmp_tax;
										$totalRow = $totalRow + $booking->mp_tax+$booking->exmp_tax;
									}
									else {
										echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
										$dataEr = 1;
									}
								} else {
									if($dataEr != 1){
										echo '<span style="font-weight:600"><i class="fa fa-inr" aria-hidden="true"></i> ';
										echo $booking->mp_tax_g+$booking->exmp_tax_g.'</span>';
										$totalRow = $totalRow + $booking->mp_tax_g+$booking->exmp_tax_g;
									}
									else {
										echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
										$dataEr = 1;
									}
								}
							?>
						</td>
							
						<td>
							<?php
								echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
								
								if ( $booking->type == 'single' ) {
									$pos = $this->reports_model->getPOS_byBk($booking->bkid,'sb');
									if(isset($pos) && $pos){									
										foreach($pos as $pos){ 
											echo number_format($pos->total, 2);
											$totalRow = $totalRow + $pos->total;
										}
									}
									else
										echo 0;
								} else {
									$pos = $this->reports_model->getPOS_byBk($booking->bkid,'gb');
									if(isset($pos) && $pos){									
										foreach($pos as $pos){ 
											echo number_format($pos->total, 2);
											$totalRow = $totalRow + $pos->total;
										}
									}
									else
										echo 0;
								}
							?>
						</td>
						
						<td>
							<?php
								echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
								if ( $booking->type == 'single' ) {
									$exCharge = $this->dashboard_model->getExtraCharge_byBk($booking->bkid,'sb');
									if($dataEr != 1){
										if(isset($exCharge) && $exCharge){
											foreach($exCharge as $exCharge){ 
												echo number_format($exCharge->total,2);
												$totalPosAmt = $totalPosAmt + $exCharge->total;
												$totalRow = $totalRow + $exCharge->total;
											}
										} else
										echo 0;
									}
									else {
										echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
										$dataEr = 1;
									}
								} else {
									$exCharge = $this->dashboard_model->getExtraCharge_byBk($booking->bkid,'gb');
									if($dataEr != 1){
										if(isset($exCharge) && $exCharge){
											foreach($exCharge as $exCharge){
												echo number_format($exCharge->total,2);
												$totalRow = $totalRow + $exCharge->total;
											}
										} else
										echo 0;
									}
									else {
										echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
										$dataEr = 1;
									}
								}
							?>
						</td>
							
						<td>
							<?php 
								if ( $booking->type == 'single' ) {
									if($dataEr != 1){
										echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
										echo 0;
											
									}
									else {
										echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
										$dataEr = 1;
									}
								} else {
									if($dataEr != 1){
										echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
										echo 0;
									}
									else {
										echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
										$dataEr = 1;
									}
								}
							?>
						</td>
						
						<td>
							<?php 
								echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
								if ( $booking->type == 'single' ) {
									$laun = $this->reports_model->getLaundry_byBk($booking->bkid,'sb');
									if(isset($laun) && $laun){									
										foreach($laun as $laun){ 
											echo number_format($laun->total, 2);
											$totalRow = $totalRow + $laun->total;
										}
									}
									else
										echo 0;
								} else {
									$laun = $this->reports_model->getLaundry_byBk($booking->bkid,'gb');
									if(isset($laun) && $laun){									
										foreach($laun as $laun){ 
											echo number_format($laun->total, 2);
											$totalRow = $totalRow + $laun->total;
										}
									}
									else
										echo 0;
								}
							?>
						</td>
						
						<td>
							<?php 
								echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
								if ( $booking->type == 'single' ) {
									
									$disc = $this->reports_model->getDisc_byBk($booking->bkid,'sb');
									if(isset($disc) && $disc){									
										foreach($disc as $disc){ 
											echo number_format($disc->total, 2);
											$totalRow = $totalRow - $disc->total;
										}
									}
									else
										echo 0;
								} else {
									
									$disc = $this->reports_model->getDisc_byBk($booking->bkid,'gb');
									if(isset($disc) && $disc){									
										foreach($disc as $disc){ 
											echo number_format($disc->total, 2);
											$totalRow = $totalRow - $disc->total;
										}
									}
									else
										echo 0;
								}
							?>
						</td>
						
						<td>
							<?php 
								echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
								if ( $booking->type == 'single' ) {
									$disc = $this->reports_model->getAdj_byBk($booking->bkid,'sb');
									if(isset($disc) && $disc){									
										foreach($disc as $disc){ 
											echo number_format($disc->total, 2);
											$totalRow = $totalRow + $disc->total;
										}
									}
									else
										echo 0;
								} else {
									$disc = $this->reports_model->getAdj_byBk($booking->bkid,'gb');
									if(isset($disc) && $disc){									
										foreach($disc as $disc){ 
											echo number_format($disc->total, 2);
											$totalRow = $totalRow + $disc->total;
										}
									}
									else
										echo 0;
								}
							?>
						</td>
						
						<td>
							<?php
								echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
								echo $totalRow;
							?>
						</td>
						
						<td>							<?php 								echo '<i class="fa fa-inr" aria-hidden="true"></i> ';								if ( $booking->type == 'single' ) {									$paid = $this->reports_model->getPaid_byBk($booking->bkid,'sb');									if(isset($paid) && $paid){																			foreach($paid as $paid){ 											echo number_format($paid->total, 2);											$paidAmt = $paid->total;										}									} else										echo 0;								} else {									$paid = $this->reports_model->getPaid_byBk($booking->bkid,'gb');									if(isset($paid) && $paid){																			foreach($paid as $paid){ 											echo number_format($paid->total, 2);											$paidAmt = $paid->total;										}									}									else										echo 0;								}							?>						</td>
						<td>							<?php								echo '<i class="fa fa-inr" aria-hidden="true"></i> ';								echo $totalRow - $paidAmt;							?>						</td>
						<td></td>
						
						<td>
							<?php
								echo 'admin';
							?>
						</td>

					</tr>


					<?php 
				if($booking->type == 'group')
				$i++; 
			?>

					<?php  
			
			
		   if($booking->type == 'group')
		   {
			  $i--;
			   
			  $sbkug = $this->reports_model->singleBk_undGrp($booking->bkid);
			  
			  if(isset($sbkug) && $sbkug){
					
				$k=0;
				
                 foreach($sbkug as $sbkug){
					$k++;
			   
			?>
					<tr style="background-color:#FFFAE3;">
						<td>
							<?php

							echo $i . '.' . $k;

							?>
						</td>

						<td>
							<?php
								echo '<span class="label" style="color:' . $booking->bar_color_code . '; background-color:#fff;">BK0' . $this->session->userdata( 'user_hotel' ) . '-0' . $sbkug->booking_id . '</span>';
							?>
						</td>

						<td>
							<?php
								echo $sbkug->booking_status;
							?>
						</td>

						<td>
							<?php
								if($sbkug->g_gender == 'male' || $sbkug->g_gender == 'Male'){
									 $sex='<label style="color:#0476F6"><i style"color:#0476F6;" class="fa fa-male" aria-hidden="true"></i></label>';
								}else if($sbkug->g_gender == 'female' || $sbkug->g_gender == 'Female'){
									$sex='<label style="color:#FF2A6A"><i style"color:#FF2A6A;" class="fa fa-female" aria-hidden="true"></i></label>';
								}else{
									$sex='';
								}
							
								if($sbkug->g_gender == 'Male' || $sbkug->g_gender == 'male')
									$salu = 'Mr. ';
								else if($sbkug->g_gender == 'Female' || $sbkug->g_gender == 'male'){
									if($sbkug->g_married == 'married')
										$salu = 'Mrs. ';
									else
										$salu = 'Ms. ';
								}
								else
									$salu = ' ';
								
								echo $salu.' '.$sbkug->g_name.' '.$sex.' </br>'.$sbkug->g_occupation;
							?>
						</td>
						
						<td>
							<?php
									echo '<i style"color:#AAAAAA;" class="fa fa-home" aria-hidden="true"></i>  &nbsp;';
										$fg = 10;
									if(isset($sbkug->g_address) && ($sbkug->g_address != '' && $sbkug->g_address != ' ')){
											echo ($sbkug->g_address);
											$fg = 0	;
											
									}
									if(isset($sbkug->g_city) && ($sbkug->g_city != '' && $sbkug->g_city != ' ')){
											if($fg == 0){
												echo ', ';
												
											}
												
											echo $sbkug->g_city;
											echo '<br>';
											$fg = 1;
									}
									else if($fg == 0)
											$fg = 5;
									if(isset($sbkug->g_state) && ($sbkug->g_state != '' && $sbkug->g_state != ' ')){
											if($fg == 5)
												echo '<br>';
											echo $sbkug->g_state;
											$fg = 2;
											
										}
									if(isset($sbkug->g_pincode) && $sbkug->g_pincode!= NULL){
											if($fg == 2 || $fg == 5){
												echo ', ';
											}									
											echo 'Postal Code - '.$sbkug->g_pincode;
											$fg = 3;
										}
									if(isset($sbkug->g_country) && $sbkug->g_country != '' && $sbkug->g_country != ' '){
											if($fg == 3 || $fg == 2)
												echo ', ';
											echo $sbkug->g_country;
											if($sbkug->g_country == 'India') echo ' <i style="color:#128807;" class="fa fa-flag" aria-hidden="true"></i>';
											$fg = 4;
										}
									if($fg == 10){
											echo '<span style="color:#AAAAAA;">No info</span>';
										}
										
									
							?>
						</td>
						
						<td>
							<?php
								echo $sbkug->g_contact_no.'</br>'.$sbkug->g_email;
							?>
						</td>

						<td>
							<?php
								echo $sbkug->room_no;
								if($sbkug->room_no != $sbkug->room_name)
								echo ' </br>'.$sbkug->room_name;
							?>
						</td>
						
						<td>
							<?php
								echo $sbkug->source;
							?>
						</td>
						
						<td>
							<?php
								echo $sbkug->nature;
							?>
						</td>
						
						<td>
							<?php
								echo $sbkug->pax;
							?>
						</td>
						
						<td>
							<?php
								echo $sbkug->start;
							?>
						</td>
						
						<td>
							<?php
								echo $sbkug->end;
							?>
						</td>
						
						<td>
							<?php
								//echo $sbkug->end;
							?>
						</td>
						
						<td>
							<?php
								/*echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
								echo $sbkug->room_rent+$sbkug->exrr;*/
								if(!is_null($sbkug->room_rent)){
									echo '<span style="color:#AAAAAA"><i class="fa fa-inr" aria-hidden="true"></i> ';
									echo $sbkug->room_rent+$sbkug->exrr.'</span>';
									$totalRoomRent = $totalRoomRent + $sbkug->room_rent+$sbkug->exrr;
									$dataEr = 0;
								}
								else{
									echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
									$dataEr = 1;
								}
							?>
						</td>
						
						<td>
							<?php
								
								if($dataEr != 1){
									echo '<span style="color:#AAAAAA"><i class="fa fa-inr" aria-hidden="true"></i> ';
									echo $sbkug->rr_tot_tax+$sbkug->exrr_tax.'</span>';
									$totalRoomRentTax = $totalRoomRentTax + $sbkug->rr_tot_tax+$sbkug->exrr_tax;
								}
								else {
									echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
									$dataEr = 1;
								}
							?>
						</td>
							
						<td>
							<?php
								
								if($dataEr != 1){
									echo '<span style="color:#AAAAAA"><i class="fa fa-inr" aria-hidden="true"></i> ';
									echo $sbkug->mp_tot+$sbkug->exmp.'</span>';
									$totalMealPlan = $totalMealPlan + $sbkug->mp_tot+$sbkug->exmp;
								}
								else {
									echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
									$dataEr = 1;
								}
							?>
						</td>
							
						<td>
							<?php
								
								if($dataEr != 1){
									echo '<span style="color:#AAAAAA"><i class="fa fa-inr" aria-hidden="true"></i> ';
									echo $sbkug->mp_tax+$sbkug->exmp_tax.'</span>';
									$totalMealPlanTax = $totalMealPlanTax + $sbkug->mp_tax+$sbkug->exmp_tax;
								}
								else {
									echo '<i style="color:red;" class="fa fa-exclamation-triangle glow" aria-hidden="true"></i>';
									$dataEr = 1;
								}
							?>
						</td>
						
						<td>
							<?php
								$pos = $this->reports_model->getPOS_byBk($sbkug->booking_id,'sb');
								if(isset($pos) && $pos){									
										foreach($pos as $pos){ 
											echo number_format($pos->total, 2);
										}
								}
								else
									echo 0;
							?>
						</td>
							
						<td>
							<?php
								echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
								$exCharge = $this->dashboard_model->getExtraCharge_byBk($sbkug->booking_id,'sb');
								if(isset($exCharge) && $exCharge){
									foreach($exCharge as $exCharge){ 
										echo number_format($exCharge->total,2);
										$totalPosAmt = $totalPosAmt + $exCharge->total;
									}
								} else 
									echo 0;
								$totalExAmt = $totalExAmt + $sbkug->crg_total;
							?>
						</td>
							
						<td>
							<?php
								echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
								echo 0;
							?>
						</td>
						
						<td>
							<?php
								echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
								$laun = $this->reports_model->getLaundry_byBk($sbkug->booking_id,'sb');
								if(isset($laun) && $laun){									
									foreach($laun as $laun){ 
										echo number_format($laun->total, 2);
									}
							    } else
									echo 0;
							?>
						</td>
						
						<td>
							<?php
								echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
								$disc = $this->reports_model->getDisc_byBk($sbkug->booking_id,'sb');
								if(isset($disc) && $disc){									
									foreach($disc as $disc){ 
										echo number_format($disc->total, 2);
									}
							    } else
									echo 0;
							?>
						</td>
						
						
						<td>
							<?php
								echo '<i class="fa fa-inr" aria-hidden="true"></i> ';
								$disc = $this->reports_model->getAdj_byBk($sbkug->booking_id,'sb');
								if(isset($disc) && $disc){									
									foreach($disc as $disc){ 
										echo number_format($disc->total, 2);
									}
							    } else
									echo 0;
							?>
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						<td></td>
						
					</tr>
					<?php
					}
					}
					}
					} // end if non repeat
					} // End Foreach
					
					?>
					
					<tr style="font-weight:600;">
					
						<td>
							<?php
								echo $i;
							?>
						</td>
						
						<td>
							<?php
								
							?>
						</td>
						
						<td>
							<?php
								
							?>
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							<?php
								echo $totalRoomRent;
							?>
						</td>
						
						<td>
							<?php
								echo $totalRoomRentTax;
							?>
						</td>
						
						<td>
							<?php
								echo $totalMealPlan;
							?>
						</td>
						
						<td>
							<?php
								echo $totalMealPlanTax;
							?>
						</td>
						
						<td>
							<?php
								echo $totalPosAmt;
							?>
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						
						<td>
							
						</td>
						<td></td>
						<td></td>
						
					</tr>
					
					
					<?php
					
					}
					?>

				</tbody>

			</table>
		</div>
	</div>
</div>

<script>
	$( document ).ready( function () {
		$( "#select2_sample_modal_5" ).select2( {


		} );
	} );

	function get_val() {
		$( "#admin" ).val( $( "#select2_sample_modal_5" ).val() );
		//alert($("#admin").val());
	}
</script>
<!-- END CONTENT -->