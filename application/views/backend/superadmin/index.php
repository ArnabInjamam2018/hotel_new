<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js" manifest="manifest.appcache"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js" manifest="manifest.appcache"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Hotel Objects | <?php if(isset($heading) && $heading) echo $heading; else echo "Dashboard";?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>

<link href='https://fonts.googleapis.com/css?family=Exo+2:400,600,700,800,500' rel='stylesheet' type='text/css'>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo base_url();?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url();?>assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="<?php echo base_url();?>assets/favicon.ico" />
</head>
<body class="page-md login">
<div class="logo"> <img src="<?php echo base_url();?>/assets/dashboard/assets/admin/layout/img/hotel-object1.png" style="width:23%;"/> </div>
<div class="content">
<h3 class="form-title uppercase" style="font-size: 17px;">Select hotel you want to enter</h3>
 <?php

                            $form = array(
                                'class' 			=> 'login-form',
                                'id'				=> 'form',
                                'method'			=> 'post'
                            );

                            echo form_open_multipart('superadmin/redirect',$form);

if(isset($hotels)&& $hotels):
?>
	<div class="form-group">
  <select class="form-control" name="user_hotel" onchange="xyz()" >
    <option class="opt" value="">Select Hotel</option>
    <?php foreach($hotels as $hot):
    ?>
    <option  value="<?php echo $hot->hotel_id; ?>"><?php echo $hot->hotel_name; ?></option>
    <?php endforeach; ?>
  </select>
  </div>
  <?php endif; ?>
<?php form_close(); ?>
</div>
<div class="copyright" style="color:#fff;"> 2016 &copy; <a href="http://www.theinfinitytree.com/" title="The Infinity Tree" style="color:#fff;">The Infinity Tree</a> </div>
<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url();?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/login.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS --> 
<script>
jQuery(document).ready(function() { 
       // init background slide images
       $.backstretch([
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/1.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/2i.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/3.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/4.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/5.jpg",
        "<?php echo base_url();?>assets/global/plugins/backstretch/images/6.jpg",		
        ], {
          fade: 1500,
          duration: 5000
    });
});
function xyz(){	 
	 document.getElementById('form').submit(); 
}
</script>  

</body>
</html>

