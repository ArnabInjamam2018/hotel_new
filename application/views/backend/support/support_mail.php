<link href="<?php echo base_url();?>assets/apps/css/inbox.min.css" rel="stylesheet" type="text/css"/>
<link href="http://hayageek.github.io/jQuery-Upload-File/4.0.10/uploadfile.css" rel="stylesheet">
<script src="http://hayageek.github.io/jQuery-Upload-File/4.0.10/jquery.uploadfile.min.js"></script>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-user-plus"></i> <span class="caption-subject bold uppercase"> Outbox</span> </div>
	</div>
	<div class="portlet-body form">
		<div class="inbox">
			<div class="row">
				<div class="col-md-2">
					<a href="javascript:;" data-title="Compose" class="btn red compose-btn btn-block"> <i class="fa fa-edit"></i> Compose </a>
					<ul class="inbox-nav">
						<li class="active"> <a href="javascript:;" data-type="inbox" data-title="Inbox"> Outbox </a> </li>
						<li> <a href="javascript:;" data-type="draft" data-title="Draft"> Draft <span class="badge badge-danger">8</span> </a> </li>
					</ul>
				</div>
				<div class="col-md-10">
					<div class="inbox-content">
						<div id="inbox-list" style="display: none;">
							<table class="table table-striped table-advance table-hover">
								<thead>
									<tr>
										<th colspan="3"> <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-group-checkbox" />
							<span></span> </label>

											<div class="btn-group input-actions"> <a class="btn btn-sm blue btn-outline dropdown-toggle sbold" href="javascript:;" data-toggle="dropdown"> Actions <i class="fa fa-angle-down"></i> </a>
												<ul class="dropdown-menu">
													<li> <a href="javascript:;"> <i class="fa fa-trash-o"></i> Delete </a> </li>
												</ul>
											</div>
										</th>
										<th class="pagination-control" colspan="3"> <span class="pagination-info"> 1-30 of 789 </span> <a class="btn btn-sm blue btn-outline"> <i class="fa fa-angle-left"></i> </a> <a class="btn btn-sm blue btn-outline"> <i class="fa fa-angle-right"></i> </a> </th>
									</tr>
								</thead>
								<tbody>
									<tr class="unread" data-messageid="1">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star"></i>
										</td>
										<td class="view-message hidden-xs"> Petronas IT </td>
										<td class="view-message "> New server for datacenter needed </td>
										<td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i>
										</td>
										<td class="view-message text-right"> 16:30 PM </td>
									</tr>
									<tr class="unread" data-messageid="2">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star"></i>
										</td>
										<td class="view-message hidden-xs"> Daniel Wong </td>
										<td class="view-message"> Please help us on customization of new secure server </td>
										<td class="view-message inbox-small-cells"></td>
										<td class="view-message text-right"> March 15 </td>
									</tr>
									<tr data-messageid="3">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star"></i>
										</td>
										<td class="view-message hidden-xs"> John Doe </td>
										<td class="view-message"> Lorem ipsum dolor sit amet </td>
										<td class="view-message inbox-small-cells"></td>
										<td class="view-message text-right"> March 15 </td>
									</tr>
									<tr data-messageid="4">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star"></i>
										</td>
										<td class="view-message hidden-xs"> Facebook </td>
										<td class="view-message"> Dolor sit amet, consectetuer adipiscing </td>
										<td class="view-message inbox-small-cells"></td>
										<td class="view-message text-right"> March 14 </td>
									</tr>
									<tr data-messageid="5">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i>
										</td>
										<td class="view-message hidden-xs"> John Doe </td>
										<td class="view-message"> Lorem ipsum dolor sit amet </td>
										<td class="view-message inbox-small-cells"></td>
										<td class="view-message text-right"> March 15 </td>
									</tr>
									<tr data-messageid="6">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i>
										</td>
										<td class="view-message hidden-xs"> Facebook </td>
										<td class="view-message"> Dolor sit amet, consectetuer adipiscing </td>
										<td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i>
										</td>
										<td class="view-message text-right"> March 14 </td>
									</tr>
									<tr data-messageid="7">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i>
										</td>
										<td class="view-message hidden-xs"> John Doe </td>
										<td class="view-message"> Lorem ipsum dolor sit amet </td>
										<td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i>
										</td>
										<td class="view-message text-right"> March 15 </td>
									</tr>
									<tr data-messageid="8">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star"></i>
										</td>
										<td class="view-message hidden-xs"> Facebook </td>
										<td class="view-message view-message"> Dolor sit amet, consectetuer adipiscing </td>
										<td class="view-message inbox-small-cells"></td>
										<td class="view-message text-right"> March 14 </td>
									</tr>
									<tr data-messageid="9">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star"></i>
										</td>
										<td class="view-message hidden-xs"> John Doe </td>
										<td class="view-message view-message"> Lorem ipsum dolor sit amet </td>
										<td class="view-message inbox-small-cells"></td>
										<td class="view-message text-right"> March 15 </td>
									</tr>
									<tr data-messageid="10">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star"></i>
										</td>
										<td class="view-message hidden-xs"> Facebook </td>
										<td class="view-message view-message"> Dolor sit amet, consectetuer adipiscing </td>
										<td class="view-message inbox-small-cells"></td>
										<td class="view-message text-right"> March 14 </td>
									</tr>
									<tr data-messageid="11">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i>
										</td>
										<td class="view-message hidden-xs"> John Doe </td>
										<td class="view-message"> Lorem ipsum dolor sit amet </td>
										<td class="view-message inbox-small-cells"></td>
										<td class="view-message text-right"> March 15 </td>
									</tr>
									<tr data-messageid="12">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i>
										</td>
										<td class="hidden-xs"> Facebook </td>
										<td class="view-message"> Dolor sit amet, consectetuer adipiscing </td>
										<td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i>
										</td>
										<td class="view-message text-right"> March 14 </td>
									</tr>
									<tr data-messageid="13">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star"></i>
										</td>
										<td class="view-message hidden-xs"> John Doe </td>
										<td class="view-message"> Lorem ipsum dolor sit amet </td>
										<td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i>
										</td>
										<td class="view-message text-right"> March 15 </td>
									</tr>
									<tr data-messageid="14">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star"></i>
										</td>
										<td class="hidden-xs"> Facebook </td>
										<td class="view-message view-message"> Dolor sit amet, consectetuer adipiscing </td>
										<td class="view-message inbox-small-cells"></td>
										<td class="view-message text-right"> March 14 </td>
									</tr>
									<tr data-messageid="15">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star"></i>
										</td>
										<td class="view-message hidden-xs"> John Doe </td>
										<td class="view-message"> Lorem ipsum dolor sit amet </td>
										<td class="view-message inbox-small-cells"></td>
										<td class="view-message text-right"> March 15 </td>
									</tr>
									<tr data-messageid="16">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star"></i>
										</td>
										<td class="view-message hidden-xs"> Facebook </td>
										<td class="view-message"> Dolor sit amet, consectetuer adipiscing </td>
										<td class="view-message inbox-small-cells"></td>
										<td class="view-message text-right"> March 14 </td>
									</tr>
									<tr data-messageid="17">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i>
										</td>
										<td class="view-message hidden-xs"> John Doe </td>
										<td class="view-message"> Lorem ipsum dolor sit amet </td>
										<td class="view-message inbox-small-cells"></td>
										<td class="view-message text-right"> March 15 </td>
									</tr>
									<tr data-messageid="18">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star"></i>
										</td>
										<td class="view-message hidden-xs"> Facebook </td>
										<td class="view-message view-message"> Dolor sit amet, consectetuer adipiscing </td>
										<td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i>
										</td>
										<td class="view-message text-right"> March 14 </td>
									</tr>
									<tr data-messageid="19">
										<td class="inbox-small-cells"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
							<input type="checkbox" class="mail-checkbox" value="1" />
							<span></span> </label>
										</td>
										<td class="inbox-small-cells"><i class="fa fa-star"></i>
										</td>
										<td class="view-message hidden-xs"> John Doe </td>
										<td class="view-message"> Lorem ipsum dolor sit amet </td>
										<td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i>
										</td>
										<td class="view-message text-right"> March 15 </td>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="compose" class="form">
							<div class="form-body">
							  <div class="row">
								  <div class="col-md-4">
								  <div class="form-group form-md-line-input">
									<input name="fName" type="text" class="form-control" id="" required="required" placeholder="Name of admin *">                
									<label></label><span class="help-block">Name of admin *</span>
								  </div>
								</div>
								<div class="col-md-4">
								  <div class="form-group form-md-line-input">
									<select name="dept_role" class="form-control bs-select">
										<option value="" disabled="disabled" selected="selected">Service For</option>
										<option value="">Payment</option>
										<option value="">Support</option>
									</select>
									<label></label><span class="help-block">Support for *</span>
								  </div>
								</div>       
								<div class="col-md-4">
								  <div class="form-group form-md-line-input">
									<select name="dept_role" class="form-control bs-select">
										<option value="" disabled="disabled" selected="selected">Support for</option>
										<option value="">Booking</option>
										<option value="">Inoice</option>
										<option value="">Think of other things, etc</option>
									</select>
									<label></label><span class="help-block">Support for *</span>
								  </div>
								</div>
								<div class="col-md-12">
									<div class="form-group" style="padding-top: 20px;">
										<textarea class="form-control" style="height: 300px;"></textarea>
									</div>
								</div>
								<div class="col-md-12">
									<div id="fileuploader">Upload</div>
								</div>
							</div>
						</div>
							<!--000000-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	$("#fileuploader").uploadFile({url: "<?php echo base_url(); ?>Support/upload",
		dragDrop: true,
		fileName: "myfile",
		returnType: "json",
		showDelete: true,
		showDownload:true,
		statusBarWidth:600,
		dragdropWidth:600,
		deleteCallback: function (data, pd) {
			
			console.log(data);
			for (var i = 0; i < data.length; i++) {
				$.post("<?php echo base_url(); ?>Support/delete", {op: "delete",name: data[i]},
					function (resp,textStatus, jqXHR) {
						//Show Message	
						alert("File Deleted");
					});
			}
			pd.statusbar.hide(); //You choice.

		},
		downloadCallback:function(filename,pd)
			{
				location.href="<?php echo base_url(); ?>upload/support"+filename;
			}
	}); 
});
</script>