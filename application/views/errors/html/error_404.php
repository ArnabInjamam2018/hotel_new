<!DOCTYPE html>
<?php

$base_url = load_class('Config')->config['base_url'];

?>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php echo $base_url;?>assets/pages/css/404.css" rel="stylesheet" type="text/css" />
  <style>
  	body{
		background: #a6dbd0;
		background: -moz-linear-gradient(45deg, #a6dbd0 0%, #29b8e5 50%, #52c4b7 100%);
		background: -webkit-gradient(left bottom, right top, color-stop(0%, #a6dbd0), color-stop(50%, #29b8e5), color-stop(100%, #52c4b7));
		background: -webkit-linear-gradient(45deg, #a6dbd0 0%, #29b8e5 50%, #52c4b7 100%);
		background: -o-linear-gradient(45deg, #a6dbd0 0%, #29b8e5 50%, #52c4b7 100%);
		background: -ms-linear-gradient(45deg, #a6dbd0 0%, #29b8e5 50%, #52c4b7 100%);
		background: linear-gradient(45deg, #a6dbd0 0%, #29b8e5 50%, #52c4b7 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a6dbd0', endColorstr='#52c4b7', GradientType=1 );
		height:100%;
	}
	html{
		height:100%;
	}
  </style>
</head>

<body>
 <div class="ba" style="background:rgba(255,255,255,0.3); position:absolute; width:100%;height:100%;"></div>
  <header class="page-container page-container-responsive space-top-4" style="margin-top:0;padding-top:20px;">
    <a href="<?php echo $base_url;?>dashboard">
      <img src="<?php echo $base_url;?>assets/pages/img/logo-hotel.png">
    </a>
  </header>

  <div class="page-container page-container-responsive">
    <div class="row space-top-8 space-8 row-table">
        <div class="col-5 col-middle">
          <h1 class="text-jumbo text-ginormous">Oops!</h1>
          <h2>We can't seem to find the page you're looking for.</h2>
          <h6>Error code: 404</h6>
          <ul class="list-unstyled">
            <li>Here are some helpful links instead:</li>
            <li><a href="<?php echo $base_url;?>dashboard">Home</a></li>
            <li><a href="<?php echo $base_url;?>dashboard/all_bookings">All Bookings</a></li>
            <li><a href="<?php echo $base_url;?>dashboard/all_guest">Guests</a></li>
            <li><a href="<?php echo $base_url;?>dashboard/all_events">All Events</a></li>
            <li><a href="<?php echo $base_url;?>dashboard/housekeeping">Housekeeping</a></li>
            <li><a href="<?php echo $base_url;?>dashboard/all_billpayments">Payments</a></li>
            <li><a href="<?php echo $base_url;?>dashboard/monthly_summary_report">Monthly Summary Reports</a></li>
          </ul>
        </div>
        <div class="col-5 col-middle text-center">
          <img src="<?php echo $base_url;?>assets/pages/img/404.gif" width="313" height="428" alt="Girl has dropped her ice cream.">
        </div>
      </div>
    </div>
  </div>
</body>
</html>
