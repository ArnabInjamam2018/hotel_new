<!-- BEGIN PAGE CONTENT-->
<script>

function fetch_all_address()
{
	
	var pin_code = document.getElementById('pincode').value;
    
	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data.country);
			document.getElementById("g_country").focus();
			$('#g_country').val(data.country);
			document.getElementById("g_state").focus();
			$('#g_state').val(data.state);
			document.getElementById("g_city").focus();
			$('#g_city').val(data.city);
		}

	});
}

</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/dashboard/Colorpicker/farbtastic.css">
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/Colorpicker/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dashboard/Colorpicker/farbtastic.js" type="text/javascript"></script>

<div class="row ds">
  <div class="col-md-12">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green"> <i class="icon-pin font-green"></i> <span class="caption-subject bold uppercase"> Add Lost Item</span> </div>
      </div>
      <div class="portlet-body form">
        <?php

                            $form = array(
                                'class' 			=> 'form-body',
                                'id'				=> 'form',
                                'method'			=> 'post',								
                            );
							
							

                            echo form_open_multipart('dashboard/add_lost_item',$form);

                            ?>
        <div class="form-body"> 
          <!-- 17.11.2015-->
          <?php if($this->session->flashdata('err_msg')):?>
          <div class="form-group">
            <div class="col-md-12 control-label">
              <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
            </div>
          </div>
          <?php endif;?>
          <?php if($this->session->flashdata('succ_msg')):?>
          <div class="form-group">
            <div class="col-md-12 control-label">
              <div class="alert alert-success alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
            </div>
          </div>
          <?php endif;?>
          <!-- 17.11.2015-->
          
          <div class="row">
            <div  class="form-group form-md-line-input form-md-floating-label col-md-4">
              <input autocomplete="off" type="text" class="form-control date-picker" id="form_control_11" name="reporting_date" required="required">
              <label>Reporting Date <span class="required">*</span></label>
              <span class="help-block">Select Reporting Date...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-4 ">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="booking_id" >
              <label>Booking Id</label>
              <span class="help-block">Booking Id...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <select class="form-control focus"  name="type" required="required" >
                <option value="">---Select Type---</option>
                <option value="Jewelery">Jewelery</option>
                <option value="Electronics">Electronics</option>
                <option value="Id Card">Id Card</option>
                <option value="Bag">Bag</option>
                <option value="Others">Others</option>
              </select>
              <label>Type <span class="required">*</span></label>
              <span class="help-block">Select Type...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-4 ">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="item_title" onkeypress=" return onlyLtrs(event, this);" required="required" >
              <label>Item Title <span class="required">*</span></label>
              <span class="help-block">Item Title...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <input autocomplete="off" type="text" class="form-control" id="form_control_1" name="item_des">
              <label>Item Description</label>
              <span class="help-block">Enter Item Description...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <select class="form-control focus"  name="condition" required="required" >
                <option value="">---Select Condition---</option>
                <option value="New">New</option>
                <option value="Used">Used</option>
                <option value="Id Card">Damaged</option>
                <option value="Bag">N/A</option>
              </select>
              <label>Condition <span class="required">*</span></label>
              <span class="help-block">Condition...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="lost_in" >
              <label>Lost In<span class="required">*</span></label>
              <span class="help-block">Lost In...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <input  autocomplete="off" type="text" class="form-control date-picker" id="form_control_12" name="lost_date" >
              <label>Lost Date<span class="required">*</span></label>
              <span class="help-block">Enter Lost Date...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <input  autocomplete="off" type="text" class="form-control timepicker timepicker-24 focus" id="form_control_1" name="lost_time" >
              <label>Lost Time<span class="required">*</span></label>
              <span class="help-block">Lost Time...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <input  autocomplete="off" type="text" class="form-control" id="form_control_1" name="lost_by" >
              <label>lost By<span class="required">*</span></label>
              <span class="help-block">Lost By...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <input autocomplete="off" type="text" class="form-control" id="mobile" name="g_contact_no"  required="required" maxlength="10" onkeypress=" return onlyNos(event, this);" >
              <label>Mobile No. <span class="required">*</span></label>
              <span class="help-block">Enter Mobile No...</span> </div>
            <div class="form-group form-md-line-input form-md-floating-label col-md-4">
              <select  class="form-control focus"  name="admin_name" required="required">
                <option value="">---Select Admin---</option>
                <?php if(isset($admin)&& $admin){

											foreach ($admin as $value) {
												# code...
										?>
                <option value="<?php echo $value->admin_first_name." ".$value->admin_middle_name." ".$value->admin_last_name ?>"><?php echo $value->admin_first_name." ".$value->admin_middle_name." ".$value->admin_last_name?></option>
                <?php
										}}
									?>
              </select>
              <label>Admin Name <span class="required">*</span></label>
              <span class="help-block">Select Admin Name...</span> </div>
          </div>
          </div>
          <div class="form-actions right">
            <button type="submit" class="btn blue" >Submit</button>
            <!-- 18.11.2015  -- onclick="return check_mobile();" -->
            <button  type="reset" class="btn default">Reset</button>
          </div>
          <?php form_close(); ?>
          <!-- END CONTENT --> 
        </div>
      </div>
    </div>
  </div>
<script>
   function check_corporate(value){

       if(value =="corporate"){

           document.getElementById("c_name").style.display="block";
           document.getElementById("c_des").style.display="block";
       }else{
           document.getElementById("c_name").style.display="none";
           document.getElementById("c_des").style.display="none";
       }
   }
    function is_married(value){

        if(value =="Yes"){

            document.getElementById("g_anniv").style.display="block";

        }else{
            document.getElementById("g_anniv").style.display="none";

        }
    }
$(document).on('blur', '#form_control_11', function () {
	$('#form_control_11').addClass('focus');
});
$(document).on('blur', '#form_control_12', function () {
	$('#form_control_12').addClass('focus');
});
</script> 
