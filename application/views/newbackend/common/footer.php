<!--[if lt IE 9]>
<script src="<?php echo base_url();?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?php echo base_url();?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/ion.rangeslider/js/ion.rangeSlider.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-knob/js/jquery.knob.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>


<script src="<?php echo base_url();?>assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/star-rating/star-rating.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/msdropdown/jquery.dd.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url();?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-ion-sliders.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-knob-dials.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-color-pickers.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/form-validation-md.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-sweetalert.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/form-wizard.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo base_url();?>assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

<!-- END THEME LAYOUT SCRIPTS -->
<script>
    function price_range(){
        var range=document.getElementById("range_3").value;

        $.post("<?php echo base_url();?>bookings/hotel_backend_onchange_roomprice_tree",
            { range: range },
            function(data) {
                dp.resources = data;
                dp.update();
            });
       // alert(range);

    }
</script>
<script>
jQuery(document).ready(function() {	
	$('.clear-rating').hide();
});

function emailCheck() {//user types username on inputfiled
       var data = new FormData();
       var email = $('#hotel_email').val(); //get the string typed by user
       $.post('<?php echo base_url();?>dashboard/hotel_email_check', {'email':email}, function(data) { //make ajax call
       $("#email").html(data); //dump the data received from PHP page
   });
};

function phoneCheck() {//user types username on inputfiled
       var data = new FormData();
       var phone = $('#hotel_phone').val(); //get the string typed by user
       $.post('<?php echo base_url();?>dashboard/hotel_phone_check', {'phone':phone}, function(data) { //make ajax call
       $("#phone").html(data); //dump the data received from PHP page
   });
};

function usernameCheck() {//user types username on inputfiled
       var data = new FormData();
       var username = $('#username').val(); //get the string typed by user
       $.post('<?php echo base_url();?>dashboard/username_check', {'username':username}, function(data) { //make ajax call
       $("#user").html(data); //dump the data received from PHP page
   });
};

function allEmailCheck() {//user types username on inputfiled
       var data = new FormData();
       var email = $('#email').val(); //get the string typed by user
       $.post('<?php echo base_url();?>dashboard/email_check', {'email':email}, function(data) { //make ajax call
       $("#email_res").html(data); //dump the data received from PHP page
   });
};

function adminEmailCheck() {//user types username on inputfiled
       var data = new FormData();
       var email = $('#email').val(); //get the string typed by user
	   var hiddenemail = $('#email_hidden').val();
	   //alert(hiddenemail);
       $.post('<?php echo base_url();?>dashboard/admin_email_check', {'email':email, 'hemail':hiddenemail}, function(data) { //make ajax call
       $("#email_res").html(data); //dump the data received from PHP page
   });
};

function onlyNos(e, t) {
	try {
		if (window.event) {
			var charCode = window.event.keyCode;
		}
		else if (e) {
			var charCode = e.which;
		}
		else { return true; }
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
	catch (err) {
		alert(err.Description);
	}
}
/* 11.17.2015*/
function onlyLtrs(e, t)
{
		
	try {
		if (window.event) {
			var charCode = window.event.keyCode;
		}
		else if (e) {
			var charCode = e.which;
		}
		else { return true; }
		if ( charCode > 32 && (charCode < 64 && charCode < 90)  ) {
			return false;
		}
		return true;
	}
	catch (err) {
		alert(err.Description);
	}
}

function check_mobile()
{
	 var val = document.getElementById('mobile').value;	
	 if( val.length < 10)
	 {
		 return false;
	 }
	 else
	 {
		 return true;
	 }
	 
}



/*18.11.2015 */


function updateStatus(value,id){
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url();?>dashboard/hotel_update_status",
	   data: "value="+value+"&id="+id,
	   success: function(msg){
		    $("#status_msg").html(msg);
			$("#status_msg").show();
			$("#status_msg").fadeOut(5000);
	   }
	});
}
/*var clock;

$(document).ready(function() {
	clock = $('.clock').FlipClock({
		clockFace: 'TwelveHourClock'
	});
});*/

/*setTimeout(function(){
  window.location.href = '<?php echo base_url();?>dashboard/lock_screen';
},6000)*/
</script>
<!-----added on 4-8-2016-->
<script>

$( document ).ready(function() {
	$(".box .form-group.form-md-line-input input[type='text'] ,  input[type='email'] ,  input[type='number'] ,  input[type='password']").each(function(){
		if($(this).val() == "")
		{
			$( this ).after( "<div class='highlight'></div>" );
		}

	});
	
	$( ".form-group.form-md-line-input" ).addClass('help-show');
	
	
	$(".box .form-group .btn-group").each(function(){
		if( $(".box .form-group .btn-group").is('.bs-placeholder') ){
			//$(".box .form-group.form-md-line-input").removeClass('help-show');
			alert("here");
		} else {
			$(".box .form-group.form-md-line-input").addClass('help-show');
		}

	});
	 
});

</script>
<script>
//var tc;
$(document).ready(function(e) {
	$("#payments").msDropdown({visibleRows:10});
	$("#tech").msDropdown().data("dd");//{animStyle:'none'} /{animStyle:'slideDown'} {animStyle:'show'}		
	//no use
	try {
		var pages = $("#pages").msDropdown({on:{change:function(data, ui) {
												var val = data.value;												
												if(val!="")
													window.location = val;
											}}}).data("dd");
		var pagename = document.location.pathname.toString();
		pagename = pagename.split("/");
		pages.setIndexByValue(pagename[pagename.length-1]);
		$("#ver").html(msBeautify.version.msDropdown);
	} catch(e) {
		//console.log(e);	
	}
	
	$("#ver").html(msBeautify.version.msDropdown);
});

</script>

</body>
<!-- END BODY -->
</html>