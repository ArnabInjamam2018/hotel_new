<?php //$this->output->cache(20);
$array = explode(',',$this->session->userdata('user_permission'));
			 //$found = in_array("1",$array);
		//if($found)
?>

<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu page-header-fixed page-sidebar-menu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <li class="nav-item <?php if(isset($active) && $active=='dashboard') echo 'active';?>">
                <a href="<?php echo base_url();?>dashboard">
                    <i class="fa fa-dashboard"></i> <span class="title">Dashboard </span>
                </a>
            </li>
          	<?php if(1):  ?>
          	<?php if(1):  ?>
          	<li class="nav-item <?php if(isset($heading) && $heading=='Chain Master')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-user-o"></i> 
                    <span class="title">Chain Master</span>                
                    <span class="arrow <?php if(isset($active) && $active=='chain_master') echo 'active open';?>"></span> 
                </a>
                <ul class="sub-menu">
					
                    <li class="nav-item <?php if(isset($active) && $active=='add_admin') echo 'active open';?>"> 
                    	<a href="<?php echo base_url();?>account/view_chain_master" class="nav-link"> 
                        	<i class="fa fa-user-plus"></i> 
                        	<span class="title">All Chain</span>
                        </a> 
                    </li>
					<li class="nav-item <?php if(isset($active) && $active=='add_admin') echo 'active open';?>"> 
                    	<a href="<?php echo base_url();?>account/add_chain_master" class="nav-link"> 
                        	<i class="fa fa-user-plus"></i> 
                        	<span class="title">Add Chain</span>
                        </a> 
                    </li>
                   
                </ul>
          	</li>
          	<?php endif; ?>
          	<?php endif; ?>
			<li class="nav-item <?php if(isset($heading) && $heading=='Child Account')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-wrench" aria-hidden="true"></i> 
                    <span class="title">Child Account</span>
                    <span class="arrow <?php if(isset($active) && ($active=='child')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">            
                   
                    <li class="nav-item <?php if(isset($active) && $active=='misc_settings')echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>account/view_child_account" class="nav-link"> 
                            <i class="fa fa-toggle-on" aria-hidden="true"></i> 
                            <span class="title">All Child</span>
                        </a> 
                    </li>
					<li class="nav-item <?php if(isset($active) && $active=='misc_settings')echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>account/add_hotel_m" class="nav-link"> 
                            <i class="fa fa-toggle-on" aria-hidden="true"></i> 
                            <span class="title">Add Hotel</span>
                        </a> 
                    </li>
				</ul>
			</li>
			<li class="nav-item <?php if(isset($heading) && $heading=='Bill Account')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-wrench" aria-hidden="true"></i> 
                    <span class="title">Bill Account</span>
                    <span class="arrow <?php if(isset($active) && ($active=='bill_account' || $active=='bill_account')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">            
                   
                    <li class="nav-item <?php if(isset($active) && $active=='bill_account')echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>account/view_bill_account" class="nav-link"> 
                            <i class="fa fa-toggle-on" aria-hidden="true"></i> 
                            <span class="title">All Bill Account</span>
                        </a> 
                    </li>
					<li class="nav-item <?php if(isset($active) && $active=='bill_account')echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>account/add_bill_account" class="nav-link"> 
                            <i class="fa fa-toggle-on" aria-hidden="true"></i> 
                            <span class="title">Add Bill Account</span>
                        </a> 
                    </li>
				</ul>
			</li>
			




			<?php if($this->session->userdata('room') =='1'):  ?>
            <li class="nav-item <?php if(isset($heading) && $heading=='Setting')echo 'active open';?>"> 
                <a href="javascript:;" class="nav-link nav-toggle"> 
                    <i class="fa fa-wrench" aria-hidden="true"></i> 
                    <span class="title"> Transaction</span>
                    <span class="arrow <?php if(isset($active) && ($active=='add_hotel_m' || $active=='all_hotel_m' || $active=='rate_plan_settings' || $active=='all_rate_plan' || $active=='rate_plan_setting_view' || $active=='invoice_settings' || $active=='tax_type_settings' || $active=='all_tax_rule' || $active=='all_tax_category' || $active=='booking_nature_visit' || $active=='hotel_booking_source' || $active=='all_profit_center' || $active=='hotel_area_code' || $active=='booking_status_type' || $active=='housekeeping_status' || $active=='marketing_personnel' || $active=='all_charges')) echo 'open';?>"></span> 
                </a>
                <ul class="sub-menu">            
                   
                    <li class="nav-item <?php if(isset($active) && $active=='misc_settings')echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>account/view_transaction_account" class="nav-link"> 
                            <i class="fa fa-toggle-on" aria-hidden="true"></i> 
                            <span class="title">All Transaction Account</span>
                        </a> 
                    </li>
					<li class="nav-item <?php if(isset($active) && $active=='misc_settings')echo 'active open';?>"> 
                        <a href="<?php echo base_url();?>account/add_transaction_account" class="nav-link"> 
                            <i class="fa fa-toggle-on" aria-hidden="true"></i> 
                            <span class="title">Add Transaction Account</span>
                        </a> 
                    </li>
                    
                   
                   
                </ul>            
            </li>
            <?php endif;?>
			<li class="nav-item "> 
                        <a href="http://122.163.127.3/smapos_share/login" class="nav-link"> 
                            <i class="fa fa-toggle-on" aria-hidden="true"></i> 
                            <span class="title">Yumpos</span>
                        </a> 
                    </li>
        </ul>
    	<?php if($page=="backend/dashboard/index"){ ?>
        <div class="panel-group accordion" id="accordion3" style="margin-top:60px; padding:0 10px;">
          <div class="panel panel-default" style="background-color:lightgrey">
            <div class="panel-heading">
              <h4 class="panel-title" style="font-size:14px;"> <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" style="text-decoration:none; font-weight:bold;"> Booking Status</a> </h4>
            </div>
            <div id="collapse_3_1" class="panel-collapse collapse">
              <div class="panel-body">
                <ul class="list-unstyled brdr-list">
        
                <?php
                $color=$this->unit_class_model->booking_status_type();
                if(isset($color) && $color){
                        foreach($color as $col){
                            $name=$col->booking_status;
                            $co=$col->bar_color_code;
                        ?>
                  <li class="nav-item"><span class="brdr" style="background:<?php echo $co;?>;"></span><?php echo $name; ?></li>
        
                <?php }} ?>
                </ul>
              </div>
            </div>
          </div>
          <div class="panel panel-default" style="background-color:lightgrey">
            <div class="panel-heading">
              <h4 class="panel-title" style="font-size:14px;"> <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" style="text-decoration:none; font-weight:bold;"> Housekeeping Status </a> </h4>
            </div>
            <div id="collapse_3_2" class="panel-collapse collapse">
              <div class="panel-body">
                <ul class="list-unstyled brdr-list">
                <?php
                $color=$this->unit_class_model->housekeeping_status();
                if(isset($color) && $color){
                        foreach($color as $col){
                            $name=$col->status_name;
                            $co=$col->color_primary;
                        ?>
                  <li class="nav-item"><span class="brdr" style="background:<?php echo $co;?>;"></span><?php echo $name; ?></li>
                 <?php }} ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
    	<?php } ?>
    </div>
</div>
<?php //$this->cache->clean();?>
<script>
  (function() {

    "use strict";

    var toggles = document.querySelectorAll(".c-hamburger");

    for (var i = toggles.length - 1; i >= 0; i--) {
      var toggle = toggles[i];
      toggleHandler(toggle);
    };

    function toggleHandler(toggle) {
      toggle.addEventListener( "click", function(e) {
        e.preventDefault();
        (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
      });
    }

  })();
  
  
  function openNav() {
		document.getElementById("mySidenav").style.width = "250px";
		document.getElementById("main").style.marginLeft = "250px";
	}


</script>
<!-- END SIDEBAR -->
