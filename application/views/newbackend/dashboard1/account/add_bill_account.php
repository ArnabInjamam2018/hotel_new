<!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>
<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Bill Account</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
		$form = array(
			'class' 			=> '',
			'id'				=> 'form',
			'method'			=> 'post',								
		);
		echo form_open_multipart('account/add_bill_account',$form);
	?>
	
	
    <div class="form-body">
      <div class="row">
        <!--<div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="" placeholder="Bill ID *" name="bill_id" required="required">
            <label></label>
            <span class="help-block">Bill ID *</span> </div>
        </div>-->
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control"   placeholder="Billing Cycle *" name="bill_cycle" required="required">
            <label></label>
            <span class="help-block">Billing Cycle *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="bill_cycle_details" placeholder="Billing Cycle Details" required>
            <label></label>
            <span class="help-block">Billing Cycle Details</span> </div>
        </div>        
		<div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="payable_amount" placeholder="Payable Amount">
            <label></label>
            <span class="help-block">Payable Amount</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="prev_payable_amount" class="form-control" placeholder="Previous Pending Amount">
            <label></label>
            <span class="help-block">Previous Pending Amount</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="service_tax" class="form-control" placeholder="Service Tax Amount">
            <label></label>
            <span class="help-block">Service Tax Amount</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="other_tax_1" class="form-control" placeholder="Other Tax 1 Amount">
            <label></label>
            <span class="help-block">Other Tax 1 Amount</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="other_tax_2" class="form-control" placeholder="Other Tax 2 Amount">
            <label></label>
            <span class="help-block">Other Tax 2 Amount</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="mise_charge" placeholder="Misc Charges">
            <label></label>
            <span class="help-block">Misc Charges</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="time" class="form-control timepicker timepicker-default timepicker-24" placeholder="Time Stamp">
            <label></label>
            <span class="help-block">Time Stamp</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" name="status">
            	<option value="1"  selected="selected" disabled="disabled">Select Status</option>
                <option value="option 1">option 1</option>
                <option value="option 2">option 2</option>
                <option value="option 3">option 3</option>
            </select>
            <label></label>
            <span class="help-block">Select Status</span> 
            </div>            
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="child_acc_id" class="form-control" placeholder="Child Account ID">
            <label></label>
            <span class="help-block">Child Account ID</span> </div>
        </div>
		<div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="bill_generate_date" class="form-control date-picker" placeholder="Bill Generated Date">
            <label></label>
            <span class="help-block">Bill Generated Date</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="amount_due" class="form-control" placeholder="Minimum Amount Due">
            <label></label>
            <span class="help-block">Minimum Amount Due</span> </div>
        </div>
      </div>
    </div>
    <div class="form-actions right">
      <button type="submit" id="btnSubmit" class="btn submit" >Submit</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <button  type="reset" class="btn default">Reset</button>
    </div>
    <?php echo form_close(); ?> 
  </div>
  
  <!-- END CONTENT --> 
</div>
