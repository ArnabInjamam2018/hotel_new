<!-- 17.11.2015-->
<?php if($this->session->flashdata('err_msg')):?>

<div class="alert alert-danger alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
<div class="alert alert-success alert-dismissible text-center" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
  <strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="icon-pin"></i> <span class="caption-subject bold uppercase"> Add Child Account</span> </div>
  </div>
  <div class="portlet-body form">
    <?php
		$form = array(
			'class' 			=> '',
			'id'				=> 'form',
			'method'			=> 'post',								
		);
		echo form_open_multipart('account/edit_child_account',$form);
	?>
	<?php if(isset($child_account) && $child_account){ ;?>
	
    <div class="form-body">
      <div class="row">
        <div class="col-md-12">
			<h3 class="form-heading">Account Details</h3>
		</div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input"><input type="hidden" name="id" value="<?php echo $child_account->id; ?>">
            <input type="text" class="form-control" name="child_id" value="<?php echo $child_account->id; ?>" placeholder="Child ID *" required="required">
            <label></label>
            <span class="help-block">Child ID *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="hotel_name" value="<?php echo $child_account->hotel_name; ?>" placeholder="Hotel Name *" required="required">
            <label></label>
            <span class="help-block">Hotel Name *</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" name="no_room" value="<?php echo $child_account->no_of_room; ?>" placeholder="No of Rooms">
            <label></label>
            <span class="help-block">No of Rooms</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" value="<?php echo $child_account->plan; ?>" name="plan" required>
            	<option value="" disabled="disabled" selected="selected">Select Plan</option>
                <option value="1">option 1</option>
                <option value="2">option 2</option>
                <option value="3">option 3</option>
            </select>
            <label></label>
            <span class="help-block">Select Plan</span> </div>
			
        </div>
		<div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" value="<?php echo $child_account->billing_cycle; ?>" name="bill_cycle" placeholder="Billing Cycle">
            <label></label>
            <span class="help-block">Billing Cycle</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="bill_cycle_details" value="<?php echo $child_account->billing_cycle_details; ?>" class="form-control" placeholder="Billing Cycle Details">
            <label></label>
            <span class="help-block">Billing Cycle Details</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="renewal_change" value="<?php echo $child_account->renewalcharge; ?>" class="form-control" placeholder="Renewal Change">
            <label></label>
            <span class="help-block">Renewal Change</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="total_amount_paid" value="<?php echo $child_account->total_mount_paid; ?>" class="form-control" placeholder="Total Amount Paid">
            <label></label>
            <span class="help-block">Total Amount Paid</span> </div>
        </div>
        <div class="col-md-12">
			<h3 class="form-heading">Contact Details</h3>
		</div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="contact_no" value="<?php echo $child_account->contact_no; ?>" class="form-control" placeholder="Contact No">
            <label></label>
            <span class="help-block">Contact No</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" class="form-control" value="<?php echo $child_account->address; ?>" name="address" placeholder="Address">
            <label></label>
            <span class="help-block">Address</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="pincode" value="<?php echo $child_account->pincode; ?>" class="form-control" placeholder="Pincode">
            <label></label>
            <span class="help-block">Pincode</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="city" value="<?php echo $child_account->city; ?>" class="form-control" placeholder="City">
            <label></label>
            <span class="help-block">City</span> </div>
        </div>
		<div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" name="state" value="<?php echo $child_account->state; ?>" class="form-control" placeholder="State">
            <label></label>
            <span class="help-block">State</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" value="<?php echo $child_account->country; ?>" name="country" class="form-control" placeholder="Country">
            <label></label>
            <span class="help-block">Country</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" value="<?php echo $child_account->contact_person; ?>" class="form-control" name="contact_person" placeholder="Contact Person">
            <label></label>
            <span class="help-block">Contact Person</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" value="<?php echo $child_account->email; ?>" class="form-control" name="email_id" placeholder="Email ID">
            <label></label>
            <span class="help-block">Email ID</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" value="<?php echo $child_account->secondary_email; ?>" name="secondary_email" class="form-control" placeholder="Secondary Email">
            <label></label>
            <span class="help-block">Secondary Email</span> </div>
        </div>
        <div class="col-md-12">
			<h3 class="form-heading">Other Details</h3>
		</div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" value="<?php echo $child_account->password; ?>" name="password" class="form-control" placeholder="Password">
            <label></label>
            <span class="help-block">Password</span> </div>
        </div> 
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" value="<?php echo $child_account->pending_amount; ?>" name="pending_amount" class="form-control" placeholder="Pending Amount">
            <label></label>
            <span class="help-block">Pending Amount</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" value="<?php echo $child_account->date_added; ?>" name="date_added" class="form-control" placeholder="Date Added">
            <label></label>
            <span class="help-block">Date Added</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input type="text" value="<?php echo $child_account->master_account_id; ?>" name="master_account_id" class="form-control" placeholder="Master Account ID">
            <label></label>
            <span class="help-block">Master Account ID</span> </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select class="form-control bs-select" value="<?php echo $child_account->status; ?>" name="status">
            	<option value="" disabled="disabled" selected="selected">Select Status</option>
                <option value="1">option 1</option>
                <option value="2">option 2</option>
                <option value="3">option 3</option>
            </select>
            <label></label>
            <span class="help-block">Select Status</span> </div>
        </div>
        <div class="col-md-12">
			<h3 class="form-heading">Config Details</h3>
		</div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
            <textarea class="form-control" name="pos_config_details" required placeholder="POS Config Details *"><?php echo $child_account->pos_config_details; ?></textarea>
            <label></label>
            <span class="help-block">POS Config Details *</span> </div>
        </div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
            <textarea class="form-control" name="hrm_config_details" placeholder="HRM Config Details *"><?php echo $child_account->HRM_config_details; ?></textarea>
            <label></label>
            <span class="help-block">HRM Config Details *</span> </div>
        </div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
            <textarea class="form-control" name="account_config_details" placeholder="Account Config Details"><?php echo $child_account->account_config_details; ?></textarea>
            <label></label>
            <span class="help-block">Account Config Details</span> </div>
        </div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
            <textarea class="form-control" name="axis_room_config_details" placeholder="Axis Room Config Details"><?php echo $child_account->axis_room_config_details	; ?></textarea>
            <label></label>
            <span class="help-block">Axis Room Config Details</span> </div>
        </div>        
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
            <textarea class="form-control" name="weather_API_Details" placeholder="Weather API Details"><?php echo $child_account->axis_room_config_details; ?></textarea>
            <label></label>
            <span class="help-block">Weather API Details</span> </div>
        </div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
            <textarea class="form-control" name="special_notes" placeholder="Special Notes"><?php echo $child_account->special_notes; ?></textarea>
            <label></label>
            <span class="help-block">Special Notes</span> </div>
        </div>
        <div class="col-md-12">
			<h3 class="form-heading">Upload Section</h3>
		</div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input uploadss">
          <label>Upload Photo</label>
          <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/dashboard/assets/global/img/no-img.png" alt=""/> </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
            <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php echo form_upload('image_photo');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
          </div>
          </div>
        </div>
      </div>
    </div><?php } ?>
    <div class="form-actions right">
      <button type="submit" id="btnSubmit" class="btn blue" >Update</button>
      <!-- 18.11.2015  -- onclick="return check_mobile();" -->
      <!--<button  type="reset" class="btn default">Reset</button>-->
    </div>
  </div>
  <?php echo form_close(); ?> 
  <!-- END CONTENT --> 
</div>
