<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of All Master Account - Be Careful</div>
    <div class="tools"> 
      <a href="javascript:;" class="collapse">
        </a>
        <a href="#portlet-config" data-toggle="modal" class="config">
        </a>
        <a href="javascript:;" class="reload">
        </a>
        <a href="javascript:;" class="remove">
        </a> 
    </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-6">
          <div class="btn-group"> <a href="<?php echo base_url();?>account/add_chain_master">
            <button  class="btn green"> Add New <i class="fa fa-plus"></i> </button>
            </a> </div>
        </div>        
      </div>
    </div>
    <table class="table table-striped table-hover table-bordered table-scrollable" id="sample_1">
      <thead>
        <tr> 
          <!--<th scope="col">
                Select
            </th>-->
          <th scope="col"> Id </th>
          <th scope="col"> Image </th>
          <th class="col"> Hotel Group Name </th>
          <th scope="col"> Address </th>
		  
          <th scope="col"> City</th>
          <th scope="col"> State </th>
          <th scope="col"> Country </th>
          <th scope="col"> Contact No</th>
		  
          <th scope="col"> Email </th>
          <th scope="col"> Password </th>
          <th scope="col"> Contact Person</th>
          <th scope="col"> Account No</th>
		  
          <th scope="col"> Plan Id </th>
          <th scope="col"> Total Amount Paid </th>
          <th class="none"> Amount Due </th>
          <th class="none"> Bill Cycle </th>
		  
          <th class="none"> Bill Cycle Details </th>
          <th class="none"> Comments </th>
          <th class="none"> Email Config Details </th>
          <th class="none"> Sms Config Details </th>
          <th class="none"> Mise API Config Details </th>          
          <th scope="col"> Actions </th>
        </tr>
      </thead>
      <tbody>
        <?php
            $x = 1;
            if(isset($chain_master) && $chain_master){
            foreach($chain_master as $master){
                ?>
        <tr id="row_<?php echo $master->master_id;?>">
          <td><?php echo $master->master_id; ?></td>
          <td><img src="<?php echo base_url();?>upload/account/<?php echo $master->image ?>" alt="" style="width:100%;"/></a></td>
		  
          <td><?php echo $master->hotel_group_name; ?></td>
          <td><?php echo $master->Address; ?></td>
		  
          <td><?php echo $master->city; ?></td>
          <td><?php echo $master->state; ?></td>
          <td><?php echo $master->country; ?></td>
          <!--<td style="text-transform: uppercase; color: #8258FA; text-shadow: -0.3px 0.3px #FF0000;"><?php //echo $service->s_rules; ?></td>-->
         <td><?php echo $master->contact_no; ?></td>
		 
         <td><?php echo $master->email; ?></td>
         <td><?php echo $master->password; ?></td>
         <td><?php echo $master->contact_prerson_name; ?></td>
         <td><?php echo $master->no_of_accounts; ?></td>
		 
         <td><?php echo $master->plan_id; ?></td>
         <td><?php echo $master->toal_amount_paid; ?></td>
         <td><?php echo $master->amount_due; ?></td>
         <td><?php echo $master->bill_cycle; ?></td>
		 
         <td><?php echo $master->bill_cycle_details; ?></td>
         <td><?php echo $master->comments; ?></td>
         <td><?php echo $master->email_config_details; ?></td>
         <td><?php echo $master->sms_config_details; ?></td>
         <td><?php echo $master->Mise_API_config_details; ?></td>
          <td class="ba">
            <div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $master->master_id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
                <li><a href="<?php echo base_url() ?>account/edit_chain_master?s_id=<?php echo $master->master_id;?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i> </a></li>
              </ul>
            </div>
           </td>


        </tr>
        <?php $x++ ; 
            }} ?>
       
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/common/js/table_sort_style.js"></script> 
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          

            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>account/delete_chain_master",
                data:{id:id},
                success:function(data)
                {
                    
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                           $('#row_'+id).remove();
						   

                        });
                }
            });



        });
    }
</script>