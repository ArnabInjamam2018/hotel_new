<?php if($this->session->flashdata('err_msg')):?>
  <div class="alert alert-danger alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('err_msg');?></strong> </div>
<?php endif;?>
<?php if($this->session->flashdata('succ_msg')):?>
  <div class="alert alert-success alert-dismissible text-center" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
	<strong><?php echo $this->session->flashdata('succ_msg');?></strong> </div>
<?php endif;?>
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption"> <i class="fa fa-edit"></i>List of Hotels </div>
    <div class="tools"> 
      <a href="javascript:;" class="collapse">
        </a>
        <a href="#portlet-config" data-toggle="modal" class="config">
        </a>
        <a href="javascript:;" class="reload">
        </a>
        <a href="javascript:;" class="remove">
        </a> 
    </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div class="row">
        <div class="col-md-6">
          <div class="btn-group"> <a href="<?php echo base_url();?>account/add_child_account">
            <button  class="btn green"> Add New <i class="fa fa-plus"></i> </button>
            </a> </div>
        </div>
        
      </div>
    </div>
    <table class="table table-striped table-hover table-bordered table-scrollable" id="sample_1">
      <thead>
        <tr> 
          <!--<th scope="col">
                Select
            </th>-->
          <th scope="col"> Id </th>
          <th scope="col"> Image </th>
          <th scope="col"> Hotel Group Name </th>
          <th scope="col"> No of Room </th>
          <th scope="col"> Plan </th>
		  <th scope="col"> Bill Cycle </th>
		  <th scope="col"> Bill Cycle Details </th>
		  <th scope="col"> Renewal Charge </th>
		  <th scope="col"> Total Amount Paid </th>
		    <th scope="col"> Contact No</th>
		    <th scope="col"> Address</th>
          <th scope="col"> City</th>
          <th scope="col"> Pincode</th>
          <th scope="col"> State </th>
          <th scope="col"> axis room config details </th>
          <th scope="col"> Special Notes </th>
          <th scope="col"> API Details </th>		  
          <th scope="col"> Country </th>        
		  <th scope="col"> Contact Person</th>
          <th scope="col"> Email </th>
          <th scope="col"> Password </th>
          <th scope="col"> Secondary Email </th>
          <th scope="col"> DB Config Details </th>		  
          <th scope="col"> Date Added</th>
          <th scope="col"> Master Account Id</th>		  
          <th scope="col"> Status </th>
          <th scope="col"> Pos Config Details</th>
          <th scope="col">HRM Config Details </th>
          <th scope="col">Account Config Details </th>                
          <th> Actions </th>
        </tr>
      </thead>
      <tbody>
        <?php
            $x = 1;
            if(isset($child_account) && $child_account){
            foreach($child_account as $child){
                ?>
        <tr id="row_<?php echo $child->id;?>">
          <td><?php echo $child->id; ?></td>
          <td><a class="single_2" href="<?php echo base_url();?>upload/account/<?php if( $child->image== '') { echo "no_images.png"; } else { echo $child->image; }?>"><img src="<?php echo base_url();?>upload/account/<?php if( $child->image== '') { echo "no_images.png"; } else { echo $child->image; }?>" alt="" style="width:100%;"/></a></td>
          <td><?php echo $child->hotel_name; ?></td>
          <td><?php echo $child->no_of_room; ?></td>
          <td><?php echo $child->plan; ?></td>
		  
          <td><?php echo $child->billing_cycle; ?></td>
          <td><?php echo $child->billing_cycle_details; ?></td>
          <td><?php echo $child->renewalcharge; ?></td>
          <!--<td style="text-transform: uppercase; color: #8258FA; text-shadow: -0.3px 0.3px #FF0000;"><?php //echo $service->s_rules; ?></td>-->
         <td><?php echo $child->total_mount_paid; ?></td>
		 
         <td><?php echo $child->contact_no; ?></td>
         <td><?php echo $child->address; ?></td>
         <td><?php echo $child->city; ?></td>
         <td><?php echo $child->pincode; ?></td>		 
         <td><?php echo $child->state; ?></td>
         <td><?php echo $child->axis_room_config_details; ?></td>
         <td><?php echo $child->special_notes; ?></td>
         <td><?php echo $child->wheather_API_details; ?></td>		 
         <td><?php echo $child->country; ?></td>
         <td><?php echo $child->contact_person; ?></td>
         <td><?php echo $child->email; ?></td>
         <td><?php echo $child->password; ?></td>
         <td><?php echo $child->secondary_email; ?></td>
         
         <td><?php echo $child->db_config_details; ?></td>
         <td><?php echo $child->date_added; ?></td>
         <td><?php echo $child->master_account_id; ?></td>
         <td><?php echo $child->status; ?></td>
         <td><?php echo $child->pos_config_details; ?></td>
         <td><?php echo $child->HRM_config_details; ?></td>
         <td><?php echo $child->account_config_details; ?></td>
          <td class="ba">
            <div class="btn-group">
              <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a onclick="soft_delete('<?php echo $child->id;?>')" data-toggle="modal" class="btn red btn-xs"><i class="fa fa-trash"></i></a></li>
                <li><a href="<?php echo base_url() ?>account/edit_child_account?s_id=<?php echo $child->id;?>" data-toggle="modal" class="btn green btn-xs"><i class="fa fa-edit"></i> </a></li>
              </ul>
            </div>
           </td>
        </tr>
        <?php $x++ ; 
            }} ?>
       
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/common/js/table_sort_style.js"></script> 
<script>
    function soft_delete(id){
        swal({   title: "Are you sure?",   text: "All the releted transactions and data will be deleted",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){

          

            $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>account/delete_child_account",
                data:{id:id},
                success:function(data)
                {
                    
                    swal({
                            title: data.data,
                            text: "",
                            type: "success"
                        },
                        function(){

                           $('#row_'+id).remove();
						   

                        });
                }
            });



        });
    }
</script>