<!-- BEGIN PAGE CONTENT-->
<script src="<?php echo base_url();?>assets/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<div class="portlet box blue">
<div class="portlet-title">
  <div class="caption"> <i class="fa fa-user-plus"></i> <span class="caption-subject bold uppercase"> Create Admin User</span> </div>
</div>
<div class="portlet-body form">
    <?php
		$form = array(
			'id'				=> 'form',
			'method'			=> 'post'
		);
		echo form_open_multipart('setting/add_admin_user',$form);
		//print_r($_SESSION);
		//exit;
    ?>
    <div class="form-body">
      <div class="row">
    	  <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="fName" type="text" class="form-control" id="" required="required" placeholder="First Name *">                
            <label></label><span class="help-block">First Name *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="lName" type="text" class="form-control" id="" required="required" placeholder="Last Name *">                
            <label></label><span class="help-block">Last Name *</span>
          </div>
        </div>       
        <div class="col-md-4">
          <div class="form-group form-md-line-input" id="ckp">
            <input name="uName" type="text" class="form-control" id="uName" required="required" placeholder="Usename *" onblur="checkUser(this.value)">                
            <label></label><span class="help-block">Usename *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="contact" type="number" class="form-control" id="" required="required" placeholder="Contact No *">                
            <label></label><span class="help-block">Contact No *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="alt_contact" type="number" class="form-control" id="" placeholder="Alt Contact No">                
            <label></label><span class="help-block">Alt Contact No</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="email" type="email" class="form-control" id="" required="required" placeholder="Email *">                
            <label></label><span class="help-block">Email *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="dept" type="text" class="form-control" id="" placeholder="Department">                
            <label></label><span class="help-block">Department</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="dept_role" class="form-control bs-select">
            	<option value="" disabled="disabled" selected="selected">Role</option>
				<option value="Cashier">Cashier</option>
                <option value="Accountant">Accountant</option>
                <option value="Store Manager">Store Manager</option>
            </select>
            <label></label><span class="help-block">Role *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="hotel_id[]" class="form-control bs-select" required="required" multiple>
              <?php 
              $ho=$this->dashboard_model->total_hotels();
              foreach($ho as $ho){
              ?>          
              <option value="<?php echo $ho->hotel_id;?>"><?php echo  $ho->hotel_name;?></option>
              <?php } ?>				
            </select>
            <label></label><span class="help-block">Select Hotels *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="admin_type" class="form-control bs-select" required="required">
            	<option value="" disabled="disabled" selected="selected">Admin level</option>
				<option value="1">Super Admin</option>
                <option value="2">Admin</option>
                <option value="3">Sub Admin</option>
            </select>
            <label></label><span class="help-block">Admin level *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="admin_role_id" class="form-control bs-select" required="required">
            	<option value="" disabled="disabled" selected="selected">Admin Role</option>
              <?php 
              $ar=$this->setting_model->allAdminRole();
              foreach($ar as $admin_role){
              ?>          
              <option value="<?php echo $admin_role->user_permission_id;?>"><?php echo  $admin_role->admin_roll_name;?></option>
              <?php } ?>
            </select>
            <label></label><span class="help-block">Admin Role *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="profit_center[]" class="form-control bs-select" required="required" multiple>
            	<!--<option value="" disabled="disabled" selected="selected">Profit Centre</option>-->
              <?php 
              $pc=$this->setting_model->allProfitCenter();
              foreach($pc as $prfit_center){
              ?>          
              <option value="<?php echo $prfit_center->profit_center_location;?>"><?php echo  $prfit_center->profit_center_location;?></option>
              <?php } ?>
            </select>
            <label></label><span class="help-block">Profit Centre *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <input name="employee_id" type="text" class="form-control" id="" placeholder="Employee id">                
            <label></label><span class="help-block">Employee id</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <select name="class" class="form-control bs-select" required="required">
            	<option value="" disabled="disabled" selected="selected">Class</option>
				        <option value="Frontdesk">Frontdesk</option>
                <option value="Cashier">Cashier</option>
                <option value="Supervisor">Supervisor</option>
                <option value="Executive">Executive</option>
                <option value="Accountant">Accountant</option>
                <option value="Facility Management">Facility Management</option>
                <option value="Stakeholder">Stakeholder</option>
            </select>
            <label></label><span class="help-block">Class *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input" id="pd">
            <input name="password" type="Password" class="form-control" id="pwd" required="required" placeholder="Password *"onblur="checkPassword(this.value)">                
            <label></label><span class="help-block">Password *</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group form-md-line-input" id="pds">
            <input name="cpassword" type="Password" class="form-control" id="cpwd" required="required" placeholder="Confirm Password *" onblur="confirmPassword(this.value)">                
            <label></label><span class="help-block">Confirm Password *</span>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group form-md-line-input">
          	<textarea name="about_me" class="form-control" cols="" rows="" placeholder="About Me"></textarea>
            <label></label><span class="help-block">About Me</span>
          </div>
        </div>
        <!--<div class="col-md-4">
              <div class="form-group form-md-line-input uploadss">
              <label>Profile Image</label>
              <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo base_url();?>assets/dashboard/assets/global/img/no-img.png" alt=""/> </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span> <?php //echo form_upload('image_photo');?> </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
              </div>
              </div>
         </div>-->
    </div>
  </div>
    <div class="form-actions right">
        <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn blue">Create</button>
              <a href="<?php echo base_url();?>setting/all_admin_user" class="btn red">Cancel</a>
            </div>
        </div>
    </div>
  
  <?php echo form_close(); ?> 
</div>
</div>
<script>
function checkUser(val) {
  if (val != ""){
     $.ajax({
      url: "<?php echo base_url()?>setting/checkUser",
      type: "POST",
      data:'val='+val,
      success: function(data){
        //console.log(data);
        if(data == 1){
              $("#uName").val("");
			  $("#ckp span").removeClass ( 'help-block' );
			  $("#ckp span").text("User name already exist please try with another one.");
			} else {
			  $("#ckp span").addClass ( 'help-block' );	
			}
      }        
      });
  }
}
//this function is not working properly
function checkPassword(val) {
  if(val.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/)){
	  //alert("TRUE");
	  $("#pd span").addClass ( 'help-block' );
  }	else {
	  $("#pwd").val("");
	  $("#pd span").removeClass ( 'help-block' );
	  $("#pd span").text("Password must be alphanumeric with atleast 1 number & 1 uppercase character");
  }
}
function confirmPassword(val) {
   	var pwd = $("#pwd").val();
	//alert(pwd);
    if(val != pwd) {
	  $("#cpwd").val("");
	  $("#pds span").removeClass ( 'help-block' );
	  $("#pds span").text("Password & confirm password must be same.");
    } else {
	  $("#pds span").addClass ( 'help-block' );	
	}
}	
</script>
