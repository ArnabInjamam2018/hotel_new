<!-- BEGIN PAGE CONTENT-->
<!--<style>
    .ds .required {
        color: #e02222;
        font-size: 12px;
        padding-left: 2px;
    }
    .ds .form-group.form-md-line-input .form-control ~ label{width: 94%;left: 19px; top:14px;}
    .ds .form-group.form-md-line-input{ margin-bottom:25px; margin-left:0px; margin-right:0px;}
    .ds .lt{    color: #999;font-size: 16px;}
    .ds .tld{  margin-bottom: 10px !important;  padding-top: 10px;}
    .tld_in{   width: 100%;float: left;padding-top: 7px;}
    .ds .form-group.form-md-line-input.form-md-floating-label .form-control.edited ~ label{top: -10px;}
	
	.nav>li>a {
    position: relative;
    display: block;
    padding: 10px 11px;
}
.col-md-10 {
    width: 83.33333333%;
    margin-top: 8px;
}
::-webkit-input-placeholder {
    font-size: 11px;
    padding-top: 2px;
}
::-moz-placeholder{
	 font-size: 11px;
    padding-top: 2px;
}
</style>-->

<script type="text/javascript">

	$(document).ready(function(){
    	 $("#service_tax").hide();
		 $("#service_tax1").hide();
		 $("#service_tax2").hide();
		 $("#service_tax3").hide();
		 $("#upload_logo").hide();	
    	 $("#upload_text").hide();
    });

	function hideShowLogo()
	{
		//alert(document.getElementById('st_logo'));
		if($('#st_logo').val() == 'Yes')
		{
			$('#upload_logo').show();	
    	 	$('#upload_text').hide();
			$("#text").removeAttr("required");
			$("#logo").attr("required","true");
		}
		else if ($('#st_logo').val() == 'No')
		{
			$('#upload_logo').hide();	
    	 	$('#upload_text').show();
			$("#logo").removeAttr("required");
			$("#text").attr("required","true");
		}
		else
		{
			$('#upload_logo').hide();	
    	 	$('#upload_text').hide();
			$("#logo").removeAttr("required");
			$("#text").removeAttr("required");
		}
	}
	function hideShow()
	{
		if($('#tax').val() == 'Yes')
		{
			$("#service_tax").show();
			$("#service_tax1").show();
			$("#service_tax2").show();
			$("#service_tax3").show();
		}
		else
		{
			$("#service_tax").hide();
			$("#service_tax1").hide();
			$("#service_tax2").hide();
			$("#service_tax3").hide();
		}
	}
	
</script>
<script>

function fetch_all_address_hotel(pincode,g_country,g_state,g_city)
{
	var pin_code = document.getElementById(pincode).value;
	
	

	jQuery.ajax(
	{
		type: "POST",
		url: "<?php echo base_url(); ?>bookings/fetch_address",
		dataType: 'json',
		data: {pincode: pin_code},
		success: function(data){
			//alert(data);
			$(g_country).val(data.country);
			$(g_state).val(data.state);
			$(g_city).val(data.city);
		}

	});
}




</script>
<script type="text/javascript">

/*$("#submit_form").validate({
  submitHandler: function() {
   $.post("<?php echo base_url();?>dashboard/add_hotel_m", 
    $("#submit_form").serialize(), 
    function(data){
     //$('#booking_3rd').val(data.bookings_id);
     //$("#submit4").prop("disabled", true);
     //$('#print_tab').show();
     
   });
   return false; //don't let the page refresh on submit.
   
  }
 });*/
function submit_form()
{
	document.getElementById('submit_form').submit();
}
</script>
<!-- BEGIN PAGE CONTENT-->

    <div class="portlet box blue" id="form_wizard_1">
      <div class="portlet-title">
        <div class="caption"> <i class="icon-pin font-green"></i> ADD HOTELS - <span class="step-title"> Step 1 of 4 </span> </div>
        <div class="tools hidden-xs"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
      </div>
      <div class="portlet-body form">
        <form action="<?php echo base_url();?>account/add_hotel_m" class=""  enctype="multipart/form-data" id="submit_form" method="POST">
          <div class="form-wizard"> 
          	<div class="form-body">         
              <ul class="nav nav-pills nav-justified steps">
                <li> <a href="#tab1" data-toggle="tab" class="step"> <span class="number"> 1 </span> <span class="desc"> <i class="fa fa-check"></i> Account</span> </a> </li>
                <li> <a href="#tab2" data-toggle="tab" class="step"> <span class="number"> 2 </span> <span class="desc"> <i class="fa fa-check"></i> Profile</span> </a> </li>
                <li> <a href="#tab3" data-toggle="tab" class="step"> <span class="number"> 3 </span> <span class="desc"> <i class="fa fa-check"></i> User </span> </a> </li>
                
                <li> <a href="#tab4" data-toggle="tab" class="step"> <span class="number"> 4 </span> <span class="desc"> <i class="fa fa-check"></i> Preferences </span> </a> </li>
                <li> <a href="#tab5" data-toggle="tab" class="step"> <span class="number"> 5 </span> <span class="desc"> <i class="fa fa-check"></i> Confirm </span> </a> </li>
              </ul>
              <div id="bar" class="progress progress-striped" role="progressbar">
                <div class="progress-bar progress-bar-success"> </div>
              </div>
              <div class="tab-content">
                <div class="alert alert-danger display-none">
                  <button class="close" data-dismiss="alert"></button>
                  You have some form errors. Please check below. </div>
                <div class="alert alert-success display-none">
                  <button class="close" data-dismiss="alert"></button>
                  Your form validation is successful! </div>
                <div class="tab-pane" id="tab1">
                  <h3 class="block">General Details</h3>
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group form-md-line-input">
                        <select class="form-control bs-select"  name="hotel_chain_id" required>
                          <option value="" disabled="disabled" selected="selected">Select Hotel Chain</option>
						<?php
			$chain=$this->account_model->all_chain_master();
			foreach($chain as $chain){
			?>
            	
                <option value="<?php echo $chain->chain_id; ?>">  <?php echo  $chain->hotel_group_name; ?></option>
			<?php }?>
            </select>
                        <label></label>
                        <span class="help-block">Hotel Chain *</span> </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group form-md-line-input">
                        <input type="text" class="form-control" name="hotel_name" required placeholder="Hotel Name *"/>
                        <label></label>
                        <span class="help-block">Hotel Name *</span> </div>
                    </div>
					<div class="col-md-3">
                      <div class="form-group form-md-line-input">
                        <input type="text" class="form-control" name="hotel_gpname"  placeholder="Hotel Group Name "/>
                        <label></label>
                        <span class="help-block">Hotel Group Name </span> </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group form-md-line-input">
                        <input type="text" class="form-control" name="hotel_year_established" required maxlength="4" onkeypress="return onlyNos(event,this)" placeholder="Year Established *">
                        <label></label>
                        <span class="help-block">Year Established *</span> </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group form-md-line-input">
                        <input type="text" class="form-control" name="hotel_floor" required maxlength="4" onkeypress="return onlyNos(event,this)" placeholder="No of Floor *">
                        <span class="help-block">No of Floor *</span> </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group form-md-line-input">
                        <select class="form-control bs-select"  name="hotel_ownership_type" required>
                          <option value="" disabled="disabled" selected="selected">Select Hotel Ownership Type</option>
                          <option value="Sole Proprietorship">Sole Proprietorship</option>
                          <option value="Partnership">Partnership</option>
                          <option value="Cooperative Business">Cooperative Business</option>
                        </select>
                        <label></label>
                        <span class="help-block">Hotel Ownership Type</span> </div>
                    </div>
                    <div class="col-md-9">
                      <div class="form-group form-md-line-input">
                          <label>Hotel Type<span class="required">* </span></label>
                          <div class="md-checkbox-inline">
                            <div class="md-checkbox">
                              <input type="checkbox"  value="Business Hotel" id="checkbox1" class="md-check" name="hotel_type[]">
                              <label for="checkbox1"> <span></span> <span class="check"></span> <span class="box"></span> Business Hotel</label>
                            </div>
                            <div class="md-checkbox "><!--has-error-->
                              <input type="checkbox" value="Airport Hotel" id="checkbox2" class="md-check" name="hotel_type[]" >
                              <label for="checkbox2"> <span></span> <span class="check"></span> <span class="box"></span> Airport Hotel </label>
                            </div>
                            <div class="md-checkbox "><!--has-info-->
                              <input type="checkbox" value="Suite Hotel" id="checkbox3" class="md-check" name="hotel_type[]">
                              <label for="checkbox3"> <span></span> <span class="check"></span> <span class="box"></span> Suite Hotel</label>
                            </div>
                            <div class="md-checkbox "><!--has-info-->
                              <input type="checkbox" value="Extended Stay Hotel" id="checkbox4" class="md-check" name="hotel_type[]">
                              <label for="checkbox4"> <span></span> <span class="check"></span> <span class="box"></span> Extended Stay Hotel</label>
                            </div>
                            <div class="md-checkbox "><!--has-info-->
                              <input type="checkbox" value="Resort Hotel" id="checkbox5" class="md-check" name="hotel_type[]">
                              <label for="checkbox5"> <span></span> <span class="check"></span> <span class="box"></span> Resort Hotel</label>
                            </div>
                          </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group form-md-line-input">
                        <select class="form-control bs-select" id="" name="hotel_rooms" required>
                          <option value="" disabled="disabled" selected="selected">Select No of Rooms</option>
                          <option value="Under 20">Under 20</option>
                          <option value="20-49">20 - 49</option>
                          <option value="50-99">50 - 99</option>
                          <option value="100-199">100 - 199</option>
                          <option value="200-399">200 - 399</option>
                          <option value="400-699">400 - 699</option>
                          <option value="More than 700">More than 700</option>
                        </select>
                        <label></label>
                        <span class="help-block">No of Rooms *</span> </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group form-md-line-input">
                        <select class="form-control bs-select" id="st_logo" required onchange="return hideShowLogo();">
                          <option value="" disabled="disabled" selected="selected">Select Logo Applied?</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                        </select>
                        <label></label>
                        <span class="help-block">Logo Applied?</span> </div>
                    </div>
                    <div id="upload_logo" class="col-md-3">
                      <label class="control-label col-md-2">Upload Logo<span class="required">* </span></label>
                      <div class="col-md-3">
                        <input type="file" class="form-control" id="logo" name="image_photo" >
                        <span class="help-block">Upload Logo...</span> </div>
                    </div>
                    <div id="upload_text" class="col-md-3">
                      <div class="col-md-6"> </div>
                      <label class="control-label col-md-2">Logo As Text<span class="required">* </span></label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" id="text" name="images_text"  >
                        <span class="help-block">Enter Logo Text...</span> </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane tab-block" id="tab2">
                  <h3 class="block">Contact Details </h3>
                  <div class="row">
                    <label class="col-md-12">Hotel Address<span class="required">* </span></label>
                    <div class="col-md-3">
                        <div class="form-group form-md-line-input">                        
                          <input type="text" class="form-control" name="hotel_street1" placeholder="Street Details Line 1"  required="required"/>
                          <label></label>
                          <span class="help-block">Street Details Line 1 *</span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_street2" placeholder="Street Details Line 2"  required="required"/>
                          <label></label>
                          <span class="help-block">Street Details Line 2 *</span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_landmark" placeholder="Landmark" />
                          <label></label>
                          <span class="help-block">Landmark</span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input type="text" class="form-control" maxlength="6" name="hotel_pincode" id="pincode" placeholder="Pincode" onkeypress="return onlyNos(event,this);" required onblur='fetch_all_address_hotel("pincode","#g_country","#g_state","#g_city")' />
                          <label></label>
                          <span class="help-block">Pincode *</span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input type="text" class="form-control" id="g_city" name="hotel_district" placeholder="District"  required="required" />
                          <label></label>
                          <span class="help-block">District *</span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input type="text" class="form-control" id="g_state"  name="hotel_state" placeholder="State" required>
                          <label></label>
                          <span class="help-block">State *</span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input type="text" class="form-control"  id="g_country" name="hotel_country" placeholder="Country" required />
                          <label></label>
                          <span class="help-block">Country</span>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-md-12">Branch Office Address</label>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_branch_street1" placeholder="Street Details Line 1" />
                          <label></label>
                          <span class="help-block">Street Details Line 1 </span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_branch_street2" placeholder="Street Details Line 2" />
                          <label></label>
                          <span class="help-block">Street Details Line 2 </span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_branch_landmark" placeholder="Landmark" />
                          <label></label>
                          <span class="help-block">Landmark</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" maxlength="6" name="hotel_branch_pincode" id="pincode_branch" placeholder="Pincode" onkeypress="return onlyNos(event,this)" onblur='fetch_all_address_hotel("pincode_branch","#branch_country","#branch_state","#branch_city")' />
                          <label></label>
                          <span class="help-block">Pincode</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" id="branch_city" name="hotel_branch_district" placeholder="District"/>
                          <label></label>
                          <span class="help-block">District</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" id="branch_state"  name="hotel_branch_state" placeholder="State" />
                          <label></label>
                          <span class="help-block">State</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control"  id="branch_country" name="hotel_branch_country" placeholder="Country" />
                          <label></label>
                          <span class="help-block">Country</span>
                      	</div> 
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-md-12">Booking Office Address</label>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_booking_street1" placeholder="Street Details Line 1" />
                          <label></label>
                          <span class="help-block">Street Details Line 1 </span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_booking_street2" placeholder="Street Details Line 2" />
                          <label></label>
                          <span class="help-block">Street Details Line 2 </span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_booking_landmark" placeholder="Landmark" />
                          <label></label>
                          <span class="help-block">Landmark</span>
                      	</div> 
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" maxlength="6" id="pincode_booking" name="hotel_booking_pincode" placeholder="Pincode" onkeypress="return onlyNos(event,this)" onblur='fetch_all_address_hotel("pincode_booking","#booking_country","#booking_state","#booking_city")' />
                          <label></label>
                          <span class="help-block">Pincode</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" id="booking_city" name="hotel_booking_district"  placeholder="District"/>
                          <label></label>
                          <span class="help-block">District</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control"  id="booking_state" name="hotel_booking_state" placeholder="State"/>
                          <label></label>                          
                          <span class="help-block">State</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" id="booking_country"  name="hotel_booking_country" placeholder="Country" />
                          <label></label>
                          <span class="help-block">Country</span>
                      	</div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-md-12">Front Desk<span class="required">* </span></label>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_frontdesk_name" placeholder="Contact Person" required />
                          <label></label>
                          <span class="help-block">Contact Person</span>
                      	</div> 
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" required name="hotel_frontdesk_mobile" placeholder="Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10">
                          <label></label>
                          <span class="help-block">Mobile No.</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_frontdesk_mobile_alternative"  required="required" placeholder="Alternative Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
                          <label></label>
                          <span class="help-block">Alternative Mobile No.</span>
                      	</div> 
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="email" class="form-control" name="hotel_frontdesk_email" required placeholder="Email"/>
                          <label></label>
                          <span class="help-block">Email</span>
                      	</div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-md-12">Owner<span class="required">* </span></label>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_owner_name" required placeholder="Contact Person" />
                          <label></label>
                          <span class="help-block">Contact Person</span>
                      	</div> 
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_owner_mobile" placeholder="Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
                          <label></label>
                          <span class="help-block">Mobile No.</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_owner_mobile_alternative" placeholder="Alternative Mobile No." onkeypress="return onlyNos(event,this)" maxlength="10"/>
                          <label></label>
                          <span class="help-block">Alternative Mobile No.</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="email"  required="required" class="form-control" name="hotel_owner_email" placeholder="Email" />
                          <label></label>
                          <span class="help-block">Email</span>
                      	</div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-md-12">HR</label>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_hr_name" placeholder="Contact Person" />
                          <label></label>
                          <span class="help-block">Contact Person</span>
                      	</div>
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_hr_mobile" placeholder="Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
                          <label></label>
                          <span class="help-block">Mobile No.</span>
                      	</div>
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_hr_mobile_alternative" placeholder="Alternative Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10" />
                          <span class="help-block">Alternative Mobile No.</span>
                      	</div>
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_hr_email" placeholder="Email"/>
                          <label></label>
                          <span class="help-block">Email</span>
                      	</div> 
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-md-12">Accounts</label>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_accounts_name" placeholder="Contact Person" />
                          <span class="help-block">Contact Person</span>
                      	</div>
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_accounts_mobile" placeholder="Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10" />
                          <label></label>
                          <span class="help-block">Mobile No.</span>
                      	</div> 
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_accounts_mobile_alternative" placeholder="Alternative Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
                          <label></label>
                          <span class="help-block">Alternative Mobile No.</span>
                      	</div>
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_accounts_email" placeholder="Email" />
                          <label></label>
                          <span class="help-block">Email</span>
                      	</div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-md-12">Nearest Police Station<span class="required">* </span></label>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_near_police_name" placeholder="Contact Person"/>
                          <span class="help-block">Contact Person</span>
                      	</div>
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_near_police_mobile" placeholder="Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10" />
                          <label></label>
                          <span class="help-block">Mobile No.</span> 
                      	</div>
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_near_police_mobile_alternative" placeholder="Alternative Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10" />
                          <span class="help-block">Alternative Mobile No.</span>
                      	</div>
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_near_police_email"  placeholder="Email"/>
                          <label></label>
                          <span class="help-block">Email</span>
                      	</div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-md-12">Nearest Medial Establishment</label>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_near_medical_name" placeholder="Contact Person"/>
                          <label></label>
                          <span class="help-block">Contact Person</span>
                      	</div>
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_near_medical_mobile"  placeholder="Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10"/>
                          <span class="help-block">Mobile No.</span>
                      	</div>
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_near_medical_mobile_alternative" placeholder="Alternative Mobile No." onkeypress="return onlyNos(event,this)"/ maxlength="10" />
                          <span class="help-block">Alternative Mobile No.</span>
                      	</div> 
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_near_medical_email" placeholder="Email" />
                          <label></label>
                          <span class="help-block">Email</span>
                      	</div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_fax"  placeholder="Enter Fax"/>
                          <label></label>
                          <span class="help-block">Enter Fax</span>
                      	</div>
                    </div>
                    <div class="col-md-6">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="hotel_website" placeholder="Enter Website" />
                          <label></label>
                          <span class="help-block">Enter Website</span>
                      	</div>
                    </div>
                  </div>                  
                </div>
                <div class="tab-pane" id="tab3">
                  <h3 class="block">Add User</h3>
                  <div class="row">
                <div class="col-md-3">
                    <div class="form-group form-md-line-input">       
                      <select id="tax" onchange="return hideShow();" name="hotel_tax_applied" class="form-control bs-select">
                        <option>Select An Option</option>
                        <option>Yes</option>
                        <option>No</option>
                      </select>
                      <label></label>
                          <span class="help-block">Select Tax Applied?</span>
                    </div>
                </div>
                  <div id="service_tax">
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input name="fName" type="text" class="form-control" id="" required="required" placeholder="First Name *">
                          <label></label>
                          <span class="help-block">First Name *</span> </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input name="lName" type="text" class="form-control" id="" required="required" placeholder="Last Name *">
                          <label></label>
                          <span class="help-block">Last Name *</span> </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input" id="ckp">
                          <input name="uName" type="email" class="form-control" onblur="check_username(this.value)" id="uName" required="required" placeholder="email id *" >
                          <label></label>
                          <span class="help-block">Usename *</span> </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input name="contact" type="number" class="form-control" id="" required="required" placeholder="Contact No *">
                          <label></label>
                          <span class="help-block">Contact No *</span> </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input name="alt_contact" type="number" class="form-control" id="" placeholder="Alt Contact No">
                          <label></label>
                          <span class="help-block">Alt Contact No</span> </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input name="email" type="email" class="form-control" id="" required="required" placeholder="Email *">
                          <label></label>
                          <span class="help-block">Email *</span> </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input name="dept" type="text" class="form-control" id="" placeholder="Department">
                          <label></label>
                          <span class="help-block">Department</span> </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <select name="dept_role" class="form-control bs-select">
                            <option value="" disabled="disabled" selected="selected">Role</option>
                            <option value="Cashier">Cashier</option>
                            <option value="Accountant">Accountant</option>
                            <option value="Store Manager">Store Manager</option>
                          </select>
                          <label></label>
                          <span class="help-block">Role *</span> </div>
                      </div>
                      <!--<div class="col-md-3">
              <div class="form-group form-md-line-input">
                <select name="hotel_id[]" class="form-control bs-select" required="required" multiple>
                  <?php 
                 // $ho=$this->dashboard_model->total_hotels();
                  //foreach($ho as $ho){
                  ?>          
                  <option value="<?php //echo $ho->hotel_id;?>"><?php //echo  $ho->hotel_name;?></option>
                  <?php //} ?>				
                </select>
                <label></label><span class="help-block">Select Hotels *</span>
              </div>
            </div>-->
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <select name="admin_type" class="form-control bs-select" required>
                            <option value="" disabled="disabled" selected="selected">Admin level</option>
                            <option value="1">Super Admin</option>
                            <option value="2">Admin</option>
                            <option value="3">Sub Admin</option>
                          </select>
                          <label></label>
                          <span class="help-block">Admin level *</span> </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <select name="admin_role_id" class="form-control bs-select" required>
                            <option value="" disabled="disabled" selected="selected">Admin Role</option>
                           
                            <option value='1'>Super Admin</option>
                            
                          </select>
                          <label></label>
                          <span class="help-block">Admin Role *</span> </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <select name="profit_center[]" class="form-control bs-select" required multiple>
                            <option value="" disabled="disabled" selected="selected">Profit Centre</option>
                            <option value="1">Default Profit Center</option>
                          </select>
                          <label></label>
                          <span class="help-block">Profit Centre *</span> </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <input name="employee_id" type="text" class="form-control" id="" placeholder="Employee id">
                          <label></label>
                          <span class="help-block">Employee id</span> </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input">
                          <select name="class" class="form-control bs-select" required>
                            <option value="" disabled="disabled" selected="selected">Class</option>
                            <option value="Frontdesk">Frontdesk</option>
                            <option value="Cashier">Cashier</option>
                            <option value="Supervisor">Supervisor</option>
                            <option value="Executive">Executive</option>
                            <option value="Accountant">Accountant</option>
                            <option value="Facility Management">Facility Management</option>
                            <option value="Stakeholder">Stakeholder</option>
                          </select>
                          <label></label>
                          <span class="help-block">Class *</span> </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input" id="pd">
                          <input name="password" type="Password" class="form-control" id="pwd" required="required" placeholder="Password *"onblur="checkPassword(this.value)">
                          <label></label>
                          <span class="help-block">Password *</span> </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group form-md-line-input" id="pds">
                          <input name="cpassword" type="Password" class="form-control" id="cpwd" required="required" placeholder="Confirm Password *" onblur="confirmPassword(this.value)">
                          <label></label>
                          <span class="help-block">Confirm Password *</span> </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group form-md-line-input">
                          <textarea name="about_me" class="form-control" cols="" rows="" placeholder="About Me"></textarea>
                          <label></label>
                          <span class="help-block">About Me</span> </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="tab4">
                  <h3 class="block">Billing Settings</h3>
                  <div class="row">
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="billing_name" required placeholder="Enter Name On Invoice" />
                          <label></label>
                          <span class="help-block">Enter Name On Invoice</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="billing_street1" required placeholder="Street Details Line 1" />
                          <label></label>
                          <span class="help-block">Street Details Line 1 </span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="billing_street2" placeholder="Street Details Line 2" required />
                          <label></label>
                          <span class="help-block">Street Details Line 1 </span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="billing_landmark"  placeholder="Landmark"/>
                          <label></label>
                          <span class="help-block">Landmark</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="billing_district" required id="billing_city" placeholder="District" />
                          <label></label>
                          <span class="help-block">District</span>
                      	</div> 
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" maxlength="6" name="billing_pincode"  id="pincode_billing" required onkeypress="return onlyNos(event,this);" placeholder="Pincode"  onblur='fetch_all_address_hotel("pincode_billing","#billing_country","#billing_state","#billing_city")'/>
                          <label></label>
                          <span class="help-block">Pincode</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" required  id="billing_state" name="billing_state" placeholder="State" />
                          <label></label>
                          <span class="help-block">State</span>
                      	</div> 
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control"  name="billing_country" id="billing_country" required placeholder="Country" />
                          <label></label>
                          <span class="help-block">Country</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="email" required class="form-control" name="billing_email" placeholder="Enter Email" />
                          <label></label>
                          <span class="help-block">Enter Email</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="billing_phone" placeholder="Enter Phone No" required maxlength="10" onkeypress="return onlyNos(event,this)" />
                          <label></label>
                          <span class="help-block">Enter Phone No</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="billing_fax" placeholder="Enter Fax No" />
                          <label></label>
                          <span class="help-block">Enter Fax No</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="billing_vat" placeholder="Enter VAT No" />
                          <label></label>
                          <span class="help-block">Enter VAT No</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="billing_bank_name" required placeholder="Enter Bank Name" />
                          <label></label>
                          <span class="help-block">Enter Bank Name</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="billing_account_no" required onkeypress="return onlyNos(event,this);"  placeholder="Enter Account No"/>
                          <label></label>
                          <span class="help-block">Enter Account No ... </span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="billing_ifsc_code" placeholder="Enter IFSC Coad" />
                          <label></label>
                          <span class="help-block">Enter IFSC Coad</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="luxury_tax_reg_no" placeholder="Enter Luxury Tax Reg No" />
                          <label></label>
                          <span class="help-block">Enter IFSC Coad</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="service_tax_no" placeholder="Enter Service Tax No" />
                          <label></label>
                          <span class="help-block">Enter IFSC Coad</span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="vat_reg_no" placeholder="Enter Vat Reg No" />
                          <label></label>
                          <span class="help-block">Enter IFSC Coad ... </span>
                      	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="cin_no" placeholder="Enter CIN No" />
                          <label></label>
                          <span class="help-block">Enter IFSC Coad</span>
                            </div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group form-md-line-input">
                          <input type="text" class="form-control" name="tin_no" placeholder="Enter TIN No" />
                          <label></label>
                          <span class="help-block">Enter TIN Coad</span>
                      	</div>
                    </div>
                  </div>
                  
                  
                </div>
                <div class="tab-pane tab-block" id="tab5">
                  <h3 class="block">General Preferences</h3>
                  <div class="row">
                      <div class="col-md-6">
                      	<div class="row">
                            <label class="col-md-12">Default Check In Time</label>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control" name="hotel_check_in_time_hr" placeholder="HH" />
                                    <label></label>
                                    <span class="help-block">HH</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input">
                                  <input type="text" class="form-control" name="hotel_check_in_time_mm"  placeholder="MM"/>
                                  <label></label>
                                  <span class="help-block">Hotel Chain *</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input">
                                  <select class="form-control bs-select" name="hotel_check_in_time_fr">
                                    <option></option>
                                    <option>AM</option>
                                    <option>PM</option>
                                  </select>
                                  <label></label>
                                  <span class="help-block">Hotel Chain *</span>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                      	<div class="row">
                            <label class="col-md-12">Default Check Out Time</label>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input">
                                  <input type="text" class="form-control" name="hotel_check_out_time_hr" placeholder="HH" />
                                  <label></label>
                                  <span class="help-block">Hotel Chain *</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input">
                                  <input type="text" class="form-control" name="hotel_check_out_time_mm"  placeholder="MM"/>
                                  <label></label>
                                  <span class="help-block">Hotel Chain *</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input">
                                    <select class="form-control bs-select" name="hotel_check_out_time_fr">
                                        <option></option>
                                        <option>AM</option>
                                        <option>PM</option>
                                    </select>
                                    <label></label>
                                    <span class="help-block">Hotel Chain *</span>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group form-md-line-input">
                          <select class="form-control bs-select" required name="hotel_date_format" >
                            <option>Select Date Format</option>
                            <option>DD-MM-YYYY</option>
                            <option>MM-DD-YYYY</option>
                            <option>YYYY-MM-DD</option>
                          </select>
                          <label></label>
                          <span class="help-block">Select Date Format</span> </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group form-md-line-input">
                          <select class="form-control bs-select" required name="hotel_time_format"  >
                            <option>Select Date Format</option>
                            <option>12 Hours</option>
                            <option>24 Hours</option>
                          </select>
                          <span class="help-block">Select Time Format</span> </div>
                      </div>
                      <div class="col-md-12">
                      	 <div class="form-group form-md-line-input">
                            <label> Guest<span class="required">* </span></label>
                            <div class="md-checkbox-inline">
                                <div class="md-checkbox">
                                  <input type="checkbox"  value="Guest must be present in system for booking purpose" id="checkbox11" class="md-check" name="guest[]">
                                  <label for="checkbox11"> <span></span> <span class="check"></span> <span class="box"></span> Guest must be present in system for booking purpose</label>
                                </div>
                                <div class="md-checkbox">
                                  <input type="checkbox"  value="Take Duplicate Entry" id="checkbox12" class="md-check" name="guest[]">
                                  <label for="checkbox12"> <span></span> <span class="check"></span> <span class="box"></span> Take Duplicate Entry</label>
                                </div>
                                <div class="md-checkbox">
                                  <input type="checkbox"  value="Photo Mandatory" id="checkbox13" class="md-check" name="guest[]">
                                  <label for="checkbox13"> <span></span> <span class="check"></span> <span class="box"></span> Photo Mandatory</label>
                                </div>
                                <div class="md-checkbox">
                                  <input type="checkbox"  value="Id Mandatory" id="checkbox14" class="md-check" name="guest[]">
                                  <label for="checkbox14"> <span></span> <span class="check"></span> <span class="box"></span> Id Mandatory</label>
                                </div>
                             </div>
                         </div>
                      </div>
                      <div class="col-md-12">                        
                        <div class="form-group form-md-line-input">
                        	<label>Broker<span class="required">* </span></label>
                            <div class="md-checkbox-inline">
                                <div class="md-checkbox">
                                  <input type="checkbox"  value="Broker must be present in system for booking purpose" id="checkbox22" class="md-check" name="broker[]">
                                  <label for="checkbox22"> <span></span> <span class="check"></span> <span class="box"></span> Broker must be present in system for booking purpose</label>
                                </div>
                                <div class="md-checkbox">
                                  <input type="checkbox"  value="Photo Mandatory" id="checkbox23" class="md-check" name="broker[]">
                                  <label for="checkbox23"> <span></span> <span class="check"></span> <span class="box"></span> Photo Mandatory</label>
                                </div>
                                <div class="md-checkbox">
                                  <input type="checkbox"  value="Id Mandatory" id="checkbox24" class="md-check" name="broker[]">
                                  <label for="checkbox24"> <span></span> <span class="check"></span> <span class="box"></span> Id Mandatory</label>
                                </div>
                                <div class="md-checkbox">
                                  <input type="checkbox"  value="Change Commission%" id="checkbox25" class="md-check" name="broker[]">
                                  <label for="checkbox25"> <span></span> <span class="check"></span> <span class="box"></span> Change Commission%</label>
                                </div>
                             </div>
                        </div>
                      </div> 
                  </div>                 
                </div>
              </div>
            </div>
            <div class="form-actions right">
              <a href="javascript:;" class="btn default button-previous"> <i class="m-icon-swapleft"></i> Back </a> <a href="javascript:;" class="btn blue button-next" onclick="check();"> Continue <i class="m-icon-swapright m-icon-white"></i> </a> <a href="javascript:;" class="btn green button-submit" onclick="submit_form()"> Submit <i class="m-icon-swapright m-icon-white"></i> </a>
            </div>
          </div>
        </form>
      </div>
    </div>
 <script>
	 function check_username(name){
		  $.ajax({
                type:"POST",
                url: "<?php echo base_url()?>account/checkUserName",
                data:{userName:name},
                 success:function(data)
                {
					//alert(data.message);
					
			/*	if(data.status == 'fail'){
				    
				    swal("Dublicate Entry!", name+" Already Exist!", "success");
			    	$('#uName').val(''); 
				    
				}
				
*/

				 
                }
            })
	 }
	 </script>
<!-- END PAGE CONTENT-->