<script src="<?php echo base_url();?>assets/global/plugins/daypilot/js/daypilot/daypilot-all.min.js" type="text/javascript"></script>

<?php
/*if(isset($bookings_date) && $bookings){
    $av=0; $co=0; $ci=0; $ad=0; $con=0; $th=0;

    foreach($bookings as $booking){
    }
}*/

//echo "hello world";
?>
<div id="home">
<input type="hidden" id="txd" value="0">
  <div class="page-toolbar">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue-madison">
          <div class="visual"> <i class="fa fa-users" style="color: #242424;"></i> </div>
          <div class="details">
            <div class="number">
              
			  <?php   
			  $get_no = $this->super_model->count_chain();
			  if(isset($get_no) && $get_no!= ''){
				?>  
				  <span data-counter="counterup" data-value="<?php echo $get_no; ?>"></span>
				  <?php
			  }
			  else{
				  
				  ?>
				  <span data-counter="counterup" data-value="0"></span>
				  <?php
			  }
			  ?>
              
			  
              </div>
            <div class="desc"> All Chain </div>
          </div>
          <a class="more" href="<?php echo base_url() ?>account/view_chain_master"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red-intense">
          <div class="visual"> <i class="fa fa-inr" style="margin-left: -15px; color: #242424;"></i> </div>
          <div class="details">
            <div class="number">
             <?php   
			  $get_no = $this->super_model->count_child_account();
			  if(isset($get_no) && $get_no!= ''){
				?>  
				  <span data-counter="counterup" data-value="<?php echo $get_no; ?>"></span>
				  <?php
			  }
			  else{
				  
				  ?>
				  <span data-counter="counterup" data-value="0"></span>
				  <?php
			  }
			  ?>
            </div>
            <div class="desc"> Total Hotels </div>
          </div>
          <a class="more" href="<?php echo base_url() ?>account/view_child_account"> Details <i class="m-icon-swapright m-icon-white"></i> </a></a> </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green-haze">
          <div class="visual"> <i class="fa fa-book" style="color: #242424;"></i> </div>
          <div class="details">
            <div class="number">
                           
              <span data-counter="counterup" data-value="<?php 'test'; ?>">0</span>
              </div>
            <div class="desc"> New Bookings </div>
          </div>
          <a class="more" href="<?php echo base_url() ?>dashboard/all_bookings"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple-plum">
          <div class="visual"> <i class="fa fa-building" style="color: #242424;"></i> </div>
          <div class="details">
            <div class="number">
              
            </div>
            <div class="desc"> Room Vaccancy today </div>
          </div>
          <a class="more" href="<?php echo base_url() ?>dashboard/all_rooms"> Details <i class="m-icon-swapright m-icon-white"></i> </a> </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div id="booking_calendar">
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-green"> <i class="fa fa-table font-green"></i> <span class="caption-subject bold uppercase" style="letter-spacing: 1px;"> Reservation Management</span> </div>
          </div>
          <div class="portlet-body form">
            <form role="form">
              <div class="form-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="clearfix" style="margin-bottom:35px;">
                      <div class="btn-group" data-toggle="buttons">
                        <input type="hidden" id="hidden_field1" value="">
                        <label class="btn green dark-stripe">
                          <input id="5" type="checkbox" onchange="calendar()" class="toggle">
                          Detailed View</label>
                        <label class="btn default blue-stripe" style="margin-left:10px;">
                          <input id="7" type="checkbox" onchange="calendar2()" class="toggle">
                          Detailed View 2 </label>
                        <label class="btn default yellow-stripe" style="margin-left:10px;">
                          <input id="6" type="checkbox" onchange="snapshot()" class="toggle">
                          Snapshot View </label>
                      </div>
                      <button class="btn btn-warning pull-right" id="showfilter" type="button" style="padding:0;"><span class="tooltips"   data-style="default" data-container="body" data-original-title="Booking Filter" style="padding: 7px 14px; display:block;"><i class="fa fa-filter"></i></span></button>
                    </div>
                  </div>
                  <script type="text/javascript">
                       function calendar(){
                         window.location="<?php echo base_url(); ?>dashboard/add_booking_calendar";
                       }
                       function calendar2(){
                         window.location="<?php echo base_url(); ?>dashboard/calendar_load_2";
                       }
                       function snapshot(){
                         window.location="<?php echo base_url(); ?>dashboard/snapshot_load";
                       }
					   $('#showfilter').click(function(){
						   if($('#bookfilter').css('opacity') == 1)
						   {
							$('#bookfilter').css('height','0');
							$('#bookfilter').css('opacity','0');
							$('#bookfilter').css('overflow','hidden');
							$("#scl").val('500');
						   }
						   else
						   {
							$('#bookfilter').css('height','190px');
							$('#bookfilter').css('opacity','1');
							$('#bookfilter').css('overflow','visible');
							$("#scl").val('660');
						   }
						});
                   </script>
                  <div id="bookfilter" style="height:0; opacity:0; overflow:hidden;" class="col-md-12">
                  	<div class="row">
                        <div class="col-md-6 col-sm-6">
                          <div class="portlet light bordered">
                            <div class="row">
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Unit Type:</label>
                                  <?php $units=$this->unit_class_model->all_unit_type_category_data(); ?>
                                  <select  id="filter1" class="form-control input-sm bs-select">
                                    <?php if(isset($units) && $units): ?>
                                    <option value="all">All</option>
                                    <?php foreach($units as $unit): ?>
                                    <option value="<?php echo $unit->id; ?>"><?php echo $unit->name; ?></option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>No Of Beds:</label>
                                  <select id="filter4" class="form-control input-sm bs-select">
                                    <option value="all">All</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Unit Category :</label>
                                  <?php $units=$this->dashboard_model->all_units(); ?>
                                  <select  id="filter121" class="form-control input-sm bs-select">
                                    <?php if(isset($units) && $units): ?>
                                    <option value="all">All</option>
                                    <?php foreach($units as $unit): ?>
                                    <option value="<?php echo $unit->id; ?>"><?php echo $unit->unit_name; ?></option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Unit Class:</label>
                                  <select id="filter3" class="form-control input-sm bs-select">
                                    <option value="all" >All</option>
                                    <?php 
                                    $unit_class= $this->dashboard_model->get_unit_class_list();
                                    foreach($unit_class as $uc)
                                    {?>
                                    <option value="<?php echo $uc->hotel_unit_class_name;?>"><?php echo $uc->hotel_unit_class_name; ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Start date:</label>
                                  <div class="input-group">
                                    <input class="form-control input-md" id="start" type="text">
                                    <span class="input-group-btn"> <a class="btn green btn-md" onclick="picker.show(); return false;"><i class="fa fa-calendar-o"></i></a> </span> </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Time range:</label>
                                  <select id="timerange" class="form-control input-sm bs-select">
                                    <option value="week">Week</option>
                                    <option value="2weeks">2 Weeks</option>
                                    <option value="month" selected>Month</option>
                                    <option value="2months">2 Months</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                          <div class="portlet light bordered">
                            <h5 style="font-size:14px; font-weight:400;">Price Range</h5>
                            <div class="range" style="padding: 5px 0 13px;">
                              <input id="range_3" type="text" name="range_1" value=""/>
                            </div>
                            <button type="button" onclick="price_range()" Class="btn pink btn-sm">Search</button>
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-inline" style="margin-bottom:35px;">
                        <div class="form-group form-md-line-input">
                            <input id="filter_room_no" type="text" class="form-control" placeholder="Search Room"></input>
                            <label></label>
                            <span class="help-block">Search Room</span>
                        </div>
                        <div class="btn-group pull-right">
                          <label for="autocellwidth" class="auto_cl btn grey-cascade" id="auto_cl_id">
                            <input type="checkbox" id="autocellwidth" class="toggle" style="display:none;"><span id="disp">Expanded View</span></label>
                        </div>
                    </div>
                  </div>
                </div>
                
            
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-sm-6">
      <div class="portlet light ">
        <div class="portlet-title">
		
          <div class="caption"> <i class="icon-share font-blue-steel hide"></i> <span class="caption-subject font-blue-steel bold uppercase">Recent Bookings</span> </div>
        </div>
        <div class="portlet-body">
          <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
            <ul class="feeds">
            <?php               
			 if(isset($recent_bookings) && $recent_bookings):
			 
			 foreach($recent_bookings as $rb):
			?>
              <li>
                <div class="col1">
                  <div class="cont">
                    <div class="cont-col1">
					<?php 
					  $s_id=$rb->booking_status_id;
				     $color=$this->unit_class_model->status_color($s_id);
					
					?>
					
                      <div style="background-color:<?php echo $color->body_color_code; ?>;" class="label label-sm label-info"> <i style="color:<?php echo $color->bar_color_code; ?>;" class="fa fa-check"></i> </div>
                    </div>
                    <div class="cont-col2">
                      <div class="desc">
                   <?php 
                        $room_number=$this->dashboard_model->get_room_number($rb->room_id);
							//echo "<pre>";
							//print_r($room_number);
						echo $hotel_name->hotel_name; ?>: 
						<strong><?php echo $rb->cust_name; ?></strong> | ID: <?php echo $rb->booking_id; ?> 
                        <a href="<?php echo base_url();?>dashboard/booking_edit?b_id=<?php echo $rb->booking_id; ?>" class="btn btn-warning btn-xs"> Room No:
                        <?php 
                        if(isset($room_number)){
                        foreach($room_number as $r){ 
                        echo $r->room_no; 
		} }?>
                        <?php if($rb->group_id!=0){?><i class="fa fa-users" aria-hidden="true" style="font-size: 10px;"></i>
						<?php }?></a> 
</div> 				
                    </div>
                  </div>
                </div>
                <div class="col2">
                  <div class="date"> <?php echo $rb->stay_days ?> days
                    <?php
						$status=$this->dashboard_model->get_status($rb->booking_status_id);
						foreach($status as $stat){
						echo $stat->booking_status;
						}
					?>
                  </div>
                </div>
              </li>
              <?php endforeach;
              //endforeach;
                    endif;                                  
              ?>
            </ul>
          </div>
          <div class="scroller-footer">
            <div class="btn-arrow-link pull-right"> <a href="<?php echo base_url();?>dashboard/all_bookings">See All Bookings</a> <i class="icon-arrow-right"></i> </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-sm-6">
      <div class="portlet light tasks-widget">
        <div class="portlet-title">
          <div class="caption"> 
          <i class="icon-share font-green-haze hide"></i>
          <span class="caption-subject font-green-haze bold uppercase">Tasks</span> 
          <span class="caption-helper">tasks summary...</span> </div>
          <div class="actions">
            <div class="btn-group">
              <button class="btn green-haze btn-circle btn-sm"  data-toggle="modal" href="#responsive"> Add Task </button>
              <!-- /.modal -->
              <div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <?php
						$form = array(
							'class'             => 'form-body',
							'id'                => 'form',
							'method'            => 'post',
						);
						echo form_open_multipart('dashboard/add_task',$form);
					?>
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h4 class="modal-title">Assign a Task</h4>
                    </div>
                    <div class="modal-body">
                      <div class="scroller" style="height:200px" data-always-visible="1" data-rail-visible1="1">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group form-md-line-input">
                              <input type="text" autocomplete="off" name="title" class="form-control" id="title" placeholder="Title *" required="required">
                              <label></label><span class="help-block">Title *</span>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group form-md-line-input">
                                <select name="asign_from" id="booking_rent" class="form-control bs-select" required="required">
                                  <option value="" disabled selected>Assign From</option>
                                  <option value="<?php echo $this->session->userdata('user_id'); ?>"> <?php echo $this->session->userdata('user_name'); ?></option>
                                </select>
                                <label></label><span class="help-block">Assign From</span>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group form-md-line-input">
                                <select name="asign_to" id="booking_rent" class="form-control bs-select" required="required">
                                  <option value="" disabled selected>Assign To</option>
                                    <?php
										if(isset($tasks_assigee) && $tasks_assigee):
										foreach($tasks_assigee as $tasks_assigee):
									?>
                                  <option value="<?php echo $tasks_assigee->admin_id; ?>"> <?php echo $tasks_assigee->admin_first_name .' '. $tasks_assigee->admin_middle_name .' '. $tasks_assigee->admin_last_name; ?></option>
                                    <?php 
										endforeach; 
										endif;
									?>
                                </select>
                                <label></label><span class="help-block">Assign To</span>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group form-md-line-input">
                              <textarea name="task_desc" class="form-control" row="3" required="required" placeholder="Task Description *"></textarea>
                              <label></label><span class="help-block">Task Description *</span>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <select name="task_priority" id="task_priority" class="form-control bs-select" required="required">
                                  <option value="" disabled selected>Priority</option>
                                  <option value="1">High</option>
                                  <option value="2">Medium</option>
                                  <option value="3">Low</option>
                                </select>
                                <label></label><span class="help-block">Priority</span>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group form-md-line-input">
                                <input type="text" autocomplete="off" name="due_date" class="form-control date-picker" id="due_date" required="required">
                                <label></label><span class="help-block">Due Date</span>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" data-dismiss="modal" class="btn default">Close</button>
                      <button type="submit" class="btn green">Assign Task</button>
                    </div>
                    <?php form_close(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="portlet-body">
          <div class="task-content" id="task_manage">
            <div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="1"> 
              <!-- START TASK LIST -->
              <ul class="task-list">
                <?php
					if(isset($tasks_pending) && $tasks_pending):
					foreach($tasks_pending as $tasks_pending):

						$priority = $tasks_pending->priority;
						$from = $tasks_pending->from_id;
						$to = $tasks_pending->to_id;

						$from_details = $this->dashboard_model->get_task_user($from);
						$to_details = $this->dashboard_model->get_task_user($to);
						if ($priority == '1')                                           
                            {
                                $color = "#f3565d";
                                $priority = 'High';
                            }
                            else if ($priority == '2') 
                            {
                                $color = "#428bca";
                                $priority = 'Medium';
                            }
                            else
                            {
                                $color = "#45b6af";
                                $priority = 'Low';
                            }
				?>
                <li>
                  <div class="task-checkbox">
                    <?php if ($priority == '1') { ?>
                    <span class="label label-sm" style="background-color:<?php echo $color; ?>"><?php echo $priority; ?></span>
                    <?php } else if ($priority == '2') { ?>
                    <span class="label label-sm" style="background-color:<?php echo $color; ?>"><?php echo $priority; ?></span>
                    <?php  } else { ?>
                    <span class="label label-sm" style="background-color:<?php echo $color; ?>"><?php echo $priority; ?></span>
                    <?php  } ?>
                  </div>
                  <div class="task-title"> <span class="task-title-sp">
                  	<?php if ($priority == '1') { ?>
                  	<a href="<?php echo base_url(); ?>dashboard/task_details?task_id=<?php echo $tasks_pending->id ;?>&task_asignee=<?php echo $tasks_pending->to_id; ?>" style="color:<?php echo $color; ?>; font-weight: 500;"><?php echo  $tasks_pending->title; ?></a>  
                    <?php } else if ($priority == '2') { ?> 
                    <a href="<?php echo base_url(); ?>dashboard/task_details?task_id=<?php echo $tasks_pending->id ;?>&task_asignee=<?php echo $tasks_pending->to_id; ?>" style="color:<?php echo $color; ?>; font-weight: 500;"><?php echo  $tasks_pending->title; ?></a>  
                    <?php  } else { ?> 
                    <a href="<?php echo base_url(); ?>dashboard/task_details?task_id=<?php echo $tasks_pending->id ;?>&task_asignee=<?php echo $tasks_pending->to_id; ?>" style="color:<?php echo $color; ?>; font-weight: 500;"><?php echo  $tasks_pending->title; ?></a>    
                    <?php  } ?>            
                    <strong><?php							
					    if(isset($from_details) && $from_details){
							foreach ($from_details as $from_details){
								echo '('. $from_details->admin_first_name .' '. $from_details->admin_middle_name .' '. $from_details->admin_last_name;
							}
						}
						echo " <i class='fa fa-angle-double-right bounce'></i> ";
						if(isset($to_details) && $to_details) {
							foreach ($to_details as $to_details) {
								echo $to_details->admin_first_name .' '. $to_details->admin_middle_name .' '. $to_details->admin_last_name .')';
							}
						}
					?></strong>
                    <?php echo $tasks_pending->task; ?>
                    <i class="fa fa-exclamation-triangle" style="color:#c23f44;"></i> <span class="task-bell"><?php echo date('d/m/Y', strtotime($tasks_pending->due_date)); ?> </span> </span></div>
                  <div class="task-config">
                    <div class="task-config-btn btn-group"> 
                    <a href="<?php base_url()?>dashboard/complete_task?task_id=<?php echo $tasks_pending->id; ?>" class="btn green btn-sm" style="padding: 4px 3px 4px;"> <i class="fa fa-check"></i></a>
                    </div>
                  </div>
                </li>
                <?php 
					endforeach; 
					endif;
				?>
              </ul>
              <!-- END START TASK LIST --> 
            </div>
          </div>
          <div class="task-footer">
            <div class="btn-arrow-link pull-right"> <a href="<?php base_url()?>dashboard/all_tasks">See All Tasks</a> <i class="icon-arrow-right"></i> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-sm-6">
      <div class="portlet light tasks-widget">
        <div class="portlet-title">
          <div class="caption"> <span class="caption-subject font-blue-steel bold uppercase">Hotel Charts</span> </div>
        </div>
        <div class="portlet-body">
          <div class="task-content">
            <div class="input-group">
              <input class="form-control date-picker"  type="text" placeholder="Select Date" id="datepicker_date">
              <span class="input-group-btn">
              <button type="button" class="btn blue" onclick="pie_date()"> Go</button>
              </span> </div>
            <div id="chartdiv"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-sm-6">
      <div class="portlet light tasks-widget">
        <div class="portlet-title">
          <div class="caption"> <span class="caption-subject font-green-haze bold uppercase">Room Status Chart</span> </div>
        </div>
        <div class="portlet-body">
          <div class="task-content">
            <ul id="chart_table" class="list-group">
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="portlet light tasks-widget">
        <div class="portlet-title">
          <div class="caption"> <span class="caption-subject font-green-haze bold uppercase">Recent Booking & income Cont</span> </div>
        </div>
        <div class="portlet-body">
          <div class="task-content">
            <div id="chartdiv_one" style="width:100%; height:400px;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php 
			$color=$this->unit_class_model->booking_status_type();
			$flg=0;
			if(isset($color) && $color){
					foreach($color as $col){
						$name=$col->booking_status;
						$co=$col->bar_color_code;
						$flg++;
					?>
              <input type="hidden" id="hrow_<?php echo $flg; ?>"  value=<?php echo $co;?>>
             <?php }} ?>
<?php 
$Temporary_hold=$this->unit_class_model->status_color(1);
$advance=$this->unit_class_model->status_color(2);
$pending=$this->unit_class_model->status_color(3);
$confirmed=$this->unit_class_model->status_color(4);
$checkedin=$this->unit_class_model->status_color(5);
$checkedout=$this->unit_class_model->status_color(6);
$cancelled=$this->unit_class_model->status_color(7);
$incomplete=$this->unit_class_model->status_color(8);
$due=$this->unit_class_model->status_color(9);


 ?>
<input type="hidden" id="th" value=<?php echo $Temporary_hold->body_color_code; ?>>
<input type="hidden" id="adv" value=<?php echo $advance->body_color_code; ?>>
<input type="hidden" id="pen" value=<?php echo $pending->body_color_code; ?>>
<input type="hidden" id="conf" value=<?php echo $confirmed->body_color_code; ?>>
<input type="hidden" id="chkin" value=<?php echo $checkedin->body_color_code; ?>>
<input type="hidden" id="thkout" value=<?php echo $checkedout->body_color_code; ?>>
<input type="hidden" id="can" value=<?php echo $cancelled->body_color_code; ?>>
<input type="hidden" id="inc" value=<?php echo $incomplete->body_color_code; ?>>
<input type="hidden" id="due" value=<?php echo $due->body_color_code; ?>>


<script type="text/javascript">
var th_color=$('#th').val();
		var adv_color=$('#adv').val();
		var pen_color=$('#pen').val();
		var conf_color=$('#conf').val();
		var chkin_color=$('#chkin').val();
		var thkout_color=$('#thkout').val();
		var can_color=$('#can').val();
		var inc_color=$('#inc').val();
		var due_color=$('#due').val();
function update_task(val){
    var result = confirm("are you want to delete this task?");
    var linkurl = '<?php echo base_url() ?>dashboard/update_task/'+val;
    if (result) {
        alert(linkurl);
    }
    
}

$(function () {
	$('#datepicker2').datepicker({
		locale: 'ru'
	});
});

    var chart;
    var chart2;
    var av;
    var co;
    var ci;
    var ad;
    var con;
    var th;
    var chartData;

    function pie_date() {
        var date = document.getElementById('datepicker_date').value;
        if(date!=""){



        // PIE CHART
        chart = new AmCharts.AmPieChart();
		
		

        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>dashboard/all_bookings_date",
                datatype:'json',
                data:{date:date},
                success:function(data)
                {document.getElementById("chart_table").innerHTML='<li class="list-group-item note">Available<span class="badge badge-success badge-roundless">'+data.av+'</span></li>'+
				' <li  id="row1" class="list-group-item note" style="background-color:'+thkout_color+';">Checked Out<span class="badge badge-warning badge-roundless">'+data.cho+'</span></li>'+
				' <li  class="list-group-item note" style="background-color:'+chkin_color+';">Checked In<span class="badge badge-info badge-roundless">'+data.chi+'</span></li>'+
				' <li class="list-group-item note"style="background-color:'+adv_color+';" >Advance<span class="badge badge-danger badge-roundless">'+data.advnce+'</span></li>'+
				' <li class="list-group-item note" style="background-color:'+conf_color+';">Confirmed<span class="badge badge-success badge-roundless">'+data.con+'</span></li> '+
				'<li class="list-group-item note" style="background-color:'+th_color+';">Temporary Hold<span class="badge badge-danger badge-roundless">'+data.thld+'</span></li>';





                    //load chart data dynamically

                    //  av=data.av;

                    chartData = [
                       {
                            "country": "",
                            "visits": data.av

                        },
                        {
                            "country": "",
                            "visits": data.cho
                        },
                        {
                            "country": "",
                            "visits": data.chi
                        },
                        {
                            "country": "",
                            "visits": data.advnce
                        },
                        {
                            "country": "",
                            "visits": data.con
                        },
                        {
                            "country": "",
                            "visits": data.thld
                        },

                    ];

                    //alert(chartData);


                    // title of the chart
                    chart.dataProvider = chartData;
                    chart.titleField = "country";
                    chart.valueField = "visits";
                    chart.sequencedAnimation = true;
                    chart.startEffect = "elastic";
                    chart.innerRadius = "30%";
                    chart.startDuration = 2;
                    chart.labelRadius = 15;
                    chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                    // the following two lines makes the chart 3D
                    chart.depth3D = 10;
                    chart.angle = 15;

                    // WRITE
                    chart.write("chartdiv");

                }
            });
    }}

    $( document ).ready(function() {
		
 //alert(thkout_color);
        chart = new AmCharts.AmPieChart();
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>dashboard/all_bookings_date_today",
                datatype:'json',
                data:{date:"abc"},
                success:function(data)
                { document.getElementById("chart_table").innerHTML='<li id="row0" class="list-group-item note" >Available<span class="badge badge-success badge-roundless">'+data.av+'</span></li>'+
				'<li id="row1" class="list-group-item note"  style="background-color:'+thkout_color+';">Checked Out<span class="badge badge-warning badge-roundless">'+data.cho+'</span></li>'+
				'<li id="row3"class="list-group-item note"  style="background-color:'+chkin_color+';">Checked In<span class="badge badge-info badge-roundless">'+data.chi+'</span></li>'+
				'<li id="row4"class="list-group-item note"  style="background-color:'+adv_color+';">Advance<span class="badge badge-danger badge-roundless">'+data.advnce+'</span></li>'+
				'<li id="row5"class="list-group-item note"  style="background-color:'+conf_color+';">Confirmed<span class="badge badge-success badge-roundless">'+data.con+'</span></li>'+
				'<li id="row6" class="list-group-item note"  style="background-color:'+th_color+';">Temporary Hold<span class="badge badge-danger badge-roundless">'+data.thld+'</span></li>';
				var a=1;				
				var b=$('#hrow_'+a).val();
				//alert(b);
				document.getElementById("row"+a).style.backgroundColor = b;
                    chartData = [
                        {
                            "country": "",
                            "visits": data.av

                        },
                        {
                            "country": "",
                            "visits": data.cho
                        },
                        {
                            "country": "",
                            "visits": data.chi
                        },
                        {
                            "country": "",
                            "visits": data.advnce
                        },
                        {
                            "country": "",
                            "visits": data.con
                        },
                        {
                            "country": "",
                            "visits": data.thld
                        },

                    ];
				
                    //alert(chartData);
                    // title of the chart
                    chart.dataProvider = chartData;
                    chart.titleField = "country";
                    chart.valueField = "visits";
                    chart.sequencedAnimation = true;
                    chart.startEffect = "elastic";
                    chart.innerRadius = "30%";
                    chart.startDuration = 2;
                    chart.labelRadius = 15;
                    chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                    // the following two lines makes the chart 3D
                    chart.depth3D = 10;
                    chart.angle = 15;

                    // WRITE
                    chart.write("chartdiv");

                }
            });
    });
	
</script> 
<script>
    var chart2;
    var chartData;
    $( document ).ready(function() {
        jQuery.ajax(
            {
                type:"POST",
                url:"<?php echo base_url(); ?>dashboard/ten_days_revenue_bookings",
                datatype:'json',
                data:{date:"abc"},
                success:function(data1)
                {
  
        chart2 = new AmCharts.AmSerialChart();

        chart2.dataProvider = data1; //chartData;
        chart2.categoryField = "year";
        chart2.startDuration = 1;

        chart2.handDrawn = true;
        chart2.handDrawnScatter = 3;

        // AXES
        // category
        var categoryAxis = chart2.categoryAxis;
        categoryAxis.gridPosition = "start";

        // value
        var valueAxis = new AmCharts.ValueAxis();
        valueAxis.axisAlpha = 0;
        chart2.addValueAxis(valueAxis);

        // GRAPHS
        // column graph
        var graph1 = new AmCharts.AmGraph();
        graph1.type = "column";
        graph1.title = "Income";
        graph1.lineColor = "#E27979";
        graph1.valueField = "income";
        graph1.lineAlpha = 1;
        graph1.fillAlphas = 1;
        graph1.dashLengthField = "dashLengthColumn";
        graph1.alphaField = "alpha";
        graph1.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]] Thousands</b> [[additional]]</span>";
        chart2.addGraph(graph1);

        // line
        var graph2 = new AmCharts.AmGraph();
        graph2.type = "line";
        graph2.title = "New Bookings";
        graph2.lineColor = "#18B2C5";
        graph2.valueField = "expenses";
        graph2.lineThickness = 3;
        graph2.bullet = "round";
        graph2.bulletBorderThickness = 3;
        graph2.bulletBorderColor = "#18B2C5";
        graph2.bulletBorderAlpha = 1;
        graph2.bulletColor = "#ffffff";
        graph2.dashLengthField = "dashLengthLine";
        graph2.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        chart2.addGraph(graph2);

        // LEGEND
        var legend = new AmCharts.AmLegend();
        legend.useGraphSettings = true;
        chart2.addLegend(legend);

        // WRITE
        chart2.write("chartdiv_one");
                }
            });


    },10000);
	
$(window).scroll(function(){
  var scl = $("#scl").val();	
  var sticky = $('#dp'),
      scroll = $(window).scrollTop();

  if (scroll >= scl) sticky.addClass('fixed');
  else sticky.removeClass('fixed');  
});
$(window).ready(function(){
	$( ".scheduler_default_main > div:nth-child(3) > div:nth-child(1)" ).addClass( "sdv");
	$( ".scheduler_default_main .sdv" ).wrap( "<div class='aa'></div>" );
});


var myEvent = window.attachEvent || window.addEventListener;
var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload';
myEvent(chkevent, function (e) {
var booking_id = document.getElementById('txd').value;
if( booking_id > 0 ){
	 $.ajax({
	  url: "<?php echo base_url()?>bookings/cancel_booking",
	  type: "POST",
	  data:{booking_id:booking_id},
	  success: function(data){
		//console.log(data);
	  }        
	  });				
}		 
 //console.log(document.getElementById('txd').value);
});
</script>  