var TableDatatablesButtons = function() {
    var e = function() {
            var e = $("#sample_1"),
				a = 0;
			App.getViewPort().width < App.getResponsiveBreakpoint("md") ? $(".page-header").hasClass("page-header-fixed-mobile") && (a = $(".page-header").outerHeight(!0)) : $(".page-header").hasClass("navbar-fixed-top") ? a = $(".page-header").outerHeight(!0) : $("body").hasClass("page-header-fixed") && (a = 64);
            e.dataTable({
                language: {
                    aria: {
                        sortAscending: ": activate to sort column ascending",
                        sortDescending: ": activate to sort column descending"
                    },
                    emptyTable: "No data available in table",
                    info: "Showing _START_ to _END_ of _TOTAL_ entries",
                    infoEmpty: "No entries found",
                    infoFiltered: "(filtered1 from _MAX_ total entries)",
                    lengthMenu: "_MENU_ ",
                    search: "",
                    zeroRecords: "No matching records found"
                },
                buttons: [{
                    extend: "print",
                    className: "btn blue-hoki btn-outline",
					text: "<i class='fa fa-print'></i>"
                }, {
                    extend: "copy",
                    className: "btn red btn-outline",
					text: "<i class='fa fa-copy'></i>"
                }, {
                    extend: "pdf",
                    className: "btn green btn-outline",
					text: "<i class='fa fa-file-pdf-o'></i>"
                }, {
                    extend: "excel",
                    className: "btn yellow btn-outline ",
					text: "<i class='fa fa-file-excel-o'></i>"
                }, {
                    extend: "csv",
                    className: "btn purple btn-outline ",
					text: "<i class='fa fa-file-text-o'></i>"
                }, {
                    extend: "colvis",
                    className: "btn dark btn-outline",
                    text: "<i class='fa fa-filter'></i>"
                }],
				fixedHeader: {
                    header: !0,
					headerOffset: a
                },
                responsive: !0,
				colReorder: {
                    reorderCallback: function() {
                        console.log("callback")
                    }
                },
                order: [
                    [0, "asc"]
                ],
                lengthMenu: [
                    [10, 20, 50, 100, 200, 500, -1],
                    [10, 20, 50, 100, 200, 500, "All"]
                ],
                pageLength: 10,
                dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
            })
        },
        t = function() {
            var e = $("#sample_2"),
				a = 0;
			App.getViewPort().width < App.getResponsiveBreakpoint("md") ? $(".page-header").hasClass("page-header-fixed-mobile") && (a = $(".page-header").outerHeight(!0)) : $(".page-header").hasClass("navbar-fixed-top") ? a = $(".page-header").outerHeight(!0) : $("body").hasClass("page-header-fixed") && (a = 64);
            e.dataTable({
                language: {
                    aria: {
                        sortAscending: ": activate to sort column ascending",
                        sortDescending: ": activate to sort column descending"
                    },
                    emptyTable: "No data available in table",
                    info: "Showing _START_ to _END_ of _TOTAL_ entries",
                    infoEmpty: "No entries found",
                    infoFiltered: "(filtered1 from _MAX_ total entries)",
                    lengthMenu: "_MENU_ ",
                    search: "",
                    zeroRecords: "No matching records found"
                },
                buttons: [{
                    extend: "print",
                    className: "btn blue-hoki btn-outline",
					text: "<i class='fa fa-print'></i>"
                }, {
                    extend: "copy",
                    className: "btn red btn-outline",
					text: "<i class='fa fa-copy'></i>"
                }, {
                    extend: "pdf",
                    className: "btn green btn-outline",
					text: "<i class='fa fa-file-pdf-o'></i>"
                }, {
                    extend: "excel",
                    className: "btn yellow btn-outline ",
					text: "<i class='fa fa-file-excel-o'></i>"
                }, {
                    extend: "csv",
                    className: "btn purple btn-outline ",
					text: "<i class='fa fa-file-text-o'></i>"
                }, {
                    extend: "colvis",
                    className: "btn dark btn-outline",
                    text: "<i class='fa fa-filter'></i>"
                }],
				fixedHeader: {
                    header: !0,
					headerOffset: a
                },
				responsive: !0,
                order: [
                    [0, "asc"]
                ],
                lengthMenu: [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],
                pageLength: 10,
                dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
            })
        },
		q = function() {
            var e = $("#sample_3"),
				a = 0;
			App.getViewPort().width < App.getResponsiveBreakpoint("md") ? $(".page-header").hasClass("page-header-fixed-mobile") && (a = $(".page-header").outerHeight(!0)) : $(".page-header").hasClass("navbar-fixed-top") ? a = $(".page-header").outerHeight(!0) : $("body").hasClass("page-header-fixed") && (a = 64);
            e.dataTable({
                language: {
                    aria: {
                        sortAscending: ": activate to sort column ascending",
                        sortDescending: ": activate to sort column descending"
                    },
                    emptyTable: "No data available in table",
                    info: "Showing _START_ to _END_ of _TOTAL_ entries",
                    infoEmpty: "No entries found",
                    infoFiltered: "(filtered1 from _MAX_ total entries)",
                    lengthMenu: "_MENU_ ",
                    search: "",
                    zeroRecords: "No matching records found"
                },
                buttons: [{
                    extend: "print",
                    className: "btn blue-hoki btn-outline",
					text: "<i class='fa fa-print'></i>"
                }, {
                    extend: "copy",
                    className: "btn red btn-outline",
					text: "<i class='fa fa-copy'></i>"
                }, {
                    extend: "pdf",
                    className: "btn green btn-outline",
					text: "<i class='fa fa-file-pdf-o'></i>"
                }, {
                    extend: "excel",
                    className: "btn yellow btn-outline ",
					text: "<i class='fa fa-file-excel-o'></i>"
                }, {
                    extend: "csv",
                    className: "btn purple btn-outline ",
					text: "<i class='fa fa-file-text-o'></i>"
                }, {
                    extend: "colvis",
                    className: "btn dark btn-outline",
                    text: "<i class='fa fa-filter'></i>"
                }],
				fixedHeader: {
                    header: !0,
					headerOffset: a
                },
                order: [
                    [0, "asc"]
                ],
                lengthMenu: [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],
                pageLength: 10,
                dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
            })
        },
		j = function() {
            var e = $("#sample_4");
			    e.dataTable({
                language: {
                    aria: {
                        sortAscending: ": activate to sort column ascending",
                        sortDescending: ": activate to sort column descending"
                    },
                    emptyTable: "No data available in table",
                    info: "Showing _START_ to _END_ of _TOTAL_ entries",
                    infoEmpty: "No entries found",
                    infoFiltered: "(filtered1 from _MAX_ total entries)",
                    lengthMenu: "_MENU_ ",
                    search: "",
                    zeroRecords: "No matching records found"
                },
                buttons: [{
                    extend: "print",
                    className: "btn blue-hoki btn-outline",
					text: "<i class='fa fa-print'></i>"
                }, {
                    extend: "copy",
                    className: "btn red btn-outline",
					text: "<i class='fa fa-copy'></i>"
                }, {
                    extend: "pdf",
                    className: "btn green btn-outline",
					text: "<i class='fa fa-file-pdf-o'></i>"
                }, {
                    extend: "excel",
                    className: "btn yellow btn-outline ",
					text: "<i class='fa fa-file-excel-o'></i>"
                }, {
                    extend: "csv",
                    className: "btn purple btn-outline ",
					text: "<i class='fa fa-file-text-o'></i>"
                }, {
                    extend: "colvis",
                    className: "btn dark btn-outline",
                    text: "<i class='fa fa-filter'></i>"
                }],
                order: [
                    [0, "asc"]
                ],
                lengthMenu: [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],
                pageLength: 10,
                dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
            })
        };
    return {
        init: function() {
            jQuery().dataTable && (e(), t(), q(), j())
        }
    }
}();
jQuery(document).ready(function() {
    TableDatatablesButtons.init()
});